START TRANSACTION;

create table brokerage.advisor_statistics (
  id BIGINT PRIMARY KEY,
  advisor_id BIGINT NOT NULL,
  creation_date date NOT NULL DEFAULT now()::DATE,
  advised_lead_count INT NOT NULL DEFAULT 0,
  db_creation_date TIMESTAMP(3) NOT NULL DEFAULT NOW(),
  db_modify_date TIMESTAMP(3) NOT NULL DEFAULT NOW(),
  CONSTRAINT advisor_creation_date_unique UNIQUE (advisor_id, creation_date),
  CONSTRAINT advisor_counter_fk_advisor_id FOREIGN KEY (advisor_id) REFERENCES "public".advisor(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

select public.add_trigger_modify_date('brokerage','{"advisor_statistics"}'::text[]);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'iivaniv',
  'migration.AF-1071.sql',
  'Create advisor_statistics for advisor statistic',
  '6.335'
);

COMMIT;

-- START TRANSACTION;
-- DROP TABLE public.advisor_statistics EXISTS;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'iivaniv',
--   'migration.AF-1071.sql',
--   'Revert migration.AF-1071.sql Create advisor_statistics for advisor statistic',
--   '6.335'
-- );
--
-- COMMIT;