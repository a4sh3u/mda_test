START TRANSACTION;

CREATE TABLE IF NOT EXISTS customer_assignment
(
  id                BIGINT NOT NULL
    CONSTRAINT customer_assignment_id_pk
    PRIMARY KEY,
  customer_number   BIGINT NOT NULL UNIQUE,
  credit_advisor_id BIGINT NOT NULL,
  db_creation_date  TIMESTAMP NOT NULL DEFAULT NOW(),
  db_modify_date    TIMESTAMP NOT NULL DEFAULT NOW()
);

select public.add_trigger_modify_date('public','{"customer_assignment"}'::text[]);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mmaksymovych',
  'migration.AF-1413_0001.sql',
  'AF-1413 - Two advisors shouldn''t pick the same customer',
  '6.325'
);

COMMIT;


-- START TRANSACTION;
--
-- DROP TABLE IF EXISTS customer_assignment;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'mmaksymovych',
--   'rollback.AF-1413_0001.sql',
--   'AF-1413 - Two advisors shouldn''t pick the same customer',
--   '6.325'
-- );
--
-- COMMIT;