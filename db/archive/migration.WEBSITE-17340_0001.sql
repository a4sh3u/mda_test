DROP VIEW IF EXISTS borrower_search;

CREATE OR REPLACE VIEW borrower_search AS
  SELECT
    acc.id AS id,
    la.id as loan_application_id,
    ps.id AS customer_process_status_id
  FROM account acc
    INNER JOIN customer_process_status ps on ps.account_id = acc.id
    LEFT JOIN loan_application la ON la.account_id = acc.id AND la.creation_date =
      (SELECT max(loan_application.creation_date) AS max FROM loan_application WHERE loan_application.account_id = acc.id)
  WHERE ps.expiration_date IS NULL;

-- ALTER TABLE borrower_search OWNER TO smava;
-- GRANT ALL ON TABLE borrower_search TO smava;
-- GRANT SELECT ON TABLE borrower_search TO smava_read;

-- Index on loan application
CREATE INDEX "loan_appication_account_creation_date"
ON loan_application
USING btree
(account_id, creation_date);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pvitic',
  'migration.WEBSITE-17340_0001.sql',
  'WEBSITE-17340 - Casi borrower search view by customer status and loan application index',
  '5.17'
);
