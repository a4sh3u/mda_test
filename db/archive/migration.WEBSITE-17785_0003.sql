-- =================== --
-- uniques constraints --
-- =================== --
ALTER TABLE brokerage.household_calculation ADD CONSTRAINT unique_type_name UNIQUE ("type", "name");
ALTER TABLE brokerage.bank_household_calculation ADD CONSTRAINT unique_household_bank UNIQUE (brokerage_bank_id, household_calculation_id);


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pvitic',
  'migration.WEBSITE-17785_0003.sql', 'WEBSITE-17785 - Unique constraints',
  '5.20'
);