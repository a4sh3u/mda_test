-- Table: brokerage.customer_uploaded_doc

-- DROP TABLE brokerage.customer_uploaded_doc;

CREATE TABLE brokerage.customer_uploaded_doc
(
  id bigint NOT NULL,
  account_id bigint,
  creation_date timestamp with time zone,
  deletion_date timestamp with time zone,
  status character varying(255),
  external_storage_id character varying(255),
  category character varying(255),
  file_name character varying(255),
  applicant_id bigint,
  CONSTRAINT customer_uploaded_doc_pkey PRIMARY KEY (id),
  CONSTRAINT "customer_uploaded_doc_account_FK" FOREIGN KEY (account_id)
      REFERENCES brokerage.account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);

-- Index: brokerage.customer_uploaded_doc_account

-- DROP INDEX brokerage.customer_uploaded_doc_account;

CREATE INDEX customer_uploaded_doc_account
  ON brokerage.customer_uploaded_doc
  USING btree
  (account_id);



-- Table: brokerage.brokerage_uploaded_doc

-- DROP TABLE brokerage.brokerage_uploaded_doc;

CREATE TABLE brokerage.brokerage_uploaded_doc
(
  id bigint NOT NULL,
  document_id bigint,
  brokerage_bank_id bigint,
  status character varying(50) NOT NULL,
  send_date timestamp with time zone NOT NULL,
  receive_date timestamp with time zone,
  advisor_id bigint NOT NULL,
  deletion_date timestamp with time zone,
  CONSTRAINT brokerage_uploaded_doc_pkey PRIMARY KEY (id),
  CONSTRAINT "brokerage_uploaded_doc_brokerage_FK" FOREIGN KEY (brokerage_bank_id)
      REFERENCES brokerage.brokerage_bank (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT "brokerage_uploaded_doc_doc_FK" FOREIGN KEY (document_id)
      REFERENCES brokerage.customer_uploaded_doc (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);
-- Index: brokerage.brokerage_uploaded_doc_brokerage

-- DROP INDEX brokerage.brokerage_uploaded_doc_brokerage;

CREATE INDEX brokerage_uploaded_doc_brokerage
  ON brokerage.brokerage_uploaded_doc
  USING btree
  (brokerage_bank_id);

-- Index: brokerage.brokerage_uploaded_doc_doc

-- DROP INDEX brokerage.brokerage_uploaded_doc_doc;

CREATE INDEX brokerage_uploaded_doc_doc
  ON brokerage.brokerage_uploaded_doc
  USING btree
  (document_id);



INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'asaadat',
  'migration.WEBSITE-17420_0001.sql',
  'WEBSITE-17420 - expand database for document upload',
  '5.21'
);