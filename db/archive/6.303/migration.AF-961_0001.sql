START TRANSACTION;

create table IF NOT EXISTS brokerage.brokerage_denied_offline_reason
(
  id bigint not null
    constraint brokerage_denied_offline_reason_pk
    primary key,

  advisor_notification_id BIGINT,
  reason_code varchar(255) not null,
  reason_description varchar(255),
  db_creation_date       TIMESTAMP NOT NULL DEFAULT NOW(),
  db_modify_date         TIMESTAMP NOT NULL DEFAULT NOW(),
  FOREIGN KEY (advisor_notification_id)
  REFERENCES brokerage.advisor_notification(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE
)
;

select public.add_trigger_modify_date('brokerage','{"brokerage_denied_offline_reason"}'::text[]);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mmaksymovych',
  'migration.AF-961_0001.sql',
  'AF-961 - As a ca I want to choose a denied offline reason',
  '6.303'
);

COMMIT;


-- START TRANSACTION;
--
-- DROP TABLE brokerage.brokerage_denied_offline_reason
--
-- nextval('"MAIN_SEQUENCE"'),
-- (now()),
-- 'mmaksymovych',
-- 'migration.AF-961_0001.sql',
-- 'REVERT AF-961 - As a ca I want to choose a denied offline reason',
-- '6.303'
-- );
--
-- COMMIT;