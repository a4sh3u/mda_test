START TRANSACTION;

ALTER TABLE brokerage.brokerage_bank ADD COLUMN status_update VARCHAR(255) NULL;

ALTER TABLE brokerage.brokerage_bank ADD COLUMN db_modified_date TIMESTAMPTZ;

INSERT INTO database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'WTeperek',
    'migration.SME-6017_0001.sql',
    'SME-6017 Bank status update method name indicator',
    '6.229'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_bank DROP COLUMN db_modified_date;
--
-- ALTER TABLE brokerage.brokerage_bank DROP COLUMN status_update;
--
-- INSERT INTO database_history VALUES (
--     nextval('public."MAIN_SEQUENCE"'),
--     (now()),
--     'WTeperek',
--     'migration.SME-6017_0001.sql',
--     'REVERT - SME-6017 Bank status update method name indicator',
--     '6.229'
-- );
--
-- COMMIT;