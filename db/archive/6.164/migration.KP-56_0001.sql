CREATE INDEX bank_account_iban
  ON public.bank_account
  USING btree
  (iban COLLATE pg_catalog."default");

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mbiletskyi',
  'migration.KP-56_0001.sql',
  'KP-56_0001 - Add additional search fields in backoffice',
  '6.164'
);