DROP INDEX public.bank_account_iban;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mbiletskyi',
      'rollback.KP-56_0001.sql',
      'KP-56_0001 - Add additional search fields in backoffice',
      '6.164'
);