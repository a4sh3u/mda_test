DROP INDEX IF EXISTS brokerage_journal_test;

CREATE INDEX "brokerage_journal_account_and_type"
  ON public.brokerage_journal
  USING btree
  (account_id, "type");

CREATE INDEX "brokerage_application_rule_engine_result_N01"
  ON public.brokerage_application_rule_engine_result
  USING btree
  (brokerage_application_id);

CREATE INDEX "brokerage_application_rule_engine_result_N02"
  ON public.brokerage_application_rule_engine_result
  USING btree
  (brokerage_application_id,creation_date);

CREATE INDEX "event_queue_N52"
  ON public.event_queue
  USING btree
  (due_date, expiration_date, account_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mpetrick',
  'migration.WEBSITE-17154_0001.sql',
  'WEBSITE-17154 - Introduce indeces for CASI',
  null
);