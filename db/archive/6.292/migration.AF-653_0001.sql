START TRANSACTION;

ALTER TABLE brokerage.brokerage_journal ADD COLUMN content text;
ALTER TABLE brokerage.brokerage_journal ADD COLUMN subject character varying(255);
ALTER TABLE brokerage.brokerage_journal ADD COLUMN direction character varying(255);
ALTER TABLE brokerage.brokerage_journal ADD COLUMN external_ref_id character varying(255);


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'ovolkovsky',
  'migration.AF-653_001.sql',
  'Move Brokerage Journal from public to brokerage',
  '6.292'
);

COMMIT;

--  START TRANSACTION;

--  ALTER TABLE brokerage.brokerage_journal DROP COLUMN IF EXISTS content ;
--  ALTER TABLE brokerage.brokerage_journal DROP COLUMN IF EXISTS subject ;
--  ALTER TABLE brokerage.brokerage_journal DROP COLUMN IF EXISTS direction ;
--  ALTER TABLE brokerage.brokerage_journal DROP COLUMN IF EXISTS external_ref_id ;

-- INSERT INTO database_history VALUES (
--  nextval('"MAIN_SEQUENCE"'),
--  (now()),
--  'ovolkovsky',
--  'rollback.AF-653_001.sql',
--  'Move Brokerage Journal from public to brokerage',
--  '6.292'
-- );
-- COMMIT;