START TRANSACTION;

ALTER TABLE brokerage.brokerage_journal RENAME COLUMN account_id TO customer_number;

ALTER TABLE brokerage.brokerage_journal DROP COLUMN document_id;

ALTER TABLE public.account ADD CONSTRAINT unigue_person UNIQUE (account_id);

ALTER TABLE brokerage.brokerage_journal ADD CONSTRAINT "brokerage_journal_FK1" FOREIGN KEY (customer_number) REFERENCES public.account (account_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE brokerage.brokerage_journal ADD CONSTRAINT "brokerage_journal_FK2" FOREIGN KEY (advisor_id) REFERENCES public.account (id) MATCH SIMPLE  ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE brokerage.brokerage_journal ADD CONSTRAINT "brokerage_journal_FK3" FOREIGN KEY (loan_application_id) REFERENCES public.loan_application (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;
ALTER TABLE brokerage.brokerage_journal ADD CONSTRAINT "brokerage_journal_FK4" FOREIGN KEY (brokerage_application_id) REFERENCES public.brokerage_application (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED ;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'ovolkovsky',
  'migration.AF-653_003.sql',
  'Move Brokerage Journal from public to brokerage',
  '6.292'
);

COMMIT;

--  START TRANSACTION;

--  ALTER TABLE brokerage.brokerage_journal RENAME COLUMN customer_number TO account_id;
--  ALTER TABLE brokerage.brokerage_journal ADD COLUMN document_id bigint ;

--  ALTER TABLE brokerage.brokerage_journal DROP CONSTRAINT IF EXISTS "brokerage_journal_FK1" ;
--  ALTER TABLE brokerage.brokerage_journal DROP CONSTRAINT IF EXISTS "brokerage_journal_FK2" ;
--  ALTER TABLE brokerage.brokerage_journal DROP CONSTRAINT IF EXISTS "brokerage_journal_FK3" ;
--  ALTER TABLE brokerage.brokerage_journal DROP CONSTRAINT IF EXISTS "brokerage_journal_FK4" ;

--  ALTER TABLE public.account DROP CONSTRAINT IF EXISTS unigue_person;

-- INSERT INTO database_history VALUES (
--  nextval('"MAIN_SEQUENCE"'),
--  (now()),
--  'ovolkovsky',
--  'rollback.AF-653_003.sql',
--  'Move Brokerage Journal from public to brokerage',
--  '6.292'
-- );
-- COMMIT;