ALTER TABLE brokerage_bank ADD COLUMN comparison_only boolean DEFAULT false;
ALTER TABLE brokerage.brokerage_bank ADD COLUMN comparison_only boolean DEFAULT false;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mcichowski',
  'migration.WEBSITE-17874_0001.sql',
  'WEBSITE-17874 - add new column comparison_only for brokerage_bank',
  '5.22'
);