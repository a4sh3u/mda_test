START TRANSACTION;

ALTER TABLE public.advisor_assignment_log RENAME assigned_by_id TO created_by_id;
ALTER TABLE public.advisor_assignment_log
  ADD CONSTRAINT "public_advisor_assignment_log_created_by_FK" FOREIGN KEY (created_by_id) REFERENCES public.advisor(id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-765_0001.sql',
  'AF-765 - Move Advisor assignment/deassignment logic to AdvisorService',
  '6.257'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE public.advisor_assignment_log
--   DROP CONSTRAINT "public_advisor_assignment_log_created_by_FK";
-- ALTER TABLE public.advisor_assignment_log RENAME created_by_id TO assigned_by_id;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-765_0001.sql',
--   'AF-765 - Move Advisor assignment/deassignment logic to AdvisorService',
--   '6.257'
-- );
--
-- COMMIT;