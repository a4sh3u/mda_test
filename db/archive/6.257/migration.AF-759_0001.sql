START TRANSACTION;

ALTER TABLE brokerage.advisor_group_assignment RENAME "account_id" TO "advisor_id";

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-759_0001.sql',
  'AF-759 - Refactor mda to use Advisor instead of Account for advisor accounts',
  '6.257'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE brokerage.advisor_group_assignment RENAME "advisor_id" TO "account_id";
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-759_0001.sql',
--   'AF-759 - Refactor mda to use Advisor instead of Account for advisor accounts',
--   '6.257'
-- );
--
-- COMMIT;