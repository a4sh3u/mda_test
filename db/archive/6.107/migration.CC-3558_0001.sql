SET SCHEMA 'brokerage';

START TRANSACTION;

ALTER TABLE brokerage.income ADD self_employed_type character varying(255);

COMMIT;

SET SCHEMA 'public';

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pobrebski',
  'migration.CC-3558_0001.sql',
  'CC-3558 Extend self employed persistence',
  '6.107'
);

COMMIT;

-- Rollback:

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.income DROP COLUMN self_employed_type;
--
-- COMMIT;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'pobrebski',
--   'migration.CC-3558_0001.sql',
--   'Rollback: CC-3558 Extend self employed persistence',
--   '6.107'
-- );