SET SCHEMA 'brokerage';

DROP TABLE IF EXISTS household_calculation_history;

CREATE TABLE household_calculation_history (
  id BIGINT PRIMARY KEY NOT NULL,
  household_calculation_id BIGINT NOT NULL,
  old_mapping_expression VARCHAR NOT NULL,
  changed_on TIMESTAMPTZ NOT NULL,
  FOREIGN KEY (household_calculation_id) REFERENCES household_calculation(id)
)
WITH (
OIDS=FALSE
);

--ALTER TABLE household_calculation_history OWNER TO smava;
--GRANT ALL ON TABLE household_calculation_history TO smava;
--GRANT SELECT ON TABLE household_calculation_history TO smava_read;

CREATE OR REPLACE FUNCTION log_household_calculation_update()
  RETURNS trigger AS
$$
BEGIN
 IF NEW.mapping_expression <> OLD.mapping_expression THEN
 INSERT INTO brokerage.household_calculation_history(id, household_calculation_id, old_mapping_expression,changed_on)
 VALUES(nextval('"public"."MAIN_SEQUENCE"'), OLD.id, OLD.mapping_expression, now());
 END IF;

 RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS household_calculation_update ON brokerage.household_calculation;

CREATE TRIGGER household_calculation_update
  BEFORE UPDATE
  ON brokerage.household_calculation
  FOR EACH ROW
  EXECUTE PROCEDURE log_household_calculation_update();

-- ======================================================

DROP TABLE IF EXISTS bank_household_calculation_history;

CREATE TABLE bank_household_calculation_history (
  id BIGINT PRIMARY KEY NOT NULL,
  bank_household_calculation_id BIGINT NOT NULL,
  brokerage_bank_id BIGINT NOT NULL,
  household_calculation_id BIGINT NOT NULL,
  old_bank_expression VARCHAR NOT NULL,
  changed_on TIMESTAMPTZ NOT NULL,
  FOREIGN KEY (bank_household_calculation_id) REFERENCES bank_household_calculation(id),
  FOREIGN KEY (brokerage_bank_id) REFERENCES brokerage_bank(id),
  FOREIGN KEY (household_calculation_id) REFERENCES household_calculation(id)
)
WITH (
OIDS=FALSE
);

--ALTER TABLE bank_household_calculation_history OWNER TO smava;
--GRANT ALL ON TABLE bank_household_calculation_history TO smava;
--GRANT SELECT ON TABLE bank_household_calculation_history TO smava_read;

CREATE OR REPLACE FUNCTION log_bank_household_calculation_update()
  RETURNS trigger AS
  $$
BEGIN
 IF NEW.bank_expression <> OLD.bank_expression THEN
 INSERT INTO brokerage.bank_household_calculation_history(id, bank_household_calculation_id, brokerage_bank_id, household_calculation_id, old_bank_expression, changed_on)
 VALUES(nextval('"public"."MAIN_SEQUENCE"'), OLD.id, OLD.brokerage_bank_id, OLD.household_calculation_id, OLD.bank_expression, now());
 END IF;

 RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS bank_household_calculation_update ON brokerage.bank_household_calculation;

CREATE TRIGGER bank_household_calculation_update
BEFORE UPDATE
ON brokerage.bank_household_calculation
FOR EACH ROW
EXECUTE PROCEDURE log_bank_household_calculation_update();

-- =================
SET SCHEMA 'public';

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pvitic',
  'migration.WEBSITE-17358_0001.sql',
  'WEBSITE-17358 - add household calculation history trigger',
  '5.20'
);
