create schema resources;



CREATE TABLE resources.resource (
	id bigint NOT NULL,
	key character varying NOT NULL,
	value character varying NOT NULL,
	type character varying NOT NULL,
	parent_id bigint,
	CONSTRAINT "resource_PK" PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

--ALTER TABLE resources.resource OWNER TO smava;
--GRANT ALL ON TABLE resources.resource TO smava;
--GRANT SELECT ON TABLE resources.resource TO smava_read;


ALTER TABLE resources.resource ADD CONSTRAINT "resource_parent_FK" FOREIGN KEY (parent_id)
      REFERENCES resources.resource (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;



 INSERT INTO resources.resource values(
 nextval('"MAIN_SEQUENCE"'),
 'smava-options',
 'smava-options',
 'FOLDER',
 null
 );

  INSERT INTO resources.resource values(
 nextval('"MAIN_SEQUENCE"'),
 'i18n.academicTitle',
 'NO_TITLE=- kein Titel-
DR=Dr.
PROF=Prof.
PROF_DR=Prof. Dr.',
 'ENUM',
 ( select  id from resources.resource where key = 'smava-options')
 );



INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dkeller',
  'migration.WEBSITE-17481_0001.sql',
  'WEBSITE-17481 Crate resources schema and table',
  '5.21'
);