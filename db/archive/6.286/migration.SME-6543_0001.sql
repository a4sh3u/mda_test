START TRANSACTION;

CREATE TABLE brokerage.brokerage_application_document
(
  id bigint NOT NULL,
  creation_date timestamp(3) without time zone NOT NULL,
  brokerage_application_id bigint NOT NULL,
  content_type character varying(255) NOT NULL,
  dispatch_type character varying(255) NOT NULL,
  file_storage_id bigint NOT NULL,
  name character varying(255) NOT NULL,
  notes text,
  state character varying(255) NOT NULL,
  subject character varying(255) NOT NULL,
  db_creation_date timestamp(3) without time zone DEFAULT now(),
  db_modify_date timestamp(3) without time zone DEFAULT now(),
  CONSTRAINT brokerage_application_document_pkey PRIMARY KEY (id),
  CONSTRAINT "brokerage_application_document_brokerage_application_FK" FOREIGN KEY (brokerage_application_id)
      REFERENCES brokerage.brokerage_application (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH
(
  OIDS=FALSE
);

CREATE INDEX brokerage_application_document_brokerage_application
  ON brokerage.brokerage_application_document
  USING btree
  (brokerage_application_id);

SELECT public.add_trigger_modify_date('brokerage', ARRAY['brokerage_application_document']);

INSERT INTO database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'atomecki',
  'migration.SME-6543_0001.sql',
  'SME-6543 Banking-webapp - bank documents in FileStorage',
  '6.286'
);

COMMIT;

-- START TRANSACTION;
--
-- DROP TABLE IF EXISTS brokerage.brokerage_application_document;
--
-- INSERT INTO database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'atomecki',
--   'migration.SME-6543_0001.sql',
--   'REVERT: SME-6543 Banking-webapp - bank documents in FileStorage',
--   '6.286'
-- );
--
-- COMMIT;
