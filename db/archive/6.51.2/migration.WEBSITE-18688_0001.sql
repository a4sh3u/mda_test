create or replace view brokerage.bank_document_view
as
select
	pba.id,
	pba.creation_date,
	pba.ext_ref_number,
	t1.*
from
	public.brokerage_application pba,				
	(select
		bankdoc.account_id,
		bankdoc.brokerage_bank_id,
		bankdoc.send_date,
		bankdoc.receive_date,
		bankdoc.bank_name,
		pdata.first_name,
		pdata.last_name,
		(select pacc.account_id from public.account pacc where pacc.id = map.kredit_privat_id) customer_number,
		map.kredit_privat_id public_account_id
	from
		(select * from (
			select
				ROW_NUMBER() OVER (PARTITION BY account_id, brokerage_bank_id) AS RN,
				doc.account_id,
				bdoc.brokerage_bank_id,
				bdoc.send_date,
				bdoc.receive_date,
				bank.name bank_name
			from
				brokerage.brokerage_bank bank,
				brokerage.brokerage_uploaded_doc bdoc,
				(select * from brokerage.customer_uploaded_doc where status in ('ACCEPTED')) doc
			where
				bank.id = bdoc.brokerage_bank_id
				and bdoc.document_id = doc.id) t where RN = 1) bankdoc,
		brokerage.account acc,
		brokerage.applicant app,
		brokerage.personal_data pdata,
		public.brokerage_entity_map map

	where
		bankdoc.account_id = acc.id
		and acc.account_owner_id = app.id
		and app.personal_data_id = pdata.id
		and bankdoc.account_id = map.brokerage_id) t1
where
	t1.public_account_id = pba.account_id
	and t1.brokerage_bank_id = pba.brokerage_bank_id
	and pba.STATE IN (
				'APPLICATION_APPLIED',
				'APPLICATION_ACCEPTED',
				'APPLICATION_DOCUMENTS_RECEIVED',
				'APPLICATION_DOCUMENTS_MISSING',
				'APPLICATION_PENDING');


START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'asaadat',
      'migration.WEBSITE-18688_0001.sql',
      'WEBSITE-18688 - Doku Upload documents not shown for Creditplus',
      '6.51.2'
);

COMMIT;