START TRANSACTION;

DROP TABLE public.advisor_team_assignment;

ALTER TABLE public.advisor
  DROP CONSTRAINT "public_advisor_team_FK";

DROP TABLE public.team;
DROP TABLE public.advisor;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'rollback.AF-352_0001.sql',
  'AF-352 - Add advisor team management tables',
  '6.180'
);

COMMIT;