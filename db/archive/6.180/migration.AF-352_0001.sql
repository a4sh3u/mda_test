START TRANSACTION;

CREATE TABLE IF NOT EXISTS public.advisor (
  id                   BIGINT NOT NULL,
  location             VARCHAR(255),
  team_id              BIGINT,
  blocked_date         TIMESTAMP WITH TIME ZONE,
  max_customer_per_day INTEGER,
  CONSTRAINT "public_advisor_PK" PRIMARY KEY (id),
  CONSTRAINT "public_advisor_account_FK" FOREIGN KEY (id) REFERENCES public.account
);

CREATE TABLE  IF NOT EXISTS public.team (
  id              BIGINT       NOT NULL,
  name            VARCHAR(255) NOT NULL,
  expiration_date TIMESTAMP WITH TIME ZONE,
  team_lead_id    BIGINT,
  CONSTRAINT "public_team_PK" PRIMARY KEY (id),
  CONSTRAINT "public_team_team_lead_FK" FOREIGN KEY (team_lead_id) REFERENCES public.advisor
);

ALTER TABLE public.advisor
  ADD CONSTRAINT "public_advisor_team_FK" FOREIGN KEY (team_id) REFERENCES public.team;

CREATE TABLE  IF NOT EXISTS public.advisor_team_assignment (
  id BIGINT NOT NULL,
  advisor_id BIGINT NOT NULL,
  team_id BIGINT NOT NULL,
  creation_date TIMESTAMP WITH TIME ZONE,
  expiration_date TIMESTAMP WITH TIME ZONE,
  CONSTRAINT "public_advisor_team_assignment_PK" PRIMARY KEY(id),
  CONSTRAINT "public_advisor_team_assignment_advisor_FK" FOREIGN KEY (advisor_id) REFERENCES public.advisor,
  CONSTRAINT "public_advisor_team_assignment_team_FK" FOREIGN KEY (team_id) REFERENCES public.team
);


INSERT INTO public.advisor (id,location, max_customer_per_day) SELECT DISTINCT a.id, 'UNKNOWN', 50
                                FROM public.account a INNER JOIN public.account_role r ON a.id = r.account_id
                                WHERE r.name IN ('ROLE_CREDIT_ADVISOR', 'ROLE_CREDIT_ADVISORY_ADMIN');


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-352_0001.sql',
  'AF-352 - Add advisor team management tables',
  '6.180'
);

COMMIT;