START TRANSACTION;

CREATE TABLE brokerage.sms_template
(
  id bigint NOT NULL,
  key character varying(100) NOT NULL,
  display_name character varying(100) NOT NULL,
  content character varying(1000) NOT NULL,
  editable boolean NOT NULL,
  CONSTRAINT sms_template_pkey PRIMARY KEY (id)
);

ALTER TABLE brokerage.sms_template
  ADD CONSTRAINT sms_template_key_uq UNIQUE (key);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'migration.AF-516_0001.sql',
  'AF-516 - Create table that store SMS templates',
  '6.244'
);

COMMIT;


-- START TRANSACTION;
--
-- DROP TABLE IF EXISTS brokerage.sms_template;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'dmaidaniuk',
--   'rollback.AF-516_0001.sql',
--   'AF-516 - Create table that store SMS templates',
--   '6.244'
-- );
--
-- COMMIT;