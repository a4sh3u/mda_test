CREATE TABLE brokerage.brokerage_bank_maintenance (
    id bigint PRIMARY KEY NOT NULL,
    brokerage_bank_id bigint NOT NULL,
    expression_from VARCHAR NOT NULL,
    expression_to VARCHAR NOT NULL,
    bank_state BOOLEAN NULL DEFAULT NULL,
    CONSTRAINT "brokerage_bank_maintenance_FK" FOREIGN KEY (brokerage_bank_id) REFERENCES brokerage.brokerage_bank(id)
);

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'jjanus',
      'migration.SME-6223_0001_mda.sql',
      'SME-6223 - Pause bank request while maintenance periods',
      '6.211'
);