START TRANSACTION;

CREATE OR REPLACE VIEW borrower_search AS
  SELECT acc.id,
    la.id AS loan_application_id,
    ps.id AS customer_process_status_id
  FROM account acc
    JOIN customer_process_status ps ON ps.account_id = acc.id
    LEFT JOIN loan_application la ON la.account_id = acc.id AND la.id = (( SELECT max(loan_application.id) AS max_id
                                                                           FROM loan_application
                                                                           WHERE loan_application.account_id = acc.id))
  WHERE acc.state::text = 'ACTIVATED'::text AND ps.expiration_date IS NULL;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  now(),
  'dmaidaniuk',
  'migration.AF-1325_001.sql',
  'CPS Bug in List view',
  '6.318'
);

COMMIT;



-- START TRANSACTION;
--
-- CREATE OR REPLACE VIEW borrower_search AS
--   SELECT acc.id,
--     la.id AS loan_application_id,
--     ps.id AS customer_process_status_id
--   FROM account acc
--     JOIN customer_process_status ps ON ps.account_id = acc.id
--     LEFT JOIN loan_application la ON la.account_id = acc.id AND la.creation_date = (( SELECT max(loan_application.creation_date) AS max
--                                                                                       FROM loan_application
--                                                                                       WHERE loan_application.account_id = acc.id))
--   WHERE acc.state::text = 'ACTIVATED'::text AND ps.expiration_date IS NULL;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   now(),
--   'dmaidaniuk',
--   'rollback.AF-1325_001.sql',
--   'CPS Bug in List view',
--   '6.318'
-- );
--
-- COMMIT;