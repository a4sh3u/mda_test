START TRANSACTION;


ALTER TABLE public.advisor_assignment_log
  ADD COLUMN assigned_by_id bigint;

ALTER TABLE brokerage.advisor_assignment_log
  ADD COLUMN assigned_by_id bigint;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mbiletskyi',
  'migration.AF-573_0001.sql',
  'AF-573 - As smava I want to track who did the manual assignment in CASI',
  '6.201'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE public.advisor_assignment_log DROP COLUMN assigned_by_id;
--
-- ALTER TABLE brokerage.advisor_assignment_log DROP COLUMN assigned_by_id;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'mbiletskyi',
--   'rollback.AF-573_0001.sql',
--   'AF-573 - As smava I want to track who did the manual assignment in CASI',
--   '6.201'
-- );
--
-- COMMIT;