CREATE OR REPLACE VIEW casi_search AS
 SELECT p.id,
    p.type,
    a.id AS account_id,
    a.phone_code::text || a.phone::text AS phone,
    a.phone2_code::text || a.phone2::text AS phone2,
    a.email,
    p.first_name,
    p.last_name,
    p.date_of_birth,
    a.personal_advisor,
    l.id AS loan_application_id
   FROM account a
     INNER JOIN person p ON a.id = p.account_id AND p.valid_until IS NULL
     INNER JOIN account_role r ON a.id = r.account_id
     LEFT JOIN loan_application l ON l.account_id = a.id AND l.creation_date = ( SELECT max(loan_application.creation_date) AS max
           FROM loan_application WHERE loan_application.account_id = a.id)
     WHERE r.name = 'ROLE_BORROWER' or r.name = 'ROLE_MEMBER';


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mpetrick',
  'migration.WEBSITE-17394_0001.sql',
  'WEBSITE-17394 - Filter out credit advisors from CASI search',
  null
);