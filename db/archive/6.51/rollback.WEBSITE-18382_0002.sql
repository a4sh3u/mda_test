DROP FUNCTION calculate_customer_value_timefactor(double precision, timestamp with time zone, timestamp with time zone);

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pharbert',
      'rollback.WEBSITE-18382_0002.sql',
      'WEBSITE-18382 - Stored procedure for time factor handling in customer value calculation',
      '6.51'
);

COMMIT;
