DROP VIEW casi_event_queue;

CREATE OR REPLACE VIEW casi_event_queue AS
 SELECT eq.id AS id,
    eq.creation_date AS event_queue_creation_date,
    eq.account_id,
    eq.loan_application_id,
    eq.type AS event_queue_type,
    eq.state AS event_queue_state,
    eq.due_date AS event_queue_due_date,
    eq.expiration_date AS event_queue_expiration_date,
    eq.bank_id,
        CASE
            WHEN (( SELECT count(*) AS count
               FROM brokerage.customer_uploaded_doc
              WHERE customer_uploaded_doc.account_id = (( SELECT brokerage_entity_map.brokerage_id
                       FROM brokerage_entity_map
                      WHERE brokerage_entity_map.kredit_privat_id = eq.account_id)) AND (customer_uploaded_doc.status::text = 'CREATED'::text OR customer_uploaded_doc.status::text = 'ACCEPTED'::text OR customer_uploaded_doc.status::text = 'REJECTED'::text OR customer_uploaded_doc.status::text = 'OUTDATED'::text))) > 0 THEN 1
            ELSE 0
        END AS has_document,
    cps.id AS customer_status_id,
    cps.creation_date AS customer_status_creation_date,
    cps.state AS customer_status_state,
    cps.reason AS customer_status_reason,
    cps.action AS customer_status_action,
    cps.created_by AS customer_status_created_by,
    ( SELECT loan_application.customer_value
           FROM loan_application
          WHERE loan_application.id = eq.loan_application_id) AS customer_value
   FROM event_queue eq
     LEFT JOIN customer_process_status cps ON eq.account_id = cps.account_id AND cps.creation_date = (( SELECT max(customer_process_status.creation_date) AS max
           FROM customer_process_status
          WHERE customer_process_status.account_id = eq.account_id));



START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pharbert',
      'migration.WEBSITE-18382_0001.sql',
      'WEBSITE-18382 - Update loan application and event queue view for lead assignment changes',
      '6.51'
);

COMMIT;