CREATE OR REPLACE FUNCTION calculate_customer_value_timefactor (
  customer_value double precision,
  lapp_date timestamp with time zone,
  curr_date timestamp with time zone
) RETURNS double precision AS
$BODY$
DECLARE
  timegap double precision;
  timegap1 double precision;
  timevalue1 double precision;
  timegap2 double precision;
  timevalue2 double precision;
  timegap3 double precision;
  timevalue3 double precision;
  timegap4 double precision;
  timevalue4 double precision;
  timegap5 double precision;
  timevalue5 double precision;
  timegap6 double precision;
  timevalue6 double precision;
  remaining_timevalue double precision;
BEGIN
  timegap = (SELECT EXTRACT (EPOCH FROM (AGE(curr_date, lapp_date))) / 60);
  SELECT c.value into timegap1 from configvalue c where key = 'casi.leadAssignment.timeFactor.timegap1';
  SELECT c.value into timevalue1 from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalue1';
  SELECT c.value into timegap2 from configvalue c where key = 'casi.leadAssignment.timeFactor.timegap2';
  SELECT c.value into timevalue2 from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalue2';
  SELECT c.value into timegap3 from configvalue c where key = 'casi.leadAssignment.timeFactor.timegap3';
  SELECT c.value into timevalue3 from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalue3';
  SELECT c.value into timegap4 from configvalue c where key = 'casi.leadAssignment.timeFactor.timegap4';
  SELECT c.value into timevalue4 from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalue4';
  SELECT c.value into timegap5 from configvalue c where key = 'casi.leadAssignment.timeFactor.timegap5';
  SELECT c.value into timevalue5 from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalue5';
  SELECT c.value into timegap6 from configvalue c where key = 'casi.leadAssignment.timeFactor.timegap6';
  SELECT c.value into timevalue6 from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalue6';
  SELECT c.value into remaining_timevalue from configvalue c where key = 'casi.leadAssignment.timeFactor.timevalueRemaining';

  if (timegap < timegap1) then
    RETURN customer_value * timevalue1;
  elsif (timegap < timegap2) then
    RETURN customer_value * timevalue2;
  elsif (timegap < timegap3) then
    RETURN customer_value * timevalue3;
  elsif (timegap < timegap4) then
    RETURN customer_value * timevalue4;
  elsif (timegap < timegap5) then
    RETURN customer_value * timevalue5;
  elsif (timegap < timegap6) then
    RETURN customer_value * timevalue6;
  else
    RETURN customer_value * remaining_timevalue;
  end if;
END
$BODY$
LANGUAGE plpgsql;


START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pharbert',
      'migration.WEBSITE-18382_0002.sql',
      'WEBSITE-18382 - Stored procedure for time factor handling in customer value calculation',
      '6.51'
);

COMMIT;