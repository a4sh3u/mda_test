CREATE TABLE public.customer_process_status (
	id bigint NOT NULL,
	account_id bigint NOT NULL,
	state CHARACTER VARYING NOT NULL,
  reason CHARACTER VARYING NOT NULL,
  action CHARACTER VARYING,
  created_by CHARACTER VARYING NOT NULL,
  creation_date timestamp with time zone NOT NULL,
  expiration_date timestamp with time zone
)
WITH (
OIDS=FALSE
);

CREATE INDEX "customer_process_status_account"
  ON public.customer_process_status
  USING btree
  (account_id);



--ALTER TABLE pap_affiliate_partner OWNER TO smava;
--GRANT ALL ON TABLE pap_affiliate_partner TO smava;
--GRANT SELECT ON TABLE pap_affiliate_partner TO smava_read;


CREATE TABLE brokerage.customer_process_status (
	id bigint NOT NULL,
	account_id bigint NOT NULL,
	state CHARACTER VARYING NOT NULL,
	reason CHARACTER VARYING NOT NULL,
	action CHARACTER VARYING,
	created_by CHARACTER VARYING NOT NULL,
	creation_date timestamp with time zone NOT NULL,
	expiration_date timestamp with time zone
)
WITH (
OIDS=FALSE
);

CREATE INDEX "customer_process_status_account"
  ON brokerage.customer_process_status
  USING btree
  (account_id);

--ALTER TABLE pap_affiliate_partner OWNER TO smava;
--GRANT ALL ON TABLE pap_affiliate_partner TO smava;
--GRANT SELECT ON TABLE pap_affiliate_partner TO smava_read;


INSERT INTO database_history VALUES (
	nextval('"MAIN_SEQUENCE"'),
	(now()),
	'pvitic',
	'migration.WEBSITE-17214_0001.sql',
	'WEBSITE-17214 - Create customer process status tables',
    '5.17'
);