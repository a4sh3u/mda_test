UPDATE brokerage.advisor_group_filter_parameter set creation_date=now();

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pvitic',
  'migration.WEBSITE-16675_0002.sql',
  'WEBSITE-16675 Group Parameter: add deafult creation date',
  null
);
