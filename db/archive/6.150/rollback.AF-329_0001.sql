START TRANSACTION;

DROP VIEW public.casi_event_queue;

CREATE OR REPLACE VIEW public.casi_event_queue AS
 SELECT eq.id,
    eq.creation_date AS event_queue_creation_date,
    eq.account_id,
    eq.loan_application_id,
    eq.type AS event_queue_type,
    eq.state AS event_queue_state,
    eq.due_date AS event_queue_due_date,
    eq.expiration_date AS event_queue_expiration_date,
    eq.bank_id,
        CASE
            WHEN (( SELECT count(*) AS count
               FROM brokerage.customer_uploaded_doc
              WHERE customer_uploaded_doc.account_id = (( SELECT brokerage_entity_map.brokerage_id
                       FROM brokerage_entity_map
                      WHERE brokerage_entity_map.kredit_privat_id = eq.account_id)) AND (customer_uploaded_doc.status::text = 'CREATED'::text OR customer_uploaded_doc.status::text = 'ACCEPTED'::text OR customer_uploaded_doc.status::text = 'REJECTED'::text OR customer_uploaded_doc.status::text = 'OUTDATED'::text))) > 0 THEN 1
            ELSE 0
        END AS has_document,
    cps.id AS customer_status_id,
    cps.creation_date AS customer_status_creation_date,
    cps.state AS customer_status_state,
    cps.reason AS customer_status_reason,
    cps.action AS customer_status_action,
    cps.created_by AS customer_status_created_by,
    ( SELECT loan_application.customer_value
           FROM loan_application
          WHERE loan_application.id = eq.loan_application_id) AS customer_value,
        CASE
            WHEN (( SELECT count(*) AS count
               FROM schufa_score
                 JOIN loan_application ON loan_application.id = eq.loan_application_id AND loan_application.account_id = schufa_score.account AND (loan_application.creation_date - '60 days'::interval) <= schufa_score.change_date AND (loan_application.creation_date + '5 days'::interval) > schufa_score.change_date AND schufa_score.state::text = 'failed'::text
                 JOIN schufa_request ON schufa_score.request_id = schufa_request.id
                 JOIN person ON person.id = schufa_request.person_id AND person.type = 1)) > 0 THEN false
            ELSE true
        END AS event_queue_is_schufa_positive,
    aai.sale_affiliate_information_id AS sale_affiliate_info_id
   FROM event_queue eq
     LEFT JOIN customer_process_status cps ON eq.account_id = cps.account_id AND cps.creation_date = (( SELECT max(customer_process_status.creation_date) AS max
           FROM customer_process_status
          WHERE customer_process_status.account_id = eq.account_id))
     LEFT JOIN account_affiliate_information aai ON eq.account_id = aai.account_id;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mbiletskyi',
  'rollback.AF-329_0001.sql',
  'AF-329 - Aktion auswählen can be removed',
  '6.150'
);

COMMIT;