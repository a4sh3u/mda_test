START TRANSACTION;

ALTER TABLE public.customer_process_status ADD COLUMN account_smava_id BIGINT;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'ovolkovsky',
  'migration.AF-1119_001.sql',
  'Collect CustomerProcessStatus-related logic behind the facade',
  '6.311'
);

COMMIT;


-- START TRANSACTION;

-- ALTER TABLE public.customer_process_status RENAME COLUMN account_smava_id TO account_id;

--INSERT INTO database_history VALUES (
--  nextval('"MAIN_SEQUENCE"'),
--  (now()),
--  'ovolkovsky',
--  'rollback.AF-1119_001.sql',
--  'Collect CustomerProcessStatus-related logic behind the facade',
--  '6.311'
--);

-- COMMIT;