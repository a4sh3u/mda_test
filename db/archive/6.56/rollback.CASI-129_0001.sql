DROP FUNCTION calculate_customer_value_relevancefactor(double precision, bigint);

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pharbert',
      'rollback.WEBSITE-18382_0001.sql',
      'CASI-129 - Relevance factor for customer value lead assignment',
      '6.56'
      );

COMMIT;
