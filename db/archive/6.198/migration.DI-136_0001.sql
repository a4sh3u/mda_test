CREATE TABLE brokerage.esign
(
  id                       BIGINT                   NOT NULL,
  status                   CHARACTER VARYING(255),
  brokerage_application_id BIGINT,
  applicant_id             BIGINT,
  esign_url                CHARACTER VARYING(255),
  contract_doc_available   BOOLEAN                  NOT NULL DEFAULT FALSE,
  creation_date            TIMESTAMP WITH TIME ZONE NOT NULL,
  update_date              TIMESTAMP WITH TIME ZONE,

  CONSTRAINT esign_pk PRIMARY KEY (id),
  CONSTRAINT esign_brokerage_application_id FOREIGN KEY (brokerage_application_id)
  REFERENCES brokerage.brokerage_application (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT esign_applicant_id FOREIGN KEY (applicant_id)
  REFERENCES brokerage.applicant (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
OIDS = FALSE
);

ALTER TABLE brokerage.esign
  ADD CONSTRAINT uq_esign_ba_id_applicant_id UNIQUE (brokerage_application_id, applicant_id);

ALTER TABLE brokerage.brokerage_bank_configuration
  ADD COLUMN esign boolean NOT NULL DEFAULT FALSE;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'asaadat',
  'migration.DI-136_0001.sql',
  'Digital Loan Creditplus: Implement WebID QES API',
  '6.198'
);

COMMIT;
