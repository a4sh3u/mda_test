DROP VIEW dunning_process_view;

CREATE OR REPLACE VIEW dunning_process_view AS
  SELECT
    acc_id,
    a.account_id,
    p.first_name || ' ' || p.last_name as username,
    group_id as id,
    group_id, --Vertrag (Group ID),
    sel_main.transaction_id,
    (select sum(amount) from booking b where b.transaction_id = sel_main.transaction_id) transaction_amount,
    transaction_due_date,
    ds.level,
    COALESCE(sel_arrear_counter.val, 0) as arrear_counter, --Verträge mit Mahnstufe
    rkv,
    credit_term,
    substring(bid_int.market_name from 1 for 1) as rating,
    date_part('year',age(p.date_of_birth)) as age,
    (select sum(bid.amount) from bid where bid."groupId" = group_id) as amount,
    (SELECT
       CASE
       WHEN employment = 'EMPLOYEE' THEN 'Angestellter'
       WHEN employment = 'LABOURER' THEN 'Arbeiter'
       WHEN employment = 'UNEMPLOYED' THEN 'Arbeitslos'
       WHEN employment = 'CIVIL_CERVANT' THEN 'Beamter'
       WHEN employment = 'CEO' THEN 'Geschäftsführender Gesellschafter'
       WHEN employment = 'HOMEKEEPER' THEN 'Hausfrau/Hausmann'
       WHEN employment = 'RETIREE' THEN 'Rentner'
       WHEN employment = 'STUDENT' THEN 'Schüler'
       WHEN employment IN ('GENERAL_FREELANCER', 'TRADESMAN', 'FREELANCER') THEN 'Selbstständiger/Freiberufler'
       WHEN employment = 'SALARIED_STAFF' THEN 'Soldat'
       WHEN employment = 'OTHER' THEN 'Sonstige'
       WHEN employment = 'SCHOLAR' THEN 'Student'
       ELSE employment
       END
     FROM
       (SELECT ed1.*, row_number() over (partition by ed1.account_id order by creation_date desc) rn
        FROM
          "economicalData" ed1
        WHERE ed1.account_id = acc_id) ed2
     WHERE rn = 1) as occupation,
    start_date
  FROM
    (SELECT
       acc_id,
       group_id,
       transaction_id,
       transaction_due_date,
       rkv,
       credit_term,
       min(c_start_date) as start_date
     FROM
       (SELECT
          a.id as acc_id,
          bid."groupId" as group_id,
          t.id as transaction_id,
          t."dueDate" as transaction_due_date,
          rdi.TYPE as rkv,
          c.id as contract_id,
          o.credit_term,
          min(c.start_date) as c_start_date
        FROM
          bank_account ba, TRANSACTION t,   dunning_state ds, account a, "order" o, contract c, booking b,
          bid LEFT OUTER JOIN rdi_contract rdi ON (bid."groupId" = rdi.order_id),
          (SELECT transaction_id, level
           FROM (
                  SELECT transaction_id, level, row_number() over (partition by transaction_id order by creation_date desc) r
                  FROM dunning_state ds
                ) ss WHERE r = 1)as ds_latest
        WHERE t.credit_account=ba.id AND ds.transaction_id=t.id AND ba.account_id=a.id
              AND t.state='OPEN'
              AND ds.level=1
              AND bid.id=o.id
              AND c.order_id = o.id
              AND b.contract_id = c.id
              AND b.transaction_id = t.id
              AND ds_latest.transaction_id = t.id
              AND rdi."expirationDate" IS NULL
        GROUP BY
          a.id,
          bid."groupId",
          t."dueDate",
          t.id,
          c.id,
          o.credit_term,
          rdi.TYPE
       ) sel
     GROUP BY
       acc_id,
       group_id,
       transaction_id,
       transaction_due_date,
       credit_term,
       rkv
    ) sel_main
    LEFT JOIN
    (
      SELECT
        account_id, sum(val) as val
      FROM
        ((SELECT t2.ref_month, t2.account_id, COUNT( DISTINCT(mahng)) AS val
          FROM
            (SELECT date_trunc('month', t.creation_date) AS ref_month, t.id AS mahng, ba.account_id
             FROM TRANSACTION t
               INNER JOIN dunning_state ds ON t.id = ds.transaction_id
               INNER JOIN bank_account ba ON ba.id = t.credit_account
             WHERE t.TYPE = 'TRANSFER'
                   AND ds.level = 0
                   AND date_trunc('month', ds.creation_date) = date_trunc('month', t.creation_date)
                   AND t.state != 'DELETED'
            ) t2
            LEFT JOIN
            (SELECT date_trunc('month', ta.transaction_date) AS ref_month, ba.account_id
             FROM TRANSACTION ta
               INNER JOIN bank_account ba ON ba.id = ta.debit_account
             WHERE ta.TYPE = 'RETURN_DEBIT') t3
              ON t3.account_id = t2.account_id
                 AND t2.ref_month = t3.ref_month
          WHERE t3.account_id IS NULL
          GROUP BY t2.ref_month, t2.account_id)
         UNION
         (SELECT
              date_trunc('month', transaction_date) AS ref_month
            , ba.account_id
            , COUNT(*) AS val
          FROM TRANSACTION t
            INNER JOIN bank_account ba ON ba.id = t.credit_account
          WHERE t.state = 'SUCCESSFUL'
                AND t.TYPE IN ('RETURN_DEBIT')
          GROUP BY ref_month, ba.account_id
         )) u_sel
      GROUP BY account_id
    ) sel_arrear_counter
      ON (sel_main.acc_id = sel_arrear_counter.account_id)
    LEFT JOIN
    (
      SELECT *
      FROM
        (SELECT ds1.*, row_number() over (partition by ds1.transaction_id order by creation_date desc) rn
         FROM
           dunning_state ds1) ds2
      WHERE rn = 1
    ) ds
      ON (
      ds.transaction_id = sel_main.transaction_id
      )
    LEFT JOIN
    (
      SELECT * FROM bid_interest WHERE valid_until is NULL
    ) bid_int
      ON (bid_int.bid_id = sel_main.group_id)
    INNER JOIN account a
      ON (a.id = sel_main.acc_id)
    INNER JOIN (select * from person  where valid_until is null and type = 1) p
      ON (p.account_id = sel_main.acc_id);

DROP INDEX dunning_view_data_order_id;

DROP TABLE dunning_view_data;


INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mbiletskyi',
      'rollback.KP-198_0001.sql',
      'KP-198 - Commenting Functionality in Dunning Process View',
      '6.198'
);