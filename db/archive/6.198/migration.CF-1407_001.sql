START TRANSACTION;

ALTER TABLE brokerage.pap_affiliate_partner
  ADD COLUMN creation_date TIMESTAMP DEFAULT now();

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'achemparathy',
      'migration.CF-1407_001.sql',
      'creation_date column for table brokerage.pap_affiliate_partner',
      '6.198'
);

COMMIT;