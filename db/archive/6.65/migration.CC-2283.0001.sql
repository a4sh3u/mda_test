START TRANSACTION;

SET SCHEMA 'brokerage';

DROP TABLE loan_check, loan_check_request, loan_check_request_data;

SET SCHEMA 'public';

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'jjanus',
  'migration.CC-2283_0001.sql',
  'Refactor NewAbstractRemoteBankSync and NewAbstractRemoteBank',
  '6.65'
);

COMMIT;

-- /* ROLLBACK */
-- START TRANSACTION;
--
-- SET SCHEMA 'brokerage';
--
-- CREATE TABLE loan_check (
--     id bigint NOT NULL,
--     account_id bigint,
--     completion_date timestamp with time zone,
--     creation_date timestamp with time zone,
--     type character varying(255)
-- );
--
-- CREATE TABLE loan_check_request (
--     id bigint NOT NULL,
--     amount double precision,
--     brokerage_bank_id bigint,
--     creation_date timestamp with time zone,
--     duration integer,
--     effective_interest double precision,
--     first_applicant_id bigint,
--     loan_check_id bigint,
--     monthly_rate double precision,
--     requested_amount double precision NOT NULL,
--     requested_duration integer NOT NULL,
--     requested_rdi_type character varying(255),
--     second_applicant_id bigint,
--     state character varying(255)
-- );
--
-- CREATE TABLE loan_check_request_data (
--     id bigint NOT NULL,
--     creation_date timestamp with time zone,
--     loan_check_request_id bigint,
--     request text,
--     response text,
--     state character varying(255)
-- );
--
-- SET SCHEMA 'public';
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'jjanus',
--   'migration.CC-2283_0001.sql',
--   'rolled back Refactor NewAbstractRemoteBankSync and NewAbstractRemoteBank',
--   '6.65'
-- );
--
-- COMMIT;