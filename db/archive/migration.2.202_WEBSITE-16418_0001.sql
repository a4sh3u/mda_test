
CREATE TABLE brokerage.brokerage_bank_guarantee_configuration (
	id bigint NOT NULL,
	brokerage_bank_id bigint NOT NULL,
	force_top_chance boolean,
	CONSTRAINT "brokerage_bank_guarantee_configuration_PK" PRIMARY KEY (id),
		CONSTRAINT "brokerage_bank_guarantee_configuration_FK_brokerage_bank" FOREIGN KEY (brokerage_bank_id)
	  REFERENCES brokerage_bank (id) MATCH SIMPLE
)
WITH (
OIDS=FALSE
);

--ALTER TABLE pap_affiliate_partner OWNER TO smava;
--GRANT ALL ON TABLE pap_affiliate_partner TO smava;
--GRANT SELECT ON TABLE pap_affiliate_partner TO smava_read;


CREATE TABLE public.brokerage_bank_guarantee_configuration (
	id bigint NOT NULL,
	brokerage_bank_id bigint NOT NULL,
	force_top_chance boolean,
	CONSTRAINT "brokerage_bank_guarantee_configuration_PK" PRIMARY KEY (id),
		CONSTRAINT "brokerage_bank_guarantee_configuration_FK_brokerage_bank" FOREIGN KEY (brokerage_bank_id)
	  REFERENCES brokerage_bank (id) MATCH SIMPLE
)
WITH (
OIDS=FALSE
);

--ALTER TABLE pap_affiliate_partner OWNER TO smava;
--GRANT ALL ON TABLE pap_affiliate_partner TO smava;
--GRANT SELECT ON TABLE pap_affiliate_partner TO smava_read;



ALTER TABLE brokerage.brokerage_application_guarantee ADD reason character varying(255);
ALTER TABLE public.brokerage_application_guarantee ADD reason character varying(255);

insert into public.brokerage_bank_guarantee_configuration VALUES (
	nextval('"MAIN_SEQUENCE"'),
	(select id from brokerage_bank where name = 'bankofscotland'),
	true);



INSERT INTO database_history VALUES (
	nextval('"MAIN_SEQUENCE"'),
	(now()),
	'dkeller',
	'migration.2.202_WEBSITE-16418_0001.sql',
	'WEBSITE-16418 - Promote banks independant from Scoring Model'
);