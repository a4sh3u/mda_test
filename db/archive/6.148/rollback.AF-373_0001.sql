START TRANSACTION;

ALTER TABLE brokerage.advisor_group DROP COLUMN priority;

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'rollback.AF-373_0001.sql',
  'AF-373 - As smava I want to have an exclusive priority lead assignment for our partner MediDate',
  '6.148'
);

COMMIT;