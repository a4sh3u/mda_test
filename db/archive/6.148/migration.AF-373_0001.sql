START TRANSACTION;

ALTER TABLE brokerage.advisor_group ADD COLUMN priority integer;

UPDATE brokerage.advisor_group SET priority = 1;

ALTER TABLE brokerage.advisor_group ALTER COLUMN priority SET NOT NULL;

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.AF-373_0001.sql',
      'AF-373 - As smava I want to have an exclusive priority lead assignment for our partner MediDate',
      '6.148'
);

COMMIT;