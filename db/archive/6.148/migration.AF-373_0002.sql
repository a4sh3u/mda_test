START TRANSACTION;

INSERT INTO brokerage.advisor_group(
      id, name, condition, expiration_date, priority)
VALUES (nextval('"MAIN_SEQUENCE"'), 'MediDate.de', '#event.saleAffiliateInfo.externalAffiliateId.equals("eedb3c4a")', NULL, 2);

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.AF-373_0002.sql',
      'AF-373 - As smava I want to have an exclusive priority lead assignment for our partner MediDate',
      '6.148'
);

COMMIT;