START TRANSACTION;

DROP TABLE brokerage.acd_extra_information;

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'rollback.AF-437_0001.sql',
  'AF-437 - Store extra information needed for ACD, which is not available in LA, in a separate table',
  '6.199'
);

COMMIT;