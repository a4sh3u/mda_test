START TRANSACTION;

CREATE TABLE brokerage.acd_extra_information
(
  id bigint NOT NULL,
  loan_application_id bigint,
  ingdibahhc double precision,
  page integer,
  smava_schufa_score double precision,
  smava_schufa_state boolean,
  CONSTRAINT acd_extra_information_pkey PRIMARY KEY (id),
  UNIQUE (loan_application_id)
);

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.AF-437_0001.sql',
      'AF-437 - Store extra information needed for ACD, which is not available in LA, in a separate table',
      '6.199'
);

COMMIT;