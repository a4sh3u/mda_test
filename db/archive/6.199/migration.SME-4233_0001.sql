START TRANSACTION;

ALTER TABLE brokerage.customer_uploaded_doc
ADD COLUMN "bank_document_id" VARCHAR NULL;


INSERT INTO public.database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'WTeperek',
    'migration.SME-4233_0001.sql',
    'SME-4233 - Implement Postbank API to send customer documents',
    '6.199'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE brokerage.customer_uploaded_doc DROP COLUMN bank_document_id;
--
--
-- INSERT INTO  public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'WTeperek',
--   'migration.SME-4233_0001.sql',
--   'SME-4233 - Implement Postbank API to send customer documents',
--   '6.199'
-- );
--
-- COMMIT;