START TRANSACTION;

ALTER TABLE brokerage.brokerage_bank DROP COLUMN db_modified_date;

INSERT INTO database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'WTeperek',
    'migration.SME-7125_0002.sql',
    'SME-7125 Unify naming convention: use db_modify_date instead of db_modified_date',
    '6.241'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_bank ADD COLUMN db_modified_date TIMESTAMPTZ;
--
-- UPDATE brokerage.brokerage_bank bb SET db_modified_date = bkp.db_modified_date
-- FROM config_backup.sme_6017_brokerage_bank_modified AS bkp
-- WHERE bb.id = bkp.id;
--
-- INSERT INTO database_history VALUES (
--     nextval('public."MAIN_SEQUENCE"'),
--     (now()),
--     'WTeperek',
--     'REVERT migration.SME-7125_0002.sql',
--     'REVERT SME-7125 Unify naming convention: use db_modify_date instead of db_modified_date',
--     '6.241'
-- );
--
-- COMMIT;