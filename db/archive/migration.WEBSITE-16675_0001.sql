DROP INDEX IF EXISTS brokerage_journal_test;

CREATE INDEX brokerage_journal_account_and_type ON brokerage_journal (account_id, "type");

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pvitic',
  'migration.WEBSITE-16675_0001.sql',
  'WEBSITE-16675 - Index Brokerage journal by loan application and clean a test index',
  null
);
