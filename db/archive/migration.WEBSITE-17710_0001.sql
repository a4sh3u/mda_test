--DROP TABLE IF EXISTS NUCLEUS_TABLES;


CREATE TABLE NUCLEUS_TABLES (
  CLASS_NAME     VARCHAR(128) PRIMARY KEY  NOT NULL,
  LTABLE_NAME    VARCHAR(128),
  TYPE           VARCHAR(4),
  OWNER          VARCHAR(2),
  VERSION        VARCHAR(20),
  INTERFACE_NAME VARCHAR(255)
);

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aibrahim',
      'migration.WEBSITE-17710_0001.sql',
      'WEBSITE-17710_0001 - Create table to cache tables-classes mappings to allow the database mapping to be initialized during the webapps startup to avoid deadlocks',
      '5.24.3'

);