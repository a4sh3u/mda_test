START TRANSACTION;

CREATE TABLE brokerage.brokerage_multi_status_request_data
(
  id bigint NOT NULL,
  creation_date timestamp with time zone NOT NULL,
  brokerage_bank_id bigint NOT NULL,
  state character varying(255) NOT NULL,
  request text,
  response text,
  CONSTRAINT "brokerage_multi_status_request_data_PK" PRIMARY KEY (id),
  CONSTRAINT "brokerage_multi_status_request_data_FK" FOREIGN KEY (brokerage_bank_id)
    REFERENCES brokerage.brokerage_bank (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE brokerage.brokerage_application_request_data
  ADD COLUMN brokerage_multi_status_request_data_id bigint DEFAULT NULL;

ALTER TABLE brokerage.brokerage_application_request_data
  ADD CONSTRAINT "brokerage_application_request_data_FK2" FOREIGN KEY (brokerage_multi_status_request_data_id)
  REFERENCES brokerage.brokerage_multi_status_request_data (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'atomecki',
  'migration.CC-2249_0001.sql',
  'CC-2249 ING DiBa: Call all status in one request - getStatusList()',
  '6.70'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_application_request_data DROP CONSTRAINT brokerage_application_request_data_FK2;
--
-- ALTER TABLE brokerage.brokerage_application_request_data DROP COLUMN brokerage_multi_status_request_data_id;
--
-- DROP TABLE brokerage.brokerage_multi_status_request_data;
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'atomecki',
--   'migration.CC-2249_0001.sql',
--   'Rollback',
--   '6.70'
-- );
--
-- COMMIT;