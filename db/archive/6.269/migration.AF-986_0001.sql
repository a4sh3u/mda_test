START TRANSACTION;

ALTER TABLE public.advisor_assignment_log DROP CONSTRAINT "advisor_assignment_log_FK2";
ALTER TABLE public.advisor_assignment_log RENAME "borrower_id" TO "customer_number";

UPDATE public.advisor_assignment_log
SET customer_number = (SELECT account_id
                       FROM public.account a
                       WHERE a.id = customer_number);

CREATE INDEX "advisor_assignment_log_customer_number_idx"
  ON public.advisor_assignment_log
  USING BTREE
  (customer_number);

INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-986_0001.sql',
  'AF-986 - Refactor Borrower relations to be based on customerNumber',
  '6.269'
);

COMMIT;


-- START TRANSACTION;
--
-- DROP INDEX public."advisor_assignment_log_customer_number_idx";
-- UPDATE public.advisor_assignment_log
-- SET customer_number = (SELECT id
--                        FROM public.account a
--                        WHERE a.account_id = customer_number);
--
-- ALTER TABLE public.advisor_assignment_log RENAME "customer_number" TO "borrower_id";
-- ALTER TABLE public.advisor_assignment_log
--   ADD CONSTRAINT "advisor_assignment_log_FK2" FOREIGN KEY (borrower_id) REFERENCES public.account (id);
--
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-986_0001.sql',
--   'AF-986 - Refactor Borrower relations to be based on customerNumber',
--   '6.269'
-- );
--
-- COMMIT;