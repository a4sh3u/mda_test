START TRANSACTION;

ALTER TABLE brokerage.call_journal RENAME "advisor_id" TO "advisor_smava_ref_id";

UPDATE brokerage.call_journal
SET advisor_smava_ref_id = (SELECT account_id
                       FROM public.account a
                       WHERE a.id = advisor_smava_ref_id);


INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-992_0001.sql',
  'AF-992 - Refactor CallJournal relations to remove direct relation to Advisor entity',
  '6.269'
);

COMMIT;


-- START TRANSACTION;
--
-- UPDATE brokerage.call_journal
-- SET advisor_smava_ref_id = (SELECT id
--                             FROM public.account a
--                             WHERE a.account_id = advisor_smava_ref_id);
--
-- ALTER TABLE brokerage.call_journal RENAME "advisor_smava_ref_id" TO "advisor_id";
--
--
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-992_0001.sql',
--   'AF-992 - Refactor CallJournal relations to remove direct relation to Advisor entity',
--   '6.269'
-- );
--
-- COMMIT;