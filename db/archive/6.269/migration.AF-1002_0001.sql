START TRANSACTION;

CREATE TABLE config_backup.AF_1002_Remove_personalAdvisorId_from_applicant_Account(
  account_id BIGINT,
  advisor_id BIGINT
);

INSERT INTO config_backup.AF_1002_Remove_personalAdvisorId_from_applicant_Account(account_id, advisor_id)
  SELECT b_a.id, b_a.personal_advisor_id FROM brokerage.account b_a WHERE b_a.personal_advisor_id IS NOT NULL;

ALTER TABLE brokerage.account DROP COLUMN "personal_advisor_id";


INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-1002_0001.sql',
  'AF-1002 - Remove personalAdvisor relation from applicant Account',
  '6.269'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE brokerage.account ADD COLUMN "personal_advisor_id" BIGINT;
--   UPDATE brokerage.account b_a SET "personal_advisor_id" = (SELECT advisor_id
--                                                             FROM
--                                                               config_backup.AF_1002_Remove_personalAdvisorId_from_applicant_Account
--                                                             WHERE account_id = b_a.id);
--
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-1002_0001.sql',
--   'AF-1002 - Remove personalAdvisor relation from applicant Account',
--   '6.269'
-- );
--
-- COMMIT;