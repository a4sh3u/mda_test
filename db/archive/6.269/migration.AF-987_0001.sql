START TRANSACTION;

CREATE TABLE config_backup.AF_987_Remove_Applicant_AdvisorAssignmentLog AS
  SELECT * FROM brokerage.advisor_assignment_log;

DELETE FROM brokerage.advisor_assignment_log;

INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-987_0001.sql',
  'AF-987 - Remove applicant AdvisorAssignmentLog',
  '6.269'
);

COMMIT;


-- START TRANSACTION;
--
-- INSERT INTO brokerage.advisor_assignment_log
--   SELECT * FROM config_backup.AF_987_Remove_Applicant_AdvisorAssignmentLog;
--
-- DELETE FROM config_backup.AF_987_Remove_Applicant_AdvisorAssignmentLog;
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-987_0001.sql',
--   'AF-987 - Remove applicant AdvisorAssignmentLog',
--   '6.269'
-- );
--
-- COMMIT;