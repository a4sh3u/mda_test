ALTER TABLE brokerage.brokerage_application ADD COLUMN documents_requested timestamp DEFAULT NULL, ADD COLUMN documents_sent timestamp DEFAULT NULL , ADD COLUMN email_type VARCHAR(255) DEFAULT NULL;
ALTER TABLE brokerage_application ADD COLUMN documents_requested timestamp DEFAULT NULL, ADD COLUMN documents_sent timestamp DEFAULT NULL , ADD COLUMN email_type VARCHAR(255) DEFAULT NULL;
ALTER TABLE configvalue ALTER COLUMN value TYPE text;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mcichowski',
  'migration.WEBSITE-17581_0001.sql',
  'WEBSITE-17581 - add new columns email_type, documents_requested, documents_sent for brokerage.brokerage_application and public.brokerage_application, alter column type of configvalue.value to text',
  '5.22'
);