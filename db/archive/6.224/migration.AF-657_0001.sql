START TRANSACTION;

ALTER TABLE public.advisor ADD COLUMN account_id BIGINT;
UPDATE public.advisor SET account_id = id;
ALTER TABLE public.advisor ALTER COLUMN account_id SET NOT NULL;
ALTER TABLE public.advisor DROP CONSTRAINT "public_advisor_account_FK";
ALTER TABLE public.advisor
  ADD CONSTRAINT "public_advisor_account_FK" FOREIGN KEY (account_id) REFERENCES public.account;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-657_0001.sql',
  'AF-657 - Implement composition between Advisor and Account',
  '6.224'
);

COMMIT;


-- START TRANSACTION;
--
-- UPDATE public.advisor SET id = account_id;
-- ALTER TABLE public.advisor DROP CONSTRAINT "public_advisor_account_FK";
-- ALTER TABLE public.advisor DROP COLUMN account_id;
-- ALTER TABLE public.advisor
--   ADD CONSTRAINT "public_advisor_account_FK" FOREIGN KEY (id) REFERENCES public.account;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-657_0001.sql',
--   'AF-657 - Implement composition between Advisor and Account',
--   '6.224'
-- );
--
-- COMMIT;