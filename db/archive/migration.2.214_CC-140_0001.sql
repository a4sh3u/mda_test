ALTER TABLE rule_engine_rate_class ADD rate_min double precision;
ALTER TABLE rule_engine_rate_class ADD rate_max double precision;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'kflakus',
  'migration.2.214_CC-140_0001.sql',
  'CC-140 Barclay 2.0 > Calculate risk_class_barclays variable',
  '5.07-f'
);
