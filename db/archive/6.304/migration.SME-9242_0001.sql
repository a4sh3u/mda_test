START TRANSACTION;

ALTER TABLE brokerage.brokerage_application_document ALTER COLUMN file_storage_id DROP NOT NULL;

INSERT INTO public.database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'atomecki',
    'migration.SME-9242_0001.sql',
    'SME-9242 Change file_storage_id column in brokerage_application_document to nullable',
    '6.304'
);

COMMIT;