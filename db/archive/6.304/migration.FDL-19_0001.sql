START TRANSACTION;

ALTER TABLE brokerage.brokerage_interest_rate_details_data ADD COLUMN fully_digital_offer BOOLEAN DEFAULT FALSE;

ALTER TABLE brokerage_interest_rate_details_data ADD COLUMN fully_digital_offer BOOLEAN DEFAULT FALSE;

INSERT INTO database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'mcichowski',
    'migration.FDL-19_0001.sql',
    'FDL-19 add fully digital loan offer flag to brokerageInterestRateDetailsData',
    '6.304'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_interest_rate_details_data DROP COLUMN fully_digital_offer;
--
-- ALTER TABLE brokerage_interest_rate_details_data DROP COLUMN fully_digital_offer;
--
-- INSERT INTO database_history VALUES (
--     nextval('public."MAIN_SEQUENCE"'),
--     (now()),
--     'mcichowski',
--     'migration.FDL-19_0001.sql',
--     'REVERT - FDL-19 add fully digital loan offer flag to brokerageInterestRateDetailsData',
--     '6.304'
-- );
--
-- COMMIT;