START TRANSACTION;

CREATE TABLE brokerage.brokerage_household_calculation_details
(
  id bigint NOT NULL,
  brokerage_household_calculation_id bigint NOT NULL,
  type character varying NOT NULL,
  name character varying NOT NULL,
  first_applicant_value double precision,
  second_applicant_value double precision,
  CONSTRAINT "brokerage_household_calculation_details_PK" PRIMARY KEY (id),
  CONSTRAINT "brokerage_household_calculation_details_FK" FOREIGN KEY (brokerage_household_calculation_id)
    REFERENCES brokerage.brokerage_household_calculation (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'atomecki',
  'migration.CC-2289_0001.sql',
  'CC-2289 HHC logging: save every detail value to log table',
  '6.63'
);

COMMIT;

-- START TRANSACTION;
--
-- DROP TABLE IF EXISTS brokerage.brokerage_household_calculation_details;
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'atomecki',
--   'migration.CC-2289_0001.sql',
--   'Rollback',
--   '6.63'
-- );
--
-- COMMIT;