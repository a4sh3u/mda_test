ALTER TABLE brokerage.advisor_group_filter_parameter ADD creation_date timestamp with time zone;
ALTER TABLE brokerage.advisor_group_filter_parameter ADD expiration_date timestamp with time zone;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mpetrick',
  'migration.2.212_WEBSITE-16569_0001.sql',
  'WEBSITE-16569 Group Parameter: add creation date and expriy date',
  '5.06'
);
