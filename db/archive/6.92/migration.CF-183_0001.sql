ALTER TABLE brokerage.pap_affiliate_partner ADD type CHARACTER VARYING(255) ;
ALTER TABLE brokerage.advisor_group ADD custom_condition text;
ALTER TABLE brokerage.advisor_group ADD type CHARACTER VARYING(255);

update brokerage.advisor_group set type = 'STANDARD';

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mcichowski',
  'migration.CF-183_0001.sql',
  'add type to pap_affiliate_partner add custom_condition and type to advisor_group',
  '6.92'
);