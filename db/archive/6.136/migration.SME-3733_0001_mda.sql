START TRANSACTION ;

ALTER TABLE public.brokerage_bank ADD COLUMN is_subprime Boolean DEFAULT FALSE;
ALTER TABLE brokerage.brokerage_bank ADD COLUMN is_subprime Boolean DEFAULT FALSE;
ALTER TABLE public.brokerage_bucket_configuration ADD COLUMN is_subprime Boolean DEFAULT FALSE;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mzymon',
      'migration.SME-3733_0001_mda.sql',
      'SME-3736 - Subprime bucket for application engine',
      '6.136'
);

COMMIT;
--
-- START TRANSACTION ;
--
-- ALTER TABLE public.brokerage_bank DROP COLUMN is_subprime;
-- ALTER TABLE brokerage.brokerage_bank DROP COLUMN is_subprime;
-- ALTER TABLE public.brokerage_bucket_configuration DROP COLUMN is_subprime;
--
-- INSERT INTO database_history VALUES (
--       nextval('"MAIN_SEQUENCE"'),
--       (now()),
--       'mzymon',
--       'migration.SME-3733_0001_mda.sql',
--       'SME-3736 - Subprime bucket for application engine - ROLLBACK',
--       '6.136'
-- );
--
-- COMMIT;