-- NOTE: For testing systems please comment out the entries for owner settings

DROP VIEW casi_search;

CREATE OR REPLACE VIEW casi_search AS
 SELECT p.id, p.type as type, a.id AS account_id, a.phone_code::text || a.phone::text AS phone, a.phone2_code::text || a.phone2::text AS phone2, a.email, p.first_name, p.last_name, p.date_of_birth, a.personal_advisor, l.id AS loan_application_id
   FROM account a
   LEFT JOIN person p ON a.id = p.account_id AND p.valid_until IS NULL
   LEFT JOIN loan_application l ON l.account_id = a.id AND l.creation_date = (( SELECT max(loan_application.creation_date) AS max
   FROM loan_application
  WHERE loan_application.account_id = a.id));

ALTER TABLE casi_search
  OWNER TO smava;
GRANT ALL ON TABLE casi_search
  OWNER TO smava;
GRANT SELECT ON TABLE casi_search
  OWNER TO smava_read;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pharbert',
  'migration.WEBSITE-16782_0001.sql',
  'WEBSITE-16782 - Extend casi search view by parameter loan application date',
  null
);
