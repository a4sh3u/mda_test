START TRANSACTION;

CREATE TABLE brokerage.call_journal
(
  id bigint NOT NULL,
  advisor_id bigint NOT NULL,
  borrower_id bigint NOT NULL,
  destination_number character varying(255),
  loan_application_id bigint,
  call_date_time timestamp with time zone NOT NULL,
  call_successful boolean,
  failure_reason character varying(255),
  CONSTRAINT call_journal_pkey PRIMARY KEY (id)
);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'migration.AF-580_0001.sql',
  'AF-580 - Log calls from CASI in a new database table',
  '6.209'
);

COMMIT;

-- START TRANSACTION;
--
-- DROP TABLE brokerage.call_journal;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'dmaidaniuk',
--   'rollback.AF-580_0001.sql',
--   'AF-580 - Log calls from CASI in a new database table',
--   '6.209'
-- );
--
-- COMMIT;