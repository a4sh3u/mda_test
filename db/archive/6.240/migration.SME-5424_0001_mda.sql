START TRANSACTION;

ALTER TABLE public.brokerage_bucket_configuration ADD COLUMN bucket_type VARCHAR(255) DEFAULT E'STANDARD';
ALTER TABLE public.brokerage_bucket_configuration ADD COLUMN following_bucket_type VARCHAR(255) NULL DEFAULT NULL;
ALTER TABLE public.brokerage_bank ADD COLUMN bucket_type VARCHAR(255) DEFAULT E'STANDARD';
ALTER TABLE public.brokerage_bucket ADD COLUMN processed BOOLEAN DEFAULT NULL;
ALTER TABLE brokerage.brokerage_bank ADD COLUMN bucket_type VARCHAR(255) DEFAULT E'STANDARD';

UPDATE public.brokerage_bucket_configuration SET bucket_type = 'SUBPRIME', bucket_number = 1 WHERE is_subprime = TRUE AND bucket_number = 0;
UPDATE public.brokerage_bucket_configuration SET bucket_type = 'INITIAL', bucket_number = 1 WHERE is_subprime = FALSE AND bucket_number = 0;
UPDATE public.brokerage_bucket_configuration SET delay = 120000 WHERE bucket_type = 'SUBPRIME';
UPDATE public.brokerage_bank SET bucket_type = 'SUBPRIME' WHERE is_subprime = TRUE;
UPDATE brokerage.brokerage_bank SET bucket_type = 'SUBPRIME' WHERE is_subprime = TRUE;

INSERT INTO public.brokerage_bucket_configuration (id, bucket_number, number_of_interest_rate, number_of_payout,
                                                   payout_probability, delay, sufficient_applications_number,
                                                   bucket_type, following_bucket_type) 
VALUES (nextval('public."MAIN_SEQUENCE"'), 1, 0, 0, 0, 90000, 0, 'FOLLOWING', 'VONESSEN');

INSERT INTO public.brokerage_bucket_configuration_wildcard_bank (id, brokerage_bank_id, brokerage_bucket_configuration_id) 
VALUES(nextval('public."MAIN_SEQUENCE"'),(SELECT id FROM public.brokerage_bank WHERE name = 'vonessen'),(SELECT id FROM public.brokerage_bucket_configuration WHERE following_bucket_type = 'VONESSEN'));

ALTER TABLE public.brokerage_bucket_configuration DROP COLUMN is_subprime;
ALTER TABLE public.brokerage_bank DROP COLUMN is_subprime;
ALTER TABLE brokerage.brokerage_bank DROP COLUMN is_subprime;

INSERT INTO public.database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'WTeperek',
    'migration.SME-5424_0001_mda.sql',
    'SME-5424 - Application engine: Follow-up Bucket with cancel condition',
    '6.240'
);

COMMIT;

-- START TRANSACTION;
-- 
-- ALTER TABLE public.brokerage_bank ADD COLUMN is_subprime BOOLEAN DEFAULT FALSE;
-- ALTER TABLE brokerage.brokerage_bank ADD COLUMN is_subprime BOOLEAN DEFAULT FALSE;
-- ALTER TABLE public.brokerage_bucket_configuration ADD COLUMN is_subprime BOOLEAN DEFAULT FALSE;
-- 
-- UPDATE public.brokerage_bucket_configuration SET is_subprime = FALSE, bucket_number = 0 WHERE bucket_type = 'INITIAL' AND bucket_number = 1;
-- UPDATE public.brokerage_bucket_configuration SET is_subprime = TRUE, bucket_number = 0 WHERE bucket_type = 'SUBPRIME' AND bucket_number = 1;
-- UPDATE public.brokerage_bucket_configuration SET delay = 90000 WHERE bucket_type = 'SUBPRIME';
-- UPDATE public.brokerage_bank SET is_subprime = TRUE WHERE bucket_type = 'SUBPRIME';
-- UPDATE brokerage.brokerage_bank SET is_subprime = TRUE WHERE bucket_type = 'SUBPRIME';
-- 
-- DELETE FROM public.brokerage_bucket_configuration_wildcard_bank wild WHERE wild.brokerage_bucket_configuration_id 
--        IN (SELECT id FROM public.brokerage_bucket_configuration bbc WHERE bbc.bucket_type = 'FOLLOWING');
-- DELETE FROM public.brokerage_bucket_configuration bbc WHERE bbc.bucket_type = 'FOLLOWING';
-- 
-- ALTER TABLE public.brokerage_bucket_configuration DROP COLUMN bucket_type;
-- ALTER TABLE public.brokerage_bucket_configuration DROP COLUMN following_bucket_type;
-- ALTER TABLE public.brokerage_bank DROP COLUMN bucket_type;
-- ALTER TABLE brokerage.brokerage_bank DROP COLUMN bucket_type;
-- ALTER TABLE public.brokerage_bucket DROP COLUMN processed;
-- 
-- INSERT INTO public.database_history VALUES (
--     nextval('public."MAIN_SEQUENCE"'),
--     (now()),
--     'WTeperek',
--     'migration.SME-5424_0001_mda.sql',
--     'SME-5424 - Application engine: Follow-up Bucket with cancel condition - ROLLBACK',
--     '6.240'
-- );
-- 
-- COMMIT;