create table brokerage.brokerage_filter_reason_multi_val (
  id bigint primary key not null,
  loan_application_id bigint not null,
  brokerage_application_id bigint not null,
  applicant_id bigint,
  filter_level character varying(50),
  filter_reason_id bigint not null,

  CONSTRAINT "filter_reason_FK" FOREIGN KEY (filter_reason_id) REFERENCES brokerage.brokerage_application_filter_reason(id),
  CONSTRAINT "loan_application_FK" FOREIGN KEY (loan_application_id) REFERENCES brokerage.loan_application(id),
  CONSTRAINT "brokerage_application_FK" FOREIGN KEY (brokerage_application_id) REFERENCES brokerage.brokerage_application(id)
);

INSERT INTO public.database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'ABudzisnki',
  'migration.SME-6744_0001_mda.sql',
  'SME-6744 - Integrate smarket-engines with brokerage-side code',
  '6.240'
);