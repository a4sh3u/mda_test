START TRANSACTION;

ALTER TABLE brokerage.brokerage_application
  ADD COLUMN is_second_applicant_allowed boolean DEFAULT NULL;


INSERT INTO public.database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'ABudzisnki',
  'migration.SME-6744_0002_mda.sql',
  'SME-6744 - Integrate smarket-engines with brokerage-side code',
  '6.240'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_application DROP COLUMN is_second_applicant_allowed;
--
-- INSERT INTO public.database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'ABudzisnki',
--   'migration.SME-6744_0002_mda.sql',
--   'rollback',
--   '6.240'
-- );
--
-- COMMIT;