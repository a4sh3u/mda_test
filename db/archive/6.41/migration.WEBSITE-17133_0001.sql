﻿CREATE TABLE brokerage.video_identification
(
  id bigint NOT NULL,
  status character varying(255),
  brokerage_application_id bigint,
  applicant_id bigint,
  creation_date TIMESTAMP WITH TIME ZONE NOT NULL,
  update_date TIMESTAMP WITH TIME ZONE,

  CONSTRAINT video_identification_pk PRIMARY KEY (id),
  CONSTRAINT video_identification_brokerage_application_id FOREIGN KEY (brokerage_application_id)
      REFERENCES brokerage.brokerage_application(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT  video_identification_applicant_id  FOREIGN KEY (applicant_id)
      REFERENCES brokerage.applicant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);
--ALTER TABLE brokerage.video_identification
--  OWNER TO smava;
--GRANT ALL ON TABLE brokerage.video_identification
--  OWNER TO smava;
--GRANT SELECT ON TABLE brokerage.video_identification
--  OWNER TO smava_read;




DROP table brokerage.identification_brokerage_bank_result_applicant;
DROP TABLE brokerage.identification_brokerage_bank_result;

CREATE TABLE brokerage.identification_brokerage_bank_result
(
  id bigint NOT NULL,
  account_id bigint NOT NULL,
  brokerage_bank_id bigint NOT NULL,

  CONSTRAINT identification_brokerage_bank_result_pkey PRIMARY KEY (id),
  CONSTRAINT "identification_brokerage_bank_result_FK1" FOREIGN KEY (brokerage_bank_id)
      REFERENCES brokerage.brokerage_bank (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT "identification_brokerage_bank_result_FK2" FOREIGN KEY (account_id)
      REFERENCES brokerage.account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);

CREATE INDEX "identification_brokerage_bank_result_N49"
  ON brokerage.identification_brokerage_bank_result
  USING btree
  (brokerage_bank_id);

-- Index: brokerage."identification_brokerage_bank_result_N50"

-- DROP INDEX brokerage."identification_brokerage_bank_result_N50";

CREATE INDEX "identification_brokerage_bank_result_N50"
  ON brokerage.identification_brokerage_bank_result
  USING btree
  (account_id);



--ALTER TABLE brokerage.identification_brokerage_bank_result
--  OWNER TO smava;
--GRANT ALL ON TABLE brokerage.identification_brokerage_bank_result
--  OWNER TO smava;
--GRANT SELECT ON TABLE brokerage.identification_brokerage_bank_result
--  OWNER TO smava_read;




CREATE TABLE brokerage.identification_brokerage_bank_result_applicant
(
  id bigint NOT NULL,
  identification_brokerage_bank_result_id bigint NOT NULL,
  applicant_id bigint NOT NULL,
  creation_date timestamp with time zone NOT NULL,
  response_data text,
  state character varying(255),
  type character varying(255),

  CONSTRAINT identification_brokerage_bank_result_applicant_pkey PRIMARY KEY (id),
  CONSTRAINT "identification_brokerage_bank_result_applicant_FK1" FOREIGN KEY (identification_brokerage_bank_result_id)
      REFERENCES brokerage.identification_brokerage_bank_result (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
   CONSTRAINT "identification_brokerage_bank_result_applicant_FK2" FOREIGN KEY (applicant_id)
      REFERENCES brokerage.applicant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);



CREATE TABLE brokerage.http_log (
  id            SERIAL,
  creation_date TIMESTAMP WITH TIME ZONE,
  type          CHARACTER VARYING(255),
  url           CHARACTER VARYING(1000),
  method        CHARACTER VARYING(255),
  body          TEXT,
  params        TEXT,
  headers       TEXT,
  response      TEXT,
  CONSTRAINT http_log_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--ALTER TABLE brokerage.http_log
--  OWNER TO smava;
--GRANT ALL ON TABLE brokerage.http_log
--  OWNER TO smava;
--GRANT SELECT ON TABLE brokerage.http_log
--  OWNER TO smava_read;



START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aibrahim',
      'migration.WEBSITE-17133_0001.sql',
      'WEBSITE-17133 - new table to store video ident information',
      '6.41'
);

COMMIT;
