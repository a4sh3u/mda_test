CREATE INDEX bank_state_index
ON brokerage.brokerage_application (brokerage_bank_id, state);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'kTomczyk',
  'migration.SME-8828_0001.sql',
  'SME-8828 Split BROKERAGE_RUN_STATE_REQUEST_PENDING queue',
  '6.297'
);
