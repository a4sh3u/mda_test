START TRANSACTION;

ALTER TABLE brokerage.brokerage_multi_status_request_data
  ADD COLUMN http_state VARCHAR DEFAULT NULL;

INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'mptasinski',
  'migration.SME-7155_0001.sql',
  'SME-7155 Add missing HTTP status logs for MultiState request',
  '6.249'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_multi_status_request_data
--   DROP COLUMN http_state;
--
-- INSERT INTO public.database_history VALUES (
--  nextval('public."MAIN_SEQUENCE"'),
--  (now()),
--  'mptasinski',
--  'migration.SME-7155_0001.sql',
--  'REVERT SME-7155 Add missing HTTP status logs for MultiState request',
--  '6.249'
-- );
--
-- COMMIT;


