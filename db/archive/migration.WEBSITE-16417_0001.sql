-- NOTE: For testing systems please comment out the entries for owner settings

CREATE OR REPLACE VIEW casi_search AS 
 SELECT a.id, a.phone_code::text || a.phone::text AS phone, a.phone2_code::text || a.phone2::text AS phone2, a.email, p.first_name, p.last_name, p.date_of_birth, a.personal_advisor
   FROM account a
   LEFT JOIN person p ON a.id = p.account_id AND p.valid_until IS NULL;

ALTER TABLE casi_search
  OWNER TO smava;
GRANT ALL ON TABLE casi_search
  OWNER TO smava;
GRANT SELECT ON TABLE casi_search
  OWNER TO smava_read;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'pharbert',
  'migration.WEBSITE-16417_0001.sql',
  'WEBSITE-16417 - Extend casi search view by parameter personal advisor',
  '5.05'
);
