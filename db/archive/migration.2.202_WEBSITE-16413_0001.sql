CREATE INDEX "event_queue_N49"
  ON public.event_queue
  USING btree
  (bank_id);

CREATE INDEX "event_queue_N50"
  ON public.event_queue
  USING btree
  (loan_application_id);

CREATE INDEX "event_queue_N51"
  ON public.event_queue
  USING btree
  (account_id);

CREATE INDEX "event_queue_N52"
  ON public.event_queue
  USING btree
  (type);

  INSERT INTO database_history VALUES (
	nextval('"MAIN_SEQUENCE"'),
	(now()),
	'mpetrick',
	'migration.2.202_WEBSITE-16413_0001.sql',
	'WEBSITE-16413 - Indexing public event queue table for faster queries',
    '5.05'
);