START TRANSACTION;

ALTER TABLE brokerage.brokerage_application_request_data
  ADD COLUMN http_state VARCHAR DEFAULT NULL;

INSERT INTO public.database_history VALUES (
  nextval('public."MAIN_SEQUENCE"'),
  (now()),
  'jjanus',
  'migration.SME-6757_0001.sql',
  'SME-6757 Technical Solution: Determine BARDS that FAIL because of an HTTP Error Code',
  '6.231'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE brokerage.brokerage_application_request_data DROP COLUMN http_state;
--
-- INSERT INTO public.database_history VALUES (
--   nextval('public."MAIN_SEQUENCE"'),
--   (now()),
--   'jjanus',
--   'migration.SME-6757_0001.sql',
--   'Rollback',
--   '6.231'
-- );
--
-- COMMIT;