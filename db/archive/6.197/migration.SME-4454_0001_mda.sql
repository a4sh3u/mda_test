CREATE TABLE brokerage.external_customer_accounts_info (
    id bigint PRIMARY KEY NOT NULL,
    account_id bigint NULL,
    type VARCHAR,
    external_id VARCHAR,
    creation_date timestamptz NULL,
    change_date timestamptz NULL,
    UNIQUE (account_id, type),
    CONSTRAINT "external_customer_accounts_info_FK" FOREIGN KEY (account_id) REFERENCES brokerage.account(id)
);

CREATE INDEX "external_customer_accounts_info_account_id"
  ON brokerage.external_customer_accounts_info
  USING btree
  (account_id);

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mzymon',
      'migration.SME-4454_0001_mda.sql',
      'SME-4473 - add ADAC Membership',
      '6.197'
);