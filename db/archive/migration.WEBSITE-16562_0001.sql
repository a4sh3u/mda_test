ALTER TABLE database_history ADD release_version character varying(255);

INSERT INTO database_history VALUES (
	nextval('"MAIN_SEQUENCE"'),
	(now()),
	'pharbert',
	'migration.WEBSITE-16562_0001.sql',
	'WEBSITE-16562 - New column for db table database_history',
	null
);