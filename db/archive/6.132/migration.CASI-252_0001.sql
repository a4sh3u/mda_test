CREATE INDEX brokerage_journal_brokerage_application_id
  ON public.brokerage_journal
  USING btree
  (brokerage_application_id);

CREATE INDEX barer_brokerage_application_id
  ON public.brokerage_application_rule_engine_result
  USING btree
  (brokerage_application_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mbiletskyi',
  'migration.CASI-252_0001.sql',
  'CASI-252_0001 - increasing loading customer page',
  '6.132'
);