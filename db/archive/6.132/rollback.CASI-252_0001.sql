DROP INDEX public.brokerage_journal_brokerage_application_id;

DROP INDEX public.barer_brokerage_application_id;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mbiletskyi',
      'rollback.CASI-252_0001.sql',
      'CASI-252_0001 - increasing loading customer page',
      '6.132'
      );