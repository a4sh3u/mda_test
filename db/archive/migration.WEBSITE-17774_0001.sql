DROP TABLE IF EXISTS public.advisor_notification;

CREATE TABLE public.advisor_notification (
	id bigint NOT NULL,
	brokerage_application_id bigint NOT NULL,
	event_type CHARACTER VARYING NOT NULL,
  timestamp timestamp with time zone NOT NULL,
  read BOOLEAN DEFAULT false
)
WITH (
OIDS=FALSE
);

CREATE INDEX "advisor_notification_brokerage_application"
  ON public.advisor_notification
  USING btree
  (brokerage_application_id);



--ALTER TABLE pap_affiliate_partner OWNER TO smava;
--GRANT ALL ON TABLE pap_affiliate_partner TO smava;
--GRANT SELECT ON TABLE pap_affiliate_partner TO smava_read;

DROP TABLE IF EXISTS brokerage.advisor_notification;

CREATE TABLE brokerage.advisor_notification (
	id bigint NOT NULL,
	brokerage_application_id bigint NOT NULL,
	event_type CHARACTER VARYING NOT NULL,
  timestamp timestamp with time zone NOT NULL,
	read BOOLEAN DEFAULT false
)
WITH (
OIDS=FALSE
);

CREATE INDEX "advisor_notification_brokerage_application"
  ON brokerage.advisor_notification
  USING btree
  (brokerage_application_id);

--ALTER TABLE pap_affiliate_partner OWNER TO smava;
--GRANT ALL ON TABLE pap_affiliate_partner TO smava;
--GRANT SELECT ON TABLE pap_affiliate_partner TO smava_read;


INSERT INTO database_history VALUES (
	nextval('"MAIN_SEQUENCE"'),
	(now()),
	'pvitic',
	'migration.WEBSITE-17774_0001.sql',
	'WEBSITE-17774 - Create advisor notification tables',
   null
);