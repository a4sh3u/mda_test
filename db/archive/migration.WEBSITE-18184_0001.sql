ALTER TABLE brokerage.brokerage_household_calculation
ADD COLUMN income_second_applicant double precision,
ADD COLUMN expenses_second_applicant double precision,
ADD COLUMN disposable_income_second_applicant double precision,
ADD COLUMN indebtedness_second_applicant double precision;

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'asaadat',
      'migration.WEBSITE-18184_0001.sql',
      'WEBSITE-18184_0001 - HHC as a Service: Additional fields in brokerApplication for disposableIncome and indebtedness',
      '5.26.2'
);

COMMIT;