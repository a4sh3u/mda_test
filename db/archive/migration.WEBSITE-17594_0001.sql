ALTER TABLE brokerage.brokerage_application ADD COLUMN household_calculation_id bigint;

CREATE TABLE brokerage.brokerage_household_calculation
(
  id bigint NOT NULL,
  brokerage_application_id bigint NOT NULL,
  income double precision,
  expenses double precision,
  disposable_income double precision,
  indebtedness double precision,
  creation_date timestamp with time zone NOT NULL,
  CONSTRAINT brokerage_household_calc_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'asaadat',
  'migration.WEBSITE-17594_0001.sql',
  'WEBSITE-17594 - expand database for household calculation as a service',
  '5.23'
);

