
CREATE TABLE brokerage.external_third_party_loan
(
  id bigint NOT NULL,
  bank_account_id bigint,
  drawing_limit double precision,
  effective_interest double precision,
  loan_end_date timestamp with time zone,
  loan_id bigint,
  loan_start_date timestamp with time zone,
  loan_type character varying(255),
  monthly_rate double precision,
  original_amount double precision,
  redemption_sum double precision,
  same_credit_bank_account boolean,
  schufa_token_code character varying(255),
  loan_source character varying(255),
  CONSTRAINT external_third_party_loan_pkey PRIMARY KEY (id),
  CONSTRAINT "external_third_party_loan_FK1" FOREIGN KEY (bank_account_id)
      REFERENCES brokerage.bank_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);


CREATE TABLE brokerage.applicant_external_third_party_loan
(
  applicant_id bigint NOT NULL,
  external_third_party_loan_id bigint NOT NULL,
  CONSTRAINT applicant_external_third_party_loan_pkey PRIMARY KEY (applicant_id, external_third_party_loan_id),
  CONSTRAINT "applicant_external_third_party_loan_FK1" FOREIGN KEY (external_third_party_loan_id)
      REFERENCES brokerage.external_third_party_loan (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT "applicant_external_third_party_loan_FK2" FOREIGN KEY (applicant_id)
      REFERENCES brokerage.applicant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);


CREATE INDEX "applicant_external_third_party_loan_N49"
  ON brokerage.applicant_external_third_party_loan
  USING btree
  (external_third_party_loan_id);


CREATE INDEX "applicant_external_third_party_loan_N50"
  ON brokerage.applicant_external_third_party_loan
  USING btree
  (applicant_id);

CREATE TABLE brokerage.customer_third_party_loan
(
  id bigint NOT NULL,
  bank_account_id bigint,
  consolidation_wish boolean NOT NULL,
  drawing_limit double precision,
  effective_interest double precision,
  loan_end_date timestamp with time zone,
  loan_id bigint,
  loan_start_date timestamp with time zone,
  loan_type character varying(255),
  monthly_rate double precision,
  original_amount double precision,
  redemption_sum double precision,
  same_credit_bank_account boolean,
  loan_source character varying(255),
  CONSTRAINT customer_third_party_loan_pkey PRIMARY KEY (id),
  CONSTRAINT "customer_third_party_loan_FK1" FOREIGN KEY (bank_account_id)
      REFERENCES brokerage.bank_account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);


CREATE TABLE brokerage.applicant_customer_third_party_loan
(
  applicant_id bigint NOT NULL,
  customer_third_party_loan_id bigint NOT NULL,
  CONSTRAINT applicant_customer_third_party_loan_pkey PRIMARY KEY (applicant_id, customer_third_party_loan_id),
  CONSTRAINT "applicant_customer_third_party_loan_FK1" FOREIGN KEY (customer_third_party_loan_id)
      REFERENCES brokerage.customer_third_party_loan (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT "applicant_customer_third_party_loan_FK2" FOREIGN KEY (applicant_id)
      REFERENCES brokerage.applicant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);


CREATE INDEX "applicant_customer_third_party_loan_N49"
  ON brokerage.applicant_customer_third_party_loan
  USING btree
  (customer_third_party_loan_id);


CREATE INDEX "applicant_customer_third_party_loan_N50"
  ON brokerage.applicant_customer_third_party_loan
  USING btree
  (applicant_id);

ALTER TABLE brokerage.third_party_loan ADD COLUMN customer_third_party_loan_id bigint;
ALTER TABLE brokerage.third_party_loan ADD COLUMN external_third_party_loan_id bigint;

ALTER TABLE brokerage.third_party_loan ADD   CONSTRAINT "third_party_loan_FK2" FOREIGN KEY (customer_third_party_loan_id)
      REFERENCES brokerage.customer_third_party_loan (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;


ALTER TABLE brokerage.third_party_loan ADD   CONSTRAINT "third_party_loan_FK3" FOREIGN KEY (external_third_party_loan_id)
      REFERENCES brokerage.external_third_party_loan (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;

CREATE TABLE brokerage.third_party_loan_processing_log
(
  id BIGINT PRIMARY KEY NOT NULL,
  external_third_party_loan_id  BIGINT,
  customer_third_party_loan_id  BIGINT,
  third_party_loan_id           BIGINT,
  processing_result             CHARACTER VARYING(255),

  FOREIGN KEY (external_third_party_loan_id) REFERENCES brokerage.external_third_party_loan (id),
  FOREIGN KEY (customer_third_party_loan_id) REFERENCES brokerage.customer_third_party_loan (id),
  FOREIGN KEY (third_party_loan_id) REFERENCES brokerage.third_party_loan (id)
);

CREATE TABLE brokerage.third_party_loan_merge_settings
(
  id BIGINT PRIMARY KEY NOT NULL,
  schufa_token_code     CHARACTER VARYING(255),
  maximum_merge_level  INTEGER NOT NULL,
  CONSTRAINT "third_party_loan_merge_settings_unique_schufa_token_code" UNIQUE (schufa_token_code)
);



INSERT INTO database_history VALUES(
    nextval('"MAIN_SEQUENCE"'),
    (now()),
    'lulitzky',
    'migration.WEBSITE-17597_0001.sql',
    'WEBSITE-17597: Übernahme Schufa-Fremdkredite in application',
    '5.26'
);