ALTER TABLE sector ADD COLUMN is_official_sector boolean DEFAULT false;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'lbinh',
  'migration.WEBSITE-17616_0001.sql',
  'WEBSITE-17616 - add new column is_official_sector for sector'
);