ALTER TABLE lead_cycle ADD latest_loan_application_id bigint;

ALTER TABLE lead_cycle ADD CONSTRAINT latest_loan_application_fk FOREIGN KEY (latest_loan_application_id)
REFERENCES loan_application (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'achemparathy',
  'migration.WEBSITE-18545_0001.sql',
  'add latest loan application in lead_cyle',
  '6.35'
);