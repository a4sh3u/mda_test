START TRANSACTION;

CREATE TABLE IF NOT EXISTS brokerage.save_to_mail(
	id bigint NOT NULL,
	registration_route text,
	current_page text,
	loan_application_id bigint ,
	mail_triggered timestamp with time zone,
	CONSTRAINT pk_save_to_mail PRIMARY KEY (id),
	CONSTRAINT loan_application_fk FOREIGN KEY (loan_application_id)
      REFERENCES brokerage.loan_application (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);


INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'achemparathy',
      'migration.WEBSITE-18505_0001.sql from mda',
      'support save to mail feature',
      '6.35'
);

COMMIT;