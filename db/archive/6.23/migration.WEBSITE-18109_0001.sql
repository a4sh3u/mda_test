CREATE TABLE IF NOT EXISTS lead_cycle(
	id bigint NOT NULL,
	account_id bigint NOT NULL,
	first_loan_application_id bigint NOT NULL,
	state varchar NOT NULL,
	last_touchpoint timestamp with time zone NOT NULL,
	creation_date timestamp with time zone NOT NULL,
	state_changed_date timestamp with time zone NOT NULL,
	affiliate_information_id bigint NOT NULL,
	CONSTRAINT pk_lead_cycle PRIMARY KEY (id)
)
WITH (OIDS=FALSE);

ALTER TABLE lead_cycle ADD CONSTRAINT account_fk FOREIGN KEY (account_id)
REFERENCES account (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE lead_cycle ADD CONSTRAINT loan_application_fk FOREIGN KEY (first_loan_application_id)
REFERENCES loan_application (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE lead_cycle ADD CONSTRAINT affiliate_information_fk FOREIGN KEY (affiliate_information_id)
REFERENCES affiliate_information (id) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE NO ACTION;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'achemparathy',
  'migration.WEBSITE-18109_0001.sql',
  'WEBSITE-18109 - new table schema for lead_cycle',
  '6.23'
);
