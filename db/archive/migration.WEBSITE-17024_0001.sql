CREATE TABLE brokerage.household_calculation (
	id bigint NOT NULL,
	type character varying NOT NULL,
	name character varying NOT NULL,
	mapping_expression character varying NOT NULL,
	CONSTRAINT "brokerage_household_calculation_PK" PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

CREATE TABLE brokerage.bank_household_calculation (
	id bigint NOT NULL,
	brokerage_bank_id bigint NOT NULL,
	household_calculation_id bigint NOT NULL,
	bank_expression character varying NOT NULL,
	CONSTRAINT "brokerage_bank_household_calculation_PK" PRIMARY KEY (id),
	CONSTRAINT brokerage_bank_household_calculation_fk_brokerage_bank FOREIGN KEY (brokerage_bank_id)
      REFERENCES brokerage.brokerage_bank (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT brokerage_bank_household_calculation_fk_household_calculation FOREIGN KEY (household_calculation_id)
  REFERENCES brokerage.household_calculation (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mpetrick',
  'migration.WEBSITE-17024_0001.sql',
  'WEBSITE-17024 - Create household calculation-related tables',
  '5.14.0'
);