insert into brokerage.advisor_notification (
  select *, "timestamp", "timestamp" from public.advisor_notification
);

UPDATE brokerage.advisor_notification
SET borrower_id = public.brokerage_entity_map.brokerage_id
FROM public.brokerage_entity_map
WHERE public.brokerage_entity_map.kredit_privat_id = brokerage.advisor_notification.borrower_id ;

UPDATE brokerage.advisor_notification
SET brokerage_application_id = public.brokerage_entity_map.brokerage_id
FROM public.brokerage_entity_map
WHERE public.brokerage_entity_map.kredit_privat_id = brokerage.advisor_notification.brokerage_application_id ;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'ovolkovsky',
  'migration.AF-650_0001.sql',
  'Move AdvisorNotification from public to brokerage',
  '6.228'
);


-- truncate brokerage.advisor_notification;

-- INSERT INTO database_history VALUES (
--  nextval('"MAIN_SEQUENCE"'),
--  (now()),
--  'ovolkovsky',
--  'rollback.AF-650_0001.sql',
--  'Move Brokerage Journal from public to brokerage',
--  '6.228'
-- );