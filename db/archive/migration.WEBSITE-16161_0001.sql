-- Table: public.investor_asset

-- DROP TABLE public.investor_asset;

CREATE TABLE public.investor_asset
(
  id bigint NOT NULL,
  amount double precision,
  creation_date timestamp with time zone,
  active boolean,
  account_id bigint,
  CONSTRAINT investor_asset_pkey PRIMARY KEY (id),
  CONSTRAINT "investor_asset_account_FK" FOREIGN KEY (account_id)
      REFERENCES public.account (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);

-- Index: public."investor_asset_account"

-- DROP INDEX public."investor_asset_account";

CREATE INDEX "investor_asset_account"
  ON public.investor_asset
  USING btree
  (account_id);


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'asaadat',
  'migration.WEBSITE-16161_0001.sql',
  'WEBSITE-16161 - Save investor asset in log table',
  '5.20.4'
);
