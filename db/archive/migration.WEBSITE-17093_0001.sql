CREATE TABLE brokerage.debt_type_weight (
	id bigint NOT NULL,
	debt_type character varying(255) NOT NULL,
	weight double precision,
	brokerage_bank_id bigint,
	CONSTRAINT "debt_type_weight_PK" PRIMARY KEY (id),
	CONSTRAINT "debt_type_weight_FK_brokerage_bank" FOREIGN KEY (brokerage_bank_id)
	REFERENCES brokerage_bank (id) MATCH SIMPLE
)
WITH (
OIDS=FALSE
);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mpetrick',
  'migration.WEBSITE-17093_0001.sql',
  'WEBSITE-17093 Create indebtedness calculation related tables',
  '5.14.0'
);