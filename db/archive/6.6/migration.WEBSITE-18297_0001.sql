CREATE TABLE public.email_coupon (
    id bigint PRIMARY KEY NOT NULL,
    creation_date timestamp with time zone,
    expiration_date timestamp with time zone,
    account_id bigint,
    loan_application_id bigint,
    amount integer,
    type text,
    payout boolean,
    payout_validation_date timestamp with time zone,
    coupon_generated boolean,
    coupon_generated_date timestamp with time zone
);

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mcichowski',
      'migration.WEBSITE-18297_0001.sql',
      'WEBSITE-18297_0001 - add table for email coupon tracking',
      '6.6'

);