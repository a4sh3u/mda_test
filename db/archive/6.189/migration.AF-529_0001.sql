START TRANSACTION;

ALTER TABLE public.advisor
  ADD COLUMN sip VARCHAR(255);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-529_0001.sql',
  'Add SIP columns to Advisor table',
  '6.189'
);

COMMIT;


-- START TRANSACTION;
-- 
-- ALTER TABLE public.advisor
--   DROP COLUMN sip;
-- 
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-529_0001.sql',
--   'Add SIP column to Advisor table',
--   '6.189'
-- );
-- 
-- COMMIT;

