ALTER TABLE brokerage_application ADD payout_date timestamp with time zone;
ALTER TABLE brokerage.brokerage_application ADD payout_date timestamp with time zone;


INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dkeller',
  'migration.WEBSITE-18840_0001.sql',
  'add payout date',
  '6.85'
);