START TRANSACTION ;

ALTER TABLE public.advisor_assignment_log DROP CONSTRAINT unique_advisory_assignment;

ALTER TABLE brokerage.advisor_assignment_log DROP CONSTRAINT unique_advisory_assignment;

COMMIT;

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'rollback.CASI-192_0002.sql',
      'CASI-192 - Multiple advisors "assigned" to the same customer',
      '6.111'
);

COMMIT;