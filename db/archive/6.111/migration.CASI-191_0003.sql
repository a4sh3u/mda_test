--------------------------------------------------------------------------------
-- BUS: Lead Assignment Value - create a time factor function (ANA-1174)	  --
--                                                                            --
-- change the time values within configvalue table so that the new time       --
-- function can take effect.												  --
--																			  --
-- author: paulo                                                              --
-- email: paulo.morales@smava.de                                              --
--------------------------------------------------------------------------------


start transaction;

UPDATE public.configvalue
SET value = 20160
where key = 'casi.leadAssignment.timeFactor.timegap6';

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pmorales',
      'migration.CASI-191_0003.sql',
      'changing the time factor value so that updates from to ANA-1174 take place',
      '6.111'
      );

commit;