START TRANSACTION ;

ALTER TABLE public.advisor_assignment_log ADD CONSTRAINT unique_advisory_assignment UNIQUE (borrower_id, loan_application_id, customer_type);

ALTER TABLE brokerage.advisor_assignment_log ADD CONSTRAINT unique_advisory_assignment UNIQUE (borrower_id, loan_application_id, customer_type);

COMMIT;

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'migration.CASI-192_0002.sql',
      'CASI-192 - Multiple advisors "assigned" to the same customer',
      '6.111'
);

COMMIT;