CREATE OR REPLACE FUNCTION calculate_customer_value_timefactor (
  customer_value double precision,
  lapp_date timestamp with time zone,
  curr_date timestamp with time zone,
  loan_application_state text,
  timegap0 double precision,
  timevalue0 double precision
) RETURNS double precision AS
$BODY$
DECLARE
  timegap double precision;
BEGIN
  timegap = (SELECT EXTRACT (EPOCH FROM (AGE(curr_date, lapp_date))) / 60);

  if (loan_application_state = 'APPLICATION_INCOMPLETE') then
    if (timegap < timegap0) then
      RETURN customer_value * timevalue0;
    end if;
  end if;

  RETURN customer_value * (exp(-8.775e-02 + -3.773e-04 * timegap) + 0.08745979);
END
$BODY$
LANGUAGE plpgsql;


START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'migration.CASI-191_0002.sql',
      'CASI-191 - Lead Assignment - schufa negativ identification',
      '6.111'
);

COMMIT;