START TRANSACTION ;

delete from public.advisor_assignment_log where id IN
(
 select aal1.id from public.advisor_assignment_log aal1, public.advisor_assignment_log aal2
 where aal1.id != aal2.id
 and aal1.borrower_id = aal2.borrower_id
 and aal1.loan_application_id = aal2.loan_application_id
 and aal1.customer_type = aal2.customer_type
 and aal1.credit_advisor_id !=  (select personal_advisor from public.account where id = aal1.borrower_id)
);

COMMIT;

START TRANSACTION ;

delete from brokerage.advisor_assignment_log where id IN
(
 select aal1.id from brokerage.advisor_assignment_log aal1, brokerage.advisor_assignment_log aal2
 where aal1.id != aal2.id
 and aal1.borrower_id = aal2.borrower_id
 and aal1.loan_application_id = aal2.loan_application_id
 and aal1.customer_type = aal2.customer_type
 and aal1.credit_advisor_id !=  (select personal_advisor_id from brokerage.account where id = aal1.borrower_id)
);

COMMIT;

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'migration.CASI-192_0001.sql',
      'CASI-192 - Multiple advisors "assigned" to the same customer',
      '6.111'
);

COMMIT;