--------------------------------------------------------------------------------
-- BUS: Customer Value - change the employment factor (ANA-1172)		      --
--                                                                            --
-- Set the relevance factor of KDF negativ customers from 0.25 to 0.0         --
--																			  --
-- author: paulo                                                              --
-- email: paulo.morales@smava.de                                              --
--------------------------------------------------------------------------------


start transaction;

UPDATE public.configvalue
SET value = 0.25
where key = 'casi.leadAssignment.relevanceFactor.leadKdf';

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pmorales',
      'rollback.CASI-191_0003.sql',
      'rollback of KDF negativ relevnce factor from 0 to 0.25 added by ANA-1017',
      '6.111'
      );

commit;