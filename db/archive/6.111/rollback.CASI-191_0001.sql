DROP FUNCTION calculate_customer_value_relevancefactor(double precision, bigint);

CREATE OR REPLACE FUNCTION calculate_customer_value_relevancefactor(
    customer_value double precision,
    brokerage_loan_application_id bigint)
  RETURNS double precision AS
$BODY$
DECLARE
  state varchar(255);
  has_brokerage_applications boolean;
  has_applied_brokerage_applications boolean;
  is_schufa_positive boolean;
  qualified_lead double precision;
  lead_no_offer double precision;
  lead_kdf double precision;
  short_lead double precision;
  lead_schufa_negative double precision;
BEGIN
  SELECT la.state into state from brokerage.loan_application la where id = brokerage_loan_application_id;
  SELECT count(ba) > 0 into has_brokerage_applications from brokerage.brokerage_application ba where ba.loan_application_id = brokerage_loan_application_id;
  SELECT count(ba) > 0 into has_applied_brokerage_applications from brokerage.brokerage_application ba where ba.state = 'APPLICATION_APPLIED' and ba.loan_application_id = brokerage_loan_application_id;
  SELECT ss.state = 'success' into is_schufa_positive from credit_score cs, schufa_score ss where cs.schufa_score_id = ss.id and ss.account = (select la.account_id from brokerage.loan_application bla, brokerage_entity_map map, loan_application la where bla.id = map.brokerage_id and map.kredit_privat_id = la.id and bla.id = brokerage_loan_application_id) and cs.type = 'MAIN_BORROWER' order by cs.change_date desc limit 1;

  SELECT c.value into qualified_lead from configvalue c where key = 'casi.leadAssignment.relevanceFactor.qualifiedLead';
  SELECT c.value into lead_no_offer from configvalue c where key = 'casi.leadAssignment.relevanceFactor.leadNoOffer';
  SELECT c.value into lead_kdf from configvalue c where key = 'casi.leadAssignment.relevanceFactor.leadKdf';
  SELECT c.value into short_lead from configvalue c where key = 'casi.leadAssignment.relevanceFactor.shortLead';
  SELECT c.value into lead_schufa_negative from configvalue c where key = 'casi.leadAssignment.relevanceFactor.leadSchufaNegative';

  if (is_schufa_positive is false) then
    RETURN customer_value * lead_schufa_negative;
  elsif (brokerage_loan_application_id is null or state = 'APPLICATION_INCOMPLETE') then
    RETURN customer_value * short_lead;
  elsif (has_applied_brokerage_applications is true) then
    RETURN customer_value * qualified_lead;
  elsif (has_applied_brokerage_applications is false and has_brokerage_applications is true) then
    RETURN customer_value * lead_no_offer;
  else
    RETURN customer_value * lead_kdf;
  end if;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'rollback.CASI-191_0001.sql',
      'CASI-191 - Lead Assignment - schufa negativ identification',
      '6.111'
      );

commit;
