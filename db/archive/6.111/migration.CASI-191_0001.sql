DROP FUNCTION calculate_customer_value_relevancefactor(double precision, bigint);

CREATE OR REPLACE FUNCTION calculate_customer_value_relevancefactor(
    customer_value double precision,
    brokerage_loan_application_id bigint,
    qualified_lead double precision,
    lead_no_offer double precision,
    lead_kdf double precision,
    short_lead double precision,
    lead_schufa_negative double precision)
  RETURNS double precision AS
$BODY$
DECLARE
  state varchar(255);
  has_brokerage_applications boolean;
  has_applied_brokerage_applications boolean;
  is_schufa_positive boolean;
BEGIN
  SELECT la.state into state from brokerage.loan_application la where id = brokerage_loan_application_id;
  SELECT count(ba) > 0 into has_brokerage_applications from brokerage.brokerage_application ba where ba.loan_application_id = brokerage_loan_application_id;
  SELECT count(ba) > 0 into has_applied_brokerage_applications from brokerage.brokerage_application ba where ba.state = 'APPLICATION_APPLIED' and ba.loan_application_id = brokerage_loan_application_id;
  SELECT count(*) = 0 into is_schufa_positive from public.brokerage_entity_map inner join public.loan_application
  on
    brokerage_entity_map.brokerage_id = brokerage_loan_application_id
    and brokerage_entity_map.kredit_privat_id = loan_application.id
  inner join
    public.schufa_score
  on
    loan_application.account_id = schufa_score.account
    and loan_application.creation_date - interval '60 days' <=
    schufa_score.change_date
    and loan_application.creation_date + INTERVAL '5 days' >
    schufa_score.change_date
    and schufa_score.state = 'failed'
  inner join
    public.schufa_request
  on
    schufa_score.request_id = schufa_request.id
  inner join
    public.person
  on
    person.id = schufa_request.person_id
    and person.type = 1;

  if (is_schufa_positive is false) then
    RETURN customer_value * lead_schufa_negative;
  elsif (brokerage_loan_application_id is null or state = 'APPLICATION_INCOMPLETE') then
    RETURN customer_value * short_lead;
  elsif (has_applied_brokerage_applications is true) then
    RETURN customer_value * qualified_lead;
  elsif (has_applied_brokerage_applications is false and has_brokerage_applications is true) then
    RETURN customer_value * lead_no_offer;
  else
    RETURN customer_value * lead_kdf;
  end if;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'migration.CASI-191_0001.sql',
      'CASI-191 - Lead Assignment - schufa negativ identification',
      '6.111'
      );

COMMIT;
