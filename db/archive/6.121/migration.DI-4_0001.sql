DROP VIEW brokerage.bank_document_view;

CREATE OR REPLACE VIEW brokerage.bank_document_view
AS
  SELECT
    pba.id,
    pba.creation_date,
    pba.ext_ref_number,
    pba.info additional_data,
    t1.*
  FROM
    public.brokerage_application pba,
    (SELECT
       bankdoc.account_id,
       acc.id brokerage_account_id,
       bankdoc.brokerage_bank_id,
       bankdoc.send_date,
       bankdoc.receive_date,
       bankdoc.bank_name,
       pdata.first_name,
       pdata.last_name,
       (SELECT pacc.account_id
        FROM public.account pacc
        WHERE pacc.id = map.kredit_privat_id) customer_number,
       map.kredit_privat_id                   public_account_id
     FROM
       (SELECT *
        FROM (
               SELECT
                 ROW_NUMBER()
                 OVER (PARTITION BY account_id, brokerage_bank_id) AS RN,
                 doc.account_id,
                 bdoc.brokerage_bank_id,
                 bdoc.send_date,
                 bdoc.receive_date,
                 bank.name                                            bank_name
               FROM
                 brokerage.brokerage_bank bank,
                 brokerage.brokerage_uploaded_doc bdoc,
                 (SELECT *
                  FROM brokerage.customer_uploaded_doc
                  WHERE status IN ('ACCEPTED')) doc
               WHERE
                 bank.id = bdoc.brokerage_bank_id
                 AND bdoc.document_id = doc.id
                 AND bdoc.status IN ('SENT', 'RECEIVED')
             ) t
        WHERE RN = 1) bankdoc,
       brokerage.account acc,
       brokerage.applicant app,
       brokerage.personal_data pdata,
       public.brokerage_entity_map map

     WHERE
       bankdoc.account_id = acc.id
       AND acc.account_owner_id = app.id
       AND app.personal_data_id = pdata.id
       AND bankdoc.account_id = map.brokerage_id) t1
  WHERE
    t1.public_account_id = pba.account_id
    AND t1.brokerage_bank_id = pba.brokerage_bank_id
    AND pba.STATE IN (
      'APPLICATION_APPLIED',
      'APPLICATION_ACCEPTED',
      'APPLICATION_DOCUMENTS_RECEIVED',
      'APPLICATION_DOCUMENTS_MISSING',
      'APPLICATION_PENDING');

ALTER TABLE brokerage.brokerage_bank_configuration
ADD COLUMN document_upload_via_sftp boolean NOT NULL DEFAULT FALSE,
ADD COLUMN document_upload_via_post boolean NOT NULL DEFAULT FALSE,
ADD COLUMN document_upload_via_bank_api boolean NOT NULL DEFAULT FALSE;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'asaadat',
      'migration.DI-4_0001.sql',
      'DI-4_0001 - Add credit plus sftp channel credential',
      '6.121'

);