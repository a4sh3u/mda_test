START TRANSACTION;

INSERT INTO brokerage.advisor_group(
      id, name, condition, expiration_date)
VALUES (nextval('"MAIN_SEQUENCE"'), 'OldShortLeads',
        '#event.loanApplication.state.name() == "APPLICATION_INCOMPLETE" AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 22) AND #event.loanApplication.account.personalAdvisor == null', NULL);

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.CASI-202_0001.sql',
      'CASI-202 - Add old short lead advisory group',
      '6.133'
);

COMMIT;
