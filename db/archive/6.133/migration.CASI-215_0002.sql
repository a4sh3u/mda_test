START TRANSACTION;
-- making table backup
SELECT * INTO config_backup."2017-03-31_advisor_group_CASI_215" FROM brokerage.advisor_group;

COMMIT;

START TRANSACTION;
-- migrate condition column
DO $$
DECLARE
      min_value_record RECORD;
      max_value_record RECORD;

BEGIN
      FOR min_value_record IN
      SELECT *
      FROM brokerage.advisor_group_filter_parameter
      WHERE name = 'minRequestedAmount'
      LOOP

            UPDATE brokerage.advisor_group ag
            SET condition = replace(condition, 'minRequestedAmount', min_value_record.value)
            WHERE ag.id = min_value_record.advisor_group_id;

      END LOOP;

      FOR max_value_record IN
      SELECT *
      FROM brokerage.advisor_group_filter_parameter
      WHERE name = 'maxRequestedAmount'
      LOOP

            UPDATE brokerage.advisor_group ag
            SET condition = replace(condition, 'maxRequestedAmount', max_value_record.value)
            WHERE ag.id = max_value_record.advisor_group_id;

      END LOOP;
END$$;

COMMIT;

START TRANSACTION;
-- making table backup
SELECT * INTO config_backup."2017-03-31_advisor_group_filter_parameter_CASI_215" FROM brokerage.advisor_group_filter_parameter;

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.CASI-215_0002.sql',
      'CASI-215 - Implement exclusive offline agent advisor group',
      '6.133'
);

COMMIT;