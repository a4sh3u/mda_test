START TRANSACTION;

UPDATE brokerage.advisor_group
SET condition = condition || ' AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)';

INSERT INTO brokerage.advisor_group(
      id, name, condition, expiration_date)
VALUES (nextval('"MAIN_SEQUENCE"'), 'AffiliateOfflineAgents', '#offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)', NULL);

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.CASI-215_0003.sql',
      'CASI-215 - Implement exclusive offline agent advisor group',
      '6.133'
);

COMMIT;