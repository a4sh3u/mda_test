START TRANSACTION;

DELETE FROM brokerage.advisor_group
WHERE name = 'OldShortLeads';

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'migration.CASI-202_0001.sql',
  'CASI-202 - Add old short lead advisory group',
  '6.133'
);

COMMIT;
