START TRANSACTION;

UPDATE brokerage.advisor_group ag SET condition = t.condition, custom_condition = t.custom_condition, type = t.type
  FROM config_backup."2017-03-31_advisor_group_CASI_215" t WHERE ag.id = t.id;

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'rollback.CASI-215_0002.sql',
  'CASI-215 - Implement exclusive offline agent advisor group',
  '6.133'
);

COMMIT;