START TRANSACTION;

DELETE FROM brokerage.advisor_group
WHERE name = 'AffiliateOfflineAgents';

UPDATE brokerage.advisor_group
SET condition = replace(condition, ' AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)', '');

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'migration.CASI-215_0003.sql',
  'CASI-215 - Implement exclusive offline agent advisor group',
  '6.133'
);

COMMIT;