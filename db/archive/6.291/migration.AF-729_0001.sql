START TRANSACTION;

ALTER TABLE public.loan_application
  ADD COLUMN initiator_smava_id BIGINT;

ALTER TABLE brokerage.loan_application
  ADD COLUMN initiator_smava_id BIGINT;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'mmaksymovych',
  'migration.AF-729_0001.sql',
  'AF-729 - As smava I want to have more details in the LA history',
  '6.291'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE public.loan_application
--   DROP COLUMN initiator_smava_id;
--
-- ALTER TABLE brokerage.loan_application
--   DROP COLUMN initiator_smava_id;
--
-- nextval('"MAIN_SEQUENCE"'),
-- (now()),
-- 'mmaksymovych',
-- 'migration.AF-729_0001.sql',
-- 'REVERT AF-729 - As smava I want to have more details in the LA history',
-- '6.291'
-- );
--
-- COMMIT;