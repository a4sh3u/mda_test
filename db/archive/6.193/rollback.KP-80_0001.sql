DROP VIEW dunning_process_view;


INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mbiletskyi',
      'rollback.KP-80_0001.sql',
      'KP-80 - Integrate Mahnprozessübersicht (=Dunning process view) for KreditPrivat into BO',
      '6.193'
);