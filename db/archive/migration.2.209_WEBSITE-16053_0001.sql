ALTER TABLE brokerage_bank_filter ADD creation_date TIMESTAMP;
ALTER TABLE brokerage_bank_filter ADD comments TEXT;

ALTER TABLE brokerage.brokerage_bank_filter ADD creation_date TIMESTAMP;
ALTER TABLE brokerage.brokerage_bank_filter ADD comments TEXT;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'wteperek',
  'migration.2.209_WEBSITE-16053_0001.sql',
  'WEBSITE-16053 Bank filter table: Add creation date and comment'
);
