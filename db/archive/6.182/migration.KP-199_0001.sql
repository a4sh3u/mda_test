-- Table: public.document_type

-- DROP TABLE public.document_type;

CREATE TABLE public.document_type
(
  id   BIGINT NOT NULL,
  type CHARACTER VARYING(255),
  CONSTRAINT document_type_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

ALTER TABLE public.document_type
  ADD CONSTRAINT unq_document_type UNIQUE (type);

-- Table: public.document_placeholder

-- DROP TABLE public.document_placeholder;

CREATE TABLE public.document_placeholder
(
  id    BIGINT NOT NULL,
  key   CHARACTER VARYING(255),
  value CHARACTER VARYING(500),
  CONSTRAINT document_placeholder_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

ALTER TABLE public.document_placeholder
  ADD CONSTRAINT unq_document_placeholder UNIQUE (key);

-- Table: public.document_type_placeholder

-- DROP TABLE public.document_type_placeholder;

CREATE TABLE public.document_type_placeholder
(
  id                      BIGINT NOT NULL,
  document_type_id        BIGINT,
  document_placeholder_id BIGINT,
  CONSTRAINT document_type_placeholder_pkey PRIMARY KEY (id)
)
WITH (
OIDS = FALSE
);

ALTER TABLE public.document_type_placeholder
  ADD CONSTRAINT unq_document_type_placeholder UNIQUE (document_type_id, document_placeholder_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'asaadat',
  'migration.KP-199_0001.sql',
  'KP-199_0001 - switch generating document in webapp',
  '6.182'
);