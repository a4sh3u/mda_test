START TRANSACTION;

ALTER TABLE public.advisor
  ADD COLUMN unlimited_picking_date TIMESTAMP WITH TIME ZONE;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'tboychuk',
  'migration.AF-350_0001.sql',
  'AF-350 - Add advisor unlimited picking date',
  '6.182'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE public.advisor DROP COLUMN unlimited_picking_date;
--
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'tboychuk',
--   'rollback.AF-350_0001.sql',
--   'AF-350 - Add advisor unlimited picking date',
--   '6.182'
-- );
--
-- COMMIT;