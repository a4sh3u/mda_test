START TRANSACTION;

CREATE TABLE brokerage."postal_service_job" (
    "id"               BIGINT    NOT NULL,
    "job_start_date"   TIMESTAMP NULL,
    "job_end_date"     TIMESTAMP NULL,
    "period_from"      TIMESTAMP NULL,
    "period_to"        TIMESTAMP NULL,
    "db_creation_date" TIMESTAMP NOT NULL DEFAULT NOW(),
    "db_modify_date"   TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY ("id")
);

CREATE TABLE brokerage."postal_service_master_application" (
    "id"                     BIGINT    NOT NULL,
    "date"                   TIMESTAMP NULL,
    "postal_service_job_id"  BIGINT    NULL,
    "loan_application_id"    BIGINT    NULL,
    "brokerage_applications" TEXT NULL,
    "db_creation_date"       TIMESTAMP NOT NULL DEFAULT NOW(),
    "db_modify_date"         TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY ("id")
);

select public.smava_add_trigger_modify_date('brokerage','{"postal_service_job","postal_service_master_application"}'::text[]);

INSERT INTO public.database_history VALUES (
    nextval('public."MAIN_SEQUENCE"'),
    (now()),
    'WTeperek',
    'migration.SME-8357_0001.sql',
    'SME-8357 Postal Service: Qualified Lead Trigger',
    '6.288'
);

COMMIT;

-- START TRANSACTION;
--
-- DROP TABLE IF EXISTS brokerage."postal_service_master_application";
-- DROP TABLE IF EXISTS brokerage."postal_service_job";
--
-- INSERT INTO public.database_history VALUES (
--     nextval('public."MAIN_SEQUENCE"'),
--     (now()),
--     'WTeperek',
--     'migration.SME-8357_0001.sql',
--     'SME-8357 REVERSE - Postal Service: Qualified Lead Trigger',
--     '6.288'
-- );
--
-- COMMIT;
