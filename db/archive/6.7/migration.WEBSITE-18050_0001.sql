CREATE TABLE brokerage.brokerage_bank_detail
(
  id bigint NOT NULL,
  brokerage_bank_id bigint,
  name character varying(255),
  value character varying(255),
  CONSTRAINT brokerage_bank_detail_pkey PRIMARY KEY (id),
  CONSTRAINT "brokerage_bank_detail_bank_FK" FOREIGN KEY (brokerage_bank_id)
      REFERENCES brokerage.brokerage_bank (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED
)
WITH (
  OIDS=FALSE
);

CREATE INDEX brokerage_bank_detail_bank
  ON brokerage.brokerage_bank_detail
  USING btree
  (brokerage_bank_id);

CREATE INDEX brokerage_bank_detail_bank_name
  ON brokerage.brokerage_bank_detail
  USING btree
  (brokerage_bank_id, name);

ALTER TABLE brokerage.brokerage_bank_detail ADD CONSTRAINT bank_detail_bank_unique UNIQUE (brokerage_bank_id, name);


INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Post Bank' from brokerage.brokerage_bank where name = 'postbank';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Targobank' from brokerage.brokerage_bank where name = 'targobank';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'ING-DiBa' from brokerage.brokerage_bank where name = 'ingdiba';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Kreditprivat' from brokerage.brokerage_bank where name = 'kreditprivat';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Credit Europe Bank' from brokerage.brokerage_bank where name = 'crediteurope';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Easy-credit' from brokerage.brokerage_bank where name = 'easycredit';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'CreditPlus Bank' from brokerage.brokerage_bank where name = 'creditplus';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Santander' from brokerage.brokerage_bank where name = 'santander';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Santander Bestcredit' from brokerage.brokerage_bank where name = 'santanderbestcredit';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Onlinekredit' from brokerage.brokerage_bank where name = 'onlinekredit';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'RECIPIENT_NAME', 'Deutsche Kreditbank AG' from brokerage.brokerage_bank where name = 'dkb';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'ADDRESS', 'DKB-Privatdarlehen' from brokerage.brokerage_bank where name = 'dkb';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'POST_CODE', '66085' from brokerage.brokerage_bank where name = 'dkb';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'CITY', 'Saarbrücken' from brokerage.brokerage_bank where name = 'dkb';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Deutsche Kredit Bank' from brokerage.brokerage_bank where name = 'dkb';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'RECIPIENT_NAME', 'SKG BANK, Niederlassung der Deutsche Kreditbank AG' from brokerage.brokerage_bank where name = 'skg';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'ADDRESS', 'Postfach 10 27 62' from brokerage.brokerage_bank where name = 'skg';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'POST_CODE', '66027' from brokerage.brokerage_bank where name = 'skg';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'CITY', 'Saarbrücken' from brokerage.brokerage_bank where name = 'skg';
INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'SKG Bank' from brokerage.brokerage_bank where name = 'skg';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'DSL Bank' from brokerage.brokerage_bank where name = 'dsl';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Barclaycard' from brokerage.brokerage_bank where name = 'barclays';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'SWK Bank' from brokerage.brokerage_bank where name = 'swk';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Deutsche Bank' from brokerage.brokerage_bank where name = 'deutschebank';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Norisbank' from brokerage.brokerage_bank where name = 'norisbank';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Allgemeine Beamten Bank' from brokerage.brokerage_bank where name = 'abk';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Wunschkredit' from brokerage.brokerage_bank where name = 'wunschkredit';

INSERT INTO brokerage.brokerage_bank_detail (id, brokerage_bank_id, name, value)
  SELECT nextval('"MAIN_SEQUENCE"'), id, 'LABEL', 'Bank of Scotland' from brokerage.brokerage_bank where name = 'bankofscotland';


START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'asaadat',
      'migration.WEBSITE-18050_0001.sql',
      'WEBSITE-18050_0001 - DU- Provide documents for bank partners (postal mail)',
      '6.7'
);

COMMIT;