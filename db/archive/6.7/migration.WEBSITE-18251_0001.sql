ALTER TABLE brokerage.brokerage_uploaded_doc
  ADD COLUMN concatenated_document_id bigint NULL ;

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aibrahim',
      'migration.WEBSITE-18184_0001.sql',
      'WEBSITE-18250_0001 - Add brokerage.brokerage_uploaded_doc.compilation_document_id for documents sent by post',
      '6.7'
);

COMMIT;