
ALTER TABLE brokerage.customer_uploaded_doc
  ADD COLUMN source character varying(20) NOT NULL DEFAULT 'CUSTOMER';

ALTER TABLE brokerage.customer_uploaded_doc
  ADD COLUMN advisor_id bigint;

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'asaadat',
      'migration.DI-27_0001.sql',
      'DI-27 Doc Upload: Implement upload functionality in CASI',
      '6.123'
      );

COMMIT;
