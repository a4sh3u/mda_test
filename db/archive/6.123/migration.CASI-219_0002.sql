DROP VIEW casi_event_queue;

CREATE OR REPLACE VIEW casi_event_queue AS
 SELECT eq.id,
    eq.creation_date AS event_queue_creation_date,
    eq.account_id,
    eq.loan_application_id,
    eq.type AS event_queue_type,
    eq.state AS event_queue_state,
    eq.due_date AS event_queue_due_date,
    eq.expiration_date AS event_queue_expiration_date,
    eq.bank_id,
        CASE
            WHEN (( SELECT count(*) AS count
               FROM brokerage.customer_uploaded_doc
              WHERE customer_uploaded_doc.account_id = (( SELECT brokerage_entity_map.brokerage_id
                       FROM brokerage_entity_map
                      WHERE brokerage_entity_map.kredit_privat_id = eq.account_id)) AND (customer_uploaded_doc.status::text = 'CREATED'::text OR customer_uploaded_doc.status::text = 'ACCEPTED'::text OR customer_uploaded_doc.status::text = 'REJECTED'::text OR customer_uploaded_doc.status::text = 'OUTDATED'::text))) > 0 THEN 1
            ELSE 0
        END AS has_document,
    cps.id AS customer_status_id,
    cps.creation_date AS customer_status_creation_date,
    cps.state AS customer_status_state,
    cps.reason AS customer_status_reason,
    cps.action AS customer_status_action,
    cps.created_by AS customer_status_created_by,
    ( SELECT loan_application.customer_value
           FROM loan_application
          WHERE loan_application.id = eq.loan_application_id) AS customer_value,

     CASE
       WHEN ((SELECT count(*) AS count from public.schufa_score inner join public.loan_application on loan_application.id = eq.loan_application_id
            and loan_application.account_id = public.schufa_score.account
            and loan_application.creation_date - interval '60 days' <= schufa_score.change_date and loan_application.creation_date + INTERVAL '5 days' > schufa_score.change_date
            and schufa_score.state = 'failed' inner join public.schufa_request on schufa_score.request_id = schufa_request.id inner join public.person
            on person.id = schufa_request.person_id and person.type = 1)) > 0 THEN FALSE
        ELSE TRUE
      END AS event_queue_is_schufa_positive

   FROM event_queue eq
     LEFT JOIN customer_process_status cps ON eq.account_id = cps.account_id AND cps.creation_date = (( SELECT max(customer_process_status.creation_date) AS max
           FROM customer_process_status
          WHERE customer_process_status.account_id = eq.account_id));

START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'migration.CASI-219_0002.sql',
      'CASI-219 - Removing Shcufa, Kdf negatives from queue, Improvements to picking performance',
      '6.123'
      );

COMMIT;