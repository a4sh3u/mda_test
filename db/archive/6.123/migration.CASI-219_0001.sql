DROP FUNCTION calculate_customer_value_relevancefactor(double precision, bigint, double precision, double precision, double precision, double precision, double precision);

CREATE OR REPLACE FUNCTION calculate_customer_value_relevancefactor(
    customer_value double precision,
    brokerage_loan_application_id bigint,
    qualified_lead double precision,
    lead_no_offer double precision,
    lead_kdf double precision,
    short_lead double precision,
    lead_schufa_negative double precision,
    is_schufa_positive boolean)
  RETURNS double precision AS
$BODY$
DECLARE
  state varchar(255);
  has_brokerage_applications boolean;
  has_applied_brokerage_applications boolean;
BEGIN
  SELECT la.state into state from brokerage.loan_application la where id = brokerage_loan_application_id;
  SELECT count(ba) > 0 into has_brokerage_applications from brokerage.brokerage_application ba where ba.loan_application_id = brokerage_loan_application_id;
  SELECT count(ba) > 0 into has_applied_brokerage_applications from brokerage.brokerage_application ba where ba.state = 'APPLICATION_APPLIED' and ba.loan_application_id = brokerage_loan_application_id;

  if (is_schufa_positive is false) then
    RETURN customer_value * lead_schufa_negative;
  elsif (brokerage_loan_application_id is null or state = 'APPLICATION_INCOMPLETE') then
    RETURN customer_value * short_lead;
  elsif (has_applied_brokerage_applications is true) then
    RETURN customer_value * qualified_lead;
  elsif (has_applied_brokerage_applications is false and has_brokerage_applications is true) then
    RETURN customer_value * lead_no_offer;
  else
    RETURN customer_value * lead_kdf;
  end if;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


START TRANSACTION ;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'aomkaram',
      'migration.CASI-219_0001.sql',
      'CASI-219 - Removing Shcufa, Kdf negatives from queue, Improvements to picking performance',
      '6.123'
      );

COMMIT;