﻿DROP FUNCTION calculate_customer_value_timefactor(double precision, timestamp with time zone, timestamp with time zone, text, double precision, double precision);

CREATE OR REPLACE FUNCTION calculate_customer_value_timefactor(
    customer_value double precision,
    lapp_date timestamp with time zone,
    curr_date timestamp with time zone,
    loan_application_state text,
    timegap0 double precision,
    timegap6 double precision,
    timevalue0 double precision,
    remaining_timevalue double precision)
  RETURNS double precision AS
$BODY$
DECLARE
  timegap double precision;
BEGIN
  timegap = (SELECT EXTRACT (EPOCH FROM (AGE(curr_date, lapp_date))) / 60);

  if (loan_application_state = 'APPLICATION_INCOMPLETE') then
    if (timegap < timegap0) then
      RETURN customer_value * timevalue0;
    end if;
  end if;

  if (timegap < timegap6) then
     RETURN customer_value * (exp(-8.775e-02 + -3.773e-04 * timegap) + 0.08745979);
  else
     RETURN customer_value * remaining_timevalue;
  end if;
END
$BODY$
LANGUAGE plpgsql;


START TRANSACTION;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'dmaidaniuk',
      'migration.AF-686_0001.sql',
      'AF-686 - Wrong timefactor calculation in DB function',
      '6.206'
);

COMMIT;



-- DROP FUNCTION calculate_customer_value_timefactor(double precision, timestamp with time zone, timestamp with time zone, text, double precision, double precision, double precision, double precision);
--
-- CREATE OR REPLACE FUNCTION calculate_customer_value_timefactor(
--     customer_value double precision,
--     lapp_date timestamp with time zone,
--     curr_date timestamp with time zone,
--     loan_application_state text,
--     timegap0 double precision,
--     timevalue0 double precision)
--   RETURNS double precision AS
-- $BODY$
-- DECLARE
--   timegap double precision;
-- BEGIN
--   timegap = (SELECT EXTRACT (EPOCH FROM (AGE(curr_date, lapp_date))) / 60);
--
--   if (loan_application_state = 'APPLICATION_INCOMPLETE') then
--     if (timegap < timegap0) then
--       RETURN customer_value * timevalue0;
--     end if;
--   end if;
--
--   RETURN customer_value * (exp(-8.775e-02 + -3.773e-04 * timegap) + 0.08745979);
-- END
-- $BODY$
--   LANGUAGE plpgsql;
--
--
-- START TRANSACTION;
--
-- INSERT INTO database_history VALUES (
--       nextval('"MAIN_SEQUENCE"'),
--       (now()),
--       'dmaidaniuk',
--       'rollback.AF-686_0001.sql',
--       'AF-686 - Wrong timefactor calculation in DB function',
--       '6.206'
-- );
--
-- COMMIT;