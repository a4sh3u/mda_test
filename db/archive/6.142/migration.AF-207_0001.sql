START TRANSACTION;

DO $$
DECLARE
  expired_group_record RECORD;

BEGIN
  FOR expired_group_record IN
  select a.id as account_id, ag.name as group_name, ag.expiration_date as group_expiration_date, aga.id as assignment_id
  from account a
    join brokerage.advisor_group_assignment aga on aga.account_id = a.id
    join brokerage.advisor_group ag on ag.id = aga.advisor_group_id
  where aga.expiration_date is null and ag.expiration_date is not null
  LOOP

    UPDATE brokerage.advisor_group_assignment
    SET expiration_date = expired_group_record.group_expiration_date
    WHERE id = expired_group_record.assignment_id;

  END LOOP;
END$$;

COMMIT;

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'migration.AF-207_0001.sql',
  'AF-207 - Advisor group assignment log bug',
  '6.142'
);

COMMIT;