-- Nothing to revert. It could be restored via UI

START TRANSACTION;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmaidaniuk',
  'rollback.AF-207_0001.sql',
  'AF-207 - Advisor group assignment log bug',
  '6.142'
);

COMMIT;