CREATE TABLE public.customer_value_factors (
    id bigint PRIMARY KEY NOT NULL,
    loan_application_id bigint NOT NULL UNIQUE,
    loan_application_factors VARCHAR,
    event_queue_factors VARCHAR,
    last_update timestamp with time zone
);

CREATE INDEX "customer_value_factors_loan_application_id"
  ON public.customer_value_factors
  USING btree
  (loan_application_id);

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'pvitic',
      'migration.CASI-152_0001.sql',
      'CASI-152_0001 - add table for customer value factors',
      '6.90'
);