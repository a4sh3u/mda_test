START TRANSACTION;

ALTER TABLE brokerage.advisor_notification
    ADD COLUMN borrower_smava_id bigint;

CREATE INDEX "advisor_notification_borrower_smava_id_idx"
  ON brokerage.advisor_notification
  USING btree
  (borrower_smava_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dmygaienko',
  'migration.AF-1126_0001.sql',
  'Refactor AdvisorNotification to use borrower smavaId: add new attribute',
  '6.321'
);

COMMIT;


-- -- REVERT TRANSACTION
--
-- START TRANSACTION;
--
-- ALTER TABLE brokerage.advisor_notification
--     DROP COLUMN borrower_smava_id;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   (now()),
--   'dmygaienko',
--   'migration.AF-1126_0001.sql',
--   'REVERT: Refactor AdvisorNotification to use borrower smavaId: add new attribute',
--   '6.321'
-- );
--
-- COMMIT;