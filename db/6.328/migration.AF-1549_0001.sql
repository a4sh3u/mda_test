START TRANSACTION;

ALTER TABLE public.advisor_assignment_log ALTER COLUMN customer_number SET NOT NULL;

ALTER TABLE casiadvisor.customer_assignment_history ALTER COLUMN advisor_id DROP NOT NULL;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  now(),
  'ivaniv',
  'migration.AF-1549_0001.sql',
  'AF-1549 - fix constraints on public.advisor_assignment_log and casiadvisor.customer_assignment_history',
  '6.328'
);

COMMIT;

-- START TRANSACTION;
--
-- ALTER TABLE public.advisor_assignment_log ALTER COLUMN customer_number DROP NOT NULL;
--
-- ALTER TABLE casiadvisor.customer_assignment_history ALTER COLUMN advisor_id SET NOT NULL;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   now(),
--   'ivaniv',
--   'revert migration.AF-1549_0001.sql',
--   'AF-1549 - fix constraints on public.advisor_assignment_log and casiadvisor.customer_assignment_history',
--   '6.328'
-- );
--
-- COMMIT;