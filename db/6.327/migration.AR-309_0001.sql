
ALTER TABLE brokerage.applicant ADD COLUMN person_id  character varying(255);

CREATE INDEX applicant_personid
ON brokerage.applicant (person_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'dkeller',
  'migration.AR-309_0001.sql',
  'add person_id to applicant',
  '6.327'
);

--  ALTER TABLE brokerage.applicant DROP COLUMN person_id ;

-- INSERT INTO database_history VALUES (
--  nextval('"MAIN_SEQUENCE"'),
--  (now()),
--  'dkeller',
--  'migration.AR-309_0001.sql',
--  'add person_id to applicant',
--  '6.327'
-- );