START TRANSACTION;

ALTER TABLE brokerage.advisor_group ADD COLUMN monthly_goal integer default 400;

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  now(),
  'imartyniuk',
  'migration.AF-1178_0001.sql',
  'AF-1178 - Extend group configuration with monthly goal',
  '6.327'
);

COMMIT;


-- START TRANSACTION;
--
-- ALTER TABLE brokerage.advisor_group DROP COLUMN monthly_goal;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   now(),
--   'imartyniuk',
--   'rollback.AF-1178_0001.sql',
--   'AF-1178 - Extend group configuration with monthly goal',
--   '6.327'
-- );
--
-- COMMIT;
