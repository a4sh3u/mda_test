ALTER TABLE lead_cycle ALTER COLUMN affiliate_information_id DROP NOT NULL;

INSERT INTO database_history VALUES (
      nextval('"MAIN_SEQUENCE"'),
      (now()),
      'mcichowski',
      'migration.WEBSITE-18477_0001.sql',
      'WEBSITE-18477_0001 - Drop not null constraint for affiliate_information_id',
      ''

);