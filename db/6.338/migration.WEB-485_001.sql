START TRANSACTION;

CREATE TABLE brokerage.master_application
(
  id bigint NOT NULL,
  account_id bigint NOT NULL,
  first_loan_application_id bigint NOT NULL,
  state character varying NOT NULL,
  last_touchpoint timestamp with time zone NOT NULL,
  creation_date timestamp with time zone NOT NULL,
  state_changed_date timestamp with time zone NOT NULL,
  affiliate_information_id bigint,
  latest_loan_application_id bigint,
  db_creation_date TIMESTAMP NOT NULL DEFAULT NOW(),
  db_modify_date   TIMESTAMP NOT NULL DEFAULT NOW(),
  CONSTRAINT pk_master_application PRIMARY KEY (id),
  CONSTRAINT account_fk FOREIGN KEY (account_id)
  REFERENCES brokerage.account (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT affiliate_information_fk FOREIGN KEY (affiliate_information_id)
  REFERENCES public.affiliate_information (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT latest_loan_application_fk FOREIGN KEY (latest_loan_application_id)
  REFERENCES brokerage.loan_application (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT loan_application_fk FOREIGN KEY (first_loan_application_id)
  REFERENCES brokerage.loan_application (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

select public.smava_add_trigger_modify_date('brokerage','{"master_application"}'::text[]);

CREATE INDEX master_application_account_id_last_touchpoint_idx
  ON brokerage.master_application
  USING btree
  (account_id, last_touchpoint);

CREATE INDEX master_application_first_loan_application_id_idx
  ON brokerage.master_application
  USING btree
  (first_loan_application_id);

CREATE INDEX master_application_last_loan_application_id_idx
  ON brokerage.master_application
  USING btree
  (latest_loan_application_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'htahan',
  'migration.WEB-485_001.sql',
  'new table brokerage.master_application',
  '6.338'
);

COMMIT;