START TRANSACTION;

CREATE INDEX lead_cycle_first_loan_application_id_idx
  ON public.lead_cycle
  USING btree
  (first_loan_application_id);

CREATE INDEX lead_cycle_last_loan_application_id_idx
  ON public.lead_cycle
  USING btree
  (latest_loan_application_id);

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  (now()),
  'htahan',
  'migration.WEB-485_002.sql',
  'more indexs for table public.lead_cycle',
  '6.338'
);

COMMIT;