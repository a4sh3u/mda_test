START TRANSACTION;

CREATE SCHEMA IF NOT EXISTS casiadvisor;

CREATE SEQUENCE casiadvisor.id_seq START 1 INCREMENT 1;

CREATE TABLE casiadvisor.advisor
(
  id bigint NOT NULL,
  smava_ref_id bigint NOT NULL CONSTRAINT smava_ref_id_uq UNIQUE,
  email character varying(255) NOT NULL CONSTRAINT advisor_email_uq UNIQUE,
  username character varying(255) NOT NULL,
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  location character varying(255),
  team_id bigint,
  blocked_picking_date timestamp with time zone,
  max_customer_per_day integer,
  unlimited_picking_date timestamp with time zone,
  sip character varying(255),
  phone character varying(255),
  state character varying(50) NOT NULL,
  version integer DEFAULT 0,
  CONSTRAINT advisor_PK PRIMARY KEY (id)
);

CREATE TABLE casiadvisor.team
(
  id bigint NOT NULL,
  name character varying(255) NOT NULL,
  expiration_date timestamp with time zone,
  team_lead_id bigint,
  version integer DEFAULT 0,
  CONSTRAINT team_PK PRIMARY KEY (id),
  CONSTRAINT team_team_lead_FK FOREIGN KEY (team_lead_id)
  REFERENCES casiadvisor.advisor (id)
);

ALTER TABLE casiadvisor.advisor ADD CONSTRAINT advisor_team_FK FOREIGN KEY (team_id)
REFERENCES casiadvisor.team (id);

CREATE TABLE casiadvisor.customer_assignment
(
  id bigint NOT NULL,
  advisor_id bigint NOT NULL,
  borrower_smava_id bigint NOT NULL,
  version integer DEFAULT 0,
  CONSTRAINT customer_assignment_PK PRIMARY KEY (id),
  CONSTRAINT customer_assignment_advisor_FK FOREIGN KEY (advisor_id)
  REFERENCES casiadvisor.advisor (id),
  CONSTRAINT customer_advisor_UQ UNIQUE (borrower_smava_id, advisor_id),
  CONSTRAINT borrower_smava_id_UQ UNIQUE (borrower_smava_id)
);

CREATE TABLE casiadvisor.customer_assignment_history
(
  id bigint NOT NULL,
  advisor_id bigint NOT NULL,
  borrower_smava_id bigint NOT NULL,
  loan_application_ref_id bigint,
  assignment_type character varying(255),
  executed_by_id bigint,
  CONSTRAINT advisor_assignment_log_PK PRIMARY KEY (id),
  CONSTRAINT customer_assignment_history_advisor_FK FOREIGN KEY (advisor_id)
  REFERENCES casiadvisor.advisor (id),
  CONSTRAINT customer_assignment_history_executed_by_FK FOREIGN KEY (executed_by_id)
  REFERENCES casiadvisor.advisor (id)
);

CREATE TABLE casiadvisor.advisor_group
(
  id bigint NOT NULL,
  name character varying(255) NOT NULL,
  description character varying(255),
  priority integer,
  properties text,
  state character varying(50) NOT NULL,
  version integer DEFAULT 0,
  CONSTRAINT group_PK PRIMARY KEY (id)
);

CREATE TABLE casiadvisor.group_membership
(
  id bigint NOT NULL,
  advisor_id bigint NOT NULL,
  group_id bigint NOT NULL,
  state character varying(50) NOT NULL,
  version integer DEFAULT 0,
  CONSTRAINT group_membership_PK PRIMARY KEY (id),
  CONSTRAINT group_membership_advisor_FK FOREIGN KEY (advisor_id)
  REFERENCES casiadvisor.advisor (id),
  CONSTRAINT group_membership_group_FK FOREIGN KEY (group_id)
  REFERENCES casiadvisor.advisor_group (id)
);

CREATE TABLE casiadvisor.role
(
  id bigint NOT NULL,
  advisor_id bigint NOT NULL,
  name character varying(255) NOT NULL,
  state character varying(255) NOT NULL,
  version integer DEFAULT 0,
  CONSTRAINT role_PK PRIMARY KEY (id),
  CONSTRAINT role_advisor_FK FOREIGN KEY (advisor_id)
  REFERENCES casiadvisor.advisor (id),
  CONSTRAINT role_name_advisor_UQ UNIQUE (name, advisor_id)
);

CREATE TABLE casiadvisor.call_journal
(
  id bigint NOT NULL,
  advisor_id bigint NOT NULL,
  borrower_smava_id bigint NOT NULL,
  destination_number character varying(255),
  loan_application_ref_id bigint,
  call_successful boolean,
  failure_reason character varying(255),
  CONSTRAINT call_journal_PK PRIMARY KEY (id),
  CONSTRAINT call_journal_advisor_FK FOREIGN KEY (advisor_id)
  REFERENCES casiadvisor.advisor (id)
);

--------------------------------------------------------------------
--------------------------------------------------------------------

ALTER TABLE casiadvisor.advisor
  ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();

ALTER TABLE casiadvisor.advisor
  ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

ALTER TABLE casiadvisor.team
  ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();

ALTER TABLE casiadvisor.team
  ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

ALTER TABLE casiadvisor.customer_assignment
  ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();

ALTER TABLE casiadvisor.customer_assignment
  ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

ALTER TABLE casiadvisor.advisor_group
  ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();

ALTER TABLE casiadvisor.advisor_group
  ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

ALTER TABLE casiadvisor.group_membership
  ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();

ALTER TABLE casiadvisor.group_membership
  ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

ALTER TABLE casiadvisor.role
  ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();

ALTER TABLE casiadvisor.role
  ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

CREATE TABLE casiadvisor.audit_log
(
  id bigint NOT NULL
    CONSTRAINT auditlog_pk PRIMARY KEY,
  action character varying(255) NOT NULL,
  entity character varying(255) NOT NULL,
  entity_id bigint NOT NULL,
  entity_version integer,
  modified_by character varying(255) NOT NULL,
  object_snapshot json,
  change_time timestamp NOT NULL,
  db_modify_date TIMESTAMP NOT NULL,
  db_creation_date TIMESTAMP NOT NULL DEFAULT now()
);

ALTER TABLE casiadvisor.call_journal ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();
ALTER TABLE casiadvisor.call_journal ADD COLUMN db_modify_date TIMESTAMP NOT NULL;
ALTER TABLE casiadvisor.customer_assignment_history ADD COLUMN db_creation_date TIMESTAMP NOT NULL DEFAULT now();
ALTER TABLE casiadvisor.customer_assignment_history ADD COLUMN db_modify_date TIMESTAMP NOT NULL;

CREATE OR REPLACE FUNCTION casiadvisor.audit_modify_entity() RETURNS TRIGGER AS $body$
BEGIN
  new.db_modify_date := now();
  RETURN new;
END;
$body$ LANGUAGE plpgsql;

-- add triggers on db_modify_date column
CREATE TRIGGER audit_modify_ADVISOR BEFORE INSERT OR UPDATE ON casiadvisor.advisor
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_ADVISOR_GROUP BEFORE INSERT OR UPDATE ON casiadvisor.advisor_group
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_AUDIT_LOG BEFORE INSERT OR UPDATE ON casiadvisor.audit_log
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_CALL_JOURNAL BEFORE INSERT OR UPDATE ON casiadvisor.call_journal
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_CUSTOMER_ASSIGNMENT BEFORE INSERT OR UPDATE ON casiadvisor.customer_assignment
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_CUSTOMER_ASSIGNMENT_HISTORY BEFORE INSERT OR UPDATE ON casiadvisor.customer_assignment_history
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_GROUP_MEMBERSHIP BEFORE INSERT OR UPDATE ON casiadvisor.group_membership
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_ROLE BEFORE INSERT OR UPDATE ON casiadvisor.role
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

CREATE TRIGGER audit_modify_TEAM BEFORE INSERT OR UPDATE ON casiadvisor.team
FOR EACH ROW EXECUTE PROCEDURE casiadvisor.audit_modify_entity();

INSERT INTO database_history VALUES (
  nextval('"MAIN_SEQUENCE"'),
  now(),
  'dmaidaniuk',
  'migration.AF-1015_0001.sql',
  'AF-1015 - Added new schema casiadvisor',
  '6.326'
);

COMMIT;


-- START TRANSACTION;
--
-- DROP SCHEMA casiadvisor;
--
-- INSERT INTO database_history VALUES (
--   nextval('"MAIN_SEQUENCE"'),
--   now(),
--   'dmaidaniuk',
--   'rollback.AF-1015_0001.sql',
--   'AF-1015 - Added new schema casiadvisor',
--   '6.326'
-- );
--
-- COMMIT;