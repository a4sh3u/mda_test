package de.smava.test;


import de.smava.webapp.account.domain.Credentials;
import org.springframework.security.authentication.dao.SaltSource;


public class FakeCredentials extends Credentials {

	@Override
	public String getTransactionPin() {
		return "1234";
	}

	public String toString(){
		return "FakeCredentials";
	}
}
