package de.smava.test;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.i18n.Employment;
import de.smava.webapp.commons.security.Role;
import de.smava.webapp.commons.util.PersonConstants;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static de.smava.webapp.account.domain.AccountRoleState.STATE_ACCEPTED;

/**
 * @author dkeller
 *
 */
public final class TestDataCreator {

	private TestDataCreator() {
	}

	private static BankAccountProvider fidor = null;
	
	public static Collection<BankAccount> createAllDepositAccounts( ){
		BankAccount depositAccount = new BankAccount();
		depositAccount.setAccountNo( "88888888");
		depositAccount.setBankCode("11111111");
		depositAccount.setBank( "deposit bankName");
		depositAccount.setType( BankAccount.TYPE_BORROWER_DEPOSIT_ACCOUNT);
		
		BankAccount lenderDepositAccount = new BankAccount();
		lenderDepositAccount.setAccountNo( "99999999");
		lenderDepositAccount.setBankCode("22222222");
		lenderDepositAccount.setBank("lender deposit bankName");
		lenderDepositAccount.setType( BankAccount.TYPE_LENDER_DEPOSIT_ACCOUNT);
		
		Collection<BankAccount> depositAccounts = new ArrayList<BankAccount>();
		depositAccounts.add(depositAccount);
		depositAccounts.add(lenderDepositAccount);
		
		return depositAccounts;
	}
	
	public static BankAccount createInternalBankAccount( Account a, BankAccountProvider provider) {
		
		BankAccount internalBa = new BankAccount();
		internalBa.setId(6544544l);
		internalBa.setAccountNo( "99999999");
		internalBa.setBankCode("22222222");
		internalBa.setBank("lender deposit bankName");
		internalBa.setCreationDate(new Date());
		if ( a.isBorrower()) {
			internalBa.setType( BankAccount.TYPE_BORROWER_ACCOUNT);
		} else {
			internalBa.setType( BankAccount.TYPE_LENDER_ACCOUNT);
		}
		internalBa.setProvider( provider);
		internalBa.setAccount(a);
		internalBa.setValid(true);
		internalBa.setCustomerNumber(""+a.getAccountId());
		List<BankAccount> bas = a.getBankAccounts();
		if (  bas == null) {
			bas = new ArrayList<BankAccount>();
		}
		bas.add(internalBa);
		a.setBankAccounts(bas);
		return internalBa;
	}
	
	public static BankAccount createRefBankAccount( Account a) {
		
		BankAccount refBa = new BankAccount();
		
		refBa.setAccountNo( "9777777");
		refBa.setBankCode("11111111");
		refBa.setBank("ref bankName");
		refBa.setIban("DE93500500000090085135");
		refBa.setBic("HELADEFFXXX");
		refBa.setSepaMandateReference(BankAccount.SEPA_MADATE_REFERENCE_PREFIX + a.getAccountId());
		refBa.setAccount(a);
		
		List<BankAccount> bas = a.getBankAccounts();
		if (  bas == null) {
			bas = new ArrayList<BankAccount>();
		}
		bas.add(refBa);
		a.setBankAccounts(bas);
		return refBa;
	}

	public static BankAccount createSmavaBankAccount( BankAccountProvider provider) {
		
		BankAccount smavaBankAccount = new BankAccount();
		
		smavaBankAccount.setAccountNo( "7855554");
		smavaBankAccount.setBankCode("444400");
		smavaBankAccount.setBank("smava bank account");
		smavaBankAccount.setExpirationDate( null);
		smavaBankAccount.setProvider(provider);
		smavaBankAccount.setType( BankAccount.TYPE_SMAVA_ACCOUNT);
		
		return smavaBankAccount;
	}

	public static BankAccount createInterimInAccount( BankAccountProvider provider) {
		
		BankAccount interimAccount = new BankAccount();
		interimAccount.setId(11223236l);
		interimAccount.setAccountNo( "73222222");
		interimAccount.setBankCode("444400");
		interimAccount.setBank("smava bank account interim in");
		interimAccount.setExpirationDate( null);
		interimAccount.setProvider(provider);
		interimAccount.setType( BankAccount.TYPE_SMAVA_INTERIM_IN_ACCOUNT);
		
		return interimAccount;
	}

	public static BankAccount createInterimOutAccount( BankAccountProvider provider) {

		BankAccount interimAccount = new BankAccount();
		
		interimAccount.setAccountNo( "1231111");
		interimAccount.setBankCode("222222");
		interimAccount.setBank("smava bank account interim out");
		interimAccount.setExpirationDate( null);
		interimAccount.setProvider(provider);
		interimAccount.setType( BankAccount.TYPE_SMAVA_INTERIM_OUT_ACCOUNT);
		
		return interimAccount;
	}

	public static CreditScore createCreditScore(){
		CreditScore cs = new CreditScore();
		cs.setRating("A");
		cs.setScore(9900l);
		
		SchufaScore sc = new SchufaScore();
		cs.setSchufaScore(sc);
		return cs;
	}

	public static EconomicalData createEconomicalData(Account a){
	
		EconomicalData economicalData = new CoBorrowerEconomicalData();
		economicalData.setAccount(a);
		Calendar startOfEmployment = CurrentDate.getCalendar();
		startOfEmployment.add(Calendar.YEAR, -1);
		startOfEmployment.set(Calendar.DAY_OF_MONTH, 1);

		Calendar endOfTempEmployment = CurrentDate.getCalendar();
		endOfTempEmployment.add(Calendar.YEAR, 1);
		endOfTempEmployment.set(Calendar.DAY_OF_MONTH, 1);

		Sector sector = new Sector();
		sector.setId(4869L);

		economicalData.setSector(sector);
		economicalData.setEmployment(Employment.EMPLOYEE);
		economicalData.setEmployerName("The Company");
		economicalData.setTempEmployment(true);
		economicalData.setStartOfOccupation(startOfEmployment.getTime());
		economicalData.setEndOfTempEmployment(endOfTempEmployment.getTime());
		economicalData.setCreditCardType(CreditCardType.DEBIT_AND_CREDIT_CARD);
		economicalData.setIncome(4000d);
		economicalData.setMiscEarnType(MiscEarnType.SIDELINE);
		economicalData.setMiscEarnAmount1(100d);
		economicalData.setReceivedRent(10d);
		economicalData.setReceivedPalimony(12d);
		economicalData.setSavings(15d);
		economicalData.setPropertyOwnedSquareMetres(200);
		economicalData.setModeOfAccommodation(EconomicalData.ACCOMODATION_TYPE_OWN);
		economicalData.setPaidRent(250d);
		economicalData.setPrivateHealthInsurance(20d);
		economicalData.setPaidAlimony(15d);
		economicalData.setModeOfAccommodation(EconomicalData.ACCOMODATION_TYPE_OWN);
		economicalData.setPropertyOwnedType(PropertyOwnedType.SELF_USED_AND_LETTED_PROPERTY);
		economicalData.setNumOfCars("1_CAR");
		economicalData.setNumberOfOtherInHousehold(1);
		economicalData.setNumberOfChildren(2);
		economicalData.setEconomicalDataType(EconomicalDataType.CUSTOMER);
		a.setEconomicalData(economicalData);
		return economicalData;
		
	}

	public static EconomicalData createEconomicalDataForFreelancer(Account a){
		EconomicalData economicalData = new EconomicalData();
		economicalData.setAccount(a);
		Calendar startOfEmployment = CurrentDate.getCalendar();
		startOfEmployment.add(Calendar.YEAR, -3);
		startOfEmployment.set(Calendar.DAY_OF_MONTH, 1);

		Sector sector = new Sector();
		sector.setId(4869L);

		economicalData.setSector(sector);
		economicalData.setEmployment(Employment.FREELANCER);
		
		economicalData.setTempEmployment(false);
		economicalData.setStartOfOccupation(startOfEmployment.getTime());
		economicalData.setCreditCardType(CreditCardType.DEBIT_AND_CREDIT_CARD);
		economicalData.setMiscEarnType(MiscEarnType.CHILD_BENEFIT);
		economicalData.setMiscEarnAmount1(100d);
		economicalData.setReceivedRent(10d);
		economicalData.setReceivedPalimony(12d);
		economicalData.setSavings(15d);
		economicalData.setPropertyOwnedSquareMetres(200);
		economicalData.setPaidRent(250d);
		economicalData.setPrivateHealthInsurance(20d);
		economicalData.setPaidAlimony(15d);
		economicalData.setModeOfAccommodation(EconomicalData.ACCOMODATION_TYPE_OWN);
		economicalData.setPropertyOwnedType(PropertyOwnedType.SELF_USED_AND_LETTED_PROPERTY);
		economicalData.setNumOfCars("1_CAR");
		economicalData.setNumberOfOtherInHousehold(1);
		economicalData.setNumberOfChildren(2);
		economicalData.setEconomicalDataType(EconomicalDataType.CUSTOMER);
		
		Calendar cal = CurrentDate.getCalendar();
	    int year = cal.get(Calendar.YEAR);
	    if (cal.get(Calendar.MONTH) < Calendar.APRIL){
	    	year--;
	    }
		FreelancerIncome fi1 = new FreelancerIncome();
		fi1.setYear(year - 1);
		fi1.setDuration(12);
		fi1.setExpenses(12500.0);
		fi1.setVolume(50000.0);
		fi1.setAnnualEarningsBeforeTaxes(37500.0);
		fi1.setEconomicalData(economicalData);
		economicalData.addFreelancerIncome(fi1);
		
		FreelancerIncome fi2 = new FreelancerIncome();
		fi2.setYear(year - 2);
		fi2.setDuration(12);
		fi2.setExpenses(15500.0);
		fi2.setVolume(70000.0);
		fi2.setAnnualEarningsBeforeTaxes(54500.0);
		fi2.setEconomicalData(economicalData);
		economicalData.addFreelancerIncome(fi2);
		
		a.setEconomicalData(economicalData);
		return economicalData;
		
	}

    public static AdvisorAccount createAdvisorAccount() {
        AdvisorAccount account = new AdvisorAccount();
        account.setAccountRoles(new HashSet<AdvisorRole>());
        account.setPerson(createAdvisorPerson(account));

        account.setCreationDate(new Date());
        account.setUsername("smava");
        account.setEmail("advisor@smava.de");
        account.setPhoneCode("0999");
        account.setPhone("8888");

        AdvisorRole role = new AdvisorRole();
        role.setName(Role.ROLE_CREDIT_ADVISOR.name());
        role.getStates().add(new AdvisorRoleState(STATE_ACCEPTED, CurrentDate.getDate()));
        account.getAccountRoles().add( role);

        account.setState( Account.USER_STATE_ACTIVATED);
        account.setId(1566L);
        account.setSmavaId(87L);

        return account;
    }

    public static Account convertToGeneralAccount(AdvisorAccount advisorAccount) {
        Account account = new Account();

        account.setCreationDate(advisorAccount.getCreationDate());
        account.setUsername(advisorAccount.getUsername());
        account.setEmail(advisorAccount.getEmail());
        account.setPhoneCode(advisorAccount.getPhoneCode());
        account.setPhone(advisorAccount.getPhone());

        AdvisorRole advisorRole = advisorAccount.getAccountRoles().iterator().next();
        AccountRole role = new AccountRole();
        role.setName(advisorRole.getName());
        role.setCurrentAccountRoleState(new AccountRoleState(STATE_ACCEPTED, CurrentDate.getDate()));
        account.getAccountRoles().add(role);

        account.setState(Account.USER_STATE_ACTIVATED);
        account.setId(advisorAccount.getId());
        account.setCustomerNumber(advisorAccount.getSmavaId());
        account.setPerson(createPerson(account));
        return account;
    }

    private static AdvisorPerson createAdvisorPerson(AdvisorAccount account) {
        AdvisorPerson p = new AdvisorPerson();
        p.setId(1L);
        p.setAccount(account);
        p.setCreationDate(CurrentDate.getDate());
        p.setFirstName("Hans");
        p.setLastName("Meier");
        Calendar birthday = Calendar.getInstance();
        birthday.set(1980, Calendar.DECEMBER, 3, 0, 0);
        return p;
    }

	public static Account createBorrower( ) {
		
		Account a = new Account();
		a.setPerson(createPerson(a));
		
		CustomerAgreement ca = new CustomerAgreement();
		ca.setAgreementDate(new Date());
		ca.setType(CustomerAgreementType.FIDOR_AGREEMENT);
		a.getCustomerAgreements().add(ca);
		a.setCreationDate(new Date());
		a.setUsername("smava");
		Calendar cal = Calendar.getInstance();
		cal.set(1975, 3, 4, 14, 12);
		a.setAcceptanceDate( cal.getTime());

		a.setEmail("testi@tester.de");
		a.setPhoneCode("0999");
		a.setPhone("8888");
		a.setPhone2Code("0111");
		a.setPhone2("2222");

		AccountRole role = new AccountRole();
		role.setName(Role.ROLE_BORROWER.name());
		role.setCurrentAccountRoleState(new AccountRoleState(STATE_ACCEPTED, CurrentDate.getDate()));
		a.getAccountRoles().add( role);
		a.setOrders( new HashSet<Order>());
		
		a.setCreditScores( new ArrayList<CreditScore>());
		a.setCurrentCreditScore(createCreditScore());
		
		a.setState( Account.USER_STATE_ACTIVATED);
		a.setId(1566l);
		a.setAccountId(87l);

		List<BankAccount> bankAccounts = new ArrayList<BankAccount>();
		bankAccounts.add(createBankAccount(a));
		a.setBankAccounts(bankAccounts);

		return a;
	}

	private static BankAccount createBankAccount(Account a) {
		BankAccount bankAccount = new BankAccount();

		bankAccount.setAccount(a);
		bankAccount.setType(BankAccount.TYPE_REFRENCE_ACCOUNT);
		bankAccount.setCreationDate(CurrentDate.getDate());
		bankAccount.setAccountNo("12345");
		bankAccount.setBankCode("10061006");
		bankAccount.setBank("My little Bank");
		bankAccount.setIban("DE93500500000090085135");
		bankAccount.setBic("HELADEFFXXX");
		bankAccount.setSepaMandateReference(BankAccount.SEPA_MADATE_REFERENCE_PREFIX + a.getAccountId());

		return bankAccount;
	}

	public static Person createPerson(Account a) {
		Person p = new Person();
        p.setId(1l);
		p.setAccount(a);
		p.setCreationDate(CurrentDate.getDate());
		p.setSalutation(PersonConstants.MALE_SALUTATION);
		p.setFirstName("Hans");
		p.setLastName("Meier");
		p.setBirthName("Hansen");
		p.setPlaceOfBirth("Geburtsstadt");
		p.setCitizenship("DE");
		p.setFamilyStatus(Person.MARRIED);
		Calendar birthday = Calendar.getInstance();
		birthday.set(1980, 11, 3, 0, 0);
		p.setDateOfBirth(birthday.getTime());
		return p;
	}

	public static Document createPostident(){
		Document pi = new Document();
        pi.setCreationDate(CurrentDate.getDate());
        pi.setApprovalState(Document.APPROVAL_STATE_OK);
		pi.setDirection(Document.DIRECTION_IN);
		pi.setState(Document.STATE_SUCCESSFUL);
		pi.setType(Document.DOCUMENT_TYPE_POSTIDENT);
		return pi;
	}

	public static Account createLender( ) {
		
		Account a = new Account();
		Person p = new Person();
		p.setAccount(a);
		p.setFirstName("Peter");
		p.setLastName("Geber");
		p.setCreationDate(CurrentDate.getDate());
		a.setPerson(p);
		Calendar cal = Calendar.getInstance();
		cal.set(1973, 1, 2, 17, 00);
		a.setAcceptanceDate( cal.getTime());
		
		a.setCreationDate(CurrentDate.getDate());
		a.setEmail("lender@smava.de");
		a.setUsername("lender");
		
		AccountRole role = new AccountRole();
		role.setName(Role.ROLE_LENDER.name());
		role.setCurrentAccountRoleState(new AccountRoleState(STATE_ACCEPTED, CurrentDate.getDate()));
		a.getAccountRoles().add( role);
		
		a.setAccountId(45678l);
		a.addDocument(createPostident());
		
		a.setOffers( new LinkedHashSet<Offer>() );
		a.setCredentials(new FakeCredentials());
		
		a.setState( Account.USER_STATE_ACTIVATED);
		
		CustomerAgreement ca = new CustomerAgreement();
		ca.setAgreementDate(new Date());
		ca.setType(CustomerAgreementType.TERMS_AND_CONDITIONS);
		a.getCustomerAgreements().add(ca);
		a.setCreationDate(new Date());
		
		return a;
	}

	public static BankAccountProvider createFidorProvider( ) {
		if ( fidor != null){
			return fidor;
		}
		
		BankAccountProvider provider = new BankAccountProvider();
		provider.setType( BankAccountProviderType.E_MONEY);
		provider.setName(BankAccountProvider.FIDOR);

		fidor = provider;
		return provider;
	}
	
	public static Collection<BookingAssignment> createBookingAssignment( Booking b, BookingGroup bg) {
		BookingAssignment ba = BookingAssignment.connectSimple(bg, b, true);
		Collection<BookingAssignment> bas = new ArrayList<BookingAssignment>();
		bas.add(ba);
		return bas;
		
	}
	
	public static Transaction createTransaction(Account a){
		BankAccountProvider provider = createFidorProvider();
		BankAccount credit = TestDataCreator.createInterimInAccount( provider);
		BankAccount debit = TestDataCreator.createInternalBankAccount(a, provider);

		return createTransaction(credit, debit);
	}
	
	public static Transaction createTransaction( BankAccount credit, BankAccount debit){
		
		Transaction t = new Transaction();
		t.setBookings( new ArrayList<Booking>());
		t.setCreditAccount(credit);
		t.setDebitAccount(debit);
		t.setDescription("From" + debit.getAccountNo() + " to" + credit.getAccountNo());
		t.setState( Transaction.STATE_OPEN);
		t.setType(Transaction.TYPE_DIRECT_DEBIT);
		t.setId(45978l);
		return t;
	}

	
	public static Booking createBooking( double amount, Transaction t){
		
		Booking b = new Booking();
		b.setAmount( amount);
		b.setType( BookingType.LENDER_DEPOSIT);
		b.setTransaction(t);
		b.setDescription( "desc");
		
		
		if (t.getBookings() == null) {
			t.setBookings( new ArrayList<Booking>());
		}
		t.getBookings().add(b);
		
		return b;
		
	}
	
	public static BookingGroup createBookingGroup(){
		
		BookingGroup bg = new BookingGroup();
		bg.setType( BookingGroup.TYPE_CREDIT_PAYOUT);
		bg.setDate( CurrentDate.getDate());
		bg.setName( "Oktober 2007|B36");
		return bg;
		
	}
	
	public static Contract createContract( Collection<Booking> bookings) {
		
		Contract c = new Contract();
		
		double a = 0.0;
		for ( Booking b: bookings){
			a += b.getAmount();
		}
		c.setAmount( a);
		c.setBookings(bookings);
		c.setContractNumber( "12345");

		
		return c;
		
	}
	

	public static Offer createOffer( Account lender){
		Offer o = new Offer();
		o.setAccount(lender);
		o.setAmount(250.0);
		o.setContract( createContract());
		return o;
	}
	public static Order createOrder(){
		return  createOrder( createBorrower());
	}
	
	public static Order createOrder(Account a){
		Order o = new Order();
		o.setAccount( a);
		BankAccountProvider myFidor = createFidorProvider();
		a.setInternalAccount(createInternalBankAccount(a, myFidor));
		o.setGroupId(1256l);
		o.setRoi(3.1478);
		o.setCreditTerm("60");
		o.setBankAccountProvider(myFidor);
		return o;
	}
	public static BidSearch createBidSearch(){
		BidSearch bs = new BidSearch();
		Account a = createLender();
		bs.setAccount( a);
		BankAccountProvider myFidor = createFidorProvider();
		createInternalBankAccount(a, myFidor);
		a.isLender();
		return bs;
	}
	
	public static Contract createContract(){
		Contract c = new Contract();
		
		Offer of = new Offer();
		of.setAccount(createLender());
		of.setId(7272l);
		of.setCreationDate(CurrentDate.getDate());
		
		Order or = new Order();
		or.setAccount(createBorrower());
		or.setId(3333l);
		or.setCreationDate(CurrentDate.getDate());
		c.setOffer(of);
		c.setOrder(or);
		c.setCreationDate(CurrentDate.getDate());
		return c;
	}
	
	public static ConsolidatedDebt createDebt(){
		ConsolidatedDebt cd = new ConsolidatedDebt();
		
		cd.setMonthsRemaining(31);
		cd.setCreditType(ConsolidatedDebt.TYPE_PRIVATE_LOAN);
		cd.setConsolidation(false);
		cd.setState(ConsolidatedDebt.STATE_APPLIED);
		cd.setMonthlyInstallment(100.0);
		cd.setInterestRate(5.0);
		cd.setCreationDate(CurrentDate.getDate());
		return cd;
	}
	
	public static ConsolidatedDebt createDebtWithConsolidation(){
		ConsolidatedDebt cd = new ConsolidatedDebt();
		
		cd.setMonthsRemaining(31);
		cd.setCreditType(ConsolidatedDebt.TYPE_PRIVATE_LOAN);
		cd.setConsolidation(true);
		cd.setState(ConsolidatedDebt.STATE_APPLIED);
		cd.setMonthlyInstallment(100.0);
		cd.setInterestRate(5.0);
		cd.setCreationDate(CurrentDate.getDate());
		cd.setDebtAmount(10116.30);
		Calendar c = CurrentDate.getCalendar();
		c.add(Calendar.YEAR, -2);
		cd.setStartDate(c.getTime());

		BankAccount bankAccount = new BankAccount();
		bankAccount.setAccountNo("52525252");
		bankAccount.setBankCode("10000000");
		bankAccount.setBank("Meine tolle Bank");
		bankAccount.setCreationDate(CurrentDate.getDate());

		ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();
		bankAccounts.add(bankAccount);

		cd.setBankAccounts(bankAccounts);
		
		return cd;
	}
	
	public static Address createAddress(){
		Address adr = new Address();
		adr.setCity("Berlin");
		adr.setCountry("DE");
		adr.setCreationDate(new Date());
		adr.setStreet("Kopernikusstr");
		adr.setStreetNumber("35A");
		adr.setZipCode("10243");
		adr.setType(Address.TYPE_MAIN_ADDRESS);
		
		return adr;
	}
	
	public static Set<Contract> createCreditProject2Contracts( Account a){
		BankAccountProvider provider = createFidorProvider();
		BankAccount credit = TestDataCreator.createInterimInAccount( provider);
		BankAccount debit = TestDataCreator.createInternalBankAccount(TestDataCreator.createBorrower(), provider);

		List<Transaction> transactions = new ArrayList<Transaction>();
		
		
		Transaction t1 = TestDataCreator.createTransaction(credit, debit);
		Transaction t2 = TestDataCreator.createTransaction(credit, debit);
		t1.setId(1l);
		t2.setId(2l);
		
		transactions.add( t1);
		transactions.add( t2);
		
		Booking b11 = createBooking( 100.d, t1);
		Booking b12 = createBooking( 200.d, t1);
		
		
		Order o1 = createOrder();
		o1.setAmount(2000.0);
		Collection<Booking> b1 = new ArrayList<Booking>();
		b1.add( b11);
		b1.add( b12);
		
		Contract c1 =  TestDataCreator.createContract( b1);
		
		c1.setOrder(o1);
		
		Booking b21 = createBooking( 60.d, t1);
		Collection<Booking> b2 = new ArrayList<Booking>();
		b1.add( b21);
		
		Contract c2 =  TestDataCreator.createContract( b2);
		Order o2 = createOrder();
		o2.setAmount(1000.0);
		c2.setOrder(o2);
		Set<Contract> contracts = new HashSet<Contract>();
		contracts.add( c1);
		contracts.add( c2);
		
		
		return contracts;
	}

	/**
	 * Create AccountAddress entity with reference to specified account.
	 * @param a
	 * @return
	 */
	public static AccountAddress createAccountAddress(Account a) {
		AccountAddress result = new AccountAddress();
		result.setAccount(a);
		result.setCity("Berlin");
		result.setCountry("DE");
		result.setStreet("test strasse");
		result.setZipCode("12312");
		result.setStreetNumber("12");
		result.setType(Address.TYPE_MAIN_ADDRESS);
		a.addAddress(result);
		return result;
	}

	/**
	 * Create AccountAddress entity with reference to specified account.
	 * @param a
	 * @return
	 */
	public static AccountAddress createPreviousAccountAddress(Account a) {
		AccountAddress result = new AccountAddress();
		result.setAccount(a);
		result.setCity("Berlin");
		result.setCountry("DE");
		result.setStreet("vohertest strasse");
		result.setZipCode("12312");
		result.setStreetNumber("12");
		result.setType(Address.TYPE_MAIN_ADDRESS);
		result.setExpirationDate(DateUtils.getDateEndOfPreviousMonth(new Date()));
		a.addAddress(result);
		return result;
	}
	
    /**
     * Create minimal persistable account entity
     * @return
     */
    public static Account createBaseAccount() {
        Account result = new Account();
        result.setUsername("test");
        result.setEmail("test@test.de");
        result.setCreationDate(CurrentDate.getDate());
        return result;
    }


}
