package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractCustomerAgreement;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CustomerAgreements'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CustomerAgreementHistory extends AbstractCustomerAgreement {

    protected transient Date _agreementDateInitVal;
    protected transient boolean _agreementDateIsSet;
    protected transient Date _revocationDateInitVal;
    protected transient boolean _revocationDateIsSet;


	
    /**
     * Returns the initial value of the property 'agreement date'.
     */
    public Date agreementDateInitVal() {
        Date result;
        if (_agreementDateIsSet) {
            result = _agreementDateInitVal;
        } else {
            result = getAgreementDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'agreement date'.
     */
    public boolean agreementDateIsDirty() {
        return !valuesAreEqual(agreementDateInitVal(), getAgreementDate());
    }

    /**
     * Returns true if the setter method was called for the property 'agreement date'.
     */
    public boolean agreementDateIsSet() {
        return _agreementDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'revocation date'.
     */
    public Date revocationDateInitVal() {
        Date result;
        if (_revocationDateIsSet) {
            result = _revocationDateInitVal;
        } else {
            result = getRevocationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'revocation date'.
     */
    public boolean revocationDateIsDirty() {
        return !valuesAreEqual(revocationDateInitVal(), getRevocationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'revocation date'.
     */
    public boolean revocationDateIsSet() {
        return _revocationDateIsSet;
    }
			
}
