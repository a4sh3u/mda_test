//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.backoffice.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bo search)}
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.backoffice.domain.BoSearch;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BoSearchs'.
 *
 * @author generator
 */
public interface BoSearchDao extends BaseDao<BoSearch> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bo search identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BoSearch getBoSearch(Long id);

    /**
     * Saves the bo search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBoSearch(BoSearch boSearch);

    /**
     * Deletes an bo search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bo search
     */
    void deleteBoSearch(Long id);

    /**
     * Retrieves all 'BoSearch' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BoSearch> getBoSearchList();

    /**
     * Retrieves a page of 'BoSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BoSearch> getBoSearchList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BoSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BoSearch> getBoSearchList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BoSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BoSearch> getBoSearchList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BoSearch' instances.
     */
    long getBoSearchCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bo search)}
    //
    // insert custom methods here

    Collection<BoSearch> retrieveCustomers(String email, String firstName, String lastName, String phone, Date birth,
                                                 Integer offset, Integer limit, String orderBy, boolean desc);

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
