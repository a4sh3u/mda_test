//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(password algorithm)}
import de.smava.webapp.account.domain.history.PasswordAlgorithmHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PasswordAlgorithms'.
 *
 * @author generator
 */
public class PasswordAlgorithm extends PasswordAlgorithmHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(password algorithm)}
    // !!!!!!!! End of insert code section !!!!!!!!


            protected PasswordAlgorithmType _type;
            protected Boolean _active;
        
                        /**
            * Setter for the property 'type'.
            */
            public void setType(PasswordAlgorithmType type) {
            _type = type;
            }

            /**
            * Returns the property 'type'.
            */
            public PasswordAlgorithmType getType() {
            return _type;
            }
                                /**
            * Setter for the property 'active'.
            */
            public void setActive(Boolean active) {
            _active = active;
            }

            /**
            * Returns the property 'active'.
            */
            public Boolean getActive() {
            return _active;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(PasswordAlgorithm.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(PasswordAlgorithm.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public PasswordAlgorithm asPasswordAlgorithm() {
        return this;
    }
}
