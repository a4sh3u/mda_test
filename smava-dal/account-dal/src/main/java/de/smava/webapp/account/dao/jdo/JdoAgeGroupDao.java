//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(age group)}

import de.smava.webapp.account.domain.AgeGroup;
import de.smava.webapp.account.todo.dao.AgeGroupDao;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'AgeGroups'.
 *
 * @author generator
 */
@Repository(value = "ageGroupDao")
public class JdoAgeGroupDao extends JdoBaseDao implements AgeGroupDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAgeGroupDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(age group)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the age group identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public AgeGroup load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroup - start: id=" + id);
        }
        AgeGroup result = getEntity(AgeGroup.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroup - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public AgeGroup getAgeGroup(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	AgeGroup entity = findUniqueEntity(AgeGroup.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the age group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(AgeGroup ageGroup) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAgeGroup: " + ageGroup);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(age group)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(ageGroup);
    }

    /**
     * @deprecated Use {@link #save(AgeGroup) instead}
     */
    public Long saveAgeGroup(AgeGroup ageGroup) {
        return save(ageGroup);
    }

    /**
     * Deletes an age group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the age group
     */
    public void deleteAgeGroup(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAgeGroup: " + id);
        }
        deleteEntity(AgeGroup.class, id);
    }

    /**
     * Retrieves all 'AgeGroup' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<AgeGroup> getAgeGroupList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - start");
        }
        Collection<AgeGroup> result = getEntities(AgeGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'AgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<AgeGroup> getAgeGroupList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - start: pageable=" + pageable);
        }
        Collection<AgeGroup> result = getEntities(AgeGroup.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'AgeGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<AgeGroup> getAgeGroupList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - start: sortable=" + sortable);
        }
        Collection<AgeGroup> result = getEntities(AgeGroup.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'AgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<AgeGroup> getAgeGroupList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<AgeGroup> result = getEntities(AgeGroup.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'AgeGroup' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AgeGroup> findAgeGroupList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - start: whereClause=" + whereClause);
        }
        Collection<AgeGroup> result = findEntities(AgeGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'AgeGroup' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<AgeGroup> findAgeGroupList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<AgeGroup> result = findEntities(AgeGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'AgeGroup' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AgeGroup> findAgeGroupList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<AgeGroup> result = findEntities(AgeGroup.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'AgeGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AgeGroup> findAgeGroupList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<AgeGroup> result = findEntities(AgeGroup.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'AgeGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AgeGroup> findAgeGroupList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<AgeGroup> result = findEntities(AgeGroup.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'AgeGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AgeGroup> findAgeGroupList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<AgeGroup> result = findEntities(AgeGroup.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'AgeGroup' instances.
     */
    public long getAgeGroupCount() {
        long result = getEntityCount(AgeGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'AgeGroup' instances which match the given whereClause.
     */
    public long getAgeGroupCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(AgeGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'AgeGroup' instances which match the given whereClause together with params specified in object array.
     */
    public long getAgeGroupCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(AgeGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAgeGroupCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(age group)}
    //

    public Collection<AgeGroup> getActiveAgeGroups() {
        return findByNamedQuery(AgeGroup.class, "getActiveAgeGroups", null);
    }
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
