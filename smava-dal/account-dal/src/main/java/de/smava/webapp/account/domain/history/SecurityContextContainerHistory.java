package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractSecurityContextContainer;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'SecurityContextContainers'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SecurityContextContainerHistory extends AbstractSecurityContextContainer {

    protected transient String _sessionIdInitVal;
    protected transient boolean _sessionIdIsSet;
    protected transient Date _updateTimestampInitVal;
    protected transient boolean _updateTimestampIsSet;


	
    /**
     * Returns the initial value of the property 'session id'.
     */
    public String sessionIdInitVal() {
        String result;
        if (_sessionIdIsSet) {
            result = _sessionIdInitVal;
        } else {
            result = getSessionId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'session id'.
     */
    public boolean sessionIdIsDirty() {
        return !valuesAreEqual(sessionIdInitVal(), getSessionId());
    }

    /**
     * Returns true if the setter method was called for the property 'session id'.
     */
    public boolean sessionIdIsSet() {
        return _sessionIdIsSet;
    }
		
    /**
     * Returns the initial value of the property 'update timestamp'.
     */
    public Date updateTimestampInitVal() {
        Date result;
        if (_updateTimestampIsSet) {
            result = _updateTimestampInitVal;
        } else {
            result = getUpdateTimestamp();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'update timestamp'.
     */
    public boolean updateTimestampIsDirty() {
        return !valuesAreEqual(updateTimestampInitVal(), getUpdateTimestamp());
    }

    /**
     * Returns true if the setter method was called for the property 'update timestamp'.
     */
    public boolean updateTimestampIsSet() {
        return _updateTimestampIsSet;
    }

}
