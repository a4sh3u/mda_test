package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractAttachment;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Attachments'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AttachmentHistory extends AbstractAttachment {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient String _contentTypeInitVal;
    protected transient boolean _contentTypeIsSet;
    protected transient String _locationInitVal;
    protected transient boolean _locationIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'content type'.
     */
    public String contentTypeInitVal() {
        String result;
        if (_contentTypeIsSet) {
            result = _contentTypeInitVal;
        } else {
            result = getContentType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'content type'.
     */
    public boolean contentTypeIsDirty() {
        return !valuesAreEqual(contentTypeInitVal(), getContentType());
    }

    /**
     * Returns true if the setter method was called for the property 'content type'.
     */
    public boolean contentTypeIsSet() {
        return _contentTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'location'.
     */
    public String locationInitVal() {
        String result;
        if (_locationIsSet) {
            result = _locationInitVal;
        } else {
            result = getLocation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'location'.
     */
    public boolean locationIsDirty() {
        return !valuesAreEqual(locationInitVal(), getLocation());
    }

    /**
     * Returns true if the setter method was called for the property 'location'.
     */
    public boolean locationIsSet() {
        return _locationIsSet;
    }
		
}
