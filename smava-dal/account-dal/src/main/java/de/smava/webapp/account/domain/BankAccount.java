package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.BankAccountHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'BankAccounts'.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@bankAccountId")
public class BankAccount extends BankAccountHistory  {

    public BankAccount() {
        _valid = true;
    }

    protected Date _creationDate;
    protected Date _expirationDate;
    protected String _name;
    protected String _bank;
    protected String _accountNo;
    protected String _bankCode;
    protected boolean _smavaOnly;
    protected boolean _debitAllowed;
    protected Account _account;
    protected boolean _valid;
    protected String _type;
    protected String _customerNumber;
    protected String _bic;
    protected String _iban;
    protected de.smava.webapp.account.domain.BankAccountProvider _provider;
    protected ConsolidatedDebt _consolidatedDebt;
    protected Collection<BankAccountClearingRun> _bankAccountClearingRuns;
    protected boolean _activityFee;
    protected Boolean _paythroughPossible;
    protected String _sepaMandateReference;

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }

    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }

    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }

    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
    /**
     * Setter for the property 'bank'.
     */
    public void setBank(String bank) {
        if (!_bankIsSet) {
            _bankIsSet = true;
            _bankInitVal = getBank();
        }
        registerChange("bank", _bankInitVal, bank);
        _bank = bank;
    }

    /**
     * Returns the property 'bank'.
     */
    public String getBank() {
        return _bank;
    }
    /**
     * Setter for the property 'account no'.
     */
    public void setAccountNo(String accountNo) {
        if (!_accountNoIsSet) {
            _accountNoIsSet = true;
            _accountNoInitVal = getAccountNo();
        }
        registerChange("account no", _accountNoInitVal, accountNo);
        _accountNo = accountNo;
    }

    /**
     * Returns the property 'account no'.
     */
    public String getAccountNo() {
        return _accountNo;
    }
    /**
     * Setter for the property 'bank code'.
     */
    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = getBankCode();
        }
        registerChange("bank code", _bankCodeInitVal, bankCode);
        _bankCode = bankCode;
    }

    /**
     * Returns the property 'bank code'.
     */
    public String getBankCode() {
        return _bankCode;
    }
    /**
     * Setter for the property 'smava only'.
     */
    public void setSmavaOnly(boolean smavaOnly) {
        if (!_smavaOnlyIsSet) {
            _smavaOnlyIsSet = true;
            _smavaOnlyInitVal = getSmavaOnly();
        }
        registerChange("smava only", _smavaOnlyInitVal, smavaOnly);
        _smavaOnly = smavaOnly;
    }

    /**
     * Returns the property 'smava only'.
     */
    public boolean getSmavaOnly() {
        return _smavaOnly;
    }
    /**
     * Setter for the property 'debit allowed'.
     */
    public void setDebitAllowed(boolean debitAllowed) {
        if (!_debitAllowedIsSet) {
            _debitAllowedIsSet = true;
            _debitAllowedInitVal = getDebitAllowed();
        }
        registerChange("debit allowed", _debitAllowedInitVal, debitAllowed);
        _debitAllowed = debitAllowed;
    }

    /**
     * Returns the property 'debit allowed'.
     */
    public boolean getDebitAllowed() {
        return _debitAllowed;
    }

    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
    /**
     * Setter for the property 'valid'.
     */
    public void setValid(boolean valid) {
        if (!_validIsSet) {
            _validIsSet = true;
            _validInitVal = getValid();
        }
        registerChange("valid", _validInitVal, valid);
        _valid = valid;
    }

    /**
     * Returns the property 'valid'.
     */
    public boolean getValid() {
        return _valid;
    }
    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }

    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
    /**
     * Setter for the property 'customer number'.
     */
    public void setCustomerNumber(String customerNumber) {
        if (!_customerNumberIsSet) {
            _customerNumberIsSet = true;
            _customerNumberInitVal = getCustomerNumber();
        }
        registerChange("customer number", _customerNumberInitVal, customerNumber);
        _customerNumber = customerNumber;
    }

    /**
     * Returns the property 'customer number'.
     */
    public String getCustomerNumber() {
        return _customerNumber;
    }
    /**
     * Setter for the property 'bic'.
     */
    public void setBic(String bic) {
        if (!_bicIsSet) {
            _bicIsSet = true;
            _bicInitVal = getBic();
        }
        registerChange("bic", _bicInitVal, bic);
        _bic = bic;
    }

    /**
     * Returns the property 'bic'.
     */
    public String getBic() {
        return _bic;
    }
    /**
     * Setter for the property 'iban'.
     */
    public void setIban(String iban) {
        if (!_ibanIsSet) {
            _ibanIsSet = true;
            _ibanInitVal = getIban();
        }
        registerChange("iban", _ibanInitVal, iban);
        _iban = iban;
    }

    /**
     * Returns the property 'iban'.
     */
    public String getIban() {
        return _iban;
    }

    /**
     * Setter for the property 'provider'.
     */
    public void setProvider(de.smava.webapp.account.domain.BankAccountProvider provider) {
        _provider = provider;
    }

    /**
     * Returns the property 'provider'.
     */
    public de.smava.webapp.account.domain.BankAccountProvider getProvider() {
        return _provider;
    }

    /**
     * Setter for the property 'consolidated debt'.
     */
    public void setConsolidatedDebt(ConsolidatedDebt consolidatedDebt) {
        _consolidatedDebt = consolidatedDebt;
    }

    /**
     * Returns the property 'consolidated debt'.
     */
    public ConsolidatedDebt getConsolidatedDebt() {
        return _consolidatedDebt;
    }

    /**
     * Setter for the property 'bank account clearing runs'.
     */
    public void setBankAccountClearingRuns(Collection<BankAccountClearingRun> bankAccountClearingRuns) {
        _bankAccountClearingRuns = bankAccountClearingRuns;
    }

    /**
     * Returns the property 'bank account clearing runs'.
     */
    public Collection<BankAccountClearingRun> getBankAccountClearingRuns() {
        return _bankAccountClearingRuns;
    }
    /**
     * Setter for the property 'activity fee'.
     */
    public void setActivityFee(boolean activityFee) {
        if (!_activityFeeIsSet) {
            _activityFeeIsSet = true;
            _activityFeeInitVal = getActivityFee();
        }
        registerChange("activity fee", _activityFeeInitVal, activityFee);
        _activityFee = activityFee;
    }

    /**
     * Returns the property 'activity fee'.
     */
    public boolean getActivityFee() {
        return _activityFee;
    }

    /**
     * Setter for the property 'paythrough possible'.
     */
    public void setPaythroughPossible(Boolean paythroughPossible) {
        _paythroughPossible = paythroughPossible;
    }

    /**
     * Returns the property 'paythrough possible'.
     */
    public Boolean getPaythroughPossible() {
        return _paythroughPossible;
    }
    /**
     * Setter for the property 'sepa mandate reference'.
     */
    public void setSepaMandateReference(String sepaMandateReference) {
        if (!_sepaMandateReferenceIsSet) {
            _sepaMandateReferenceIsSet = true;
            _sepaMandateReferenceInitVal = getSepaMandateReference();
        }
        registerChange("sepa mandate reference", _sepaMandateReferenceInitVal, sepaMandateReference);
        _sepaMandateReference = sepaMandateReference;
    }

    /**
     * Returns the property 'sepa mandate reference'.
     */
    public String getSepaMandateReference() {
        return _sepaMandateReference;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BankAccount.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _bank=").append(_bank);
            builder.append("\n    _accountNo=").append(_accountNo);
            builder.append("\n    _bankCode=").append(_bankCode);
            builder.append("\n    _smavaOnly=").append(_smavaOnly);
            builder.append("\n    _debitAllowed=").append(_debitAllowed);
            builder.append("\n    _valid=").append(_valid);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _customerNumber=").append(_customerNumber);
            builder.append("\n    _bic=").append(_bic);
            builder.append("\n    _iban=").append(_iban);
            builder.append("\n    _activityFee=").append(_activityFee);
            builder.append("\n    _sepaMandateReference=").append(_sepaMandateReference);
            builder.append("\n}");
        } else {
            builder.append(BankAccount.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
