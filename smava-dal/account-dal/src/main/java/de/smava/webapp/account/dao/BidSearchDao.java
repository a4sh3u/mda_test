//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid search)}
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BidSearchs'.
 *
 * @author generator
 */
public interface BidSearchDao extends BaseDao<BidSearch>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bid search identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BidSearch getBidSearch(Long id);

    /**
     * Saves the bid search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBidSearch(BidSearch bidSearch);

    /**
     * Deletes an bid search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid search
     */
    void deleteBidSearch(Long id);

    /**
     * Retrieves all 'BidSearch' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BidSearch> getBidSearchList();

    /**
     * Retrieves a page of 'BidSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BidSearch> getBidSearchList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BidSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BidSearch> getBidSearchList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BidSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BidSearch> getBidSearchList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BidSearch' instances.
     */
    long getBidSearchCount();




    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid search)}

    Collection<BidSearch> getActiveBidAssistants();

    Collection<BidSearch> getActiveBidAssistants(Account account);

    Collection<BidSearch> getBidAssistants(Account account);

    /**
     * Returns only bid.agents. No portfolio.agents are retrieved.
     * @param account the lender which bid.agents we wont to have.
     * @return Collection of bid.agents for the given account.
     */
    Collection<BidSearch> getPlainBidAssistants(Account account);



    Collection<BidSearch> getActiveMailNotifications();



    Collection<BidSearch> getPortfolioBidAssistants(Long portfolioId, boolean active);

    public Collection<BidSearch> getActivePortfolioAgents();


    Set<Long> getActiveBidSearchIds(Collection<String> types);

    boolean hasActiveBidAssistantsWithRestriction(Account account);

    void removeBidAssistantsRestriction(Account account);

    // !!!!!!!! End of insert code section !!!!!!!!
}
