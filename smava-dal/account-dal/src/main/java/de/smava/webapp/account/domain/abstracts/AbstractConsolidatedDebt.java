package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.ConsolidatedDebt;
import de.smava.webapp.account.domain.interfaces.ConsolidatedDebtEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * The domain object that represents 'ConsolidatedDebts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractConsolidatedDebt
    extends KreditPrivatEntity implements ConsolidatedDebtEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractConsolidatedDebt.class);

    public static Set<String> OPEN_STATES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(ConsolidatedDebt.STATE_ACCEPTED, ConsolidatedDebt.STATE_APPLIED)));

    public static final String STATE_APPLIED 	= "APPLIED";
    public static final String STATE_ACCEPTED 	= "ACCEPTED";
    public static final String STATE_DENIED 	= "DENIED";
    public static final String STATE_COMPLETED	= "COMPLETED";
    public static final String STATE_DELETED 	= "DELETED";

    public static final Set<String> ALL_STATES  = ConstCollector.getAll("STATE");

    public static final String TYPE_PRIVATE_LOAN = "PRIVATE_LOAN";
    public static final String TYPE_BUSINESS_LOAN = "BUSINESS_LOAN";

    public BankAccount getCurrentBankAccount() {
        BankAccount result = null;
        for (BankAccount bankAccount : getBankAccounts()) {
            if (bankAccount.getExpirationDate() == null) {
                result = bankAccount;
                break;
            }
        }
        return result;
    }

    public boolean functionallyEquals ( ConsolidatedDebt compare){
        if (compare == null
                || compare.getCreditType() == null
                || compare.getMonthsRemaining() == null
                || compare.getMonthlyInstallment() == null){
            return false;
        }

        if ( TYPE_BUSINESS_LOAN.equals( getCreditType()) || getConsolidation()){
            if (  getMonthsRemaining().equals(compare.getMonthsRemaining())
                    && getMonthlyInstallment().equals(compare.getMonthlyInstallment()))
            {
                return true;
            }
        } else {
            if ( getMonthsRemaining().equals(compare.getMonthsRemaining())
                    && getMonthlyInstallment().equals(compare.getMonthlyInstallment())
                    && getDueDate().equals(compare.getDueDate())
                    && getCurrentBankAccount().functionallyEqual( compare.getCurrentBankAccount(), true))
            {
                return true;
            }
        }
        return false;
    }

}

