package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;

import java.util.Date;


/**
 * The domain object that represents 'InvestorAssets'.
 *
 * @author generator
 */
public interface InvestorAssetEntityInterface {

    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    double getAmount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'active'.
     *
     * 
     *
     */
    void setActive(boolean active);

    /**
     * Returns the property 'active'.
     *
     * 
     *
     */
    boolean getActive();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();

}
