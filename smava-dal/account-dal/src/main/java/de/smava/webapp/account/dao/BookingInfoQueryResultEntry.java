package de.smava.webapp.account.dao;

import de.smava.webapp.account.domain.BookingType;

import java.io.Serializable;

/**
 * @author Dimitar Robev
 * 
 */
public class BookingInfoQueryResultEntry implements Serializable {

	private static final long serialVersionUID = -1860659792350239037L;
	
	private double _amountSum;  
	private Integer _bookingTypeInteger;
	private BookingType _bookingType;
	private long _bookingGroupId;  
	private String _transactionState;  
	private long _transactionId; 
	private long _creditAccount;
	private long _debitAccount;
	private long _contractId;
	private boolean _target;
	private String _debitBankAccountType;
	private String _creditBankAccountType;
//	private Timestamp _transactionDate;

	public double getAmountSum() {
		return _amountSum;
	}

	public void setAmountSum(double sum) {
		_amountSum = sum;
	}

	public BookingType getBookingType() {
		if (_bookingType == null) {
			_bookingType = BookingType.enumFromDbValue(_bookingTypeInteger);
		}
		return _bookingType;
	}

	public void setBookingType(BookingType type) {
		_bookingType = type;
		if (type != null) {
			_bookingTypeInteger = type.getDbValue();
		} else {
			_bookingTypeInteger = null;
		}
	}
	
	public Integer getBookingTypeInteger() {
		return _bookingTypeInteger;
	}

	public void setBookingTypeInteger(Integer bookingTypeInteger) {
		_bookingTypeInteger = bookingTypeInteger;
	}

	public long getBookingGroupId() {
		return _bookingGroupId;
	}

	public void setBookingGroupId(long id) {
		_bookingGroupId = id;
	}

	public String getTransactionState() {
		return _transactionState;
	}

	public void setTransactionState(String state) {
		_transactionState = state;
	}

	public long getTransactionId() {
		return _transactionId;
	}

	public void setTransactionId(long id) {
		_transactionId = id;
	}

	public long getContractId() {
		return _contractId;
	}

	public void setContractId(long id) {
		_contractId = id;
	}

	public boolean isTarget() {
		return _target;
	}

	public void setTarget(boolean target) {
		_target = target;
	} 

	public long getCreditAccount() {
		return _creditAccount;
	}

	public void setCreditAccount(long account) {
		_creditAccount = account;
	}

	public String getDebitBankAccountType() {
		return _debitBankAccountType;
	}

	public void setDebitBankAccountType(String bankAccountType) {
		_debitBankAccountType = bankAccountType;
	}

	public String getCreditBankAccountType() {
		return _creditBankAccountType;
	}

	public void setCreditBankAccountType(String bankAccountType) {
		_creditBankAccountType = bankAccountType;
	}

	public long getDebitAccount() {
		return _debitAccount;
	}

	public void setDebitAccount(long account) {
		_debitAccount = account;
	}

//	public Date getTransactionDate() {
//		return _transactionDate;
//	}
//
//	public void setTransactionDate(Timestamp date) {
//		_transactionDate = date;
//	}
}
