package de.smava.webapp.account.dao.events;

import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.springframework.context.ApplicationEvent;

/**
 * @author Daniel Doubleday
 */
public final class EntityEvent extends ApplicationEvent {
    private static final long serialVersionUID = 1200070781927044875L;

    /**
     * Allowed event types.
     */
    public enum Type {
        CREATED,
        MODIFIED,
        DELETED
    }

    private final KreditPrivatEntity _entity;
    private final Type _type;

    public static EntityEvent createCreatedEvent(Object source, KreditPrivatEntity entity) {
        return new EntityEvent(Type.CREATED, source, entity);
    }

    public static EntityEvent createModifiedEvent(Object source, KreditPrivatEntity entity) {
        return new EntityEvent(Type.MODIFIED, source, entity);
    }

    public static EntityEvent createDeletedEvent(Object source, KreditPrivatEntity entity) {
        return new EntityEvent(Type.DELETED, source, entity);
    }

    /**
     * Create a new ApplicationEvent.
     *
     * @param sourceObject the component that published the event
     */
    private EntityEvent(Type type, Object sourceObject, KreditPrivatEntity entity) {
        super(sourceObject);
        _type = type;
        _entity = entity;
    }

    public KreditPrivatEntity getEntity() {
        return _entity;
    }

    public Type getType() {
        return _type;
    }
}