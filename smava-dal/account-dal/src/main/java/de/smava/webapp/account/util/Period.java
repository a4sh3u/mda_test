/**
 * 
 */
package de.smava.webapp.account.util;

import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Date;

/**
 * @author bvoss
 *
 */
public class Period {
	
	private final Date _start;
	private final Date _end;
	
	
	public Period(final Date start, final Date end) {
		super();
		if (end.before(start)) {
			throw new IllegalArgumentException("start wasn't before end");
		}
		_start = createClone(start);
		_end = createClone(end);
	}

	public Date getStart() {
		return createClone(_start);
	}

	public Date getEnd() {
		return createClone(_end);
	}
	
	private Date createClone(final Date orig) {
		return (Date) orig.clone();
	}
	
	// factory methodes
	
	public static Period getMonthPeriodFromDate(final Date date) {
		final Date start = DateUtils.getDateStartOfMonth(date);
		final Date end = DateUtils.getDateEndOfMonth(date);
		return new Period(start, end);
	}
	
	public static Period getCurrentMonthPeriod() {
		return getMonthPeriodFromDate(CurrentDate.getDate());
	}
}
