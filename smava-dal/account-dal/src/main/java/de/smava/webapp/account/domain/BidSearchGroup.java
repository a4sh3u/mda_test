package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BidSearchGroupHistory;

import java.util.Collection;
import java.util.HashSet;

/**
 * The domain object that represents 'BidSearchGroups'.
 */
public class BidSearchGroup extends BidSearchGroupHistory  {

    public static final String TYPE_PORTFOLIO = "portfolio";
    public static final String TYPE_TRANCHE   = "tranche";

    public BidSearchGroup() {
        if (getBidSearches() == null) {
            setBidSearches(new HashSet<BidSearch>());
        }
    }

        protected String _name;
        protected String _type;
        protected Collection<BidSearch> _bidSearches;
        
                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'bid searches'.
     */
    public void setBidSearches(Collection<BidSearch> bidSearches) {
        _bidSearches = bidSearches;
    }
            
    /**
     * Returns the property 'bid searches'.
     */
    public Collection<BidSearch> getBidSearches() {
        return _bidSearches;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BidSearchGroup.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(BidSearchGroup.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
