package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.MessageConfigHistory;

import java.util.List;

/**
 * The domain object that represents 'MessageConfigs'.
 */
public class MessageConfig extends MessageConfigHistory  {

    public MessageConfig() {
        _forward = true;
    }

        protected boolean _forward;
        protected boolean _forwardLenderStatusMail;
        protected List<Account> _ignoreList;
        protected Account _account;

                            /**
     * Setter for the property 'forward'.
     */
    public void setForward(boolean forward) {
        if (!_forwardIsSet) {
            _forwardIsSet = true;
            _forwardInitVal = getForward();
        }
        registerChange("forward", _forwardInitVal, forward);
        _forward = forward;
    }
                        
    /**
     * Returns the property 'forward'.
     */
    public boolean getForward() {
        return _forward;
    }
                                    /**
     * Setter for the property 'forward lender status mail'.
     */
    public void setForwardLenderStatusMail(boolean forwardLenderStatusMail) {
        if (!_forwardLenderStatusMailIsSet) {
            _forwardLenderStatusMailIsSet = true;
            _forwardLenderStatusMailInitVal = getForwardLenderStatusMail();
        }
        registerChange("forward lender status mail", _forwardLenderStatusMailInitVal, forwardLenderStatusMail);
        _forwardLenderStatusMail = forwardLenderStatusMail;
    }
                        
    /**
     * Returns the property 'forward lender status mail'.
     */
    public boolean getForwardLenderStatusMail() {
        return _forwardLenderStatusMail;
    }
                                            
    /**
     * Setter for the property 'ignore list'.
     */
    public void setIgnoreList(List<Account> ignoreList) {
        _ignoreList = ignoreList;
    }
            
    /**
     * Returns the property 'ignore list'.
     */
    public List<Account> getIgnoreList() {
        return _ignoreList;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MessageConfig.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _forward=").append(_forward);
            builder.append("\n    _forwardLenderStatusMail=").append(_forwardLenderStatusMail);
            builder.append("\n}");
        } else {
            builder.append(MessageConfig.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
