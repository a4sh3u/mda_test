package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.EconomicalDataEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.i18n.Employment;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'EconomicalDatas'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractEconomicalData
    extends KreditPrivatEntity implements EconomicalDataEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractEconomicalData.class);

    private static final long serialVersionUID = 1L;

    public static final String ACCOMODATION_TYPE_OWN = "OWN";
    public static final String ACCOMODATION_TYPE_RENT = "RENT";
    public static final String ACCOMODATION_TYPE_PARENT_PLACE = "PARENTS_PLACE";
    public static final String ACCOMODATION_TYPE_COLLECTIVE = "COLLECTIVE";
    public static final String ACCOMODATION_TYPE_OTHER = "OTHER";

    public static final int TYPE_NORMAL = 1;
    public static final int TYPE_SHARED_LOAN = 2;

    @SuppressWarnings("unchecked")
    public static final Collection<String> TYPES = ConstCollector.getAll("TYPE_");

    @SuppressWarnings("unchecked")
    public static final Collection<String> ACCOMODATION_TYPES = ConstCollector.getAll("ACCOMODATION_TYPE_");

    private transient List<Account> _modifiers = null;

    public void addFreelancerIncome(FreelancerIncome newIncome) {
        if (getFreelancerIncomes() == null) {
            setFreelancerIncomes(new ArrayList<FreelancerIncome>());
        }
        if (newIncome.getEconomicalData() != this) {
            newIncome.setEconomicalData(this.instance());
        }
        getFreelancerIncomes().add(newIncome);

    }

    public void removeFreelancerIncome(FreelancerIncome newIncome) {
        if (getFreelancerIncomes() != null) {
            if (newIncome.getEconomicalData() != null) {
                newIncome.setEconomicalData(null);
            }
            getFreelancerIncomes().remove(newIncome);
        }
    }

    public boolean isSharedLoan() {
        return TYPE_SHARED_LOAN == getType();
    }

    public void clearOtherIncomeFields() {
        setMiscEarn1(StringUtils.EMPTY);
        setMiscEarnAmount1(0.0d);
        setReceivedPalimony(0.0d);
        setReceivedRent(0.0d);
    }


    public double getAdjustedPrivateHealthInsurance( double freelancerMinPrivateHealthInsurance) {
        String employment = getEmployment();
        double result = getPrivateHealthInsurance();
        if (Employment.FREELANCER_EMPLOYMENTS.contains(employment)) {
            result = Math.max(result, freelancerMinPrivateHealthInsurance);
        }

        return result;
    }


    public Collection<ConsolidatedDebt> getOpenDebts() {
        //return _consolidatedDebtDao.findConsolidatedDebtsForAccount(account, OPEN_STATES, RELEVANT_ORDER_STATES, true);
        Collection<ConsolidatedDebt> ret = new HashSet<ConsolidatedDebt>();
        for (ConsolidatedDebt debt: getDebts()){
            if ( ConsolidatedDebt.OPEN_STATES.contains( debt.getState())){
                ret.add(debt);
            }
        }
        return ret;
    }


    public double getMonthlyInstallmentSumDebtsWithConsolidation() {
        double sum = 0.0;
        for (ConsolidatedDebt debt: getDebts()){
            if ( ConsolidatedDebt.OPEN_STATES.contains( debt.getState()) && debt.getConsolidation()){
                sum += debt.getMonthlyInstallment();
            }
        }
        return sum;
    }

    /**
     * Checks if there is at least consolidated debt
     * that is already consolidated.
     *
     * @return true if at least one debts.getConsolidation is true
     */
    public boolean isConsolidatedDebt() {
        for (ConsolidatedDebt debt : getDebts()) {
            if (debt.getConsolidation()) {
                return true;
            }
        }
        return false;
    }

    public List<? extends Entity> getModifier() {
        if (this._modifiers == null) {
            this._modifiers = Arrays.asList(new Account[]{getAccount()});
        }
        return this._modifiers;
    }

    public abstract EconomicalData instance();
}

