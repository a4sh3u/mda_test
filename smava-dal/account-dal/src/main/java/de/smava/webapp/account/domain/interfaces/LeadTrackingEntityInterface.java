package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.TrackingType;

import java.util.Date;


/**
 * The domain object that represents 'LeadTrackings'.
 *
 * @author generator
 */
public interface LeadTrackingEntityInterface {

    /**
     * Setter for the property 'tracking date'.
     *
     * 
     *
     */
    void setTrackingDate(Date trackingDate);

    /**
     * Returns the property 'tracking date'.
     *
     * 
     *
     */
    Date getTrackingDate();
    /**
     * Setter for the property 'tracking type'.
     *
     * 
     *
     */
    void setTrackingType(TrackingType trackingType);

    /**
     * Returns the property 'tracking type'.
     *
     * 
     *
     */
    TrackingType getTrackingType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();

}
