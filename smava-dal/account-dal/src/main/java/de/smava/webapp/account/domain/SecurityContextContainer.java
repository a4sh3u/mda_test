//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(security context container)}
import de.smava.webapp.account.domain.history.SecurityContextContainerHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SecurityContextContainers'.
 *
 * 
 *
 * @author generator
 */
public class SecurityContextContainer extends SecurityContextContainerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(security context container)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _sessionId;
        protected byte[] _securityContext;
        protected Date _updateTimestamp;
        
                            /**
     * Setter for the property 'session id'.
     *
     * 
     *
     */
    public void setSessionId(String sessionId) {
        if (!_sessionIdIsSet) {
            _sessionIdIsSet = true;
            _sessionIdInitVal = getSessionId();
        }
        registerChange("session id", _sessionIdInitVal, sessionId);
        _sessionId = sessionId;
    }
                        
    /**
     * Returns the property 'session id'.
     *
     * 
     *
     */
    public String getSessionId() {
        return _sessionId;
    }
                                            
    /**
     * Setter for the property 'security context'.
     *
     * 
     *
     */
    public void setSecurityContext(byte[] securityContext) {
        _securityContext = securityContext;
    }
            
    /**
     * Returns the property 'security context'.
     *
     * 
     *
     */
    public byte[] getSecurityContext() {
        return _securityContext;
    }
                                    /**
     * Setter for the property 'update timestamp'.
     *
     * 
     *
     */
    public void setUpdateTimestamp(Date updateTimestamp) {
        if (!_updateTimestampIsSet) {
            _updateTimestampIsSet = true;
            _updateTimestampInitVal = getUpdateTimestamp();
        }
        registerChange("update timestamp", _updateTimestampInitVal, updateTimestamp);
        _updateTimestamp = updateTimestamp;
    }
                        
    /**
     * Returns the property 'update timestamp'.
     *
     * 
     *
     */
    public Date getUpdateTimestamp() {
        return _updateTimestamp;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SecurityContextContainer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _sessionId=").append(_sessionId);
            builder.append("\n}");
        } else {
            builder.append(SecurityContextContainer.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SecurityContextContainer asSecurityContextContainer() {
        return this;
    }
}
