//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(encashment rate)}

import de.smava.webapp.account.domain.EncashmentRate;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'EncashmentRates'.
 *
 * @author generator
 */
public interface EncashmentRateDao extends BaseDao<EncashmentRate>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the encashment rate identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    EncashmentRate getEncashmentRate(Long id);

    /**
     * Saves the encashment rate.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveEncashmentRate(EncashmentRate encashmentRate);

    /**
     * Deletes an encashment rate, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the encashment rate
     */
    void deleteEncashmentRate(Long id);

    /**
     * Retrieves all 'EncashmentRate' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<EncashmentRate> getEncashmentRateList();

    /**
     * Retrieves a page of 'EncashmentRate' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<EncashmentRate> getEncashmentRateList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'EncashmentRate' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<EncashmentRate> getEncashmentRateList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'EncashmentRate' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<EncashmentRate> getEncashmentRateList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'EncashmentRate' instances.
     */
    long getEncashmentRateCount();


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(encashment rate)}
    //
    // insert custom methods here
    //
 
	EncashmentRate getEncashmentRateByMarketName(String key);
    
    // !!!!!!!! End of insert code section !!!!!!!!
}
