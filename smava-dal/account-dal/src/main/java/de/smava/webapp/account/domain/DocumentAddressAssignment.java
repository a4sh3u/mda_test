package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DocumentAddressAssignmentHistory;

/**
 * The domain object that represents 'DocumentAddressAssignments'.
 */
public class DocumentAddressAssignment extends DocumentAddressAssignmentHistory  {

    public DocumentAddressAssignment(Document document, Address address, String type) {
        LOGGER.debug("DocumentAddressAssignment.constructor: create assignment with document " + document.getId() + " and address " + address.getId() + " (type = " + type + ")");
        _document = document;
        _address = address;
        _type = type;
    }

        protected Document _document;
        protected Address _address;
        protected String _type;

    /**
     * Setter for the property 'document'.
     */
    public void setDocument(Document document) {
        _document = document;
    }
            
    /**
     * Returns the property 'document'.
     */
    public Document getDocument() {
        return _document;
    }
                                            
    /**
     * Setter for the property 'address'.
     */
    public void setAddress(Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     */
    public Address getAddress() {
        return _address;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentAddressAssignment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(DocumentAddressAssignment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
