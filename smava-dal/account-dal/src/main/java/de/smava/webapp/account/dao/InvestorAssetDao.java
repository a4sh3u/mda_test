//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(investor asset)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.account.domain.InvestorAsset;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'InvestorAssets'.
 *
 * @author generator
 */
public interface InvestorAssetDao extends BaseDao<InvestorAsset> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the investor asset identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    InvestorAsset getInvestorAsset(Long id);

    /**
     * Saves the investor asset.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveInvestorAsset(InvestorAsset investorAsset);

    /**
     * Deletes an investor asset, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the investor asset
     */
    void deleteInvestorAsset(Long id);

    /**
     * Retrieves all 'InvestorAsset' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<InvestorAsset> getInvestorAssetList();

    /**
     * Retrieves a page of 'InvestorAsset' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<InvestorAsset> getInvestorAssetList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'InvestorAsset' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<InvestorAsset> getInvestorAssetList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'InvestorAsset' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<InvestorAsset> getInvestorAssetList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'InvestorAsset' instances.
     */
    long getInvestorAssetCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(investor asset)}
    //
    // insert custom methods here

    /**
     * fetch active investor asset information
     *
     * @param lenderAccountId investor account id
     * @return collection of {@link InvestorAsset}
     */
    InvestorAsset getActiveAssetByAccountId(final Long lenderAccountId);

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
