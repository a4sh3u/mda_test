package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.MatchSortingRunHistory;

import java.util.Date;
import java.util.List;

/**
 * The domain object that represents 'MatchSortingRuns'.
 */
public class MatchSortingRun extends MatchSortingRunHistory  {

        protected Date _creationDate;
        protected String _marketName;
        protected List<MatchSorting> _matchSortings;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'market name'.
     */
    public void setMarketName(String marketName) {
        if (!_marketNameIsSet) {
            _marketNameIsSet = true;
            _marketNameInitVal = getMarketName();
        }
        registerChange("market name", _marketNameInitVal, marketName);
        _marketName = marketName;
    }
                        
    /**
     * Returns the property 'market name'.
     */
    public String getMarketName() {
        return _marketName;
    }
                                            
    /**
     * Setter for the property 'match sortings'.
     */
    public void setMatchSortings(List<MatchSorting> matchSortings) {
        _matchSortings = matchSortings;
    }
            
    /**
     * Returns the property 'match sortings'.
     */
    public List<MatchSorting> getMatchSortings() {
        return _matchSortings;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MatchSortingRun.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _marketName=").append(_marketName);
            builder.append("\n}");
        } else {
            builder.append(MatchSortingRun.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
