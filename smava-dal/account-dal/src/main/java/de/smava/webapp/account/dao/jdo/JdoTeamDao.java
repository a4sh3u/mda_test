package de.smava.webapp.account.dao.jdo;

import de.smava.webapp.account.dao.TeamDao;
import de.smava.webapp.account.domain.Team;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collection;

/**
 * DAO implementation for the domain object 'Teams'.
 */
@Repository
public class JdoTeamDao extends JdoGenericDao<Team> implements TeamDao {

    private static final Logger LOGGER = Logger.getLogger(JdoTeamDao.class);

    private static final String CLASS_NAME = "Team";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    /**
     * Returns an attached copy of the team identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Team load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        Team result = getPersistenceManager().getObjectById(Team.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        return getPersistenceManager().getObjectById(Team.class, id) != null;
    }

    /**
     * Saves the team.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Team team) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveTeam: " + team);
        }
        return saveEntity(team);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Team> getActiveTeamList() {
        Query query = getPersistenceManager().newQuery(Team.class);
        query.setFilter("this._expirationDate == null");
        return (Collection<Team>) query.execute();
    }

}
