package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.interfaces.BidSearchAgeGroupEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'BidSearchAgeGroups'.
 */
public class BidSearchAgeGroup extends KreditPrivatEntity
        implements BidSearchAgeGroupEntityInterface {

    private static final Logger LOGGER = Logger.getLogger(BidSearchAgeGroup.class);
	private static final long serialVersionUID = 4210595597602002149L;

    public BidSearchAgeGroup(BidSearch bidSearch, AgeGroup ageGroup) {
    	_bidSearch = bidSearch;
    	_ageGroup = ageGroup;
    }

            protected BidSearch _bidSearch;
            protected AgeGroup _ageGroup;
        
                        /**
            * Setter for the property 'bid search'.
            */
            public void setBidSearch(BidSearch bidSearch) {
            _bidSearch = bidSearch;
            }

            /**
            * Returns the property 'bid search'.
            */
            public BidSearch getBidSearch() {
            return _bidSearch;
            }
                                /**
            * Setter for the property 'age group'.
            */
            public void setAgeGroup(AgeGroup ageGroup) {
            _ageGroup = ageGroup;
            }

            /**
            * Returns the property 'age group'.
            */
            public AgeGroup getAgeGroup() {
            return _ageGroup;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(BidSearchAgeGroup.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(BidSearchAgeGroup.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

}
