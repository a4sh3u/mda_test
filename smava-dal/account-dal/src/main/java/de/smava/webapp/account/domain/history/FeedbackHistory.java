package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractFeedback;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Feedbacks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class FeedbackHistory extends AbstractFeedback {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _releaseApprovalDateInitVal;
    protected transient boolean _releaseApprovalDateIsSet;
    protected transient String _textInitVal;
    protected transient boolean _textIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'release approval date'.
     */
    public Date releaseApprovalDateInitVal() {
        Date result;
        if (_releaseApprovalDateIsSet) {
            result = _releaseApprovalDateInitVal;
        } else {
            result = getReleaseApprovalDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'release approval date'.
     */
    public boolean releaseApprovalDateIsDirty() {
        return !valuesAreEqual(releaseApprovalDateInitVal(), getReleaseApprovalDate());
    }

    /**
     * Returns true if the setter method was called for the property 'release approval date'.
     */
    public boolean releaseApprovalDateIsSet() {
        return _releaseApprovalDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'text'.
     */
    public String textInitVal() {
        String result;
        if (_textIsSet) {
            result = _textInitVal;
        } else {
            result = getText();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'text'.
     */
    public boolean textIsDirty() {
        return !valuesAreEqual(textInitVal(), getText());
    }

    /**
     * Returns true if the setter method was called for the property 'text'.
     */
    public boolean textIsSet() {
        return _textIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
		
}
