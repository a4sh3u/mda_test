package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractCreditScore;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CreditScores'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CreditScoreHistory extends AbstractCreditScore {

    protected transient String _ratingInitVal;
    protected transient boolean _ratingIsSet;
    protected transient Date _changeDateInitVal;
    protected transient boolean _changeDateIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;


		
    /**
     * Returns the initial value of the property 'rating'.
     */
    public String ratingInitVal() {
        String result;
        if (_ratingIsSet) {
            result = _ratingInitVal;
        } else {
            result = getRating();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rating'.
     */
    public boolean ratingIsDirty() {
        return !valuesAreEqual(ratingInitVal(), getRating());
    }

    /**
     * Returns true if the setter method was called for the property 'rating'.
     */
    public boolean ratingIsSet() {
        return _ratingIsSet;
    }
				
    /**
     * Returns the initial value of the property 'change date'.
     */
    public Date changeDateInitVal() {
        Date result;
        if (_changeDateIsSet) {
            result = _changeDateInitVal;
        } else {
            result = getChangeDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'change date'.
     */
    public boolean changeDateIsDirty() {
        return !valuesAreEqual(changeDateInitVal(), getChangeDate());
    }

    /**
     * Returns true if the setter method was called for the property 'change date'.
     */
    public boolean changeDateIsSet() {
        return _changeDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }

}
