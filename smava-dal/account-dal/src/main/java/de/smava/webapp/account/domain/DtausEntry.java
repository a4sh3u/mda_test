package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DtausEntryHistory;

import java.util.Date;
import java.util.Set;

/**
 * The domain object that represents 'DtausEntrys'.
 */
public class DtausEntry extends DtausEntryHistory  {

        protected Date _creationDate;
        protected Date _transactionDate;
        protected String _state;
        protected String _type;
        protected String _key;
        protected String _returnReason;
        protected String _creditAccountName;
        protected String _creditAccountBankCode;
        protected String _creditAccountBank;
        protected String _creditAccountNumber;
        protected String _creditAccountIban;
        protected String _creditAccountBic;
        protected String _debitAccountName;
        protected String _debitAccountBankCode;
        protected String _debitAccountBank;
        protected String _debitAccountNumber;
        protected String _debitAccountIban;
        protected String _debitAccountBic;
        protected Date _debitNoteDate;
        protected String _mandateId;
        protected String _eref;
        protected String _description;
        protected double _amount;
        protected String _relatedTransactionIdList;
        protected DtausFile _dtausFile;
        protected int _dtausIndex;
        protected int _dtausTransactionIndex;
        protected Set<DtausEntryTransaction> _dtausEntryTransactions;
        protected String _errorCode;
        protected String _externalTransactionId;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'transaction date'.
     */
    public void setTransactionDate(Date transactionDate) {
        if (!_transactionDateIsSet) {
            _transactionDateIsSet = true;
            _transactionDateInitVal = getTransactionDate();
        }
        registerChange("transaction date", _transactionDateInitVal, transactionDate);
        _transactionDate = transactionDate;
    }
                        
    /**
     * Returns the property 'transaction date'.
     */
    public Date getTransactionDate() {
        return _transactionDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'key'.
     */
    public void setKey(String key) {
        if (!_keyIsSet) {
            _keyIsSet = true;
            _keyInitVal = getKey();
        }
        registerChange("key", _keyInitVal, key);
        _key = key;
    }
                        
    /**
     * Returns the property 'key'.
     */
    public String getKey() {
        return _key;
    }
                                    /**
     * Setter for the property 'return reason'.
     */
    public void setReturnReason(String returnReason) {
        if (!_returnReasonIsSet) {
            _returnReasonIsSet = true;
            _returnReasonInitVal = getReturnReason();
        }
        registerChange("return reason", _returnReasonInitVal, returnReason);
        _returnReason = returnReason;
    }
                        
    /**
     * Returns the property 'return reason'.
     */
    public String getReturnReason() {
        return _returnReason;
    }
                                    /**
     * Setter for the property 'credit account name'.
     */
    public void setCreditAccountName(String creditAccountName) {
        if (!_creditAccountNameIsSet) {
            _creditAccountNameIsSet = true;
            _creditAccountNameInitVal = getCreditAccountName();
        }
        registerChange("credit account name", _creditAccountNameInitVal, creditAccountName);
        _creditAccountName = creditAccountName;
    }
                        
    /**
     * Returns the property 'credit account name'.
     */
    public String getCreditAccountName() {
        return _creditAccountName;
    }
                                    /**
     * Setter for the property 'credit account bank code'.
     */
    public void setCreditAccountBankCode(String creditAccountBankCode) {
        if (!_creditAccountBankCodeIsSet) {
            _creditAccountBankCodeIsSet = true;
            _creditAccountBankCodeInitVal = getCreditAccountBankCode();
        }
        registerChange("credit account bank code", _creditAccountBankCodeInitVal, creditAccountBankCode);
        _creditAccountBankCode = creditAccountBankCode;
    }
                        
    /**
     * Returns the property 'credit account bank code'.
     */
    public String getCreditAccountBankCode() {
        return _creditAccountBankCode;
    }
                                    /**
     * Setter for the property 'credit account bank'.
     */
    public void setCreditAccountBank(String creditAccountBank) {
        if (!_creditAccountBankIsSet) {
            _creditAccountBankIsSet = true;
            _creditAccountBankInitVal = getCreditAccountBank();
        }
        registerChange("credit account bank", _creditAccountBankInitVal, creditAccountBank);
        _creditAccountBank = creditAccountBank;
    }
                        
    /**
     * Returns the property 'credit account bank'.
     */
    public String getCreditAccountBank() {
        return _creditAccountBank;
    }
                                    /**
     * Setter for the property 'credit account number'.
     */
    public void setCreditAccountNumber(String creditAccountNumber) {
        if (!_creditAccountNumberIsSet) {
            _creditAccountNumberIsSet = true;
            _creditAccountNumberInitVal = getCreditAccountNumber();
        }
        registerChange("credit account number", _creditAccountNumberInitVal, creditAccountNumber);
        _creditAccountNumber = creditAccountNumber;
    }
                        
    /**
     * Returns the property 'credit account number'.
     */
    public String getCreditAccountNumber() {
        return _creditAccountNumber;
    }
                                    /**
     * Setter for the property 'credit account iban'.
     */
    public void setCreditAccountIban(String creditAccountIban) {
        if (!_creditAccountIbanIsSet) {
            _creditAccountIbanIsSet = true;
            _creditAccountIbanInitVal = getCreditAccountIban();
        }
        registerChange("credit account iban", _creditAccountIbanInitVal, creditAccountIban);
        _creditAccountIban = creditAccountIban;
    }
                        
    /**
     * Returns the property 'credit account iban'.
     */
    public String getCreditAccountIban() {
        return _creditAccountIban;
    }
                                    /**
     * Setter for the property 'credit account bic'.
     */
    public void setCreditAccountBic(String creditAccountBic) {
        if (!_creditAccountBicIsSet) {
            _creditAccountBicIsSet = true;
            _creditAccountBicInitVal = getCreditAccountBic();
        }
        registerChange("credit account bic", _creditAccountBicInitVal, creditAccountBic);
        _creditAccountBic = creditAccountBic;
    }
                        
    /**
     * Returns the property 'credit account bic'.
     */
    public String getCreditAccountBic() {
        return _creditAccountBic;
    }
                                    /**
     * Setter for the property 'debit account name'.
     */
    public void setDebitAccountName(String debitAccountName) {
        if (!_debitAccountNameIsSet) {
            _debitAccountNameIsSet = true;
            _debitAccountNameInitVal = getDebitAccountName();
        }
        registerChange("debit account name", _debitAccountNameInitVal, debitAccountName);
        _debitAccountName = debitAccountName;
    }
                        
    /**
     * Returns the property 'debit account name'.
     */
    public String getDebitAccountName() {
        return _debitAccountName;
    }
                                    /**
     * Setter for the property 'debit account bank code'.
     */
    public void setDebitAccountBankCode(String debitAccountBankCode) {
        if (!_debitAccountBankCodeIsSet) {
            _debitAccountBankCodeIsSet = true;
            _debitAccountBankCodeInitVal = getDebitAccountBankCode();
        }
        registerChange("debit account bank code", _debitAccountBankCodeInitVal, debitAccountBankCode);
        _debitAccountBankCode = debitAccountBankCode;
    }
                        
    /**
     * Returns the property 'debit account bank code'.
     */
    public String getDebitAccountBankCode() {
        return _debitAccountBankCode;
    }
                                    /**
     * Setter for the property 'debit account bank'.
     */
    public void setDebitAccountBank(String debitAccountBank) {
        if (!_debitAccountBankIsSet) {
            _debitAccountBankIsSet = true;
            _debitAccountBankInitVal = getDebitAccountBank();
        }
        registerChange("debit account bank", _debitAccountBankInitVal, debitAccountBank);
        _debitAccountBank = debitAccountBank;
    }
                        
    /**
     * Returns the property 'debit account bank'.
     */
    public String getDebitAccountBank() {
        return _debitAccountBank;
    }
                                    /**
     * Setter for the property 'debit account number'.
     */
    public void setDebitAccountNumber(String debitAccountNumber) {
        if (!_debitAccountNumberIsSet) {
            _debitAccountNumberIsSet = true;
            _debitAccountNumberInitVal = getDebitAccountNumber();
        }
        registerChange("debit account number", _debitAccountNumberInitVal, debitAccountNumber);
        _debitAccountNumber = debitAccountNumber;
    }
                        
    /**
     * Returns the property 'debit account number'.
     */
    public String getDebitAccountNumber() {
        return _debitAccountNumber;
    }
                                    /**
     * Setter for the property 'debit account iban'.
     */
    public void setDebitAccountIban(String debitAccountIban) {
        if (!_debitAccountIbanIsSet) {
            _debitAccountIbanIsSet = true;
            _debitAccountIbanInitVal = getDebitAccountIban();
        }
        registerChange("debit account iban", _debitAccountIbanInitVal, debitAccountIban);
        _debitAccountIban = debitAccountIban;
    }
                        
    /**
     * Returns the property 'debit account iban'.
     */
    public String getDebitAccountIban() {
        return _debitAccountIban;
    }
                                    /**
     * Setter for the property 'debit account bic'.
     */
    public void setDebitAccountBic(String debitAccountBic) {
        if (!_debitAccountBicIsSet) {
            _debitAccountBicIsSet = true;
            _debitAccountBicInitVal = getDebitAccountBic();
        }
        registerChange("debit account bic", _debitAccountBicInitVal, debitAccountBic);
        _debitAccountBic = debitAccountBic;
    }
                        
    /**
     * Returns the property 'debit account bic'.
     */
    public String getDebitAccountBic() {
        return _debitAccountBic;
    }
                                    /**
     * Setter for the property 'debit note date'.
     */
    public void setDebitNoteDate(Date debitNoteDate) {
        if (!_debitNoteDateIsSet) {
            _debitNoteDateIsSet = true;
            _debitNoteDateInitVal = getDebitNoteDate();
        }
        registerChange("debit note date", _debitNoteDateInitVal, debitNoteDate);
        _debitNoteDate = debitNoteDate;
    }
                        
    /**
     * Returns the property 'debit note date'.
     */
    public Date getDebitNoteDate() {
        return _debitNoteDate;
    }
                                    /**
     * Setter for the property 'mandate id'.
     */
    public void setMandateId(String mandateId) {
        if (!_mandateIdIsSet) {
            _mandateIdIsSet = true;
            _mandateIdInitVal = getMandateId();
        }
        registerChange("mandate id", _mandateIdInitVal, mandateId);
        _mandateId = mandateId;
    }
                        
    /**
     * Returns the property 'mandate id'.
     */
    public String getMandateId() {
        return _mandateId;
    }
                                    /**
     * Setter for the property 'eref'.
     */
    public void setEref(String eref) {
        if (!_erefIsSet) {
            _erefIsSet = true;
            _erefInitVal = getEref();
        }
        registerChange("eref", _erefInitVal, eref);
        _eref = eref;
    }
                        
    /**
     * Returns the property 'eref'.
     */
    public String getEref() {
        return _eref;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     */
    public double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'related transaction id list'.
     */
    public void setRelatedTransactionIdList(String relatedTransactionIdList) {
        if (!_relatedTransactionIdListIsSet) {
            _relatedTransactionIdListIsSet = true;
            _relatedTransactionIdListInitVal = getRelatedTransactionIdList();
        }
        registerChange("related transaction id list", _relatedTransactionIdListInitVal, relatedTransactionIdList);
        _relatedTransactionIdList = relatedTransactionIdList;
    }
                        
    /**
     * Returns the property 'related transaction id list'.
     */
    public String getRelatedTransactionIdList() {
        return _relatedTransactionIdList;
    }
                                            
    /**
     * Setter for the property 'dtaus file'.
     */
    public void setDtausFile(DtausFile dtausFile) {
        _dtausFile = dtausFile;
    }
            
    /**
     * Returns the property 'dtaus file'.
     */
    public DtausFile getDtausFile() {
        return _dtausFile;
    }
                                    /**
     * Setter for the property 'dtaus index'.
     */
    public void setDtausIndex(int dtausIndex) {
        if (!_dtausIndexIsSet) {
            _dtausIndexIsSet = true;
            _dtausIndexInitVal = getDtausIndex();
        }
        registerChange("dtaus index", _dtausIndexInitVal, dtausIndex);
        _dtausIndex = dtausIndex;
    }
                        
    /**
     * Returns the property 'dtaus index'.
     */
    public int getDtausIndex() {
        return _dtausIndex;
    }
                                    /**
     * Setter for the property 'dtaus transaction index'.
     */
    public void setDtausTransactionIndex(int dtausTransactionIndex) {
        if (!_dtausTransactionIndexIsSet) {
            _dtausTransactionIndexIsSet = true;
            _dtausTransactionIndexInitVal = getDtausTransactionIndex();
        }
        registerChange("dtaus transaction index", _dtausTransactionIndexInitVal, dtausTransactionIndex);
        _dtausTransactionIndex = dtausTransactionIndex;
    }
                        
    /**
     * Returns the property 'dtaus transaction index'.
     */
    public int getDtausTransactionIndex() {
        return _dtausTransactionIndex;
    }
                                            
    /**
     * Setter for the property 'dtaus entry transactions'.
     */
    public void setDtausEntryTransactions(Set<DtausEntryTransaction> dtausEntryTransactions) {
        _dtausEntryTransactions = dtausEntryTransactions;
    }
            
    /**
     * Returns the property 'dtaus entry transactions'.
     */
    public Set<DtausEntryTransaction> getDtausEntryTransactions() {
        return _dtausEntryTransactions;
    }
                                    /**
     * Setter for the property 'error code'.
     */
    public void setErrorCode(String errorCode) {
        if (!_errorCodeIsSet) {
            _errorCodeIsSet = true;
            _errorCodeInitVal = getErrorCode();
        }
        registerChange("error code", _errorCodeInitVal, errorCode);
        _errorCode = errorCode;
    }
                        
    /**
     * Returns the property 'error code'.
     */
    public String getErrorCode() {
        return _errorCode;
    }
                                    /**
     * Setter for the property 'external transaction id'.
     */
    public void setExternalTransactionId(String externalTransactionId) {
        if (!_externalTransactionIdIsSet) {
            _externalTransactionIdIsSet = true;
            _externalTransactionIdInitVal = getExternalTransactionId();
        }
        registerChange("external transaction id", _externalTransactionIdInitVal, externalTransactionId);
        _externalTransactionId = externalTransactionId;
    }
                        
    /**
     * Returns the property 'external transaction id'.
     */
    public String getExternalTransactionId() {
        return _externalTransactionId;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DtausEntry.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _key=").append(_key);
            builder.append("\n    _returnReason=").append(_returnReason);
            builder.append("\n    _creditAccountName=").append(_creditAccountName);
            builder.append("\n    _creditAccountBankCode=").append(_creditAccountBankCode);
            builder.append("\n    _creditAccountBank=").append(_creditAccountBank);
            builder.append("\n    _creditAccountNumber=").append(_creditAccountNumber);
            builder.append("\n    _creditAccountIban=").append(_creditAccountIban);
            builder.append("\n    _creditAccountBic=").append(_creditAccountBic);
            builder.append("\n    _debitAccountName=").append(_debitAccountName);
            builder.append("\n    _debitAccountBankCode=").append(_debitAccountBankCode);
            builder.append("\n    _debitAccountBank=").append(_debitAccountBank);
            builder.append("\n    _debitAccountNumber=").append(_debitAccountNumber);
            builder.append("\n    _debitAccountIban=").append(_debitAccountIban);
            builder.append("\n    _debitAccountBic=").append(_debitAccountBic);
            builder.append("\n    _mandateId=").append(_mandateId);
            builder.append("\n    _eref=").append(_eref);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _relatedTransactionIdList=").append(_relatedTransactionIdList);
            builder.append("\n    _dtausIndex=").append(_dtausIndex);
            builder.append("\n    _dtausTransactionIndex=").append(_dtausTransactionIndex);
            builder.append("\n    _errorCode=").append(_errorCode);
            builder.append("\n    _externalTransactionId=").append(_externalTransactionId);
            builder.append("\n}");
        } else {
            builder.append(DtausEntry.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public DtausEntry instance() {
        return this;
    }
}
