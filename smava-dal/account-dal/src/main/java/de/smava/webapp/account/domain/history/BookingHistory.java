package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBooking;




/**
 * The domain object that has all history aggregation related fields for 'Bookings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BookingHistory extends AbstractBooking {

    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;


		
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
			
}
