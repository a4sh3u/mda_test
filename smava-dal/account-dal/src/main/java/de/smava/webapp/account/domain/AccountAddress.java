package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.AccountAddressHistory;

/**
 * The domain object that represents 'AccountAddresss'.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@addressId")
public class AccountAddress extends AccountAddressHistory  {

    public AccountAddress() {
        super();
    }

    public AccountAddress(Address address) {
        setType(address.getType());
        setStreet(address.getStreet());
        setStreetNumber(address.getStreetNumber());
        setZipCode(address.getZipCode());
        setCity(address.getCity());
        setCountry(address.getCountry());
        setLivingThereSince(address.getLivingThereSince());
    }

    public AccountAddress(Account account, String type, String street, String streetNumber, String zipCode, String city, String country) {
        super();
        setType(type);
        setStreet(street);
        setStreetNumber(streetNumber);
        setZipCode(zipCode);
        setCity(city);
        setCountry(country);
        _account = account;
    }

            protected Account _account;
        
                        /**
            * Setter for the property 'account'.
            */
            public void setAccount(Account account) {
            _account = account;
            }

            /**
            * Returns the property 'account'.
            */
            public Account getAccount() {
            return _account;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(AccountAddress.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(AccountAddress.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

}
