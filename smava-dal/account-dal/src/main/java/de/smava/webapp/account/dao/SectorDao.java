//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(sector)}

import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Sectors'.
 *
 * @author generator
 */
public interface SectorDao extends BaseDao<Sector>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the sector identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Sector getSector(Long id);

    /**
     * Saves the sector.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveSector(Sector sector);

    /**
     * Deletes an sector, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the sector
     */
    void deleteSector(Long id);

    /**
     * Retrieves all 'Sector' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Sector> getSectorList();

    /**
     * Retrieves a page of 'Sector' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Sector> getSectorList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Sector' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Sector> getSectorList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Sector' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Sector> getSectorList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'Sector' instances.
     */
    long getSectorCount();


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(sector)}
    //
    
    
    /**
     * Load all root Sectors with preloaded children down to deep 3.  
     * 
     * @return root sectors
     */
    Collection<Sector> getRootSectorsDeep();
    
    
    /**
     * Load all employee Sectors with preloaded children down to deep 3.  
     * 
     * @return employee sectors
     */
    Collection<Sector> getEmployeeSectorsDeep();

    /**
     * Load all official Sectors.
     *
     * @return official sectors
     */
    Collection<Sector> getOfficialSectors();
    
    
    Set<Long> getSectorIdsNeededDescription();
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
