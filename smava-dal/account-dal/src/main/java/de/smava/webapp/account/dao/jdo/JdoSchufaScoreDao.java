//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score)}

import de.smava.webapp.account.dao.SchufaScoreDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.SchufaScore;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'SchufaScores'.
 *
 * @author generator
 */
@Repository(value = "schufaScoreDao")
public class JdoSchufaScoreDao extends JdoBaseDao implements SchufaScoreDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSchufaScoreDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(schufa score)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the schufa score identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public SchufaScore load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScore - start: id=" + id);
        }
        SchufaScore result = getEntity(SchufaScore.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScore - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public SchufaScore getSchufaScore(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            SchufaScore entity = findUniqueEntity(SchufaScore.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the schufa score.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(SchufaScore schufaScore) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveSchufaScore: " + schufaScore);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(schufa score)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(schufaScore);
    }

    /**
     * @deprecated Use {@link #save(SchufaScore) instead}
     */
    public Long saveSchufaScore(SchufaScore schufaScore) {
        return save(schufaScore);
    }

    /**
     * Deletes an schufa score, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa score
     */
    public void deleteSchufaScore(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteSchufaScore: " + id);
        }
        deleteEntity(SchufaScore.class, id);
    }

    /**
     * Retrieves all 'SchufaScore' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<SchufaScore> getSchufaScoreList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - start");
        }
        Collection<SchufaScore> result = getEntities(SchufaScore.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'SchufaScore' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<SchufaScore> getSchufaScoreList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - start: pageable=" + pageable);
        }
        Collection<SchufaScore> result = getEntities(SchufaScore.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScore' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<SchufaScore> getSchufaScoreList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - start: sortable=" + sortable);
        }
        Collection<SchufaScore> result = getEntities(SchufaScore.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScore' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<SchufaScore> getSchufaScoreList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScore> result = getEntities(SchufaScore.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScore' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScore> findSchufaScoreList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - start: whereClause=" + whereClause);
        }
        Collection<SchufaScore> result = findEntities(SchufaScore.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScore' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<SchufaScore> findSchufaScoreList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<SchufaScore> result = findEntities(SchufaScore.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'SchufaScore' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScore> findSchufaScoreList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<SchufaScore> result = findEntities(SchufaScore.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScore' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScore> findSchufaScoreList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<SchufaScore> result = findEntities(SchufaScore.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScore' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScore> findSchufaScoreList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScore> result = findEntities(SchufaScore.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScore' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScore> findSchufaScoreList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScore> result = findEntities(SchufaScore.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScore' instances.
     */
    public long getSchufaScoreCount() {
        long result = getEntityCount(SchufaScore.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScore' instances which match the given whereClause.
     */
    public long getSchufaScoreCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(SchufaScore.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScore' instances which match the given whereClause together with params specified in object array.
     */
    public long getSchufaScoreCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(SchufaScore.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa score)}
    //
    // insert custom methods here
    //

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<SchufaScore> loadScoresWithStates(List<String> scoreStates) {
        Query q = getPersistenceManager().newNamedQuery(SchufaScore.class, "scoresWithStates");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("scoreStates", scoreStates);

        @SuppressWarnings("unchecked")
        Collection<SchufaScore> result = (Collection<SchufaScore>) q.executeWithMap(params);

        LOGGER.debug("result:" + result);

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public SchufaScore getFirstScoreOfUser(Account account) {
        
        SchufaScore result = null;
        
        Query q = getPersistenceManager().newNamedQuery(SchufaScore.class, "firstScoreOfUser");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("accountId", account.getId());

        @SuppressWarnings("unchecked")
        Collection<SchufaScore> scores = (Collection<SchufaScore>) q.executeWithMap(params);

        Iterator<SchufaScore> i = scores.iterator();
        if (i.hasNext()) {
            result = i.next();
        }
        
        return result;
    }

    @Override
    public SchufaScore findSchufaScoreForRequest(Long requestId) {
        SchufaScore result = null;
        if (requestId != null) {
            Collection<SchufaScore> possibleScores = findSchufaScoreList("_request._id == " + requestId);
            if (possibleScores != null) {
                if (possibleScores.size() == 1) {
                    result = possibleScores.iterator().next();
                } else if (possibleScores.size() > 1) {
                    LOGGER.error(possibleScores.size() + " scores found for request " + requestId);
                }
            }
        }
        return result;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
