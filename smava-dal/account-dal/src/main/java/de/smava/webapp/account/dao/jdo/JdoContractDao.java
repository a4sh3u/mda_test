//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//

//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(contract)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.Bid;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.BookingType;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.Market;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.domain.Transaction;
import de.smava.webapp.account.todo.contract.dao.ContractDao;
import de.smava.webapp.account.todo.contract.dao.ContractInterestRange;
import de.smava.webapp.account.todo.contract.dao.ContractRoiResultEntry;
import de.smava.webapp.account.todo.contract.dao.ContractStateNumbers;
import de.smava.webapp.account.todo.dao.VolumeCount;
import de.smava.webapp.account.util.BookingGroupComparator;
import de.smava.webapp.commons.constants.SqlConstants;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.jdo.Query;
import javax.jdo.datastore.Sequence;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Contracts'.
 *
 * @author generator
 */
@Repository(value = "contractDao")
public class JdoContractDao extends JdoBaseDao implements ContractDao {

    private static final String CONTRACT_END_DATE_SQL = "(date_trunc('month',c.start_date) + cast(substr(c.market_name,2,2) as integer)* interval '1 month' + interval '1 month' - interval '1 day') as end_date ";

    private static final String CONTRACT_END_DATE = "select "
                + CONTRACT_END_DATE_SQL
                + "from contract c where id = :contractId";

    private static final String COMPLETED_CONTRACTS_SQL = "SELECT id from "
                + "(SELECT c.id as id, sum(case t.state when :transactionState then 1 else 0 end) as t_open, "
                + CONTRACT_END_DATE_SQL
                + "FROM contract c, transaction t, booking b "
                + "WHERE b.contract_id = c.id and b.transaction_id = t.id and c.state in (:lenderRunningContractStates) "
                + "and b.type_new in (:bookingTypes) group by c.id, c.start_date, c.market_name) as data "
                + "where t_open = 0 group by id ";

    private static final String DEFAULTED_COMPLETED_CONTRACTS_SQL = "SELECT id from "
            + "(SELECT c.id as id, sum(case t.state when :transactionState then 1 else 0 end) as t_open, "
            + CONTRACT_END_DATE_SQL
            + "FROM contract c, transaction t, booking b "
            + "WHERE b.contract_id = c.id and b.transaction_id = t.id and c.state in (:defaultedContractStates) "
            + "and b.type_new in (:bookingTypes) group by c.id, c.start_date, c.market_name) as data "
            + "where t_open = 0 and end_date < :currentDate  and end_date > :limitDate group by id ";


    private static final Logger LOGGER = Logger.getLogger(JdoContractDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(contract)}
    private static final long serialVersionUID = -8654544282682658565L;
        //
        
    private static final String CP_COUNT_SQL
        = "select count(group_id) from (\n"
        + "select distinct b.\"groupId\" as group_id from bid b\n"
        + "inner join contract c on c.order_id = b.id\n"
        + "inner join \"order\" o on o.id = b.id\n"
        + "where c.state in (:contractStates) and o.account_id = :accountId\n"
        + ") as tmp";

    private static final String CP_COUNT_WITH_REMINDER_SQL
        = "select count(group_id) from (\n"
            + "select distinct b.\"groupId\" as group_id from bid b\n"
            + "inner join contract c on c.order_id = b.id\n"
            + "inner join \"order\" o on o.id = b.id\n"
            + "where c.state in (:contractStates) and o.account_id = :accountId\n"
            + "and exists (\n"
            + "    select b.id from bank_account ba\n"
            + "    inner join transaction t on t.credit_account = ba.id\n"
            + "    inner join dunning_state ds on ds.transaction_id = t.id\n"
            + "    inner join booking b on b.transaction_id = t.id\n"
            + "    where ba.account_id = :accountId and t.type = 'TRANSFER' and t.state = 'OPEN' and ds.level >= 0 and b.contract_id = c.id)"
            + ") as tmp";

    private static final String STATE = "_state";
    private static final String OFFER_ACCOUNT = "_offer._account";

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the contract identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Contract load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContract - start: id=" + id);
        }
        Contract result = getEntity(Contract.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContract - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Contract getContract(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Contract entity = findUniqueEntity(Contract.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the contract.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Contract contract) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveContract: " + contract);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(contract)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(contract);
    }

    /**
     * @deprecated Use {@link #save(Contract) instead}
     */
    public Long saveContract(Contract contract) {
        return save(contract);
    }

    /**
     * Deletes an contract, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the contract
     */
    public void deleteContract(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteContract: " + id);
        }
        deleteEntity(Contract.class, id);
    }

    /**
     * Retrieves all 'Contract' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Contract> getContractList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - start");
        }
        Collection<Contract> result = getEntities(Contract.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Contract' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Contract> getContractList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - start: pageable=" + pageable);
        }
        Collection<Contract> result = getEntities(Contract.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Contract' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Contract> getContractList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - start: sortable=" + sortable);
        }
        Collection<Contract> result = getEntities(Contract.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Contract' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Contract> getContractList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Contract> result = getEntities(Contract.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Contract' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Contract> findContractList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - start: whereClause=" + whereClause);
        }
        Collection<Contract> result = findEntities(Contract.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Contract' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Contract> findContractList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Contract> result = findEntities(Contract.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Contract' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Contract> findContractList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Contract> result = findEntities(Contract.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Contract' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Contract> findContractList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Contract> result = findEntities(Contract.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Contract' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Contract> findContractList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Contract> result = findEntities(Contract.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Contract' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Contract> findContractList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Contract> result = findEntities(Contract.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Contract' instances.
     */
    public long getContractCount() {
        long result = getEntityCount(Contract.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Contract' instances which match the given whereClause.
     */
    public long getContractCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Contract.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Contract' instances which match the given whereClause together with params specified in object array.
     */
    public long getContractCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Contract.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getContractCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(contract)}


    /**
     * {@inheritDoc}
     */
    @Override
    public ContractInterestRange getAverageRateAndMinMax(String whereClause, double base, Object ... params) {
        final StringBuilder sql = new StringBuilder();
        sql.append("select new " + ContractInterestRange.class.getName() + "(min(_rate) as minRate, sum(_rate * (_amount / ");
        sql.append(base);
        sql.append(")) / sum(_amount / ");
        sql.append(base);
        sql.append(") as avg, max(_rate) as maxRate) from ");
        sql.append(Contract.class.getName());
        if (StringUtils.isNotBlank(whereClause)) {
            sql.append(" where ");
            sql.append(whereClause);
        }
        final Query q = getPersistenceManager().newQuery("javax.jdo.query.JDOQL", sql.toString());
        q.setUnique(true);

        final ContractInterestRange result;
        if (params == null || params.length == 0) {
            result = (ContractInterestRange) q.execute();
        } else {
            result = (ContractInterestRange) q.executeWithArray(params);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getContractNumber() {
        Sequence seq = getPersistenceManager().getSequence("de.smava.webapp.account.domain.CONTRACT_NUMBER");
        return (Long) seq.next();
    }

    /**
     * @param market: if market == null -> all markets
     * @param contractStates: contract states to include into query
     */
    /**
     * {@inheritDoc}
     */
    @Override
    public VolumeCount getRunningContractAmountSum(Market market, Collection<String> contractStates) {
        return getContractAmountSum(market, contractStates, true);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public VolumeCount getOverallContractAmountSum(Market market, Collection<String> contractStates) {
        return getContractAmountSum(market, contractStates, false);
    }
    
    
    private VolumeCount getContractAmountSum(Market market, Collection<String> contractStates, final boolean respectDurationAge) {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ");
        sql.append(" SUM(\"this\".\"amount\") as \"volume\",");
        sql.append(" COUNT(\"this\".\"id\") as \"offerCount\" ");
        sql.append("FROM \"contract\" \"this\" "); 
        sql.append(" WHERE (");
        
        String delim = "";
        for (String state : contractStates) {
            sql.append(delim);
            sql.append("'").append(state).append("' = \"this\".\"state\"");
            delim = " OR ";
        }
        sql.append(") ");
        if (market != null) {
            sql.append(" AND \"this\".\"market_name\" = ? ");
        }
        
        if (respectDurationAge) {
            //build interval between now and contracts startdate,
            //extract month part and year part (multiplied with 12 to be addable to months) from interval
            //and compare computed months with duration part of "market_name"
            sql.append("AND date_part('month',age(now(),\"this\".start_date)) + date_part('year',age(now(),\"this\".start_date))*12 < to_number(\"this\".market_name,'99V9')");
        }
        
        
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setResultClass(VolumeCount.class);
        q.setUnique(true);
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (market != null) {
            params.put(1, market.getMarketNameWithRatingAsLetter());
        }
        VolumeCount result = (VolumeCount) q.executeWithMap(params);
        //queryresult "offerCount" is mapped to field "orderCount"
        //dont know why, so just switch the values...
        result.setOfferCount(result.getOrderCount());
        result.setOrderCount(0L);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getRunningContractCount(Market market, Collection<String> contractStates) {
        StringBuffer sql = new StringBuffer();
        sql.append("SELECT ");
        sql.append(" case \"bid\".\"groupId\" WHEN 0 then \"bid\".id else \"bid\".\"groupId\" end as distinctid");
        sql.append(" FROM \"contract\" , \"bid\" "); 
        sql.append(" WHERE ");
        sql.append(" contract.order_id = \"bid\".id ");
        sql.append(" AND (");
        
        String delim = "";
        for (String state : contractStates) {
            sql.append(delim);
            sql.append("'").append(state).append("' = \"contract\".\"state\"");
            delim = " OR ";
        }
        sql.append(") AND \"contract\".\"market_name\" = ? ");

        //build interval between now and contracts startdate, 
        //extract month part and year part (multiplied with 12 to be addable to months) from interval
        //and compare computed months with duration part of "market_name"
        sql.append("AND date_part('month',age(now(),\"contract\".start_date)) + date_part('year',age(now(),\"contract\".start_date))*12 < to_number(\"contract\".market_name,'99V9')");
        sql.append("group by distinctid");
        
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, market.getMarketNameWithRatingAsLetter());
        Long result = Long.valueOf(((Collection) q.executeWithMap(params)).size());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getContractCount(Account lender, Collection<String> contractStates) {
        StringBuilder whereClause = new StringBuilder();
        
        whereClause.append("_offer._account == account && contractStates.contains(_state)");
        

        Query q = getPersistenceManager().newQuery(Contract.class, whereClause.toString());

        String parameterDeclaration = "java.util.List contractStates";
        parameterDeclaration += ", de.smava.webapp.account.domain.Account account";
        
        q.declareParameters(parameterDeclaration);
        q.setResult("count(_id)");
        
        Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("contractStates", contractStates);
        parameterMap.put("account", lender);
        
        Long result = (Long) q.executeWithMap(parameterMap);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedSet<BookingGroup> getBookingGroups(Contract contract, boolean considerOnlyBookingGroupWithPaymentRate) {
        return getBookingGroups(Collections.singleton(contract), considerOnlyBookingGroupWithPaymentRate);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedSet<BookingGroup> getBookingGroups(Collection<Contract> contracts, boolean considerOnlyBookingGroupWithPaymentRate) {
        Collection<String> bookingGroupStates = new ArrayList<String>();
        bookingGroupStates.add(BookingGroup.TYPE_INSURANCE_GROUP);
        bookingGroupStates.add(BookingGroup.TYPE_CONTRACT_CANCELLATION);
        return getBookingGroups(contracts, considerOnlyBookingGroupWithPaymentRate, bookingGroupStates);
     
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public SortedSet<BookingGroup> getBookingGroups(Contract contract,
                                                    boolean considerOnlyBookingGroupWithPaymentRate,
                                                    Collection<String> bookingGroupStates) {
        return getBookingGroups(Collections.singleton(contract), considerOnlyBookingGroupWithPaymentRate, bookingGroupStates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortedSet<BookingGroup> getBookingGroups(Collection<Contract> contracts, 
                                                    boolean considerOnlyBookingGroupWithPaymentRate,
                                                    Collection<String> bookingGroupStates) {
        StringBuilder queryString = new StringBuilder();
        queryString.append("select distinct(ba.booking_group) as id");
        queryString.append(" from booking_assignment ba inner join booking as b on ba.booking = b.id");
        if (considerOnlyBookingGroupWithPaymentRate) {
            queryString.append(" inner join insurancepool_payout_run as ipr on ipr.booking_group_id = ba.booking_group");
        }        
        queryString.append(" where b.contract_id in (");
        
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        int paramsindex = 1;
        
        String delim = "";
        for (Contract contract : contracts) {
            queryString.append(delim);
            queryString.append(" ? ");
            params.put(paramsindex++, contract.getId());
            delim = " , ";
        }    
        queryString.append(" )");

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setClass(BookingGroup.class);

        @SuppressWarnings("unchecked")
        Collection<BookingGroup> tmpBookingGroups = (Collection<BookingGroup>) q.executeWithMap(params);

        SortedSet<BookingGroup> bookingGroups = new TreeSet<BookingGroup>(new BookingGroupComparator(contracts));

        for (BookingGroup bookingGroup : tmpBookingGroups) {
            if (bookingGroupStates.contains(bookingGroup.getType())) {
                bookingGroups.add(bookingGroup);
            }
        }
        
        return bookingGroups;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getContractsWithStates(Set<String> states) {
        Map<String, Object> params = new HashMap<String, Object>();
        String or = " || ";
        String delim = ", ";
        String parameterName = "param";
        StringBuilder parameters = new StringBuilder(" parameters ");
        StringBuilder queryString = new StringBuilder("select from ");
        queryString.append(Contract.class.getName()).append(" where ");
        int i = 0;
        for (String state : states) {
            i++;
            queryString.append("_state == ").append(parameterName).append(i);
            queryString.append(or);
            params.put(parameterName + i, state);

            parameters.append(String.class.getName());
            parameters.append(" ").append(parameterName).append(i);
            parameters.append(delim);
        }
        String query = queryString.substring(0, queryString.length() - or.length()) + parameters.substring(0, parameters.length() - delim.length());

        Query q = getPersistenceManager().newQuery(query);

        Collection<Contract> result = (Collection<Contract>) q.executeWithMap(params);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getMainContractsWithStates(Set<String> states) {
        return findByNamedQuery(Contract.class, "getMainContractsWithStates", Collections.singletonMap("states", states));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getContractsWithStates(Account account, Set<String> states) {
        Collection<Contract> result = new ArrayList<Contract>();
        Map<String, Object> params = new HashMap<String, Object>();
        String or = " || ";
        String delim = ", ";
        String parameterName = "param";
        StringBuilder statesSb = new StringBuilder("(");
        StringBuilder parameters = new StringBuilder(" parameters ");
        StringBuilder queryString = new StringBuilder("select from ").append(Contract.class.getName());

        if (account.isBorrowerAnyState()) {
            queryString.append(" where this._order.");
        } else if (account.isLenderAnyState()) {
            queryString.append(" where this._offer.");
        } else {
            throw new RuntimeException("Account must be lender or borrower");
        }
        queryString.append("_account._accountId == account_id && ");

        params.put("account_id", account.getAccountId());
        parameters.append("java.lang.Long account_id, ");

        int i = 0;
        for (String state : states) {
            i++;
            statesSb.append("_state == ").append(parameterName).append(i);
            statesSb.append(or);
            params.put(parameterName + i, state);

            parameters.append(String.class.getName());
            parameters.append(" ").append(parameterName).append(i);
            parameters.append(delim);
        }
        String query = queryString + statesSb.substring(0, statesSb.length() - or.length()) + ")" + parameters.substring(0, parameters.length() - delim.length());
        Query q = getPersistenceManager().newQuery(Query.JDOQL, query);
        result = (Collection<Contract>) q.executeWithMap(params);

        return result;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getContractsByBidSearch(BidSearch bidSearch, Set<String> activeContractStates) {
        return getContractsByBidSearch(Collections.singletonList(bidSearch), activeContractStates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getContractsByBidSearch(Collection<BidSearch> bidSearches, Set<String> activeContractStates) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("contractStates", activeContractStates);
        params.put("bidSearches", bidSearches);
        @SuppressWarnings("unchecked")
        Collection<Contract> result = (Collection<Contract>) findByNamedQuery(Contract.class, "getContractByBidSearch", params);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double getContractsAmountSumByBidSearch(Collection<BidSearch> bidSearches, Set<String> activeContractStates) {
        Query query = getPersistenceManager().newNamedQuery(Contract.class, "getContractByBidSearch");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("contractStates", activeContractStates);
        params.put("bidSearches", bidSearches);
        
        query.setResult("sum(_amount)");
        @SuppressWarnings("unchecked")
        Double result = (Double) query.executeWithMap(params);
        if (result == null) {
            result = 0.0d;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContractRoiResultEntry> getRawRoiDateForContracts(final Collection<Contract> contracts, final Collection<Long> smavaBankAccountIds, final long kokId) {
        final String firstPartSql = "SELECT c.amount as \"contractAmount\", c.state as \"contractState\", b.amount as \"bookingAmount\", b.type_new as \"bookingType\", bg.date as \"bookingGroupDate\", bg.type as \"bookingGroupType\", t.state as \"transactionState\", t.type as \"transactionType\" \n"
                + "		   		from contract c\n"
                + "					join booking b on b.contract_id = c.id\n"
                + "					join transaction t on b.transaction_id = t.id\n"
                + "					join booking_assignment ba on ba.booking = b.id\n"
                + "					join booking_group bg on bg.id = ba.booking_group\n"
                + "				where c.id in (";
        final StringBuilder sql = new StringBuilder();
        sql.append(firstPartSql);
        final Set<Long> contractIds = new HashSet<Long>(contracts.size());
        for (Contract contract : contracts) {
            contractIds.add(contract.getId());
        }
        final Map<Integer, Object> params = new HashMap<Integer, Object>();
        sql.append(prepareWhereInWithCollection(contractIds, params));
        final String secoundPartSql = ") /* für verträge */\n"
                + "and t.state = 'SUCCESSFUL'\n"
                + "and bg.type in ('CONTRACT_CANCELLATION', 'INSURANCE', 'CREDIT_PAYOUT')\n"
                + "and t.debit_account in (";
        sql.append(secoundPartSql);
        sql.append(prepareWhereInWithCollection(smavaBankAccountIds, params));
        final String thirdPartSql = ") /* von smava account */ and t.credit_account = ? /* KNK */";
        sql.append(thirdPartSql);
        params.put(params.size() + 1, kokId);
        final Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setResultClass(ContractRoiResultEntry.class);
        final Object result = q.executeWithMap(params);

        return (List<ContractRoiResultEntry>) result;
    }

    private String prepareWhereInWithCollection(final Collection<? extends Object> contractIds, final Map<Integer, Object> params) {
        final StringBuilder sb = new StringBuilder();
        for (Object value : contractIds) {
            sb.append("?,");
            params.put(params.size() + 1, value);
        }
        return sb.substring(0, sb.length() - 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getStartedContracts(Account account) {
        return findByNamedQuery(Contract.class, "getStartedContracts", Collections.singletonMap("account", account));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> findContractsById(Set<Long> contractIds) {
        Collection<Contract> result = Collections.EMPTY_LIST;
        if (CollectionUtils.isNotEmpty(contractIds)) {

            Map<Integer, Object> params = new HashMap<Integer, Object>();

            String contractsList = createContractIdsSet(contractIds, params);
            if ( contractsList.length() > 0){
                StringBuilder sql = new StringBuilder("select id from contract where id in (");
                sql.append(contractsList);
                sql.append(")");

                final Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
                q.setClass(Contract.class);

                result = (Collection<Contract>) q.execute();
            }
        }
        return result;
    }

    private String createContractIdsSet(Set<Long> contractIds, Map<Integer, Object> params) {
        StringBuilder seq = new StringBuilder();
        for (Long id : contractIds) {
            if (id != null) {
                seq.append(id).append(",");
            }
        }

        if ( seq.length() == 0){
            return "";
        }
        return seq.substring(0, seq.length() - 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date findEndDate(final Contract contract) {
        Map<String, Object> params = new HashMap<String, Object>(1);
        params.put("contractId", contract.getId());

        Query q = getPersistenceManager().newQuery(Query.SQL, CONTRACT_END_DATE);
        q.setResultClass(Date.class);
        q.setUnique(true);

        Date result = (Date) q.executeWithMap(params);

        return result;
    }

    /**
     * @return all (repaid) contracts that have no bookings in an open transaction.
     */
    @Override
    public Collection<Contract> findCompletedContracts() {
        Set<String> lenderRunningContractStates = new HashSet<String>();
        lenderRunningContractStates.add(Contract.STATE_REPAY);

        Set<Integer> bookingTypes = new HashSet<Integer>();
        bookingTypes.add(BookingType.BORROWER_AMORTISATION.getDbValue());
        bookingTypes.add(BookingType.BORROWER_INTEREST.getDbValue());
        bookingTypes.add(BookingType.BORROWER_LATE_INTEREST.getDbValue());

        Map<String, Object> params = new HashMap<String, Object>(4);
        params.put("transactionState", Transaction.STATE_OPEN);
        params.put("lenderRunningContractStates", lenderRunningContractStates);
        params.put("bookingTypes", bookingTypes);

        Query q = getPersistenceManager().newQuery(Query.SQL, replaceCollectionTypes(params, COMPLETED_CONTRACTS_SQL));
        q.setClass(Contract.class);

        @SuppressWarnings("unchecked")
        Collection<Contract> result = (Collection<Contract>) q.executeWithMap(params);

        return result;
    }

    /**
     * @return all (repaid) contracts that have no bookings in an open transaction.
     */
    @Override
    public Collection<Contract> findDefaultedCompletedContracts() {
        Set<String> defaultedContractStates = new HashSet<String>();
        defaultedContractStates.add(Contract.STATE_DEFAULT);

        Set<Integer> bookingTypes = new HashSet<Integer>();
        bookingTypes.add(BookingType.BORROWER_AMORTISATION.getDbValue());
        bookingTypes.add(BookingType.BORROWER_INTEREST.getDbValue());
        bookingTypes.add(BookingType.BORROWER_LATE_INTEREST.getDbValue());

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionState", Transaction.STATE_OPEN);
        params.put("defaultedContractStates", defaultedContractStates);
        params.put("bookingTypes", bookingTypes);
        params.put("currentDate", new Timestamp(CurrentDate.getTime()));
        Calendar expiration = CurrentDate.getCalendar();
        expiration.add(Calendar.DAY_OF_MONTH, 8 * -7);


        params.put("limitDate", new Timestamp(expiration.getTime().getTime()));

        Query q = getPersistenceManager().newQuery(Query.SQL, replaceCollectionTypes(params, DEFAULTED_COMPLETED_CONTRACTS_SQL));
        q.setClass(Contract.class);

        @SuppressWarnings("unchecked")
        Collection<Contract> result = (Collection<Contract>) q.executeWithMap(params);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getLastSuccessfullTransactionDueDate(Contract contract, Collection<BankAccount> smavaInterimAccounts) {
        Collection<BankAccount> knks = contract.getOrder().getAccount().getInternalBankAccountHistory(contract.getOrder().getBankAccountProvider());

        Map<Integer, Object> params = new HashMap<Integer, Object>();
        StringBuilder query = new StringBuilder("select max(t.\"dueDate\") ");

        query.append("from booking b left outer join transaction t on t.id = b.transaction_id ");
        query.append("where b.contract_id = ? ");
        params.put(params.size() + 1, contract.getId());

        query.append("and t.state = ? ");
        params.put(params.size() + 1, Transaction.STATE_SUCCESSFUL);

        query.append("and t.debit_account in (");
        query.append(createBankAccountIdList(knks, params));
        query.append(") and t.credit_account in (");
        query.append(createBankAccountIdList(smavaInterimAccounts, params));
        query.append(")");

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", query.toString());
        q.setResultClass(Date.class);

        Collection<Date> results = (Collection<Date>) q.executeWithMap(params);
        Date result = null;
        if (results != null && !results.isEmpty()) {
            result = results.iterator().next();
        }

        return result;
    }

    private String createBankAccountIdList(Collection<BankAccount> bankAccounts, Map<Integer, Object> params) {
        StringBuilder subQuery = new StringBuilder();
        for (BankAccount bankAccount : bankAccounts) {
            subQuery.append("?,");
            params.put(params.size() + 1, bankAccount.getId());
        }

        return subQuery.substring(0, subQuery.length() - 1);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public int getRunningContractCount(final Account account) {
        return getRunningContractCount(account, CP_COUNT_SQL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRunningContractCountWithReminderAmount(final Account account) {
        return getRunningContractCount(account, CP_COUNT_WITH_REMINDER_SQL);
    }

    private int getRunningContractCount(final Account account, final String sql) {
        Assert.notNull(account);
        final Map<String, Object> params = new HashMap<String, Object>(2);
        params.put("accountId", account.getId());
        params.put("contractStates", Contract.REPAY_RELEVANT_CONTRACTSTATES);
        return executeWithParamsUnique(sql, params, Integer.class);
    }

     /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getContractsForOrderGroup(Long orderGroupId) {
        final Query q = getPersistenceManager().newNamedQuery(Contract.class, "getContractsForOrderGroup");
        return (Collection<Contract>) q.execute(orderGroupId);
    }

     /**
     * {@inheritDoc}
     */
    @Override
    public double getContractSumBetweenLenderAndBorrower(
            final Long lenderId,
            final Long borrowerId,
            final Set<String> states) {
        final Map<String, Object> params = new HashMap<String, Object>(3);
        params.put("lenderId", lenderId);
        params.put("borrowerId", borrowerId);
        params.put("states", states);
        final Double result = executeWithParamsUnique(SqlConstants.CONTRACT_SUM_FOR_LENDER_BORROWER, params, Double.class);
        return result == null ? 0 : result;
    }

     /**
     * {@inheritDoc}
     */
    @Override
    public Long getContractCountForPlacement(Long marketingPlacementId) {
        return (Long) getAggregateByNamedQuery(Contract.class,
                                               "getContractCountForPlacment",
                                               Collections.singletonMap("marketingPlacementId", marketingPlacementId),
                                               "count(this)",
                                               null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> getContractsbyLenderAndMarket(Long lender,
            String marketName) {

        Query query = getPersistenceManager().newNamedQuery(Contract.class,
                "getContractsForLenderAndMarket");

        Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, lender);
        params.put(2, marketName);
        @SuppressWarnings("unchecked")
        Collection<Contract> contracts = (Collection<Contract>) query
                .executeWithMap(params);

        return contracts;
    }
    
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ContractStateNumbers> getContractsNumbersPerStateForLender(Long lender) {
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, lender);

        String sql = "select state, count(*) "+
                        "from Contract c " +
                        "inner join offer o on c.offer_id = o.id "+
                        "where o.account_id = ? group by c.state";

        Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setResultClass(ContractStateNumbers.class);
        q.setUnique(false);


        Collection<ContractStateNumbers> queryResult = (Collection<ContractStateNumbers>) q.executeWithMap(params);


        return queryResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long countContracts(Collection<Market> markets, int term){
        StringBuilder marketCodes = new StringBuilder();
        String orDelim = "";

        for (Market market : markets) {
            marketCodes.append(orDelim).append("_marketName == '").append(market.getMarketName()).append("'");
            orDelim = " || ";
        }

        final Calendar calendar = CurrentDate.getCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, -term);

        String baseContractSql = "(" + marketCodes.toString() + ") && _state != '" + Contract.STATE_DELETED + "'";

        final String contractSql = "(" + baseContractSql + " && _creationDate >= param1) PARAMETERS java.util.Date param1";
        return getContractCount(contractSql, calendar.getTime());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Contract> findOrderedContracts(Collection<Market> markets, int term, long minimumCount){
        StringBuilder marketCodes = new StringBuilder();
        String orDelim = "";

        for (Market market : markets) {
            marketCodes.append(orDelim).append("_marketName == '").append(market.getMarketName()).append("'");
            orDelim = " || ";
        }

        final Calendar calendar = CurrentDate.getCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, -term);

        String baseContractSql = "(" + marketCodes.toString() + ") && _state != '" + Contract.STATE_DELETED + "'";
        final String allContractTerm = "(" + baseContractSql + ") RANGE 0, " + minimumCount;

        final Sortable sortable = new Sortable() {
            @Override
            public String getSort() {
                return "_creationDate";
            }

            @Override
            public boolean hasSort() {
                return true;
            }

            @Override
            public boolean sortDescending() {
                return true;
            }
        };
        return findContractList(allContractTerm, sortable);
    }

    @Override
    public ContractInterestRange getAverageRateAndMinMax(Collection<Market> markets, int term, double amoutStep){
        StringBuilder marketCodes = new StringBuilder();
        String orDelim = "";

        for (Market market : markets) {
            marketCodes.append(orDelim).append("_marketName == '").append(market.getMarketName()).append("'");
            orDelim = " || ";
        }

        final Calendar calendar = CurrentDate.getCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, -term);

        String baseContractSql = "(" + marketCodes.toString() + ") && _state != '" + Contract.STATE_DELETED + "'";

        final String contractSql = "(" + baseContractSql + " && _creationDate >= param1) PARAMETERS java.util.Date param1";

        return getAverageRateAndMinMax(contractSql, amoutStep, calendar.getTime());
    }

    @Override
    public List<Contract> getPendingContractsForLender(Account lender) {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals(OFFER_ACCOUNT, lender), term.equals(STATE, Contract.STATE_PENDING));
        return new ArrayList<Contract>(findContractList(term.toString(), lender));
    }

    @Override
    public List<Contract> getPayoutContractsForLender(Account lender) {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals(OFFER_ACCOUNT, lender), term.notEquals(STATE, Contract.STATE_NEW), term.notEquals(STATE, Contract.STATE_PENDING), term.notEquals(STATE, Contract.STATE_DELETED));
        return new ArrayList<Contract>(findContractList(term.toString(), lender));
    }

    @Override
    public List<Contract> getNewContractsForLender(Account lender/*, boolean onlyManualBids*/) {
        OqlTerm stateTerm = OqlTerm.newTerm();

        stateTerm.or(stateTerm.equals(STATE, Contract.STATE_NEW), stateTerm.and(stateTerm.equals("_offer._state", Bid.STATE_EXPIRED), stateTerm.equals(STATE, Contract.STATE_DELETED)));
        OqlTerm matchedTerm = OqlTerm.newTerm();
        matchedTerm.and(matchedTerm.equals(OFFER_ACCOUNT, lender), stateTerm);

        return new ArrayList<Contract>(findContractList(matchedTerm.toString(), lender));
    }

    @Override
    public List<Contract> getManualNewContractsForLender(Account lender) {
        OqlTerm stateTerm = OqlTerm.newTerm();
        stateTerm.equals(STATE, Contract.STATE_NEW);

        OqlTerm matchedTerm = OqlTerm.newTerm();
        matchedTerm.and(matchedTerm.equals(OFFER_ACCOUNT, lender), matchedTerm.equals("_offer._bidSearch", null), stateTerm);

        return new ArrayList<Contract>(findContractList(matchedTerm.toString(), lender));
    }

    @Override
    public List<Contract> getPayoutContractsForBorrower(Account borrower) {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals("_order._account", borrower), term.notEquals(STATE, Contract.STATE_NEW), term.notEquals(STATE, Contract.STATE_PENDING), term.notEquals(STATE, Contract.STATE_DELETED));

        return new ArrayList<Contract>(findContractList(term.toString(), borrower));
    }

    @Override
    public List<Contract> getNewContractsForBorrower(Account borrower) {
        OqlTerm matchedTerm = OqlTerm.newTerm();
        matchedTerm.and(matchedTerm.equals("_order._account", borrower), matchedTerm.equals(STATE, Contract.STATE_NEW));

        return new ArrayList<Contract>(findContractList(matchedTerm.toString(), borrower));
    }

    @Override
    public List<Contract> getPendingContractsForBorrower(Account borrower) {
        OqlTerm matchedTerm = OqlTerm.newTerm();
        matchedTerm.and(matchedTerm.equals("_order._account", borrower), matchedTerm.equals(STATE, Contract.STATE_PENDING));

        return new ArrayList<Contract>(findContractList(matchedTerm.toString(), borrower));
    }

    public Collection<Contract> getRelatedContracts(Order mainOrder, Collection<String> states) {

        final String orderRestriction = " && _order._groupId == " + mainOrder.getGroupId();

        String stateWhereClausePart = "(";

        int i = 0;
        for (String state : states) {

            stateWhereClausePart += "_state == '" + state + "'";
            i++;
            if (i < states.size()) {
                stateWhereClausePart += " || ";
            }
        }

        stateWhereClausePart += ")";

        Collection<Contract> relatedContracts = findContractList(stateWhereClausePart + orderRestriction);

        return relatedContracts;
    }

    @Override
    public Collection<Contract> findNewContractListSortByDate() {
        OqlTerm matchedTerm = OqlTerm.newTerm();
        matchedTerm.setSortBy("_creationDate");
        matchedTerm.setSortDescending(true);
        matchedTerm.equals("_state", Contract.STATE_NEW);
        matchedTerm.setRange(0L, 20L);

        return findContractList(matchedTerm.toString());
    }

    @Override
    public Collection<Contract> findNotNewContractListSortByDate() {
        OqlTerm notMatchedTerm = OqlTerm.newTerm();
        notMatchedTerm.setSortBy("_creationDate");
        notMatchedTerm.setSortDescending(true);
        notMatchedTerm.setRange(0L, 20L);
        notMatchedTerm.and(notMatchedTerm.notEquals("_state", Contract.STATE_NEW), notMatchedTerm.notEquals("_state", Contract.STATE_DELETED));

        return findContractList(notMatchedTerm.toString());
    }



    @Override
    public Collection<Contract> getObjectedContracts(){
        return findContractList("_state == '" + Contract.STATE_TO_BE_OBJECTED + "' || _state == '" + Contract.STATE_TO_BE_CANCELED + "'"); // WS-3131.3
    }


    @Override
    public Collection<Contract> getPendingContracts(){
        return findContractList("_state == '" + Contract.STATE_PENDING + "'");
    }
    @Override
    public Collection<Contract> getLatestContracts(Pageable pageable) {
        OqlTerm term = OqlTerm.newTerm();
        term.notEquals("_state", Contract.STATE_DELETED);
        term.setSortBy("_creationDate");
        term.setSortDescending(true);
        return findContractList(term.toString(),
                pageable);
    }

    // !!!!!!!! End of insert code section !!!!!!!!

}
