package de.smava.webapp.account.domain;

/**
 * Generic frelancer income calculation support for forms and entities.
 * 
 * @author Ferry Trunschke
 */
public interface FreelancerIncomeCalculation {

	// editable input fields.

	Integer getYear();
	void setYear(Integer year);
	Integer getDuration();
	void setDuration(Integer duration);
	Double getVolumeValue();
	void setVolumeValue(Double volume);	
	Double getExpensesValue();
	void setExpensesValue(Double expenses);
	
	// editable result fields.
	
	Double getTradeTaxValue();
	void setTradeTaxValue(Double tradeTax);
	Double getIncomeTaxValue();
	void setIncomeTaxValue(Double incomeTaxValue);
	Double getSolidarityTaxValue();
	void setSolidarityTaxValue(Double solidarityTax);
	Double getChurchTaxValue();
	void setChurchTaxValue(Double churchTax);
	
	// immutable result fields.
	 
	Double getPeriodEarningsBeforeTaxes();
	void setPeriodEarningsBeforeTaxes(Double periodEarningsBeforeTaxes);
	Double getAnnualEarningsBeforeTaxes();
	void setAnnualEarningsBeforeTaxes(Double annualEarningsBeforeTaxes);
	Double getAnnualProfitAfterTax();
	void setAnnualProfitAfterTax(Double annualProfitAfterTax);
	Double getMonthlyNetIncome();
	void setMonthlyNetIncome(Double monthlyNetIncome);
    
	// special backoffice fields
	
	Double getSpecialReserveValue();
	void setSpecialReserveValue(Double specialReserveValue);
	Double getSpecialReserveDissolutionValue();
	void setSpecialReserveDissolutionValue(Double specialReserveDissolutionValue);
	Double getNonCashExpenses();
	void setNonCashExpenses(Double nonCashExpenses);
	Double getOwnConsumptionValue();
	void setOwnConsumptionValue(Double ownConsumptionValue);
	Double getNonCashRevenuesValue();
	void setNonCashRevenuesValue(Double nonCashRevenues);
    Double getAdjustments();
    void setAdjustments(Double adjustments);
    Double getProfit();
    void setProfit(Double profit);
}
