package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BorrowerPromotionHistory;

import java.util.Date;
import java.util.Set;

/**
 * The domain object that represents 'BorrowerPromotions'.
 */
public class BorrowerPromotion extends BorrowerPromotionHistory  {

        protected Double _minAmount;
        protected Double _maxAmount;
        protected Float _interest;
        protected String _duration;
        protected Integer _minScore;
        protected Date _startDate;
        protected Date _expirationDate;
        protected String _name;
        protected Boolean _summatePromotionAmounts;
        protected Set<Rating> _ratings;
        
                                    
    /**
     * Setter for the property 'min amount'.
     */
    public void setMinAmount(Double minAmount) {
        _minAmount = minAmount;
    }
            
    /**
     * Returns the property 'min amount'.
     */
    public Double getMinAmount() {
        return _minAmount;
    }
                                            
    /**
     * Setter for the property 'max amount'.
     */
    public void setMaxAmount(Double maxAmount) {
        _maxAmount = maxAmount;
    }
            
    /**
     * Returns the property 'max amount'.
     */
    public Double getMaxAmount() {
        return _maxAmount;
    }
                                            
    /**
     * Setter for the property 'interest'.
     */
    public void setInterest(Float interest) {
        _interest = interest;
    }
            
    /**
     * Returns the property 'interest'.
     */
    public Float getInterest() {
        return _interest;
    }
                                    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(String duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = getDuration();
        }
        registerChange("duration", _durationInitVal, duration);
        _duration = duration;
    }
                        
    /**
     * Returns the property 'duration'.
     */
    public String getDuration() {
        return _duration;
    }
                                            
    /**
     * Setter for the property 'min score'.
     */
    public void setMinScore(Integer minScore) {
        _minScore = minScore;
    }
            
    /**
     * Returns the property 'min score'.
     */
    public Integer getMinScore() {
        return _minScore;
    }
                                    /**
     * Setter for the property 'start date'.
     */
    public void setStartDate(Date startDate) {
        if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = getStartDate();
        }
        registerChange("start date", _startDateInitVal, startDate);
        _startDate = startDate;
    }
                        
    /**
     * Returns the property 'start date'.
     */
    public Date getStartDate() {
        return _startDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                            
    /**
     * Setter for the property 'summatePromotionAmounts'.
     */
    public void setSummatePromotionAmounts(Boolean summatePromotionAmounts) {
        _summatePromotionAmounts = summatePromotionAmounts;
    }
            
    /**
     * Returns the property 'summatePromotionAmounts'.
     */
    public Boolean getSummatePromotionAmounts() {
        return _summatePromotionAmounts;
    }
                                            
    /**
     * Setter for the property 'ratings'.
     */
    public void setRatings(Set<Rating> ratings) {
        _ratings = ratings;
    }
            
    /**
     * Returns the property 'ratings'.
     */
    public Set<Rating> getRatings() {
        return _ratings;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BorrowerPromotion.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _duration=").append(_duration);
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(BorrowerPromotion.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
