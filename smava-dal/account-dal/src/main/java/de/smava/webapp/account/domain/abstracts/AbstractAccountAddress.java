package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;

import java.util.*;

import de.smava.webapp.account.domain.interfaces.AccountAddressEntityInterface;
import de.smava.webapp.account.domain.interfaces.AddressEntityInterface;import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * The domain object that represents 'AccountAddresss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractAccountAddress
    extends Address    implements AccountAddressEntityInterface    ,AddressEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractAccountAddress.class);

    /**
     * generated serial.
     */
    private static final long serialVersionUID = 4179554909075393845L;

    private transient List<Account> _modifiers = null;

    public boolean functionallyEquals ( Address compare){

        if ( compare == null){
            return false;
        }

        if ( this.getCity() == null
                || this.getCountry() == null
                || this.getStreet() == null
                || this.getStreetNumber() == null
                || this.getZipCode() == null
                || this.getType() == null
                ||  ( this.getLivingThereSince() == null && compare.getLivingThereSince() != null )) {
            return false;
        }

        //do vague date comparison using joda
        boolean dateEquals = true;

        if ( this.getLivingThereSince() != null){
            if ( compare.getLivingThereSince() == null){
                return false;
            }

            DateTime thisDate = new DateTime( this.getLivingThereSince());
            DateTime otherDate = new DateTime(compare.getLivingThereSince());

            LocalDate thisLD = thisDate.toLocalDate();
            LocalDate otherLD = otherDate.toLocalDate();

            dateEquals = (thisLD.compareTo(otherLD) == 0);
        }

        if ( this.getCity().equals( compare.getCity())
                && this.getCountry().equals(compare.getCountry())
                && this.getStreet().equals(compare.getStreet())
                && this.getStreetNumber().equals(compare.getStreetNumber())
                && this.getZipCode().equals(compare.getZipCode())
                && this.getType().equals(compare.getType())
                && dateEquals ){
            return true;
        }
        return false;
    }

    public List<? extends Entity> getModifier() {
        if (this._modifiers == null) {
            this._modifiers = Arrays.asList(new Account[]{getAccount()});
        }
        return this._modifiers;
    }

}

