package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractDtausFileOutTransaction;




/**
 * The domain object that has all history aggregation related fields for 'DtausFileOutTransactions'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DtausFileOutTransactionHistory extends AbstractDtausFileOutTransaction {



			
}
