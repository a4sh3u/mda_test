package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.account.domain.interfaces.SectorEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/**
 * The domain object that represents 'Sectors'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractSector
    extends KreditPrivatEntity implements java.io.Externalizable,SectorEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSector.class);

    private static final long serialVersionUID = -4140570946503421818L;

    public static final Long SECTOR_AGRICULTURE_FORESTRY_FISHERY = 4860L;
    public static final Long SECTOR_MINING = 4861L;
    public static final Long SECTOR_POWER_WATER_WASTE_MANAGEMENT = 4862L;
    public static final Long SECTOR_CHEMICAL_INDUSTRY = 4863L;
    public static final Long SECTOR_MECHANICAL_INDUSTRY = 4864L;
    public static final Long SECTOR_FOOD_INDUSTRY = 4865L;
    public static final Long SECTOR_BUILDING_INDUSTRY = 4866L;
    public static final Long SECTOR_CAR_INDUSTRY = 4867L;
    public static final Long SECTOR_CAR_SERVICES = 4868L;
    public static final Long SECTOR_WHOLESALE_RETAIL = 4869L;
    public static final Long SECTOR_TRANSPORT_TOURISM = 4870L;
    public static final Long SECTOR_MAIL_TELECOMMUNICATION = 4871L;
    public static final Long SECTOR_BANK_INSURANCE = 4872L;
    public static final Long SECTOR_EDUCATION = 4873L;
    public static final Long SECTOR_CRAFTS_AND_TRADES = 4874L;
    public static final Long SECTOR_HEALTH_CARE  = 4875L;
    public static final Long SECTOR_INFORMATION_TECHNOLOGY = 4876L;
    public static final Long SECTOR_MEDIA = 4877L;
    public static final Long SECTOR_LEGAL_TAX_CONSULTANCY = 4878L;
    public static final Long SECTOR_REAL_ESTATE_ARCHITECTURE = 4879L;
    public static final Long SECTOR_CIVIL_SERVICE  = 4880L;
    public static final Long SECTOR_OTHER_SERVICE = 4881L;
    public static final Long SECTOR_OTHER_INDUSTRIES = 4882L;
    public static final Long SECTOR_OTHER = 4883L;

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Object o = in.readObject();
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this);
    }

    public String getFullCode() {
        String code = getCode();
        if (getLevel() == 1 && code.length() == 1) {
            code = "0" + code;
        }
        Sector parent = getParent();
        if (parent != null) {
            code = parent.getFullCode() + code;
        }
        return code;
    }

    public String getFullName() {
        return getFullName(" / ");
    }

    public String getFullName(String delim) {
        String name = getName();
        Sector parent = getParent();
        if (parent != null) {
            name = parent.getFullName(delim) + delim + name;
        }
        return name;
    }

    /** Level 0 = first level. */
    public int getLevel() {
        List<Sector> path = getPath();
        return path.size() - 1;
    }

    /** Get path from root (first element) to this sector (included). */
    public List<Sector> getPath() {
        List<Sector> path = new ArrayList<Sector>();
        if (getParent() != null) {
            path.addAll(getParent().getPath());
        }
        path.add(this.instance());
        return path;
    }

    public void add(Sector child) {
        List<Sector> children = getChildren();
        if (children == null) {
            children = new ArrayList<Sector>();
        }
        children.add(child);
        setChildren(children);
        child.setParent(this.instance());
    }

    public abstract Sector instance();

}

