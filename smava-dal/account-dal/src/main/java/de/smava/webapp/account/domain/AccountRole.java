package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.AccountRoleHistory;

import java.util.Collection;

/**
 * The domain object that represents 'AccountRoles'.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@accountRoleId")
public class AccountRole extends AccountRoleHistory  {

    protected String _name;
    protected Account _account;
    protected Collection<Approval> _approvals;
    protected Collection<AccountRoleState> _states;

    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }

    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }

    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }

    /**
     * Setter for the property 'approvals'.
     */
    public void setApprovals(Collection<Approval> approvals) {
        _approvals = approvals;
    }

    /**
     * Returns the property 'approvals'.
     */
    public Collection<Approval> getApprovals() {
        return _approvals;
    }

    /**
     * Setter for the property 'states'.
     */
    public void setStates(Collection<AccountRoleState> states) {
        _states = states;
    }

    /**
     * Returns the property 'states'.
     */
    public Collection<AccountRoleState> getStates() {
        return _states;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AccountRole.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(AccountRole.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public AccountRole instance() {
        return this;
    }
}

