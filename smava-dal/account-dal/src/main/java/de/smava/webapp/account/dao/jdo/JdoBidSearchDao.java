//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid search)}

import de.smava.webapp.account.dao.BidSearchDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BidSearchs'.
 *
 * @author generator
 */
@Repository(value = "bidSearchDao")
public class JdoBidSearchDao extends JdoBaseDao implements BidSearchDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBidSearchDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bid search)}

    private static final String FIND_ACTIVE_BID_ASSISTANTS_WITH_RESTRICTION =
            "select bs.id as id from bid_search bs, bid bbs " +
                    "where bs.id=bbs.id and type in ('portfolio.agent','bid.agent') and (valid_until is null or valid_until>now()) " +
                    "and account_id = ? and service_provider_restriction = (select id from bank_service_provider where name='biw') " +
                    "group by bs.account_id, bs.id, bbs.amount " +
                    "having ((select sum(bo.amount) from  offer o, bid bo, contract c " +
                    "where o.bid_search_id=bs.id and o.id=bo.id and c.offer_id=o.id and c.state not in ('DELETED','OBJECTED','NEW') " +
                    "group by bs.id ) is null or ( select  sum(bo.amount) from  offer o, bid bo, contract c " +
                    "where o.bid_search_id=bs.id and o.id=bo.id and c.offer_id=o.id and c.state not in ('DELETED','OBJECTED','NEW') " +
                    "group by bs.id) < bbs.amount)";

    private static final String REMOVE_BID_ASSISTANTS_RESTRICTION =
            "update bid_search set service_provider_restriction = null where account_id = ? and type in ('portfolio.agent','bid.agent')";

    private static final long serialVersionUID = -6026181706277924554L;

    private static final String ACTIVE_BIDSEARCHES_SQL
            = "select id from bid_search bs"
            + " where (bs.valid_until is null or bs.valid_until > :now)\n"
            + " and bs.type in (:types)";

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bid search identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BidSearch load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearch - start: id=" + id);
        }
        BidSearch result = getEntity(BidSearch.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearch - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BidSearch getBidSearch(Long id) {
        return load(id);
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            BidSearch entity = findUniqueEntity(BidSearch.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the bid search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BidSearch bidSearch) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBidSearch: " + bidSearch);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bid search)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bidSearch);
    }

    /**
     * @deprecated Use {@link #save(BidSearch) instead}
     */
    public Long saveBidSearch(BidSearch bidSearch) {
        return save(bidSearch);
    }

    /**
     * Deletes an bid search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid search
     */
    public void deleteBidSearch(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBidSearch: " + id);
        }
        deleteEntity(BidSearch.class, id);
    }

    /**
     * Retrieves all 'BidSearch' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BidSearch> getBidSearchList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - start");
        }
        Collection<BidSearch> result = getEntities(BidSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BidSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BidSearch> getBidSearchList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - start: pageable=" + pageable);
        }
        Collection<BidSearch> result = getEntities(BidSearch.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BidSearch> getBidSearchList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - start: sortable=" + sortable);
        }
        Collection<BidSearch> result = getEntities(BidSearch.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BidSearch> getBidSearchList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearch> result = getEntities(BidSearch.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidSearch' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearch> findBidSearchList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - start: whereClause=" + whereClause);
        }
        Collection<BidSearch> result = findEntities(BidSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidSearch' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BidSearch> findBidSearchList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BidSearch> result = findEntities(BidSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BidSearch' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearch> findBidSearchList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BidSearch> result = findEntities(BidSearch.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearch> findBidSearchList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BidSearch> result = findEntities(BidSearch.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearch> findBidSearchList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearch> result = findEntities(BidSearch.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearch> findBidSearchList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearch> result = findEntities(BidSearch.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearch' instances.
     */
    public long getBidSearchCount() {
        long result = getEntityCount(BidSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearch' instances which match the given whereClause.
     */
    public long getBidSearchCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BidSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearch' instances which match the given whereClause together with params specified in object array.
     */
    public long getBidSearchCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BidSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid search)}

    @Override
    public Collection<BidSearch> getActiveBidAssistants() {
        return getActiveBidAssistants(BidSearch.BID_AGENT_TYPES);
    }

    private Collection<BidSearch> getActiveBidAssistants(List<String> bidAssistantTypes) {
        Query query = getPersistenceManager().newNamedQuery(BidSearch.class, "findActiveBidAssistants");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bidSearchTypes", bidAssistantTypes);
        params.put("sysDate", CurrentDate.getDate());

        query.setOrdering("_roiMin ASC, _creationDate ASC");

//        execute query
        @SuppressWarnings("unchecked")
        Collection<BidSearch> result = (Collection<BidSearch>) query.executeWithMap(params);

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<BidSearch> getActiveBidAssistants(Account account) {
        Query query = getPersistenceManager().newNamedQuery(BidSearch.class, "findActiveBidAssistantsByAccount");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bidSearchTypes", BidSearch.BID_AGENT_TYPES);
        params.put("sysDate", CurrentDate.getDate());
        params.put("account", account);

//        execute query
        @SuppressWarnings("unchecked")
        Collection<BidSearch> result = (Collection<BidSearch>) query.executeWithMap(params);

        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<BidSearch> getBidAssistants(Account account) {
        Query query = getPersistenceManager().newNamedQuery(BidSearch.class, "findBidAssistantsByAccount");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bidSearchTypes", BidSearch.BID_AGENT_TYPES);
        params.put("account", account);

//        execute query
        @SuppressWarnings("unchecked")
        Collection<BidSearch> result = (Collection<BidSearch>) query.executeWithMap(params);

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public Collection<BidSearch> getPlainBidAssistants(Account account) {
        Query query = getPersistenceManager().newNamedQuery(BidSearch.class, "findBidAssistantsByAccount");
        Map<String, Object> params = new HashMap<String, Object>();
        List<String> bidSearchTypes = Collections.singletonList(BidSearch.TYPE_BID_AGENT);
        params.put("bidSearchTypes", bidSearchTypes);
        params.put("account", account);

//        execute query
        @SuppressWarnings("unchecked")
        Collection<BidSearch> result = (Collection<BidSearch>) query.executeWithMap(params);

        return result;
    }


    /** {@inheritDoc} */
    @Override
    public Collection<BidSearch> getActiveMailNotifications() {
        Query query = getPersistenceManager().newNamedQuery(BidSearch.class, "findActiveBidAssistants");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bidSearchTypes", Collections.singletonList(BidSearch.TYPE_MAIL_NOTIFICATION));

        Date theFuture = CurrentDate.getDate();
        theFuture = DateUtils.addYear(theFuture, 20);
        params.put("sysDate", theFuture);

        query.setOrdering("_creationDate ASC");

//        execute query
        @SuppressWarnings("unchecked")
        Collection<BidSearch> result = (Collection<BidSearch>) query.executeWithMap(params);

        return result;
    }

    @Override
    public Collection<BidSearch> getActivePortfolioAgents() {
        return getActiveBidAssistants(Collections.singletonList(BidSearch.TYPE_PORTFOLIO_AGENT));
    }

    /** {@inheritDoc} */
    @Override
    public Collection<BidSearch> getPortfolioBidAssistants(Long portfolioId, boolean active) {
        Query query;
        if (active) {
            query = getPersistenceManager().newNamedQuery(BidSearch.class, "findActiveBidAssistantsByPortfolio");
        } else {
            query = getPersistenceManager().newNamedQuery(BidSearch.class, "findInactiveBidAssistantsByPortfolio");
        }
        query.setOrdering("_creationDate ASC");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bidSearchTypes", Collections.singletonList(BidSearch.TYPE_PORTFOLIO_AGENT));
        params.put("sysDate", CurrentDate.getDate());
        params.put("portfolioId", portfolioId);

        @SuppressWarnings("unchecked")
        Collection<BidSearch> result = (Collection<BidSearch>) query.executeWithMap(params);

        return	result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean hasActiveBidAssistantsWithRestriction(Account account) {
        Query query = getPersistenceManager().newQuery(Query.SQL, FIND_ACTIVE_BID_ASSISTANTS_WITH_RESTRICTION);

        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (account != null) {
            params.put(1, account.getId());
        }

        @SuppressWarnings("unchecked")
        Collection<Object> result = (Collection<Object>) query.executeWithMap(params);

        return result != null && result.size() > 0;
    }

    /** {@inheritDoc} */
    @Override
    public void removeBidAssistantsRestriction(Account account) {
        Query query = getPersistenceManager().newQuery(Query.SQL, REMOVE_BID_ASSISTANTS_RESTRICTION);

        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (account != null) {
            params.put(1, account.getId());
        }

        query.executeWithMap(params);
    }



    /** {@inheritDoc} */
    @Override
    public Set<Long> getActiveBidSearchIds(final Collection<String> types) {
        if (CollectionUtils.isEmpty(types)) {
            throw new IllegalArgumentException("types must be given");
        }
        final Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("types", types);
        params.put("now", new Timestamp(CurrentDate.getTime()));
        final String sql = replaceCollectionTypes(params, ACTIVE_BIDSEARCHES_SQL);
        final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setResultClass(Long.class);
        final Collection<Long> ids = (Collection<Long>) query.executeWithMap(params);
        final Set<Long> result = new LinkedHashSet<Long>(ids);
        return result;
    }

    // !!!!!!!! End of insert code section !!!!!!!!

}
