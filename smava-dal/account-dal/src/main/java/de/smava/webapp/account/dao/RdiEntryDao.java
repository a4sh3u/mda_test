//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    D:\projects\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    D:\projects\smava-trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi entry)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;


import de.smava.webapp.account.domain.RdiEntry;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RdiEntrys'.
 *
 * @author generator
 */
public interface RdiEntryDao extends BaseDao<RdiEntry>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the rdi entry identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RdiEntry getRdiEntry(Long id);

    /**
     * Saves the rdi entry.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRdiEntry(RdiEntry rdiEntry);

    /**
     * Deletes an rdi entry, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi entry
     */
    void deleteRdiEntry(Long id);

    /**
     * Retrieves all 'RdiEntry' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RdiEntry> getRdiEntryList();

    /**
     * Retrieves a page of 'RdiEntry' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RdiEntry> getRdiEntryList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RdiEntry' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RdiEntry> getRdiEntryList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RdiEntry' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RdiEntry> getRdiEntryList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'RdiEntry' instances.
     */
    long getRdiEntryCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi entry)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
