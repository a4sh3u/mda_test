package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.PasswordAlgorithm;

import java.util.Date;


/**
 * The domain object that represents 'Credentialss'.
 *
 * @author generator
 */
public interface CredentialsEntityInterface {

    /**
     * Setter for the property 'password changed date'.
     *
     * 
     *
     */
    void setPasswordChangedDate(Date passwordChangedDate);

    /**
     * Returns the property 'password changed date'.
     *
     * 
     *
     */
    Date getPasswordChangedDate();
    /**
     * Setter for the property 'identification name'.
     *
     * 
     *
     */
    void setIdentificationName(String identificationName);

    /**
     * Returns the property 'identification name'.
     *
     * 
     *
     */
    String getIdentificationName();
    /**
     * Setter for the property 'password'.
     *
     * 
     *
     */
    void setPassword(String password);

    /**
     * Returns the property 'password'.
     *
     * 
     *
     */
    String getPassword();
    /**
     * Setter for the property 'password changed'.
     *
     * 
     *
     */
    void setPasswordChanged(boolean passwordChanged);

    /**
     * Returns the property 'password changed'.
     *
     * 
     *
     */
    boolean getPasswordChanged();
    /**
     * Setter for the property 'transaction pin'.
     *
     * 
     *
     */
    void setTransactionPin(String transactionPin);

    /**
     * Returns the property 'transaction pin'.
     *
     * 
     *
     */
    String getTransactionPin();
    /**
     * Setter for the property 'transaction pin changed'.
     *
     * 
     *
     */
    void setTransactionPinChanged(boolean transactionPinChanged);

    /**
     * Returns the property 'transaction pin changed'.
     *
     * 
     *
     */
    boolean getTransactionPinChanged();
    /**
     * Setter for the property 'transaction pin failure count'.
     *
     * 
     *
     */
    void setTransactionPinFailureCount(int transactionPinFailureCount);

    /**
     * Returns the property 'transaction pin failure count'.
     *
     * 
     *
     */
    int getTransactionPinFailureCount();
    /**
     * Setter for the property 'password algorithm'.
     *
     * 
     *
     */
    void setPasswordAlgorithm(PasswordAlgorithm passwordAlgorithm);

    /**
     * Returns the property 'password algorithm'.
     *
     * 
     *
     */
    PasswordAlgorithm getPasswordAlgorithm();
    /**
     * Setter for the property 'salt'.
     *
     * 
     *
     */
    void setSalt(String salt);

    /**
     * Returns the property 'salt'.
     *
     * 
     *
     */
    String getSalt();

}
