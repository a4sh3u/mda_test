//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking assignment)}

import de.smava.webapp.account.dao.BookingAssignmentDao;
import de.smava.webapp.account.domain.BookingAssignment;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BookingAssignments'.
 *
 * @author generator
 */
@Repository(value = "bookingAssignmentDao")
public class JdoBookingAssignmentDao extends JdoBaseDao implements BookingAssignmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBookingAssignmentDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(booking assignment)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the booking assignment identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BookingAssignment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignment - start: id=" + id);
        }
        BookingAssignment result = getEntity(BookingAssignment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignment - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BookingAssignment getBookingAssignment(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BookingAssignment entity = findUniqueEntity(BookingAssignment.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the booking assignment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BookingAssignment bookingAssignment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBookingAssignment: " + bookingAssignment);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(booking assignment)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bookingAssignment);
    }

    /**
     * @deprecated Use {@link #save(BookingAssignment) instead}
     */
    public Long saveBookingAssignment(BookingAssignment bookingAssignment) {
        return save(bookingAssignment);
    }

    /**
     * Deletes an booking assignment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the booking assignment
     */
    public void deleteBookingAssignment(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBookingAssignment: " + id);
        }
        deleteEntity(BookingAssignment.class, id);
    }

    /**
     * Retrieves all 'BookingAssignment' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BookingAssignment> getBookingAssignmentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - start");
        }
        Collection<BookingAssignment> result = getEntities(BookingAssignment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BookingAssignment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BookingAssignment> getBookingAssignmentList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - start: pageable=" + pageable);
        }
        Collection<BookingAssignment> result = getEntities(BookingAssignment.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BookingAssignment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BookingAssignment> getBookingAssignmentList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - start: sortable=" + sortable);
        }
        Collection<BookingAssignment> result = getEntities(BookingAssignment.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingAssignment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BookingAssignment> getBookingAssignmentList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingAssignment> result = getEntities(BookingAssignment.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BookingAssignment' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingAssignment> findBookingAssignmentList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - start: whereClause=" + whereClause);
        }
        Collection<BookingAssignment> result = findEntities(BookingAssignment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BookingAssignment' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BookingAssignment> findBookingAssignmentList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BookingAssignment> result = findEntities(BookingAssignment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BookingAssignment' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingAssignment> findBookingAssignmentList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BookingAssignment> result = findEntities(BookingAssignment.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BookingAssignment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingAssignment> findBookingAssignmentList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BookingAssignment> result = findEntities(BookingAssignment.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingAssignment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingAssignment> findBookingAssignmentList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingAssignment> result = findEntities(BookingAssignment.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingAssignment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingAssignment> findBookingAssignmentList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingAssignment> result = findEntities(BookingAssignment.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BookingAssignment' instances.
     */
    public long getBookingAssignmentCount() {
        long result = getEntityCount(BookingAssignment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BookingAssignment' instances which match the given whereClause.
     */
    public long getBookingAssignmentCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BookingAssignment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BookingAssignment' instances which match the given whereClause together with params specified in object array.
     */
    public long getBookingAssignmentCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BookingAssignment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingAssignmentCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(booking assignment)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
