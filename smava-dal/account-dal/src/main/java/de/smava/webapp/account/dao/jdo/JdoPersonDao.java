//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(person)}

import de.smava.webapp.account.dao.PersonDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Persons'.
 *
 * @author generator
 */
@Repository(value = "personDao")
public class JdoPersonDao extends JdoBaseDao implements PersonDao {

    private static final Logger LOGGER = Logger.getLogger(JdoPersonDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(person)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the person identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Person load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPerson - start: id=" + id);
        }
        Person result = getEntity(Person.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPerson - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Person getPerson(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Person entity = findUniqueEntity(Person.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the person.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Person person) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("savePerson: " + person);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(person)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(person);
    }

    /**
     * @deprecated Use {@link #save(Person) instead}
     */
    public Long savePerson(Person person) {
        return save(person);
    }

    /**
     * Deletes an person, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the person
     */
    public void deletePerson(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deletePerson: " + id);
        }
        deleteEntity(Person.class, id);
    }

    /**
     * Retrieves all 'Person' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Person> getPersonList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - start");
        }
        Collection<Person> result = getEntities(Person.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Person' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Person> getPersonList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - start: pageable=" + pageable);
        }
        Collection<Person> result = getEntities(Person.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Person' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Person> getPersonList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - start: sortable=" + sortable);
        }
        Collection<Person> result = getEntities(Person.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Person' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Person> getPersonList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Person> result = getEntities(Person.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Person' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Person> findPersonList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - start: whereClause=" + whereClause);
        }
        Collection<Person> result = findEntities(Person.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Person' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Person> findPersonList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Person> result = findEntities(Person.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Person' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Person> findPersonList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Person> result = findEntities(Person.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Person' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Person> findPersonList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Person> result = findEntities(Person.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Person' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Person> findPersonList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Person> result = findEntities(Person.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Person' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Person> findPersonList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Person> result = findEntities(Person.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findPersonList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Person' instances.
     */
    public long getPersonCount() {
        long result = getEntityCount(Person.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Person' instances which match the given whereClause.
     */
    public long getPersonCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Person.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Person' instances which match the given whereClause together with params specified in object array.
     */
    public long getPersonCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Person.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getPersonCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(person)}


    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Person> findPerson(final String firstName, final String lastName, final Timestamp birthDate, final Long currentAccountId) {

    	// set range of day (00:00 ... 23:59:59)
    	Calendar cal = CurrentDate.getCalendar();

    	cal.setTime(birthDate);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
    	Date fromDate = cal.getTime();

    	cal.setTime(birthDate);
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	cal.set(Calendar.MILLISECOND, 999);
    	Date toDate = cal.getTime();

    	final Set<String> ignoreStates = new HashSet<String>(2);
    	ignoreStates.add(Account.USER_STATE_DEACTIVATED);
    	ignoreStates.add(Account.USER_STATE_DELETED);

    	// fill parameters into query
    	Query query = getPersistenceManager().newNamedQuery(Person.class, "findPersonsByFirstNameAndLastNameAndBirthDateAndNotDeactivatedState");
    	Map<String, Object> params = new HashMap<String, Object>();
        params.put("fromDate", fromDate);
        params.put("toDate", toDate);
        params.put("firstName", firstName.trim());
        params.put("lastName", lastName.trim());
        params.put("state", ignoreStates);
        params.put("accountId", currentAccountId);

        // execute query
        @SuppressWarnings("unchecked")
        List<Person> queryResult = (List<Person>) query.executeWithMap(params);

        // just debug
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("hasPerson() firstName=" + firstName + " lastName=" + lastName + " birthDate=" + birthDate + " fromDate=" + fromDate + " toDate=" + toDate + " results=" + queryResult.size());
        }

        // return result
        return queryResult;
    }

    @Override
    public Collection<Person> findActivePersonsByAccountIds(List<Long> accountIds) {
        Query query = getPersistenceManager().newQuery(Person.class);
        query.setFilter(":accountIds.contains(this._account._id) && this._validUntil==null");

        return (Collection<Person>) query.execute(accountIds);

    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
