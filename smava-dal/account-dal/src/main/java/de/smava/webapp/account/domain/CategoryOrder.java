package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CategoryOrderHistory;

/**
 * The domain object that represents 'CategoryOrders'.
 */
public class CategoryOrder extends CategoryOrderHistory  {

    public CategoryOrder() {
        super();
    }

    public CategoryOrder(Category category, Order order, boolean main) {
        super();
        setCategory(category);
        setOrder(order);
        setMain(main);
    }

    public CategoryOrder(Category category, Order order) {
        super();
        setCategory(category);
        setOrder(order);
    }

        protected boolean _main;
        protected Category _category;
        protected Order _order;
        
                            /**
     * Setter for the property 'main'.
     */
    public void setMain(boolean main) {
        if (!_mainIsSet) {
            _mainIsSet = true;
            _mainInitVal = getMain();
        }
        registerChange("main", _mainInitVal, main);
        _main = main;
    }
                        
    /**
     * Returns the property 'main'.
     */
    public boolean getMain() {
        return _main;
    }
                                            
    /**
     * Setter for the property 'category'.
     */
    public void setCategory(Category category) {
        _category = category;
    }
            
    /**
     * Returns the property 'category'.
     */
    public Category getCategory() {
        return _category;
    }
                                            
    /**
     * Setter for the property 'order'.
     */
    public void setOrder(Order order) {
        _order = order;
    }
            
    /**
     * Returns the property 'order'.
     */
    public Order getOrder() {
        return _order;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CategoryOrder.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _main=").append(_main);
            builder.append("\n}");
        } else {
            builder.append(CategoryOrder.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
