//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head15/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head15/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(repayment break)}

import de.smava.webapp.account.dao.RepaymentBreakDao;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RepaymentBreaks'.
 *
 * @author generator
 */
@Repository(value = "repaymentBreakDao")
public class JdoRepaymentBreakDao extends JdoBaseDao implements RepaymentBreakDao {


	private static final Logger LOGGER = Logger.getLogger(JdoRepaymentBreakDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(repayment break)}
        //
	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 5947234683331655929L;

	private static final String REQUESTED_SQL 
    	= "select rbd.requested_date, sum(rbd.requested_amount) from repayment_break_detail rbd\n" 
    	+ "where rbd.repayment_break_id = ?\n"
    	+ "group by rbd.requested_date";
    
    private static final String CONFIRMED_SQL
    	= "select rbd.confirmed_date, sum(rbd.confirmed_amount) from repayment_break_detail rbd\n" 
    	+ "where rbd.repayment_break_id = ? and rbd.confirmed_date is not null \n" 
    	+ "group by rbd.confirmed_date";
    
    private static final String CURRENT_MONTHLY_SQL
    	= "select cid, sum(amount_sum) as a_sum, sum(interest) as i_sum from (\n" 
		+ "						select \n"
		+ "						    b.contract_id as cid,  \n" 
		+ "						    b.amount as amount_sum,\n" 
		+ "						    case b.type_new \n"
		+ "						        when 8 then b.amount\n" 
		+ "						        else 0\n"
		+ "						    end as interest\n" 
		+ "						from booking b \n"
		+ "						inner join transaction t on t.id = b.transaction_id\n" 
		+ "						inner join booking_assignment ba on ba.booking = b.id\n" 
		+ "						inner join booking_group bg on ba.booking_group = bg.id\n" 
		+ "						where b.type_new in (8, 7) and t.state = 'OPEN' and t.credit_account = :creditBankAccount and t.debit_account = :debitBankAccount\n" 
		+ "						and ba.is_target = true and bg.type = 'INSURANCE' and bg.date = :date \n" 
		+ "						and b.contract_id in (:contracts)\n"
		+ "						) as tmp\n"
		+ "					group by cid";
    
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the repayment break identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RepaymentBreak load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreak - start: id=" + id);
        }
        RepaymentBreak result = getEntity(RepaymentBreak.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreak - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RepaymentBreak getRepaymentBreak(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RepaymentBreak entity = findUniqueEntity(RepaymentBreak.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the repayment break.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RepaymentBreak repaymentBreak) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRepaymentBreak: " + repaymentBreak);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(repayment break)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(repaymentBreak);
    }

    /**
     * @deprecated Use {@link #save(RepaymentBreak) instead}
     */
    public Long saveRepaymentBreak(RepaymentBreak repaymentBreak) {
        return save(repaymentBreak);
    }

    /**
     * Deletes an repayment break, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the repayment break
     */
    public void deleteRepaymentBreak(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRepaymentBreak: " + id);
        }
        deleteEntity(RepaymentBreak.class, id);
    }

    /**
     * Retrieves all 'RepaymentBreak' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RepaymentBreak> getRepaymentBreakList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - start");
        }
        Collection<RepaymentBreak> result = getEntities(RepaymentBreak.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RepaymentBreak' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RepaymentBreak> getRepaymentBreakList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - start: pageable=" + pageable);
        }
        Collection<RepaymentBreak> result = getEntities(RepaymentBreak.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RepaymentBreak' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RepaymentBreak> getRepaymentBreakList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - start: sortable=" + sortable);
        }
        Collection<RepaymentBreak> result = getEntities(RepaymentBreak.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RepaymentBreak' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RepaymentBreak> getRepaymentBreakList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RepaymentBreak> result = getEntities(RepaymentBreak.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RepaymentBreak' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreak> findRepaymentBreakList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - start: whereClause=" + whereClause);
        }
        Collection<RepaymentBreak> result = findEntities(RepaymentBreak.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RepaymentBreak' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RepaymentBreak> findRepaymentBreakList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RepaymentBreak> result = findEntities(RepaymentBreak.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RepaymentBreak' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreak> findRepaymentBreakList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RepaymentBreak> result = findEntities(RepaymentBreak.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RepaymentBreak' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreak> findRepaymentBreakList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RepaymentBreak> result = findEntities(RepaymentBreak.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RepaymentBreak' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreak> findRepaymentBreakList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RepaymentBreak> result = findEntities(RepaymentBreak.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RepaymentBreak' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreak> findRepaymentBreakList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RepaymentBreak> result = findEntities(RepaymentBreak.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RepaymentBreak' instances.
     */
    public long getRepaymentBreakCount() {
        long result = getEntityCount(RepaymentBreak.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RepaymentBreak' instances which match the given whereClause.
     */
    public long getRepaymentBreakCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RepaymentBreak.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RepaymentBreak' instances which match the given whereClause together with params specified in object array.
     */
    public long getRepaymentBreakCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RepaymentBreak.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakCount() - result: count=" + result);
        }
        return result;
    }

	
    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(repayment break)}
    //
    public Map<Date, Double> getConfirmedMonthAndSum(final Long repaymentBreakId) {
    	return executeForMap(CONFIRMED_SQL, repaymentBreakId);	
    }

    public Map<Date, Double> getConfirmedMonthAndSum(final RepaymentBreak repaymentBreak) {
		return getConfirmedMonthAndSum(repaymentBreak.getId());
	}

    public Map<Date, Double> getRequestedMonthAndSum(final Long repaymentBreakId) {
		return executeForMap(REQUESTED_SQL, repaymentBreakId);
	}

	public Map<Date, Double> getRequestedMonthAndSum(final RepaymentBreak repaymentBreak) {
		return getRequestedMonthAndSum(repaymentBreak.getId());
	}


	@Override
	public Map<Long, RepaymentBreakMonthData> getCurrentMonthlyData(
			Account account, Collection<BankAccount> interimAccounts, Set<Contract> contracts, Date month) {
		final Date normalizedMonth = DateUtils.getDateStartOfMonth(month);
		final Map<String, Object> params = new HashMap<String, Object>();
		Collection<Long> internalBankAccountIds = new LinkedList<Long>();
		for (BankAccount internalBankAccount : account.getInternalBankAccounts()) {
			internalBankAccountIds.add(internalBankAccount.getId());			
		}
        // TODO: interimAccounts is a Collection but SQL doesnt work for collection bigger than 1 element
		params.put("creditBankAccount", interimAccounts);
        // TODO: debitBankAccount is a Collection but SQL doesnt work for collection bigger than 1 element
		params.put("debitBankAccount", internalBankAccountIds);
		params.put("date", new Timestamp(normalizedMonth.getTime()));
		params.put("contracts", contracts);
		final String sql = replaceCollectionTypes(params, CURRENT_MONTHLY_SQL);
		final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
		q.setResultClass(Object[].class);
		final List<Object[]> rawResult = (List<Object[]>) q.executeWithMap(params);
		final Map<Long, RepaymentBreakMonthData> result = new HashMap<Long, RepaymentBreakMonthData>(rawResult.size());
		for (Object[] row : rawResult) {
			final RepaymentBreakMonthData monthData = new RepaymentBreakMonthData();
			monthData.setOriginalMonthlyRate((Double) row[1]);
			monthData.setMinRate((Double) row[2]);
			result.put((Long) row[0], monthData);
		}
		return result;
	}

	@Override
	public int getConfirmedRepaymentBreakCount(final Account account) {
		final Query q = getPersistenceManager().newNamedQuery(RepaymentBreak.class, "getConfirmedRepaymentBreakCount");
		q.setResultClass(Long.class);
		q.setUnique(true);
		final Long result = (Long) q.executeWithMap(Collections.singletonMap("accountId", account.getId()));
		return result == null ? 0 : result.intValue();
	}
	
	
	
	
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
