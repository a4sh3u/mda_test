package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.DtausEntry;
import de.smava.webapp.account.domain.DtausFileOutTransaction;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'DtausFiles'.
 *
 * @author generator
 */
public interface DtausFileEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'due date'.
     *
     * 
     *
     */
    void setDueDate(Date dueDate);

    /**
     * Returns the property 'due date'.
     *
     * 
     *
     */
    Date getDueDate();
    /**
     * Setter for the property 'transactions type'.
     *
     * 
     *
     */
    void setTransactionsType(String transactionsType);

    /**
     * Returns the property 'transactions type'.
     *
     * 
     *
     */
    String getTransactionsType();
    /**
     * Setter for the property 'release date'.
     *
     * 
     *
     */
    void setReleaseDate(Date releaseDate);

    /**
     * Returns the property 'release date'.
     *
     * 
     *
     */
    Date getReleaseDate();
    /**
     * Setter for the property 'content'.
     *
     * 
     *
     */
    void setContent(byte[] content);

    /**
     * Returns the property 'content'.
     *
     * 
     *
     */
    byte[] getContent();
    /**
     * Setter for the property 'original content'.
     *
     * 
     *
     */
    void setOriginalContent(byte[] originalContent);

    /**
     * Returns the property 'original content'.
     *
     * 
     *
     */
    byte[] getOriginalContent();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'direction'.
     *
     * 
     *
     */
    void setDirection(String direction);

    /**
     * Returns the property 'direction'.
     *
     * 
     *
     */
    String getDirection();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'note'.
     *
     * 
     *
     */
    void setNote(String note);

    /**
     * Returns the property 'note'.
     *
     * 
     *
     */
    String getNote();
    /**
     * Setter for the property 'dtausFileOutTransactions'.
     *
     * 
     *
     */
    void setDtausFileOutTransactions(List<DtausFileOutTransaction> dtausFileOutTransactions);

    /**
     * Returns the property 'dtausFileOutTransactions'.
     *
     * 
     *
     */
    List<DtausFileOutTransaction> getDtausFileOutTransactions();
    /**
     * Setter for the property 'dtaus entries'.
     *
     * 
     *
     */
    void setDtausEntries(List<DtausEntry> dtausEntries);

    /**
     * Returns the property 'dtaus entries'.
     *
     * 
     *
     */
    List<DtausEntry> getDtausEntries();

}
