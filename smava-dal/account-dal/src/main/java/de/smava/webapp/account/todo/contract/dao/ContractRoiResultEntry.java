/**
 * 
 */
package de.smava.webapp.account.todo.contract.dao;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author bvoss
 * c.amount as _contractAmount, c.state as _contractState, b.amount as _bookingAmount, 
 * b.type as _bookingType, bg.date as _bookingGroupDate, bg.type as _bookingGroupType, 
 * t.state as _transactionState, t.type as _transactionType
 */
public class ContractRoiResultEntry {
	
	private long _contractId;
	private long _bookingId;
	private long _transactionId;
	private double _contractAmount;
	private String _contractState;
	private double _bookingAmount;
	private String _bookingType;
	private Date _bookingGroupDate;
	private String _bookingGroupType;
	private String _transactionState;
	private String _transactionType;
	
	public long getContractId() {
		return _contractId;
	}


	public void setContractId(long contractId) {
		_contractId = contractId;
	}


	public long getBookingId() {
		return _bookingId;
	}


	public void setBookingId(long bookingId) {
		_bookingId = bookingId;
	}


	public long getTransactionId() {
		return _transactionId;
	}


	public void setTransactionId(long transactionId) {
		_transactionId = transactionId;
	}


	public double getContractAmount() {
		return _contractAmount;
	}


	public void setContractAmount(double contractAmount) {
		_contractAmount = contractAmount;
	}


	public String getContractState() {
		return _contractState;
	}


	public void setContractState(String contractState) {
		_contractState = contractState;
	}


	public double getBookingAmount() {
		return _bookingAmount;
	}


	public void setBookingAmount(double bookingAmount) {
		_bookingAmount = bookingAmount;
	}


	public String getBookingType() {
		return _bookingType;
	}


	public void setBookingType(String bookingType) {
		_bookingType = bookingType;
	}


	public Date getBookingGroupDate() {
		return _bookingGroupDate;
	}


	public void setBookingGroupDate(Timestamp bookingGroupDate) {
		_bookingGroupDate = bookingGroupDate;
	}


	public String getBookingGroupType() {
		return _bookingGroupType;
	}


	public void setBookingGroupType(String bookingGroupType) {
		_bookingGroupType = bookingGroupType;
	}


	public String getTransactionState() {
		return _transactionState;
	}


	public void setTransactionState(String transactionState) {
		_transactionState = transactionState;
	}


	public String getTransactionType() {
		return _transactionType;
	}


	public void setTransactionType(String transactionType) {
		_transactionType = transactionType;
	}	
}
