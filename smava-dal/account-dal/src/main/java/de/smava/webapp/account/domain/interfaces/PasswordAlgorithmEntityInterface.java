package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.PasswordAlgorithmType;


/**
 * The domain object that represents 'PasswordAlgorithms'.
 *
 * @author generator
 */
public interface PasswordAlgorithmEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(PasswordAlgorithmType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    PasswordAlgorithmType getType();
    /**
     * Setter for the property 'active'.
     *
     * 
     *
     */
    void setActive(Boolean active);

    /**
     * Returns the property 'active'.
     *
     * 
     *
     */
    Boolean getActive();

}
