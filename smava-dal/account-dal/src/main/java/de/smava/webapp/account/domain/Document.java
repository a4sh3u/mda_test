package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DocumentHistory;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * The domain object that represents 'Documents'.
 */
public class Document extends DocumentHistory  {

        protected String _state;
        protected String _approvalState;
        protected String _subject;
        protected String _text;
        protected String _notes;
        protected Date _creationDate;
        protected Date _date;
        protected String _direction;
        protected Account _lastModifier;
        protected Account _owner;
        protected String _type;
        protected String _typeOfDispatch;
        protected List<DocumentAddressAssignment> _documentAddressAssignments;
        protected String _email;
        protected String _name;
        protected String _phone;
        protected String _fax;
        protected String _emailFrom;
        protected String _nameFrom;
        protected String _phoneFrom;
        protected String _faxFrom;
        protected List<DocumentContainer> _documentContainers;
        protected Set<Attachment> _attachments;
        protected List<Contract> _contracts;
        protected ConsolidatedDebt _consolidatedDebt;
        protected de.smava.webapp.account.domain.BankAccountProvider _serviceProviderRestriction;

    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }

    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'approval state'.
     */
    public void setApprovalState(String approvalState) {
        if (!_approvalStateIsSet) {
            _approvalStateIsSet = true;
            _approvalStateInitVal = getApprovalState();
        }
        registerChange("approval state", _approvalStateInitVal, approvalState);
        _approvalState = approvalState;
    }
                        
    /**
     * Returns the property 'approval state'.
     */
    public String getApprovalState() {
        return _approvalState;
    }
                                    /**
     * Setter for the property 'subject'.
     */
    public void setSubject(String subject) {
        if (!_subjectIsSet) {
            _subjectIsSet = true;
            _subjectInitVal = getSubject();
        }
        registerChange("subject", _subjectInitVal, subject);
        _subject = subject;
    }
                        
    /**
     * Returns the property 'subject'.
     */
    public String getSubject() {
        return _subject;
    }
                                    /**
     * Setter for the property 'text'.
     */
    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = getText();
        }
        registerChange("text", _textInitVal, text);
        _text = text;
    }
                        
    /**
     * Returns the property 'text'.
     */
    public String getText() {
        return _text;
    }
                                    /**
     * Setter for the property 'notes'.
     */
    public void setNotes(String notes) {
        if (!_notesIsSet) {
            _notesIsSet = true;
            _notesInitVal = getNotes();
        }
        registerChange("notes", _notesInitVal, notes);
        _notes = notes;
    }
                        
    /**
     * Returns the property 'notes'.
     */
    public String getNotes() {
        return _notes;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'date'.
     */
    public void setDate(Date date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = getDate();
        }
        registerChange("date", _dateInitVal, date);
        _date = date;
    }
                        
    /**
     * Returns the property 'date'.
     */
    public Date getDate() {
        return _date;
    }
                                    /**
     * Setter for the property 'direction'.
     */
    public void setDirection(String direction) {
        if (!_directionIsSet) {
            _directionIsSet = true;
            _directionInitVal = getDirection();
        }
        registerChange("direction", _directionInitVal, direction);
        _direction = direction;
    }
                        
    /**
     * Returns the property 'direction'.
     */
    public String getDirection() {
        return _direction;
    }
                                            
    /**
     * Setter for the property 'last modifier'.
     */
    public void setLastModifier(Account lastModifier) {
        _lastModifier = lastModifier;
    }
            
    /**
     * Returns the property 'last modifier'.
     */
    public Account getLastModifier() {
        return _lastModifier;
    }
                                            
    /**
     * Setter for the property 'owner'.
     */
    public void setOwner(Account owner) {
        _owner = owner;
    }
            
    /**
     * Returns the property 'owner'.
     */
    public Account getOwner() {
        return _owner;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'type of dispatch'.
     */
    public void setTypeOfDispatch(String typeOfDispatch) {
        if (!_typeOfDispatchIsSet) {
            _typeOfDispatchIsSet = true;
            _typeOfDispatchInitVal = getTypeOfDispatch();
        }
        registerChange("type of dispatch", _typeOfDispatchInitVal, typeOfDispatch);
        _typeOfDispatch = typeOfDispatch;
    }
                        
    /**
     * Returns the property 'type of dispatch'.
     */
    public String getTypeOfDispatch() {
        return _typeOfDispatch;
    }
                                            
    /**
     * Setter for the property 'document address assignments'.
     */
    public void setDocumentAddressAssignments(List<DocumentAddressAssignment> documentAddressAssignments) {
        _documentAddressAssignments = documentAddressAssignments;
    }
            
    /**
     * Returns the property 'document address assignments'.
     */
    public List<DocumentAddressAssignment> getDocumentAddressAssignments() {
        return _documentAddressAssignments;
    }
                                    /**
     * Setter for the property 'email'.
     */
    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = getEmail();
        }
        registerChange("email", _emailInitVal, email);
        _email = email;
    }
                        
    /**
     * Returns the property 'email'.
     */
    public String getEmail() {
        return _email;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'phone'.
     */
    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = getPhone();
        }
        registerChange("phone", _phoneInitVal, phone);
        _phone = phone;
    }
                        
    /**
     * Returns the property 'phone'.
     */
    public String getPhone() {
        return _phone;
    }
                                    /**
     * Setter for the property 'fax'.
     */
    public void setFax(String fax) {
        if (!_faxIsSet) {
            _faxIsSet = true;
            _faxInitVal = getFax();
        }
        registerChange("fax", _faxInitVal, fax);
        _fax = fax;
    }
                        
    /**
     * Returns the property 'fax'.
     */
    public String getFax() {
        return _fax;
    }
                                    /**
     * Setter for the property 'email from'.
     */
    public void setEmailFrom(String emailFrom) {
        if (!_emailFromIsSet) {
            _emailFromIsSet = true;
            _emailFromInitVal = getEmailFrom();
        }
        registerChange("email from", _emailFromInitVal, emailFrom);
        _emailFrom = emailFrom;
    }
                        
    /**
     * Returns the property 'email from'.
     */
    public String getEmailFrom() {
        return _emailFrom;
    }
                                    /**
     * Setter for the property 'name from'.
     */
    public void setNameFrom(String nameFrom) {
        if (!_nameFromIsSet) {
            _nameFromIsSet = true;
            _nameFromInitVal = getNameFrom();
        }
        registerChange("name from", _nameFromInitVal, nameFrom);
        _nameFrom = nameFrom;
    }
                        
    /**
     * Returns the property 'name from'.
     */
    public String getNameFrom() {
        return _nameFrom;
    }
                                    /**
     * Setter for the property 'phone from'.
     */
    public void setPhoneFrom(String phoneFrom) {
        if (!_phoneFromIsSet) {
            _phoneFromIsSet = true;
            _phoneFromInitVal = getPhoneFrom();
        }
        registerChange("phone from", _phoneFromInitVal, phoneFrom);
        _phoneFrom = phoneFrom;
    }
                        
    /**
     * Returns the property 'phone from'.
     */
    public String getPhoneFrom() {
        return _phoneFrom;
    }
                                    /**
     * Setter for the property 'fax from'.
     */
    public void setFaxFrom(String faxFrom) {
        if (!_faxFromIsSet) {
            _faxFromIsSet = true;
            _faxFromInitVal = getFaxFrom();
        }
        registerChange("fax from", _faxFromInitVal, faxFrom);
        _faxFrom = faxFrom;
    }
                        
    /**
     * Returns the property 'fax from'.
     */
    public String getFaxFrom() {
        return _faxFrom;
    }
                                            
    /**
     * Setter for the property 'document containers'.
     */
    public void setDocumentContainers(List<DocumentContainer> documentContainers) {
        _documentContainers = documentContainers;
    }
            
    /**
     * Returns the property 'document containers'.
     */
    public List<DocumentContainer> getDocumentContainers() {
        return _documentContainers;
    }
                                            
    /**
     * Setter for the property 'attachments'.
     */
    public void setAttachments(Set<Attachment> attachments) {
        _attachments = attachments;
    }
            
    /**
     * Returns the property 'attachments'.
     */
    public Set<Attachment> getAttachments() {
        return _attachments;
    }
                                            
    /**
     * Setter for the property 'contracts'.
     */
    public void setContracts(List<Contract> contracts) {
        _contracts = contracts;
    }
            
    /**
     * Returns the property 'contracts'.
     */
    public List<Contract> getContracts() {
        return _contracts;
    }
                                            
    /**
     * Setter for the property 'consolidated debt'.
     */
    public void setConsolidatedDebt(ConsolidatedDebt consolidatedDebt) {
        _consolidatedDebt = consolidatedDebt;
    }
            
    /**
     * Returns the property 'consolidated debt'.
     */
    public ConsolidatedDebt getConsolidatedDebt() {
        return _consolidatedDebt;
    }
                                            
    /**
     * Setter for the property 'service provider restriction'.
     */
    public void setServiceProviderRestriction(de.smava.webapp.account.domain.BankAccountProvider serviceProviderRestriction) {
        _serviceProviderRestriction = serviceProviderRestriction;
    }
            
    /**
     * Returns the property 'service provider restriction'.
     */
    public de.smava.webapp.account.domain.BankAccountProvider getServiceProviderRestriction() {
        return _serviceProviderRestriction;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Document.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _approvalState=").append(_approvalState);
            builder.append("\n    _subject=").append(_subject);
            builder.append("\n    _text=").append(_text);
            builder.append("\n    _notes=").append(_notes);
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _date=").append(_date);
            builder.append("\n    _direction=").append(_direction);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _typeOfDispatch=").append(_typeOfDispatch);
            builder.append("\n    _email=").append(_email);
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _phone=").append(_phone);
            builder.append("\n    _fax=").append(_fax);
            builder.append("\n    _emailFrom=").append(_emailFrom);
            builder.append("\n    _nameFrom=").append(_nameFrom);
            builder.append("\n    _phoneFrom=").append(_phoneFrom);
            builder.append("\n    _faxFrom=").append(_faxFrom);
            builder.append("\n}");
        } else {
            builder.append(Document.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
