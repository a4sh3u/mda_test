//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(message config)}

import de.smava.webapp.account.dao.MessageConfigDao;
import de.smava.webapp.account.domain.MessageConfig;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'MessageConfigs'.
 *
 * @author generator
 */
@Repository(value = "messageConfigDao")
public class JdoMessageConfigDao extends JdoBaseDao implements MessageConfigDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMessageConfigDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(message config)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the message config identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MessageConfig load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfig - start: id=" + id);
        }
        MessageConfig result = getEntity(MessageConfig.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfig - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MessageConfig getMessageConfig(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MessageConfig entity = findUniqueEntity(MessageConfig.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the message config.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MessageConfig messageConfig) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMessageConfig: " + messageConfig);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(message config)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(messageConfig);
    }

    /**
     * @deprecated Use {@link #save(MessageConfig) instead}
     */
    public Long saveMessageConfig(MessageConfig messageConfig) {
        return save(messageConfig);
    }

    /**
     * Deletes an message config, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the message config
     */
    public void deleteMessageConfig(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMessageConfig: " + id);
        }
        deleteEntity(MessageConfig.class, id);
    }

    /**
     * Retrieves all 'MessageConfig' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MessageConfig> getMessageConfigList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - start");
        }
        Collection<MessageConfig> result = getEntities(MessageConfig.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MessageConfig' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MessageConfig> getMessageConfigList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - start: pageable=" + pageable);
        }
        Collection<MessageConfig> result = getEntities(MessageConfig.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MessageConfig' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MessageConfig> getMessageConfigList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - start: sortable=" + sortable);
        }
        Collection<MessageConfig> result = getEntities(MessageConfig.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MessageConfig' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MessageConfig> getMessageConfigList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MessageConfig> result = getEntities(MessageConfig.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MessageConfig' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MessageConfig> findMessageConfigList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - start: whereClause=" + whereClause);
        }
        Collection<MessageConfig> result = findEntities(MessageConfig.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MessageConfig' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MessageConfig> findMessageConfigList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MessageConfig> result = findEntities(MessageConfig.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MessageConfig' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MessageConfig> findMessageConfigList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MessageConfig> result = findEntities(MessageConfig.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MessageConfig' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MessageConfig> findMessageConfigList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MessageConfig> result = findEntities(MessageConfig.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MessageConfig' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MessageConfig> findMessageConfigList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MessageConfig> result = findEntities(MessageConfig.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MessageConfig' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MessageConfig> findMessageConfigList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MessageConfig> result = findEntities(MessageConfig.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageConfigList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MessageConfig' instances.
     */
    public long getMessageConfigCount() {
        long result = getEntityCount(MessageConfig.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MessageConfig' instances which match the given whereClause.
     */
    public long getMessageConfigCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MessageConfig.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MessageConfig' instances which match the given whereClause together with params specified in object array.
     */
    public long getMessageConfigCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MessageConfig.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageConfigCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(message config)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
