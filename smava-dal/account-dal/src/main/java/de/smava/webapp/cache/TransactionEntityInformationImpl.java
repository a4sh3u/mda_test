/**
 * 
 */
package de.smava.webapp.cache;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.Offer;
import de.smava.webapp.account.domain.Transaction;
import de.smava.webapp.account.domain.history.HistoryEntry;
import de.smava.webapp.account.domain.history.HistoryEntryDetail;
import de.smava.webapp.commons.domain.Change;
import de.smava.webapp.commons.domain.Entity;

import java.util.*;

/**
 * @author bvoss
 *
 */
public class TransactionEntityInformationImpl implements
        TransactionEntityInformation {
	
	public static final String ENTITY_INFORMATION_KEY = TransactionEntityInformationImpl.class.getName() + ".entity_information_key";
	
	private Set<Class<? extends Entity>> _involvedClasses = new HashSet<Class<? extends Entity>>();
	
	private Set<Class<? extends Entity>> _loadedClasses = new HashSet<Class<? extends Entity>>();
	private Set<Long> _involvedAssetAmountIds = new HashSet<Long>();
	private Set<Long> _involvedAccountIds = new HashSet<Long>();
	
	private Set<Entity> _involvedEntities = new HashSet<Entity>(24);
	private Map<Class<? extends Entity>, Set<Entity>> _classMap = new HashMap<Class<? extends Entity>, Set<Entity>>();
	
	private String _transactionName;
	
	private boolean _finished = false;

	/* (non-Javadoc)
	 * @see de.smava.webapp.cache.TransactionEntityInformation#getAllInvolvedEntityTypes()
	 */
	@Override
	public Set<Class<? extends Entity>> getAllInvolvedEntityTypes() {
		if (!_finished) {
			throw new IllegalStateException();
		}
		return _involvedClasses;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.cache.TransactionEntityInformation#isEntityInvolved(java.lang.Class)
	 */
	@Override
	public boolean isEntityInvolved(Class<? extends Entity> type) {
		if (!_finished) {
			throw new IllegalStateException();
		}
		return _involvedClasses.contains(type);
	}

	
	public void addEntity(Entity entity) {
		if (entity != null 
			&& !isHistoryEntry(entity)
			&& !isAccountWithOnlyLoginChanged(entity)
			&& !_involvedEntities.contains(entity)) {
			_involvedEntities.add(entity);
			_involvedClasses.add(entity.getClass());
			addInMap(entity);
			checkAssetAmountRelevance(entity);
		}
	}
	
	private void checkAssetAmountRelevance(Entity entity) {
		if (entity instanceof Offer) {
			Offer offer = (Offer) entity;
			final Account account = offer.getAccount();
			if (account != null) {
				if ( account.getAccountId() != null){
					_involvedAccountIds.add( account.getId());
				}
				Collection<BankAccount> bankAccounts = 
					account.getActiveBankAccounts();
				for (BankAccount bankAccount : bankAccounts) {
                    _involvedAssetAmountIds.add(bankAccount.getId());
                }
			}
		} else if (entity instanceof Transaction) {
			final Transaction transaction = (Transaction) entity;
			removeBankAccountOwnerFromCach(transaction.getCreditAccount());
			removeBankAccountOwnerFromCach(transaction.getDebitAccount());
		}
	}
	
	private void removeBankAccountOwnerFromCach(final BankAccount bankAccount) {
		if (bankAccount != null) {
			_involvedAssetAmountIds.add(bankAccount.getId());
		}
	}

	private boolean isAccountWithOnlyLoginChanged(Entity entity) {
		boolean result = false;
		if (entity instanceof Account) {
			final Map<String, Change> changes = entity.getChangeMap();
			if (changes.size() == 1 && changes.containsKey(Account.LAST_LOGIN_CHANGE)) {
				result = true;
			}
		}
		return result;
	}

	private boolean isHistoryEntry(final Entity entity) {
		final boolean result;
		if (entity instanceof HistoryEntry || entity instanceof HistoryEntryDetail) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}
	
	private void addInMap(Entity entity) {
		Set<Entity> values = _classMap.get(entity.getClass());
		if (values == null) {
			values = new HashSet<Entity>();
			_classMap.put(entity.getClass(), values);
		}
		values.add(entity);
	}

	public void doOnAfterComplete() {
		_loadedClasses = Collections.unmodifiableSet(_loadedClasses);
		_involvedClasses = Collections.unmodifiableSet(_involvedClasses);
		_involvedEntities = Collections.unmodifiableSet(_involvedEntities);
		_involvedAssetAmountIds = Collections.unmodifiableSet(_involvedAssetAmountIds);
		_involvedAccountIds =  Collections.unmodifiableSet(_involvedAccountIds);
		_finished = true;
	}

	public Set<Entity> getAllInvolvedEntities() {
		return _involvedEntities;
	}

	@SuppressWarnings("unchecked")
	public <T extends Entity> Set<T> getAllInvolvedEntitiesForType(
			Class<T> type) {
		Set<Entity> returnVal = _classMap.get(type);
		if (returnVal == null) {
			returnVal = Collections.emptySet();
		} else {
			returnVal = Collections.unmodifiableSet(returnVal);
		}
		return (Set<T>) returnVal;
	}
	
	public String getTransactionName() {
		return _transactionName;
	}

	public void setTransactionName(String transactionName) {
		this._transactionName = transactionName;
	}
	
	public void addLoaded(Entity entity) {
		_loadedClasses.add(entity.getClass());
	}

	public Set<Class<? extends Entity>> getLoadedClasses() {
		return _loadedClasses;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(super.toString());
		sb.append("[txname='");
		sb.append(_transactionName);
		sb.append("'");
		if (!_involvedEntities.isEmpty()) {
			sb.append(" changed classes[");
			for (Entity entity : _involvedEntities) {
				sb.append(entity.getClass().getSimpleName());
				sb.append(entity.getId());
				sb.append(",");
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append("]");
		}
		if (!_loadedClasses.isEmpty()) {
			sb.append(" loaded classes[");
			for (Class<?> type : _loadedClasses) {
				sb.append(type.getSimpleName());
				sb.append(",");
			}
			sb.deleteCharAt(sb.length() - 1);
			sb.append("]");
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	public Set<Long> getInvolvedAssetAmountRelevantAccountIds() {
		return _involvedAssetAmountIds;
	}
	
	@Override
	public Set<Long> getInvolvedAccountIds() {
		return _involvedAccountIds;
	}
	
}
