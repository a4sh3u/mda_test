package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Document;
import de.smava.webapp.account.domain.interfaces.DocumentContainerEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;

/**
 * The domain object that represents 'DocumentContainers'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractDocumentContainer
    extends KreditPrivatEntity implements DocumentContainerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDocumentContainer.class);

    public static final String TYPE_REGISTRATION_COMPILATION = "REGISTRATION_COMPILATION";
    public static final String TYPE_LETTER_COMPILATION = "LETTER_COMPILATION";
    public static final String REGISTRATION_COMPILATION_NAME_KEY = "REGISTRATION_COMPILATION";
    public static final String CONTRACT_COMPILATION_NAME_KEY = "CONTRACT_COMPILATION";

    public static final String TYPE_CONTRACT_COMPILATION = "CONTRACT_COMPILATION";

    public static final String TYPE_CONSOLIDATED_DEBT_REQUEST_COMPILATION = "CONSOLIDATED_DEBT_REQUEST_COMPILATION";

    public void addDocument(Document document) {
        if (getDocuments() == null) {
            setDocuments(new LinkedList<Document>());
        }

        getDocuments().add(document);
    }

    public void removeDocument(Document document) {
        if (getDocuments() != null) {
            getDocuments().remove(document);
        }
    }

}

