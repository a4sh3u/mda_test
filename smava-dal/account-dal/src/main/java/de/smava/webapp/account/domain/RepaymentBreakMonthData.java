/**
 * 
 */
package de.smava.webapp.account.domain;

/**
 * @author Dimitar Robev
 *
 */
public class RepaymentBreakMonthData {

	private double _actualMonthlyRate;
	private double _originalMonthlyRate;
	private double _minRate;

	public double getActualMonthlyRate() {
		return _actualMonthlyRate;
	}
	public void setActualMonthlyRate(double actualMonthlyRate) {
		_actualMonthlyRate = actualMonthlyRate;
	}
	public double getOriginalMonthlyRate() {
		return _originalMonthlyRate;
	}
	public void setOriginalMonthlyRate(double originalMonthlyRate) {
		_originalMonthlyRate = originalMonthlyRate;
	}
	public double getMinRate() {
		return _minRate;
	}
	public void setMinRate(double minRate) {
		_minRate = minRate;
	}
	public double getOriginalAmortisationRate() {
		return _originalMonthlyRate - _minRate;
	}
}
