package de.smava.webapp.account.dao.jdo;

/**
 * holds the data of the query from getAmountForTransactions.
 * @author rfiedler
 *
 */
public class TransactionAmount {
    private Long _transactionId = null;
    private Double _amount = null;
    
    public Long getTransactionid() {
        return _transactionId;
    }
    public void setTransactionid(Long transactionId) {
        this._transactionId = transactionId;
    }
    public Double getAmount() {
        return _amount;
    }
    public void setAmount(Double amount) {
        this._amount = amount;
    }
    
    
}