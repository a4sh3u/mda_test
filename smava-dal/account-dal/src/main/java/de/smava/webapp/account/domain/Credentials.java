package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CredentialsHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.springframework.security.authentication.dao.SaltSource;

import java.util.Date;

/**
 * The domain object that represents 'Credentialss'.
 */
public class Credentials extends CredentialsHistory  {

    /**
     * Returns the property 'password'.
     */
    public String getPassword() {
        return _password;
    }

    /**
     * Returns the property 'transaction pin'.
     */
    public String getTransactionPin() {
        return _transactionPin;
    }


    /**
     * Setter for the property 'password'.
     */
    public void setPassword(String password) {
        password = password;

        if (!_passwordIsSet) {
            _passwordIsSet = true;
            _passwordInitVal = getPassword();
        }
        registerChange("password", _passwordInitVal, password);
        _passwordChangedDate = CurrentDate.getDate();
        _password = password;
    }

    public void setTransactionPin(String transactionPin, SaltSource saltSource) {
        transactionPin = encodeTPin(transactionPin, saltSource);

        if (!_transactionPinIsSet) {
            _transactionPinIsSet = true;
            _transactionPinInitVal = getTransactionPin();
        }
        registerChange("transaction pin", _transactionPinInitVal, transactionPin);
        _transactionPin = transactionPin;
    }

    @Override
    @Deprecated
    public void setPasswordChangedDate(Date passwordChangedDate) {
    }

    @Override
    @Deprecated
    public Date getPasswordChangedDate() {
        return _passwordChangedDate;
    }


    @Override
    @Deprecated
    public void setTransactionPin(String transactionPin) {
    }

        protected Date _passwordChangedDate;
        protected String _identificationName;
        protected String _password;
        protected boolean _passwordChanged;
        protected String _transactionPin;
        protected boolean _transactionPinChanged;
        protected int _transactionPinFailureCount;
        protected PasswordAlgorithm _passwordAlgorithm;
        protected String _salt;
        
                                        /**
     * Setter for the property 'identification name'.
     */
    public void setIdentificationName(String identificationName) {
        if (!_identificationNameIsSet) {
            _identificationNameIsSet = true;
            _identificationNameInitVal = getIdentificationName();
        }
        registerChange("identification name", _identificationNameInitVal, identificationName);
        _identificationName = identificationName;
    }
                        
    /**
     * Returns the property 'identification name'.
     */
    public String getIdentificationName() {
        return _identificationName;
    }
                                                /**
     * Setter for the property 'password changed'.
     */
    public void setPasswordChanged(boolean passwordChanged) {
        if (!_passwordChangedIsSet) {
            _passwordChangedIsSet = true;
            _passwordChangedInitVal = getPasswordChanged();
        }
        registerChange("password changed", _passwordChangedInitVal, passwordChanged);
        _passwordChanged = passwordChanged;
    }
                        
    /**
     * Returns the property 'password changed'.
     */
    public boolean getPasswordChanged() {
        return _passwordChanged;
    }
                                                /**
     * Setter for the property 'transaction pin changed'.
     */
    public void setTransactionPinChanged(boolean transactionPinChanged) {
        if (!_transactionPinChangedIsSet) {
            _transactionPinChangedIsSet = true;
            _transactionPinChangedInitVal = getTransactionPinChanged();
        }
        registerChange("transaction pin changed", _transactionPinChangedInitVal, transactionPinChanged);
        _transactionPinChanged = transactionPinChanged;
    }
                        
    /**
     * Returns the property 'transaction pin changed'.
     */
    public boolean getTransactionPinChanged() {
        return _transactionPinChanged;
    }
                                    /**
     * Setter for the property 'transaction pin failure count'.
     */
    public void setTransactionPinFailureCount(int transactionPinFailureCount) {
        if (!_transactionPinFailureCountIsSet) {
            _transactionPinFailureCountIsSet = true;
            _transactionPinFailureCountInitVal = getTransactionPinFailureCount();
        }
        registerChange("transaction pin failure count", _transactionPinFailureCountInitVal, transactionPinFailureCount);
        _transactionPinFailureCount = transactionPinFailureCount;
    }
                        
    /**
     * Returns the property 'transaction pin failure count'.
     */
    public int getTransactionPinFailureCount() {
        return _transactionPinFailureCount;
    }
                                            
    /**
     * Setter for the property 'password algorithm'.
     */
    public void setPasswordAlgorithm(PasswordAlgorithm passwordAlgorithm) {
        _passwordAlgorithm = passwordAlgorithm;
    }
            
    /**
     * Returns the property 'password algorithm'.
     */
    public PasswordAlgorithm getPasswordAlgorithm() {
        return _passwordAlgorithm;
    }
                                    /**
     * Setter for the property 'salt'.
     */
    public void setSalt(String salt) {
        if (!_saltIsSet) {
            _saltIsSet = true;
            _saltInitVal = getSalt();
        }
        registerChange("salt", _saltInitVal, salt);
        _salt = salt;
    }
                        
    /**
     * Returns the property 'salt'.
     */
    public String getSalt() {
        return _salt;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Credentials.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _identificationName=").append(_identificationName);
            builder.append("\n    _password=").append(_password);
            builder.append("\n    _passwordChanged=").append(_passwordChanged);
            builder.append("\n    _transactionPin=").append(_transactionPin);
            builder.append("\n    _transactionPinChanged=").append(_transactionPinChanged);
            builder.append("\n    _transactionPinFailureCount=").append(_transactionPinFailureCount);
            builder.append("\n    _salt=").append(_salt);
            builder.append("\n}");
        } else {
            builder.append(Credentials.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
