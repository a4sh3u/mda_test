package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;

import java.util.*;

import de.smava.webapp.account.domain.interfaces.ApprovalEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'Approvals'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractApproval
    extends KreditPrivatEntity implements DateComparable,ApprovalEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractApproval.class);

    private static final long serialVersionUID = 1L;

    public static final int DENY = 0;
    public static final int ACCEPT = 1;

    public Date getDateToCompare() {
        return getApprovalDate();
    }


    public List<? extends Entity> getModifier() {
        return Collections.singletonList(getApprover());
    }

}

