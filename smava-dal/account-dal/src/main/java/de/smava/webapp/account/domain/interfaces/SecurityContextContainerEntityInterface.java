package de.smava.webapp.account.domain.interfaces;


import java.util.Date;


/**
 * The domain object that represents 'SecurityContextContainers'.
 *
 * @author generator
 */
public interface SecurityContextContainerEntityInterface {

    /**
     * Setter for the property 'session id'.
     *
     * 
     *
     */
    void setSessionId(String sessionId);

    /**
     * Returns the property 'session id'.
     *
     * 
     *
     */
    String getSessionId();
    /**
     * Setter for the property 'security context'.
     *
     * 
     *
     */
    void setSecurityContext(byte[] securityContext);

    /**
     * Returns the property 'security context'.
     *
     * 
     *
     */
    byte[] getSecurityContext();
    /**
     * Setter for the property 'update timestamp'.
     *
     * 
     *
     */
    void setUpdateTimestamp(Date updateTimestamp);

    /**
     * Returns the property 'update timestamp'.
     *
     * 
     *
     */
    Date getUpdateTimestamp();

}
