/**
 * 
 */
package de.smava.webapp.account.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.smava.webapp.commons.domain.MappableEnum;
import de.smava.webapp.commons.domainutil.MappableEnumHelper;

/**
 * @author bvoss
 *
 */
public enum BookingType implements MappableEnum<BookingType> {
	
	LENDER_AGIO(1), //relevant            //KOK->VVW
	LENDER_BASE_PROVISION(2),             //KOK->smava
	UNSCHED_BASE_PROVISION(3),            //KOK->smava
	BLACKLIST_FEE(4),                     //???
	TRANSACTION_FEE(5),                   //???
	BORROWER_PAYOUT(6),                   //INTERIM_OUT->KNK
	BORROWER_AMORTISATION(7),             //KNK->KN_REF
	BORROWER_INTEREST(8),                 //???
	BORROWER_LATE_INTEREST(9),            //???
	BORROWER_UNSCHED_REPAYMENT(10),       
	LENDER_PAYMENT_CREDIT(11),            //KOK->INTERIM_OUT
	LENDER_AMORTISATION(12),
	LENDER_INSURANCE_POOL_PAYOUT(13),
	LENDER_INTEREST(14),
	LENDER_LATE_INTEREST(15),
	LENDER_AUTHENTICATION(16),
	LENDER_UNSCHED_PAYMENT(17),
	OVERDUE_FINE(18),
	RETURN_DEBIT_CHARGE(19),
	GROUPFEE(20),//relevant
	INVITATION_BONUS(21),//relevant
	LENDER_DEPOSIT(22),//relevant
	LENDER_PAYOUT(23),//relevant
	BORROWER_INITIAL_INTEREST(24),
	ROUNDING_ADJUSTMENT(25),
	BORROWER_ENCASHMENT_AMORTISATION(26), 
	OVERPAY(27),
	RDI_BORROWER_AMORTISATION(28),
	RDI_PREMIUM(29),
	RDI_RISK_PREMIUM(30),
	RDI_BORROWER_INTEREST(31),
	RDI_BORROWER_UNSCHED_REPAYMENT(32),
	MARKETING_BONUS(33),
	BORROWER_CHARGE(34),
	BORROWER_CHARGE_REPAYMENT(35),
	LENDER_CHARGE(36),//relevant
	LENDER_CHARGE_REPAYMENT(37),//relevant
	BORROWER_REPAYMENTBREAK_CHARGE(38),
	BORROWER_ACTIVITY_FEE(39),
	LENDER_ACTIVITY_FEE(40),
	POSTIDENT_FEE(41),
	RETURN_DEBIT_CHARGE_INTERNAL(42),
	LENDER_INITIAL_INTEREST(43); 
	
	public static final Set<BookingType> BORROWER_CHARGE_BOOKING_TYPES
	    = Collections.unmodifiableSet(
				new LinkedHashSet<BookingType>(
						Arrays.asList(
								BORROWER_ACTIVITY_FEE,
								POSTIDENT_FEE,
								RETURN_DEBIT_CHARGE,
								RETURN_DEBIT_CHARGE_INTERNAL,
								BORROWER_CHARGE,
								BORROWER_CHARGE_REPAYMENT,
								OVERDUE_FINE)));
	
	public static final Set<BookingType> BORROWER_RATE_BOOKING_TYPES
	    = Collections.unmodifiableSet(
				new LinkedHashSet<BookingType>(
						Arrays.asList(
								BORROWER_AMORTISATION,
								BORROWER_INTEREST,
								BORROWER_INITIAL_INTEREST)));
	
	public static final Set<BookingType> BORROWER_RATE_INCL_LATE_INTEREST_BOOKING_TYPES
	    = Collections.unmodifiableSet(
				new LinkedHashSet<BookingType>(
						Arrays.asList(
								BORROWER_AMORTISATION,
								BORROWER_INTEREST,
								BORROWER_INITIAL_INTEREST,
								BORROWER_LATE_INTEREST)));
	
	
	public static final Set<BookingType> OVERPAY_PROCESS_FORWARD_TO_INTERIM_BOOKING_TYPES
    	= Collections.unmodifiableSet(
			new LinkedHashSet<BookingType>(
					Arrays.asList(
							BORROWER_AMORTISATION,
							BORROWER_UNSCHED_REPAYMENT,
							BORROWER_INTEREST,
							BORROWER_INITIAL_INTEREST,
							BORROWER_LATE_INTEREST)));
	
	
	public static final List<Set<BookingType>> REMINDER_PROCESS_FILLUP_ORDER;
	
	static {
		final List<Set<BookingType>> reminderFillUp = new ArrayList<Set<BookingType>>();
    	reminderFillUp.add(Collections.singleton(BookingType.MARKETING_BONUS));
    	reminderFillUp.add(Collections.singleton(BookingType.RETURN_DEBIT_CHARGE));
    	reminderFillUp.add(Collections.singleton(BookingType.RETURN_DEBIT_CHARGE_INTERNAL));
    	reminderFillUp.add(Collections.singleton(BookingType.OVERDUE_FINE));    	
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_AMORTISATION));
    	reminderFillUp.add(Collections.unmodifiableSet(new HashSet<BookingType>(Arrays.asList(BookingType.BORROWER_INTEREST, BookingType.BORROWER_INITIAL_INTEREST))));
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_LATE_INTEREST));
    	reminderFillUp.add(Collections.singleton(BookingType.POSTIDENT_FEE));
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_ACTIVITY_FEE));
    	REMINDER_PROCESS_FILLUP_ORDER = Collections.unmodifiableList(reminderFillUp);
	}
	
	public static final List<Set<BookingType>> OVERPAY_PROCESS_FILLUP_ORDER;
	
	static {
		final List<Set<BookingType>> reminderFillUp = new ArrayList<Set<BookingType>>();
    	reminderFillUp.add(Collections.singleton(BookingType.MARKETING_BONUS));
    	reminderFillUp.add(Collections.singleton(BookingType.RETURN_DEBIT_CHARGE));
    	reminderFillUp.add(Collections.singleton(BookingType.RETURN_DEBIT_CHARGE_INTERNAL));
    	reminderFillUp.add(Collections.singleton(BookingType.OVERDUE_FINE));    	
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_AMORTISATION));
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_UNSCHED_REPAYMENT));
    	reminderFillUp.add(Collections.unmodifiableSet(new HashSet<BookingType>(Arrays.asList(BookingType.BORROWER_INTEREST, BookingType.BORROWER_INITIAL_INTEREST))));
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_LATE_INTEREST));
    	reminderFillUp.add(Collections.singleton(BookingType.POSTIDENT_FEE));
    	reminderFillUp.add(Collections.singleton(BookingType.BORROWER_ACTIVITY_FEE));
    	OVERPAY_PROCESS_FILLUP_ORDER = Collections.unmodifiableList(reminderFillUp);
	}
	
	private final Integer _dbValue;
	
	private BookingType(final int dbValue) {
		_dbValue = dbValue;
	}

	public Integer getDbValue() {
		return _dbValue;
	}


	public BookingType getEnumFromDbValue(final Number dbValue) {
		return enumFromDbValue(dbValue);
	}
	
	public static BookingType enumFromDbValue(final Number dbValue) {
		return MappableEnumHelper.getEnumFromDbValue(BookingType.class, dbValue);
	}

	public String getName() {
		return name();
	}

}
