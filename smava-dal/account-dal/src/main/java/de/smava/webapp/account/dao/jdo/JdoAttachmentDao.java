//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(attachment)}

import de.smava.webapp.account.dao.AttachmentDao;
import de.smava.webapp.account.domain.Attachment;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.datastore.Sequence;
import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Attachments'.
 *
 * @author generator
 */
@Repository(value = "attachmentDao")
public class JdoAttachmentDao extends JdoBaseDao implements AttachmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAttachmentDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(attachment)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the attachment identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Attachment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachment - start: id=" + id);
        }
        Attachment result = getEntity(Attachment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachment - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Attachment getAttachment(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Attachment entity = findUniqueEntity(Attachment.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the attachment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Attachment attachment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAttachment: " + attachment);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(attachment)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(attachment);
    }

    /**
     * @deprecated Use {@link #save(Attachment) instead}
     */
    public Long saveAttachment(Attachment attachment) {
        return save(attachment);
    }

    /**
     * Deletes an attachment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the attachment
     */
    public void deleteAttachment(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAttachment: " + id);
        }
        deleteEntity(Attachment.class, id);
    }

    /**
     * Retrieves all 'Attachment' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Attachment> getAttachmentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - start");
        }
        Collection<Attachment> result = getEntities(Attachment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Attachment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Attachment> getAttachmentList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - start: pageable=" + pageable);
        }
        Collection<Attachment> result = getEntities(Attachment.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Attachment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Attachment> getAttachmentList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - start: sortable=" + sortable);
        }
        Collection<Attachment> result = getEntities(Attachment.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Attachment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Attachment> getAttachmentList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Attachment> result = getEntities(Attachment.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Attachment' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Attachment> findAttachmentList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - start: whereClause=" + whereClause);
        }
        Collection<Attachment> result = findEntities(Attachment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Attachment' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Attachment> findAttachmentList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Attachment> result = findEntities(Attachment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Attachment' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Attachment> findAttachmentList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Attachment> result = findEntities(Attachment.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Attachment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Attachment> findAttachmentList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Attachment> result = findEntities(Attachment.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Attachment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Attachment> findAttachmentList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Attachment> result = findEntities(Attachment.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Attachment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Attachment> findAttachmentList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Attachment> result = findEntities(Attachment.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAttachmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Attachment' instances.
     */
    public long getAttachmentCount() {
        long result = getEntityCount(Attachment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Attachment' instances which match the given whereClause.
     */
    public long getAttachmentCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Attachment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Attachment' instances which match the given whereClause together with params specified in object array.
     */
    public long getAttachmentCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Attachment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAttachmentCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(attachment)}
    //
    // insert custom methods here
    //

    public Long createAttachmentId() {
        Sequence seq = getPersistenceManager().getSequence("de.smava.webapp.account.domain.ATTACHMENT_NUMBER");
        return (Long) seq.next();
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
