package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.MatchSortingEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'MatchSortings'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 * @author generator
 */
public abstract class AbstractMatchSorting
    extends KreditPrivatEntity implements MatchSortingEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMatchSorting.class);

    private static final long serialVersionUID = -7132268848082596432L;

}

