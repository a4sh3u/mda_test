/**
 * 
 */
package de.smava.webapp.account.domain;

import java.util.Set;

import org.springframework.util.Assert;

import de.smava.webapp.account.util.Period;

/**
 * Holder for all Parameters for the getBookingSumBetweenAccountsAndTypes.
 * 
 * @author Dimitar Robev
 *
 */
public class BookingSumParameters {
	private final BankAccountTransferDirection _transferDirection;
	private final Set<String> _transactionStates;
	private final Set<BookingType> _bookingTypes;
	private final Set<String> _contractStates;
	private final Boolean _targetOnly;
	private final Set<String> _bookingGroupTypes; 
	private final Period _period;
	private final Set<Order> _orders;
	
	/**
	 * 
	 * Constructor.
	 * 
	 * @param transferDirection - defining debit and credit {@link BankAccount} (required) 
	 * @param transactionStates - set of transaction states to filter (optional). if null all will be taken
	 * @param bookingTypes - set of booking types to filter (optional). if null all will be taken
	 * @param contractStates - set of contract states to filter (optional). if null all will be taken 
	 * @param targetOnly - only target transactions (optional). if null all will be taken
	 * @param bookingGroupTypes - set of booking group states to filter (optional) if null all will be taken
	 * @param period - period for transaction due date (optional). if null all will be taken
	 * @param orders - set of orders to filter (optional). if null all will be taken. 
	 */
	public BookingSumParameters(
			BankAccountTransferDirection transferDirection, 
    		Set<String> transactionStates, 
    		Set<BookingType> bookingTypes, 
    		Set<String> contractStates, 
    		Boolean targetOnly,
    		Set<String> bookingGroupTypes, 
    		Period period,
    		Set<Order> orders) {
		super();
		Assert.notNull(transferDirection);
		_transferDirection = transferDirection;
    	_transactionStates = transactionStates;
    	_bookingTypes = bookingTypes;
    	_contractStates = contractStates;
    	_targetOnly = targetOnly;
    	_bookingGroupTypes  = bookingGroupTypes;
    	_period = period;
    	_orders = orders;
	}

	public BankAccountTransferDirection getTransferDirection() {
		return _transferDirection;
	}

	public Set<String> getTransactionStates() {
		return _transactionStates;
	}

	public Set<BookingType> getBookingTypes() {
		return _bookingTypes;
	}

	public Set<String> getContractStates() {
		return _contractStates;
	}

	public Boolean getTargetOnly() {
		return _targetOnly;
	}

	public Set<String> getBookingGroupTypes() {
		return _bookingGroupTypes;
	}

	public Period getPeriod() {
		return _period;
	}

	public Set<Order> getOrders() {
		return _orders;
	}
}
