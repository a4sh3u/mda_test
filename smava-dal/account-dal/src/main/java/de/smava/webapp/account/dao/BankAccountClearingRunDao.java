//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Tools\workspace\smava-webapp-parent\account\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Tools\workspace\smava-webapp-parent\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account clearing run)}
import java.util.Collection;
import java.util.Set;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.BankAccountClearingRun;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BankAccountClearingRuns'.
 *
 * @author generator
 */
public interface BankAccountClearingRunDao extends BaseDao<BankAccountClearingRun> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bank account clearing run identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BankAccountClearingRun getBankAccountClearingRun(Long id);

    /**
     * Saves the bank account clearing run.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBankAccountClearingRun(BankAccountClearingRun bankAccountClearingRun);

    /**
     * Deletes an bank account clearing run, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bank account clearing run
     */
    void deleteBankAccountClearingRun(Long id);

    /**
     * Retrieves all 'BankAccountClearingRun' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BankAccountClearingRun> getBankAccountClearingRunList();

    /**
     * Retrieves a page of 'BankAccountClearingRun' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BankAccountClearingRun> getBankAccountClearingRunList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BankAccountClearingRun' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BankAccountClearingRun> getBankAccountClearingRunList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BankAccountClearingRun' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BankAccountClearingRun> getBankAccountClearingRunList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'BankAccountClearingRun' instances.
     */
    long getBankAccountClearingRunCount();


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bank account clearing run)}
    //
    
    /**
     * Searches for all bank account clearing runs that have no delivery date
     * 
     * @return
     */
    Set<Long> findUnsentBankAccountClearingRunIds();
    
    /**
     * Checks whether the given bank account has entries in BankAccountClearingRun that have no delivery date
     * 
     * @param ba
     * @return
     */
    public boolean containsUnsentClearingRun( BankAccount ba); 
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
