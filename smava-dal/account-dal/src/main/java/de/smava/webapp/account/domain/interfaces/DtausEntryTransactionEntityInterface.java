package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.DtausEntry;
import de.smava.webapp.account.domain.Transaction;


/**
 * The domain object that represents 'DtausEntryTransactions'.
 *
 * @author generator
 */
public interface DtausEntryTransactionEntityInterface {

    /**
     * Setter for the property 'dtaus entry'.
     *
     * 
     *
     */
    void setDtausEntry(DtausEntry dtausEntry);

    /**
     * Returns the property 'dtaus entry'.
     *
     * 
     *
     */
    DtausEntry getDtausEntry();
    /**
     * Setter for the property 'transaction'.
     *
     * 
     *
     */
    void setTransaction(Transaction transaction);

    /**
     * Returns the property 'transaction'.
     *
     * 
     *
     */
    Transaction getTransaction();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();

}
