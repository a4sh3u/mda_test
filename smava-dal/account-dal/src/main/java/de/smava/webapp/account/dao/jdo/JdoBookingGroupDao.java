//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking group)}

import de.smava.webapp.account.dao.BookingGroupDao;
import de.smava.webapp.account.dao.BookingInfoQueryResultEntry;
import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.util.FormatUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BookingGroups'.
 *
 * @author generator
 */
@Repository(value = "bookingGroupDao")
public class JdoBookingGroupDao extends JdoBaseDao implements BookingGroupDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBookingGroupDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(booking group)}

    private static final long serialVersionUID = 7436281466369603766L;

    private static final String GROUPED_CONTRACT_COUNT_SQL
        = "select count(distinct b.contract_id), bg.date from booking_group bg\n"
        + "inner join booking_assignment ba on ba.booking_group = bg.id and ba.is_target \n"
        + "inner join booking b on b.id = ba.booking\n"
        + "where bg.date >= :minDate and bg.type in (:bookingGroupTypes) and b.contract_id in (:contracts)\n"
        + "group by bg.date\n"
        + "order by bg.date";

    private static final String BGS_FOR_CONTRACTS_AND_MONTH_SQL
        = "select distinct bg.id as id from booking_group bg\n"
        + "inner join booking_assignment ba on ba.booking_group = bg.id and ba.is_target \n"
        + "inner join booking b on b.id = ba.booking\n"
        + "where bg.date = :month and bg.type in (:bookingGroupTypes) and b.contract_id in (:contracts)";

    private static final String FILTER_BG_SQL
        = "select bg.id as id from \n"
        + "booking_group bg, booking_assignment ba, booking b, contract c \n"
        + "where bg.id = ba.booking_group and b.id = ba.booking	and c.id = b.contract_id \n"
                + "and bg.id in (:bookingGroupIds) and c.id = :contractId ";

    private static final String CANCELD_CONTRACTS_SQL
        = "select distinct bg.id as id from booking_group bg "
        + "inner join booking_assignment ba on ba.booking_group = bg.id "
        + "inner join booking b on b.id = ba.booking "
        + "where bg.type = '" + BookingGroup.TYPE_CONTRACT_CANCELLATION + "' and b.contract_id in (:contracts) ";


    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the booking group identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BookingGroup load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroup - start: id=" + id);
        }
        BookingGroup result = getEntity(BookingGroup.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroup - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BookingGroup getBookingGroup(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            BookingGroup entity = findUniqueEntity(BookingGroup.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the booking group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BookingGroup bookingGroup) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBookingGroup: " + bookingGroup);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(booking group)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bookingGroup);
    }

    /**
     * @deprecated Use {@link #save(BookingGroup) instead}
     */
    public Long saveBookingGroup(BookingGroup bookingGroup) {
        return save(bookingGroup);
    }

    /**
     * Deletes an booking group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the booking group
     */
    public void deleteBookingGroup(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBookingGroup: " + id);
        }
        deleteEntity(BookingGroup.class, id);
    }

    /**
     * Retrieves all 'BookingGroup' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BookingGroup> getBookingGroupList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - start");
        }
        Collection<BookingGroup> result = getEntities(BookingGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BookingGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BookingGroup> getBookingGroupList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - start: pageable=" + pageable);
        }
        Collection<BookingGroup> result = getEntities(BookingGroup.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BookingGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BookingGroup> getBookingGroupList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - start: sortable=" + sortable);
        }
        Collection<BookingGroup> result = getEntities(BookingGroup.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BookingGroup> getBookingGroupList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingGroup> result = getEntities(BookingGroup.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BookingGroup' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingGroup> findBookingGroupList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - start: whereClause=" + whereClause);
        }
        Collection<BookingGroup> result = findEntities(BookingGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BookingGroup' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BookingGroup> findBookingGroupList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BookingGroup> result = findEntities(BookingGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BookingGroup' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingGroup> findBookingGroupList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BookingGroup> result = findEntities(BookingGroup.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BookingGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingGroup> findBookingGroupList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BookingGroup> result = findEntities(BookingGroup.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingGroup> findBookingGroupList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingGroup> result = findEntities(BookingGroup.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingGroup> findBookingGroupList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingGroup> result = findEntities(BookingGroup.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BookingGroup' instances.
     */
    public long getBookingGroupCount() {
        long result = getEntityCount(BookingGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BookingGroup' instances which match the given whereClause.
     */
    public long getBookingGroupCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BookingGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BookingGroup' instances which match the given whereClause together with params specified in object array.
     */
    public long getBookingGroupCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BookingGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingGroupCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(booking group)}
    //

    
    public Collection<BookingGroup> getBookingGroup(String type, String market, Date end) {
        Query q = getPersistenceManager().newNamedQuery(BookingGroup.class, "bookingGroupsForContractAndEnddate");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        params.put("market", market);        
        params.put("end", end);     
        
        @SuppressWarnings("unchecked")    
        Collection<BookingGroup> result = (Collection<BookingGroup>) q.executeWithMap(params);
        
        return result;
    }

    public Collection<BookingGroup> getBookingGroups(String type) {
        return findBookingGroupList("_type=='" + type + "'");
    }
    
    public Collection<BookingGroup> getContractCancellationBookingGroups(Long mainOrderId, Date objectionDate) {
        Query q = getPersistenceManager().newNamedQuery(BookingGroup.class, "contractCancellationBookingGroups");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", BookingGroup.TYPE_CONTRACT_CANCELLATION);
        params.put("objectionDate", objectionDate);        
        params.put("name", BookingGroup.TYPE_CONTRACT_CANCELLATION + "_" + mainOrderId);     
        
        @SuppressWarnings("unchecked")    
        Collection<BookingGroup> result = (Collection<BookingGroup>) q.executeWithMap(params);
        
        return result;


    }

    @SuppressWarnings("unchecked")
    public Collection<BookingInfoQueryResultEntry> getGroupedBookingInfosQueryResults(Collection<Long> bookingGroupIds, Collection<Long> contractIds, Collection<Long> interimAccounts) {
        StringBuilder contractsQuery = new StringBuilder();
        StringBuilder bookingGroupsQuery = new StringBuilder();
        StringBuilder interimAccountsQuery = new StringBuilder();
        String or = " or ";

        Map<Integer, Object> argsMap = new HashMap<Integer, Object>();
        int paramIndex = 0;

        if (bookingGroupIds != null && bookingGroupIds.size() > 0) {
            bookingGroupsQuery.append("and (");
            for (Long id : bookingGroupIds) {
                argsMap.put(++paramIndex, id);
                bookingGroupsQuery.append("bg.id = ?");
                bookingGroupsQuery.append(or);
            }
            bookingGroupsQuery.replace(bookingGroupsQuery.lastIndexOf(or), bookingGroupsQuery.length(), ") ");
        }

        if (contractIds != null && contractIds.size() > 0) {
            contractsQuery.append("and (");
            for (Long id : contractIds) {
                argsMap.put(++paramIndex, id);
                contractsQuery.append("c.id = ?");
                contractsQuery.append(or);
            }
            contractsQuery.replace(contractsQuery.lastIndexOf(or), contractsQuery.length(), ") ");
        }

        if (interimAccounts != null && interimAccounts.size() > 0) {
            interimAccountsQuery.append("and (");
            for (Long interimId : interimAccounts) {
                argsMap.put(++paramIndex, interimId);
                argsMap.put(++paramIndex, interimId);
                interimAccountsQuery.append("t.credit_account = ? or t.debit_account = ?");
                interimAccountsQuery.append(or);
            }
            interimAccountsQuery.replace(interimAccountsQuery.lastIndexOf(or), interimAccountsQuery.length(), ") ");
        }

        String query = "select sum(b.amount) as \"amountSum\", b.type_new as \"bookingTypeInteger\", debit_bankaccount.type as \"debitBankAccountType\", credit_bankaccount.type as \"creditBankAccountType\", t.credit_account as \"creditAccount\", t.debit_account as \"debitAccount\", bg.id as \"bookingGroupId\", t.state as \"transactionState\", t.id as \"transactionId\", c.id as \"contractId\", ba.is_target as \"target\" " // , t.transaction_date as \"transactionDate\"
                + "from contract c inner join booking b on c.id = b.contract_id "
                    + "inner join booking_assignment ba on ba.booking = b.id "
                    + "inner join booking_group bg on bg.id = ba.booking_group "
                    + "inner join \"transaction\" t on b.transaction_id = t.id "
                    + "inner join bank_account credit_bankaccount on (t.credit_account = credit_bankaccount.id) "
                    + "inner join bank_account debit_bankaccount on (t.debit_account = debit_bankaccount.id) "
                + "where 1=1 " //
                    + "and t.state in ('OPEN', 'SUCCESSFUL', 'FAILED', 'VOID') " // WEBSITE-3338
                    + bookingGroupsQuery
                    + contractsQuery
                    + interimAccountsQuery
                + "group by b.type_new, debit_bankaccount.type, credit_bankaccount.type, bg.id, t.state, t.id, t.credit_account, t.debit_account, c.id, ba.is_target "
                + "order by c.id, t.id asc";

        Query q = null;

        q = getPersistenceManager().newQuery("javax.jdo.query.SQL", query);
        q.setResultClass(BookingInfoQueryResultEntry.class);
        q.setIgnoreCache(false);

        Collection<BookingInfoQueryResultEntry> res = (Collection<BookingInfoQueryResultEntry>) q.executeWithMap(argsMap);

        return res;
    }

    public Collection<BookingGroup> findBookingGroupsById(Set<Long> bookingGroupIds) {
        final Collection<BookingGroup> result;
        if (CollectionUtils.isNotEmpty(bookingGroupIds)) {
            Map<Integer, Object> params = new HashMap<Integer, Object>();

            StringBuilder sql = new StringBuilder("select id from booking_group where id in (");
            sql.append(createBookingGroupIdsList(bookingGroupIds, params));
            sql.append(")");

            final Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
            q.setClass(BookingGroup.class);

            result = (Collection<BookingGroup>) q.executeWithMap(params);
        } else {
            result = Collections.EMPTY_LIST;
        }
        return result;
    }

    private String createBookingGroupIdsList(Set<Long> bookingGroupIds, Map<Integer, Object> params) {
        StringBuilder seq = new StringBuilder();
        for (Long id : bookingGroupIds) {
            seq.append("?,");
            params.put(params.size() + 1, id);
        }

        return seq.substring(0, seq.length() - 1);
    }

    @Override
    public List<Object[]> getBookingGroupContractCountAndDate(Set<Contract> contracts,
            Date from, Set<String> types) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("minDate", new Timestamp(from.getTime()));
        params.put("contracts", contracts);
        params.put("bookingGroupTypes", types);
        final String sql = replaceCollectionTypes(params, GROUPED_CONTRACT_COUNT_SQL);
        final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setResultClass(Object[].class);
        return (List<Object[]>) query.executeWithMap(params);
    }

    @Override
    public Set<BookingGroup> getBookingGroups(Set<Contract> contracts, Date month, Set<String> types) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("month", new Timestamp(month.getTime()));
        params.put("contracts", contracts);
        params.put("bookingGroupTypes", types);
        final String sql = replaceCollectionTypes(params, BGS_FOR_CONTRACTS_AND_MONTH_SQL);
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setClass(BookingGroup.class);
        return new LinkedHashSet<BookingGroup>((List<BookingGroup>) q.executeWithMap(params));
    }

        @Override
    public Set<BookingGroup> getBookingGroups(Contract contract, Collection<BookingGroup> bookingGroups) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("contractId", contract.getId());
                params.put("bookingGroupIds", getIds(bookingGroups));
                
        final String sql = replaceCollectionTypes(params, FILTER_BG_SQL);
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setClass(BookingGroup.class);
        return new LinkedHashSet<BookingGroup>((List<BookingGroup>) q.executeWithMap(params));
    }

    public Collection<BookingGroup> findContractCancellationBookingGroupsForContracts(
            final Collection<Contract> contracts) {
        final Map<String, Object> params = getSingleParamMap("contracts", contracts);
        final String sql = replaceCollectionTypes(params, CANCELD_CONTRACTS_SQL);
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setClass(BookingGroup.class);
        return (Collection<BookingGroup>) q.executeWithMap(params);
    }

    public Collection<BookingGroup> findUnprocessedInsuranceBookingGroupsForMonth(Date date){


        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM");
        String datePart = sdf.format(date);

        String query = "SELECT booking_group.* FROM booking_group INNER JOIN insurancepool_payout_run ON booking_group.id = insurancepool_payout_run.booking_group_id WHERE " +
                        "booking_group.type = 'INSURANCE' and booking_group.date = '" + datePart + "-01' ";
        ;

        Query q  = getPersistenceManager().newQuery("javax.jdo.query.SQL", query);
        q.setResultClass(BookingGroup.class);

        return null;
    }


    public BookingGroup findCorrespondingBookingGroup(final Date date, final Order order){
        final Collection<BookingGroup> bookingGroups = findBookingGroupList("this._type == :type && this._date == :date && this._market == :market", BookingGroup.TYPE_INSURANCE_GROUP, new Timestamp(date.getTime()), order.getMarket());
        if (bookingGroups.size() != 1) {
            throw new IllegalStateException("schould be exactly one booking group here but we get " + bookingGroups.size());
        }
        return bookingGroups.iterator().next();
    }

    public Collection<BookingGroup> getBookingGroup(String bookingGroupType, Date date, String specifier, Locale locale, boolean legalMarket){
        // Properties for possible new BookingGroup...
        String groupName;
        String groupMarket = "";
        String groupSpecifier = "";

        // Create query for booking group...
//        Date monthDate = FormatUtils.parseMonthDateFormat(FormatUtils.formatMonthDateFormat(date, getConfigurationService().getCurrentLocale())); // don't really make sense
//        we need the first of the month, so we get it cleaner
        Date monthDate = DateUtils.getDateStartOfMonth(date);
        OqlTerm oql = OqlTerm.newTerm();
        OqlTerm typeTerm = oql.equals("_type", bookingGroupType);
        OqlTerm dateTerm = oql.equals("_date", monthDate);
        OqlTerm specTerm;
        if (BookingGroup.TYPE_INSURANCE_GROUP.equals(bookingGroupType) || BookingGroup.TYPE_REPAYMENT_BREAK.equals(bookingGroupType)) {

            if (!legalMarket) {
                // Actually in this case the legal market check should already have thrown an exception but to be sure:
                throw new RuntimeException("Illegal market code " + specifier);
            }
            specTerm = oql.equals("_market", specifier);
            groupMarket = specifier;
            groupName = FormatUtils.formatMonthDateFormat(date, locale) + "|" + specifier;
        } else if (BookingGroup.TYPE_SMAVA_GROUP.equals(bookingGroupType)) {
            specTerm = oql.equals("_specifier", specifier);
            groupSpecifier = specifier;
            groupName = FormatUtils.formatMonthDateFormat(date, locale) + "|Groupfee " + specifier;
        } else if (BookingGroup.TYPE_CREDIT_PAYOUT.equals(bookingGroupType)) {
            specTerm = oql.equals("_market", specifier);
            groupMarket = specifier;
            groupSpecifier = specifier;
            groupName = FormatUtils.formatMonthDateFormat(date, locale) + "|Payout " + specifier;
        } else {
            throw new UnsupportedOperationException("Unsupported booking group type: " + bookingGroupType);
        }
        if (specTerm == null) {
            oql.and(typeTerm, dateTerm);
        } else {
            oql.and(typeTerm, dateTerm, specTerm);
        }

        // Look for groups now...
        return findBookingGroupList(oql.toString(), monthDate);
    }

    public Collection<BookingGroup> getBookingGroups(Date myDate) {
        String whereClause = "this._type == '" + BookingGroup.TYPE_INSURANCE_GROUP + "' && this._date == :myDate order by this._market asc";

        LOGGER.info("WHERE" + whereClause);

        return this.findBookingGroupList(whereClause, myDate);
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
