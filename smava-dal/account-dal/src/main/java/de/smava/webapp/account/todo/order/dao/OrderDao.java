//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.order.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(order)}

import java.io.Serializable;
import java.util.*;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccountProvider;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.Market;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.todo.order.AdvancedOrderInfo;
import de.smava.webapp.account.todo.order.OrderInfo;
import de.smava.webapp.account.todo.util.SortAndPageable;
import de.smava.webapp.account.todo.web.PageableCommand;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Orders'.
 *
 * @author generator
 */
public interface OrderDao extends BaseDao<Order>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the order identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Order getOrder(Long id);

    /**
     * Saves the order.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveOrder(Order order);

    /**
     * Deletes an order, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the order
     */
    void deleteOrder(Long id);

    /**
     * Retrieves all 'Order' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Order> getOrderList();

    /**
     * Retrieves a page of 'Order' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Order> getOrderList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Order' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Order> getOrderList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Order' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Order> getOrderList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Order' instances.
     */
    long getOrderCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(order)}
    /**
     * Attaches a Category to the Order instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachCategory(Order order, Category category);
    
    void detachCategory(Order order, Category category);

    /**
     * Helper methods for the order book.
     */
    Collection<Order> findOrderList(Account borrower, Collection<String> states);
    long getCreditProjectListCount(Account borrower, Collection<String> states);
    
    Collection<Order> findOrderList(String market, Collection<String> states);
    Collection<Order> findOrderList(String market, Collection<String> states, Pageable pageable, Sortable sortable);

    /**
     *
     * Find all orders that belong together, connected by groupId.
     * If states == null, all orders of the group will be returned.
     *
     * @param order
     * @param states
     * @return
     */
    Collection<Order> findOrdersOfSameGroup(Order order, Collection<String> states);
    Collection<Order> findCurrentOpenOrders();
    

    Collection<Order> findOrderList(Account account, Pageable pageable, Sortable sortable);
    
    Collection<Order> findTopLevelOrders(Account account, Collection<String> states, Sortable sortable);

    /**
     * That is a special method for identifying orders in state deleted.
     * Currently deletion can be done manually by user. Those records are loaded here. Furthermore orders
     * are also deleted automatically by casi.
     *
     * Those orders typically does not have a activation date, why here all deleted orders with activation
     * date are loaded.
     *
     * see: http://intranet.smava.de/jira/browse/WEBSITE-13656
     *
     * @param account
     * @param sortable
     * @return
     */
    Collection<Order> findDeletedTopLevelOrders(Account account,Sortable sortable);


    boolean exists(Long orderId);
    


    Collection<Order> findOrders(BidSearch bidSearch, int maxOrderDuration,double cfgMinAmount, double cfgMaxAmount);
    Collection<Order> findOrders(BidSearch bidSearch, int maxOrderDuration, Order order, BankAccountProvider bankAccountProvider, double cfgMinAmount, double cfgMaxAmount);
    int getOrderCount(BidSearch bidSearch, int maxOrderDuration, double cfgMinAmount, double cfgMaxAmount);
    int getOrderCount(BidSearch bidSearch, int maxOrderDuration, Order order,double cfgMinAmount, double cfgMaxAmount);

    

	Order getMainOrder(Order order);


	
	SortAndPageable<OrderInfo> getOrderInfos(SortAndPageable<OrderInfo> input, Date now, Date recentlyDate, int maxOrderDuration, boolean archiveOnly, Category category);
	
	List<AdvancedOrderInfo> getRecentlyFinancedAndCurrentOpenAdvancedOrderInfos(Date now, Date recentlyDate, String sortcolumn, boolean ascending, int offset, int pageCount);
	
	public List<AdvancedOrderInfo> findAdvancedOrders(BidSearch bidSearch, int maxOrderDuration, final String sortcolumn, final boolean ascending, double cfgMinAmount, double cfgMaxAmount);
	
	double getOriginalAmount(Order order);
	double getMatchedAmount(Order order);
	
	List<OrderInfo> getOrderInfosForGroup(Long groupId);
	

	
	Set<Long> getUpdateRoiOrders(final Set<String> states);

    
    /**
     * returns the internal debt of the given account
     * @param account
     * @return
     */
    Set<Order> getInternalDebConsolidationRelevantMainOrders(Account account);

    /**
     * returns the number of orders that need to be payed back and have the flag internal_debt_consolidation set.
     * @param account
     * @return
     */
	Long countInternalDebtConsolidationOrders(Account account);

    Collection<Order> getOrdersWithOpenOrQueueStateInDates(Calendar before, Calendar after);

    Collection<Order> findOrdersOfStateOlderThan(String state, Date date);

    Collection<Order> findOrdersByBorrowers(List<Account> borrowers, Pageable pageable, Sortable sortable);

    Collection<Order> findByRateMarketCodes(String rateMarketCodes, Date orderDate);

    Collection<Order> getExpiredOrders(Account account);

    // !!!!!!!! End of insert code section !!!!!!!!

}
