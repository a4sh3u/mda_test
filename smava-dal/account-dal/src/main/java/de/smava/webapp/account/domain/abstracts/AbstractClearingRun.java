package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.ClearingRunEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'ClearingRuns'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractClearingRun
    extends KreditPrivatEntity implements ClearingRunEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractClearingRun.class);

}

