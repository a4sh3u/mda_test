package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.DocumentAttachmentContainerEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'DocumentAttachmentContainers'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractDocumentAttachmentContainer
    extends KreditPrivatEntity implements DocumentAttachmentContainerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDocumentAttachmentContainer.class);

    public static final String STATE_OPEN = "OPEN";
    public static final String STATE_PROCESSED = "PROCESSED";
    public static final String STATE_RELEASED = "RELEASED";
    public static final String STATE_DELETED = "DELETED";

    public static final String DIRECTION_IN  = "IN";
    public static final String DIRECTION_OUT = "OUT";

}

