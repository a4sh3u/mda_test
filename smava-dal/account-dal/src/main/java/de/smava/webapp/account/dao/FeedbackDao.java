//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(feedback)}

import java.util.Collection;
import java.util.Date;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.Feedback;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

/**
 * Remove this as it has been replaced by ekomi
 */
@Deprecated
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Feedbacks'.
 *
 * @author generator
 */
public interface FeedbackDao extends BaseDao<Feedback> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the feedback identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Feedback getFeedback(Long id);

    /**
     * Saves the feedback.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveFeedback(Feedback feedback);

    /**
     * Deletes an feedback, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the feedback
     */
    void deleteFeedback(Long id);

    /**
     * Retrieves all 'Feedback' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Feedback> getFeedbackList();

    /**
     * Retrieves a page of 'Feedback' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Feedback> getFeedbackList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Feedback' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Feedback> getFeedbackList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Feedback' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Feedback> getFeedbackList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'Feedback' instances.
     */
    long getFeedbackCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(feedback)}
    //
    // insert custom methods here
    //
	
    boolean getFeedbackAlllowedForOrder(Long orderId, Long accountId, Date startDate);
    
    // !!!!!!!! End of insert code section !!!!!!!!
}
