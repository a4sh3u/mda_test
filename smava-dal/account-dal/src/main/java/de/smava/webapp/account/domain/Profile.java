package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.ProfileHistory;

import java.util.Date;

/**
 * The domain object that represents 'Profiles'.
 */
public class Profile extends ProfileHistory  implements ImageAwareEntity {

        protected String _hobbies;
        protected String _description;
        protected String _image;
        protected int _imageWidth;
        protected int _imageHeight;
        protected Account _account;
        protected Date _changeDate;
        protected String _state;

                            /**
     * Setter for the property 'hobbies'.
     */
    public void setHobbies(String hobbies) {
        if (!_hobbiesIsSet) {
            _hobbiesIsSet = true;
            _hobbiesInitVal = getHobbies();
        }
        registerChange("hobbies", _hobbiesInitVal, hobbies);
        _hobbies = hobbies;
    }

    /**
     * Returns the property 'hobbies'.
     */
    public String getHobbies() {
        return _hobbies;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }

    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'image'.
     */
    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = getImage();
        }
        registerChange("image", _imageInitVal, image);
        _image = image;
    }
                        
    /**
     * Returns the property 'image'.
     */
    public String getImage() {
        return _image;
    }
                                    /**
     * Setter for the property 'image width'.
     */
    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = getImageWidth();
        }
        registerChange("image width", _imageWidthInitVal, imageWidth);
        _imageWidth = imageWidth;
    }
                        
    /**
     * Returns the property 'image width'.
     */
    public int getImageWidth() {
        return _imageWidth;
    }
                                    /**
     * Setter for the property 'image height'.
     */
    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = getImageHeight();
        }
        registerChange("image height", _imageHeightInitVal, imageHeight);
        _imageHeight = imageHeight;
    }
                        
    /**
     * Returns the property 'image height'.
     */
    public int getImageHeight() {
        return _imageHeight;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'change date'.
     */
    public void setChangeDate(Date changeDate) {
        if (!_changeDateIsSet) {
            _changeDateIsSet = true;
            _changeDateInitVal = getChangeDate();
        }
        registerChange("change date", _changeDateInitVal, changeDate);
        _changeDate = changeDate;
    }
                        
    /**
     * Returns the property 'change date'.
     */
    public Date getChangeDate() {
        return _changeDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Profile.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _hobbies=").append(_hobbies);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _image=").append(_image);
            builder.append("\n    _imageWidth=").append(_imageWidth);
            builder.append("\n    _imageHeight=").append(_imageHeight);
            builder.append("\n    _state=").append(_state);
            builder.append("\n}");
        } else {
            builder.append(Profile.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
