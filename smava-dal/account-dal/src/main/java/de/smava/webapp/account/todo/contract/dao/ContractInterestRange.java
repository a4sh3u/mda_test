package de.smava.webapp.account.todo.contract.dao;

/**
 * Holds avg, min, max of interest rates.
 * <p/>
 * Date: 13.07.2006
 *
 * @author ingmar.decker
 */
public class ContractInterestRange {
    private final Float _avg;
    private final Float _min;
    private final Float _max;

    public ContractInterestRange(Float min, Float avg, Float max) {
        _avg = avg;
        _max = max;
        _min = min;
    }

    public Float getAvg() {
        return _avg;
    }

    public Float getMin() {
        return _min;
    }

    public Float getMax() {
        return _max;
    }
}