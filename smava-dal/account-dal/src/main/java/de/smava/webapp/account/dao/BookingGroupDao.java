//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse-jee-ganymede\eclipse\workspace\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse-jee-ganymede\eclipse\workspace\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking group)}

import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BookingGroups'.
 *
 * @author generator
 */
public interface BookingGroupDao extends BaseDao<BookingGroup>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the booking group identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BookingGroup getBookingGroup(Long id);

    /**
     * Saves the booking group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBookingGroup(BookingGroup bookingGroup);

    /**
     * Deletes an booking group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the booking group
     */
    void deleteBookingGroup(Long id);

    /**
     * Retrieves all 'BookingGroup' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BookingGroup> getBookingGroupList();

    /**
     * Retrieves a page of 'BookingGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BookingGroup> getBookingGroupList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BookingGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BookingGroup> getBookingGroupList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BookingGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BookingGroup> getBookingGroupList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'BookingGroup' instances.
     */
    long getBookingGroupCount();




    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(booking group)}
    //

    Collection<BookingGroup> getBookingGroup(String type, String market, Date end);
    
    Collection<BookingGroup> getBookingGroups(String type);


    Collection<BookingGroup> getContractCancellationBookingGroups(Long mainOrderId, Date objectionDate);
    
    Collection<BookingInfoQueryResultEntry> getGroupedBookingInfosQueryResults(Collection<Long> bookingGroupIds, Collection<Long> contractIds, Collection<Long> interimAccounts);
    
    Collection<BookingGroup> findBookingGroupsById(Set<Long> bookingGroupIds);
    
    List<Object[]> getBookingGroupContractCountAndDate(Set<Contract> contracts, Date from, Set<String> types);
    
    Set<BookingGroup> getBookingGroups(Set<Contract> contracts, Date from, Set<String> types);
    Set<BookingGroup> getBookingGroups(Contract contract, Collection<BookingGroup> bookingGroups);
    
    Collection<BookingGroup> findContractCancellationBookingGroupsForContracts(Collection<Contract> contracts);
    
    Collection<BookingGroup> findUnprocessedInsuranceBookingGroupsForMonth(Date date);


    public BookingGroup findCorrespondingBookingGroup(final Date date, final Order order);

    public Collection<BookingGroup> getBookingGroup(String bookingGroupType, Date date, String specifier, Locale locale, boolean legalMarket);

    public Collection<BookingGroup> getBookingGroups(Date myDate);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
