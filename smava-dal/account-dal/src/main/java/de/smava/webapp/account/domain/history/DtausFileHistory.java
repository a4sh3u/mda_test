package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractDtausFile;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'DtausFiles'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DtausFileHistory extends AbstractDtausFile {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _dueDateInitVal;
    protected transient boolean _dueDateIsSet;
    protected transient String _transactionsTypeInitVal;
    protected transient boolean _transactionsTypeIsSet;
    protected transient Date _releaseDateInitVal;
    protected transient boolean _releaseDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _directionInitVal;
    protected transient boolean _directionIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient String _noteInitVal;
    protected transient boolean _noteIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'due date'.
     */
    public Date dueDateInitVal() {
        Date result;
        if (_dueDateIsSet) {
            result = _dueDateInitVal;
        } else {
            result = getDueDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'due date'.
     */
    public boolean dueDateIsDirty() {
        return !valuesAreEqual(dueDateInitVal(), getDueDate());
    }

    /**
     * Returns true if the setter method was called for the property 'due date'.
     */
    public boolean dueDateIsSet() {
        return _dueDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'transactions type'.
     */
    public String transactionsTypeInitVal() {
        String result;
        if (_transactionsTypeIsSet) {
            result = _transactionsTypeInitVal;
        } else {
            result = getTransactionsType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'transactions type'.
     */
    public boolean transactionsTypeIsDirty() {
        return !valuesAreEqual(transactionsTypeInitVal(), getTransactionsType());
    }

    /**
     * Returns true if the setter method was called for the property 'transactions type'.
     */
    public boolean transactionsTypeIsSet() {
        return _transactionsTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'release date'.
     */
    public Date releaseDateInitVal() {
        Date result;
        if (_releaseDateIsSet) {
            result = _releaseDateInitVal;
        } else {
            result = getReleaseDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'release date'.
     */
    public boolean releaseDateIsDirty() {
        return !valuesAreEqual(releaseDateInitVal(), getReleaseDate());
    }

    /**
     * Returns true if the setter method was called for the property 'release date'.
     */
    public boolean releaseDateIsSet() {
        return _releaseDateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'direction'.
     */
    public String directionInitVal() {
        String result;
        if (_directionIsSet) {
            result = _directionInitVal;
        } else {
            result = getDirection();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'direction'.
     */
    public boolean directionIsDirty() {
        return !valuesAreEqual(directionInitVal(), getDirection());
    }

    /**
     * Returns true if the setter method was called for the property 'direction'.
     */
    public boolean directionIsSet() {
        return _directionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'note'.
     */
    public String noteInitVal() {
        String result;
        if (_noteIsSet) {
            result = _noteInitVal;
        } else {
            result = getNote();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'note'.
     */
    public boolean noteIsDirty() {
        return !valuesAreEqual(noteInitVal(), getNote());
    }

    /**
     * Returns true if the setter method was called for the property 'note'.
     */
    public boolean noteIsSet() {
        return _noteIsSet;
    }
		
}
