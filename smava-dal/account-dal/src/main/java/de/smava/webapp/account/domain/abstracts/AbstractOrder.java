package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.BidEntityInterface;
import de.smava.webapp.account.domain.interfaces.OrderEntityInterface;
import de.smava.webapp.commons.constants.StringConstants;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'Orders'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractOrder
    extends Bid     implements MarketingTrackingEntity, ImageAwareEntity,OrderEntityInterface    ,BidEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractOrder.class);

    private static final long serialVersionUID = 6677460267724663830L;

    public static final String FETCH_GROUP_BORROWER = "Borrower";
    public static final int INTERESTING_STATE_NONE = 0;
    public static final int INTERESTING_STATE_TRUE = 1;
    public static final int INTERESTING_STATE_SPECIAL = 2;

    public Order split(double newAmount) {
        Order splitOrder = new Order(this.instance());
        splitOrder.setId(null); //Mark as new offer
        splitOrder.setAmount(newAmount);
        setAmount(getAmount() - newAmount);
        final Collection<EconomicalData> eds = getEconomicalDatas();
        if (eds != null) {
            Collection<EconomicalData> toAdd = new HashSet<EconomicalData>();
            if (getCurrentEconomicalData() != null){
                toAdd.add(getCurrentEconomicalData());
            }
            splitOrder.setEconomicalDatas(toAdd);
        }

        return splitOrder;
    }

    public String getFullImagePath(String imagePath) {
        return imagePath + "/" + getId();
    }

    public boolean isGroupOrder() {
        return !isMasterOfGroup();
    }

    public boolean isMainOrder() {
        return isMasterOfGroup();
    }

    public Float getRate() {
        BidInterest currentBidInterest = getCurrentBidInterest();
        return currentBidInterest.getRate();
    }

    public String getMarket() {
        BidInterest currentBidInterest = getCurrentBidInterest();
        // there should be only one market and one rate (orders only)
        return currentBidInterest.getMarketName();
    }

    // for jstl
    public String getMarketLetter() {
        return getMarket().substring(0, 1);
    }

    public void addRdiContract(RdiContract rdiContract) {
        if (getRdiContracts() == null) {
            setRdiContracts(new ArrayList<RdiContract>());
        }

        RdiContract lastContract = getRdiContract();
        if (lastContract != null) {
            lastContract.setExpirationDate(new Date(rdiContract.getCreationDate().getTime()));
            if (rdiContract.getTermsAndConditionsAcceptanceDate() == null && lastContract.getTermsAndConditionsAcceptanceDate() != null) {
                rdiContract.setTermsAndConditionsAcceptanceDate(new Date(lastContract.getTermsAndConditionsAcceptanceDate().getTime()));
            }
        }
        rdiContract.setOrder(this.instance());
        getRdiContracts().add(rdiContract);
    }

    public RdiContract getRdiContract() {
        RdiContract result = null;
        if (getRdiContracts() != null) {
            if (!getRdiContracts().isEmpty()) {
                RdiContract[] rdiContracts = getRdiContracts().toArray(new RdiContract[getRdiContracts().size()]);
                result = rdiContracts[rdiContracts.length - 1];
            }
        }
        return result;
    }

    public String getCurrentRdiContractType() {
        String result = StringConstants.RDI_CONTRACT_DENIAL_KEY;
        final RdiContract rdiContract = getCurrentRdiContract();
        if (rdiContract != null) {
            result = rdiContract.getType();
        }
        return result;
    }

    public RdiContract getCurrentRdiContract() {
        return getCurrentRdiContract(CurrentDate.getDate());
    }

    public RdiContract getCurrentRdiContract(Date atTime) {
        RdiContract rdiContract = null;
        if (getRdiContracts() != null && !getRdiContracts().isEmpty()) {
            // iterate to last rdi contract (sorted by rdi_contract.order_ordering) ...
            for (RdiContract c : getRdiContracts()) {
                rdiContract = c;
            }

            // drop result when expired
            if (rdiContract.isExpired(atTime)) {
                rdiContract = null;
            }
        }

        return rdiContract;
    }

    public Set<Category> getCategories() {
        Set<Category> result = new HashSet<Category>();
        for (CategoryOrder catOrder : getCategoryOrders()) {
            if (catOrder != null) {
                result.add(catOrder.getCategory());
            }
        }

        return result;
    }

    public void setCategories(Set<Category> categories) {
        Collection<CategoryOrder> categoryOrders = new HashSet<CategoryOrder>();
        for (Category category : categories) {
            CategoryOrder categoryOrder = new CategoryOrder();
            categoryOrder.setCategory(category);
            categoryOrder.setOrder(this.instance());

            categoryOrders.add(categoryOrder);
        }
        setCategoryOrders(categoryOrders);
    }

    public CategoryOrder getMainCategoryOrder() {
        CategoryOrder result = null;
        for (CategoryOrder catOrder : getCategoryOrders()) {
            if (catOrder.getMain()) {
                result = catOrder;
                break;
            }
        }

        return result;
    }

    public CategoryOrder getSecondaryCategoryOrder() {
        CategoryOrder result = null;
        for (CategoryOrder catOrder : getCategoryOrders()) {
            if (!catOrder.getMain()) {
                result = catOrder;
                break;
            }
        }

        return result;
    }

    public void addDocument(Document document) {
        if (getDocuments() == null) {
            Collection<Document> documents = new LinkedList<Document>();
            setDocuments(documents);
        }

        getDocuments().add(document);
    }

    public BankAccount getInternalBankAccount() {
        return getAccount().getInternalBankAccount(getBankAccountProvider());
    }

    public BankAccount getOfferInternalBankAccount(){
        if (null == getContract() ||
                null == getContract().getOffer() ||
                null == getContract().getOffer().getAccount())
            return null;

        return getContract().getOffer().getAccount().getInternalBankAccount(
                getBankAccountProvider());
    }

    public EconomicalData getCurrentEconomicalData(){
        EconomicalData ret = null;
        for ( EconomicalData ed : getEconomicalDatas()){
            if ( ret == null ||
                    ret.getCreationDate().before(ed.getCreationDate())){
                ret = ed;
            }
        }
        return ret;
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        if (getAccount() != null) {
            list.add(getAccount());
        }
        return list;
    }

    public abstract Order instance();

}

