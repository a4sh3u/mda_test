package de.smava.webapp.account.domain.interfaces;


import java.util.Map;


/**
 * The domain object that represents 'Ratings'.
 *
 * @author generator
 */
public interface RatingEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'failure rate'.
     *
     * 
     *
     */
    void setFailureRate(double failureRate);

    /**
     * Returns the property 'failure rate'.
     *
     * 
     *
     */
    double getFailureRate();
    /**
     * Setter for the property 'group bonus rate'.
     *
     * 
     *
     */
    void setGroupBonusRate(double groupBonusRate);

    /**
     * Returns the property 'group bonus rate'.
     *
     * 
     *
     */
    double getGroupBonusRate();
    /**
     * Setter for the property 'credit discounts'.
     *
     * 
     *
     */
    void setCreditDiscounts(Map<Integer, Double> creditDiscounts);

    /**
     * Returns the property 'credit discounts'.
     *
     * 
     *
     */
    Map<Integer, Double> getCreditDiscounts();
    /**
     * Setter for the property 'interest reductions'.
     *
     * 
     *
     */
    void setInterestReductions(Map<Integer, Double> interestReductions);

    /**
     * Returns the property 'interest reductions'.
     *
     * 
     *
     */
    Map<Integer, Double> getInterestReductions();

}
