//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(category image)}
import de.smava.webapp.account.domain.history.CategoryImageHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CategoryImages'.
 *
 * @author generator
 */
public class CategoryImage extends CategoryImageHistory  implements ImageAwareEntity {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(category image)}
	private static final long serialVersionUID = -8069274622145452073L;

    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _image;
        protected int _imageHeight;
        protected int _imageWidth;
        
                            /**
     * Setter for the property 'image'.
     */
    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = getImage();
        }
        registerChange("image", _imageInitVal, image);
        _image = image;
    }
                        
    /**
     * Returns the property 'image'.
     */
    public String getImage() {
        return _image;
    }
                                    /**
     * Setter for the property 'image height'.
     */
    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = getImageHeight();
        }
        registerChange("image height", _imageHeightInitVal, imageHeight);
        _imageHeight = imageHeight;
    }
                        
    /**
     * Returns the property 'image height'.
     */
    public int getImageHeight() {
        return _imageHeight;
    }
                                    /**
     * Setter for the property 'image width'.
     */
    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = getImageWidth();
        }
        registerChange("image width", _imageWidthInitVal, imageWidth);
        _imageWidth = imageWidth;
    }
                        
    /**
     * Returns the property 'image width'.
     */
    public int getImageWidth() {
        return _imageWidth;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CategoryImage.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _image=").append(_image);
            builder.append("\n    _imageHeight=").append(_imageHeight);
            builder.append("\n    _imageWidth=").append(_imageWidth);
            builder.append("\n}");
        } else {
            builder.append(CategoryImage.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CategoryImage asCategoryImage() {
        return this;
    }
}
