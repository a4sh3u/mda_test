//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score token)}

import de.smava.webapp.account.dao.SchufaScoreTokenDao;
import de.smava.webapp.account.domain.SchufaScoreToken;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'SchufaScoreTokens'.
 *
 * @author generator
 */
@Repository(value = "schufaScoreTokenDao")
public class JdoSchufaScoreTokenDao extends JdoBaseDao implements SchufaScoreTokenDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSchufaScoreTokenDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(schufa score token)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the schufa score token identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public SchufaScoreToken load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreToken - start: id=" + id);
        }
        SchufaScoreToken result = getEntity(SchufaScoreToken.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreToken - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public SchufaScoreToken getSchufaScoreToken(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	SchufaScoreToken entity = findUniqueEntity(SchufaScoreToken.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the schufa score token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(SchufaScoreToken schufaScoreToken) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveSchufaScoreToken: " + schufaScoreToken);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(schufa score token)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(schufaScoreToken);
    }

    /**
     * @deprecated Use {@link #save(SchufaScoreToken) instead}
     */
    public Long saveSchufaScoreToken(SchufaScoreToken schufaScoreToken) {
        return save(schufaScoreToken);
    }

    /**
     * Deletes an schufa score token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa score token
     */
    public void deleteSchufaScoreToken(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteSchufaScoreToken: " + id);
        }
        deleteEntity(SchufaScoreToken.class, id);
    }

    /**
     * Retrieves all 'SchufaScoreToken' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<SchufaScoreToken> getSchufaScoreTokenList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - start");
        }
        Collection<SchufaScoreToken> result = getEntities(SchufaScoreToken.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'SchufaScoreToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<SchufaScoreToken> getSchufaScoreTokenList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - start: pageable=" + pageable);
        }
        Collection<SchufaScoreToken> result = getEntities(SchufaScoreToken.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScoreToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<SchufaScoreToken> getSchufaScoreTokenList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - start: sortable=" + sortable);
        }
        Collection<SchufaScoreToken> result = getEntities(SchufaScoreToken.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<SchufaScoreToken> getSchufaScoreTokenList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreToken> result = getEntities(SchufaScoreToken.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScoreToken' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreToken> findSchufaScoreTokenList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - start: whereClause=" + whereClause);
        }
        Collection<SchufaScoreToken> result = findEntities(SchufaScoreToken.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScoreToken' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<SchufaScoreToken> findSchufaScoreTokenList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<SchufaScoreToken> result = findEntities(SchufaScoreToken.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'SchufaScoreToken' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreToken> findSchufaScoreTokenList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<SchufaScoreToken> result = findEntities(SchufaScoreToken.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScoreToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreToken> findSchufaScoreTokenList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<SchufaScoreToken> result = findEntities(SchufaScoreToken.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreToken> findSchufaScoreTokenList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreToken> result = findEntities(SchufaScoreToken.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreToken> findSchufaScoreTokenList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreToken> result = findEntities(SchufaScoreToken.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreTokenList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreToken' instances.
     */
    public long getSchufaScoreTokenCount() {
        long result = getEntityCount(SchufaScoreToken.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreToken' instances which match the given whereClause.
     */
    public long getSchufaScoreTokenCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(SchufaScoreToken.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreToken' instances which match the given whereClause together with params specified in object array.
     */
    public long getSchufaScoreTokenCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(SchufaScoreToken.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreTokenCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa score token)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
