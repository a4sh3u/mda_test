//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(issue authority)}
import de.smava.webapp.account.domain.history.IssueAuthorityHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IssueAuthoritys'.
 *
 * @author generator
 */
public class IssueAuthority extends IssueAuthorityHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(issue authority)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _bkz;
        protected String _zip;
        protected String _city;
        protected String _name;
        
                            /**
     * Setter for the property 'bkz'.
     */
    public void setBkz(String bkz) {
        if (!_bkzIsSet) {
            _bkzIsSet = true;
            _bkzInitVal = getBkz();
        }
        registerChange("bkz", _bkzInitVal, bkz);
        _bkz = bkz;
    }
                        
    /**
     * Returns the property 'bkz'.
     */
    public String getBkz() {
        return _bkz;
    }
                                    /**
     * Setter for the property 'zip'.
     */
    public void setZip(String zip) {
        if (!_zipIsSet) {
            _zipIsSet = true;
            _zipInitVal = getZip();
        }
        registerChange("zip", _zipInitVal, zip);
        _zip = zip;
    }
                        
    /**
     * Returns the property 'zip'.
     */
    public String getZip() {
        return _zip;
    }
                                    /**
     * Setter for the property 'city'.
     */
    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = getCity();
        }
        registerChange("city", _cityInitVal, city);
        _city = city;
    }
                        
    /**
     * Returns the property 'city'.
     */
    public String getCity() {
        return _city;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IssueAuthority.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _bkz=").append(_bkz);
            builder.append("\n    _zip=").append(_zip);
            builder.append("\n    _city=").append(_city);
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(IssueAuthority.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IssueAuthority asIssueAuthority() {
        return this;
    }
}
