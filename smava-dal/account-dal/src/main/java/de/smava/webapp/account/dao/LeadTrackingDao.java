//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(lead tracking)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.LeadTracking;
import de.smava.webapp.account.domain.TrackingType;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'LeadTrackings'.
 *
 * @author generator
 */
public interface LeadTrackingDao extends BaseDao<LeadTracking> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the lead tracking identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    LeadTracking getLeadTracking(Long id);

    /**
     * Saves the lead tracking.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveLeadTracking(LeadTracking leadTracking);

    /**
     * Deletes an lead tracking, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the lead tracking
     */
    void deleteLeadTracking(Long id);

    /**
     * Retrieves all 'LeadTracking' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<LeadTracking> getLeadTrackingList();

    /**
     * Retrieves a page of 'LeadTracking' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<LeadTracking> getLeadTrackingList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'LeadTracking' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<LeadTracking> getLeadTrackingList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'LeadTracking' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<LeadTracking> getLeadTrackingList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'LeadTracking' instances.
     */
    long getLeadTrackingCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(lead tracking)}
    //
    // insert custom methods here
    

    /**
     * Returns all LeadTracking for the given account and TrackingType
     * @param account
     * @param type
     * @return
     */
    public Collection<LeadTracking> findLeadTrackingList(Account account, TrackingType type);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
