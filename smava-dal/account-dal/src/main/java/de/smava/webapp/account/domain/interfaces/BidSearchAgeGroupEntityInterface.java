package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.AgeGroup;
import de.smava.webapp.account.domain.BidSearch;


/**
 * The domain object that represents 'BidSearchAgeGroups'.
 *
 * @author generator
 */
public interface BidSearchAgeGroupEntityInterface {

    /**
     * Setter for the property 'bid search'.
     *
     * 
     *
     */
    void setBidSearch(BidSearch bidSearch);

    /**
     * Returns the property 'bid search'.
     *
     * 
     *
     */
    BidSearch getBidSearch();
    /**
     * Setter for the property 'age group'.
     *
     * 
     *
     */
    void setAgeGroup(AgeGroup ageGroup);

    /**
     * Returns the property 'age group'.
     *
     * 
     *
     */
    AgeGroup getAgeGroup();

}
