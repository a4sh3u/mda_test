package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBorrowerPromotion;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BorrowerPromotions'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BorrowerPromotionHistory extends AbstractBorrowerPromotion {

    protected transient String _durationInitVal;
    protected transient boolean _durationIsSet;
    protected transient Date _startDateInitVal;
    protected transient boolean _startDateIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;


				
    /**
     * Returns the initial value of the property 'duration'.
     */
    public String durationInitVal() {
        String result;
        if (_durationIsSet) {
            result = _durationInitVal;
        } else {
            result = getDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'duration'.
     */
    public boolean durationIsDirty() {
        return !valuesAreEqual(durationInitVal(), getDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'duration'.
     */
    public boolean durationIsSet() {
        return _durationIsSet;
    }
		
    /**
     * Returns the initial value of the property 'start date'.
     */
    public Date startDateInitVal() {
        Date result;
        if (_startDateIsSet) {
            result = _startDateInitVal;
        } else {
            result = getStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start date'.
     */
    public boolean startDateIsDirty() {
        return !valuesAreEqual(startDateInitVal(), getStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'start date'.
     */
    public boolean startDateIsSet() {
        return _startDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
		
}
