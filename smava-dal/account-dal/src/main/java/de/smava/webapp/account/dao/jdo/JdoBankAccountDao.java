//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account)}

import de.smava.webapp.account.dao.BankAccountDao;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.BankAccountProvider;
import de.smava.webapp.account.security.SmavaUserDetailsIf;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.commons.security.Role;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BankAccounts'.
 *
 * @author generator
 */
@Repository(value = "bankAccountDao")
public class JdoBankAccountDao extends JdoBaseDao implements BankAccountDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBankAccountDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bank account)}
        //
        // insert custom fields here
        //

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bank account identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BankAccount load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccount - start: id=" + id);
        }
        BankAccount result = getEntity(BankAccount.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccount - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BankAccount getBankAccount(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BankAccount entity = findUniqueEntity(BankAccount.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the bank account.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BankAccount bankAccount) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBankAccount: " + bankAccount);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bank account)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bankAccount);
    }

    /**
     * @deprecated Use {@link #save(BankAccount) instead}
     */
    public Long saveBankAccount(BankAccount bankAccount) {
        return save(bankAccount);
    }

    /**
     * Deletes an bank account, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bank account
     */
    public void deleteBankAccount(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBankAccount: " + id);
        }
        deleteEntity(BankAccount.class, id);
    }

    /**
     * Retrieves all 'BankAccount' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BankAccount> getBankAccountList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - start");
        }
        Collection<BankAccount> result = getEntities(BankAccount.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BankAccount' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BankAccount> getBankAccountList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - start: pageable=" + pageable);
        }
        Collection<BankAccount> result = getEntities(BankAccount.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BankAccount' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BankAccount> getBankAccountList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - start: sortable=" + sortable);
        }
        Collection<BankAccount> result = getEntities(BankAccount.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankAccount' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BankAccount> getBankAccountList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BankAccount> result = getEntities(BankAccount.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BankAccount' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccount> findBankAccountList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - start: whereClause=" + whereClause);
        }
        Collection<BankAccount> result = findEntities(BankAccount.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BankAccount' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BankAccount> findBankAccountList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BankAccount> result = findEntities(BankAccount.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BankAccount' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccount> findBankAccountList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BankAccount> result = findEntities(BankAccount.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BankAccount' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccount> findBankAccountList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BankAccount> result = findEntities(BankAccount.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankAccount' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccount> findBankAccountList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BankAccount> result = findEntities(BankAccount.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankAccount' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccount> findBankAccountList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BankAccount> result = findEntities(BankAccount.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BankAccount' instances.
     */
    public long getBankAccountCount() {
        long result = getEntityCount(BankAccount.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BankAccount' instances which match the given whereClause.
     */
    public long getBankAccountCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BankAccount.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BankAccount' instances which match the given whereClause together with params specified in object array.
     */
    public long getBankAccountCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BankAccount.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bank account)}
    //
    // insert custom methods here
    //
 
   
    private BankAccount getSmavaAccountByType(BankAccountProvider bankAccountProvider, String type) {
    	List<BankAccount> acc = 
    		getBankAccounts(bankAccountProvider, type, "_id ascending", -1, -1);
    	if (null == acc || acc.isEmpty()) {
            return null;
        }
    	return acc.iterator().next();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BankAccount> getKokBankAccounts(BankAccountProvider bankAccountProvider, int page, int pageSize) {
    	return getUnexpiredBankAccounts(bankAccountProvider, "KOK", "_id ascending", page, pageSize); 
	}
    
    /**
     * {@inheritDoc}
     */
    @Override
	public BankAccount getSmavaInterimInAccount(BankAccountProvider bankAccountProvider) {
	   	List<BankAccount> acc = 
	   		getBankAccounts(bankAccountProvider, BankAccount.TYPE_SMAVA_INTERIM_IN_ACCOUNT, "_id ascending", -1, -1); 
	   	return acc.isEmpty()?null:acc.iterator().next();
   }
  
    /**
     * {@inheritDoc}
     */
    @Override
	public BankAccount getSmavaInterimOutAccount(BankAccountProvider bankAccountProvider) {
	   	List<BankAccount> acc = 
	   		getBankAccounts(bankAccountProvider, BankAccount.TYPE_SMAVA_INTERIM_OUT_ACCOUNT, "_id ascending", -1, -1); 
	   	return acc.isEmpty()?null:acc.iterator().next();
   }
   
    private List<BankAccount> getBankAccounts(BankAccountProvider bankAccountProvider, String type, String ordering, int page, int pageSize) {
    	final Query query = getPersistenceManager().newQuery(createQueryString(BankAccount.class, "_type == '"+type+"' && _provider == myprovider PARAMETERS " + BankAccountProvider.class.getName()+ " myprovider"));
    	if (-1 < page && -1 < pageSize) {
            query.setRange(page * pageSize, (page + 1) * pageSize);
        }
    	if (null != ordering) {
            query.setOrdering(ordering);
        }
    	return (List<BankAccount>) query.executeWithArray(bankAccountProvider); 
	}
    
    private List<BankAccount> getUnexpiredBankAccounts(BankAccountProvider bankAccountProvider, String type, String ordering, int page, int pageSize) {
    	final Query query = getPersistenceManager().newQuery(createQueryString(BankAccount.class, "_expirationDate == null" + "&& _type == '"+type+"' && _provider == myprovider PARAMETERS " + BankAccountProvider.class.getName()+ " myprovider"));
    	if (-1 < page && -1 < pageSize) {
            query.setRange(page * pageSize, (page + 1) * pageSize);
        }
    	if (null != ordering) {
            query.setOrdering(ordering);
        }
    	return (List<BankAccount>) query.executeWithArray(bankAccountProvider); 
	}
    
    private List<BankAccount> getBankAccounts( String type, String ordering, int page, int pageSize) {
    	final Query query = getPersistenceManager().newQuery(createQueryString(BankAccount.class, "_type == '"+type+"'"));
    	if (-1 < page && -1 < pageSize) {
            query.setRange(page * pageSize, (page + 1) * pageSize);
        }
    	if (null != ordering) {
            query.setOrdering(ordering);
        }
    	return (List<BankAccount>) query.executeWithArray(); 
	}
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public BankAccount getLenderDepositAccount(BankAccountProvider provider) {
    	return getSmavaAccountByType(provider, BankAccount.TYPE_LENDER_DEPOSIT_ACCOUNT);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public BankAccount getBorrowerDepositAccount(BankAccountProvider provider) {
    	return getSmavaAccountByType(provider, BankAccount.TYPE_BORROWER_DEPOSIT_ACCOUNT);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BankAccount> getAllDepositAccounts() {
    	Collection<BankAccount> result = new ArrayList<BankAccount>();
    	result.addAll( getBankAccounts( BankAccount.TYPE_LENDER_DEPOSIT_ACCOUNT, "_id ascending", -1, -1));
    	result.addAll( getBankAccounts( BankAccount.TYPE_BORROWER_DEPOSIT_ACCOUNT, "_id ascending", -1, -1));
    	return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankAccount getInternalBorrowerAccountOriginal(BankAccountProvider provider) {
    	return getSmavaAccountByType(provider, BankAccount.TYPE_BORROWER_ACCOUNT_ORIGINAL);
    }
    
    @Override
    public Collection<BankAccount> findBankAccount(String bank, String bankAccountNo) {
    	String term = "_bankCode == '" + bank + "' && _accountNo == '" + bankAccountNo + "'";
    	return findBankAccountList( term);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BankAccount> findBankAccountNotExpired(String accountNo, String bankCode) {
        String query = "_accountNo == '" + accountNo + "' && _bankCode == '" + bankCode + "' && _expirationDate == null";
        return findBankAccountList( query);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BankAccount> findLikeBankAccount(String bank, String bankAccountNo) {
    	String term = "_bankCode == '" + bank + "' && _accountNo.matches('.*" + bankAccountNo + ".*')";
    	return findBankAccountList( term);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Long> findActiveBankAccountsWithoutIban( String type){
    	final String whereClause = "_expirationDate == null" + " && _type == '"+type+"' && _iban == null";
    	Collection<Long> ids = new ArrayList<Long>();
    	Collection<BankAccount> result = findEntities(BankAccount.class, whereClause);
    	for (BankAccount ba: result){
    		ids.add(ba.getId());
    	}
    	return ids;
    }

    public Collection<BankAccount> getBankAccountsByAccountNo(
            String bankAccountNo) {
        String jdoqlWhere = "_accountNo == '"+bankAccountNo+"'";
        jdoqlWhere = getRestrictionWhereForBoBankuserBankaccountSearch(jdoqlWhere);

        return this.findBankAccountList(jdoqlWhere);
    }

    public String getRestrictionWhereForBoBankuserBankaccountSearch(final String whereClause){

        String jdoqlWhere = whereClause;

        //sort out contrary bankAccounts for bank users
        SmavaUserDetailsIf loggedInBoUser = (SmavaUserDetailsIf) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        for(GrantedAuthority ga : loggedInBoUser.getAuthorities()){
            if(ga.getAuthority().equals(Role.ROLE_BANK_BIW.name())){
                jdoqlWhere += " && _valid == true && _expirationDate == null && _provider._name == 'biw'";
            }
            if(ga.getAuthority().equals(Role.ROLE_BANK_FIDOR.name())){
                jdoqlWhere += " && _valid == true && _expirationDate == null && _provider._name == 'fidor'";
            }
        }

        return jdoqlWhere;
    }

    @Override
    public BankAccount getKokImitationBankAccount(BankAccountProvider provider) {

        BankAccount imitationBankAccount = null;

        Collection<BankAccount> acc = findBankAccountList("_type == '" + BankAccount.TYPE_KOK_IMITATION_ACCOUNT + "' && _provider == thisProvider PARAMETERS "
                        + BankAccountProvider.class.getName()+ " thisProvider",
                provider);

        if (acc != null) {
            if (acc.size() == 1) {
                imitationBankAccount = acc.iterator().next();
            }
        }

        return imitationBankAccount;

    }


    @Override
    public Collection<BankAccount> getSmavaInterimInBankAccounts() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_SMAVA_INTERIM_IN_ACCOUNT + "'");
        return bankAccounts;
    }

    @Override
    public Collection<BankAccount> getSmavaInterimOutBankAccounts() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_SMAVA_INTERIM_OUT_ACCOUNT + "'");
        return bankAccounts;
    }


    /* (non-Javadoc)
     * @see com.aperto.smava.service.ConfigurationService#getRdiAmortisationBankAccount()
     */
    public BankAccount getRdiAmortisationBankAccount() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_RDI_AMORTISATION_ACCOUNT + "' && _expirationDate == null");
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            }
        }

        return result;
    }


    /* (non-Javadoc)
     * @see com.aperto.smava.service.ConfigurationService#getRdiAmortisationBankAccounts()
     */
    public Collection<BankAccount> getRdiAmortisationBankAccounts() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_RDI_AMORTISATION_ACCOUNT + "'");
        return bankAccounts;
    }

    public BankAccount getRdiPremiumBankAccount() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_RDI_PREMIUM_ACCOUNT + "' && _expirationDate == null");
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            }
        }

        return result;
    }

    public BankAccount getSmavaCallMoneyBankAccount() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_SMAVA_CALLMONEY_ACCOUNT + "' && _expirationDate == null");
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            }
        }

        return result;
    }

    public BankAccount getSmavaOutpaymentBankAccount() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_SMAVA_OUTPAYMENT_ACCOUNT + "' && _expirationDate == null");
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            }
        }

        return result;
    }

    public BankAccount getEncashmentBankAccount() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_ENCASHMENT_ACCOUNT + "' && _expirationDate == null");
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            } else {
                LOGGER.warn("2 TYPE_ENCASHMENT_ACCOUNT found");
            }
        }

        return result;
    }

    public BankAccount getProvisionInterimBankAccount(BankAccountProvider bankAccountProvider) {
        Collection<BankAccount> bankAccounts = findBankAccountList(
                "_type == '" + BankAccount.TYPE_PROVISION_INTERIM_ACCOUNT +
                        "' && _expirationDate == null && _provider == myprovider PARAMETERS " +
                        BankAccountProvider.class.getName()+ " myprovider", bankAccountProvider);
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            } else {
                LOGGER.error("found not exactly 1 provision interim account " + bankAccounts);
            }
        }

        return result;
    }

    public Collection<BankAccount> getProvisionInterimBankAccounts() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_PROVISION_INTERIM_ACCOUNT + "'");
        return bankAccounts;
    }

    public Collection<BankAccount> getEncashmentInterimBankAccounts() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_ENCASHMENT_INTERIM_ACCOUNT + "'");
        return bankAccounts;
    }

    public BankAccount getEncashmentInterimBankAccount(BankAccountProvider bankAccountProvider) {
        Collection<BankAccount> bankAccounts = findBankAccountList(
                "_type == '" + BankAccount.TYPE_ENCASHMENT_INTERIM_ACCOUNT +
                        "' && _expirationDate == null && _provider == myprovider PARAMETERS " +
                        BankAccountProvider.class.getName()+ " myprovider", bankAccountProvider);
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            }
        }

        return result;
    }

    public Collection<BankAccount> getEncashmentBankAccounts() {
        Collection<BankAccount> bankAccounts = findBankAccountList("_type == '" + BankAccount.TYPE_ENCASHMENT_ACCOUNT + "'");
        return bankAccounts;
    }

    public BankAccount getSmavaBankAccount(BankAccountProvider provider) {
        Collection<BankAccount> bankAccounts =
                findBankAccountList(
                        "_type == '" + BankAccount.TYPE_SMAVA_ACCOUNT +
                                "' && _expirationDate == null && _provider == myprovider " +
                                "PARAMETERS "+
                                BankAccountProvider.class.getName()+ " myprovider",
                        provider);
        BankAccount result = null;
        if (bankAccounts != null) {
            if (bankAccounts.size() == 1) {
                result = bankAccounts.iterator().next();
            }
        }

        return result;
    }

    public Collection<BankAccount> getSmavaBankAccounts() {
        return findBankAccountList("_type == '" + BankAccount.TYPE_SMAVA_ACCOUNT + "'");
    }

    public Collection<BankAccount> getByIban(String bankIban) {
        // create query
        String term = "_iban.matches('" + bankIban + "')";

        // query for results & drop invariants ...
        Collection<BankAccount> list = findBankAccountList(term);
        return list;
    }

    public Collection<BankAccount> getNonExpiredByIban(String bankIban) {
        // create query
        String term = "_iban.matches('" + bankIban + "') && _expirationDate == null";

        // query for results & drop invariants ...
        Collection<BankAccount> list = findBankAccountList(term);
        return list;
    }

    public Collection<BankAccount> getByCodeAndAccount(String bankCode, String bankAccountNumberStripped) {
        final String term = "_bankCode == '" + bankCode + "' && _accountNo.matches('.*" + bankAccountNumberStripped + "')";
        Collection<BankAccount> list = findBankAccountList(term);
        return list;
    }

    public Collection<BankAccount> getByCodeAndAccountAndBicAndIban(String bankCode, String bankAccountNumberStripped, String bic, String iban) {
        String term = "_bankCode == '" + bankCode + "' && _accountNo.matches('.*" + bankAccountNumberStripped + "') && _bic == '" + bic + "' && _iban == '" + iban + "'";
        Collection<BankAccount> list = findBankAccountList(term);
        return list;
    }

    public Collection<BankAccount> getByAccount(Long accountId) {
        return findBankAccountList("_account._id ==" + accountId);
    }

    // !!!!!!!! End of insert code section !!!!!!!!

	
}
