/**
 * 
 */
package de.smava.webapp.account.todo.order;

import java.sql.Timestamp;

import de.smava.webapp.account.domain.Order;
import de.smava.webapp.commons.domain.AbstractImageInfo;

/**
 * @author bvoss
 *
 */
public class OrderInfo extends AbstractImageInfo<Order> {
	
	private String _name;
	private Timestamp _activationDate;
	private String _username;
	private Long _accountId;
	private Double _originalAmount;
	private Double _openAmount;
	private Double _rate;
	private String _market;
	private Integer _term;
	private Double _matchedRate;
	private Double _roi;

	public OrderInfo() {
		super(Order.class);
	}

	public String getName() {
		return _name;
	}

	public void setName(final String name) {
		_name = name;
	}

	public Timestamp getActivationDate() {
		return _activationDate;
	}

	public void setActivationDate(final Timestamp activationDate) {
		_activationDate = activationDate;
	}

	public String getUsername() {
		return _username;
	}

	public void setUsername(final String username) {
		_username = username;
	}

	public Long getAccountId() {
		return _accountId;
	}

	public void setAccountId(final Long accountId) {
		_accountId = accountId;
	}

	public Double getOriginalAmount() {
		return _originalAmount;
	}

	public void setOriginalAmount(final Double originalAmount) {
		_originalAmount = originalAmount;
	}

	public Double getMatchedAmount() {
		return _originalAmount - _openAmount;
	}

	public Double getOpenAmount() {
		return _openAmount;
	}

	public void setOpenAmount(Double openAmount) {
		_openAmount = openAmount;
	}

	public Double getRate() {
		return _rate;
	}

	public void setRate(final Double interest) {
		_rate = interest;
	}

	public String getMarket() {
		return _market;
	}

	public void setMarket(final String market) {
		_market = market;
	}

	public Integer getTerm() {
		return _term;
	}

	public void setTerm(final Integer duration) {
		_term = duration;
	}

	public Double getMatchedRate() {
		return _matchedRate;
	}

	public void setMatchedRate(final Double matchedRate) {
		_matchedRate = matchedRate;
	}

	public Double getRoi() {
		return _roi;
	}

	public void setRoi(Double roi) {
		_roi = roi;
	}
}
