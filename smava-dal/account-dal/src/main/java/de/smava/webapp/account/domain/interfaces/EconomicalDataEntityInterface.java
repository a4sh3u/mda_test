package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.ConsolidatedDebt;
import de.smava.webapp.account.domain.FreelancerIncome;
import de.smava.webapp.account.domain.Sector;

import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'EconomicalDatas'.
 *
 * @author generator
 */
public interface EconomicalDataEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'occupation'.
     *
     * 
     *
     */
    void setOccupation(String occupation);

    /**
     * Returns the property 'occupation'.
     *
     * 
     *
     */
    String getOccupation();
    /**
     * Setter for the property 'employment'.
     *
     * 
     *
     */
    void setEmployment(String employment);

    /**
     * Returns the property 'employment'.
     *
     * 
     *
     */
    String getEmployment();
    /**
     * Setter for the property 'start of occupation'.
     *
     * 
     *
     */
    void setStartOfOccupation(Date startOfOccupation);

    /**
     * Returns the property 'start of occupation'.
     *
     * 
     *
     */
    Date getStartOfOccupation();
    /**
     * Setter for the property 'mode of accommodation'.
     *
     * 
     *
     */
    void setModeOfAccommodation(String modeOfAccommodation);

    /**
     * Returns the property 'mode of accommodation'.
     *
     * 
     *
     */
    String getModeOfAccommodation();
    /**
     * Setter for the property 'sector'.
     *
     * 
     *
     */
    void setSector(Sector sector);

    /**
     * Returns the property 'sector'.
     *
     * 
     *
     */
    Sector getSector();
    /**
     * Setter for the property 'sector description'.
     *
     * 
     *
     */
    void setSectorDescription(String sectorDescription);

    /**
     * Returns the property 'sector description'.
     *
     * 
     *
     */
    String getSectorDescription();
    /**
     * Setter for the property 'savings'.
     *
     * 
     *
     */
    void setSavings(double savings);

    /**
     * Returns the property 'savings'.
     *
     * 
     *
     */
    double getSavings();
    /**
     * Setter for the property 'credit expenses'.
     *
     * 
     *
     */
    void setCreditExpenses(double creditExpenses);

    /**
     * Returns the property 'credit expenses'.
     *
     * 
     *
     */
    double getCreditExpenses();
    /**
     * Setter for the property 'total private credit amount'.
     *
     * 
     *
     */
    void setTotalPrivateCreditAmount(double totalPrivateCreditAmount);

    /**
     * Returns the property 'total private credit amount'.
     *
     * 
     *
     */
    double getTotalPrivateCreditAmount();
    /**
     * Setter for the property 'temp employment'.
     *
     * 
     *
     */
    void setTempEmployment(boolean tempEmployment);

    /**
     * Returns the property 'temp employment'.
     *
     * 
     *
     */
    boolean getTempEmployment();
    /**
     * Setter for the property 'end of temp employment'.
     *
     * 
     *
     */
    void setEndOfTempEmployment(Date endOfTempEmployment);

    /**
     * Returns the property 'end of temp employment'.
     *
     * 
     *
     */
    Date getEndOfTempEmployment();
    /**
     * Setter for the property 'received palimony'.
     *
     * 
     *
     */
    void setReceivedPalimony(double receivedPalimony);

    /**
     * Returns the property 'received palimony'.
     *
     * 
     *
     */
    double getReceivedPalimony();
    /**
     * Setter for the property 'paid rent'.
     *
     * 
     *
     */
    void setPaidRent(double paidRent);

    /**
     * Returns the property 'paid rent'.
     *
     * 
     *
     */
    double getPaidRent();
    /**
     * Setter for the property 'received rent'.
     *
     * 
     *
     */
    void setReceivedRent(double receivedRent);

    /**
     * Returns the property 'received rent'.
     *
     * 
     *
     */
    double getReceivedRent();
    /**
     * Setter for the property 'misc earn amount1'.
     *
     * 
     *
     */
    void setMiscEarnAmount1(double miscEarnAmount1);

    /**
     * Returns the property 'misc earn amount1'.
     *
     * 
     *
     */
    double getMiscEarnAmount1();
    /**
     * Setter for the property 'misc earn1'.
     *
     * 
     *
     */
    void setMiscEarn1(String miscEarn1);

    /**
     * Returns the property 'misc earn1'.
     *
     * 
     *
     */
    String getMiscEarn1();
    /**
     * Setter for the property 'other earn description'.
     *
     * 
     *
     */
    void setOtherEarnDescription(String otherEarnDescription);

    /**
     * Returns the property 'other earn description'.
     *
     * 
     *
     */
    String getOtherEarnDescription();
    /**
     * Setter for the property 'num of cars'.
     *
     * 
     *
     */
    void setNumOfCars(String numOfCars);

    /**
     * Returns the property 'num of cars'.
     *
     * 
     *
     */
    String getNumOfCars();
    /**
     * Setter for the property 'paid alimony'.
     *
     * 
     *
     */
    void setPaidAlimony(double paidAlimony);

    /**
     * Returns the property 'paid alimony'.
     *
     * 
     *
     */
    double getPaidAlimony();
    /**
     * Setter for the property 'income'.
     *
     * 
     *
     */
    void setIncome(double income);

    /**
     * Returns the property 'income'.
     *
     * 
     *
     */
    double getIncome();
    /**
     * Setter for the property 'private health insurance'.
     *
     * 
     *
     */
    void setPrivateHealthInsurance(double privateHealthInsurance);

    /**
     * Returns the property 'private health insurance'.
     *
     * 
     *
     */
    double getPrivateHealthInsurance();
    /**
     * Setter for the property 'mortgage'.
     *
     * 
     *
     */
    void setMortgage(double mortgage);

    /**
     * Returns the property 'mortgage'.
     *
     * 
     *
     */
    double getMortgage();
    /**
     * Setter for the property 'business loan'.
     *
     * 
     *
     */
    void setBusinessLoan(double businessLoan);

    /**
     * Returns the property 'business loan'.
     *
     * 
     *
     */
    double getBusinessLoan();
    /**
     * Setter for the property 'leasing'.
     *
     * 
     *
     */
    void setLeasing(double leasing);

    /**
     * Returns the property 'leasing'.
     *
     * 
     *
     */
    double getLeasing();
    /**
     * Setter for the property 'business leasing'.
     *
     * 
     *
     */
    void setBusinessLeasing(double businessLeasing);

    /**
     * Returns the property 'business leasing'.
     *
     * 
     *
     */
    double getBusinessLeasing();
    /**
     * Setter for the property 'freelancer incomes'.
     *
     * 
     *
     */
    void setFreelancerIncomes(List<FreelancerIncome> freelancerIncomes);

    /**
     * Returns the property 'freelancer incomes'.
     *
     * 
     *
     */
    List<FreelancerIncome> getFreelancerIncomes();
    /**
     * Setter for the property 'freelancer income confirmation limit date'.
     *
     * 
     *
     */
    void setFreelancerIncomeConfirmationLimitDate(Date freelancerIncomeConfirmationLimitDate);

    /**
     * Returns the property 'freelancer income confirmation limit date'.
     *
     * 
     *
     */
    Date getFreelancerIncomeConfirmationLimitDate();
    /**
     * Setter for the property 'trade tax paid'.
     *
     * 
     *
     */
    void setTradeTaxPaid(boolean tradeTaxPaid);

    /**
     * Returns the property 'trade tax paid'.
     *
     * 
     *
     */
    boolean getTradeTaxPaid();
    /**
     * Setter for the property 'church tax paid'.
     *
     * 
     *
     */
    void setChurchTaxPaid(boolean churchTaxPaid);

    /**
     * Returns the property 'church tax paid'.
     *
     * 
     *
     */
    boolean getChurchTaxPaid();
    /**
     * Setter for the property 'credit rate indicator'.
     *
     * 
     *
     */
    void setCreditRateIndicator(int creditRateIndicator);

    /**
     * Returns the property 'credit rate indicator'.
     *
     * 
     *
     */
    int getCreditRateIndicator();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'schufa leasing'.
     *
     * 
     *
     */
    void setSchufaLeasing(double schufaLeasing);

    /**
     * Returns the property 'schufa leasing'.
     *
     * 
     *
     */
    double getSchufaLeasing();
    /**
     * Setter for the property 'schufa loan'.
     *
     * 
     *
     */
    void setSchufaLoan(double schufaLoan);

    /**
     * Returns the property 'schufa loan'.
     *
     * 
     *
     */
    double getSchufaLoan();
    /**
     * Setter for the property 'schufa loan related savings'.
     *
     * 
     *
     */
    void setSchufaLoanRelatedSavings(double schufaLoanRelatedSavings);

    /**
     * Returns the property 'schufa loan related savings'.
     *
     * 
     *
     */
    double getSchufaLoanRelatedSavings();
    /**
     * Setter for the property 'schufa business leasing'.
     *
     * 
     *
     */
    void setSchufaBusinessLeasing(double schufaBusinessLeasing);

    /**
     * Returns the property 'schufa business leasing'.
     *
     * 
     *
     */
    double getSchufaBusinessLeasing();
    /**
     * Setter for the property 'schufa business loan'.
     *
     * 
     *
     */
    void setSchufaBusinessLoan(double schufaBusinessLoan);

    /**
     * Returns the property 'schufa business loan'.
     *
     * 
     *
     */
    double getSchufaBusinessLoan();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(int type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    int getType();
    /**
     * Setter for the property 'misc earn type'.
     *
     * 
     *
     */
    void setMiscEarnType(de.smava.webapp.account.domain.MiscEarnType miscEarnType);

    /**
     * Returns the property 'misc earn type'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.MiscEarnType getMiscEarnType();
    /**
     * Setter for the property 'property owned type'.
     *
     * 
     *
     */
    void setPropertyOwnedType(de.smava.webapp.account.domain.PropertyOwnedType propertyOwnedType);

    /**
     * Returns the property 'property owned type'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.PropertyOwnedType getPropertyOwnedType();
    /**
     * Setter for the property 'credit card type'.
     *
     * 
     *
     */
    void setCreditCardType(de.smava.webapp.account.domain.CreditCardType creditCardType);

    /**
     * Returns the property 'credit card type'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.CreditCardType getCreditCardType();
    /**
     * Setter for the property 'property owned square metres'.
     *
     * 
     *
     */
    void setPropertyOwnedSquareMetres(Integer propertyOwnedSquareMetres);

    /**
     * Returns the property 'property owned square metres'.
     *
     * 
     *
     */
    Integer getPropertyOwnedSquareMetres();
    /**
     * Setter for the property 'employer name'.
     *
     * 
     *
     */
    void setEmployerName(String employerName);

    /**
     * Returns the property 'employer name'.
     *
     * 
     *
     */
    String getEmployerName();
    /**
     * Setter for the property 'number of children'.
     *
     * 
     *
     */
    void setNumberOfChildren(int numberOfChildren);

    /**
     * Returns the property 'number of children'.
     *
     * 
     *
     */
    int getNumberOfChildren();
    /**
     * Setter for the property 'number of other in household'.
     *
     * 
     *
     */
    void setNumberOfOtherInHousehold(int numberOfOtherInHousehold);

    /**
     * Returns the property 'number of other in household'.
     *
     * 
     *
     */
    int getNumberOfOtherInHousehold();
    /**
     * Setter for the property 'selfemployed profession'.
     *
     * 
     *
     */
    void setSelfemployedProfession(de.smava.webapp.account.domain.SelfemployedProfessionType selfemployedProfession);

    /**
     * Returns the property 'selfemployed profession'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.SelfemployedProfessionType getSelfemployedProfession();
    /**
     * Setter for the property 'economical data type'.
     *
     * 
     *
     */
    void setEconomicalDataType(de.smava.webapp.account.domain.EconomicalDataType economicalDataType);

    /**
     * Returns the property 'economical data type'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.EconomicalDataType getEconomicalDataType();
    /**
     * Setter for the property 'debts'.
     *
     * 
     *
     */
    void setDebts(Collection<ConsolidatedDebt> debts);

    /**
     * Returns the property 'debts'.
     *
     * 
     *
     */
    Collection<ConsolidatedDebt> getDebts();

}
