//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account address)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.account.dao.AccountAddressDao;
import de.smava.webapp.account.domain.AccountAddress;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'AccountAddresss'.
 *
 * @author generator
 */
@Repository(value = "accountAddressDao")
public class JdoAccountAddressDao extends JdoBaseDao implements AccountAddressDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAccountAddressDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(account address)}
    /**
	 * generated serial.
	 */
	private static final long serialVersionUID = 6710671037318158861L;

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the account address identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public AccountAddress load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddress - start: id=" + id);
        }
        AccountAddress result = getEntity(AccountAddress.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddress - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public AccountAddress getAccountAddress(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	AccountAddress entity = findUniqueEntity(AccountAddress.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the account address.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(AccountAddress accountAddress) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAccountAddress: " + accountAddress);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(account address)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(accountAddress);
    }

    /**
     * @deprecated Use {@link #save(AccountAddress) instead}
     */
    public Long saveAccountAddress(AccountAddress accountAddress) {
        return save(accountAddress);
    }

    /**
     * Deletes an account address, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account address
     */
    public void deleteAccountAddress(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAccountAddress: " + id);
        }
        deleteEntity(AccountAddress.class, id);
    }

    /**
     * Retrieves all 'AccountAddress' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<AccountAddress> getAccountAddressList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - start");
        }
        Collection<AccountAddress> result = getEntities(AccountAddress.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'AccountAddress' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<AccountAddress> getAccountAddressList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - start: pageable=" + pageable);
        }
        Collection<AccountAddress> result = getEntities(AccountAddress.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'AccountAddress' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<AccountAddress> getAccountAddressList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - start: sortable=" + sortable);
        }
        Collection<AccountAddress> result = getEntities(AccountAddress.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'AccountAddress' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<AccountAddress> getAccountAddressList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<AccountAddress> result = getEntities(AccountAddress.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'AccountAddress' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AccountAddress> findAccountAddressList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - start: whereClause=" + whereClause);
        }
        Collection<AccountAddress> result = findEntities(AccountAddress.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'AccountAddress' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<AccountAddress> findAccountAddressList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<AccountAddress> result = findEntities(AccountAddress.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'AccountAddress' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AccountAddress> findAccountAddressList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<AccountAddress> result = findEntities(AccountAddress.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'AccountAddress' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AccountAddress> findAccountAddressList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<AccountAddress> result = findEntities(AccountAddress.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'AccountAddress' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AccountAddress> findAccountAddressList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<AccountAddress> result = findEntities(AccountAddress.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'AccountAddress' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<AccountAddress> findAccountAddressList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<AccountAddress> result = findEntities(AccountAddress.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAccountAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'AccountAddress' instances.
     */
    public long getAccountAddressCount() {
        long result = getEntityCount(AccountAddress.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'AccountAddress' instances which match the given whereClause.
     */
    public long getAccountAddressCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(AccountAddress.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'AccountAddress' instances which match the given whereClause together with params specified in object array.
     */
    public long getAccountAddressCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(AccountAddress.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAccountAddressCount() - result: count=" + result);
        }
        return result;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(account address)}
    /**
     *
     * {@inheritDoc}
     */
    @Override
    public Collection<AccountAddress> getAddressList(Long accountId, Pageable pageable, Sortable sortable) {
        return findAccountAddressList("_account._id == " + accountId, pageable, sortable);
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
