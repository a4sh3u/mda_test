package de.smava.webapp.account.domain.interfaces;


import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'Attachments'.
 *
 * @author generator
 */
public interface AttachmentEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'attachment id'.
     *
     * 
     *
     */
    void setAttachmentId(Long attachmentId);

    /**
     * Returns the property 'attachment id'.
     *
     * 
     *
     */
    Long getAttachmentId();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'content type'.
     *
     * 
     *
     */
    void setContentType(String contentType);

    /**
     * Returns the property 'content type'.
     *
     * 
     *
     */
    String getContentType();
    /**
     * Setter for the property 'location'.
     *
     * 
     *
     */
    void setLocation(String location);

    /**
     * Returns the property 'location'.
     *
     * 
     *
     */
    String getLocation();
    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    void setDocument(de.smava.webapp.account.domain.Document document);

    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Document getDocument();
    /**
     * Setter for the property 'document attachment container'.
     *
     * 
     *
     */
    void setDocumentAttachmentContainer(Collection<de.smava.webapp.account.domain.DocumentAttachmentContainer> documentAttachmentContainer);

    /**
     * Returns the property 'document attachment container'.
     *
     * 
     *
     */
    Collection<de.smava.webapp.account.domain.DocumentAttachmentContainer> getDocumentAttachmentContainer();

}
