package de.smava.webapp.account.security;

import de.smava.webapp.commons.security.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

/**
 * SmavaUserDetails uncommented.
 *
 * @author robert porscha (03.05.2007)
 */


public class SmavaUserDetails implements UserDetails, Serializable, SmavaUserDetailsIf {

    private static final long serialVersionUID = -4611955817952933473L;

    private Collection<GrantedAuthority> _authorities;
    private String _password;
    private String _userName;

    private boolean _isEnabled;
    private boolean _isNotLocked;
    private boolean _isLender;
    private Long smavaId;
    private String userToken;

    public SmavaUserDetails(String password, String userName, Collection<String> roleNames, Long smavaId, boolean enabled, boolean notLocked, boolean lender, boolean isVoid) {
        _password = password;
        _userName = userName;
        this. smavaId = smavaId;
        _isEnabled = enabled; // !(account.isDeactivated() || account.isFake() || account.isDeleted());
        _isNotLocked = notLocked; //account.getLockDate() == null;

        _isLender = roleNames.contains(Role.ROLE_LENDER.name());
         if (roleNames.contains(Role.ROLE_MEMBER.name())) {
            _authorities = Collections.singleton(Role.ROLE_MEMBER.getAuthority());
        } else {
            _authorities = new LinkedHashSet<GrantedAuthority>(roleNames.size());
            for (String role : roleNames) {
                _authorities.add(new SimpleGrantedAuthority(role));
            }
        }

        _authorities = Collections.unmodifiableCollection(_authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return _authorities;
    }

    @Override
    public String getPassword() {
        return _password;
    }

    @Override
    public String getUsername() {
        return _userName;
    }

    /**
     * Indicates whether the user's account has expired. An expired account cannot be authenticated.
     */
    @Override
    public boolean isAccountNonExpired() {
        // todo implement
        return true;
    }

    /**
     * Indicates whether the user is locked or unlocked. A locked user cannot be authenticated.
     */
    public boolean isAccountNonLocked() {
        return _isNotLocked;
    }

    /**
     * Indicates whether the user's credentials (password) has expired. Expired credentials prevent authentication.
     * @return true if the user's credentials are valid (ie non-expired), false if no longer valid (ie expired)
     */
    @Override
    public boolean isCredentialsNonExpired() {
        // todo implement
        return true;
    }

    @Override
    public boolean isEnabled() {
        return _isEnabled;
    }

    @Override
    public Long getSmavaId() {
        return smavaId;
    }

    public boolean isLender() {
        return _isLender;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
