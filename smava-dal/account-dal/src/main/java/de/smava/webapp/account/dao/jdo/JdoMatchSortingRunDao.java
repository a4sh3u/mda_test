//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(match sorting run)}

import de.smava.webapp.account.dao.MatchSortingRunDao;
import de.smava.webapp.account.domain.MatchSortingRun;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'MatchSortingRuns'.
 *
 * @author generator
 */
@Repository(value = "matchSortingRunDao")
public class JdoMatchSortingRunDao extends JdoBaseDao implements MatchSortingRunDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMatchSortingRunDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(match sorting run)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the match sorting run identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MatchSortingRun load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRun - start: id=" + id);
        }
        MatchSortingRun result = getEntity(MatchSortingRun.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRun - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MatchSortingRun getMatchSortingRun(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MatchSortingRun entity = findUniqueEntity(MatchSortingRun.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the match sorting run.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MatchSortingRun matchSortingRun) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMatchSortingRun: " + matchSortingRun);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(match sorting run)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(matchSortingRun);
    }

    /**
     * @deprecated Use {@link #save(MatchSortingRun) instead}
     */
    public Long saveMatchSortingRun(MatchSortingRun matchSortingRun) {
        return save(matchSortingRun);
    }

    /**
     * Deletes an match sorting run, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the match sorting run
     */
    public void deleteMatchSortingRun(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMatchSortingRun: " + id);
        }
        deleteEntity(MatchSortingRun.class, id);
    }

    /**
     * Retrieves all 'MatchSortingRun' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MatchSortingRun> getMatchSortingRunList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - start");
        }
        Collection<MatchSortingRun> result = getEntities(MatchSortingRun.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MatchSortingRun' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MatchSortingRun> getMatchSortingRunList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - start: pageable=" + pageable);
        }
        Collection<MatchSortingRun> result = getEntities(MatchSortingRun.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MatchSortingRun' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MatchSortingRun> getMatchSortingRunList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - start: sortable=" + sortable);
        }
        Collection<MatchSortingRun> result = getEntities(MatchSortingRun.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MatchSortingRun' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MatchSortingRun> getMatchSortingRunList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MatchSortingRun> result = getEntities(MatchSortingRun.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MatchSortingRun' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MatchSortingRun> findMatchSortingRunList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - start: whereClause=" + whereClause);
        }
        Collection<MatchSortingRun> result = findEntities(MatchSortingRun.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MatchSortingRun' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MatchSortingRun> findMatchSortingRunList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MatchSortingRun> result = findEntities(MatchSortingRun.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MatchSortingRun' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MatchSortingRun> findMatchSortingRunList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MatchSortingRun> result = findEntities(MatchSortingRun.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MatchSortingRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MatchSortingRun> findMatchSortingRunList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MatchSortingRun> result = findEntities(MatchSortingRun.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MatchSortingRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MatchSortingRun> findMatchSortingRunList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MatchSortingRun> result = findEntities(MatchSortingRun.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MatchSortingRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MatchSortingRun> findMatchSortingRunList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MatchSortingRun> result = findEntities(MatchSortingRun.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMatchSortingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MatchSortingRun' instances.
     */
    public long getMatchSortingRunCount() {
        long result = getEntityCount(MatchSortingRun.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MatchSortingRun' instances which match the given whereClause.
     */
    public long getMatchSortingRunCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MatchSortingRun.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MatchSortingRun' instances which match the given whereClause together with params specified in object array.
     */
    public long getMatchSortingRunCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MatchSortingRun.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMatchSortingRunCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(match sorting run)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
