package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.SchufaScoreHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'SchufaScores'.
 */
public class SchufaScore extends SchufaScoreHistory  {

        protected Account _account;
        protected Date _creationDate;
        protected int _score;
        protected String _rating;
        protected String _type;
        protected String _remark;
        protected String _state;
        protected SchufaScoreRequestData _requestData;
        protected SchufaRequest _request;
        protected String _version;
        protected Collection<SchufaScoreToken> _tokens;

    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'score'.
     */
    public void setScore(int score) {
        if (!_scoreIsSet) {
            _scoreIsSet = true;
            _scoreInitVal = getScore();
        }
        registerChange("score", _scoreInitVal, score);
        _score = score;
    }
                        
    /**
     * Returns the property 'score'.
     */
    public int getScore() {
        return _score;
    }
                                    /**
     * Setter for the property 'rating'.
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     */
    public String getRating() {
        return _rating;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'remark'.
     */
    public void setRemark(String remark) {
        if (!_remarkIsSet) {
            _remarkIsSet = true;
            _remarkInitVal = getRemark();
        }
        registerChange("remark", _remarkInitVal, remark);
        _remark = remark;
    }
                        
    /**
     * Returns the property 'remark'.
     */
    public String getRemark() {
        return _remark;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'request data'.
     */
    public void setRequestData(SchufaScoreRequestData requestData) {
        _requestData = requestData;
    }
            
    /**
     * Returns the property 'request data'.
     */
    public SchufaScoreRequestData getRequestData() {
        return _requestData;
    }
                                            
    /**
     * Setter for the property 'request'.
     */
    public void setRequest(SchufaRequest request) {
        _request = request;
    }
            
    /**
     * Returns the property 'request'.
     */
    public SchufaRequest getRequest() {
        return _request;
    }
                                    /**
     * Setter for the property 'version'.
     */
    public void setVersion(String version) {
        if (!_versionIsSet) {
            _versionIsSet = true;
            _versionInitVal = getVersion();
        }
        registerChange("version", _versionInitVal, version);
        _version = version;
    }
                        
    /**
     * Returns the property 'version'.
     */
    public String getVersion() {
        return _version;
    }
                                            
    /**
     * Setter for the property 'tokens'.
     */
    public void setTokens(Collection<SchufaScoreToken> tokens) {
        _tokens = tokens;
    }
            
    /**
     * Returns the property 'tokens'.
     */
    public Collection<SchufaScoreToken> getTokens() {
        return _tokens;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaScore.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _score=").append(_score);
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _remark=").append(_remark);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _version=").append(_version);
            builder.append("\n}");
        } else {
            builder.append(SchufaScore.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SchufaScore instance() {
        return this;
    }
}
