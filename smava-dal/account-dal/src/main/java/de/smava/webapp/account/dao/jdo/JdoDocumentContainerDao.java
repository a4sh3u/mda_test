//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document container)}

import de.smava.webapp.account.dao.DocumentContainerDao;
import de.smava.webapp.account.domain.DocumentContainer;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'DocumentContainers'.
 *
 * @author generator
 */
@Repository(value = "documentContainerDao")
public class JdoDocumentContainerDao extends JdoBaseDao implements DocumentContainerDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDocumentContainerDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(document container)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the document container identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public DocumentContainer load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainer - start: id=" + id);
        }
        DocumentContainer result = getEntity(DocumentContainer.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainer - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public DocumentContainer getDocumentContainer(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	DocumentContainer entity = findUniqueEntity(DocumentContainer.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the document container.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(DocumentContainer documentContainer) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDocumentContainer: " + documentContainer);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(document container)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(documentContainer);
    }

    /**
     * @deprecated Use {@link #save(DocumentContainer) instead}
     */
    public Long saveDocumentContainer(DocumentContainer documentContainer) {
        return save(documentContainer);
    }

    /**
     * Deletes an document container, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document container
     */
    public void deleteDocumentContainer(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDocumentContainer: " + id);
        }
        deleteEntity(DocumentContainer.class, id);
    }

    /**
     * Retrieves all 'DocumentContainer' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<DocumentContainer> getDocumentContainerList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - start");
        }
        Collection<DocumentContainer> result = getEntities(DocumentContainer.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'DocumentContainer' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<DocumentContainer> getDocumentContainerList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - start: pageable=" + pageable);
        }
        Collection<DocumentContainer> result = getEntities(DocumentContainer.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentContainer' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<DocumentContainer> getDocumentContainerList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - start: sortable=" + sortable);
        }
        Collection<DocumentContainer> result = getEntities(DocumentContainer.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentContainer' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<DocumentContainer> getDocumentContainerList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentContainer> result = getEntities(DocumentContainer.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentContainer' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentContainer> findDocumentContainerList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - start: whereClause=" + whereClause);
        }
        Collection<DocumentContainer> result = findEntities(DocumentContainer.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentContainer' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<DocumentContainer> findDocumentContainerList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<DocumentContainer> result = findEntities(DocumentContainer.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'DocumentContainer' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentContainer> findDocumentContainerList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<DocumentContainer> result = findEntities(DocumentContainer.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentContainer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentContainer> findDocumentContainerList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<DocumentContainer> result = findEntities(DocumentContainer.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentContainer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentContainer> findDocumentContainerList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentContainer> result = findEntities(DocumentContainer.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentContainer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentContainer> findDocumentContainerList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentContainer> result = findEntities(DocumentContainer.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentContainer' instances.
     */
    public long getDocumentContainerCount() {
        long result = getEntityCount(DocumentContainer.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentContainer' instances which match the given whereClause.
     */
    public long getDocumentContainerCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(DocumentContainer.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentContainer' instances which match the given whereClause together with params specified in object array.
     */
    public long getDocumentContainerCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(DocumentContainer.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentContainerCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document container)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
