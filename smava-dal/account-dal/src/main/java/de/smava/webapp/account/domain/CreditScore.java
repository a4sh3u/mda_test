package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CreditScoreHistory;

import java.util.Date;

/**
 * The domain object that represents 'CreditScores'.
 */
public class CreditScore extends CreditScoreHistory  {

        protected Long _score;
        protected String _rating;
        protected Account _account;
        protected SchufaScore _schufaScore;
        protected EconomicalData _economicalData;
        protected Date _changeDate;
        protected String _type;

    /**
     * Setter for the property 'score'.
     */
    public void setScore(Long score) {
        _score = score;
    }
            
    /**
     * Returns the property 'score'.
     */
    public Long getScore() {
        return _score;
    }
                                    /**
     * Setter for the property 'rating'.
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     */
    public String getRating() {
        return _rating;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'schufa score'.
     */
    public void setSchufaScore(SchufaScore schufaScore) {
        _schufaScore = schufaScore;
    }
            
    /**
     * Returns the property 'schufa score'.
     */
    public SchufaScore getSchufaScore() {
        return _schufaScore;
    }
                                            
    /**
     * Setter for the property 'economical data'.
     */
    public void setEconomicalData(EconomicalData economicalData) {
        _economicalData = economicalData;
    }
            
    /**
     * Returns the property 'economical data'.
     */
    public EconomicalData getEconomicalData() {
        return _economicalData;
    }
                                    /**
     * Setter for the property 'change date'.
     */
    public void setChangeDate(Date changeDate) {
        if (!_changeDateIsSet) {
            _changeDateIsSet = true;
            _changeDateInitVal = getChangeDate();
        }
        registerChange("change date", _changeDateInitVal, changeDate);
        _changeDate = changeDate;
    }
                        
    /**
     * Returns the property 'change date'.
     */
    public Date getChangeDate() {
        return _changeDate;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CreditScore.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(CreditScore.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
