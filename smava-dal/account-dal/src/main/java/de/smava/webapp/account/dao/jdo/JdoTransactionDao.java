//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(transaction)}
import com.aperto.smava.util.IsNullDate;
import de.smava.webapp.account.todo.dao.TransactionDao;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.DefaultSortable;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.dao.util.IDaoUtils;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.exception.InvalidValueException;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Transactions'.
 *
 * @author generator
 */
@Repository(value = "transactionDao")
public class JdoTransactionDao extends JdoBaseDao implements TransactionDao {

    private static final Logger LOGGER = Logger.getLogger(JdoTransactionDao.class);
    
    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(transaction)}
    private static final long serialVersionUID = -8650995139396536356L;
    
    private static final String FIND_EARLIEST_TRANSAVTION_FRO_CONTRACT_SQL_BSP_ALLOCATION 
        = "select c.id as contract_id, ("
            + "    select t.id from transaction t"
            + "    where t.type = 'TRANSFER' and t.state = 'OPEN' "
            + "        and t.debit_account = ?"
            + "        and t.credit_account is not null"
            + "        and t.credit_account = ("
            + "                select ba.id from offer o "
            + "                inner join bank_account ba on o.account_id = ba.account_id "
            + "                where o.id = c.offer_id"
            + "                    and ba.expiration_date is null"
            + " 					and ba.type = 'KOK'"
            + "                     and ba.bank_service_provider_id in ("
            + "                        select id from bank_service_provider where type = 'ALLOCATION' "
            + "                     )"
            + "        )"
            + "    order by t.\"dueDate\" asc limit 1"
            + ") as transaction_id "
            + "from contract c "
            + "where c.id in (";
    
    private static final String MIN_DIRECT_PER_CONTRACT_SQL
        = "select min(t_count) from (\n"
        + "select count(distinct(t.id)) as t_count from transaction t\n"
        + "inner join bank_account ba on t.credit_account = ba.id\n"
        + "inner join booking b on b.transaction_id = t.id\n"
        + "inner join contract c on b.contract_id = c.id\n"
        + "where ba.account_id = :accountId and ba.expiration_date is null and ba.type = '" + BankAccount.TYPE_BORROWER_ACCOUNT + "' and t.state = '" + Transaction.STATE_SUCCESSFUL + "' and t.type = '" + Transaction.TYPE_DIRECT_DEBIT + "' and c.state in (:contractStates)\n"
        + "group by c.id\n"
        + ") as tmp";
    
    private static final String START_GET_AMOUNT_SUM 
        = "select b.transaction_id as \"id\" from booking b "
            + "where b.transaction_id in (";
    
    private static final String END_GET_AMOUNT_SUM
        = ") group by b.transaction_id "
            + "having sum(b.amount) < -0.00001";
    
    private static final String GET_FOR_CONTRACT_AND_ACCOUNTS
        = "select distinct t.id as id from transaction t\n"
        + "inner join booking b on b.transaction_id = t.id\n"
        + "inner join booking_assignment ba on ba.booking = b.id\n"
        + "where t.state = '" + Transaction.STATE_OPEN + "' and t.credit_account = :creditAccountId and t.debit_account = :debitAccountId and t.\"dueDate\" = :dueDate and t.type = :transactionType \n"
        + "and b.contract_id = :contractId \n"
        + "and ba.is_target = true and ba.booking_group = :bookingGroupId";

    private static final String FIND_EARLIEST_TRANSACTION_AFTER_DATE_SQL
        = "select distinct t.id as id, t.\"dueDate\""
        + " from transaction t"
        + " left outer join booking bo on t.id = bo.transaction_id"
//    	+ " left outer join booking_assignment ba on ba.booking = bo.id"
        + " left outer join contract c on bo.contract_id = c.id"
        + " where t.state = '" + Transaction.STATE_OPEN + "'"
//    	+ " and ba.is_target = 't'"
        + " and bo.type_new = :bookingType"
        + " and t.type = :transactionType"
        + " and c.id in (:conracts)"
        + " and t.credit_account = :credit"
        + " and t.debit_account = :debit"
        + " and t.\"dueDate\" > :date"
        + " order by t.\"dueDate\" asc";

    private IDaoUtils _daoUtils;

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the transaction identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Transaction load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransaction - start: id=" + id);
        }
        Transaction result = getEntity(Transaction.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransaction - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Transaction getTransaction(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Transaction entity = findUniqueEntity(Transaction.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the transaction.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Transaction transaction) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveTransaction: " + transaction);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(transaction)}
        transaction.setModifiedDate(CurrentDate.getDate());
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(transaction);
    }

    /**
     * @deprecated Use {@link #save(Transaction) instead}
     */
    public Long saveTransaction(Transaction transaction) {
        return save(transaction);
    }

    /**
     * Deletes an transaction, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the transaction
     */
    public void deleteTransaction(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteTransaction: " + id);
        }
        deleteEntity(Transaction.class, id);
    }

    /**
     * Retrieves all 'Transaction' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Transaction> getTransactionList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - start");
        }
        Collection<Transaction> result = getEntities(Transaction.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Transaction' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Transaction> getTransactionList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - start: pageable=" + pageable);
        }
        Collection<Transaction> result = getEntities(Transaction.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Transaction' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Transaction> getTransactionList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - start: sortable=" + sortable);
        }
        Collection<Transaction> result = getEntities(Transaction.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Transaction' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Transaction> getTransactionList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Transaction> result = getEntities(Transaction.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Transaction' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Transaction> findTransactionList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - start: whereClause=" + whereClause);
        }
        Collection<Transaction> result = findEntities(Transaction.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Transaction' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Transaction> findTransactionList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        
        Sortable sorting = new DefaultSortable("_id", false);
        Collection<Transaction> result = findEntities(Transaction.class, whereClause, null, sorting, params );
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Transaction' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Transaction> findTransactionList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Transaction> result = findEntities(Transaction.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Transaction' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Transaction> findTransactionList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Transaction> result = findEntities(Transaction.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Transaction' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Transaction> findTransactionList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Transaction> result = findEntities(Transaction.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Transaction' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Transaction> findTransactionList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Transaction> result = findEntities(Transaction.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findTransactionList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Transaction' instances.
     */
    public long getTransactionCount() {
        long result = getEntityCount(Transaction.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Transaction' instances which match the given whereClause.
     */
    public long getTransactionCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Transaction.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Transaction' instances which match the given whereClause together with params specified in object array.
     */
    public long getTransactionCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Transaction.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getTransactionCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!! {|customMethods|bean(transaction)}
    //

    /**
     {@inheritDoc}
     */
    @Override
    public void replaceBankAccount(BankAccount oldBankAccount, BankAccount bankAccount) {
        OqlTerm creditTerm = OqlTerm.newTerm();
        creditTerm.and(creditTerm.equals("_creditAccount", oldBankAccount), creditTerm.equals("_state", Transaction.STATE_OPEN));
        Collection<Transaction> creditTransactions = findTransactionList(creditTerm.toString(), oldBankAccount);
        for (Transaction transaction : creditTransactions) {
            transaction.setCreditAccount(bankAccount);
            save(transaction);
        }

        OqlTerm debitTerm = OqlTerm.newTerm();
        debitTerm.and(debitTerm.equals("_debitAccount", oldBankAccount), debitTerm.equals("_state", Transaction.STATE_OPEN));
        Collection<Transaction> debitTransactions = findTransactionList(debitTerm.toString(), oldBankAccount);
        for (Transaction transaction : debitTransactions) {
            transaction.setDebitAccount(bankAccount);
            save(transaction);
        }
    }

    
    public double getRepaySum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, BankAccount smavaBankAccount) {
        List<BankAccount> debitAccounts = new LinkedList<BankAccount>();
        debitAccounts.add(smavaBankAccount);
        return getBookingSum(bookingTypes, limitDate, transactionStates, debitAccounts, account.getInternalBankAccountHistory(bankAccountProvider), null);
    }



    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Booking> getBookings(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getBookingsQuery(integerTypes, limitDate, transactionStates, debitAccounts, creditAccounts, null, null);
        Map<String, Object> parameterMap = buildParameterMap(integerTypes, limitDate, transactionStates, debitAccounts, creditAccounts, null, null);

        
        @SuppressWarnings("unchecked")
        Collection<Booking> result = (Collection<Booking>) q.executeWithMap(parameterMap);
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public double getBookingSum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames, Set<String> contractStates, Collection<Contract> contracts) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getBookingsQuery2(integerTypes, limitDate, transactionStates, debitAccounts, creditAccounts, marketNames, contractStates, contracts);

        Map<Integer, Object> parameterMap = new HashMap<Integer, Object>();
        if (limitDate != null) {
            parameterMap.put(1, new java.sql.Date(limitDate.getTime()));
        }

        q.setResultClass(Double.class);
        q.setUnique(true);
        Double result = (Double) q.executeWithMap(parameterMap);
        return (result == null) ? 0 : result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public double getBookingSum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames) {
        return getBookingSum(bookingTypes, limitDate, transactionStates, debitAccounts, creditAccounts, marketNames, null, null);
    }
    
    private List<Integer> getIntegerTypes(final List<BookingType> bookingTypes) {
        final List<Integer> result;
        if (bookingTypes == null) {
            result = null;
        } else if (bookingTypes.isEmpty()) {
            result = Collections.EMPTY_LIST;
        } else {
            result = new ArrayList<Integer>(bookingTypes.size());
            for (BookingType bookingType : bookingTypes) {
                result.add(bookingType.getDbValue());
            }
        }
        return result;
    }

    private Map<String, Object> buildParameterMap(List<Integer> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames, Set<String> contractStates) {
        Map<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("transactionStates", transactionStates);
        
        if (bookingTypes != null) {
            parameterMap.put("bookingTypes", bookingTypes);    
        }        
        if (creditAccounts != null) {
            parameterMap.put("creditAccountList", creditAccounts);
        }
        if (debitAccounts != null) {
            parameterMap.put("debitAccountList", debitAccounts);
        }
        if (limitDate != null) {
            parameterMap.put("sysDate", limitDate);
        }
        
        if (marketNames != null) {
            parameterMap.put("marketNames", marketNames);
        }
        if (contractStates != null && !contractStates.isEmpty()) {
            parameterMap.put("contractStates", contractStates);
        }
        return parameterMap;
    }



    private Query getBookingsQuery2(List<Integer> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames, Set<String> contractStates, Collection<Contract> contracts) {
        StringBuilder sqlStatement = new StringBuilder(); 
        sqlStatement.append("select sum(b.amount) from booking b, transaction t");
        
        // for logging to find very long running useless query
        boolean allEmpty = true;
        
        if ((marketNames != null && !marketNames.isEmpty()) 
            || (contractStates != null && !contractStates.isEmpty())
            || (contracts != null && !contracts.isEmpty())) {
            sqlStatement.append(",contract c");
            allEmpty = false;
        }
        
        sqlStatement.append(" where b.transaction_id = t.id ");

        if ((marketNames != null && !marketNames.isEmpty()) 
            || (contractStates != null && !contractStates.isEmpty())
            || (contracts != null && !contracts.isEmpty())) {
            sqlStatement.append(" and b.contract_id = c.id");
        }

        
        if (bookingTypes != null && !bookingTypes.isEmpty()) {
            sqlStatement.append(" and b.type_new in ( ").append(StringUtils.join(bookingTypes, ",")).append(")");
            allEmpty = false;
        }
        
        if (limitDate != null) {
            sqlStatement.append(" and t.transaction_date <= ?");
        }
        
        if (transactionStates != null && !transactionStates.isEmpty()) {
            sqlStatement.append(" and t.state in ('").append(StringUtils.join(transactionStates, "','")).append("')");
        }
        
        if (debitAccounts != null && !debitAccounts.isEmpty()) {
            Collection<Long> ids = extractIds(debitAccounts);
            sqlStatement.append(" and t.debit_account in (").append(StringUtils.join(ids, ",")).append(")");
            allEmpty = false;
        }

        if (creditAccounts != null && !creditAccounts.isEmpty()) {
            Collection<Long> ids = extractIds(creditAccounts);
            sqlStatement.append(" and t.credit_account in (").append(StringUtils.join(ids, ",")).append(")");
            allEmpty = false;
        }
        
        if (marketNames != null && !marketNames.isEmpty()) {
            sqlStatement.append(" and c.market_name in ('").append(StringUtils.join(marketNames, "','")).append("')");
        }

        if (contractStates != null && !contractStates.isEmpty()) {
            sqlStatement.append(" and c.state in ('").append(StringUtils.join(contractStates, "','")).append("')");
        }

        if (contracts != null && !contracts.isEmpty()) {
            Collection<Long> ids = extractIds(contracts);
            sqlStatement.append(" and c.id in ('").append(StringUtils.join(ids, "','")).append("')");
        }

        if (allEmpty) {
            LOGGER.error("very long running sql query found!!!", new RuntimeException());
        }

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sqlStatement.toString());
        return q;
    }

    private Collection<Long> extractIds(Collection<? extends Entity> entities) {
        Collection<Long> ids = new HashSet<Long>();
        for (Entity entity : entities) {
            ids.add(entity.getId());
        }
        return ids;
    }
    
    
    
    private Query getBookingsQuery(List<Integer> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames, Set<String> contractStates) {
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("transactionStates.contains(_transaction._state)");

        
        if (bookingTypes != null) {
            whereClause.append(" && bookingTypes.contains(_integerType)");
        }
        if (creditAccounts != null) {
            whereClause.append(" && creditAccountList.contains(_transaction._creditAccount)");
        }
        if (debitAccounts != null) {
            whereClause.append(" && debitAccountList.contains(_transaction._debitAccount) ");
        }
        if (limitDate != null) {
            whereClause.append(" && _transaction._transactionDate <= sysDate");
        }
        
        if (marketNames != null) {
            whereClause.append(" && marketNames.contains(_contract._marketName)");
        }
        if (contractStates != null && !contractStates.isEmpty()) {
            whereClause.append(" && contractStates.contains(_contract._state)");
        }
        
        Query q = getPersistenceManager().newQuery(Booking.class, whereClause.toString());

        String parameterDeclaration = "java.util.Set transactionStates";
        if (bookingTypes != null) {
            parameterDeclaration += ", java.util.List bookingTypes";
        }
        if (debitAccounts != null) {
            parameterDeclaration += ", java.util.List debitAccountList";
        }
        if (creditAccounts != null) {
            parameterDeclaration += ", java.util.List creditAccountList";
        }
        if (limitDate != null) {
            parameterDeclaration += ", java.util.Date sysDate";
        }
        if (contractStates != null && !contractStates.isEmpty()) {
            parameterDeclaration += ", java.util.List contractStates";
        }        
        if (marketNames != null) {
            parameterDeclaration += ", java.util.Collection marketNames";
        }
        
        q.declareParameters(parameterDeclaration);
        return q;

    }

    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> getDepositTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "creditTransactions");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionStates", transactionStates);
        params.put("bookingTypes", integerTypes);
        params.put("internalBankAccountList", account.getInternalBankAccountHistory(bankAccountProvider));
        params.put("smavaBankAccountList", new LinkedList<BankAccount>());
        addReferenceBankAccountList(params, account);
        params.put("limitDate", limitDate);
        
        Collection result = (Collection) q.executeWithMap(params);

        LOGGER.debug("result:" + result);

        return (Collection<Transaction>) result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> getPayoutTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "debitTransactions");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionStates", transactionStates);
        params.put("bookingTypes", integerTypes);
        params.put("internalBankAccountList", account.getInternalBankAccountHistory(bankAccountProvider));
        params.put("smavaBankAccountList", new LinkedList<BankAccount>());
        addReferenceBankAccountList(params, account);
        params.put("limitDate", limitDate);

        Collection result = (Collection) q.executeWithMap(params);

        LOGGER.debug("result:" + result);

        return (Collection<Transaction>) result;
    }
    /**
     {@inheritDoc}
     */
    @Override
    public double getInvestmentSum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, List <BankAccount> smavaAccounts, Collection<String> marketNames, Set<String> contractStates) throws InvalidValueException {
        double result = 0.0;
        if (null == smavaAccounts 
                || smavaAccounts.size() <= 0) {
            throw new InvalidValueException(
                    "Smava interim BankAccount must not be null!");
        }
        List<BankAccount> internalBankAccounts = account.getInternalBankAccountHistory(bankAccountProvider);
        if (internalBankAccounts != null && !internalBankAccounts.isEmpty()) {
            result = getBookingSum(bookingTypes,
                    limitDate, 
                    transactionStates, 
                    internalBankAccounts, 
                    smavaAccounts,
                    marketNames,
                    contractStates,
                    null);
        }

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> getInvestmentTransactions(List<BookingType> bookingTypes, Date limitDate,
        Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider,
        Collection<BankAccount> smavaAccounts) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "debitTransactions");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionStates", transactionStates);
        params.put("bookingTypes", integerTypes);
        params.put("internalBankAccountList", account.getInternalBankAccountHistory(bankAccountProvider));
        params.put("smavaBankAccountList", smavaAccounts);
        params.put("referenceBankAccountList", new LinkedList<BankAccount>());
        params.put("limitDate", limitDate);

        Collection result = (Collection) q.executeWithMap(params);

        LOGGER.debug("result:" + result);

        return (Collection<Transaction>) result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> getCreditTransactions(List<BookingType> bookingTypes,
            Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider,
            BankAccount smavaAccount) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "creditTransactions");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionStates", transactionStates);
        params.put("bookingTypes", integerTypes);
        params.put("internalBankAccountList", account.getInternalBankAccountHistory(bankAccountProvider));
        params.put("smavaBankAccountList", Collections.singletonList(smavaAccount));
        addReferenceBankAccountList(params, account);

        params.put("limitDate", limitDate);

        Collection result = (Collection) q.executeWithMap(params);

        LOGGER.debug("result:" + result);

        return (Collection<Transaction>) result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> getDebitTransactions(List<BookingType> bookingTypes,
                                                        Date limitDate,
                                                        Set<String> transactionStates,
                                                        Account account,
                                                        BankAccountProvider bankAccountProvider,
                                                        BankAccount smavaAccount) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "debitTransactions");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionStates", transactionStates);
        params.put("bookingTypes", integerTypes);
        params.put("internalBankAccountList", account.getInternalBankAccountHistory(bankAccountProvider));
        params.put("smavaBankAccountList", Collections.singletonList(smavaAccount));
        addReferenceBankAccountList(params, account);
        params.put("limitDate", limitDate);

        @SuppressWarnings("unchecked")
        Collection<Transaction> result = (Collection<Transaction>) q.executeWithMap(params);

        LOGGER.debug("result:" + result);

        return result;
    }

    private void addReferenceBankAccountList(Map<String, Object> params, Account account) {
        List<BankAccount> referenceBankAccounts = new ArrayList<BankAccount>();
        referenceBankAccounts.addAll(account.getReferenceBankAccountHistory());
        referenceBankAccounts.addAll(account.getOtherBankAccounts());
        params.put("referenceBankAccountList", referenceBankAccounts);
    }

    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> getTransactions(BankAccount creditAccount, BankAccount debitAccount, Date date, String state) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "getAllTransactionWithDebitAccAndCreditAcc");
        q.declareImports("import de.smava.webapp.account.domain.BankAccount");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("creditAccount", creditAccount);
        params.put("debitAccount", debitAccount);
        params.put("transactionDate", date);
        params.put("state", state);
        return (Collection<Transaction>) q.executeWithMap(params);
    }

    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> getOpenTransactions(BankAccount creditAccount, BankAccount debitAccount, Date date) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "getOpenTransactionWithDebitAccAndCreditAcc");
        q.declareImports("import de.smava.webapp.account.domain.BankAccount");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("creditAccount", creditAccount);
        params.put("debitAccount", debitAccount);
        params.put("dueDate", date);
        return (Collection<Transaction>) q.executeWithMap(params);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction getRandomOpenTransaction( BankAccount creditAccount, BankAccount debitAccount, Date date, Contract contract){

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("credit", creditAccount.getId());
        params.put("debit", debitAccount.getId());
        params.put("date", new Timestamp(date.getTime()));
        params.put("transactionType", Transaction.TYPE_TRANSFER);
        params.put("contractId", contract.getId());

        String sql  =  "select distinct t.id as id from transaction t, booking b"
        + " where t.state = '" + Transaction.STATE_OPEN + "'"
        + " and b.transaction_id = t.id"
        + " and b.contract_id = :contractId"
        + " and t.type = :transactionType"
        + " and t.credit_account = :credit"
        + " and t.debit_account = :debit"
        + " and t.\"dueDate\" <= :date"
        + " limit 1";

    
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setClass(Transaction.class);

        Collection<Transaction> results = (Collection<Transaction>) q.executeWithMap(params);

        if (!results.isEmpty()) {
            return results.iterator().next();
        }
        return null;
    }
    

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findEarliestOpenTransferTransaction(BankAccount creditAccount, BankAccount debitAccount, Date dueDate, String type) {
        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = findEarliestOpenTransferTransactions(creditAccount, debitAccount, dueDate, type);
        Transaction transaction = null;
        if (!transactions.isEmpty()) {
            transaction = transactions.iterator().next();
        }
        return transaction;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findEarliestOpenTransferTransactions(BankAccount creditAccount, BankAccount debitAccount, Date dueDate, String type) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findEarliestOpenTransferTransaction");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("creditAccount", creditAccount);
        params.put("debitAccount", debitAccount);
        params.put("dueDate", dueDate);
        params.put("type", type);

        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = (Collection<Transaction>) q.executeWithMap(params);
        return transactions;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findEarliestOpenTransaction(BankAccount debitAccount, BankAccount creditAccount, Date dueDate, String type) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findEarliestOpenTransaction");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("creditAccount", creditAccount);
        params.put("debitAccount", debitAccount);
        params.put("dueDate", dueDate);
        params.put("type", type);

        q.setRange(0, 1);
        q.setUnique(true);
        final Transaction transaction = (Transaction) q.executeWithMap(params);

        return transaction;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findOpenTransactionForUnscheduledRepayment(BankAccount debitAccount, BankAccount creditAccount, String type) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findOpenTransactionForUnscheduledRepayment");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("creditAccount", creditAccount);
        params.put("debitAccount", debitAccount);
        params.put("type", type);

        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = (Collection<Transaction>) q.executeWithMap(params);
        Transaction transaction = null;
        if (!transactions.isEmpty()) {
            transaction = transactions.iterator().next();
        }

        return transaction;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findEarliestOpenTransactionAfterDate(String type, BankAccount debitAccount, BankAccount creditAccount, Date date) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findEarliestOpenTransactionAfterDate");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("creditAccount", creditAccount);
        params.put("debitAccount", debitAccount);
        params.put("date", date);
        params.put("type", type);

        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = (Collection<Transaction>) q.executeWithMap(params);
        Transaction transaction = null;
        if (!transactions.isEmpty()) {
            transaction = transactions.iterator().next();
        }
        return transaction;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findEarliestOpenTransactionForBookingInContractsAfterDate(
            String transactionType, BookingType bookingType, BankAccount debit, BankAccount credit, Date date,
            Set<Contract> contracts) {
        Transaction result = null;
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionType", transactionType);
        params.put("bookingType", bookingType.getDbValue());
        params.put("credit", credit.getId());
        params.put("debit", debit.getId());
        params.put("date", new Timestamp(date.getTime()));
        params.put("conracts", contracts);

        final String sql = replaceCollectionTypes(params, FIND_EARLIEST_TRANSACTION_AFTER_DATE_SQL);
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setClass(Transaction.class);

        Collection<Transaction> results = (Collection<Transaction>) q.executeWithMap(params);

        if (!results.isEmpty()) {
            result = results.iterator().next();
        }

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findBorrowerReminderTransactions(Date date) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findReminderTransactions");
        
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        
        params.put(1, Transaction.TYPE_TRANSFER); //1
        params.put(2, Transaction.STATE_OPEN); //2
        params.put(3, new Timestamp(date.getTime())); //3
        
        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = (Collection<Transaction>) q.executeWithMap(params);

        return transactions;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public ReminderState findEncashmentReminderState(Contract contract) {
        Query q = getPersistenceManager().newNamedQuery(ReminderState.class, "findEncashmentReminderState");
        
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, 4);
        params.put(2, Transaction.STATE_FAILED);
        params.put(3, contract.getId());
        
        @SuppressWarnings("unchecked")
        Collection<ReminderState> reminderStates = (Collection<ReminderState>) q.executeWithMap(params);
        
        return reminderStates == null || reminderStates.size() != 1 ? null : reminderStates.iterator().next(); 
    }


    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findReminderTransactionsForLenderNotification(Date date) {

        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findReminderTransactionsForLenderNotification");
        LOGGER.debug(q.toString());
        
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, Transaction.TYPE_TRANSFER);
        params.put(2, Transaction.STATE_OPEN);
        Timestamp sqlDate = new Timestamp(date.getTime());
        params.put(3, sqlDate);
        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = (Collection<Transaction>) q.executeWithMap(params);

        return transactions;
    }



    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Transaction> findTransactions(String transactionType, String transactionState, Collection<BankAccount> debitAccounts, Collection<BankAccount> creditAccounts, Date fromDate, Date toDate) {
        Query q = getPersistenceManager().newNamedQuery(Transaction.class, "findTransactions");
        
        if (debitAccounts == null) {
            debitAccounts = new ArrayList<BankAccount>();
        }
        
        if (creditAccounts == null) {
            creditAccounts = new ArrayList<BankAccount>();
        }
        
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionType", transactionType);
        params.put("transactionState", transactionState);
        params.put("creditAccounts", creditAccounts);
        params.put("debitAccounts", debitAccounts);
        params.put("fromDate", fromDate);
        params.put("toDate", toDate);

        @SuppressWarnings("unchecked")
        Collection<Transaction> transactions = (Collection<Transaction>) q.executeWithMap(params);
        return transactions;
    }

    @SuppressWarnings("unchecked")
    public Collection<Transaction> findTransactionCandidates(String sqlStatement) {
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sqlStatement);
        q.setClass(Transaction.class);

        return (Collection<Transaction>) q.execute();
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findEarliestOpenTransferTransactionByMainOrder(BankAccount debit, BankAccount credit, Booking booking, Order mainOrder, Date dueDate) {
        Transaction result = null;
        String sql = "select t.id as id "
            + "from transaction t "
                + "left outer join booking bo on t.id = bo.transaction_id "
                + "left outer join contract c on bo.contract_id = c.id "
                + "left outer join bid b on c.order_id = b.id "
            + "where 1=1 "
                + "and t.state = '" + Transaction.STATE_OPEN + "' "
                + "and t.type = '" + Transaction.TYPE_TRANSFER + "' "
                + "and (c.id = " + booking.getContract().getId() + " or b.\"groupId\" = " + mainOrder.getId() + " or b.id = " + mainOrder.getId() + ") "
                + "and t.credit_account = " + credit.getId() + " "
                + "and t.debit_account = " + debit.getId();
        if (dueDate != null) {
            sql += " and t.\"dueDate\" = ? ";
        }

        sql += " order by t.\"dueDate\" asc";
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setClass(Transaction.class);
        @SuppressWarnings("unchecked")
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (dueDate != null) {
            params.put(1, new java.sql.Date(dueDate.getTime()));
        }

        Collection<Transaction> results = (Collection<Transaction>) q.executeWithMap(params);

        if (!results.isEmpty()) {
            result = results.iterator().next();
        }

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findOpenTransaction(final BankAccount credit, final BankAccount debit, final Date dueDate, final String transactionType, final Contract contract, final BookingGroup bookingGroup) {
        final Map<String, Object> params = new HashMap<String, Object>(6);
        params.put("creditAccountId", credit.getId());
        params.put("debitAccountId", debit.getId());
        params.put("dueDate", new Timestamp(dueDate.getTime()));
        params.put("transactionType", transactionType);
        params.put("contractId", contract.getId());
        params.put("bookingGroupId", bookingGroup.getId());
        final Query q = getPersistenceManager().newQuery(Query.SQL, GET_FOR_CONTRACT_AND_ACCOUNTS);
        q.setClass(Transaction.class);
        q.setUnique(true);
        return (Transaction) q.executeWithMap(params);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public int getOpenReminderTransactionsCount(Account account) {
        Integer result = null;

        String sql = "select count(distinct(t.id)) as id "
            + "from transaction t "
                + "left outer join dunning_state ds on ds.transaction_id = t.id "
                + "left outer join bank_account ba on t.credit_account = ba.id "
                + "left outer join account a on ba.account_id = a.id "
                + "where 1=1 "
                + "and ds.level >= 0 "
                + "and t.type = '" + Transaction.TYPE_TRANSFER + "' "
                + "and t.state = '" + Transaction.STATE_OPEN + "' "
                + "and a.id = " + account.getId() + ";";
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setResultClass(Integer.class);

        result = (Integer) ((List) q.execute()).iterator().next();

        return result.intValue();
    }

    /**
     {@inheritDoc}
     */
    @Override
    public int getMaxDunnungLevel(Account account) {
        Integer result = null;

        String sql = "select max(ds.\"level\") as id "
            + "from transaction t "
                + "left outer join dunning_state ds on ds.transaction_id = t.id "
                + "left outer join bank_account ba on t.credit_account = ba.id "
                + "left outer join account a on ba.account_id = a.id "
                + "where 1=1 "
                + "and ds.level >= 0 "
                + "and t.type = '" + Transaction.TYPE_TRANSFER + "' "
                + "and t.state = '" + Transaction.STATE_OPEN + "' "
                + "and a.id = " + account.getId() + ";";
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setResultClass(Integer.class);

        result = (Integer) ((List) q.execute()).iterator().next();

        return result.intValue();
    }

    /**
     {@inheritDoc}
     */
    @Override
    public int getReturnDebitCount(final Account account) {
        return getReturnDebitCount(account, null, Transaction.TYPE_RETURN_DEBIT, Collections.singleton(Transaction.STATE_SUCCESSFUL));
    }

    /**
     {@inheritDoc}
     */
    @Override
    public int getReturnDebitCount(final Account account, final Integer dunningLevel) {
        return getReturnDebitCount(account, dunningLevel, Transaction.TYPE_TRANSFER, Transaction.OPEN_SUCCESSFUL_STATE);
    }

    private int getReturnDebitCount(final Account account, final Integer dunningLevel, final String type, final Set<String> states) {
        Integer result = null;

        StringBuilder sqlBuilder = new StringBuilder("select count(t.id) as id ");
            sqlBuilder.append("from transaction t ");
            sqlBuilder.append("left outer join bank_account ba on t.credit_account = ba.id ");
            if (dunningLevel != null) {
                sqlBuilder.append("inner join dunning_state ds on ds.transaction_id = t.id ");
            }
            sqlBuilder.append("where t.type = :type and t.state in (:states) and ba.account_id = :accountId");
            if (dunningLevel != null) {
                sqlBuilder.append(" and ds.level = :dunninglevel");
            }
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("accountId", account.getId());
        params.put("dunninglevel", dunningLevel);
        params.put("type", type);
        params.put("states", states);
        final String sql = replaceCollectionTypes(params, sqlBuilder.toString());
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setResultClass(Integer.class);

        result = (Integer) ((List) q.executeWithMap(params)).iterator().next();

        return result.intValue();
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Date getLastReturnDebitDate(Account account) {
        Date result = null;

        StringBuilder sql = new StringBuilder("select max(t.transaction_date) as id ");
            sql.append("from transaction t ");
            sql.append("left outer join bank_account ba on t.credit_account = ba.id ");
            sql.append("left outer join account a on ba.account_id = a.id ");
            sql.append("where t.type = '");
            sql.append(Transaction.TYPE_RETURN_DEBIT);
            sql.append("' and t.state = '");
            sql.append(Transaction.STATE_SUCCESSFUL);
            sql.append("' and a.id = ");
            sql.append(account.getId());
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setResultClass(Date.class);

        result = (Date) ((List) q.execute()).iterator().next();

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public double getReminderAmount(Account account) {
        Double result = null;

        StringBuilder sql = new StringBuilder("select sum(amount) from (select distinct b.id, b.amount ");
            sql.append("from bank_account ba ");
            sql.append("inner join transaction t on t.credit_account = ba.id ");
            sql.append("inner join dunning_state ds on ds.transaction_id = t.id ");
            sql.append("inner join booking b on b.transaction_id = t.id ");
            sql.append("where ba.account_id = ");
            sql.append(account.getId());
            sql.append(" and t.type = '");
            sql.append(Transaction.TYPE_TRANSFER);
            sql.append("' and t.state = '");
            sql.append(Transaction.STATE_OPEN);
            sql.append("' and ds.level >= 0) tmp");
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setResultClass(Double.class);

        result = (Double) ((List) q.execute()).iterator().next();

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public double getPaybackSum(Account lender, BankAccountProvider bankAccountProvider, Date date, List<BankAccount> debitAccounts, List<BookingType> bookingTypes) {
        final List<Integer> integerTypes = getIntegerTypes(bookingTypes);
        Query q = getPersistenceManager().newNamedQuery(Booking.class, "getPaybackSum");
        q.setResult("sum(_amount)");
        q.setResultClass(Double.class);
        q.addExtension("datanucleus.jdoql.useInnerJoinsOnly", "true");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionType", Transaction.TYPE_TRANSFER);
        params.put("bookingTypes", integerTypes);
        params.put("transactionState", Transaction.STATE_SUCCESSFUL);
        params.put("creditAccounts", lender.getInternalBankAccountHistory(bankAccountProvider));
        params.put("debitAccounts", debitAccounts);
        Timestamp sqlDate = new Timestamp(date.getTime());
        params.put("dueDate", sqlDate);

        Double sum = (Double) q.executeWithMap(params);

        return sum;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Map<Long, Double> getAmountForTransactions(Collection<Transaction> transactions) {
        Map<Long, Double> result = new HashMap<Long, Double>();
        if (transactions != null && !transactions.isEmpty()) {

            StringBuilder sql = new StringBuilder();
            sql.append("select b.transaction_id as transactionId,sum(amount) as amount from booking b where b.transaction_id in ( ");
            boolean first = true;
            for (Transaction transaction : transactions) {
                if (!first) {
                    sql.append(",");
                } else {
                    first = false;
                }
                sql.append(transaction.getId());
            }
            sql.append(" ) group by b.transaction_id");
            Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
            q.setClass(Transaction.class);
            q.setResultClass(TransactionAmount.class);
            long startQuery = CurrentDate.getTime();
            Collection<TransactionAmount> c = (Collection<TransactionAmount>) q.execute();
            long query = CurrentDate.getTime() - startQuery;
            long startMap = CurrentDate.getTime();
            
            for (TransactionAmount ta : c) {
                result.put(ta.getTransactionid(), ta.getAmount());
            }
            long map = CurrentDate.getTime() - startMap;
            LOGGER.info("query: " + query + " - map: " + map + " count: " + transactions.size());
        }
        return result;
    }


    /**
     {@inheritDoc}
     */
    @Override
    public Map<Long, Boolean> getIsInterimMap(final Collection<BankAccount> interimAccounts, final String inIdsString) {
        Collection<String> interimAccountIds = new LinkedList<String>();
        for (BankAccount interimAccount : interimAccounts) {
            interimAccountIds.add(String.valueOf(interimAccount.getId()));
        }
        final Map<Long, Boolean> result;
        if (StringUtils.isNotBlank(inIdsString)) {
            final StringBuilder sql = new StringBuilder("select t.id, case when t.credit_account in (");
            sql.append(StringUtils.join(interimAccountIds, ','));
            sql.append(") then true else false end as is_interim from transaction t where t.id in (select b.transaction_id from booking b where b.id in (");
            sql.append(inIdsString);
            sql.append(") group by b.transaction_id)");
            final Query q = getPersistenceManager().newQuery(Query.SQL, sql.toString());
            final List<Object[]> sqlResult = (List<Object[]>) q.execute();
            final Map<Long, Boolean> resultMap = new HashMap<Long, Boolean>(sqlResult.size());
            for (Object[] row : sqlResult) {
                resultMap.put((Long) row[0], (Boolean) row[1]);
            }
            result = Collections.unmodifiableMap(resultMap);
        } else {
            result = Collections.emptyMap();
        }
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> getTransactionsWithNegativeAmount(
            Collection<Transaction> inputTransactions) {
        final List<Transaction> result;
        if (CollectionUtils.isNotEmpty(inputTransactions)) {
            final StringBuilder sql = new StringBuilder(START_GET_AMOUNT_SUM);
            sql.append(_daoUtils.getIdList(inputTransactions));
            sql.append(END_GET_AMOUNT_SUM);
            final Query q = getPersistenceManager().newQuery(Query.SQL, sql.toString());
            q.setClass(Transaction.class);
            result = (List<Transaction>) q.execute();
        } else {
            result = Collections.EMPTY_LIST;
        }
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findEarliestOpenTransactionAfterDateForContracts(String typeTransfer, BankAccount debit, BankAccount credit,
            Date transactionDate, Set<Contract> transactionContracts) {
        @SuppressWarnings("unchecked")
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        Transaction result = null;

        StringBuilder sql = new StringBuilder("select distinct t.id as id, t.\"dueDate\"");
        sql.append(" from transaction t");
        sql.append(" left outer join booking bo on t.id = bo.transaction_id");
        sql.append(" left outer join booking_assignment ba on ba.booking = bo.id");
        sql.append(" left outer join contract c on bo.contract_id = c.id");
        sql.append(" where t.state = '").append(Transaction.STATE_OPEN).append("'");
        sql.append(" and t.type = '").append(Transaction.TYPE_TRANSFER).append("'");
        sql.append(" and ba.is_target = 't'");
        sql.append(" and c.id in (");
        sql.append(buildContractList(transactionContracts, params));
        sql.append(")");
        sql.append(" and t.credit_account = ").append(credit.getId());
        sql.append(" and t.debit_account = ").append(debit.getId());
        sql.append(" and t.\"dueDate\" > ?");
        params.put(params.size() + 1, new Timestamp(transactionDate.getTime()));

        sql.append(" order by t.\"dueDate\" asc");

        LOGGER.info("SQL: " + sql.toString());

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setClass(Transaction.class);

        Collection<Transaction> results = (Collection<Transaction>) q.executeWithMap(params);

        if (!results.isEmpty()) {
            result = results.iterator().next();
        }

        return result;
    }

    private Object buildContractList(Set<Contract> transactionContracts, Map<Integer, Object> params) {
        StringBuilder list = new StringBuilder();
        for (Contract contract : transactionContracts) {
            list.append("?,");
            params.put(params.size() + 1, contract.getId());
        }

        return list.substring(0, list.length() - 1);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public int getMinSuccessfullDirectDebits(final Account account, final Set<String> contractStates) {
        Assert.notNull(account);
        Assert.notEmpty(contractStates);
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("accountId", account.getId());
        params.put("contractStates", contractStates);
        final String sql = replaceCollectionTypes(params, MIN_DIRECT_PER_CONTRACT_SQL);
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setUnique(true);
        final Long result = (Long) q.executeWithMap(params);
        return (int) com.aperto.smava.util.NumberUtils.valueOf(result, 0);
    }


    /**
     {@inheritDoc}
     */
    @Override
    public List<Transaction> getTransactionWithBookingsOfTypeAndContracts(
            final String transactionType, final String state, final BookingType bookingType,
            final Set<BankAccount> debitAccounts, final Set<BankAccount> creditAccounts,
            final Set<Contract> contracts, final Date dueDate) {
        final BankAccountTransferDirection direction = new BankAccountTransferDirection(debitAccounts, creditAccounts);
        return getTransactionWithBookingsOfTypeAndContracts(transactionType, state, bookingType, direction, contracts, dueDate, null);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findTransactionsWithBookingTypes(
            final Set<String> transactionTypes,
            final Set<String> transactionStates,
            final Set<BankAccount> debitAccounts,
            final Set<BankAccount> creditAccounts,
            final Set<BookingType> bookingTypes,
            final Date fromDate, final Date toDate) {
        final Map<String, Object> params = new LinkedHashMap<String, Object>();

        List<Long> debitAccountIds = new ArrayList<Long>();
        for (BankAccount bankAccount : debitAccounts) {
            debitAccountIds.add(bankAccount.getId());
        }
        List<Long> creditAccountIds = new ArrayList<Long>();
        for (BankAccount bankAccount : creditAccounts) {
            creditAccountIds.add(bankAccount.getId());
        }

        params.put("transactionTypes", transactionTypes == null ? Collections.emptySet() : transactionTypes);
        params.put("transactionStates", transactionStates == null ? Collections.emptySet() : transactionStates);
        params.put("debitAccounts", debitAccountIds);
        params.put("creditAccounts",  creditAccountIds);
        if (CollectionUtils.isNotEmpty(bookingTypes)) {
            final Set<Integer> integerBookingTypes = new LinkedHashSet<Integer>(bookingTypes.size());
            for (BookingType bookingType : bookingTypes) {
                integerBookingTypes.add(bookingType.getDbValue());
            }
            params.put("bookingTypes", integerBookingTypes);
        } else {
            params.put("bookingTypes", Collections.EMPTY_SET);
        }
        params.put("fromDate", fromDate);
        params.put("toDate", toDate);
        final Collection<Transaction> rowResult = executeNamedQuery(Transaction.class, "findTransactionsWithBookingTypes", params, null, false);
        return new LinkedHashSet<Transaction>(rowResult);
    }
    /**
     {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Transaction> getTransactionsForBankAccounts(
            Collection<BankAccount> bankAccounts, Date from, Date to,
            String ... transactionStates){
        if (null == from) {
            from = new Date(0);
        }
        if (null == to) {
            to = CurrentDate.getDate();
        }
        java.sql.Date dbFrom = new java.sql.Date(from.getTime());
        java.sql.Date dbTo = new java.sql.Date(to.getTime());

        List<Object> params = new ArrayList<Object>();
        String sql = "select id from transaction " +
                     " where state in (";
        for (int i = 0; i < transactionStates.length; i++) {
            if (i != 0) {
                sql += ", ";
            }
            sql += "?";
            params.add(transactionStates[i]);
        }
        String creditSql = "credit_account in (";
        String debitSql = "debit_account in (";
        boolean first = true;
        List<Long> bankAccountIds = new ArrayList<Long>();
        for (BankAccount bankAccount : bankAccounts) {
            if (!first){
                creditSql += ", ";
                debitSql += ", ";
            }
            creditSql += "?";
            debitSql += "?";
            bankAccountIds.add(bankAccount.getId());
            first = false;
        }
        /*
         * once for credit_account and a second time for debit_account
         */
        params.addAll(bankAccountIds);
        params.addAll(bankAccountIds);

        sql += ") and ("+creditSql+") or "+debitSql+
               ")) and (transaction_date between ? and ? " +
                  " or \"dueDate\" between ? and ? " +
                  " or (state = 'RESERVED' " +
                  "and creation_date between ? and ?)) ORDER BY \"dueDate\", \"creation_date\" ASC";
        params.add(dbFrom);
        params.add(dbTo);
        params.add(dbFrom);
        params.add(dbTo);
        params.add(dbFrom);
        params.add(dbTo);
        Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setClass(Transaction.class);

        return (List<Transaction>) q.executeWithArray(params.toArray(new Object[0]));
    }
    /**
     {@inheritDoc}
     */
    @Override
    public List<Transaction> getTransactionWithBookingsOfTypeAndContracts(
            final String transactionType, final String state, final BookingType bookingType,
            final BankAccountTransferDirection bankAccountTransferDirection,
            final Set<Contract> contracts, final Date dueDate, final String bookingGroupType) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionType", transactionType);
        params.put("state", state);
        if (bookingType != null) {
            params.put("bookingType", bookingType.getDbValue());
        }
        params.put("contracts", contracts);
        if (bankAccountTransferDirection != null) {
            params.put("debitAccounts", bankAccountTransferDirection.getDebits());
            params.put("creditAccounts", bankAccountTransferDirection.getCredits());
        }
        if (dueDate != null) {
            params.put("dueDate", new Timestamp(dueDate.getTime()));
        }
        params.put("bookingGroupType", bookingGroupType);
        final String sql = replaceCollectionTypes(params, getContractsAndBookingTypeSql(transactionType, state, bookingType, bankAccountTransferDirection, contracts, dueDate, bookingGroupType));
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setClass(Transaction.class);
        return (List<Transaction>) q.executeWithMap(params);
    }

    private String getContractsAndBookingTypeSql(
            final String transactionType, final String state, final BookingType bookingType,
            final BankAccountTransferDirection bankAccountTransferDirection,
            final Set<Contract> contracts, final Date dueDate, final String bookingGroupType) {
        final StringBuilder sql = new StringBuilder("select distinct t.id as id, t.\"dueDate\" from booking b inner join transaction t on b.transaction_id = t.id ");
        if (StringUtils.isNotBlank(bookingGroupType)) {
            sql.append(" inner join booking_assignment ba on ba.booking = b.id inner join booking_group bg on bg.id = ba.booking_group ");
        }
        boolean firstWhere = addCollectionParam(contracts, "b.contract_id in (:contracts) ", true, sql);
        firstWhere = addSimpleParam(bookingType, "b.type_new = :bookingType ", firstWhere, sql);
        firstWhere = addSimpleParam(transactionType, "t.type = :transactionType ", firstWhere, sql);
        firstWhere = addSimpleParam(state, "t.state = :state ", firstWhere, sql);
        if (bankAccountTransferDirection != null) {
            firstWhere = addCollectionParam(bankAccountTransferDirection.getDebits(), "t.debit_account in (:debitAccounts) ", firstWhere, sql);
            firstWhere = addCollectionParam(bankAccountTransferDirection.getCredits(), "t.credit_account in (:creditAccounts) ", firstWhere, sql);
        }
        if (dueDate != null) {
            if (dueDate instanceof IsNullDate) {
                firstWhere = addSqlPart(" t.\"dueDate\" is null ", firstWhere, sql);
            } else {
                firstWhere = addSqlPart(" t.\"dueDate\" = :dueDate ", firstWhere, sql);
            }
        }
        firstWhere = addSimpleParam(bookingGroupType, " bg.type = :bookingGroupType ", firstWhere, sql);
        sql.append("order by t.\"dueDate\"");
        return sql.toString();
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findTransactionsByTransactionId(String transactionID){
        String clause = "_transactionId == " + transactionID;
        return findTransactionList( clause);
    }

    private boolean addCollectionParam(final Collection<?> paramValue, final String sqlPart, final boolean firstWhere, final StringBuilder sql) {
        boolean result = firstWhere;
        if (CollectionUtils.isNotEmpty(paramValue)) {
            result = addSqlPart(sqlPart, firstWhere, sql);
        }
        return result;
    }

    private boolean addSimpleParam(final Object paramValue, final String sqlPart, final boolean firstWhere, final StringBuilder sql) {
        boolean result = firstWhere;
        if (paramValue != null) {
            result = addSqlPart(sqlPart, firstWhere, sql);
        }
        return result;
    }

    private boolean addSqlPart(final String sqlPart, final boolean firstWhere,
            final StringBuilder sql) {
        if (!firstWhere) {
            sql.append("and ");
        } else {
            sql.append("where ");
        }
        sql.append(sqlPart);
        return false;
    }

    public void setDaoUtils(IDaoUtils daoUtils) {
        _daoUtils = daoUtils;
    }



    /**
     {@inheritDoc}
     */
    @Override
    public Transaction findByEref(String eref) {
        Transaction result = null;
        Collection<Transaction> results = this.findAllByEref(eref);
        if (results != null && results.size() > 0) {
            result = results.iterator().next();
        }
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findAllByEref(String eref) {
        Collection<Transaction> result = null;
        if (eref != null) {
            try {
                OqlTerm term = OqlTerm.newTerm();
                term = term.equals("_transactionId", Long.valueOf(eref));
                term = term.or(term,OqlTerm.newTerm().equals("_id",Long.valueOf(eref)));
                Collection <Transaction> results = findEntities(Transaction.class, term.toString());
                result = results;
            } catch ( NumberFormatException e) {
                LOGGER.warn("cant match transaction with EREF, as EREF is not a number [" +eref + "]" );
            }
        }
        return result;
    }

    /**
    {@inheritDoc}
    */
    @Override
    public Transaction findEarliestOpenTransaction(String type, BankAccount debit, BankAccount credit){
        Transaction transaction = null;
        String term = "_type == '" + type  + "'"
                + " && _state == '" + Transaction.STATE_OPEN + "' "
                + " && _debitAccount._id == " + debit.getId()
                + " && _creditAccount._id == " + credit.getId() + " "
                + " order by _dueDate ascending";
        LOGGER.debug("findEarliestOpenTransaction: " + term);
        Collection<Transaction> transactions = findTransactionList(term);
        if (transactions.size() > 0) {
            transaction = transactions.iterator().next();
        }

        return transaction;

    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Transaction> findOpenTransactionsToIllegalBankAccount(){
        Date untilNow = CurrentDate.getDate();

        String where = "this._dueDate <= :untilNow && this._type == '" + Transaction.TYPE_DIRECT_DEBIT + "' && this._debitAccount._valid == false && this._state == '" + Transaction.STATE_OPEN + "'";
        return findTransactionList(where, untilNow);
    }

    @Override
    public Collection<Transaction> getOpenTransactions(
            String transactionType, BankAccount debit, BankAccount credit, Date dueDate) {
        OqlTerm term = OqlTerm.newTerm();
        final Date date = new Date(dueDate.getTime());
        term.and(term.equals("_type", transactionType),
                term.equals("_debitAccount", debit),
                term.equals("_creditAccount", credit),
                term.equals("_dueDate", date),
                term.equals("_state", Transaction.STATE_OPEN));
        final String oqlTerm = term.toString();
        LOGGER.debug("OQL: " + oqlTerm);
        Collection<Transaction> transactions = findTransactionList(oqlTerm, debit, credit, date);
        return transactions;
    }

    @Override
    public void deletePayoutsWithInvalidReference() {
        Query query = getPersistenceManager().newNamedQuery(Transaction.class, "deletePayoutsWithInvalidReference");
        query.execute();
    }

    @Override
    public void deleteNegativeFidorPayouts() {
        Query query = getPersistenceManager().newNamedQuery(Transaction.class, "deleteNegativeFidorPayouts");
        query.execute();
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!

}
