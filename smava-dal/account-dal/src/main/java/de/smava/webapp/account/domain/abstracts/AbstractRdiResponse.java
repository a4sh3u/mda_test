package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.RdiEntry;
import de.smava.webapp.account.domain.interfaces.RdiResponseEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The domain object that represents 'RdiResponses'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractRdiResponse
    extends KreditPrivatEntity implements RdiResponseEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRdiResponse.class);

    // Verarbeitet, kann aber inhaltliche Fehler enthalten -> hasProcessingError()
    public static final String STATE_SUCCESS = "success";
    // Verbindungsfehler
    public static final String STATE_ERROR_IO = "error_io";
    // z.b. Keine Request gefunden
    public static final String STATE_ERROR_UNKNOWN = "error_unknown";
    // Fehler im content
    public static final String STATE_ERROR_FIELDS = "error_fields";

    private static final String ERROR_MESSAGE_TYPE = "1";
    private static final String NOTICE_MESSAGE_TYPE = "2";

    protected transient Map<String, String> _entryMap;

    /**
     * Returns entry value from entry map.
     */
    public String getValue(String key) {
        String result = null;

        if (_entryMap != null) {
            result = _entryMap.get(key);
        } else {
            _entryMap = new HashMap<String, String>();
            if (getEntries() != null) {
                for (RdiEntry rdiEntry : getEntries()) {
                    if (rdiEntry.getKey().equals(key)) {
                        result = rdiEntry.getValue();
                        _entryMap.put(rdiEntry.getKey(), rdiEntry.getValue());
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Meldungstyp auf Fehler pr�fen.
     * Siehe {@link #NOTICE_MESSAGE_TYPE} und {@link #ERROR_MESSAGE_TYPE}.
     */
    public boolean hasProcessingError() {
        return ERROR_MESSAGE_TYPE.equals(getValue("status_message_type"));
    }

    public boolean hasError() {
        return hasProcessingError() || (getErrorMessage() != null);
    }

    public String getStatusMessage() {
        return getValue("status_message_text");
    }

    public Integer getStatusCode() {
        Integer result = null;
        String messageCode = getValue("status_message_code");
        if (messageCode != null && !"".equals(messageCode.trim())) {
            result = Integer.valueOf(messageCode);
        }
        return result;
    }

    public String getProcessId() {
        return getValue("status_processId");
    }

    public String getContractNumber() {
        return getValue("contractData_contractNumber");
    }

    public String getMethod() {
        return getValue("method");
    }

    public String getContractReferenceNumber() {
        return getValue("contractData_contractReferenceNumber");
    }

    public boolean isStateSuccess() {
        return STATE_SUCCESS.equals(getState());
    }

    public boolean isStateErrorIo() {
        return STATE_ERROR_IO.equals(getState());
    }

    public boolean isStateErrorUnknown() {
        return STATE_ERROR_UNKNOWN.equals(getState());
    }

    public boolean isStateErrorFields() {
        return STATE_ERROR_FIELDS.equals(getState());
    }

    public String getCancelationDate() {
        return getValue("contractData_dateOfCancellation");
    }

    public void addRdiEntry(RdiEntry rdiEntry) {
        if (getEntries() == null) {
            setEntries(new ArrayList<RdiEntry>());
        }

        getEntries().add(rdiEntry);

        // otherwise getValue does not work:
        if (_entryMap == null) {
            _entryMap = new HashMap<String, String>();
        }
        _entryMap.put(rdiEntry.getKey(), rdiEntry.getValue());
    }

    public RdiEntry getEntry(String key) {
        RdiEntry result = null;
        if (getEntries() != null) {
            for (RdiEntry entry : getEntries()) {
                if (entry.getKey().equals(key)) {
                    result = entry;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Method must only be used for testing.
     */
    public void setContractReferenceNumber(Long referenceNumber) {
        if (referenceNumber != null) {
            if (getEntries() == null) {
                RdiEntry entry = new RdiEntry();
                entry.setKey("contractData_contractReferenceNumber");
                entry.setValue(String.valueOf(referenceNumber));
                addRdiEntry(entry);
            }  else {
                RdiEntry referenceNumberEntry = getEntry("contractData_contractReferenceNumber");
                if (referenceNumberEntry == null) {
                    RdiEntry entry = new RdiEntry();
                    entry.setKey("contractData_contractReferenceNumber");
                    entry.setValue(String.valueOf(referenceNumber));
                    addRdiEntry(entry);
                } else {
                    referenceNumberEntry.setValue(String.valueOf(referenceNumber));
                    _entryMap.put("contractData_contractReferenceNumber", String.valueOf(referenceNumber));
                }
            }
        } else {

            RdiEntry removableRdiEntry = getEntry("contractData_contractReferenceNumber");
            if (removableRdiEntry != null) {
                getEntries().remove(removableRdiEntry);
            }

            if (_entryMap != null) {
                _entryMap.remove("contractData_contractReferenceNumber");
            }
        }
    }

}

