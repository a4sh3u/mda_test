//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(encashment rate)}

import de.smava.webapp.account.dao.EncashmentRateDao;
import de.smava.webapp.account.domain.EncashmentRate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'EncashmentRates'.
 *
 * @author generator
 */
@Repository(value = "encashmentRateDao")
public class JdoEncashmentRateDao extends JdoBaseDao implements EncashmentRateDao {

    private static final Logger LOGGER = Logger.getLogger(JdoEncashmentRateDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(encashment rate)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the encashment rate identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public EncashmentRate load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRate - start: id=" + id);
        }
        EncashmentRate result = getEntity(EncashmentRate.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRate - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public EncashmentRate getEncashmentRate(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	EncashmentRate entity = findUniqueEntity(EncashmentRate.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the encashment rate.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(EncashmentRate encashmentRate) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveEncashmentRate: " + encashmentRate);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(encashment rate)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(encashmentRate);
    }

    /**
     * @deprecated Use {@link #save(EncashmentRate) instead}
     */
    public Long saveEncashmentRate(EncashmentRate encashmentRate) {
        return save(encashmentRate);
    }

    /**
     * Deletes an encashment rate, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the encashment rate
     */
    public void deleteEncashmentRate(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteEncashmentRate: " + id);
        }
        deleteEntity(EncashmentRate.class, id);
    }

    /**
     * Retrieves all 'EncashmentRate' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<EncashmentRate> getEncashmentRateList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - start");
        }
        Collection<EncashmentRate> result = getEntities(EncashmentRate.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'EncashmentRate' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<EncashmentRate> getEncashmentRateList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - start: pageable=" + pageable);
        }
        Collection<EncashmentRate> result = getEntities(EncashmentRate.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'EncashmentRate' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<EncashmentRate> getEncashmentRateList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - start: sortable=" + sortable);
        }
        Collection<EncashmentRate> result = getEntities(EncashmentRate.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'EncashmentRate' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<EncashmentRate> getEncashmentRateList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<EncashmentRate> result = getEntities(EncashmentRate.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'EncashmentRate' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EncashmentRate> findEncashmentRateList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - start: whereClause=" + whereClause);
        }
        Collection<EncashmentRate> result = findEntities(EncashmentRate.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'EncashmentRate' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<EncashmentRate> findEncashmentRateList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<EncashmentRate> result = findEntities(EncashmentRate.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'EncashmentRate' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EncashmentRate> findEncashmentRateList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<EncashmentRate> result = findEntities(EncashmentRate.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'EncashmentRate' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EncashmentRate> findEncashmentRateList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<EncashmentRate> result = findEntities(EncashmentRate.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'EncashmentRate' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EncashmentRate> findEncashmentRateList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<EncashmentRate> result = findEntities(EncashmentRate.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'EncashmentRate' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EncashmentRate> findEncashmentRateList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<EncashmentRate> result = findEntities(EncashmentRate.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEncashmentRateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'EncashmentRate' instances.
     */
    public long getEncashmentRateCount() {
        long result = getEntityCount(EncashmentRate.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'EncashmentRate' instances which match the given whereClause.
     */
    public long getEncashmentRateCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(EncashmentRate.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'EncashmentRate' instances which match the given whereClause together with params specified in object array.
     */
    public long getEncashmentRateCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(EncashmentRate.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEncashmentRateCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(encashment rate)}
    //
    // insert custom methods here
    //
    
    public EncashmentRate getEncashmentRateByMarketName(String marketName) {
    	EncashmentRate result = null;
    	if (StringUtils.isNotEmpty(marketName)) {
    		result = findUniqueEntity(EncashmentRate.class, "_marketName == '" + marketName + "'");
    	}
		return result;
	}

    // !!!!!!!! End of insert code section !!!!!!!!
}
