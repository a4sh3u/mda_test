package de.smava.webapp.account.dao;

import de.smava.webapp.account.domain.history.HistoryEntryDetail;
import de.smava.webapp.commons.dao.BaseDao;

import java.io.Serializable;

/**
 * DAO interface for the domain object 'HistoryEntrys'.
 *
 * @author generator
 */
public interface HistoryEntryDetailDao extends BaseDao<HistoryEntryDetail>, Serializable {

}