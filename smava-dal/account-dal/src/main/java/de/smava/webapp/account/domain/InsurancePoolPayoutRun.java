package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.InsurancePoolPayoutRunHistory;

import java.util.Date;
import java.util.List;

/**
 * The domain object that represents 'InsurancePoolPayoutRuns'.
 */
public class InsurancePoolPayoutRun extends InsurancePoolPayoutRunHistory  {

        protected Date _creationDate;
        protected List<Booking> _bookings;
        protected BookingGroup _bookingGroup;
        protected Account _account;
        protected Double _paymentRate;

                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'bookings'.
     */
    public void setBookings(List<Booking> bookings) {
        _bookings = bookings;
    }
            
    /**
     * Returns the property 'bookings'.
     */
    public List<Booking> getBookings() {
        return _bookings;
    }
                                            
    /**
     * Setter for the property 'booking group'.
     */
    public void setBookingGroup(BookingGroup bookingGroup) {
        _bookingGroup = bookingGroup;
    }
            
    /**
     * Returns the property 'booking group'.
     */
    public BookingGroup getBookingGroup() {
        return _bookingGroup;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'payment rate'.
     */
    public void setPaymentRate(Double paymentRate) {
        _paymentRate = paymentRate;
    }
            
    /**
     * Returns the property 'payment rate'.
     */
    public Double getPaymentRate() {
        return _paymentRate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(InsurancePoolPayoutRun.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(InsurancePoolPayoutRun.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
