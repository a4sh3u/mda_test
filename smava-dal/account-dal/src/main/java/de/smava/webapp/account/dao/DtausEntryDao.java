//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dtaus entry)}

import de.smava.webapp.account.domain.DtausEntry;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'DtausEntrys'.
 *
 * @author generator
 */
public interface DtausEntryDao extends BaseDao<DtausEntry>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the dtaus entry identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    DtausEntry getDtausEntry(Long id);

    /**
     * Saves the dtaus entry.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveDtausEntry(DtausEntry dtausEntry);

    /**
     * Deletes an dtaus entry, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the dtaus entry
     */
    void deleteDtausEntry(Long id);

    /**
     * Retrieves all 'DtausEntry' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<DtausEntry> getDtausEntryList();

    /**
     * Retrieves a page of 'DtausEntry' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<DtausEntry> getDtausEntryList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'DtausEntry' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<DtausEntry> getDtausEntryList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'DtausEntry' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<DtausEntry> getDtausEntryList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'DtausEntry' instances.
     */
    long getDtausEntryCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(dtaus entry)}
    //
    
    Map<Long, Long> getAccountIdsForDtausEntries(final Collection<DtausEntry> dtausEntries, final Set<String> bankAccountTypes);
    
    Collection<Long> getDtausEntryIds(Long dtausFileId);
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
