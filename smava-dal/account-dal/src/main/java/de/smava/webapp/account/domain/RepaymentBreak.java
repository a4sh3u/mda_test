package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.RepaymentBreakHistory;

import java.util.Date;
import java.util.Set;

/**
 * The domain object that represents 'RepaymentBreaks'.
 */
public class RepaymentBreak extends RepaymentBreakHistory  {

        protected Date _creationDate;
        protected Account _account;
        protected de.smava.webapp.account.domain.Document _offerDocument;
        protected de.smava.webapp.account.domain.Document _formDocument;
        protected Set<RepaymentBreakDetail> _details;
        protected Set<Approval> _approvals;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'offer document'.
     */
    public void setOfferDocument(de.smava.webapp.account.domain.Document offerDocument) {
        _offerDocument = offerDocument;
    }
            
    /**
     * Returns the property 'offer document'.
     */
    public de.smava.webapp.account.domain.Document getOfferDocument() {
        return _offerDocument;
    }
                                            
    /**
     * Setter for the property 'form document'.
     */
    public void setFormDocument(de.smava.webapp.account.domain.Document formDocument) {
        _formDocument = formDocument;
    }
            
    /**
     * Returns the property 'form document'.
     */
    public de.smava.webapp.account.domain.Document getFormDocument() {
        return _formDocument;
    }
                                            
    /**
     * Setter for the property 'details'.
     */
    public void setDetails(Set<RepaymentBreakDetail> details) {
        _details = details;
    }
            
    /**
     * Returns the property 'details'.
     */
    public Set<RepaymentBreakDetail> getDetails() {
        return _details;
    }
                                            
    /**
     * Setter for the property 'approvals'.
     */
    public void setApprovals(Set<Approval> approvals) {
        _approvals = approvals;
    }
            
    /**
     * Returns the property 'approvals'.
     */
    public Set<Approval> getApprovals() {
        return _approvals;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RepaymentBreak.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(RepaymentBreak.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
