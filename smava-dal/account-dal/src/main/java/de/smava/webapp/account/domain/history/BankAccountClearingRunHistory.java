package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBankAccountClearingRun;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BankAccountClearingRuns'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BankAccountClearingRunHistory extends AbstractBankAccountClearingRun {

    protected transient Date _deliveryDateInitVal;
    protected transient boolean _deliveryDateIsSet;


				
    /**
     * Returns the initial value of the property 'delivery date'.
     */
    public Date deliveryDateInitVal() {
        Date result;
        if (_deliveryDateIsSet) {
            result = _deliveryDateInitVal;
        } else {
            result = getDeliveryDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'delivery date'.
     */
    public boolean deliveryDateIsDirty() {
        return !valuesAreEqual(deliveryDateInitVal(), getDeliveryDate());
    }

    /**
     * Returns true if the setter method was called for the property 'delivery date'.
     */
    public boolean deliveryDateIsSet() {
        return _deliveryDateIsSet;
    }

}
