//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head11/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head11/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document attachment container)}

import de.smava.webapp.account.dao.DocumentAttachmentContainerDao;
import de.smava.webapp.account.domain.Attachment;
import de.smava.webapp.account.domain.DocumentAttachmentContainer;
import de.smava.webapp.account.domain.DocumentContainer;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'DocumentAttachmentContainers'.
 *
 * @author generator
 */
@Repository(value = "documentAttachmentContainerDao")
public class JdoDocumentAttachmentContainerDao extends JdoBaseDao implements DocumentAttachmentContainerDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDocumentAttachmentContainerDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(document attachment container)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the document attachment container identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public DocumentAttachmentContainer load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainer - start: id=" + id);
        }
        DocumentAttachmentContainer result = getEntity(DocumentAttachmentContainer.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainer - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public DocumentAttachmentContainer getDocumentAttachmentContainer(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	DocumentAttachmentContainer entity = findUniqueEntity(DocumentAttachmentContainer.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the document attachment container.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(DocumentAttachmentContainer documentAttachmentContainer) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDocumentAttachmentContainer: " + documentAttachmentContainer);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(document attachment container)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(documentAttachmentContainer);
    }

    /**
     * @deprecated Use {@link #save(DocumentAttachmentContainer) instead}
     */
    public Long saveDocumentAttachmentContainer(DocumentAttachmentContainer documentAttachmentContainer) {
        return save(documentAttachmentContainer);
    }

    /**
     * Deletes an document attachment container, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document attachment container
     */
    public void deleteDocumentAttachmentContainer(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDocumentAttachmentContainer: " + id);
        }
        deleteEntity(DocumentAttachmentContainer.class, id);
    }

    /**
     * Retrieves all 'DocumentAttachmentContainer' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - start");
        }
        Collection<DocumentAttachmentContainer> result = getEntities(DocumentAttachmentContainer.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'DocumentAttachmentContainer' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - start: pageable=" + pageable);
        }
        Collection<DocumentAttachmentContainer> result = getEntities(DocumentAttachmentContainer.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentAttachmentContainer' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - start: sortable=" + sortable);
        }
        Collection<DocumentAttachmentContainer> result = getEntities(DocumentAttachmentContainer.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentAttachmentContainer' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentAttachmentContainer> result = getEntities(DocumentAttachmentContainer.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentAttachmentContainer' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAttachmentContainer> findDocumentAttachmentContainerList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - start: whereClause=" + whereClause);
        }
        Collection<DocumentAttachmentContainer> result = findEntities(DocumentAttachmentContainer.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentAttachmentContainer' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<DocumentAttachmentContainer> findDocumentAttachmentContainerList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<DocumentAttachmentContainer> result = findEntities(DocumentAttachmentContainer.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'DocumentAttachmentContainer' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAttachmentContainer> findDocumentAttachmentContainerList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<DocumentAttachmentContainer> result = findEntities(DocumentAttachmentContainer.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentAttachmentContainer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAttachmentContainer> findDocumentAttachmentContainerList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<DocumentAttachmentContainer> result = findEntities(DocumentAttachmentContainer.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentAttachmentContainer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAttachmentContainer> findDocumentAttachmentContainerList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentAttachmentContainer> result = findEntities(DocumentAttachmentContainer.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentAttachmentContainer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAttachmentContainer> findDocumentAttachmentContainerList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentAttachmentContainer> result = findEntities(DocumentAttachmentContainer.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAttachmentContainerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentAttachmentContainer' instances.
     */
    public long getDocumentAttachmentContainerCount() {
        long result = getEntityCount(DocumentAttachmentContainer.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentAttachmentContainer' instances which match the given whereClause.
     */
    public long getDocumentAttachmentContainerCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(DocumentAttachmentContainer.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentAttachmentContainer' instances which match the given whereClause together with params specified in object array.
     */
    public long getDocumentAttachmentContainerCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(DocumentAttachmentContainer.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAttachmentContainerCount() - result: count=" + result);
        }
        return result;
    }



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document attachment container)}
    //

    @Override
    public Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerByName(String name) {
        return findByNamedQuery(DocumentAttachmentContainer.class, "findDocumentAttachmentContainerByName", Collections.singletonMap("name", name));
    }

    @Override
    public Collection<DocumentAttachmentContainer> getOpenOutgoingDocumentAttachmentContainer() {
        return findByNamedQuery(DocumentAttachmentContainer.class, "findOpenOutgoingDocumentAttachmentContainer", null);
    }

	@Override
	public Map<Attachment, DocumentContainer> getAttachmentToDocumentContainerMap(final DocumentAttachmentContainer dac) {
		final Query q = getPersistenceManager().newNamedQuery(DocumentAttachmentContainer.class, "getAttachmentToDocumentContainerMap");
		q.setResultClass(Object[].class);
		final Map<Attachment, DocumentContainer> result;
		final List<Object[]> rawResult = (List<Object[]>) q.execute(dac.getId());
		if (CollectionUtils.isNotEmpty(rawResult)) {
			final Set<Long> attachmentIds = new HashSet<Long>(rawResult.size());
			final Set<Long> dcIds = new HashSet<Long>();
			final Map<Long, Long> idMap = new HashMap<Long, Long>(rawResult.size());
			result = new HashMap<Attachment, DocumentContainer>(rawResult.size());
			for (Object[] row : rawResult) {
				final Long attId = (Long) row[0];
				attachmentIds.add(attId);
				final Long dcId = (Long) row[1];
				if (dcId != null) {
					dcIds.add(dcId);
				}
				idMap.put(attId, dcId);
			}
			final Collection<Attachment> attachments = findByIds(Attachment.class, attachmentIds);
			final Collection<DocumentContainer> containers = findByIds(DocumentContainer.class, dcIds);
			final Map<Long, DocumentContainer> dcIdObjectMap = new HashMap<Long, DocumentContainer>(containers.size());
			for (DocumentContainer documentContainer : containers) {
				dcIdObjectMap.put(documentContainer.getId(), documentContainer);
			}
			for (Attachment attachment : attachments) {
				result.put(attachment, dcIdObjectMap.get(idMap.get(attachment.getId())));
			}
		} else {
			result = Collections.emptyMap();
		}
		return result;
	}
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
