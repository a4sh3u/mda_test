package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.interfaces.OrderRecommendationEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The domain object that represents 'OrderRecommendations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractOrderRecommendation
    extends KreditPrivatEntity implements OrderRecommendationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractOrderRecommendation.class);

    public static final String STATE_OPEN = "open";
    public static final String STATE_DELETED = "deleted";

    public List<? extends Entity> getModifier() {
        List<Account> modifiers = new LinkedList<Account>();
        modifiers.add(getAuthor());
        return modifiers;
    }

}

