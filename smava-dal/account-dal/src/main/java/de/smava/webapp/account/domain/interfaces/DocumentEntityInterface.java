package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The domain object that represents 'Documents'.
 *
 * @author generator
 */
public interface DocumentEntityInterface {

    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'approval state'.
     *
     * 
     *
     */
    void setApprovalState(String approvalState);

    /**
     * Returns the property 'approval state'.
     *
     * 
     *
     */
    String getApprovalState();
    /**
     * Setter for the property 'subject'.
     *
     * 
     *
     */
    void setSubject(String subject);

    /**
     * Returns the property 'subject'.
     *
     * 
     *
     */
    String getSubject();
    /**
     * Setter for the property 'text'.
     *
     * 
     *
     */
    void setText(String text);

    /**
     * Returns the property 'text'.
     *
     * 
     *
     */
    String getText();
    /**
     * Setter for the property 'notes'.
     *
     * 
     *
     */
    void setNotes(String notes);

    /**
     * Returns the property 'notes'.
     *
     * 
     *
     */
    String getNotes();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'date'.
     *
     * 
     *
     */
    void setDate(Date date);

    /**
     * Returns the property 'date'.
     *
     * 
     *
     */
    Date getDate();
    /**
     * Setter for the property 'direction'.
     *
     * 
     *
     */
    void setDirection(String direction);

    /**
     * Returns the property 'direction'.
     *
     * 
     *
     */
    String getDirection();
    /**
     * Setter for the property 'last modifier'.
     *
     * 
     *
     */
    void setLastModifier(Account lastModifier);

    /**
     * Returns the property 'last modifier'.
     *
     * 
     *
     */
    Account getLastModifier();
    /**
     * Setter for the property 'owner'.
     *
     * 
     *
     */
    void setOwner(Account owner);

    /**
     * Returns the property 'owner'.
     *
     * 
     *
     */
    Account getOwner();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'type of dispatch'.
     *
     * 
     *
     */
    void setTypeOfDispatch(String typeOfDispatch);

    /**
     * Returns the property 'type of dispatch'.
     *
     * 
     *
     */
    String getTypeOfDispatch();
    /**
     * Setter for the property 'document address assignments'.
     *
     * 
     *
     */
    void setDocumentAddressAssignments(List<DocumentAddressAssignment> documentAddressAssignments);

    /**
     * Returns the property 'document address assignments'.
     *
     * 
     *
     */
    List<DocumentAddressAssignment> getDocumentAddressAssignments();
    /**
     * Setter for the property 'email'.
     *
     * 
     *
     */
    void setEmail(String email);

    /**
     * Returns the property 'email'.
     *
     * 
     *
     */
    String getEmail();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'phone'.
     *
     * 
     *
     */
    void setPhone(String phone);

    /**
     * Returns the property 'phone'.
     *
     * 
     *
     */
    String getPhone();
    /**
     * Setter for the property 'fax'.
     *
     * 
     *
     */
    void setFax(String fax);

    /**
     * Returns the property 'fax'.
     *
     * 
     *
     */
    String getFax();
    /**
     * Setter for the property 'email from'.
     *
     * 
     *
     */
    void setEmailFrom(String emailFrom);

    /**
     * Returns the property 'email from'.
     *
     * 
     *
     */
    String getEmailFrom();
    /**
     * Setter for the property 'name from'.
     *
     * 
     *
     */
    void setNameFrom(String nameFrom);

    /**
     * Returns the property 'name from'.
     *
     * 
     *
     */
    String getNameFrom();
    /**
     * Setter for the property 'phone from'.
     *
     * 
     *
     */
    void setPhoneFrom(String phoneFrom);

    /**
     * Returns the property 'phone from'.
     *
     * 
     *
     */
    String getPhoneFrom();
    /**
     * Setter for the property 'fax from'.
     *
     * 
     *
     */
    void setFaxFrom(String faxFrom);

    /**
     * Returns the property 'fax from'.
     *
     * 
     *
     */
    String getFaxFrom();
    /**
     * Setter for the property 'document containers'.
     *
     * 
     *
     */
    void setDocumentContainers(List<DocumentContainer> documentContainers);

    /**
     * Returns the property 'document containers'.
     *
     * 
     *
     */
    List<DocumentContainer> getDocumentContainers();
    /**
     * Setter for the property 'attachments'.
     *
     * 
     *
     */
    void setAttachments(Set<Attachment> attachments);

    /**
     * Returns the property 'attachments'.
     *
     * 
     *
     */
    Set<Attachment> getAttachments();
    /**
     * Setter for the property 'contracts'.
     *
     * 
     *
     */
    void setContracts(List<Contract> contracts);

    /**
     * Returns the property 'contracts'.
     *
     * 
     *
     */
    List<Contract> getContracts();
    /**
     * Setter for the property 'consolidated debt'.
     *
     * 
     *
     */
    void setConsolidatedDebt(ConsolidatedDebt consolidatedDebt);

    /**
     * Returns the property 'consolidated debt'.
     *
     * 
     *
     */
    ConsolidatedDebt getConsolidatedDebt();
    /**
     * Setter for the property 'service provider restriction'.
     *
     * 
     *
     */
    void setServiceProviderRestriction(de.smava.webapp.account.domain.BankAccountProvider serviceProviderRestriction);

    /**
     * Returns the property 'service provider restriction'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccountProvider getServiceProviderRestriction();

}
