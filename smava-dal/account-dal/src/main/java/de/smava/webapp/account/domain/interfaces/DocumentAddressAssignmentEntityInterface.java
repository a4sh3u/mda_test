package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Address;
import de.smava.webapp.account.domain.Document;


/**
 * The domain object that represents 'DocumentAddressAssignments'.
 *
 * @author generator
 */
public interface DocumentAddressAssignmentEntityInterface {

    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    void setDocument(Document document);

    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    Document getDocument();
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    Address getAddress();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();

}
