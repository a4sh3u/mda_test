package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractMessage;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Messages'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MessageHistory extends AbstractMessage {

    protected transient String _subjectInitVal;
    protected transient boolean _subjectIsSet;
    protected transient String _textInitVal;
    protected transient boolean _textIsSet;
    protected transient String _folderInitVal;
    protected transient boolean _folderIsSet;
    protected transient String _recipientEmailInitVal;
    protected transient boolean _recipientEmailIsSet;
    protected transient boolean _readInitVal;
    protected transient boolean _readIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


	
    /**
     * Returns the initial value of the property 'subject'.
     */
    public String subjectInitVal() {
        String result;
        if (_subjectIsSet) {
            result = _subjectInitVal;
        } else {
            result = getSubject();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'subject'.
     */
    public boolean subjectIsDirty() {
        return !valuesAreEqual(subjectInitVal(), getSubject());
    }

    /**
     * Returns true if the setter method was called for the property 'subject'.
     */
    public boolean subjectIsSet() {
        return _subjectIsSet;
    }
	
    /**
     * Returns the initial value of the property 'text'.
     */
    public String textInitVal() {
        String result;
        if (_textIsSet) {
            result = _textInitVal;
        } else {
            result = getText();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'text'.
     */
    public boolean textIsDirty() {
        return !valuesAreEqual(textInitVal(), getText());
    }

    /**
     * Returns true if the setter method was called for the property 'text'.
     */
    public boolean textIsSet() {
        return _textIsSet;
    }
	
    /**
     * Returns the initial value of the property 'folder'.
     */
    public String folderInitVal() {
        String result;
        if (_folderIsSet) {
            result = _folderInitVal;
        } else {
            result = getFolder();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'folder'.
     */
    public boolean folderIsDirty() {
        return !valuesAreEqual(folderInitVal(), getFolder());
    }

    /**
     * Returns true if the setter method was called for the property 'folder'.
     */
    public boolean folderIsSet() {
        return _folderIsSet;
    }
	
    /**
     * Returns the initial value of the property 'recipient email'.
     */
    public String recipientEmailInitVal() {
        String result;
        if (_recipientEmailIsSet) {
            result = _recipientEmailInitVal;
        } else {
            result = getRecipientEmail();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'recipient email'.
     */
    public boolean recipientEmailIsDirty() {
        return !valuesAreEqual(recipientEmailInitVal(), getRecipientEmail());
    }

    /**
     * Returns true if the setter method was called for the property 'recipient email'.
     */
    public boolean recipientEmailIsSet() {
        return _recipientEmailIsSet;
    }
	
    /**
     * Returns the initial value of the property 'read'.
     */
    public boolean readInitVal() {
        boolean result;
        if (_readIsSet) {
            result = _readInitVal;
        } else {
            result = getRead();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'read'.
     */
    public boolean readIsDirty() {
        return !valuesAreEqual(readInitVal(), getRead());
    }

    /**
     * Returns true if the setter method was called for the property 'read'.
     */
    public boolean readIsSet() {
        return _readIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
			
}
