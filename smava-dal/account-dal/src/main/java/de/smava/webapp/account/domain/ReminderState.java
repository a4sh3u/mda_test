package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.ReminderStateHistory;

import java.util.Date;

/**
 * The domain object that represents 'ReminderStates'.
 */
public class ReminderState extends ReminderStateHistory  {

        protected Transaction _transaction;
        protected Date _creationDate;
        protected Date _calculationDate;
        protected int _level;

    /**
     * Setter for the property 'transaction'.
     */
    public void setTransaction(Transaction transaction) {
        _transaction = transaction;
    }
            
    /**
     * Returns the property 'transaction'.
     */
    public Transaction getTransaction() {
        return _transaction;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'calculation date'.
     */
    public void setCalculationDate(Date calculationDate) {
        if (!_calculationDateIsSet) {
            _calculationDateIsSet = true;
            _calculationDateInitVal = getCalculationDate();
        }
        registerChange("calculation date", _calculationDateInitVal, calculationDate);
        _calculationDate = calculationDate;
    }
                        
    /**
     * Returns the property 'calculation date'.
     */
    public Date getCalculationDate() {
        return _calculationDate;
    }
                                    /**
     * Setter for the property 'level'.
     */
    public void setLevel(int level) {
        if (!_levelIsSet) {
            _levelIsSet = true;
            _levelInitVal = getLevel();
        }
        registerChange("level", _levelInitVal, level);
        _level = level;
    }
                        
    /**
     * Returns the property 'level'.
     */
    public int getLevel() {
        return _level;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ReminderState.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _level=").append(_level);
            builder.append("\n}");
        } else {
            builder.append(ReminderState.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
