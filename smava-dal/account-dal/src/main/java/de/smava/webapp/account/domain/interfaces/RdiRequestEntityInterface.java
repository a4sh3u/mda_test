package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.RdiContract;
import de.smava.webapp.account.domain.RdiEntry;
import de.smava.webapp.account.domain.RdiResponse;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'RdiRequests'.
 *
 * @author generator
 */
public interface RdiRequestEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'contract'.
     *
     * 
     *
     */
    void setContract(RdiContract contract);

    /**
     * Returns the property 'contract'.
     *
     * 
     *
     */
    RdiContract getContract();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'error message'.
     *
     * 
     *
     */
    void setErrorMessage(String errorMessage);

    /**
     * Returns the property 'error message'.
     *
     * 
     *
     */
    String getErrorMessage();
    /**
     * Setter for the property 'entries'.
     *
     * 
     *
     */
    void setEntries(Collection<RdiEntry> entries);

    /**
     * Returns the property 'entries'.
     *
     * 
     *
     */
    Collection<RdiEntry> getEntries();
    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    void setResponse(RdiResponse response);

    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    RdiResponse getResponse();

}
