package de.smava.webapp.account.domain.history;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;

/**
 * Base class for HistoryEntry implementations.
 *
 * @author michael.schwalbe (21.10.2005)
 */
public class HistoryEntry extends KreditPrivatEntity {
    /* BEGIN STATIC BLOCK */
    public static final String ACTION_TYPE_CREATED     = "CREATED";
    public static final String ACTION_TYPE_MODIFIED    = "MODIFIED";
    public static final String ACTION_TYPE_DELETED     = "DELETED";

    private static final long serialVersionUID = -7589727421034546108L;
    /* END STATIC BLOCK */

    private Long _accountId;
    private Long _principalId;
    private Long _entityId;
    private Date _actionDate;
    private String _actionType;
    private String _historyEntryDetailClass;
    private String _description;
//    private HistoryEntryDetail _historyEntryDetail;
    
    private List<HistoryEntryDetail> _details = null;
    private String _historyEntryDetailXml;

    /**
     * Get the id of the account which is connected with this action.
     */
    public Long getAccountId() {
        return _accountId;
    }

    public void setAccountId(Long accountId) {
        _accountId = accountId;
    }

    /**
     * The id of the user who performed the change.
     */
    public Long getPrincipalId() {
        return _principalId;
    }

    public void setPrincipalId(Long principalId) {
        _principalId = principalId;
    }

    /**
     * The id of the entity which was created, changed ...
     */
    public Long getEntityId() {
        return _entityId;
    }

    public void setEntityId(Long entityId) {
        _entityId = entityId;
    }

    /**
     * Get the date when the action was executed by the user.
     *
     * @return Date the date when the action was executed
     */
    public Date getActionDate() {
        return _actionDate;
    }

    public void setActionDate(Date actionDate) {
        _actionDate = actionDate;
    }

    public String getActionType() {
        return _actionType;
    }

    public void setActionType(String actionType) {
        _actionType = actionType;
    }

    public String getHistoryEntryDetailClass() {
        return _historyEntryDetailClass;
    }

    public void setHistoryEntryDetailClass(String historyEntryDetailClass) {
        _historyEntryDetailClass = historyEntryDetailClass;
    }

    public void setHistoryEntryDetailClass(Class<? extends HistoryEntryDetail> historyEntryDetailClass) {
        _historyEntryDetailClass = historyEntryDetailClass.getName();
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    /**
     * Get the user action for this log entry.
     *
     * @return UserAction the user action for this log entry
     */
//    public HistoryEntryDetail getHistoryEntryDetail() {
//        HistoryEntryDetail result = null;
//        if (_historyEntryDetail == null) {
//            if (_historyEntryDetailXml != null) {
//                result = HistoryEntryDetailMarshaller.unmarshalHistoryEntryDetail(_historyEntryDetailClass, _historyEntryDetailXml);
//            }
//        } else {
//            result = _historyEntryDetail;
//        }
//        return result;
//    }
//
//    public void setHistoryEntryDetail(HistoryEntryDetail detail) {
//        _historyEntryDetail = detail;
//        _historyEntryDetailXml = HistoryEntryDetailMarshaller.marshalHistoryEntryDetail(_historyEntryDetail);
//    }

    
    
    public String getHistoryEntryDetailXml() {
        return _historyEntryDetailXml;
    }

    public List<Account> getModifier() {
        return new LinkedList<Account>();
    }

    public List<HistoryEntryDetail> getDetails() {
        return _details;
    }

    public void setDetails(List<HistoryEntryDetail> details) {
        this._details = details;
    }
}