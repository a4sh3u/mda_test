package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBookingGroup;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BookingGroups'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BookingGroupHistory extends AbstractBookingGroup {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient Date _dateInitVal;
    protected transient boolean _dateIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _marketInitVal;
    protected transient boolean _marketIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _specifierInitVal;
    protected transient boolean _specifierIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'date'.
     */
    public Date dateInitVal() {
        Date result;
        if (_dateIsSet) {
            result = _dateInitVal;
        } else {
            result = getDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date'.
     */
    public boolean dateIsDirty() {
        return !valuesAreEqual(dateInitVal(), getDate());
    }

    /**
     * Returns true if the setter method was called for the property 'date'.
     */
    public boolean dateIsSet() {
        return _dateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'market'.
     */
    public String marketInitVal() {
        String result;
        if (_marketIsSet) {
            result = _marketInitVal;
        } else {
            result = getMarket();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'market'.
     */
    public boolean marketIsDirty() {
        return !valuesAreEqual(marketInitVal(), getMarket());
    }

    /**
     * Returns true if the setter method was called for the property 'market'.
     */
    public boolean marketIsSet() {
        return _marketIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'specifier'.
     */
    public String specifierInitVal() {
        String result;
        if (_specifierIsSet) {
            result = _specifierInitVal;
        } else {
            result = getSpecifier();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'specifier'.
     */
    public boolean specifierIsDirty() {
        return !valuesAreEqual(specifierInitVal(), getSpecifier());
    }

    /**
     * Returns true if the setter method was called for the property 'specifier'.
     */
    public boolean specifierIsSet() {
        return _specifierIsSet;
    }

}
