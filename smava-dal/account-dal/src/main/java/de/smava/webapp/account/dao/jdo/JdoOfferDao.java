//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(offer)}

import com.aperto.webkit.utils.StringTools;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.todo.offer.dao.OfferDao;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Offers'.
 *
 * @author generator
 */
@Repository(value = "offerDao")
public class JdoOfferDao extends JdoBaseDao implements OfferDao {

    private static final Logger LOGGER = Logger.getLogger(JdoOfferDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(offer)}
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the offer identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Offer load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOffer - start: id=" + id);
        }
        Offer result = getEntity(Offer.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOffer - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Offer getOffer(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Offer entity = findUniqueEntity(Offer.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the offer.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Offer offer) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveOffer: " + offer);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(offer)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(offer);
    }

    /**
     * @deprecated Use {@link #save(Offer) instead}
     */
    public Long saveOffer(Offer offer) {
        return save(offer);
    }

    /**
     * Deletes an offer, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the offer
     */
    public void deleteOffer(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteOffer: " + id);
        }
        deleteEntity(Offer.class, id);
    }

    /**
     * Retrieves all 'Offer' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Offer> getOfferList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - start");
        }
        Collection<Offer> result = getEntities(Offer.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Offer' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Offer> getOfferList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - start: pageable=" + pageable);
        }
        Collection<Offer> result = getEntities(Offer.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Offer' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Offer> getOfferList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - start: sortable=" + sortable);
        }
        Collection<Offer> result = getEntities(Offer.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Offer' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Offer> getOfferList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Offer> result = getEntities(Offer.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Offer' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Offer> findOfferList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: whereClause=" + whereClause);
        }
        Collection<Offer> result = findEntities(Offer.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Offer' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Offer> findOfferList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Offer> result = findEntities(Offer.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Offer' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Offer> findOfferList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Offer> result = findEntities(Offer.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Offer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Offer> findOfferList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Offer> result = findEntities(Offer.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Offer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Offer> findOfferList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Offer> result = findEntities(Offer.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Offer' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Offer> findOfferList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Offer> result = findEntities(Offer.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Offer' instances.
     */
    public long getOfferCount() {
        long result = getEntityCount(Offer.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Offer' instances which match the given whereClause.
     */
    public long getOfferCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Offer.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Offer' instances which match the given whereClause together with params specified in object array.
     */
    public long getOfferCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Offer.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOfferCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(offer)}

    /** Find all offers that belong together, connected by groupId.
     */
    public Collection<Offer> findOffersOfSameGroup(Offer offer) {
        OqlTerm oqlTerm = OqlTerm.newTerm();

        Collection<Offer> result = new ArrayList<Offer>();

        long groupId = offer.getGroupId();
        if (groupId == 0L) {
            result.add(offer);
            oqlTerm.equals("_groupId", offer.getId());
        } else if (groupId == offer.getId()) {
            oqlTerm.equals("_groupId", offer.getId());
        } else {
            oqlTerm.or(oqlTerm.equals("_groupId", offer.getGroupId()), oqlTerm.equals("_id", offer.getGroupId()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOffersOfSameGroup() - start: " + oqlTerm.toString());
        }
        result.addAll(findEntities(Offer.class, oqlTerm.toString()));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOffersOfSameGroup() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<Offer> findOfferList(Account lender) {
        return findOfferList("_account._id == " + lender.getId());
    }

    public Collection<Offer> findOfferList(Account lender, BankAccountProvider bankAccountProvider, Collection<String> states) {
        Collection<Offer> filteredResult = new LinkedList<Offer>();
        for (Offer offer : findOfferList(lender, states)) {
            Contract contract = offer.getContract();

            if (contract != null) {
                if (bankAccountProvider.equals(contract.getOrder().getBankAccountProvider())) {
                    filteredResult.add( offer );
                }
            }
            else {
                LOGGER.warn( "findOfferList(): Offer with id " + offer.getId() + " has no contract" );
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + filteredResult.size());
        }
        return filteredResult;
    }

    public Collection<Offer> findOfferListWithoutBankAccountProvider(Account lender, Collection<String> states) {
        Collection<Offer> result = findOfferList(lender, states);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }

        return result;
    }

    private Collection<Offer> findOfferList(Account lender, Collection<String> states) {
        if (!lender.isLenderAnyState()) {
            throw new IllegalArgumentException("given account (" + lender + ") is not a lender but " + lender.getRoles());
        }

        OqlTerm oqlTerm = OqlTerm.newTerm();
        OqlTerm statesTerm = getStatesTerm(states);
        if (states.size() >= 1) {
            oqlTerm.and(oqlTerm.equals("_account", lender), statesTerm);
        } else {
            oqlTerm.equals("_account", lender);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: lender=" + lender.getUsername());
        }
        return findEntities(Offer.class, oqlTerm.toString(), lender);
    }

    /**
     * Gets the states term for finding offers and orders.
     */
    private OqlTerm getStatesTerm(Collection<String> states) {
        OqlTerm statesTerm = OqlTerm.newTerm();
        OqlTerm [] orStates = new OqlTerm[states.size()];
        if (states.size() > 1) {
            int i = 0;
            for (String state : states) {
                orStates[i++] = statesTerm.equals("_state", state);
            }
            statesTerm = statesTerm.or(orStates);
        } else if (states.size() == 1) {
            statesTerm = statesTerm.equals("_state", states.iterator().next());
        }
        return statesTerm;
    }

    public Collection<Offer> findOfferList(String market, Collection<String> states) {
        String stateString = buildJdoStateString(states);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: market=" + market);
        }
        Collection<Offer> result = findEntities(Offer.class, "_bidInterests.contains(bidInterest) && bidInterest._marketName == " + StringTools.asJavaString(market) + " && bidInterest._validUntil == null && (" + stateString + ")");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    public Collection<Offer> findOfferList(String market, Collection<String> states, Pageable pageable, Sortable sortable) {
        String stateString = buildJdoStateString(states);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - start: market=" + market);
        }
        Collection<Offer> result = findEntities(Offer.class, "_bidInterests.contains(bidInterest) && bidInterest._marketName == " + StringTools.asJavaString(market) + " && bidInterest._validUntil == null && (" + stateString + ")", pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOfferList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Builds a string based on a passed array of possible states.
     */
    private String buildJdoStateString(Collection<String> states) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String state : states) {
            if (first) {
                sb.append("_state==").append("'").append(state).append("'");
                first = false;
            } else {
                sb.append(" || _state==").append("'").append(state).append("'");
            }
        }
        return sb.toString();
    }
    public Collection<Offer> findOfferList(Account account, Pageable pageable, Sortable sortable) {
        Collection<Offer> list;
        if (account == null) {
            list = getOfferList(pageable, sortable);
        } else {
            OqlTerm oql = OqlTerm.newTerm();
            if (account != null) {
                oql.equals("._account", account);
            } else {
                throw new IllegalArgumentException("Account not lender.");
            }
            if (pageable != null && sortable != null) {
                list = findEntities(Offer.class, oql.toString(), pageable, sortable, new Object[]{account});
            } else {
                list = findEntities(Offer.class, oql.toString(), new Object[]{account});
            }
        }
        return list;
    }

	public Collection<Offer> findOfferByBidSearch(BidSearch bidSearch) {
		Query q = getPersistenceManager().newNamedQuery(Offer.class, "findOffersByBidSearch");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("bidSearch", bidSearch);
		@SuppressWarnings("unchecked")
		Collection<Offer> list = (Collection<Offer>) q.executeWithMap(params);
		return list;
	}

    public Long getOfferCountForPlacement(Long marketingPlacementId) {
        return (Long) getAggregateByNamedQuery(Offer.class,
                                               "getOfferCountForPlacment",
                                               Collections.singletonMap("marketingPlacementId", marketingPlacementId),
                                               "count(this)",
                                               null);
    }

    @Override
    public Collection<Offer> findOpenOffersForAccount(Account account) {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals("_account", account), term.notEquals("_state", Bid.STATE_DELETED), term.notEquals("_state", Bid.STATE_ARCHIVE));

        Collection<Offer> offers = findOfferList(term.toString(), account);
        return offers;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
