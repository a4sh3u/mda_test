package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.BookingAssignment;
import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.interfaces.BookingEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.util.MathUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * The domain object that represents 'Bookings'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBooking
    extends KreditPrivatEntity implements BookingEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBooking.class);

    public static final double PRECISION = MathUtils.PRECISION;

    /**
     * cuts the given value to 8 after comma digits.
     *
     * @param amount the amount
     * @return the rounded amount
     */
    public static double cutAmount(double amount) {
        return MathUtils.roundAmount(amount);
    }

    public boolean isContractCancellationBooking() {
        boolean result = false;

        if (getBookingAssignments() != null) {
            for (BookingAssignment assignment : getBookingAssignments()) {
                if (assignment.getBookingGroup() != null && BookingGroup.TYPE_CONTRACT_CANCELLATION.equals(assignment.getBookingGroup().getType())) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    public Collection<BookingGroup> getBookingGroups() {
        return getBookingGroups(false, false);
    }

    public Collection<BookingGroup> getBookingGroups(boolean target) {
        return getBookingGroups(true, target);
    }

    private Collection<BookingGroup> getBookingGroups(boolean respectTarget, boolean target) {
        List<BookingGroup> bookingGroups = new ArrayList<BookingGroup>();
        for (BookingAssignment ass : getBookingAssignments()) {
            if (!respectTarget || target == ass.getIsTarget()) {
                bookingGroups.add(ass.getBookingGroup());
            }
        }
        return bookingGroups;
    }

    public boolean hasTargetInsuranceBookingAssignments() {
        boolean result = false;
        for (BookingAssignment ass : getBookingAssignments()) {
            if (ass.getIsTarget() && BookingGroup.TYPE_INSURANCE_GROUP.equals(ass.getBookingGroup().getType())) {
                result = true;
                break;
            }
        }
        return result;
    }

    public Date getBookingGroupDate() {
        Date result = null;
        if (getBookingGroups() != null) {
            if (!getBookingGroups().isEmpty()) {
                result = getBookingGroups().iterator().next().getDate();
            }
        }
        return result;
    }

    public boolean isInInsuranceBookingGroup() {
        boolean result = false;
        Collection<BookingAssignment> bookingAssignments = getBookingAssignments();

        if (bookingAssignments != null && !bookingAssignments.isEmpty()) {
            for (BookingAssignment ba : bookingAssignments) {
                if ((ba.getBookingGroup() != null) && (ba.getBookingGroup().getType().equals(BookingGroup.TYPE_INSURANCE_GROUP))) {
                    result = true;
                }
            }
        }
        return result;
    }

    public BookingGroup getInsuranceBookingGroup() {
        BookingGroup result = null;
        Collection<BookingAssignment> bookingAssignments = getBookingAssignments();

        if (bookingAssignments != null && !bookingAssignments.isEmpty()) {
            for (BookingAssignment ba : bookingAssignments) {
                if ((ba.getBookingGroup() != null) && (ba.getBookingGroup().getType().equals(BookingGroup.TYPE_INSURANCE_GROUP))) {
                    result = ba.getBookingGroup();
                    break;
                }
            }
        }
        return result;
    }

    public BookingGroup getCancelationBookingGroup() {
        BookingGroup result = null;
        Collection<BookingAssignment> bookingAssignments = getBookingAssignments();

        if (bookingAssignments != null && !bookingAssignments.isEmpty()) {
            for (BookingAssignment ba : bookingAssignments) {
                if ((ba.getBookingGroup() != null) && (ba.getBookingGroup().getType().equals(BookingGroup.TYPE_CONTRACT_CANCELLATION))) {
                    result = ba.getBookingGroup();
                    break;
                }
            }
        }
        return result;
    }

}

