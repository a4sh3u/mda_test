package de.smava.webapp.account.domain;

/**
 * String holder for the housing type.
 * @author Dimitar Robev
 *
 */
public class TypeOfAccomodation {
	private TypeOfAccomodation() {
		super();
	}
	public static final String PARENTS_PLACE = "PARENTS_PLACE";
	public static final String OWN = "OWN";
	public static final String COLLECTIVE = "COLLECTIVE";
	public static final String RENT = "RENT";
	public static final String OTHER = "OTHER";
}
