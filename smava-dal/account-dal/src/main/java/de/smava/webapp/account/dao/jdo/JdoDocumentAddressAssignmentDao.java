//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head11/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head11/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document address assignment)}

import de.smava.webapp.account.dao.DocumentAddressAssignmentDao;
import de.smava.webapp.account.domain.DocumentAddressAssignment;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'DocumentAddressAssignments'.
 *
 * @author generator
 */
@Repository(value = "documentAddressAssignmentDao")
public class JdoDocumentAddressAssignmentDao extends JdoBaseDao implements DocumentAddressAssignmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDocumentAddressAssignmentDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(document address assignment)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the document address assignment identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public DocumentAddressAssignment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignment - start: id=" + id);
        }
        DocumentAddressAssignment result = getEntity(DocumentAddressAssignment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignment - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public DocumentAddressAssignment getDocumentAddressAssignment(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	DocumentAddressAssignment entity = findUniqueEntity(DocumentAddressAssignment.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the document address assignment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(DocumentAddressAssignment documentAddressAssignment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDocumentAddressAssignment: " + documentAddressAssignment);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(document address assignment)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(documentAddressAssignment);
    }

    /**
     * @deprecated Use {@link #save(DocumentAddressAssignment) instead}
     */
    public Long saveDocumentAddressAssignment(DocumentAddressAssignment documentAddressAssignment) {
        return save(documentAddressAssignment);
    }

    /**
     * Deletes an document address assignment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document address assignment
     */
    public void deleteDocumentAddressAssignment(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDocumentAddressAssignment: " + id);
        }
        deleteEntity(DocumentAddressAssignment.class, id);
    }

    /**
     * Retrieves all 'DocumentAddressAssignment' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<DocumentAddressAssignment> getDocumentAddressAssignmentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - start");
        }
        Collection<DocumentAddressAssignment> result = getEntities(DocumentAddressAssignment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'DocumentAddressAssignment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<DocumentAddressAssignment> getDocumentAddressAssignmentList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - start: pageable=" + pageable);
        }
        Collection<DocumentAddressAssignment> result = getEntities(DocumentAddressAssignment.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentAddressAssignment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<DocumentAddressAssignment> getDocumentAddressAssignmentList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - start: sortable=" + sortable);
        }
        Collection<DocumentAddressAssignment> result = getEntities(DocumentAddressAssignment.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentAddressAssignment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<DocumentAddressAssignment> getDocumentAddressAssignmentList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentAddressAssignment> result = getEntities(DocumentAddressAssignment.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentAddressAssignment' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAddressAssignment> findDocumentAddressAssignmentList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - start: whereClause=" + whereClause);
        }
        Collection<DocumentAddressAssignment> result = findEntities(DocumentAddressAssignment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentAddressAssignment' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<DocumentAddressAssignment> findDocumentAddressAssignmentList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<DocumentAddressAssignment> result = findEntities(DocumentAddressAssignment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'DocumentAddressAssignment' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAddressAssignment> findDocumentAddressAssignmentList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<DocumentAddressAssignment> result = findEntities(DocumentAddressAssignment.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentAddressAssignment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAddressAssignment> findDocumentAddressAssignmentList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<DocumentAddressAssignment> result = findEntities(DocumentAddressAssignment.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentAddressAssignment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAddressAssignment> findDocumentAddressAssignmentList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentAddressAssignment> result = findEntities(DocumentAddressAssignment.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentAddressAssignment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentAddressAssignment> findDocumentAddressAssignmentList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DocumentAddressAssignment> result = findEntities(DocumentAddressAssignment.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentAddressAssignmentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentAddressAssignment' instances.
     */
    public long getDocumentAddressAssignmentCount() {
        long result = getEntityCount(DocumentAddressAssignment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentAddressAssignment' instances which match the given whereClause.
     */
    public long getDocumentAddressAssignmentCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(DocumentAddressAssignment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentAddressAssignment' instances which match the given whereClause together with params specified in object array.
     */
    public long getDocumentAddressAssignmentCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(DocumentAddressAssignment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentAddressAssignmentCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document address assignment)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
