package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.FreelancerIncomeCalculation;
import de.smava.webapp.account.domain.interfaces.FreelancerIncomeEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'FreelancerIncomes'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractFreelancerIncome
    extends KreditPrivatEntity implements FreelancerIncomeCalculation,FreelancerIncomeEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractFreelancerIncome.class);

    private static final long serialVersionUID = 1L;

    private transient Double _adjustments;
    private transient Double _profit;

    public Double getAdjustments() {
        return _adjustments;
    }

    public void setAdjustments(Double adjustments) {
        _adjustments = adjustments;
    }

    public Double getProfit() {
        return _profit;
    }

    public void setProfit(Double profit) {
        _profit = profit;
    }


    public Double getExpensesValue() {
        return getExpenses();
    }

    public Double getVolumeValue() {
        return getVolume();
    }

    public void setExpensesValue(Double expenses) {
        setExpenses(expenses);
    }

    public void setVolumeValue(Double volume) {
        setVolume(volume);
    }


    public Double getIncomeTaxValue() {
        return getIncomeTax();
    }

    public Double getOwnConsumptionValue() {
        return getOwnConsumption();
    }

    public Double getSpecialReserveDissolutionValue() {
        return getSpecialReserveDissolution();
    }

    public Double getSpecialReserveValue() {
        return getSpecialReserve();
    }

    public void setIncomeTaxValue(Double incomeTaxValue) {
        setIncomeTax(incomeTaxValue);
    }

    public void setOwnConsumptionValue(Double ownConsumptionValue) {
        setOwnConsumption(ownConsumptionValue);
    }

    public void setSpecialReserveDissolutionValue(Double specialReserveDissolutionValue) {
        setSpecialReserveDissolution(specialReserveDissolutionValue);
    }

    public void setSpecialReserveValue(Double specialReserveValue) {
        setSpecialReserve(specialReserveValue);
    }


    public Double getChurchTaxValue() {
        return getChurchTax();
    }

    public Double getNonCashRevenuesValue() {
        return getNonCashRevenues();
    }

    public Double getSolidarityTaxValue() {
        return getSolidarityTax();
    }

    public Double getTradeTaxValue() {
        return getTradeTax();
    }

    public void setChurchTaxValue(Double churchTax) {
        setChurchTax(churchTax);
    }

    public void setNonCashRevenuesValue(Double nonCashRevenues) {
        setNonCashRevenues(nonCashRevenues);
    }

    public void setSolidarityTaxValue(Double solidarityTax) {
        setSolidarityTax(solidarityTax);
    }

    public void setTradeTaxValue(Double tradeTax) {
        setTradeTax(tradeTax);
    }

}

