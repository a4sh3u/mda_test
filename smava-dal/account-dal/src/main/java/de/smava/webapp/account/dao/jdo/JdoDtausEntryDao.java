//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dtaus entry)}

import de.smava.webapp.account.dao.DtausEntryDao;
import de.smava.webapp.account.domain.DtausEntry;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'DtausEntrys'.
 *
 * @author generator
 */
@Repository(value = "dtausEntryDao")
public class JdoDtausEntryDao extends JdoBaseDao implements DtausEntryDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDtausEntryDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(dtaus entry)}
    private static final long serialVersionUID = 3879413172808975662L;
    
    private static final String ACCOUNT_MAP_SQL
    	= "select de.id, ba.account_id from dtaus_entry de "
    	+ "inner join bank_account ba on de.credit_account_number = ba.account_no and de.credit_account_bank_code = ba.bank_code " 
    	+ "where ba.expiration_date is null and de.id in (:entries) and ba.type in (:bankAccountTypes)";
    
    
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the dtaus entry identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public DtausEntry load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntry - start: id=" + id);
        }
        DtausEntry result = getEntity(DtausEntry.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntry - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public DtausEntry getDtausEntry(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	DtausEntry entity = findUniqueEntity(DtausEntry.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the dtaus entry.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(DtausEntry dtausEntry) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDtausEntry: " + dtausEntry);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(dtaus entry)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(dtausEntry);
    }

    /**
     * @deprecated Use {@link #save(DtausEntry) instead}
     */
    public Long saveDtausEntry(DtausEntry dtausEntry) {
        return save(dtausEntry);
    }

    /**
     * Deletes an dtaus entry, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the dtaus entry
     */
    public void deleteDtausEntry(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDtausEntry: " + id);
        }
        deleteEntity(DtausEntry.class, id);
    }

    /**
     * Retrieves all 'DtausEntry' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<DtausEntry> getDtausEntryList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - start");
        }
        Collection<DtausEntry> result = getEntities(DtausEntry.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'DtausEntry' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<DtausEntry> getDtausEntryList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - start: pageable=" + pageable);
        }
        Collection<DtausEntry> result = getEntities(DtausEntry.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DtausEntry' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<DtausEntry> getDtausEntryList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - start: sortable=" + sortable);
        }
        Collection<DtausEntry> result = getEntities(DtausEntry.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DtausEntry' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<DtausEntry> getDtausEntryList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DtausEntry> result = getEntities(DtausEntry.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DtausEntry' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausEntry> findDtausEntryList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - start: whereClause=" + whereClause);
        }
        Collection<DtausEntry> result = findEntities(DtausEntry.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DtausEntry' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<DtausEntry> findDtausEntryList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<DtausEntry> result = findEntities(DtausEntry.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'DtausEntry' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausEntry> findDtausEntryList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<DtausEntry> result = findEntities(DtausEntry.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DtausEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausEntry> findDtausEntryList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<DtausEntry> result = findEntities(DtausEntry.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DtausEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausEntry> findDtausEntryList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DtausEntry> result = findEntities(DtausEntry.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DtausEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausEntry> findDtausEntryList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DtausEntry> result = findEntities(DtausEntry.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'DtausEntry' instances.
     */
    public long getDtausEntryCount() {
        long result = getEntityCount(DtausEntry.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DtausEntry' instances which match the given whereClause.
     */
    public long getDtausEntryCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(DtausEntry.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DtausEntry' instances which match the given whereClause together with params specified in object array.
     */
    public long getDtausEntryCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(DtausEntry.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausEntryCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(dtaus entry)}
    //
    
    public Map<Long, Long> getAccountIdsForDtausEntries(final Collection<DtausEntry> dtausEntries, final Set<String> bankAccountTypes) {
    	final Map<String, Object> params = new LinkedHashMap<String, Object>();
    	params.put("entries", dtausEntries);
    	params.put("bankAccountTypes", bankAccountTypes);
    	final String sql = replaceCollectionTypes(params, ACCOUNT_MAP_SQL);
    	return executeForMap(sql);
    }

	@Override 
	public Collection<Long> getDtausEntryIds(final Long dtausFileId) {
		final Map<String, Object> params = getSingleParamMap("dtausFileId", dtausFileId);
		final Collection<Long> result = executeNamedQuery(DtausEntry.class, "getDtausEntryIds", params, null, false);
		return result;
	}
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
