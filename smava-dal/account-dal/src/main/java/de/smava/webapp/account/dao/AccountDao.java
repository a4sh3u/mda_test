package de.smava.webapp.account.dao;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.MessageConfig;
import de.smava.webapp.account.domain.Profile;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.security.Role;
import de.smava.webapp.commons.web.AbstractListCommand;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface AccountDao extends BaseDao<Account> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Deletes an account, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account
     */
    void deleteAccount(Long id);

    /**
     * Retrieves all 'Account' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Account> getAccountList();

    /**
     * Retrieves a sorted page of 'Account' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Account> getAccountList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Account' instances.
     */
    long getAccountCount();

    /**
     * Attaches a Profile to the Account instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachProfile(Account account, Profile profile);

    /**
     * Attaches a MessageConfig to the Account instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachMessageConfig(Account account, MessageConfig messageConfig);

    /**
     * Returns the account for the given username.
     */
    Account findAccountByUsername(String username);

    /**
     * Returns the account for the given email adress.
     */
    Account findAccountByEmail(String email);

    /**
     * Returns an account with the given smavaId
     * @param smavaId is a general account identifier (also called accountId)
     * @return
     */
    Account findAccountBySmavaId(Long smavaId);

    /**
     * returns THE smava account.  The account all business related bank account belongs to
     * @return
     */
    Account getSmavaAccount();

    /**
     * Creates a fake person with the samva account.
     */
    void createSmavaAccountPerson();
    
    /**
     * Returns all account with an active role of the given type
     * 
     * @param role
     * @return
     */
    List<Account> findAccountByRole(Role role);
    
    /**
     * Returns all account with an active role of one of the given types
     * @param roles
     * @return
     */
    @Deprecated
    Collection<Account> findAccountByRoles(Collection<String> roles);
    
    /**
     * retrieves the Account that should be merged.
     * 
     * This are: All accounts that have accepted the Fidor AGB but doesn't have a 
     *           KOK or KNK with service provider fidor. 
     * 
     * @param roles the roles to be checked
     * @param bankAccountProviderId the id of service provider fidor 
     * @return the accounts to merge
     */
    Collection<Account> findFidorAccountsToMerge(final Collection<String> roles, Long bankAccountProviderId);
    
    /**
     * don't use this method as it return multiple lines on account with  more than 1 KOK/KNK
     * @see de.smava.webapp.account.dao.AccountDao#findAccountByRoleWithInternalBankAccount(de.smava.webapp.commons.security.Role)
     */
    @Deprecated
    Collection<Account> findAccountByRoleWithInternalBankAccount(Role role);
    
    List<Account> findActiveAccountsByRole(Role role);

    /**
     * Returns a list of accounts that are invited by the given account.
     * @param account inviter
     */
    List<Account> findInvitedAccounts(Account account);
    
    /**
     * loads the account and accesses the getters to fake eager loading
     * @param id
     * @return
     */
    Account preLoadAccount(Long id);

	/**
	 * Returns all borrower account that are paying their first rate of a smava privat credit
	 * @return
	 */
	Collection<FirstRepaymentMappingBean> findFirstRepayers();
	/**
	 * Returns all borrower accounts that were late on paying and still a contract running 
	 * @return
	 */
	Collection<DelayedRepaymentMappingBean> findDelayedRepayers();
	
	/**
	 * Returns all borrower account that are ready for the first audition by 
	 * the credit checks users
	 * 
	 * @param account  if not null only check that account
	 * @return
	 */
    Collection<Account> getBorrowerFirstAuditionAccounts(Account account);  
    
    /**
     * Returns all borrower accounts that have documents missing to perform a proper credit check
     * @return
     */
    Collection<Account> getBorrowerFurtherEnquiryAccounts();
    /**
     * Returns all borrower account that are ready for the second audition by 
	 * the credit checks users
	 * 
	 * @param account  if not null only check that account
     * @return
     */
    Collection<Account> getBorrowerSecondAuditionAccounts(Account account);    

    Collection<Account> getLenderAuditionAccounts(Account account);

    /**
     * returns a list of all Roles that are currently active
     * @param account
     * @return
     */
    Collection<String> getActiveRolenames(Account account);

    /**
     * Returns a list of all borrower accounts are currently handled by the smava privat 
     * dunning process.
     * @param pageable
     * @param sortable
     * @return
     */
    Collection<Account> getRemindedBorrowers(Pageable pageable, Sortable sortable);

    /**
     * returns all account that are managed by the given account
     * @param account
     * @return
     */
    Collection<Account> getManagedAccounts(Account account);
    
    /**
     * Returns a list of all accounts that have unchecked incoming mail (mail, email or fax)
     * @param role the role (borrower or lender)
     * @return
     */
    Collection<Account> getAccountWithIncomingMail(Role role);

    /**
     * Returns all lender accounts that receive mohtly payments
     * All lender account with transaction to a KOK  
     *  
     * @return
     */
	List<Account> findAccountsForPayout();

	/**
	 * ???
	 * @param account
	 * @return
	 */
    Collection<Account> getAccountsForRepaymentBreak(Account account);
    
    /**
     * Finds all account that have multiple account roles that needs to be corrected
     * 
     * @param role the Role not search for
     * @return
     */
    Collection<Account> findAccountsListForRoleConsolidation(String role);


    /**
     * Returns the accounts that should be informed by email about missing documents
     * @param days
     * @return
     */
    Collection<Account> getAccountsForMissingDocumentsReminderMail(final int days);

 
    /**
     * Returns all accounts that are supposed to pay activity fee.
     * The information is saved at his internal bank account 
     * 
     * @param contractStates the states of contracts
     * @param bankAccountType KOk or KNK
     * @return
     */
    Set<Long> getAccountIdsWithActivityFee(Set<String> contractStates, String bankAccountType);

    
    /**
     * retrieves the accounts who's documents might be deleted by the regular task
     * @return
     */
    Collection<Account> getAccountsForDeletionOfDocumentFiles();

	/**
	 * Recieves all Accounts with a date in recieve loan offers and that have no
	 * loan application (except Loan applications with given states)
	 * 
	 * @param date
	 *            the date for to find the accounts
	 * @param preferredRoles
	 *            A list of preferred roles that are allowed
	 * @param allowedLoanApplicationStates
	 *            allowed loan applications
	 * @return a list of accounts
	 */
	Collection<Account> findAccountsWithReciveLoanOffersForGivenDateAndPreferredRoles(
			Date date, Set<String> preferredRoles,
			Set<String> allowedLoanApplicationStates);

	/**
	 * Checks whether this customer has a smava privat credit in a dunning mode > 2 which means he is handled 
	 * by the smava privat dunning process.
	 * 
	 * @param account
	 * @return
	 */
    boolean getIsCurrentlyDelayedPayer(Account account);

    Collection<Account> getAccountsByPersonNames(String firstName, String lastName, AbstractListCommand<Account> pageAndSortable);

    Collection<Account> getAccountsByUsername(String username, AbstractListCommand<Account> pageAndSortable);

    /**
     * Search account entity by first name, last name, mail address and date of birth
     *
     * @param firstName
     * @param lastName
     * @param mail
     * @return account or NULL
     */
    Account findAccount(String firstName, String lastName, String mail, Date dateOfBirth);

    /**
     * This generic method is written as part of the fix for WEBSITE-17166.1
     * return accounts with allowed roles that are not expired and excludes accounts which have one of the
     * excluded roles which is not expired.
     *
     * Additionally performs check if account is active or not.
     *
     * this implementation is based on named query execution.
     *
     * @param allowedRoles
     * @param excludedRoles
     * @return
     */
    List<Account> findActiveAccountsByRoles(List<String> allowedRoles, List<String> excludedRoles) ;

    List<Account> findAllAccountsByPersonalAdvisorId(Long advisorId);

    Long getSmavaId(Long id);

}
