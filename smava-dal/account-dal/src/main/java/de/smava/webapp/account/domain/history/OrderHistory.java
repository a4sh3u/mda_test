package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractOrder;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Orders'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class OrderHistory extends AbstractOrder {

    protected transient Date _activationDateInitVal;
    protected transient boolean _activationDateIsSet;
    protected transient String _creditTermInitVal;
    protected transient boolean _creditTermIsSet;
    protected transient String _imageInitVal;
    protected transient boolean _imageIsSet;
    protected transient int _interestingInitVal;
    protected transient boolean _interestingIsSet;
    protected transient int _imageWidthInitVal;
    protected transient boolean _imageWidthIsSet;
    protected transient int _imageHeightInitVal;
    protected transient boolean _imageHeightIsSet;
    protected transient double _roiInitVal;
    protected transient boolean _roiIsSet;
    protected transient double _matchedRateInitVal;
    protected transient boolean _matchedRateIsSet;
    protected transient Date _agreementBlacklistCheckInitVal;
    protected transient boolean _agreementBlacklistCheckIsSet;
    protected transient Date _agreementTransferInitVal;
    protected transient boolean _agreementTransferIsSet;
    protected transient Date _agreementTaxCommitmentInitVal;
    protected transient boolean _agreementTaxCommitmentIsSet;
    protected transient boolean _fastFinancingInitVal;
    protected transient boolean _fastFinancingIsSet;
    protected transient boolean _automaticDiscriptionInitVal;
    protected transient boolean _automaticDiscriptionIsSet;
    protected transient boolean _internalDebtConsolidationInitVal;
    protected transient boolean _internalDebtConsolidationIsSet;


		
    /**
     * Returns the initial value of the property 'activation date'.
     */
    public Date activationDateInitVal() {
        Date result;
        if (_activationDateIsSet) {
            result = _activationDateInitVal;
        } else {
            result = getActivationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'activation date'.
     */
    public boolean activationDateIsDirty() {
        return !valuesAreEqual(activationDateInitVal(), getActivationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'activation date'.
     */
    public boolean activationDateIsSet() {
        return _activationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit term'.
     */
    public String creditTermInitVal() {
        String result;
        if (_creditTermIsSet) {
            result = _creditTermInitVal;
        } else {
            result = getCreditTerm();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit term'.
     */
    public boolean creditTermIsDirty() {
        return !valuesAreEqual(creditTermInitVal(), getCreditTerm());
    }

    /**
     * Returns true if the setter method was called for the property 'credit term'.
     */
    public boolean creditTermIsSet() {
        return _creditTermIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image'.
     */
    public String imageInitVal() {
        String result;
        if (_imageIsSet) {
            result = _imageInitVal;
        } else {
            result = getImage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image'.
     */
    public boolean imageIsDirty() {
        return !valuesAreEqual(imageInitVal(), getImage());
    }

    /**
     * Returns true if the setter method was called for the property 'image'.
     */
    public boolean imageIsSet() {
        return _imageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'interesting'.
     */
    public int interestingInitVal() {
        int result;
        if (_interestingIsSet) {
            result = _interestingInitVal;
        } else {
            result = getInteresting();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'interesting'.
     */
    public boolean interestingIsDirty() {
        return !valuesAreEqual(interestingInitVal(), getInteresting());
    }

    /**
     * Returns true if the setter method was called for the property 'interesting'.
     */
    public boolean interestingIsSet() {
        return _interestingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image width'.
     */
    public int imageWidthInitVal() {
        int result;
        if (_imageWidthIsSet) {
            result = _imageWidthInitVal;
        } else {
            result = getImageWidth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image width'.
     */
    public boolean imageWidthIsDirty() {
        return !valuesAreEqual(imageWidthInitVal(), getImageWidth());
    }

    /**
     * Returns true if the setter method was called for the property 'image width'.
     */
    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image height'.
     */
    public int imageHeightInitVal() {
        int result;
        if (_imageHeightIsSet) {
            result = _imageHeightInitVal;
        } else {
            result = getImageHeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image height'.
     */
    public boolean imageHeightIsDirty() {
        return !valuesAreEqual(imageHeightInitVal(), getImageHeight());
    }

    /**
     * Returns true if the setter method was called for the property 'image height'.
     */
    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }
				
    /**
     * Returns the initial value of the property 'roi'.
     */
    public double roiInitVal() {
        double result;
        if (_roiIsSet) {
            result = _roiInitVal;
        } else {
            result = getRoi();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'roi'.
     */
    public boolean roiIsDirty() {
        return !valuesAreEqual(roiInitVal(), getRoi());
    }

    /**
     * Returns true if the setter method was called for the property 'roi'.
     */
    public boolean roiIsSet() {
        return _roiIsSet;
    }
	
    /**
     * Returns the initial value of the property 'matched rate'.
     */
    public double matchedRateInitVal() {
        double result;
        if (_matchedRateIsSet) {
            result = _matchedRateInitVal;
        } else {
            result = getMatchedRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'matched rate'.
     */
    public boolean matchedRateIsDirty() {
        return !valuesAreEqual(matchedRateInitVal(), getMatchedRate());
    }

    /**
     * Returns true if the setter method was called for the property 'matched rate'.
     */
    public boolean matchedRateIsSet() {
        return _matchedRateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'agreement blacklist check'.
     */
    public Date agreementBlacklistCheckInitVal() {
        Date result;
        if (_agreementBlacklistCheckIsSet) {
            result = _agreementBlacklistCheckInitVal;
        } else {
            result = getAgreementBlacklistCheck();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'agreement blacklist check'.
     */
    public boolean agreementBlacklistCheckIsDirty() {
        return !valuesAreEqual(agreementBlacklistCheckInitVal(), getAgreementBlacklistCheck());
    }

    /**
     * Returns true if the setter method was called for the property 'agreement blacklist check'.
     */
    public boolean agreementBlacklistCheckIsSet() {
        return _agreementBlacklistCheckIsSet;
    }
	
    /**
     * Returns the initial value of the property 'agreement transfer'.
     */
    public Date agreementTransferInitVal() {
        Date result;
        if (_agreementTransferIsSet) {
            result = _agreementTransferInitVal;
        } else {
            result = getAgreementTransfer();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'agreement transfer'.
     */
    public boolean agreementTransferIsDirty() {
        return !valuesAreEqual(agreementTransferInitVal(), getAgreementTransfer());
    }

    /**
     * Returns true if the setter method was called for the property 'agreement transfer'.
     */
    public boolean agreementTransferIsSet() {
        return _agreementTransferIsSet;
    }
	
    /**
     * Returns the initial value of the property 'agreement tax commitment'.
     */
    public Date agreementTaxCommitmentInitVal() {
        Date result;
        if (_agreementTaxCommitmentIsSet) {
            result = _agreementTaxCommitmentInitVal;
        } else {
            result = getAgreementTaxCommitment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'agreement tax commitment'.
     */
    public boolean agreementTaxCommitmentIsDirty() {
        return !valuesAreEqual(agreementTaxCommitmentInitVal(), getAgreementTaxCommitment());
    }

    /**
     * Returns true if the setter method was called for the property 'agreement tax commitment'.
     */
    public boolean agreementTaxCommitmentIsSet() {
        return _agreementTaxCommitmentIsSet;
    }
		
    /**
     * Returns the initial value of the property 'fast financing'.
     */
    public boolean fastFinancingInitVal() {
        boolean result;
        if (_fastFinancingIsSet) {
            result = _fastFinancingInitVal;
        } else {
            result = getFastFinancing();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'fast financing'.
     */
    public boolean fastFinancingIsDirty() {
        return !valuesAreEqual(fastFinancingInitVal(), getFastFinancing());
    }

    /**
     * Returns true if the setter method was called for the property 'fast financing'.
     */
    public boolean fastFinancingIsSet() {
        return _fastFinancingIsSet;
    }
		
    /**
     * Returns the initial value of the property 'automatic discription'.
     */
    public boolean automaticDiscriptionInitVal() {
        boolean result;
        if (_automaticDiscriptionIsSet) {
            result = _automaticDiscriptionInitVal;
        } else {
            result = getAutomaticDiscription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'automatic discription'.
     */
    public boolean automaticDiscriptionIsDirty() {
        return !valuesAreEqual(automaticDiscriptionInitVal(), getAutomaticDiscription());
    }

    /**
     * Returns true if the setter method was called for the property 'automatic discription'.
     */
    public boolean automaticDiscriptionIsSet() {
        return _automaticDiscriptionIsSet;
    }
			
    /**
     * Returns the initial value of the property 'internal debt consolidation'.
     */
    public boolean internalDebtConsolidationInitVal() {
        boolean result;
        if (_internalDebtConsolidationIsSet) {
            result = _internalDebtConsolidationInitVal;
        } else {
            result = getInternalDebtConsolidation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'internal debt consolidation'.
     */
    public boolean internalDebtConsolidationIsDirty() {
        return !valuesAreEqual(internalDebtConsolidationInitVal(), getInternalDebtConsolidation());
    }

    /**
     * Returns true if the setter method was called for the property 'internal debt consolidation'.
     */
    public boolean internalDebtConsolidationIsSet() {
        return _internalDebtConsolidationIsSet;
    }
		
}
