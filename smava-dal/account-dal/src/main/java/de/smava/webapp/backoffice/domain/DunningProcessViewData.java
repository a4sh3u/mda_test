//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.backoffice.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dunning process view data)}
import de.smava.webapp.backoffice.domain.history.DunningProcessViewDataHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DunningProcessViewDatas'.
 *
 * 
 *
 * @author generator
 */
public class DunningProcessViewData extends DunningProcessViewDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(dunning process view data)}

    public DunningProcessViewData() {
        super();
    }


    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Order _order;
        protected de.smava.webapp.account.domain.Account _changedBy;
        protected String _comment;
        protected Date _creationDate;
        
                                    
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    public void setOrder(de.smava.webapp.account.domain.Order order) {
        _order = order;
    }
            
    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Order getOrder() {
        return _order;
    }
                                            
    /**
     * Setter for the property 'changed by'.
     *
     * 
     *
     */
    public void setChangedBy(de.smava.webapp.account.domain.Account changedBy) {
        _changedBy = changedBy;
    }
            
    /**
     * Returns the property 'changed by'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getChangedBy() {
        return _changedBy;
    }
                                    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = getComment();
        }
        registerChange("comment", _commentInitVal, comment);
        _comment = comment;
    }
                        
    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    public String getComment() {
        return _comment;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DunningProcessViewData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _comment=").append(_comment);
            builder.append("\n}");
        } else {
            builder.append(DunningProcessViewData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DunningProcessViewData asDunningProcessViewData() {
        return this;
    }
}
