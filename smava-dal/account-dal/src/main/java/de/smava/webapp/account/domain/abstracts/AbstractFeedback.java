package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.FeedbackEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'Feedbacks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractFeedback
    extends KreditPrivatEntity implements FeedbackEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractFeedback.class);

    public static final String TYPE_GENERAL_OPINION = "general_opinion";
    public static final String TYPE_UNIQUE_SELLING_PROPOSITION = "unique_selling_ proposition";
    public static final String TYPE_LENDER_MESSAGE = "lender_message";

}

