package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.BookingAssignment;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.Transaction;

import java.util.Collection;


/**
 * The domain object that represents 'Bookings'.
 *
 * @author generator
 */
public interface BookingEntityInterface {

    /**
     * Setter for the property 'integer type'.
     *
     * 
     *
     */
    void setIntegerType(Integer integerType);

    /**
     * Returns the property 'integer type'.
     *
     * 
     *
     */
    Integer getIntegerType();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    double getAmount();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'transaction'.
     *
     * 
     *
     */
    void setTransaction(Transaction transaction);

    /**
     * Returns the property 'transaction'.
     *
     * 
     *
     */
    Transaction getTransaction();
    /**
     * Setter for the property 'contract'.
     *
     * 
     *
     */
    void setContract(Contract contract);

    /**
     * Returns the property 'contract'.
     *
     * 
     *
     */
    Contract getContract();
    /**
     * Setter for the property 'booking assignments'.
     *
     * 
     *
     */
    void setBookingAssignments(Collection<BookingAssignment> bookingAssignments);

    /**
     * Returns the property 'booking assignments'.
     *
     * 
     *
     */
    Collection<BookingAssignment> getBookingAssignments();

}
