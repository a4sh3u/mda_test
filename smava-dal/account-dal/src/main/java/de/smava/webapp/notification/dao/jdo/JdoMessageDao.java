//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.notification.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(message)}

import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.notification.dao.MessageDao;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Message;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'Messages'.
 *
 * @author generator
 */
@Repository(value = "messageDao")
public class JdoMessageDao extends JdoBaseDao implements MessageDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMessageDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(message)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the message identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Message load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessage - start: id=" + id);
        }
        Message result = getEntity(Message.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessage - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Message getMessage(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Message entity = findUniqueEntity(Message.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the message.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Message message) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMessage: " + message);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(message)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(message);
    }

    /**
     * @deprecated Use {@link #save(Message) instead}
     */
    public Long saveMessage(Message message) {
        return save(message);
    }

    /**
     * Deletes an message, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the message
     */
    public void deleteMessage(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMessage: " + id);
        }
        deleteEntity(Message.class, id);
    }

    /**
     * Retrieves all 'Message' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Message> getMessageList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - start");
        }
        Collection<Message> result = getEntities(Message.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Message' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Message> getMessageList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - start: pageable=" + pageable);
        }
        Collection<Message> result = getEntities(Message.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Message' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Message> getMessageList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - start: sortable=" + sortable);
        }
        Collection<Message> result = getEntities(Message.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Message' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Message> getMessageList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Message> result = getEntities(Message.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Message' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Message> findMessageList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - start: whereClause=" + whereClause);
        }
        Collection<Message> result = findEntities(Message.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Message' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Message> findMessageList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Message> result = findEntities(Message.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Message' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Message> findMessageList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Message> result = findEntities(Message.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Message' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Message> findMessageList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Message> result = findEntities(Message.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Message' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Message> findMessageList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Message> result = findEntities(Message.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Message' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Message> findMessageList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Message> result = findEntities(Message.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMessageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Message' instances.
     */
    public long getMessageCount() {
        long result = getEntityCount(Message.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Message' instances which match the given whereClause.
     */
    public long getMessageCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Message.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Message' instances which match the given whereClause together with params specified in object array.
     */
    public long getMessageCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Message.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMessageCount() - result: count=" + result);
        }
        return result;
    }


    /**
     * Attaches a Account to the Message instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    public void attachAccount(Message message, Account account) {
        message.setOwner(account);
        account.getMessages().add(message);
        saveEntity(account);
        saveEntity(message);
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(message)}
    //
    @Override
    public Collection<Message> findMessagesByOwner(Account searchObject, Pageable pageable, Sortable sortable) {
        return findMessageList("_owner._id == " + searchObject.getId(), pageable, sortable);

    }

    @Override
    public Collection<Message> findMessagesByOwnerAndFolder(String folder, Account account) {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals("_owner", account), term.equals("_folder", folder));
        term.setSortBy("_creationDate");
        term.setSortDescending(true);

        Collection<Message> messages = findMessageList(term.toString(), account);
        return messages;
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
