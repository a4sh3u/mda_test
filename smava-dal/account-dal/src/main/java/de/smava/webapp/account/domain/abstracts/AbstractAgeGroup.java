package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.AgeGroupEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'AgeGroups'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractAgeGroup
    extends KreditPrivatEntity implements AgeGroupEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAgeGroup.class);
}

