//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi response)}

import de.smava.webapp.account.domain.RdiResponse;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.account.dao.RdiResponseDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RdiResponses'.
 *
 * @author generator
 */
@Repository(value = "rdiResponseDao")
public class JdoRdiResponseDao extends JdoBaseDao implements RdiResponseDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRdiResponseDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rdi response)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the rdi response identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RdiResponse load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponse - start: id=" + id);
        }
        RdiResponse result = getEntity(RdiResponse.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponse - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RdiResponse getRdiResponse(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RdiResponse entity = findUniqueEntity(RdiResponse.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the rdi response.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RdiResponse rdiResponse) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRdiResponse: " + rdiResponse);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rdi response)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(rdiResponse);
    }

    /**
     * @deprecated Use {@link #save(RdiResponse) instead}
     */
    public Long saveRdiResponse(RdiResponse rdiResponse) {
        return save(rdiResponse);
    }

    /**
     * Deletes an rdi response, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi response
     */
    public void deleteRdiResponse(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRdiResponse: " + id);
        }
        deleteEntity(RdiResponse.class, id);
    }

    /**
     * Retrieves all 'RdiResponse' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RdiResponse> getRdiResponseList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - start");
        }
        Collection<RdiResponse> result = getEntities(RdiResponse.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RdiResponse' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RdiResponse> getRdiResponseList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - start: pageable=" + pageable);
        }
        Collection<RdiResponse> result = getEntities(RdiResponse.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiResponse' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RdiResponse> getRdiResponseList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - start: sortable=" + sortable);
        }
        Collection<RdiResponse> result = getEntities(RdiResponse.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiResponse' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RdiResponse> getRdiResponseList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiResponse> result = getEntities(RdiResponse.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiResponse' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiResponse> findRdiResponseList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - start: whereClause=" + whereClause);
        }
        Collection<RdiResponse> result = findEntities(RdiResponse.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiResponse' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RdiResponse> findRdiResponseList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RdiResponse> result = findEntities(RdiResponse.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RdiResponse' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiResponse> findRdiResponseList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RdiResponse> result = findEntities(RdiResponse.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiResponse' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiResponse> findRdiResponseList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RdiResponse> result = findEntities(RdiResponse.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiResponse' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiResponse> findRdiResponseList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiResponse> result = findEntities(RdiResponse.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiResponse' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiResponse> findRdiResponseList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiResponse> result = findEntities(RdiResponse.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiResponseList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RdiResponse' instances.
     */
    public long getRdiResponseCount() {
        long result = getEntityCount(RdiResponse.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiResponse' instances which match the given whereClause.
     */
    public long getRdiResponseCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RdiResponse.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiResponse' instances which match the given whereClause together with params specified in object array.
     */
    public long getRdiResponseCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RdiResponse.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiResponseCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi response)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
