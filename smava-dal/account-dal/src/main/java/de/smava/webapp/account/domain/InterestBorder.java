//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(interest border)}
import de.smava.webapp.account.domain.history.InterestBorderHistory;

import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'InterestBorders'.
 *
 * @author generator
 */
public class InterestBorder extends InterestBorderHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(interest border)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _rating;
        protected Integer _duration;
        protected List<Double> _values;
        protected Boolean _adjusted;
        
                            /**
     * Setter for the property 'rating'.
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     */
    public String getRating() {
        return _rating;
    }
                                            
    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration) {
        _duration = duration;
    }
            
    /**
     * Returns the property 'duration'.
     */
    public Integer getDuration() {
        return _duration;
    }
                                            
    /**
     * Setter for the property 'values'.
     */
    public void setValues(List<Double> values) {
        _values = values;
    }
            
    /**
     * Returns the property 'values'.
     */
    public List<Double> getValues() {
        return _values;
    }
                                            
    /**
     * Setter for the property 'adjusted'.
     */
    public void setAdjusted(Boolean adjusted) {
        _adjusted = adjusted;
    }
            
    /**
     * Returns the property 'adjusted'.
     */
    public Boolean getAdjusted() {
        return _adjusted;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(InterestBorder.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n}");
        } else {
            builder.append(InterestBorder.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public InterestBorder asInterestBorder() {
        return this;
    }
}
