package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Approval;
import de.smava.webapp.account.domain.RepaymentBreak;
import de.smava.webapp.account.domain.RepaymentBreakDetail;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'RepaymentBreaks'.
 *
 * @author generator
 */
public interface RepaymentBreakEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'offer document'.
     *
     * 
     *
     */
    void setOfferDocument(de.smava.webapp.account.domain.Document offerDocument);

    /**
     * Returns the property 'offer document'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Document getOfferDocument();
    /**
     * Setter for the property 'form document'.
     *
     * 
     *
     */
    void setFormDocument(de.smava.webapp.account.domain.Document formDocument);

    /**
     * Returns the property 'form document'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Document getFormDocument();
    /**
     * Setter for the property 'details'.
     *
     * 
     *
     */
    void setDetails(Set<RepaymentBreakDetail> details);

    /**
     * Returns the property 'details'.
     *
     * 
     *
     */
    Set<RepaymentBreakDetail> getDetails();
    /**
     * Setter for the property 'approvals'.
     *
     * 
     *
     */
    void setApprovals(Set<Approval> approvals);

    /**
     * Returns the property 'approvals'.
     *
     * 
     *
     */
    Set<Approval> getApprovals();

}
