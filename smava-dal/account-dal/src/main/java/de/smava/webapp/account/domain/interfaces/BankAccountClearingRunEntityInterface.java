package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.ClearingRun;

import java.util.Date;


/**
 * The domain object that represents 'BankAccountClearingRuns'.
 *
 * @author generator
 */
public interface BankAccountClearingRunEntityInterface {

    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    void setBankAccount(BankAccount bankAccount);

    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    BankAccount getBankAccount();
    /**
     * Setter for the property 'clearing run'.
     *
     * 
     *
     */
    void setClearingRun(ClearingRun clearingRun);

    /**
     * Returns the property 'clearing run'.
     *
     * 
     *
     */
    ClearingRun getClearingRun();
    /**
     * Setter for the property 'transaction id'.
     *
     * 
     *
     */
    void setTransactionId(Long transactionId);

    /**
     * Returns the property 'transaction id'.
     *
     * 
     *
     */
    Long getTransactionId();
    /**
     * Setter for the property 'delivery date'.
     *
     * 
     *
     */
    void setDeliveryDate(Date deliveryDate);

    /**
     * Returns the property 'delivery date'.
     *
     * 
     *
     */
    Date getDeliveryDate();

}
