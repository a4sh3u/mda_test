//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(address)}

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.interfaces.AddressEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Set;
                                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Addresss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAddress
    extends KreditPrivatEntity implements AddressEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAddress.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(address)}
    private static final long serialVersionUID = -5277813248584311806L;

    public static final String DOCUMENT_ASSIGNMENT_SENDER = "sender";
    public static final String DOCUMENT_ASSIGNMENT_RECIPIENT = "recipient";

    public static final String TYPE_MAIN_ADDRESS = "main"; // Meldeadresse (last one is valid)
    public static final String TYPE_MAIL_ADDRESS = "mail"; // Postadresse

    public static final String TYPE_CO_BORROWER_MAIN_ADDRESS = "co_borrower_main";

    public static final String TYPE_BORROWER_EMPLOYER_ADDRESS = "borrower_employer";
    public static final String TYPE_CO_BORROWER_EMPLOYER_ADDRESS = "co_borrower_employer";

    public static final String TYPE_CO_BORROWER_SCHUFA_ADDRESS = "co_borrower_schufa";

    public static final Set<String> TYPE_NAMES = ConstCollector.getAll("TYPE_");



    public boolean isMailAddress() {
        return TYPE_MAIL_ADDRESS.equals(getType());
    }

    public boolean isMainAddress() {
        return TYPE_MAIN_ADDRESS.equals(getType());
    }

    public boolean isBorrowerEmployerAddress() {
        return TYPE_BORROWER_EMPLOYER_ADDRESS.equals(getType());
    }

    public boolean isCoBorrowerEmployerAddress() {
        return TYPE_CO_BORROWER_EMPLOYER_ADDRESS.equals(getType());
    }

    public boolean isCoBorrowerSchufaAddress() {
        return TYPE_CO_BORROWER_SCHUFA_ADDRESS.equals(getType());
    }

    /**
     * Returns the property 'enabled'.
     */
    public boolean getEnabled() {
        // enabled := address has no expiration date
        // disabled := address has expiration date
        return getExpirationDate() == null;
    }


    /*
	public boolean hasAddressChanged(Address address) {
		return
			(_city == null && address._city != null) || (_city != null && !_city.equals(address._city))
			|| (_country == null && address._country != null) || (_country != null && !_country.equals(address._country))
			|| (_zipCode == null && address._zipCode != null) || (_zipCode != null && !_zipCode.equals(address._zipCode))
			|| (_street == null && address._street != null) || (_street != null && !_street.equals(address._street))
			|| (_streetNumber == null && address._streetNumber != null) || (_streetNumber != null && !_streetNumber.equals(address._streetNumber));
	}
	*/

    public String getStreetAndNumber() {
        return getStreet() + " " + getStreetNumber();
    }

    public String getZipAndCity() {
        return getZipCode() + " " + getCity();
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

