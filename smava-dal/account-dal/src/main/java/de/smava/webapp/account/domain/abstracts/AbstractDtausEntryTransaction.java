package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.DtausEntry;
import de.smava.webapp.account.domain.DtausEntryTransaction;
import de.smava.webapp.account.domain.Transaction;
import de.smava.webapp.account.domain.interfaces.DtausEntryTransactionEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.ObjectUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The domain object that represents 'DtausEntryTransactions'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractDtausEntryTransaction
    extends KreditPrivatEntity implements DtausEntryTransactionEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDtausEntryTransaction.class);

    private static final long serialVersionUID = 6610267321415699628L;

    // "States" (types) for DtausEntry-Transaction-Relation specifying the relation
    public static final String TYPE_IN_RELATED = "IN"; // DtausEntry belongs to direction-IN-DtausFile and transaction was the "origin" of the DtausEntry
    public static final String TYPE_IN_FOLLOW = "IN_FOLLOW"; // DtausEntry belongs to direction-IN-DtausFile and transaction is a resulting/new created transaction
    public static final String TYPE_IN_CONFIRMED = "IN_CONFIRMED"; // DtausEntry belongs to direction-IN-DtausFile and transaction is confirmed by DtausEntry
    public static final String TYPE_OUT_RELATED = "OUT"; // (Not yet used) DtausEntry belongs to direction-OUT-DausFile

    /**
     *
     * @param dtausEntry
     * @param transaction
     * @param dtausEntryTransactionType
     * @return
     */
    public static DtausEntryTransaction connect(DtausEntry dtausEntry, Transaction transaction, String dtausEntryTransactionType) {
        Set<DtausEntryTransaction> entryTransactions = dtausEntry.getDtausEntryTransactions();
        if (entryTransactions == null) {
            entryTransactions = new HashSet<DtausEntryTransaction>();
            dtausEntry.setDtausEntryTransactions(entryTransactions);
        }
        Set<DtausEntryTransaction> transactionEntries = transaction.getDtausEntryTransactions();
        if (transactionEntries == null) {
            transactionEntries = new HashSet<DtausEntryTransaction>();
            transaction.setDtausEntryTransactions(transactionEntries);
        }

        DtausEntryTransaction assignment = new DtausEntryTransaction(dtausEntry, transaction, dtausEntryTransactionType);
        if (CollectionUtils.find(dtausEntry.getDtausEntryTransactions(), new DtausEntryTransactionPredicate(assignment)) == null) {
            dtausEntry.getDtausEntryTransactions().add(assignment);
        }
        if (CollectionUtils.find(transaction.getDtausEntryTransactions(), new DtausEntryTransactionPredicate(assignment)) == null) {
            transaction.getDtausEntryTransactions().add(assignment);
        }
        if ( ! DtausEntryTransaction.TYPE_IN_FOLLOW.equals( dtausEntryTransactionType)){
            transaction.setExternalTransactionId(dtausEntry.getExternalTransactionId());
        }
        return assignment;
    }

    /**
     * predicate to find exact copies of dtausEntryTransactions.
     * @author rfiedler
     *
     */
    static class DtausEntryTransactionPredicate implements Predicate {
        private DtausEntryTransaction _det = null;

        DtausEntryTransactionPredicate(DtausEntryTransaction det) {
            this._det = det;
        }

        public boolean evaluate(Object arg) {
            boolean result = false;
            if (arg instanceof DtausEntryTransaction) {
                DtausEntryTransaction det = (DtausEntryTransaction) arg;
                result = ObjectUtils.equals(det.getTransaction(), _det.getTransaction()) && ObjectUtils.equals(det.getDtausEntry(), _det.getDtausEntry());
            }
            return result;
        }
    }


    public static List<DtausEntryTransaction> disconnect(DtausEntry dtausEntry, Transaction transaction, String dtausEntryTransactionType) {
        List<DtausEntryTransaction> toRemove = new ArrayList<DtausEntryTransaction>();
        for (DtausEntryTransaction assigment : dtausEntry.getDtausEntryTransactions()) {
            if (assigment.getDtausEntry().equals(dtausEntry) && assigment.getTransaction().equals(transaction) && (assigment.getType().equals(dtausEntryTransactionType) || "*".equals(dtausEntryTransactionType))) {
                toRemove.add(assigment);
            }
        }
        for (DtausEntryTransaction det : toRemove) {
            dtausEntry.getDtausEntryTransactions().remove(det);
            transaction.getDtausEntryTransactions().remove(det);
        }
        return toRemove;
    }

    /**
     * For given entry get all transactions that are connected with the entry respecting the type.
     * @param dtausEntryTransactionType Type constant or "*" for all
     */
    public static List<Transaction> getRelatedTransactions(DtausEntry entry, String dtausEntryTransactionType) {
        List<Transaction> list = new ArrayList<Transaction>();
        if (entry.getDtausEntryTransactions() != null) {
            for (DtausEntryTransaction det : entry.getDtausEntryTransactions()) {
                if (det.getDtausEntry().equals(entry) && (det.getType().equals(dtausEntryTransactionType) || "*".equals(dtausEntryTransactionType))) {
                    list.add(det.getTransaction());
                }
            }
        }
        return list;
    }

    /**
     * For given transaction get (all different) transactions that are connected by same DtausEntry.
     * @param dtausEntryTransactionType Type constant or "*" for all
     */
    public static List<Transaction> getRelatedTransactions(Transaction transaction, String dtausEntryTransactionType) {
        List<Transaction> list = new ArrayList<Transaction>();
        if (transaction.getDtausEntryTransactions() != null) {
            for (DtausEntryTransaction det : transaction.getDtausEntryTransactions()) {
                DtausEntry entry = det.getDtausEntry();
                if (entry != null) {
                    for (Transaction entryTransaction : DtausEntryTransaction.getRelatedTransactions(entry, dtausEntryTransactionType)) {
                        if (!entryTransaction.equals(transaction)) {
                            list.add(entryTransaction);
                        }
                    }
                }
            }
        }
        return list;
    }

    public static List<DtausEntry> getRelatedDtausEntries(Transaction transaction, String dtausEntryTransactionType) {
        List<DtausEntry> list = new ArrayList<DtausEntry>();
        if (transaction.getDtausEntryTransactions() != null) {
            for (DtausEntryTransaction det : transaction.getDtausEntryTransactions()) {
                if (det.getTransaction().equals(transaction) && (det.getType().equals(dtausEntryTransactionType) || "*".equals(dtausEntryTransactionType))) {
                    list.add(det.getDtausEntry());
                }
            }
        }
        return list;
    }

}

