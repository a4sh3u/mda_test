package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractOrderRecommendation;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'OrderRecommendations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class OrderRecommendationHistory extends AbstractOrderRecommendation {

    protected transient String _textInitVal;
    protected transient boolean _textIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;


	
    /**
     * Returns the initial value of the property 'text'.
     */
    public String textInitVal() {
        String result;
        if (_textIsSet) {
            result = _textInitVal;
        } else {
            result = getText();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'text'.
     */
    public boolean textIsDirty() {
        return !valuesAreEqual(textInitVal(), getText());
    }

    /**
     * Returns true if the setter method was called for the property 'text'.
     */
    public boolean textIsSet() {
        return _textIsSet;
    }
			
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }

}
