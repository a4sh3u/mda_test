package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractMatchSorting;




/**
 * The domain object that has all history aggregation related fields for 'MatchSortings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MatchSortingHistory extends AbstractMatchSorting {

    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient int _positionInitVal;
    protected transient boolean _positionIsSet;
    protected transient int _resultNumberInitVal;
    protected transient boolean _resultNumberIsSet;
    protected transient String _offerSortingInitVal;
    protected transient boolean _offerSortingIsSet;
    protected transient String _orderSortingInitVal;
    protected transient boolean _orderSortingIsSet;


	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'position'.
     */
    public int positionInitVal() {
        int result;
        if (_positionIsSet) {
            result = _positionInitVal;
        } else {
            result = getPosition();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'position'.
     */
    public boolean positionIsDirty() {
        return !valuesAreEqual(positionInitVal(), getPosition());
    }

    /**
     * Returns true if the setter method was called for the property 'position'.
     */
    public boolean positionIsSet() {
        return _positionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'result number'.
     */
    public int resultNumberInitVal() {
        int result;
        if (_resultNumberIsSet) {
            result = _resultNumberInitVal;
        } else {
            result = getResultNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'result number'.
     */
    public boolean resultNumberIsDirty() {
        return !valuesAreEqual(resultNumberInitVal(), getResultNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'result number'.
     */
    public boolean resultNumberIsSet() {
        return _resultNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'offer sorting'.
     */
    public String offerSortingInitVal() {
        String result;
        if (_offerSortingIsSet) {
            result = _offerSortingInitVal;
        } else {
            result = getOfferSorting();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'offer sorting'.
     */
    public boolean offerSortingIsDirty() {
        return !valuesAreEqual(offerSortingInitVal(), getOfferSorting());
    }

    /**
     * Returns true if the setter method was called for the property 'offer sorting'.
     */
    public boolean offerSortingIsSet() {
        return _offerSortingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'order sorting'.
     */
    public String orderSortingInitVal() {
        String result;
        if (_orderSortingIsSet) {
            result = _orderSortingInitVal;
        } else {
            result = getOrderSorting();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'order sorting'.
     */
    public boolean orderSortingIsDirty() {
        return !valuesAreEqual(orderSortingInitVal(), getOrderSorting());
    }

    /**
     * Returns true if the setter method was called for the property 'order sorting'.
     */
    public boolean orderSortingIsSet() {
        return _orderSortingIsSet;
    }
	
}
