/**
 * 
 */
package de.smava.webapp.account.todo.dao;


/**
 * class encapsulates volume and count of a given collection.
 * 
 * @author rfiedler
 *
 */
public class VolumeCount {
    private Double _volume = 0.0;
    private Long _orderCount = 0L;
    private Long _offerCount = 0L;

    public VolumeCount() {
        super();
    }
    
    public VolumeCount(Double volume, Long orderCount) {
        super();
        this._volume = (volume == null ? 0.0 : volume);
        this._orderCount = orderCount;
    }
    public Double getVolume() {
        return _volume;
    }
    public void setVolume(Double volume) {
        this._volume = (volume == null ? 0.0 : volume);
    }
    public Long getOrderCount() {
        return _orderCount;
    }
    public void setOrderCount(Long count) {
        this._orderCount = count;
    }

    public Long getOfferCount() {
        return _offerCount;
    }

    public void setOfferCount(Long offerCount) {
        this._offerCount = offerCount;
    }
    
    
    
}