package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.SchufaScore;


/**
 * The domain object that represents 'SchufaScoreTokens'.
 *
 * @author generator
 */
public interface SchufaScoreTokenEntityInterface {

    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    void setScore(SchufaScore score);

    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    SchufaScore getScore();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'token code'.
     *
     * 
     *
     */
    void setTokenCode(String tokenCode);

    /**
     * Returns the property 'token code'.
     *
     * 
     *
     */
    String getTokenCode();
    /**
     * Setter for the property 'currency'.
     *
     * 
     *
     */
    void setCurrency(String currency);

    /**
     * Returns the property 'currency'.
     *
     * 
     *
     */
    String getCurrency();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(String amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    String getAmount();
    /**
     * Setter for the property 'date'.
     *
     * 
     *
     */
    void setDate(String date);

    /**
     * Returns the property 'date'.
     *
     * 
     *
     */
    String getDate();
    /**
     * Setter for the property 'rate type'.
     *
     * 
     *
     */
    void setRateType(String rateType);

    /**
     * Returns the property 'rate type'.
     *
     * 
     *
     */
    String getRateType();
    /**
     * Setter for the property 'rate count'.
     *
     * 
     *
     */
    void setRateCount(String rateCount);

    /**
     * Returns the property 'rate count'.
     *
     * 
     *
     */
    String getRateCount();
    /**
     * Setter for the property 'account number'.
     *
     * 
     *
     */
    void setAccountNumber(String accountNumber);

    /**
     * Returns the property 'account number'.
     *
     * 
     *
     */
    String getAccountNumber();
    /**
     * Setter for the property 'token text'.
     *
     * 
     *
     */
    void setTokenText(String tokenText);

    /**
     * Returns the property 'token text'.
     *
     * 
     *
     */
    String getTokenText();
    /**
     * Setter for the property 'text field'.
     *
     * 
     *
     */
    void setTextField(String textField);

    /**
     * Returns the property 'text field'.
     *
     * 
     *
     */
    String getTextField();
    /**
     * Setter for the property 'custom token code'.
     *
     * 
     *
     */
    void setCustomTokenCode(String customTokenCode);

    /**
     * Returns the property 'custom token code'.
     *
     * 
     *
     */
    String getCustomTokenCode();
    /**
     * Setter for the property 'calculated rate'.
     *
     * 
     *
     */
    void setCalculatedRate(double calculatedRate);

    /**
     * Returns the property 'calculated rate'.
     *
     * 
     *
     */
    double getCalculatedRate();
    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    void setComment(String comment);

    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    String getComment();
    /**
     * Setter for the property 'finished'.
     *
     * 
     *
     */
    void setFinished(boolean finished);

    /**
     * Returns the property 'finished'.
     *
     * 
     *
     */
    boolean getFinished();
    /**
     * Setter for the property 'business'.
     *
     * 
     *
     */
    void setBusiness(boolean business);

    /**
     * Returns the property 'business'.
     *
     * 
     *
     */
    boolean getBusiness();
    /**
     * Setter for the property 'custom rate'.
     *
     * 
     *
     */
    void setCustomRate(Double customRate);

    /**
     * Returns the property 'custom rate'.
     *
     * 
     *
     */
    Double getCustomRate();
    /**
     * Setter for the property 'is smava token'.
     *
     * 
     *
     */
    void setIsSmavaToken(boolean isSmavaToken);

    /**
     * Returns the property 'is smava token'.
     *
     * 
     *
     */
    boolean getIsSmavaToken();
    /**
     * Setter for the property 'refinance'.
     *
     * 
     *
     */
    void setRefinance(boolean refinance);

    /**
     * Returns the property 'refinance'.
     *
     * 
     *
     */
    boolean getRefinance();

}
