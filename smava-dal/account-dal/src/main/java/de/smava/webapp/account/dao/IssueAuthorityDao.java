//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(issue authority)}

import de.smava.webapp.account.domain.IssueAuthority;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'IssueAuthoritys'.
 *
 * @author generator
 */
public interface IssueAuthorityDao extends BaseDao<IssueAuthority>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the issue authority identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    IssueAuthority getIssueAuthority(Long id);

    /**
     * Saves the issue authority.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveIssueAuthority(IssueAuthority issueAuthority);

    /**
     * Deletes an issue authority, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the issue authority
     */
    void deleteIssueAuthority(Long id);

    /**
     * Retrieves all 'IssueAuthority' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<IssueAuthority> getIssueAuthorityList();

    /**
     * Retrieves a page of 'IssueAuthority' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<IssueAuthority> getIssueAuthorityList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'IssueAuthority' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<IssueAuthority> getIssueAuthorityList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'IssueAuthority' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<IssueAuthority> getIssueAuthorityList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'IssueAuthority' instances.
     */
    long getIssueAuthorityCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(issue authority)}
    //

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
