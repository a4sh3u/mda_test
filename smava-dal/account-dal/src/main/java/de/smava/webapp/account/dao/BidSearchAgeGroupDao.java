//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid search age group)}

import de.smava.webapp.account.domain.BidSearchAgeGroup;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BidSearchAgeGroups'.
 *
 * @author generator
 */
public interface BidSearchAgeGroupDao extends BaseDao<BidSearchAgeGroup>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bid search age group identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BidSearchAgeGroup getBidSearchAgeGroup(Long id);

    /**
     * Saves the bid search age group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBidSearchAgeGroup(BidSearchAgeGroup bidSearchAgeGroup);

    /**
     * Deletes an bid search age group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid search age group
     */
    void deleteBidSearchAgeGroup(Long id);

    /**
     * Retrieves all 'BidSearchAgeGroup' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BidSearchAgeGroup> getBidSearchAgeGroupList();

    /**
     * Retrieves a page of 'BidSearchAgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BidSearchAgeGroup> getBidSearchAgeGroupList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BidSearchAgeGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BidSearchAgeGroup> getBidSearchAgeGroupList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BidSearchAgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BidSearchAgeGroup> getBidSearchAgeGroupList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'BidSearchAgeGroup' instances.
     */
    long getBidSearchAgeGroupCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid search age group)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
