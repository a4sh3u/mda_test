package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.DtausFile;
import de.smava.webapp.account.domain.DtausFileOutTransactionType;
import de.smava.webapp.account.domain.Transaction;


/**
 * The domain object that represents 'DtausFileOutTransactions'.
 *
 * @author generator
 */
public interface DtausFileOutTransactionEntityInterface {

    /**
     * Setter for the property 'transaction'.
     *
     * 
     *
     */
    void setTransaction(Transaction transaction);

    /**
     * Returns the property 'transaction'.
     *
     * 
     *
     */
    Transaction getTransaction();
    /**
     * Setter for the property 'dtaus file'.
     *
     * 
     *
     */
    void setDtausFile(DtausFile dtausFile);

    /**
     * Returns the property 'dtaus file'.
     *
     * 
     *
     */
    DtausFile getDtausFile();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(DtausFileOutTransactionType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    DtausFileOutTransactionType getType();

}
