package de.smava.webapp.account.todo.dao;

import de.smava.webapp.account.domain.Category;

import java.io.Serializable;

/**
 * contains result of categoryOrderCount query.
 * @author rfiedler
 *
 */
public class CategoryOrderResult implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private Category _category;
    private Long _count;
    
    public CategoryOrderResult() {
    }
    
    
    
    public CategoryOrderResult(Category category, Long count) {
        super();
        this._category = category;
        this._count = count;
    }


    public Category getCategory() {
        return _category;
    }
    public void setCategory(Category category) {
        this._category = category;
    }
    public Long getCount() {
        return _count;
    }
    public void setCount(Long count) {
        this._count = count;
    }
    
    
}
