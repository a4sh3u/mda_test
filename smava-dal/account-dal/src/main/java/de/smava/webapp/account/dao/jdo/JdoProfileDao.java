//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(profile)}

import de.smava.webapp.account.dao.ProfileDao;
import de.smava.webapp.account.domain.Profile;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Profiles'.
 *
 * @author generator
 */
@Repository(value = "profileDao")
public class JdoProfileDao extends JdoBaseDao implements ProfileDao {

    private static final Logger LOGGER = Logger.getLogger(JdoProfileDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(profile)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the profile identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Profile load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfile - start: id=" + id);
        }
        Profile result = getEntity(Profile.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfile - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Profile getProfile(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Profile entity = findUniqueEntity(Profile.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the profile.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Profile profile) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveProfile: " + profile);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(profile)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(profile);
    }

    /**
     * @deprecated Use {@link #save(Profile) instead}
     */
    public Long saveProfile(Profile profile) {
        return save(profile);
    }

    /**
     * Deletes an profile, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the profile
     */
    public void deleteProfile(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteProfile: " + id);
        }
        deleteEntity(Profile.class, id);
    }

    /**
     * Retrieves all 'Profile' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Profile> getProfileList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - start");
        }
        Collection<Profile> result = getEntities(Profile.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Profile' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Profile> getProfileList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - start: pageable=" + pageable);
        }
        Collection<Profile> result = getEntities(Profile.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Profile' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Profile> getProfileList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - start: sortable=" + sortable);
        }
        Collection<Profile> result = getEntities(Profile.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Profile' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Profile> getProfileList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Profile> result = getEntities(Profile.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Profile' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Profile> findProfileList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - start: whereClause=" + whereClause);
        }
        Collection<Profile> result = findEntities(Profile.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Profile' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Profile> findProfileList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Profile> result = findEntities(Profile.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Profile' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Profile> findProfileList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Profile> result = findEntities(Profile.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Profile' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Profile> findProfileList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Profile> result = findEntities(Profile.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Profile' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Profile> findProfileList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Profile> result = findEntities(Profile.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Profile' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Profile> findProfileList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Profile> result = findEntities(Profile.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findProfileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Profile' instances.
     */
    public long getProfileCount() {
        long result = getEntityCount(Profile.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Profile' instances which match the given whereClause.
     */
    public long getProfileCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Profile.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Profile' instances which match the given whereClause together with params specified in object array.
     */
    public long getProfileCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Profile.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getProfileCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(profile)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
