package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.RdiPaymentEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * The domain object that represents 'RdiPayments'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractRdiPayment
    extends KreditPrivatEntity implements RdiPaymentEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRdiPayment.class);

    public static final String STATE_NEW = "new";
    public static final String STATE_DECLINED = "declined";
    public static final String STATE_ACCEPTED = "accepted";

    public static final String TYPE_LV = "LV";
    public static final String TYPE_AU = "AU";
    public static final String TYPE_AL = "AL";

    public boolean isExpired(Date atTime) {
        return getExpirationDate() != null && (getExpirationDate().equals(atTime) || getExpirationDate().before(atTime));
    }

    public boolean isExpired() {
        return isExpired(CurrentDate.getDate());
    }

    public boolean isStateAccepted() {
        return STATE_ACCEPTED.equals(getState());
    }

    public boolean isStateDeclined() {
        return STATE_DECLINED.equals(getState());
    }

    public boolean isStateNew() {
        return STATE_NEW.equals(getState());
    }

}

