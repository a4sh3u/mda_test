package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Document;
import de.smava.webapp.account.domain.DocumentAttachmentContainer;
import de.smava.webapp.account.domain.DocumentContainer;
import de.smava.webapp.account.domain.interfaces.AttachmentEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The domain object that represents 'Attachments'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractAttachment
    extends KreditPrivatEntity implements AttachmentEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAttachment.class);

    public List<? extends Entity> getModifier() {
        List<Account> accountList = new LinkedList<Account>();
        if (getDocument() != null) {
            accountList.add(getDocument().getOwner());
        }
        return accountList;
    }

}

