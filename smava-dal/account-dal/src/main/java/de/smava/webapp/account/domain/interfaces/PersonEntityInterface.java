package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.IdCard;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'Persons'.
 *
 * @author generator
 */
public interface PersonEntityInterface {

    /**
     * Setter for the property 'title'.
     *
     * 
     *
     */
    void setTitle(String title);

    /**
     * Returns the property 'title'.
     *
     * 
     *
     */
    String getTitle();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'salutation'.
     *
     * 
     *
     */
    void setSalutation(String salutation);

    /**
     * Returns the property 'salutation'.
     *
     * 
     *
     */
    String getSalutation();
    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    void setFirstName(String firstName);

    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    String getFirstName();
    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    void setLastName(String lastName);

    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    String getLastName();
    /**
     * Setter for the property 'birth name'.
     *
     * 
     *
     */
    void setBirthName(String birthName);

    /**
     * Returns the property 'birth name'.
     *
     * 
     *
     */
    String getBirthName();
    /**
     * Setter for the property 'date of birth'.
     *
     * 
     *
     */
    void setDateOfBirth(Date dateOfBirth);

    /**
     * Returns the property 'date of birth'.
     *
     * 
     *
     */
    Date getDateOfBirth();
    /**
     * Setter for the property 'country of birth'.
     *
     * 
     *
     */
    void setCountryOfBirth(String countryOfBirth);

    /**
     * Returns the property 'country of birth'.
     *
     * 
     *
     */
    String getCountryOfBirth();
    /**
     * Setter for the property 'place of birth'.
     *
     * 
     *
     */
    void setPlaceOfBirth(String placeOfBirth);

    /**
     * Returns the property 'place of birth'.
     *
     * 
     *
     */
    String getPlaceOfBirth();
    /**
     * Setter for the property 'citizenship'.
     *
     * 
     *
     */
    void setCitizenship(String citizenship);

    /**
     * Returns the property 'citizenship'.
     *
     * 
     *
     */
    String getCitizenship();
    /**
     * Setter for the property 'family status'.
     *
     * 
     *
     */
    void setFamilyStatus(String familyStatus);

    /**
     * Returns the property 'family status'.
     *
     * 
     *
     */
    String getFamilyStatus();
    /**
     * Setter for the property 'pesel'.
     *
     * 
     *
     */
    void setPesel(String pesel);

    /**
     * Returns the property 'pesel'.
     *
     * 
     *
     */
    String getPesel();
    /**
     * Setter for the property 'identity card number'.
     *
     * 
     *
     */
    void setIdentityCardNumber(String identityCardNumber);

    /**
     * Returns the property 'identity card number'.
     *
     * 
     *
     */
    String getIdentityCardNumber();
    /**
     * Setter for the property 'nip'.
     *
     * 
     *
     */
    void setNip(String nip);

    /**
     * Returns the property 'nip'.
     *
     * 
     *
     */
    String getNip();
    /**
     * Setter for the property 'education earned'.
     *
     * 
     *
     */
    void setEducationEarned(String educationEarned);

    /**
     * Returns the property 'education earned'.
     *
     * 
     *
     */
    String getEducationEarned();
    /**
     * Setter for the property 'id card history'.
     *
     * 
     *
     */
    void setIdCardHistory(Collection<IdCard> idCardHistory);

    /**
     * Returns the property 'id card history'.
     *
     * 
     *
     */
    Collection<IdCard> getIdCardHistory();
    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(int type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    int getType();
    /**
     * Setter for the property 'postident fee'.
     *
     * 
     *
     */
    void setPostidentFee(Date postidentFee);

    /**
     * Returns the property 'postident fee'.
     *
     * 
     *
     */
    Date getPostidentFee();
    /**
     * Setter for the property 'number of other in household check'.
     *
     * 
     *
     */
    void setNumberOfOtherInHouseholdCheck(Integer numberOfOtherInHouseholdCheck);

    /**
     * Returns the property 'number of other in household check'.
     *
     * 
     *
     */
    Integer getNumberOfOtherInHouseholdCheck();
    /**
     * Setter for the property 'confession'.
     *
     * 
     *
     */
    void setConfession(Integer confession);

    /**
     * Returns the property 'confession'.
     *
     * 
     *
     */
    Integer getConfession();
    /**
     * Setter for the property 'tax compliance act'.
     *
     * 
     *
     */
    void setTaxComplianceAct(Boolean taxComplianceAct);

    /**
     * Returns the property 'tax compliance act'.
     *
     * 
     *
     */
    Boolean getTaxComplianceAct();
    /**
     * Setter for the property 'taxes paid in usa'.
     *
     * 
     *
     */
    void setTaxesPaidInUsa(Boolean taxesPaidInUsa);

    /**
     * Returns the property 'taxes paid in usa'.
     *
     * 
     *
     */
    Boolean getTaxesPaidInUsa();

}
