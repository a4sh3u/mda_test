package de.smava.webapp.account.domain.interfaces;


import java.util.List;


/**
 * The domain object that represents 'InterestBorders'.
 *
 * @author generator
 */
public interface InterestBorderEntityInterface {

    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    String getRating();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'values'.
     *
     * 
     *
     */
    void setValues(List<Double> values);

    /**
     * Returns the property 'values'.
     *
     * 
     *
     */
    List<Double> getValues();
    /**
     * Setter for the property 'adjusted'.
     *
     * 
     *
     */
    void setAdjusted(Boolean adjusted);

    /**
     * Returns the property 'adjusted'.
     *
     * 
     *
     */
    Boolean getAdjusted();

}
