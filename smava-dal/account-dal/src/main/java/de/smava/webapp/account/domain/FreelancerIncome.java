package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.FreelancerIncomeHistory;

import java.util.Date;

/**
 * The domain object that represents 'FreelancerIncomes'.
 */
public class FreelancerIncome extends FreelancerIncomeHistory  implements FreelancerIncomeCalculation {

        protected Date _creationDate;
        protected Date _expirationDate;
        protected EconomicalData _economicalData;
        protected Integer _year;
        protected Integer _duration;
        protected Double _volume;
        protected Double _expenses;
        protected Double _tradeTax;
        protected Double _incomeTax;
        protected Double _solidarityTax;
        protected Double _churchTax;
        protected Double _periodEarningsBeforeTaxes;
        protected Double _annualEarningsBeforeTaxes;
        protected Double _annualProfitAfterTax;
        protected Double _monthlyNetIncome;
        protected Double _specialReserve;
        protected Double _specialReserveDissolution;
        protected Double _nonCashExpenses;
        protected Double _ownConsumption;
        protected Double _nonCashRevenues;

                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                            
    /**
     * Setter for the property 'economical data'.
     */
    public void setEconomicalData(EconomicalData economicalData) {
        _economicalData = economicalData;
    }
            
    /**
     * Returns the property 'economical data'.
     */
    public EconomicalData getEconomicalData() {
        return _economicalData;
    }
                                            
    /**
     * Setter for the property 'year'.
     */
    public void setYear(Integer year) {
        _year = year;
    }
            
    /**
     * Returns the property 'year'.
     */
    public Integer getYear() {
        return _year;
    }
                                            
    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration) {
        _duration = duration;
    }
            
    /**
     * Returns the property 'duration'.
     */
    public Integer getDuration() {
        return _duration;
    }
                                            
    /**
     * Setter for the property 'volume'.
     */
    public void setVolume(Double volume) {
        _volume = volume;
    }
            
    /**
     * Returns the property 'volume'.
     */
    public Double getVolume() {
        return _volume;
    }
                                            
    /**
     * Setter for the property 'expenses'.
     */
    public void setExpenses(Double expenses) {
        _expenses = expenses;
    }
            
    /**
     * Returns the property 'expenses'.
     */
    public Double getExpenses() {
        return _expenses;
    }
                                            
    /**
     * Setter for the property 'trade tax'.
     */
    public void setTradeTax(Double tradeTax) {
        _tradeTax = tradeTax;
    }
            
    /**
     * Returns the property 'trade tax'.
     */
    public Double getTradeTax() {
        return _tradeTax;
    }
                                            
    /**
     * Setter for the property 'income tax'.
     */
    public void setIncomeTax(Double incomeTax) {
        _incomeTax = incomeTax;
    }
            
    /**
     * Returns the property 'income tax'.
     */
    public Double getIncomeTax() {
        return _incomeTax;
    }
                                            
    /**
     * Setter for the property 'solidarity tax'.
     */
    public void setSolidarityTax(Double solidarityTax) {
        _solidarityTax = solidarityTax;
    }
            
    /**
     * Returns the property 'solidarity tax'.
     */
    public Double getSolidarityTax() {
        return _solidarityTax;
    }
                                            
    /**
     * Setter for the property 'church tax'.
     */
    public void setChurchTax(Double churchTax) {
        _churchTax = churchTax;
    }
            
    /**
     * Returns the property 'church tax'.
     */
    public Double getChurchTax() {
        return _churchTax;
    }
                                            
    /**
     * Setter for the property 'period earnings before taxes'.
     */
    public void setPeriodEarningsBeforeTaxes(Double periodEarningsBeforeTaxes) {
        _periodEarningsBeforeTaxes = periodEarningsBeforeTaxes;
    }
            
    /**
     * Returns the property 'period earnings before taxes'.
     */
    public Double getPeriodEarningsBeforeTaxes() {
        return _periodEarningsBeforeTaxes;
    }
                                            
    /**
     * Setter for the property 'annual earnings before taxes'.
     */
    public void setAnnualEarningsBeforeTaxes(Double annualEarningsBeforeTaxes) {
        _annualEarningsBeforeTaxes = annualEarningsBeforeTaxes;
    }
            
    /**
     * Returns the property 'annual earnings before taxes'.
     */
    public Double getAnnualEarningsBeforeTaxes() {
        return _annualEarningsBeforeTaxes;
    }
                                            
    /**
     * Setter for the property 'annual profit after tax'.
     */
    public void setAnnualProfitAfterTax(Double annualProfitAfterTax) {
        _annualProfitAfterTax = annualProfitAfterTax;
    }
            
    /**
     * Returns the property 'annual profit after tax'.
     */
    public Double getAnnualProfitAfterTax() {
        return _annualProfitAfterTax;
    }
                                            
    /**
     * Setter for the property 'monthly net income'.
     */
    public void setMonthlyNetIncome(Double monthlyNetIncome) {
        _monthlyNetIncome = monthlyNetIncome;
    }
            
    /**
     * Returns the property 'monthly net income'.
     */
    public Double getMonthlyNetIncome() {
        return _monthlyNetIncome;
    }
                                            
    /**
     * Setter for the property 'special reserve'.
     */
    public void setSpecialReserve(Double specialReserve) {
        _specialReserve = specialReserve;
    }
            
    /**
     * Returns the property 'special reserve'.
     */
    public Double getSpecialReserve() {
        return _specialReserve;
    }
                                            
    /**
     * Setter for the property 'special reserve dissolution'.
     */
    public void setSpecialReserveDissolution(Double specialReserveDissolution) {
        _specialReserveDissolution = specialReserveDissolution;
    }
            
    /**
     * Returns the property 'special reserve dissolution'.
     */
    public Double getSpecialReserveDissolution() {
        return _specialReserveDissolution;
    }
                                            
    /**
     * Setter for the property 'non cash expenses'.
     */
    public void setNonCashExpenses(Double nonCashExpenses) {
        _nonCashExpenses = nonCashExpenses;
    }
            
    /**
     * Returns the property 'non cash expenses'.
     */
    public Double getNonCashExpenses() {
        return _nonCashExpenses;
    }
                                            
    /**
     * Setter for the property 'own consumption'.
     */
    public void setOwnConsumption(Double ownConsumption) {
        _ownConsumption = ownConsumption;
    }
            
    /**
     * Returns the property 'own consumption'.
     */
    public Double getOwnConsumption() {
        return _ownConsumption;
    }
                                            
    /**
     * Setter for the property 'non cash revenues'.
     */
    public void setNonCashRevenues(Double nonCashRevenues) {
        _nonCashRevenues = nonCashRevenues;
    }
            
    /**
     * Returns the property 'non cash revenues'.
     */
    public Double getNonCashRevenues() {
        return _nonCashRevenues;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(FreelancerIncome.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(FreelancerIncome.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
