package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.CategoryImage;
import de.smava.webapp.account.domain.CategoryOrder;

import java.util.Collection;
import java.util.List;


/**
 * The domain object that represents 'Categorys'.
 *
 * @author generator
 */
public interface CategoryEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'parent'.
     *
     * 
     *
     */
    void setParent(Category parent);

    /**
     * Returns the property 'parent'.
     *
     * 
     *
     */
    Category getParent();
    /**
     * Setter for the property 'children'.
     *
     * 
     *
     */
    void setChildren(List<Category> children);

    /**
     * Returns the property 'children'.
     *
     * 
     *
     */
    List<Category> getChildren();
    /**
     * Setter for the property 'category orders'.
     *
     * 
     *
     */
    void setCategoryOrders(Collection<CategoryOrder> categoryOrders);

    /**
     * Returns the property 'category orders'.
     *
     * 
     *
     */
    Collection<CategoryOrder> getCategoryOrders();
    /**
     * Setter for the property 'images'.
     *
     * 
     *
     */
    void setImages(List<CategoryImage> images);

    /**
     * Returns the property 'images'.
     *
     * 
     *
     */
    List<CategoryImage> getImages();
    /**
     * Setter for the property 'shortlisted'.
     *
     * 
     *
     */
    void setShortlisted(boolean shortlisted);

    /**
     * Returns the property 'shortlisted'.
     *
     * 
     *
     */
    boolean getShortlisted();
    /**
     * Setter for the property 'image'.
     *
     * 
     *
     */
    void setImage(String image);

    /**
     * Returns the property 'image'.
     *
     * 
     *
     */
    String getImage();
    /**
     * Setter for the property 'image height'.
     *
     * 
     *
     */
    void setImageHeight(int imageHeight);

    /**
     * Returns the property 'image height'.
     *
     * 
     *
     */
    int getImageHeight();
    /**
     * Setter for the property 'image width'.
     *
     * 
     *
     */
    void setImageWidth(int imageWidth);

    /**
     * Returns the property 'image width'.
     *
     * 
     *
     */
    int getImageWidth();

}
