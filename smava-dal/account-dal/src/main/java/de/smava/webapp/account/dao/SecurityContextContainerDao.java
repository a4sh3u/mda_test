//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(security context container)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.exception.SmavaException;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.account.domain.SecurityContextContainer;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'SecurityContextContainers'.
 *
 * @author generator
 */
public interface SecurityContextContainerDao extends BaseDao<SecurityContextContainer> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the security context container identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    SecurityContextContainer getSecurityContextContainer(Long id);

    /**
     * Saves the security context container.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveSecurityContextContainer(SecurityContextContainer securityContextContainer);

    /**
     * Deletes an security context container, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the security context container
     */
    void deleteSecurityContextContainer(Long id);

    /**
     * Retrieves all 'SecurityContextContainer' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<SecurityContextContainer> getSecurityContextContainerList();

    /**
     * Retrieves a page of 'SecurityContextContainer' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<SecurityContextContainer> getSecurityContextContainerList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'SecurityContextContainer' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<SecurityContextContainer> getSecurityContextContainerList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'SecurityContextContainer' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<SecurityContextContainer> getSecurityContextContainerList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'SecurityContextContainer' instances.
     */
    long getSecurityContextContainerCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(security context container)}
    //
    SecurityContextContainer getBySessionId(String sessionId) throws Exception;
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}