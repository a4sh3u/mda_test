//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head2/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head2/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(insurance pool payout run)}

import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.InsurancePoolPayoutRun;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'InsurancePoolPayoutRuns'.
 *
 * @author generator
 */
public interface InsurancePoolPayoutRunDao extends BaseDao<InsurancePoolPayoutRun>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the insurance pool payout run identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    InsurancePoolPayoutRun getInsurancePoolPayoutRun(Long id);

    /**
     * Saves the insurance pool payout run.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveInsurancePoolPayoutRun(InsurancePoolPayoutRun insurancePoolPayoutRun);

    /**
     * Deletes an insurance pool payout run, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the insurance pool payout run
     */
    void deleteInsurancePoolPayoutRun(Long id);

    /**
     * Retrieves all 'InsurancePoolPayoutRun' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList();

    /**
     * Retrieves a page of 'InsurancePoolPayoutRun' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'InsurancePoolPayoutRun' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'InsurancePoolPayoutRun' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList(Pageable pageable, Sortable sortable);





    /**
     * Returns the number of 'InsurancePoolPayoutRun' instances.
     */
    long getInsurancePoolPayoutRunCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(insurance pool payout run)}
    //
    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRun(BookingGroup bookingGroup, Date endDate, Date startDate);
    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunOrdered( );

    Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRuns(BookingGroup bookingGroup);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
