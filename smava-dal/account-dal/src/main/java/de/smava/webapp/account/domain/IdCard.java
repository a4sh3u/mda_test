package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.IdCardHistory;

import java.util.Date;

/**
 * The domain object that represents 'IdCards'.
 */
public class IdCard extends IdCardHistory  {

        protected String _bkz;
        protected Date _issueDate;
        protected Date _expiryDate;
        protected Date _bkzCheck;
        protected Date _checksumCheck;
        protected Date _birthdateCheck;
        protected Date _expiryDateCheck;
        protected Date _addressCheck;
        protected Date _validityDate;
        protected Date _validUntil;
        protected Date _creationDate;
        protected Date _skippedDate;
        protected Person _person;
        protected Date _birthdayCheckIdcard;
        protected String _descriptions;

                            /**
     * Setter for the property 'bkz'.
     */
    public void setBkz(String bkz) {
        if (!_bkzIsSet) {
            _bkzIsSet = true;
            _bkzInitVal = getBkz();
        }
        registerChange("bkz", _bkzInitVal, bkz);
        _bkz = bkz;
    }
                        
    /**
     * Returns the property 'bkz'.
     */
    public String getBkz() {
        return _bkz;
    }
                                    /**
     * Setter for the property 'issue date'.
     */
    public void setIssueDate(Date issueDate) {
        if (!_issueDateIsSet) {
            _issueDateIsSet = true;
            _issueDateInitVal = getIssueDate();
        }
        registerChange("issue date", _issueDateInitVal, issueDate);
        _issueDate = issueDate;
    }
                        
    /**
     * Returns the property 'issue date'.
     */
    public Date getIssueDate() {
        return _issueDate;
    }
                                    /**
     * Setter for the property 'expiry date'.
     */
    public void setExpiryDate(Date expiryDate) {
        if (!_expiryDateIsSet) {
            _expiryDateIsSet = true;
            _expiryDateInitVal = getExpiryDate();
        }
        registerChange("expiry date", _expiryDateInitVal, expiryDate);
        _expiryDate = expiryDate;
    }
                        
    /**
     * Returns the property 'expiry date'.
     */
    public Date getExpiryDate() {
        return _expiryDate;
    }
                                    /**
     * Setter for the property 'bkz check'.
     */
    public void setBkzCheck(Date bkzCheck) {
        if (!_bkzCheckIsSet) {
            _bkzCheckIsSet = true;
            _bkzCheckInitVal = getBkzCheck();
        }
        registerChange("bkz check", _bkzCheckInitVal, bkzCheck);
        _bkzCheck = bkzCheck;
    }
                        
    /**
     * Returns the property 'bkz check'.
     */
    public Date getBkzCheck() {
        return _bkzCheck;
    }
                                    /**
     * Setter for the property 'checksum check'.
     */
    public void setChecksumCheck(Date checksumCheck) {
        if (!_checksumCheckIsSet) {
            _checksumCheckIsSet = true;
            _checksumCheckInitVal = getChecksumCheck();
        }
        registerChange("checksum check", _checksumCheckInitVal, checksumCheck);
        _checksumCheck = checksumCheck;
    }
                        
    /**
     * Returns the property 'checksum check'.
     */
    public Date getChecksumCheck() {
        return _checksumCheck;
    }
                                    /**
     * Setter for the property 'birthdate check'.
     */
    public void setBirthdateCheck(Date birthdateCheck) {
        if (!_birthdateCheckIsSet) {
            _birthdateCheckIsSet = true;
            _birthdateCheckInitVal = getBirthdateCheck();
        }
        registerChange("birthdate check", _birthdateCheckInitVal, birthdateCheck);
        _birthdateCheck = birthdateCheck;
    }
                        
    /**
     * Returns the property 'birthdate check'.
     */
    public Date getBirthdateCheck() {
        return _birthdateCheck;
    }
                                    /**
     * Setter for the property 'expiry date check'.
     */
    public void setExpiryDateCheck(Date expiryDateCheck) {
        if (!_expiryDateCheckIsSet) {
            _expiryDateCheckIsSet = true;
            _expiryDateCheckInitVal = getExpiryDateCheck();
        }
        registerChange("expiry date check", _expiryDateCheckInitVal, expiryDateCheck);
        _expiryDateCheck = expiryDateCheck;
    }
                        
    /**
     * Returns the property 'expiry date check'.
     */
    public Date getExpiryDateCheck() {
        return _expiryDateCheck;
    }
                                    /**
     * Setter for the property 'address check'.
     */
    public void setAddressCheck(Date addressCheck) {
        if (!_addressCheckIsSet) {
            _addressCheckIsSet = true;
            _addressCheckInitVal = getAddressCheck();
        }
        registerChange("address check", _addressCheckInitVal, addressCheck);
        _addressCheck = addressCheck;
    }
                        
    /**
     * Returns the property 'address check'.
     */
    public Date getAddressCheck() {
        return _addressCheck;
    }
                                    /**
     * Setter for the property 'validity date'.
     */
    public void setValidityDate(Date validityDate) {
        if (!_validityDateIsSet) {
            _validityDateIsSet = true;
            _validityDateInitVal = getValidityDate();
        }
        registerChange("validity date", _validityDateInitVal, validityDate);
        _validityDate = validityDate;
    }
                        
    /**
     * Returns the property 'validity date'.
     */
    public Date getValidityDate() {
        return _validityDate;
    }
                                    /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'skipped date'.
     */
    public void setSkippedDate(Date skippedDate) {
        if (!_skippedDateIsSet) {
            _skippedDateIsSet = true;
            _skippedDateInitVal = getSkippedDate();
        }
        registerChange("skipped date", _skippedDateInitVal, skippedDate);
        _skippedDate = skippedDate;
    }
                        
    /**
     * Returns the property 'skipped date'.
     */
    public Date getSkippedDate() {
        return _skippedDate;
    }
                                            
    /**
     * Setter for the property 'person'.
     */
    public void setPerson(Person person) {
        _person = person;
    }
            
    /**
     * Returns the property 'person'.
     */
    public Person getPerson() {
        return _person;
    }
                                    /**
     * Setter for the property 'birthday check idcard'.
     */
    public void setBirthdayCheckIdcard(Date birthdayCheckIdcard) {
        if (!_birthdayCheckIdcardIsSet) {
            _birthdayCheckIdcardIsSet = true;
            _birthdayCheckIdcardInitVal = getBirthdayCheckIdcard();
        }
        registerChange("birthday check idcard", _birthdayCheckIdcardInitVal, birthdayCheckIdcard);
        _birthdayCheckIdcard = birthdayCheckIdcard;
    }
                        
    /**
     * Returns the property 'birthday check idcard'.
     */
    public Date getBirthdayCheckIdcard() {
        return _birthdayCheckIdcard;
    }
                                    /**
     * Setter for the property 'descriptions'.
     */
    public void setDescriptions(String descriptions) {
        if (!_descriptionsIsSet) {
            _descriptionsIsSet = true;
            _descriptionsInitVal = getDescriptions();
        }
        registerChange("descriptions", _descriptionsInitVal, descriptions);
        _descriptions = descriptions;
    }
                        
    /**
     * Returns the property 'descriptions'.
     */
    public String getDescriptions() {
        return _descriptions;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdCard.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _bkz=").append(_bkz);
            builder.append("\n    _descriptions=").append(_descriptions);
            builder.append("\n}");
        } else {
            builder.append(IdCard.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
