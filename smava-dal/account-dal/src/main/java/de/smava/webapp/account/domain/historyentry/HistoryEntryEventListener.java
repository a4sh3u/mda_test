package de.smava.webapp.account.domain.historyentry;

import de.smava.webapp.account.dao.HistoryEntryDao;
import de.smava.webapp.account.dao.events.EntityEvent;
import de.smava.webapp.account.util.SecurityAccountHelper;
import de.smava.webapp.account.domain.history.HistoryEntry;
import de.smava.webapp.account.domain.history.HistoryEntryDetail;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Change;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import java.util.Map;

/**
 * This event listener creates history entries by listening to entity events.
 * Only such events are used for which an account id could be identified and
 * change data is present.
 *
 * @author Daniel Doubleday
 */
public class HistoryEntryEventListener implements ApplicationListener {
    private static final Logger LOGGER = Logger.getLogger(HistoryEntryEventListener.class);

    private HistoryEntryDao _historyEntryDao;
    /**
     * Handles ApplicationEvents which are (1) EntityEvents and (2) carry an entity
     * which is no HistoryEntry.
     */
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof EntityEvent) {
            EntityEvent entityEvent = (EntityEvent) event;
            KreditPrivatEntity entity = entityEvent.getEntity();
            // we don't create history entries for modification of history entries ...
            if (!(entity instanceof HistoryEntry)) {
                handleEvent(entity, entityEvent.getType());
            }
        }
    }

    /**
     * Dispatcher which creates entries bases on the entity type.
     */
    private void handleEvent(KreditPrivatEntity entity, EntityEvent.Type type) {
        for (Entity modifier : entity.getModifier()) {
            if (modifier != null) {
                createHistoryEntry(entity, type, modifier.getId());
            }
        }
    }

    /**
     * Creates a history entry if there is something to say: accountId must be set and
     * the change map of the entity must contain values or the change type must be DELETE.
     * After creating the history entry the changes map of the entity is cleared.
     */
    private void createHistoryEntry(Entity entity, EntityEvent.Type type, Long accountId) {
        HistoryEntry entry = new HistoryEntry();
        Map<String, Change> changeMap = entity.getChangeMap();
        if (accountId != null && (type == EntityEvent.Type.DELETED || changeMap.size() > 0)) {
            entry.setAccountId(accountId);
            entry.setEntityId(entity.getId());
            entry.setActionDate(CurrentDate.getDate());
            // the description will be a key in a ressource bundle
            entry.setDescription(entity.getClass().getName() + "." + type);
            entry.setActionType(translateActionType(type));
//            entry.setHistoryEntryDetailClass(SimpleUserAction.class.getName());
            entry.setPrincipalId(SecurityAccountHelper.getCurrentUserId());
//            SimpleUserAction detail = new SimpleUserAction();
//            detail.setChangeData(changeMap);
//            entry.setHistoryEntryDetail(detail);
            entry.setDetails(HistoryEntryDetail.buildHistoryEntryDetailsFromChangeMap(changeMap));
            _historyEntryDao.saveHistoryEntry(entry);
            entry.clearChanges();
        }
    }

    /**
     * Translates entity event types to history entry constants.
     */
    private String translateActionType(EntityEvent.Type type) {
        String result;
        switch (type) {
            case MODIFIED:
                result = HistoryEntry.ACTION_TYPE_MODIFIED;
                break;
            case CREATED:
                result = HistoryEntry.ACTION_TYPE_CREATED;
                break;
            case DELETED:
                result = HistoryEntry.ACTION_TYPE_DELETED;
                break;
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
        return result;
    }
}