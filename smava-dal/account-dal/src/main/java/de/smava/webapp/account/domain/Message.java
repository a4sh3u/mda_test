package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.MessageHistory;

import java.util.Date;

/**
 * The domain object that represents 'Messages'.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@messageId")
public class Message extends MessageHistory  {

        protected String _subject;
        protected String _text;
        protected String _folder;
        protected String _recipientEmail;
        protected boolean _read;
        protected Date _creationDate;
        protected Account _owner;
        protected Account _from;
        protected Account _recipient;

                            /**
     * Setter for the property 'subject'.
     */
    public void setSubject(String subject) {
        if (!_subjectIsSet) {
            _subjectIsSet = true;
            _subjectInitVal = getSubject();
        }
        registerChange("subject", _subjectInitVal, subject);
        _subject = subject;
    }
                        
    /**
     * Returns the property 'subject'.
     */
    public String getSubject() {
        return _subject;
    }
                                    /**
     * Setter for the property 'text'.
     */
    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = getText();
        }
        registerChange("text", _textInitVal, text);
        _text = text;
    }
                        
    /**
     * Returns the property 'text'.
     */
    public String getText() {
        return _text;
    }
                                    /**
     * Setter for the property 'folder'.
     */
    public void setFolder(String folder) {
        if (!_folderIsSet) {
            _folderIsSet = true;
            _folderInitVal = getFolder();
        }
        registerChange("folder", _folderInitVal, folder);
        _folder = folder;
    }
                        
    /**
     * Returns the property 'folder'.
     */
    public String getFolder() {
        return _folder;
    }
                                    /**
     * Setter for the property 'recipient email'.
     */
    public void setRecipientEmail(String recipientEmail) {
        if (!_recipientEmailIsSet) {
            _recipientEmailIsSet = true;
            _recipientEmailInitVal = getRecipientEmail();
        }
        registerChange("recipient email", _recipientEmailInitVal, recipientEmail);
        _recipientEmail = recipientEmail;
    }
                        
    /**
     * Returns the property 'recipient email'.
     */
    public String getRecipientEmail() {
        return _recipientEmail;
    }
                                    /**
     * Setter for the property 'read'.
     */
    public void setRead(boolean read) {
        if (!_readIsSet) {
            _readIsSet = true;
            _readInitVal = getRead();
        }
        registerChange("read", _readInitVal, read);
        _read = read;
    }
                        
    /**
     * Returns the property 'read'.
     */
    public boolean getRead() {
        return _read;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'owner'.
     */
    public void setOwner(Account owner) {
        _owner = owner;
    }
            
    /**
     * Returns the property 'owner'.
     */
    public Account getOwner() {
        return _owner;
    }
                                            
    /**
     * Setter for the property 'from'.
     */
    public void setFrom(Account from) {
        _from = from;
    }
            
    /**
     * Returns the property 'from'.
     */
    public Account getFrom() {
        return _from;
    }
                                            
    /**
     * Setter for the property 'recipient'.
     */
    public void setRecipient(Account recipient) {
        _recipient = recipient;
    }
            
    /**
     * Returns the property 'recipient'.
     */
    public Account getRecipient() {
        return _recipient;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Message.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _subject=").append(_subject);
            builder.append("\n    _text=").append(_text);
            builder.append("\n    _folder=").append(_folder);
            builder.append("\n    _recipientEmail=").append(_recipientEmail);
            builder.append("\n    _read=").append(_read);
            builder.append("\n}");
        } else {
            builder.append(Message.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
