/**
 * 
 */
package de.smava.webapp.account.todo.order;

import java.sql.Timestamp;


/**
 * @author bvoss
 *
 */
public class AdvancedOrderInfo extends OrderInfo {
	
	private int _matchCounts;
	private int _creditRateIndicator;
	private Timestamp _lastUpdate;
	private String _categoriesString;

	public int getMatchCounts() {
		return _matchCounts;
	}
	public void setMatchCounts(int matchCounts) {
		_matchCounts = matchCounts;
	}
	public int getCreditRateIndicator() {
		return _creditRateIndicator;
	}
	public void setCreditRateIndicator(int creditRateIndicator) {
		_creditRateIndicator = creditRateIndicator;
	}
	public Timestamp getLastUpdate() {
		return _lastUpdate;
	}
	public void setLastUpdate(Timestamp lastUpdate) {
		_lastUpdate = lastUpdate;
	}
	public String getCategoriesString() {
		return _categoriesString;
	}
	public void setCategoriesString(String categoriesString) {
		_categoriesString = categoriesString;
	}
}
