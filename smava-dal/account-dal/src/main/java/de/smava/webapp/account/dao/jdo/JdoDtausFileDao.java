//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dtaus file)}

import de.smava.webapp.account.dao.DtausFileDao;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.BookingType;
import de.smava.webapp.account.domain.DtausFile;
import de.smava.webapp.account.domain.Transaction;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'DtausFiles'.
 *
 * @author generator
 */
@Repository(value = "dtausFileDao")
public class JdoDtausFileDao extends JdoBaseDao implements DtausFileDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDtausFileDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(dtaus file)}
    private static final long serialVersionUID = -9064963187229680053L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the dtaus file identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public DtausFile load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFile - start: id=" + id);
        }
        DtausFile result = getEntity(DtausFile.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFile - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public DtausFile getDtausFile(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	DtausFile entity = findUniqueEntity(DtausFile.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the dtaus file.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(DtausFile dtausFile) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDtausFile: " + dtausFile);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(dtaus file)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(dtausFile);
    }

    /**
     * @deprecated Use {@link #save(DtausFile) instead}
     */
    public Long saveDtausFile(DtausFile dtausFile) {
        return save(dtausFile);
    }

    /**
     * Deletes an dtaus file, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the dtaus file
     */
    public void deleteDtausFile(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDtausFile: " + id);
        }
        deleteEntity(DtausFile.class, id);
    }

    /**
     * Retrieves all 'DtausFile' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<DtausFile> getDtausFileList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - start");
        }
        Collection<DtausFile> result = getEntities(DtausFile.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'DtausFile' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<DtausFile> getDtausFileList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - start: pageable=" + pageable);
        }
        Collection<DtausFile> result = getEntities(DtausFile.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DtausFile' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<DtausFile> getDtausFileList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - start: sortable=" + sortable);
        }
        Collection<DtausFile> result = getEntities(DtausFile.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DtausFile' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<DtausFile> getDtausFileList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DtausFile> result = getEntities(DtausFile.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DtausFile' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausFile> findDtausFileList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - start: whereClause=" + whereClause);
        }
        Collection<DtausFile> result = findEntities(DtausFile.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DtausFile' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<DtausFile> findDtausFileList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<DtausFile> result = findEntities(DtausFile.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'DtausFile' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausFile> findDtausFileList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<DtausFile> result = findEntities(DtausFile.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DtausFile' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausFile> findDtausFileList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<DtausFile> result = findEntities(DtausFile.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DtausFile' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausFile> findDtausFileList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DtausFile> result = findEntities(DtausFile.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DtausFile' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DtausFile> findDtausFileList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<DtausFile> result = findEntities(DtausFile.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDtausFileList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'DtausFile' instances.
     */
    public long getDtausFileCount() {
        long result = getEntityCount(DtausFile.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DtausFile' instances which match the given whereClause.
     */
    public long getDtausFileCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(DtausFile.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DtausFile' instances which match the given whereClause together with params specified in object array.
     */
    public long getDtausFileCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(DtausFile.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDtausFileCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(dtaus file)}
    //
    /**
     {@inheritDoc}
     */
    @Override
    public Collection<DtausFile> getLatestDraftDtausFiles() {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals("_direction", DtausFile.DIRECTION_OUT),
                term.or(term.equals("_state", DtausFile.STATE_OPEN),
                        term.equals("_state", DtausFile.STATE_PROCESSED)));
        term.setSortBy("_creationDate");
        term.setSortDescending(true);

        return findDtausFileList(term.toString());
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Map<Long, Double> getBookingSumForTransactionsInterimToInternal(BankAccount interimAccount, BookingType bookingType, Date date, DtausFile dtausFile) {
        return getBookingSumForTransactionsBetweenInterimAndInternal(interimAccount, bookingType, date, dtausFile, "getBookingSumForTransactionsInterimToInternal");
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Map<Long, Double> getBookingSumForTransactionsInternalToInterim(BankAccount interimAccount, BookingType bookingType, Date date, DtausFile dtausFile) {
        return getBookingSumForTransactionsBetweenInterimAndInternal(interimAccount, bookingType, date, dtausFile, "getBookingSumForTransactionsInternalToInterim");
    }
    
    private Map<Long, Double> getBookingSumForTransactionsBetweenInterimAndInternal(BankAccount interimAccount, BookingType bookingType, Date date, DtausFile dtausFile, String queryName) {
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, "SUCCESSFUL");
        params.put(2, interimAccount.getId());
        params.put(3, interimAccount.getId());
        params.put(4, bookingType.getDbValue());
        params.put(5, new java.sql.Date(date.getTime()));
        params.put(6, dtausFile.getId());
        
        Query query = getPersistenceManager().newNamedQuery(DtausFile.class, queryName);
        query.setResultClass(BookingSumForTransaction.class);
        
        Map<Long, Double> result = new HashMap<Long, Double>(); 
        
        @SuppressWarnings("unchecked")    
        Collection<BookingSumForTransaction> queryResult = (Collection<BookingSumForTransaction>) query.executeWithMap(params);
        for (BookingSumForTransaction bsft : queryResult) {
            result.put(bsft.getId(), bsft.getSum());
        }
        return result;
        
    }



    /**
     {@inheritDoc}
     */
    @Override
    public DtausFile getLatestDtausFileOutFromTransaction(Transaction transaction) {
    	return getLatestDtausFileOutFromTransaction(transaction.getId());
    }

    private DtausFile getLatestDtausFileOutFromTransaction(Long transactionId) {
    	Map<String, Object> params = new HashMap<String, Object>();
        params.put("transactionId", transactionId);

        Long id = executeNamedQuery(DtausFile.class, "getLatestDtausFileOutFromTransaction", params, Long.class, true);
        if (id != null) {
        	return load(id);
        } else {
        	return null;
        }
    }

    /**
     * contains result for query getBookingSumForTransactions.
     * 
     * @author rfiedler
     *
     */
    public static class BookingSumForTransaction {
        private Long _id = null;
        private Double _sum = null;
        
        public BookingSumForTransaction() {
            
        }
        public BookingSumForTransaction(Long id, Double sum) {
            this._id = id;
            this._sum = sum;
        }
        
        public Long getId() {
            return _id;
        }
        public void setId(Long id) {
            this._id = id;
        }
        public Double getSum() {
            return _sum;
        }
        public void setSum(Double sum) {
            this._sum = sum;
        }
        
    }


    public Collection<DtausFile> getLatestDraftDtausFilesOutOpen() {
        OqlTerm term = OqlTerm.newTerm();
        term.and(
                term.equals("_direction", DtausFile.DIRECTION_OUT),
                term.equals("_state", DtausFile.STATE_OPEN));
        term.setSortBy("_creationDate");
        term.setSortDescending(true);

        return this.findDtausFileList(term.toString());
    }

    public long getInOpenErrorDtausFileCount() {
        String whereClause = "(_state == :open || _state == :error) && _direction == :direction";
        return this.getDtausFileCount(whereClause, DtausFile.STATE_OPEN, DtausFile.STATE_ERROR, DtausFile.DIRECTION_IN);
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
