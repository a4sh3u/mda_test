//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rating)}
import de.smava.webapp.account.domain.history.RatingHistory;

import java.util.Map;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Ratings'.
 *
 * @author generator
 */
public class Rating extends RatingHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(rating)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected double _failureRate;
        protected double _groupBonusRate;
        protected Map<Integer, Double> _creditDiscounts;
        protected Map<Integer, Double> _interestReductions;
        
                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'failure rate'.
     */
    public void setFailureRate(double failureRate) {
        if (!_failureRateIsSet) {
            _failureRateIsSet = true;
            _failureRateInitVal = getFailureRate();
        }
        registerChange("failure rate", _failureRateInitVal, failureRate);
        _failureRate = failureRate;
    }
                        
    /**
     * Returns the property 'failure rate'.
     */
    public double getFailureRate() {
        return _failureRate;
    }
                                    /**
     * Setter for the property 'group bonus rate'.
     */
    public void setGroupBonusRate(double groupBonusRate) {
        if (!_groupBonusRateIsSet) {
            _groupBonusRateIsSet = true;
            _groupBonusRateInitVal = getGroupBonusRate();
        }
        registerChange("group bonus rate", _groupBonusRateInitVal, groupBonusRate);
        _groupBonusRate = groupBonusRate;
    }
                        
    /**
     * Returns the property 'group bonus rate'.
     */
    public double getGroupBonusRate() {
        return _groupBonusRate;
    }
                                    /**
     * Setter for the property 'credit discounts'.
     */
    public void setCreditDiscounts(Map<Integer, Double> creditDiscounts) {
        if (!_creditDiscountsIsSet) {
            _creditDiscountsIsSet = true;
            _creditDiscountsInitVal = getCreditDiscounts();
        }
        registerChange("credit discounts", _creditDiscountsInitVal, creditDiscounts);
        _creditDiscounts = creditDiscounts;
    }
                        
    /**
     * Returns the property 'credit discounts'.
     */
    public Map<Integer, Double> getCreditDiscounts() {
        return _creditDiscounts;
    }
                                    /**
     * Setter for the property 'interest reductions'.
     */
    public void setInterestReductions(Map<Integer, Double> interestReductions) {
        if (!_interestReductionsIsSet) {
            _interestReductionsIsSet = true;
            _interestReductionsInitVal = getInterestReductions();
        }
        registerChange("interest reductions", _interestReductionsInitVal, interestReductions);
        _interestReductions = interestReductions;
    }
                        
    /**
     * Returns the property 'interest reductions'.
     */
    public Map<Integer, Double> getInterestReductions() {
        return _interestReductions;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Rating.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _failureRate=").append(_failureRate);
            builder.append("\n    _groupBonusRate=").append(_groupBonusRate);
            builder.append("\n    _creditDiscounts=").append(_creditDiscounts);
            builder.append("\n    _interestReductions=").append(_interestReductions);
            builder.append("\n}");
        } else {
            builder.append(Rating.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Rating asRating() {
        return this;
    }
}
