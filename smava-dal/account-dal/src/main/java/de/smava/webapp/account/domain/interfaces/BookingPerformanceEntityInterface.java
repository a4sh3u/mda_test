package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'BookingPerformances'.
 *
 * @author generator
 */
public interface BookingPerformanceEntityInterface {

    /**
     * Setter for the property 'successful rate'.
     *
     * 
     *
     */
    void setSuccessfulRate(Long successfulRate);

    /**
     * Returns the property 'successful rate'.
     *
     * 
     *
     */
    Long getSuccessfulRate();
    /**
     * Setter for the property 'failed rate'.
     *
     * 
     *
     */
    void setFailedRate(Long failedRate);

    /**
     * Returns the property 'failed rate'.
     *
     * 
     *
     */
    Long getFailedRate();
    /**
     * Setter for the property 'delayed rate'.
     *
     * 
     *
     */
    void setDelayedRate(Long delayedRate);

    /**
     * Returns the property 'delayed rate'.
     *
     * 
     *
     */
    Long getDelayedRate();

}
