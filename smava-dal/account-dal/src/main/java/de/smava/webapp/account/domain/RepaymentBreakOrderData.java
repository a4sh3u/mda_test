/**
 * 
 */
package de.smava.webapp.account.domain;

import java.util.Date;
import java.util.SortedMap;

/**
 * @author Dimitar Robev
 *
 */
public class RepaymentBreakOrderData {

	private double _matchedAmount;
	private double _interest;
	private SortedMap<Date, RepaymentBreakMonthData> _monthData;

	public double getMatchedAmount() {
		return _matchedAmount;
	}
	public void setMatchedAmount(double matchedAmount) {
		_matchedAmount = matchedAmount;
	}
	public double getInterest() {
		return _interest;
	}
	public void setInterest(double interest) {
		_interest = interest;
	}
	public SortedMap<Date, RepaymentBreakMonthData> getMonthData() {
		return _monthData;
	}
	public void setMonthData(SortedMap<Date, RepaymentBreakMonthData> monthData) {
		_monthData = monthData;
	}
}
