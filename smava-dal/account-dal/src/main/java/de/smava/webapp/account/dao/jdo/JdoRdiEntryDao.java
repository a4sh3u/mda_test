//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi entry)}

import de.smava.webapp.account.domain.RdiEntry;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.account.dao.RdiEntryDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RdiEntrys'.
 *
 * @author generator
 */
@Repository(value = "rdiEntryDao")
public class JdoRdiEntryDao extends JdoBaseDao implements RdiEntryDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRdiEntryDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rdi entry)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the rdi entry identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RdiEntry load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntry - start: id=" + id);
        }
        RdiEntry result = getEntity(RdiEntry.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntry - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RdiEntry getRdiEntry(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RdiEntry entity = findUniqueEntity(RdiEntry.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the rdi entry.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RdiEntry rdiEntry) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRdiEntry: " + rdiEntry);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rdi entry)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(rdiEntry);
    }

    /**
     * @deprecated Use {@link #save(RdiEntry) instead}
     */
    public Long saveRdiEntry(RdiEntry rdiEntry) {
        return save(rdiEntry);
    }

    /**
     * Deletes an rdi entry, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi entry
     */
    public void deleteRdiEntry(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRdiEntry: " + id);
        }
        deleteEntity(RdiEntry.class, id);
    }

    /**
     * Retrieves all 'RdiEntry' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RdiEntry> getRdiEntryList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - start");
        }
        Collection<RdiEntry> result = getEntities(RdiEntry.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RdiEntry' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RdiEntry> getRdiEntryList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - start: pageable=" + pageable);
        }
        Collection<RdiEntry> result = getEntities(RdiEntry.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiEntry' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RdiEntry> getRdiEntryList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - start: sortable=" + sortable);
        }
        Collection<RdiEntry> result = getEntities(RdiEntry.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiEntry' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RdiEntry> getRdiEntryList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiEntry> result = getEntities(RdiEntry.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiEntry' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiEntry> findRdiEntryList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - start: whereClause=" + whereClause);
        }
        Collection<RdiEntry> result = findEntities(RdiEntry.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiEntry' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RdiEntry> findRdiEntryList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RdiEntry> result = findEntities(RdiEntry.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RdiEntry' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiEntry> findRdiEntryList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RdiEntry> result = findEntities(RdiEntry.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiEntry> findRdiEntryList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RdiEntry> result = findEntities(RdiEntry.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiEntry> findRdiEntryList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiEntry> result = findEntities(RdiEntry.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiEntry> findRdiEntryList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiEntry> result = findEntities(RdiEntry.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RdiEntry' instances.
     */
    public long getRdiEntryCount() {
        long result = getEntityCount(RdiEntry.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiEntry' instances which match the given whereClause.
     */
    public long getRdiEntryCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RdiEntry.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiEntry' instances which match the given whereClause together with params specified in object array.
     */
    public long getRdiEntryCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RdiEntry.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiEntryCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi entry)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
