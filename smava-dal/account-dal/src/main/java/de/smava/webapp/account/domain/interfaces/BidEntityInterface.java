package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.BidInterest;
import de.smava.webapp.account.domain.Contract;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'Bids'.
 *
 * @author generator
 */
public interface BidEntityInterface {

    /**
     * Setter for the property 'title'.
     *
     * 
     *
     */
    void setTitle(String title);

    /**
     * Returns the property 'title'.
     *
     * 
     *
     */
    String getTitle();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    double getAmount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'update date'.
     *
     * 
     *
     */
    void setUpdateDate(Date updateDate);

    /**
     * Returns the property 'update date'.
     *
     * 
     *
     */
    Date getUpdateDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'group id'.
     *
     * 
     *
     */
    void setGroupId(long groupId);

    /**
     * Returns the property 'group id'.
     *
     * 
     *
     */
    long getGroupId();
    /**
     * Setter for the property 'bid interests'.
     *
     * 
     *
     */
    void setBidInterests(Collection<BidInterest> bidInterests);

    /**
     * Returns the property 'bid interests'.
     *
     * 
     *
     */
    Collection<BidInterest> getBidInterests();
    /**
     * Setter for the property 'contract'.
     *
     * 
     *
     */
    void setContract(Contract contract);

    /**
     * Returns the property 'contract'.
     *
     * 
     *
     */
    Contract getContract();
    /**
     * Setter for the property 'last marketplace date'.
     *
     * 
     *
     */
    void setLastMarketplaceDate(Date lastMarketplaceDate);

    /**
     * Returns the property 'last marketplace date'.
     *
     * 
     *
     */
    Date getLastMarketplaceDate();

}
