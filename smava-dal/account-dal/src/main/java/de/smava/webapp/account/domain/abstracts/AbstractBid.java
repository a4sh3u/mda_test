package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.Bid;
import de.smava.webapp.account.domain.BidInterest;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.domain.interfaces.BidEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'Bids'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBid
    extends KreditPrivatEntity implements BidEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBid.class);

    private static final long serialVersionUID = 6661479567860241735L;

    public static final String STATE_QUEUE = "QUEUE"; // new in the system, not touched by the matching algorithm
    public static final String STATE_NEW = "NEW"; // This state indicates bids that are created by limited lenders and borrowers
    public static final String STATE_OPEN = "OPEN"; // touched, but not matched yet
    public static final String STATE_MATCHED = "MATCHED"; // matched, linked with an open contract
    public static final String STATE_ARCHIVE = "ARCHIVED"; // closed and archived, linked with a closed contract
    public static final String STATE_DELETED = "DELETED"; // deleted, not used anywhere
    public static final String STATE_EXPIRED = "EXPIRED"; // This state indicates orders that are outdated but not fully matched
    public static final String STATE_DEACTIVATED = "DEACTIVATED";  // ...
    public static final String STATE_OBSOLETE = "OBSOLETE"; // This state indicates open orders with partial matches that are refused by the borrower
    public static final String STATE_SUCCESSFUL = "SUCCESSFUL"; // This state indicates offers that are related to a contract which has entered the payout lifecycle.
    public static final String STATE_BROKERAGE = "BROKERAGE";//This state identifies an Brokerge offer that gets passed to other banks and should not be considered anywhere in our system
    public static final String STATE_DENIED = "DENIED";//This state identifies that an order is ultimately(!) denied

    public static final Collection<String> STATES = ConstCollector.getAll("STATE_");

    public static final Collection<String> OPEN_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[]{STATE_OPEN, STATE_QUEUE}));
    public static final Collection<String> RUNNING_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[] {STATE_MATCHED}));
    public static final Collection<String> ENDED_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[] {STATE_MATCHED, STATE_EXPIRED, STATE_DELETED, STATE_OBSOLETE}));
    public static final Collection<String> PREPARED_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[] {Order.STATE_DEACTIVATED, Order.STATE_NEW}));

    public Map<String, Float> getRatesForMarkets() {
        Map<String, Float> result = new HashMap<String, Float>();

        Collection<BidInterest> bidInterests = getBidInterests();
        if (bidInterests == null) {
            LOGGER.error("");
            bidInterests = Collections.emptySet();
        }
        for (BidInterest bidInterest : bidInterests) {
            if (bidInterest.getValidUntil() == null) {
                result.put(bidInterest.getMarketName(), bidInterest.getRate());
            }
        }

        return result;
    }

    public void removeRateForMarket(String marketName) {
        Date now = CurrentDate.getDate();
        Collection<BidInterest> bidInterests = getBidInterests();
        if (bidInterests != null) {
            for (BidInterest bidInterest : bidInterests) {
                if (bidInterest.getMarketName().equals(marketName) && bidInterest.getValidUntil() == null) {
                    bidInterest.setValidUntil(now);
                    break;
                }
            }
        }
    }

    public BidInterest getCurrentBidInterest() {
        BidInterest currentBidInterest = null;
        if (getBidInterests() != null) {
            for (BidInterest bidInterest : getBidInterests()) {
                if (bidInterest.getValidUntil() == null) {
                    currentBidInterest = bidInterest;
                    break;
                }
            }
        }

        return currentBidInterest;
    }

    public boolean isStateDeactivated() {
        return STATE_DEACTIVATED.equals(getState());
    }

    public boolean isStateExpired() {
        return STATE_EXPIRED.equals(getState());
    }

    public boolean isStateMatched() {
        return STATE_MATCHED.equals(getState());
    }

    public boolean isStateDeleted() {
        return STATE_DELETED.equals(getState());
    }

    public boolean isStateObsolete() {
        return STATE_OBSOLETE.equals(getState());
    }

    public boolean isStateOpen() {
        return STATE_OPEN.equals(getState());
    }

    public boolean isStateQueued() {
        return STATE_QUEUE.equals(getState());
    }

    public boolean isStateNew() {
        return STATE_NEW.equals(getState());
    }

    public boolean hasContract() {
        return getContract() != null;
    }

    protected boolean isMasterOfGroup() {
        final Long id = getId();
        return id == null || id.longValue() == getGroupId();
    }

    /**
     * Returns the property 'bid interests'.
     */
    public Collection<BidInterest> getActiveBidInterests() {
        Collection<BidInterest> result = new ArrayList<BidInterest>();
        for (BidInterest bidInterest : getBidInterests()) {
            if (bidInterest.getValidUntil() == null) {
                result.add(bidInterest);
            }
        }
        return result;
    }

    public void putRateForMarket(String marketName, Float rate, boolean invalidateAllPreviousInterests) {
        boolean isUnequal = false;
        Date now = CurrentDate.getDate();

        String duration = marketName.substring(1);
        String rating = marketName.substring(0, 1);

        if (getBidInterests() == null) {
            setBidInterests(new ArrayList<BidInterest>());
        }

        Collection<BidInterest> bidInterests = getBidInterests();

        for (BidInterest bidInterest : bidInterests) {
            if (!invalidateAllPreviousInterests) {
                if (bidInterest.getMarketName().equals(marketName) && bidInterest.getValidUntil() == null) {
                    // checks rate in case of multiple calls (e.g. at edit order -> update contracts interest):
                    if (!bidInterest.getRate().equals(rate)) {
                        bidInterest.setValidUntil(now);
                        isUnequal = true;
                    }

                    break;
                }
            } else {
                if (!(bidInterest.getRate().equals(rate)
                        && bidInterest.getMarketName().substring(1).equals(duration)
                        && bidInterest.getMarketName().substring(0, 1).equals(rating))
                        && bidInterest.getValidUntil() == null) {
                    bidInterest.setValidUntil(now);
                    isUnequal = true;
                }
            }
        }

        if (isUnequal || bidInterests.isEmpty()) {
            BidInterest newBidInterest = new BidInterest();
            newBidInterest.setBid(this.instance());
            newBidInterest.setCreationDate(now);
            newBidInterest.setMarketName(marketName);
            newBidInterest.setRate(rate);
            getBidInterests().add(newBidInterest);
        }
    }

    /**
     * Creates a new BidInterest and invalidates all other bidInterests without checking the market.
     */
    public void putRateForMarket(String marketName, Float rate) {
        putRateForMarket(marketName, rate, true);
    }

    /**
     * Only used in test sources.
     */
    public void setRatesForMarkets(Map<String, Float> ratesForMarkets) {

        setBidInterests(new ArrayList<BidInterest>());

        for (Map.Entry<String, Float> entry : ratesForMarkets.entrySet()) {
            putRateForMarket(entry.getKey(), entry.getValue());
        }
    }

    public abstract Bid instance();

}

