package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractSchufaScore;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'SchufaScores'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SchufaScoreHistory extends AbstractSchufaScore {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient int _scoreInitVal;
    protected transient boolean _scoreIsSet;
    protected transient String _ratingInitVal;
    protected transient boolean _ratingIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _remarkInitVal;
    protected transient boolean _remarkIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _versionInitVal;
    protected transient boolean _versionIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'score'.
     */
    public int scoreInitVal() {
        int result;
        if (_scoreIsSet) {
            result = _scoreInitVal;
        } else {
            result = getScore();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'score'.
     */
    public boolean scoreIsDirty() {
        return !valuesAreEqual(scoreInitVal(), getScore());
    }

    /**
     * Returns true if the setter method was called for the property 'score'.
     */
    public boolean scoreIsSet() {
        return _scoreIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rating'.
     */
    public String ratingInitVal() {
        String result;
        if (_ratingIsSet) {
            result = _ratingInitVal;
        } else {
            result = getRating();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rating'.
     */
    public boolean ratingIsDirty() {
        return !valuesAreEqual(ratingInitVal(), getRating());
    }

    /**
     * Returns true if the setter method was called for the property 'rating'.
     */
    public boolean ratingIsSet() {
        return _ratingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'remark'.
     */
    public String remarkInitVal() {
        String result;
        if (_remarkIsSet) {
            result = _remarkInitVal;
        } else {
            result = getRemark();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'remark'.
     */
    public boolean remarkIsDirty() {
        return !valuesAreEqual(remarkInitVal(), getRemark());
    }

    /**
     * Returns true if the setter method was called for the property 'remark'.
     */
    public boolean remarkIsSet() {
        return _remarkIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'version'.
     */
    public String versionInitVal() {
        String result;
        if (_versionIsSet) {
            result = _versionInitVal;
        } else {
            result = getVersion();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'version'.
     */
    public boolean versionIsDirty() {
        return !valuesAreEqual(versionInitVal(), getVersion());
    }

    /**
     * Returns true if the setter method was called for the property 'version'.
     */
    public boolean versionIsSet() {
        return _versionIsSet;
    }
	
}
