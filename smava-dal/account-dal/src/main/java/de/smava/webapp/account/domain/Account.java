package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.AccountHistory;

import java.util.*;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@accountId")
public class Account extends AccountHistory  implements MarketingTrackingEntity {

    protected Long _accountId;
    protected Date _creationDate;
    protected Collection<Person> _personHistory;
    protected Credentials _credentials;
    protected String _state;
    protected Date _lockDate;
    protected String _username;
    protected String _email;
    protected String _phone;
    protected String _phoneCode;
    protected String _phone2;
    protected String _phone2Code;
    protected Date _assetManagerGuidelineDate;
    protected Date _acceptanceDate;
    protected Date _lastLogin;
    protected boolean _newsletterSubscription;
    protected Collection<AccountRole> _accountRoles;
    protected Set<Order> _orders;
    protected Set<Offer> _offers;
    protected Profile _profile;
    protected List<BankAccount> _bankAccounts;
    protected List<Order> _markedOrders;
    protected List<Document> _documents;
    protected List<Message> _messages;
    protected MessageConfig _messageConfig;
    protected List<MailLink> _mailLinks;
    protected Account _invitedBy;
    protected List<AccountAddress> _addresses;
    protected Long _marketingPlacementId;
    protected List<CreditScore> _creditScores;
    protected CreditScore _currentCreditScore;
    protected List<EconomicalData> _economicalDataHistory;
    protected String _preferredRole;
    protected List<OrderRecommendation> _orderRecommendations;
    protected Set<RepaymentBreak> _repaymentBreaks;
    protected String _externalReferenceId;
    protected Collection<BidSearch> _bidSearches;
    protected Collection<CustomerAgreement> _customerAgreements;
    protected BookingPerformance _bookingPerformance;
    protected Date _paymentProfilShowDate;
    protected double _averageUpdatedRoi;
    protected boolean _isManaged;
    protected String _activationString;
    protected String _marketingTest;
    protected Date _callAgreementDate;
    protected Double _lastRequestedAmount;
    protected String _phoneEmployer;
    protected String _phoneCodeEmployer;
    protected String _phoneEmployerCoBorrower;
    protected String _phoneCodeEmployerCoBorrower;
    protected Date _lastFrontendChanges;
    protected Double _brokerageNetIncome;
    protected Double _brokerageDisposableIncome;
    protected Date _emailValidatedDate;
    protected String _payoutMode;
    protected Date _papLeadTracked;
    protected Long _personalAdvisorId;
    protected Date _receiveLoanOffers;
    protected Date _receiveConsolidationOffers;

    public Account() {
        setState(USER_STATE_DEACTIVATED);
        setAccountRoles(new HashSet<AccountRole>());
        setCustomerAgreements(new HashSet<CustomerAgreement>());
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {

        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = getEmail();
        }

        registerChange("email", _emailInitVal, email);

        _email = email;
        if (getCredentials() != null) {
            getCredentials().setIdentificationName(getEmail());
        }
    }

    /**
     *
     * @deprecated Use setCustomerNumber(...) instead
     * @param accountId customer number
     */
    @Deprecated
    public void setAccountId(Long accountId) {
        _accountId = accountId;
    }

    /**
     * @deprecated Use getCustomerNumber() instead
     * @return requested customer number
     */
    @Deprecated
    public Long getAccountId() {
        return _accountId;
    }

    public void setCustomerNumber(Long customerNumber) {
        _accountId = customerNumber;
    }

    public Long getCustomerNumber() {
        return _accountId;
    }

    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setPersonHistory(Collection<Person> personHistory) {
        _personHistory = personHistory;
    }

    public Collection<Person> getPersonHistory() {
        return _personHistory;
    }

    public void setCredentials(Credentials credentials) {
        _credentials = credentials;
    }

    public Credentials getCredentials() {
        return _credentials;
    }

    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public void setLockDate(Date lockDate) {
        if (!_lockDateIsSet) {
            _lockDateIsSet = true;
            _lockDateInitVal = getLockDate();
        }
        registerChange("lock date", _lockDateInitVal, lockDate);
        _lockDate = lockDate;
    }

    public Date getLockDate() {
        return _lockDate;
    }

    public void setUsername(String username) {
        if (!_usernameIsSet) {
            _usernameIsSet = true;
            _usernameInitVal = getUsername();
        }
        registerChange("username", _usernameInitVal, username);
        _username = username;
    }

    public String getUsername() {
        return _username;
    }

    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = getPhone();
        }
        registerChange("phone", _phoneInitVal, phone);
        _phone = phone;
    }

    public String getPhone() {
        return _phone;
    }

    public void setPhoneCode(String phoneCode) {
        if (!_phoneCodeIsSet) {
            _phoneCodeIsSet = true;
            _phoneCodeInitVal = getPhoneCode();
        }
        registerChange("phone code", _phoneCodeInitVal, phoneCode);
        _phoneCode = phoneCode;
    }

    public String getPhoneCode() {
        return _phoneCode;
    }

    public void setPhone2(String phone2) {
        if (!_phone2IsSet) {
            _phone2IsSet = true;
            _phone2InitVal = getPhone2();
        }
        registerChange("phone2", _phone2InitVal, phone2);
        _phone2 = phone2;
    }

    public String getPhone2() {
        return _phone2;
    }

    public void setPhone2Code(String phone2Code) {
        if (!_phone2CodeIsSet) {
            _phone2CodeIsSet = true;
            _phone2CodeInitVal = getPhone2Code();
        }
        registerChange("phone2 code", _phone2CodeInitVal, phone2Code);
        _phone2Code = phone2Code;
    }

    public String getPhone2Code() {
        return _phone2Code;
    }

    public void setAssetManagerGuidelineDate(Date assetManagerGuidelineDate) {
        if (!_assetManagerGuidelineDateIsSet) {
            _assetManagerGuidelineDateIsSet = true;
            _assetManagerGuidelineDateInitVal = getAssetManagerGuidelineDate();
        }
        registerChange("asset manager guideline date", _assetManagerGuidelineDateInitVal, assetManagerGuidelineDate);
        _assetManagerGuidelineDate = assetManagerGuidelineDate;
    }

    public Date getAssetManagerGuidelineDate() {
        return _assetManagerGuidelineDate;
    }

    public void setAcceptanceDate(Date acceptanceDate) {
        if (!_acceptanceDateIsSet) {
            _acceptanceDateIsSet = true;
            _acceptanceDateInitVal = getAcceptanceDate();
        }
        registerChange("acceptance date", _acceptanceDateInitVal, acceptanceDate);
        _acceptanceDate = acceptanceDate;
    }

    public Date getAcceptanceDate() {
        return _acceptanceDate;
    }

    public void setLastLogin(Date lastLogin) {
        if (!_lastLoginIsSet) {
            _lastLoginIsSet = true;
            _lastLoginInitVal = getLastLogin();
        }
        registerChange("last login", _lastLoginInitVal, lastLogin);
        _lastLogin = lastLogin;
    }

    public Date getLastLogin() {
        return _lastLogin;
    }

    public void setNewsletterSubscription(boolean newsletterSubscription) {
        if (!_newsletterSubscriptionIsSet) {
            _newsletterSubscriptionIsSet = true;
            _newsletterSubscriptionInitVal = getNewsletterSubscription();
        }
        registerChange("newsletter subscription", _newsletterSubscriptionInitVal, newsletterSubscription);
        _newsletterSubscription = newsletterSubscription;
    }

    public boolean getNewsletterSubscription() {
        return _newsletterSubscription;
    }

    public void setAccountRoles(Collection<AccountRole> accountRoles) {
        _accountRoles = accountRoles;
    }

    public Collection<AccountRole> getAccountRoles() {
        return _accountRoles;
    }

    public void setOrders(Set<Order> orders) {
        _orders = orders;
    }

    public Set<Order> getOrders() {
        return _orders;
    }
                                            
    public void setOffers(Set<Offer> offers) {
        _offers = offers;
    }
            
    public Set<Offer> getOffers() {
        return _offers;
    }
                                            
    public void setProfile(Profile profile) {
        _profile = profile;
    }
            
    public Profile getProfile() {
        return _profile;
    }
                                            
    public void setBankAccounts(List<BankAccount> bankAccounts) {
        _bankAccounts = bankAccounts;
    }
            
    public List<BankAccount> getBankAccounts() {
        return _bankAccounts;
    }
                                            
    public void setMarkedOrders(List<Order> markedOrders) {
        _markedOrders = markedOrders;
    }
            
    public List<Order> getMarkedOrders() {
        return _markedOrders;
    }
                                            
    public void setDocuments(List<Document> documents) {
        _documents = documents;
    }

    public List<Document> getDocuments() {
        return _documents;
    }
                                            
    public void setMessages(List<Message> messages) {
        _messages = messages;
    }
            
    public List<Message> getMessages() {
        return _messages;
    }
                                            
    public void setMessageConfig(MessageConfig messageConfig) {
        _messageConfig = messageConfig;
    }
            
    public MessageConfig getMessageConfig() {
        return _messageConfig;
    }

    public void setMailLinks(List<MailLink> mailLinks) {
        _mailLinks = mailLinks;
    }
            
    public List<MailLink> getMailLinks() {
        return _mailLinks;
    }

    public void setInvitedBy(Account invitedBy) {
        _invitedBy = invitedBy;
    }
            
    public Account getInvitedBy() {
        return _invitedBy;
    }
                                            
    public void setAddresses(List<AccountAddress> addresses) {
        _addresses = addresses;
    }
            
    public List<AccountAddress> getAddresses() {
        return _addresses;
    }

    public void setMarketingPlacementId(Long marketingPlacementId) {
        if (!_marketingPlacementIdIsSet) {
            _marketingPlacementIdIsSet = true;
            _marketingPlacementIdInitVal = getMarketingPlacementId();
        }
        registerChange("marketing placement id", _marketingPlacementIdInitVal, marketingPlacementId);
        _marketingPlacementId = marketingPlacementId;
    }

    public Long getMarketingPlacementId() {
        return _marketingPlacementId;
    }
                                            
    public void setCreditScores(List<CreditScore> creditScores) {
        _creditScores = creditScores;
    }
            
    public List<CreditScore> getCreditScores() {
        return _creditScores;
    }
                                            
    public void setCurrentCreditScore(CreditScore currentCreditScore) {
        _currentCreditScore = currentCreditScore;
    }
            
    public CreditScore getCurrentCreditScore() {
        return _currentCreditScore;
    }
                                            
    public void setEconomicalDataHistory(List<EconomicalData> economicalDataHistory) {
        _economicalDataHistory = economicalDataHistory;
    }
            
    public List<EconomicalData> getEconomicalDataHistory() {
        return _economicalDataHistory;
    }

    public void setPreferredRole(String preferredRole) {
        if (!_preferredRoleIsSet) {
            _preferredRoleIsSet = true;
            _preferredRoleInitVal = getPreferredRole();
        }
        registerChange("preferred role", _preferredRoleInitVal, preferredRole);
        _preferredRole = preferredRole;
    }
                        
    public String getPreferredRole() {
        return _preferredRole;
    }
                                            
    public void setOrderRecommendations(List<OrderRecommendation> orderRecommendations) {
        _orderRecommendations = orderRecommendations;
    }
            
    public List<OrderRecommendation> getOrderRecommendations() {
        return _orderRecommendations;
    }
                                            
    public void setRepaymentBreaks(Set<RepaymentBreak> repaymentBreaks) {
        _repaymentBreaks = repaymentBreaks;
    }

    public Set<RepaymentBreak> getRepaymentBreaks() {
        return _repaymentBreaks;
    }

    public void setExternalReferenceId(String externalReferenceId) {
        if (!_externalReferenceIdIsSet) {
            _externalReferenceIdIsSet = true;
            _externalReferenceIdInitVal = getExternalReferenceId();
        }
        registerChange("external reference id", _externalReferenceIdInitVal, externalReferenceId);
        _externalReferenceId = externalReferenceId;
    }

    public String getExternalReferenceId() {
        return _externalReferenceId;
    }

    public void setBidSearches(Collection<BidSearch> bidSearches) {
        _bidSearches = bidSearches;
    }
            
    public Collection<BidSearch> getBidSearches() {
        return _bidSearches;
    }
                                            
    public void setCustomerAgreements(Collection<CustomerAgreement> customerAgreements) {
        _customerAgreements = customerAgreements;
    }
            
    public Collection<CustomerAgreement> getCustomerAgreements() {
        return _customerAgreements;
    }
                                            
    public void setBookingPerformance(BookingPerformance bookingPerformance) {
        _bookingPerformance = bookingPerformance;
    }
            
    public BookingPerformance getBookingPerformance() {
        return _bookingPerformance;
    }

    public void setPaymentProfilShowDate(Date paymentProfilShowDate) {
        if (!_paymentProfilShowDateIsSet) {
            _paymentProfilShowDateIsSet = true;
            _paymentProfilShowDateInitVal = getPaymentProfilShowDate();
        }
        registerChange("payment profil show date", _paymentProfilShowDateInitVal, paymentProfilShowDate);
        _paymentProfilShowDate = paymentProfilShowDate;
    }
                        
    public Date getPaymentProfilShowDate() {
        return _paymentProfilShowDate;
    }

    public void setAverageUpdatedRoi(double averageUpdatedRoi) {
        if (!_averageUpdatedRoiIsSet) {
            _averageUpdatedRoiIsSet = true;
            _averageUpdatedRoiInitVal = getAverageUpdatedRoi();
        }
        registerChange("average updated roi", _averageUpdatedRoiInitVal, averageUpdatedRoi);
        _averageUpdatedRoi = averageUpdatedRoi;
    }

    public double getAverageUpdatedRoi() {
        return _averageUpdatedRoi;
    }

    public void setIsManaged(boolean isManaged) {
        if (!_isManagedIsSet) {
            _isManagedIsSet = true;
            _isManagedInitVal = getIsManaged();
        }
        registerChange("is managed", _isManagedInitVal, isManaged);
        _isManaged = isManaged;
    }
                        
    public boolean getIsManaged() {
        return _isManaged;
    }

    public void setActivationString(String activationString) {
        if (!_activationStringIsSet) {
            _activationStringIsSet = true;
            _activationStringInitVal = getActivationString();
        }
        registerChange("activation string", _activationStringInitVal, activationString);
        _activationString = activationString;
    }
                        
    public String getActivationString() {
        return _activationString;
    }

    public void setMarketingTest(String marketingTest) {
        if (!_marketingTestIsSet) {
            _marketingTestIsSet = true;
            _marketingTestInitVal = getMarketingTest();
        }
        registerChange("marketing test", _marketingTestInitVal, marketingTest);
        _marketingTest = marketingTest;
    }

    public String getMarketingTest() {
        return _marketingTest;
    }

    public void setCallAgreementDate(Date callAgreementDate) {
        if (!_callAgreementDateIsSet) {
            _callAgreementDateIsSet = true;
            _callAgreementDateInitVal = getCallAgreementDate();
        }
        registerChange("call agreement date", _callAgreementDateInitVal, callAgreementDate);
        _callAgreementDate = callAgreementDate;
    }
                        
    public Date getCallAgreementDate() {
        return _callAgreementDate;
    }
                                            
    public void setLastRequestedAmount(Double lastRequestedAmount) {
        _lastRequestedAmount = lastRequestedAmount;
    }
            
    public Double getLastRequestedAmount() {
        return _lastRequestedAmount;
    }

    public void setPhoneEmployer(String phoneEmployer) {
        if (!_phoneEmployerIsSet) {
            _phoneEmployerIsSet = true;
            _phoneEmployerInitVal = getPhoneEmployer();
        }
        registerChange("phone employer", _phoneEmployerInitVal, phoneEmployer);
        _phoneEmployer = phoneEmployer;
    }

    public String getPhoneEmployer() {
        return _phoneEmployer;
    }

    public void setPhoneCodeEmployer(String phoneCodeEmployer) {
        if (!_phoneCodeEmployerIsSet) {
            _phoneCodeEmployerIsSet = true;
            _phoneCodeEmployerInitVal = getPhoneCodeEmployer();
        }
        registerChange("phone code employer", _phoneCodeEmployerInitVal, phoneCodeEmployer);
        _phoneCodeEmployer = phoneCodeEmployer;
    }

    public String getPhoneCodeEmployer() {
        return _phoneCodeEmployer;
    }

    public void setPhoneEmployerCoBorrower(String phoneEmployerCoBorrower) {
        if (!_phoneEmployerCoBorrowerIsSet) {
            _phoneEmployerCoBorrowerIsSet = true;
            _phoneEmployerCoBorrowerInitVal = getPhoneEmployerCoBorrower();
        }
        registerChange("phone employer co borrower", _phoneEmployerCoBorrowerInitVal, phoneEmployerCoBorrower);
        _phoneEmployerCoBorrower = phoneEmployerCoBorrower;
    }

    public String getPhoneEmployerCoBorrower() {
        return _phoneEmployerCoBorrower;
    }

    public void setPhoneCodeEmployerCoBorrower(String phoneCodeEmployerCoBorrower) {
        if (!_phoneCodeEmployerCoBorrowerIsSet) {
            _phoneCodeEmployerCoBorrowerIsSet = true;
            _phoneCodeEmployerCoBorrowerInitVal = getPhoneCodeEmployerCoBorrower();
        }
        registerChange("phone code employer co borrower", _phoneCodeEmployerCoBorrowerInitVal, phoneCodeEmployerCoBorrower);
        _phoneCodeEmployerCoBorrower = phoneCodeEmployerCoBorrower;
    }

    public String getPhoneCodeEmployerCoBorrower() {
        return _phoneCodeEmployerCoBorrower;
    }

    public void setLastFrontendChanges(Date lastFrontendChanges) {
        if (!_lastFrontendChangesIsSet) {
            _lastFrontendChangesIsSet = true;
            _lastFrontendChangesInitVal = getLastFrontendChanges();
        }
        registerChange("last frontend changes", _lastFrontendChangesInitVal, lastFrontendChanges);
        _lastFrontendChanges = lastFrontendChanges;
    }

    public Date getLastFrontendChanges() {
        return _lastFrontendChanges;
    }
                                            
    public void setBrokerageNetIncome(Double brokerageNetIncome) {
        _brokerageNetIncome = brokerageNetIncome;
    }
            
    public Double getBrokerageNetIncome() {
        return _brokerageNetIncome;
    }
                                            
    public void setBrokerageDisposableIncome(Double brokerageDisposableIncome) {
        _brokerageDisposableIncome = brokerageDisposableIncome;
    }
            
    public Double getBrokerageDisposableIncome() {
        return _brokerageDisposableIncome;
    }

    public void setEmailValidatedDate(Date emailValidatedDate) {
        if (!_emailValidatedDateIsSet) {
            _emailValidatedDateIsSet = true;
            _emailValidatedDateInitVal = getEmailValidatedDate();
        }
        registerChange("email validated date", _emailValidatedDateInitVal, emailValidatedDate);
        _emailValidatedDate = emailValidatedDate;
    }

    public Date getEmailValidatedDate() {
        return _emailValidatedDate;
    }

    public void setPayoutMode(String payoutMode) {
        if (!_payoutModeIsSet) {
            _payoutModeIsSet = true;
            _payoutModeInitVal = getPayoutMode();
        }
        registerChange("payout mode", _payoutModeInitVal, payoutMode);
        _payoutMode = payoutMode;
    }

    public String getPayoutMode() {
        return _payoutMode;
    }

    public void setPapLeadTracked(Date papLeadTracked) {
        if (!_papLeadTrackedIsSet) {
            _papLeadTrackedIsSet = true;
            _papLeadTrackedInitVal = getPapLeadTracked();
        }
        registerChange("pap lead tracked", _papLeadTrackedInitVal, papLeadTracked);
        _papLeadTracked = papLeadTracked;
    }

    public Date getPapLeadTracked() {
        return _papLeadTracked;
    }

    public Long getPersonalAdvisorId() {
        return _personalAdvisorId;
    }

    public void setPersonalAdvisorId(Long personalAdvisorId) {
        this._personalAdvisorId = personalAdvisorId;
    }

    public void setReceiveLoanOffers(Date receiveLoanOffers) {
        if (!_receiveLoanOffersIsSet) {
            _receiveLoanOffersIsSet = true;
            _receiveLoanOffersInitVal = getReceiveLoanOffers();
        }
        registerChange("receive loan offers", _receiveLoanOffersInitVal, receiveLoanOffers);
        _receiveLoanOffers = receiveLoanOffers;
    }

    public Date getReceiveLoanOffers() {
        return _receiveLoanOffers;
    }

    public void setReceiveConsolidationOffers(Date receiveConsolidationOffers) {
        if (!_receiveConsolidationOffersIsSet) {
            _receiveConsolidationOffersIsSet = true;
            _receiveConsolidationOffersInitVal = getReceiveConsolidationOffers();
        }
        registerChange("receive consolidation offers", _receiveConsolidationOffersInitVal, receiveConsolidationOffers);
        _receiveConsolidationOffers = receiveConsolidationOffers;
    }
                        
    public Date getReceiveConsolidationOffers() {
        return _receiveConsolidationOffers;
    }
            
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Account.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _username=").append(_username);
            builder.append("\n    _email=").append(_email);
            builder.append("\n    _phone=").append(_phone);
            builder.append("\n    _phoneCode=").append(_phoneCode);
            builder.append("\n    _phone2=").append(_phone2);
            builder.append("\n    _phone2Code=").append(_phone2Code);
            builder.append("\n    _assetManagerGuidelineDate=").append(_assetManagerGuidelineDate);
            builder.append("\n    _newsletterSubscription=").append(_newsletterSubscription);
            builder.append("\n    _marketingPlacementId=").append(_marketingPlacementId);
            builder.append("\n    _preferredRole=").append(_preferredRole);
            builder.append("\n    _externalReferenceId=").append(_externalReferenceId);
            builder.append("\n    _averageUpdatedRoi=").append(_averageUpdatedRoi);
            builder.append("\n    _isManaged=").append(_isManaged);
            builder.append("\n    _activationString=").append(_activationString);
            builder.append("\n    _marketingTest=").append(_marketingTest);
            builder.append("\n    _phoneEmployer=").append(_phoneEmployer);
            builder.append("\n    _phoneCodeEmployer=").append(_phoneCodeEmployer);
            builder.append("\n    _phoneEmployerCoBorrower=").append(_phoneEmployerCoBorrower);
            builder.append("\n    _phoneCodeEmployerCoBorrower=").append(_phoneCodeEmployerCoBorrower);
            builder.append("\n    _payoutMode=").append(_payoutMode);
            builder.append("\n}");
        } else {
            builder.append(Account.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Account instance() {
        return this;
    }
}
