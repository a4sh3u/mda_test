package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.CreditAdvisorAvailabilityEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'CreditAdvisorAvailabilitys'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractCreditAdvisorAvailability
    extends KreditPrivatEntity implements CreditAdvisorAvailabilityEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCreditAdvisorAvailability.class);

}

