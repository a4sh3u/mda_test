//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace_new\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace_new\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(transaction)}

import de.smava.webapp.account.domain.*;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.exception.InvalidValueException;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Transactions'.
 *
 * @author generator
 */
public interface TransactionDao extends BaseDao<Transaction>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the transaction identified by the given id.
     *
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Transaction getTransaction(Long id);

    /**
     * Saves the transaction.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao<>#save(T)} instead.
     */
    Long saveTransaction(Transaction transaction);

    /**
     * Deletes an transaction, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the transaction
     */
    void deleteTransaction(Long id);

    /**
     * Retrieves all 'Transaction' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Transaction> getTransactionList();

    /**
     * Retrieves a page of 'Transaction' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     */
    Collection<Transaction> getTransactionList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Transaction' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<Transaction> getTransactionList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Transaction' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<Transaction> getTransactionList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Transaction' instances.
     */
    long getTransactionCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(transaction)}
    void replaceBankAccount(BankAccount oldBankAccount, BankAccount bankAccount);




    double getRepaySum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, BankAccount smavaBankAccount);
    double getBookingSum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames);
    double getBookingSum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts, Collection<String> marketNames, Set<String> contractStates, Collection<Contract> contracts);

    Collection<Booking> getBookings(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, List<BankAccount> debitAccounts, List<BankAccount> creditAccounts);

    /**
     *
     * @param bookingTypes
     * @param limitDate
     * @param transactionStates
     * @param account
     * @param bankAccountProvider
     * @param smavaAccounts
     * @param marketNames
     * @param contractStates
     * @return
     * @throws InvalidValueException if smavaAccounts was empty or null
     */
    double getInvestmentSum(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, List<BankAccount> smavaAccounts, Collection<String> marketNames, Set<String> contractStates) throws InvalidValueException;

    Collection<Transaction> getInvestmentTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, Collection<BankAccount> smavaAccounts);

    Collection<Transaction> getCreditTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, BankAccount smavaAccount);
    Collection<Transaction> getDebitTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider, BankAccount smavaAccount);
    Collection<Transaction> getDepositTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider);
    Collection<Transaction> getPayoutTransactions(List<BookingType> bookingTypes, Date limitDate, Set<String> transactionStates, Account account, BankAccountProvider bankAccountProvider);

    Collection<Transaction> getTransactions(BankAccount creditAccount, BankAccount debitAccount, Date date, String state);
    Collection<Transaction> getOpenTransactions(BankAccount creditAccount, BankAccount debitAccount, Date dueDate);
    Transaction findEarliestOpenTransferTransaction(BankAccount creditAccount, BankAccount debitAccount, Date dueDate, String type);

    /**
     * Attention the underlining query searches for transaction in the opposite direction too.
     * Remember to change the sign of the added booking-amount in that case!!!!
     */
    Collection<Transaction> findEarliestOpenTransferTransactions(BankAccount creditAccount, BankAccount debitAccount, Date dueDate, String type);
    Transaction findEarliestOpenTransaction(BankAccount debitAccount, BankAccount creditAccount, Date dueDate, String type);
    Transaction findEarliestOpenTransactionAfterDate(String type, BankAccount debitAccount, BankAccount creditAccount, Date date);

    Collection<Transaction> findBorrowerReminderTransactions(Date date);
    /**
     * collects all transactions with reminder state 0.
     * For contained bookings (interest/amortisation) there should already exist an insurancepoolpayoutrunt
     *
     */
    Collection<Transaction> findReminderTransactionsForLenderNotification(Date date);
    Collection<Transaction> findTransactions(String transactionType, String transactionState, Collection<BankAccount> debitAccounts, Collection<BankAccount> creditAccounts, Date fromDate, Date toDate);

    Collection<Transaction> findTransactionsWithBookingTypes(Set<String> transactionTypes, Set<String> transactionStates, Set<BankAccount> debitAccounts, Set<BankAccount> creditAccounts, Set<BookingType> bookingTypes, Date fromDate, Date toDate);

    Collection<Transaction> findTransactionCandidates(String sqlStatement);

	Transaction findEarliestOpenTransferTransactionByMainOrder(BankAccount debit, BankAccount credit, Booking booking, Order mainOrder, Date dueDate);

	Transaction findOpenTransaction(final BankAccount credit, final BankAccount debit, final Date dueDate, final String transactionType, final Contract contract, final BookingGroup bookingGroup);

	int getOpenReminderTransactionsCount(Account account);

	int getMaxDunnungLevel(Account account);

	/**
	 * @param account - to count for
	 * @param dunningLevel - optional, if null all will be counted
	 * @return count of return debits
	 */
	int getReturnDebitCount(Account account, Integer dunningLevel);

	int getReturnDebitCount(Account account);

	Date getLastReturnDebitDate(Account account);

	double getReminderAmount(Account account);

    /**
     * returns the payback (interest + amortisation) for a given lender
     * placed in here because all xxxSum Methods are here. BookingDao is better location.
     * */
	double getPaybackSum(Account lender, BankAccountProvider bankAccountProvider, Date date, List<BankAccount> debitAccounts, List<BookingType> bookingTypes);
	Map<Long, Double> getAmountForTransactions(Collection<Transaction> transactions);



	void evictAll();
	void forceFlush();

	Map<Long, Boolean> getIsInterimMap(Collection<BankAccount> interimAccounts, String inIdsString);

	Collection<Transaction> getTransactionsWithNegativeAmount(Collection<Transaction> inputTransactions);

	Transaction findOpenTransactionForUnscheduledRepayment(BankAccount internalBankAccount, BankAccount callMoneyAccount, String type);

	Transaction findEarliestOpenTransactionAfterDateForContracts(String typeTransfer, BankAccount debit, BankAccount credit, Date transactionDate, Set<Contract> transactionContracts);

	/**
	 * calculates the minimum number of successful transactions of type {@link de.smava.webapp.account.domain.Transaction#TYPE_DIRECT_DEBIT} for all
	 * contracts with the given states.
	 *
	 * @param account - to check for
	 * @param contractStates - sub set of {@link de.smava.webapp.account.domain.Contract#STATE_NAMES}
	 * 
	 * @return minimum transaction count
	 */
	int getMinSuccessfullDirectDebits(Account account, Set<String> contractStates);
	
	List<Transaction> getTransactionWithBookingsOfTypeAndContracts(String transactionType, String state, BookingType bookingType, Set<BankAccount> debitAccounts, Set<BankAccount> creditAccounts, Set<Contract> contracts, Date dueDate);
	
	List<Transaction> getTransactionWithBookingsOfTypeAndContracts(String transactionType, String state, BookingType bookingType, BankAccountTransferDirection bankAccountTransferDirection, Set<Contract> contracts, Date dueDate, String bookingGroupType);

	List<Transaction> getTransactionsForBankAccounts(
            Collection<BankAccount> bankAccounts, Date from, Date to,
            String... transactionStates);
	


	Transaction findEarliestOpenTransactionForBookingInContractsAfterDate(
            String transactionType, BookingType bookingType, BankAccount debit,
            BankAccount credit, Date date,
            Set<Contract> contracts);
	
	ReminderState findEncashmentReminderState(Contract contract);
	
	Collection<Transaction> findTransactionsByTransactionId(String transactionID);
	
	public Transaction getRandomOpenTransaction(BankAccount creditAccount, BankAccount debitAccount, Date date, Contract contract);

    /**
     *
     * WEBSITE-12115
     *
     * search for transaction based on transaction_id and id fields criteria. Used during
     * Dtaus etities processing. If EREF is null returns null without query to database.
     *
     * @param eref - assumed eref from DtausEntry
     * @return null if no transaction found, transaction otherwise
     */
    Transaction findByEref(String eref);

    /**
     *
     * WEBSITE-12115
     *
     * search for all transactions based on transaction_id and id fields criteria. Used during
     * Dtaus etities processing. If EREF is null returns null without query to database.
     *
     * @param eref - assumed eref from DtausEntry
     * @return null if no transactions found, transactions otherwise
     */
    Collection<Transaction> findAllByEref(String eref);


    public Transaction findEarliestOpenTransaction(String type, BankAccount debit, BankAccount credit);


    Collection<Transaction> findOpenTransactionsToIllegalBankAccount();

    Collection<Transaction> getOpenTransactions(String transactionType, BankAccount debit, BankAccount credit, Date dueDate);

    /**
     * Set status to deleted for all payout transactions
     * to invalid reference accounts
     */
    void deletePayoutsWithInvalidReference();

    /**
     * Set status to deleted for all negative Fidor payout transactions
     */
    void deleteNegativeFidorPayouts();
    // !!!!!!!! End of insert code section !!!!!!!!
}
