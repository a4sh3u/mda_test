/**
 * 
 */
package de.smava.webapp.cache;

import de.smava.webapp.commons.domain.Entity;

import java.util.Set;

/**
 * @author bvoss
 *
 */
public interface TransactionEntityInformation {
	
	Set<Class<? extends Entity>> getAllInvolvedEntityTypes();
	
	Set<Entity> getAllInvolvedEntities();
	
	<T extends Entity> Set<T> getAllInvolvedEntitiesForType(Class<T> type);
	
	boolean isEntityInvolved(Class<? extends Entity> type);
	
	String toString();
	
	Set<Long> getInvolvedAssetAmountRelevantAccountIds();
	
	public Set<Long> getInvolvedAccountIds();

}
