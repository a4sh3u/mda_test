package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CategoryHistory;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * The domain object that represents 'Categorys'.
 */
public class Category extends CategoryHistory  implements ImageAwareEntity {

    public Category() {
        this._categoryOrders = new HashSet<CategoryOrder>();
    }

        protected String _name;
        protected String _description;
        protected Category _parent;
        protected List<Category> _children;
        protected Collection<CategoryOrder> _categoryOrders;
        protected List<CategoryImage> _images;
        protected boolean _shortlisted;
        protected String _image;
        protected int _imageHeight;
        protected int _imageWidth;
        
                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                            
    /**
     * Setter for the property 'parent'.
     */
    public void setParent(Category parent) {
        _parent = parent;
    }
            
    /**
     * Returns the property 'parent'.
     */
    public Category getParent() {
        return _parent;
    }
                                            
    /**
     * Setter for the property 'children'.
     */
    public void setChildren(List<Category> children) {
        _children = children;
    }
            
    /**
     * Returns the property 'children'.
     */
    public List<Category> getChildren() {
        return _children;
    }
                                            
    /**
     * Setter for the property 'category orders'.
     */
    public void setCategoryOrders(Collection<CategoryOrder> categoryOrders) {
        _categoryOrders = categoryOrders;
    }
            
    /**
     * Returns the property 'category orders'.
     */
    public Collection<CategoryOrder> getCategoryOrders() {
        return _categoryOrders;
    }
                                            
    /**
     * Setter for the property 'images'.
     */
    public void setImages(List<CategoryImage> images) {
        _images = images;
    }
            
    /**
     * Returns the property 'images'.
     */
    public List<CategoryImage> getImages() {
        return _images;
    }
                                    /**
     * Setter for the property 'shortlisted'.
     */
    public void setShortlisted(boolean shortlisted) {
        if (!_shortlistedIsSet) {
            _shortlistedIsSet = true;
            _shortlistedInitVal = getShortlisted();
        }
        registerChange("shortlisted", _shortlistedInitVal, shortlisted);
        _shortlisted = shortlisted;
    }
                        
    /**
     * Returns the property 'shortlisted'.
     */
    public boolean getShortlisted() {
        return _shortlisted;
    }
                                    /**
     * Setter for the property 'image'.
     */
    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = getImage();
        }
        registerChange("image", _imageInitVal, image);
        _image = image;
    }
                        
    /**
     * Returns the property 'image'.
     */
    public String getImage() {
        return _image;
    }
                                    /**
     * Setter for the property 'image height'.
     */
    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = getImageHeight();
        }
        registerChange("image height", _imageHeightInitVal, imageHeight);
        _imageHeight = imageHeight;
    }
                        
    /**
     * Returns the property 'image height'.
     */
    public int getImageHeight() {
        return _imageHeight;
    }
                                    /**
     * Setter for the property 'image width'.
     */
    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = getImageWidth();
        }
        registerChange("image width", _imageWidthInitVal, imageWidth);
        _imageWidth = imageWidth;
    }
                        
    /**
     * Returns the property 'image width'.
     */
    public int getImageWidth() {
        return _imageWidth;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Category.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _shortlisted=").append(_shortlisted);
            builder.append("\n    _image=").append(_image);
            builder.append("\n    _imageHeight=").append(_imageHeight);
            builder.append("\n    _imageWidth=").append(_imageWidth);
            builder.append("\n}");
        } else {
            builder.append(Category.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Category instance() {
        return this;
    }
}
