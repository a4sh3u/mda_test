//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.contract.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(contract)}

import java.io.Serializable;
import java.util.*;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.todo.dao.VolumeCount;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Contracts'.
 *
 * @author generator
 */
public interface ContractDao extends BaseDao<Contract>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the contract identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Contract getContract(Long id);

    /**
     * Saves the contract.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveContract(Contract contract);

    /**
     * Deletes an contract, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the contract
     */
    void deleteContract(Long id);

    /**
     * Retrieves all 'Contract' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Contract> getContractList();

    /**
     * Retrieves a page of 'Contract' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Contract> getContractList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Contract' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Contract> getContractList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Contract' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Contract> getContractList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Contract' instances.
     */
    long getContractCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(contract)}

    /**
     * Calculates the weighted arithmetic average.
     * 
     * <i>result = (rate_a * (w_a) + rate_b * (w_b)) / (w_a + w_b); w_x = amount_a / base</i><br />
     * and the minimum and maximum rate
     * 
     * @param whereClause
     * @param base
     * @param params
     * @return
     */
    ContractInterestRange getAverageRateAndMinMax(String whereClause, double base, Object ... params);

//    Collection<Contract> findContractList(Account account, boolean withMatched, Pageable pageable, Sortable sortable);

    Long getContractNumber();
    
    VolumeCount getRunningContractAmountSum(Market market, Collection<String> contractStates);
    VolumeCount getOverallContractAmountSum(Market market, Collection<String> contractStates);
    Long getRunningContractCount(Market market, Collection<String> contractStates);
    
    long getContractCount(Account lender, Collection<String> contractStates);
    SortedSet<BookingGroup> getBookingGroups(Contract contract, boolean considerOnlyBookingGroupWithPaymentRate);
    SortedSet<BookingGroup> getBookingGroups(Collection<Contract> contracts, boolean considerOnlyBookingGroupWithPaymentRate);
    SortedSet<BookingGroup> getBookingGroups(Contract contract, boolean considerOnlyBookingGroupWithPaymentRate, Collection<String> bookingGroupStates);
    SortedSet<BookingGroup> getBookingGroups(Collection<Contract> contracts, boolean considerOnlyBookingGroupWithPaymentRate, Collection<String> bookingGroupStates);
    Collection<Contract> getContractsWithStates(Set<String> states);
	Collection<Contract> getContractsWithStates(Account account, Set<String> lenderRunningContractStates);
	
	Collection<Contract> getMainContractsWithStates(Set<String> states);
	
	Collection<Contract> getContractsByBidSearch(BidSearch bidSearch, Set<String> activeContractStates);
	Collection<Contract> getContractsByBidSearch(Collection<BidSearch> bidSearches, Set<String> activeContractStates);
	Double getContractsAmountSumByBidSearch(Collection<BidSearch> bidSearches, Set<String> activeContractStates);
	

	List<ContractRoiResultEntry> getRawRoiDateForContracts(Collection<Contract> contracts, Collection<Long> smavaBankAccountIds, long knkId);
	Collection<Contract> getStartedContracts(Account account);
	Collection<Contract> findContractsById(Set<Long> contractIds);

	Collection<Contract> findCompletedContracts();
	public Collection<Contract> findDefaultedCompletedContracts();
	
	Date findEndDate(Contract contract);
	
	Date getLastSuccessfullTransactionDueDate(Contract contract, Collection<BankAccount> smavaInterimAccounts);
	
	/**
	 * count all contracts of account with state {@link Contract#STATE_REPAY}, {@link Contract#STATE_LATE}, {@link Contract#STATE_TO_BE_CANCELED} and {@link Contract#STATE_TO_BE_OBJECTED}.
	 * 
	 * @param account - {@link Account} to look at 
	 * @return count
	 */
	int getRunningContractCount(Account account);
	
	/**
	 * same as {@link #getRunningContractCount(Account)} but only the ons with a open reminder amount.
	 * 
	 * @param account - {@link Account} to look at
	 * @return count
	 */
	int getRunningContractCountWithReminderAmount(Account account);
	
	
	Collection<Contract> getContractsForOrderGroup(Long orderGroupId);
	
	double getContractSumBetweenLenderAndBorrower(Long lenderId, Long borrowerId, Set<String> states);
        Long getContractCountForPlacement(Long marketingPlacementId);

    /**
     * returns all contracts of the given lender for the given market
     * @param lender
     * @param marketName
     * @return
     */
    public Collection<Contract> getContractsbyLenderAndMarket( Long lender, String marketName);

    /**
     * returns the numbers of 
     * @param lender
     * @return
     */
    public Collection<ContractStateNumbers> getContractsNumbersPerStateForLender(Long lender);

    long countContracts(Collection<Market> markets, int term);
    Collection<Contract> findOrderedContracts(Collection<Market> markets, int term, long minimumCount);


    public ContractInterestRange getAverageRateAndMinMax(Collection<Market> markets, int term, double amoutStep);

    List<Contract> getPendingContractsForLender(Account lender);

    List<Contract> getPayoutContractsForLender(Account lender);

    List<Contract> getNewContractsForLender(Account lender);

    List<Contract> getManualNewContractsForLender(Account lender);

    List<Contract> getPayoutContractsForBorrower(Account borrower);

    List<Contract> getNewContractsForBorrower(Account borrower);

    List<Contract> getPendingContractsForBorrower(Account borrower);

    Collection<Contract> getRelatedContracts(Order mainOrder, Collection<String> states);

    Collection<Contract> findNewContractListSortByDate();

    Collection<Contract> findNotNewContractListSortByDate();


    Collection<Contract> getObjectedContracts();

    Collection<Contract> getPendingContracts();

    Collection<Contract> getLatestContracts(Pageable pageable);
    // !!!!!!!! End of insert code section !!!!!!!!


}
