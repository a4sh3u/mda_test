package de.smava.webapp.account.dao.jdo;

import de.smava.webapp.account.dao.CreditWorthinessCalculationDao;
import de.smava.webapp.account.domain.CreditWorthinessCalculation;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;

import javax.jdo.JDOHelper;
import java.util.Collection;

/**
 * DAO implementation for the domain object 'CreditWorthinessCalculations'.
 */
public class JdoCreditWorthinessCalculationDao extends JdoBaseDao implements CreditWorthinessCalculationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCreditWorthinessCalculationDao.class);

    public Long saveEntity(CreditWorthinessCalculation entity) {
        //entity.isNew checks, if there is an id available
        if (entity.getId() == null) {
            //JDOHelper checks, if this instance already has been made persistent
            //this subsequent check is necessary, because after the first "makepersistent" call the entity will be marked as new for JDO,
            //but not id is assigned, so entity.isNew() will return true wich would lead to more "makepersistent" calls
            if (!JDOHelper.isNew(entity)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Make entity persistent: " + entity);
                }
                getPersistenceManager().makePersistent(entity);
            }
        } 
        return entity.getId();
    }

    public void deleteCreditWorthinessCalculation(Long id) {
        // TODO Auto-generated method stub
        
    }

    public CreditWorthinessCalculation getCreditWorthinessCalculation(Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getCreditWorthinessCalculationCount() {
        // TODO Auto-generated method stub
        return 0;
    }

    public Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList() {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList(Pageable pageable) {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList(Sortable sortable) {
        // TODO Auto-generated method stub
        return null;
    }

    public Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList(Pageable pageable, Sortable sortable) {
        // TODO Auto-generated method stub
        return null;
    }

    public Long saveCreditWorthinessCalculation(CreditWorthinessCalculation creditWorthinessCalculation) {
        // TODO Auto-generated method stub
        return null;
    }

    public Long save(CreditWorthinessCalculation message) {
        return saveEntity(message);
    }

    public CreditWorthinessCalculation load(Long id) {
        return null;
    }

	public boolean exists(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

}
