package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.MessageConfig;

import java.util.List;


/**
 * The domain object that represents 'MessageConfigs'.
 *
 * @author generator
 */
public interface MessageConfigEntityInterface {

    /**
     * Setter for the property 'forward'.
     *
     * 
     *
     */
    void setForward(boolean forward);

    /**
     * Returns the property 'forward'.
     *
     * 
     *
     */
    boolean getForward();
    /**
     * Setter for the property 'forward lender status mail'.
     *
     * 
     *
     */
    void setForwardLenderStatusMail(boolean forwardLenderStatusMail);

    /**
     * Returns the property 'forward lender status mail'.
     *
     * 
     *
     */
    boolean getForwardLenderStatusMail();
    /**
     * Setter for the property 'ignore list'.
     *
     * 
     *
     */
    void setIgnoreList(List<Account> ignoreList);

    /**
     * Returns the property 'ignore list'.
     *
     * 
     *
     */
    List<Account> getIgnoreList();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();

}
