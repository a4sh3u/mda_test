package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Rating;
import de.smava.webapp.account.domain.interfaces.BorrowerPromotionEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'BorrowerPromotions'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBorrowerPromotion
    extends KreditPrivatEntity implements BorrowerPromotionEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBorrowerPromotion.class);

    /**
     * generated serial.
     */
    private static final long serialVersionUID = 2774718746622803365L;

    private transient Set<String> _ratingLetters;

    public boolean isRatingAllowed(final String ratingLetter) {
        if (_ratingLetters == null) {
            _ratingLetters = new LinkedHashSet<String>();
            for (Rating rating : getRatings()) {
                _ratingLetters.add(rating.getName());
            }
        }
        return _ratingLetters.contains(ratingLetter);
    }

    public boolean isPromotionAllowed(final double amount, final int duration, final String rating, final long score) {
        final boolean result = (getDurationsAsList() == null
                || getDurationsAsList().contains(duration))
                && getMinAmount() <= amount
                && amount <= getMaxAmount()
                && getMinScore() <= score
                && isRatingAllowed(rating);
        return result;
    }

    private List<Integer> getDurationsAsList() {
        String durations = getDuration();
        List<Integer> list = null;
        if (durations != null) {
            list = new ArrayList<Integer>();
            for (String duration : getDuration().split(",")) {
                list.add(Integer.parseInt(duration.trim()));
            }
            Collections.sort(list);
        }
        return list;
    }

}

