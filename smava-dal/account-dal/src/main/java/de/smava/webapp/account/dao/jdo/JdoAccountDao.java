package de.smava.webapp.account.dao.jdo;

import de.smava.webapp.account.dao.AccountDao;
import de.smava.webapp.account.dao.DelayedRepaymentMappingBean;
import de.smava.webapp.account.dao.FirstRepaymentMappingBean;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.security.SmavaUserDetailsIf;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.constants.NumericalData;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.security.Role;
import de.smava.webapp.commons.web.AbstractListCommand;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

@Repository(value = "accountDao")
public class JdoAccountDao extends JdoBaseDao implements AccountDao, Serializable {

    private static final Logger LOGGER = Logger.getLogger(JdoAccountDao.class);

    private static final String CLASS_NAME = "Account";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    /**
     * generated serial.
     */
    private static final long serialVersionUID = 5760218737995782852L;


    private static final String FIRST_REPAYERS_SQL = "select bo.id as \"accountId\", sum(b.amount) as \"allRates\","
        + " sum("
        + "case when c.start_date between (date_trunc('month',to_timestamp(:currentDate,'YYYY-MM-DD')) - interval '1month')"
        + " and (date_trunc('month',to_timestamp(:currentDate,'YYYY-MM-DD')))"
        + " then b.amount else 0 end"
        + ") as \"firstRate\","
        + "sum(case when b.type_new = 24 then b.amount else 0 end) as \"initialInterest\","
        + "(select sum(b2.amount) from booking b2 where b2.type_new = 39 and b2.transaction_id = t.id) as \"activityFee\","
        + "(select sum(b3.amount) from booking b3 where b3.type_new = 41 and b3.transaction_id = t.id) as \"postidentFee\""

        + " from booking b, transaction t, account bo, bank_account debit, contract c"
        + " where b.transaction_id = t.id"
        + " and t.type = '" + Transaction.TYPE_DIRECT_DEBIT + "'"
        + " and t.debit_account = debit.id"
        + " and debit.account_id = bo.id"
        + " and debit.type = '" + BankAccount.TYPE_REFRENCE_ACCOUNT + "'"
        + " and t.\"dueDate\" = (date_trunc('month',to_timestamp(:currentDate,'YYYY-MM-DD')) + interval '1month' - interval '1day')"
        + " and t.state = '" + Transaction.STATE_OPEN + "'"
        + " and (select count(*) from account_role where account_role.name = '" + Role.ROLE_BORROWER.toString() + "' and account_role.account_id = bo.id) > 0"
        + " and b.contract_id = c.id"
        + "	group by bo.id, t.id"
        + "	having sum(case when c.start_date between (date_trunc('month',to_timestamp(:currentDate,'YYYY-MM-DD')) - interval '1month') and (date_trunc('month',to_timestamp(:currentDate,'YYYY-MM-DD'))) then b.amount else 0 end) > 0"
        + " order by bo.id";

    private static final String MISSING_DOCUMENTS_SQL
    = String.format("SELECT DISTINCT a.id as id "
    + "FROM document d INNER JOIN account a ON a.id = d.account_id INNER JOIN account_role ar ON a.id = ar.account_id INNER JOIN account_role_state ars ON ars.account_role_id = ar.id "
    + "WHERE ars.valid_until IS NULL "
    + "AND ars.name = '%s' "
    + "AND ar.name = '%s' "
    + "AND a.creation_date BETWEEN :bottom AND :top "
    + "AND NOT EXISTS ( "
    + "			SELECT doc1.id "
    + "			FROM document doc1 "
    + "			WHERE (doc1.type_of_dispatch = '%s' "
    + "			OR doc1.direction = '%s')"
    + "			AND doc1.state = '%s' "
    + "			AND doc1.account_id = d.account_id "
    + "			) "
    + "AND NOT EXISTS ( "
    + "			SELECT docu.id "
    + "			FROM document docu "
    + "			WHERE docu.direction = '%s' "
    + "			AND docu.state = '%s' "
    + "			AND docu.account_id = d.account_id "
    + "			AND docu.type != '%s' "
    + "			);", AccountRoleState.STATE_APPLIED, Role.ROLE_BORROWER, Document.TYPE_OF_DISPATCH_INTERNAL, Document.DIRECTION_INTERNAL, Document.STATE_SUCCESSFUL, Document.DIRECTION_IN, Document.STATE_SUCCESSFUL, Document.DOCUMENT_TYPE_SCHUFA_RECORD);

    /**
     * Returns an attached copy of the account identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Account load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        Account result = getEntity(Account.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Account entity = findUniqueEntity(Account.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the account.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Account account) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAccount: " + account);
        }
        return saveEntity(account);
    }

    /**
     * Deletes an account, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account
     */
    public void deleteAccount(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAccount: " + id);
        }
        deleteEntity(Account.class, id);
    }

    /**
     * Retrieves all 'Account' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Account> getAccountList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<Account> result = getEntities(Account.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Account' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Account> getAccountList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<Account> result = getEntities(Account.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Account' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Account> findAccountList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<Account> result = findEntities(Account.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Account' instances.
     */
    public long getAccountCount() {
        long result = getEntityCount(Account.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Attaches a Profile to the Account instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    public void attachProfile(Account account, Profile profile) {
        account.setProfile(profile);
        profile.setAccount(account);
        saveEntity(profile);
        saveEntity(account);
    }

    /**
     * Attaches a MessageConfig to the Account instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    public void attachMessageConfig(Account account, MessageConfig messageConfig) {
        account.setMessageConfig(messageConfig);
        messageConfig.setAccount(account);
        saveEntity(messageConfig);
        saveEntity(account);
    }

    /**
     * {@inheritDoc}
     */
    public Account findAccountByUsername(final String username) {
        Account result = null;
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByUsername");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", username);
        @SuppressWarnings("unchecked")
        Collection<Account> accounts = (Collection<Account>) query.executeWithMap(params);
        if (accounts.size() == 1) {
            result = accounts.iterator().next();
        }
        else if (accounts.size() > 1) {
            LOGGER.fatal("got invalid account resultset for username \"" + username + "\": " + accounts);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public Account findAccountByEmail(final String email) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByEmail");
        query.setUnique(true);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", email);
        Account result = (Account) query.executeWithMap(params);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account findAccountBySmavaId(Long smavaId) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByAccountId");
        query.setUnique(true);
        return (Account) query.execute(smavaId);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void createSmavaAccountPerson() {
        Account account = getSmavaAccount();
        Person person = new Person(Person.PRIMARY_PERSON_TYPE);
        person.setCreationDate(CurrentDate.getDate());
        account.setPerson(person);
        save(account);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account getSmavaAccount() {
        Account account = findAccountByUsername("smava");
        if (account == null) {
            account = new Account();
            account.setCreationDate(CurrentDate.getDate());
            account.setUsername("smava");
            account.setEmail("info@smava.de");
            MessageConfig messageConfig = new MessageConfig();
            messageConfig.setAccount(account);
            messageConfig.setForward(true);
            messageConfig.setIgnoreList(new LinkedList<Account>());
            account.setMessageConfig(messageConfig);
            save(account);
        }
        return account;
    }

    /**
     * @see de.smava.webapp.account.dao.AccountDao#findAccountsWithReciveLoanOffersForGivenDateAndPreferredRoles(Date, Set, Set)
     */
    @Override
    public Collection<Account> findAccountsWithReciveLoanOffersForGivenDateAndPreferredRoles(
            Date date, Set<String> preferredRoles,
            Set<String> allowedLoanApplicationStates) {
        Date startDate = getStartTimeFromDate(date);
        Date endDate = getEndTimeFromDate(date);

        StringBuilder queryString = new StringBuilder();

        queryString.append("select account.id as id from account ");
        queryString.append("where receive_loan_offers is not null ");
        queryString.append("and creation_date between '");
        queryString.append(new Timestamp(startDate.getTime()));
        queryString.append("' and '");
        queryString.append(new Timestamp(endDate.getTime()));
        queryString.append("' and id not in ");
        queryString.append("(select account_id from loan_application");

        if (null != allowedLoanApplicationStates) {
            queryString.append(" where");

            String andDelim = "";
            for (String state : allowedLoanApplicationStates) {
                queryString.append(andDelim).append(" state <> '").append(state).append("'");
                andDelim = " and ";
            }
        }

        queryString.append(") ");
        if (null != preferredRoles) {
            queryString.append("and (");

            String orDelim = "";
            for (String role : preferredRoles) {
                queryString.append(orDelim).append("preferred_role = '").append(role).append("'");
                orDelim = " or ";
            }

            queryString.append(")");
        }

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setClass(Account.class);
        @SuppressWarnings("unchecked")
        Collection<Account> result = (Collection<Account>) q.execute();
        return result;
    }

    /**
     * @see de.smava.webapp.account.dao.AccountDao#findAccountsForPayout()
     */
    @Override
    public List<Account> findAccountsForPayout() {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountForPayout");
        query.setClass(Account.class);

        @SuppressWarnings("unchecked")
        List<Account> result = (List<Account>) query.execute();

        return result;
    }

    /**
     * @see de.smava.webapp.account.dao.AccountDao#findAccountByRole(Role role)
     */
    @Override
    public List<Account> findAccountByRole(final Role role) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByRole");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("role", role.name());
        @SuppressWarnings("unchecked")
        List<Account> result = (List<Account>) query.executeWithMap(params);
        return result;
    }

    public Collection<Account> findAccountByRoles(final Collection<String> roles) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("select distinct(account.id) as id ");
        queryString.append("from  ");
        queryString.append("account ,  ");
        queryString.append("account_role, ");
        queryString.append("account_role_state ars ");
        queryString.append("where  ");
        queryString.append("account.id = account_role.account_id ");
        queryString.append("and ars.account_role_id = account_role.id ");
        queryString.append("and ars.valid_until is null ");
        queryString.append("and (");

        String orDelim = "";
        for (String role : roles) {
            queryString.append(orDelim).append(" account_role.name='").append(role).append("'");
            orDelim = " or ";
        }

        queryString.append(") ");


        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setClass(Account.class);
        @SuppressWarnings("unchecked")
        Collection<Account> result = (Collection<Account>) q.execute();
        return result;
    }

    /**
     * @see de.smava.webapp.account.dao.AccountDao#findFidorAccountsToMerge(java.util.Collection, java.lang.Long)
     * <p/>
     * creating the query as follows:
     *  select distinct(account.id) as id
     *	from
     *	account INNER JOIN account_role ON account.id = account_role.account_id
     *	INNER JOIN account_role_state ars ON ars.account_role_id = account_role.id
     *	where
     *	 account.fidor_term_and_cond_date is not null
     *	and ars.valid_until is null and ( account_role.name='ROLE_LENDER' or account_role.name='ROLE_BORROWER')
     *	and account.id not in
     *	(select distinct(account.id) as id from account , bank_account where bank_account.bank_service_provider_id = 28035
     *		and bank_account.account_id = account.id
     *		and bank_account.type in ('KOK', 'KNK')
     *		and bank_account.valid = true
     *		and bank_account.expiration_date is null)
     *QUERY
     */
    @Override
    public Collection<Account> findFidorAccountsToMerge(final Collection<String> roles, Long bankAccountProviderId) {

        StringBuilder queryString = new StringBuilder();

        queryString.append("select distinct(account.id) as id ");
        queryString.append("from  ");
        queryString.append("account INNER JOIN account_role ");
        queryString.append("ON account.id = account_role.account_id ");
        queryString.append("INNER JOIN account_role_state ars ");
        queryString.append("ON ars.account_role_id = account_role.id ");
        queryString.append("INNER JOIN customer_agreement ca ");
        queryString.append("ON account.id = ca.account_id ");
        queryString.append("where  ");
        queryString.append("ars.name not in ('VOID', 'DENIED') and ");
        queryString.append("ca.type = 'FIDOR_AGREEMENT' and ");
        queryString.append("ca.revocation_date is null and ");
        queryString.append("ars.valid_until is null and ");
        queryString.append("account.state not in ('DELETED', 'DEACTIVATED') and ");
        queryString.append("(");

        String orDelim = "";
        for (String role : roles) {
            queryString.append(orDelim).append(" account_role.name='").append(role).append("'");
            orDelim = " or ";
        }
        queryString.append(") ");

        queryString.append("and account.id not in ");
        queryString.append("(select distinct(account.id) as id ");
        queryString.append("from  ");
        queryString.append("account ");
        queryString.append(", bank_account ");
        queryString.append("where  ");
        queryString.append("bank_account.bank_service_provider_id = ").append(bankAccountProviderId.longValue());
        queryString.append("and bank_account.account_id = account.id  ");
        queryString.append("and bank_account.type in ('KOK', 'KNK') ");
        queryString.append("and bank_account.valid = true ");
        queryString.append("and bank_account.expiration_date is null)");

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setClass(Account.class);
        @SuppressWarnings("unchecked")
        Collection<Account> result = (Collection<Account>) q.execute();
        return result;
    }

    /**
     * don't use this method as it return multiple lines on account with  more than 1 KOK/KNK
     *
     * @see de.smava.webapp.account.dao.AccountDao#findAccountByRoleWithInternalBankAccount(de.smava.webapp.commons.security.Role)
     */
    @Deprecated
    public Collection<Account> findAccountByRoleWithInternalBankAccount(final Role role) {
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, role.name());

        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByRoleWithInternalBankAccount");
        query.setClass(Account.class);

        @SuppressWarnings("unchecked")
        List<Account> result = (List<Account>) query.executeWithMap(params);

        return result;
    }

    public List<Account> findActiveAccountsByRole(final Role role) {
        long start = System.nanoTime();
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findActiveAccountsByRole");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("role", role.name());
        @SuppressWarnings("unchecked")
        List<Account> result = (List<Account>) query.executeWithMap(params);

        LOGGER.info("method call took " + ((System.nanoTime() - start) / NumericalData.NANO_TO_MILLI) + " ms. for " + result.size() + " users (" + role.name() + ")");

        return result;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<Account> findInvitedAccounts(final Account account) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findInvitedAccounts");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("inviterId", account.getId());
        @SuppressWarnings("unchecked")
        List<Account> result = (List<Account>) query.executeWithMap(params);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Account preLoadAccount(final Long id) {
        Account result = load(id);
        result.getEmail();
        result.getRoles();
        result.getState();
        return result;
    }


    /**
     * @see de.smava.webapp.account.dao.AccountDao#findDelayedRepayers()
     */
    @SuppressWarnings("unchecked")
    @Override
    public Collection<DelayedRepaymentMappingBean> findDelayedRepayers() {
        String queryTerm = "select a.id as \"accountId\", sum(b.amount) as \"rate\" "
                + getDelayedRepayersFromWhereString(0, true)
                + " group by a.id, t.id"
                + " order by a.id";

        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryTerm);

        query.setResultClass(DelayedRepaymentMappingBean.class);

        return (Collection<DelayedRepaymentMappingBean>) query.execute();
    }

    private String getDelayedRepayersFromWhereString(int level, boolean checkForDueDate) {
        StringBuilder builder = new StringBuilder();
        builder.append(" from booking b, transaction t, account a, bank_account debit");
        builder.append(" where b.transaction_id = t.id");
        builder.append(" and t.debit_account = debit.id");
        builder.append("  and debit.account_id = a.id");
        if (checkForDueDate) {
            builder.append(" and t.\"dueDate\" = (date_trunc('month',to_timestamp('").append(new java.sql.Date(CurrentDate.getTime())).append("','YYYY-MM-DD')) + interval '1month' - interval '1day')");
        }
        builder.append(" and t.state = 'OPEN'");
        builder.append(" and (select count(*) from account_role where account_role.name = 'ROLE_BORROWER' and account_role.account_id = a.id) > 0");
            builder.append(" and debit.type = 'reference account' and t.type='DIRECT_DEBIT'");
            builder.append(" and t.id = b.transaction_id");
            builder.append(" and debit.account_id in");
                builder.append(" (select debu.account_id from transaction tu, bank_account debu");
                    builder.append(" where tu.debit_account=debu.id");
                    builder.append(" and tu.state = 'SUCCESSFUL'");
                    builder.append(" and exists");
                    builder.append(" (select * from dunning_state ds");
                        builder.append(" where ds.transaction_id = tu.id");
                        builder.append(" and ds.level >= ") .append(level);
                    builder.append(")");
                builder.append(")");

            builder.append(" and debit.account_id not in");
                builder.append(" (select debu.account_id from transaction tu, bank_account debu");
                    builder.append(" where tu.debit_account=debu.id ");
                    builder.append(" and tu.state = 'OPEN'");
                    builder.append(" and exists");
                        builder.append(" (select * from dunning_state ds");
                            builder.append(" where ds.transaction_id = tu.id and ds.level >= ") .append(level);
                    builder.append(")");
                builder.append(")");

        return builder.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<FirstRepaymentMappingBean> findFirstRepayers() {

        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", FIRST_REPAYERS_SQL);
        query.setResultClass(FirstRepaymentMappingBean.class);

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("currentDate", new Timestamp(CurrentDate.getTime()));

        @SuppressWarnings("unchecked")
        Collection<FirstRepaymentMappingBean> result = (Collection<FirstRepaymentMappingBean>) query.executeWithMap(parameters);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getBorrowerFurtherEnquiryAccounts() {
        return getBorrowerFirstAuditionAccounts(null, null, null, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getBorrowerFirstAuditionAccounts(final Account account) {
        return getBorrowerFirstAuditionAccounts(account, null, null, false);
    }

    /* (non-Javadoc)
     * @see de.smava.webapp.account.dao.AccountDao#getBorrowerFirstAuditionAccounts(de.smava.webapp.account.domain.Account, de.smava.webapp.account.dao.Pageable, de.smava.webapp.account.dao.Sortable)
     */
    private Collection<Account> getBorrowerFirstAuditionAccounts(final Account account, final Pageable pageable, final Sortable sortable, boolean allowOpenDocuments) {
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct(a.id) as id "
                            + "from account a,account_role ar,account_role_state ars "
                            + "where "
                            + "1=1 "
                            + "and a.state = 'ACTIVATED' "
                            + "and ar.account_id = a.id "
                            + "and ar.\"name\"='ROLE_BORROWER' "
                            + "and ar.id = ars.account_role_id "
                            + "and ars.\"name\" = 'APPLIED' "
                            + "and ars.valid_until is null ");
        if (allowOpenDocuments) {
            sql.append("and ((select count(*) from document where account_id =  a.id and direction = 'IN' and state='OPEN' and type_of_dispatch in ('MAIL','FAX','EMAIL') and type not in ('")
                    .append(Document.DOCUMENT_TYPE_CREDIT_AGREEMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_CREDIT_PROCUREMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_RDI_REQUEST)
                    .append("' , '")
                    .append(Document.DOCUMENT_TYPE_POSTIDENT)
                    .append("')) > 0 and (select count(*) from document where account_id =  a.id and direction = 'IN' and state='OPEN' and type_of_dispatch in ('MAIL','FAX','EMAIL','DOWNLOAD') and type not in ('")
                    .append(Document.DOCUMENT_TYPE_CREDIT_AGREEMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_CREDIT_PROCUREMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_RDI_REQUEST)
                    .append("' , '")
                    .append(Document.DOCUMENT_TYPE_VIDEOIDENT)
                    .append("')) > 0 ) ");

        }
        else {
            sql.append("and ((select count(*) from document where account_id =  a.id and direction = 'IN' and state='OPEN' and type_of_dispatch in ('MAIL','FAX','EMAIL') and type not in ('")
                    .append(Document.DOCUMENT_TYPE_CREDIT_AGREEMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_CREDIT_PROCUREMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_RDI_REQUEST)
                    .append("' , '")
                    .append(Document.DOCUMENT_TYPE_POSTIDENT)
                    .append("')) = 0 or (select count(*) from document where account_id =  a.id and direction = 'IN' and state='OPEN' and type_of_dispatch in ('MAIL','FAX','EMAIL','DOWNLOAD') and type not in ('")
                    .append(Document.DOCUMENT_TYPE_CREDIT_AGREEMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_CREDIT_PROCUREMENT)
                    .append("','")
                    .append(Document.DOCUMENT_TYPE_RDI_REQUEST)
                    .append("' , '")
                    .append(Document.DOCUMENT_TYPE_VIDEOIDENT)
                    .append("')) = 0 ) ");
        }

        sql.append("and (select count(*) from document where account_id =  a.id and direction = 'IN' and state='SUCCESSFUL' and type_of_dispatch in ('MAIL','FAX','EMAIL') and approval_state = 'OK') > 0 "
                            + "and (select count(*) from account_role sub_ar, approval ara where sub_ar.account_id = a.id and ara.account_role_id = sub_ar.id and ara.vote=1 and sub_ar.id = ar.id) = 0");
        if (account != null) {
            sql.append(" and a.id = ?");
        }

        if (pageable != null) {
            sql.append(" OFFSET = ").append(pageable.getOffset());
            sql.append(" LIMIT = ").append(pageable.getItemsPerPage());
        }

        if (sortable != null) {
            sql.append(" ORDER BY = " + sortable.getSort() + " " + (sortable.sortDescending() ? "DESC" : "ASC"));
        }


        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        query.setClass(Account.class);
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (account != null) {
            params.put(1, account.getId());
        }

        @SuppressWarnings("unchecked")
        Collection<Account> accounts = (Collection<Account>) query.executeWithMap(params);

        return accounts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getBorrowerSecondAuditionAccounts(final Account account) {
        return getBorrowerSecondAuditionAccounts(account, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getLenderAuditionAccounts(final Account account) {
        return getLenderAuditionAccounts(account, null, null);
    }


    private Collection<Account> getBorrowerSecondAuditionAccounts(final Account account, final Pageable pageable, final Sortable sortable) {
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct(a.id) as id "
                   + "from "
                   + "account a, "
                   + "account_role ar, "
                   + "account_role_state ars, "
                   + "approval ara "
                   + "where "
                   + "1=1 "
                   + "and a.state = 'ACTIVATED' "
                   + "and ar.account_id = a.id and ar.\"name\"='ROLE_BORROWER' "
                   + "and ar.id = ars.account_role_id and ars.\"name\" = 'APPLIED' "
                   + "and ars.valid_until is null "
                   + "and ara.account_role_id = ar.id and ara.vote=1 "
                   + "and (select count(*) from account_role sub_ar, approval ara where sub_ar.account_id = a.id and ara.account_role_id = sub_ar.id and ara.vote=1 and sub_ar.id = ar.id) = 1 ");
        if (account != null) {
            sql.append(" and a.id = ?");
        }

        if (pageable != null) {
            sql.append(" OFFSET = ").append(pageable.getOffset());
            sql.append(" LIMIT = ").append(pageable.getItemsPerPage());
        }

        if (sortable != null) {
            sql.append(" ORDER BY = ").append(sortable.getSort()).append(" ").append((sortable.sortDescending() ? "DESC" : "ASC"));
        }


        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(Account.class);
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (account != null) {
            params.put(1, account.getId());
        }

        @SuppressWarnings("unchecked")
        Collection<Account> accounts = (Collection<Account>) query.executeWithMap(params);

        return accounts;
    }

    private Collection<Account> getLenderAuditionAccounts(final Account account, final Pageable pageable, final Sortable sortable) {
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct(a.id) as id "
                   + "from  "
                   + "account a, "
                   + "account_role ar, "
                   + "account_role_state ars "
                   + "where  "
                   + "1=1 "
                   + "and a.state = 'ACTIVATED' "
                   + "and ar.account_id = a.id and ar.\"name\"='ROLE_LENDER' "
                   + "and ar.id = ars.account_role_id and ars.\"name\" = 'APPLIED' "
                   + "and ars.valid_until is null "
                   + "and (select count(*) from document where account_id =  a.id and direction = 'IN' and state='OPEN' and type_of_dispatch in ('MAIL','FAX','EMAIL')) = 0 "
                   + "and (select count(*) from document where account_id =  a.id and direction = 'IN' and state='SUCCESSFUL' and type_of_dispatch in ('MAIL','FAX','EMAIL') and approval_state = 'OK') > 0 "
                   + "and (select count(*) from account_role sub_ar, approval ara where sub_ar.account_id = a.id and ara.account_role_id = sub_ar.id and ara.vote=1 and sub_ar.id = ar.id) = 0 ");

        if (account != null) {
            sql.append(" and a.id = ?");
        }

        if (pageable != null) {
            sql.append(" OFFSET = ").append(pageable.getOffset());
            sql.append(" LIMIT = ").append(pageable.getItemsPerPage());
        }

        if (sortable != null) {
            sql.append(" ORDER BY = ").append(sortable.getSort()).append(" ").append((sortable.sortDescending() ? "DESC" : "ASC"));
        }


        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(Account.class);
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (account != null) {
            params.put(1, account.getId());
        }

        @SuppressWarnings("unchecked")
        Collection<Account> accounts = (Collection<Account>) query.executeWithMap(params);

        return accounts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<String> getActiveRolenames(final Account account) {
        StringBuffer sql = new StringBuffer();
        sql.append("select distinct(ar.name) as name "
                   + "from  "
                   + "account a inner join account_role ar on ar.account_id = a.id "
                   + "inner join account_role_state ars on ar.id = ars.account_role_id "
                   + "where  "
                   + "1=1 "
                   + "and ars.valid_until is null ");

        if (account != null) {
            sql.append(" and a.id = ?");
        }


        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setResultClass(String.class);
        Map<Integer, Object> params = new HashMap<Integer, Object>();
        if (account != null) {
            params.put(1, account.getId());
        }

        @SuppressWarnings("unchecked")
        Collection<String> accountRoleNames = (Collection<String>) query.executeWithMap(params);
        return accountRoleNames;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Account> findAccountsListForRoleConsolidation(final String role) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountsListForRoleConsolidation");
        Map<Integer, String> parameters = new HashMap<Integer, String>();
        parameters.put(1, role);

        Collection<Account> result = (Collection<Account>) query.executeWithMap(parameters);

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getRemindedBorrowers(final Pageable pageable, final Sortable sortable) {
        StringBuilder sql = new StringBuilder("select distinct a.id as id "
                + "from transaction t, account a, bank_account ba_c, bank_account ba_d , dunning_state ds "
                + "where t.credit_account = ba_c.id "
                + "and	t.debit_account = ba_d.id "
                + "and	ba_c.account_id = a.id "
                + "and	ba_d.account_id = a.id "
                + "and	ds.transaction_id = t.id "
                + "and	t.type = 'TRANSFER' "
                + "and 	t.state = 'OPEN' "
                + "and	ba_c.\"type\" = 'KNK' "
                + "and	(ba_d.\"type\" = 'reference account' or ba_d.\"type\" = 'other account') "
                + "and	ds.level > 0 ");

        // evaluate Sortable information if present
        if (sortable != null && sortable.hasSort()) {
            String sort = sortable.getSort();
            sort = sort.replaceAll("_", "");
            StringBuilder sortStringBuilder = new StringBuilder("order by ");
            sortStringBuilder.append(sort);
            sortStringBuilder.append(" ");
            if (sortable.sortDescending()) {
                sortStringBuilder.append("desc");
            }
            else {
                sortStringBuilder.append("asc");
            }
            sql.append(sortStringBuilder.toString());
        }

        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(Account.class);

        // evaluate Pageable information
        if (pageable != null) {
            long start = pageable.getOffset();
            long end = start + pageable.getItemsPerPage();
            query.setRange(start, end);
        }

        @SuppressWarnings("unchecked")
        Collection<Account> accounts = (Collection<Account>) query.execute();

        return accounts;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getManagedAccounts(final Account account) {
        StringBuilder sqlQuery = new StringBuilder("select distinct a.* as id ");
        sqlQuery.append("from account a ");
        sqlQuery.append("left outer join marketing_placement mp on a.placement_id = mp.id ");
        sqlQuery.append("left outer join marketing_contact_placement mcp on mcp.marketing_placement_id = mp.id ");
        sqlQuery.append("left outer join marketing_contact mc on mcp.marketing_contact_id = mc.id ");
        sqlQuery.append("where a.is_managed = 't' ");
        sqlQuery.append("and mc.account_id = ");
        sqlQuery.append(account.getId());
        sqlQuery.append(" ");
        sqlQuery.append("order by a.id asc");

        Query query = getPersistenceManager().newQuery(Query.SQL, sqlQuery.toString());
        query.setClass(Account.class);

        @SuppressWarnings("unchecked")
        Collection<Account> result = (Collection<Account>) query.execute();

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getAccountWithIncomingMail(Role role) {
        StringBuffer sql = new StringBuffer();

        sql.append("select * ");
        sql.append("from account a ");
        sql.append("where a.state = 'ACTIVATED' ");
        sql.append(" and  id in ( ");
        sql.append("  select account_id from document , document_attachment ");
        sql.append("        where direction = 'IN' and state='SUCCESSFUL' and (type_of_dispatch in ('MAIL','FAX','EMAIL') and approval_state = 'UNCHECKED' or type = 'VIDEOIDENT' and (approval_state = 'UNCHECKED' or approval_state is null)) and document.id = document_attachment.document_id ");
        sql.append(" ) ");
        sql.append("and exists (select id from account_role ar where ar.account_id = a.id and ar.name='").append(role.name()).append("') ");
        sql.append("order by a.id asc ");

        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(Account.class);

        @SuppressWarnings("unchecked")
        Collection<Account> result = (Collection<Account>) query.execute();

        return result;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getIsCurrentlyDelayedPayer(Account account) {

        StringBuilder builder = new StringBuilder();
        builder.append("select ");
        if (account != null) {
            builder.append("distinct ");
        }
        builder.append("credit.account_id from transaction tu, bank_account credit , dunning_state ds");
        builder.append(" where tu.state in ('OPEN')");
        builder.append(" and credit.type = 'KNK'");
        builder.append(" and tu.credit_account = credit.id");
        builder.append(" and credit.account_id = ");

        if (account == null) {
            builder.append("a.id");
        }
        else {
            builder.append("?");
        }

        builder.append(" and ds.transaction_id = tu.id and ds.level >= 2 ");


        String delayedPayersSelect = builder.toString();
        Query query = getPersistenceManager().newQuery(Query.SQL, delayedPayersSelect);
        query.setUnique(Boolean.TRUE);
        query.setResultClass(Long.class);

        final Long result = (Long) query.execute(account.getId());
        if (result != null) {
            LOGGER.info(account + " is currently a late payer!");
        }
        return result != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getAccountsForRepaymentBreak(Account account) {
        StringBuilder sb = new StringBuilder();
        sb.append("select distinct(a.id) as id from account a, document d, repayment_break rb left outer join approval on approval.repayment_break_id = rb.id ");
        sb.append("where ");
        sb.append("1=1 ");
        sb.append("and d.account_id = a.id ");
        sb.append("and d.direction = 'IN' ");
        sb.append("and d.type = 'REPAYMENT_BREAK_FORM' ");
        sb.append("and d.state = 'SUCCESSFUL' ");
        sb.append("and d.approval_state = 'OK' ");
        sb.append("and rb.form_document_id = d.id ");
        sb.append("and approval.id is null ");
        sb.append("and ");
        sb.append("(select count(*) ");
        sb.append("from transaction t, bank_account ba, dunning_state ds ");
        sb.append("where ");
        sb.append("t.credit_account = ba.id ");
        sb.append("and ba.account_id = a.id ");
        sb.append("and ds.transaction_id = t.id ");
        sb.append("and ds.level >=0 ");
        sb.append("and t.state = 'OPEN' ");
        sb.append("and t.type = 'TRANSFER' ");
        sb.append(") = 0");
        Query query = getPersistenceManager().newQuery(Query.SQL, sb.toString());

        query.setClass(Account.class);

        @SuppressWarnings("unchecked")
        Collection<Account> result = (Collection<Account>) query.execute();

        return result;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getAccountsForMissingDocumentsReminderMail(int days) {
        final Query q = getPersistenceManager().newQuery(Query.SQL, MISSING_DOCUMENTS_SQL);
        q.setClass(Account.class);

        Date now = CurrentDate.getDate();
        Date limitDateBottom = DateUtils.addDays(now, -days);
        limitDateBottom = DateUtils.clean(limitDateBottom);
        Date limitDateTop = DateUtils.addDays(now, -(days - 1));
        limitDateTop = DateUtils.clean(limitDateTop);

        Map<String, Object> parameters = new HashMap<String, Object>(2);
        parameters.put("bottom", new Timestamp(limitDateBottom.getTime()));
        parameters.put("top", new Timestamp(limitDateTop.getTime()));

        return (Collection<Account>) q.executeWithMap(parameters);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Long> getAccountIdsWithActivityFee(final Set<String> contractStates, final String bankAccountType) {
        final Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("bankAccountType", bankAccountType);
        params.put("contractStates", contractStates);
        final Collection<Long> rawResult = executeNamedQuery(Account.class, "getBorrowerAccountIdsForActivityFeeCalculation", params, null, false);
        return new LinkedHashSet<Long>(rawResult);
    }


    @Override
    public Collection<Account> getAccountsForDeletionOfDocumentFiles() {

        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountsWithDeletableDocuments");

        Collection<Account> result = (Collection<Account>) query.execute();

        return result;
    }

    private Date getStartTimeFromDate(Date date) {
        Calendar start = CurrentDate.getCalendar();
        start.setTime(date);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        return start.getTime();
    }

    private Date getEndTimeFromDate(Date date) {
        Calendar end = CurrentDate.getCalendar();
        end.setTime(date);
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 29);
        end.set(Calendar.SECOND, 59);
        end.set(Calendar.MILLISECOND, 999);
        return end.getTime();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getAccountsByPersonNames(String firstName, String lastName, AbstractListCommand<Account> pageAndSortable) {
        LOGGER.debug("try to find accounts for names: " + firstName + ", " + lastName);
        String oql = "_personHistory.contains(person)";
        if (!StringUtils.isEmpty(firstName)) {
            oql += " && person._firstName.toLowerCase().matches('.*" + firstName.toLowerCase(Locale.GERMANY) + ".*') == true";
        }

        if (!StringUtils.isEmpty(lastName)) {
            oql += " && person._lastName.toLowerCase().matches('.*" + lastName.toLowerCase(Locale.GERMANY) + ".*') == true";
        }
        oql = getRestrictionWhereForBoBankuserAccountSearch(oql);
        return findAccountList(oql, pageAndSortable, pageAndSortable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Account> getAccountsByUsername(String username, AbstractListCommand<Account> pageAndSortable) {
        LOGGER.debug("try to find accounts for username: " + username);
        Collection<Account> result = new ArrayList<Account>();
        if (StringUtils.isNotEmpty(username)) {
            String jdoqlWhere = "_username.toLowerCase().matches('.*" + username.trim().toLowerCase(Locale.GERMANY) + ".*') == true";
            jdoqlWhere = getRestrictionWhereForBoBankuserAccountSearch(jdoqlWhere);
            result = findAccountList(jdoqlWhere, pageAndSortable, pageAndSortable);
        }
        LOGGER.debug("found " + result.size() + " accounts with username = " + username);
        return result;
    }


    /**
     * WEBSITE-9388
     * extends an JDOQL Query that searches for accounts (in BO) to only show bankusers accounts that belong to bank
     *
     * @param whereClause for an query that searches for accounts
     * @return
     */
    private String getRestrictionWhereForBoBankuserAccountSearch(final String whereClause) {

        //sort out contrary accounts for bank users
        SmavaUserDetailsIf loggedInBoUser = (SmavaUserDetailsIf) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String where = whereClause;

        for (GrantedAuthority titty : loggedInBoUser.getAuthorities()) {
            if (titty.getAuthority().equals(Role.ROLE_BANK_BIW.name())) {
                where += " && (this._bankAccounts.contains(ba) && ba._valid == true && ba._expirationDate == null && ba._provider._name ==  \"biw\")";
            }
            if (titty.getAuthority().equals(Role.ROLE_BANK_FIDOR.name())) {
                where += " && (this._bankAccounts.contains(ba) && ba._valid == true && ba._expirationDate == null && ba._provider._name ==  \"fidor\")";
            }

        }
        return where;
    }

    /**
     * Search account entity by first name, last name, mail address and date of birth
     *
     * @param firstName
     * @param lastName
     * @param mail
     * @return account or NULL
     */
    @Override
    public Account findAccount(String firstName, String lastName, String mail, Date dateOfBirth) {
        final DateTime dateTime = new DateTime(dateOfBirth);
        LOGGER.debug("try to find accounts for names: [" + firstName + ", " + lastName + "], mail [" + mail + "] and date of birth [" + dateTime.toString("dd.MM.yyyy") + "]");

        final Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, mail);
        params.put(2, firstName);
        params.put(3, lastName);
        params.put(4, new Timestamp(dateTime.withTime(0, 0, 0, 0).getMillis()));
        params.put(5, new Timestamp(dateTime.withTime(23, 59, 59, 99).getMillis()));

        final Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByEmailAndPersonName");
        Collection<Account> accounts = (Collection <Account>) query.executeWithMap(params);

        if(accounts == null || accounts.isEmpty()){
            return null;
        }

        return accounts.iterator().next();
    }

    /**
     * This generic method is written as part of the fix for WEBSITE-17166.1
     * return accounts with allowed roles that are not expired and excludes accounts which have one of the
     * excluded roles which is not expired.
     *
     * Additionally performs check if account is active or not.
     *
     * this implementation is based on named query execution. If one of those set is empty or null it will be replaced
     * with list containing empty string.
     *
     * @param allowedRoles - should not be empty, if empty result list will be empty too
     * @param excludedRoles
     * @return
     */
    public List<Account> findActiveAccountsByRoles(List<String> allowedRoles, List<String> excludedRoles) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findActiveAccountsByRoles");
        List<String> aRoles = allowedRoles;
        List<String> eRoles = excludedRoles;
        if (allowedRoles == null || allowedRoles.isEmpty()) {
            aRoles = new ArrayList<String>();
        }
        if (eRoles == null || eRoles.isEmpty()) {
            eRoles = new ArrayList<String>();
        }

        Map<String,List<String>> params = new HashMap<String,List<String>> ();
        params.put("includedRoles",aRoles);
        params.put("excludedRoles",eRoles);
        return (List<Account>) query.executeWithMap(params);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Account> findAllAccountsByPersonalAdvisorId(Long advisorId) {
        Query query = getPersistenceManager().newQuery(Account.class);
        query.setFilter("this._personalAdvisorId == :advisorId");

        return (List<Account>) query.execute(advisorId);
    }

    @Override
    public Long getSmavaId(Long id) {
        Query query = getPersistenceManager().newQuery(Query.SQL,
                "select account_id from account where id = ?");
        query.setResultClass(Long.class);
        query.setUnique(true);
        Map<Integer, Object> params = Collections.<Integer, Object>singletonMap(1, id);

        return (Long) query.executeWithMap(params);
    }

}
