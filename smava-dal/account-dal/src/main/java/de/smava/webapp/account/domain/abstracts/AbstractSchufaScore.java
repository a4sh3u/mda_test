package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;

import java.util.*;

import de.smava.webapp.account.domain.interfaces.SchufaScoreEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'SchufaScores'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractSchufaScore
    extends KreditPrivatEntity implements SchufaScoreEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaScore.class);

    private static final long serialVersionUID = -6684956849255792087L;

    public static final String TYPE_MANUALLY_SET = "setManually";
    /**  only used in test sources. */
    public static final String TYPE_AUTOMATIC_SET = "setAutomatically";
    public static final String TYPE_FORCE_REQUEST = "requestForced";
    public static final String TYPE_AUTOMATIC_REQUEST = "requestedAutomatically";

    public static final String TYPE_MANUALLY_REQUEST = "requestedManually";
    public static final String TYPE_MANUALLY_REQUEST_SILENT = "requestedManually_silent";

    //Identifies new score sent by Schufa that we use to calculate our composite score
    public static final String SMAVA_SCORECARD_2011 = "SMAVA SCORECARD 2011";

    public static final String SMAVA_SCORECARD_OLD = "Spezialkreditinstitut";

    public static final String SMAVA_SCORECARD_UNKNOWN = "unknown";


    /** successful request. */
    public static final String STATE_SUCCESS = "success";
    /** unsuccessful request with after treatment. */
    public static final String STATE_OPEN_AUTOMATICALLY = "open_automatically";
    /** errors in request or timeout. */
    public static final String STATE_ERROR = "error";
    /** for successful aftertreatment. */
    public static final String STATE_PROVISIONAL_SUCCESS = "provisional_success";
    /** no score + negative properties. */
    public static final String STATE_FAILED = "failed";
    /** no score, schufa does not know anything about the customer. */
    public static final String STATE_UNKNOWN = "unknown";
    /** manuelle nachbehandlung (telefonischer R�ckruf erforderlich). */
    public static final String STATE_OPEN_MANUALLY = "open_manually";

    public static final String GENDER_MALE = "MAENNLICH";
    public static final String GENDER_FEMALE = "WEIBLICH";
    public static final String GENDER_UNKOWN = "UNBEKANNT";

    public void addSchufaScoreToken(SchufaScoreToken scoreToken) {

        if (getTokens() == null) {
            setTokens(new ArrayList<SchufaScoreToken>());
        }
        if (scoreToken.getScore() != this) {
            scoreToken.setScore(this.instance());
        }
        getTokens().add(scoreToken);
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        if (getAccount() != null) {
            list.add(getAccount());
        }
        return list;

    }

    public abstract SchufaScore instance();

}

