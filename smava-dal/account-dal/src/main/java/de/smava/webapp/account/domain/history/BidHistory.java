package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBid;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Bids'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BidHistory extends AbstractBid {

    protected transient String _titleInitVal;
    protected transient boolean _titleIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _updateDateInitVal;
    protected transient boolean _updateDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient Date _lastMarketplaceDateInitVal;
    protected transient boolean _lastMarketplaceDateIsSet;


	
    /**
     * Returns the initial value of the property 'title'.
     */
    public String titleInitVal() {
        String result;
        if (_titleIsSet) {
            result = _titleInitVal;
        } else {
            result = getTitle();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'title'.
     */
    public boolean titleIsDirty() {
        return !valuesAreEqual(titleInitVal(), getTitle());
    }

    /**
     * Returns true if the setter method was called for the property 'title'.
     */
    public boolean titleIsSet() {
        return _titleIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'update date'.
     */
    public Date updateDateInitVal() {
        Date result;
        if (_updateDateIsSet) {
            result = _updateDateInitVal;
        } else {
            result = getUpdateDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'update date'.
     */
    public boolean updateDateIsDirty() {
        return !valuesAreEqual(updateDateInitVal(), getUpdateDate());
    }

    /**
     * Returns true if the setter method was called for the property 'update date'.
     */
    public boolean updateDateIsSet() {
        return _updateDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'last marketplace date'.
     */
    public Date lastMarketplaceDateInitVal() {
        Date result;
        if (_lastMarketplaceDateIsSet) {
            result = _lastMarketplaceDateInitVal;
        } else {
            result = getLastMarketplaceDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last marketplace date'.
     */
    public boolean lastMarketplaceDateIsDirty() {
        return !valuesAreEqual(lastMarketplaceDateInitVal(), getLastMarketplaceDate());
    }

    /**
     * Returns true if the setter method was called for the property 'last marketplace date'.
     */
    public boolean lastMarketplaceDateIsSet() {
        return _lastMarketplaceDateIsSet;
    }

}
