package de.smava.webapp.account.domain.interfaces;


import java.util.Date;


/**
 * The domain object that represents 'BookingGroups'.
 *
 * @author generator
 */
public interface BookingGroupEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'date'.
     *
     * 
     *
     */
    void setDate(Date date);

    /**
     * Returns the property 'date'.
     *
     * 
     *
     */
    Date getDate();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'market'.
     *
     * 
     *
     */
    void setMarket(String market);

    /**
     * Returns the property 'market'.
     *
     * 
     *
     */
    String getMarket();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'specifier'.
     *
     * 
     *
     */
    void setSpecifier(String specifier);

    /**
     * Returns the property 'specifier'.
     *
     * 
     *
     */
    String getSpecifier();

}
