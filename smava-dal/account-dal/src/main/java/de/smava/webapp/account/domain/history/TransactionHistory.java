package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractTransaction;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Transactions'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class TransactionHistory extends AbstractTransaction {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _transactionDateInitVal;
    protected transient boolean _transactionDateIsSet;
    protected transient Date _dueDateInitVal;
    protected transient boolean _dueDateIsSet;
    protected transient Date _modifiedDateInitVal;
    protected transient boolean _modifiedDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient String _externalTransactionIdInitVal;
    protected transient boolean _externalTransactionIdIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'transaction date'.
     */
    public Date transactionDateInitVal() {
        Date result;
        if (_transactionDateIsSet) {
            result = _transactionDateInitVal;
        } else {
            result = getTransactionDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'transaction date'.
     */
    public boolean transactionDateIsDirty() {
        return !valuesAreEqual(transactionDateInitVal(), getTransactionDate());
    }

    /**
     * Returns true if the setter method was called for the property 'transaction date'.
     */
    public boolean transactionDateIsSet() {
        return _transactionDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'due date'.
     */
    public Date dueDateInitVal() {
        Date result;
        if (_dueDateIsSet) {
            result = _dueDateInitVal;
        } else {
            result = getDueDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'due date'.
     */
    public boolean dueDateIsDirty() {
        return !valuesAreEqual(dueDateInitVal(), getDueDate());
    }

    /**
     * Returns true if the setter method was called for the property 'due date'.
     */
    public boolean dueDateIsSet() {
        return _dueDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'modified date'.
     */
    public Date modifiedDateInitVal() {
        Date result;
        if (_modifiedDateIsSet) {
            result = _modifiedDateInitVal;
        } else {
            result = getModifiedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'modified date'.
     */
    public boolean modifiedDateIsDirty() {
        return !valuesAreEqual(modifiedDateInitVal(), getModifiedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'modified date'.
     */
    public boolean modifiedDateIsSet() {
        return _modifiedDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
			
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
					
    /**
     * Returns the initial value of the property 'external transaction id'.
     */
    public String externalTransactionIdInitVal() {
        String result;
        if (_externalTransactionIdIsSet) {
            result = _externalTransactionIdInitVal;
        } else {
            result = getExternalTransactionId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external transaction id'.
     */
    public boolean externalTransactionIdIsDirty() {
        return !valuesAreEqual(externalTransactionIdInitVal(), getExternalTransactionId());
    }

    /**
     * Returns true if the setter method was called for the property 'external transaction id'.
     */
    public boolean externalTransactionIdIsSet() {
        return _externalTransactionIdIsSet;
    }
	
}
