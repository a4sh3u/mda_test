//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk-createOrderFlow\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk-createOrderFlow\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi request)}
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Synchronization;


import de.smava.webapp.account.domain.RdiRequest;
import de.smava.webapp.account.domain.RdiResponse;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RdiRequests'.
 *
 * @author generator
 */
public interface RdiRequestDao extends BaseDao<RdiRequest>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the rdi request identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RdiRequest getRdiRequest(Long id);

    /**
     * Saves the rdi request.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRdiRequest(RdiRequest rdiRequest);

    /**
     * Deletes an rdi request, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi request
     */
    void deleteRdiRequest(Long id);

    /**
     * Retrieves all 'RdiRequest' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RdiRequest> getRdiRequestList();

    /**
     * Retrieves a page of 'RdiRequest' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RdiRequest> getRdiRequestList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RdiRequest' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RdiRequest> getRdiRequestList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RdiRequest' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RdiRequest> getRdiRequestList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'RdiRequest' instances.
     */
    long getRdiRequestCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi request)}
    //
    // insert custom methods here
    //
    
    /**
	 * Tries to find corresponding request to the given response.
	 * Search parameters: method, given request date (comes from ftp filename), contract number.
	 */
	RdiRequest getRequestByResponse(RdiResponse response, Date requestDate, Long contractNumber);
	
	boolean exists(Long id);
    // !!!!!!!! End of insert code section !!!!!!!!
}
