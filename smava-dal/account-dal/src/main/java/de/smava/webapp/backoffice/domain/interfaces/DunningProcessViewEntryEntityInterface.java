package de.smava.webapp.backoffice.domain.interfaces;

import de.smava.webapp.backoffice.domain.DunningProcessViewEntry;

import java.util.Date;

/**
 * The domain object that represents 'DunningProcessViewEntrys'.
 *
 * @author generator
 */
public interface DunningProcessViewEntryEntityInterface {

    /**
     * Setter for the property 'id'.
     *
     * 
     *
     */
    public void setId(Long id);

    /**
     * Returns the property 'id'.
     *
     * 
     *
     */
    public Long getId();
    /**
     * Setter for the property 'group id'.
     *
     * 
     *
     */
    public void setGroupId(Long groupId);

    /**
     * Returns the property 'group id'.
     *
     * 
     *
     */
    public Long getGroupId();
    /**
     * Setter for the property 'arrear counter'.
     *
     * 
     *
     */
    public void setArrearCounter(Integer arrearCounter);

    /**
     * Returns the property 'arrear counter'.
     *
     * 
     *
     */
    public Integer getArrearCounter();
    /**
     * Setter for the property 'rkv'.
     *
     * 
     *
     */
    public void setRkv(String rkv);

    /**
     * Returns the property 'rkv'.
     *
     * 
     *
     */
    public String getRkv();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public double getAmount();
    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    public void setStartDate(Date startDate);

    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    public Date getStartDate();
    /**
     * Setter for the property 'transaction amount'.
     *
     * 
     *
     */
    public void setTransactionAmount(double transactionAmount);

    /**
     * Returns the property 'transaction amount'.
     *
     * 
     *
     */
    public double getTransactionAmount();
    /**
     * Setter for the property 'transaction due date'.
     *
     * 
     *
     */
    public void setTransactionDueDate(Date transactionDueDate);

    /**
     * Returns the property 'transaction due date'.
     *
     * 
     *
     */
    public Date getTransactionDueDate();
    /**
     * Setter for the property 'level'.
     *
     * 
     *
     */
    public void setLevel(Integer level);

    /**
     * Returns the property 'level'.
     *
     * 
     *
     */
    public Integer getLevel();
    /**
     * Setter for the property 'acc id'.
     *
     * 
     *
     */
    public void setAccId(Long accId);

    /**
     * Returns the property 'acc id'.
     *
     * 
     *
     */
    public Long getAccId();
    /**
     * Setter for the property 'account id'.
     *
     * 
     *
     */
    public void setAccountId(Long accountId);

    /**
     * Returns the property 'account id'.
     *
     * 
     *
     */
    public Long getAccountId();
    /**
     * Setter for the property 'user name'.
     *
     * 
     *
     */
    public void setUserName(String userName);

    /**
     * Returns the property 'user name'.
     *
     * 
     *
     */
    public String getUserName();
    /**
     * Setter for the property 'credit term'.
     *
     * 
     *
     */
    public void setCreditTerm(String creditTerm);

    /**
     * Returns the property 'credit term'.
     *
     * 
     *
     */
    public String getCreditTerm();
    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    public void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    public String getRating();
    /**
     * Setter for the property 'age'.
     *
     * 
     *
     */
    public void setAge(Integer age);

    /**
     * Returns the property 'age'.
     *
     * 
     *
     */
    public Integer getAge();
    /**
     * Setter for the property 'occupation'.
     *
     * 
     *
     */
    public void setOccupation(String occupation);

    /**
     * Returns the property 'occupation'.
     *
     * 
     *
     */
    public String getOccupation();
    /**
     * Helper method to get reference of this object as model type.
     */
    public DunningProcessViewEntry asDunningProcessViewEntry();
}
