package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CustomerAgreementHistory;

import java.util.Date;

/**
 * The domain object that represents 'CustomerAgreements'.
 */
public class CustomerAgreement extends CustomerAgreementHistory  {

        protected Date _agreementDate;
        protected Date _revocationDate;
        protected CustomerAgreementType _type;
        protected Account _account;
        protected Long _affiliateInformationId;
        
                            /**
     * Setter for the property 'agreement date'.
     */
    public void setAgreementDate(Date agreementDate) {
        if (!_agreementDateIsSet) {
            _agreementDateIsSet = true;
            _agreementDateInitVal = getAgreementDate();
        }
        registerChange("agreement date", _agreementDateInitVal, agreementDate);
        _agreementDate = agreementDate;
    }
                        
    /**
     * Returns the property 'agreement date'.
     */
    public Date getAgreementDate() {
        return _agreementDate;
    }
                                    /**
     * Setter for the property 'revocation date'.
     */
    public void setRevocationDate(Date revocationDate) {
        if (!_revocationDateIsSet) {
            _revocationDateIsSet = true;
            _revocationDateInitVal = getRevocationDate();
        }
        registerChange("revocation date", _revocationDateInitVal, revocationDate);
        _revocationDate = revocationDate;
    }
                        
    /**
     * Returns the property 'revocation date'.
     */
    public Date getRevocationDate() {
        return _revocationDate;
    }
                                            
    /**
     * Setter for the property 'type'.
     */
    public void setType(CustomerAgreementType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     */
    public CustomerAgreementType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'affiliate information id'.
     */
    public void setAffiliateInformationId(Long affiliateInformationId) {
        _affiliateInformationId = affiliateInformationId;
    }
            
    /**
     * Returns the property 'affiliate information id'.
     */
    public Long getAffiliateInformationId() {
        return _affiliateInformationId;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CustomerAgreement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(CustomerAgreement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
