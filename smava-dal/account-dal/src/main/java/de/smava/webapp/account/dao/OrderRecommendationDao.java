//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(order recommendation)}

import de.smava.webapp.account.domain.OrderRecommendation;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'OrderRecommendations'.
 *
 * @author generator
 */
public interface OrderRecommendationDao extends BaseDao<OrderRecommendation>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the order recommendation identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    OrderRecommendation getOrderRecommendation(Long id);

    /**
     * Saves the order recommendation.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveOrderRecommendation(OrderRecommendation orderRecommendation);

    /**
     * Deletes an order recommendation, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the order recommendation
     */
    void deleteOrderRecommendation(Long id);

    /**
     * Retrieves all 'OrderRecommendation' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<OrderRecommendation> getOrderRecommendationList();

    /**
     * Retrieves a page of 'OrderRecommendation' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<OrderRecommendation> getOrderRecommendationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'OrderRecommendation' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<OrderRecommendation> getOrderRecommendationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'OrderRecommendation' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<OrderRecommendation> getOrderRecommendationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'OrderRecommendation' instances.
     */
    long getOrderRecommendationCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(order recommendation)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
