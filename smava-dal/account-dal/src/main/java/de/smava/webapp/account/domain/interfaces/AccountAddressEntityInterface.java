package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;


/**
 * The domain object that represents 'AccountAddresss'.
 *
 * @author generator
 */
public interface AccountAddressEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();

}
