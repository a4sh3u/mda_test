package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;

import java.util.*;

import de.smava.webapp.account.domain.interfaces.IdCardEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'IdCards'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractIdCard
    extends KreditPrivatEntity implements IdCardEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractIdCard.class);

    private transient Set<IdCardRejectReasons> _rejectReasons;

    public Set<IdCardRejectReasons> getRejectReasons() {
        return Collections.unmodifiableSet(getRejectReasonsInternal());
    }

    private Set<IdCardRejectReasons> getRejectReasonsInternal() {
        if (_rejectReasons == null) {
            _rejectReasons = new HashSet<IdCardRejectReasons>();
            final String desc = getDescriptions();
            if (StringUtils.isNotEmpty(desc)) {
                for (String reason : StringUtils.split(getDescriptions(), ",")) {
                    _rejectReasons.add(IdCardRejectReasons.valueOf(reason));
                }
            }
        }
        return _rejectReasons;
    }

    public boolean addRejectReason(final IdCardRejectReasons reason) {
        final Set<IdCardRejectReasons> reasons = getRejectReasonsInternal();
        final boolean added = reasons.add(reason);
        if (added) {
            final StringBuilder sb = new StringBuilder(StringUtils.defaultString(getDescriptions()));
            if (sb.length() > 0) {
                sb.append(",");
            }
            sb.append(reason.name());
            setDescriptions(sb.toString());
        }
        return added;
    }

    public void addAll(Collection<IdCardRejectReasons> reasons) {
        if (CollectionUtils.isNotEmpty(reasons)) {
            for (IdCardRejectReasons reason : reasons) {
                addRejectReason(reason);
            }
        }
    }

    public List<? extends Entity> getModifier() {
        return Collections.singletonList(getPerson().getAccount());
    }

}

