package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DtausFileHistory;

import java.util.Date;
import java.util.List;

/**
 * The domain object that represents 'DtausFiles'.
 *
 * @author generator
 */
public class DtausFile extends DtausFileHistory  {

        protected String _name;
        protected Date _creationDate;
        protected Date _dueDate;
        protected String _transactionsType;
        protected Date _releaseDate;
        protected byte[] _content;
        protected byte[] _originalContent;
        protected String _state;
        protected String _direction;
        protected String _description;
        protected String _note;
        protected List<DtausFileOutTransaction> _dtausFileOutTransactions;
        protected List<DtausEntry> _dtausEntries;

                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'due date'.
     */
    public void setDueDate(Date dueDate) {
        if (!_dueDateIsSet) {
            _dueDateIsSet = true;
            _dueDateInitVal = getDueDate();
        }
        registerChange("due date", _dueDateInitVal, dueDate);
        _dueDate = dueDate;
    }
                        
    /**
     * Returns the property 'due date'.
     */
    public Date getDueDate() {
        return _dueDate;
    }
                                    /**
     * Setter for the property 'transactions type'.
     */
    public void setTransactionsType(String transactionsType) {
        if (!_transactionsTypeIsSet) {
            _transactionsTypeIsSet = true;
            _transactionsTypeInitVal = getTransactionsType();
        }
        registerChange("transactions type", _transactionsTypeInitVal, transactionsType);
        _transactionsType = transactionsType;
    }
                        
    /**
     * Returns the property 'transactions type'.
     */
    public String getTransactionsType() {
        return _transactionsType;
    }
                                    /**
     * Setter for the property 'release date'.
     */
    public void setReleaseDate(Date releaseDate) {
        if (!_releaseDateIsSet) {
            _releaseDateIsSet = true;
            _releaseDateInitVal = getReleaseDate();
        }
        registerChange("release date", _releaseDateInitVal, releaseDate);
        _releaseDate = releaseDate;
    }
                        
    /**
     * Returns the property 'release date'.
     */
    public Date getReleaseDate() {
        return _releaseDate;
    }
                                            
    /**
     * Setter for the property 'content'.
     */
    public void setContent(byte[] content) {
        _content = content;
    }
            
    /**
     * Returns the property 'content'.
     */
    public byte[] getContent() {
        return _content;
    }
                                            
    /**
     * Setter for the property 'original content'.
     */
    public void setOriginalContent(byte[] originalContent) {
        _originalContent = originalContent;
    }
            
    /**
     * Returns the property 'original content'.
     */
    public byte[] getOriginalContent() {
        return _originalContent;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'direction'.
     */
    public void setDirection(String direction) {
        if (!_directionIsSet) {
            _directionIsSet = true;
            _directionInitVal = getDirection();
        }
        registerChange("direction", _directionInitVal, direction);
        _direction = direction;
    }
                        
    /**
     * Returns the property 'direction'.
     */
    public String getDirection() {
        return _direction;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'note'.
     */
    public void setNote(String note) {
        if (!_noteIsSet) {
            _noteIsSet = true;
            _noteInitVal = getNote();
        }
        registerChange("note", _noteInitVal, note);
        _note = note;
    }
                        
    /**
     * Returns the property 'note'.
     */
    public String getNote() {
        return _note;
    }
                                            
    /**
     * Setter for the property 'dtausFileOutTransactions'.
     */
    public void setDtausFileOutTransactions(List<DtausFileOutTransaction> dtausFileOutTransactions) {
        _dtausFileOutTransactions = dtausFileOutTransactions;
    }
            
    /**
     * Returns the property 'dtausFileOutTransactions'.
     */
    public List<DtausFileOutTransaction> getDtausFileOutTransactions() {
        return _dtausFileOutTransactions;
    }
                                            
    /**
     * Setter for the property 'dtaus entries'.
     */
    public void setDtausEntries(List<DtausEntry> dtausEntries) {
        _dtausEntries = dtausEntries;
    }
            
    /**
     * Returns the property 'dtaus entries'.
     */
    public List<DtausEntry> getDtausEntries() {
        return _dtausEntries;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DtausFile.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _transactionsType=").append(_transactionsType);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _direction=").append(_direction);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _note=").append(_note);
            builder.append("\n}");
        } else {
            builder.append(DtausFile.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
