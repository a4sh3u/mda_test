//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi request)}

import de.smava.webapp.account.domain.RdiRequest;
import de.smava.webapp.account.domain.RdiResponse;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.account.dao.RdiRequestDao;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RdiRequests'.
 *
 * @author generator
 */
@Repository(value = "rdiRequestDao")
public class JdoRdiRequestDao extends JdoBaseDao implements RdiRequestDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRdiRequestDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rdi request)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the rdi request identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RdiRequest load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequest - start: id=" + id);
        }
        RdiRequest result = getEntity(RdiRequest.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequest - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RdiRequest getRdiRequest(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            RdiRequest entity = findUniqueEntity(RdiRequest.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the rdi request.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RdiRequest rdiRequest) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRdiRequest: " + rdiRequest);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rdi request)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(rdiRequest);
    }

    /**
     * @deprecated Use {@link #save(RdiRequest) instead}
     */
    public Long saveRdiRequest(RdiRequest rdiRequest) {
        return save(rdiRequest);
    }

    /**
     * Deletes an rdi request, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi request
     */
    public void deleteRdiRequest(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRdiRequest: " + id);
        }
        deleteEntity(RdiRequest.class, id);
    }

    /**
     * Retrieves all 'RdiRequest' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RdiRequest> getRdiRequestList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - start");
        }
        Collection<RdiRequest> result = getEntities(RdiRequest.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RdiRequest' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RdiRequest> getRdiRequestList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - start: pageable=" + pageable);
        }
        Collection<RdiRequest> result = getEntities(RdiRequest.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiRequest' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RdiRequest> getRdiRequestList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - start: sortable=" + sortable);
        }
        Collection<RdiRequest> result = getEntities(RdiRequest.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiRequest' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RdiRequest> getRdiRequestList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiRequest> result = getEntities(RdiRequest.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiRequest' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiRequest> findRdiRequestList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - start: whereClause=" + whereClause);
        }
        Collection<RdiRequest> result = findEntities(RdiRequest.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiRequest' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RdiRequest> findRdiRequestList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RdiRequest> result = findEntities(RdiRequest.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RdiRequest' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiRequest> findRdiRequestList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RdiRequest> result = findEntities(RdiRequest.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiRequest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiRequest> findRdiRequestList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RdiRequest> result = findEntities(RdiRequest.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiRequest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiRequest> findRdiRequestList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiRequest> result = findEntities(RdiRequest.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiRequest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiRequest> findRdiRequestList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiRequest> result = findEntities(RdiRequest.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RdiRequest' instances.
     */
    public long getRdiRequestCount() {
        long result = getEntityCount(RdiRequest.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiRequest' instances which match the given whereClause.
     */
    public long getRdiRequestCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RdiRequest.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiRequest' instances which match the given whereClause together with params specified in object array.
     */
    public long getRdiRequestCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RdiRequest.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiRequestCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi request)}
    //
    // insert custom methods here
    //
    /* (non-Javadoc)
     * @see de.smava.webapp.account.dao.RdiRequestDao#getRequestByResponse(de.smava.webapp.account.domain.RdiResponse, java.util.Date)
     */
    @SuppressWarnings("unchecked")
    public RdiRequest getRequestByResponse(RdiResponse response, Date requestDate, Long contractNumber) {
        RdiRequest result = null;
        if (requestDate != null && contractNumber != null) {
            Date upperLimitDate = DateUtils.addMinutes(requestDate, 1);

            Query query = getPersistenceManager().newNamedQuery(RdiRequest.class, "findRequestByCreationDate");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("requestDate", requestDate);
            params.put("upperLimitDate", upperLimitDate);
            params.put("contractNumber", contractNumber);

            Collection<RdiRequest> allRequests = (Collection<RdiRequest>) query.executeWithMap(params);

            Collection<RdiRequest> requests = new ArrayList<RdiRequest>();
            for (RdiRequest request : allRequests) {
                if (request.getMethod().equals(response.getMethod())) {
                    requests.add(request);
                }
            }

            if (requests.isEmpty()) {
                LOGGER.warn("no rdi request entity found for rdi response " + response.getId());
            } else if (requests.size() > 1) {
                LOGGER.warn("too many rdi request entities found for rdi response " + response.getId());
            } else {
                result = requests.iterator().next();
            }
        } else {
            if (requestDate == null) {
                LOGGER.error("no request date found for rdi response " + response.getProcessId());
            }

            if (StringUtils.isEmpty(response.getContractNumber())) {
                LOGGER.error("no contract number found in rdi response");
            }
        }
        return result;
    }
    
    // !!!!!!!! End of insert code section !!!!!!!!
}
