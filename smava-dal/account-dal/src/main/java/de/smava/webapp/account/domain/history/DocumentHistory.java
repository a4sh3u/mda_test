package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractDocument;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Documents'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DocumentHistory extends AbstractDocument {

    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _approvalStateInitVal;
    protected transient boolean _approvalStateIsSet;
    protected transient String _subjectInitVal;
    protected transient boolean _subjectIsSet;
    protected transient String _textInitVal;
    protected transient boolean _textIsSet;
    protected transient String _notesInitVal;
    protected transient boolean _notesIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _dateInitVal;
    protected transient boolean _dateIsSet;
    protected transient String _directionInitVal;
    protected transient boolean _directionIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _typeOfDispatchInitVal;
    protected transient boolean _typeOfDispatchIsSet;
    protected transient String _emailInitVal;
    protected transient boolean _emailIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _phoneInitVal;
    protected transient boolean _phoneIsSet;
    protected transient String _faxInitVal;
    protected transient boolean _faxIsSet;
    protected transient String _emailFromInitVal;
    protected transient boolean _emailFromIsSet;
    protected transient String _nameFromInitVal;
    protected transient boolean _nameFromIsSet;
    protected transient String _phoneFromInitVal;
    protected transient boolean _phoneFromIsSet;
    protected transient String _faxFromInitVal;
    protected transient boolean _faxFromIsSet;


	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'approval state'.
     */
    public String approvalStateInitVal() {
        String result;
        if (_approvalStateIsSet) {
            result = _approvalStateInitVal;
        } else {
            result = getApprovalState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'approval state'.
     */
    public boolean approvalStateIsDirty() {
        return !valuesAreEqual(approvalStateInitVal(), getApprovalState());
    }

    /**
     * Returns true if the setter method was called for the property 'approval state'.
     */
    public boolean approvalStateIsSet() {
        return _approvalStateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'subject'.
     */
    public String subjectInitVal() {
        String result;
        if (_subjectIsSet) {
            result = _subjectInitVal;
        } else {
            result = getSubject();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'subject'.
     */
    public boolean subjectIsDirty() {
        return !valuesAreEqual(subjectInitVal(), getSubject());
    }

    /**
     * Returns true if the setter method was called for the property 'subject'.
     */
    public boolean subjectIsSet() {
        return _subjectIsSet;
    }
	
    /**
     * Returns the initial value of the property 'text'.
     */
    public String textInitVal() {
        String result;
        if (_textIsSet) {
            result = _textInitVal;
        } else {
            result = getText();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'text'.
     */
    public boolean textIsDirty() {
        return !valuesAreEqual(textInitVal(), getText());
    }

    /**
     * Returns true if the setter method was called for the property 'text'.
     */
    public boolean textIsSet() {
        return _textIsSet;
    }
	
    /**
     * Returns the initial value of the property 'notes'.
     */
    public String notesInitVal() {
        String result;
        if (_notesIsSet) {
            result = _notesInitVal;
        } else {
            result = getNotes();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'notes'.
     */
    public boolean notesIsDirty() {
        return !valuesAreEqual(notesInitVal(), getNotes());
    }

    /**
     * Returns true if the setter method was called for the property 'notes'.
     */
    public boolean notesIsSet() {
        return _notesIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'date'.
     */
    public Date dateInitVal() {
        Date result;
        if (_dateIsSet) {
            result = _dateInitVal;
        } else {
            result = getDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date'.
     */
    public boolean dateIsDirty() {
        return !valuesAreEqual(dateInitVal(), getDate());
    }

    /**
     * Returns true if the setter method was called for the property 'date'.
     */
    public boolean dateIsSet() {
        return _dateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'direction'.
     */
    public String directionInitVal() {
        String result;
        if (_directionIsSet) {
            result = _directionInitVal;
        } else {
            result = getDirection();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'direction'.
     */
    public boolean directionIsDirty() {
        return !valuesAreEqual(directionInitVal(), getDirection());
    }

    /**
     * Returns true if the setter method was called for the property 'direction'.
     */
    public boolean directionIsSet() {
        return _directionIsSet;
    }
			
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type of dispatch'.
     */
    public String typeOfDispatchInitVal() {
        String result;
        if (_typeOfDispatchIsSet) {
            result = _typeOfDispatchInitVal;
        } else {
            result = getTypeOfDispatch();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type of dispatch'.
     */
    public boolean typeOfDispatchIsDirty() {
        return !valuesAreEqual(typeOfDispatchInitVal(), getTypeOfDispatch());
    }

    /**
     * Returns true if the setter method was called for the property 'type of dispatch'.
     */
    public boolean typeOfDispatchIsSet() {
        return _typeOfDispatchIsSet;
    }
		
    /**
     * Returns the initial value of the property 'email'.
     */
    public String emailInitVal() {
        String result;
        if (_emailIsSet) {
            result = _emailInitVal;
        } else {
            result = getEmail();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email'.
     */
    public boolean emailIsDirty() {
        return !valuesAreEqual(emailInitVal(), getEmail());
    }

    /**
     * Returns true if the setter method was called for the property 'email'.
     */
    public boolean emailIsSet() {
        return _emailIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone'.
     */
    public String phoneInitVal() {
        String result;
        if (_phoneIsSet) {
            result = _phoneInitVal;
        } else {
            result = getPhone();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone'.
     */
    public boolean phoneIsDirty() {
        return !valuesAreEqual(phoneInitVal(), getPhone());
    }

    /**
     * Returns true if the setter method was called for the property 'phone'.
     */
    public boolean phoneIsSet() {
        return _phoneIsSet;
    }
	
    /**
     * Returns the initial value of the property 'fax'.
     */
    public String faxInitVal() {
        String result;
        if (_faxIsSet) {
            result = _faxInitVal;
        } else {
            result = getFax();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'fax'.
     */
    public boolean faxIsDirty() {
        return !valuesAreEqual(faxInitVal(), getFax());
    }

    /**
     * Returns true if the setter method was called for the property 'fax'.
     */
    public boolean faxIsSet() {
        return _faxIsSet;
    }
	
    /**
     * Returns the initial value of the property 'email from'.
     */
    public String emailFromInitVal() {
        String result;
        if (_emailFromIsSet) {
            result = _emailFromInitVal;
        } else {
            result = getEmailFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email from'.
     */
    public boolean emailFromIsDirty() {
        return !valuesAreEqual(emailFromInitVal(), getEmailFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'email from'.
     */
    public boolean emailFromIsSet() {
        return _emailFromIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name from'.
     */
    public String nameFromInitVal() {
        String result;
        if (_nameFromIsSet) {
            result = _nameFromInitVal;
        } else {
            result = getNameFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name from'.
     */
    public boolean nameFromIsDirty() {
        return !valuesAreEqual(nameFromInitVal(), getNameFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'name from'.
     */
    public boolean nameFromIsSet() {
        return _nameFromIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone from'.
     */
    public String phoneFromInitVal() {
        String result;
        if (_phoneFromIsSet) {
            result = _phoneFromInitVal;
        } else {
            result = getPhoneFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone from'.
     */
    public boolean phoneFromIsDirty() {
        return !valuesAreEqual(phoneFromInitVal(), getPhoneFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'phone from'.
     */
    public boolean phoneFromIsSet() {
        return _phoneFromIsSet;
    }
	
    /**
     * Returns the initial value of the property 'fax from'.
     */
    public String faxFromInitVal() {
        String result;
        if (_faxFromIsSet) {
            result = _faxFromInitVal;
        } else {
            result = getFaxFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'fax from'.
     */
    public boolean faxFromIsDirty() {
        return !valuesAreEqual(faxFromInitVal(), getFaxFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'fax from'.
     */
    public boolean faxFromIsSet() {
        return _faxFromIsSet;
    }
					
}
