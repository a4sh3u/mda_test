//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(sector)}

import de.smava.webapp.account.dao.SectorDao;
import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Sectors'.
 *
 * @author generator
 */
@Repository(value = "sectorDao")
public class JdoSectorDao extends JdoBaseDao implements SectorDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSectorDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(sector)}
    private static final long serialVersionUID = -7149177714085474762L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the sector identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Sector load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSector - start: id=" + id);
        }
        Sector result = getEntity(Sector.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSector - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Sector getSector(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Sector entity = findUniqueEntity(Sector.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the sector.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Sector sector) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveSector: " + sector);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(sector)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(sector);
    }

    /**
     * @deprecated Use {@link #save(Sector) instead}
     */
    public Long saveSector(Sector sector) {
        return save(sector);
    }

    /**
     * Deletes an sector, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the sector
     */
    public void deleteSector(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteSector: " + id);
        }
        deleteEntity(Sector.class, id);
    }

    /**
     * Retrieves all 'Sector' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Sector> getSectorList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - start");
        }
        Collection<Sector> result = getEntities(Sector.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Sector' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Sector> getSectorList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - start: pageable=" + pageable);
        }
        Collection<Sector> result = getEntities(Sector.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Sector' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Sector> getSectorList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - start: sortable=" + sortable);
        }
        Collection<Sector> result = getEntities(Sector.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Sector' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Sector> getSectorList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Sector> result = getEntities(Sector.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Sector' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Sector> findSectorList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - start: whereClause=" + whereClause);
        }
        Collection<Sector> result = findEntities(Sector.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Sector' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Sector> findSectorList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Sector> result = findEntities(Sector.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Sector' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Sector> findSectorList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Sector> result = findEntities(Sector.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Sector' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Sector> findSectorList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Sector> result = findEntities(Sector.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Sector' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Sector> findSectorList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Sector> result = findEntities(Sector.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Sector' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Sector> findSectorList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Sector> result = findEntities(Sector.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSectorList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Sector' instances.
     */
    public long getSectorCount() {
        long result = getEntityCount(Sector.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Sector' instances which match the given whereClause.
     */
    public long getSectorCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Sector.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Sector' instances which match the given whereClause together with params specified in object array.
     */
    public long getSectorCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Sector.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSectorCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(sector)}
    //
    

    public Collection<Sector> getRootSectorsDeep() {
    	final Collection<Sector> result = findSectorList("_parent == null && _isEmployeeSector == true ORDER BY _name ascending");
    	loadSectors(result);
        return result;
    }


    public Collection<Sector> getEmployeeSectorsDeep() {
    	final Collection<Sector> result = findSectorList("_isEmployeeSector == true ORDER BY _name ascending"); 
    	loadSectors(result);
        return result; 
    }

    public Collection<Sector> getOfficialSectors() {
        final Collection<Sector> result = findSectorList("_isOfficialSector == true ORDER BY _name ascending");
        loadSectors(result);
        return result;
    }
    
    /**
     * desperate measures. load all sectors in all hierarchies to prevent empty box in personal_data.jsp.
     * Forces lazy loading.
     *
     * @param sectors the sectors to load
     */
    private void loadSectors (Collection<Sector> sectors) {
    	for (Sector sector : sectors) {
    		 sector.getName();
            if (sector.getChildren() != null && !sector.getChildren().isEmpty()) {
                loadSectors(sector.getChildren());
            }
        }
    }

    public Set<Long> getSectorIdsNeededDescription() {
		return new HashSet<Long>((Collection) getPersistenceManager().newNamedQuery(Sector.class, "getSectorIdsNeededDescriptions").execute());
	}
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
