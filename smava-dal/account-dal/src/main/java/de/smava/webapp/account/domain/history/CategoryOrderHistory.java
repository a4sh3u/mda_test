package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractCategoryOrder;




/**
 * The domain object that has all history aggregation related fields for 'CategoryOrders'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CategoryOrderHistory extends AbstractCategoryOrder {

    protected transient boolean _mainInitVal;
    protected transient boolean _mainIsSet;


	
    /**
     * Returns the initial value of the property 'main'.
     */
    public boolean mainInitVal() {
        boolean result;
        if (_mainIsSet) {
            result = _mainInitVal;
        } else {
            result = getMain();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'main'.
     */
    public boolean mainIsDirty() {
        return !valuesAreEqual(mainInitVal(), getMain());
    }

    /**
     * Returns true if the setter method was called for the property 'main'.
     */
    public boolean mainIsSet() {
        return _mainIsSet;
    }
		
}
