//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid search age group)}

import de.smava.webapp.account.dao.BidSearchAgeGroupDao;
import de.smava.webapp.account.domain.BidSearchAgeGroup;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BidSearchAgeGroups'.
 *
 * @author generator
 */
@Repository(value = "bidSearchAgeGroupDao")
public class JdoBidSearchAgeGroupDao extends JdoBaseDao implements BidSearchAgeGroupDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBidSearchAgeGroupDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bid search age group)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bid search age group identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BidSearchAgeGroup load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroup - start: id=" + id);
        }
        BidSearchAgeGroup result = getEntity(BidSearchAgeGroup.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroup - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BidSearchAgeGroup getBidSearchAgeGroup(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BidSearchAgeGroup entity = findUniqueEntity(BidSearchAgeGroup.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the bid search age group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BidSearchAgeGroup bidSearchAgeGroup) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBidSearchAgeGroup: " + bidSearchAgeGroup);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bid search age group)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bidSearchAgeGroup);
    }

    /**
     * @deprecated Use {@link #save(BidSearchAgeGroup) instead}
     */
    public Long saveBidSearchAgeGroup(BidSearchAgeGroup bidSearchAgeGroup) {
        return save(bidSearchAgeGroup);
    }

    /**
     * Deletes an bid search age group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid search age group
     */
    public void deleteBidSearchAgeGroup(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBidSearchAgeGroup: " + id);
        }
        deleteEntity(BidSearchAgeGroup.class, id);
    }

    /**
     * Retrieves all 'BidSearchAgeGroup' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BidSearchAgeGroup> getBidSearchAgeGroupList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - start");
        }
        Collection<BidSearchAgeGroup> result = getEntities(BidSearchAgeGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BidSearchAgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BidSearchAgeGroup> getBidSearchAgeGroupList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - start: pageable=" + pageable);
        }
        Collection<BidSearchAgeGroup> result = getEntities(BidSearchAgeGroup.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidSearchAgeGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BidSearchAgeGroup> getBidSearchAgeGroupList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - start: sortable=" + sortable);
        }
        Collection<BidSearchAgeGroup> result = getEntities(BidSearchAgeGroup.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearchAgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BidSearchAgeGroup> getBidSearchAgeGroupList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearchAgeGroup> result = getEntities(BidSearchAgeGroup.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidSearchAgeGroup' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchAgeGroup> findBidSearchAgeGroupList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - start: whereClause=" + whereClause);
        }
        Collection<BidSearchAgeGroup> result = findEntities(BidSearchAgeGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidSearchAgeGroup' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BidSearchAgeGroup> findBidSearchAgeGroupList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BidSearchAgeGroup> result = findEntities(BidSearchAgeGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BidSearchAgeGroup' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchAgeGroup> findBidSearchAgeGroupList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BidSearchAgeGroup> result = findEntities(BidSearchAgeGroup.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidSearchAgeGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchAgeGroup> findBidSearchAgeGroupList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BidSearchAgeGroup> result = findEntities(BidSearchAgeGroup.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearchAgeGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchAgeGroup> findBidSearchAgeGroupList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearchAgeGroup> result = findEntities(BidSearchAgeGroup.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearchAgeGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchAgeGroup> findBidSearchAgeGroupList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearchAgeGroup> result = findEntities(BidSearchAgeGroup.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchAgeGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearchAgeGroup' instances.
     */
    public long getBidSearchAgeGroupCount() {
        long result = getEntityCount(BidSearchAgeGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearchAgeGroup' instances which match the given whereClause.
     */
    public long getBidSearchAgeGroupCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BidSearchAgeGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearchAgeGroup' instances which match the given whereClause together with params specified in object array.
     */
    public long getBidSearchAgeGroupCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BidSearchAgeGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchAgeGroupCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid search age group)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
