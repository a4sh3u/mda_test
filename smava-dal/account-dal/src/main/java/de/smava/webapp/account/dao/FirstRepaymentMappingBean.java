/**
 * 
 */
package de.smava.webapp.account.dao;

/**
 * FirstRepaymentMappingBean.
 * <br />
 * <br />
 * Creation date: 14.08.2008
 * @author Robert Porscha
 * 
 */
public class FirstRepaymentMappingBean {
	
	private Long _accountId;
	private Double _firstRate;
	private Double _allRates;
	private Long _contractId;
	private Double _initialInterest;
	private Double _activityFee;
	private Double _postidentFee;
	
	public Long getAccountId() {
		return _accountId;
	}
	public void setAccountId(Long accountId) {
		_accountId = accountId;
	}
	public Double getFirstRate() {
		return _firstRate;
	}
	public void setFirstRate(Double firstRate) {
		_firstRate = firstRate;
	}
	public Double getAllRates() {
		return _allRates;
	}
	public void setAllRates(Double allRates) {
		_allRates = allRates;
	}
	public Long getContractId() {
		return _contractId;
	}
	public void setContractId(Long contractId) {
		_contractId = contractId;
	}
	public Double getInitialInterest() {
		return _initialInterest;
	}
	public void setInitialInterest(Double initialInterest) {
		_initialInterest = initialInterest;
	}
	public Double getActivityFee() {
		return _activityFee;
	}
	public void setActivityFee(Double activityFee) {
		_activityFee = activityFee;
	}
	public Double getPostidentFee() {
		return _postidentFee;
	}
	public void setPostidentFee(Double postidentFee) {
		_postidentFee = postidentFee;
	}
}
