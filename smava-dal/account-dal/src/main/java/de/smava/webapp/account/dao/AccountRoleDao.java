package de.smava.webapp.account.dao;

import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.AccountRole;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

/**
 * DAO interface for the domain object 'AccountRoles'.
 */
public interface AccountRoleDao extends BaseDao<AccountRole> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Deletes an account role, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account role
     */
    void deleteAccountRole(Long id);

    /**
     * Retrieves all 'AccountRole' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<AccountRole> getAccountRoleList();

    /**
     * Retrieves a page of 'AccountRole' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<AccountRole> getAccountRoleList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'AccountRole' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<AccountRole> getAccountRoleList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'AccountRole' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<AccountRole> getAccountRoleList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'AccountRole' instances.
     */
    long getAccountRoleCount();

}
