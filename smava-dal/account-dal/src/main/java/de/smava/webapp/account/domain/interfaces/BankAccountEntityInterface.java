package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccountClearingRun;
import de.smava.webapp.account.domain.ConsolidatedDebt;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'BankAccounts'.
 *
 * @author generator
 */
public interface BankAccountEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'bank'.
     *
     * 
     *
     */
    void setBank(String bank);

    /**
     * Returns the property 'bank'.
     *
     * 
     *
     */
    String getBank();
    /**
     * Setter for the property 'account no'.
     *
     * 
     *
     */
    void setAccountNo(String accountNo);

    /**
     * Returns the property 'account no'.
     *
     * 
     *
     */
    String getAccountNo();
    /**
     * Setter for the property 'bank code'.
     *
     * 
     *
     */
    void setBankCode(String bankCode);

    /**
     * Returns the property 'bank code'.
     *
     * 
     *
     */
    String getBankCode();
    /**
     * Setter for the property 'smava only'.
     *
     * 
     *
     */
    void setSmavaOnly(boolean smavaOnly);

    /**
     * Returns the property 'smava only'.
     *
     * 
     *
     */
    boolean getSmavaOnly();
    /**
     * Setter for the property 'debit allowed'.
     *
     * 
     *
     */
    void setDebitAllowed(boolean debitAllowed);

    /**
     * Returns the property 'debit allowed'.
     *
     * 
     *
     */
    boolean getDebitAllowed();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'valid'.
     *
     * 
     *
     */
    void setValid(boolean valid);

    /**
     * Returns the property 'valid'.
     *
     * 
     *
     */
    boolean getValid();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'customer number'.
     *
     * 
     *
     */
    void setCustomerNumber(String customerNumber);

    /**
     * Returns the property 'customer number'.
     *
     * 
     *
     */
    String getCustomerNumber();
    /**
     * Setter for the property 'bic'.
     *
     * 
     *
     */
    void setBic(String bic);

    /**
     * Returns the property 'bic'.
     *
     * 
     *
     */
    String getBic();
    /**
     * Setter for the property 'iban'.
     *
     * 
     *
     */
    void setIban(String iban);

    /**
     * Returns the property 'iban'.
     *
     * 
     *
     */
    String getIban();
    /**
     * Setter for the property 'provider'.
     *
     * 
     *
     */
    void setProvider(de.smava.webapp.account.domain.BankAccountProvider provider);

    /**
     * Returns the property 'provider'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccountProvider getProvider();
    /**
     * Setter for the property 'consolidated debt'.
     *
     * 
     *
     */
    void setConsolidatedDebt(ConsolidatedDebt consolidatedDebt);

    /**
     * Returns the property 'consolidated debt'.
     *
     * 
     *
     */
    ConsolidatedDebt getConsolidatedDebt();
    /**
     * Setter for the property 'bank account clearing runs'.
     *
     * 
     *
     */
    void setBankAccountClearingRuns(Collection<BankAccountClearingRun> bankAccountClearingRuns);

    /**
     * Returns the property 'bank account clearing runs'.
     *
     * 
     *
     */
    Collection<BankAccountClearingRun> getBankAccountClearingRuns();
    /**
     * Setter for the property 'activity fee'.
     *
     * 
     *
     */
    void setActivityFee(boolean activityFee);

    /**
     * Returns the property 'activity fee'.
     *
     * 
     *
     */
    boolean getActivityFee();
    /**
     * Setter for the property 'paythrough possible'.
     *
     * 
     *
     */
    void setPaythroughPossible(Boolean paythroughPossible);

    /**
     * Returns the property 'paythrough possible'.
     *
     * 
     *
     */
    Boolean getPaythroughPossible();
    /**
     * Setter for the property 'sepa mandate reference'.
     *
     * 
     *
     */
    void setSepaMandateReference(String sepaMandateReference);

    /**
     * Returns the property 'sepa mandate reference'.
     *
     * 
     *
     */
    String getSepaMandateReference();

}
