package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Market;
import de.smava.webapp.account.domain.interfaces.BidInterestEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'BidInterests'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBidInterest
    extends KreditPrivatEntity implements BidInterestEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBidInterest.class);

    public String getDuration() {
        Market market = new Market(getMarketName());
        return market.getDurationAsString();
    }

    public String getRating() {
        Market market = new Market(getMarketName());
        return market.getRatingAsLetter();
    }

}

