package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.SchufaRequestEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'SchufaRequests'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractSchufaRequest
    extends KreditPrivatEntity implements SchufaRequestEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaRequest.class);

    public static final String TYPE_CONTRACT_INFORMATION = "CONTRACT_INFORMATION";
    public static final String TYPE_CONTRACT_OBJECTION = "CONTRACT_OBJECTION";
    public static final String TYPE_ENCASHMENT_INFORMATION = "ENCASHMENT_INFORMATION";
    public static final String TYPE_SCORE_REQUEST = "SCORE_REQUEST";
    public static final String TYPE_AFTERTREATMENT = "AFTERTREATMENT";
    public static final String TYPE_SUPPLEMENTARY_SCORE_REQUEST = "SUPPLEMENTARY_SCORE_REQUEST";

    public static final String STATE_SUCCESSFUL = "SUCCESSFUL";
    // failed if the request is not sent successfully (e.g. host not available)
    public static final String STATE_FAILED = "FAILED";

}

