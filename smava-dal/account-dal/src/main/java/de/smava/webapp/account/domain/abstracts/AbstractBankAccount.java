package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.BankAccountEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The domain object that represents 'BankAccounts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBankAccount
    extends KreditPrivatEntity implements BankAccountEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBankAccount.class);

    private static final long serialVersionUID = -2859453101057915084L;
    public static final String TYPE_REFRENCE_ACCOUNT = "reference account";
    public static final String TYPE_LENDER_ACCOUNT = "KOK";
    public static final String TYPE_BORROWER_ACCOUNT = "KNK";
    public static final String TYPE_BORROWER_ACCOUNT_ORIGINAL = "KNK_original";
    public static final String TYPE_SMAVA_ACCOUNT = "smava account";
    public static final String TYPE_OTHER_ACCOUNT = "other account";
    public static final String TYPE_ENCASHMENT_ACCOUNT = "encashment account";
    public static final String TYPE_SMAVA_INTERIM_IN_ACCOUNT = "smava interim account in";
    public static final String TYPE_SMAVA_INTERIM_OUT_ACCOUNT = "smava interim account out";
    public static final String TYPE_SMAVA_OUTPAYMENT_ACCOUNT = "smava outpayment account";
    public static final String TYPE_SMAVA_CALLMONEY_ACCOUNT = "smava callmoney account";
    public static final String TYPE_RDI_PREMIUM_ACCOUNT = "rdi premium account";
    public static final String TYPE_RDI_AMORTISATION_ACCOUNT = "rdi amortisation account";
    public static final String TYPE_ENCASHMENT_INTERIM_ACCOUNT = "encashment interim account";
    public static final String TYPE_PROVISION_INTERIM_ACCOUNT = "provision interim account";
    public static final String TYPE_DEBT_CONSOLIDATION_ACCOUNT = "debt consolidation account";
    public static final String TYPE_LENDER_DEPOSIT_ACCOUNT = "lender deposit account";
    public static final String TYPE_BORROWER_DEPOSIT_ACCOUNT = "borrower deposit account";
    public static final String TYPE_KOK_IMITATION_ACCOUNT = "KOK_imitation";

    public static final String NORDEA_CLEARING_NUMBER = "14401156";
    public static final String SMAVA_ACCOUNT_IDENTIFIER = "6601";
    public static final String CONTROLL_NUMBER = "252100";

    public static final String SEPA_MADATE_REFERENCE_PREFIX = "SM";

    /**
     * returns whether the bankaccount is an emoney account of type kok or knk
     *
     * @return
     */
    public boolean isInternalEmoneyBankAccount() {
        if ( ( BankAccount.TYPE_BORROWER_ACCOUNT.equalsIgnoreCase(getType())
                || ( BankAccount.TYPE_LENDER_ACCOUNT.equalsIgnoreCase(getType() ) ) )
                && getProvider() != null
                && getProvider().getType() == BankAccountProviderType.E_MONEY){
            return true;
        }
        return false;
    }

    public boolean isInternalBankAccount(){
        return TYPE_BORROWER_ACCOUNT.equals(getType()) || TYPE_LENDER_ACCOUNT.equals(getType());
    }

    public boolean isSmavaAccount() {
        return BankAccount.TYPE_SMAVA_ACCOUNT.equals(getType());
    }

    public boolean isSmavaInterimAccount() {
        return BankAccount.TYPE_SMAVA_INTERIM_IN_ACCOUNT.equals(getType());
    }


    public String getUsername() {
        String username = "?";
        if (getAccount() == null) {
            if (getName() != null && !getName().equals("")) {
                username = getName();
            }
        } else {
            username = getAccount().getUsername();
        }
        return username;
    }

    /**
     * Checks if this bank account object is functionally equal to another bank account.<br/ >
     * Compares if the values for accountNo and bankCode are equal.<br />
     * In case of parameter checkAlsoForIban is true, also compares the values for iban and bic.
     *
     * @param other the other bank account to compare with
     * @param checkAlsoForIban true if also compare values for iban and bic
     * @return true if the accounts are functionally equal
     */
    public boolean functionallyEqual(BankAccount other, boolean checkAlsoForIban) {
        if (null == other || null == other.getAccountNo()
                || null == other.getBankCode())
            return false;

        boolean eq = adjustAccountNo(other.getAccountNo()).equals(adjustAccountNo(getAccountNo()))
                && other.getBankCode().equals(getBankCode());

        if (!eq) {
            return false;
        }

        // Check must also work for
        if (checkAlsoForIban) {
            if (null == other.getIban()
                    || !other.getIban().equals(getIban())
                    || null == other.getBic()
                    || !other.getBic().equals(getBic())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Adjust an account number by deleting leading 0
     *
     * @param accountNo The account number to adjust
     * @return the adjusted account number
     */
    private String adjustAccountNo(String accountNo) {
        if (accountNo == null) {
            return null;
        }
        if ( accountNo.length() == 0){
            return "";
        }


        String result = accountNo;
        while (result.charAt(0) == '0') {
            if ( result.length() == 1){
                return "";
            }
            result = result.substring(1);
        }

        return result;
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        list.add(getAccount());
        return list;
    }

}

