//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account)}

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.BankAccountProvider;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BankAccounts'.
 *
 * @author generator
 */
public interface BankAccountDao extends BaseDao<BankAccount>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bank account identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BankAccount getBankAccount(Long id);

    /**
     * Saves the bank account.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBankAccount(BankAccount bankAccount);

    /**
     * Deletes an bank account, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bank account
     */
    void deleteBankAccount(Long id);

    /**
     * Retrieves all 'BankAccount' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BankAccount> getBankAccountList();

    /**
     * Retrieves a page of 'BankAccount' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BankAccount> getBankAccountList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BankAccount' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BankAccount> getBankAccountList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BankAccount' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BankAccount> getBankAccountList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'BankAccount' instances.
     */
    long getBankAccountCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bank account)}
    //
    // insert custom methods here
    //
    /**
     * Returns all not expired KOK bank accounts
     * 
     * @param bankAccountProvider
     * @param page
     * @param pageSize
     * @return
     */
    List<BankAccount> getKokBankAccounts(BankAccountProvider bankAccountProvider, int page, int pageSize);
    
    /**
     * Returns the lender deposit account. Unique for a provider owned by user smava
     * 
     * @param provider
     * @return
     */
    BankAccount getLenderDepositAccount(BankAccountProvider provider);
    
    /**
     * returns the BankAccount of type borrower deposit account
     * Unique for a provider owned by user smava
     * 
     * @param provider the provider
     * @return the BankAccount of type borrower deposit account
     */
    public BankAccount getBorrowerDepositAccount(BankAccountProvider provider);
    
    /**
     * returns all BankAccount of type * deposit account 
     * 
     * @return all BankAccounts deposit account
     */
    public Collection<BankAccount> getAllDepositAccounts();
    
    /**
     * search the first Bankaccount of type "smava interim in account" from DB.
     * @param bankAccountProvider the service provider
     * @return the first Bankaccount of type "smava interim in account" from DB. Null if not available!
     */
    public BankAccount getSmavaInterimInAccount(BankAccountProvider bankAccountProvider);
    
    /**
     * search the first BankAccount of type "smava interim out account" from DB.
     * @param bankAccountProvider the service provider
     * @return the first BankAccount of type "smava interim out account" from DB.  Null if not available!
     */
    public BankAccount getSmavaInterimOutAccount(BankAccountProvider bankAccountProvider);
    
    /**
     * search the first BankAccount of type "KNK_original" from DB.
     * @param provider the service provider
     * @return the first BankAccount of type "KNK_original" from DB.  Null if not available!
     */
    public BankAccount getInternalBorrowerAccountOriginal(BankAccountProvider provider);
    
    /**
     * search all bank accounts with the given bank number, account number
     * 
     * @param bank the bank number
     * @param bankAccountNo the account number
     * @return
     */
    public Collection<BankAccount> findBankAccount(String bank, String bankAccountNo);

    Collection<BankAccount> findBankAccountNotExpired(String accountNo, String bankCode);

    /**
     * search all bank accounts with the given bank number, account number
     *      
     * @param bank the bank number
     * @param bankAccountNo the account number
     * @return
     */
    public Collection<BankAccount> findLikeBankAccount(String bank, String bankAccountNo);
    
    /**
     * Retrieves all BankAccount without IBAN for the given type.
     * @param type
     * @return
     */
    public Collection<Long> findActiveBankAccountsWithoutIban( String type);

    public Collection<BankAccount> getBankAccountsByAccountNo(
            String bankAccountNo);

    /**Retrieves fake bank account with account number/ bank code combination to be copied
     * for lenders that invested in a creditproject which was migrated to fidor bank including the borrower.
     * In this case this fake bank account is used to update the payout plan - it mustn't be used
     * for any real life payment.
     *
     * Has to be copied individually for each lender.
     *
     * @param provider
     * @return bankAccount to be copied to lender
     */
    BankAccount getKokImitationBankAccount(BankAccountProvider provider);


    BankAccount getEncashmentBankAccount();
    Collection<BankAccount> getEncashmentBankAccounts();

    BankAccount getProvisionInterimBankAccount(BankAccountProvider bankAccountProvider);
    Collection<BankAccount> getProvisionInterimBankAccounts();

    BankAccount getEncashmentInterimBankAccount(BankAccountProvider bankAccountProvider);
    Collection<BankAccount> getEncashmentInterimBankAccounts();

    /**
     * Searches a bank account with type {@link BankAccount#TYPE_SMAVA_ACCOUNT} and no expiration date.
     * @return null or a valid bank account.
     */
    BankAccount getSmavaBankAccount(BankAccountProvider provider);

    /**
     * Searches all bank accounts of type {@link BankAccount#TYPE_SMAVA_ACCOUNT}.
     * @return all bank accounts of type {@link BankAccount#TYPE_SMAVA_ACCOUNT}.
     */
    Collection<BankAccount> getSmavaBankAccounts();

    /**
     * Looks for a rdi amortisation bank account without expiration date.
     * If more than one bank account exists null will be returned.
     * @return latest rdi amortisation bank account
     */
    BankAccount getRdiAmortisationBankAccount();
    /**
     * Returns the whole rdi amortisation bank account history.
     */

    Collection<BankAccount> getRdiAmortisationBankAccounts();

    BankAccount getRdiPremiumBankAccount();
    BankAccount getSmavaCallMoneyBankAccount();
    BankAccount getSmavaOutpaymentBankAccount();

    /**
     * Returns the whole smava interim in bank account history.
     */
    Collection<BankAccount> getSmavaInterimInBankAccounts();

    /**
     * Returns the whole smava interim out bank account history.
     */
    Collection<BankAccount> getSmavaInterimOutBankAccounts();

    Collection<BankAccount> getByIban(String bankIban);

    Collection<BankAccount> getByCodeAndAccount(String bankCode, String bankAccountNumberStripped);

    Collection<BankAccount> getByCodeAndAccountAndBicAndIban(String bankCode, String bankAccountNumberStripped, String bic, String iban);

    Collection<BankAccount> getByAccount(Long accountId);

    /**
     * WEBSITE-15545 - do not take expired accounts while checking for uniqueness in KP order creation
     *
     * @param bankIban
     * @return
     */
    public Collection<BankAccount> getNonExpiredByIban(String bankIban);

    // !!!!!!!! End of insert code section !!!!!!!!
}
