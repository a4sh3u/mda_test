package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractInterestBorder;




/**
 * The domain object that has all history aggregation related fields for 'InterestBorders'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class InterestBorderHistory extends AbstractInterestBorder {

    protected transient String _ratingInitVal;
    protected transient boolean _ratingIsSet;


	
    /**
     * Returns the initial value of the property 'rating'.
     */
    public String ratingInitVal() {
        String result;
        if (_ratingIsSet) {
            result = _ratingInitVal;
        } else {
            result = getRating();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rating'.
     */
    public boolean ratingIsDirty() {
        return !valuesAreEqual(ratingInitVal(), getRating());
    }

    /**
     * Returns true if the setter method was called for the property 'rating'.
     */
    public boolean ratingIsSet() {
        return _ratingIsSet;
    }
			
}
