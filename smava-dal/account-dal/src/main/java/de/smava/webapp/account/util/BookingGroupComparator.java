package de.smava.webapp.account.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;

import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.todo.util.DateUtils;

/**
 * used to sort bookinggroups by date.
 * @author rfiedler
 *
 */
public class BookingGroupComparator implements Comparator<BookingGroup> {

    private Date _contractStartDate = null;
    
    public BookingGroupComparator(Collection<Contract> contracts) {
        Date contractStartDate = contracts.iterator().next().getStartDate();
        this._contractStartDate = contractStartDate;
    }
    
    public BookingGroupComparator() {
        
    }
    
    public int compare(BookingGroup o1, BookingGroup o2) {
    	Date date1 = o1.getDate();
    	Date date2 = o2.getDate();

    	if (BookingGroup.TYPE_CREDIT_PAYOUT.equals(o1.getType()) && this._contractStartDate != null) {
   	        date1 = this._contractStartDate;    	        
    	}

        if (BookingGroup.TYPE_CREDIT_PAYOUT.equals(o2.getType()) && this._contractStartDate != null) {
            date2 = this._contractStartDate;                
        }

    	if (BookingGroup.TYPE_INSURANCE_GROUP.equals(o1.getType())) {
    		date1 = DateUtils.getDateEndOfMonth(o1.getDate());
    	}

    	if (BookingGroup.TYPE_INSURANCE_GROUP.equals(o2.getType())) {
    		date2 = DateUtils.getDateEndOfMonth(o2.getDate());
    	}

        int result = date1.compareTo(date2);    

        if (result == 0) {
            result = o1.getType().compareTo(o2.getType());
        }

        return result;
    }
}