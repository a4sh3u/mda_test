package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.AccountRoleState;
import de.smava.webapp.account.domain.Approval;

import java.util.Collection;


/**
 * The domain object that represents 'AccountRoles'.
 *
 * @author generator
 */
public interface AccountRoleEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'approvals'.
     *
     * 
     *
     */
    void setApprovals(Collection<Approval> approvals);

    /**
     * Returns the property 'approvals'.
     *
     * 
     *
     */
    Collection<Approval> getApprovals();
    /**
     * Setter for the property 'states'.
     *
     * 
     *
     */
    void setStates(Collection<AccountRoleState> states);

    /**
     * Returns the property 'states'.
     *
     * 
     *
     */
    Collection<AccountRoleState> getStates();

}
