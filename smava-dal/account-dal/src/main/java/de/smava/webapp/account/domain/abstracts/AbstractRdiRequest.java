package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.RdiEntry;
import de.smava.webapp.account.domain.interfaces.RdiRequestEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The domain object that represents 'RdiRequests'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractRdiRequest
    extends KreditPrivatEntity implements RdiRequestEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRdiRequest.class);

    public static final String STATE_SUCCESS = "success";
    public static final String STATE_ERROR_IO = "error_io";
    public static final String STATE_ERROR_UNKNOWN = "error_unknown";
    public static final String STATE_ERROR_FIELDS = "error_fields";

    protected transient Map<String, String> _entryMap;

    /**
     * Returns entry value from entry map.
     */
    public String getValue(String key) {
        String result = null;

        RdiEntry rdiEntry = getEntry(key);
        if (rdiEntry != null) {
            result = rdiEntry.getValue();
        }

        return result;
    }

    public RdiEntry getEntry(String key) {
        RdiEntry result = null;
        for (RdiEntry entry : getEntries()) {
            if (entry.getKey().equals(key)) {
                result = entry;
                break;
            }
        }
        return result;
    }

    public void addRdiEntry(RdiEntry rdiEntry) {
        if (getEntries() == null) {
            setEntries(new ArrayList<RdiEntry>());
        }

        getEntries().add(rdiEntry);

        // otherwise getValue does not work:
        if (_entryMap == null) {
            _entryMap = new HashMap<String, String>();
        }
        _entryMap.put(rdiEntry.getKey(), rdiEntry.getValue());
    }

    public void setContractReferenceNumber(Long referenceNumber) {
        if (referenceNumber != null) {
            if (getEntries() == null) {
                RdiEntry entry = new RdiEntry();
                entry.setKey("contractData_contractReferenceNumber");
                entry.setValue(String.valueOf(referenceNumber));
                addRdiEntry(entry);
            }  else {
                RdiEntry referenceNumberEntry = getEntry("contractData_contractReferenceNumber");
                if (referenceNumberEntry == null) {
                    RdiEntry entry = new RdiEntry();
                    entry.setKey("contractData_contractReferenceNumber");
                    entry.setValue(String.valueOf(referenceNumber));
                    addRdiEntry(entry);
                } else {
                    referenceNumberEntry.setValue(String.valueOf(referenceNumber));
                    _entryMap.put("contractData_contractReferenceNumber", String.valueOf(referenceNumber));
                }
            }
        }
    }

    public String getContractReferenceNumber() {
        return getValue("contractData_contractReferenceNumber");
    }

    public String getContractNumber() {
        return getValue("contractData_contractNumber");
    }

    public String getMethod() {
        return getValue("method");
    }

}

