package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.ReminderStateEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'ReminderStates'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractReminderState
    extends KreditPrivatEntity implements ReminderStateEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractReminderState.class);

}

