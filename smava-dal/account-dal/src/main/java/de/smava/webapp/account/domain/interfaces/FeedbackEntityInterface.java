package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Order;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'Feedbacks'.
 *
 * @author generator
 */
public interface FeedbackEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'release approval date'.
     *
     * 
     *
     */
    void setReleaseApprovalDate(Date releaseApprovalDate);

    /**
     * Returns the property 'release approval date'.
     *
     * 
     *
     */
    Date getReleaseApprovalDate();
    /**
     * Setter for the property 'text'.
     *
     * 
     *
     */
    void setText(String text);

    /**
     * Returns the property 'text'.
     *
     * 
     *
     */
    String getText();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'release'.
     *
     * 
     *
     */
    void setRelease(Boolean release);

    /**
     * Returns the property 'release'.
     *
     * 
     *
     */
    Boolean getRelease();
    /**
     * Setter for the property 'orders'.
     *
     * 
     *
     */
    void setOrders(Set<Order> orders);

    /**
     * Returns the property 'orders'.
     *
     * 
     *
     */
    Set<Order> getOrders();

}
