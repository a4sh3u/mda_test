package de.smava.webapp.account.domain;

import com.aperto.webkit.utils.StringTools;
import de.smava.webapp.account.domain.history.OrderHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.*;

/**
 * The domain object that represents 'Orders'.
 */
public class Order extends OrderHistory  implements MarketingTrackingEntity, ImageAwareEntity {

    /**
     * Default constructor (would have been blocked by copy constructor).
     * used for tests only
     */
    public Order() {
        super();

        _categoryOrders = new HashSet<CategoryOrder>();
        _economicalDatas  = new HashSet<EconomicalData>();
    }

    /**
     * Default constructor (would have been blocked by copy constructor).
     * Is also called from fitnesse. So it initializes a default provision rate of 1.0.
     */
    public Order(Account account, BankAccountProvider bankAccountProvider) {

        this._account = account;
        setCreationDate(CurrentDate.getDate());
        setProvisionRate(1.0D);

        _categoryOrders = new HashSet<CategoryOrder>();

        setBankAccountProvider(bankAccountProvider);

        if (account != null) {
            if (account.getOrders() == null) {
                account.setOrders(new HashSet<Order>());
            }
            if (account.getOrders() != null) {
                account.getOrders().add(this);
            }
        }
    }

    /**
     * copy constructor for matching algorithm and contract creation.
     */
    public Order(Order order) {
        setId(order.getId()); //TODO: Check if this is necessary (matching test suite)
        _title = order.getTitle();
        _description = order.getDescription();
        _amount = order.getAmount();
        if (order.getCreationDate() != null) {
            _creationDate = new Date(order.getCreationDate().getTime());
        }
        if (order.getUpdateDate() != null) {
            _updateDate = new Date(order.getUpdateDate().getTime());
        }

        _bidInterests = new ArrayList<BidInterest>();
        for (BidInterest bidInterest : order.getBidInterests()) {
            String marketName = bidInterest.getMarketName();
            if (marketName.length() == 2) {
                marketName = marketName.charAt(0) + "0" + marketName.charAt(1);
            }
            putRateForMarket(marketName, bidInterest.getRate());
        }

        _state = order.getState();

        if (order.getGroupId() == 0) {
            _groupId = order.getId();
            order.setGroupId(order.getId());
        } else {
            _groupId = order.getGroupId();
        }
        if (order.getContract() != null) {
            _contract = order.getContract();
        }

        if (order.getAccount() != null) {
            _account = order.getAccount();
        }
        _provisionRate = order.getProvisionRate();
        _provisionRateFix = order.getProvisionRateFix();
        if (!StringTools.isEmpty(order.getCreditTerm())) {
            _creditTerm = order.getCreditTerm();
        }

        _categoryOrders = new HashSet<CategoryOrder>();

        for (CategoryOrder oldCategoryOrder : order.getCategoryOrders()) {
            CategoryOrder categoryOrder = new CategoryOrder();
            categoryOrder.setMain(oldCategoryOrder.getMain());
            categoryOrder.setCategory(oldCategoryOrder.getCategory());
            categoryOrder.setOrder(this);
            _categoryOrders.add(categoryOrder);
        }

        _image = order.getImage();

        setImageHeight(order.getImageHeight());
        setImageWidth(order.getImageWidth());

        if (order.getActivationDate() != null) {
            setActivationDate(order.getActivationDate());
        }
        setBorrowerPromotionId(order.getBorrowerPromotionId());
        setInternalDebtConsolidation(order.getInternalDebtConsolidation());

        setBankAccountProvider(order.getBankAccountProvider());

    }

        protected Account _account;
        protected Date _activationDate;
        protected String _creditTerm;
        protected String _image;
        protected int _interesting;
        protected int _imageWidth;
        protected int _imageHeight;
        protected Long _marketingPlacementId;
        protected List<OrderRecommendation> _recommendations;
        protected Collection<RdiContract> _rdiContracts;
        protected double _roi;
        protected double _matchedRate;
        protected Collection<CategoryOrder> _categoryOrders;
        protected Double _provisionRate;
        protected Double _provisionRateFix;
        protected Date _agreementBlacklistCheck;
        protected Date _agreementTransfer;
        protected Date _agreementTaxCommitment;
        protected Collection<Document> _documents;
        protected boolean _fastFinancing;
        protected Set<Feedback> _borrowerFeedback;
        protected boolean _automaticDiscription;
        protected Long _borrowerPromotionId;
        protected Boolean _sharedLoan;
        protected boolean _internalDebtConsolidation;
        protected de.smava.webapp.account.domain.BankAccountProvider _bankAccountProvider;
        protected Collection<EconomicalData> _economicalDatas;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'activation date'.
     */
    public void setActivationDate(Date activationDate) {
        if (!_activationDateIsSet) {
            _activationDateIsSet = true;
            _activationDateInitVal = getActivationDate();
        }
        registerChange("activation date", _activationDateInitVal, activationDate);
        _activationDate = activationDate;
    }
                        
    /**
     * Returns the property 'activation date'.
     */
    public Date getActivationDate() {
        return _activationDate;
    }
                                    /**
     * Setter for the property 'credit term'.
     */
    public void setCreditTerm(String creditTerm) {
        if (!_creditTermIsSet) {
            _creditTermIsSet = true;
            _creditTermInitVal = getCreditTerm();
        }
        registerChange("credit term", _creditTermInitVal, creditTerm);
        _creditTerm = creditTerm;
    }
                        
    /**
     * Returns the property 'credit term'.
     */
    public String getCreditTerm() {
        return _creditTerm;
    }
                                    /**
     * Setter for the property 'image'.
     */
    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = getImage();
        }
        registerChange("image", _imageInitVal, image);
        _image = image;
    }
                        
    /**
     * Returns the property 'image'.
     */
    public String getImage() {
        return _image;
    }
                                    /**
     * Setter for the property 'interesting'.
     */
    public void setInteresting(int interesting) {
        if (!_interestingIsSet) {
            _interestingIsSet = true;
            _interestingInitVal = getInteresting();
        }
        registerChange("interesting", _interestingInitVal, interesting);
        _interesting = interesting;
    }
                        
    /**
     * Returns the property 'interesting'.
     */
    public int getInteresting() {
        return _interesting;
    }
                                    /**
     * Setter for the property 'image width'.
     */
    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = getImageWidth();
        }
        registerChange("image width", _imageWidthInitVal, imageWidth);
        _imageWidth = imageWidth;
    }
                        
    /**
     * Returns the property 'image width'.
     */
    public int getImageWidth() {
        return _imageWidth;
    }
                                    /**
     * Setter for the property 'image height'.
     */
    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = getImageHeight();
        }
        registerChange("image height", _imageHeightInitVal, imageHeight);
        _imageHeight = imageHeight;
    }
                        
    /**
     * Returns the property 'image height'.
     */
    public int getImageHeight() {
        return _imageHeight;
    }
                                            
    /**
     * Setter for the property 'marketing placement id'.
     */
    public void setMarketingPlacementId(Long marketingPlacementId) {
        _marketingPlacementId = marketingPlacementId;
    }
            
    /**
     * Returns the property 'marketing placement id'.
     */
    public Long getMarketingPlacementId() {
        return _marketingPlacementId;
    }
                                            
    /**
     * Setter for the property 'recommendations'.
     */
    public void setRecommendations(List<OrderRecommendation> recommendations) {
        _recommendations = recommendations;
    }
            
    /**
     * Returns the property 'recommendations'.
     */
    public List<OrderRecommendation> getRecommendations() {
        return _recommendations;
    }
                                            
    /**
     * Setter for the property 'rdi contracts'.
     */
    public void setRdiContracts(Collection<RdiContract> rdiContracts) {
        _rdiContracts = rdiContracts;
    }
            
    /**
     * Returns the property 'rdi contracts'.
     */
    public Collection<RdiContract> getRdiContracts() {
        return _rdiContracts;
    }
                                    /**
     * Setter for the property 'roi'.
     */
    public void setRoi(double roi) {
        if (!_roiIsSet) {
            _roiIsSet = true;
            _roiInitVal = getRoi();
        }
        registerChange("roi", _roiInitVal, roi);
        _roi = roi;
    }
                        
    /**
     * Returns the property 'roi'.
     */
    public double getRoi() {
        return _roi;
    }
                                    /**
     * Setter for the property 'matched rate'.
     */
    public void setMatchedRate(double matchedRate) {
        if (!_matchedRateIsSet) {
            _matchedRateIsSet = true;
            _matchedRateInitVal = getMatchedRate();
        }
        registerChange("matched rate", _matchedRateInitVal, matchedRate);
        _matchedRate = matchedRate;
    }
                        
    /**
     * Returns the property 'matched rate'.
     */
    public double getMatchedRate() {
        return _matchedRate;
    }
                                            
    /**
     * Setter for the property 'category orders'.
     */
    public void setCategoryOrders(Collection<CategoryOrder> categoryOrders) {
        _categoryOrders = categoryOrders;
    }
            
    /**
     * Returns the property 'category orders'.
     */
    public Collection<CategoryOrder> getCategoryOrders() {
        return _categoryOrders;
    }
                                            
    /**
     * Setter for the property 'provision rate'.
     */
    public void setProvisionRate(Double provisionRate) {
        _provisionRate = provisionRate;
    }
            
    /**
     * Returns the property 'provision rate'.
     */
    public Double getProvisionRate() {
        return _provisionRate;
    }
                                            
    /**
     * Setter for the property 'provision rate fix'.
     */
    public void setProvisionRateFix(Double provisionRateFix) {
        _provisionRateFix = provisionRateFix;
    }
            
    /**
     * Returns the property 'provision rate fix'.
     */
    public Double getProvisionRateFix() {
        return _provisionRateFix;
    }
                                    /**
     * Setter for the property 'agreement blacklist check'.
     */
    public void setAgreementBlacklistCheck(Date agreementBlacklistCheck) {
        if (!_agreementBlacklistCheckIsSet) {
            _agreementBlacklistCheckIsSet = true;
            _agreementBlacklistCheckInitVal = getAgreementBlacklistCheck();
        }
        registerChange("agreement blacklist check", _agreementBlacklistCheckInitVal, agreementBlacklistCheck);
        _agreementBlacklistCheck = agreementBlacklistCheck;
    }
                        
    /**
     * Returns the property 'agreement blacklist check'.
     */
    public Date getAgreementBlacklistCheck() {
        return _agreementBlacklistCheck;
    }
                                    /**
     * Setter for the property 'agreement transfer'.
     */
    public void setAgreementTransfer(Date agreementTransfer) {
        if (!_agreementTransferIsSet) {
            _agreementTransferIsSet = true;
            _agreementTransferInitVal = getAgreementTransfer();
        }
        registerChange("agreement transfer", _agreementTransferInitVal, agreementTransfer);
        _agreementTransfer = agreementTransfer;
    }
                        
    /**
     * Returns the property 'agreement transfer'.
     */
    public Date getAgreementTransfer() {
        return _agreementTransfer;
    }
                                    /**
     * Setter for the property 'agreement tax commitment'.
     */
    public void setAgreementTaxCommitment(Date agreementTaxCommitment) {
        if (!_agreementTaxCommitmentIsSet) {
            _agreementTaxCommitmentIsSet = true;
            _agreementTaxCommitmentInitVal = getAgreementTaxCommitment();
        }
        registerChange("agreement tax commitment", _agreementTaxCommitmentInitVal, agreementTaxCommitment);
        _agreementTaxCommitment = agreementTaxCommitment;
    }
                        
    /**
     * Returns the property 'agreement tax commitment'.
     */
    public Date getAgreementTaxCommitment() {
        return _agreementTaxCommitment;
    }
                                            
    /**
     * Setter for the property 'documents'.
     */
    public void setDocuments(Collection<Document> documents) {
        _documents = documents;
    }
            
    /**
     * Returns the property 'documents'.
     */
    public Collection<Document> getDocuments() {
        return _documents;
    }
                                    /**
     * Setter for the property 'fast financing'.
     */
    public void setFastFinancing(boolean fastFinancing) {
        if (!_fastFinancingIsSet) {
            _fastFinancingIsSet = true;
            _fastFinancingInitVal = getFastFinancing();
        }
        registerChange("fast financing", _fastFinancingInitVal, fastFinancing);
        _fastFinancing = fastFinancing;
    }
                        
    /**
     * Returns the property 'fast financing'.
     */
    public boolean getFastFinancing() {
        return _fastFinancing;
    }
                                            
    /**
     * Setter for the property 'borrower feedback'.
     */
    public void setBorrowerFeedback(Set<Feedback> borrowerFeedback) {
        _borrowerFeedback = borrowerFeedback;
    }
            
    /**
     * Returns the property 'borrower feedback'.
     */
    public Set<Feedback> getBorrowerFeedback() {
        return _borrowerFeedback;
    }
                                    /**
     * Setter for the property 'automatic discription'.
     */
    public void setAutomaticDiscription(boolean automaticDiscription) {
        if (!_automaticDiscriptionIsSet) {
            _automaticDiscriptionIsSet = true;
            _automaticDiscriptionInitVal = getAutomaticDiscription();
        }
        registerChange("automatic discription", _automaticDiscriptionInitVal, automaticDiscription);
        _automaticDiscription = automaticDiscription;
    }
                        
    /**
     * Returns the property 'automatic discription'.
     */
    public boolean getAutomaticDiscription() {
        return _automaticDiscription;
    }
                                            
    /**
     * Setter for the property 'borrower promotion id'.
     */
    public void setBorrowerPromotionId(Long borrowerPromotionId) {
        _borrowerPromotionId = borrowerPromotionId;
    }
            
    /**
     * Returns the property 'borrower promotion id'.
     */
    public Long getBorrowerPromotionId() {
        return _borrowerPromotionId;
    }
                                            
    /**
     * Setter for the property 'shared loan'.
     */
    public void setSharedLoan(Boolean sharedLoan) {
        _sharedLoan = sharedLoan;
    }
            
    /**
     * Returns the property 'shared loan'.
     */
    public Boolean getSharedLoan() {
        return _sharedLoan;
    }
                                    /**
     * Setter for the property 'internal debt consolidation'.
     */
    public void setInternalDebtConsolidation(boolean internalDebtConsolidation) {
        if (!_internalDebtConsolidationIsSet) {
            _internalDebtConsolidationIsSet = true;
            _internalDebtConsolidationInitVal = getInternalDebtConsolidation();
        }
        registerChange("internal debt consolidation", _internalDebtConsolidationInitVal, internalDebtConsolidation);
        _internalDebtConsolidation = internalDebtConsolidation;
    }
                        
    /**
     * Returns the property 'internal debt consolidation'.
     */
    public boolean getInternalDebtConsolidation() {
        return _internalDebtConsolidation;
    }
                                            
    /**
     * Setter for the property 'bank account provider'.
     */
    public void setBankAccountProvider(de.smava.webapp.account.domain.BankAccountProvider bankAccountProvider) {
        _bankAccountProvider = bankAccountProvider;
    }
            
    /**
     * Returns the property 'bank account provider'.
     */
    public de.smava.webapp.account.domain.BankAccountProvider getBankAccountProvider() {
        return _bankAccountProvider;
    }
                                            
    /**
     * Setter for the property 'economical datas'.
     */
    public void setEconomicalDatas(Collection<EconomicalData> economicalDatas) {
        _economicalDatas = economicalDatas;
    }
            
    /**
     * Returns the property 'economical datas'.
     */
    public Collection<EconomicalData> getEconomicalDatas() {
        return _economicalDatas;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Order.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creditTerm=").append(_creditTerm);
            builder.append("\n    _image=").append(_image);
            builder.append("\n    _interesting=").append(_interesting);
            builder.append("\n    _imageWidth=").append(_imageWidth);
            builder.append("\n    _imageHeight=").append(_imageHeight);
            builder.append("\n    _roi=").append(_roi);
            builder.append("\n    _matchedRate=").append(_matchedRate);
            builder.append("\n    _fastFinancing=").append(_fastFinancing);
            builder.append("\n    _automaticDiscription=").append(_automaticDiscription);
            builder.append("\n    _internalDebtConsolidation=").append(_internalDebtConsolidation);
            builder.append("\n}");
        } else {
            builder.append(Order.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Order instance() {
        return this;
    }
}
