//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(registration route)}
import de.smava.webapp.account.domain.history.RegistrationRouteHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RegistrationRoutes'.
 *
 * @author generator
 */
public class RegistrationRoute extends RegistrationRouteHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(registration route)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected String _jsonConfig;
        protected Integer _shortId;
        
                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'json config'.
     */
    public void setJsonConfig(String jsonConfig) {
        if (!_jsonConfigIsSet) {
            _jsonConfigIsSet = true;
            _jsonConfigInitVal = getJsonConfig();
        }
        registerChange("json config", _jsonConfigInitVal, jsonConfig);
        _jsonConfig = jsonConfig;
    }
                        
    /**
     * Returns the property 'json config'.
     */
    public String getJsonConfig() {
        return _jsonConfig;
    }
                                            
    /**
     * Setter for the property 'short id'.
     */
    public void setShortId(Integer shortId) {
        _shortId = shortId;
    }
            
    /**
     * Returns the property 'short id'.
     */
    public Integer getShortId() {
        return _shortId;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RegistrationRoute.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _jsonConfig=").append(_jsonConfig);
            builder.append("\n}");
        } else {
            builder.append(RegistrationRoute.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RegistrationRoute asRegistrationRoute() {
        return this;
    }
}
