package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBookingAssignment;




/**
 * The domain object that has all history aggregation related fields for 'BookingAssignments'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BookingAssignmentHistory extends AbstractBookingAssignment {

    protected transient boolean _isTargetInitVal;
    protected transient boolean _isTargetIsSet;


	
    /**
     * Returns the initial value of the property 'is target'.
     */
    public boolean isTargetInitVal() {
        boolean result;
        if (_isTargetIsSet) {
            result = _isTargetInitVal;
        } else {
            result = getIsTarget();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'is target'.
     */
    public boolean isTargetIsDirty() {
        return !valuesAreEqual(isTargetInitVal(), getIsTarget());
    }

    /**
     * Returns true if the setter method was called for the property 'is target'.
     */
    public boolean isTargetIsSet() {
        return _isTargetIsSet;
    }
		
}
