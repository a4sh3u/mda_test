//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.notification.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(message)}

import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Message;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Messages'.
 *
 * @author generator
 */
public interface MessageDao extends BaseDao<Message>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the message identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Message getMessage(Long id);

    /**
     * Saves the message.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMessage(Message message);

    /**
     * Deletes an message, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the message
     */
    void deleteMessage(Long id);

    /**
     * Retrieves all 'Message' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Message> getMessageList();

    /**
     * Retrieves a page of 'Message' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Message> getMessageList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Message' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Message> getMessageList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Message' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Message> getMessageList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Message' instances.
     */
    long getMessageCount();

    /**
     * Attaches a Account to the Message instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachAccount(Message message, Account account);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(message)}
    //
    Collection<Message> findMessagesByOwner(Account searchObject, Pageable pageable, Sortable sortable);

    Collection<Message> findMessagesByOwnerAndFolder(String folder, Account account);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
