package de.smava.webapp.account.todo.contract.dao;

/**
 * 
 * @author dkeller
 *
 */
public class ContractStateNumbers {
	private String _state;
    private Long _number = null;
   
    public ContractStateNumbers() {
        
    }
    public ContractStateNumbers(String state, Long number) {
        _state = state;
        _number = number;
    }
	public String getState() {
		return _state;
	}
	public void setState(String state) {
		_state = state;
	}
	public Long getNumber() {
		return _number;
	}
	public void setNumber(Long number) {
		_number = number;
	}
        
    
}
