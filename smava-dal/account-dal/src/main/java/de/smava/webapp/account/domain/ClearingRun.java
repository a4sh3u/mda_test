package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.ClearingRunHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'ClearingRuns'.
 */
public class ClearingRun extends ClearingRunHistory  {

	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = -8772281842538071282L;

        protected Account _processor;
        protected Date _creationDate;
        protected Collection<BankAccountClearingRun> _bankAccountClearingRuns;

    /**
     * Setter for the property 'processor'.
     */
    public void setProcessor(Account processor) {
        _processor = processor;
    }
            
    /**
     * Returns the property 'processor'.
     */
    public Account getProcessor() {
        return _processor;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'bank account clearing runs'.
     */
    public void setBankAccountClearingRuns(Collection<BankAccountClearingRun> bankAccountClearingRuns) {
        _bankAccountClearingRuns = bankAccountClearingRuns;
    }
            
    /**
     * Returns the property 'bank account clearing runs'.
     */
    public Collection<BankAccountClearingRun> getBankAccountClearingRuns() {
        return _bankAccountClearingRuns;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ClearingRun.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(ClearingRun.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
