package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import com.fasterxml.jackson.annotation.JsonIgnore;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.AccountEntityInterface;
import de.smava.webapp.commons.constants.StringConstants;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.i18n.Employment;
import de.smava.webapp.commons.security.Role;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.Interval;
import org.joda.time.PeriodType;

import java.util.*;

/**
 * The domain object that represents 'Accounts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractAccount
    extends KreditPrivatEntity implements MarketingTrackingEntity,AccountEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAccount.class);
    public static final String USER_STATE_DEACTIVATED = "DEACTIVATED";
    public static final String USER_STATE_ACTIVATED = "ACTIVATED";
    public static final String USER_STATE_ACCRED_IN_PROGRESS = "ACCRED_IN_PROGRESS";
    public static final String USER_STATE_ACCRED_DENIED = "ACCRED_DENIED";
    public static final String USER_STATE_FAKE = "FAKE";
    public static final String USER_STATE_DELETED = "DELETED";

    public static final String BORROWER_REG_STARTED = "BORROWER_REG_STARTED";

    public static final Set<String> GOING_TO_BE_BORROWER_PREFEREDROLES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(BORROWER_REG_STARTED, StringConstants.SHORTLEAD)));
    public static final Set<String> GOING_TO_BE_BORROWER_OR_SALESLEAD_PREFEREDROLES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(BORROWER_REG_STARTED, StringConstants.SHORTLEAD, StringConstants.SALESLEAD)));

    private static final long serialVersionUID = -4603699160347935409L;

    @SuppressWarnings("unchecked")
    public static final Collection<String> USER_STATES = ConstCollector.getAll("USER_STATE");

    protected transient Set<Role> _roles;

    public static final String LAST_LOGIN_CHANGE = "last login";

    public CustomerAgreement getLatestCustomerAgreementForType( CustomerAgreementType type){
        CustomerAgreement ret = null;
        for ( CustomerAgreement agreement :getCustomerAgreements()){
            if ( type.equals(agreement.getType())){
                if ( ret == null || ( ret.getAgreementDate().before(agreement.getAgreementDate()))){
                    ret = agreement;
                }
            }

        }
        return ret;
    }

    private Date getLatestCustomerAgreement( CustomerAgreementType type){
        CustomerAgreement agreement = getLatestCustomerAgreementForType(type);
        if ( agreement != null && agreement.getRevocationDate() == null){
            return agreement.getAgreementDate();
        }
        return null;
    }

    /**
     * Returns the property 'term and cond date'.
     */
    public Date getTermAndCondDate() {
        return getLatestCustomerAgreement( CustomerAgreementType.TERMS_AND_CONDITIONS);
    }
    /**
     * Returns the property 'fidor term and cond'.
     */
    public Date getFidorTermAndCond() {
        return getLatestCustomerAgreement( CustomerAgreementType.FIDOR_AGREEMENT);
    }

    /**
     * Returns the property 'brokerage agreement'.
     */
    public Date getBrokerageAgreement() {
        return getLatestCustomerAgreement( CustomerAgreementType.BROKERAGE_AGREEMENT);
    }

    /**
     * Returns the property 'privacy date'.
     */
    public Date getPrivacyDate() {
        return getLatestCustomerAgreement( CustomerAgreementType.PRIVACY_AGREEMENT);
    }

    /**
     * Returns the property 'schufa date'.
     */
    public Date getSchufaDate() {
        return getLatestCustomerAgreement( CustomerAgreementType.SCHUFA_AGREEMENT);

    }

    /**
     * Checks all documents on successful incoming credit procurements.
     * Is used in email conditions.
     */
    public boolean isLoanProcurementSuccessful() {
        boolean result = false;
        Collection<Document> documents = getDocuments();
        if (documents != null) {
            for (Document document : documents) {
                boolean isCorrectType = Document.DOCUMENT_TYPE_CREDIT_PROCUREMENT.equals(document.getType());
                boolean isCorrectState = Document.STATE_SUCCESSFUL.equals(document.getState());
                boolean isCorrectDirection = Document.DIRECTION_IN.equals(document.getDirection());
                boolean isCorrectDate = document.getDate() != null && !document.getDate().after(CurrentDate.getDate());
                boolean isCorrectTypeOfDispatch = Document.TYPE_OF_DISPATCH_MAIL.equals(document.getTypeOfDispatch());

                if (isCorrectType && isCorrectState && isCorrectDirection && isCorrectDate && isCorrectTypeOfDispatch) {
                    result = true;
                    break;
                }
            }

        }

        return result;
    }

    public boolean isInAfterTreatment() {
        boolean result = false;
        List<CreditScore> scores = getCreditScores();
        if (scores != null && !scores.isEmpty()) {
            for (CreditScore lastScore :scores ){

                if (lastScore.getType() == null || lastScore.getType().equals(CreditScore.MAIN_BORROWER) )
                    if ( SchufaScore.STATE_OPEN_AUTOMATICALLY.equals(lastScore.getSchufaScore().getState())) {
                        result = true;
                    } else {
                        result = false;
                    }
            }
        }
        return result;
    }

    public String getCalculatedState() {
        String result = getState();

        if (isInAccreditation()) {
            result = Account.USER_STATE_ACCRED_IN_PROGRESS;
        } else if (isActive()) {
            result = Account.USER_STATE_ACTIVATED;
        } else if (isDeactivated()) {
            result = Account.USER_STATE_DEACTIVATED;
        } else if (isDenied()) {
            result = Account.USER_STATE_ACCRED_DENIED;
        }

        return result;
    }

    public boolean isCeo() {
        boolean result = false;

        if (getEconomicalData() != null) {
            result = Employment.CEO.equals(getEconomicalData().getEmployment());
        }
        return result;
    }

    public boolean isGeneralFreelancer() {
        boolean result = false;

        if (getEconomicalData() != null) {
            result = Employment.FREELANCER.equals(getEconomicalData().getEmployment())
                    || Employment.TRADESMAN.equals(getEconomicalData().getEmployment())
                    || Employment.GENERAL_FREELANCER.equals(getEconomicalData().getEmployment());
        }
        return result;
    }

    private AccountRole createAccountRole(Account account, String name, Date creationDate, AccountRoleState accountRoleState) {
        AccountRole accountRole = new AccountRole();
        accountRole.setAccount(account);
        accountRole.setApprovals(new HashSet<Approval>());
        accountRole.setStates(new HashSet<AccountRoleState>());
        accountRole.setName(name);
        if (accountRoleState == null) {
            accountRoleState = AccountRoleState.createAppliedAccountRoleState(creationDate);
        }
        accountRole.getStates().add(accountRoleState);
        return accountRole;

    }

    @JsonIgnore
    public void setRole(AccountRole role) {

    }

    public Set<Role> getRoles() {
        if (_roles == null) {
            _roles = EnumSet.noneOf(Role.class);
            Collection<AccountRole> accountRoles = getAccountRoles();
            if (accountRoles != null) {
                for (AccountRole accountRole : accountRoles) {
                    _roles.add(Enum.valueOf(Role.class, accountRole.getName()));
                }
            }
        }
        return _roles;
    }

    public Set<Role> getActiveRoles() {
        Set<Role> result = new HashSet<Role>();

        Collection<AccountRole> accountRoles = getAccountRoles();
        if (accountRoles != null) {
            for (AccountRole accountRole : accountRoles) {
                AccountRoleState state = accountRole.getCurrentAccountRoleState();
                if (state != null && state.getValidUntil() == null) {
                    result.add(Enum.valueOf(Role.class, accountRole.getName()));
                }
            }
        }

        return result;
    }

    public Set<AccountRole> getActiveAccountRoles() {
        Set<AccountRole> result = new HashSet<AccountRole>();
        Collection<AccountRole> accountRoles = getAccountRoles();
        if (accountRoles != null) {
            for (AccountRole accountRole : accountRoles) {
                AccountRoleState state = accountRole.getCurrentAccountRoleState();
                if (state != null && state.getValidUntil() == null) {
                    result.add(accountRole);
                }
            }
        }

        return result;
    }

    public boolean hasAccountRoleWithState(final Role role, final String state) {
        boolean result = false;
        Collection<AccountRole> accountRoles = getAccountRoles();
        if (accountRoles != null) {
            for (AccountRole accountRole : accountRoles) {
                AccountRoleState accountRoleState = accountRole.getCurrentAccountRoleState();
                if (accountRoleState != null && accountRoleState.getValidUntil() == null) {
                    if (accountRole.getName().equals(role.name()) && (state == null || state.equals(accountRoleState.getName()))) {
                        result = true;
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * ATENTION this may return null in case that the account has multiple roles.
     *
     * @return ATENTION may be null!
     */
    public AccountRole getActiveAccountRole() {
        AccountRole result = null;
        Collection<AccountRole> accountRoles = getActiveAccountRoles();
        if (accountRoles != null) {
            if (accountRoles.size() == 1) {
                result = accountRoles.iterator().next();
            }
        }

        return result;
    }

    /**
     * Return mailAddress if enabled, otherwise return mainAddress.
     */
    public AccountAddress getAddress() {

        AccountAddress currentAddress = getMailAddress();
        if (currentAddress == null || !currentAddress.getEnabled()) {
            currentAddress = getMainAddress();
        }

        return currentAddress;
    }

    /**
     * Returns previous main address.
     * Use getPreviousAddress(String addressType), getPreviousMailAddress() or getPreviousMainAddress() instead of getPreviousAddress().
     */
    @Deprecated
    public AccountAddress getPreviousAddress() {
        // fallback to main address for backward compatibility
        return getPreviousMainAddress();
    }

    public AccountAddress getMailAddress() {
        return getAddress(Address.TYPE_MAIL_ADDRESS);
    }

    public AccountAddress getMainAddress() {
        return  getAddress(Address.TYPE_MAIN_ADDRESS);
    }

    public AccountAddress getBorrowerEmployerAddress() {

        AccountAddress accountAddress = getAddress(Address.TYPE_BORROWER_EMPLOYER_ADDRESS);

        if (accountAddress != null && accountAddress.getExpirationDate() == null) {
            return accountAddress;
        } else {
            return null;
        }
    }

    public AccountAddress getCoBorrowerEmployerAddress() {
        AccountAddress accountAddress = getAddress(Address.TYPE_CO_BORROWER_EMPLOYER_ADDRESS);
        if (accountAddress != null && accountAddress.getExpirationDate() == null) {
            return accountAddress;
        } else {
            return null;
        }
    }

    public AccountAddress getCoBorrowerSchufaAddress() {
        AccountAddress accountAddress = getAddress(Address.TYPE_CO_BORROWER_SCHUFA_ADDRESS);
        return accountAddress;
    }

    public AccountAddress getAddress(String addressType) {
        AccountAddress address = null;
        if (getAddresses() == null) {
            setAddresses(new ArrayList<AccountAddress>());
        }

        List<AccountAddress> addresses = getAddresses();
        if (!addresses.isEmpty()) {

            // backward address list iterator to find last address
            ListIterator<AccountAddress> addressIter = addresses.listIterator(addresses.size());
            while (addressIter.hasPrevious()) {
                AccountAddress candidate = (AccountAddress) addressIter.previous();
                if (addressType.equals(candidate.getType())) {
                    address = candidate;
                    break;
                }
            }
        }
        return address;
    }

    /**
     * Get addresses by type.
     */
    public List<AccountAddress> getAddresses(String addressType) {
        List<AccountAddress> resultAddresses = new ArrayList<AccountAddress>();
        if (getAddresses() == null) {
            setAddresses(new ArrayList<AccountAddress>());
        }

        List<AccountAddress> addresses = getAddresses();
        if (!addresses.isEmpty()) {

            // backward address list iterator to find last address
            ListIterator<AccountAddress> addressIter = addresses.listIterator(addresses.size());
            while (addressIter.hasPrevious()) {
                AccountAddress candidate = (AccountAddress) addressIter.previous();
                if (addressType.equals(candidate.getType())) {
                    resultAddresses.add(candidate);
                }
            }
        }
        return resultAddresses;
    }

    public AccountAddress getPreviousMailAddress() {
        return getPreviousAddress(Address.TYPE_MAIL_ADDRESS);
    }

    public AccountAddress getPreviousMainAddress() {
        return getPreviousAddress(Address.TYPE_MAIN_ADDRESS);
    }

    public AccountAddress getPreviousBorrowerEmployerAddress() {
        return getPreviousAddress(Address.TYPE_BORROWER_EMPLOYER_ADDRESS);
    }

    public AccountAddress getPreviousCoBorrowerEmployerAddress() {
        return getPreviousAddress(Address.TYPE_CO_BORROWER_EMPLOYER_ADDRESS);
    }

    public AccountAddress getPreviousAddress(String addressType) {
        AccountAddress prevAddress = null;
        if (getAddresses() == null) {
            setAddresses(new ArrayList<AccountAddress>());
        }

        int candiateCount = 0;
        List<AccountAddress> addresses = getAddresses();
        if (!addresses.isEmpty()) {

            // backward address list iterator to find last address
            ListIterator<AccountAddress> addressIter = addresses.listIterator(addresses.size());
            while (addressIter.hasPrevious()) {
                AccountAddress candidate = (AccountAddress) addressIter.previous();
                if (addressType.equals(candidate.getType())) {

                    // get second match
                    candiateCount++;
                    if (candiateCount == 2) {
                        prevAddress = candidate;
                        break;
                    }
                }
            }
        }
        return prevAddress;
    }

    public BankAccount getReferenceBankAccount() {
        return getBankAccountByType(BankAccount.TYPE_REFRENCE_ACCOUNT);
    }

    public BankAccount getBankAccount() {
        return getReferenceBankAccount();
    }

    public boolean getHasBiwInternalBankAccount(){
        return null != getInternalBankAccount(BankAccountProvider.BIW);
    }

    public boolean hasInternalBankAccount(){
        for (BankAccount bank : getActiveBankAccounts()) {
            if (bank.isInternalBankAccount()){
                return true;
            }
        }
        return false;
    }

    public BankAccount getInternalBankAccount(String bankAccountProviderName) {
        BankAccountProvider provider = null;

        for (BankAccount bankAccount : getBankAccounts()) {
            if (null != bankAccount.getProvider() && bankAccountProviderName.equals(bankAccount.getProvider().getName())) {
                provider = bankAccount.getProvider();
                break;
            }
        }

        return null == provider ? null : getInternalBankAccount(provider);
    }

    public BankAccount getInternalBankAccount(BankAccountProvider bankAccountProvider) {
        BankAccount bankAccount = null;

        if (isLender()) {
            bankAccount = getBankAccountByTypeAndProvider(BankAccount.TYPE_LENDER_ACCOUNT, bankAccountProvider);
        } else if (isBorrower()) {
            bankAccount = getBankAccountByTypeAndProvider(BankAccount.TYPE_BORROWER_ACCOUNT, bankAccountProvider);
        } else if ( isVoidBorrower()) {
            bankAccount = getBankAccountByTypeAndProvider(BankAccount.TYPE_BORROWER_ACCOUNT, bankAccountProvider);
        } else if ( isVoidLender()) {
            bankAccount = getBankAccountByTypeAndProvider(BankAccount.TYPE_LENDER_ACCOUNT, bankAccountProvider);
        }
        return bankAccount;
    }

    /**
     * This is sad excuse for good software design. Since there is no concept which internal bank account is a
     * preferred, default or active bank account this method is here to guess which internal bank account is likely
     * the right one to pay fees from, etc.
     *
     * @param 	isFidorActive
     * @return
     */
    public BankAccount getMostLikelyInternalBankAccount(boolean isFidorActive) {
        BankAccount internalBankAccount = null;

        List<BankAccount> internalBankAccounts = getInternalBankAccounts();

        if (internalBankAccounts.size() == 1) {
            internalBankAccount = internalBankAccounts.iterator().next();
        } else {
            if (isFidorActive) {
                for (BankAccount nextInternalBankAccount : internalBankAccounts) {
                    if (BankAccountProvider.FIDOR.equals(nextInternalBankAccount.getProvider().getName())) {
                        internalBankAccount = nextInternalBankAccount;
                    }
                }
            }

            if (internalBankAccount == null) {
                for (BankAccount nextInternalBankAccount : internalBankAccounts) {
                    if (BankAccountProvider.BIW.equals(nextInternalBankAccount.getProvider().getName())) {
                        internalBankAccount = nextInternalBankAccount;
                    }
                }
            }

            if ( internalBankAccount == null &&
                    internalBankAccounts.size() > 0 ) {
                internalBankAccount = internalBankAccounts.iterator().next();
            }
        }

        return internalBankAccount;
    }

    public List<BankAccount> getInternalBankAccounts() {
        return isLender() ? getBankAccountsByType(BankAccount.TYPE_LENDER_ACCOUNT) :
                getBankAccountsByType(BankAccount.TYPE_BORROWER_ACCOUNT);
    }

    private BankAccount getBankAccountByType(String type) {
        if (getBankAccounts() != null && !getBankAccounts().isEmpty()) {
            for (BankAccount bankAccount : getBankAccounts()) {
                if (bankAccount.getExpirationDate() == null && type.equals(bankAccount.getType())) {
                    return bankAccount;
                }
            }
        }

        return null;
    }

    public List<BankAccount> getActiveBankAccounts() {
        List<BankAccount> actives = new ArrayList<BankAccount>();
        for (BankAccount bankAccount : getBankAccounts()) {
            if (null == bankAccount.getExpirationDate() &&
                    bankAccount.getValid()){
                actives.add(bankAccount);
            }
        }
        return actives;
    }

    private List<BankAccount> getBankAccountsByType(String type) {
        List<BankAccount> bankAccounts = new LinkedList<BankAccount>();
        if (getBankAccounts() != null && !getBankAccounts().isEmpty()) {
            for (BankAccount bankAccount : getBankAccounts()) {
                if (bankAccount.getExpirationDate() == null && type.equals(bankAccount.getType())) {
                    bankAccounts.add(bankAccount);
                }
            }
        }

        return bankAccounts;
    }

    private BankAccount getBankAccountByTypeAndProvider(String type, BankAccountProvider bankAccountProvider) {
        if (getBankAccounts() != null && !getBankAccounts().isEmpty()) {
            for (BankAccount bankAccount : getBankAccounts()) {
                if (bankAccount.getExpirationDate() == null && type.equals(bankAccount.getType()) &&
                        bankAccountProvider.equals(bankAccount.getProvider())) {
                    return bankAccount;
                }
            }
        }

        return null;
    }

    public CreditScore getCurrentCreditScoreCoBorrower(){
        CreditScore coBorrowerScore = null;
        if ( this.getCreditScores() != null){
            for (CreditScore sc : this.getCreditScores()){
                if ( CreditScore.CO_BORROWER.equals( sc.getType())){
                    if ( coBorrowerScore == null
                            || coBorrowerScore.getChangeDate().before(sc.getChangeDate())){
                        coBorrowerScore = sc;
                    }
                }
            }
        }
        return coBorrowerScore;
    }

    /**
     * BankAccount has to have a provider to set as internal bank account.
     *
     * @param bankAccount
     */
    public void setInternalAccount(BankAccount bankAccount) {
        BankAccount previousAccount = getInternalBankAccount(bankAccount.getProvider());

        if (previousAccount != null) {
            Date creationDate = bankAccount.getCreationDate();
            if (creationDate != null) {
                creationDate = new Date(creationDate.getTime());
            }
            previousAccount.setExpirationDate(creationDate);
        }
        addBankAccount(bankAccount);
    }

    public void setReferenceAccount(BankAccount bankAccount) {
        BankAccount previousAccount = getReferenceBankAccount();

        if (previousAccount != null) {
            previousAccount.setExpirationDate(bankAccount.getCreationDate());
        }
        addBankAccount(bankAccount);
    }

    private void addBankAccount(BankAccount bankAccount) {
        if (getBankAccounts() == null) {
            setBankAccounts(new LinkedList<BankAccount>());
        }

        getBankAccounts().add(bankAccount);
    }

    /**
     * Generates the bank account history for the given type.
     * @param   type is one of the defined bank account types (i.e. {@link BankAccount#TYPE_BORROWER_ACCOUNT}, {@link BankAccount#TYPE_LENDER_ACCOUNT} or {@link BankAccount#TYPE_REFRENCE_ACCOUNT}).
     * @return  the list of bank accounts
     */
    private List<BankAccount> getBankAccountHistoryByType(String type) {
        List<BankAccount> bankAccounts = new LinkedList<BankAccount>();
        if ( getBankAccounts() != null && !getBankAccounts().isEmpty()) {
            for (BankAccount bAccount : getBankAccounts() ) {
                if (type.equals(bAccount.getType())) {
                    bankAccounts.add(bAccount);
                }
            }
        }
        return bankAccounts;
    }

    /**
     * Generates the bank account history for the given type.
     * @param   type is one of the defined bank account types (i.e. {@link BankAccount#TYPE_BORROWER_ACCOUNT}, {@link BankAccount#TYPE_LENDER_ACCOUNT} or {@link BankAccount#TYPE_REFRENCE_ACCOUNT}).
     * @param   bankAccountProvider the provider to look for
     * @return  the list of bank accounts
     */
    private List<BankAccount> getBankAccountHistoryByTypeAndProvider(String type, BankAccountProvider bankAccountProvider) {
        List<BankAccount> bankAccounts = new LinkedList<BankAccount>();
        if ( getBankAccounts() != null && !getBankAccounts().isEmpty()) {
            for (BankAccount bAccount : getBankAccounts() ) {
                if (type.equals(bAccount.getType()) && bankAccountProvider.equals(bAccount.getProvider())) {
                    bankAccounts.add(bAccount);
                }
            }
        }
        return bankAccounts;
    }

    public List<BankAccount> getInternalBankAccountHistory(BankAccountProvider bankAccountProvider) {

        List<BankAccount> bankAccounts = new LinkedList<BankAccount>();

        String type = "";
        if (isBorrowerAnyState()) {
            type = BankAccount.TYPE_BORROWER_ACCOUNT;
        } else if (isLenderAnyState()) {
            type = BankAccount.TYPE_LENDER_ACCOUNT;
        }
        if (!StringUtils.isEmpty(type)) {
            bankAccounts = getBankAccountHistoryByTypeAndProvider(type, bankAccountProvider);
        }

        return bankAccounts;
    }

    public List<BankAccount> getAllInternalBankAccounts(){
        List<BankAccount> bankAccounts = new ArrayList<BankAccount>();
        List<String> types = Arrays.asList(
                BankAccount.TYPE_BORROWER_ACCOUNT,
                BankAccount.TYPE_LENDER_ACCOUNT);
        for (BankAccount bAccount : getBankAccounts() )
            if (types.contains(bAccount.getType()))
                bankAccounts.add(bAccount);
        return bankAccounts;
    }

    public List<BankAccount> getInternalBankAccountHistories() {

        List<BankAccount> bankAccounts = new LinkedList<BankAccount>();

        String type = "";
        if (isBorrowerAnyState()) {
            type = BankAccount.TYPE_BORROWER_ACCOUNT;
        } else if (isLenderAnyState()) {
            type = BankAccount.TYPE_LENDER_ACCOUNT;
        }
        if (!StringUtils.isEmpty(type)) {
            bankAccounts = getBankAccountHistoryByType(type);
        }

        return bankAccounts;
    }

    public List<BankAccount> getOtherBankAccounts() {
        return getBankAccountHistoryByType(BankAccount.TYPE_OTHER_ACCOUNT);
    }

    public List<BankAccount> getReferenceBankAccountHistory() {
        return getBankAccountHistoryByType(BankAccount.TYPE_REFRENCE_ACCOUNT);
    }

    public List<BankAccount> getConsolidationBankAccountHistory() {
        return getBankAccountHistoryByType(BankAccount.TYPE_DEBT_CONSOLIDATION_ACCOUNT);
    }

    // only used for tests
    protected void setBankAccount(BankAccount bankAccount) {
        if (getBankAccounts() == null) {
            setBankAccounts(new LinkedList<BankAccount>());
        }

        getBankAccounts().add(bankAccount);
    }

    public boolean isLender() {
        boolean result = false;
        if (!isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_LENDER.name())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }


    public boolean isAssetManager() {
        boolean result = false;
        if (!isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_ASSET_MANAGER.name())) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    public boolean isBorrower() {
        return isBorrower(false);
    }

    private boolean isBorrower(final boolean checkAccepted) {
        boolean result = false;
        if (!isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_BORROWER.name())) {
                    if (checkAccepted) {
                        result = AccountRoleState.STATE_ACCEPTED.equals(role.getCurrentAccountRoleState().getName());
                    } else {
                        result = true;
                    }
                    break;
                }
            }
        }

        return result;
    }

    public boolean isVoidBorrower() {
        boolean result = false;
        if (isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_BORROWER.name())) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    public boolean isVoidLender() {
        boolean result = false;
        if (isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_LENDER.name())) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    public boolean isBorrowerAnyState() {
        return hasAccountRoleWithState(Role.ROLE_BORROWER, null);
    }

    public boolean isLenderAnyState() {
        return hasAccountRoleWithState(Role.ROLE_LENDER, null);
    }

    public boolean isBorrowerFriend() {
        return isMember() && Role.ROLE_BORROWER_FRIEND.getAuthority().equals(getPreferredRole());
    }

    public boolean isMember() {
        boolean result = false;
        if (isVoid()) {
            result = true;
        } else {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_MEMBER.name())) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public boolean isAdvisor() {
        boolean result = false;
        if (!isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_CREDIT_ADVISOR.name())
                        || role.getName().equals(Role.ROLE_CREDIT_ADVISORY_ADMIN.name())
                        || role.getName().equals(Role.ROLE_TEAM_LEAD.name())
                        ) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    public boolean isAcceptedMember() {
        boolean result = false;
        if (!isVoid()) {
            for (AccountRole role : getActiveAccountRoles()) {
                if (role.getName().equals(Role.ROLE_MEMBER.name())) {
                    result = AccountRoleState.STATE_ACCEPTED.equals(role.getCurrentAccountRoleState().getName());
                    break;
                }
            }
        }

        return result;
    }

    public boolean isShortLead() {
        return StringConstants.SHORTLEAD.equals(getPreferredRole());
    }

    public boolean isShortLeadMember() {
        return isShortLead() && isMember();
    }

    public boolean isGuest() {
        boolean result = false;
        for (AccountRole role : getActiveAccountRoles()) {
            if (role.getName().equals(Role.ROLE_GUEST.name())) {
                result = true;
                break;
            }
        }

        return result;
    }

    public boolean isDeactivated() {
        return Account.USER_STATE_DEACTIVATED.equals(getState());
    }

    public boolean isActive() {
        boolean result = false;
        if (Account.USER_STATE_ACTIVATED.equals(getState())) {
            AccountRole currentRole = getActiveAccountRole();
            if (currentRole != null) {
                if (currentRole.getCurrentAccountRoleState() != null) {
                    result = AccountRoleState.STATE_ACCEPTED.equals(currentRole.getCurrentAccountRoleState().getName());
                }
            }
        }
        return result;
    }

    public boolean isAcceptedBorrower() {
        return isBorrower(true);
    }

    public boolean isInAccreditation() {
        boolean result = false;
        if (Account.USER_STATE_ACTIVATED.equals(getState())) {
            AccountRole currentRole = getActiveAccountRole();
            if (currentRole != null) {
                if (currentRole.getCurrentAccountRoleState() != null) {
                    result = AccountRoleState.STATE_APPLIED.equals(currentRole.getCurrentAccountRoleState().getName());
                }
            }
        }
        return result;
    }

    public boolean isDenied() {
        boolean result = false;
        if (Account.USER_STATE_ACTIVATED.equals(getState())) {
            Collection<AccountRole> accountRoles = getActiveAccountRoles();
            if (accountRoles != null) {
                if (accountRoles.size() == 1) {
                    AccountRole currentRole = accountRoles.iterator().next();

                    if (currentRole.getCurrentAccountRoleState() != null) {
                        result = AccountRoleState.STATE_DENIED.equals(currentRole.getCurrentAccountRoleState().getName());
                    }
                }
            }
        }
        return result;
    }

    public boolean isVoid() {
        boolean result = false;
        if (Account.USER_STATE_ACTIVATED.equals(getState())) {
            Collection<AccountRole> accountRoles = getActiveAccountRoles();
            if (accountRoles != null) {
                if (accountRoles.size() == 1) {
                    AccountRole currentRole = accountRoles.iterator().next();

                    if (currentRole.getCurrentAccountRoleState() != null) {
                        result = AccountRoleState.STATE_VOID.equals(currentRole.getCurrentAccountRoleState().getName());
                    }
                }
            }
        }
        return result;
    }

    public boolean isFake() {
        return Account.USER_STATE_FAKE.equals(getState());
    }

    public boolean isDeleted() {
        return Account.USER_STATE_DELETED.equals(getState());
    }

    /**
     * Returns the property 'market'.
     */
    public String getMarket() {
        String market = null;
        if (getCurrentCreditScore() != null) {
            market = getCurrentCreditScore().getRating();
        }
        return market;
    }

    public List<Contract> getContracts(Role role) {
        List<Contract> result = new ArrayList<Contract>();
        Set<? extends Bid> bids = null;
        if (role.equals(Role.ROLE_BORROWER)) {
            bids = getOrders();
        } else if (role.equals(Role.ROLE_LENDER)) {
            bids = getOffers();
        }
        for (Bid bid : bids) {
            if (bid.getContract() != null) {
                result.add(bid.getContract());
            }
        }
        return result;
    }

    public Person getPerson(int type) {
        Person result = null;
        Collection<Person> personHistory = this.getPersonHistory();
        if (personHistory != null && !personHistory.isEmpty()) {
            for (Person person : personHistory) {
                if (person.getValidUntil() == null && person.getType() == type) {
                    result = person;
                    break;
                }
            }
        }

        return result;
    }

    public Collection<Person> getActivePersons() {
        final Collection<Person> activePersons = new LinkedList<Person>();
        final Collection<Person> personHistory = this.getPersonHistory();
        if (personHistory != null && !personHistory.isEmpty()) {
            for (Person person : personHistory) {
                if (person.getValidUntil() == null) {
                    activePersons.add(person);
                }
            }
        }
        return activePersons;
    }

    public Person getPerson() {
        return getPerson(Person.PRIMARY_PERSON_TYPE);
    }

    public Person getSecondaryPerson() {
        return getPerson(Person.SECONDARY_PERSON_TYPE);
    }

    public void setPerson(Person person) {
        setPerson(person, Person.PRIMARY_PERSON_TYPE);
    }

    public void setSecondaryPerson(Person person) {
        setPerson(person, Person.SECONDARY_PERSON_TYPE);
    }

    public void setPerson(final Person person, final int personType) {
        if (getPersonHistory() == null) {
            setPersonHistory(new HashSet<Person>());
        } else {
            Person actualPerson = getPerson(personType);
            if (actualPerson != null) {
                actualPerson.setValidUntil(CurrentDate.getDate());
            }
        }
        this.getPersonHistory().add(person);
    }

    public EconomicalData getEconomicalData( ) {
        EconomicalData result = null;
        if (this.getEconomicalDataHistory() != null && !this.getEconomicalDataHistory().isEmpty()) {
            for ( EconomicalData ed:getEconomicalDataHistory()){
                result = ed;
                break;
            }
        }
        return result;
    }

    /**
     * returns that latest ED that is either changed by a credit advisor or the customer
     * @return
     */
    public EconomicalData getCustomerRelatedEconomicalData( ) {
        EconomicalData resultAdv = getEconomicalData( EconomicalDataType.CREDIT_ADVISOR);
        EconomicalData resultCust = getEconomicalData( EconomicalDataType.CUSTOMER);

        if ( resultAdv != null) {
            if ( resultCust == null){
                return resultAdv;
            }
            if ( resultAdv.getCreationDate().after(resultCust.getCreationDate())) {
                return resultAdv;
            }
        }
        return resultCust;
    }

    /**
     * returns that latest ED that is related to an order that is not deleted
     * @return
     */
    public EconomicalData getOrderRelatedEconomicalData( ) {
        EconomicalData resultAdv = null;
        for ( Order order : getOrders()){
            if ( order != null
                    && order.getCurrentEconomicalData() != null
                    && ! Order.STATE_DELETED.equals(order.getState())) {
                if ( resultAdv == null || resultAdv.getCreationDate().before(order.getCurrentEconomicalData().getCreationDate())) {
                    resultAdv = order.getCurrentEconomicalData();
                }
            }
        }
        return resultAdv;
    }

    /**
     * attention this access is slow. in case of larger list of data please access data an other way!
     * @param type
     * @return
     */
    public EconomicalData getEconomicalData( EconomicalDataType type) {
        EconomicalData result = null;
        if (this.getEconomicalDataHistory() != null && !this.getEconomicalDataHistory().isEmpty()) {
            for ( EconomicalData ed:getEconomicalDataHistory()){
                if ( type.equals( ed.getEconomicalDataType())){
                    result = ed;
                    break;
                }
            }
        }
        return result;
    }

    public void setEconomicalData(EconomicalData data) {
        if (getEconomicalDataHistory() == null) {
            this.setEconomicalDataHistory(new ArrayList<EconomicalData>());
        }

        this.getEconomicalDataHistory().add(0, data);
    }

    public void addDocument(Document document) {
        if (getDocuments() == null) {
            setDocuments(new LinkedList<Document>());
        }

        getDocuments().add(document);
    }

    public void removeDocument(Document document) {
        if (getDocuments() != null) {
            getDocuments().remove(document);
        }
    }

    public void addMarkedOrder(Order order) {
        if (getMarkedOrders() == null) {
            setMarkedOrders(new ArrayList<Order>());
        }

        if (!getMarkedOrders().contains(order)) {
            getMarkedOrders().add(order);
        }
    }

    public void removeMarkedOrder(Order order) {
        if (getMarkedOrders() != null) {
            if (!getMarkedOrders().isEmpty()) {
                getMarkedOrders().removeAll( Collections.singleton((order)));
            }
        }
    }

    public void addOrderRecommendation(OrderRecommendation recommendation) {
        if (getOrderRecommendations() == null) {
            setOrderRecommendations(new ArrayList<OrderRecommendation>());
        }

        getOrderRecommendations().add(recommendation);
    }

    @JsonIgnore
    public OrderRecommendation getLastRecommendation() {
        OrderRecommendation result = null;
        if (getOrderRecommendations() != null && getOrderRecommendations().size() > 0) {
            result = getOrderRecommendations().get(getOrderRecommendations().size() - 1);
        }

        return result;
    }


    public int getAgeInDays() {
        Interval age = null;
        if (this.getCreationDate().getTime() < CurrentDate.getTime()) {
            age = new Interval(this.getCreationDate().getTime(), CurrentDate.getTime());
        } else {
            age = new Interval(CurrentDate.getTime(), this.getCreationDate().getTime());
        }

        return age.toPeriod(PeriodType.days()).getDays();
    }

    /**
     *
     * @return ob der Account per Postident oder PA-Kopie oder Online identifiziert wurde.
     */
    public boolean isIdentified() {
        return this.isPersonalIdIdentified() || this.isPostOrVideoIdentified() || this.isOnlineIdentified();
    }

    public boolean isOnlineIdentified() {
        boolean result = false;
        final Person person = getPerson();
        if (person != null) {
            final IdCard idCard = person.getCurrentIdCard();
            if (idCard != null && idCard.getValidityDate() != null) {
                result = true;
            }
        }
        return result;
    }

    /**
     *
     * @return true if account is identified with postident
     */
    public boolean isPostIdentified() {
        boolean result = false;
        Document document = getLastDocumentByType(Document.DOCUMENT_TYPE_POSTIDENT);
        if (document != null && Document.APPROVAL_STATE_OK.equals(document.getApprovalState())) {
            result = true;
        }

        return result;
    }

    /**
     *
     * @return true if account is identified with videoident
     */
    public boolean isVideoIdentified() {
        boolean result = false;
        Document document = getLastDocumentByType(Document.DOCUMENT_TYPE_VIDEOIDENT);
        if (document != null && Document.APPROVAL_STATE_OK.equals(document.getApprovalState())) {
            result = true;
        }

        return result;
    }

    /**
     *
     * @return true if account is identified with postident or videoident
     */
    public boolean isPostOrVideoIdentified() {
        return isPostIdentified() || isVideoIdentified();
    }

    /**
     *
     * @return ob der Account per PA-Kopie identifiziert wurde.
     */
    public boolean isPersonalIdIdentified() {
        boolean result = false;
        Document document = getLastDocumentByType(Document.DOCUMENT_TYPE_ID_COPY);
        if (document != null && Document.APPROVAL_STATE_OK.equals(document.getApprovalState())) {
            result = true;
        }

        return result;
    }

    /**
     * Returns the last incoming document from given type that is successful and not deleted.
     * Returns null if it does not find any document that fulfills these criteria.
     */
    public Document getLastDocumentByType(String documentType) {

        Document lastDocument = null;

        List<Document> documents = getDocuments();
        LOGGER.debug("try to get last " + documentType + " document for user '" + getUsername() + "' (" + getId() + ")");

        if (documents != null) {
            for (Document document : documents) {
                if (!Document.STATE_DELETED.equals(document.getState())
                        && documentType.equals(document.getType())
                        && Document.DIRECTION_IN.equals(document.getDirection())
                        && Document.STATE_SUCCESSFUL.equals(document.getState())) {
                    if (lastDocument == null) {
                        lastDocument = document;
                    } else {
                        if (lastDocument.getDate() != null && document.getDate() != null) {
                            if (document.getDate().after(lastDocument.getDate())) {
                                lastDocument = document;
                            } else if (lastDocument.getDate().equals(document.getDate()) && !document.getCreationDate().before(lastDocument.getCreationDate())) {
                                lastDocument = document;
                            }
                        } else {
                            if (document.getCreationDate().after(lastDocument.getCreationDate())) {
                                lastDocument = document;
                            }
                        }
                    }
                }
            }
        }

        LOGGER.debug("last postident document for user '" + getUsername() + "' (" + getId() + ") is " + ((lastDocument == null) ? "not set" : "set"));

        return lastDocument;
    }

    /**
     * Returns the last outgoing document from given type that is not deleted.
     * Returns null if it does not find any document that fulfills these criteria.
     */
    public Document getLastOutgoingDocumentByType(String documentType) {

        Document lastDocument = null;

        List<Document> documents = getDocuments();
        LOGGER.debug("try to get last " + documentType + " document for user '" + getUsername() + "' (" + getId() + ")");

        if (documents != null) {
            for (Document document : documents) {
                if (!Document.STATE_DELETED.equals(document.getState())
                        && documentType.equals(document.getType())
                        && Document.DIRECTION_OUT.equals(document.getDirection())) {
                    if (lastDocument == null) {
                        lastDocument = document;
                    } else {
                        if (lastDocument.getDate() != null && document.getDate() != null) {
                            if (document.getDate().after(lastDocument.getDate())) {
                                lastDocument = document;
                            } else if (lastDocument.getDate().equals(document.getDate()) && !document.getCreationDate().before(lastDocument.getCreationDate())) {
                                lastDocument = document;
                            }
                        } else {
                            if (document.getCreationDate().after(lastDocument.getCreationDate())) {
                                lastDocument = document;
                            }
                        }
                    }
                }
            }
        }

        LOGGER.debug("last postident document for user '" + getUsername() + "' (" + getId() + ") is " + ((lastDocument == null) ? "not set" : "set"));

        return lastDocument;
    }

    public Date getLastRequestedDocumentsDate() {
        Date result = null;

        for (Document document : getDocuments()) {
            if (Document.STATE_SUCCESSFUL.equals(document.getState())
                    && Document.DIRECTION_IN.equals(document.getDirection())
                    && !Document.TYPE_OF_DISPATCH_PHONE.equals(document.getTypeOfDispatch())
                    && !Document.TYPE_OF_DISPATCH_INTERNAL.equals(document.getTypeOfDispatch())
                    && Document.APPROVAL_STATE_OK.equals(document.getApprovalState())
                    && document.getAttachments() != null && !document.getAttachments().isEmpty()) {

                if (result == null) {
                    result = document.getDate();
                } else if (result.before(document.getDate())) {
                    result = document.getDate();
                }
            }
        }

        return result;
    }

    /**
     * Get the value for Fidor Mandate SigratureDate request field based on SQL script:
     * select (case when max(date)>='2014-02-01' then max(date) else '2014-02-01' end) as mdate
     from document d
     where type in ('CREDIT_AGREEMENT','DEBIT_AUTHORIZATION') and state='SUCCESSFUL' and direction='IN' and approval_state='OK'
     and (date_trunc('day',date)<=(select min(start_date) from contract where order_id in (select id from "order" where account_id=d.account_id))
     )
     group by account_id
     order by min_start

     * @return
     */
    public Date getMandateSignatureDate() {
        Date result = null;

        for (Document document : getDocuments()) {
            if((Document.DOCUMENT_TYPE_CREDIT_AGREEMENT.equals(document.getType()) || Document.DOCUMENT_TYPE_DEBIT_AUTHORIZATION.equals(document.getType())) &&
                    Document.STATE_SUCCESSFUL.equals(document.getState()) &&
                    Document.DIRECTION_IN.equals(document.getDirection()) &&
                    Document.APPROVAL_STATE_OK.equals(document.getApprovalState())) {

                for (Order order : getOrders()) {
                    if ( order.getState().equals(Bid.STATE_MATCHED)
                            && order.getContract() != null
                            && order.getContract().getStartDate() != null) {
                        if (DateUtils.truncate(document.getDate(), Calendar.DAY_OF_MONTH).before(DateUtils.truncate(order.getContract().getStartDate(), Calendar.DAY_OF_MONTH)) ||
                                DateUtils.truncate(document.getDate(), Calendar.DAY_OF_MONTH).equals(DateUtils.truncate(order.getContract().getStartDate(), Calendar.DAY_OF_MONTH))) {
                            if (result == null) {
                                result = document.getDate();
                            } else if (result.before(document.getDate())) {
                                result = document.getDate();
                            }
                        }
                    }
                }
            }
        }

        return result;
    }

    public Date getLastUncheckedRequestedDocumentsDate() {
        Date result = null;

        for (Document document : getDocuments()) {
            if (Document.STATE_SUCCESSFUL.equals(document.getState())
                    && Document.DIRECTION_IN.equals(document.getDirection())
                    && !Document.TYPE_OF_DISPATCH_PHONE.equals(document.getTypeOfDispatch())
                    && !Document.TYPE_OF_DISPATCH_INTERNAL.equals(document.getTypeOfDispatch())
                    && (Document.APPROVAL_STATE_UNCHECKED.equals(document.getApprovalState()))
                    && document.getAttachments() != null && !document.getAttachments().isEmpty()) {

                if (result == null) {
                    result = document.getDate();
                } else if (result.after(document.getDate())) {
                    result = document.getDate();
                }
            }
        }

        return result;
    }

    @JsonIgnore
    public int getAgeInYears() {
        Interval age = null;
        Date birthDay = this.getPerson().getDateOfBirth();
        Date now = CurrentDate.getDate();
        if (birthDay.getTime() < now.getTime()) {
            age = new Interval(birthDay.getTime(), now.getTime());
        } else {
            age = new Interval(now.getTime(), birthDay.getTime());
        }

        return age.toPeriod(PeriodType.years()).getYears();
    }


    /**
     * @return <code>true</code> if and only if the current account is a member and the {@link #getPreferredRole()}
     * 			indicates that borrower registration was started. In Case of registration aborted or short leads.
     */
    public boolean isGoingToBeBorrower() {
        final String preferedRole = getPreferredRole();
        return preferedRole != null && GOING_TO_BE_BORROWER_PREFEREDROLES.contains(preferedRole) && isMember();
    }

    /**
     * @return <code>true</code> if and only if the current account is a member and the {@link #getPreferredRole()}
     * 			indicates that borrower registration was started. In Case of registration aborted or short leads,
     * 			or sales lead.
     */
    public boolean isGoingToBeBorrowerOrSalesLead() {
        final String preferedRole = getPreferredRole();
        return preferedRole != null && GOING_TO_BE_BORROWER_OR_SALESLEAD_PREFEREDROLES.contains(preferedRole) && isMember();
    }

    public boolean isBrokerageAccount() {
        return getBrokerageNetIncome() != null || getBrokerageDisposableIncome() != null;
    }

    /**
     * Check if the account is already migrated to fidor.
     *
     * @return <code>true</code> if {@link #getFidorTermAndCond()} is not null.
     */
    public boolean isFidorMigratedAccount() {
        return getFidorTermAndCond() == null ? Boolean.FALSE : Boolean.TRUE;
    }

    /**
     * Check if the account has an internal bank account of the given provider
     *
     * @param bankAccountProvider
     *            the bank account provider to check
     * @return <code>true</code> if the account has an internal bank account of
     *         the given bank account provider
     */
    public boolean hasInternalBankAccount(final String bankAccountProvider) {
        final List<BankAccount> bankAccounts = this.getInternalBankAccounts();
        boolean result = Boolean.FALSE;

        for (BankAccount bankAccount : bankAccounts) {
            if (bankAccountProvider.equals(bankAccount.getProvider().getName())) {
                result = Boolean.TRUE;
                break;
            }
        }

        return result;
    }

    public boolean hasDefaultedContracts(){
        List<Contract> contracts = getContracts(Role.ROLE_BORROWER);

        boolean defaulted = false;
        for (Contract contr : contracts){
            if ( Contract.STATE_DEFAULT.equals( contr.getState())){
                defaulted = true;
                break;
            }
        }
        return defaulted;
    }

    public void setRole(Role role) {
        Set<Role> roles = new HashSet<Role>();
        roles.add(role);
        setRoles(roles);
    }

    public void setRoles(Set<Role> roles) {
        if (getAccountRoles() == null) {
            setAccountRoles(new HashSet<AccountRole>());
        }

        Date currentDate = CurrentDate.getDate();

        for (AccountRole currentRole : getActiveAccountRoles()) {
            if (currentRole.getCurrentAccountRoleState() != null) {
                if (currentRole.getCurrentAccountRoleState().getValidUntil() == null) {
                    currentRole.getCurrentAccountRoleState().setValidUntil(currentDate);
                }
            }
        }

        for (Role role : roles) {
            getRoles().add(role);
            AccountRole newRole = createAccountRole(this.instance(), role.name(), currentDate, null);
            getAccountRoles().add(newRole);
        }

        _roles = roles;
    }

    public void addAddress(AccountAddress address) {
        // avoid NPE
        if (getAddresses() == null) {
            setAddresses(new ArrayList<AccountAddress>());
        }

        // find old address with corresponding type of added address for expiration!
        AccountAddress oldAddress = null;
        if (address.isMailAddress()) {
            oldAddress = getMailAddress();
        } else if (address.isMainAddress()) {
            oldAddress = getMainAddress();
        } else if (address.isBorrowerEmployerAddress()) {
            oldAddress = getBorrowerEmployerAddress();
        } else if (address.isCoBorrowerEmployerAddress()) {
            oldAddress = getCoBorrowerEmployerAddress();
        } else if (address.isCoBorrowerSchufaAddress()){
            oldAddress = getCoBorrowerSchufaAddress();
        } else {
            throw new RuntimeException("Unknown address type. Don't know how to handle!");
        }

        if ( oldAddress != null){
            if (oldAddress.functionallyEquals(address)){
                return;
            }
        }

        // expire old address
        if (oldAddress != null) {
//        	only set expiration date to now, when the address doesn't have an expiration date
            if (oldAddress.getExpirationDate() == null) {
                oldAddress.setExpirationDate(CurrentDate.getDate());
            }
            if (oldAddress.getAccount() == null) {
                oldAddress.setAccount(this.instance());
            }

            // don't disable old address [could be main address, when new mail address is added]
            // oldAddress.setEnabled(false);
        }

        // force assign to this account
        if (address.getAccount() != this) {
            address.setAccount(this.instance());
        }

        // add new address
        List<AccountAddress> addresses = getAddresses();
        addresses.add(address);
    }

    public void addRole(Role role) {
        addRole(role, null);
    }

    public void addRole(Role role, AccountRoleState accountRoleState) {
        getRoles().add(role);
        if (getAccountRoles() == null) {
            setAccountRoles(new HashSet<AccountRole>());
        }
        AccountRole newRole = createAccountRole(this.instance(), role.name(), null, accountRoleState);
        for (AccountRole currentRole : getActiveAccountRoles()) {
            if (currentRole.getCurrentAccountRoleState() != null) {
                if (currentRole.getCurrentAccountRoleState().getValidUntil() == null) {
                    currentRole.getCurrentAccountRoleState().setValidUntil(newRole.getCurrentAccountRoleState().getValidFrom());
                }
            }
        }

        getAccountRoles().add(newRole);
    }

    public void addBidSearch(BidSearch bidSearch) {
        if (getBidSearches() == null) {
            setBidSearches(new ArrayList<BidSearch>());
        }
        if (!this.equals(bidSearch.getAccount())) {
            bidSearch.setAccount(this.instance());
        }
        getBidSearches().add(bidSearch);
    }

    public void addCreditScore(CreditScore score) {
        if (getCreditScores() == null) {
            setCreditScores(new ArrayList<CreditScore>());
        } else {
			/* there are some inconsistencies when this method is called, so this is just to make sure that the score
				is not added when it already exists. normally this should not be necessary, but it seams it is.
				--> happy refacturing. */
            for (CreditScore nextCreditScore : getCreditScores()) {
                if (nextCreditScore.equals(score)) {
                    return;
                }
            }
        }

        if (score.getAccount() != this) {
            score.setAccount(this.instance());
        }

        getCreditScores().add(score);
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        list.add(this.instance());
        return list;
    }

    public abstract Account instance();

}

