package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;

import java.util.*;

import de.smava.webapp.account.domain.interfaces.BidSearchEntityInterface;
import de.smava.webapp.account.domain.interfaces.BidEntityInterface;import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'BidSearchs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBidSearch
    extends Bid    implements BidSearchEntityInterface    ,BidEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractBidSearch.class);

    private static final long serialVersionUID = 1L;

    private transient long _hits = 0;
    private transient int _minTerm = 0;
    private transient int _maxTerm = 0;
    private transient String _minRating = "";
    private transient String _maxRating = "";

    public static final String TYPE_BID_AGENT = "bid.agent";
    public static final String TYPE_MAIL_NOTIFICATION = "mail.notification";
    public static final String TYPE_PORTFOLIO_AGENT = "portfolio.agent";

    public static final List<String> BID_AGENT_TYPES = Collections.unmodifiableList(Arrays.asList(TYPE_BID_AGENT, TYPE_PORTFOLIO_AGENT));

    public static final Set<String> CONTRACT_STATES;

    public static final Set<String> CONTRACT_PAYED_OUT_STATES;

    static {
        final Set<String> states = new HashSet<String>();
        states.add(Contract.STATE_NEW);
        states.add(Contract.STATE_PENDING);
        states.add(Contract.STATE_REPAY);
        states.add(Contract.STATE_CANCELED);
        states.add(Contract.STATE_LATE);
        states.add(Contract.STATE_DEFAULT);
        states.add(Contract.STATE_TO_BE_CANCELED);
        states.add(Contract.STATE_TO_BE_OBJECTED);
        CONTRACT_STATES = Collections.unmodifiableSet(states);

        final Set<String> states2 = new HashSet<String>();
        states2.add(Contract.STATE_REPAY);
        states2.add(Contract.STATE_CANCELED);
        states2.add(Contract.STATE_LATE);
        states2.add(Contract.STATE_DEFAULT);
        states2.add(Contract.STATE_TO_BE_CANCELED);
        states2.add(Contract.STATE_COMPLETE);
        CONTRACT_PAYED_OUT_STATES = Collections.unmodifiableSet(states2);
    }


    public static Set<String> getContractStates() {
        return CONTRACT_STATES;
    }

    public static Set<String> getPayedOutContractStates() {
        return CONTRACT_PAYED_OUT_STATES;
    }

    private transient Collection<EmploymentType> _employmentTypes;

    public static final int GENDER_BOTH = 0;
    public static final int GENDER_MAN = 1;
    public static final int GENDER_WOMAN = 2;



    public Set<String> getRatings() {
        Set<String> ratings = new TreeSet<String>();
        for (BidInterest bi : this.getActiveBidInterests()) {
            ratings.add(bi.getRating());
        }
        return ratings;
    }

    public SortedSet<String> getDuration() {
        SortedSet<String> duration = new TreeSet<String>();
        for (BidInterest bi : this.getActiveBidInterests()) {
            duration.add(bi.getDuration()); // bi.getDuration().startsWith("0") ? bi.getDuration().substring(1) : bi.getDuration()
        }
        return duration;
    }

    public void addBidInterest(BidInterest bidInterest) {
        if (getBidInterests() == null) {
            setBidInterests(new ArrayList<BidInterest>());
        }
        getBidInterests().add(bidInterest);
        if (bidInterest.getBid() != this) {
            bidInterest.setBid(this);
        }
    }

    public BidSearchGroup getBidSearchPortfolio() {
        return getBidSearchGroup(BidSearchGroup.TYPE_PORTFOLIO);
    }

    public BidSearchGroup getBidSearchTranche() {
        return getBidSearchGroup(BidSearchGroup.TYPE_TRANCHE);
    }

    private BidSearchGroup getBidSearchGroup(String type) {
        BidSearchGroup portfolio = null;
        Collection<BidSearchGroup> groups = getBidSearchGroups();
        if (groups != null) {
            for (BidSearchGroup bidSearchGroup : groups) {
                if (type.equals(bidSearchGroup.getType())) {
                    portfolio = bidSearchGroup;
                    break;
                }
            }
        }

        return portfolio;
    }

    @Override
    public Contract getContract() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public long getGroupId() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public Date getLastMarketplaceDate() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public Map<String, Float> getRatesForMarkets() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean hasContract() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateDeactivated() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateDeleted() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateExpired() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateMatched() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateNew() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateObsolete() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateOpen() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public boolean isStateQueued() {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public void putRateForMarket(String marketName, Float rate) {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public void removeRateForMarket(String marketName) {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public void setContract(Contract contract) {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public void setGroupId(long groupId) {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public void setLastMarketplaceDate(Date lastMarketplaceDate) {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }

    @Override
    public void setRatesForMarkets(Map<String, Float> ratesForMarkets) {
        throw new UnsupportedOperationException("not supported by class BidSearch");
    }


    public long getHits() {
        return _hits;
    }

    public void setHits(long hits) {
        _hits = hits;
    }

    public int getMinTerm() {
        return _minTerm;
    }

    public void setMinTerm(int minTerm) {
        _minTerm = minTerm;
    }

    public int getMaxTerm() {
        return _maxTerm;
    }

    public void setMaxTerm(int maxTerm) {
        _maxTerm = maxTerm;
    }

    public String getMinRating() {
        String result = "Z";
        Collection<BidInterest> interests = getActiveBidInterests();
        for (BidInterest bidInterest : interests) {
            Market market = new Market(bidInterest.getMarketName());
            if (market.getRatingAsLetter().compareTo(result) <= 0) {
                result = market.getRatingAsLetter();
            }
        }

        return result;
    }

    public void setMinRating(String minRating) {
        _minRating = minRating;
    }

    public String getMaxRating() {
        String result = _maxRating;
        Collection<BidInterest> interests = getActiveBidInterests();
        for (BidInterest bidInterest : interests) {
            Market market = new Market(bidInterest.getMarketName());
            if (market.getRatingAsLetter().compareTo(result) >= 0) {
                result = market.getRatingAsLetter();
            }
        }

        return result;
    }

    public void setMaxRating(String maxRating) {
        _maxRating = maxRating;
    }

    /**
     * Setter for the property 'integer employments'.
     */
    public void setEmployments(Collection<EmploymentType> employments) {
        Collection<Integer> integerEmployments = new HashSet<Integer>();
        if (employments == null) {
            setIntegerEmployments(null);
        } else {
            for (EmploymentType employment : employments) {
                integerEmployments.add((Integer) employment.getDbValue());
            }
            setIntegerEmployments(integerEmployments);
        }

        _employmentTypes = employments;
    }

    /**
     * Returns the property 'integer employments'.
     */
    public Collection<EmploymentType> getEmployments() {
        if (_employmentTypes == null && getIntegerEmployments() != null) {
            _employmentTypes = new HashSet<EmploymentType>();
            for (Integer integerEmployment : getIntegerEmployments()) {
                _employmentTypes.add(EmploymentType.enumFromDbValue(integerEmployment));
            }
        }

        return _employmentTypes;
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        if (getAccount() != null) {
            list.add(getAccount());
        }
        return list;
    }

}

