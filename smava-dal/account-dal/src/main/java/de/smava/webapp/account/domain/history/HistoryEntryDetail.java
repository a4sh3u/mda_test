package de.smava.webapp.account.domain.history;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.commons.domain.Change;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;

/**
 * Tag interface for detail information for a history entry.
 *
 * @author Daniel Doubleday
 */
public class HistoryEntryDetail extends KreditPrivatEntity {
    
    private String _key = null;
    private String _oldValue = null;
    private String _newValue = null;

    
    public HistoryEntryDetail(String key, String oldValue, String newValue) {
        super();
        this._key = key;
        this._oldValue = oldValue;
        this._newValue = newValue;
    }

    @Override
    public List<Account> getModifier() {
        return null;
    }

    public String getKey() {
        return _key;
    }

    public void setKey(String key) {
        this._key = key;
    }

    public String getNewValue() {
        return _newValue;
    }

    public void setNewValue(String newValue) {
        this._newValue = newValue;
    }

    public String getOldValue() {
        return _oldValue;
    }

    public void setOldValue(String oldValue) {
        this._oldValue = oldValue;
    }
    
    public static List<HistoryEntryDetail> buildHistoryEntryDetailsFromChangeMap(Map<String, Change> changeMap) {
        List<HistoryEntryDetail> result = new ArrayList<HistoryEntryDetail>();
        for (Map.Entry<String, Change> entry : changeMap.entrySet()) {
            result.add(new HistoryEntryDetail(entry.getKey(), entry.getValue().getOldValue(), entry.getValue().getNewValue()));
        }
        return result;
    }
}