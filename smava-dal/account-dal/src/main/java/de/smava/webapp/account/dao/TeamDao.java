package de.smava.webapp.account.dao;

import de.smava.webapp.account.domain.Team;
import de.smava.webapp.commons.dao.BaseDao;

import javax.transaction.Synchronization;
import java.util.Collection;

/**
 * DAO interface for the domain object 'Teams'.
 */
public interface TeamDao extends BaseDao<Team> {

    Collection<Team> getActiveTeamList();

}
