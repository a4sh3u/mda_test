//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head3/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head3/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(credit worthiness calculation)}

import de.smava.webapp.account.domain.CreditWorthinessCalculation;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'CreditWorthinessCalculations'.
 *
 * @author generator
 */
public interface CreditWorthinessCalculationDao extends BaseDao<CreditWorthinessCalculation>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the credit worthiness calculation identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    CreditWorthinessCalculation getCreditWorthinessCalculation(Long id);

    /**
     * Saves the credit worthiness calculation.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveCreditWorthinessCalculation(CreditWorthinessCalculation creditWorthinessCalculation);

    /**
     * Deletes an credit worthiness calculation, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the credit worthiness calculation
     */
    void deleteCreditWorthinessCalculation(Long id);

    /**
     * Retrieves all 'CreditWorthinessCalculation' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList();

    /**
     * Retrieves a page of 'CreditWorthinessCalculation' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'CreditWorthinessCalculation' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'CreditWorthinessCalculation' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<CreditWorthinessCalculation> getCreditWorthinessCalculationList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'CreditWorthinessCalculation' instances.
     */
    long getCreditWorthinessCalculationCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(credit worthiness calculation)}
    //

    Long save(CreditWorthinessCalculation message);

    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
