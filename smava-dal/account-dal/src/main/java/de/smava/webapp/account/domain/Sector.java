package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.SectorHistory;

import java.util.ArrayList;
import java.util.List;

/**
 * The domain object that represents 'Sectors'.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@sectorId")
public class Sector extends SectorHistory  implements java.io.Externalizable {

    public Sector() {
        _children = new ArrayList<Sector>();
    }

    public Sector(String name, String code, Sector parent) {
        setName(name);
        setCode(code);
        setParent(parent);
        if (parent != null) {
            parent.add(this);
        }
    }

        protected String _name;
        protected String _code;
        protected Sector _parent;
        protected List<Sector> _children;
        protected Boolean _isEmployeeSector;
        protected Boolean _isOfficialSector;
        protected Boolean _isDetailDescriptionNeeded;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'code'.
     *
     * 
     *
     */
    public void setCode(String code) {
        if (!_codeIsSet) {
            _codeIsSet = true;
            _codeInitVal = getCode();
        }
        registerChange("code", _codeInitVal, code);
        _code = code;
    }
                        
    /**
     * Returns the property 'code'.
     *
     * 
     *
     */
    public String getCode() {
        return _code;
    }
                                            
    /**
     * Setter for the property 'parent'.
     *
     * 
     *
     */
    public void setParent(Sector parent) {
        _parent = parent;
    }
            
    /**
     * Returns the property 'parent'.
     *
     * 
     *
     */
    public Sector getParent() {
        return _parent;
    }
                                            
    /**
     * Setter for the property 'children'.
     *
     * 
     *
     */
    public void setChildren(List<Sector> children) {
        _children = children;
    }
            
    /**
     * Returns the property 'children'.
     *
     * 
     *
     */
    public List<Sector> getChildren() {
        return _children;
    }
                                            
    /**
     * Setter for the property 'is employee sector'.
     *
     * 
     *
     */
    public void setIsEmployeeSector(Boolean isEmployeeSector) {
        _isEmployeeSector = isEmployeeSector;
    }
            
    /**
     * Returns the property 'is employee sector'.
     *
     * 
     *
     */
    public Boolean getIsEmployeeSector() {
        return _isEmployeeSector;
    }
                                            
    /**
     * Setter for the property 'is official sector'.
     *
     * 
     *
     */
    public void setIsOfficialSector(Boolean isOfficialSector) {
        _isOfficialSector = isOfficialSector;
    }
            
    /**
     * Returns the property 'is official sector'.
     *
     * 
     *
     */
    public Boolean getIsOfficialSector() {
        return _isOfficialSector;
    }
                                            
    /**
     * Setter for the property 'is detail description needed'.
     *
     * 
     *
     */
    public void setIsDetailDescriptionNeeded(Boolean isDetailDescriptionNeeded) {
        _isDetailDescriptionNeeded = isDetailDescriptionNeeded;
    }
            
    /**
     * Returns the property 'is detail description needed'.
     *
     * 
     *
     */
    public Boolean getIsDetailDescriptionNeeded() {
        return _isDetailDescriptionNeeded;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Sector.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _code=").append(_code);
            builder.append("\n}");
        } else {
            builder.append(Sector.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Sector instance() {
        return this;
    }
}
