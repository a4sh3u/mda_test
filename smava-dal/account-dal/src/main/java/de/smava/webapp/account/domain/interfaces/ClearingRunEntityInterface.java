package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccountClearingRun;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'ClearingRuns'.
 *
 * @author generator
 */
public interface ClearingRunEntityInterface {

    /**
     * Setter for the property 'processor'.
     *
     * 
     *
     */
    void setProcessor(Account processor);

    /**
     * Returns the property 'processor'.
     *
     * 
     *
     */
    Account getProcessor();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'bank account clearing runs'.
     *
     * 
     *
     */
    void setBankAccountClearingRuns(Collection<BankAccountClearingRun> bankAccountClearingRuns);

    /**
     * Returns the property 'bank account clearing runs'.
     *
     * 
     *
     */
    Collection<BankAccountClearingRun> getBankAccountClearingRuns();

}
