package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;

import java.util.Date;


/**
 * The domain object that represents 'Messages'.
 *
 * @author generator
 */
public interface MessageEntityInterface {

    /**
     * Setter for the property 'subject'.
     *
     * 
     *
     */
    void setSubject(String subject);

    /**
     * Returns the property 'subject'.
     *
     * 
     *
     */
    String getSubject();
    /**
     * Setter for the property 'text'.
     *
     * 
     *
     */
    void setText(String text);

    /**
     * Returns the property 'text'.
     *
     * 
     *
     */
    String getText();
    /**
     * Setter for the property 'folder'.
     *
     * 
     *
     */
    void setFolder(String folder);

    /**
     * Returns the property 'folder'.
     *
     * 
     *
     */
    String getFolder();
    /**
     * Setter for the property 'recipient email'.
     *
     * 
     *
     */
    void setRecipientEmail(String recipientEmail);

    /**
     * Returns the property 'recipient email'.
     *
     * 
     *
     */
    String getRecipientEmail();
    /**
     * Setter for the property 'read'.
     *
     * 
     *
     */
    void setRead(boolean read);

    /**
     * Returns the property 'read'.
     *
     * 
     *
     */
    boolean getRead();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'owner'.
     *
     * 
     *
     */
    void setOwner(Account owner);

    /**
     * Returns the property 'owner'.
     *
     * 
     *
     */
    Account getOwner();
    /**
     * Setter for the property 'from'.
     *
     * 
     *
     */
    void setFrom(Account from);

    /**
     * Returns the property 'from'.
     *
     * 
     *
     */
    Account getFrom();
    /**
     * Setter for the property 'recipient'.
     *
     * 
     *
     */
    void setRecipient(Account recipient);

    /**
     * Returns the property 'recipient'.
     *
     * 
     *
     */
    Account getRecipient();

}
