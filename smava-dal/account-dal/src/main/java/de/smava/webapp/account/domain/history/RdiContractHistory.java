package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractRdiContract;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'RdiContracts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RdiContractHistory extends AbstractRdiContract {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient Date _fromDateInitVal;
    protected transient boolean _fromDateIsSet;
    protected transient Date _toDateInitVal;
    protected transient boolean _toDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _tariffInitVal;
    protected transient boolean _tariffIsSet;
    protected transient Date _termsAndConditionsAcceptanceDateInitVal;
    protected transient boolean _termsAndConditionsAcceptanceDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'from date'.
     */
    public Date fromDateInitVal() {
        Date result;
        if (_fromDateIsSet) {
            result = _fromDateInitVal;
        } else {
            result = getFromDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'from date'.
     */
    public boolean fromDateIsDirty() {
        return !valuesAreEqual(fromDateInitVal(), getFromDate());
    }

    /**
     * Returns true if the setter method was called for the property 'from date'.
     */
    public boolean fromDateIsSet() {
        return _fromDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'to date'.
     */
    public Date toDateInitVal() {
        Date result;
        if (_toDateIsSet) {
            result = _toDateInitVal;
        } else {
            result = getToDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'to date'.
     */
    public boolean toDateIsDirty() {
        return !valuesAreEqual(toDateInitVal(), getToDate());
    }

    /**
     * Returns true if the setter method was called for the property 'to date'.
     */
    public boolean toDateIsSet() {
        return _toDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'tariff'.
     */
    public String tariffInitVal() {
        String result;
        if (_tariffIsSet) {
            result = _tariffInitVal;
        } else {
            result = getTariff();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'tariff'.
     */
    public boolean tariffIsDirty() {
        return !valuesAreEqual(tariffInitVal(), getTariff());
    }

    /**
     * Returns true if the setter method was called for the property 'tariff'.
     */
    public boolean tariffIsSet() {
        return _tariffIsSet;
    }
	
    /**
     * Returns the initial value of the property 'terms and conditions acceptance date'.
     */
    public Date termsAndConditionsAcceptanceDateInitVal() {
        Date result;
        if (_termsAndConditionsAcceptanceDateIsSet) {
            result = _termsAndConditionsAcceptanceDateInitVal;
        } else {
            result = getTermsAndConditionsAcceptanceDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'terms and conditions acceptance date'.
     */
    public boolean termsAndConditionsAcceptanceDateIsDirty() {
        return !valuesAreEqual(termsAndConditionsAcceptanceDateInitVal(), getTermsAndConditionsAcceptanceDate());
    }

    /**
     * Returns true if the setter method was called for the property 'terms and conditions acceptance date'.
     */
    public boolean termsAndConditionsAcceptanceDateIsSet() {
        return _termsAndConditionsAcceptanceDateIsSet;
    }
		
}
