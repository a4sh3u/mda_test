//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(encashment rate)}
import de.smava.webapp.account.domain.history.EncashmentRateHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'EncashmentRates'.
 *
 * @author generator
 */
public class EncashmentRate extends EncashmentRateHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(encashment rate)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _marketName;
        protected Double _rate;
        protected Float _borrowerPercentalCharge;
        protected Float _borrowerFixCharge;
        protected Float _lenderFixCharge;
        protected Float _lenderPercentalCharge;
        
                            /**
     * Setter for the property 'market name'.
     */
    public void setMarketName(String marketName) {
        if (!_marketNameIsSet) {
            _marketNameIsSet = true;
            _marketNameInitVal = getMarketName();
        }
        registerChange("market name", _marketNameInitVal, marketName);
        _marketName = marketName;
    }
                        
    /**
     * Returns the property 'market name'.
     */
    public String getMarketName() {
        return _marketName;
    }
                                            
    /**
     * Setter for the property 'rate'.
     */
    public void setRate(Double rate) {
        _rate = rate;
    }
            
    /**
     * Returns the property 'rate'.
     */
    public Double getRate() {
        return _rate;
    }
                                            
    /**
     * Setter for the property 'borrower percental charge'.
     */
    public void setBorrowerPercentalCharge(Float borrowerPercentalCharge) {
        _borrowerPercentalCharge = borrowerPercentalCharge;
    }
            
    /**
     * Returns the property 'borrower percental charge'.
     */
    public Float getBorrowerPercentalCharge() {
        return _borrowerPercentalCharge;
    }
                                            
    /**
     * Setter for the property 'borrower fix charge'.
     */
    public void setBorrowerFixCharge(Float borrowerFixCharge) {
        _borrowerFixCharge = borrowerFixCharge;
    }
            
    /**
     * Returns the property 'borrower fix charge'.
     */
    public Float getBorrowerFixCharge() {
        return _borrowerFixCharge;
    }
                                            
    /**
     * Setter for the property 'lender fix charge'.
     */
    public void setLenderFixCharge(Float lenderFixCharge) {
        _lenderFixCharge = lenderFixCharge;
    }
            
    /**
     * Returns the property 'lender fix charge'.
     */
    public Float getLenderFixCharge() {
        return _lenderFixCharge;
    }
                                            
    /**
     * Setter for the property 'lender percental charge'.
     */
    public void setLenderPercentalCharge(Float lenderPercentalCharge) {
        _lenderPercentalCharge = lenderPercentalCharge;
    }
            
    /**
     * Returns the property 'lender percental charge'.
     */
    public Float getLenderPercentalCharge() {
        return _lenderPercentalCharge;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(EncashmentRate.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _marketName=").append(_marketName);
            builder.append("\n}");
        } else {
            builder.append(EncashmentRate.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public EncashmentRate asEncashmentRate() {
        return this;
    }
}
