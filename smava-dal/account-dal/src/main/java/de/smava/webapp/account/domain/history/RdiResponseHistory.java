package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractRdiResponse;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'RdiResponses'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RdiResponseHistory extends AbstractRdiResponse {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _errorMessageInitVal;
    protected transient boolean _errorMessageIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'error message'.
     */
    public String errorMessageInitVal() {
        String result;
        if (_errorMessageIsSet) {
            result = _errorMessageInitVal;
        } else {
            result = getErrorMessage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'error message'.
     */
    public boolean errorMessageIsDirty() {
        return !valuesAreEqual(errorMessageInitVal(), getErrorMessage());
    }

    /**
     * Returns true if the setter method was called for the property 'error message'.
     */
    public boolean errorMessageIsSet() {
        return _errorMessageIsSet;
    }
		
}
