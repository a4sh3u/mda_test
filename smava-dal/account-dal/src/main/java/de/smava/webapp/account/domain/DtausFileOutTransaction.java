package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DtausFileOutTransactionHistory;

/**
 * The domain object that represents 'DtausFileOutTransactions'.
 */
public class DtausFileOutTransaction extends DtausFileOutTransactionHistory  {

            protected Transaction _transaction;
            protected DtausFile _dtausFile;
            protected DtausFileOutTransactionType _type;
        
                        /**
            * Setter for the property 'transaction'.
            */
            public void setTransaction(Transaction transaction) {
            _transaction = transaction;
            }

            /**
            * Returns the property 'transaction'.
            */
            public Transaction getTransaction() {
            return _transaction;
            }
                                /**
            * Setter for the property 'dtaus file'.
            */
            public void setDtausFile(DtausFile dtausFile) {
            _dtausFile = dtausFile;
            }

            /**
            * Returns the property 'dtaus file'.
            */
            public DtausFile getDtausFile() {
            return _dtausFile;
            }
                                /**
            * Setter for the property 'type'.
            */
            public void setType(DtausFileOutTransactionType type) {
            _type = type;
            }

            /**
            * Returns the property 'type'.
            */
            public DtausFileOutTransactionType getType() {
            return _type;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(DtausFileOutTransaction.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(DtausFileOutTransaction.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

}
