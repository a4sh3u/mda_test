package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractRating;

import java.util.Map;




/**
 * The domain object that has all history aggregation related fields for 'Ratings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RatingHistory extends AbstractRating {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient double _failureRateInitVal;
    protected transient boolean _failureRateIsSet;
    protected transient double _groupBonusRateInitVal;
    protected transient boolean _groupBonusRateIsSet;
    protected transient Map<Integer, Double> _creditDiscountsInitVal;
    protected transient boolean _creditDiscountsIsSet;
    protected transient Map<Integer, Double> _interestReductionsInitVal;
    protected transient boolean _interestReductionsIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'failure rate'.
     */
    public double failureRateInitVal() {
        double result;
        if (_failureRateIsSet) {
            result = _failureRateInitVal;
        } else {
            result = getFailureRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'failure rate'.
     */
    public boolean failureRateIsDirty() {
        return !valuesAreEqual(failureRateInitVal(), getFailureRate());
    }

    /**
     * Returns true if the setter method was called for the property 'failure rate'.
     */
    public boolean failureRateIsSet() {
        return _failureRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'group bonus rate'.
     */
    public double groupBonusRateInitVal() {
        double result;
        if (_groupBonusRateIsSet) {
            result = _groupBonusRateInitVal;
        } else {
            result = getGroupBonusRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'group bonus rate'.
     */
    public boolean groupBonusRateIsDirty() {
        return !valuesAreEqual(groupBonusRateInitVal(), getGroupBonusRate());
    }

    /**
     * Returns true if the setter method was called for the property 'group bonus rate'.
     */
    public boolean groupBonusRateIsSet() {
        return _groupBonusRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit discounts'.
     */
    public Map<Integer, Double> creditDiscountsInitVal() {
        Map<Integer, Double> result;
        if (_creditDiscountsIsSet) {
            result = _creditDiscountsInitVal;
        } else {
            result = getCreditDiscounts();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit discounts'.
     */
    public boolean creditDiscountsIsDirty() {
        return !valuesAreEqual(creditDiscountsInitVal(), getCreditDiscounts());
    }

    /**
     * Returns true if the setter method was called for the property 'credit discounts'.
     */
    public boolean creditDiscountsIsSet() {
        return _creditDiscountsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'interest reductions'.
     */
    public Map<Integer, Double> interestReductionsInitVal() {
        Map<Integer, Double> result;
        if (_interestReductionsIsSet) {
            result = _interestReductionsInitVal;
        } else {
            result = getInterestReductions();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'interest reductions'.
     */
    public boolean interestReductionsIsDirty() {
        return !valuesAreEqual(interestReductionsInitVal(), getInterestReductions());
    }

    /**
     * Returns true if the setter method was called for the property 'interest reductions'.
     */
    public boolean interestReductionsIsSet() {
        return _interestReductionsIsSet;
    }

}
