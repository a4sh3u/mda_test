//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainEntityInterface.tpl
//
//
//
//
package de.smava.webapp.backoffice.domain.interfaces;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dunning process view data)}
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.backoffice.domain.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;

import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * The domain object that represents 'DunningProcessViewDatas'.
 *
 * @author generator
 */
public interface DunningProcessViewDataEntityInterface {

    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    public void setOrder(de.smava.webapp.account.domain.Order order);

    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Order getOrder();
    /**
     * Setter for the property 'changed by'.
     *
     * 
     *
     */
    public void setChangedBy(de.smava.webapp.account.domain.Account changedBy);

    /**
     * Returns the property 'changed by'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getChangedBy();
    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    public void setComment(String comment);

    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    public String getComment();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    public DunningProcessViewData asDunningProcessViewData();
}
