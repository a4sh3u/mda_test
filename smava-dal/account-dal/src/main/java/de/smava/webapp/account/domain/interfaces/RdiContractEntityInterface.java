package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.domain.RdiPayment;
import de.smava.webapp.account.domain.RdiRequest;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'RdiContracts'.
 *
 * @author generator
 */
public interface RdiContractEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'contract number'.
     *
     * 
     *
     */
    void setContractNumber(Long contractNumber);

    /**
     * Returns the property 'contract number'.
     *
     * 
     *
     */
    Long getContractNumber();
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    void setOrder(Order order);

    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    Order getOrder();
    /**
     * Setter for the property 'from date'.
     *
     * 
     *
     */
    void setFromDate(Date fromDate);

    /**
     * Returns the property 'from date'.
     *
     * 
     *
     */
    Date getFromDate();
    /**
     * Setter for the property 'to date'.
     *
     * 
     *
     */
    void setToDate(Date toDate);

    /**
     * Returns the property 'to date'.
     *
     * 
     *
     */
    Date getToDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'tariff'.
     *
     * 
     *
     */
    void setTariff(String tariff);

    /**
     * Returns the property 'tariff'.
     *
     * 
     *
     */
    String getTariff();
    /**
     * Setter for the property 'terms and conditions acceptance date'.
     *
     * 
     *
     */
    void setTermsAndConditionsAcceptanceDate(Date termsAndConditionsAcceptanceDate);

    /**
     * Returns the property 'terms and conditions acceptance date'.
     *
     * 
     *
     */
    Date getTermsAndConditionsAcceptanceDate();
    /**
     * Setter for the property 'requests'.
     *
     * 
     *
     */
    void setRequests(Collection<RdiRequest> requests);

    /**
     * Returns the property 'requests'.
     *
     * 
     *
     */
    Collection<RdiRequest> getRequests();
    /**
     * Setter for the property 'payments'.
     *
     * 
     *
     */
    void setPayments(Collection<RdiPayment> payments);

    /**
     * Returns the property 'payments'.
     *
     * 
     *
     */
    Collection<RdiPayment> getPayments();

}
