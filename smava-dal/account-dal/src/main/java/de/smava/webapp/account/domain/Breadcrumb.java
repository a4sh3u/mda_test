/**
 * 
 */
package de.smava.webapp.account.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author dkeller
 *
 */
public class Breadcrumb implements Serializable{

	Map<String, String> _titles;

	public Map<String, String> getTitles() {
		return _titles;
	}

	public void setTitles(Map<String, String> titles) {
		_titles = titles;
	}
}
