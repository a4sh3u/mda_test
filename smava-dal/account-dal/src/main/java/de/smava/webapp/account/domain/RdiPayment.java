package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.RdiPaymentHistory;

import java.util.Date;

/**
 * The domain object that represents 'RdiPayments'.
 */
public class RdiPayment extends RdiPaymentHistory  {

        protected String _state;
        protected String _type;
        protected RdiContract _contract;
        protected Date _creationDate;
        protected Date _expirationDate;
        protected Date _fromDate;
        protected Date _toDate;
        
                            /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'contract'.
     */
    public void setContract(RdiContract contract) {
        _contract = contract;
    }
            
    /**
     * Returns the property 'contract'.
     */
    public RdiContract getContract() {
        return _contract;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'from date'.
     */
    public void setFromDate(Date fromDate) {
        if (!_fromDateIsSet) {
            _fromDateIsSet = true;
            _fromDateInitVal = getFromDate();
        }
        registerChange("from date", _fromDateInitVal, fromDate);
        _fromDate = fromDate;
    }
                        
    /**
     * Returns the property 'from date'.
     */
    public Date getFromDate() {
        return _fromDate;
    }
                                    /**
     * Setter for the property 'to date'.
     */
    public void setToDate(Date toDate) {
        if (!_toDateIsSet) {
            _toDateIsSet = true;
            _toDateInitVal = getToDate();
        }
        registerChange("to date", _toDateInitVal, toDate);
        _toDate = toDate;
    }
                        
    /**
     * Returns the property 'to date'.
     */
    public Date getToDate() {
        return _toDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RdiPayment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(RdiPayment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
