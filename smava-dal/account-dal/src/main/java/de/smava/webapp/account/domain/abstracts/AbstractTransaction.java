package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.TransactionEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'Transactions'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractTransaction
    extends KreditPrivatEntity implements TransactionEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractTransaction.class);

    private static final long serialVersionUID = 6366779699108438129L;

    public static final String STATE_OPEN = "OPEN";
    public static final String STATE_SUCCESSFUL = "SUCCESSFUL";
    public static final String STATE_FAILED = "FAILED";
    public static final String STATE_VOID = "VOID";
    public static final String STATE_DELETED = "DELETED";
    public static final String STATE_DRAFT = "DRAFT";
    public static final String STATE_RESERVED = "RESERVED";
    public static final Set STATES = ConstCollector.getAll("STATE_");

    public static final Set<String> OPEN_SUCCESSFUL_STATE = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(STATE_OPEN, STATE_SUCCESSFUL)));

    //    1)	Überweisung
    public static final String TYPE_TRANSFER = "TRANSFER";
    //    2)	Lastschrifteinzug
    public static final String TYPE_DIRECT_DEBIT = "DIRECT_DEBIT";
    //    3)	Rücklastschrift (RLS)
    public static final String TYPE_RETURN_DEBIT = "RETURN_DEBIT";
    //    4)	Rücküberweisung
    public static final String TYPE_RETURN_TRANSFER = "RETURN_TRANSFER";

    public static final Set TYPES = ConstCollector.getAll("TYPE_");
    //  This is a pseudo type existing only during processing and aggregating reference to all not DIRECT_DEBIT types
    public static final String NOT_DIRECT_DEBIT = "NOT_DIRECT_DEBIT";

    public static final String MAPPING_TYPE_IBAN = "IBAN";
    public static final String MAPPING_TYPE_ACCOUNT_NUMBER = "ACCOUNT_NUMBER";

    public static final Set MAPPING_TYPES = ConstCollector.getAll("MAPPING_TYPE_");

    private final Collection<String> _externalTypes = new ArrayList<String>();
    {
        _externalTypes.add(BankAccount.TYPE_REFRENCE_ACCOUNT);
        _externalTypes.add(BankAccount.TYPE_OTHER_ACCOUNT);
        _externalTypes.add(BankAccount.TYPE_ENCASHMENT_ACCOUNT);
        _externalTypes.add(BankAccount.TYPE_RDI_PREMIUM_ACCOUNT);
        _externalTypes.add(BankAccount.TYPE_RDI_AMORTISATION_ACCOUNT);
    }

    public boolean isTransfer() {
        return TYPE_TRANSFER.equals(getType());
    }

    public boolean isReturnDebit() {
        return TYPE_RETURN_DEBIT.equals(getType());
    }

    public boolean isReturnTransfer() {
        return TYPE_RETURN_TRANSFER.equals(getType());
    }

    public boolean isDirectDebit() {
        return TYPE_DIRECT_DEBIT.equals(getType());
    }

    public boolean isOpen() {
        return STATE_OPEN.equals(getState());
    }

    public boolean isSuccessful() {
        return STATE_SUCCESSFUL.equals(getState());
    }

    public boolean isDeleted() {
        return STATE_DELETED.equals(getState());
    }

    public boolean isDraft() {
        return STATE_DRAFT.equals(getState());
    }

    public boolean isVoid() {
        return STATE_VOID.equals(getState());
    }

    public boolean isFailed() {
        return STATE_FAILED.equals(getState());
    }

    public boolean isReserved() {
        return STATE_RESERVED.equals(getState());
    }

    public double getAmount(final BookingType type) {
        double amount = 0.0d;
        for (Booking booking : getBookings()) {
            if (type == null || booking.getType().equals(type)) {
                amount += booking.getAmount();
            }
        }
        return Booking.cutAmount(amount);
    }

    public double getAmount() {
        return getAmount(null);
    }

    /**
     * Get all entries connected with realtion IN CONFIRMED or IN RELATED.
     */
    public List<DtausEntry> getInRelatedAndConfirmedEntries() {
        List<DtausEntry> list = DtausEntryTransaction.getRelatedDtausEntries(this.instance(), DtausEntryTransaction.TYPE_IN_CONFIRMED);
        list.addAll(DtausEntryTransaction.getRelatedDtausEntries(this.instance(), DtausEntryTransaction.TYPE_IN_RELATED));
        return list;
    }

    /**
     * Get all entries connected with realtion IN RELATED.
     */
    public List<DtausEntry> getInRelatedEntries() {
        return DtausEntryTransaction.getRelatedDtausEntries(this.instance(), DtausEntryTransaction.TYPE_IN_RELATED);
    }

    /**
     * Get all entries connected with realtion IN CONFIRMED.
     */
    public List<DtausEntry> getInConfirmedEntries() {
        return DtausEntryTransaction.getRelatedDtausEntries(this.instance(), DtausEntryTransaction.TYPE_IN_CONFIRMED);
    }

    /**
     * Get all entries connected with realtion IN FOLLOW.
     */
    public List<DtausEntry> getInFollowEntries() {
        return DtausEntryTransaction.getRelatedDtausEntries(this.instance(), DtausEntryTransaction.TYPE_IN_FOLLOW);
    }

    public void addReminderState(ReminderState reminderState) {
        if (getReminderStates() == null) {
            setReminderStates(new LinkedList<ReminderState>());
        }

        if (!this.equals(reminderState.getTransaction())) {
            reminderState.setTransaction(this.instance());
        }

        getReminderStates().add(reminderState);
    }

    public ReminderState getCurrentReminderState() {
        ReminderState result = null;
        if (getReminderStates() != null) {
            for (ReminderState reminderState : getReminderStates()) {
                if (result == null) {
                    result = reminderState;
                } else {
                    if (reminderState.getCreationDate().after(result.getCreationDate())) {
                        result = reminderState;
                    }
                }
            }
        }

        return result;
    }

    public void addBooking(Booking newBooking) {
        if (getBookings() == null) {
            setBookings(new ArrayList<Booking>());
        }
        getBookings().add(newBooking);
        newBooking.setTransaction(this.instance());
    }

    public void addAllBookings(final Collection<Booking> bookingsToAdd) {
        for (Booking booking : bookingsToAdd) {
            booking.setTransaction(this.instance());
        }
        if (getBookings() == null) {
            setBookings(new ArrayList<Booking>());
        }
        getBookings().addAll(bookingsToAdd);
    }

    public Set<BookingType> getAllContainedBookingTypes() {
        final Set<BookingType> result = new LinkedHashSet<BookingType>();
        final Collection<Booking> bookings = getBookings();
        if (bookings != null) {
            for (Booking booking : bookings) {
                result.add(booking.getType());
            }
        }
        return result;
    }

    public Collection<String> getExternalTypes() {
        return _externalTypes;
    }

    public boolean isInternal() {
        boolean result = true;
        if (getExternalTypes().contains(getDebitAccount().getType()) || getExternalTypes().contains(getCreditAccount().getType())) {
            result = false;
        }

        return result;
    }

    public BankAccountProvider getOwningProvider(){
        if (getType().equals(TYPE_DIRECT_DEBIT) ||
                getType().equals(TYPE_RETURN_DEBIT)){
            BankAccountProvider owner = getCreditAccount().getProvider();
            return null == owner?getDebitAccount().getProvider():
                    owner;
        } else if (getType().equals(TYPE_TRANSFER) ||
                getType().equals(TYPE_RETURN_TRANSFER)){
            BankAccountProvider owner = getDebitAccount().getProvider();
            return null == owner?getCreditAccount().getProvider():owner;
        } else {
            return null;
        }
    }

    public abstract Transaction instance();
}

