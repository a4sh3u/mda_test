package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.LeadTrackingHistory;

import java.util.Date;

/**
 * The domain object that represents 'LeadTrackings'.
 */
public class LeadTracking extends LeadTrackingHistory  {

        protected Date _trackingDate;
        protected TrackingType _trackingType;
        protected Account _account;
        
                            /**
     * Setter for the property 'tracking date'.
     */
    public void setTrackingDate(Date trackingDate) {
        if (!_trackingDateIsSet) {
            _trackingDateIsSet = true;
            _trackingDateInitVal = getTrackingDate();
        }
        registerChange("tracking date", _trackingDateInitVal, trackingDate);
        _trackingDate = trackingDate;
    }
                        
    /**
     * Returns the property 'tracking date'.
     */
    public Date getTrackingDate() {
        return _trackingDate;
    }
                                            
    /**
     * Setter for the property 'tracking type'.
     */
    public void setTrackingType(TrackingType trackingType) {
        _trackingType = trackingType;
    }
            
    /**
     * Returns the property 'tracking type'.
     */
    public TrackingType getTrackingType() {
        return _trackingType;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LeadTracking.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(LeadTracking.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
