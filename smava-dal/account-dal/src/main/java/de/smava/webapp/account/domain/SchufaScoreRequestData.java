package de.smava.webapp.account.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.account.domain.history.SchufaScoreRequestDataHistory;

import java.util.Date;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@schufaScoreRequestDataId")

/**
 * The domain object that represents 'SchufaScoreRequestDatas'.
 */
public class SchufaScoreRequestData extends SchufaScoreRequestDataHistory  {

        protected String _firstName;
        protected String _lastName;
        protected String _gender;
        protected Date _birthDate;
        protected List<SchufaScoreAddress> _addresses;

                            /**
     * Setter for the property 'first name'.
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("first name", _firstNameInitVal, firstName);
        _firstName = firstName;
    }

    /**
     * Returns the property 'first name'.
     */
    public String getFirstName() {
        return _firstName;
    }
                                    /**
     * Setter for the property 'last name'.
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("last name", _lastNameInitVal, lastName);
        _lastName = lastName;
    }

    /**
     * Returns the property 'last name'.
     */
    public String getLastName() {
        return _lastName;
    }
                                    /**
     * Setter for the property 'gender'.
     */
    public void setGender(String gender) {
        if (!_genderIsSet) {
            _genderIsSet = true;
            _genderInitVal = getGender();
        }
        registerChange("gender", _genderInitVal, gender);
        _gender = gender;
    }

    /**
     * Returns the property 'gender'.
     */
    public String getGender() {
        return _gender;
    }
                                    /**
     * Setter for the property 'birth date'.
     */
    public void setBirthDate(Date birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;
            _birthDateInitVal = getBirthDate();
        }
        registerChange("birth date", _birthDateInitVal, birthDate);
        _birthDate = birthDate;
    }

    /**
     * Returns the property 'birth date'.
     */
    public Date getBirthDate() {
        return _birthDate;
    }

    /**
     * Setter for the property 'addresses'.
     */
    public void setAddresses(List<SchufaScoreAddress> addresses) {
        _addresses = addresses;
    }
            
    /**
     * Returns the property 'addresses'.
     */
    public List<SchufaScoreAddress> getAddresses() {
        return _addresses;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaScoreRequestData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _gender=").append(_gender);
            builder.append("\n}");
        } else {
            builder.append(SchufaScoreRequestData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public SchufaScoreRequestData instance() {
        return this;
    }
}
