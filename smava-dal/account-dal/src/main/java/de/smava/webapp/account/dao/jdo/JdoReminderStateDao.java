//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(reminder state)}

import de.smava.webapp.account.dao.ReminderStateDao;
import de.smava.webapp.account.domain.ReminderState;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'ReminderStates'.
 *
 * @author generator
 */
@Repository(value = "reminderStateDao")
public class JdoReminderStateDao extends JdoBaseDao implements ReminderStateDao {

    private static final Logger LOGGER = Logger.getLogger(JdoReminderStateDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(reminder state)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the reminder state identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public ReminderState load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderState - start: id=" + id);
        }
        ReminderState result = getEntity(ReminderState.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderState - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public ReminderState getReminderState(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	ReminderState entity = findUniqueEntity(ReminderState.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the reminder state.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(ReminderState reminderState) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveReminderState: " + reminderState);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(reminder state)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(reminderState);
    }

    /**
     * @deprecated Use {@link #save(ReminderState) instead}
     */
    public Long saveReminderState(ReminderState reminderState) {
        return save(reminderState);
    }

    /**
     * Deletes an reminder state, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the reminder state
     */
    public void deleteReminderState(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteReminderState: " + id);
        }
        deleteEntity(ReminderState.class, id);
    }

    /**
     * Retrieves all 'ReminderState' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<ReminderState> getReminderStateList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - start");
        }
        Collection<ReminderState> result = getEntities(ReminderState.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'ReminderState' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<ReminderState> getReminderStateList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - start: pageable=" + pageable);
        }
        Collection<ReminderState> result = getEntities(ReminderState.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ReminderState' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<ReminderState> getReminderStateList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - start: sortable=" + sortable);
        }
        Collection<ReminderState> result = getEntities(ReminderState.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ReminderState' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<ReminderState> getReminderStateList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<ReminderState> result = getEntities(ReminderState.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ReminderState' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ReminderState> findReminderStateList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - start: whereClause=" + whereClause);
        }
        Collection<ReminderState> result = findEntities(ReminderState.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ReminderState' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<ReminderState> findReminderStateList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<ReminderState> result = findEntities(ReminderState.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'ReminderState' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ReminderState> findReminderStateList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<ReminderState> result = findEntities(ReminderState.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ReminderState' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ReminderState> findReminderStateList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<ReminderState> result = findEntities(ReminderState.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ReminderState' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ReminderState> findReminderStateList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<ReminderState> result = findEntities(ReminderState.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ReminderState' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ReminderState> findReminderStateList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<ReminderState> result = findEntities(ReminderState.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findReminderStateList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'ReminderState' instances.
     */
    public long getReminderStateCount() {
        long result = getEntityCount(ReminderState.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ReminderState' instances which match the given whereClause.
     */
    public long getReminderStateCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(ReminderState.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ReminderState' instances which match the given whereClause together with params specified in object array.
     */
    public long getReminderStateCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(ReminderState.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getReminderStateCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(reminder state)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
