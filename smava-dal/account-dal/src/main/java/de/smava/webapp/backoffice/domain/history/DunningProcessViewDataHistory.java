//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainHistoryClass.tpl
//
//
//
//
package de.smava.webapp.backoffice.domain.history;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dunning process view data)}

import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.backoffice.domain.*;
import de.smava.webapp.backoffice.domain.abstracts.AbstractDunningProcessViewData;

                
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that has all history aggregation related fields for 'DunningProcessViewDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DunningProcessViewDataHistory extends AbstractDunningProcessViewData {

    protected transient String _commentInitVal;
    protected transient boolean _commentIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


			
    /**
     * Returns the initial value of the property 'comment'.
     */
    public String commentInitVal() {
        String result;
        if (_commentIsSet) {
            result = _commentInitVal;
        } else {
            result = getComment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comment'.
     */
    public boolean commentIsDirty() {
        return !valuesAreEqual(commentInitVal(), getComment());
    }

    /**
     * Returns true if the setter method was called for the property 'comment'.
     */
    public boolean commentIsSet() {
        return _commentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

}
