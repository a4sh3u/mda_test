package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.CredentialsEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

/**
 * The domain object that represents 'Credentialss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractCredentials
    extends KreditPrivatEntity implements CredentialsEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCredentials.class);

    private static final Md5PasswordEncoder MD5_ENCODER = new Md5PasswordEncoder();

    public static boolean checkMd5EncodedTPin(String encodedString, String plainString, SaltSource saltSource) {
        return MD5_ENCODER.isPasswordValid(encodedString, plainString, saltSource.getSalt(null));
    }

    public static String encodeTPin(String password, SaltSource saltSource) {
        return MD5_ENCODER.encodePassword(password, saltSource.getSalt(null));
    }

}

