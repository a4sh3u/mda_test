package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'IssueAuthoritys'.
 *
 * @author generator
 */
public interface IssueAuthorityEntityInterface {

    /**
     * Setter for the property 'bkz'.
     *
     * 
     *
     */
    void setBkz(String bkz);

    /**
     * Returns the property 'bkz'.
     *
     * 
     *
     */
    String getBkz();
    /**
     * Setter for the property 'zip'.
     *
     * 
     *
     */
    void setZip(String zip);

    /**
     * Returns the property 'zip'.
     *
     * 
     *
     */
    String getZip();
    /**
     * Setter for the property 'city'.
     *
     * 
     *
     */
    void setCity(String city);

    /**
     * Returns the property 'city'.
     *
     * 
     *
     */
    String getCity();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();

}
