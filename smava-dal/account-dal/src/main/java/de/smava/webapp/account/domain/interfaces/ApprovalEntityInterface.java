package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.AccountRole;
import de.smava.webapp.account.domain.RepaymentBreak;

import java.util.Date;


/**
 * The domain object that represents 'Approvals'.
 *
 * @author generator
 */
public interface ApprovalEntityInterface {

    /**
     * Setter for the property 'account role'.
     *
     * 
     *
     */
    void setAccountRole(AccountRole accountRole);

    /**
     * Returns the property 'account role'.
     *
     * 
     *
     */
    AccountRole getAccountRole();
    /**
     * Setter for the property 'approver'.
     *
     * 
     *
     */
    void setApprover(Account approver);

    /**
     * Returns the property 'approver'.
     *
     * 
     *
     */
    Account getApprover();
    /**
     * Setter for the property 'approval date'.
     *
     * 
     *
     */
    void setApprovalDate(Date approvalDate);

    /**
     * Returns the property 'approval date'.
     *
     * 
     *
     */
    Date getApprovalDate();
    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    void setComment(String comment);

    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    String getComment();
    /**
     * Setter for the property 'vote'.
     *
     * 
     *
     */
    void setVote(int vote);

    /**
     * Returns the property 'vote'.
     *
     * 
     *
     */
    int getVote();
    /**
     * Setter for the property 'repayment break'.
     *
     * 
     *
     */
    void setRepaymentBreak(RepaymentBreak repaymentBreak);

    /**
     * Returns the property 'repayment break'.
     *
     * 
     *
     */
    RepaymentBreak getRepaymentBreak();

}
