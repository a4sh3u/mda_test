/**
 * 
 */
package de.smava.webapp.account.domain;

/**
 * @author dkeller
 *
 */
public class SchufaTokenSummation {

	private double _personalLoan = 0.0;
	private double _mortgage = 0.0;
	private double _privateLeasing = 0.0;
	private double _businessLoan = 0.0;
	private double _businessLeasing =0.0;
	
	public SchufaTokenSummation( double personalLoan,
								 double privateLeasing,
								 double mortgage,
								 double businessLoan,
								 double businessLeasing){
		
		this._personalLoan = personalLoan;
		this._privateLeasing = privateLeasing;
		this._mortgage = mortgage;
		this._businessLoan = businessLoan;
		this._businessLeasing = businessLeasing;
		
	}
	
	public double getPersonalLoan() {
		return _personalLoan;
	}
	public void setPersonalLoan(double personalLoan) {
		this._personalLoan = personalLoan;
	}
	public double getMortgage() {
		return _mortgage;
	}
	public void setMortgage(double mortgage) {
		this._mortgage = mortgage;
	}
	public double getPrivateLeasing() {
		return _privateLeasing;
	}
	public void setPrivateLeasing(double privateLeasing) {
		this._privateLeasing = privateLeasing;
	}

	public double getBusinessLeasing() {
		return _businessLeasing;
	}

	public void setBusinessLeasing(double businessLeasing) {
		this._businessLeasing = businessLeasing;
	}
	
	public double getBusinessLoan() {
		return _businessLoan;
	}
	public void setBusinessLoan(double businessLoan) {
		this._businessLoan = businessLoan;
	}
	
	
}
