//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(reminder state)}

import de.smava.webapp.account.domain.ReminderState;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'ReminderStates'.
 *
 * @author generator
 */
public interface ReminderStateDao extends BaseDao<ReminderState>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the reminder state identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    ReminderState getReminderState(Long id);

    /**
     * Saves the reminder state.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveReminderState(ReminderState reminderState);

    /**
     * Deletes an reminder state, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the reminder state
     */
    void deleteReminderState(Long id);

    /**
     * Retrieves all 'ReminderState' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<ReminderState> getReminderStateList();

    /**
     * Retrieves a page of 'ReminderState' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<ReminderState> getReminderStateList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'ReminderState' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<ReminderState> getReminderStateList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'ReminderState' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<ReminderState> getReminderStateList(Pageable pageable, Sortable sortable);





    /**
     * Returns the number of 'ReminderState' instances.
     */
    long getReminderStateCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(reminder state)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
