package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CreditAdvisorAvailabilityHistory;

/**
 * The domain object that represents 'CreditAdvisorAvailabilitys'.
 */
public class CreditAdvisorAvailability extends CreditAdvisorAvailabilityHistory  {

        protected Account _account;
        protected double _availability;

    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'availability'.
     */
    public void setAvailability(double availability) {
        if (!_availabilityIsSet) {
            _availabilityIsSet = true;
            _availabilityInitVal = getAvailability();
        }
        registerChange("availability", _availabilityInitVal, availability);
        _availability = availability;
    }
                        
    /**
     * Returns the property 'availability'.
     */
    public double getAvailability() {
        return _availability;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CreditAdvisorAvailability.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _availability=").append(_availability);
            builder.append("\n}");
        } else {
            builder.append(CreditAdvisorAvailability.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
