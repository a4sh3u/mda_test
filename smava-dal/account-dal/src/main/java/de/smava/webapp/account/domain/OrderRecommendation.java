package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.OrderRecommendationHistory;

import java.util.Date;

/**
 * The domain object that represents 'OrderRecommendations'.
 *
 * @author generator
 */
public class OrderRecommendation extends OrderRecommendationHistory  {

        protected String _text;
        protected Account _author;
        protected Order _order;
        protected Date _creationDate;
        protected String _state;

                            /**
     * Setter for the property 'text'.
     */
    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = getText();
        }
        registerChange("text", _textInitVal, text);
        _text = text;
    }
                        
    /**
     * Returns the property 'text'.
     */
    public String getText() {
        return _text;
    }
                                            
    /**
     * Setter for the property 'author'.
     */
    public void setAuthor(Account author) {
        _author = author;
    }
            
    /**
     * Returns the property 'author'.
     */
    public Account getAuthor() {
        return _author;
    }
                                            
    /**
     * Setter for the property 'order'.
     */
    public void setOrder(Order order) {
        _order = order;
    }
            
    /**
     * Returns the property 'order'.
     */
    public Order getOrder() {
        return _order;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(OrderRecommendation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _text=").append(_text);
            builder.append("\n    _state=").append(_state);
            builder.append("\n}");
        } else {
            builder.append(OrderRecommendation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
