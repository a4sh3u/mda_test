package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.interfaces.SchufaScoreTokenEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'SchufaScoreTokens'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractSchufaScoreToken
    extends KreditPrivatEntity implements SchufaScoreTokenEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaScoreToken.class);

    private static final long serialVersionUID = -5729774169129170254L;

    /** Kredit. */
    public static final String CODE_KR = "KR";
    /** Leasing. */
    public static final String CODE_ML = "ML";
    /** B�rgschaft. */
    public static final String CODE_BU = "BU";
    /** Mitantragssteller. */
    public static final String CODE_MA = "MA";
    /** Hypo-Kredit. */
    public static final String CODE_HP = "HP";

    public static final Collection<String> RATE_CODES = Collections.unmodifiableCollection(Arrays.asList(new String[]{CODE_KR, CODE_ML, CODE_BU, CODE_MA, CODE_HP}));

    /** Kreditkartenkonto in Abwicklung. */
    public static final String CODE_CA = "CA";
    /** eidesstattliche Versicherung. */
    public static final String CODE_EV = "EV";
    /** Girokonto in Abwicklung. */
    public static final String CODE_GA = "GA";
    /** Haftbefehl. */
    public static final String CODE_HB = "HB";
    /** Insolvenzverfahren abgewiesen. */
    public static final String CODE_IA = "IA";
    /** Inkasso nach Verzug des Betroffenen und/oder Erteilung eines Inkassoauftrages bei unbest. Forderung. */
    public static final String CODE_IB = "IB";
    /** Insolvenzverfahren eröffnet. */
    public static final String CODE_IE = "IE";
    /** Abwicklungskonto. */
    public static final String CODE_KW = "KW";
    /** Saldo. */
    public static final String CODE_SD = "SD";
    /** titulierter Saldo. */
    public static final String CODE_SE = "SE";
    /** Saldo nach Gesamtfälligstellung. */
    public static final String CODE_SG = "SG";
    /** Saldovergleich. */
    public static final String CODE_SV = "SV";
    /** Forderungsverkauf. */
    public static final String CODE_SZ = "SZ";
    /** uneinbringliche titulierte Forderung. */
    public static final String CODE_UF = "UF";
    /** Übergabe einer Forderung an Inkassounternehmen zur Beitreibung. */
    public static final String CODE_UI = "UI";
    /** uneinbringlicher Saldo (ohne Titel). */
    public static final String CODE_US = "US";
    /** Insolvenzverfahren aufgehoben. */
    public static final String CODE_IS = "IS";
    /** Kontomissbrauch. */
    public static final String CODE_KM = "KM";
    /** Restschuldbefreiung angekündigt. */
    public static final String CODE_RA = "RA";
    /** Restschuldbefreiung erteilt. */
    public static final String CODE_RB = "RB";
    /** Rückgabe einer Forderung vom Inkassounternehmen an den Gläubiger. */
    public static final String CODE_RI = "RI";
    /** Restschuldbefreiung versagt. */
    public static final String CODE_RV = "RV";
    /** Schuldsaldo zum Auftrag zur Anschriftenermittlung über einen Schuldner, der unter Hinterlassung von Verbindlichkeiten mit unbekannter Anschrift verzogen ist. */
    public static final String CODE_SU = "SU";

    /** all negative schufa tokens. */
    public static final Collection<String> NEGATIVE_CODES = Collections.unmodifiableCollection(Arrays.asList(new String[] {CODE_CA, CODE_EV, CODE_GA, CODE_HB, CODE_IA, CODE_IB, CODE_IE, CODE_KW, CODE_SD, CODE_SE, CODE_SG, CODE_SV, CODE_SZ, CODE_UF, CODE_UI, CODE_US, CODE_IS, CODE_KM, CODE_RA, CODE_RB, CODE_RI, CODE_RV, CODE_SU}));

    // Tokens needed for Rule Engine
    public static final String CODE_HN = "HN";
    public static final String CODE_HY = "HY";
    public static final String CODE_AI = "AI";
    public static final String CODE_H3 = "H3";
    public static final String CODE_CR = "CR";

    public static final String CODE_K3 = "K3";
    public static final String CODE_K4 = "K4";

    // Tokens needed for Brokerage Banks
    /** Widerspruch zum titulierten Saldo. */
    public static final String CODE_SW = "SW";
    /** Widerspruch gegen gemeldeten Saldo. */
    public static final String CODE_WS = "WS";

    public static final String CODE_KX = "KX";

    public static final String CODE_RK = "RK";

    public static final String CODE_KG = "KG";

    public static final String CODE_DK = "DK";

    public static final String CODE_CC = "CC";

    public static final String CODE_VC = "VC";

    public static final String CODE_MX = "MX";

    public static final String CODE_MY = "MY";

    public static final String CODE_MK = "MK";

    public static final String CODE_BX = "BX";
    public static final String CODE_GI = "GI";
    public static final String CODE_UG = "UG";
    public static final String CODE_VY = "VY";
    public static final String CODE_HF = "HF";
    public static final String CODE_VG = "VG";
    public static final String CODE_K1 = "K1";


    public static final Collection<String> THIRD_PARTY_LOAN_CODES = Collections.unmodifiableCollection(Arrays.asList(
            new String[] {CODE_KR, CODE_KX, CODE_RK, CODE_KG, CODE_DK, CODE_CC, CODE_VC, CODE_CR,  CODE_ML, CODE_VY, CODE_HY, CODE_HP, CODE_HF, CODE_MA, CODE_MX, CODE_MY, CODE_MK, CODE_BU, CODE_BX, CODE_GI, CODE_UG, CODE_K4}));

    public double getAppliedRate(){
        return getCustomRate() == null ? getCalculatedRate() : getCustomRate();
    }

    public List<? extends Entity> getModifier() {
        List<Account> result = new LinkedList<Account>();
        if (getScore() != null) {
            result.add(getScore().getAccount());
        }
        return result;
    }

    public boolean isBrokerageThirdPartyLoan() {
        return (!getFinished()) && THIRD_PARTY_LOAN_CODES.contains(getTokenCode());
    }

}

