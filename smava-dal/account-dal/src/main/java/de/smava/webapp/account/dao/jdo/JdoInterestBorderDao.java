//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(interest border)}

import de.smava.webapp.account.dao.InterestBorderDao;
import de.smava.webapp.account.domain.InterestBorder;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'InterestBorders'.
 *
 * @author generator
 */
@Repository(value = "interestBorderDao")
public class JdoInterestBorderDao extends JdoBaseDao implements InterestBorderDao {

    private static final Logger LOGGER = Logger.getLogger(JdoInterestBorderDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(interest border)}
        //
	private static final long serialVersionUID = 2264926555362659302L;
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the interest border identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public InterestBorder load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorder - start: id=" + id);
        }
        InterestBorder result = getEntity(InterestBorder.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorder - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public InterestBorder getInterestBorder(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	InterestBorder entity = findUniqueEntity(InterestBorder.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the interest border.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(InterestBorder interestBorder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveInterestBorder: " + interestBorder);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(interest border)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(interestBorder);
    }

    /**
     * @deprecated Use {@link #save(InterestBorder) instead}
     */
    public Long saveInterestBorder(InterestBorder interestBorder) {
        return save(interestBorder);
    }

    /**
     * Deletes an interest border, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the interest border
     */
    public void deleteInterestBorder(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteInterestBorder: " + id);
        }
        deleteEntity(InterestBorder.class, id);
    }

    /**
     * Retrieves all 'InterestBorder' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<InterestBorder> getInterestBorderList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - start");
        }
        Collection<InterestBorder> result = getEntities(InterestBorder.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'InterestBorder' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<InterestBorder> getInterestBorderList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - start: pageable=" + pageable);
        }
        Collection<InterestBorder> result = getEntities(InterestBorder.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'InterestBorder' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<InterestBorder> getInterestBorderList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - start: sortable=" + sortable);
        }
        Collection<InterestBorder> result = getEntities(InterestBorder.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'InterestBorder' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<InterestBorder> getInterestBorderList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<InterestBorder> result = getEntities(InterestBorder.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'InterestBorder' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InterestBorder> findInterestBorderList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - start: whereClause=" + whereClause);
        }
        Collection<InterestBorder> result = findEntities(InterestBorder.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'InterestBorder' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<InterestBorder> findInterestBorderList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<InterestBorder> result = findEntities(InterestBorder.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'InterestBorder' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InterestBorder> findInterestBorderList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<InterestBorder> result = findEntities(InterestBorder.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'InterestBorder' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InterestBorder> findInterestBorderList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<InterestBorder> result = findEntities(InterestBorder.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'InterestBorder' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InterestBorder> findInterestBorderList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<InterestBorder> result = findEntities(InterestBorder.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'InterestBorder' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InterestBorder> findInterestBorderList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<InterestBorder> result = findEntities(InterestBorder.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInterestBorderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'InterestBorder' instances.
     */
    public long getInterestBorderCount() {
        long result = getEntityCount(InterestBorder.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'InterestBorder' instances which match the given whereClause.
     */
    public long getInterestBorderCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(InterestBorder.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'InterestBorder' instances which match the given whereClause together with params specified in object array.
     */
    public long getInterestBorderCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(InterestBorder.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInterestBorderCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(interest border)}
    //

    /**
     {@inheritDoc}
     */
    @Override
	public InterestBorder getValuesForMarket( String rating, int duration, boolean adjusted){
		
		Collection<InterestBorder> val = findInterestBorderList("_adjusted == " + adjusted + " && _duration == " + duration + " && _rating =='" + rating +"'");
		for ( InterestBorder ib: val){
			return ib;
		}
		return null;
	}

    public Collection<InterestBorder> getInterestBorderValues(String rating, int duration, boolean adjusted){
        StringBuilder where = new StringBuilder("_rating == '");
        where.append(rating);
        where.append("' && _duration == ");
        where.append(duration);

        if (adjusted) {
            where.append(" && _adjusted == true");
        } else {
            where.append(" && _adjusted == false");
        }

        return findInterestBorderList(where.toString());
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
