package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBankAccountProvider;




/**
 * The domain object that has all history aggregation related fields for 'BankAccountProviders'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BankAccountProviderHistory extends AbstractBankAccountProvider {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _schufaUsernameInitVal;
    protected transient boolean _schufaUsernameIsSet;
    protected transient String _schufaPasswordInitVal;
    protected transient boolean _schufaPasswordIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufaUsername'.
     */
    public String schufaUsernameInitVal() {
        String result;
        if (_schufaUsernameIsSet) {
            result = _schufaUsernameInitVal;
        } else {
            result = getSchufaUsername();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufaUsername'.
     */
    public boolean schufaUsernameIsDirty() {
        return !valuesAreEqual(schufaUsernameInitVal(), getSchufaUsername());
    }

    /**
     * Returns true if the setter method was called for the property 'schufaUsername'.
     */
    public boolean schufaUsernameIsSet() {
        return _schufaUsernameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufaPassword'.
     */
    public String schufaPasswordInitVal() {
        String result;
        if (_schufaPasswordIsSet) {
            result = _schufaPasswordInitVal;
        } else {
            result = getSchufaPassword();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufaPassword'.
     */
    public boolean schufaPasswordIsDirty() {
        return !valuesAreEqual(schufaPasswordInitVal(), getSchufaPassword());
    }

    /**
     * Returns true if the setter method was called for the property 'schufaPassword'.
     */
    public boolean schufaPasswordIsSet() {
        return _schufaPasswordIsSet;
    }
	
}
