package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;

import java.util.*;

import de.smava.webapp.account.domain.interfaces.DocumentAddressAssignmentEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'DocumentAddressAssignments'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractDocumentAddressAssignment
    extends KreditPrivatEntity implements DocumentAddressAssignmentEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDocumentAddressAssignment.class);

    /**
     * Does nothing if either document or address is not set.
     */
    public static DocumentAddressAssignment connect(Document document, Address address, String documentAddressType) {
        DocumentAddressAssignment assignment = null;
        if (document != null && address != null) {
            assignment = new DocumentAddressAssignment(document, address, documentAddressType);
            document.addDocumentAddressAssignment(assignment);
        }

        return assignment;
    }

    public static List<DocumentAddressAssignment> disconnect(Document document, Address address, String documentAddressType) {
        LOGGER.debug("disconnect: try to disconnect document " + document.getId() + " from address " + address.getId() + " (type = " + documentAddressType + ")");
        List<DocumentAddressAssignment> toRemove = new ArrayList<DocumentAddressAssignment>();
        for (DocumentAddressAssignment assignment : document.getDocumentAddressAssignments()) {
            if (assignment.getDocument().equals(document) && assignment.getAddress().equals(address) && (assignment.getType().equals(documentAddressType) || "*".equals(documentAddressType))) {
                LOGGER.debug("disconnect: mark assignment " + assignment.getId() + " for removal");
                toRemove.add(assignment);
            }
        }
        for (DocumentAddressAssignment det : toRemove) {
            LOGGER.debug("disconnect: remove assignment + " + det.getId() + " from document " + document.getId());
            document.getDocumentAddressAssignments().remove(det);
            LOGGER.debug("disconnect: remove assignment + " + det.getId() + " from address " + address.getId());
        }
        return toRemove;
    }

    /**
     * For given doc get all addresss that are connected with the doc respecting the type.
     * @param documentAddressType Type constant or "*" for all
     */
    public static List<Address> getRelatedAddresses(Document doc, String documentAddressType) {
        List<Address> list = new ArrayList<Address>();
        if (doc.getDocumentAddressAssignments() != null) {
            for (DocumentAddressAssignment det : doc.getDocumentAddressAssignments()) {
                if (det.getDocument().equals(doc) && (det.getType().equals(documentAddressType) || "*".equals(documentAddressType))) {
                    list.add(det.getAddress());
                }
            }
        }
        return list;
    }

    /**
     * For given address get (all different) addresss that are connected by same Document.
     * @param documentAddressType Type constant or "*" for all
     */
    public static List<Address> getRelatedAddress(Address address, String documentAddressType) {
        List<Address> list = new ArrayList<Address>();

        if (true) {
            throw new RuntimeException("not implemented");
        }
        return list;
    }

    public List<? extends Entity> getModifier() {
        return Collections.singletonList(getDocument().getOwner());
    }

}

