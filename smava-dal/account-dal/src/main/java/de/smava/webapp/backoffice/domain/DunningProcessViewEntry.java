//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.backoffice.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dunning process view entry)}
import de.smava.webapp.backoffice.domain.history.DunningProcessViewEntryHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!

/**
 * The domain object that represents 'DunningProcessViewEntrys'.
 *
 * 
 *
 * @author generator
 */
    public class DunningProcessViewEntry extends DunningProcessViewEntryHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(dunning process view entry)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _id;
        protected Long _groupId;
        protected Integer _arrearCounter;
        protected String _rkv;
        protected double _amount;
        protected Date _startDate;
        protected double _transactionAmount;
        protected Date _transactionDueDate;
        protected Integer _level;
        protected Long _accId;
        protected Long _accountId;
        protected String _userName;
        protected String _creditTerm;
        protected String _rating;
        protected Integer _age;
        protected String _occupation;
        protected de.smava.webapp.backoffice.domain.DunningProcessViewData _userData;
        
                                    
    /**
     * Setter for the property 'id'.
     *
     * 
     *
     */
    public void setId(Long id) {
        _id = id;
    }
            
    /**
     * Returns the property 'id'.
     *
     * 
     *
     */
    public Long getId() {
        return _id;
    }
                                            
    /**
     * Setter for the property 'group id'.
     *
     * 
     *
     */
    public void setGroupId(Long groupId) {
        _groupId = groupId;
    }
            
    /**
     * Returns the property 'group id'.
     *
     * 
     *
     */
    public Long getGroupId() {
        return _groupId;
    }
                                            
    /**
     * Setter for the property 'arrear counter'.
     *
     * 
     *
     */
    public void setArrearCounter(Integer arrearCounter) {
        _arrearCounter = arrearCounter;
    }
            
    /**
     * Returns the property 'arrear counter'.
     *
     * 
     *
     */
    public Integer getArrearCounter() {
        return _arrearCounter;
    }
                                    /**
     * Setter for the property 'rkv'.
     *
     * 
     *
     */
    public void setRkv(String rkv) {
                if (!_rkvIsSet) {
            _rkvIsSet = true;
            _rkvInitVal = getRkv();
        }
        registerChange("rkv", _rkvInitVal, rkv);
                _rkv = rkv;
    }
                        
    /**
     * Returns the property 'rkv'.
     *
     * 
     *
     */
    public String getRkv() {
        return _rkv;
    }
                                    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(double amount) {
                if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
                _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    public void setStartDate(Date startDate) {
                if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = getStartDate();
        }
        registerChange("start date", _startDateInitVal, startDate);
                _startDate = startDate;
    }
                        
    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    public Date getStartDate() {
        return _startDate;
    }
                                    /**
     * Setter for the property 'transaction amount'.
     *
     * 
     *
     */
    public void setTransactionAmount(double transactionAmount) {
                if (!_transactionAmountIsSet) {
            _transactionAmountIsSet = true;
            _transactionAmountInitVal = getTransactionAmount();
        }
        registerChange("transaction amount", _transactionAmountInitVal, transactionAmount);
                _transactionAmount = transactionAmount;
    }
                        
    /**
     * Returns the property 'transaction amount'.
     *
     * 
     *
     */
    public double getTransactionAmount() {
        return _transactionAmount;
    }
                                    /**
     * Setter for the property 'transaction due date'.
     *
     * 
     *
     */
    public void setTransactionDueDate(Date transactionDueDate) {
                if (!_transactionDueDateIsSet) {
            _transactionDueDateIsSet = true;
            _transactionDueDateInitVal = getTransactionDueDate();
        }
        registerChange("transaction due date", _transactionDueDateInitVal, transactionDueDate);
                _transactionDueDate = transactionDueDate;
    }
                        
    /**
     * Returns the property 'transaction due date'.
     *
     * 
     *
     */
    public Date getTransactionDueDate() {
        return _transactionDueDate;
    }
                                            
    /**
     * Setter for the property 'level'.
     *
     * 
     *
     */
    public void setLevel(Integer level) {
        _level = level;
    }
            
    /**
     * Returns the property 'level'.
     *
     * 
     *
     */
    public Integer getLevel() {
        return _level;
    }
                                            
    /**
     * Setter for the property 'acc id'.
     *
     * 
     *
     */
    public void setAccId(Long accId) {
        _accId = accId;
    }
            
    /**
     * Returns the property 'acc id'.
     *
     * 
     *
     */
    public Long getAccId() {
        return _accId;
    }
                                            
    /**
     * Setter for the property 'account id'.
     *
     * 
     *
     */
    public void setAccountId(Long accountId) {
        _accountId = accountId;
    }
            
    /**
     * Returns the property 'account id'.
     *
     * 
     *
     */
    public Long getAccountId() {
        return _accountId;
    }
                                    /**
     * Setter for the property 'user name'.
     *
     * 
     *
     */
    public void setUserName(String userName) {
                if (!_userNameIsSet) {
            _userNameIsSet = true;
            _userNameInitVal = getUserName();
        }
        registerChange("user name", _userNameInitVal, userName);
                _userName = userName;
    }
                        
    /**
     * Returns the property 'user name'.
     *
     * 
     *
     */
    public String getUserName() {
        return _userName;
    }
                                    /**
     * Setter for the property 'credit term'.
     *
     * 
     *
     */
    public void setCreditTerm(String creditTerm) {
                if (!_creditTermIsSet) {
            _creditTermIsSet = true;
            _creditTermInitVal = getCreditTerm();
        }
        registerChange("credit term", _creditTermInitVal, creditTerm);
                _creditTerm = creditTerm;
    }
                        
    /**
     * Returns the property 'credit term'.
     *
     * 
     *
     */
    public String getCreditTerm() {
        return _creditTerm;
    }
                                    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    public void setRating(String rating) {
                if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
                _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    public String getRating() {
        return _rating;
    }
                                            
    /**
     * Setter for the property 'age'.
     *
     * 
     *
     */
    public void setAge(Integer age) {
        _age = age;
    }
            
    /**
     * Returns the property 'age'.
     *
     * 
     *
     */
    public Integer getAge() {
        return _age;
    }
                                    /**
     * Setter for the property 'occupation'.
     *
     * 
     *
     */
    public void setOccupation(String occupation) {
                if (!_occupationIsSet) {
            _occupationIsSet = true;
            _occupationInitVal = getOccupation();
        }
        registerChange("occupation", _occupationInitVal, occupation);
                _occupation = occupation;
    }
                        
    /**
     * Returns the property 'occupation'.
     *
     * 
     *
     */
    public String getOccupation() {
        return _occupation;
    }
                                            
    /**
     * Setter for the property 'user data'.
     *
     * 
     *
     */
    public void setUserData(de.smava.webapp.backoffice.domain.DunningProcessViewData userData) {
        _userData = userData;
    }
            
    /**
     * Returns the property 'user data'.
     *
     * 
     *
     */
    public de.smava.webapp.backoffice.domain.DunningProcessViewData getUserData() {
        return _userData;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DunningProcessViewEntry.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rkv=").append(_rkv);
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _transactionAmount=").append(_transactionAmount);
            builder.append("\n    _userName=").append(_userName);
            builder.append("\n    _creditTerm=").append(_creditTerm);
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _occupation=").append(_occupation);
            builder.append("\n}");
        } else {
            builder.append(DunningProcessViewEntry.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DunningProcessViewEntry asDunningProcessViewEntry() {
        return this;
    }
}
