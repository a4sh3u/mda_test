package de.smava.webapp.account.todo.web;


/**
 * base command to support paging.
 * 
 * @author rfiedler
 *
 */
public class PageableCommand extends SortableCommand {

    private int _offset;
    private long _count;
    private int _pageCount = 10;

    public PageableCommand() {
        super();
    }

    public int getOffset() {
        return _offset;
    }

    public void setOffset(int offset) {
        this._offset = offset;
    }

    public long getCount() {
        return _count;
    }

    public void setCount(long count) {
        this._count = count;
    }

    public int getPageCount() {
        return _pageCount;
    }

    public void setPageCount(int pageCount) {
        this._pageCount = pageCount;
    }

}