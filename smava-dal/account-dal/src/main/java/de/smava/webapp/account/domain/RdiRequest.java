package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.RdiRequestHistory;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * The domain object that represents 'RdiRequests'.
 */
public class RdiRequest extends RdiRequestHistory  {

    public Collection<RdiEntry> getEntries() {
        return _entries;
    }

    /**
     * Custom method to initialize transient map after setting the entries collection.
     */
    public void setEntries(Collection<RdiEntry> entries) {
        _entries = entries;
        if (_entryMap == null) {
            _entryMap = new HashMap<String, String>();
        }
        if (entries != null) {
            for (RdiEntry rdiEntry : entries) {
                _entryMap.put(rdiEntry.getKey(), rdiEntry.getValue());
            }
        }
    }

        protected Date _creationDate;
        protected RdiContract _contract;
        protected String _state;
        protected String _errorMessage;
        protected Collection<RdiEntry> _entries;
        protected RdiResponse _response;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'contract'.
     */
    public void setContract(RdiContract contract) {
        _contract = contract;
    }
            
    /**
     * Returns the property 'contract'.
     */
    public RdiContract getContract() {
        return _contract;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'error message'.
     */
    public void setErrorMessage(String errorMessage) {
        if (!_errorMessageIsSet) {
            _errorMessageIsSet = true;
            _errorMessageInitVal = getErrorMessage();
        }
        registerChange("error message", _errorMessageInitVal, errorMessage);
        _errorMessage = errorMessage;
    }
                        
    /**
     * Returns the property 'error message'.
     */
    public String getErrorMessage() {
        return _errorMessage;
    }
                                                        
    /**
     * Setter for the property 'response'.
     */
    public void setResponse(RdiResponse response) {
        _response = response;
    }
            
    /**
     * Returns the property 'response'.
     */
    public RdiResponse getResponse() {
        return _response;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RdiRequest.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _errorMessage=").append(_errorMessage);
            builder.append("\n}");
        } else {
            builder.append(RdiRequest.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
