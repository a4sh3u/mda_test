package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBankAccount;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BankAccounts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BankAccountHistory extends AbstractBankAccount {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _bankInitVal;
    protected transient boolean _bankIsSet;
    protected transient String _accountNoInitVal;
    protected transient boolean _accountNoIsSet;
    protected transient String _bankCodeInitVal;
    protected transient boolean _bankCodeIsSet;
    protected transient boolean _smavaOnlyInitVal;
    protected transient boolean _smavaOnlyIsSet;
    protected transient boolean _debitAllowedInitVal;
    protected transient boolean _debitAllowedIsSet;
    protected transient boolean _validInitVal;
    protected transient boolean _validIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _customerNumberInitVal;
    protected transient boolean _customerNumberIsSet;
    protected transient String _bicInitVal;
    protected transient boolean _bicIsSet;
    protected transient String _ibanInitVal;
    protected transient boolean _ibanIsSet;
    protected transient boolean _activityFeeInitVal;
    protected transient boolean _activityFeeIsSet;
    protected transient String _sepaMandateReferenceInitVal;
    protected transient boolean _sepaMandateReferenceIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank'.
     */
    public String bankInitVal() {
        String result;
        if (_bankIsSet) {
            result = _bankInitVal;
        } else {
            result = getBank();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank'.
     */
    public boolean bankIsDirty() {
        return !valuesAreEqual(bankInitVal(), getBank());
    }

    /**
     * Returns true if the setter method was called for the property 'bank'.
     */
    public boolean bankIsSet() {
        return _bankIsSet;
    }
	
    /**
     * Returns the initial value of the property 'account no'.
     */
    public String accountNoInitVal() {
        String result;
        if (_accountNoIsSet) {
            result = _accountNoInitVal;
        } else {
            result = getAccountNo();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'account no'.
     */
    public boolean accountNoIsDirty() {
        return !valuesAreEqual(accountNoInitVal(), getAccountNo());
    }

    /**
     * Returns true if the setter method was called for the property 'account no'.
     */
    public boolean accountNoIsSet() {
        return _accountNoIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank code'.
     */
    public String bankCodeInitVal() {
        String result;
        if (_bankCodeIsSet) {
            result = _bankCodeInitVal;
        } else {
            result = getBankCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank code'.
     */
    public boolean bankCodeIsDirty() {
        return !valuesAreEqual(bankCodeInitVal(), getBankCode());
    }

    /**
     * Returns true if the setter method was called for the property 'bank code'.
     */
    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'smava only'.
     */
    public boolean smavaOnlyInitVal() {
        boolean result;
        if (_smavaOnlyIsSet) {
            result = _smavaOnlyInitVal;
        } else {
            result = getSmavaOnly();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'smava only'.
     */
    public boolean smavaOnlyIsDirty() {
        return !valuesAreEqual(smavaOnlyInitVal(), getSmavaOnly());
    }

    /**
     * Returns true if the setter method was called for the property 'smava only'.
     */
    public boolean smavaOnlyIsSet() {
        return _smavaOnlyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit allowed'.
     */
    public boolean debitAllowedInitVal() {
        boolean result;
        if (_debitAllowedIsSet) {
            result = _debitAllowedInitVal;
        } else {
            result = getDebitAllowed();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit allowed'.
     */
    public boolean debitAllowedIsDirty() {
        return !valuesAreEqual(debitAllowedInitVal(), getDebitAllowed());
    }

    /**
     * Returns true if the setter method was called for the property 'debit allowed'.
     */
    public boolean debitAllowedIsSet() {
        return _debitAllowedIsSet;
    }
		
    /**
     * Returns the initial value of the property 'valid'.
     */
    public boolean validInitVal() {
        boolean result;
        if (_validIsSet) {
            result = _validInitVal;
        } else {
            result = getValid();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid'.
     */
    public boolean validIsDirty() {
        return !valuesAreEqual(validInitVal(), getValid());
    }

    /**
     * Returns true if the setter method was called for the property 'valid'.
     */
    public boolean validIsSet() {
        return _validIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'customer number'.
     */
    public String customerNumberInitVal() {
        String result;
        if (_customerNumberIsSet) {
            result = _customerNumberInitVal;
        } else {
            result = getCustomerNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'customer number'.
     */
    public boolean customerNumberIsDirty() {
        return !valuesAreEqual(customerNumberInitVal(), getCustomerNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'customer number'.
     */
    public boolean customerNumberIsSet() {
        return _customerNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bic'.
     */
    public String bicInitVal() {
        String result;
        if (_bicIsSet) {
            result = _bicInitVal;
        } else {
            result = getBic();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bic'.
     */
    public boolean bicIsDirty() {
        return !valuesAreEqual(bicInitVal(), getBic());
    }

    /**
     * Returns true if the setter method was called for the property 'bic'.
     */
    public boolean bicIsSet() {
        return _bicIsSet;
    }
	
    /**
     * Returns the initial value of the property 'iban'.
     */
    public String ibanInitVal() {
        String result;
        if (_ibanIsSet) {
            result = _ibanInitVal;
        } else {
            result = getIban();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'iban'.
     */
    public boolean ibanIsDirty() {
        return !valuesAreEqual(ibanInitVal(), getIban());
    }

    /**
     * Returns true if the setter method was called for the property 'iban'.
     */
    public boolean ibanIsSet() {
        return _ibanIsSet;
    }
				
    /**
     * Returns the initial value of the property 'activity fee'.
     */
    public boolean activityFeeInitVal() {
        boolean result;
        if (_activityFeeIsSet) {
            result = _activityFeeInitVal;
        } else {
            result = getActivityFee();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'activity fee'.
     */
    public boolean activityFeeIsDirty() {
        return !valuesAreEqual(activityFeeInitVal(), getActivityFee());
    }

    /**
     * Returns true if the setter method was called for the property 'activity fee'.
     */
    public boolean activityFeeIsSet() {
        return _activityFeeIsSet;
    }
		
    /**
     * Returns the initial value of the property 'sepa mandate reference'.
     */
    public String sepaMandateReferenceInitVal() {
        String result;
        if (_sepaMandateReferenceIsSet) {
            result = _sepaMandateReferenceInitVal;
        } else {
            result = getSepaMandateReference();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'sepa mandate reference'.
     */
    public boolean sepaMandateReferenceIsDirty() {
        return !valuesAreEqual(sepaMandateReferenceInitVal(), getSepaMandateReference());
    }

    /**
     * Returns true if the setter method was called for the property 'sepa mandate reference'.
     */
    public boolean sepaMandateReferenceIsSet() {
        return _sepaMandateReferenceIsSet;
    }

}
