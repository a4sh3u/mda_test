package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractDocumentAttachmentContainer;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'DocumentAttachmentContainers'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DocumentAttachmentContainerHistory extends AbstractDocumentAttachmentContainer {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _processDateInitVal;
    protected transient boolean _processDateIsSet;
    protected transient Date _dateInitVal;
    protected transient boolean _dateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _directionInitVal;
    protected transient boolean _directionIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'process date'.
     */
    public Date processDateInitVal() {
        Date result;
        if (_processDateIsSet) {
            result = _processDateInitVal;
        } else {
            result = getProcessDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'process date'.
     */
    public boolean processDateIsDirty() {
        return !valuesAreEqual(processDateInitVal(), getProcessDate());
    }

    /**
     * Returns true if the setter method was called for the property 'process date'.
     */
    public boolean processDateIsSet() {
        return _processDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'date'.
     */
    public Date dateInitVal() {
        Date result;
        if (_dateIsSet) {
            result = _dateInitVal;
        } else {
            result = getDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date'.
     */
    public boolean dateIsDirty() {
        return !valuesAreEqual(dateInitVal(), getDate());
    }

    /**
     * Returns true if the setter method was called for the property 'date'.
     */
    public boolean dateIsSet() {
        return _dateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'direction'.
     */
    public String directionInitVal() {
        String result;
        if (_directionIsSet) {
            result = _directionInitVal;
        } else {
            result = getDirection();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'direction'.
     */
    public boolean directionIsDirty() {
        return !valuesAreEqual(directionInitVal(), getDirection());
    }

    /**
     * Returns true if the setter method was called for the property 'direction'.
     */
    public boolean directionIsSet() {
        return _directionIsSet;
    }
		
}
