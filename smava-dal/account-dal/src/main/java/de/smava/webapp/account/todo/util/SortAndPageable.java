/**
 * 
 */
package de.smava.webapp.account.todo.util;

import java.util.List;

/**
 * @author bvoss
 *
 * @param <T> type of page content
 */
public interface SortAndPageable<T> {
	
	int getOffset();
	int getPageCount();
	String getOrderBy();
	void setOrderBy(String sortBy);
	boolean isSortAscending();
	
	List<T> getPageContent();
	void setPageContent(List<T> contentList);
	long getCount();
	void setCount(long count);

}
