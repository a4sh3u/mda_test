package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.TransactionHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * The domain object that represents 'Transactions'.
 */
public class Transaction extends TransactionHistory  {

    public Transaction() {
        _bookings = new LinkedList<Booking>();
        _creationDate = CurrentDate.getDate();
    }

        protected Long _transactionId;
        protected Date _creationDate;
        protected Date _transactionDate;
        protected Date _dueDate;
        protected Date _modifiedDate;
        protected String _state;
        protected String _type;
        protected BankAccount _creditAccount;
        protected BankAccount _debitAccount;
        protected String _description;
        protected List<Booking> _bookings;
        protected DtausFile _dtausFile;
        protected Set<DtausEntryTransaction> _dtausEntryTransactions;
        protected List<ReminderState> _reminderStates;
        protected String _externalTransactionId;
        protected List<DtausFileOutTransaction> _dtausFileOutTransactions;
        
                                    
    /**
     * Setter for the property 'transaction id'.
     */
    public void setTransactionId(Long transactionId) {
        _transactionId = transactionId;
    }
            
    /**
     * Returns the property 'transaction id'.
     */
    public Long getTransactionId() {
        return _transactionId;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'transaction date'.
     */
    public void setTransactionDate(Date transactionDate) {
        if (!_transactionDateIsSet) {
            _transactionDateIsSet = true;
            _transactionDateInitVal = getTransactionDate();
        }
        registerChange("transaction date", _transactionDateInitVal, transactionDate);
        _transactionDate = transactionDate;
    }
                        
    /**
     * Returns the property 'transaction date'.
     */
    public Date getTransactionDate() {
        return _transactionDate;
    }
                                    /**
     * Setter for the property 'due date'.
     */
    public void setDueDate(Date dueDate) {
        if (!_dueDateIsSet) {
            _dueDateIsSet = true;
            _dueDateInitVal = getDueDate();
        }
        registerChange("due date", _dueDateInitVal, dueDate);
        _dueDate = dueDate;
    }
                        
    /**
     * Returns the property 'due date'.
     */
    public Date getDueDate() {
        return _dueDate;
    }
                                    /**
     * Setter for the property 'modified date'.
     */
    public void setModifiedDate(Date modifiedDate) {
        if (!_modifiedDateIsSet) {
            _modifiedDateIsSet = true;
            _modifiedDateInitVal = getModifiedDate();
        }
        registerChange("modified date", _modifiedDateInitVal, modifiedDate);
        _modifiedDate = modifiedDate;
    }
                        
    /**
     * Returns the property 'modified date'.
     */
    public Date getModifiedDate() {
        return _modifiedDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'credit account'.
     */
    public void setCreditAccount(BankAccount creditAccount) {
        _creditAccount = creditAccount;
    }
            
    /**
     * Returns the property 'credit account'.
     */
    public BankAccount getCreditAccount() {
        return _creditAccount;
    }
                                            
    /**
     * Setter for the property 'debit account'.
     */
    public void setDebitAccount(BankAccount debitAccount) {
        _debitAccount = debitAccount;
    }
            
    /**
     * Returns the property 'debit account'.
     */
    public BankAccount getDebitAccount() {
        return _debitAccount;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                            
    /**
     * Setter for the property 'bookings'.
     */
    public void setBookings(List<Booking> bookings) {
        _bookings = bookings;
    }
            
    /**
     * Returns the property 'bookings'.
     */
    public List<Booking> getBookings() {
        return _bookings;
    }
                                            
    /**
     * Setter for the property 'dtaus file'.
     */
    public void setDtausFile(DtausFile dtausFile) {
        _dtausFile = dtausFile;
    }
            
    /**
     * Returns the property 'dtaus file'.
     */
    public DtausFile getDtausFile() {
        return _dtausFile;
    }
                                            
    /**
     * Setter for the property 'dtaus entry transactions'.
     */
    public void setDtausEntryTransactions(Set<DtausEntryTransaction> dtausEntryTransactions) {
        _dtausEntryTransactions = dtausEntryTransactions;
    }
            
    /**
     * Returns the property 'dtaus entry transactions'.
     */
    public Set<DtausEntryTransaction> getDtausEntryTransactions() {
        return _dtausEntryTransactions;
    }
                                            
    /**
     * Setter for the property 'reminder states'.
     */
    public void setReminderStates(List<ReminderState> reminderStates) {
        _reminderStates = reminderStates;
    }
            
    /**
     * Returns the property 'reminder states'.
     */
    public List<ReminderState> getReminderStates() {
        return _reminderStates;
    }
                                    /**
     * Setter for the property 'external transaction id'.
     */
    public void setExternalTransactionId(String externalTransactionId) {
        if (!_externalTransactionIdIsSet) {
            _externalTransactionIdIsSet = true;
            _externalTransactionIdInitVal = getExternalTransactionId();
        }
        registerChange("external transaction id", _externalTransactionIdInitVal, externalTransactionId);
        _externalTransactionId = externalTransactionId;
    }
                        
    /**
     * Returns the property 'external transaction id'.
     */
    public String getExternalTransactionId() {
        return _externalTransactionId;
    }
                                            
    /**
     * Setter for the property 'dtausFileOutTransactions'.
     */
    public void setDtausFileOutTransactions(List<DtausFileOutTransaction> dtausFileOutTransactions) {
        _dtausFileOutTransactions = dtausFileOutTransactions;
    }
            
    /**
     * Returns the property 'dtausFileOutTransactions'.
     */
    public List<DtausFileOutTransaction> getDtausFileOutTransactions() {
        return _dtausFileOutTransactions;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Transaction.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _externalTransactionId=").append(_externalTransactionId);
            builder.append("\n}");
        } else {
            builder.append(Transaction.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Transaction instance() {
        return this;
    }
}
