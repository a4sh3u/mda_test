//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking)}

import de.smava.webapp.account.dao.BookingDao;
import de.smava.webapp.account.domain.BookingSumParameters;
import de.smava.webapp.account.util.Period;
import de.smava.webapp.account.domain.PayoutValuesContainer;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.Booking.PoolPerformanceBookingInformation;
import de.smava.webapp.commons.constants.NumericalData;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Bookings'.
 *
 * @author generator
 */
@Repository(value = "bookingDao")
public class JdoBookingDao extends JdoBaseDao implements BookingDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBookingDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(booking)}
    /**
     * generated serial.
     */
    private static final long serialVersionUID = -2707284463553985427L;

    private static final Collection<Integer> LENDER_ARMORTISATION_TYPES = Collections.unmodifiableCollection(
            Arrays.asList(
                    BookingType.LENDER_AMORTISATION.getDbValue(),
                    BookingType.LENDER_LATE_INTEREST.getDbValue(),
                    BookingType.LENDER_UNSCHED_PAYMENT.getDbValue()));
    
    private static final Collection<Integer> BORROWER_ARMORTISATION_TYPES = Collections.unmodifiableCollection(
            Arrays.asList(
                    BookingType.BORROWER_AMORTISATION.getDbValue(),
                    BookingType.BORROWER_LATE_INTEREST.getDbValue(),
                    BookingType.BORROWER_ENCASHMENT_AMORTISATION.getDbValue()));

    private static final String ISTARGET_MAP_SQL_BEGIN
        = "select ba.booking, "
        + "case bg.type when 'CONTRACT_CANCELLATION' then true "
        + "else ba.is_target "
        + "end as is_target "
        + "from booking_assignment ba "
        + "inner join booking_group bg on ba.booking_group = bg.id "
        + "where bg.type in ('INSURANCE', 'CONTRACT_CANCELLATION') and bg.id = ? "
        + " and ba.booking in (";
    
    
    // !!!!!!!! End of insert code section !!!!!!!!

    @Override
    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the booking identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    @Override
    public Booking load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBooking - start: id=" + id);
        }
        Booking result = getEntity(Booking.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBooking - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    @Deprecated
    @Override
    public Booking getBooking(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    @Override
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Booking entity = findUniqueEntity(Booking.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the booking.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    @Override
    public Long save(Booking booking) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBooking: " + booking);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(booking)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(booking);
    }

    /**
     * @deprecated Use {@link #save(Booking) instead}
     */
    @Deprecated
    @Override
    public Long saveBooking(Booking booking) {
        return save(booking);
    }

    /**
     * Deletes an booking, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the booking
     */
    @Override
    public void deleteBooking(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBooking: " + id);
        }
        deleteEntity(Booking.class, id);
    }

    /**
     * Retrieves all 'Booking' instances and returns them attached to
     * the current persistence manager.
     */
    @Override
    public Collection<Booking> getBookingList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - start");
        }
        Collection<Booking> result = getEntities(Booking.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Booking' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    @Override
    public Collection<Booking> getBookingList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - start: pageable=" + pageable);
        }
        Collection<Booking> result = getEntities(Booking.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Booking' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    @Override
    public Collection<Booking> getBookingList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - start: sortable=" + sortable);
        }
        Collection<Booking> result = getEntities(Booking.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Booking' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    @Override
    public Collection<Booking> getBookingList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Booking> result = getEntities(Booking.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Booking' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Booking> findBookingList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - start: whereClause=" + whereClause);
        }
        Collection<Booking> result = findEntities(Booking.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Booking' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Booking> findBookingList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Booking> result = findEntities(Booking.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Booking' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Booking> findBookingList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Booking> result = findEntities(Booking.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Booking' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Booking> findBookingList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Booking> result = findEntities(Booking.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Booking' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Booking> findBookingList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Booking> result = findEntities(Booking.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Booking' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Booking> findBookingList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Booking> result = findEntities(Booking.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Booking' instances.
     */
    @Override
    public long getBookingCount() {
        long result = getEntityCount(Booking.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Booking' instances which match the given whereClause.
     */
    public long getBookingCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Booking.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Booking' instances which match the given whereClause together with params specified in object array.
     */
    public long getBookingCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Booking.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingCount() - result: count=" + result);
        }
        return result;
    }

    @Override
    public Booking findInvitationBooking(Account sender, Account recipient) {
        Query q = getPersistenceManager().newNamedQuery(Booking.class, "getInvitationBookings");
        q.setUnique(true);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("recipient", recipient);
        params.put("type", BookingType.INVITATION_BONUS.getDbValue());
        params.put("transactionState", Transaction.STATE_SUCCESSFUL);
        params.put("senderCreditAccount", sender.getReferenceBankAccount());

        return (Booking) q.executeWithMap(params);
    }



    @Override
    @SuppressWarnings("unchecked")
    public Collection<Booking> getTargetBookingsForGroupAndContract(BookingGroup bookingGroup, Contract contract) {
        final PersistenceManager pm = getPersistenceManager();
        final Query q = pm.newNamedQuery(Booking.class, "findTargetBookingsForGroupAndContract");
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("bookingGroupId", bookingGroup.getId());
        params.put("contractId", contract.getId());
        final Collection<Booking> result = (Collection<Booking>) q.executeWithMap(params);
        return result;
    }
    
    
    
    @Override
    public Collection<Booking> getBookingsForBankAccount(
            BankAccount bankAccount,
            Set<String> transactionStates,
            Set<BookingType> types){

        List<Object> params = new ArrayList<Object>();

        StringBuilder query = new StringBuilder();
        query.append("select b.id as id");
        query.append("  from booking b, transaction t, bank_account a");
        query.append(" where b.transaction_id = t.id");
        query.append("   and t.state in (");
        for (Iterator<String> iter = transactionStates.iterator(); iter.hasNext();) {
            String state = iter.next();
            params.add(state);
            query.append("?");
            if (iter.hasNext()) {
                query.append(", ");
            }
        }
        query.append(") and b.type_new in (");
        for (Iterator<BookingType> iter = types.iterator(); iter.hasNext();) {
            BookingType type = iter.next();
            params.add(type.getDbValue());
            query.append("?");
            if (iter.hasNext()) {
                query.append(", ");
            }
        }
        //subselect proven to be the most efficient method to fetch this by EXPLAIN in postgres
        query.append(") and a.id in (select id from bank_account where ");
        query.append("account_no = ? and bank_code = ?) ");
        params.add(bankAccount.getAccountNo());
        params.add(bankAccount.getBankCode());
        query.append("and (t.credit_account = a.id or t.debit_account = a.id)");

        Query q = getPersistenceManager().newQuery(
                "javax.jdo.query.SQL", query.toString());
        q.setClass(Booking.class);
        @SuppressWarnings("unchecked")
        Collection<Booking> result =
            (Collection<Booking>) q.executeWithArray(params.toArray(new Object[0]));
        return result;
    }
    
    @Override
    public Collection<Booking> getInternalBookingsForGroupAndContracts(BookingGroup bookingGroup, Collection<BankAccount> smavaInterimAccounts, Collection<Contract> contracts) {

        Map<Integer, Object> params = new HashMap<Integer, Object>();
        int paramsIndex = 1;
        StringBuilder queryString = new StringBuilder();
        queryString.append("select booking.id as id from booking,transaction,booking_assignment where");

        queryString.append(" (");
        
        boolean isFirstBankAccount = true;
        for (BankAccount bankAccount : smavaInterimAccounts) {
            if (!isFirstBankAccount) {
                queryString.append(" or ");
            } else {
                isFirstBankAccount = false;
            }            
            queryString.append(" transaction.credit_account = ").append(bankAccount.getId()).append(" or transaction.debit_account =  ").append(bankAccount.getId());
            params.put(paramsIndex++, bankAccount.getId());
            params.put(paramsIndex++, bankAccount.getId());            
        }
        queryString.append(")");
        queryString.append(" and booking.id = booking_assignment.booking ");
        queryString.append(" and booking.transaction_id = transaction.id ");
        queryString.append(" and transaction.state in ('OPEN', 'SUCCESSFUL', 'FAILED', 'VOID')"); // WEBSITE-3338
        queryString.append(" and booking_assignment.booking_group =  ").append(bookingGroup.getId());
        params.put(paramsIndex++, bookingGroup.getId());
        
        queryString.append(" and (");
        boolean isFirstContract = true;
        for (Contract contract : contracts) {
            if (!isFirstContract) {
                queryString.append(" or ");
            } else {
                isFirstContract = false;
            }            
            queryString.append(" booking.contract_id =  ").append(contract.getId());
            params.put(paramsIndex++, contract.getId());
        }
        queryString.append(" )");
        
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setClass(Booking.class);
        @SuppressWarnings("unchecked")
        Collection<Booking> result = (Collection<Booking>) q.execute(); 
        return result;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public Collection<Booking> findMarketingBonusBookings(Account account) {

        Collection<Booking> result = new ArrayList<Booking>();
        if (account != null && account.getReferenceBankAccountHistory() != null) {
            final Query q = getPersistenceManager().newNamedQuery(Booking.class, "findMarketingBonusBookings");

            final Collection<String> transactionStates = new ArrayList<String>();
            transactionStates.add(Transaction.STATE_OPEN);
            transactionStates.add(Transaction.STATE_SUCCESSFUL);

            final Map<String, Object> params = new HashMap<String, Object>();
            params.put("types", Collections.singleton(BookingType.MARKETING_BONUS.getDbValue()));
            params.put("creditBankAccounts", account.getReferenceBankAccountHistory());
            params.put("transactionStates", transactionStates);

            result = (Collection<Booking>) q.executeWithMap(params);
        }
        return result;
  
    }

    
    private String getMarketNamesExtension(Collection<String> marketNames) {
        String marketNamesExtension = "";

        if (marketNames != null) {
            marketNamesExtension += " && (";
            String operator = "";
            for (String marketName : marketNames) {
                marketNamesExtension += operator + "_contract._marketName == '" + marketName + "'";
                operator = " || ";
            }

            marketNamesExtension += ")";
        }
        return marketNamesExtension;
    }

    
    private double getBookingSum(final Account account, final Collection<BankAccount> interimAccounts, final Collection<Integer> bookingTypes, final String transactionState, final String whereClauseExtension) {
        double result = 0.0;

        String accountPart = "";
        if (account != null) {
            if (account.isLender()) {
                accountPart = "_contract._offer._account == account && ";
            } else if (account.isBorrower()) {
                accountPart = "_contract._order._account == account && ";
            }
        }

        String parameterDeclaration = "java.util.Collection bookingTypes, java.util.Collection interimAccounts, de.smava.webapp.account.domain.Account account";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bookingTypes", bookingTypes);
        params.put("interimAccounts", interimAccounts);
        params.put("account", account);


        StringBuilder whereClause = new StringBuilder();
        whereClause.append(accountPart + "bookingTypes.contains(_integerType) && interimAccounts.contains(_transaction._creditAccount) && _transaction._state == '" + transactionState + "'" + whereClauseExtension);

        Query q = getPersistenceManager().newQuery(Booking.class, whereClause.toString());

        q.declareParameters(parameterDeclaration);

        q.setResult("sum(_amount)");

        Double sum1 = (Double) q.executeWithMap(params);
        sum1 = (sum1 == null) ? 0.0 : sum1;


        whereClause = new StringBuilder();
        whereClause.append(accountPart + "bookingTypes.contains(_integerType) && interimAccounts.contains(_transaction._debitAccount) && _transaction._state == '" + transactionState + "'" + whereClauseExtension);

        q = getPersistenceManager().newQuery(Booking.class, whereClause.toString());
        q.declareParameters(parameterDeclaration);
        q.setResult("sum(_amount)");

        Double sum2 = (Double) q.executeWithMap(params);
        sum2 = (sum2 == null) ? 0.0 : sum2;

        result = sum2 - sum1;

        return result;
    }



    @Override
    public double getAgioSum(Collection<String> transactionStates, Collection<BankAccount> debitAccounts, Collection<BankAccount> creditAccounts) {
        return getBookingSumBetweenTowAccounts(transactionStates, debitAccounts, creditAccounts, BookingType.LENDER_AGIO, null, null);
    }

    @Override
    public double getBaseProvisionSum(Collection<String> transactionStates, Collection<BankAccount> debitAccounts, Collection<BankAccount> creditAccounts) {
        return getBookingSumBetweenTowAccounts(transactionStates, debitAccounts, creditAccounts, BookingType.LENDER_BASE_PROVISION, null, null);
    }

    @Override
    public double getBookingSumForTransactions(final BookingType bookingType, List<Transaction> transactions) {
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        int paramIndex = 1;
        params.put(paramIndex, bookingType.getDbValue());
        StringBuilder sql = new StringBuilder();
        sql.append("select sum(b.amount) from booking b where b.type_new = ? and b.transaction_id in ( ");
        boolean first = true;
        for (Transaction t : transactions) {
            if (!first) {
                sql.append(",");
            } else {
                first = false;
            }
            sql.append("?");

            params.put(++paramIndex, t.getId());
        }
        sql.append(" )");

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setResultClass(Double.class);
        q.setUnique(true);

        return (Double) q.executeWithMap(params);
    }



    private double getBookingSumBetweenTowAccounts(final Collection<String> transactionStates, final Collection<BankAccount> debitAccounts, final Collection<BankAccount> creditAccounts, final BookingType bookingType, Date date, Long dtausFileId) {
        final StringBuilder queryString = new StringBuilder("select sum(b.amount) ");
        queryString.append("from booking b, transaction t ");
        queryString.append("where ");
        queryString.append(" t.id = b.transaction_id ");
        int paramCount = 1;
        final Map<Integer, Object> params = new HashMap<Integer, Object>(transactionStates.size() + 3);
        if (transactionStates.size() == 1) {
            queryString.append(" and t.state = ? ");
            params.put(paramCount++, transactionStates.iterator().next());
        } else {
            queryString.append(" and t.state in (");
            final String delimiter = ", ";
            for (String state : transactionStates) {
                if (paramCount > 1) {
                    queryString.append(delimiter);
                }
                queryString.append("?");
                params.put(paramCount++, state);
            }
            queryString.append(") ");
        }
        if (creditAccounts != null && !creditAccounts.isEmpty()) {
            if (creditAccounts.size() == 1) {
                queryString.append("and t.credit_account = ? ");
                params.put(paramCount++, creditAccounts.iterator().next().getId());
            } else {
                queryString.append("and t.credit_account in (");
                boolean firstAccount = true;
                for (BankAccount creditAccount : creditAccounts) {
                    if (!firstAccount) {
                        queryString.append(",");
                    }
                    queryString.append("?");
                    params.put(paramCount++, creditAccount.getId());
                    firstAccount=false;
                }
                queryString.append(") ");
            }
        }
        if (debitAccounts != null && !debitAccounts.isEmpty()) {
            if (debitAccounts.size() == 1) {
                queryString.append("and t.debit_account = ? ");
                params.put(paramCount++, debitAccounts.iterator().next().getId());
            } else {
                queryString.append("and t.debit_account in (");
                boolean firstAccount = true;
                for (BankAccount debitAccount : debitAccounts) {
                    if (!firstAccount) {
                        queryString.append(",");
                    }
                    queryString.append("?");
                    firstAccount=false;
                    params.put(paramCount++, debitAccount.getId());
                }
                queryString.append(") ");
            }
        }
        queryString.append(" and b.type_new = ? ");
        params.put(paramCount++, bookingType.getDbValue());

        if (date != null) {
            queryString.append(" and t.transaction_date <= ? ");
            params.put(paramCount++, new java.sql.Date(date.getTime()));
        }
        if (dtausFileId != null) {
            queryString.append(" and t.dtaus_file_id = ? ");
            params.put(paramCount++, dtausFileId);
        }

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setResultClass(Double.class);
        q.setUnique(true);

        Double result = (Double) q.executeWithMap(params);

        if (result == null) {
            result = 0.0d;
        }
        return result;
    }


    @Override
    public Map<Long, Boolean> getIsTargetMap(final BookingGroup bookingGroup, final String inIdsString) {
        final Map<Long, Boolean> result;
        if (StringUtils.isNotBlank(inIdsString)) {
            final StringBuilder sql = new StringBuilder(ISTARGET_MAP_SQL_BEGIN);
            sql.append(inIdsString);
            sql.append(")");
            final Query q = getPersistenceManager().newQuery(Query.SQL, sql.toString());
            @SuppressWarnings("unchecked")
            final List<Object[]> sqlResult = (List<Object[]>) q.execute(bookingGroup.getId());
            final Map<Long, Boolean> resultMap = new HashMap<Long, Boolean>(sqlResult.size());
            for (Object[] row : sqlResult) {
                resultMap.put((Long) row[0], (Boolean) row[1]);
            }
            result = Collections.unmodifiableMap(resultMap);
        } else {
            result = Collections.emptyMap();
        }
        return result;
    }


    /**
     * Collects data for lender payout.
     * @return collection with data for payout.
     */
    @Override
    public Collection<PayoutValuesContainer> getPayoutValues(final List<BankAccount> bankAccounts,
            final List<BookingType> bookingTypes,
            final Date endDate,
            final boolean groupByAccount,
            final BankAccount interimAccount,
            final BankAccount provisionInterimBankAccount) {
        Timestamp sqlDate = new Timestamp(endDate.getTime());
        final Map<Integer, Object> params = new HashMap<Integer, Object>();
        int paramCount = 1;
        final StringBuilder queryString = new StringBuilder("select case when debit.type='KOK' then debit.id else credit.id end as \"bankAccountId\", ");
        if (!groupByAccount) {
            queryString.append("b.type_new as \"bookingType\", b.contract_id as \"contractId\", bg.id as \"bookingGroupId\", ");
        } else { // we need this because of the object structure of PayoutValuesContainer
            queryString.append("int4('-1') as \"bookingType\", int8('-1') as \"contractId\", int8('-1') as \"bookingGroupId\", ");
        }
        queryString.append("sum(case debit.id when ").append(provisionInterimBankAccount.getId()).append(" then b.amount else 0 end) as \"fromProvisionInterim\", ");
        queryString.append("sum(case credit.id when ").append(provisionInterimBankAccount.getId()).append(" then b.amount else 0 end) as \"toProvisionInterim\", ");
        queryString.append("sum(case debit.id when ").append(interimAccount.getId()).append(" then b.amount else 0 end) as \"fromInterim\", ");
        queryString.append("sum(case credit.id when ").append(interimAccount.getId()).append(" then b.amount else 0 end) as \"toInterim\", ");
        queryString.append("sum(case debit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) as \"fromRef\", ");
        queryString.append("sum(case t.state when 'SUCCESSFUL' then (case credit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) else 0 end) as \"toRefSuccessful\", ");
        queryString.append("sum(case t.state when 'OPEN' then (case credit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) else 0 end) as \"toRefOpen\", ");
        queryString.append("sum(case debit.id when ").append(interimAccount.getId()).append(" then b.amount else 0 end) ");
        queryString.append("-sum(case credit.id when ").append(interimAccount.getId()).append(" then b.amount else 0 end) ");
        queryString.append("+sum(case debit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) ");
        queryString.append("-sum(case credit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) ");
        queryString.append("-sum(case credit.id when ").append(provisionInterimBankAccount.getId()).append(" then b.amount else 0 end) ");
        queryString.append("as difference ");
        queryString.append("from transaction t, bank_account credit, bank_account debit, booking b left outer join booking_assignment ba on b.id = ba.booking left outer join booking_group bg on bg.id = ba.booking_group ");

        queryString.append("where b.transaction_id = t.id ");

        if (bankAccounts != null && !bankAccounts.isEmpty()) {
            queryString.append("and (");
//			credit.id in (x, y, z)
            queryString.append("credit.id in (");
            paramCount = createBankAccounts(bankAccounts, params, paramCount, queryString);
            queryString.append(")");
//			 or debit.id in (x, y, z)
            queryString.append(" or debit.id in (");
            paramCount = createBankAccounts(bankAccounts, params, paramCount, queryString);
            queryString.append(")");
            queryString.append(") ");
        }

        queryString.append("and (credit.type = 'KOK' or debit.type = 'KOK') ");
    
        queryString.append("and t.debit_account = debit.id ");
        queryString.append("and t.credit_account = credit.id ");
        queryString.append("and (");
        queryString.append("(t.state = 'SUCCESSFUL' and t.transaction_date <= ?) ");

        params.put(paramCount++, sqlDate);

        queryString.append("or (t.state = 'OPEN' and debit.type = 'KOK' and t.\"dueDate\" <= ?)");

        params.put(paramCount++, sqlDate);

        queryString.append(") ");

        queryString.append("and b.type_new in (");

        createBookingTypes(bookingTypes, params, paramCount, queryString);

        queryString.append(") ");

        queryString.append("group by \"bankAccountId\" ");

        if (!groupByAccount) {
            queryString.append(", \"bookingType\", \"contractId\", \"bookingGroupId\" ");
            queryString.append("having round((");
            queryString.append("sum(case debit.id when ").append(interimAccount.getId()).append(" then b.amount else 0 end) ");
            queryString.append("-sum(case credit.id when ").append(interimAccount.getId()).append(" then b.amount else 0 end) ");
            queryString.append("+sum(case debit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) ");
            queryString.append("-sum(case credit.type when 'reference account' then b.amount when 'other account' then b.amount else 0 end) ");
            queryString.append("-sum(case credit.id when ").append(provisionInterimBankAccount.getId()).append(" then b.amount else 0 end)");
            queryString.append(") * 10000000) != 0 ");
        }
        queryString.append("order by \"bankAccountId\" ");

        if (!groupByAccount) {
            queryString.append(", \"bookingType\", \"contractId\", \"bookingGroupId\"");
        }

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", queryString.toString());
        q.setResultClass(PayoutValuesContainer.class);

        long start = System.nanoTime();
        @SuppressWarnings("unchecked")
        Collection<PayoutValuesContainer> result = (Collection<PayoutValuesContainer>) q.executeWithMap(params);
        LOGGER.info("getPayoutValues: got " + result.size() + " rows in " + ((System.nanoTime() - start) / NumericalData.NANO_TO_MILLI) + " ms. from DB");

        return result;
    }

    private void createBookingTypes(final List<BookingType> bookingTypes,
            final Map<Integer, Object> params, int paramCount,
            final StringBuilder queryString) {
        short count = 0;
        String delimiter = "";
        for (BookingType bookingType : bookingTypes) {
            if (count == 1) {
                delimiter = ", ";
            }
            queryString.append(delimiter);
            queryString.append("?");

            params.put(paramCount++, bookingType.getDbValue());
            count++;
        }
    }

    private int createBankAccounts(final List<BankAccount> bankAccounts,
            final Map<Integer, Object> params, int paramCount,
            final StringBuilder queryString) {
        short count = 0;
        String delimiter = "";
        for (BankAccount internalBankAccount : bankAccounts) {
            if (count == 1) {
                delimiter = ", ";
            }
            queryString.append(delimiter);
            queryString.append("?");
            params.put(paramCount++, internalBankAccount.getId());
            count++;
        }

        return paramCount;
    }

    @Override
    public double getBookingSumBetweenAccountsAndTypes(BookingSumParameters parameters) {
        final BankAccountTransferDirection transferDirection = parameters.getTransferDirection();
        final Set<String> transactionStates = parameters.getTransactionStates();
        final Set<BookingType> bookingTypes = parameters.getBookingTypes();
        final Set<String> contractStates = parameters.getContractStates();
        final Boolean targetOnly = parameters.getTargetOnly();
        final Set<String> bookingGroupTypes = parameters.getBookingGroupTypes();
        final Period period = parameters.getPeriod();
        final Set<Order> orders = parameters.getOrders();

        Assert.notNull(transferDirection);

        final String sql = getSumSql(bookingTypes, transactionStates, bookingGroupTypes, targetOnly, period, contractStates, orders);
        final Map<String, Object> params = new HashMap<String, Object>(9);
        params.put("debit", transferDirection.getDebit().getId());
        params.put("credit", transferDirection.getCredit().getId());
        params.put("transactionStates", transactionStates);
        params.put("bookingTypes", bookingTypes);
        params.put("contractStates", contractStates);
        params.put("bookingGroupTypes", bookingGroupTypes);
        params.put("targetOnly", targetOnly);
        if (period != null) {
            params.put("start", new Timestamp(period.getStart().getTime()));
            params.put("end", new Timestamp(period.getEnd().getTime()));
        }
        params.put("orders", orders);
        final String replacedSql = replaceCollectionTypes(params, sql);
        final Query q = getPersistenceManager().newQuery(Query.SQL, replacedSql);
        q.setUnique(true);
        final Double result = (Double) q.executeWithMap(params);

        return result == null ? 0.0 : result;
    }

    private String getSumSql(final Set<BookingType> bookingTypes,
            final Set<String> transactionStates,
            final Set<String> bookingGroupTypes,
            final Boolean targetOnly,
            final Period period,
            final Set<String> contractStates,
            final Set<Order> orders) {
        final boolean bookingTypesPresent = CollectionUtils.isNotEmpty(bookingTypes);
        final boolean transactionStatesPresent = CollectionUtils.isNotEmpty(transactionStates);
        final boolean bookingGroupTypesPresent = CollectionUtils.isNotEmpty(bookingGroupTypes);
        final boolean taragetOnlyPresent = targetOnly != null;
        final boolean periodPresent = period != null;
        final boolean contractStatesPresent = CollectionUtils.isNotEmpty(contractStates);
        final boolean ordersPresent = CollectionUtils.isNotEmpty(orders);
        final StringBuilder sql = new StringBuilder("select sum(b.amount) from transaction t inner join booking b on b.transaction_id = t.id ");
        if (contractStatesPresent || ordersPresent) {
            sql.append("inner join contract c on b.contract_id = c.id ");
        }
        if (taragetOnlyPresent || bookingGroupTypesPresent) {
            sql.append("inner join booking_assignment ba on ba.booking = b.id ");
        }
        if (bookingGroupTypesPresent) {
            sql.append("inner join booking_group bg on bg.id = ba.booking_group ");
        }
        sql.append("where t.debit_account = :debit and t.credit_account = :credit ");
        if (transactionStatesPresent) {
            sql.append("and t.state in (:transactionStates) ");
        }
        if (periodPresent) {
            sql.append("and t.\"dueDate\" between :start and :end ");
        }
        if (bookingTypesPresent) {
            sql.append("and b.type_new in (:bookingTypes) ");
        }
        if (contractStatesPresent) {
            sql.append("and c.state in (:contractStates) ");
        }
        if (taragetOnlyPresent) {
            sql.append("and ba.is_target = :targetOnly ");
        }
        if (bookingGroupTypesPresent) {
            sql.append("and bg.type in (:bookingGroupTypes) ");
        }
        if (ordersPresent) {
            sql.append("and c.order_id in (:orders) ");
        }

        return sql.toString();
    }

    @Override
    public Set<Booking> getCanceledBookingsForContract(final Contract contract, final Booking booking) {
        Assert.notNull(booking.getCancelationBookingGroup());

        Collection<Integer> bookingTypes = new ArrayList<Integer>(2);
        bookingTypes.add(BookingType.BORROWER_UNSCHED_REPAYMENT.getDbValue());
        bookingTypes.add(BookingType.BORROWER_INTEREST.getDbValue());

        StringBuilder sql = new StringBuilder("select b.id as id ");
        sql.append("from booking b ");
        sql.append("inner join booking_assignment ba on ba.booking = b.id ");
        sql.append("inner join transaction t on t.id = b.transaction_id ");
        sql.append("where ");
        sql.append("b.contract_id = :contractId ");
        sql.append("and b.type_new in (:bookingTypes) ");
        sql.append("and ba.booking_group = :bookingGroupId ");
        sql.append("and t.state = 'SUCCESSFUL' ");
        sql.append("and t.type = 'TRANSFER' ");
        sql.append("and t.debit_account in (:debit) ");
        sql.append("and t.credit_account in (:credit)");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("contractId", contract.getId());
        params.put("bookingTypes", bookingTypes);
        params.put("bookingGroupId", booking.getCancelationBookingGroup().getId());
        params.put("debit", contract.getOrder().getAccount().getReferenceBankAccount().getId());
        params.put("credit", contract.getOrder().getAccount().getInternalBankAccount(contract.getOrder().getBankAccountProvider()).getId());

        String replacedSql = replaceCollectionTypes(params, sql.toString());
        final Query q = getPersistenceManager().newQuery(Query.SQL, replacedSql);
        q.setClass(Booking.class);

        @SuppressWarnings("unchecked")
        Collection<Booking> collectionResult = (Collection<Booking>) q.executeWithMap(params);
        Set<Booking> result = new HashSet<Booking>(collectionResult);

        return result;
    }

    @Override
    public Collection<Booking> getBookingsForContracts(final String sql, final Set<Contract> contracts,
            final Collection<BookingType> bookingTypes, final Map<String, Object> params) {

        final Query q = getPersistenceManager().newQuery(Query.SQL, replaceCollectionTypes(params, sql));
        q.setClass(Booking.class);

        return (Collection<Booking>) q.executeWithMap(params);
    }


    @Override
    public Collection<Booking> getPayoutBookings(final Set<Contract> contracts, BankAccount smavaInterimBankAccount, boolean considerConsolidation) {

        if ( smavaInterimBankAccount == null) {
            return new ArrayList<Booking>();
        }
        final Map<String, Object> params = new HashMap<String, Object>();

        final Account borrower = contracts.iterator().next().getOrder().getAccount();

        if ( smavaInterimBankAccount.getProvider() != null
                && smavaInterimBankAccount.getProvider().getType().equals( BankAccountProviderType.E_MONEY)) {
            ArrayList<BankAccount> bas = new ArrayList<BankAccount>();
            bas.add(smavaInterimBankAccount);
            params.put("debit",  bas);

            List<BankAccount> creditAccounts = new ArrayList<BankAccount>();
            creditAccounts.addAll( borrower.getReferenceBankAccountHistory());
            if ( considerConsolidation){
                creditAccounts.addAll( borrower.getConsolidationBankAccountHistory());
            }
            params.put("credit", creditAccounts);
        } else{
            params.put("debit", borrower.getInternalBankAccountHistories());
            params.put("credit", borrower.getReferenceBankAccountHistory());
        }
        params.put("contracts", contracts);
        params.put("bookingType", BookingType.BORROWER_PAYOUT.getDbValue());
        params.put("transactionState", Transaction.STATE_SUCCESSFUL);


        StringBuilder sql = new StringBuilder("select distinct b.id as id from booking b inner join transaction t on b.transaction_id = t.id ");
        sql.append("where type_new = (:bookingType) and b.contract_id in (:contracts) and t.state = (:transactionState) ");
        sql.append("and t.debit_account in (:debit) ");
        sql.append("and t.credit_account in (:credit)");

        final Query q = getPersistenceManager().newQuery(Query.SQL, replaceCollectionTypes(params, sql.toString()));
        q.setClass(Booking.class);

        return (Collection<Booking>) q.executeWithMap(params);
    }



    /** {@inheritDoc} */
    @Override
     public Collection<Booking> findInvitationBonusBookings( Account account){
         return findBookingList("_integerType == " + BookingType.INVITATION_BONUS.getDbValue() + " && _transaction._creditAccount._account._id == " + account.getId() + " && (_transaction._state == '" + Transaction.STATE_OPEN + "' || _transaction._state == '" + Transaction.STATE_SUCCESSFUL + "')");
     }

    /** {@inheritDoc} */
    @Override
     public Collection<Booking> findInvitationBonusBookings( Account account, Contract contract){
         return findBookingList("_integerType == " + BookingType.INVITATION_BONUS.getDbValue() + " && _transaction._creditAccount._account._id == " + account.getId() + " && _contract != null && _contract._id == " + contract.getId() + " && (_transaction._state == '" + Transaction.STATE_OPEN + "' || _transaction._state == '" + Transaction.STATE_SUCCESSFUL + "')");
     }



    /**
     * select contract_id, booking_group, is_target
    , replace(replace(replace(replace(cast(b.type_new as varchar),'9','7'),'26','7'),'28','7'),'31','8') as bookingtype
    , (select replace(replace(type,'reference account','KOK'),'encashment interim account','KNK') from bank_account where id=(case when debit_account in (select id from bank_account where type='smava interim account in') then credit_account else debit_account end)) as gegenkto
    , (select name from bank_service_provider where id=422182850 ) as servprov
    , (case when debit_account in (select id from bank_account where type='smava interim account in') then false else true end) as to_interim
    , t.state, t.transaction_date
    , sum(case when debit_account in (select id from bank_account where type='smava interim account in') then -b.amount else b.amount end) as betrag
        from booking b, transaction t, booking_assignment bas
        where b.transaction_id=t.id and bas.booking=b.id
        and t.state in ('OPEN','SUCCESSFUL','FAILED')
        and (debit_account in (select id from bank_account where type='smava interim account in') or credit_account in (select id from bank_account where type='smava interim account in') )
        and bas.booking_group= 18594567
        group by contract_id, type_new, booking_group , gegenkto, servprov, t.state , t.transaction_date, is_target, to_interim
        order by contract_id, type_new
     * @see de.smava.webapp.account.dao.BookingDao#collectPoolPerformanceBookingInformation(de.smava.webapp.account.domain.BankAccountProvider, de.smava.webapp.account.domain.BookingGroup)
     */
    /** {@inheritDoc} */
    @Override
    public List<PoolPerformanceBookingInformation> collectPoolPerformanceBookingInformation(
            BankAccountProvider bap, BookingGroup bookingGroup) {

        if (bap != null && bookingGroup != null) {
            final StringBuilder sql = new StringBuilder(
                    "select contract_id, booking_group , is_target"
                            + ", replace(replace(replace(replace(cast(b.type_new as varchar),'9','7'),'26','7'),'28','7'),'31','8') as bookingtype "
                            + ", (case when debit_account in (select id from bank_account where type='smava interim account in') then credit_account else debit_account end) as gegenkto "
                            + ", (select type from bank_account where id=(case when debit_account in (select id from bank_account where type='smava interim account in') then credit_account else debit_account end)) as gegenkto_typ "
                            + ", t.state"
                            + ", t.transaction_date"
                            + ", (case when debit_account in (select id from bank_account where type='smava interim account in') then false else true end) as to_interim "
                            + ", sum(case when debit_account in (select id from bank_account where type='smava interim account in') then -b.amount else b.amount end) as betrag "
                            + "from booking b, transaction t, booking_assignment bas "
                            + "where b.transaction_id=t.id and bas.booking=b.id "
                            + "and t.state in ('OPEN','SUCCESSFUL','FAILED') "
                            + "and (debit_account in (select id from bank_account where type='smava interim account in' " +
                                    " and bank_service_provider_id=" + bap.getId() +
                                    ") "
                            + "or "
                            + "credit_account in (select id from bank_account where type='smava interim account in' " +
                                                    " and bank_service_provider_id=" + bap.getId() +
                                                    ") "
                            + ") "
                            + "and bas.booking_group = "
                            + bookingGroup.getId()
                            + "group by contract_id, type_new, booking_group "
                            + ", gegenkto, gegenkto_typ, t.state , t.transaction_date, is_target, to_interim "
                            + "order by contract_id, type_new");

            final Query q = getPersistenceManager().newQuery(Query.SQL,
                    sql.toString());
            final List<Object[]> sqlResult = (List<Object[]>) q.execute();
            final List<PoolPerformanceBookingInformation> result = new ArrayList<Booking.PoolPerformanceBookingInformation>(
                    sqlResult.size());
            for (Object[] row : sqlResult) {
                String state = (String) row[6];
                Date trxDate = null;
                if (row[7] != null) {
                    trxDate = (Date) row[7];
                }
                long contractId = (Long) row[0];
                boolean isTraget = (Boolean) row[2];
                int type = Integer.parseInt((String) row[3]);
                boolean tointerim =(Boolean) row[8];
                double amount = (Double) row[9];
                long lenderBankAccount = 0;

                String baType = (String) row[5];
                if ( Transaction.STATE_OPEN.equals(state)
                        && BankAccount.TYPE_LENDER_ACCOUNT.equals(baType)
                        && bap.getName().equals( BankAccountProvider.FIDOR)){
                    lenderBankAccount = (Long) row[4];
                }

                result.add(new PoolPerformanceBookingInformation(
                        state.equals(Transaction.STATE_OPEN),
                        state.equals(Transaction.STATE_SUCCESSFUL),
                        contractId,
                        BookingType.enumFromDbValue(type),
                        trxDate,
                        amount,
                        isTraget,
                        tointerim,
                        lenderBankAccount));
            }
            return result;
        }
        return new ArrayList<Booking.PoolPerformanceBookingInformation>();
    }


    public boolean hasRelevantInvitationTransaction(Account invitedAccount) {
        String whereClause = "_integerType == " + BookingType.INVITATION_BONUS.getDbValue() + " && _transaction._creditAccount._account._id == " + invitedAccount.getId() + " && (_transaction._state == '" + Transaction.STATE_OPEN + "' || _transaction._state == '" + Transaction.STATE_SUCCESSFUL + "')";

        Collection<Booking> invitationBookings = findBookingList(whereClause);
        for (Booking booking : invitationBookings) {
            if (booking.getTransaction() != null && booking.getTransaction().getCreditAccount() != null) {
                Assert.isTrue(booking.getTransaction().getCreditAccount().getAccount().equals(invitedAccount),
                        "found account = " + booking.getTransaction().getCreditAccount().getAccount().getId() + "; expected account = " + invitedAccount.getId());
            }
        }

        return invitationBookings.size() > 0;
    }

    public Collection<Booking> findCurrentLenderPayoutAffectedBookings(Collection<Transaction> trxs) {
        String trxClause = "";
        for ( Transaction trx: trxs) {
            if ( StringUtils.isEmpty(trxClause.trim())) {
                trxClause = "_transaction._id ==" + trx.getId();
            }else{
                trxClause += "  || _transaction._id == " + trx.getId();
            }
        }
        if ( trxs.size() > 0) {
            return findBookingList( trxClause);
        }

        return new ArrayList<Booking>();
    }


    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
