package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.RepaymentBreakEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;

/**
 * The domain object that represents 'RepaymentBreaks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractRepaymentBreak
    extends KreditPrivatEntity implements RepaymentBreakEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRepaymentBreak.class);

    public List<? extends Entity> getModifier() {
        return Collections.singletonList(getAccount());
    }

}

