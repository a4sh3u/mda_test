package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Booking;
import de.smava.webapp.account.domain.BookingAssignment;
import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.interfaces.BookingAssignmentEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'BookingAssignments'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractBookingAssignment
    extends KreditPrivatEntity implements BookingAssignmentEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBookingAssignment.class);

    public static BookingAssignment connectSimple(BookingGroup bookingGroup, Booking booking, boolean isTarget) {
        BookingAssignment assignment = new BookingAssignment(bookingGroup, booking, isTarget);

        return assignment;
    }

    public static boolean isAssignable(Booking booking, BookingAssignment bookingAssignment, BookingGroup bookingGroup) {
        boolean result = true;
        if (booking.getId() != null && bookingGroup.getId() != null) {
            boolean sameContent = false;
            for (BookingAssignment ba : booking.getBookingAssignments()) {
                sameContent = ((ba.getBooking() == null && bookingAssignment.getBooking() == null) || (ba.getBooking() != null && ba.getBooking().equals(bookingAssignment.getBooking())))
                        && ((ba.getBookingGroup() == null && bookingAssignment.getBookingGroup() == null) || (ba.getBookingGroup() != null && ba.getBookingGroup().equals(bookingAssignment.getBookingGroup())));
                if (sameContent) {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    public static boolean isAssignable(Booking booking, BookingGroup bookingGroup) {
        boolean result = true;
        if (booking.getId() != null && bookingGroup.getId() != null) {
            boolean sameContent = false;
            for (BookingAssignment ba : booking.getBookingAssignments()) {
                if (ba.getBookingGroup().equals(bookingGroup)) {
                    sameContent = true;
                }

                if (sameContent) {
                    result = false;
                    break;
                }
            }
        }

        return result;
    }

    public static void disconnect(Booking booking, Long bookingAssigmentId) {
        BookingAssignment bookingAssigment = null;
        for (BookingAssignment ba : booking.getBookingAssignments()) {
            if (ba.getId().equals(bookingAssigmentId)) {
                bookingAssigment = ba;
                break;
            }
        }

        if (bookingAssigment != null) {
            BookingGroup bookingGroup = bookingAssigment.getBookingGroup();
            booking.getBookingAssignments().remove(bookingAssigment);
//            bookingGroup.getBookingAssignments().remove(bookingAssigment);
        }
    }

    /**
     * disconnect all booking assignments from a booking.
     * @param booking the booking to disconnect the assignments from.
     * @return set of booking assignment ids.
     */
    public static Set<Long> disconnectAll(Booking booking) {
        Set<Long> disconnectedIds = new HashSet<Long>(booking.getBookingAssignments().size());
        Collection<BookingAssignment> temporaryAssignments = new ArrayList<BookingAssignment>();
        temporaryAssignments.addAll(booking.getBookingAssignments());

        for (BookingAssignment assignment : temporaryAssignments) {
            if (assignment != null
                    && assignment.getBooking() != null
                    && assignment.getBooking().equals(booking)) {
                booking.getBookingAssignments().remove(assignment);
                disconnectedIds.add(assignment.getId());
            }
        }

        return disconnectedIds;
    }


    private static void removeBooking(Booking booking, BookingGroup bookingGroup, Collection<BookingAssignment> bookingAssignments) {
        int i = 0;
        int toRemoveIndex = -1;
        for (BookingAssignment assignment : bookingAssignments) {
            if (assignment != null
                    && assignment.getBookingGroup() != null
                    && assignment.getBooking() != null
                    && assignment.getBookingGroup().equals(bookingGroup)
                    && assignment.getBooking().equals(booking)) {
                toRemoveIndex = i;
                break;
            }
            i++;
        }
        if (toRemoveIndex >= 0) {
            bookingAssignments.remove(toRemoveIndex);
        }
    }

    public static List<BookingGroup> getBookingGroups(Booking booking) {
        List<BookingGroup> bookingGroups = new ArrayList<BookingGroup>();
        for (BookingAssignment assignment : booking.getBookingAssignments()) {
            if (assignment.getBookingGroup() != null) {
                bookingGroups.add(assignment.getBookingGroup());
            } else {
                LOGGER.error("BookingAssignment " + assignment.getId() + " of " + booking.getId() + " without BookingGroup!");
            }
        }
        return bookingGroups;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

