package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Person;

import java.util.Date;


/**
 * The domain object that represents 'IdCards'.
 *
 * @author generator
 */
public interface IdCardEntityInterface {

    /**
     * Setter for the property 'bkz'.
     *
     * 
     *
     */
    void setBkz(String bkz);

    /**
     * Returns the property 'bkz'.
     *
     * 
     *
     */
    String getBkz();
    /**
     * Setter for the property 'issue date'.
     *
     * 
     *
     */
    void setIssueDate(Date issueDate);

    /**
     * Returns the property 'issue date'.
     *
     * 
     *
     */
    Date getIssueDate();
    /**
     * Setter for the property 'expiry date'.
     *
     * 
     *
     */
    void setExpiryDate(Date expiryDate);

    /**
     * Returns the property 'expiry date'.
     *
     * 
     *
     */
    Date getExpiryDate();
    /**
     * Setter for the property 'bkz check'.
     *
     * 
     *
     */
    void setBkzCheck(Date bkzCheck);

    /**
     * Returns the property 'bkz check'.
     *
     * 
     *
     */
    Date getBkzCheck();
    /**
     * Setter for the property 'checksum check'.
     *
     * 
     *
     */
    void setChecksumCheck(Date checksumCheck);

    /**
     * Returns the property 'checksum check'.
     *
     * 
     *
     */
    Date getChecksumCheck();
    /**
     * Setter for the property 'birthdate check'.
     *
     * 
     *
     */
    void setBirthdateCheck(Date birthdateCheck);

    /**
     * Returns the property 'birthdate check'.
     *
     * 
     *
     */
    Date getBirthdateCheck();
    /**
     * Setter for the property 'expiry date check'.
     *
     * 
     *
     */
    void setExpiryDateCheck(Date expiryDateCheck);

    /**
     * Returns the property 'expiry date check'.
     *
     * 
     *
     */
    Date getExpiryDateCheck();
    /**
     * Setter for the property 'address check'.
     *
     * 
     *
     */
    void setAddressCheck(Date addressCheck);

    /**
     * Returns the property 'address check'.
     *
     * 
     *
     */
    Date getAddressCheck();
    /**
     * Setter for the property 'validity date'.
     *
     * 
     *
     */
    void setValidityDate(Date validityDate);

    /**
     * Returns the property 'validity date'.
     *
     * 
     *
     */
    Date getValidityDate();
    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'skipped date'.
     *
     * 
     *
     */
    void setSkippedDate(Date skippedDate);

    /**
     * Returns the property 'skipped date'.
     *
     * 
     *
     */
    Date getSkippedDate();
    /**
     * Setter for the property 'person'.
     *
     * 
     *
     */
    void setPerson(Person person);

    /**
     * Returns the property 'person'.
     *
     * 
     *
     */
    Person getPerson();
    /**
     * Setter for the property 'birthday check idcard'.
     *
     * 
     *
     */
    void setBirthdayCheckIdcard(Date birthdayCheckIdcard);

    /**
     * Returns the property 'birthday check idcard'.
     *
     * 
     *
     */
    Date getBirthdayCheckIdcard();
    /**
     * Setter for the property 'descriptions'.
     *
     * 
     *
     */
    void setDescriptions(String descriptions);

    /**
     * Returns the property 'descriptions'.
     *
     * 
     *
     */
    String getDescriptions();

}
