package de.smava.webapp.account.domain;

import java.util.LinkedList;
import java.util.List;

import de.smava.webapp.commons.domain.BaseEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'CreditWorthinessCalculations'.
 */
public class CreditWorthinessCalculation implements BaseEntity {

    private static final Logger LOGGER = Logger.getLogger(CreditWorthinessCalculation.class);

    private Long _id;

    private java.util.Date _date;
    private String _sessionId;
    private String _idAddress;
    private Long _placementId;
    private Boolean _otherIncome;
    private String _otherEarnDescription;
    private String _income;
    private String _receivedPalimony;
    private String _miscEarn1;
    private String _miscEarnAmount1;
    private String _receivedRent;
    private String _modeOfAccomodation;
    private String _paidAlimony;
    private String _paidRent;
    private String _expensesCar;
    private String _numOfCars;
    private Boolean _paidAlimonyRadio;
    private String _leasing;
    private Boolean _leasingRadio;
    private String _creditExpenses;
    private Boolean _creditExpensesRadio;
    private String _savings;
    private Boolean _savingsRadio;
    private String _numberOfChildren;
    private String _numberOfOtherInHousehold;
    private String _insurance;
    private String _privateHealthInsurance;
    private Boolean _privateHealthInsuranceRadio;
    private String _mortgage;
    private Boolean _mortgageRadio;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    @Override
    public boolean isNew() {
        return this._id == null;
    }

    /**
     * Setter for the property 'date'.
     */
    public void setDate(java.util.Date date) {
        _date = date;
    }

    /**
     * Returns the property 'date'.
     */
    public java.util.Date getDate() {
        return _date;
    }
	    /**
     * Setter for the property 'session id'.
     */
    public void setSessionId(String sessionId) {
        _sessionId = sessionId;
    }

    /**
     * Returns the property 'session id'.
     */
    public String getSessionId() {
        return _sessionId;
    }
	    /**
     * Setter for the property 'id address'.
     */
    public void setIdAddress(String idAddress) {
        _idAddress = idAddress;
    }

    /**
     * Returns the property 'id address'.
     */
    public String getIdAddress() {
        return _idAddress;
    }
	    /**
     * Setter for the property 'placement id'.
     */
    public void setPlacementId(Long placementId) {
        _placementId = placementId;
    }

    /**
     * Returns the property 'placement id'.
     */
    public Long getPlacementId() {
        return _placementId;
    }
	    /**
     * Setter for the property 'other income'.
     */
    public void setOtherIncome(Boolean otherIncome) {
        _otherIncome = otherIncome;
    }

    /**
     * Returns the property 'other income'.
     */
    public Boolean getOtherIncome() {
        return _otherIncome;
    }
	    /**
     * Setter for the property 'other earn description'.
     */
    public void setOtherEarnDescription(String otherEarnDescription) {
        _otherEarnDescription = otherEarnDescription;
    }

    /**
     * Returns the property 'other earn description'.
     */
    public String getOtherEarnDescription() {
        return _otherEarnDescription;
    }
	    /**
     * Setter for the property 'income'.
     */
    public void setIncome(String income) {
        _income = income;
    }

    /**
     * Returns the property 'income'.
     */
    public String getIncome() {
        return _income;
    }
	    /**
     * Setter for the property 'received palimony'.
     */
    public void setReceivedPalimony(String receivedPalimony) {
        _receivedPalimony = receivedPalimony;
    }

    /**
     * Returns the property 'received palimony'.
     */
    public String getReceivedPalimony() {
        return _receivedPalimony;
    }
	    /**
     * Setter for the property 'misc earn 1'.
     */
    public void setMiscEarn1(String miscEarn1) {
        _miscEarn1 = miscEarn1;
    }

    /**
     * Returns the property 'misc earn 1'.
     */
    public String getMiscEarn1() {
        return _miscEarn1;
    }
	    /**
     * Setter for the property 'misc earn amount 1'.
     */
    public void setMiscEarnAmount1(String miscEarnAmount1) {
        _miscEarnAmount1 = miscEarnAmount1;
    }

    /**
     * Returns the property 'misc earn amount 1'.
     */
    public String getMiscEarnAmount1() {
        return _miscEarnAmount1;
    }
	    /**
     * Setter for the property 'received rent'.
     */
    public void setReceivedRent(String receivedRent) {
        _receivedRent = receivedRent;
    }

    /**
     * Returns the property 'received rent'.
     */
    public String getReceivedRent() {
        return _receivedRent;
    }
	    /**
     * Setter for the property 'mode of accomodation'.
     */
    public void setModeOfAccomodation(String modeOfAccomodation) {
        _modeOfAccomodation = modeOfAccomodation;
    }

    /**
     * Returns the property 'mode of accomodation'.
     */
    public String getModeOfAccomodation() {
        return _modeOfAccomodation;
    }
	    /**
     * Setter for the property 'paid alimony'.
     */
    public void setPaidAlimony(String paidAlimony) {
        _paidAlimony = paidAlimony;
    }

    /**
     * Returns the property 'paid alimony'.
     */
    public String getPaidAlimony() {
        return _paidAlimony;
    }
	    /**
     * Setter for the property 'paid rent'.
     */
    public void setPaidRent(String paidRent) {
        _paidRent = paidRent;
    }

    /**
     * Returns the property 'paid rent'.
     */
    public String getPaidRent() {
        return _paidRent;
    }
	    /**
     * Setter for the property 'expenses car'.
     */
    public void setExpensesCar(String expensesCar) {
        _expensesCar = expensesCar;
    }

    /**
     * Returns the property 'expenses car'.
     */
    public String getExpensesCar() {
        return _expensesCar;
    }
	    /**
     * Setter for the property 'num of cars'.
     */
    public void setNumOfCars(String numOfCars) {
        _numOfCars = numOfCars;
    }

    /**
     * Returns the property 'num of cars'.
     */
    public String getNumOfCars() {
        return _numOfCars;
    }
	 
	    /**
     * Setter for the property 'misc expenses radio'.
     */
    public void setPaidAlimonyRadio(Boolean paidAlimonyRadio) {
        _paidAlimonyRadio = paidAlimonyRadio;
    }

    /**
     * Returns the property 'misc expenses radio'.
     */
    public Boolean getPaidAlimonyRadio() {
        return _paidAlimonyRadio;
    }
	    /**
     * Setter for the property 'leasing'.
     */
    public void setLeasing(String leasing) {
        _leasing = leasing;
    }

    /**
     * Returns the property 'leasing'.
     */
    public String getLeasing() {
        return _leasing;
    }
	    /**
     * Setter for the property 'leasing radio'.
     */
    public void setLeasingRadio(Boolean leasingRadio) {
        _leasingRadio = leasingRadio;
    }

    /**
     * Returns the property 'leasing radio'.
     */
    public Boolean getLeasingRadio() {
        return _leasingRadio;
    }
	    /**
     * Setter for the property 'credit expenses'.
     */
    public void setCreditExpenses(String creditExpenses) {
        _creditExpenses = creditExpenses;
    }

    /**
     * Returns the property 'credit expenses'.
     */
    public String getCreditExpenses() {
        return _creditExpenses;
    }
	    /**
     * Setter for the property 'credit expenses radio'.
     */
    public void setCreditExpensesRadio(Boolean creditExpensesRadio) {
        _creditExpensesRadio = creditExpensesRadio;
    }

    /**
     * Returns the property 'credit expenses radio'.
     */
    public Boolean getCreditExpensesRadio() {
        return _creditExpensesRadio;
    }
	    /**
     * Setter for the property 'savings'.
     */
    public void setSavings(String savings) {
        _savings = savings;
    }

    /**
     * Returns the property 'savings'.
     */
    public String getSavings() {
        return _savings;
    }
	    /**
     * Setter for the property 'savings radio'.
     */
    public void setSavingsRadio(Boolean savingsRadio) {
        _savingsRadio = savingsRadio;
    }

    /**
     * Returns the property 'savings radio'.
     */
    public Boolean getSavingsRadio() {
        return _savingsRadio;
    }
	    /**
     * Setter for the property 'number of children'.
     */
    public void setNumberOfChildren(String numberOfChildren) {
        _numberOfChildren = numberOfChildren;
    }

    /**
     * Returns the property 'number of children'.
     */
    public String getNumberOfChildren() {
        return _numberOfChildren;
    }
	    /**
     * Setter for the property 'number of other in household'.
     */
    public void setNumberOfOtherInHousehold(String numberOfOtherInHousehold) {
        _numberOfOtherInHousehold = numberOfOtherInHousehold;
    }

    /**
     * Returns the property 'number of other in household'.
     */
    public String getNumberOfOtherInHousehold() {
        return _numberOfOtherInHousehold;
    }
	    /**
     * Setter for the property 'insurance'.
     */
    public void setInsurance(String insurance) {
        _insurance = insurance;
    }

    /**
     * Returns the property 'insurance'.
     */
    public String getInsurance() {
        return _insurance;
    }
	    /**
     * Setter for the property 'private health insurance'.
     */
    public void setPrivateHealthInsurance(String privateHealthInsurance) {
        _privateHealthInsurance = privateHealthInsurance;
    }

    /**
     * Returns the property 'private health insurance'.
     */
    public String getPrivateHealthInsurance() {
        return _privateHealthInsurance;
    }
	    /**
     * Setter for the property 'private health insurance radio'.
     */
    public void setPrivateHealthInsuranceRadio(Boolean privateHealthInsuranceRadio) {
        _privateHealthInsuranceRadio = privateHealthInsuranceRadio;
    }

    /**
     * Returns the property 'private health insurance radio'.
     */
    public Boolean getPrivateHealthInsuranceRadio() {
        return _privateHealthInsuranceRadio;
    }
	    /**
     * Setter for the property 'loan related savings'.
     */
    public void setMortgage(String mortgage) {
        _mortgage = mortgage;
    }

    /**
     * Returns the property 'loan related savings'.
     */
    public String getMortgage() {
        return _mortgage;
    }
	    /**
     * Setter for the property 'loan related savings radio'.
     */
    public void setMortgageRadio(Boolean mortgageRadio) {
        _mortgageRadio = mortgageRadio;
    }

    /**
     * Returns the property 'loan related savings radio'.
     */
    public Boolean getMortgageRadio() {
        return _mortgageRadio;
    }

    /**
     * Return a list of accounts that changed values of the entity.
     *
     * @return A list of accounts that can be empty but must never be null.
     */
    public List<Account> getModifier() {
        return new LinkedList<Account>();
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CreditWorthinessCalculation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _sessionId=").append(_sessionId);
            builder.append("\n    _idAddress=").append(_idAddress);
            builder.append("\n    _otherEarnDescription=").append(_otherEarnDescription);
            builder.append("\n    _income=").append(_income);
            builder.append("\n    _receivedPalimony=").append(_receivedPalimony);
            builder.append("\n    _miscEarn1=").append(_miscEarn1);
            builder.append("\n    _miscEarnAmount1=").append(_miscEarnAmount1);
            builder.append("\n    _receivedRent=").append(_receivedRent);
            builder.append("\n    _modeOfAccomodation=").append(_modeOfAccomodation);
            builder.append("\n    _paidAlimony=").append(_paidAlimony);
            builder.append("\n    _paidRent=").append(_paidRent);
            builder.append("\n    _expensesCar=").append(_expensesCar);
            builder.append("\n    _numOfCars=").append(_numOfCars);
            builder.append("\n    _leasing=").append(_leasing);
            builder.append("\n    _creditExpenses=").append(_creditExpenses);
            builder.append("\n    _savings=").append(_savings);
            builder.append("\n    _numberOfChildren=").append(_numberOfChildren);
            builder.append("\n    _numberOfOtherInHousehold=").append(_numberOfOtherInHousehold);
            builder.append("\n    _insurance=").append(_insurance);
            builder.append("\n    _privateHealthInsurance=").append(_privateHealthInsurance);
            builder.append("\n    _mortgage=").append(_mortgage);
            builder.append("\n}");
        } else {
            builder.append(CreditWorthinessCalculation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }
}
