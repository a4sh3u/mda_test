package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractRepaymentBreak;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'RepaymentBreaks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RepaymentBreakHistory extends AbstractRepaymentBreak {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
					
}
