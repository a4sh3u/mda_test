package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.MatchSortingRunEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'MatchSortingRuns'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractMatchSortingRun
    extends KreditPrivatEntity implements MatchSortingRunEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMatchSortingRun.class);

    private static final long serialVersionUID = 3967568216374622276L;

}

