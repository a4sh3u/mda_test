package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.Order;


/**
 * The domain object that represents 'CategoryOrders'.
 *
 * @author generator
 */
public interface CategoryOrderEntityInterface {

    /**
     * Setter for the property 'main'.
     *
     * 
     *
     */
    void setMain(boolean main);

    /**
     * Returns the property 'main'.
     *
     * 
     *
     */
    boolean getMain();
    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    void setCategory(Category category);

    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    Category getCategory();
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    void setOrder(Order order);

    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    Order getOrder();

}
