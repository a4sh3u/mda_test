//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(config value)}
import de.smava.webapp.account.domain.history.ConfigValueHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ConfigValues'.
 *
 * @author generator
 */
public class ConfigValue extends ConfigValueHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(config value)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _group;
        protected String _key;
        protected String _value;
        
                            /**
     * Setter for the property 'group'.
     */
    public void setGroup(String group) {
        if (!_groupIsSet) {
            _groupIsSet = true;
            _groupInitVal = getGroup();
        }
        registerChange("group", _groupInitVal, group);
        _group = group;
    }
                        
    /**
     * Returns the property 'group'.
     */
    public String getGroup() {
        return _group;
    }
                                    /**
     * Setter for the property 'key'.
     */
    public void setKey(String key) {
        if (!_keyIsSet) {
            _keyIsSet = true;
            _keyInitVal = getKey();
        }
        registerChange("key", _keyInitVal, key);
        _key = key;
    }
                        
    /**
     * Returns the property 'key'.
     */
    public String getKey() {
        return _key;
    }
                                    /**
     * Setter for the property 'value'.
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     */
    public String getValue() {
        return _value;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ConfigValue.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _group=").append(_group);
            builder.append("\n    _key=").append(_key);
            builder.append("\n    _value=").append(_value);
            builder.append("\n}");
        } else {
            builder.append(ConfigValue.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ConfigValue asConfigValue() {
        return this;
    }
}
