//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.backoffice.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bo search)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.backoffice.dao.BoSearchDao;
import de.smava.webapp.backoffice.domain.BoSearch;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;


import java.util.*;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BoSearchs'.
 *
 * @author generator
 */
@Repository(value = "boSearchDao")
public class JdoBoSearchDao extends JdoBaseDao implements BoSearchDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBoSearchDao.class);

    private static final String CLASS_NAME = "BoSearch";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bo search)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the bo search identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BoSearch load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BoSearch result = getEntity(BoSearch.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BoSearch getBoSearch(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BoSearch entity = findUniqueEntity(BoSearch.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the bo search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BoSearch boSearch) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBoSearch: " + boSearch);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bo search)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(boSearch);
    }

    /**
     * @deprecated Use {@link #save(BoSearch) instead}
     */
    public Long saveBoSearch(BoSearch boSearch) {
        return save(boSearch);
    }

    /**
     * Deletes an bo search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bo search
     */
    public void deleteBoSearch(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBoSearch: " + id);
        }
        deleteEntity(BoSearch.class, id);
    }

    /**
     * Retrieves all 'BoSearch' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BoSearch> getBoSearchList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BoSearch> result = getEntities(BoSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BoSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BoSearch> getBoSearchList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BoSearch> result = getEntities(BoSearch.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BoSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BoSearch> getBoSearchList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BoSearch> result = getEntities(BoSearch.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BoSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BoSearch> getBoSearchList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BoSearch> result = getEntities(BoSearch.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BoSearch' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BoSearch> findBoSearchList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BoSearch> result = findEntities(BoSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BoSearch' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BoSearch> findBoSearchList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BoSearch> result = findEntities(BoSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BoSearch' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BoSearch> findBoSearchList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BoSearch> result = findEntities(BoSearch.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BoSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BoSearch> findBoSearchList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BoSearch> result = findEntities(BoSearch.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BoSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BoSearch> findBoSearchList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BoSearch> result = findEntities(BoSearch.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BoSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BoSearch> findBoSearchList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BoSearch> result = findEntities(BoSearch.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BoSearch' instances.
     */
    public long getBoSearchCount() {
        long result = getEntityCount(BoSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BoSearch' instances which match the given whereClause.
     */
    public long getBoSearchCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BoSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BoSearch' instances which match the given whereClause together with params specified in object array.
     */
    public long getBoSearchCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BoSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bo search)}
    //
    // insert custom methods here


    @Override
    public Collection<BoSearch> retrieveCustomers(String email, String firstName, String lastName, String phone, Date birth, Integer offset, Integer limit, String orderBy, boolean desc) {
        LOGGER.debug("try to find accounts for names: " + firstName + ", " + lastName);
        Collection<BoSearch> result;
        if (checkSearchValues(email, firstName, lastName, phone, birth)) {
            Query query = this.getPersistenceManager().newQuery(BoSearch.class);
            List<String> andFilters = setCasiCustomersFilter(email, firstName, lastName, phone, birth);

            if (!StringUtils.isEmpty(orderBy)){
                if (desc){
                    query.setOrdering(orderBy + " DESC");
                } else {
                    query.setOrdering(orderBy +" ASC");
                }
            }


            if (offset != null && limit != null) {
                query.setRange(offset, offset + limit);
            }

            query.setFilter(and(andFilters));

            result = (Collection<BoSearch>) query.execute();

        } else {
            result = new ArrayList<BoSearch>();
        }
//      dirty hack because of duplications in result set

        return result;
    }

    public List<String> setCasiCustomersFilter(String email, String firstName, String lastName, String phone, Date birth) {
        List <String> andFilters = new ArrayList<String>();
        if (!StringUtils.isEmpty(firstName)) {
            andFilters.add(like("_firstName", firstName));
        }

        if (!StringUtils.isEmpty(lastName)) {
            andFilters.add(like("_lastName", lastName));
        }

        if (!StringUtils.isEmpty(email)) {
            andFilters.add(like("_email", email));
        }

        if (!StringUtils.isEmpty(phone)) {
            List<String> phones = new ArrayList<String>();
            phones.add(like("_phone", phone));
            phones.add(like("_phone2", phone));
            andFilters.add(or(phones));
        }
        if (birth != null ) {
            Calendar c = Calendar.getInstance();
            c.setTime(birth);
            andFilters.add(eq("_birthDate.getDay()", c.get(Calendar.DAY_OF_MONTH)));
            andFilters.add(eq("_birthDate.getMonth()", c.get(Calendar.MONTH)));
            andFilters.add(eq("_birthDate.getYear()", c.get(Calendar.YEAR)));

        }

        return andFilters;
    }

    private String and(List<String> andFilters) {
        String result = "";
        for (String filter : andFilters) {
            result = result + "(" +filter + ") && ";
        }
        if (!result.isEmpty()) {
            result = result.substring(0, result.lastIndexOf(" && "));
        }
        return result;
    }

    private String eq(String field, Object value) {
        return field + " == " + value.toString();
    }

    private String or(List<String> filters) {
        String result = "";
        for (String filter : filters) {
            result = result + "(" + filter + ") || ";
        }
        if (!result.isEmpty()) {
            result = result.substring(0, result.lastIndexOf(" || "));
        }
        return result;
    }

    private String like(String field, String value) {
        return field + ".toLowerCase().matches('.*" + value.toLowerCase(Locale.GERMANY) + ".*') == true";
    }

    public boolean checkSearchValues(String email, String firstName, String lastName, String phone, Date birth) {
        return !StringUtils.isEmpty(firstName) || !StringUtils.isEmpty(email) || !StringUtils.isEmpty(lastName) || !StringUtils.isEmpty(phone) || birth != null ;
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
