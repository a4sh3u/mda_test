//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.Document;
import de.smava.webapp.account.todo.dao.DocumentDao;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Documents'.
 *
 * @author generator
 */
@Repository(value = "documentDao")
public class JdoDocumentDao extends JdoBaseDao implements DocumentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDocumentDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(document)}
    private static final long serialVersionUID = -2813288610477852799L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the document identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Document load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocument - start: id=" + id);
        }
        Document result = getEntity(Document.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocument - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Document getDocument(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Document entity = findUniqueEntity(Document.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the document.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Document document) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDocument: " + document);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(document)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(document);
    }

    /**
     * @deprecated Use {@link #save(Document) instead}
     */
    public Long saveDocument(Document document) {
        return save(document);
    }

    /**
     * Deletes an document, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document
     */
    public void deleteDocument(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDocument: " + id);
        }
        deleteEntity(Document.class, id);
    }

    /**
     * Retrieves all 'Document' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Document> getDocumentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - start");
        }
        Collection<Document> result = getEntities(Document.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Document' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Document> getDocumentList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - start: pageable=" + pageable);
        }
        Collection<Document> result = getEntities(Document.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Document' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Document> getDocumentList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - start: sortable=" + sortable);
        }
        Collection<Document> result = getEntities(Document.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Document' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Document> getDocumentList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Document> result = getEntities(Document.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Document' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Document> findDocumentList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - start: whereClause=" + whereClause);
        }
        Collection<Document> result = findEntities(Document.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Document' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Document> findDocumentList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Document> result = findEntities(Document.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Document' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Document> findDocumentList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Document> result = findEntities(Document.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Document' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Document> findDocumentList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Document> result = findEntities(Document.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Document' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Document> findDocumentList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Document> result = findEntities(Document.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Document' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Document> findDocumentList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Document> result = findEntities(Document.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findDocumentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Document' instances.
     */
    public long getDocumentCount() {
        long result = getEntityCount(Document.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Document' instances which match the given whereClause.
     */
    public long getDocumentCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Document.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Document' instances which match the given whereClause together with params specified in object array.
     */
    public long getDocumentCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Document.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getDocumentCount() - result: count=" + result);
        }
        return result;
    }


    /**
     * Attaches a Account to the Document instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    public void attachAccount(Document document, Account account) {
        document.setOwner(account);
        account.getDocuments().add(document);
        saveEntity(account);
        saveEntity(document);
    }

    /**
     * Attaches a Contract to the Document instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    public void attachContract(Document document, Contract contract) {
        document.getContracts().add(contract);
        contract.getDocuments().add(document);
        saveEntity(contract);
        saveEntity(document);
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document)}
    //



    /**
     {@inheritDoc}
     */
    @Override
    public void detachContract(Document document, Contract contract) {
        document.removeContract(contract);
        contract.removeDocument(document);
        saveEntity(contract);
        saveEntity(document);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findIncomingDocumentWithAttachmentsList(Account account, Pageable pageable, Sortable sortable) {
        StringBuilder where = new StringBuilder("_attachments.size() > 0 && _direction == param1");
        where.append(" && _owner == param2 ");
        where.append("PARAMETERS java.lang.String param1, de.smava.webapp.account.domain.Account param2");

        Collection<Document> docs;
        if (pageable != null && sortable != null) {
            docs = findEntities(Document.class, where.toString(), pageable, sortable, new Object[] {Document.DIRECTION_IN, account});
        } else {
            docs = findEntities(Document.class, where.toString(), new Object[] {Document.DIRECTION_IN, account});
        }
        return docs;
    }

    public Collection<Document> findOpenOutgoingDocumentList(Account account, Pageable pageable, Sortable sortable) {
        return findOpenOutgoingDocumentList(account, pageable, sortable, false);
    }
    
    public Collection<Document> findOpenOutgoingDocumentListWithAttachmentsOrderByCreation(Account account, Pageable pageable, Sortable sortable) {
        return findOpenOutgoingDocumentList(account, pageable, sortable, true);
    }
    
    private Collection<Document> findOpenOutgoingDocumentList(final Account account, final Pageable pageable, final Sortable sortable, final boolean onlyWithAttachmentsAndOrderByCreation) {
        StringBuilder where = new StringBuilder();
        if (onlyWithAttachmentsAndOrderByCreation) {
            where.append("_attachments.size() > 0 && ");
        }
        where.append("_direction == param1");
        where.append(" && _owner == param2 ");
        where.append(" && _state == param3 ");        
        where.append("PARAMETERS java.lang.String param1, de.smava.webapp.account.domain.Account param2,java.lang.String param3");
        if (onlyWithAttachmentsAndOrderByCreation) {
            where.append(" ORDER BY _creationDate descending");
        }
        Collection<Document> docs;
        if (pageable != null && sortable != null) {
            docs = findEntities(Document.class, where.toString(), pageable, sortable, new Object[] {Document.DIRECTION_OUT, account, Document.STATE_OPEN});
        } else {
            docs = findEntities(Document.class, where.toString(), new Object[] {Document.DIRECTION_OUT, account, Document.STATE_OPEN});
        }
        return docs;
    }
    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findOutgoingDocumentWithAttachmentsList(Account account, Pageable pageable, Sortable sortable) {
        StringBuilder where = new StringBuilder("_attachments.size() > 0 && _direction == param1");
        where.append(" && _owner == param2 ");
        where.append("PARAMETERS java.lang.String param1, de.smava.webapp.account.domain.Account param2");

        Collection<Document> docs;
        if (pageable != null && sortable != null) {
            docs = findEntities(Document.class, where.toString(), pageable, sortable, new Object[] {Document.DIRECTION_OUT, account});
        } else {
            docs = findEntities(Document.class, where.toString(), new Object[] {Document.DIRECTION_OUT, account});
        }
        return docs;
    }


    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findCreditPayoutDocuments(Pageable pageable, Sortable sortable) {
//		query taken from WEBSITE-3858
        String sql = "select distinct d.id as id, date_trunc('day',d.date) as eingangsdatum, a.username, sum(c.amount), c.market_name "
            + "from contract c, document d, account a, \"order\" o, document_contracts dc "
            + "where c.order_id = o.id "
            + "and o.account_id = a.id "
            + "and dc.contract_id = c.id "
            + "and dc.document_id = d.id "
            + "and d.direction = '" + Document.DIRECTION_IN
            + "' and d.state = '" + Document.STATE_SUCCESSFUL
            + "' and c.state = '" + Contract.STATE_PENDING
            + "' and d.type = '" + Document.DOCUMENT_TYPE_CREDIT_AGREEMENT
            + "' group by username, eingangsdatum, d.id, market_name";

        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql);
        q.setClass(Document.class);

        @SuppressWarnings("unchecked")
        Collection<Document> results = (Collection<Document>) q.execute();

        return results;
    }
    /**
     {@inheritDoc}
     */
    @Override
    public Document findLastDocument(Account account, String type, String direction, String state) {
        StringBuilder where = new StringBuilder("_direction == param1");
        where.append(" && _type == param2 ");
        where.append(" && _owner == param3 ");
        where.append(" && _state == param4 ");
        where.append("PARAMETERS java.lang.String param1, java.lang.String param2, de.smava.webapp.account.domain.Account param3, java.lang.String param4 ");
        where.append("ORDER BY _creationDate DESCENDING");

        Collection<Document> docs = findEntities(Document.class, where.toString(), new Object[] {direction, type, account, state});
        
        Document result = null;
        if (!docs.isEmpty()) {
            result = docs.iterator().next();
        }

        return result;
    }
    /**
     {@inheritDoc}
     */
    @Override
    public Document findFirstLastDomument(final Account owner, final Set<String> types, final Set<String> states, final Set<String> directions, final boolean last) {
        final Query q = getPersistenceManager().newNamedQuery(Document.class, "findDocumentsByOwnerTypesStatesDirection");
        if (last) {
            q.setOrdering("_creationDate DESCENDING");
        } else {
            q.setOrdering("_creationDate ASCENDING");
        }
        q.setRange(0, 1);
        q.setUnique(true);
        final Map<String, Object> parameters = new LinkedHashMap<String, Object>();
        parameters.put("account", owner);
        parameters.put("types", types);
        parameters.put("states", states);
        parameters.put("directions", directions);
        final Document result = (Document) q.executeWithMap(parameters);
        return result;
    }
    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findAllDocuments(final Account owner, final Set<String> types, final Set<String> states, final Set<String> directions) {
        final Map<String, Object> parameters = new LinkedHashMap<String, Object>();
        parameters.put("account", owner);
        parameters.put("types", types);
        parameters.put("states", states);
        parameters.put("directions", directions);
        return findByNamedQuery(Document.class, "findDocumentsByOwnerTypesStatesDirection", parameters);
    }
    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findDocumentsByAccountAndType(Long accountId, String documentType, Collection<String> typeOfDispatch, String direction) {
        final Set<String> dispatchTypes;
        if (typeOfDispatch != null) {
            dispatchTypes = new HashSet<String>(typeOfDispatch);
        } else {
            dispatchTypes = Collections.emptySet();
        }
        return findDocumentsByAccountAndTypeAndState(accountId, Collections.singleton(documentType), dispatchTypes, direction, Collections.singleton(Document.STATE_OPEN));
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findDocumentsByAccountAndTypeAndState(final Long accountId, final Set<String> types, final Set<String> typeOfDispatch,
            final String direction, final Set<String> states) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("types", types == null ? Collections.EMPTY_SET : types);
        params.put("accountId", accountId);
        params.put("typeOfDispatch", typeOfDispatch == null ? Collections.EMPTY_SET : typeOfDispatch);
        params.put("states", states == null ? Collections.EMPTY_SET : states);
        params.put("direction", direction);

        return findByNamedQuery(Document.class, "findDocumentsByAccountAndTypeAndState", params);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findDocumentsByAccountAndTypeAndStateWithAttachmentsList(final Long accountId, final Set<String> types, final Set<String> typeOfDispatch,
            final String direction, final Set<String> states) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("types", types == null ? Collections.EMPTY_SET : types);
        params.put("accountId", accountId);
        params.put("typeOfDispatch", typeOfDispatch == null ? Collections.EMPTY_SET : typeOfDispatch);
        params.put("states", states == null ? Collections.EMPTY_SET : states);
        params.put("direction", direction);

        return findByNamedQuery(Document.class, "findDocumentsByAccountAndTypeAndStateWithAttachmentsList", params);
    }


    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> getRepaymentBreakCalls(Account account) {
        StringBuilder whereClause = new StringBuilder("_owner == param1 ");
        whereClause.append("&& _type == param2 ");
        whereClause.append("&& _typeOfDispatch == param3 ");

        whereClause.append("PARAMETERS de.smava.webapp.account.domain.Account param1, java.lang.String param2, java.lang.String param3 ");
        whereClause.append("ORDER BY _creationDate ASCENDING");

        Collection<Document> result = findDocumentList(whereClause.toString(), account, Document.DOCUMENT_TYPE_REPAYMENT_BREAK, Document.TYPE_OF_DISPATCH_PHONE);

        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> findDocuments(Account account, Set<String> documentTypes, Set<String> states) {
        Query query = getPersistenceManager().newNamedQuery(Document.class, "findDocuments");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("account", account);
        params.put("types", documentTypes);
        params.put("states", states);

        return (Collection<Document>) query.executeWithMap(params);
    }



    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> retrieveDocumentsWithDeletableFiles(Long accountId){

        Map<String, Object> params = new HashMap<String, Object>();

        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(CurrentDate.getDate());
        cal.add(Calendar.MONTH, -6);

        Date olderThanSixMonths = cal.getTime();

        params.put("accountId", accountId);
        params.put("olderThan6Months", new Timestamp(olderThanSixMonths.getTime()));

        String queryString = "SELECT d.id AS id FROM document d INNER JOIN document_attachment att on d.id = att.document_id " +
                "WHERE d.account_id = :accountId " +
                "AND d.type in ('CHECKLIST','SALARY_INFO', 'SELF_INFO', 'CREDIT_AGREEMENT', 'CREDIT_PROCUREMENT', 'RDI_REQUEST', 'POSTIDENT', 'SECCI', 'CREDIT_INFO', 'RDI_INFO', 'RDI_CONDITION' ) " +
                "AND d.\"date\" < :olderThan6Months " +
                "AND d.direction = 'OUT'";


        Query query = getPersistenceManager().newQuery(Query.SQL, queryString);
        query.setClass(Document.class);

        return (Collection<Document>) query.executeWithMap(params);

    }

    @Override
    public Collection<Document> retrieveDocumentsWithDeletableFilesShortPeriod(){

        Map<String, Object> params = new HashMap<String, Object>();

        DateTime dateTime = new DateTime(CurrentDate.getDate());
        Date olderThanSixWeeks = dateTime.minusWeeks(6).toDate();

        params.put("olderThan6Weeks", new Timestamp(olderThanSixWeeks.getTime()));
        Query query = getPersistenceManager().newNamedQuery(Document.class, "retrieveDocumentsWithDeletableFilesShortPeriod");

        return (Collection<Document>) query.executeWithMap(params);

    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> retrieveBrokerageBanksDocumentsWithDeletableFiles(){

        Map<String, Object> params = new HashMap<String, Object>();

        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(CurrentDate.getDate());
        cal.add(Calendar.DAY_OF_YEAR, -30);

        Date olderClause = cal.getTime();

        params.put("olderClause", new Timestamp(olderClause.getTime()));

        String queryString = "SELECT d.id AS id FROM document d INNER JOIN document_attachment att on d.id = att.document_id " +
                "WHERE d.type = 'BROKERAGE_APPLICATION_DOCUMENT' " +
                "AND d.\"creation_date\" < :olderClause";

        Query query = getPersistenceManager().newQuery(Query.SQL, queryString);
        query.setClass(Document.class);

        return (Collection<Document>) query.executeWithMap(params);

    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Document> getOpenIncomingDocuments(Account account) {
        return findDocumentList("_state == '" + Document.STATE_OPEN + "' && _owner._id == " + account.getId() + " && _direction == '" + Document.DIRECTION_IN + "' && _typeOfDispatch != '" + Document.TYPE_OF_DISPATCH_SMS + "'");
    }

    @Override
    public Map<Account, List<Document>> getMissingDocumentsGroupedByAccount(int days) {
        Map<Account, List<Document>> result = new HashMap<Account, List<Document>>();

        Date now = CurrentDate.getDate();
        Date limitDateBottom = DateUtils.addDays(now, -days);
        limitDateBottom = DateUtils.clean(limitDateBottom);
        Date limitDateTop = DateUtils.addDays(now, -(days - 1));
        limitDateTop = DateUtils.clean(limitDateTop);


        StringBuffer queryBuffer = new StringBuffer();
        queryBuffer.append("(");
        queryBuffer.append("_direction == '").append(Document.DIRECTION_IN).append("' && ");
        queryBuffer.append("_state == '").append(Document.STATE_OPEN).append("' && ");
        queryBuffer.append("_typeOfDispatch == '").append(Document.TYPE_OF_DISPATCH_MAIL).append("' && ");
        queryBuffer.append("_creationDate >= param1 && _creationDate <= param2 ");
        queryBuffer.append(") ");
        queryBuffer.append("PARAMETERS java.util.Date param1, java.util.Date param2");

        Collection<Document> documents = findDocumentList(queryBuffer.toString(), limitDateBottom, limitDateTop);
        LOGGER.debug("found " + documents.size() + " missing documents");
        for (Document document : documents) {
            if (document.getOwner() != null) {
                List<Document> resultDocuments = result.get(document.getOwner());
                if (resultDocuments == null) {
                    resultDocuments = new LinkedList<Document>();
                }
                resultDocuments.add(document);
                result.put(document.getOwner(), resultDocuments);
            }

        }

        return result;
    }

    @Override
    public Collection<Document> findDocumentsByAccount(Account account, String direction) {
        StringBuilder where = new StringBuilder("_owner._id == ");
        where.append(account.getId());
        where.append(" && _attachments.size() > 0"); // we need only documents with attachments


        String substring = " && (_state == '"
                + Document.STATE_SUCCESSFUL
                + "' || _state == '"
                + Document.STATE_OPEN
                + "') && (_type != '"
                + Document.DOCUMENT_TYPE_FACTORIZATION_CONTRACT
                + "' && _type != '"
                + Document.DOCUMENT_TYPE_BROKERAGE_APPLICATION_DOCUMENT
                + "') ";
        where.append(substring);
        if (direction != null) {
            if (Document.DIRECTION_IN.equalsIgnoreCase(direction)) {
                where.append(" && _direction == '");
                where.append(Document.DIRECTION_IN).append("'");
            } else if (Document.DIRECTION_OUT.equalsIgnoreCase(direction)) {
                where.append(" && _direction == '");
                where.append(Document.DIRECTION_OUT).append("'");
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(where);
        }
        Collection<Document> docs = findDocumentList(where.toString(), new Sortable() {

            @Override
            public boolean sortDescending() {
                return true;
            }

            @Override
            public boolean hasSort() {
                return true;
            }

            @Override
            public String getSort() {
                return "_date";
            }
        });
        return docs;
    }

    @Override
    public Collection<Document> findDocumentsWithContract(long contractId) {
        return findDocumentList("_contracts.contains(c) && c._id == " + contractId);
    }

    @Override
    public Collection<Document> getDocumentsOrderedById(List<Long> ids){
        return findDocumentList("ids.contains(this._id) PARAMETERS java.util.Collection ids ORDER BY _creationDate descending", ids);
    }

    public Collection<Document> getDocumentsBySubject(String subject, Account a, Date laCreation) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("subject",subject);
        params.put("account",a);
        params.put("laCreation",laCreation);
        Date laCreationWithInterval = new DateTime(laCreation).plusDays(3).toDate();
        params.put("laCreationWithInterval",laCreationWithInterval);
        final Query q = getPersistenceManager().newNamedQuery(Document.class, "getDocumentsBySubject");
        Collection<Document> result = executeQuery(q, params, null, false);
        return result;
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
