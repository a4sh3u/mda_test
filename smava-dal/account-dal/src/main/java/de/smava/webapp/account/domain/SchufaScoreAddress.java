package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.interfaces.SchufaScoreAddressEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'SchufaScoreAddresss'.
 */
public class SchufaScoreAddress extends KreditPrivatEntity
        implements SchufaScoreAddressEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(SchufaScoreAddress.class);

            protected Address _address;
            protected SchufaScoreRequestData _requestData;
        
                        /**
            * Setter for the property 'address'.
            */
            public void setAddress(Address address) {
            _address = address;
            }

            /**
            * Returns the property 'address'.
            */
            public Address getAddress() {
            return _address;
            }
                                /**
            * Setter for the property 'request data'.
            */
            public void setRequestData(SchufaScoreRequestData requestData) {
            _requestData = requestData;
            }

            /**
            * Returns the property 'request data'.
            */
            public SchufaScoreRequestData getRequestData() {
            return _requestData;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(SchufaScoreAddress.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(SchufaScoreAddress.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

}
