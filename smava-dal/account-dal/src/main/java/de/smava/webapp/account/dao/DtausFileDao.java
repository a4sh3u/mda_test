//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dtaus file)}

import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.BookingType;
import de.smava.webapp.account.domain.DtausFile;
import de.smava.webapp.account.domain.Transaction;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'DtausFiles'.
 *
 * @author generator
 */
public interface DtausFileDao extends BaseDao<DtausFile>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the dtaus file identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    DtausFile getDtausFile(Long id);

    /**
     * Saves the dtaus file.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveDtausFile(DtausFile dtausFile);

    /**
     * Deletes an dtaus file, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the dtaus file
     */
    void deleteDtausFile(Long id);

    /**
     * Retrieves all 'DtausFile' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<DtausFile> getDtausFileList();

    /**
     * Retrieves a page of 'DtausFile' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<DtausFile> getDtausFileList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'DtausFile' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<DtausFile> getDtausFileList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'DtausFile' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<DtausFile> getDtausFileList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'DtausFile' instances.
     */
    long getDtausFileCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(dtaus file)}
    //
    Collection<DtausFile> getLatestDraftDtausFiles();
    Map<Long, Double> getBookingSumForTransactionsInterimToInternal(BankAccount interimAccount, BookingType bookingType, Date date, DtausFile dtausFile);
    Map<Long, Double> getBookingSumForTransactionsInternalToInterim(BankAccount interimAccount, BookingType bookingType, Date date, DtausFile dtausFile);

    /**
     * Returns a 'DtausFile' with type 'OUT' with the given transaction
     */
    DtausFile getLatestDtausFileOutFromTransaction(Transaction transaction);

    Collection<DtausFile> getLatestDraftDtausFilesOutOpen();

    long getInOpenErrorDtausFileCount();
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
