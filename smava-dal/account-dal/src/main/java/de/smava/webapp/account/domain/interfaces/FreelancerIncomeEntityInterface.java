package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.EconomicalData;

import java.util.Date;


/**
 * The domain object that represents 'FreelancerIncomes'.
 *
 * @author generator
 */
public interface FreelancerIncomeEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    void setEconomicalData(EconomicalData economicalData);

    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    EconomicalData getEconomicalData();
    /**
     * Setter for the property 'year'.
     *
     * 
     *
     */
    void setYear(Integer year);

    /**
     * Returns the property 'year'.
     *
     * 
     *
     */
    Integer getYear();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'volume'.
     *
     * 
     *
     */
    void setVolume(Double volume);

    /**
     * Returns the property 'volume'.
     *
     * 
     *
     */
    Double getVolume();
    /**
     * Setter for the property 'expenses'.
     *
     * 
     *
     */
    void setExpenses(Double expenses);

    /**
     * Returns the property 'expenses'.
     *
     * 
     *
     */
    Double getExpenses();
    /**
     * Setter for the property 'trade tax'.
     *
     * 
     *
     */
    void setTradeTax(Double tradeTax);

    /**
     * Returns the property 'trade tax'.
     *
     * 
     *
     */
    Double getTradeTax();
    /**
     * Setter for the property 'income tax'.
     *
     * 
     *
     */
    void setIncomeTax(Double incomeTax);

    /**
     * Returns the property 'income tax'.
     *
     * 
     *
     */
    Double getIncomeTax();
    /**
     * Setter for the property 'solidarity tax'.
     *
     * 
     *
     */
    void setSolidarityTax(Double solidarityTax);

    /**
     * Returns the property 'solidarity tax'.
     *
     * 
     *
     */
    Double getSolidarityTax();
    /**
     * Setter for the property 'church tax'.
     *
     * 
     *
     */
    void setChurchTax(Double churchTax);

    /**
     * Returns the property 'church tax'.
     *
     * 
     *
     */
    Double getChurchTax();
    /**
     * Setter for the property 'period earnings before taxes'.
     *
     * 
     *
     */
    void setPeriodEarningsBeforeTaxes(Double periodEarningsBeforeTaxes);

    /**
     * Returns the property 'period earnings before taxes'.
     *
     * 
     *
     */
    Double getPeriodEarningsBeforeTaxes();
    /**
     * Setter for the property 'annual earnings before taxes'.
     *
     * 
     *
     */
    void setAnnualEarningsBeforeTaxes(Double annualEarningsBeforeTaxes);

    /**
     * Returns the property 'annual earnings before taxes'.
     *
     * 
     *
     */
    Double getAnnualEarningsBeforeTaxes();
    /**
     * Setter for the property 'annual profit after tax'.
     *
     * 
     *
     */
    void setAnnualProfitAfterTax(Double annualProfitAfterTax);

    /**
     * Returns the property 'annual profit after tax'.
     *
     * 
     *
     */
    Double getAnnualProfitAfterTax();
    /**
     * Setter for the property 'monthly net income'.
     *
     * 
     *
     */
    void setMonthlyNetIncome(Double monthlyNetIncome);

    /**
     * Returns the property 'monthly net income'.
     *
     * 
     *
     */
    Double getMonthlyNetIncome();
    /**
     * Setter for the property 'special reserve'.
     *
     * 
     *
     */
    void setSpecialReserve(Double specialReserve);

    /**
     * Returns the property 'special reserve'.
     *
     * 
     *
     */
    Double getSpecialReserve();
    /**
     * Setter for the property 'special reserve dissolution'.
     *
     * 
     *
     */
    void setSpecialReserveDissolution(Double specialReserveDissolution);

    /**
     * Returns the property 'special reserve dissolution'.
     *
     * 
     *
     */
    Double getSpecialReserveDissolution();
    /**
     * Setter for the property 'non cash expenses'.
     *
     * 
     *
     */
    void setNonCashExpenses(Double nonCashExpenses);

    /**
     * Returns the property 'non cash expenses'.
     *
     * 
     *
     */
    Double getNonCashExpenses();
    /**
     * Setter for the property 'own consumption'.
     *
     * 
     *
     */
    void setOwnConsumption(Double ownConsumption);

    /**
     * Returns the property 'own consumption'.
     *
     * 
     *
     */
    Double getOwnConsumption();
    /**
     * Setter for the property 'non cash revenues'.
     *
     * 
     *
     */
    void setNonCashRevenues(Double nonCashRevenues);

    /**
     * Returns the property 'non cash revenues'.
     *
     * 
     *
     */
    Double getNonCashRevenues();

}
