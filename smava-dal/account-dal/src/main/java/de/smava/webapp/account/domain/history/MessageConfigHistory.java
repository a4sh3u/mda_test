package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractMessageConfig;




/**
 * The domain object that has all history aggregation related fields for 'MessageConfigs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MessageConfigHistory extends AbstractMessageConfig {

    protected transient boolean _forwardInitVal;
    protected transient boolean _forwardIsSet;
    protected transient boolean _forwardLenderStatusMailInitVal;
    protected transient boolean _forwardLenderStatusMailIsSet;


	
    /**
     * Returns the initial value of the property 'forward'.
     */
    public boolean forwardInitVal() {
        boolean result;
        if (_forwardIsSet) {
            result = _forwardInitVal;
        } else {
            result = getForward();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'forward'.
     */
    public boolean forwardIsDirty() {
        return !valuesAreEqual(forwardInitVal(), getForward());
    }

    /**
     * Returns true if the setter method was called for the property 'forward'.
     */
    public boolean forwardIsSet() {
        return _forwardIsSet;
    }
	
    /**
     * Returns the initial value of the property 'forward lender status mail'.
     */
    public boolean forwardLenderStatusMailInitVal() {
        boolean result;
        if (_forwardLenderStatusMailIsSet) {
            result = _forwardLenderStatusMailInitVal;
        } else {
            result = getForwardLenderStatusMail();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'forward lender status mail'.
     */
    public boolean forwardLenderStatusMailIsDirty() {
        return !valuesAreEqual(forwardLenderStatusMailInitVal(), getForwardLenderStatusMail());
    }

    /**
     * Returns true if the setter method was called for the property 'forward lender status mail'.
     */
    public boolean forwardLenderStatusMailIsSet() {
        return _forwardLenderStatusMailIsSet;
    }
		
}
