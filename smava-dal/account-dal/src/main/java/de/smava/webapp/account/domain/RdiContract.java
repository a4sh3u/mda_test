package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.RdiContractHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'RdiContracts'.
 */
public class RdiContract extends RdiContractHistory  {

        protected Date _creationDate;
        protected Date _expirationDate;
        protected Long _contractNumber;
        protected Order _order;
        protected Date _fromDate;
        protected Date _toDate;
        protected String _state;
        protected String _type;
        protected String _tariff;
        protected Date _termsAndConditionsAcceptanceDate;
        protected Collection<RdiRequest> _requests;
        protected Collection<RdiPayment> _payments;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                            
    /**
     * Setter for the property 'contract number'.
     */
    public void setContractNumber(Long contractNumber) {
        _contractNumber = contractNumber;
    }
            
    /**
     * Returns the property 'contract number'.
     */
    public Long getContractNumber() {
        return _contractNumber;
    }
                                            
    /**
     * Setter for the property 'order'.
     */
    public void setOrder(Order order) {
        _order = order;
    }
            
    /**
     * Returns the property 'order'.
     */
    public Order getOrder() {
        return _order;
    }
                                    /**
     * Setter for the property 'from date'.
     */
    public void setFromDate(Date fromDate) {
        if (!_fromDateIsSet) {
            _fromDateIsSet = true;
            _fromDateInitVal = getFromDate();
        }
        registerChange("from date", _fromDateInitVal, fromDate);
        _fromDate = fromDate;
    }
                        
    /**
     * Returns the property 'from date'.
     */
    public Date getFromDate() {
        return _fromDate;
    }
                                    /**
     * Setter for the property 'to date'.
     */
    public void setToDate(Date toDate) {
        if (!_toDateIsSet) {
            _toDateIsSet = true;
            _toDateInitVal = getToDate();
        }
        registerChange("to date", _toDateInitVal, toDate);
        _toDate = toDate;
    }
                        
    /**
     * Returns the property 'to date'.
     */
    public Date getToDate() {
        return _toDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'tariff'.
     */
    public void setTariff(String tariff) {
        if (!_tariffIsSet) {
            _tariffIsSet = true;
            _tariffInitVal = getTariff();
        }
        registerChange("tariff", _tariffInitVal, tariff);
        _tariff = tariff;
    }
                        
    /**
     * Returns the property 'tariff'.
     */
    public String getTariff() {
        return _tariff;
    }
                                    /**
     * Setter for the property 'terms and conditions acceptance date'.
     */
    public void setTermsAndConditionsAcceptanceDate(Date termsAndConditionsAcceptanceDate) {
        if (!_termsAndConditionsAcceptanceDateIsSet) {
            _termsAndConditionsAcceptanceDateIsSet = true;
            _termsAndConditionsAcceptanceDateInitVal = getTermsAndConditionsAcceptanceDate();
        }
        registerChange("terms and conditions acceptance date", _termsAndConditionsAcceptanceDateInitVal, termsAndConditionsAcceptanceDate);
        _termsAndConditionsAcceptanceDate = termsAndConditionsAcceptanceDate;
    }
                        
    /**
     * Returns the property 'terms and conditions acceptance date'.
     */
    public Date getTermsAndConditionsAcceptanceDate() {
        return _termsAndConditionsAcceptanceDate;
    }
                                            
    /**
     * Setter for the property 'requests'.
     */
    public void setRequests(Collection<RdiRequest> requests) {
        _requests = requests;
    }
            
    /**
     * Returns the property 'requests'.
     */
    public Collection<RdiRequest> getRequests() {
        return _requests;
    }
                                            
    /**
     * Setter for the property 'payments'.
     */
    public void setPayments(Collection<RdiPayment> payments) {
        _payments = payments;
    }
            
    /**
     * Returns the property 'payments'.
     */
    public Collection<RdiPayment> getPayments() {
        return _payments;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RdiContract.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _tariff=").append(_tariff);
            builder.append("\n}");
        } else {
            builder.append(RdiContract.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
