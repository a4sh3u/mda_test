package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.CustomerAgreementType;

import java.util.Date;


/**
 * The domain object that represents 'CustomerAgreements'.
 *
 * @author generator
 */
public interface CustomerAgreementEntityInterface {

    /**
     * Setter for the property 'agreement date'.
     *
     * 
     *
     */
    void setAgreementDate(Date agreementDate);

    /**
     * Returns the property 'agreement date'.
     *
     * 
     *
     */
    Date getAgreementDate();
    /**
     * Setter for the property 'revocation date'.
     *
     * 
     *
     */
    void setRevocationDate(Date revocationDate);

    /**
     * Returns the property 'revocation date'.
     *
     * 
     *
     */
    Date getRevocationDate();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(CustomerAgreementType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    CustomerAgreementType getType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'affiliate information id'.
     *
     * 
     *
     */
    void setAffiliateInformationId(Long affiliateInformationId);

    /**
     * Returns the property 'affiliate information id'.
     *
     * 
     *
     */
    Long getAffiliateInformationId();

}
