//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(category)}

import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.todo.dao.CategoryInfo;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Categorys'.
 *
 * @author generator
 */
public interface CategoryDao extends BaseDao<Category>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the category identified by the given id.
     *
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Category getCategory(Long id);

    /**
     * Saves the category.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao<>#save(T)} instead.
     */
    Long saveCategory(Category category);

    /**
     * Deletes an category, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the category
     */
    void deleteCategory(Long id);

    /**
     * Retrieves all 'Category' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Category> getCategoryList();

    /**
     * Retrieves a page of 'Category' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     */
    Collection<Category> getCategoryList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Category' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<Category> getCategoryList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Category' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<Category> getCategoryList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Category' instances.
     */
    long getCategoryCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(category)}
    //
    Collection<Category> getShortlistedCategories();

    Collection<Category> getRootCategories();

    /** Get children sorted. */
    Collection<Category> getChildren(Category parent);
    //

    /**
     * Attaches a Order to the Category instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachOrder(Category category, Order order);
    void attachOrder(Category category, Order order, boolean main);
    void detachOrder(Category category, Order order);

    /**
     * Attaches a Category to the Category instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachCategory(Category parent, Category child);

    void detachCategory(Category parent, Category child);

    /**
     * get a category id for the given name
     * 
     * @param name the property's name
     * @return the id of the given property
     */
	Long getCategoryIdByName(String name);

    Collection<Category> getOrderedCategories();

    // !!!!!!!! End of insert code section !!!!!!!!
}
