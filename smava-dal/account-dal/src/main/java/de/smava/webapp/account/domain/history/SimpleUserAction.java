package de.smava.webapp.account.domain.history;

import java.util.Map;

import de.smava.webapp.commons.domain.Change;

/**
 * Base class for UserAction implementations.
 *
 * @author michael.schwalbe (21.10.2005)
 */
public class SimpleUserAction implements UserAction {
    private static final long serialVersionUID = 1810144460250019150L;

    private Map<String, Change> _changedData;

    /**
     * Get the changed data, where old values are mapped to new values of the data that was modified.
     * If data was initially set, only a key can be set.
     *
     * @return Map a mapping where old values are mapped to new values of the data that was modified
     */
    public Map<String, Change> getChangeData() {
        return _changedData;
    }

    public void setChangeData(Map<String, Change> changedData) {
        _changedData = changedData;
    }
}