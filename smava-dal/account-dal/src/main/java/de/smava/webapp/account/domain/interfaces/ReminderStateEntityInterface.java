package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Transaction;

import java.util.Date;


/**
 * The domain object that represents 'ReminderStates'.
 *
 * @author generator
 */
public interface ReminderStateEntityInterface {

    /**
     * Setter for the property 'transaction'.
     *
     * 
     *
     */
    void setTransaction(Transaction transaction);

    /**
     * Returns the property 'transaction'.
     *
     * 
     *
     */
    Transaction getTransaction();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'calculation date'.
     *
     * 
     *
     */
    void setCalculationDate(Date calculationDate);

    /**
     * Returns the property 'calculation date'.
     *
     * 
     *
     */
    Date getCalculationDate();
    /**
     * Setter for the property 'level'.
     *
     * 
     *
     */
    void setLevel(int level);

    /**
     * Returns the property 'level'.
     *
     * 
     *
     */
    int getLevel();

}
