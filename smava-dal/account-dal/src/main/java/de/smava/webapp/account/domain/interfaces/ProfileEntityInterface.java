package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;

import java.util.Date;


/**
 * The domain object that represents 'Profiles'.
 *
 * @author generator
 */
public interface ProfileEntityInterface {

    /**
     * Setter for the property 'hobbies'.
     *
     * 
     *
     */
    void setHobbies(String hobbies);

    /**
     * Returns the property 'hobbies'.
     *
     * 
     *
     */
    String getHobbies();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'image'.
     *
     * 
     *
     */
    void setImage(String image);

    /**
     * Returns the property 'image'.
     *
     * 
     *
     */
    String getImage();
    /**
     * Setter for the property 'image width'.
     *
     * 
     *
     */
    void setImageWidth(int imageWidth);

    /**
     * Returns the property 'image width'.
     *
     * 
     *
     */
    int getImageWidth();
    /**
     * Setter for the property 'image height'.
     *
     * 
     *
     */
    void setImageHeight(int imageHeight);

    /**
     * Returns the property 'image height'.
     *
     * 
     *
     */
    int getImageHeight();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'change date'.
     *
     * 
     *
     */
    void setChangeDate(Date changeDate);

    /**
     * Returns the property 'change date'.
     *
     * 
     *
     */
    Date getChangeDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();

}
