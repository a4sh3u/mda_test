package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.interfaces.BidEntityInterface;
import de.smava.webapp.account.domain.interfaces.OfferEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'Offers'.

 */
public class Offer extends Bid
        implements MarketingTrackingEntity, OfferEntityInterface,
                    BidEntityInterface {

    private static final Logger LOGGER = Logger.getLogger(Offer.class);
    private static final long serialVersionUID = 6803337200325150307L;

    /**
     * Default constructor (would have been blocked by copy constructor).
     */
    public Offer() {
    }

    /**
     * Constructor with a Lender as argument.
     */
    public Offer(Account account) {
        super();
        this._account = account;
        setCreationDate(CurrentDate.getDate());
        if (account.getOffers() == null) {
            account.setOffers(new HashSet<Offer>());
        }
        account.getOffers().add(this);
    }

    /**
     * copy constructor for matching algorithm and contract creation.
     */
    public Offer(Offer offer) {
        setId(offer.getId()); //TODO: Check if this is necessary (matching test suite)
        _title = offer.getTitle();
        _description = offer.getDescription();
        _amount = offer.getAmount();
        if (offer.getCreationDate() != null) {
            _creationDate = new Date(offer.getCreationDate().getTime());
        }
        if (offer.getUpdateDate() != null) {
            _updateDate = new Date(offer.getUpdateDate().getTime());
        }

        _bidInterests = new ArrayList<BidInterest>();
        for (BidInterest bidInterest : offer.getBidInterests()) {
            String marketName = bidInterest.getMarketName();
            if (marketName.length() == 2) {
                marketName = marketName.charAt(0) + "0" + marketName.charAt(1);
            }
            putRateForMarket(marketName, bidInterest.getRate());
        }

        _state = offer.getState();

        if (offer.getGroupId() == 0) {
            _groupId = offer.getId();
            offer.setGroupId(offer.getId());
        } else {
            _groupId = offer.getGroupId();
        }
        if (offer.getContract() != null) {
            _contract = offer.getContract();
        }
        if (offer.getAccount() != null) {
            _account = offer.getAccount();
        }
    }

    public Offer split(double newAmount) {
        Offer splitOffer = new Offer(this);
        splitOffer.setId(null); //Mark as new offer
        splitOffer.setAmount(newAmount);
        //return (double) Math.round(value * HUNDRED_DOUBLE) / HUNDRED_DOUBLE;
        double value = (getAmount() - newAmount);
        setAmount( (double) Math.round( value* 100.0) / 100.0);
        return splitOffer;
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        if (getAccount() != null) {
            list.add(getAccount());
        }
        return list;
    }

            protected Account _account;
            protected BidSearch _bidSearch;
            protected Long _marketingPlacementId;
            protected BookingPerformance _bookingPerformance;
            protected EconomicalData _economicalData;
            protected Double _provisionRateFix;
            protected Double _provisionRateLender;
        
                        /**
            * Setter for the property 'account'.
            */
            public void setAccount(Account account) {
            _account = account;
            }

            /**
            * Returns the property 'account'.
            */
            public Account getAccount() {
            return _account;
            }
                                /**
            * Setter for the property 'bid search'.
            */
            public void setBidSearch(BidSearch bidSearch) {
            _bidSearch = bidSearch;
            }

            /**
            * Returns the property 'bid search'.
            */
            public BidSearch getBidSearch() {
            return _bidSearch;
            }
                                /**
            * Setter for the property 'marketing placement id'.
            */
            public void setMarketingPlacementId(Long marketingPlacementId) {
            _marketingPlacementId = marketingPlacementId;
            }

            /**
            * Returns the property 'marketing placement id'.
            */
            public Long getMarketingPlacementId() {
            return _marketingPlacementId;
            }
                                /**
            * Setter for the property 'booking performance'.
            */
            public void setBookingPerformance(BookingPerformance bookingPerformance) {
            _bookingPerformance = bookingPerformance;
            }

            /**
            * Returns the property 'booking performance'.
            */
            public BookingPerformance getBookingPerformance() {
            return _bookingPerformance;
            }
                                /**
            * Setter for the property 'economical data'.
            */
            public void setEconomicalData(EconomicalData economicalData) {
            _economicalData = economicalData;
            }

            /**
            * Returns the property 'economical data'.
            */
            public EconomicalData getEconomicalData() {
            return _economicalData;
            }
                                /**
            * Setter for the property 'provision rate fix'.
            */
            public void setProvisionRateFix(Double provisionRateFix) {
            _provisionRateFix = provisionRateFix;
            }

            /**
            * Returns the property 'provision rate fix'.
            */
            public Double getProvisionRateFix() {
            return _provisionRateFix;
            }
                                /**
            * Setter for the property 'provision rate lender'.
            */
            public void setProvisionRateLender(Double provisionRateLender) {
            _provisionRateLender = provisionRateLender;
            }

            /**
            * Returns the property 'provision rate lender'.
            */
            public Double getProvisionRateLender() {
            return _provisionRateLender;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(Offer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(Offer.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

}
