package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.LeadTrackingEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'LeadTrackings'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractLeadTracking
    extends KreditPrivatEntity implements LeadTrackingEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractLeadTracking.class);

}

