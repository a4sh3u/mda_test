package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractContract;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Contracts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ContractHistory extends AbstractContract {

    protected transient String _contractNumberInitVal;
    protected transient boolean _contractNumberIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _sealDateInitVal;
    protected transient boolean _sealDateIsSet;
    protected transient Date _startDateInitVal;
    protected transient boolean _startDateIsSet;
    protected transient Date _endDateInitVal;
    protected transient boolean _endDateIsSet;
    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient float _rateInitVal;
    protected transient boolean _rateIsSet;
    protected transient String _marketNameInitVal;
    protected transient boolean _marketNameIsSet;
    protected transient float _effectiveYieldBorrowerInitVal;
    protected transient boolean _effectiveYieldBorrowerIsSet;
    protected transient double _totalContractRepaymentInitVal;
    protected transient boolean _totalContractRepaymentIsSet;
    protected transient double _encashmentRateInitVal;
    protected transient boolean _encashmentRateIsSet;
    protected transient double _updatedRoiInitVal;
    protected transient boolean _updatedRoiIsSet;
    protected transient double _expectedOrderRoiInitVal;
    protected transient boolean _expectedOrderRoiIsSet;


	
    /**
     * Returns the initial value of the property 'contract number'.
     */
    public String contractNumberInitVal() {
        String result;
        if (_contractNumberIsSet) {
            result = _contractNumberInitVal;
        } else {
            result = getContractNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'contract number'.
     */
    public boolean contractNumberIsDirty() {
        return !valuesAreEqual(contractNumberInitVal(), getContractNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'contract number'.
     */
    public boolean contractNumberIsSet() {
        return _contractNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'seal date'.
     */
    public Date sealDateInitVal() {
        Date result;
        if (_sealDateIsSet) {
            result = _sealDateInitVal;
        } else {
            result = getSealDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'seal date'.
     */
    public boolean sealDateIsDirty() {
        return !valuesAreEqual(sealDateInitVal(), getSealDate());
    }

    /**
     * Returns true if the setter method was called for the property 'seal date'.
     */
    public boolean sealDateIsSet() {
        return _sealDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'start date'.
     */
    public Date startDateInitVal() {
        Date result;
        if (_startDateIsSet) {
            result = _startDateInitVal;
        } else {
            result = getStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start date'.
     */
    public boolean startDateIsDirty() {
        return !valuesAreEqual(startDateInitVal(), getStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'start date'.
     */
    public boolean startDateIsSet() {
        return _startDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end date'.
     */
    public Date endDateInitVal() {
        Date result;
        if (_endDateIsSet) {
            result = _endDateInitVal;
        } else {
            result = getEndDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end date'.
     */
    public boolean endDateIsDirty() {
        return !valuesAreEqual(endDateInitVal(), getEndDate());
    }

    /**
     * Returns true if the setter method was called for the property 'end date'.
     */
    public boolean endDateIsSet() {
        return _endDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'market name'.
     */
    public String marketNameInitVal() {
        String result;
        if (_marketNameIsSet) {
            result = _marketNameInitVal;
        } else {
            result = getMarketName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'market name'.
     */
    public boolean marketNameIsDirty() {
        return !valuesAreEqual(marketNameInitVal(), getMarketName());
    }

    /**
     * Returns true if the setter method was called for the property 'market name'.
     */
    public boolean marketNameIsSet() {
        return _marketNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'effective yield borrower'.
     */
    public float effectiveYieldBorrowerInitVal() {
        float result;
        if (_effectiveYieldBorrowerIsSet) {
            result = _effectiveYieldBorrowerInitVal;
        } else {
            result = getEffectiveYieldBorrower();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'effective yield borrower'.
     */
    public boolean effectiveYieldBorrowerIsDirty() {
        return !valuesAreEqual(effectiveYieldBorrowerInitVal(), getEffectiveYieldBorrower());
    }

    /**
     * Returns true if the setter method was called for the property 'effective yield borrower'.
     */
    public boolean effectiveYieldBorrowerIsSet() {
        return _effectiveYieldBorrowerIsSet;
    }
	
    /**
     * Returns the initial value of the property 'total contract repayment'.
     */
    public double totalContractRepaymentInitVal() {
        double result;
        if (_totalContractRepaymentIsSet) {
            result = _totalContractRepaymentInitVal;
        } else {
            result = getTotalContractRepayment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'total contract repayment'.
     */
    public boolean totalContractRepaymentIsDirty() {
        return !valuesAreEqual(totalContractRepaymentInitVal(), getTotalContractRepayment());
    }

    /**
     * Returns true if the setter method was called for the property 'total contract repayment'.
     */
    public boolean totalContractRepaymentIsSet() {
        return _totalContractRepaymentIsSet;
    }
			
    /**
     * Returns the initial value of the property 'encashment rate'.
     */
    public double encashmentRateInitVal() {
        double result;
        if (_encashmentRateIsSet) {
            result = _encashmentRateInitVal;
        } else {
            result = getEncashmentRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'encashment rate'.
     */
    public boolean encashmentRateIsDirty() {
        return !valuesAreEqual(encashmentRateInitVal(), getEncashmentRate());
    }

    /**
     * Returns true if the setter method was called for the property 'encashment rate'.
     */
    public boolean encashmentRateIsSet() {
        return _encashmentRateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'updated roi'.
     */
    public double updatedRoiInitVal() {
        double result;
        if (_updatedRoiIsSet) {
            result = _updatedRoiInitVal;
        } else {
            result = getUpdatedRoi();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'updated roi'.
     */
    public boolean updatedRoiIsDirty() {
        return !valuesAreEqual(updatedRoiInitVal(), getUpdatedRoi());
    }

    /**
     * Returns true if the setter method was called for the property 'updated roi'.
     */
    public boolean updatedRoiIsSet() {
        return _updatedRoiIsSet;
    }
							
    /**
     * Returns the initial value of the property 'expected order roi'.
     */
    public double expectedOrderRoiInitVal() {
        double result;
        if (_expectedOrderRoiIsSet) {
            result = _expectedOrderRoiInitVal;
        } else {
            result = getExpectedOrderRoi();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expected order roi'.
     */
    public boolean expectedOrderRoiIsDirty() {
        return !valuesAreEqual(expectedOrderRoiInitVal(), getExpectedOrderRoi());
    }

    /**
     * Returns true if the setter method was called for the property 'expected order roi'.
     */
    public boolean expectedOrderRoiIsSet() {
        return _expectedOrderRoiIsSet;
    }
		
}
