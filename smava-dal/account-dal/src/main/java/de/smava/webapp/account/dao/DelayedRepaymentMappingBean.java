/**
 * 
 */
package de.smava.webapp.account.dao;

/**
 * DelayedRepaymentMappingBean.
 * <br />
 * <br />
 * Creation date: 14.08.2008
 * @author Robert Porscha
 * 
 */
public class DelayedRepaymentMappingBean {
	
	private Long _accountId;
	private Double _rate;
	
		
	private Long _contractId;
	
	public Long getAccountId() {
		return _accountId;
	}
	public void setAccountId(Long accountId) {
		_accountId = accountId;
	}
	public Double getRate() {
		return _rate;
	}
	public void setRate(Double rate) {
		_rate = rate;
	}
	public Long getContractId() {
		return _contractId;
	}
	public void setContractId(Long contractId) {
		_contractId = contractId;
	}
	
}
