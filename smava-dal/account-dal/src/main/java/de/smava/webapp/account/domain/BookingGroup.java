//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking group)}

import de.smava.webapp.account.domain.history.BookingGroupHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BookingGroups'.
 *
 * @author generator
 */
public class BookingGroup extends BookingGroupHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(booking group)}
    public BookingGroup() {
        _creationDate = CurrentDate.getDate();
    }


    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected Date _date;
        protected Date _creationDate;
        protected String _market;
        protected String _type;
        protected String _specifier;
        
                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'date'.
     */
    public void setDate(Date date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = getDate();
        }
        registerChange("date", _dateInitVal, date);
        _date = date;
    }
                        
    /**
     * Returns the property 'date'.
     */
    public Date getDate() {
        return _date;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'market'.
     */
    public void setMarket(String market) {
        if (!_marketIsSet) {
            _marketIsSet = true;
            _marketInitVal = getMarket();
        }
        registerChange("market", _marketInitVal, market);
        _market = market;
    }
                        
    /**
     * Returns the property 'market'.
     */
    public String getMarket() {
        return _market;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'specifier'.
     */
    public void setSpecifier(String specifier) {
        if (!_specifierIsSet) {
            _specifierIsSet = true;
            _specifierInitVal = getSpecifier();
        }
        registerChange("specifier", _specifierInitVal, specifier);
        _specifier = specifier;
    }
                        
    /**
     * Returns the property 'specifier'.
     */
    public String getSpecifier() {
        return _specifier;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BookingGroup.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _market=").append(_market);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _specifier=").append(_specifier);
            builder.append("\n}");
        } else {
            builder.append(BookingGroup.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BookingGroup asBookingGroup() {
        return this;
    }
}
