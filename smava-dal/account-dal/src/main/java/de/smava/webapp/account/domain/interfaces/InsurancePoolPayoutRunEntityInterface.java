package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Booking;
import de.smava.webapp.account.domain.BookingGroup;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'InsurancePoolPayoutRuns'.
 *
 * @author generator
 */
public interface InsurancePoolPayoutRunEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'bookings'.
     *
     * 
     *
     */
    void setBookings(List<Booking> bookings);

    /**
     * Returns the property 'bookings'.
     *
     * 
     *
     */
    List<Booking> getBookings();
    /**
     * Setter for the property 'booking group'.
     *
     * 
     *
     */
    void setBookingGroup(BookingGroup bookingGroup);

    /**
     * Returns the property 'booking group'.
     *
     * 
     *
     */
    BookingGroup getBookingGroup();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'payment rate'.
     *
     * 
     *
     */
    void setPaymentRate(Double paymentRate);

    /**
     * Returns the property 'payment rate'.
     *
     * 
     *
     */
    Double getPaymentRate();

}
