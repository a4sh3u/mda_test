package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.RdiPayment;
import de.smava.webapp.account.domain.RdiRequest;
import de.smava.webapp.account.domain.interfaces.RdiContractEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

/**
 * The domain object that represents 'RdiContracts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractRdiContract
    extends KreditPrivatEntity implements RdiContractEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRdiContract.class);

    public static final String METHOD_SET_CANCELATION = "setCancellation";

    /** KP mit RSV wurde erstellt. */
    public static final String STATE_NEW = "new";

    /** KP mit RSV finanziert, vor Auszahlung. */
    public static final String STATE_PENDING = "pending";

    /** KP mit RSV in Auszahlung, wenn Unterschrift vorhanden. */
    public static final String STATE_PROCESSING = "processing";

    /** RSV Vertrag durch CL erfolgreich verarbeitet. */
    public static final String STATE_PROCESSING_SUCCESS = "success";

    /** RSV Vertrag nicht best�tigt (KN ohne Unterschrift / durch CL abgelehnt). */
    public static final String STATE_DELETED = "deleted";

    /** RSV Vertrag innerhalb von Frist durch KN widersprochen. */
    public static final String STATE_OBJECTED = "objected";

    /** RSV Vertrag nach Ablauf der Widerspruchsfrist durch KN widersprochen. */
    public static final String STATE_CANCELED = "canceled";

    public static final String TYPE_NORMAL = "normal";
    public static final String TYPE_DOUBLE = "double";
    public static final String TYPE_FULL = "full";
    public static final String TYPE_NONE = "noRdiContract";

    public static final Set<String> TYPES = ConstCollector.getAll("TYPE_");

    public boolean isExpired(Date atTime) {
        return getExpirationDate() != null && (!getExpirationDate().after(atTime));
    }

    public boolean isExpired() {
        return isExpired(CurrentDate.getDate());
    }

    public boolean hasSubmittedCancelationRequest() {
        boolean result = false;

        for (RdiRequest request : getRequests()) {
            if (METHOD_SET_CANCELATION.equals(request.getMethod()) && RdiRequest.STATE_SUCCESS.equals(request.getState())) {
                result = true;
            }
        }
        return result;
    }

    /**
     * contract states.
     */
    public void setStateNew() {
        setState(STATE_NEW);
    }
    public boolean isStateNew() {
        return STATE_NEW.equals(getState());
    }

    public void setStatePending() {
        setState(STATE_PENDING);
    }
    public boolean isStatePending() {
        return STATE_PENDING.equals(getState());
    }

    public void setStateProcessing() {
        setState(STATE_PROCESSING);
    }
    public boolean isStateProcessing() {
        return STATE_PROCESSING.equals(getState());
    }

    public void setStateProcessingSuccess() {
        setState(STATE_PROCESSING_SUCCESS);
    }
    public boolean isStateProcessingSuccess() {
        return STATE_PROCESSING_SUCCESS.equals(getState());
    }

    public void setStateDeleted() {
        setState(STATE_DELETED);
    }
    public boolean isStateDeleted() {
        return STATE_DELETED.equals(getState());
    }

    public void setStateCanceled() {
        setState(STATE_CANCELED);
    }
    public boolean isStateCanceled() {
        return STATE_CANCELED.equals(getState());
    }

    public void setStateObjected() {
        setState(STATE_OBJECTED);
    }
    public boolean isStateObjected() {
        return STATE_OBJECTED.equals(getType());
    }

    public boolean isTypeNormal() {
        return TYPE_NORMAL.equals(getType());
    }

    public boolean isTypeDouble() {
        return TYPE_DOUBLE.equals(getType());
    }

    public boolean isTypeFull() {
        return TYPE_FULL.equals(getType());
    }

    /**
     * rdi payment history access.
     */

    public RdiPayment getCurrentRdiPayment() {
        return getCurrentRdiPayment(CurrentDate.getDate());
    }

    public RdiPayment getCurrentRdiPayment(Date atTime) {
        RdiPayment rdiPayment = null;
        Collection<RdiPayment> payments = getPayments();
        if (payments != null && !payments.isEmpty()) {
            // iterate to last rdi contract
            for (RdiPayment c : payments) {
                rdiPayment = c;
            }

            // drop result when expired
            if (rdiPayment.isExpired(atTime)) {
                rdiPayment = null;
            }
        }

        return rdiPayment;
    }

    public RdiRequest getCurrentRdiRequest() {
        RdiRequest rdiRequest = null;
        Collection<RdiRequest> requests = getRequests();
        if (requests != null && !requests.isEmpty()) {
            // iterate to last rdi request
            for (RdiRequest r : requests) {
                rdiRequest = r;
            }
        }
        return rdiRequest;
    }

    public void addRdiRequest(RdiRequest rdiRequest) {
        if (getRequests() == null) {
            setRequests(new ArrayList<RdiRequest>());
        }

        getRequests().add(rdiRequest);
    }

    public void addRdiPayment(RdiPayment rdiPayment) {
        if (getPayments() == null) {
            setPayments(new ArrayList<RdiPayment>());
        }

        getPayments().add(rdiPayment);
    }

    public boolean isValidPaymentType(String rdiPaymentType) {
        boolean isValidPayment = false;

        if (isTypeNormal()) {
            isValidPayment = RdiPayment.TYPE_LV.equals(rdiPaymentType);
        } else if (isTypeDouble()) {
            isValidPayment = RdiPayment.TYPE_LV.equals(rdiPaymentType) || RdiPayment.TYPE_AU.equals(rdiPaymentType);
        } else if (isTypeFull()) {
            isValidPayment = RdiPayment.TYPE_LV.equals(rdiPaymentType) || RdiPayment.TYPE_AU.equals(rdiPaymentType) || RdiPayment.TYPE_AL.equals(rdiPaymentType);
        }

        return isValidPayment;
    }

}

