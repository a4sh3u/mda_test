package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.MatchSortingHistory;

/**
 * The domain object that represents 'MatchSortings'.
 */
public class MatchSorting extends MatchSortingHistory  {

        protected double _amount;
        protected int _position;
        protected int _resultNumber;
        protected String _offerSorting;
        protected String _orderSorting;
        protected MatchSortingRun _matchSortingRun;

                            /**
     * Setter for the property 'amount'.
     */
    public void setAmount(double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     */
    public double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'position'.
     */
    public void setPosition(int position) {
        if (!_positionIsSet) {
            _positionIsSet = true;
            _positionInitVal = getPosition();
        }
        registerChange("position", _positionInitVal, position);
        _position = position;
    }
                        
    /**
     * Returns the property 'position'.
     */
    public int getPosition() {
        return _position;
    }
                                    /**
     * Setter for the property 'result number'.
     */
    public void setResultNumber(int resultNumber) {
        if (!_resultNumberIsSet) {
            _resultNumberIsSet = true;
            _resultNumberInitVal = getResultNumber();
        }
        registerChange("result number", _resultNumberInitVal, resultNumber);
        _resultNumber = resultNumber;
    }
                        
    /**
     * Returns the property 'result number'.
     */
    public int getResultNumber() {
        return _resultNumber;
    }
                                    /**
     * Setter for the property 'offer sorting'.
     */
    public void setOfferSorting(String offerSorting) {
        if (!_offerSortingIsSet) {
            _offerSortingIsSet = true;
            _offerSortingInitVal = getOfferSorting();
        }
        registerChange("offer sorting", _offerSortingInitVal, offerSorting);
        _offerSorting = offerSorting;
    }
                        
    /**
     * Returns the property 'offer sorting'.
     */
    public String getOfferSorting() {
        return _offerSorting;
    }
                                    /**
     * Setter for the property 'order sorting'.
     */
    public void setOrderSorting(String orderSorting) {
        if (!_orderSortingIsSet) {
            _orderSortingIsSet = true;
            _orderSortingInitVal = getOrderSorting();
        }
        registerChange("order sorting", _orderSortingInitVal, orderSorting);
        _orderSorting = orderSorting;
    }
                        
    /**
     * Returns the property 'order sorting'.
     */
    public String getOrderSorting() {
        return _orderSorting;
    }
                                            
    /**
     * Setter for the property 'match sorting run'.
     */
    public void setMatchSortingRun(MatchSortingRun matchSortingRun) {
        _matchSortingRun = matchSortingRun;
    }
            
    /**
     * Returns the property 'match sorting run'.
     */
    public MatchSortingRun getMatchSortingRun() {
        return _matchSortingRun;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MatchSorting.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _position=").append(_position);
            builder.append("\n    _resultNumber=").append(_resultNumber);
            builder.append("\n    _offerSorting=").append(_offerSorting);
            builder.append("\n    _orderSorting=").append(_orderSorting);
            builder.append("\n}");
        } else {
            builder.append(MatchSorting.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
