package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.CoBorrowerEconomicalDataEntityInterface;
import de.smava.webapp.account.domain.interfaces.EconomicalDataEntityInterface;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'CoBorrowerEconomicalDatas'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractCoBorrowerEconomicalData
    extends EconomicalData    implements CoBorrowerEconomicalDataEntityInterface    ,EconomicalDataEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractCoBorrowerEconomicalData.class);

}

