//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/2.35/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/2.35/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking)}

import de.smava.webapp.account.domain.BookingSumParameters;
import de.smava.webapp.account.domain.PayoutValuesContainer;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.Booking.PoolPerformanceBookingInformation;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Bookings'.
 *
 * @author generator
 */
public interface BookingDao extends BaseDao<Booking>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the booking identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    @Deprecated
	Booking getBooking(Long id);

    /**
     * Saves the booking.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    @Deprecated
	Long saveBooking(Booking booking);

    /**
     * Deletes an booking, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the booking
     */
    void deleteBooking(Long id);

    /**
     * Retrieves all 'Booking' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Booking> getBookingList();

    /**
     * Retrieves a page of 'Booking' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Booking> getBookingList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Booking' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Booking> getBookingList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Booking' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Booking> getBookingList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'Booking' instances.
     */
    long getBookingCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(booking)}
    //

    void evictAll();


    Collection<Booking> getBookingsForBankAccount(
            BankAccount bankAccount,
            Set<String> transactionStates,
            Set<BookingType> types);


    Booking findInvitationBooking(Account sender, Account recipient);

    Collection<Booking> findMarketingBonusBookings(Account account);
    

    Collection<Booking> getInternalBookingsForGroupAndContracts(BookingGroup bookingGroup, Collection<BankAccount> smavaInterimAccounts, Collection<Contract> contracts);
    Collection<Booking> getTargetBookingsForGroupAndContract(BookingGroup bookingGroup, Contract contract);




    Collection<PayoutValuesContainer> getPayoutValues(final List<BankAccount> bankAccounts, final List<BookingType> bookingTypes, final Date endDate, final boolean groupByAccount, BankAccount interimAccount, BankAccount provisionInterimAccount);


    double getAgioSum(Collection<String> transactionStates, Collection<BankAccount> debitAccounts, Collection<BankAccount> creditAccounts);

    double getBaseProvisionSum(Collection<String> transactionStates, Collection<BankAccount> debitAccounts, Collection<BankAccount> creditAccounts);
    Map<Long, Boolean> getIsTargetMap(BookingGroup bookingGroup, final String inIdsString);

    double getBookingSumForTransactions(final BookingType bookingType, List<Transaction> transactions);
    
    /**
     * Calculates the sum of all booking, matching the given conditions.
     * 
     * @return amount sum of all bookings
     */
    double getBookingSumBetweenAccountsAndTypes(BookingSumParameters parameters);
    


    Set<Booking> getCanceledBookingsForContract(final Contract contract, final Booking booking);

    Collection<Booking> getBookingsForContracts(final String string, final Set<Contract> contracts, final Collection<BookingType> bookingTypes, final Map<String, Object> params);


    public Collection<Booking> getPayoutBookings(final Set<Contract> contracts, BankAccount smavaInterimBankAccount, boolean considerConsolidation);

    /**
     * returns all invitation bonus bookings for a given account 
     * @param account
     * @return
     */
    public Collection<Booking> findInvitationBonusBookings(Account account);
    
    /**
     * returns all invitation bonus bookings for a given account and contract
     * @param account
     * @param contract
     * @return
     */
    public Collection<Booking> findInvitationBonusBookings(Account account, Contract contract);

    
    public List<PoolPerformanceBookingInformation> collectPoolPerformanceBookingInformation(BankAccountProvider bap, BookingGroup bookingGroup);


    public boolean hasRelevantInvitationTransaction(Account invitedAccount);

    Collection<Booking> findCurrentLenderPayoutAffectedBookings(Collection<Transaction> trxs);
    //
    // !!!!!!!! End of insert code section !!!!!!!!


}
