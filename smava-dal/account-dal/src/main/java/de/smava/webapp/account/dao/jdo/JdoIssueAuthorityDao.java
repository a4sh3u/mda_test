//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(issue authority)}

import de.smava.webapp.account.dao.IssueAuthorityDao;
import de.smava.webapp.account.domain.IssueAuthority;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'IssueAuthoritys'.
 *
 * @author generator
 */
@Repository(value = "issueAuthorityDao")
public class JdoIssueAuthorityDao extends JdoBaseDao implements IssueAuthorityDao {

    private static final Logger LOGGER = Logger.getLogger(JdoIssueAuthorityDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(issue authority)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the issue authority identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public IssueAuthority load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthority - start: id=" + id);
        }
        IssueAuthority result = getEntity(IssueAuthority.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthority - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public IssueAuthority getIssueAuthority(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	IssueAuthority entity = findUniqueEntity(IssueAuthority.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the issue authority.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(IssueAuthority issueAuthority) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveIssueAuthority: " + issueAuthority);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(issue authority)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(issueAuthority);
    }

    /**
     * @deprecated Use {@link #save(IssueAuthority) instead}
     */
    public Long saveIssueAuthority(IssueAuthority issueAuthority) {
        return save(issueAuthority);
    }

    /**
     * Deletes an issue authority, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the issue authority
     */
    public void deleteIssueAuthority(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteIssueAuthority: " + id);
        }
        deleteEntity(IssueAuthority.class, id);
    }

    /**
     * Retrieves all 'IssueAuthority' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<IssueAuthority> getIssueAuthorityList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - start");
        }
        Collection<IssueAuthority> result = getEntities(IssueAuthority.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'IssueAuthority' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<IssueAuthority> getIssueAuthorityList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - start: pageable=" + pageable);
        }
        Collection<IssueAuthority> result = getEntities(IssueAuthority.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IssueAuthority' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<IssueAuthority> getIssueAuthorityList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - start: sortable=" + sortable);
        }
        Collection<IssueAuthority> result = getEntities(IssueAuthority.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IssueAuthority' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<IssueAuthority> getIssueAuthorityList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<IssueAuthority> result = getEntities(IssueAuthority.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IssueAuthority' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IssueAuthority> findIssueAuthorityList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - start: whereClause=" + whereClause);
        }
        Collection<IssueAuthority> result = findEntities(IssueAuthority.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IssueAuthority' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<IssueAuthority> findIssueAuthorityList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<IssueAuthority> result = findEntities(IssueAuthority.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'IssueAuthority' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IssueAuthority> findIssueAuthorityList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<IssueAuthority> result = findEntities(IssueAuthority.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IssueAuthority' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IssueAuthority> findIssueAuthorityList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<IssueAuthority> result = findEntities(IssueAuthority.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IssueAuthority' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IssueAuthority> findIssueAuthorityList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<IssueAuthority> result = findEntities(IssueAuthority.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IssueAuthority' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IssueAuthority> findIssueAuthorityList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<IssueAuthority> result = findEntities(IssueAuthority.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIssueAuthorityList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'IssueAuthority' instances.
     */
    public long getIssueAuthorityCount() {
        long result = getEntityCount(IssueAuthority.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IssueAuthority' instances which match the given whereClause.
     */
    public long getIssueAuthorityCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(IssueAuthority.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IssueAuthority' instances which match the given whereClause together with params specified in object array.
     */
    public long getIssueAuthorityCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(IssueAuthority.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIssueAuthorityCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(issue authority)}
    //

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
