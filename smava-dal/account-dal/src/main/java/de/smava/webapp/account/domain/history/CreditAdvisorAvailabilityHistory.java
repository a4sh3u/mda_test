package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractCreditAdvisorAvailability;




/**
 * The domain object that has all history aggregation related fields for 'CreditAdvisorAvailabilitys'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CreditAdvisorAvailabilityHistory extends AbstractCreditAdvisorAvailability {

    protected transient double _availabilityInitVal;
    protected transient boolean _availabilityIsSet;


		
    /**
     * Returns the initial value of the property 'availability'.
     */
    public double availabilityInitVal() {
        double result;
        if (_availabilityIsSet) {
            result = _availabilityInitVal;
        } else {
            result = getAvailability();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'availability'.
     */
    public boolean availabilityIsDirty() {
        return !valuesAreEqual(availabilityInitVal(), getAvailability());
    }

    /**
     * Returns true if the setter method was called for the property 'availability'.
     */
    public boolean availabilityIsSet() {
        return _availabilityIsSet;
    }

}
