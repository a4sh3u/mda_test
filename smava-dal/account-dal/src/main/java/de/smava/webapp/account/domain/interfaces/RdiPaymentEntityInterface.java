package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.RdiContract;

import java.util.Date;


/**
 * The domain object that represents 'RdiPayments'.
 *
 * @author generator
 */
public interface RdiPaymentEntityInterface {

    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'contract'.
     *
     * 
     *
     */
    void setContract(RdiContract contract);

    /**
     * Returns the property 'contract'.
     *
     * 
     *
     */
    RdiContract getContract();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'from date'.
     *
     * 
     *
     */
    void setFromDate(Date fromDate);

    /**
     * Returns the property 'from date'.
     *
     * 
     *
     */
    Date getFromDate();
    /**
     * Setter for the property 'to date'.
     *
     * 
     *
     */
    void setToDate(Date toDate);

    /**
     * Returns the property 'to date'.
     *
     * 
     *
     */
    Date getToDate();

}
