package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.AccountRole;
import de.smava.webapp.account.domain.AccountRoleState;
import de.smava.webapp.account.domain.Approval;
import de.smava.webapp.account.domain.interfaces.AccountRoleEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.security.Role;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;

/**
 * The domain object that represents 'AccountRoles'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractAccountRole
    extends KreditPrivatEntity implements AccountRoleEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAccountRole.class);

    private static final long serialVersionUID = -8494036444196493848L;

    public AccountRoleState getCurrentAccountRoleState() {
        AccountRoleState result = null;

        if (getStates() != null) {
            for (AccountRoleState state : getStates()) {
                if (state.getValidUntil() == null) {
                    result = state;
                    break;
                } else {
                    if (result == null) {
                        result = state;
                    } else if (result.getValidUntil() != null && state.getValidUntil().after(result.getValidUntil())) {
                        result = state;
                    }
                }
            }

        }

        return result;
    }

    public Role getRole() {
        return Enum.valueOf(Role.class, getName());
    }

    public Approval getFirstApproval() {
        Approval result = null;

        if (getApprovals() != null) {
            for (Approval approval : getApprovals()) {
                if (result == null) {
                    result = approval;
                } else if (approval.getApprovalDate().before(result.getApprovalDate())) {
                    result = approval;
                }
            }
        }
        return result;
    }

    public Approval getLastApproval() {
        Approval result = null;

        if (getApprovals() != null) {
            for (Approval approval : getApprovals()) {
                if (result == null) {
                    result = approval;
                } else if (approval.getApprovalDate().after(result.getApprovalDate())) {
                    result = approval;
                }
            }
        }
        return result;
    }

    public void setCurrentAccountRoleState(AccountRoleState newState) {
        if (getStates() == null) {
            setStates(new HashSet<AccountRoleState>());
        }
        for (AccountRoleState state : getStates()) {
            if (state.getValidUntil() == null) {
                state.setValidUntil(new Date(newState.getValidFrom().getTime()));
            }
        }
        getStates().add(newState);
    }

    public abstract AccountRole instance();
}

