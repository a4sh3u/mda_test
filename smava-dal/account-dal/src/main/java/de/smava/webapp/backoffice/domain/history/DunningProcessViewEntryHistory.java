package de.smava.webapp.backoffice.domain.history;

import de.smava.webapp.backoffice.domain.abstracts.AbstractDunningProcessViewEntry;

import java.util.Date;

/**
 * The domain object that has all history aggregation related fields for 'DunningProcessViewEntrys'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DunningProcessViewEntryHistory extends AbstractDunningProcessViewEntry {

    protected transient String _rkvInitVal;
    protected transient boolean _rkvIsSet;
    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient Date _startDateInitVal;
    protected transient boolean _startDateIsSet;
    protected transient double _transactionAmountInitVal;
    protected transient boolean _transactionAmountIsSet;
    protected transient Date _transactionDueDateInitVal;
    protected transient boolean _transactionDueDateIsSet;
    protected transient String _userNameInitVal;
    protected transient boolean _userNameIsSet;
    protected transient String _creditTermInitVal;
    protected transient boolean _creditTermIsSet;
    protected transient String _ratingInitVal;
    protected transient boolean _ratingIsSet;
    protected transient String _occupationInitVal;
    protected transient boolean _occupationIsSet;


    /**
     * Returns the initial value of the property 'rkv'.
     */
    public String rkvInitVal() {
        String result;
        if (_rkvIsSet) {
            result = _rkvInitVal;
        } else {
            result = getRkv();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rkv'.
     */
    public boolean rkvIsDirty() {
        return !valuesAreEqual(rkvInitVal(), getRkv());
    }

    /**
     * Returns true if the setter method was called for the property 'rkv'.
     */
    public boolean rkvIsSet() {
        return _rkvIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'start date'.
     */
    public Date startDateInitVal() {
        Date result;
        if (_startDateIsSet) {
            result = _startDateInitVal;
        } else {
            result = getStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start date'.
     */
    public boolean startDateIsDirty() {
        return !valuesAreEqual(startDateInitVal(), getStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'start date'.
     */
    public boolean startDateIsSet() {
        return _startDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'transaction amount'.
     */
    public double transactionAmountInitVal() {
        double result;
        if (_transactionAmountIsSet) {
            result = _transactionAmountInitVal;
        } else {
            result = getTransactionAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'transaction amount'.
     */
    public boolean transactionAmountIsDirty() {
        return !valuesAreEqual(transactionAmountInitVal(), getTransactionAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'transaction amount'.
     */
    public boolean transactionAmountIsSet() {
        return _transactionAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'transaction due date'.
     */
    public Date transactionDueDateInitVal() {
        Date result;
        if (_transactionDueDateIsSet) {
            result = _transactionDueDateInitVal;
        } else {
            result = getTransactionDueDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'transaction due date'.
     */
    public boolean transactionDueDateIsDirty() {
        return !valuesAreEqual(transactionDueDateInitVal(), getTransactionDueDate());
    }

    /**
     * Returns true if the setter method was called for the property 'transaction due date'.
     */
    public boolean transactionDueDateIsSet() {
        return _transactionDueDateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'user name'.
     */
    public String userNameInitVal() {
        String result;
        if (_userNameIsSet) {
            result = _userNameInitVal;
        } else {
            result = getUserName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'user name'.
     */
    public boolean userNameIsDirty() {
        return !valuesAreEqual(userNameInitVal(), getUserName());
    }

    /**
     * Returns true if the setter method was called for the property 'user name'.
     */
    public boolean userNameIsSet() {
        return _userNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit term'.
     */
    public String creditTermInitVal() {
        String result;
        if (_creditTermIsSet) {
            result = _creditTermInitVal;
        } else {
            result = getCreditTerm();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit term'.
     */
    public boolean creditTermIsDirty() {
        return !valuesAreEqual(creditTermInitVal(), getCreditTerm());
    }

    /**
     * Returns true if the setter method was called for the property 'credit term'.
     */
    public boolean creditTermIsSet() {
        return _creditTermIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rating'.
     */
    public String ratingInitVal() {
        String result;
        if (_ratingIsSet) {
            result = _ratingInitVal;
        } else {
            result = getRating();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rating'.
     */
    public boolean ratingIsDirty() {
        return !valuesAreEqual(ratingInitVal(), getRating());
    }

    /**
     * Returns true if the setter method was called for the property 'rating'.
     */
    public boolean ratingIsSet() {
        return _ratingIsSet;
    }
		
    /**
     * Returns the initial value of the property 'occupation'.
     */
    public String occupationInitVal() {
        String result;
        if (_occupationIsSet) {
            result = _occupationInitVal;
        } else {
            result = getOccupation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'occupation'.
     */
    public boolean occupationIsDirty() {
        return !valuesAreEqual(occupationInitVal(), getOccupation());
    }

    /**
     * Returns true if the setter method was called for the property 'occupation'.
     */
    public boolean occupationIsSet() {
        return _occupationIsSet;
    }

}
