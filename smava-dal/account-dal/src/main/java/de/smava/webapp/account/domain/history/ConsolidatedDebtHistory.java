package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractConsolidatedDebt;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ConsolidatedDebts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ConsolidatedDebtHistory extends AbstractConsolidatedDebt {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _loanIdInitVal;
    protected transient boolean _loanIdIsSet;
    protected transient Date _dueDateInitVal;
    protected transient boolean _dueDateIsSet;
    protected transient Date _startDateInitVal;
    protected transient boolean _startDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _creditTypeInitVal;
    protected transient boolean _creditTypeIsSet;
    protected transient boolean _consolidationInitVal;
    protected transient boolean _consolidationIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'loan id'.
     */
    public String loanIdInitVal() {
        String result;
        if (_loanIdIsSet) {
            result = _loanIdInitVal;
        } else {
            result = getLoanId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'loan id'.
     */
    public boolean loanIdIsDirty() {
        return !valuesAreEqual(loanIdInitVal(), getLoanId());
    }

    /**
     * Returns true if the setter method was called for the property 'loan id'.
     */
    public boolean loanIdIsSet() {
        return _loanIdIsSet;
    }
		
    /**
     * Returns the initial value of the property 'due date'.
     */
    public Date dueDateInitVal() {
        Date result;
        if (_dueDateIsSet) {
            result = _dueDateInitVal;
        } else {
            result = getDueDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'due date'.
     */
    public boolean dueDateIsDirty() {
        return !valuesAreEqual(dueDateInitVal(), getDueDate());
    }

    /**
     * Returns true if the setter method was called for the property 'due date'.
     */
    public boolean dueDateIsSet() {
        return _dueDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'start date'.
     */
    public Date startDateInitVal() {
        Date result;
        if (_startDateIsSet) {
            result = _startDateInitVal;
        } else {
            result = getStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start date'.
     */
    public boolean startDateIsDirty() {
        return !valuesAreEqual(startDateInitVal(), getStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'start date'.
     */
    public boolean startDateIsSet() {
        return _startDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit type'.
     */
    public String creditTypeInitVal() {
        String result;
        if (_creditTypeIsSet) {
            result = _creditTypeInitVal;
        } else {
            result = getCreditType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit type'.
     */
    public boolean creditTypeIsDirty() {
        return !valuesAreEqual(creditTypeInitVal(), getCreditType());
    }

    /**
     * Returns true if the setter method was called for the property 'credit type'.
     */
    public boolean creditTypeIsSet() {
        return _creditTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'consolidation'.
     */
    public boolean consolidationInitVal() {
        boolean result;
        if (_consolidationIsSet) {
            result = _consolidationInitVal;
        } else {
            result = getConsolidation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'consolidation'.
     */
    public boolean consolidationIsDirty() {
        return !valuesAreEqual(consolidationInitVal(), getConsolidation());
    }

    /**
     * Returns true if the setter method was called for the property 'consolidation'.
     */
    public boolean consolidationIsSet() {
        return _consolidationIsSet;
    }
		
}
