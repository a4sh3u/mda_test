package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.CoBorrowerEconomicalDataHistory;

import java.util.Date;

/**
 * The domain object that represents 'CoBorrowerEconomicalDatas'.
 */
public class CoBorrowerEconomicalData extends CoBorrowerEconomicalDataHistory  {

        protected String _coBorrowerEmployment;
        protected double _coBorrowerIncome;
        protected Date _coBorrowerStartOfOccupation;
        protected boolean _coBorrowerTempEmployment;
        protected Date _coBorrowerEndOfTempEmployment;
        protected Sector _coBorrowerSector;
        protected String _coBorrowerSectorDescription;
        protected String _coBorrowerEmployerName;
        
                            /**
     * Setter for the property 'co borrower employment'.
     */
    public void setCoBorrowerEmployment(String coBorrowerEmployment) {
        if (!_coBorrowerEmploymentIsSet) {
            _coBorrowerEmploymentIsSet = true;
            _coBorrowerEmploymentInitVal = getCoBorrowerEmployment();
        }
        registerChange("co borrower employment", _coBorrowerEmploymentInitVal, coBorrowerEmployment);
        _coBorrowerEmployment = coBorrowerEmployment;
    }
                        
    /**
     * Returns the property 'co borrower employment'.
     */
    public String getCoBorrowerEmployment() {
        return _coBorrowerEmployment;
    }
                                    /**
     * Setter for the property 'co borrower income'.
     */
    public void setCoBorrowerIncome(double coBorrowerIncome) {
        if (!_coBorrowerIncomeIsSet) {
            _coBorrowerIncomeIsSet = true;
            _coBorrowerIncomeInitVal = getCoBorrowerIncome();
        }
        registerChange("co borrower income", _coBorrowerIncomeInitVal, coBorrowerIncome);
        _coBorrowerIncome = coBorrowerIncome;
    }
                        
    /**
     * Returns the property 'co borrower income'.
     */
    public double getCoBorrowerIncome() {
        return _coBorrowerIncome;
    }
                                    /**
     * Setter for the property 'co borrower start of occupation'.
     */
    public void setCoBorrowerStartOfOccupation(Date coBorrowerStartOfOccupation) {
        if (!_coBorrowerStartOfOccupationIsSet) {
            _coBorrowerStartOfOccupationIsSet = true;
            _coBorrowerStartOfOccupationInitVal = getCoBorrowerStartOfOccupation();
        }
        registerChange("co borrower start of occupation", _coBorrowerStartOfOccupationInitVal, coBorrowerStartOfOccupation);
        _coBorrowerStartOfOccupation = coBorrowerStartOfOccupation;
    }
                        
    /**
     * Returns the property 'co borrower start of occupation'.
     */
    public Date getCoBorrowerStartOfOccupation() {
        return _coBorrowerStartOfOccupation;
    }
                                    /**
     * Setter for the property 'co borrower temp employment'.
     */
    public void setCoBorrowerTempEmployment(boolean coBorrowerTempEmployment) {
        if (!_coBorrowerTempEmploymentIsSet) {
            _coBorrowerTempEmploymentIsSet = true;
            _coBorrowerTempEmploymentInitVal = getCoBorrowerTempEmployment();
        }
        registerChange("co borrower temp employment", _coBorrowerTempEmploymentInitVal, coBorrowerTempEmployment);
        _coBorrowerTempEmployment = coBorrowerTempEmployment;
    }
                        
    /**
     * Returns the property 'co borrower temp employment'.
     */
    public boolean getCoBorrowerTempEmployment() {
        return _coBorrowerTempEmployment;
    }
                                    /**
     * Setter for the property 'co borrower end of temp employment'.
     */
    public void setCoBorrowerEndOfTempEmployment(Date coBorrowerEndOfTempEmployment) {
        if (!_coBorrowerEndOfTempEmploymentIsSet) {
            _coBorrowerEndOfTempEmploymentIsSet = true;
            _coBorrowerEndOfTempEmploymentInitVal = getCoBorrowerEndOfTempEmployment();
        }
        registerChange("co borrower end of temp employment", _coBorrowerEndOfTempEmploymentInitVal, coBorrowerEndOfTempEmployment);
        _coBorrowerEndOfTempEmployment = coBorrowerEndOfTempEmployment;
    }
                        
    /**
     * Returns the property 'co borrower end of temp employment'.
     */
    public Date getCoBorrowerEndOfTempEmployment() {
        return _coBorrowerEndOfTempEmployment;
    }
                                    /**
     * Setter for the property 'co borrower sector'.
     */
    public void setCoBorrowerSector(Sector coBorrowerSector) {
        if (!_coBorrowerSectorIsSet) {
            _coBorrowerSectorIsSet = true;
            _coBorrowerSectorInitVal = getCoBorrowerSector();
        }
        registerChange("co borrower sector", _coBorrowerSectorInitVal, coBorrowerSector);
        _coBorrowerSector = coBorrowerSector;
    }
                        
    /**
     * Returns the property 'co borrower sector'.
     */
    public Sector getCoBorrowerSector() {
        return _coBorrowerSector;
    }
                                    /**
     * Setter for the property 'co borrower sector description'.
     */
    public void setCoBorrowerSectorDescription(String coBorrowerSectorDescription) {
        if (!_coBorrowerSectorDescriptionIsSet) {
            _coBorrowerSectorDescriptionIsSet = true;
            _coBorrowerSectorDescriptionInitVal = getCoBorrowerSectorDescription();
        }
        registerChange("co borrower sector description", _coBorrowerSectorDescriptionInitVal, coBorrowerSectorDescription);
        _coBorrowerSectorDescription = coBorrowerSectorDescription;
    }
                        
    /**
     * Returns the property 'co borrower sector description'.
     */
    public String getCoBorrowerSectorDescription() {
        return _coBorrowerSectorDescription;
    }
                                    /**
     * Setter for the property 'co borrower employer name'.
     */
    public void setCoBorrowerEmployerName(String coBorrowerEmployerName) {
        if (!_coBorrowerEmployerNameIsSet) {
            _coBorrowerEmployerNameIsSet = true;
            _coBorrowerEmployerNameInitVal = getCoBorrowerEmployerName();
        }
        registerChange("co borrower employer name", _coBorrowerEmployerNameInitVal, coBorrowerEmployerName);
        _coBorrowerEmployerName = coBorrowerEmployerName;
    }
                        
    /**
     * Returns the property 'co borrower employer name'.
     */
    public String getCoBorrowerEmployerName() {
        return _coBorrowerEmployerName;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CoBorrowerEconomicalData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _coBorrowerEmployment=").append(_coBorrowerEmployment);
            builder.append("\n    _coBorrowerIncome=").append(_coBorrowerIncome);
            builder.append("\n    _coBorrowerTempEmployment=").append(_coBorrowerTempEmployment);
            builder.append("\n    _coBorrowerSector=").append(_coBorrowerSector);
            builder.append("\n    _coBorrowerSectorDescription=").append(_coBorrowerSectorDescription);
            builder.append("\n    _coBorrowerEmployerName=").append(_coBorrowerEmployerName);
            builder.append("\n}");
        } else {
            builder.append(CoBorrowerEconomicalData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
