//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi payment)}

import de.smava.webapp.account.domain.RdiPayment;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.account.dao.RdiPaymentDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RdiPayments'.
 *
 * @author generator
 */
@Repository(value = "rdiPaymentDao")
public class JdoRdiPaymentDao extends JdoBaseDao implements RdiPaymentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRdiPaymentDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rdi payment)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the rdi payment identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RdiPayment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPayment - start: id=" + id);
        }
        RdiPayment result = getEntity(RdiPayment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPayment - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RdiPayment getRdiPayment(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RdiPayment entity = findUniqueEntity(RdiPayment.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the rdi payment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RdiPayment rdiPayment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRdiPayment: " + rdiPayment);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rdi payment)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(rdiPayment);
    }

    /**
     * @deprecated Use {@link #save(RdiPayment) instead}
     */
    public Long saveRdiPayment(RdiPayment rdiPayment) {
        return save(rdiPayment);
    }

    /**
     * Deletes an rdi payment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi payment
     */
    public void deleteRdiPayment(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRdiPayment: " + id);
        }
        deleteEntity(RdiPayment.class, id);
    }

    /**
     * Retrieves all 'RdiPayment' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RdiPayment> getRdiPaymentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - start");
        }
        Collection<RdiPayment> result = getEntities(RdiPayment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RdiPayment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RdiPayment> getRdiPaymentList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - start: pageable=" + pageable);
        }
        Collection<RdiPayment> result = getEntities(RdiPayment.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiPayment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RdiPayment> getRdiPaymentList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - start: sortable=" + sortable);
        }
        Collection<RdiPayment> result = getEntities(RdiPayment.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiPayment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RdiPayment> getRdiPaymentList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiPayment> result = getEntities(RdiPayment.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiPayment' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiPayment> findRdiPaymentList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - start: whereClause=" + whereClause);
        }
        Collection<RdiPayment> result = findEntities(RdiPayment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiPayment' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RdiPayment> findRdiPaymentList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RdiPayment> result = findEntities(RdiPayment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RdiPayment' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiPayment> findRdiPaymentList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RdiPayment> result = findEntities(RdiPayment.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiPayment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiPayment> findRdiPaymentList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RdiPayment> result = findEntities(RdiPayment.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiPayment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiPayment> findRdiPaymentList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiPayment> result = findEntities(RdiPayment.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiPayment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiPayment> findRdiPaymentList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiPayment> result = findEntities(RdiPayment.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RdiPayment' instances.
     */
    public long getRdiPaymentCount() {
        long result = getEntityCount(RdiPayment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiPayment' instances which match the given whereClause.
     */
    public long getRdiPaymentCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RdiPayment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiPayment' instances which match the given whereClause together with params specified in object array.
     */
    public long getRdiPaymentCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RdiPayment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiPaymentCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi payment)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
