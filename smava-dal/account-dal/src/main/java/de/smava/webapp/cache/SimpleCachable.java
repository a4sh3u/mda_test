package de.smava.webapp.cache;

import java.lang.annotation.*;

/**
 * <strong>NOTE!!</strong><br>
 * Only use it for methods with standard, immutable (String, Long etc) or nested standard 
 * (collection, array) objects as parameters and return values.<br>
 * <br>
 * Or with caution: Complex, NONE persistent and <strong>immutable</strong> objects which honer equal and hashCode.<br>
 * <strong>NEVER</strong> use it in combination with {@link Entity} classes! 
 * 
 * @author bvoss
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
@Inherited
public @interface SimpleCachable {

}
