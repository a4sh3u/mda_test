package de.smava.webapp.account.domain;

import java.util.Date;

/**
 * Interfcae for all enitites that are supposed to be tracked.
 * <p/>
 * Date: 08.11.2006
 *
 * @author ingmar.decker
 */
public interface MarketingTrackingEntity {

    Long getMarketingPlacementId();

    Date getCreationDate();

}
