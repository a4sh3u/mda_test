//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account provider)}

import de.smava.webapp.account.domain.BankAccountProvider;
import de.smava.webapp.account.domain.interfaces.BankAccountProviderEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankAccountProviders'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBankAccountProvider
    extends KreditPrivatEntity implements Comparable<BankAccountProvider>,BankAccountProviderEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBankAccountProvider.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(bank account provider)}
    private static final long serialVersionUID = 2712133859823365347L;
    public static final String BIW = "biw";
    public static final String FIDOR = "fidor";
    public static final String SMAVA = "smava";

    public static final Set<String> PROVIDERS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList("", BIW, FIDOR, SMAVA)));

    @Override
    public int compareTo(BankAccountProvider o) {
        if (null == o)
            return -1;
        if (null == getName())
            return null == o.getName()?0:1;
        return getName().compareTo(o.getName());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        BankAccountProvider other = (BankAccountProvider) obj;
        if (getName() == null) {
            if (other.getName() != null)
                return false;
        } else if (!getName().equals(other.getName()))
            return false;
        if (getType() != other.getType())
            return false;
        return true;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

