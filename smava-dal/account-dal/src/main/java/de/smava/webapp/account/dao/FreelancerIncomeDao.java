//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(freelancer income)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.EconomicalData;
import de.smava.webapp.account.domain.FreelancerIncome;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'FreelancerIncomes'.
 *
 * @author generator
 */
public interface FreelancerIncomeDao extends BaseDao<FreelancerIncome>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the freelancer income identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    FreelancerIncome getFreelancerIncome(Long id);

    /**
     * Saves the freelancer income.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveFreelancerIncome(FreelancerIncome freelancerIncome);

    /**
     * Deletes an freelancer income, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the freelancer income
     */
    void deleteFreelancerIncome(Long id);

    /**
     * Retrieves all 'FreelancerIncome' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<FreelancerIncome> getFreelancerIncomeList();

    /**
     * Retrieves a page of 'FreelancerIncome' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<FreelancerIncome> getFreelancerIncomeList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'FreelancerIncome' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<FreelancerIncome> getFreelancerIncomeList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'FreelancerIncome' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<FreelancerIncome> getFreelancerIncomeList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'FreelancerIncome' instances.
     */
    long getFreelancerIncomeCount();




    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(freelancer income)}
    //
    //

    public List<FreelancerIncome> getFreelancerIncomes(EconomicalData ed);

    public List<FreelancerIncome> getValidFreelancerIncomes(Account account);
    // !!!!!!!! End of insert code section !!!!!!!!
}
