package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The domain object that represents 'Orders'.
 *
 * @author generator
 */
public interface OrderEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'activation date'.
     *
     * 
     *
     */
    void setActivationDate(Date activationDate);

    /**
     * Returns the property 'activation date'.
     *
     * 
     *
     */
    Date getActivationDate();
    /**
     * Setter for the property 'credit term'.
     *
     * 
     *
     */
    void setCreditTerm(String creditTerm);

    /**
     * Returns the property 'credit term'.
     *
     * 
     *
     */
    String getCreditTerm();
    /**
     * Setter for the property 'image'.
     *
     * 
     *
     */
    void setImage(String image);

    /**
     * Returns the property 'image'.
     *
     * 
     *
     */
    String getImage();
    /**
     * Setter for the property 'interesting'.
     *
     * 
     *
     */
    void setInteresting(int interesting);

    /**
     * Returns the property 'interesting'.
     *
     * 
     *
     */
    int getInteresting();
    /**
     * Setter for the property 'image width'.
     *
     * 
     *
     */
    void setImageWidth(int imageWidth);

    /**
     * Returns the property 'image width'.
     *
     * 
     *
     */
    int getImageWidth();
    /**
     * Setter for the property 'image height'.
     *
     * 
     *
     */
    void setImageHeight(int imageHeight);

    /**
     * Returns the property 'image height'.
     *
     * 
     *
     */
    int getImageHeight();
    /**
     * Setter for the property 'marketing placement id'.
     *
     * 
     *
     */
    void setMarketingPlacementId(Long marketingPlacementId);

    /**
     * Returns the property 'marketing placement id'.
     *
     * 
     *
     */
    Long getMarketingPlacementId();
    /**
     * Setter for the property 'recommendations'.
     *
     * 
     *
     */
    void setRecommendations(List<OrderRecommendation> recommendations);

    /**
     * Returns the property 'recommendations'.
     *
     * 
     *
     */
    List<OrderRecommendation> getRecommendations();
    /**
     * Setter for the property 'rdi contracts'.
     *
     * 
     *
     */
    void setRdiContracts(Collection<RdiContract> rdiContracts);

    /**
     * Returns the property 'rdi contracts'.
     *
     * 
     *
     */
    Collection<RdiContract> getRdiContracts();
    /**
     * Setter for the property 'roi'.
     *
     * 
     *
     */
    void setRoi(double roi);

    /**
     * Returns the property 'roi'.
     *
     * 
     *
     */
    double getRoi();
    /**
     * Setter for the property 'matched rate'.
     *
     * 
     *
     */
    void setMatchedRate(double matchedRate);

    /**
     * Returns the property 'matched rate'.
     *
     * 
     *
     */
    double getMatchedRate();
    /**
     * Setter for the property 'category orders'.
     *
     * 
     *
     */
    void setCategoryOrders(Collection<CategoryOrder> categoryOrders);

    /**
     * Returns the property 'category orders'.
     *
     * 
     *
     */
    Collection<CategoryOrder> getCategoryOrders();
    /**
     * Setter for the property 'provision rate'.
     *
     * 
     *
     */
    void setProvisionRate(Double provisionRate);

    /**
     * Returns the property 'provision rate'.
     *
     * 
     *
     */
    Double getProvisionRate();
    /**
     * Setter for the property 'provision rate fix'.
     *
     * 
     *
     */
    void setProvisionRateFix(Double provisionRateFix);

    /**
     * Returns the property 'provision rate fix'.
     *
     * 
     *
     */
    Double getProvisionRateFix();
    /**
     * Setter for the property 'agreement blacklist check'.
     *
     * 
     *
     */
    void setAgreementBlacklistCheck(Date agreementBlacklistCheck);

    /**
     * Returns the property 'agreement blacklist check'.
     *
     * 
     *
     */
    Date getAgreementBlacklistCheck();
    /**
     * Setter for the property 'agreement transfer'.
     *
     * 
     *
     */
    void setAgreementTransfer(Date agreementTransfer);

    /**
     * Returns the property 'agreement transfer'.
     *
     * 
     *
     */
    Date getAgreementTransfer();
    /**
     * Setter for the property 'agreement tax commitment'.
     *
     * 
     *
     */
    void setAgreementTaxCommitment(Date agreementTaxCommitment);

    /**
     * Returns the property 'agreement tax commitment'.
     *
     * 
     *
     */
    Date getAgreementTaxCommitment();
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    void setDocuments(Collection<Document> documents);

    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    Collection<Document> getDocuments();
    /**
     * Setter for the property 'fast financing'.
     *
     * 
     *
     */
    void setFastFinancing(boolean fastFinancing);

    /**
     * Returns the property 'fast financing'.
     *
     * 
     *
     */
    boolean getFastFinancing();
    /**
     * Setter for the property 'borrower feedback'.
     *
     * 
     *
     */
    void setBorrowerFeedback(Set<Feedback> borrowerFeedback);

    /**
     * Returns the property 'borrower feedback'.
     *
     * 
     *
     */
    Set<Feedback> getBorrowerFeedback();
    /**
     * Setter for the property 'automatic discription'.
     *
     * 
     *
     */
    void setAutomaticDiscription(boolean automaticDiscription);

    /**
     * Returns the property 'automatic discription'.
     *
     * 
     *
     */
    boolean getAutomaticDiscription();
    /**
     * Setter for the property 'borrower promotion id'.
     *
     * 
     *
     */
    void setBorrowerPromotionId(Long borrowerPromotionId);

    /**
     * Returns the property 'borrower promotion id'.
     *
     * 
     *
     */
    Long getBorrowerPromotionId();
    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    void setSharedLoan(Boolean sharedLoan);

    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    Boolean getSharedLoan();
    /**
     * Setter for the property 'internal debt consolidation'.
     *
     * 
     *
     */
    void setInternalDebtConsolidation(boolean internalDebtConsolidation);

    /**
     * Returns the property 'internal debt consolidation'.
     *
     * 
     *
     */
    boolean getInternalDebtConsolidation();
    /**
     * Setter for the property 'bank account provider'.
     *
     * 
     *
     */
    void setBankAccountProvider(de.smava.webapp.account.domain.BankAccountProvider bankAccountProvider);

    /**
     * Returns the property 'bank account provider'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccountProvider getBankAccountProvider();
    /**
     * Setter for the property 'economical datas'.
     *
     * 
     *
     */
    void setEconomicalDatas(Collection<EconomicalData> economicalDatas);

    /**
     * Returns the property 'economical datas'.
     *
     * 
     *
     */
    Collection<EconomicalData> getEconomicalDatas();

}
