//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head11/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head11/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid search group)}

import de.smava.webapp.account.dao.BidSearchGroupDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BidSearchGroup;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BidSearchGroups'.
 *
 * @author generator
 */
@Repository(value = "bidSearchGroupDao")
public class JdoBidSearchGroupDao extends JdoBaseDao implements BidSearchGroupDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBidSearchGroupDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bid search group)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bid search group identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BidSearchGroup load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroup - start: id=" + id);
        }
        BidSearchGroup result = getEntity(BidSearchGroup.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroup - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BidSearchGroup getBidSearchGroup(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BidSearchGroup entity = findUniqueEntity(BidSearchGroup.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the bid search group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BidSearchGroup bidSearchGroup) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBidSearchGroup: " + bidSearchGroup);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bid search group)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bidSearchGroup);
    }

    /**
     * @deprecated Use {@link #save(BidSearchGroup) instead}
     */
    public Long saveBidSearchGroup(BidSearchGroup bidSearchGroup) {
        return save(bidSearchGroup);
    }

    /**
     * Deletes an bid search group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid search group
     */
    public void deleteBidSearchGroup(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBidSearchGroup: " + id);
        }
        deleteEntity(BidSearchGroup.class, id);
    }

    /**
     * Retrieves all 'BidSearchGroup' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BidSearchGroup> getBidSearchGroupList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - start");
        }
        Collection<BidSearchGroup> result = getEntities(BidSearchGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BidSearchGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BidSearchGroup> getBidSearchGroupList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - start: pageable=" + pageable);
        }
        Collection<BidSearchGroup> result = getEntities(BidSearchGroup.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidSearchGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BidSearchGroup> getBidSearchGroupList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - start: sortable=" + sortable);
        }
        Collection<BidSearchGroup> result = getEntities(BidSearchGroup.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearchGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BidSearchGroup> getBidSearchGroupList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearchGroup> result = getEntities(BidSearchGroup.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidSearchGroup' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchGroup> findBidSearchGroupList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - start: whereClause=" + whereClause);
        }
        Collection<BidSearchGroup> result = findEntities(BidSearchGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidSearchGroup' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BidSearchGroup> findBidSearchGroupList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BidSearchGroup> result = findEntities(BidSearchGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BidSearchGroup' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchGroup> findBidSearchGroupList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BidSearchGroup> result = findEntities(BidSearchGroup.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidSearchGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchGroup> findBidSearchGroupList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BidSearchGroup> result = findEntities(BidSearchGroup.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearchGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchGroup> findBidSearchGroupList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearchGroup> result = findEntities(BidSearchGroup.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidSearchGroup' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidSearchGroup> findBidSearchGroupList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidSearchGroup> result = findEntities(BidSearchGroup.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidSearchGroupList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearchGroup' instances.
     */
    public long getBidSearchGroupCount() {
        long result = getEntityCount(BidSearchGroup.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearchGroup' instances which match the given whereClause.
     */
    public long getBidSearchGroupCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BidSearchGroup.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidSearchGroup' instances which match the given whereClause together with params specified in object array.
     */
    public long getBidSearchGroupCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BidSearchGroup.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidSearchGroupCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid search group)}
    //
    
    @Override
    public Collection<BidSearchGroup> getBidSearchPortfolios(Account account) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("account", account);
        params.put("type", BidSearchGroup.TYPE_PORTFOLIO);
        Collection<BidSearchGroup> result = findByNamedQuery(BidSearchGroup.class, "findBidSearchPortfoliosByAccount", params);
        return result;
    }
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
