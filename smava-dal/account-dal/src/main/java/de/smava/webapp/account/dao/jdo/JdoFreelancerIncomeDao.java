//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(freelancer income)}

import de.smava.webapp.account.dao.FreelancerIncomeDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.EconomicalData;
import de.smava.webapp.account.domain.FreelancerIncome;
import de.smava.webapp.commons.dao.DefaultSortable;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'FreelancerIncomes'.
 *
 * @author generator
 */
@Repository(value = "freelancerIncomeDao")
public class JdoFreelancerIncomeDao extends JdoBaseDao implements FreelancerIncomeDao {

    private static final Logger LOGGER = Logger.getLogger(JdoFreelancerIncomeDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(freelancer income)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the freelancer income identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public FreelancerIncome load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncome - start: id=" + id);
        }
        FreelancerIncome result = getEntity(FreelancerIncome.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncome - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public FreelancerIncome getFreelancerIncome(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	FreelancerIncome entity = findUniqueEntity(FreelancerIncome.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the freelancer income.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(FreelancerIncome freelancerIncome) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveFreelancerIncome: " + freelancerIncome);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(freelancer income)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(freelancerIncome);
    }

    /**
     * @deprecated Use {@link #save(FreelancerIncome) instead}
     */
    public Long saveFreelancerIncome(FreelancerIncome freelancerIncome) {
        return save(freelancerIncome);
    }

    /**
     * Deletes an freelancer income, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the freelancer income
     */
    public void deleteFreelancerIncome(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteFreelancerIncome: " + id);
        }
        deleteEntity(FreelancerIncome.class, id);
    }

    /**
     * Retrieves all 'FreelancerIncome' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<FreelancerIncome> getFreelancerIncomeList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - start");
        }
        Collection<FreelancerIncome> result = getEntities(FreelancerIncome.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'FreelancerIncome' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<FreelancerIncome> getFreelancerIncomeList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - start: pageable=" + pageable);
        }
        Collection<FreelancerIncome> result = getEntities(FreelancerIncome.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'FreelancerIncome' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<FreelancerIncome> getFreelancerIncomeList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - start: sortable=" + sortable);
        }
        Collection<FreelancerIncome> result = getEntities(FreelancerIncome.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'FreelancerIncome' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<FreelancerIncome> getFreelancerIncomeList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<FreelancerIncome> result = getEntities(FreelancerIncome.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'FreelancerIncome' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<FreelancerIncome> findFreelancerIncomeList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - start: whereClause=" + whereClause);
        }
        Collection<FreelancerIncome> result = findEntities(FreelancerIncome.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'FreelancerIncome' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<FreelancerIncome> findFreelancerIncomeList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<FreelancerIncome> result = findEntities(FreelancerIncome.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'FreelancerIncome' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<FreelancerIncome> findFreelancerIncomeList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<FreelancerIncome> result = findEntities(FreelancerIncome.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'FreelancerIncome' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<FreelancerIncome> findFreelancerIncomeList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<FreelancerIncome> result = findEntities(FreelancerIncome.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'FreelancerIncome' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<FreelancerIncome> findFreelancerIncomeList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<FreelancerIncome> result = findEntities(FreelancerIncome.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'FreelancerIncome' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<FreelancerIncome> findFreelancerIncomeList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<FreelancerIncome> result = findEntities(FreelancerIncome.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findFreelancerIncomeList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'FreelancerIncome' instances.
     */
    public long getFreelancerIncomeCount() {
        long result = getEntityCount(FreelancerIncome.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'FreelancerIncome' instances which match the given whereClause.
     */
    public long getFreelancerIncomeCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(FreelancerIncome.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'FreelancerIncome' instances which match the given whereClause together with params specified in object array.
     */
    public long getFreelancerIncomeCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(FreelancerIncome.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getFreelancerIncomeCount() - result: count=" + result);
        }
        return result;
    }

    @Override
    public List<FreelancerIncome> getFreelancerIncomes(EconomicalData ed) {
        List<FreelancerIncome> result = new ArrayList<FreelancerIncome>();
        if (ed != null) {
            DefaultSortable sortable = new DefaultSortable("_expirationDate", true);
            Collection<FreelancerIncome> queryResult = findFreelancerIncomeList("_economicalData._id == " + ed.getId(), sortable);
            result.addAll(queryResult);
        }


        return result;
    }


    @Override
    public List<FreelancerIncome> getValidFreelancerIncomes(Account account) {
        List<FreelancerIncome> result = new ArrayList<FreelancerIncome>();
        if (account != null) {
            if (account.getEconomicalData() != null) {
                Collection<FreelancerIncome> queryResult = findFreelancerIncomeList("_economicalData != null && _economicalData._id == " + account.getEconomicalData().getId() + " && _expirationDate == null ORDER BY _year DESC");
                result.addAll(queryResult);
            }
        }

        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(freelancer income)}
    //    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
