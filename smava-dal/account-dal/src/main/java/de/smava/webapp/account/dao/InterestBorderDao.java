//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse-jee-ganymede\eclipse\workspace\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse-jee-ganymede\eclipse\workspace\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(interest border)}

import de.smava.webapp.account.domain.InterestBorder;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'InterestBorders'.
 *
 * @author generator
 */
public interface InterestBorderDao extends BaseDao<InterestBorder>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the interest border identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    InterestBorder getInterestBorder(Long id);

    /**
     * Saves the interest border.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveInterestBorder(InterestBorder interestBorder);

    /**
     * Deletes an interest border, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the interest border
     */
    void deleteInterestBorder(Long id);

    /**
     * Retrieves all 'InterestBorder' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<InterestBorder> getInterestBorderList();

    /**
     * Retrieves a page of 'InterestBorder' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<InterestBorder> getInterestBorderList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'InterestBorder' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<InterestBorder> getInterestBorderList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'InterestBorder' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<InterestBorder> getInterestBorderList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'InterestBorder' instances.
     */
    long getInterestBorderCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(interest border)}
    //

	
	InterestBorder getValuesForMarket(String rating, int duration, boolean adjusted);

    Collection<InterestBorder> getInterestBorderValues(String rating, int duration, boolean adjusted);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
