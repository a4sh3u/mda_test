//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head15/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head15/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(repayment break detail)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.RepaymentBreakDetail;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RepaymentBreakDetails'.
 *
 * @author generator
 */
public interface RepaymentBreakDetailDao extends BaseDao<RepaymentBreakDetail>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the repayment break detail identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RepaymentBreakDetail getRepaymentBreakDetail(Long id);

    /**
     * Saves the repayment break detail.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRepaymentBreakDetail(RepaymentBreakDetail repaymentBreakDetail);

    /**
     * Deletes an repayment break detail, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the repayment break detail
     */
    void deleteRepaymentBreakDetail(Long id);

    /**
     * Retrieves all 'RepaymentBreakDetail' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RepaymentBreakDetail> getRepaymentBreakDetailList();

    /**
     * Retrieves a page of 'RepaymentBreakDetail' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RepaymentBreakDetail> getRepaymentBreakDetailList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RepaymentBreakDetail' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RepaymentBreakDetail> getRepaymentBreakDetailList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RepaymentBreakDetail' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RepaymentBreakDetail> getRepaymentBreakDetailList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'RepaymentBreakDetail' instances.
     */
    long getRepaymentBreakDetailCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(repayment break detail)}
    //
    // insert custom methods here

    public List<RepaymentBreakDetail> getDetailsForBreakId(final Long repaymentBreakId);


    public RepaymentBreakDetail getLatestForAccount(Account account);

    public Collection<RepaymentBreakDetail> getRepaymentBreakDetails(Long repaymentBreakId);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
