package de.smava.webapp.account.domain;

public enum PayoutModeType {
	REFERENCE, KOK;
}
