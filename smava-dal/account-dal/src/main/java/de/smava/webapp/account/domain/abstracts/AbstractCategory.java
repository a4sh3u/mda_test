package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.CategoryEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The domain object that represents 'Categorys'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractCategory
    extends KreditPrivatEntity implements ImageAwareEntity,CategoryEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCategory.class);

    private static final long serialVersionUID = -2249527886140299457L;

    public static final String CATEGORY_NAME_OTHER = "Sonstiges";
    public static final String CATEGORY_NAME_CAR = "Auto / Motorrad";
    public static final String CATEGORY_NAME_HOME = "Wohnen";
    public static final String CATEGORY_NAME_HOME_MODERNISATION = "Modernisieung am eigenen Objekt";
    public static final String CATEGORY_NAME_HOME_RENOVATION = "An-Umbau am eigenen Objekt";
    public static final String CATEGORY_NAME_HOME_RENOVATION_AGE= "Altersgerechter Umbau am eigenen Objekt";
    public static final String CATEGORY_NAME_HOME_RENOVATION_ENERGY = "Energetische Maßnahme am eigenen Objekt";
    public static final String CATEGORY_NAME_HOME_ADDITION_GARAGE_CARPORT = "Anbau Garage/Carport am eigenen Objekt";
    public static final String CATEGORY_NAME_CONSOLIDATION = "Umschuldung / Ablösung";
    public static final String CATEGORY_NAME_TRADE = "Gewerbe";
    public static final String CATEGORY_NAME_ANTIQUES = "Antiquitäten & Kunst";
    public static final String CATEGORY_NAME_EDUCATION = "Aus- & Weiterbildung";
    public static final String CATEGORY_NAME_INVESTMENTS = "Betriebsmittelinvestition / Investition";
    public static final String CATEGORY_NAME_FAMILY = "Familie & Erziehung";
    public static final String CATEGORY_NAME_CELEBRATION = "Feierlichkeiten & besondere Anlässe";
    public static final String CATEGORY_NAME_FREE_USAGE = "Freie Verwendung";
    public static final String CATEGORY_NAME_COMMUNITY_SERVICE = "Gemeinnützige Projekte";
    public static final String CATEGORY_NAME_HEALTH = "Gesundheit & Lifestyle";
    public static final String CATEGORY_NAME_COMMERCIAL_CREDIT = "Gewerblicher Kreditbedarf";
    public static final String CATEGORY_NAME_COMPENSATE_OVERDRAFT = "Liquidität";
    public static final String CATEGORY_NAME_TRAVEL = "Reisen / Urlaub";
    public static final String CATEGORY_NAME_FREE_USE = "Sammeln & Seltenes";
    public static final String CATEGORY_NAME_PC_AUDIO_VIDEO = "Unterhaltungselektronik & Technik";
    public static final String CATEGORY_NAME_MAINTENANCE = "Sport & Freizeit";
    public static final String CATEGORY_NAME_WILDLIFE = "Tierwelt";
    public static final String CATEGORY_NAME_TARGOBANK_SPECIAL = "Targobank Sonderkontingent";
    public static final String CATEGORY_NAME_BALLOON_FINANCING = "Ballonfinanzierung";
    public static final String CATEGORY_NAME_GLOBAL_CREDIT = "Rahmenkredit";

    public String getFullImagePath(String imagePath) {
        return imagePath + "/" + getId();
    }

    public Set<Order> getOrders(String state) {
        Set<Order> set = new HashSet<Order>();
        if (getOrders() != null) {
            for (Order order : getOrders()) {
                if (state == null || order.getState().equals(state)) {
                    set.add(order);
                }
            }
        }
        return set;
    }

    public Set<Order> getOrders(Collection<String> states) {
        Set<Order> set = new HashSet<Order>();
        if (getOrders() != null) {
            for (Order order : getOrders()) {
                if (states == null || states.contains(order.getState())) {
                    set.add(order);
                }
            }
        }
        return set;
    }

    public String getPath() {
        String path = getName();
        if (getParent() != null) {
            path = getParent().getPath() + "/" + path;
        }
        return path;
    }

    public Collection<Order> getOrders() {
        Collection<Order> result = new HashSet<Order>();
        Collection<CategoryOrder> categoryOrders = getCategoryOrders();
        for (CategoryOrder categoryOrder : categoryOrders) {
            Order order = categoryOrder.getOrder();
            if (order != null) {
                result.add(order);
            }
        }

        return result;
    }

    public void setOrders(Set<Order> orders) {
        Collection<CategoryOrder> categoryOrders = new HashSet<CategoryOrder>();
        for (Order order : orders) {
            CategoryOrder categoryOrder = new CategoryOrder();
            categoryOrder.setCategory(this.instance());
            categoryOrder.setOrder(order);

            categoryOrders.add(categoryOrder);
        }
        setCategoryOrders(categoryOrders);
    }

    public abstract Category instance();
}

