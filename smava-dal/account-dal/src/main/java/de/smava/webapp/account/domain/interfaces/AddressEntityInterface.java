package de.smava.webapp.account.domain.interfaces;


import java.util.Date;


/**
 * The domain object that represents 'Addresss'.
 *
 * @author generator
 */
public interface AddressEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'street'.
     *
     * 
     *
     */
    void setStreet(String street);

    /**
     * Returns the property 'street'.
     *
     * 
     *
     */
    String getStreet();
    /**
     * Setter for the property 'street number'.
     *
     * 
     *
     */
    void setStreetNumber(String streetNumber);

    /**
     * Returns the property 'street number'.
     *
     * 
     *
     */
    String getStreetNumber();
    /**
     * Setter for the property 'zip code'.
     *
     * 
     *
     */
    void setZipCode(String zipCode);

    /**
     * Returns the property 'zip code'.
     *
     * 
     *
     */
    String getZipCode();
    /**
     * Setter for the property 'city'.
     *
     * 
     *
     */
    void setCity(String city);

    /**
     * Returns the property 'city'.
     *
     * 
     *
     */
    String getCity();
    /**
     * Setter for the property 'country'.
     *
     * 
     *
     */
    void setCountry(String country);

    /**
     * Returns the property 'country'.
     *
     * 
     *
     */
    String getCountry();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'living there since'.
     *
     * 
     *
     */
    void setLivingThereSince(Date livingThereSince);

    /**
     * Returns the property 'living there since'.
     *
     * 
     *
     */
    Date getLivingThereSince();

}
