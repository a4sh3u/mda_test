package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractProfile;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Profiles'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ProfileHistory extends AbstractProfile {

    protected transient String _hobbiesInitVal;
    protected transient boolean _hobbiesIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient String _imageInitVal;
    protected transient boolean _imageIsSet;
    protected transient int _imageWidthInitVal;
    protected transient boolean _imageWidthIsSet;
    protected transient int _imageHeightInitVal;
    protected transient boolean _imageHeightIsSet;
    protected transient Date _changeDateInitVal;
    protected transient boolean _changeDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;


	
    /**
     * Returns the initial value of the property 'hobbies'.
     */
    public String hobbiesInitVal() {
        String result;
        if (_hobbiesIsSet) {
            result = _hobbiesInitVal;
        } else {
            result = getHobbies();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'hobbies'.
     */
    public boolean hobbiesIsDirty() {
        return !valuesAreEqual(hobbiesInitVal(), getHobbies());
    }

    /**
     * Returns true if the setter method was called for the property 'hobbies'.
     */
    public boolean hobbiesIsSet() {
        return _hobbiesIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image'.
     */
    public String imageInitVal() {
        String result;
        if (_imageIsSet) {
            result = _imageInitVal;
        } else {
            result = getImage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image'.
     */
    public boolean imageIsDirty() {
        return !valuesAreEqual(imageInitVal(), getImage());
    }

    /**
     * Returns true if the setter method was called for the property 'image'.
     */
    public boolean imageIsSet() {
        return _imageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image width'.
     */
    public int imageWidthInitVal() {
        int result;
        if (_imageWidthIsSet) {
            result = _imageWidthInitVal;
        } else {
            result = getImageWidth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image width'.
     */
    public boolean imageWidthIsDirty() {
        return !valuesAreEqual(imageWidthInitVal(), getImageWidth());
    }

    /**
     * Returns true if the setter method was called for the property 'image width'.
     */
    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image height'.
     */
    public int imageHeightInitVal() {
        int result;
        if (_imageHeightIsSet) {
            result = _imageHeightInitVal;
        } else {
            result = getImageHeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image height'.
     */
    public boolean imageHeightIsDirty() {
        return !valuesAreEqual(imageHeightInitVal(), getImageHeight());
    }

    /**
     * Returns true if the setter method was called for the property 'image height'.
     */
    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }
		
    /**
     * Returns the initial value of the property 'change date'.
     */
    public Date changeDateInitVal() {
        Date result;
        if (_changeDateIsSet) {
            result = _changeDateInitVal;
        } else {
            result = getChangeDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'change date'.
     */
    public boolean changeDateIsDirty() {
        return !valuesAreEqual(changeDateInitVal(), getChangeDate());
    }

    /**
     * Returns true if the setter method was called for the property 'change date'.
     */
    public boolean changeDateIsSet() {
        return _changeDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }

}
