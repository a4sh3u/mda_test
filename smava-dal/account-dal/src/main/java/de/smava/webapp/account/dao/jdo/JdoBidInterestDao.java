//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid interest)}

import de.smava.webapp.account.dao.BidInterestDao;
import de.smava.webapp.account.domain.BidInterest;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BidInterests'.
 *
 * @author generator
 */
@Repository(value = "bidInterestDao")
public class JdoBidInterestDao extends JdoBaseDao implements BidInterestDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBidInterestDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bid interest)}

    private static final long serialVersionUID = -3799085686881785581L;

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bid interest identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BidInterest load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterest - start: id=" + id);
        }
        BidInterest result = getEntity(BidInterest.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterest - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BidInterest getBidInterest(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            BidInterest entity = findUniqueEntity(BidInterest.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the bid interest.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BidInterest bidInterest) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBidInterest: " + bidInterest);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bid interest)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bidInterest);
    }

    /**
     * @deprecated Use {@link #save(BidInterest) instead}
     */
    public Long saveBidInterest(BidInterest bidInterest) {
        return save(bidInterest);
    }

    /**
     * Deletes an bid interest, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid interest
     */
    public void deleteBidInterest(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBidInterest: " + id);
        }
        deleteEntity(BidInterest.class, id);
    }

    /**
     * Retrieves all 'BidInterest' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BidInterest> getBidInterestList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - start");
        }
        Collection<BidInterest> result = getEntities(BidInterest.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BidInterest' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BidInterest> getBidInterestList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - start: pageable=" + pageable);
        }
        Collection<BidInterest> result = getEntities(BidInterest.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidInterest' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BidInterest> getBidInterestList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - start: sortable=" + sortable);
        }
        Collection<BidInterest> result = getEntities(BidInterest.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidInterest' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BidInterest> getBidInterestList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidInterest> result = getEntities(BidInterest.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidInterest' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidInterest> findBidInterestList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - start: whereClause=" + whereClause);
        }
        Collection<BidInterest> result = findEntities(BidInterest.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BidInterest' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BidInterest> findBidInterestList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BidInterest> result = findEntities(BidInterest.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BidInterest' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidInterest> findBidInterestList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BidInterest> result = findEntities(BidInterest.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BidInterest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidInterest> findBidInterestList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BidInterest> result = findEntities(BidInterest.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidInterest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidInterest> findBidInterestList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidInterest> result = findEntities(BidInterest.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BidInterest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BidInterest> findBidInterestList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BidInterest> result = findEntities(BidInterest.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidInterestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BidInterest' instances.
     */
    public long getBidInterestCount() {
        long result = getEntityCount(BidInterest.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidInterest' instances which match the given whereClause.
     */
    public long getBidInterestCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BidInterest.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BidInterest' instances which match the given whereClause together with params specified in object array.
     */
    public long getBidInterestCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BidInterest.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidInterestCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid interest)}
    //
    // insert custom methods here
    //

    /** {@inheritDoc} */
    @Override
    public BidInterest getBidInterestAtActivation(Long bidId, String market, Date activationDate) {
        Query query = getPersistenceManager().newNamedQuery(BidInterest.class, "findBidInterestAtActivation");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("activationDate", activationDate);
        params.put("bidId", bidId);
        
        query.setOrdering("_creationDate DESC");
        
        
        // execute query
        @SuppressWarnings("unchecked")
        Collection<BidInterest> queryResult = (Collection<BidInterest>) query.executeWithMap(params);
        BidInterest result = null;
        if (!queryResult.isEmpty()) {
            result = queryResult.iterator().next();
        }
        return result;
    }

    
    private Map<String, Object> prepareActiveBidInterestForMarketParams(Long bidSearchId, String market) {
        final Map<String, Object> params = new HashMap<String, Object>(3);

        params.put("now", CurrentDate.getDate());
        params.put("bidId", bidSearchId);
        params.put("market", market);
        
        return params;
    }

    /** {@inheritDoc} */
    @Override
    public Set<BidInterest> getActiveBidInterests(final Long bidSearchId) {
        final Map<String, Object> params = prepareActiveBidInterestForMarketParams(bidSearchId, null);
        final Query query = getPersistenceManager().newNamedQuery(BidInterest.class, "getActiveBidInterestForMarket");
        final Collection<BidInterest> rawResult = (Collection<BidInterest>) query.executeWithMap(params);
        final Set<BidInterest> result = new LinkedHashSet<BidInterest>(rawResult);
        return result;
    }
    
    // !!!!!!!! End of insert code section !!!!!!!!
}
