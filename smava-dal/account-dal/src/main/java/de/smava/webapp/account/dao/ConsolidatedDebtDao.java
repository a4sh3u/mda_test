//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(consolidated debt)}

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.transaction.Synchronization;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.ConsolidatedDebt;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'ConsolidatedDebts'.
 *
 * @author generator
 */
public interface ConsolidatedDebtDao extends BaseDao<ConsolidatedDebt> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the consolidated debt identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    ConsolidatedDebt getConsolidatedDebt(Long id);

    /**
     * Saves the consolidated debt.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveConsolidatedDebt(ConsolidatedDebt consolidatedDebt);

    /**
     * Deletes an consolidated debt, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the consolidated debt
     */
    void deleteConsolidatedDebt(Long id);

    /**
     * Retrieves all 'ConsolidatedDebt' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<ConsolidatedDebt> getConsolidatedDebtList();

    /**
     * Retrieves a page of 'ConsolidatedDebt' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<ConsolidatedDebt> getConsolidatedDebtList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'ConsolidatedDebt' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<ConsolidatedDebt> getConsolidatedDebtList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'ConsolidatedDebt' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<ConsolidatedDebt> getConsolidatedDebtList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'ConsolidatedDebt' instances.
     */
    long getConsolidatedDebtCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(consolidated debt)}
    //
    // insert custom methods here
    //

    
    // !!!!!!!! End of insert code section !!!!!!!!
}
