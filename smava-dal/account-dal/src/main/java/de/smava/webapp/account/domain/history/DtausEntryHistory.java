package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractDtausEntry;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'DtausEntrys'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DtausEntryHistory extends AbstractDtausEntry {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _transactionDateInitVal;
    protected transient boolean _transactionDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _keyInitVal;
    protected transient boolean _keyIsSet;
    protected transient String _returnReasonInitVal;
    protected transient boolean _returnReasonIsSet;
    protected transient String _creditAccountNameInitVal;
    protected transient boolean _creditAccountNameIsSet;
    protected transient String _creditAccountBankCodeInitVal;
    protected transient boolean _creditAccountBankCodeIsSet;
    protected transient String _creditAccountBankInitVal;
    protected transient boolean _creditAccountBankIsSet;
    protected transient String _creditAccountNumberInitVal;
    protected transient boolean _creditAccountNumberIsSet;
    protected transient String _creditAccountIbanInitVal;
    protected transient boolean _creditAccountIbanIsSet;
    protected transient String _creditAccountBicInitVal;
    protected transient boolean _creditAccountBicIsSet;
    protected transient String _debitAccountNameInitVal;
    protected transient boolean _debitAccountNameIsSet;
    protected transient String _debitAccountBankCodeInitVal;
    protected transient boolean _debitAccountBankCodeIsSet;
    protected transient String _debitAccountBankInitVal;
    protected transient boolean _debitAccountBankIsSet;
    protected transient String _debitAccountNumberInitVal;
    protected transient boolean _debitAccountNumberIsSet;
    protected transient String _debitAccountIbanInitVal;
    protected transient boolean _debitAccountIbanIsSet;
    protected transient String _debitAccountBicInitVal;
    protected transient boolean _debitAccountBicIsSet;
    protected transient Date _debitNoteDateInitVal;
    protected transient boolean _debitNoteDateIsSet;
    protected transient String _mandateIdInitVal;
    protected transient boolean _mandateIdIsSet;
    protected transient String _erefInitVal;
    protected transient boolean _erefIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient String _relatedTransactionIdListInitVal;
    protected transient boolean _relatedTransactionIdListIsSet;
    protected transient int _dtausIndexInitVal;
    protected transient boolean _dtausIndexIsSet;
    protected transient int _dtausTransactionIndexInitVal;
    protected transient boolean _dtausTransactionIndexIsSet;
    protected transient String _errorCodeInitVal;
    protected transient boolean _errorCodeIsSet;
    protected transient String _externalTransactionIdInitVal;
    protected transient boolean _externalTransactionIdIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'transaction date'.
     */
    public Date transactionDateInitVal() {
        Date result;
        if (_transactionDateIsSet) {
            result = _transactionDateInitVal;
        } else {
            result = getTransactionDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'transaction date'.
     */
    public boolean transactionDateIsDirty() {
        return !valuesAreEqual(transactionDateInitVal(), getTransactionDate());
    }

    /**
     * Returns true if the setter method was called for the property 'transaction date'.
     */
    public boolean transactionDateIsSet() {
        return _transactionDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'key'.
     */
    public String keyInitVal() {
        String result;
        if (_keyIsSet) {
            result = _keyInitVal;
        } else {
            result = getKey();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'key'.
     */
    public boolean keyIsDirty() {
        return !valuesAreEqual(keyInitVal(), getKey());
    }

    /**
     * Returns true if the setter method was called for the property 'key'.
     */
    public boolean keyIsSet() {
        return _keyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'return reason'.
     */
    public String returnReasonInitVal() {
        String result;
        if (_returnReasonIsSet) {
            result = _returnReasonInitVal;
        } else {
            result = getReturnReason();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'return reason'.
     */
    public boolean returnReasonIsDirty() {
        return !valuesAreEqual(returnReasonInitVal(), getReturnReason());
    }

    /**
     * Returns true if the setter method was called for the property 'return reason'.
     */
    public boolean returnReasonIsSet() {
        return _returnReasonIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit account name'.
     */
    public String creditAccountNameInitVal() {
        String result;
        if (_creditAccountNameIsSet) {
            result = _creditAccountNameInitVal;
        } else {
            result = getCreditAccountName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit account name'.
     */
    public boolean creditAccountNameIsDirty() {
        return !valuesAreEqual(creditAccountNameInitVal(), getCreditAccountName());
    }

    /**
     * Returns true if the setter method was called for the property 'credit account name'.
     */
    public boolean creditAccountNameIsSet() {
        return _creditAccountNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit account bank code'.
     */
    public String creditAccountBankCodeInitVal() {
        String result;
        if (_creditAccountBankCodeIsSet) {
            result = _creditAccountBankCodeInitVal;
        } else {
            result = getCreditAccountBankCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit account bank code'.
     */
    public boolean creditAccountBankCodeIsDirty() {
        return !valuesAreEqual(creditAccountBankCodeInitVal(), getCreditAccountBankCode());
    }

    /**
     * Returns true if the setter method was called for the property 'credit account bank code'.
     */
    public boolean creditAccountBankCodeIsSet() {
        return _creditAccountBankCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit account bank'.
     */
    public String creditAccountBankInitVal() {
        String result;
        if (_creditAccountBankIsSet) {
            result = _creditAccountBankInitVal;
        } else {
            result = getCreditAccountBank();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit account bank'.
     */
    public boolean creditAccountBankIsDirty() {
        return !valuesAreEqual(creditAccountBankInitVal(), getCreditAccountBank());
    }

    /**
     * Returns true if the setter method was called for the property 'credit account bank'.
     */
    public boolean creditAccountBankIsSet() {
        return _creditAccountBankIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit account number'.
     */
    public String creditAccountNumberInitVal() {
        String result;
        if (_creditAccountNumberIsSet) {
            result = _creditAccountNumberInitVal;
        } else {
            result = getCreditAccountNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit account number'.
     */
    public boolean creditAccountNumberIsDirty() {
        return !valuesAreEqual(creditAccountNumberInitVal(), getCreditAccountNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'credit account number'.
     */
    public boolean creditAccountNumberIsSet() {
        return _creditAccountNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit account iban'.
     */
    public String creditAccountIbanInitVal() {
        String result;
        if (_creditAccountIbanIsSet) {
            result = _creditAccountIbanInitVal;
        } else {
            result = getCreditAccountIban();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit account iban'.
     */
    public boolean creditAccountIbanIsDirty() {
        return !valuesAreEqual(creditAccountIbanInitVal(), getCreditAccountIban());
    }

    /**
     * Returns true if the setter method was called for the property 'credit account iban'.
     */
    public boolean creditAccountIbanIsSet() {
        return _creditAccountIbanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit account bic'.
     */
    public String creditAccountBicInitVal() {
        String result;
        if (_creditAccountBicIsSet) {
            result = _creditAccountBicInitVal;
        } else {
            result = getCreditAccountBic();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit account bic'.
     */
    public boolean creditAccountBicIsDirty() {
        return !valuesAreEqual(creditAccountBicInitVal(), getCreditAccountBic());
    }

    /**
     * Returns true if the setter method was called for the property 'credit account bic'.
     */
    public boolean creditAccountBicIsSet() {
        return _creditAccountBicIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit account name'.
     */
    public String debitAccountNameInitVal() {
        String result;
        if (_debitAccountNameIsSet) {
            result = _debitAccountNameInitVal;
        } else {
            result = getDebitAccountName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit account name'.
     */
    public boolean debitAccountNameIsDirty() {
        return !valuesAreEqual(debitAccountNameInitVal(), getDebitAccountName());
    }

    /**
     * Returns true if the setter method was called for the property 'debit account name'.
     */
    public boolean debitAccountNameIsSet() {
        return _debitAccountNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit account bank code'.
     */
    public String debitAccountBankCodeInitVal() {
        String result;
        if (_debitAccountBankCodeIsSet) {
            result = _debitAccountBankCodeInitVal;
        } else {
            result = getDebitAccountBankCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit account bank code'.
     */
    public boolean debitAccountBankCodeIsDirty() {
        return !valuesAreEqual(debitAccountBankCodeInitVal(), getDebitAccountBankCode());
    }

    /**
     * Returns true if the setter method was called for the property 'debit account bank code'.
     */
    public boolean debitAccountBankCodeIsSet() {
        return _debitAccountBankCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit account bank'.
     */
    public String debitAccountBankInitVal() {
        String result;
        if (_debitAccountBankIsSet) {
            result = _debitAccountBankInitVal;
        } else {
            result = getDebitAccountBank();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit account bank'.
     */
    public boolean debitAccountBankIsDirty() {
        return !valuesAreEqual(debitAccountBankInitVal(), getDebitAccountBank());
    }

    /**
     * Returns true if the setter method was called for the property 'debit account bank'.
     */
    public boolean debitAccountBankIsSet() {
        return _debitAccountBankIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit account number'.
     */
    public String debitAccountNumberInitVal() {
        String result;
        if (_debitAccountNumberIsSet) {
            result = _debitAccountNumberInitVal;
        } else {
            result = getDebitAccountNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit account number'.
     */
    public boolean debitAccountNumberIsDirty() {
        return !valuesAreEqual(debitAccountNumberInitVal(), getDebitAccountNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'debit account number'.
     */
    public boolean debitAccountNumberIsSet() {
        return _debitAccountNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit account iban'.
     */
    public String debitAccountIbanInitVal() {
        String result;
        if (_debitAccountIbanIsSet) {
            result = _debitAccountIbanInitVal;
        } else {
            result = getDebitAccountIban();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit account iban'.
     */
    public boolean debitAccountIbanIsDirty() {
        return !valuesAreEqual(debitAccountIbanInitVal(), getDebitAccountIban());
    }

    /**
     * Returns true if the setter method was called for the property 'debit account iban'.
     */
    public boolean debitAccountIbanIsSet() {
        return _debitAccountIbanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit account bic'.
     */
    public String debitAccountBicInitVal() {
        String result;
        if (_debitAccountBicIsSet) {
            result = _debitAccountBicInitVal;
        } else {
            result = getDebitAccountBic();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit account bic'.
     */
    public boolean debitAccountBicIsDirty() {
        return !valuesAreEqual(debitAccountBicInitVal(), getDebitAccountBic());
    }

    /**
     * Returns true if the setter method was called for the property 'debit account bic'.
     */
    public boolean debitAccountBicIsSet() {
        return _debitAccountBicIsSet;
    }
	
    /**
     * Returns the initial value of the property 'debit note date'.
     */
    public Date debitNoteDateInitVal() {
        Date result;
        if (_debitNoteDateIsSet) {
            result = _debitNoteDateInitVal;
        } else {
            result = getDebitNoteDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debit note date'.
     */
    public boolean debitNoteDateIsDirty() {
        return !valuesAreEqual(debitNoteDateInitVal(), getDebitNoteDate());
    }

    /**
     * Returns true if the setter method was called for the property 'debit note date'.
     */
    public boolean debitNoteDateIsSet() {
        return _debitNoteDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'mandate id'.
     */
    public String mandateIdInitVal() {
        String result;
        if (_mandateIdIsSet) {
            result = _mandateIdInitVal;
        } else {
            result = getMandateId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'mandate id'.
     */
    public boolean mandateIdIsDirty() {
        return !valuesAreEqual(mandateIdInitVal(), getMandateId());
    }

    /**
     * Returns true if the setter method was called for the property 'mandate id'.
     */
    public boolean mandateIdIsSet() {
        return _mandateIdIsSet;
    }
	
    /**
     * Returns the initial value of the property 'eref'.
     */
    public String erefInitVal() {
        String result;
        if (_erefIsSet) {
            result = _erefInitVal;
        } else {
            result = getEref();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'eref'.
     */
    public boolean erefIsDirty() {
        return !valuesAreEqual(erefInitVal(), getEref());
    }

    /**
     * Returns true if the setter method was called for the property 'eref'.
     */
    public boolean erefIsSet() {
        return _erefIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'related transaction id list'.
     */
    public String relatedTransactionIdListInitVal() {
        String result;
        if (_relatedTransactionIdListIsSet) {
            result = _relatedTransactionIdListInitVal;
        } else {
            result = getRelatedTransactionIdList();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'related transaction id list'.
     */
    public boolean relatedTransactionIdListIsDirty() {
        return !valuesAreEqual(relatedTransactionIdListInitVal(), getRelatedTransactionIdList());
    }

    /**
     * Returns true if the setter method was called for the property 'related transaction id list'.
     */
    public boolean relatedTransactionIdListIsSet() {
        return _relatedTransactionIdListIsSet;
    }
		
    /**
     * Returns the initial value of the property 'dtaus index'.
     */
    public int dtausIndexInitVal() {
        int result;
        if (_dtausIndexIsSet) {
            result = _dtausIndexInitVal;
        } else {
            result = getDtausIndex();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'dtaus index'.
     */
    public boolean dtausIndexIsDirty() {
        return !valuesAreEqual(dtausIndexInitVal(), getDtausIndex());
    }

    /**
     * Returns true if the setter method was called for the property 'dtaus index'.
     */
    public boolean dtausIndexIsSet() {
        return _dtausIndexIsSet;
    }
	
    /**
     * Returns the initial value of the property 'dtaus transaction index'.
     */
    public int dtausTransactionIndexInitVal() {
        int result;
        if (_dtausTransactionIndexIsSet) {
            result = _dtausTransactionIndexInitVal;
        } else {
            result = getDtausTransactionIndex();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'dtaus transaction index'.
     */
    public boolean dtausTransactionIndexIsDirty() {
        return !valuesAreEqual(dtausTransactionIndexInitVal(), getDtausTransactionIndex());
    }

    /**
     * Returns true if the setter method was called for the property 'dtaus transaction index'.
     */
    public boolean dtausTransactionIndexIsSet() {
        return _dtausTransactionIndexIsSet;
    }
		
    /**
     * Returns the initial value of the property 'error code'.
     */
    public String errorCodeInitVal() {
        String result;
        if (_errorCodeIsSet) {
            result = _errorCodeInitVal;
        } else {
            result = getErrorCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'error code'.
     */
    public boolean errorCodeIsDirty() {
        return !valuesAreEqual(errorCodeInitVal(), getErrorCode());
    }

    /**
     * Returns true if the setter method was called for the property 'error code'.
     */
    public boolean errorCodeIsSet() {
        return _errorCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'external transaction id'.
     */
    public String externalTransactionIdInitVal() {
        String result;
        if (_externalTransactionIdIsSet) {
            result = _externalTransactionIdInitVal;
        } else {
            result = getExternalTransactionId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external transaction id'.
     */
    public boolean externalTransactionIdIsDirty() {
        return !valuesAreEqual(externalTransactionIdInitVal(), getExternalTransactionId());
    }

    /**
     * Returns true if the setter method was called for the property 'external transaction id'.
     */
    public boolean externalTransactionIdIsSet() {
        return _externalTransactionIdIsSet;
    }

}
