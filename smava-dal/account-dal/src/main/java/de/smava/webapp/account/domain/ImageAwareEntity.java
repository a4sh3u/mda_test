package de.smava.webapp.account.domain;

/**
 * An interface for the entities that handles images.
 * <p/>
 * Date: 09.01.2007 18:40:33
 *
 * @author joern.stampehl
 */
public interface ImageAwareEntity {
	
	// image name
    String getImage();
    void setImage(String imageName);

    // image height
    int getImageHeight();
    void setImageHeight(int height);
    
    // image width
    int getImageWidth();
    void setImageWidth(int width);

    String getFullImagePath(String imagePath);
}
