package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractAddress;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Addresss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AddressHistory extends AbstractAddress {

    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _streetInitVal;
    protected transient boolean _streetIsSet;
    protected transient String _streetNumberInitVal;
    protected transient boolean _streetNumberIsSet;
    protected transient String _zipCodeInitVal;
    protected transient boolean _zipCodeIsSet;
    protected transient String _cityInitVal;
    protected transient boolean _cityIsSet;
    protected transient String _countryInitVal;
    protected transient boolean _countryIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient Date _livingThereSinceInitVal;
    protected transient boolean _livingThereSinceIsSet;


	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'street'.
     */
    public String streetInitVal() {
        String result;
        if (_streetIsSet) {
            result = _streetInitVal;
        } else {
            result = getStreet();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'street'.
     */
    public boolean streetIsDirty() {
        return !valuesAreEqual(streetInitVal(), getStreet());
    }

    /**
     * Returns true if the setter method was called for the property 'street'.
     */
    public boolean streetIsSet() {
        return _streetIsSet;
    }
	
    /**
     * Returns the initial value of the property 'street number'.
     */
    public String streetNumberInitVal() {
        String result;
        if (_streetNumberIsSet) {
            result = _streetNumberInitVal;
        } else {
            result = getStreetNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'street number'.
     */
    public boolean streetNumberIsDirty() {
        return !valuesAreEqual(streetNumberInitVal(), getStreetNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'street number'.
     */
    public boolean streetNumberIsSet() {
        return _streetNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'zip code'.
     */
    public String zipCodeInitVal() {
        String result;
        if (_zipCodeIsSet) {
            result = _zipCodeInitVal;
        } else {
            result = getZipCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'zip code'.
     */
    public boolean zipCodeIsDirty() {
        return !valuesAreEqual(zipCodeInitVal(), getZipCode());
    }

    /**
     * Returns true if the setter method was called for the property 'zip code'.
     */
    public boolean zipCodeIsSet() {
        return _zipCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'city'.
     */
    public String cityInitVal() {
        String result;
        if (_cityIsSet) {
            result = _cityInitVal;
        } else {
            result = getCity();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'city'.
     */
    public boolean cityIsDirty() {
        return !valuesAreEqual(cityInitVal(), getCity());
    }

    /**
     * Returns true if the setter method was called for the property 'city'.
     */
    public boolean cityIsSet() {
        return _cityIsSet;
    }
	
    /**
     * Returns the initial value of the property 'country'.
     */
    public String countryInitVal() {
        String result;
        if (_countryIsSet) {
            result = _countryInitVal;
        } else {
            result = getCountry();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'country'.
     */
    public boolean countryIsDirty() {
        return !valuesAreEqual(countryInitVal(), getCountry());
    }

    /**
     * Returns true if the setter method was called for the property 'country'.
     */
    public boolean countryIsSet() {
        return _countryIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'living there since'.
     */
    public Date livingThereSinceInitVal() {
        Date result;
        if (_livingThereSinceIsSet) {
            result = _livingThereSinceInitVal;
        } else {
            result = getLivingThereSince();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'living there since'.
     */
    public boolean livingThereSinceIsDirty() {
        return !valuesAreEqual(livingThereSinceInitVal(), getLivingThereSince());
    }

    /**
     * Returns true if the setter method was called for the property 'living there since'.
     */
    public boolean livingThereSinceIsSet() {
        return _livingThereSinceIsSet;
    }

}
