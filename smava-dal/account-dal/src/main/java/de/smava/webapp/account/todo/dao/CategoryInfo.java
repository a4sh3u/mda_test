/**
 * 
 */
package de.smava.webapp.account.todo.dao;


import de.smava.webapp.account.domain.Category;
import de.smava.webapp.commons.domain.AbstractEntityInfo;

/**
 * @author bvoss
 *
 */
public class CategoryInfo extends AbstractEntityInfo<Category> {
	
	private String _name;
	private Long _count;

	public CategoryInfo() {
		super(Category.class);
	}

	public String getName() {
		return _name;
	}

	public void setName(final String name) {
		_name = name;
	}

	public Long getCount() {
		return _count;
	}

	public void setCount(final Long count) {
		_count = count;
	}
}
