package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.interfaces.MessageEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Collection;

/**
 * The domain object that represents 'Messages'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractMessage
    extends KreditPrivatEntity implements MessageEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMessage.class);

    private static final long serialVersionUID = 2282083269196462079L;

    public static final String FOLDER_INBOX = "inbox";
    public static final String FOLDER_LENDER_STATUS_MAIL = "lender_status_mail";
    public static final String FOLDER_DRAFT = "draft";
    public static final String FOLDER_SEND = "send";
    public static final String FOLDER_DELETED = "deleted";
    public static final String FOLDER_FINALLY_DELETED = "finally_deleted";

    @SuppressWarnings("unchecked")
    public static final Collection<String> FOLDERS = ConstCollector.getAll("FOLDER_");

    private transient Long _smavaAccountId;

    public void setSmavaAccountId(Long accountId) {
        _smavaAccountId = accountId;
    }

    /**
     * Returns true if recipient's id equals _smavaAccountId.
     * This method is necessary for mail conditions that only allow beans.
     */
    public boolean isRecipientSmavaAccount() {
        boolean result = false;
        if (getRecipient() != null && getRecipient().getId() != null) {
            result = getRecipient().getId().equals(_smavaAccountId);
        }
        return result;
    }

}

