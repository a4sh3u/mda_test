package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractReminderState;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ReminderStates'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ReminderStateHistory extends AbstractReminderState {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _calculationDateInitVal;
    protected transient boolean _calculationDateIsSet;
    protected transient int _levelInitVal;
    protected transient boolean _levelIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'calculation date'.
     */
    public Date calculationDateInitVal() {
        Date result;
        if (_calculationDateIsSet) {
            result = _calculationDateInitVal;
        } else {
            result = getCalculationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'calculation date'.
     */
    public boolean calculationDateIsDirty() {
        return !valuesAreEqual(calculationDateInitVal(), getCalculationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'calculation date'.
     */
    public boolean calculationDateIsSet() {
        return _calculationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'level'.
     */
    public int levelInitVal() {
        int result;
        if (_levelIsSet) {
            result = _levelInitVal;
        } else {
            result = getLevel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'level'.
     */
    public boolean levelIsDirty() {
        return !valuesAreEqual(levelInitVal(), getLevel());
    }

    /**
     * Returns true if the setter method was called for the property 'level'.
     */
    public boolean levelIsSet() {
        return _levelIsSet;
    }

}
