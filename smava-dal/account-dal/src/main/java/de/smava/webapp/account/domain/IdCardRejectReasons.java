/**
 * 
 */
package de.smava.webapp.account.domain;

import org.apache.commons.lang.StringUtils;

/**
 * @author bvoss
 *
 */
public enum IdCardRejectReasons {
	
	ADDRESS_STREET_NUMBER,
	ADDRESS_PLZ,
	ADDRESS_CITY,
	ADDRESS_NOT_PRESENT,
	PREVIOUSADDRESS_STREET_NUMBER,
	PREVIOUSADDRESS_PLZ,
	PREVIOUSADDRESS_CITY,
	PREVIOUSADDRESS_NOT_PRESENT,
	BIRTHDAY_SCHUFA,
	IDCARD_CHECKSUM_CENTER,
	IDCARD_CHECKSUM_RIGHT,
	IDCARD_CHECKSUM,
	IDCARD_DATE_ORDERING,
	IDCARD_DATE_INTERVALL;
	
	public static final String MAIN_ADDRESS_TYPE = "ADDRESS";
	public static final String PREVIOUS_ADDRESS_TYPE = "PREVIOUSADDRESS";
	
	public static final String STREET_NUMBER_KIND = "_STREET_NUMBER";
	public static final String PLZ_KIND = "_PLZ";
	public static final String CITY_KIND = "_CITY";
	public static final String NOT_PRESENT_KIND = "_NOT_PRESENT";
	
	public static IdCardRejectReasons getForAddressType(final String type, final String kind) {
		return valueOf(type + kind);
	}
	
	
	public String getCategory() {
		return StringUtils.substringBefore(name(), "_").toLowerCase();
	}
	
	public String getLowerName() {
		return name().toLowerCase();
	}

}
