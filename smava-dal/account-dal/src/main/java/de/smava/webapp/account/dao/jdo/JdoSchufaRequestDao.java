//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa request)}

import de.smava.webapp.account.dao.SchufaRequestDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.SchufaRequest;
import de.smava.webapp.commons.dao.DefaultSortable;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'SchufaRequests'.
 *
 * @author generator
 */
@Repository(value = "schufaRequestDao")
public class JdoSchufaRequestDao extends JdoBaseDao implements SchufaRequestDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSchufaRequestDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(schufa request)}
    //
    // insert custom fields here
    //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the schufa request identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public SchufaRequest load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequest - start: id=" + id);
        }
        SchufaRequest result = getEntity(SchufaRequest.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequest - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public SchufaRequest getSchufaRequest(Long id) {
        return load(id);
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            SchufaRequest entity = findUniqueEntity(SchufaRequest.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the schufa request.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(SchufaRequest schufaRequest) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveSchufaRequest: " + schufaRequest);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(schufa request)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(schufaRequest);
    }

    /**
     * @deprecated Use {@link #save(SchufaRequest) instead}
     */
    public Long saveSchufaRequest(SchufaRequest schufaRequest) {
        return save(schufaRequest);
    }

    /**
     * Deletes an schufa request, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa request
     */
    public void deleteSchufaRequest(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteSchufaRequest: " + id);
        }
        deleteEntity(SchufaRequest.class, id);
    }

    /**
     * Retrieves all 'SchufaRequest' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<SchufaRequest> getSchufaRequestList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - start");
        }
        Collection<SchufaRequest> result = getEntities(SchufaRequest.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'SchufaRequest' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<SchufaRequest> getSchufaRequestList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - start: pageable=" + pageable);
        }
        Collection<SchufaRequest> result = getEntities(SchufaRequest.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaRequest' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<SchufaRequest> getSchufaRequestList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - start: sortable=" + sortable);
        }
        Collection<SchufaRequest> result = getEntities(SchufaRequest.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaRequest' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<SchufaRequest> getSchufaRequestList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaRequest> result = getEntities(SchufaRequest.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaRequest' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaRequest> findSchufaRequestList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - start: whereClause=" + whereClause);
        }
        Collection<SchufaRequest> result = findEntities(SchufaRequest.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaRequest' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<SchufaRequest> findSchufaRequestList(String whereClause, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<SchufaRequest> result = findEntities(SchufaRequest.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }


    /**
     * Retrieves a page of 'SchufaRequest' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaRequest> findSchufaRequestList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<SchufaRequest> result = findEntities(SchufaRequest.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaRequest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaRequest> findSchufaRequestList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<SchufaRequest> result = findEntities(SchufaRequest.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaRequest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaRequest> findSchufaRequestList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaRequest> result = findEntities(SchufaRequest.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaRequest' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaRequest> findSchufaRequestList(String whereClause, Pageable pageable, Sortable sortable, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaRequest> result = findEntities(SchufaRequest.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaRequestList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaRequest' instances.
     */
    public long getSchufaRequestCount() {
        long result = getEntityCount(SchufaRequest.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaRequest' instances which match the given whereClause.
     */
    public long getSchufaRequestCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(SchufaRequest.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaRequest' instances which match the given whereClause together with params specified in object array.
     */
    public long getSchufaRequestCount(String whereClause, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(SchufaRequest.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaRequestCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa request)}
    //
    // insert custom methods here


    @Override
    public Collection<SchufaRequest> findSupplementaryScoreRequestsForAccount(Account account) {


        Sortable sortable = new DefaultSortable("_creationDate", false);
        return  findSchufaRequestList("_account._id == " + account.getId() + " && (_type == '" + SchufaRequest.TYPE_SUPPLEMENTARY_SCORE_REQUEST + "')", sortable);
    }


    @Override
    public Collection<SchufaRequest>  findSchufaScoresByPersonKey(String personKey) {

        return findSchufaRequestList("_schufaPersonKey == '" + personKey + "'");
    }


    @Override
    public SchufaRequest  getContractObjectionRequest(Long orderId) {
        Collection<SchufaRequest> schufaRequests = findSchufaRequestList("_type == '" + SchufaRequest.TYPE_CONTRACT_OBJECTION + "' && _accountNumber.endsWith('" + orderId + "') && _state == '" + SchufaRequest.STATE_SUCCESSFUL + "'");

        SchufaRequest result = null;

        if (schufaRequests.size() == 1) {
            result = schufaRequests.iterator().next();
        }
        return result;
    }


    @Override
    public List<SchufaRequest> getContractRequests(Long orderId, Long accountId) {
        Collection<SchufaRequest> schufaRequests = findSchufaRequestList(
                "(_type == '" + SchufaRequest.TYPE_CONTRACT_INFORMATION + "' || "
                        + "_type == '" + SchufaRequest.TYPE_CONTRACT_OBJECTION + "' || "
                        + "_type == '" + SchufaRequest.TYPE_ENCASHMENT_INFORMATION + "') && "
                        + "_accountNumber == '" + accountId + "-" + orderId + "'"
        );
        return new ArrayList<SchufaRequest>(schufaRequests);
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
