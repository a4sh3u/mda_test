package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Sector;

import java.util.Date;


/**
 * The domain object that represents 'CoBorrowerEconomicalDatas'.
 *
 * @author generator
 */
public interface CoBorrowerEconomicalDataEntityInterface {

    /**
     * Setter for the property 'co borrower employment'.
     *
     * 
     *
     */
    void setCoBorrowerEmployment(String coBorrowerEmployment);

    /**
     * Returns the property 'co borrower employment'.
     *
     * 
     *
     */
    String getCoBorrowerEmployment();
    /**
     * Setter for the property 'co borrower income'.
     *
     * 
     *
     */
    void setCoBorrowerIncome(double coBorrowerIncome);

    /**
     * Returns the property 'co borrower income'.
     *
     * 
     *
     */
    double getCoBorrowerIncome();
    /**
     * Setter for the property 'co borrower start of occupation'.
     *
     * 
     *
     */
    void setCoBorrowerStartOfOccupation(Date coBorrowerStartOfOccupation);

    /**
     * Returns the property 'co borrower start of occupation'.
     *
     * 
     *
     */
    Date getCoBorrowerStartOfOccupation();
    /**
     * Setter for the property 'co borrower temp employment'.
     *
     * 
     *
     */
    void setCoBorrowerTempEmployment(boolean coBorrowerTempEmployment);

    /**
     * Returns the property 'co borrower temp employment'.
     *
     * 
     *
     */
    boolean getCoBorrowerTempEmployment();
    /**
     * Setter for the property 'co borrower end of temp employment'.
     *
     * 
     *
     */
    void setCoBorrowerEndOfTempEmployment(Date coBorrowerEndOfTempEmployment);

    /**
     * Returns the property 'co borrower end of temp employment'.
     *
     * 
     *
     */
    Date getCoBorrowerEndOfTempEmployment();
    /**
     * Setter for the property 'co borrower sector'.
     *
     * 
     *
     */
    void setCoBorrowerSector(Sector coBorrowerSector);

    /**
     * Returns the property 'co borrower sector'.
     *
     * 
     *
     */
    Sector getCoBorrowerSector();
    /**
     * Setter for the property 'co borrower sector description'.
     *
     * 
     *
     */
    void setCoBorrowerSectorDescription(String coBorrowerSectorDescription);

    /**
     * Returns the property 'co borrower sector description'.
     *
     * 
     *
     */
    String getCoBorrowerSectorDescription();
    /**
     * Setter for the property 'co borrower employer name'.
     *
     * 
     *
     */
    void setCoBorrowerEmployerName(String coBorrowerEmployerName);

    /**
     * Returns the property 'co borrower employer name'.
     *
     * 
     *
     */
    String getCoBorrowerEmployerName();

}
