package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Attachment;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'DocumentAttachmentContainers'.
 *
 * @author generator
 */
public interface DocumentAttachmentContainerEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'process date'.
     *
     * 
     *
     */
    void setProcessDate(Date processDate);

    /**
     * Returns the property 'process date'.
     *
     * 
     *
     */
    Date getProcessDate();
    /**
     * Setter for the property 'date'.
     *
     * 
     *
     */
    void setDate(Date date);

    /**
     * Returns the property 'date'.
     *
     * 
     *
     */
    Date getDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'direction'.
     *
     * 
     *
     */
    void setDirection(String direction);

    /**
     * Returns the property 'direction'.
     *
     * 
     *
     */
    String getDirection();
    /**
     * Setter for the property 'original document count'.
     *
     * 
     *
     */
    void setOriginalDocumentCount(Integer originalDocumentCount);

    /**
     * Returns the property 'original document count'.
     *
     * 
     *
     */
    Integer getOriginalDocumentCount();
    /**
     * Setter for the property 'document attachments'.
     *
     * 
     *
     */
    void setDocumentAttachments(Collection<Attachment> documentAttachments);

    /**
     * Returns the property 'document attachments'.
     *
     * 
     *
     */
    Collection<Attachment> getDocumentAttachments();

}
