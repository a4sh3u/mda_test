package de.smava.webapp.account.dao;

import de.smava.webapp.account.domain.history.HistoryEntry;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.util.ResourceBundleEnum;

import java.io.Serializable;
import java.util.Collection;

/**
 * DAO interface for the domain object 'HistoryEntryDetail'.
 *
 * @author generator
 */
public interface HistoryEntryDao extends Serializable {

    /**
     * Returns an attached copy of the history entry identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    HistoryEntry getHistoryEntry(Long id);

    /**
     * Saves the history entry.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    Long saveHistoryEntry(HistoryEntry historyEntry);

    /**
     * Deletes an history entry, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the history entry
     */
    void deleteHistoryEntry(Long id);

    /**
     * Retrieves all 'HistoryEntry' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<HistoryEntry> getHistoryEntryList();

    /**
     * Retrieves a page of 'HistoryEntry' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<HistoryEntry> getHistoryEntryList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'HistoryEntry' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<HistoryEntry> getHistoryEntryList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'HistoryEntry' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<HistoryEntry> getHistoryEntryList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'HistoryEntry' instances.
     */
    long getHistoryEntryCount();



    ResourceBundleEnum getHistoryEntryTypes();

    public Collection<HistoryEntry> findHistoryEntryListForAccountId(Long accountId, Pageable pageable, Sortable sortable);

    public long countEntryListForAccountId(Long accountId);
}