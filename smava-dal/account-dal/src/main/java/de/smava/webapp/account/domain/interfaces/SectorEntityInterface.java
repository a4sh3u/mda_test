package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.Sector;

import java.util.List;


/**
 * The domain object that represents 'Sectors'.
 *
 * @author generator
 */
public interface SectorEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'code'.
     *
     * 
     *
     */
    void setCode(String code);

    /**
     * Returns the property 'code'.
     *
     * 
     *
     */
    String getCode();
    /**
     * Setter for the property 'parent'.
     *
     * 
     *
     */
    void setParent(Sector parent);

    /**
     * Returns the property 'parent'.
     *
     * 
     *
     */
    Sector getParent();
    /**
     * Setter for the property 'children'.
     *
     * 
     *
     */
    void setChildren(List<Sector> children);

    /**
     * Returns the property 'children'.
     *
     * 
     *
     */
    List<Sector> getChildren();
    /**
     * Setter for the property 'is employee sector'.
     *
     * 
     *
     */
    void setIsEmployeeSector(Boolean isEmployeeSector);

    /**
     * Returns the property 'is employee sector'.
     *
     * 
     *
     */
    Boolean getIsEmployeeSector();
    /**
     * Setter for the property 'is official sector'.
     *
     * 
     *
     */
    void setIsOfficialSector(Boolean isOfficialSector);

    /**
     * Returns the property 'is official sector'.
     *
     * 
     *
     */
    Boolean getIsOfficialSector();
    /**
     * Setter for the property 'is detail description needed'.
     *
     * 
     *
     */
    void setIsDetailDescriptionNeeded(Boolean isDetailDescriptionNeeded);

    /**
     * Returns the property 'is detail description needed'.
     *
     * 
     *
     */
    Boolean getIsDetailDescriptionNeeded();

}
