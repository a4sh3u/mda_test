//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(order)}

import java.sql.Timestamp;
import java.util.*;

import javax.jdo.JDOObjectNotFoundException;
import javax.jdo.Query;
import javax.transaction.Synchronization;

import de.smava.webapp.commons.util.PersonConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.aperto.smava.util.NumberUtils;
import com.aperto.webkit.utils.StringTools;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.AgeGroup;
import de.smava.webapp.account.domain.BankAccountProvider;
import de.smava.webapp.account.domain.Bid;
import de.smava.webapp.account.domain.BidInterest;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.account.domain.BidSearchAgeGroup;
import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.CategoryOrder;
import de.smava.webapp.account.domain.EmploymentType;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.account.todo.dao.AgeGroupDao;
import de.smava.webapp.account.todo.order.AdvancedOrderInfo;
import de.smava.webapp.account.todo.order.OrderInfo;
import de.smava.webapp.account.todo.order.dao.OrderDao;
import de.smava.webapp.account.todo.util.SortAndPageable;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.security.Role;
import de.smava.webapp.commons.util.FormatUtils;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Orders'.
 *
 * @author generator
 */
@Repository(value = "orderDao")
public class JdoOrderDao extends JdoBaseDao implements OrderDao {

    private static final Logger LOGGER = Logger.getLogger(JdoOrderDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(order)}
    private static final long serialVersionUID = 6988020675499209942L;

    private static final String ROI = "roi";
    private static final String _STATE = "_state";
    private static final String ACTIVATION_DATE = "activationDate";
    private static final String TERM = "term";
    private static final String MARKET = "market";
    private static final String RATE = "rate";
    private static final String MATCHED_RATE = "matchedRate";
    private static final String OPEN_AMOUNT = "openAmount";
    private static final String ORIGINAL_AMOUNT = "originalAmount";

    private static final String BID_SEARCH_PERSON_JOIN = " left outer join person as p on p.account_id = a.id and p.valid_until is NULL and p.type = " + Person.PRIMARY_PERSON_TYPE;

    private static final Map<String, String> ORDER_BY_MAPPING = new HashMap<String, String>();

    private static final Map<Integer, String> GENDER_MAPPING;

    private static final Set<String> ALLOWED_ORDER_COLUMNS = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(ORIGINAL_AMOUNT, OPEN_AMOUNT, MATCHED_RATE, RATE, MARKET, TERM, ACTIVATION_DATE, ROI)));

    private static final String MAX_DURATION_PLACEHOLDER = ":maxDuration:";

    private static final String MINIMAL_FROM
        = "from bid b \n"
        + "inner join \"order\" o on b.id = o.id\n";
    private static final String CATEGORY_JOIN = " inner join category_order c_o on o.id = c_o.order_id \n";
    private static final String MINIMAL_PAGEABLE_JOIN
        = "inner join account a on o.account_id = a.id \n"
        + "inner join order_matched_amount oma on oma.oid = o.id\n"
        + "inner join order_original_amount ooa on ooa.oid = o.id\n";
    private static final String GROUP_JOIN = " inner join smava_group_member sgm on a.id = sgm.account \n";
    private static final String MAIN_ORDER_WHERE = "where  b.\"groupId\" = b.id ";
    private static final String COUNT_SELECT = "select count(b.id) \n";
    private static final String ADVANCED_JOUIN
        = "inner join \"economicalData\" ed on ed.account_id = a.id\n";

    private static final String SORT_PAGEABLE_SELECT
        = "select \n"
            + "    o.id as \"entityId\", \n"
            + "    b.title as \"name\",\n"
            + "    o.activation_date as \"activationDate\",\n"
            + "    cast(o.credit_term as int) as \"term\",\n"
            + "    coalesce (o.image, (select coalesce (c.image, '') from category_order co\n"
            + "		inner join category c on co.category_id = c.id\n"
            + "		where co.order_id = o.id and co.main = true\n"
            + "		order by co.id asc limit 1), '') as \"image\", \n"
            + "    case \n"
            + "        when o.image is null then \n"
            + "		coalesce ((select c.image_height from category_order co\n"
            + "		inner join category c on co.category_id = c.id\n"
            + "		where co.order_id = o.id and co.main = true\n"
            + "		order by co.id asc limit 1), 0)\n"
            + "        else o.image_height\n"
            + "    end as \"imageHeight\",\n"
            + "    case \n"
            + "        when o.image is null then \n"
            + "		coalesce ((select c.image_width from category_order co\n"
            + "		inner join category c on co.category_id = c.id\n"
            + "		where co.order_id = o.id and co.main = true\n"
            + "		order by co.id asc limit 1), 0)\n"
            + "        else o.image_width\n"
            + "    end as \"imageWidth\",\n"
            + "    o.matched_rate as \"matchedRate\",\n"
            + "    original_sum as \"originalAmount\",\n"
            + "    original_sum - coalesce (matched_sum, 0.0) as \"openAmount\","
            + "    o.account_id as \"accountId\",\n"
            + "    a.username as \"username\",\n"
            + "    (select substring(bi.market_name, 1, 1) from bid_interest bi\n"
            + "     where bi.bid_id = b.id and bi.valid_until is null\n"
            + "     limit 1) as \"market\",\n"
            + "	   (select bi.rate from bid_interest bi\n"
            + "     where bi.bid_id = b.id and bi.valid_until is null\n"
            + "     limit 1) as \"rate\",\n"
            + "	   o.roi as roi ";

    private static final String ADVANCED_SORT_PAGEABLE_SELECT
        = SORT_PAGEABLE_SELECT + ",\n"
        + "		(select cast(matche_count as int) from order_match_count omc where omc.oid = o.id) as \"matchCounts\","
        + "     credit_rate_indicator as \"creditRateIndicator\",\n"
        + "     coalesce (b.update_date, o.activation_date) as \"lastUpdate\" \n";

    private static final String OPEN_WHERE = "b.state in ('QUEUE', 'OPEN') ";
    private static final String RECENTLY_FINANCED_WHERE
        = "b.state in ('MATCHED','OBSOLETE') and o.activation_date between ? and ? ";
    private static final String ARCHIVED_WHERE
        = " and b.state in ('EXPIRED','MATCHED','OBSOLETE') and o.activation_date + interval '" + MAX_DURATION_PLACEHOLDER + " days' < ? ";
    private static final String CATEGORY_WHERE = " and c_o.category_id = ? ";

    private static final String OPEN_RECENTLY_FINANCED_WHERE
        = "and ((" + RECENTLY_FINANCED_WHERE + ") or " + OPEN_WHERE + ")";

    private static final String CP_FOR_GROUPE_WHERE
        = "and b.state in ('EXPIRED','MATCHED','OBSOLETE', 'OPEN', 'QUEUE') \n"
        + "and sgm.state = 'ACCEPTED' and sgm.smava_group = ? order by \"activationDate\" desc, b.id";

    private static final String CP_FOR_GROUPE_COMPLETE_SELECT
        = SORT_PAGEABLE_SELECT
        + MINIMAL_FROM
        + MINIMAL_PAGEABLE_JOIN
        + GROUP_JOIN
        + MAIN_ORDER_WHERE
        + CP_FOR_GROUPE_WHERE;


    private static final String OPEN_RECENTLY_FINANCED_COMPLETE_SELECT
        = SORT_PAGEABLE_SELECT
        + MINIMAL_FROM
        + MINIMAL_PAGEABLE_JOIN
        + MAIN_ORDER_WHERE
        + OPEN_RECENTLY_FINANCED_WHERE;

    private static final String ADVANCED_ALL_SELECT
        = ADVANCED_SORT_PAGEABLE_SELECT
        + MINIMAL_FROM
        + MINIMAL_PAGEABLE_JOIN
        + ADVANCED_JOUIN
        + MAIN_ORDER_WHERE
        + " ";

    private static final String ADVANCED_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT
        = ADVANCED_ALL_SELECT
        + OPEN_RECENTLY_FINANCED_WHERE;

    private static final String COUNT_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT
        = COUNT_SELECT
        + MINIMAL_FROM
        + MAIN_ORDER_WHERE
        + OPEN_RECENTLY_FINANCED_WHERE;

    private static final String ARCHIVED_COMPLETE_SLECT
        = SORT_PAGEABLE_SELECT
        + MINIMAL_FROM
        + MINIMAL_PAGEABLE_JOIN
        + MAIN_ORDER_WHERE
        + ARCHIVED_WHERE;
    private static final String COUNT_ARCHIVED_COMPLETE_SLECT
        = COUNT_SELECT
        + MINIMAL_FROM
        + MAIN_ORDER_WHERE
        + ARCHIVED_WHERE;


    private static final String CATEGORY_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT
        = SORT_PAGEABLE_SELECT
        + MINIMAL_FROM
        + CATEGORY_JOIN
        + MINIMAL_PAGEABLE_JOIN
        + MAIN_ORDER_WHERE
        + CATEGORY_WHERE
        + OPEN_RECENTLY_FINANCED_WHERE;
    private static final String CATEGORY_COUNT_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT
        = COUNT_SELECT
        + MINIMAL_FROM
        + CATEGORY_JOIN
        + MAIN_ORDER_WHERE
        + CATEGORY_WHERE
        + OPEN_RECENTLY_FINANCED_WHERE;

    private static final String CATEGORY_ARCHIVED_COMPLETE_SLECT
        = SORT_PAGEABLE_SELECT
        + MINIMAL_FROM
        + CATEGORY_JOIN
        + MINIMAL_PAGEABLE_JOIN
        + MAIN_ORDER_WHERE
        + CATEGORY_WHERE
        + ARCHIVED_WHERE;
    private static final String CATEGORY_COUNT_ARCHIVED_COMPLETE_SLECT
        = COUNT_SELECT
        + MINIMAL_FROM
        + CATEGORY_JOIN
        + MAIN_ORDER_WHERE
        + CATEGORY_WHERE
        + ARCHIVED_WHERE;

    private static final String UPDATE_ROI_ORDERS_SELECT
        = "select o.id from \"order\" o inner join bid on bid.id = o.id where bid.state in (:states)";


    static {
        final Map<Integer, String> gendermap = new HashMap<Integer, String>(2);
        gendermap.put(BidSearch.GENDER_MAN, PersonConstants.MALE_SALUTATION);
        gendermap.put(BidSearch.GENDER_WOMAN, PersonConstants.FEMALE_SALUTATION);
        GENDER_MAPPING = Collections.unmodifiableMap(gendermap);

        ORDER_BY_MAPPING.put(ACTIVATION_DATE, "activation_date");
        ORDER_BY_MAPPING.put("amount", "amount");
        ORDER_BY_MAPPING.put("interest", "bi.rate");
        ORDER_BY_MAPPING.put(RATE, "bi.rate");
        ORDER_BY_MAPPING.put(TERM, TERM);
        ORDER_BY_MAPPING.put(MARKET, MARKET);
        ORDER_BY_MAPPING.put("completion", "matched_rate");
    }

    private volatile Set<Long> _allAgeGroupsCache = null;
    private static final Object AGE_GROUP_MUTEX = new Object();

    private AgeGroupDao _ageGroupDao;

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the order identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Order load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrder - start: id=" + id);
        }
        Order result = getEntity(Order.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrder - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Order getOrder(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Order entity = findUniqueEntity(Order.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the order.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Order order) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveOrder: " + order);
        }
        // !!!!!!!! You can insert code here: !!!!!!!! {|saveEntity|bean(order)}
        
      //TODO better ... without second save
        if (order.getGroupId() == 0) {
            if (order.getId() == null) {
                saveEntity(order);
            }
            order.setGroupId(order.getId());
        }
        
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(order);
    }

    /**
     * @deprecated Use {@link #save(Order) instead}
     */
    public Long saveOrder(Order order) {
        return save(order);
    }

    /**
     * Deletes an order, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the order
     */
    public void deleteOrder(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteOrder: " + id);
        }
        deleteEntity(Order.class, id);
    }

    /**
     * Retrieves all 'Order' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Order> getOrderList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - start");
        }
        Collection<Order> result = getEntities(Order.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Order' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Order> getOrderList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - start: pageable=" + pageable);
        }
        Collection<Order> result = getEntities(Order.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Order' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Order> getOrderList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - start: sortable=" + sortable);
        }
        Collection<Order> result = getEntities(Order.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Order' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Order> getOrderList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Order> result = getEntities(Order.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Order' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Order> findOrderList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: whereClause=" + whereClause);
        }
        Collection<Order> result = findEntities(Order.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Order' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Order> findOrderList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Order> result = findEntities(Order.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Order' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Order> findOrderList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Order> result = findEntities(Order.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Order' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Order> findOrderList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Order> result = findEntities(Order.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Order' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Order> findOrderList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Order> result = findEntities(Order.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Order' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Order> findOrderList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Order> result = findEntities(Order.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Order' instances.
     */
    public long getOrderCount() {
        long result = getEntityCount(Order.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Order' instances which match the given whereClause.
     */
    public long getOrderCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Order.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Order' instances which match the given whereClause together with params specified in object array.
     */
    public long getOrderCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Order.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!! {|customMethods|bean(order)}

    
    
    /**
     * Attaches a Category to the Order instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    public void attachCategory(Order order, Category category) {
        saveEntity(category);
        saveEntity(order);
        
        CategoryOrder co = new CategoryOrder(category, order);
        category.getCategoryOrders().add(co);
        order.getCategoryOrders().add(co);
        
        saveEntity(co);
    }


    public void setAgeGroupDao(AgeGroupDao ageGroupDao) {
        _ageGroupDao = ageGroupDao;
    }



    @Override
    public void detachCategory(Order order, Category category) {
        order.getCategories().remove(category);
        saveEntity(category);
        saveEntity(order);
        
        Collection<CategoryOrder> deletableCategoryOrders = new HashSet<CategoryOrder>();
        
        for (CategoryOrder co : category.getCategoryOrders()) {
            if (co.getOrder().equals(order) && co.getCategory().equals(category)) {
                deletableCategoryOrders.add(co);
            }
        }

        for (CategoryOrder categoryOrder : deletableCategoryOrders) {
            deleteEntity(CategoryOrder.class, categoryOrder.getId());
        }

        category.getCategoryOrders().removeAll(deletableCategoryOrders);
        order.getCategoryOrders().removeAll(deletableCategoryOrders);
    }

    @Override
    public Collection<Order> findOrderList(Account borrower, Collection<String> states) {

        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    getStatesTerm(oqlTerm, states));
        } else {
            oqlTerm.equals("_account", borrower);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + borrower.getUsername());
        }
        Collection<Order> result = findEntities(Order.class, oqlTerm.toString(), borrower);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public long getCreditProjectListCount(Account borrower, Collection<String> states) {
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    getStatesTerm(oqlTerm, states));
        } else {
            oqlTerm.equals("_account", borrower);
        }

        String whereClause = "_groupId == _id && " + oqlTerm.toString();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderListCount() - start: borrower=" + borrower.getUsername());
        }
        long result = this.getEntityCount(Order.class, whereClause, borrower);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderListCount() - result = " + result);
        }
        return result;
    }



    @Override
    public Collection<Order> findOrdersOfSameGroup(Order order, Collection<String> states) {
        OqlTerm oqlTerm = OqlTerm.newTerm();

        Collection<Order> result = new ArrayList<Order>();

        OqlTerm groupCondition = oqlTerm.equals("_groupId", order.getGroupId());

        if (states != null && !states.isEmpty()) {
            OqlTerm oqlStates = OqlTerm.newTerm();
            getStatesTerm(oqlStates, states);
            oqlTerm.and(groupCondition, oqlStates);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrdersOfSameGroup() - start: " + oqlTerm.toString());
        }
        result.addAll(findEntities(Order.class, oqlTerm.toString()));
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrdersOfSameGroup() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Gets the states term for finding offers and orders.
     */
    private OqlTerm getStatesTerm(OqlTerm oqlTerm, Collection<String> states) {
        OqlTerm result;
        if (states.size() > 1) {
            OqlTerm [] orStates = new OqlTerm[states.size()];
            int i = 0;
            for (String state : states) {
                orStates[i] = oqlTerm.equals("_state", state);
                i++;
            }
            result = oqlTerm.or(orStates);
        } else if (states.size() == 1) {
            result = oqlTerm.equals("_state", states.iterator().next());
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }
        return result;
    }

    @Override
    public Collection<Order> findOrderList(String market, Collection<String> states) {
        String stateString = buildJdoStateString(states);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: market=" + market);
        }
        Collection<Order> result = findEntities(Order.class, "_bidInterests.contains(bidInterest) && bidInterest._marketName == " + StringTools.asJavaString(market) + " && bidInterest._validUntil == null && (" + stateString + ")");
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<Order> findOrderList(String market, Collection<String> states, Pageable pageable, Sortable sortable) {
        String stateString = buildJdoStateString(states);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: market=" + market);
        }
        Collection<Order> result = findEntities(Order.class, "_bidInterests.contains(bidInterest) && bidInterest._marketName == " + StringTools.asJavaString(market) + " && bidInterest._validUntil == null && (" + stateString + ")", pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Builds a string based on a passed array of possible states.
     */
    public static String buildJdoStateString(Collection<String> states) {
        StringBuilder query = new StringBuilder();
        boolean first = true;
        for (String state : states) {
            if (first) {
                query.append("_state==").append("'").append(state).append("'");
                first = false;
            } else {
                query.append(" || _state==").append("'").append(state).append("'");
            }
        }
        return query.toString();
    }

    public Collection<Order> findCurrentOpenOrders() {
        OqlTerm oql = OqlTerm.newTerm();
        getStatesTerm(oql, Bid.OPEN_STATES);
        oql.setSortBy("_creationDate");
        oql.setSortDescending(true);
        return findEntities(Order.class, oql.toString());
    }




    @Override
    public Collection<Order> findOrderList(Account account, Pageable pageable, Sortable sortable) {
        Collection<Order> list;
        if (account == null) {
            list = getOrderList(pageable, sortable);
        } else {
            LOGGER.warn("Account: " + account);
            OqlTerm oql = OqlTerm.newTerm();
            if (account.getRoles().contains(Role.ROLE_BORROWER)) {
                oql.equals("_account", account);
            } else {
                throw new IllegalArgumentException("Account not borrower.");
            }
            if (pageable != null && sortable != null) {
                list = findEntities(Order.class, oql.toString(), pageable, sortable, new Object[]{account});
            } else {
                list = findEntities(Order.class, oql.toString(), new Object[]{account});
            }
        }
        return list;
    }

    @Override
    public Collection<Order> findTopLevelOrders(Account account, Collection<String> states, Sortable sortable) {

        Collection<Order> result = null;
        String oql = "_groupId==_id";
        if (account != null) {
            oql = oql + " && _account._id == " + account.getId();
        }
        if (!states.isEmpty()) {
            OqlTerm oqlStates = OqlTerm.newTerm();
            getStatesTerm(oqlStates, states);
            
            oql += " && " + oqlStates.toString();
        }
        
        String sortDirection = "ascending";
        if (sortable.sortDescending()) {
            sortDirection = "descending";
        }
        
        oql += " order by " + sortable.getSort() +  " " + sortDirection;
        
        result = findEntities(Order.class, oql);
        return result;
    }

    @Override
    public Collection<Order> findDeletedTopLevelOrders(Account account,Sortable sortable) {

        Collection<Order> result = null;
        String oql = "_groupId==_id";
        if (account != null) {
            oql = oql + " && _account._id == " + account.getId();
        }
        OqlTerm deleted = OqlTerm.newTerm();
        deleted.and(deleted.notNull("_activationDate"), deleted.equals("_state", Bid.STATE_DELETED));
        oql += " && " + deleted.toString();

        String sortDirection = "ascending";
        if (sortable.sortDescending()) {
            sortDirection = "descending";
        }

        oql += " order by " + sortable.getSort() +  " " + sortDirection;

        result = findEntities(Order.class, oql);
        return result;
    }


    @Override
    public Collection<Order> findOrders(BidSearch bidSearch, int maxOrderDuration,  double cfgMinAmount, double cfgMaxAmount) {
        return findOrders(bidSearch, maxOrderDuration, null, cfgMinAmount, cfgMaxAmount);
    }

    private Collection<Order> findOrders(BidSearch bidSearch, int maxOrderDuration, Order order,  double cfgMinAmount, double cfgMaxAmount) {
        return findOrders(bidSearch, maxOrderDuration, order, null, cfgMinAmount, cfgMaxAmount);
    }
    
    @Override
    public Collection<Order> findOrders(BidSearch bidSearch, int maxOrderDuration, Order order, BankAccountProvider bankAccountProvider, double cfgMinAmount, double cfgMaxAmount) {
        Map<Integer, String> parameters = new HashMap<Integer, String>();
        String query = getOrderSearchQuery("distinct(o.id) as id", bidSearch, maxOrderDuration, order, bankAccountProvider, true, cfgMinAmount, cfgMaxAmount);
        Query q = getPersistenceManager().newQuery(Query.SQL, query);
        q.setClass(Order.class);
        
        final long start = System.nanoTime();
        @SuppressWarnings("unchecked")
        Collection<Order> result = (Collection<Order>) q.executeWithMap(parameters);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("########query took " + ((double) (System.nanoTime() - start) / 1000000) + "ms");
        }
        return result;
    }
    
    @Override
    public List<AdvancedOrderInfo> findAdvancedOrders(BidSearch bidSearch, int maxOrderDuration, final String sortcolumn, final boolean ascending, double cfgMinAmount, double cfgMaxAmount) {
        final String subQuery = getOrderSearchQuery("distinct(o.id) as order_id", bidSearch, maxOrderDuration, null, null, false, cfgMinAmount,cfgMaxAmount);
        String sql = ADVANCED_ALL_SELECT + " and o.id in (" + StringUtils.removeEnd(subQuery, ";")  + ")";
        return getListBySinglePagableQuery(
                sql,
                AdvancedOrderInfo.class,
                ALLOWED_ORDER_COLUMNS,
                sortcolumn,
                ascending, 0, 1000);
    }

    private String getOrderSearchQuery(String resultSet, BidSearch bidSearch, int maxOrderDu2ration, Order order, BankAccountProvider bankAccountProvider, final boolean getOrdered, double cfgMinAmount, double cfgMaxAmount) {
        Collection<String> openStates = new ArrayList<String>(Bid.OPEN_STATES);
        Collection<String> runningStates = new ArrayList<String>(Bid.RUNNING_STATES);
        if (!runningStates.contains(Bid.STATE_OBSOLETE)) {
            runningStates.add(Bid.STATE_OBSOLETE);
        }
        String openStatesString = "";
        String runningStatesString = "";
        String delimiter = "";
        for (String state : openStates) {
            openStatesString += delimiter + "'" + state + "'";
            delimiter = ", ";
        }
        delimiter = "";
        for (String state : runningStates) {
            runningStatesString += delimiter + "'" + state + "'";
            delimiter = ", ";
        }
        String marketNames = createMarketNamesClause(bidSearch);
        String genderWhere = StringUtils.EMPTY;
        String ageGroupsWhere = StringUtils.EMPTY;

        StringBuilder builder = new StringBuilder();
        builder.append("select ").append(resultSet).append(" ");
        if (getOrdered) {
            builder.append(",o.activation_date ");
        }
        builder.append("from \"order\" as o ");
        builder.append("left outer join bid as b on o.id = b.id ");
        builder.append("left outer join bid_interest as bi on b.id = bi.bid_id ");
        builder.append("left outer join account as a on o.account_id = a.id ");
        genderWhere = genderCheck(bidSearch, genderWhere, builder);
        builder.append("left outer join \"economicalData\" as ed on a.id = ed.account_id ");
        ageGroupsWhere = ageGroupCheck(bidSearch, StringUtils.isNotEmpty(genderWhere), builder);
        builder.append("where ");
        builder.append("b.id = b.\"groupId\" ");
        builder.append(ageGroupsWhere);
        builder.append(genderWhere);
        builder.append(employmentTypeWhere(bidSearch.getEmployments()));
        setRoi(bidSearch, builder);
        builder.append(cpAmountSizeWhere(bidSearch, cfgMinAmount, cfgMaxAmount));

        builder.append(" and (");
        builder.append("(b.state in (").append(openStatesString).append(")").append(")");
        if (bidSearch.getIncludeFinishedOrders()) {
            builder.append(" or b.state in (").append(runningStatesString).append(")");
        }
        builder.append(") ");
        builder.append("and bi.valid_until is null ");
        builder.append("and bi.market_name in (").append(marketNames).append(") ");
        builder.append("and bi.rate >= ").append((bidSearch.getInterestMin() - 0.01)).append(" ");
        builder.append("and bi.rate <= ").append(bidSearch.getInterestMax()).append(" ");
        //WEBSITE-10387 :
        builder.append("and ed.id=(select max(economical_data_id) from order_economical_data where order_id=b.id) ");
        builder.append("and ed.credit_rate_indicator > 0 and ed.credit_rate_indicator <= ").append(bidSearch.getCreditRateIndicator()).append(" ");
        builder.append("and o.matched_rate >= ").append(bidSearch.getMatchedRate()).append(" ");
        builder.append("and o.activation_date is not null ");
        if (bidSearch.getExceptLatePayer() != null && bidSearch.getExceptLatePayer()) {
            builder.append("and not exists ( ");
            builder.append(delayedPayersSelect(null));
            builder.append(") ");
        }
        addAmountPerBorrowerWhere(bidSearch, builder);

        if (order != null) {
            builder.append("and o.id = ").append(order.getId());
        }

        if (bankAccountProvider != null) {
            builder.append(" and o.bank_service_provider_id = ").append(bankAccountProvider.getId());
        }

        if (getOrdered) {
            builder.append(" order by o.activation_date asc");
        }
        builder.append(";");
        return builder.toString();
    }

    private void addAmountPerBorrowerWhere(BidSearch bidSearch,
            StringBuilder builder) {
        //account null check is needed for AA, because in this case we have no specific account and the
        //sql condition for investment amount per borrower can't be made
        //and check only if a limit is set (currently only for GAs)
        if (bidSearch.getAccount() != null && bidSearch.getInvestmentLimitPerBorrower() > 0) {
            builder.append("and (");
            builder.append("(select case when sum(c1.amount) is null then 0 else sum(c1.amount) end from contract c1 ");
            builder.append("inner join \"order\" o2 on o2.id = c1.order_id ");
            builder.append("inner join offer on offer.id = c1.offer_id ");
            builder.append("where o2.account_id = a.id and offer.account_id = ");
            builder.append(bidSearch.getAccount().getId());
            builder.append(" and c1.state in (");
            builder.append(expand(BidSearch.CONTRACT_STATES));
            builder.append(")) < ");
            builder.append(bidSearch.getInvestmentLimitPerBorrower());
            builder.append(") ");
        }
    }

    private String cpAmountSizeWhere(final BidSearch bidSearch, double cfgMinAmount, double cfgMaxAmount) {

        final boolean minDiffer = bidSearch.getAmountMin() != cfgMinAmount;
        final boolean maxDiffer = bidSearch.getAmountMax() != cfgMaxAmount;

        final StringBuilder sb = new StringBuilder();
        if (minDiffer || maxDiffer) {
            sb.append(" and ((select sum(amount) from \"order\" as o1 left outer join bid as b1 on b1.\"groupId\" = o1.id where b1.\"groupId\" = b.\"groupId\") ");
            if (minDiffer && maxDiffer) {
                sb.append(" between ");
                sb.append(bidSearch.getAmountMin());
                sb.append(" and ");
                sb.append(bidSearch.getAmountMax());
            } else if (maxDiffer) {
                sb.append(" <= ");
                sb.append(bidSearch.getAmountMax());
            } else {
                sb.append(" >= ");
                sb.append(bidSearch.getAmountMin());
            }
            sb.append(") ");
        }
        return sb.toString();
    }



    /**
     * Creates select statement that returns delayed payers from DB.
     * @param account The Account
     * @return If account is null all delayed payers are returned. When the account is NO delayed payer empty result is returned, otherwise the account id;
     */

    private String delayedPayersSelect(Account account) {
        StringBuilder builder = new StringBuilder();
        builder.append("select ");
        if (account != null) {
            builder.append("distinct ");
        }
        builder.append("credit.account_id from transaction tu, bank_account credit , dunning_state ds");
        builder.append(" where tu.state in ('SUCCESSFUL','OPEN')");
        builder.append(" and credit.type = 'KNK'");
        builder.append(" and tu.credit_account = credit.id");
        builder.append(" and credit.account_id = ");

        if (account == null) {
            builder.append("a.id");
        } else {
            builder.append("?");
        }

        builder.append(" and ds.transaction_id = tu.id and ds.level >= 1 ");

        return builder.toString();
    }


    private String employmentTypeWhere(final Collection<EmploymentType> employTypes) {
        final String result;
        if (CollectionUtils.isNotEmpty(employTypes) && !areAllEmploymentTypes(employTypes)) {
            final StringBuilder sb = new StringBuilder(" and ed.employment in (");
            for (EmploymentType employmentType : employTypes) {
                sb.append("'");
                sb.append(employmentType.name());
                sb.append("',");
            }
            sb.delete(sb.length() - 1, sb.length());
            sb.append(")");
            result = sb.toString();
        } else {
            result = StringUtils.EMPTY;
        }
        return result;
    }

    private boolean areAllEmploymentTypes(final Collection<EmploymentType> employTypes) {
        final Set<EmploymentType> checktypes = new HashSet<EmploymentType>(employTypes);
        boolean result = true;
        for (EmploymentType employmentTypeOfAll : EmploymentType.values()) {
            if (!checktypes.contains(employmentTypeOfAll)) {
                result = false;
                break;
            }
        }
        return result;
    }

    private String ageGroupCheck(final BidSearch bidSearch, final boolean personJoined,
            final StringBuilder builder) {
        String result = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(bidSearch.getAgeGroups())) {
            final List<AgeGroup> relevantAgeGroups = findRelevantAgeGroups(bidSearch.getAgeGroups());
            if (CollectionUtils.isNotEmpty(relevantAgeGroups) && notAllAgeGroups(relevantAgeGroups)) {
                result = createAgeGroupsWhere(relevantAgeGroups, CurrentDate.getDate());
                if (StringUtils.isNotEmpty(result)) {
                    builder.append(" left outer join bid_search_age_group as bsag on b.id = bsag.bid_search_id ");
                    builder.append(" left outer join age_group as ag on ag.id = bsag.age_group_id ");
                    if (!personJoined) {
                        builder.append(BID_SEARCH_PERSON_JOIN);
                    }
                }
            }
        }
        return result;
    }

    private String genderCheck(BidSearch bidSearch, String genderWhere,
            StringBuilder builder) {
        if (bidSearch.getGender() != null && bidSearch.getGender() > 0) {
            builder.append(BID_SEARCH_PERSON_JOIN);
            final String salutaion = GENDER_MAPPING.get(bidSearch.getGender());
            if (salutaion == null) {
                throw new IllegalArgumentException("unkowen gender " + bidSearch.getGender());
            }
            genderWhere = "and p.salutation = '" + salutaion + "' ";
        }
        return genderWhere;
    }

    private boolean notAllAgeGroups(List<AgeGroup> relevantAgeGroups) {
        final Set<Long> myIds = new HashSet<Long>(relevantAgeGroups.size());
        for (AgeGroup ageGroup : relevantAgeGroups) {
            myIds.add(ageGroup.getId());
        }
        if (_allAgeGroupsCache == null) {
            synchronized (AGE_GROUP_MUTEX) {
                if (_allAgeGroupsCache == null) {
                    final Collection<AgeGroup> allAgeGroups = _ageGroupDao.getAgeGroupList();
                    final Set<Long> allAgeGroupIds = new HashSet<Long>(allAgeGroups.size());
                    for (AgeGroup ageGroup : allAgeGroups) {
                        allAgeGroupIds.add(ageGroup.getId());
                    }
                    _allAgeGroupsCache = Collections.unmodifiableSet(allAgeGroupIds);
                }
            }
        }
        return !myIds.equals(_allAgeGroupsCache);
    }


    private String createAgeGroupsWhere(final List<AgeGroup> ageGroups, final Date now) {
        final String result;
        final List<AgeGroup> concatedAgeGroups = calcualteConcatedAgeGroups(ageGroups);
        if (CollectionUtils.isNotEmpty(concatedAgeGroups)) {
            final String nowStr = FormatUtils.formatDbDateFormat(now);
            final String start = " and (";
            final StringBuilder sb = new StringBuilder(start);
            for (AgeGroup ageGroup : concatedAgeGroups) {
                if (sb.length() > start.length()) {
                    sb.append(" or ");
                }
                sb.append("(extract(year from age('");
                sb.append(nowStr);
                sb.append("', p.date_of_birth)) between ");
                sb.append(ageGroup.getMinAge());
                sb.append(" and ");
                sb.append(ageGroup.getMaxAge());
                sb.append(")");
            }
            sb.append(") ");
            result = sb.toString();
        } else {
            result = StringUtils.EMPTY;
        }
        return result;
    }

    private List<AgeGroup> calcualteConcatedAgeGroups(
            List<AgeGroup> relevantAgeGroups) {
        final List<AgeGroup> result;
        if (CollectionUtils.isNotEmpty(relevantAgeGroups)) {
            result = new ArrayList<AgeGroup>();
            sortRelevantAgeGroupsByMinAge(relevantAgeGroups);
            AgeGroup current = null;
            for (AgeGroup ageGroup : relevantAgeGroups) {
                if (current == null) {
                    current = getTempGroup(ageGroup);
                    result.add(current);
                } else {
                    if (current.getMaxAge() + 1 >= ageGroup.getMinAge()) {
                        current.setMaxAge(ageGroup.getMaxAge());
                    } else {
                        current = getTempGroup(ageGroup);
                        result.add(current);
                    }
                }
            }
        } else {
            result = Collections.EMPTY_LIST;
        }
        return result;
    }

    private AgeGroup getTempGroup(final AgeGroup source) {
        return new AgeGroup(source.getMinAge(), source.getMaxAge(), null, null);
    }

    private void sortRelevantAgeGroupsByMinAge(
            final List<AgeGroup> relevantAgeGroups) {
        Collections.sort(relevantAgeGroups, new Comparator<AgeGroup>() {

            @Override
            public int compare(AgeGroup group1, AgeGroup group2) {
                return group1.getMinAge().compareTo(group2.getMinAge());
            }

        });
    }

    private List<AgeGroup> findRelevantAgeGroups(
            Collection<BidSearchAgeGroup> ageGroups) {
        final List<AgeGroup> result = new ArrayList<AgeGroup>();
        for (BidSearchAgeGroup bidSearchAgeGroup : ageGroups) {
            final AgeGroup ageGroup = bidSearchAgeGroup.getAgeGroup();
            if (ageGroup != null && ageGroup.getValidUntil() == null) {
                result.add(ageGroup);
            }
        }
        return result;
    }

    private String createMarketNamesClause(BidSearch bidSearch) {
        String delimiter;
        String marketNames = "";
        delimiter = "";
        Collection<String> marketNameCache = new ArrayList<String>();
        if (bidSearch.getActiveBidInterests() != null) {
            for (BidInterest bidInterest : bidSearch.getActiveBidInterests()) {
                if (!marketNameCache.contains(bidInterest.getMarketName())) {
                    try {
                        marketNames += delimiter + "'" + bidInterest.getMarketName() + "'";
                        delimiter = ", ";
                        marketNameCache.add(bidInterest.getMarketName());
                    } catch (Exception e) {
                        LOGGER.debug("just for checking valid marketNames");
                    }
                }

            }
        }
        return marketNames;
    }

    private void setRoi(BidSearch bidSearch, StringBuilder builder) {
        if (bidSearch.getRoiMin() != null) {
            builder.append("and o.roi >= ").append(bidSearch.getRoiMin()).append(" ");
        }
        if (bidSearch.getRoiMax() != null) {
            builder.append("and o.roi <= ").append(bidSearch.getRoiMax()).append(" ");
        }
    }



    @Override
    public int getOrderCount(BidSearch bidSearch, int maxOrderDuration, double cfgMinAmount, double cfgMaxAmount) {
        return getOrderCount(bidSearch, maxOrderDuration, null,  cfgMinAmount, cfgMaxAmount );
    }
//	
    @Override
    public int getOrderCount(BidSearch bidSearch, int maxOrderDuration, Order order, double cfgMinAmount, double cfgMaxAmount) {
        return getOrderCount(bidSearch, maxOrderDuration, order, null, cfgMinAmount, cfgMaxAmount);
    }


    private int getOrderCount(BidSearch bidSearch, int maxOrderDuration, Order order, BankAccountProvider bankAccountProvider, double cfgMinAmount, double cfgMaxAmount) {
        Map<Integer, String> parameters = new HashMap<Integer, String>();
        String query = getOrderSearchQuery("count(distinct(o.id))", bidSearch, maxOrderDuration, order, bankAccountProvider, false,  cfgMinAmount,  cfgMaxAmount);
        Query q = getPersistenceManager().newQuery(Query.SQL, query);
        q.setResultClass(Integer.class);
        q.setUnique(true);
        Integer result = (Integer) q.executeWithMap(parameters);
        return (result == null) ? 0 : result;
    }



    @Override
    public Order getMainOrder(Order order) {
        Order result = null;
        if (order != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("find main order for order.Id = " + order.getId() + " and order.groupId = " + order.getGroupId());
            }
            if (order.isMainOrder()) {
                result = order;
            } else {
                //result = getEntity(Order.class, order.getGroupId());/ tried to get around HOLLOW here
                //FIXME: lame fix for HOLLOW state in case this gets called more than once in tx
                if(order.getGroupId() == 0L){
                    order = load(order.getId());
                }
                Collection<Order> mainOrderList = findOrderList("_id == " + order.getGroupId());
                result = mainOrderList.iterator().next();
            }
        }
        return result;
    }

    @Override
    public double getMatchedAmount(Order order) {
        return executeSumQuery(order, "select matched_sum from order_matched_amount where oid = ?");
    }

    @Override
    public double getOriginalAmount(Order order) {
        return executeSumQuery(order, "select original_sum from order_original_amount where oid = ?");
    }

    private double executeSumQuery(final Order order, final String query) {
        final Query q = getPersistenceManager().newQuery(Query.SQL, query);
        q.setResultClass(Double.class);
        final List<Double> queryResult = (List<Double>) q.execute(order.getId());
        final int resultSize = queryResult.size();
        if (resultSize == 0) {
            throw new JDOObjectNotFoundException("no entry found for order " + order.getId());
        }
        if (resultSize > 1) {
            throw new IllegalStateException("there are " + resultSize + " entries for order " + order.getId() + "but there should only be one!");
        }
        return NumberUtils.valueOf(queryResult.get(0));
    }

    @Override
    public SortAndPageable<OrderInfo> getOrderInfos(
            final SortAndPageable<OrderInfo> input, final Date now, final Date recentlyDate,
            final int maxOrderDuration, final boolean archiveOnly, Category category) {
        final Timestamp nowStamp = new Timestamp(now.getTime());
        if (archiveOnly) {
            if (category == null) {
                doArchiveOnly(input, maxOrderDuration, nowStamp);
            } else {
                doArchiveWithCategory(input, maxOrderDuration, category, nowStamp);
            }
        } else {
            final Timestamp recentlyStamp = new Timestamp(recentlyDate.getTime());
            if (category == null) {
                doRecentlyOpen(input, nowStamp, recentlyStamp);
            } else {
                doRecentlyOpenWithCategory(input, category, nowStamp, recentlyStamp);
            }
        }
        return input;
    }

    private void doRecentlyOpenWithCategory(
            final SortAndPageable<OrderInfo> input, Category category,
            final Timestamp nowStamp, final Timestamp recentlyStamp) {
        input.setPageContent(
                getOrderInfoList(
                        CATEGORY_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT,
                        input,
                        category.getId(),
                        recentlyStamp,
                        nowStamp));
        input.setCount(
                getOrderInfoCount(
                        CATEGORY_COUNT_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT,
                        category.getId(),
                        recentlyStamp,
                        nowStamp));
    }

    private void doRecentlyOpen(final SortAndPageable<OrderInfo> input,
            final Timestamp nowStamp, final Timestamp recentlyStamp) {
        input.setPageContent(
                getOrderInfoList(
                        OPEN_RECENTLY_FINANCED_COMPLETE_SELECT,
                        input,
                        recentlyStamp,
                        nowStamp));
        input.setCount(
                getOrderInfoCount(
                        COUNT_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT,
                        recentlyStamp,
                        nowStamp));
    }

    private void doArchiveWithCategory(final SortAndPageable<OrderInfo> input,
            final int maxOrderDuration, Category category,
            final Timestamp nowStamp) {
        input.setPageContent(
                getOrderInfoList(
                        replaceMaxOrderDuration(CATEGORY_ARCHIVED_COMPLETE_SLECT, maxOrderDuration),
                        input,
                        category.getId(),
                        nowStamp));
        input.setCount(
                getOrderInfoCount(
                        replaceMaxOrderDuration(CATEGORY_COUNT_ARCHIVED_COMPLETE_SLECT, maxOrderDuration),
                        category.getId(),
                        nowStamp));
    }

    private void doArchiveOnly(final SortAndPageable<OrderInfo> input,
            final int maxOrderDuration, final Timestamp nowStamp) {
        input.setPageContent(
                getOrderInfoList(
                        replaceMaxOrderDuration(ARCHIVED_COMPLETE_SLECT, maxOrderDuration),
                        input,
                        nowStamp));
        input.setCount(
                getOrderInfoCount(
                        replaceMaxOrderDuration(COUNT_ARCHIVED_COMPLETE_SLECT, maxOrderDuration),
                        nowStamp));
    }


    private String replaceMaxOrderDuration(final String sql, int maxOrderDuration) {
        return StringUtils.replace(sql, MAX_DURATION_PLACEHOLDER, String.valueOf(maxOrderDuration));
    }

    private List<OrderInfo> getOrderInfoList(final String sql, final SortAndPageable<OrderInfo> input, final Object... params) {
        return getListBySinglePagableQuery(
                sql,
                OrderInfo.class,
                ALLOWED_ORDER_COLUMNS,
                input.getOrderBy(),
                input.isSortAscending(),
                ArrayUtils.addAll(params, new Object[] {input.getOffset(), input.getPageCount()}));
    }
    
    private long getOrderInfoCount(final String sql, final Object... params) {
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setUnique(true);
        final Long result = (Long) q.executeWithArray(params);
        return result == null ? 0 : result;
    }

    public List<OrderInfo> getOrderInfosForGroup(Long groupId) {
        final Query q = getPersistenceManager().newQuery(Query.SQL, CP_FOR_GROUPE_COMPLETE_SELECT);
        q.setResultClass(OrderInfo.class);
        return (List<OrderInfo>) q.execute(groupId);
    }

    @Override
    public List<AdvancedOrderInfo> getRecentlyFinancedAndCurrentOpenAdvancedOrderInfos(
            Date now, Date recentlyDate, String sortcolumn, boolean ascending,
            int offset, int pageCount) {
        final Timestamp nowStamp = new Timestamp(now.getTime());
        return getListBySinglePagableQuery(
                ADVANCED_OPEN_RECENTLY_FINANCED_COMPLETE_SELECT,
                AdvancedOrderInfo.class,
                ALLOWED_ORDER_COLUMNS,
                sortcolumn,
                ascending,
                new Timestamp(recentlyDate.getTime()),
                nowStamp,
                offset,
                pageCount);
    }


    @Override
    public Set<Long> getUpdateRoiOrders(final Set<String> states) {
        final Map<String, Object> params = Collections.singletonMap("states", (Object) states);
        final String sql = replaceCollectionTypes(params, UPDATE_ROI_ORDERS_SELECT);
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setResultClass(Long.class);
        final List<Long> rawResult = (List<Long>) q.executeWithMap(params);
        return rawResult == null ? Collections.EMPTY_SET : new LinkedHashSet<Long>(rawResult);
    }



    @Override
    public Set<Order> getInternalDebConsolidationRelevantMainOrders(Account account) {
        final Map<String, Object> params = getSingleParamMap("accountId", account.getId());
        final Collection<Order> result = (Collection<Order>) executeNamedQuery(Order.class, "getInternalDebConsolidationRelevantContracts", params, null, false);
        return new LinkedHashSet<Order>(result);
    }


    @Override
    public Long countInternalDebtConsolidationOrders(Account account) {
        Query query = getPersistenceManager().newNamedQuery(Order.class, "countInternalDebtConsolidationOrders");
        query.setUnique(true);
        query.setResultClass(Long.class);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("accountId", account.getId());

        return (Long) query.executeWithMap(params);
    }

    public Collection<Order> getOrdersWithOpenOrQueueStateInDates(Calendar before, Calendar after) {
        OqlTerm stateTerm = OqlTerm.newTerm();
        stateTerm.or(stateTerm.equals(_STATE, Bid.STATE_OPEN), stateTerm.equals(_STATE, Bid.STATE_QUEUE));
        OqlTerm term = OqlTerm.newTerm();
        term.and(stateTerm, term.greaterThanEquals("_activationDate", after.getTime()), term.lessThan("_activationDate", before.getTime()));
        return this.findOrderList(term.toString(), after.getTime(), before.getTime());
    }

    @Override
    public Collection<Order> findOrdersOfStateOlderThan(String state, Date date) {
        OqlTerm term = OqlTerm.newTerm();
        term.and(term.equals(_STATE, state), term.lessThan("_activationDate", date));
        return this.findOrderList(term.toString(), date);
    }

    @Override
    public Collection<Order> findOrdersByBorrowers(List<Account> borrowers, Pageable pageable, Sortable sortable) {
        Object[] params = borrowers.toArray(new Account[borrowers.size()]);
        String whereBorrower = "";
        String orDelim = "";
        StringBuffer whereParameters = new StringBuffer();
        String comma = "";
        for (int i = 0; i < borrowers.size(); i++) {
            whereBorrower += orDelim + "_account == param" + (i + 1);
            whereParameters.append(comma + "Account param" + (i + 1));
            comma = ", ";
            orDelim = " || ";
        }

        String where = "(_state == '" + Order.STATE_OPEN + "' || _state =='" + Order.STATE_QUEUE + "')";
        if (!"".equals(whereBorrower)) {
            where += " && (" + whereBorrower + ") PARAMETERS " + whereParameters;
        }
        LOGGER.debug("****** WHERE " + where);
        Collection<Order> list = new ArrayList<Order>();
        if (!borrowers.isEmpty()) {
            list = findOrderList(where, pageable, sortable, params);
        }
        LOGGER.debug("****** LIST " + list.size());
        return list;
    }

    @Override
    public Collection<Order> findByRateMarketCodes(String rateMarketCodes, Date orderDate) {
        final String termString = "((" + rateMarketCodes + ") && _state == '" + Bid.STATE_OPEN + "' && _creationDate <= param1) PARAMETERS java.util.Date param1";
        return findOrderList(termString, orderDate);
    }

    @Override
    public Collection<Order> getExpiredOrders(Account account) {
        OqlTerm expiredOrdersTerm = OqlTerm.newTerm();
        expiredOrdersTerm.equals("_state", Bid.STATE_EXPIRED);
        OqlTerm borrowerIdTerm = OqlTerm.newTerm();
        borrowerIdTerm.equals("_account._id", account.getId());
        OqlTerm expiredCountTerm = OqlTerm.newTerm();
        expiredCountTerm.and(expiredOrdersTerm, borrowerIdTerm);
        return findOrderList(expiredCountTerm.toString());
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
