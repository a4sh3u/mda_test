package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BidInterestHistory;
import de.smava.webapp.commons.domain.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * The domain object that represents 'BidInterests'.
 */
public class BidInterest extends BidInterestHistory  {

    private transient List<Account> _modifiers = null;

    public List<? extends Entity> getModifier() {
        if (this._modifiers == null) {
            Account account = null;
            if (getBid() != null) {
                if (getBid() instanceof Order) {
                    account = ((Order) getBid()).getAccount();
                }
                if (getBid() instanceof Offer) {
                    account = ((Offer) getBid()).getAccount();
                }
            }
            if (account != null) {
                this._modifiers = Arrays.asList(account);
            } else {
                this._modifiers = new ArrayList<Account>();
            }
        }
        return this._modifiers;
    }

        protected Date _creationDate;
        protected Bid _bid;
        protected Date _validUntil;
        protected String _marketName;
        protected Float _rate;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'bid'.
     */
    public void setBid(Bid bid) {
        _bid = bid;
    }
            
    /**
     * Returns the property 'bid'.
     */
    public Bid getBid() {
        return _bid;
    }
                                    /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }
                                    /**
     * Setter for the property 'market name'.
     */
    public void setMarketName(String marketName) {
        if (!_marketNameIsSet) {
            _marketNameIsSet = true;
            _marketNameInitVal = getMarketName();
        }
        registerChange("market name", _marketNameInitVal, marketName);
        _marketName = marketName;
    }
                        
    /**
     * Returns the property 'market name'.
     */
    public String getMarketName() {
        return _marketName;
    }
                                    /**
     * Setter for the property 'rate'.
     */
    public void setRate(Float rate) {
        if (!_rateIsSet) {
            _rateIsSet = true;
            _rateInitVal = getRate();
        }
        registerChange("rate", _rateInitVal, rate);
        _rate = rate;
    }
                        
    /**
     * Returns the property 'rate'.
     */
    public Float getRate() {
        return _rate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BidInterest.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _marketName=").append(_marketName);
            builder.append("\n    _rate=").append(_rate);
            builder.append("\n}");
        } else {
            builder.append(BidInterest.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
