//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score address)}

import de.smava.webapp.account.dao.SchufaScoreAddressDao;
import de.smava.webapp.account.domain.SchufaScoreAddress;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'SchufaScoreAddresss'.
 *
 * @author generator
 */
@Repository(value = "schufaScoreAddressDao")
public class JdoSchufaScoreAddressDao extends JdoBaseDao implements SchufaScoreAddressDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSchufaScoreAddressDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(schufa score address)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the schufa score address identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public SchufaScoreAddress load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddress - start: id=" + id);
        }
        SchufaScoreAddress result = getEntity(SchufaScoreAddress.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddress - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public SchufaScoreAddress getSchufaScoreAddress(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	SchufaScoreAddress entity = findUniqueEntity(SchufaScoreAddress.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the schufa score address.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(SchufaScoreAddress schufaScoreAddress) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveSchufaScoreAddress: " + schufaScoreAddress);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(schufa score address)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(schufaScoreAddress);
    }

    /**
     * @deprecated Use {@link #save(SchufaScoreAddress) instead}
     */
    public Long saveSchufaScoreAddress(SchufaScoreAddress schufaScoreAddress) {
        return save(schufaScoreAddress);
    }

    /**
     * Deletes an schufa score address, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa score address
     */
    public void deleteSchufaScoreAddress(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteSchufaScoreAddress: " + id);
        }
        deleteEntity(SchufaScoreAddress.class, id);
    }

    /**
     * Retrieves all 'SchufaScoreAddress' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<SchufaScoreAddress> getSchufaScoreAddressList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - start");
        }
        Collection<SchufaScoreAddress> result = getEntities(SchufaScoreAddress.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'SchufaScoreAddress' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<SchufaScoreAddress> getSchufaScoreAddressList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - start: pageable=" + pageable);
        }
        Collection<SchufaScoreAddress> result = getEntities(SchufaScoreAddress.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScoreAddress' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<SchufaScoreAddress> getSchufaScoreAddressList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - start: sortable=" + sortable);
        }
        Collection<SchufaScoreAddress> result = getEntities(SchufaScoreAddress.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreAddress' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<SchufaScoreAddress> getSchufaScoreAddressList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreAddress> result = getEntities(SchufaScoreAddress.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScoreAddress' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreAddress> findSchufaScoreAddressList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - start: whereClause=" + whereClause);
        }
        Collection<SchufaScoreAddress> result = findEntities(SchufaScoreAddress.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScoreAddress' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<SchufaScoreAddress> findSchufaScoreAddressList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<SchufaScoreAddress> result = findEntities(SchufaScoreAddress.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'SchufaScoreAddress' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreAddress> findSchufaScoreAddressList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<SchufaScoreAddress> result = findEntities(SchufaScoreAddress.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScoreAddress' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreAddress> findSchufaScoreAddressList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<SchufaScoreAddress> result = findEntities(SchufaScoreAddress.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreAddress' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreAddress> findSchufaScoreAddressList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreAddress> result = findEntities(SchufaScoreAddress.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreAddress' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreAddress> findSchufaScoreAddressList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreAddress> result = findEntities(SchufaScoreAddress.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreAddress' instances.
     */
    public long getSchufaScoreAddressCount() {
        long result = getEntityCount(SchufaScoreAddress.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreAddress' instances which match the given whereClause.
     */
    public long getSchufaScoreAddressCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(SchufaScoreAddress.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreAddress' instances which match the given whereClause together with params specified in object array.
     */
    public long getSchufaScoreAddressCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(SchufaScoreAddress.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreAddressCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa score address)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
