package de.smava.webapp.account.domain;

import java.io.Serializable;
import java.text.DecimalFormat;

import org.apache.log4j.Logger;

import com.aperto.webkit.utils.ExceptionEater;


/**
 * Domain object for the different types of markets.
 *
 *
 * Note: full market codes may look like this: A12 or 112, B24 or 224, M12 or 1312, so the code
 * might have 4 characters...
 *
 *
 * @author michael.schwalbe (25.10.2005), i.decker (21.7.2006)
 */
public class Market implements Serializable, Comparable<Market> {
	
	/**
	 * All durations we ever had since the active set might be smaller but we might have old durations in the system.
	 */
	public static final int[] ALL_DURATIONS = new int[] { 3, 6, 9, 12, 15, 18,
			24, 30, 36, 42, 48, 54, 60, 72, 84, 96, 108, 120, 132, 144 };
	
	private static final Logger LOGGER = Logger.getLogger(Market.class);
	
    private static final long serialVersionUID = -343468038162759682L;

    public static final int MAX_RATING = 13;
    
    /** @deprecated Only for tests! */
    public static final Market MARKET_A12 = new Market("A", 12);
    /** @deprecated Only for tests! */
    public static final Market MARKET_A24 = new Market("A", 24);
    /** @deprecated Only for tests! */
    public static final Market MARKET_B12 = new Market("B", 12);
    /** @deprecated Only for tests! */
    public static final Market MARKET_B24 = new Market("B", 24);
    /** @deprecated Only for tests! */
    public static final Market MARKET_C12 = new Market("C", 12);
    /** @deprecated Only for tests! */
    public static final Market MARKET_C24 = new Market("C", 24);

    private final int _rating;
    private final int _duration;

    /**
     * Get rating of market name, e.g. 1 for "A06" or "106", or 2 for "B12", or 13 for "1312".
     */
    public static int getRatingOfCode (String fullMarketCode) {
        // Note: full market codes may look like this: A12 or 112, B24 or 224, M12 or 1312, so the code
        // might have 4 characters...        int duration = getDurationOfCode(code);
    	if (fullMarketCode.length() == 2) { // we have one character duration (4 ex. 3, 6 or 9)
    		fullMarketCode = fullMarketCode.charAt(0) + "0" + fullMarketCode.charAt(1);
    	}
        if (fullMarketCode == null || fullMarketCode.length() < 3 || fullMarketCode.length() > 4) {
            throw new IllegalArgumentException("Illegal market code " + fullMarketCode);
        }
        int rating = -1;
        if (fullMarketCode.length() == 3) {
            int ratingChar = (int) fullMarketCode.charAt(0);
            if (ratingChar >= 65) {
                rating = ratingChar - 64;
            } else {
                rating = ratingChar - 48;
            }
        } else {
            String ratingString = fullMarketCode.substring(0, 2);
            try {
                rating = Integer.parseInt(ratingString);
            } catch (Exception e) {
                ExceptionEater.eat(e);
            }
        }
        if (!isLegalRating(rating)) {
            throw new IllegalArgumentException("Illegal rating of market code " + fullMarketCode);
        }
        return rating;
    }

    /**
     * Get duration of market name, e.g. 6 for "C06" or "306", or 12 for "1312".
     */
    public static  int getDurationOfCode (String fullMarketCode) {
        // Note: full market codes may look like this: A12 or 112, B24 or 224, M12 or 1312, so the code
        // might have 4 characters...        int duration = getDurationOfCode(code);
    	if (fullMarketCode.length() == 2) { // we have one character duration (4 ex. 3, 6 or 9)
    		fullMarketCode = fullMarketCode.charAt(0) + "0" + fullMarketCode.charAt(1);
    	}
        if (fullMarketCode == null || fullMarketCode.length() < 3 || fullMarketCode.length() > 4) {
            throw new IllegalArgumentException("Illegal market code " + fullMarketCode);
        }
        int duration;
        if (fullMarketCode.length() == 3) {
            duration = Integer.parseInt(fullMarketCode.substring(1));
        } else {
            duration = Integer.parseInt(fullMarketCode.substring(2));
        }
        return duration;
    }
    
    /**
     * @return True if rating in range
     */
    public static boolean isLegalRating (int rating) {
        return rating >= 1 && rating <= MAX_RATING ;
    }

    /**
     * @return True if rating in range
     */
    public static boolean isLegalRating (String ratingLetter) {
        boolean rc = false;
        try {
            if (ratingLetter.length() == 1) {
                rc = isLegalRating((int) ratingLetter.charAt(0) - 64);
            }
        } catch (Exception e) {
            LOGGER.warn("error while checking rating", e);
        }
        return rc;
    }
    
    /**
     * @return True if duration in range
     */
    public static boolean isLegalDuration (int duration) {
        boolean rc = false;
        for (int d : ALL_DURATIONS) {
            if (d == duration) {
                rc = true;
            }
        }
        return rc;
    }
    
    /**
     * Convert rating letter to number.
     */
    public static int toRatingNumber (String ratingLetter) {
        if (!isLegalRating(ratingLetter)) {
            throw new IllegalArgumentException("Rating out of range: " + ratingLetter);
        }
        return (int) ratingLetter.charAt(0) - 64;
    }
    
    /**
     * @return True if duration in range
     */
    public static boolean isLegalDuration (String duration) {
        boolean rc = false;
        try {
            rc = isLegalDuration(Integer.parseInt(duration));
        } catch (Exception e) {
            ExceptionEater.eat(e);
        }
        return rc;
    }

    
    public Market(String marketCode) {
        _rating = getRatingOfCode(marketCode);
        _duration = getDurationOfCode(marketCode);
    }

    public Market(String rating, int duration) {
        if (!isLegalRating(rating)) {
            throw new IllegalArgumentException("Rating out or range: " + rating);
        }
        if (!isLegalDuration(duration)) {
            throw new IllegalArgumentException("Duration out or range: " + duration);
        }
        _rating = toRatingNumber(rating);
        _duration = duration;
    }

    public Market(String rating, String duration) {
        if (rating.matches("[0-9]+")) {
            if (!isLegalRating(Integer.parseInt(rating))) {
                throw new IllegalArgumentException("Rating out or range: " + rating);
            }
            _rating = Integer.parseInt(rating);
        } else {
            if (!isLegalRating(rating)) {
                throw new IllegalArgumentException("Rating out or range: " + rating);
            }
            _rating = toRatingNumber(rating);
        }
        if (!isLegalDuration(duration)) {
            throw new IllegalArgumentException("Duration out or range: " + duration);
        }
        _duration = Integer.parseInt(duration);
    }

    public int getRating() {
        return _rating;
    }

    public int getDuration() {
        return _duration;
    }

    /**
     * @return Rating as letter
     */
    public String getRatingAsLetter() {
        // ascii(A) = 65, rating(A) = 1 ==> getRating() + 64
        return String.valueOf((char) (getRating() + 64));
    }

    /**
     * @return Rating as number string without leading zeros.
     */
    public String getRatingAsString() {
        return Integer.toString(getRating());
    }

    /**
     * @return Duration as string, like "12" or "06".
     */
    public String getDurationAsString() {
        return new DecimalFormat("00").format(getDuration());
    }

    /**
     * Convenience method for providing a name composed of the rating as letter and the duration.
     * @return String a name composed of the rating and the duration, like "B12".
     */
    public String getMarketName() {
        StringBuilder nameBuilder = new StringBuilder();
        //nameBuilder.append(getRatingAsString());
        nameBuilder.append(getRatingAsLetter());
        nameBuilder.append(getDurationAsString());
        return nameBuilder.toString();
    }

    /**
     * Convenience method for providing a name composed of the rating as letter and the duration.
     * @return String a name composed of the creditWorthiness and the duration like "A12".
     */
    public String getMarketNameWithRatingAsLetter() {
        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder.append(getRatingAsLetter());
        nameBuilder.append(getDurationAsString());
        return nameBuilder.toString();
    }


    public boolean equals(Object anotherMarketObject) {
        boolean result = false;
        if (anotherMarketObject instanceof Market) {
            Market anotherMarket = (Market) anotherMarketObject;
            if (_rating == anotherMarket.getRating() && _duration == anotherMarket.getDuration()) {
                result = true;
            }
        }
        return result;
    }

    public int hashCode() {
        return getMarketName().hashCode();
    }

    public int compareTo(Market o) {
        return getMarketNameWithRatingAsLetter().compareTo(o.getMarketNameWithRatingAsLetter());   
    }

    @Override
    public String toString() {
        return super.toString() + " - " + getMarketNameWithRatingAsLetter();
    }
    
    
}