//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse-ganymede\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse-ganymede\workspace\smava-trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score token)}

import de.smava.webapp.account.domain.SchufaScoreToken;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'SchufaScoreTokens'.
 *
 * @author generator
 */
public interface SchufaScoreTokenDao extends BaseDao<SchufaScoreToken>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the schufa score token identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    SchufaScoreToken getSchufaScoreToken(Long id);

    /**
     * Saves the schufa score token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveSchufaScoreToken(SchufaScoreToken schufaScoreToken);

    /**
     * Deletes an schufa score token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa score token
     */
    void deleteSchufaScoreToken(Long id);

    /**
     * Retrieves all 'SchufaScoreToken' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<SchufaScoreToken> getSchufaScoreTokenList();

    /**
     * Retrieves a page of 'SchufaScoreToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<SchufaScoreToken> getSchufaScoreTokenList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'SchufaScoreToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<SchufaScoreToken> getSchufaScoreTokenList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'SchufaScoreToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<SchufaScoreToken> getSchufaScoreTokenList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'SchufaScoreToken' instances.
     */
    long getSchufaScoreTokenCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa score token)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
