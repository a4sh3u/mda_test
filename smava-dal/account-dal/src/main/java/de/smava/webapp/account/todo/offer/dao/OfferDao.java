//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.offer.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(offer)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BankAccountProvider;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.account.domain.Offer;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Offers'.
 *
 * @author generator
 */
public interface OfferDao extends BaseDao<Offer>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the offer identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Offer getOffer(Long id);

    /**
     * Saves the offer.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveOffer(Offer offer);

    /**
     * Deletes an offer, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the offer
     */
    void deleteOffer(Long id);

    /**
     * Retrieves all 'Offer' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Offer> getOfferList();

    /**
     * Retrieves a page of 'Offer' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Offer> getOfferList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Offer' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Offer> getOfferList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Offer' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Offer> getOfferList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Offer' instances.
     */
    long getOfferCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(offer)}

    Collection<Offer> findOffersOfSameGroup(Offer offer);

    Collection<Offer> findOfferList(Account lender);

    Collection<Offer> findOfferList(Account lender, BankAccountProvider bankAccountProvider, Collection<String> states);

    Collection<Offer> findOfferListWithoutBankAccountProvider(Account lender, Collection<String> states);

    Collection<Offer> findOfferList(String market, Collection<String> states);

    Collection<Offer> findOfferList(String market, Collection<String> states, Pageable pageable, Sortable sortable);

    Collection<Offer> findOfferList(Account account, Pageable pageable, Sortable sortable);
    
    Collection<Offer> findOfferByBidSearch(BidSearch bidSearch);
    
    Long getOfferCountForPlacement(Long marketingPlacementId);

    Collection<Offer> findOpenOffersForAccount(Account account);

    // !!!!!!!! End of insert code section !!!!!!!!
}
