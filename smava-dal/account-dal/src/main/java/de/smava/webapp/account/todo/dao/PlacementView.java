/**
 * 
 */
package de.smava.webapp.account.todo.dao;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author bvoss
 *
 */
public class PlacementView implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long _id;
	private String _publicationName;
	private String _partnerName;
	private String _campaignName;
	private Timestamp _validUntil;
	
	public PlacementView(Long id) {
		super();
		_id = id;
	}
	public PlacementView() {
		super();
	}
	public final Long getId() {
		return _id;
	}
	public final void setId(Long id) {
		_id = id;
	}
	public final String getPublicationName() {
		return _publicationName;
	}
	public final void setPublicationName(String publicationName) {
		_publicationName = publicationName;
	}
	public final String getPartnerName() {
		return _partnerName;
	}
	public final void setPartnerName(String partnerName) {
		_partnerName = partnerName;
	}
	public final String getCampaignName() {
		return _campaignName;
	}
	public final void setCampaignName(String campaignName) {
		_campaignName = campaignName;
	}
	public final Timestamp getValidUntil() {
		return _validUntil;
	}
	public final void setValidUntil(Timestamp validUntil) {
		_validUntil = validUntil;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_id == null) ? 0 : _id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PlacementView other = (PlacementView) obj;
		if (_id == null) {
			if (other._id != null) {
				return false;
			}
		} else if (!_id.equals(other._id)) {
			return false;
		}
		return true;
	}
}
