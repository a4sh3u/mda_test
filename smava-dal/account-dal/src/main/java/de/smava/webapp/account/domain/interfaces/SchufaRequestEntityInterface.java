package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.SchufaScoreRequestData;

import java.util.Date;


/**
 * The domain object that represents 'SchufaRequests'.
 *
 * @author generator
 */
public interface SchufaRequestEntityInterface {

    /**
     * Setter for the property 'request xml'.
     *
     * 
     *
     */
    void setRequestXml(String requestXml);

    /**
     * Returns the property 'request xml'.
     *
     * 
     *
     */
    String getRequestXml();
    /**
     * Setter for the property 'response xml'.
     *
     * 
     *
     */
    void setResponseXml(String responseXml);

    /**
     * Returns the property 'response xml'.
     *
     * 
     *
     */
    String getResponseXml();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'data'.
     *
     * 
     *
     */
    void setData(SchufaScoreRequestData data);

    /**
     * Returns the property 'data'.
     *
     * 
     *
     */
    SchufaScoreRequestData getData();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'account number'.
     *
     * 
     *
     */
    void setAccountNumber(String accountNumber);

    /**
     * Returns the property 'account number'.
     *
     * 
     *
     */
    String getAccountNumber();
    /**
     * Setter for the property 'schufa person key'.
     *
     * 
     *
     */
    void setSchufaPersonKey(String schufaPersonKey);

    /**
     * Returns the property 'schufa person key'.
     *
     * 
     *
     */
    String getSchufaPersonKey();
    /**
     * Setter for the property 'person id'.
     *
     * 
     *
     */
    void setPersonId(Long personId);

    /**
     * Returns the property 'person id'.
     *
     * 
     *
     */
    Long getPersonId();

}
