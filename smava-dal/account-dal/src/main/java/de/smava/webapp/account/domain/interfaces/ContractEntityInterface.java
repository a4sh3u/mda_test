package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Booking;
import de.smava.webapp.account.domain.Document;
import de.smava.webapp.account.domain.Offer;
import de.smava.webapp.account.domain.Order;

import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'Contracts'.
 *
 * @author generator
 */
public interface ContractEntityInterface {

    /**
     * Setter for the property 'contract number'.
     *
     * 
     *
     */
    void setContractNumber(String contractNumber);

    /**
     * Returns the property 'contract number'.
     *
     * 
     *
     */
    String getContractNumber();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'offer'.
     *
     * 
     *
     */
    void setOffer(Offer offer);

    /**
     * Returns the property 'offer'.
     *
     * 
     *
     */
    Offer getOffer();
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    void setOrder(Order order);

    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    Order getOrder();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'seal date'.
     *
     * 
     *
     */
    void setSealDate(Date sealDate);

    /**
     * Returns the property 'seal date'.
     *
     * 
     *
     */
    Date getSealDate();
    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    void setStartDate(Date startDate);

    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    Date getStartDate();
    /**
     * Setter for the property 'end date'.
     *
     * 
     *
     */
    void setEndDate(Date endDate);

    /**
     * Returns the property 'end date'.
     *
     * 
     *
     */
    Date getEndDate();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    double getAmount();
    /**
     * Setter for the property 'rate'.
     *
     * 
     *
     */
    void setRate(float rate);

    /**
     * Returns the property 'rate'.
     *
     * 
     *
     */
    float getRate();
    /**
     * Setter for the property 'market name'.
     *
     * 
     *
     */
    void setMarketName(String marketName);

    /**
     * Returns the property 'market name'.
     *
     * 
     *
     */
    String getMarketName();
    /**
     * Setter for the property 'effective yield borrower'.
     *
     * 
     *
     */
    void setEffectiveYieldBorrower(float effectiveYieldBorrower);

    /**
     * Returns the property 'effective yield borrower'.
     *
     * 
     *
     */
    float getEffectiveYieldBorrower();
    /**
     * Setter for the property 'total contract repayment'.
     *
     * 
     *
     */
    void setTotalContractRepayment(double totalContractRepayment);

    /**
     * Returns the property 'total contract repayment'.
     *
     * 
     *
     */
    double getTotalContractRepayment();
    /**
     * Setter for the property 'bookings'.
     *
     * 
     *
     */
    void setBookings(Collection<Booking> bookings);

    /**
     * Returns the property 'bookings'.
     *
     * 
     *
     */
    Collection<Booking> getBookings();
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    void setDocuments(List<Document> documents);

    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    List<Document> getDocuments();
    /**
     * Setter for the property 'encashment rate'.
     *
     * 
     *
     */
    void setEncashmentRate(double encashmentRate);

    /**
     * Returns the property 'encashment rate'.
     *
     * 
     *
     */
    double getEncashmentRate();
    /**
     * Setter for the property 'marketing placement id'.
     *
     * 
     *
     */
    void setMarketingPlacementId(Long marketingPlacementId);

    /**
     * Returns the property 'marketing placement id'.
     *
     * 
     *
     */
    Long getMarketingPlacementId();
    /**
     * Setter for the property 'updated roi'.
     *
     * 
     *
     */
    void setUpdatedRoi(double updatedRoi);

    /**
     * Returns the property 'updated roi'.
     *
     * 
     *
     */
    double getUpdatedRoi();
    /**
     * Setter for the property 'provision rate fix lender'.
     *
     * 
     *
     */
    void setProvisionRateFixLender(Double provisionRateFixLender);

    /**
     * Returns the property 'provision rate fix lender'.
     *
     * 
     *
     */
    Double getProvisionRateFixLender();
    /**
     * Setter for the property 'provision rate lender'.
     *
     * 
     *
     */
    void setProvisionRateLender(Double provisionRateLender);

    /**
     * Returns the property 'provision rate lender'.
     *
     * 
     *
     */
    Double getProvisionRateLender();
    /**
     * Setter for the property 'provision rate borrower'.
     *
     * 
     *
     */
    void setProvisionRateBorrower(Double provisionRateBorrower);

    /**
     * Returns the property 'provision rate borrower'.
     *
     * 
     *
     */
    Double getProvisionRateBorrower();
    /**
     * Setter for the property 'provision rate fix borrower'.
     *
     * 
     *
     */
    void setProvisionRateFixBorrower(Double provisionRateFixBorrower);

    /**
     * Returns the property 'provision rate fix borrower'.
     *
     * 
     *
     */
    Double getProvisionRateFixBorrower();
    /**
     * Setter for the property 'provision refund rate'.
     *
     * 
     *
     */
    void setProvisionRefundRate(Double provisionRefundRate);

    /**
     * Returns the property 'provision refund rate'.
     *
     * 
     *
     */
    Double getProvisionRefundRate();
    /**
     * Setter for the property 'refund period'.
     *
     * 
     *
     */
    void setRefundPeriod(Integer refundPeriod);

    /**
     * Returns the property 'refund period'.
     *
     * 
     *
     */
    Integer getRefundPeriod();
    /**
     * Setter for the property 'expected order roi'.
     *
     * 
     *
     */
    void setExpectedOrderRoi(double expectedOrderRoi);

    /**
     * Returns the property 'expected order roi'.
     *
     * 
     *
     */
    double getExpectedOrderRoi();
    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    void setSharedLoan(Boolean sharedLoan);

    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    Boolean getSharedLoan();
    /**
     * Setter for the property 'encashment partner no'.
     *
     * 
     *
     */
    void setEncashmentPartnerNo(Integer encashmentPartnerNo);

    /**
     * Returns the property 'encashment partner no'.
     *
     * 
     *
     */
    Integer getEncashmentPartnerNo();

}
