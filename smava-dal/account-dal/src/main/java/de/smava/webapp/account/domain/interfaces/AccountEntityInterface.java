package de.smava.webapp.account.domain.interfaces;

import de.smava.webapp.account.domain.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface AccountEntityInterface {

    void setAccountId(Long accountId);

    Long getAccountId();

    void setCreationDate(Date creationDate);

    Date getCreationDate();

    void setPersonHistory(Collection<Person> personHistory);

    Collection<Person> getPersonHistory();

    void setCredentials(Credentials credentials);

    Credentials getCredentials();

    void setState(String state);

    String getState();

    void setLockDate(Date lockDate);

    Date getLockDate();

    void setUsername(String username);

    String getUsername();

    void setEmail(String email);

    String getEmail();

    void setPhone(String phone);

    String getPhone();

    void setPhoneCode(String phoneCode);

    String getPhoneCode();

    void setPhone2(String phone2);

    String getPhone2();

    void setPhone2Code(String phone2Code);

    String getPhone2Code();

    void setAssetManagerGuidelineDate(Date assetManagerGuidelineDate);

    Date getAssetManagerGuidelineDate();

    void setAcceptanceDate(Date acceptanceDate);

    Date getAcceptanceDate();

    void setLastLogin(Date lastLogin);

    Date getLastLogin();

    void setNewsletterSubscription(boolean newsletterSubscription);

    boolean getNewsletterSubscription();

    void setAccountRoles(Collection<AccountRole> accountRoles);

    Collection<AccountRole> getAccountRoles();

    void setOrders(Set<Order> orders);

    Set<Order> getOrders();

    void setOffers(Set<Offer> offers);

    Set<Offer> getOffers();

    void setProfile(Profile profile);

    Profile getProfile();

    void setBankAccounts(List<BankAccount> bankAccounts);

    List<BankAccount> getBankAccounts();

    void setMarkedOrders(List<Order> markedOrders);

    List<Order> getMarkedOrders();

    void setDocuments(List<Document> documents);

    List<Document> getDocuments();

    void setMessages(List<Message> messages);

    List<Message> getMessages();

    void setMessageConfig(MessageConfig messageConfig);

    MessageConfig getMessageConfig();

    void setMailLinks(List<MailLink> mailLinks);

    List<MailLink> getMailLinks();

    void setInvitedBy(Account invitedBy);

    Account getInvitedBy();

    void setAddresses(List<AccountAddress> addresses);

    List<AccountAddress> getAddresses();

    void setMarketingPlacementId(Long marketingPlacementId);

    Long getMarketingPlacementId();

    void setCreditScores(List<CreditScore> creditScores);

    List<CreditScore> getCreditScores();

    void setCurrentCreditScore(CreditScore currentCreditScore);

    CreditScore getCurrentCreditScore();

    void setEconomicalDataHistory(List<EconomicalData> economicalDataHistory);

    List<EconomicalData> getEconomicalDataHistory();

    void setPreferredRole(String preferredRole);

    String getPreferredRole();

    void setOrderRecommendations(List<OrderRecommendation> orderRecommendations);

    List<OrderRecommendation> getOrderRecommendations();

    void setRepaymentBreaks(Set<RepaymentBreak> repaymentBreaks);

    Set<RepaymentBreak> getRepaymentBreaks();

    void setExternalReferenceId(String externalReferenceId);

    String getExternalReferenceId();

    void setBidSearches(Collection<BidSearch> bidSearches);

    Collection<BidSearch> getBidSearches();

    void setCustomerAgreements(Collection<CustomerAgreement> customerAgreements);

    Collection<CustomerAgreement> getCustomerAgreements();

    void setBookingPerformance(BookingPerformance bookingPerformance);

    BookingPerformance getBookingPerformance();

    void setPaymentProfilShowDate(Date paymentProfilShowDate);

    Date getPaymentProfilShowDate();

    void setAverageUpdatedRoi(double averageUpdatedRoi);

    double getAverageUpdatedRoi();

    void setIsManaged(boolean isManaged);

    boolean getIsManaged();

    void setActivationString(String activationString);

    String getActivationString();

    void setMarketingTest(String marketingTest);

    String getMarketingTest();

    void setCallAgreementDate(Date callAgreementDate);

    Date getCallAgreementDate();

    void setLastRequestedAmount(Double lastRequestedAmount);

    Double getLastRequestedAmount();

    void setPhoneEmployer(String phoneEmployer);

    String getPhoneEmployer();

    void setPhoneCodeEmployer(String phoneCodeEmployer);

    String getPhoneCodeEmployer();

    void setPhoneEmployerCoBorrower(String phoneEmployerCoBorrower);

    String getPhoneEmployerCoBorrower();

    void setPhoneCodeEmployerCoBorrower(String phoneCodeEmployerCoBorrower);

    String getPhoneCodeEmployerCoBorrower();

    void setLastFrontendChanges(Date lastFrontendChanges);

    Date getLastFrontendChanges();

    void setBrokerageNetIncome(Double brokerageNetIncome);

    Double getBrokerageNetIncome();

    void setBrokerageDisposableIncome(Double brokerageDisposableIncome);

    Double getBrokerageDisposableIncome();

    void setEmailValidatedDate(Date emailValidatedDate);

    Date getEmailValidatedDate();

    void setPayoutMode(String payoutMode);

    String getPayoutMode();

    void setPapLeadTracked(Date papLeadTracked);

    Date getPapLeadTracked();

    void setPersonalAdvisorId(Long personalAdvisorId);

    Long getPersonalAdvisorId();

    void setReceiveLoanOffers(Date receiveLoanOffers);

    Date getReceiveLoanOffers();

    void setReceiveConsolidationOffers(Date receiveConsolidationOffers);

    Date getReceiveConsolidationOffers();

}
