package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BidSearch;
import de.smava.webapp.account.domain.BookingPerformance;
import de.smava.webapp.account.domain.EconomicalData;


/**
 * The domain object that represents 'Offers'.
 *
 * @author generator
 */
public interface OfferEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'bid search'.
     *
     * 
     *
     */
    void setBidSearch(BidSearch bidSearch);

    /**
     * Returns the property 'bid search'.
     *
     * 
     *
     */
    BidSearch getBidSearch();
    /**
     * Setter for the property 'marketing placement id'.
     *
     * 
     *
     */
    void setMarketingPlacementId(Long marketingPlacementId);

    /**
     * Returns the property 'marketing placement id'.
     *
     * 
     *
     */
    Long getMarketingPlacementId();
    /**
     * Setter for the property 'booking performance'.
     *
     * 
     *
     */
    void setBookingPerformance(BookingPerformance bookingPerformance);

    /**
     * Returns the property 'booking performance'.
     *
     * 
     *
     */
    BookingPerformance getBookingPerformance();
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    void setEconomicalData(EconomicalData economicalData);

    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    EconomicalData getEconomicalData();
    /**
     * Setter for the property 'provision rate fix'.
     *
     * 
     *
     */
    void setProvisionRateFix(Double provisionRateFix);

    /**
     * Returns the property 'provision rate fix'.
     *
     * 
     *
     */
    Double getProvisionRateFix();
    /**
     * Setter for the property 'provision rate lender'.
     *
     * 
     *
     */
    void setProvisionRateLender(Double provisionRateLender);

    /**
     * Returns the property 'provision rate lender'.
     *
     * 
     *
     */
    Double getProvisionRateLender();

}
