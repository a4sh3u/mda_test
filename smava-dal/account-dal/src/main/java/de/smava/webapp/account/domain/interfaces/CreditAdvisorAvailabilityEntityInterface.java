package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;


/**
 * The domain object that represents 'CreditAdvisorAvailabilitys'.
 *
 * @author generator
 */
public interface CreditAdvisorAvailabilityEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'availability'.
     *
     * 
     *
     */
    void setAvailability(double availability);

    /**
     * Returns the property 'availability'.
     *
     * 
     *
     */
    double getAvailability();

}
