package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Bid;

import java.util.Date;


/**
 * The domain object that represents 'BidInterests'.
 *
 * @author generator
 */
public interface BidInterestEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'bid'.
     *
     * 
     *
     */
    void setBid(Bid bid);

    /**
     * Returns the property 'bid'.
     *
     * 
     *
     */
    Bid getBid();
    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Setter for the property 'market name'.
     *
     * 
     *
     */
    void setMarketName(String marketName);

    /**
     * Returns the property 'market name'.
     *
     * 
     *
     */
    String getMarketName();
    /**
     * Setter for the property 'rate'.
     *
     * 
     *
     */
    void setRate(Float rate);

    /**
     * Returns the property 'rate'.
     *
     * 
     *
     */
    Float getRate();

}
