/**
 * 
 */
package de.smava.webapp.account.domain;

import java.util.Collections;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author bvoss
 *
 */
public class BankAccountTransferDirection {
	private final Set<BankAccount> _debits;
	private final Set<BankAccount> _credits;
	
	public BankAccountTransferDirection(BankAccount debit,
			BankAccount credit) {
		super();
		_debits = debit != null ? Collections.singleton(debit) : Collections.<BankAccount>emptySet();
		_credits = credit != null ? Collections.singleton(credit) : Collections.<BankAccount>emptySet();
	}
	
	public BankAccountTransferDirection(Set<BankAccount> debit,
			Set<BankAccount> credit) {
		super();
		_debits = debit != null ? Collections.unmodifiableSet(debit) : Collections.<BankAccount>emptySet();
		_credits = credit != null ? Collections.unmodifiableSet(credit) : Collections.<BankAccount>emptySet();
	}
	
	public Set<BankAccount> getDebits() {
		return _debits;
	}

	public Set<BankAccount> getCredits() {
		return _credits;
	}

	public BankAccount getDebit() {
		return getSingle(_debits);
	}

	public BankAccount getCredit() {
		return getSingle(_credits);
	}
	
	private BankAccount getSingle(Set<BankAccount> accounts) {
		final BankAccount result;
		if (CollectionUtils.isEmpty(accounts)) {
			result = null;
		} else if (accounts.size() == 1) {
			result = accounts.iterator().next();
		} else {
			throw new IllegalArgumentException("can't return a single bannk account for a set of size " + accounts.size());
		}
		return result;
	}

}
