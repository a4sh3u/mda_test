/**
 * 
 */
package de.smava.webapp.account.domain;

import de.smava.webapp.commons.domain.MappableEnum;
import de.smava.webapp.commons.domainutil.MappableEnumHelper;

/**
 * @author Dimitar Robev
 *
 */
public enum EmploymentType implements MappableEnum<EmploymentType> {

	EMPLOYEE(1),
	CIVIL_CERVANT(2),
	FREELANCER(3),
	TRADESMAN(4),
	CEO(5),
	RETIREE(6),
	OTHER(7),
	ANNUITANT(8),
	STUDENT(9),
	LABOURER(10);

	private final Integer _dbValue;

	private EmploymentType(final int dbValue) {
		_dbValue = dbValue;
	}

	public Number getDbValue() {
		return _dbValue;
	}

	public EmploymentType getEnumFromDbValue(Number dbValue) {
		return enumFromDbValue(dbValue);
	}

	public static EmploymentType enumFromDbValue(final Number dbValue) {
		return MappableEnumHelper.getEnumFromDbValue(EmploymentType.class, dbValue);
	}

	public String getName() {
		return name();
	}
}
