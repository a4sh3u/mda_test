package de.smava.webapp.account.dao.jdo;

import de.smava.webapp.account.dao.AdvisorDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collection;

@Repository
public class JdoAdvisorDao extends JdoGenericDao<Advisor> implements AdvisorDao {

    public Advisor load(Long id) {
        return getPersistenceManager().getObjectById(Advisor.class, id);
    }

    public boolean exists(Long id) {
        return getPersistenceManager().getObjectById(Advisor.class, id) != null;
    }

    public Long save(Advisor advisor) {
        return getPersistenceManager().makePersistent(advisor).getId();
    }

    @SuppressWarnings("unchecked")
    public Collection<Advisor> getAdvisorList() {
        return (Collection<Advisor>) getPersistenceManager().newQuery(Advisor.class).execute();
    }

    @SuppressWarnings("unchecked")
    public Collection<Advisor> getActiveAdvisorList() {
        return (Collection<Advisor>) getPersistenceManager()
                .newQuery(Advisor.class,"_account._state == 'ACTIVATED'")
                .execute();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Advisor loadActive(Long id) {
        Query query = getPersistenceManager().newQuery(Advisor.class, "_id == :id && _account._state == 'ACTIVATED'");
        Collection<Advisor> activeAdvisorCollection = (Collection<Advisor>) query.execute(id);

       return fetchSingleElement(activeAdvisorCollection);
    }

    @Override
    public Advisor findPersonalAdvisorByCustomerNumber(Long customerNumber) {
        Query query = getPersistenceManager().newNamedQuery(Advisor.class, "findPersonalAdvisorByCustomerNumber");
        @SuppressWarnings("unchecked")
        Collection<Advisor> advisors = (Collection<Advisor>) query.execute(customerNumber);

        return fetchSingleElement(advisors);
    }

    @Override
    public Advisor findBySmavaRefId(Long advisorSmavaRefId) {
        Query query = getPersistenceManager().newNamedQuery(Advisor.class, "findBySmavaRefId");
        @SuppressWarnings("unchecked")
        Collection<Advisor> advisors = (Collection<Advisor>) query.execute(advisorSmavaRefId);

        return fetchSingleElement(advisors);
    }

    @Override
    public boolean checkIfAdvisorExistsForBorrower(Long customerNumber) {
        Query query = getPersistenceManager().newQuery(Account.class);
        query.setFilter("this._accountId == :customerNumber");
        query.setResult("this._personalAdvisorId");
        query.setResultClass(Long.class);
        query.setUnique(true);
        return query.execute(customerNumber) != null;
    }

    private Advisor fetchSingleElement(Collection<Advisor> advisors) {
        if (advisors == null || advisors.isEmpty()) {
            return null;
        } else {
            return advisors.iterator().next();
        }
    }
}
