package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.BidSearchAgeGroup;
import de.smava.webapp.account.domain.BidSearchGroup;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'BidSearchs'.
 *
 * @author generator
 */
public interface BidSearchEntityInterface {

    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'interest min'.
     *
     * 
     *
     */
    void setInterestMin(double interestMin);

    /**
     * Returns the property 'interest min'.
     *
     * 
     *
     */
    double getInterestMin();
    /**
     * Setter for the property 'interest max'.
     *
     * 
     *
     */
    void setInterestMax(double interestMax);

    /**
     * Returns the property 'interest max'.
     *
     * 
     *
     */
    double getInterestMax();
    /**
     * Setter for the property 'amount min'.
     *
     * 
     *
     */
    void setAmountMin(double amountMin);

    /**
     * Returns the property 'amount min'.
     *
     * 
     *
     */
    double getAmountMin();
    /**
     * Setter for the property 'amount max'.
     *
     * 
     *
     */
    void setAmountMax(double amountMax);

    /**
     * Returns the property 'amount max'.
     *
     * 
     *
     */
    double getAmountMax();
    /**
     * Setter for the property 'include finished orders'.
     *
     * 
     *
     */
    void setIncludeFinishedOrders(boolean includeFinishedOrders);

    /**
     * Returns the property 'include finished orders'.
     *
     * 
     *
     */
    boolean getIncludeFinishedOrders();
    /**
     * Setter for the property 'roi min'.
     *
     * 
     *
     */
    void setRoiMin(Double roiMin);

    /**
     * Returns the property 'roi min'.
     *
     * 
     *
     */
    Double getRoiMin();
    /**
     * Setter for the property 'roi max'.
     *
     * 
     *
     */
    void setRoiMax(Double roiMax);

    /**
     * Returns the property 'roi max'.
     *
     * 
     *
     */
    Double getRoiMax();
    /**
     * Setter for the property 'credit rate indicator'.
     *
     * 
     *
     */
    void setCreditRateIndicator(int creditRateIndicator);

    /**
     * Returns the property 'credit rate indicator'.
     *
     * 
     *
     */
    int getCreditRateIndicator();
    /**
     * Setter for the property 'matched rate'.
     *
     * 
     *
     */
    void setMatchedRate(int matchedRate);

    /**
     * Returns the property 'matched rate'.
     *
     * 
     *
     */
    int getMatchedRate();
    /**
     * Setter for the property 'search phrase'.
     *
     * 
     *
     */
    void setSearchPhrase(String searchPhrase);

    /**
     * Returns the property 'search phrase'.
     *
     * 
     *
     */
    String getSearchPhrase();
    /**
     * Setter for the property 'employment'.
     *
     * 
     *
     */
    void setEmployment(String employment);

    /**
     * Returns the property 'employment'.
     *
     * 
     *
     */
    String getEmployment();
    /**
     * Setter for the property 'is mail notification wanted'.
     *
     * 
     *
     */
    void setIsMailNotificationWanted(boolean isMailNotificationWanted);

    /**
     * Returns the property 'is mail notification wanted'.
     *
     * 
     *
     */
    boolean getIsMailNotificationWanted();
    /**
     * Setter for the property 'integer employments'.
     *
     * 
     *
     */
    void setIntegerEmployments(Collection<Integer> integerEmployments);

    /**
     * Returns the property 'integer employments'.
     *
     * 
     *
     */
    Collection<Integer> getIntegerEmployments();
    /**
     * Setter for the property 'age range min'.
     *
     * 
     *
     */
    void setAgeRangeMin(Integer ageRangeMin);

    /**
     * Returns the property 'age range min'.
     *
     * 
     *
     */
    Integer getAgeRangeMin();
    /**
     * Setter for the property 'age range max'.
     *
     * 
     *
     */
    void setAgeRangeMax(Integer ageRangeMax);

    /**
     * Returns the property 'age range max'.
     *
     * 
     *
     */
    Integer getAgeRangeMax();
    /**
     * Setter for the property 'except late payer'.
     *
     * 
     *
     */
    void setExceptLatePayer(Boolean exceptLatePayer);

    /**
     * Returns the property 'except late payer'.
     *
     * 
     *
     */
    Boolean getExceptLatePayer();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'min investment limit per borrower'.
     *
     * 
     *
     */
    void setMinInvestmentLimitPerBorrower(double minInvestmentLimitPerBorrower);

    /**
     * Returns the property 'min investment limit per borrower'.
     *
     * 
     *
     */
    double getMinInvestmentLimitPerBorrower();
    /**
     * Setter for the property 'investment limit per borrower'.
     *
     * 
     *
     */
    void setInvestmentLimitPerBorrower(double investmentLimitPerBorrower);

    /**
     * Returns the property 'investment limit per borrower'.
     *
     * 
     *
     */
    double getInvestmentLimitPerBorrower();
    /**
     * Setter for the property 'bid search groups'.
     *
     * 
     *
     */
    void setBidSearchGroups(Collection<BidSearchGroup> bidSearchGroups);

    /**
     * Returns the property 'bid search groups'.
     *
     * 
     *
     */
    Collection<BidSearchGroup> getBidSearchGroups();
    /**
     * Setter for the property 'gender'.
     *
     * 
     *
     */
    void setGender(Integer gender);

    /**
     * Returns the property 'gender'.
     *
     * 
     *
     */
    Integer getGender();
    /**
     * Setter for the property 'service provider restriction'.
     *
     * 
     *
     */
    void setServiceProviderRestriction(de.smava.webapp.account.domain.BankAccountProvider serviceProviderRestriction);

    /**
     * Returns the property 'service provider restriction'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccountProvider getServiceProviderRestriction();
    /**
     * Setter for the property 'age groups'.
     *
     * 
     *
     */
    void setAgeGroups(Collection<BidSearchAgeGroup> ageGroups);

    /**
     * Returns the property 'age groups'.
     *
     * 
     *
     */
    Collection<BidSearchAgeGroup> getAgeGroups();

}
