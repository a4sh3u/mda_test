package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.EconomicalDataHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * The domain object that represents 'EconomicalDatas'.
 */
public class EconomicalData extends EconomicalDataHistory  {

    public EconomicalData() {
        _creationDate = CurrentDate.getDate();
        _type = TYPE_NORMAL;
        _debts = new HashSet<ConsolidatedDebt>();

    }

    @Override
    public boolean getTradeTaxPaid() {
        return _tradeTaxPaid;
    }

    @Override
    public boolean getChurchTaxPaid() {
        return _churchTaxPaid;
    }

    /**
     * Returns the property 'co borrower employment'.
     */
    public String getCoBorrowerEmployment() {
        return null;
    }

    /**
     * Returns the property 'co borrower income'.
     */
    public double getCoBorrowerIncome() {
        return 0.0;
    }

    /**
     * Returns the property 'co borrower start of occupation'.
     */
    public Date getCoBorrowerStartOfOccupation() {
        return null;
    }

    /**
     * Returns the property 'co borrower temp employment'.
     */
    public boolean getCoBorrowerTempEmployment() {
        return false;
    }

    /**
     * Returns the property 'co borrower end of temp employment'.
     */
    public Date getCoBorrowerEndOfTempEmployment() {
        return null;
    }

    /**
     * Returns the property 'co borrower sector'.
     */
    public Sector getCoBorrowerSector() {
        return null;
    }


    /**
     * Returns the property 'co borrower sector description'.
     */
    public String getCoBorrowerSectorDescription() {
        return null;
    }

    /**
     * Returns the property 'co borrower employer name'.
     */
    public String getCoBorrowerEmployerName() {
        return null;
    }

    public boolean isTradeTaxPaid() {
        return _tradeTaxPaid;
    }


    public boolean isChurchTaxPaid() {
        return _churchTaxPaid;
    }

    public void setTradeTaxPaid(boolean tradeTaxPaid) {
        if (!_tradeTaxPaid) {
            _tradeTaxPaid = true;
            _tradeTaxPaid = isTradeTaxPaid();
        }
        registerChange("tradeTaxPaid", _tradeTaxPaid, tradeTaxPaid);
        _tradeTaxPaid = tradeTaxPaid;

    }

    public void setChurchTaxPaid(boolean churchTaxPaid) {
        if (!_churchTaxPaid) {
            _churchTaxPaid = true;
            _churchTaxPaid = isChurchTaxPaid();
        }
        registerChange("churchTaxPaid", _churchTaxPaid, churchTaxPaid);
        _churchTaxPaid = churchTaxPaid;
    }

        protected Account _account;
        protected String _occupation;
        protected String _employment;
        protected Date _startOfOccupation;
        protected String _modeOfAccommodation;
        protected Sector _sector;
        protected String _sectorDescription;
        protected double _savings;
        protected double _creditExpenses;
        protected double _totalPrivateCreditAmount;
        protected boolean _tempEmployment;
        protected Date _endOfTempEmployment;
        protected double _receivedPalimony;
        protected double _paidRent;
        protected double _receivedRent;
        protected double _miscEarnAmount1;
        protected String _miscEarn1;
        protected String _otherEarnDescription;
        protected String _numOfCars;
        protected double _paidAlimony;
        protected double _income;
        protected double _privateHealthInsurance;
        protected double _mortgage;
        protected double _businessLoan;
        protected double _leasing;
        protected double _businessLeasing;
        protected List<FreelancerIncome> _freelancerIncomes;
        protected Date _freelancerIncomeConfirmationLimitDate;
        protected boolean _tradeTaxPaid;
        protected boolean _churchTaxPaid;
        protected int _creditRateIndicator;
        protected Date _creationDate;
        protected double _schufaLeasing;
        protected double _schufaLoan;
        protected double _schufaLoanRelatedSavings;
        protected double _schufaBusinessLeasing;
        protected double _schufaBusinessLoan;
        protected int _type;
        protected de.smava.webapp.account.domain.MiscEarnType _miscEarnType;
        protected de.smava.webapp.account.domain.PropertyOwnedType _propertyOwnedType;
        protected de.smava.webapp.account.domain.CreditCardType _creditCardType;
        protected Integer _propertyOwnedSquareMetres;
        protected String _employerName;
        protected int _numberOfChildren;
        protected int _numberOfOtherInHousehold;
        protected de.smava.webapp.account.domain.SelfemployedProfessionType _selfemployedProfession;
        protected de.smava.webapp.account.domain.EconomicalDataType _economicalDataType;
        protected Collection<ConsolidatedDebt> _debts;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'occupation'.
     */
    public void setOccupation(String occupation) {
        if (!_occupationIsSet) {
            _occupationIsSet = true;
            _occupationInitVal = getOccupation();
        }
        registerChange("occupation", _occupationInitVal, occupation);
        _occupation = occupation;
    }
                        
    /**
     * Returns the property 'occupation'.
     */
    public String getOccupation() {
        return _occupation;
    }
                                    /**
     * Setter for the property 'employment'.
     */
    public void setEmployment(String employment) {
        if (!_employmentIsSet) {
            _employmentIsSet = true;
            _employmentInitVal = getEmployment();
        }
        registerChange("employment", _employmentInitVal, employment);
        _employment = employment;
    }
                        
    /**
     * Returns the property 'employment'.
     */
    public String getEmployment() {
        return _employment;
    }
                                    /**
     * Setter for the property 'start of occupation'.
     */
    public void setStartOfOccupation(Date startOfOccupation) {
        if (!_startOfOccupationIsSet) {
            _startOfOccupationIsSet = true;
            _startOfOccupationInitVal = getStartOfOccupation();
        }
        registerChange("start of occupation", _startOfOccupationInitVal, startOfOccupation);
        _startOfOccupation = startOfOccupation;
    }
                        
    /**
     * Returns the property 'start of occupation'.
     */
    public Date getStartOfOccupation() {
        return _startOfOccupation;
    }
                                    /**
     * Setter for the property 'mode of accommodation'.
     */
    public void setModeOfAccommodation(String modeOfAccommodation) {
        if (!_modeOfAccommodationIsSet) {
            _modeOfAccommodationIsSet = true;
            _modeOfAccommodationInitVal = getModeOfAccommodation();
        }
        registerChange("mode of accommodation", _modeOfAccommodationInitVal, modeOfAccommodation);
        _modeOfAccommodation = modeOfAccommodation;
    }
                        
    /**
     * Returns the property 'mode of accommodation'.
     */
    public String getModeOfAccommodation() {
        return _modeOfAccommodation;
    }
                                    /**
     * Setter for the property 'sector'.
     */
    public void setSector(Sector sector) {
        if (!_sectorIsSet) {
            _sectorIsSet = true;
            _sectorInitVal = getSector();
        }
        registerChange("sector", _sectorInitVal, sector);
        _sector = sector;
    }
                        
    /**
     * Returns the property 'sector'.
     */
    public Sector getSector() {
        return _sector;
    }
                                    /**
     * Setter for the property 'sector description'.
     */
    public void setSectorDescription(String sectorDescription) {
        if (!_sectorDescriptionIsSet) {
            _sectorDescriptionIsSet = true;
            _sectorDescriptionInitVal = getSectorDescription();
        }
        registerChange("sector description", _sectorDescriptionInitVal, sectorDescription);
        _sectorDescription = sectorDescription;
    }
                        
    /**
     * Returns the property 'sector description'.
     */
    public String getSectorDescription() {
        return _sectorDescription;
    }
                                    /**
     * Setter for the property 'savings'.
     */
    public void setSavings(double savings) {
        if (!_savingsIsSet) {
            _savingsIsSet = true;
            _savingsInitVal = getSavings();
        }
        registerChange("savings", _savingsInitVal, savings);
        _savings = savings;
    }
                        
    /**
     * Returns the property 'savings'.
     */
    public double getSavings() {
        return _savings;
    }
                                    /**
     * Setter for the property 'credit expenses'.
     */
    public void setCreditExpenses(double creditExpenses) {
        if (!_creditExpensesIsSet) {
            _creditExpensesIsSet = true;
            _creditExpensesInitVal = getCreditExpenses();
        }
        registerChange("credit expenses", _creditExpensesInitVal, creditExpenses);
        _creditExpenses = creditExpenses;
    }
                        
    /**
     * Returns the property 'credit expenses'.
     */
    public double getCreditExpenses() {
        return _creditExpenses;
    }
                                    /**
     * Setter for the property 'total private credit amount'.
     */
    public void setTotalPrivateCreditAmount(double totalPrivateCreditAmount) {
        if (!_totalPrivateCreditAmountIsSet) {
            _totalPrivateCreditAmountIsSet = true;
            _totalPrivateCreditAmountInitVal = getTotalPrivateCreditAmount();
        }
        registerChange("total private credit amount", _totalPrivateCreditAmountInitVal, totalPrivateCreditAmount);
        _totalPrivateCreditAmount = totalPrivateCreditAmount;
    }
                        
    /**
     * Returns the property 'total private credit amount'.
     */
    public double getTotalPrivateCreditAmount() {
        return _totalPrivateCreditAmount;
    }
                                    /**
     * Setter for the property 'temp employment'.
     */
    public void setTempEmployment(boolean tempEmployment) {
        if (!_tempEmploymentIsSet) {
            _tempEmploymentIsSet = true;
            _tempEmploymentInitVal = getTempEmployment();
        }
        registerChange("temp employment", _tempEmploymentInitVal, tempEmployment);
        _tempEmployment = tempEmployment;
    }
                        
    /**
     * Returns the property 'temp employment'.
     */
    public boolean getTempEmployment() {
        return _tempEmployment;
    }
                                    /**
     * Setter for the property 'end of temp employment'.
     */
    public void setEndOfTempEmployment(Date endOfTempEmployment) {
        if (!_endOfTempEmploymentIsSet) {
            _endOfTempEmploymentIsSet = true;
            _endOfTempEmploymentInitVal = getEndOfTempEmployment();
        }
        registerChange("end of temp employment", _endOfTempEmploymentInitVal, endOfTempEmployment);
        _endOfTempEmployment = endOfTempEmployment;
    }
                        
    /**
     * Returns the property 'end of temp employment'.
     */
    public Date getEndOfTempEmployment() {
        return _endOfTempEmployment;
    }
                                    /**
     * Setter for the property 'received palimony'.
     */
    public void setReceivedPalimony(double receivedPalimony) {
        if (!_receivedPalimonyIsSet) {
            _receivedPalimonyIsSet = true;
            _receivedPalimonyInitVal = getReceivedPalimony();
        }
        registerChange("received palimony", _receivedPalimonyInitVal, receivedPalimony);
        _receivedPalimony = receivedPalimony;
    }
                        
    /**
     * Returns the property 'received palimony'.
     */
    public double getReceivedPalimony() {
        return _receivedPalimony;
    }
                                    /**
     * Setter for the property 'paid rent'.
     */
    public void setPaidRent(double paidRent) {
        if (!_paidRentIsSet) {
            _paidRentIsSet = true;
            _paidRentInitVal = getPaidRent();
        }
        registerChange("paid rent", _paidRentInitVal, paidRent);
        _paidRent = paidRent;
    }
                        
    /**
     * Returns the property 'paid rent'.
     */
    public double getPaidRent() {
        return _paidRent;
    }
                                    /**
     * Setter for the property 'received rent'.
     */
    public void setReceivedRent(double receivedRent) {
        if (!_receivedRentIsSet) {
            _receivedRentIsSet = true;
            _receivedRentInitVal = getReceivedRent();
        }
        registerChange("received rent", _receivedRentInitVal, receivedRent);
        _receivedRent = receivedRent;
    }
                        
    /**
     * Returns the property 'received rent'.
     */
    public double getReceivedRent() {
        return _receivedRent;
    }
                                    /**
     * Setter for the property 'misc earn amount1'.
     */
    public void setMiscEarnAmount1(double miscEarnAmount1) {
        if (!_miscEarnAmount1IsSet) {
            _miscEarnAmount1IsSet = true;
            _miscEarnAmount1InitVal = getMiscEarnAmount1();
        }
        registerChange("misc earn amount1", _miscEarnAmount1InitVal, miscEarnAmount1);
        _miscEarnAmount1 = miscEarnAmount1;
    }
                        
    /**
     * Returns the property 'misc earn amount1'.
     */
    public double getMiscEarnAmount1() {
        return _miscEarnAmount1;
    }
                                    /**
     * Setter for the property 'misc earn1'.
     */
    public void setMiscEarn1(String miscEarn1) {
        if (!_miscEarn1IsSet) {
            _miscEarn1IsSet = true;
            _miscEarn1InitVal = getMiscEarn1();
        }
        registerChange("misc earn1", _miscEarn1InitVal, miscEarn1);
        _miscEarn1 = miscEarn1;
    }
                        
    /**
     * Returns the property 'misc earn1'.
     */
    public String getMiscEarn1() {
        return _miscEarn1;
    }
                                    /**
     * Setter for the property 'other earn description'.
     */
    public void setOtherEarnDescription(String otherEarnDescription) {
        if (!_otherEarnDescriptionIsSet) {
            _otherEarnDescriptionIsSet = true;
            _otherEarnDescriptionInitVal = getOtherEarnDescription();
        }
        registerChange("other earn description", _otherEarnDescriptionInitVal, otherEarnDescription);
        _otherEarnDescription = otherEarnDescription;
    }
                        
    /**
     * Returns the property 'other earn description'.
     */
    public String getOtherEarnDescription() {
        return _otherEarnDescription;
    }
                                    /**
     * Setter for the property 'num of cars'.
     */
    public void setNumOfCars(String numOfCars) {
        if (!_numOfCarsIsSet) {
            _numOfCarsIsSet = true;
            _numOfCarsInitVal = getNumOfCars();
        }
        registerChange("num of cars", _numOfCarsInitVal, numOfCars);
        _numOfCars = numOfCars;
    }
                        
    /**
     * Returns the property 'num of cars'.
     */
    public String getNumOfCars() {
        return _numOfCars;
    }
                                    /**
     * Setter for the property 'paid alimony'.
     */
    public void setPaidAlimony(double paidAlimony) {
        if (!_paidAlimonyIsSet) {
            _paidAlimonyIsSet = true;
            _paidAlimonyInitVal = getPaidAlimony();
        }
        registerChange("paid alimony", _paidAlimonyInitVal, paidAlimony);
        _paidAlimony = paidAlimony;
    }
                        
    /**
     * Returns the property 'paid alimony'.
     */
    public double getPaidAlimony() {
        return _paidAlimony;
    }
                                    /**
     * Setter for the property 'income'.
     */
    public void setIncome(double income) {
        if (!_incomeIsSet) {
            _incomeIsSet = true;
            _incomeInitVal = getIncome();
        }
        registerChange("income", _incomeInitVal, income);
        _income = income;
    }
                        
    /**
     * Returns the property 'income'.
     */
    public double getIncome() {
        return _income;
    }
                                    /**
     * Setter for the property 'private health insurance'.
     */
    public void setPrivateHealthInsurance(double privateHealthInsurance) {
        if (!_privateHealthInsuranceIsSet) {
            _privateHealthInsuranceIsSet = true;
            _privateHealthInsuranceInitVal = getPrivateHealthInsurance();
        }
        registerChange("private health insurance", _privateHealthInsuranceInitVal, privateHealthInsurance);
        _privateHealthInsurance = privateHealthInsurance;
    }
                        
    /**
     * Returns the property 'private health insurance'.
     */
    public double getPrivateHealthInsurance() {
        return _privateHealthInsurance;
    }
                                    /**
     * Setter for the property 'mortgage'.
     */
    public void setMortgage(double mortgage) {
        if (!_mortgageIsSet) {
            _mortgageIsSet = true;
            _mortgageInitVal = getMortgage();
        }
        registerChange("mortgage", _mortgageInitVal, mortgage);
        _mortgage = mortgage;
    }
                        
    /**
     * Returns the property 'mortgage'.
     */
    public double getMortgage() {
        return _mortgage;
    }
                                    /**
     * Setter for the property 'business loan'.
     */
    public void setBusinessLoan(double businessLoan) {
        if (!_businessLoanIsSet) {
            _businessLoanIsSet = true;
            _businessLoanInitVal = getBusinessLoan();
        }
        registerChange("business loan", _businessLoanInitVal, businessLoan);
        _businessLoan = businessLoan;
    }
                        
    /**
     * Returns the property 'business loan'.
     */
    public double getBusinessLoan() {
        return _businessLoan;
    }
                                    /**
     * Setter for the property 'leasing'.
     */
    public void setLeasing(double leasing) {
        if (!_leasingIsSet) {
            _leasingIsSet = true;
            _leasingInitVal = getLeasing();
        }
        registerChange("leasing", _leasingInitVal, leasing);
        _leasing = leasing;
    }
                        
    /**
     * Returns the property 'leasing'.
     */
    public double getLeasing() {
        return _leasing;
    }
                                    /**
     * Setter for the property 'business leasing'.
     */
    public void setBusinessLeasing(double businessLeasing) {
        if (!_businessLeasingIsSet) {
            _businessLeasingIsSet = true;
            _businessLeasingInitVal = getBusinessLeasing();
        }
        registerChange("business leasing", _businessLeasingInitVal, businessLeasing);
        _businessLeasing = businessLeasing;
    }
                        
    /**
     * Returns the property 'business leasing'.
     */
    public double getBusinessLeasing() {
        return _businessLeasing;
    }
                                            
    /**
     * Setter for the property 'freelancer incomes'.
     */
    public void setFreelancerIncomes(List<FreelancerIncome> freelancerIncomes) {
        _freelancerIncomes = freelancerIncomes;
    }
            
    /**
     * Returns the property 'freelancer incomes'.
     */
    public List<FreelancerIncome> getFreelancerIncomes() {
        return _freelancerIncomes;
    }
                                    /**
     * Setter for the property 'freelancer income confirmation limit date'.
     */
    public void setFreelancerIncomeConfirmationLimitDate(Date freelancerIncomeConfirmationLimitDate) {
        if (!_freelancerIncomeConfirmationLimitDateIsSet) {
            _freelancerIncomeConfirmationLimitDateIsSet = true;
            _freelancerIncomeConfirmationLimitDateInitVal = getFreelancerIncomeConfirmationLimitDate();
        }
        registerChange("freelancer income confirmation limit date", _freelancerIncomeConfirmationLimitDateInitVal, freelancerIncomeConfirmationLimitDate);
        _freelancerIncomeConfirmationLimitDate = freelancerIncomeConfirmationLimitDate;
    }
                        
    /**
     * Returns the property 'freelancer income confirmation limit date'.
     */
    public Date getFreelancerIncomeConfirmationLimitDate() {
        return _freelancerIncomeConfirmationLimitDate;
    }
                                                            /**
     * Setter for the property 'credit rate indicator'.
     */
    public void setCreditRateIndicator(int creditRateIndicator) {
        if (!_creditRateIndicatorIsSet) {
            _creditRateIndicatorIsSet = true;
            _creditRateIndicatorInitVal = getCreditRateIndicator();
        }
        registerChange("credit rate indicator", _creditRateIndicatorInitVal, creditRateIndicator);
        _creditRateIndicator = creditRateIndicator;
    }
                        
    /**
     * Returns the property 'credit rate indicator'.
     */
    public int getCreditRateIndicator() {
        return _creditRateIndicator;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'schufa leasing'.
     */
    public void setSchufaLeasing(double schufaLeasing) {
        if (!_schufaLeasingIsSet) {
            _schufaLeasingIsSet = true;
            _schufaLeasingInitVal = getSchufaLeasing();
        }
        registerChange("schufa leasing", _schufaLeasingInitVal, schufaLeasing);
        _schufaLeasing = schufaLeasing;
    }
                        
    /**
     * Returns the property 'schufa leasing'.
     */
    public double getSchufaLeasing() {
        return _schufaLeasing;
    }
                                    /**
     * Setter for the property 'schufa loan'.
     */
    public void setSchufaLoan(double schufaLoan) {
        if (!_schufaLoanIsSet) {
            _schufaLoanIsSet = true;
            _schufaLoanInitVal = getSchufaLoan();
        }
        registerChange("schufa loan", _schufaLoanInitVal, schufaLoan);
        _schufaLoan = schufaLoan;
    }
                        
    /**
     * Returns the property 'schufa loan'.
     */
    public double getSchufaLoan() {
        return _schufaLoan;
    }
                                    /**
     * Setter for the property 'schufa loan related savings'.
     */
    public void setSchufaLoanRelatedSavings(double schufaLoanRelatedSavings) {
        if (!_schufaLoanRelatedSavingsIsSet) {
            _schufaLoanRelatedSavingsIsSet = true;
            _schufaLoanRelatedSavingsInitVal = getSchufaLoanRelatedSavings();
        }
        registerChange("schufa loan related savings", _schufaLoanRelatedSavingsInitVal, schufaLoanRelatedSavings);
        _schufaLoanRelatedSavings = schufaLoanRelatedSavings;
    }
                        
    /**
     * Returns the property 'schufa loan related savings'.
     */
    public double getSchufaLoanRelatedSavings() {
        return _schufaLoanRelatedSavings;
    }
                                    /**
     * Setter for the property 'schufa business leasing'.
     */
    public void setSchufaBusinessLeasing(double schufaBusinessLeasing) {
        if (!_schufaBusinessLeasingIsSet) {
            _schufaBusinessLeasingIsSet = true;
            _schufaBusinessLeasingInitVal = getSchufaBusinessLeasing();
        }
        registerChange("schufa business leasing", _schufaBusinessLeasingInitVal, schufaBusinessLeasing);
        _schufaBusinessLeasing = schufaBusinessLeasing;
    }
                        
    /**
     * Returns the property 'schufa business leasing'.
     */
    public double getSchufaBusinessLeasing() {
        return _schufaBusinessLeasing;
    }
                                    /**
     * Setter for the property 'schufa business loan'.
     */
    public void setSchufaBusinessLoan(double schufaBusinessLoan) {
        if (!_schufaBusinessLoanIsSet) {
            _schufaBusinessLoanIsSet = true;
            _schufaBusinessLoanInitVal = getSchufaBusinessLoan();
        }
        registerChange("schufa business loan", _schufaBusinessLoanInitVal, schufaBusinessLoan);
        _schufaBusinessLoan = schufaBusinessLoan;
    }
                        
    /**
     * Returns the property 'schufa business loan'.
     */
    public double getSchufaBusinessLoan() {
        return _schufaBusinessLoan;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(int type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public int getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'misc earn type'.
     */
    public void setMiscEarnType(de.smava.webapp.account.domain.MiscEarnType miscEarnType) {
        _miscEarnType = miscEarnType;
    }
            
    /**
     * Returns the property 'misc earn type'.
     */
    public de.smava.webapp.account.domain.MiscEarnType getMiscEarnType() {
        return _miscEarnType;
    }
                                            
    /**
     * Setter for the property 'property owned type'.
     */
    public void setPropertyOwnedType(de.smava.webapp.account.domain.PropertyOwnedType propertyOwnedType) {
        _propertyOwnedType = propertyOwnedType;
    }
            
    /**
     * Returns the property 'property owned type'.
     */
    public de.smava.webapp.account.domain.PropertyOwnedType getPropertyOwnedType() {
        return _propertyOwnedType;
    }
                                            
    /**
     * Setter for the property 'credit card type'.
     */
    public void setCreditCardType(de.smava.webapp.account.domain.CreditCardType creditCardType) {
        _creditCardType = creditCardType;
    }
            
    /**
     * Returns the property 'credit card type'.
     */
    public de.smava.webapp.account.domain.CreditCardType getCreditCardType() {
        return _creditCardType;
    }
                                            
    /**
     * Setter for the property 'property owned square metres'.
     */
    public void setPropertyOwnedSquareMetres(Integer propertyOwnedSquareMetres) {
        _propertyOwnedSquareMetres = propertyOwnedSquareMetres;
    }
            
    /**
     * Returns the property 'property owned square metres'.
     */
    public Integer getPropertyOwnedSquareMetres() {
        return _propertyOwnedSquareMetres;
    }
                                    /**
     * Setter for the property 'employer name'.
     */
    public void setEmployerName(String employerName) {
        if (!_employerNameIsSet) {
            _employerNameIsSet = true;
            _employerNameInitVal = getEmployerName();
        }
        registerChange("employer name", _employerNameInitVal, employerName);
        _employerName = employerName;
    }
                        
    /**
     * Returns the property 'employer name'.
     */
    public String getEmployerName() {
        return _employerName;
    }
                                    /**
     * Setter for the property 'number of children'.
     */
    public void setNumberOfChildren(int numberOfChildren) {
        if (!_numberOfChildrenIsSet) {
            _numberOfChildrenIsSet = true;
            _numberOfChildrenInitVal = getNumberOfChildren();
        }
        registerChange("number of children", _numberOfChildrenInitVal, numberOfChildren);
        _numberOfChildren = numberOfChildren;
    }
                        
    /**
     * Returns the property 'number of children'.
     */
    public int getNumberOfChildren() {
        return _numberOfChildren;
    }
                                    /**
     * Setter for the property 'number of other in household'.
     */
    public void setNumberOfOtherInHousehold(int numberOfOtherInHousehold) {
        if (!_numberOfOtherInHouseholdIsSet) {
            _numberOfOtherInHouseholdIsSet = true;
            _numberOfOtherInHouseholdInitVal = getNumberOfOtherInHousehold();
        }
        registerChange("number of other in household", _numberOfOtherInHouseholdInitVal, numberOfOtherInHousehold);
        _numberOfOtherInHousehold = numberOfOtherInHousehold;
    }
                        
    /**
     * Returns the property 'number of other in household'.
     */
    public int getNumberOfOtherInHousehold() {
        return _numberOfOtherInHousehold;
    }
                                            
    /**
     * Setter for the property 'selfemployed profession'.
     */
    public void setSelfemployedProfession(de.smava.webapp.account.domain.SelfemployedProfessionType selfemployedProfession) {
        _selfemployedProfession = selfemployedProfession;
    }
            
    /**
     * Returns the property 'selfemployed profession'.
     */
    public de.smava.webapp.account.domain.SelfemployedProfessionType getSelfemployedProfession() {
        return _selfemployedProfession;
    }
                                            
    /**
     * Setter for the property 'economical data type'.
     */
    public void setEconomicalDataType(de.smava.webapp.account.domain.EconomicalDataType economicalDataType) {
        _economicalDataType = economicalDataType;
    }
            
    /**
     * Returns the property 'economical data type'.
     */
    public de.smava.webapp.account.domain.EconomicalDataType getEconomicalDataType() {
        return _economicalDataType;
    }
                                            
    /**
     * Setter for the property 'debts'.
     */
    public void setDebts(Collection<ConsolidatedDebt> debts) {
        _debts = debts;
    }
            
    /**
     * Returns the property 'debts'.
     */
    public Collection<ConsolidatedDebt> getDebts() {
        return _debts;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(EconomicalData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _occupation=").append(_occupation);
            builder.append("\n    _employment=").append(_employment);
            builder.append("\n    _modeOfAccommodation=").append(_modeOfAccommodation);
            builder.append("\n    _sector=").append(_sector);
            builder.append("\n    _sectorDescription=").append(_sectorDescription);
            builder.append("\n    _savings=").append(_savings);
            builder.append("\n    _creditExpenses=").append(_creditExpenses);
            builder.append("\n    _totalPrivateCreditAmount=").append(_totalPrivateCreditAmount);
            builder.append("\n    _tempEmployment=").append(_tempEmployment);
            builder.append("\n    _receivedPalimony=").append(_receivedPalimony);
            builder.append("\n    _paidRent=").append(_paidRent);
            builder.append("\n    _receivedRent=").append(_receivedRent);
            builder.append("\n    _miscEarnAmount1=").append(_miscEarnAmount1);
            builder.append("\n    _miscEarn1=").append(_miscEarn1);
            builder.append("\n    _otherEarnDescription=").append(_otherEarnDescription);
            builder.append("\n    _numOfCars=").append(_numOfCars);
            builder.append("\n    _paidAlimony=").append(_paidAlimony);
            builder.append("\n    _income=").append(_income);
            builder.append("\n    _privateHealthInsurance=").append(_privateHealthInsurance);
            builder.append("\n    _mortgage=").append(_mortgage);
            builder.append("\n    _businessLoan=").append(_businessLoan);
            builder.append("\n    _leasing=").append(_leasing);
            builder.append("\n    _businessLeasing=").append(_businessLeasing);
            builder.append("\n    _tradeTaxPaid=").append(_tradeTaxPaid);
            builder.append("\n    _churchTaxPaid=").append(_churchTaxPaid);
            builder.append("\n    _creditRateIndicator=").append(_creditRateIndicator);
            builder.append("\n    _schufaLeasing=").append(_schufaLeasing);
            builder.append("\n    _schufaLoan=").append(_schufaLoan);
            builder.append("\n    _schufaLoanRelatedSavings=").append(_schufaLoanRelatedSavings);
            builder.append("\n    _schufaBusinessLeasing=").append(_schufaBusinessLeasing);
            builder.append("\n    _schufaBusinessLoan=").append(_schufaBusinessLoan);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _employerName=").append(_employerName);
            builder.append("\n    _numberOfChildren=").append(_numberOfChildren);
            builder.append("\n    _numberOfOtherInHousehold=").append(_numberOfOtherInHousehold);
            builder.append("\n}");
        } else {
            builder.append(EconomicalData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public EconomicalData instance() {
        return this;
    }
}
