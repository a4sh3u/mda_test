/**
 * 
 */
package de.smava.webapp.account.domain;

import java.io.Serializable;

/**
 * Container class for lender payout values.
 * @author Dimitar Robev
 *
 */
public class PayoutValuesContainer implements Serializable {

	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 3282677475315820646L;

	private Long _bankAccountId;
	private int _bookingType; 
	private Long _contractId; 
	private Long _bookingGroupId;
	private Double _fromProvisionInterim;
	private Double _toProvisionInterim;
	private Double _fromInterim;
	private Double _toInterim;
	private Double _fromRef;
	private Double _toRefSuccessful;
	private Double _toRefOpen;
	private Double _difference;

	/** no-args constructor is needed from data nucleus, to fill the object properly in case that some of the values are null. */
	public PayoutValuesContainer () {
	}

	public PayoutValuesContainer (final Long bankAccountId, final int bookingType, final Long contractId, final Long bookingGroupId,
			final Double fromInterim, final Double toInterim, final Double fromRef, final Double toRefSuccessful, final Double toRefOpen, final Double difference) {
		_bankAccountId = bankAccountId;
		_bookingType = bookingType;
		_contractId = contractId;
		_bookingGroupId = bookingGroupId;
		_fromInterim = fromInterim;
		_toInterim = toInterim;
		_fromRef = fromRef;
		_toRefSuccessful = toRefSuccessful;
		_toRefOpen = toRefOpen;
		_difference = difference;
	}

	public Long getBankAccountId() {
		return _bankAccountId;
	}

	public void setBankAccountId(Long bankAccountId) {
		this._bankAccountId = bankAccountId;
	}

	public int getBookingType() {
		return _bookingType;
	}

	public void setBookingType(int bookingType) {
		this._bookingType = bookingType;
	}

	public Long getContractId() {
		return _contractId;
	}

	public void setContractId(Long contractId) {
		this._contractId = contractId;
	}

	public Long getBookingGroupId() {
		return _bookingGroupId;
	}

	public void setBookingGroupId(Long bookingGroupId) {
		this._bookingGroupId = bookingGroupId;
	}

	public Double getToInterim() {
		return _toInterim;
	}

	public void setToInterim(Double toInterim) {
		_toInterim = toInterim;
	}

	public Double getFromInterim() {
		return _fromInterim;
	}

	public void setFromInterim(Double fromInterim) {
		_fromInterim = fromInterim;
	}

	public Double getFromRef() {
		return _fromRef;
	}

	public void setFromRef(Double fromRef) {
		_fromRef = fromRef;
	}

	public Double getDifference() {
		return _difference;
	}

	public void setDifference(Double difference) {
		_difference = difference;
	}

	public Double getToRefSuccessful() {
		return _toRefSuccessful;
	}

	public void setToRefSuccessful(Double toRefSuccessful) {
		_toRefSuccessful = toRefSuccessful;
	}

	public Double getToRefOpen() {
		return _toRefOpen;
	}

	public void setToRefOpen(Double toRefOpen) {
		_toRefOpen = toRefOpen;
	}

	public Double getFromProvisionInterim() {
		return _fromProvisionInterim;
	}

	public void setFromProvisionInterim(Double fromProvisionInterim) {
		_fromProvisionInterim = fromProvisionInterim;
	}

	public Double getToProvisionInterim() {
		return _toProvisionInterim;
	}

	public void setToProvisionInterim(Double toProvisionInterim) {
		_toProvisionInterim = toProvisionInterim;
	}
}
