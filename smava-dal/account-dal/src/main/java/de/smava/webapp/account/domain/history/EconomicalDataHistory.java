package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.account.domain.abstracts.AbstractEconomicalData;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'EconomicalDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EconomicalDataHistory extends AbstractEconomicalData {

    protected transient String _occupationInitVal;
    protected transient boolean _occupationIsSet;
    protected transient String _employmentInitVal;
    protected transient boolean _employmentIsSet;
    protected transient Date _startOfOccupationInitVal;
    protected transient boolean _startOfOccupationIsSet;
    protected transient String _modeOfAccommodationInitVal;
    protected transient boolean _modeOfAccommodationIsSet;
    protected transient Sector _sectorInitVal;
    protected transient boolean _sectorIsSet;
    protected transient String _sectorDescriptionInitVal;
    protected transient boolean _sectorDescriptionIsSet;
    protected transient double _savingsInitVal;
    protected transient boolean _savingsIsSet;
    protected transient double _creditExpensesInitVal;
    protected transient boolean _creditExpensesIsSet;
    protected transient double _totalPrivateCreditAmountInitVal;
    protected transient boolean _totalPrivateCreditAmountIsSet;
    protected transient boolean _tempEmploymentInitVal;
    protected transient boolean _tempEmploymentIsSet;
    protected transient Date _endOfTempEmploymentInitVal;
    protected transient boolean _endOfTempEmploymentIsSet;
    protected transient double _receivedPalimonyInitVal;
    protected transient boolean _receivedPalimonyIsSet;
    protected transient double _paidRentInitVal;
    protected transient boolean _paidRentIsSet;
    protected transient double _receivedRentInitVal;
    protected transient boolean _receivedRentIsSet;
    protected transient double _miscEarnAmount1InitVal;
    protected transient boolean _miscEarnAmount1IsSet;
    protected transient String _miscEarn1InitVal;
    protected transient boolean _miscEarn1IsSet;
    protected transient String _otherEarnDescriptionInitVal;
    protected transient boolean _otherEarnDescriptionIsSet;
    protected transient String _numOfCarsInitVal;
    protected transient boolean _numOfCarsIsSet;
    protected transient double _paidAlimonyInitVal;
    protected transient boolean _paidAlimonyIsSet;
    protected transient double _incomeInitVal;
    protected transient boolean _incomeIsSet;
    protected transient double _privateHealthInsuranceInitVal;
    protected transient boolean _privateHealthInsuranceIsSet;
    protected transient double _mortgageInitVal;
    protected transient boolean _mortgageIsSet;
    protected transient double _businessLoanInitVal;
    protected transient boolean _businessLoanIsSet;
    protected transient double _leasingInitVal;
    protected transient boolean _leasingIsSet;
    protected transient double _businessLeasingInitVal;
    protected transient boolean _businessLeasingIsSet;
    protected transient Date _freelancerIncomeConfirmationLimitDateInitVal;
    protected transient boolean _freelancerIncomeConfirmationLimitDateIsSet;
    protected transient int _creditRateIndicatorInitVal;
    protected transient boolean _creditRateIndicatorIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient double _schufaLeasingInitVal;
    protected transient boolean _schufaLeasingIsSet;
    protected transient double _schufaLoanInitVal;
    protected transient boolean _schufaLoanIsSet;
    protected transient double _schufaLoanRelatedSavingsInitVal;
    protected transient boolean _schufaLoanRelatedSavingsIsSet;
    protected transient double _schufaBusinessLeasingInitVal;
    protected transient boolean _schufaBusinessLeasingIsSet;
    protected transient double _schufaBusinessLoanInitVal;
    protected transient boolean _schufaBusinessLoanIsSet;
    protected transient int _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _employerNameInitVal;
    protected transient boolean _employerNameIsSet;
    protected transient int _numberOfChildrenInitVal;
    protected transient boolean _numberOfChildrenIsSet;
    protected transient int _numberOfOtherInHouseholdInitVal;
    protected transient boolean _numberOfOtherInHouseholdIsSet;


		
    /**
     * Returns the initial value of the property 'occupation'.
     */
    public String occupationInitVal() {
        String result;
        if (_occupationIsSet) {
            result = _occupationInitVal;
        } else {
            result = getOccupation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'occupation'.
     */
    public boolean occupationIsDirty() {
        return !valuesAreEqual(occupationInitVal(), getOccupation());
    }

    /**
     * Returns true if the setter method was called for the property 'occupation'.
     */
    public boolean occupationIsSet() {
        return _occupationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'employment'.
     */
    public String employmentInitVal() {
        String result;
        if (_employmentIsSet) {
            result = _employmentInitVal;
        } else {
            result = getEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employment'.
     */
    public boolean employmentIsDirty() {
        return !valuesAreEqual(employmentInitVal(), getEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'employment'.
     */
    public boolean employmentIsSet() {
        return _employmentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'start of occupation'.
     */
    public Date startOfOccupationInitVal() {
        Date result;
        if (_startOfOccupationIsSet) {
            result = _startOfOccupationInitVal;
        } else {
            result = getStartOfOccupation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start of occupation'.
     */
    public boolean startOfOccupationIsDirty() {
        return !valuesAreEqual(startOfOccupationInitVal(), getStartOfOccupation());
    }

    /**
     * Returns true if the setter method was called for the property 'start of occupation'.
     */
    public boolean startOfOccupationIsSet() {
        return _startOfOccupationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'mode of accommodation'.
     */
    public String modeOfAccommodationInitVal() {
        String result;
        if (_modeOfAccommodationIsSet) {
            result = _modeOfAccommodationInitVal;
        } else {
            result = getModeOfAccommodation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'mode of accommodation'.
     */
    public boolean modeOfAccommodationIsDirty() {
        return !valuesAreEqual(modeOfAccommodationInitVal(), getModeOfAccommodation());
    }

    /**
     * Returns true if the setter method was called for the property 'mode of accommodation'.
     */
    public boolean modeOfAccommodationIsSet() {
        return _modeOfAccommodationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'sector'.
     */
    public Sector sectorInitVal() {
        Sector result;
        if (_sectorIsSet) {
            result = _sectorInitVal;
        } else {
            result = getSector();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'sector'.
     */
    public boolean sectorIsDirty() {
        return !valuesAreEqual(sectorInitVal(), getSector());
    }

    /**
     * Returns true if the setter method was called for the property 'sector'.
     */
    public boolean sectorIsSet() {
        return _sectorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'sector description'.
     */
    public String sectorDescriptionInitVal() {
        String result;
        if (_sectorDescriptionIsSet) {
            result = _sectorDescriptionInitVal;
        } else {
            result = getSectorDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'sector description'.
     */
    public boolean sectorDescriptionIsDirty() {
        return !valuesAreEqual(sectorDescriptionInitVal(), getSectorDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'sector description'.
     */
    public boolean sectorDescriptionIsSet() {
        return _sectorDescriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'savings'.
     */
    public double savingsInitVal() {
        double result;
        if (_savingsIsSet) {
            result = _savingsInitVal;
        } else {
            result = getSavings();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'savings'.
     */
    public boolean savingsIsDirty() {
        return !valuesAreEqual(savingsInitVal(), getSavings());
    }

    /**
     * Returns true if the setter method was called for the property 'savings'.
     */
    public boolean savingsIsSet() {
        return _savingsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit expenses'.
     */
    public double creditExpensesInitVal() {
        double result;
        if (_creditExpensesIsSet) {
            result = _creditExpensesInitVal;
        } else {
            result = getCreditExpenses();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit expenses'.
     */
    public boolean creditExpensesIsDirty() {
        return !valuesAreEqual(creditExpensesInitVal(), getCreditExpenses());
    }

    /**
     * Returns true if the setter method was called for the property 'credit expenses'.
     */
    public boolean creditExpensesIsSet() {
        return _creditExpensesIsSet;
    }
	
    /**
     * Returns the initial value of the property 'total private credit amount'.
     */
    public double totalPrivateCreditAmountInitVal() {
        double result;
        if (_totalPrivateCreditAmountIsSet) {
            result = _totalPrivateCreditAmountInitVal;
        } else {
            result = getTotalPrivateCreditAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'total private credit amount'.
     */
    public boolean totalPrivateCreditAmountIsDirty() {
        return !valuesAreEqual(totalPrivateCreditAmountInitVal(), getTotalPrivateCreditAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'total private credit amount'.
     */
    public boolean totalPrivateCreditAmountIsSet() {
        return _totalPrivateCreditAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'temp employment'.
     */
    public boolean tempEmploymentInitVal() {
        boolean result;
        if (_tempEmploymentIsSet) {
            result = _tempEmploymentInitVal;
        } else {
            result = getTempEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'temp employment'.
     */
    public boolean tempEmploymentIsDirty() {
        return !valuesAreEqual(tempEmploymentInitVal(), getTempEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'temp employment'.
     */
    public boolean tempEmploymentIsSet() {
        return _tempEmploymentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end of temp employment'.
     */
    public Date endOfTempEmploymentInitVal() {
        Date result;
        if (_endOfTempEmploymentIsSet) {
            result = _endOfTempEmploymentInitVal;
        } else {
            result = getEndOfTempEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end of temp employment'.
     */
    public boolean endOfTempEmploymentIsDirty() {
        return !valuesAreEqual(endOfTempEmploymentInitVal(), getEndOfTempEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'end of temp employment'.
     */
    public boolean endOfTempEmploymentIsSet() {
        return _endOfTempEmploymentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'received palimony'.
     */
    public double receivedPalimonyInitVal() {
        double result;
        if (_receivedPalimonyIsSet) {
            result = _receivedPalimonyInitVal;
        } else {
            result = getReceivedPalimony();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'received palimony'.
     */
    public boolean receivedPalimonyIsDirty() {
        return !valuesAreEqual(receivedPalimonyInitVal(), getReceivedPalimony());
    }

    /**
     * Returns true if the setter method was called for the property 'received palimony'.
     */
    public boolean receivedPalimonyIsSet() {
        return _receivedPalimonyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'paid rent'.
     */
    public double paidRentInitVal() {
        double result;
        if (_paidRentIsSet) {
            result = _paidRentInitVal;
        } else {
            result = getPaidRent();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'paid rent'.
     */
    public boolean paidRentIsDirty() {
        return !valuesAreEqual(paidRentInitVal(), getPaidRent());
    }

    /**
     * Returns true if the setter method was called for the property 'paid rent'.
     */
    public boolean paidRentIsSet() {
        return _paidRentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'received rent'.
     */
    public double receivedRentInitVal() {
        double result;
        if (_receivedRentIsSet) {
            result = _receivedRentInitVal;
        } else {
            result = getReceivedRent();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'received rent'.
     */
    public boolean receivedRentIsDirty() {
        return !valuesAreEqual(receivedRentInitVal(), getReceivedRent());
    }

    /**
     * Returns true if the setter method was called for the property 'received rent'.
     */
    public boolean receivedRentIsSet() {
        return _receivedRentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'misc earn amount1'.
     */
    public double miscEarnAmount1InitVal() {
        double result;
        if (_miscEarnAmount1IsSet) {
            result = _miscEarnAmount1InitVal;
        } else {
            result = getMiscEarnAmount1();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'misc earn amount1'.
     */
    public boolean miscEarnAmount1IsDirty() {
        return !valuesAreEqual(miscEarnAmount1InitVal(), getMiscEarnAmount1());
    }

    /**
     * Returns true if the setter method was called for the property 'misc earn amount1'.
     */
    public boolean miscEarnAmount1IsSet() {
        return _miscEarnAmount1IsSet;
    }
	
    /**
     * Returns the initial value of the property 'misc earn1'.
     */
    public String miscEarn1InitVal() {
        String result;
        if (_miscEarn1IsSet) {
            result = _miscEarn1InitVal;
        } else {
            result = getMiscEarn1();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'misc earn1'.
     */
    public boolean miscEarn1IsDirty() {
        return !valuesAreEqual(miscEarn1InitVal(), getMiscEarn1());
    }

    /**
     * Returns true if the setter method was called for the property 'misc earn1'.
     */
    public boolean miscEarn1IsSet() {
        return _miscEarn1IsSet;
    }
	
    /**
     * Returns the initial value of the property 'other earn description'.
     */
    public String otherEarnDescriptionInitVal() {
        String result;
        if (_otherEarnDescriptionIsSet) {
            result = _otherEarnDescriptionInitVal;
        } else {
            result = getOtherEarnDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'other earn description'.
     */
    public boolean otherEarnDescriptionIsDirty() {
        return !valuesAreEqual(otherEarnDescriptionInitVal(), getOtherEarnDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'other earn description'.
     */
    public boolean otherEarnDescriptionIsSet() {
        return _otherEarnDescriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'num of cars'.
     */
    public String numOfCarsInitVal() {
        String result;
        if (_numOfCarsIsSet) {
            result = _numOfCarsInitVal;
        } else {
            result = getNumOfCars();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'num of cars'.
     */
    public boolean numOfCarsIsDirty() {
        return !valuesAreEqual(numOfCarsInitVal(), getNumOfCars());
    }

    /**
     * Returns true if the setter method was called for the property 'num of cars'.
     */
    public boolean numOfCarsIsSet() {
        return _numOfCarsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'paid alimony'.
     */
    public double paidAlimonyInitVal() {
        double result;
        if (_paidAlimonyIsSet) {
            result = _paidAlimonyInitVal;
        } else {
            result = getPaidAlimony();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'paid alimony'.
     */
    public boolean paidAlimonyIsDirty() {
        return !valuesAreEqual(paidAlimonyInitVal(), getPaidAlimony());
    }

    /**
     * Returns true if the setter method was called for the property 'paid alimony'.
     */
    public boolean paidAlimonyIsSet() {
        return _paidAlimonyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'income'.
     */
    public double incomeInitVal() {
        double result;
        if (_incomeIsSet) {
            result = _incomeInitVal;
        } else {
            result = getIncome();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'income'.
     */
    public boolean incomeIsDirty() {
        return !valuesAreEqual(incomeInitVal(), getIncome());
    }

    /**
     * Returns true if the setter method was called for the property 'income'.
     */
    public boolean incomeIsSet() {
        return _incomeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'private health insurance'.
     */
    public double privateHealthInsuranceInitVal() {
        double result;
        if (_privateHealthInsuranceIsSet) {
            result = _privateHealthInsuranceInitVal;
        } else {
            result = getPrivateHealthInsurance();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'private health insurance'.
     */
    public boolean privateHealthInsuranceIsDirty() {
        return !valuesAreEqual(privateHealthInsuranceInitVal(), getPrivateHealthInsurance());
    }

    /**
     * Returns true if the setter method was called for the property 'private health insurance'.
     */
    public boolean privateHealthInsuranceIsSet() {
        return _privateHealthInsuranceIsSet;
    }
	
    /**
     * Returns the initial value of the property 'mortgage'.
     */
    public double mortgageInitVal() {
        double result;
        if (_mortgageIsSet) {
            result = _mortgageInitVal;
        } else {
            result = getMortgage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'mortgage'.
     */
    public boolean mortgageIsDirty() {
        return !valuesAreEqual(mortgageInitVal(), getMortgage());
    }

    /**
     * Returns true if the setter method was called for the property 'mortgage'.
     */
    public boolean mortgageIsSet() {
        return _mortgageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'business loan'.
     */
    public double businessLoanInitVal() {
        double result;
        if (_businessLoanIsSet) {
            result = _businessLoanInitVal;
        } else {
            result = getBusinessLoan();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'business loan'.
     */
    public boolean businessLoanIsDirty() {
        return !valuesAreEqual(businessLoanInitVal(), getBusinessLoan());
    }

    /**
     * Returns true if the setter method was called for the property 'business loan'.
     */
    public boolean businessLoanIsSet() {
        return _businessLoanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'leasing'.
     */
    public double leasingInitVal() {
        double result;
        if (_leasingIsSet) {
            result = _leasingInitVal;
        } else {
            result = getLeasing();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'leasing'.
     */
    public boolean leasingIsDirty() {
        return !valuesAreEqual(leasingInitVal(), getLeasing());
    }

    /**
     * Returns true if the setter method was called for the property 'leasing'.
     */
    public boolean leasingIsSet() {
        return _leasingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'business leasing'.
     */
    public double businessLeasingInitVal() {
        double result;
        if (_businessLeasingIsSet) {
            result = _businessLeasingInitVal;
        } else {
            result = getBusinessLeasing();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'business leasing'.
     */
    public boolean businessLeasingIsDirty() {
        return !valuesAreEqual(businessLeasingInitVal(), getBusinessLeasing());
    }

    /**
     * Returns true if the setter method was called for the property 'business leasing'.
     */
    public boolean businessLeasingIsSet() {
        return _businessLeasingIsSet;
    }
		
    /**
     * Returns the initial value of the property 'freelancer income confirmation limit date'.
     */
    public Date freelancerIncomeConfirmationLimitDateInitVal() {
        Date result;
        if (_freelancerIncomeConfirmationLimitDateIsSet) {
            result = _freelancerIncomeConfirmationLimitDateInitVal;
        } else {
            result = getFreelancerIncomeConfirmationLimitDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'freelancer income confirmation limit date'.
     */
    public boolean freelancerIncomeConfirmationLimitDateIsDirty() {
        return !valuesAreEqual(freelancerIncomeConfirmationLimitDateInitVal(), getFreelancerIncomeConfirmationLimitDate());
    }

    /**
     * Returns true if the setter method was called for the property 'freelancer income confirmation limit date'.
     */
    public boolean freelancerIncomeConfirmationLimitDateIsSet() {
        return _freelancerIncomeConfirmationLimitDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'credit rate indicator'.
     */
    public int creditRateIndicatorInitVal() {
        int result;
        if (_creditRateIndicatorIsSet) {
            result = _creditRateIndicatorInitVal;
        } else {
            result = getCreditRateIndicator();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit rate indicator'.
     */
    public boolean creditRateIndicatorIsDirty() {
        return !valuesAreEqual(creditRateIndicatorInitVal(), getCreditRateIndicator());
    }

    /**
     * Returns true if the setter method was called for the property 'credit rate indicator'.
     */
    public boolean creditRateIndicatorIsSet() {
        return _creditRateIndicatorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufa leasing'.
     */
    public double schufaLeasingInitVal() {
        double result;
        if (_schufaLeasingIsSet) {
            result = _schufaLeasingInitVal;
        } else {
            result = getSchufaLeasing();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa leasing'.
     */
    public boolean schufaLeasingIsDirty() {
        return !valuesAreEqual(schufaLeasingInitVal(), getSchufaLeasing());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa leasing'.
     */
    public boolean schufaLeasingIsSet() {
        return _schufaLeasingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufa loan'.
     */
    public double schufaLoanInitVal() {
        double result;
        if (_schufaLoanIsSet) {
            result = _schufaLoanInitVal;
        } else {
            result = getSchufaLoan();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa loan'.
     */
    public boolean schufaLoanIsDirty() {
        return !valuesAreEqual(schufaLoanInitVal(), getSchufaLoan());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa loan'.
     */
    public boolean schufaLoanIsSet() {
        return _schufaLoanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufa loan related savings'.
     */
    public double schufaLoanRelatedSavingsInitVal() {
        double result;
        if (_schufaLoanRelatedSavingsIsSet) {
            result = _schufaLoanRelatedSavingsInitVal;
        } else {
            result = getSchufaLoanRelatedSavings();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa loan related savings'.
     */
    public boolean schufaLoanRelatedSavingsIsDirty() {
        return !valuesAreEqual(schufaLoanRelatedSavingsInitVal(), getSchufaLoanRelatedSavings());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa loan related savings'.
     */
    public boolean schufaLoanRelatedSavingsIsSet() {
        return _schufaLoanRelatedSavingsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufa business leasing'.
     */
    public double schufaBusinessLeasingInitVal() {
        double result;
        if (_schufaBusinessLeasingIsSet) {
            result = _schufaBusinessLeasingInitVal;
        } else {
            result = getSchufaBusinessLeasing();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa business leasing'.
     */
    public boolean schufaBusinessLeasingIsDirty() {
        return !valuesAreEqual(schufaBusinessLeasingInitVal(), getSchufaBusinessLeasing());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa business leasing'.
     */
    public boolean schufaBusinessLeasingIsSet() {
        return _schufaBusinessLeasingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufa business loan'.
     */
    public double schufaBusinessLoanInitVal() {
        double result;
        if (_schufaBusinessLoanIsSet) {
            result = _schufaBusinessLoanInitVal;
        } else {
            result = getSchufaBusinessLoan();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa business loan'.
     */
    public boolean schufaBusinessLoanIsDirty() {
        return !valuesAreEqual(schufaBusinessLoanInitVal(), getSchufaBusinessLoan());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa business loan'.
     */
    public boolean schufaBusinessLoanIsSet() {
        return _schufaBusinessLoanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public int typeInitVal() {
        int result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
					
    /**
     * Returns the initial value of the property 'employer name'.
     */
    public String employerNameInitVal() {
        String result;
        if (_employerNameIsSet) {
            result = _employerNameInitVal;
        } else {
            result = getEmployerName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employer name'.
     */
    public boolean employerNameIsDirty() {
        return !valuesAreEqual(employerNameInitVal(), getEmployerName());
    }

    /**
     * Returns true if the setter method was called for the property 'employer name'.
     */
    public boolean employerNameIsSet() {
        return _employerNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of children'.
     */
    public int numberOfChildrenInitVal() {
        int result;
        if (_numberOfChildrenIsSet) {
            result = _numberOfChildrenInitVal;
        } else {
            result = getNumberOfChildren();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of children'.
     */
    public boolean numberOfChildrenIsDirty() {
        return !valuesAreEqual(numberOfChildrenInitVal(), getNumberOfChildren());
    }

    /**
     * Returns true if the setter method was called for the property 'number of children'.
     */
    public boolean numberOfChildrenIsSet() {
        return _numberOfChildrenIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of other in household'.
     */
    public int numberOfOtherInHouseholdInitVal() {
        int result;
        if (_numberOfOtherInHouseholdIsSet) {
            result = _numberOfOtherInHouseholdInitVal;
        } else {
            result = getNumberOfOtherInHousehold();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of other in household'.
     */
    public boolean numberOfOtherInHouseholdIsDirty() {
        return !valuesAreEqual(numberOfOtherInHouseholdInitVal(), getNumberOfOtherInHousehold());
    }

    /**
     * Returns true if the setter method was called for the property 'number of other in household'.
     */
    public boolean numberOfOtherInHouseholdIsSet() {
        return _numberOfOtherInHouseholdIsSet;
    }
			
}
