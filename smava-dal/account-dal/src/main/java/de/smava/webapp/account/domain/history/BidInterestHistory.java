package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBidInterest;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BidInterests'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BidInterestHistory extends AbstractBidInterest {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _validUntilInitVal;
    protected transient boolean _validUntilIsSet;
    protected transient String _marketNameInitVal;
    protected transient boolean _marketNameIsSet;
    protected transient Float _rateInitVal;
    protected transient boolean _rateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'valid until'.
     */
    public Date validUntilInitVal() {
        Date result;
        if (_validUntilIsSet) {
            result = _validUntilInitVal;
        } else {
            result = getValidUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid until'.
     */
    public boolean validUntilIsDirty() {
        return !valuesAreEqual(validUntilInitVal(), getValidUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'valid until'.
     */
    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }
	
    /**
     * Returns the initial value of the property 'market name'.
     */
    public String marketNameInitVal() {
        String result;
        if (_marketNameIsSet) {
            result = _marketNameInitVal;
        } else {
            result = getMarketName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'market name'.
     */
    public boolean marketNameIsDirty() {
        return !valuesAreEqual(marketNameInitVal(), getMarketName());
    }

    /**
     * Returns true if the setter method was called for the property 'market name'.
     */
    public boolean marketNameIsSet() {
        return _marketNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rate'.
     */
    public Float rateInitVal() {
        Float result;
        if (_rateIsSet) {
            result = _rateInitVal;
        } else {
            result = getRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rate'.
     */
    public boolean rateIsDirty() {
        return !valuesAreEqual(rateInitVal(), getRate());
    }

    /**
     * Returns true if the setter method was called for the property 'rate'.
     */
    public boolean rateIsSet() {
        return _rateIsSet;
    }

}
