package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DocumentAttachmentContainerHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * The domain object that represents 'DocumentAttachmentContainers'.
 */
public class DocumentAttachmentContainer extends DocumentAttachmentContainerHistory  {

	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 71142750227408301L;

    public DocumentAttachmentContainer() {
        this._documentAttachments = new HashSet<Attachment>();
        this._creationDate = CurrentDate.getDate();
    }

        protected Date _creationDate;
        protected Date _processDate;
        protected Date _date;
        protected String _name;
        protected String _state;
        protected String _direction;
        protected Integer _originalDocumentCount;
        protected Collection<Attachment> _documentAttachments;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'process date'.
     */
    public void setProcessDate(Date processDate) {
        if (!_processDateIsSet) {
            _processDateIsSet = true;
            _processDateInitVal = getProcessDate();
        }
        registerChange("process date", _processDateInitVal, processDate);
        _processDate = processDate;
    }
                        
    /**
     * Returns the property 'process date'.
     */
    public Date getProcessDate() {
        return _processDate;
    }
                                    /**
     * Setter for the property 'date'.
     */
    public void setDate(Date date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = getDate();
        }
        registerChange("date", _dateInitVal, date);
        _date = date;
    }
                        
    /**
     * Returns the property 'date'.
     */
    public Date getDate() {
        return _date;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'direction'.
     */
    public void setDirection(String direction) {
        if (!_directionIsSet) {
            _directionIsSet = true;
            _directionInitVal = getDirection();
        }
        registerChange("direction", _directionInitVal, direction);
        _direction = direction;
    }
                        
    /**
     * Returns the property 'direction'.
     */
    public String getDirection() {
        return _direction;
    }
                                            
    /**
     * Setter for the property 'original document count'.
     */
    public void setOriginalDocumentCount(Integer originalDocumentCount) {
        _originalDocumentCount = originalDocumentCount;
    }
            
    /**
     * Returns the property 'original document count'.
     */
    public Integer getOriginalDocumentCount() {
        return _originalDocumentCount;
    }
                                            
    /**
     * Setter for the property 'document attachments'.
     */
    public void setDocumentAttachments(Collection<Attachment> documentAttachments) {
        _documentAttachments = documentAttachments;
    }
            
    /**
     * Returns the property 'document attachments'.
     */
    public Collection<Attachment> getDocumentAttachments() {
        return _documentAttachments;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentAttachmentContainer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _direction=").append(_direction);
            builder.append("\n}");
        } else {
            builder.append(DocumentAttachmentContainer.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
