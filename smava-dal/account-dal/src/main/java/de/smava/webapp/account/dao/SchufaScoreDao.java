//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.SchufaScore;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'SchufaScores'.
 *
 * @author generator
 */
public interface SchufaScoreDao extends BaseDao<SchufaScore>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the schufa score identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    SchufaScore getSchufaScore(Long id);

    /**
     * Saves the schufa score.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveSchufaScore(SchufaScore schufaScore);

    /**
     * Deletes an schufa score, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa score
     */
    void deleteSchufaScore(Long id);

    /**
     * Retrieves all 'SchufaScore' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<SchufaScore> getSchufaScoreList();

    /**
     * Retrieves a page of 'SchufaScore' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<SchufaScore> getSchufaScoreList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'SchufaScore' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<SchufaScore> getSchufaScoreList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'SchufaScore' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<SchufaScore> getSchufaScoreList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'SchufaScore' instances.
     */
    long getSchufaScoreCount();




    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa score)}
    //
    // insert custom methods here
    //

    Collection<SchufaScore> loadScoresWithStates(List<String> scoreStates);
    SchufaScore getFirstScoreOfUser(Account account);

    SchufaScore findSchufaScoreForRequest(Long requestId);
    // !!!!!!!! End of insert code section !!!!!!!!





}
