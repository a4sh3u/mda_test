//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.backoffice.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bo search)}
import de.smava.webapp.backoffice.domain.history.BoSearchHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BoSearchs'.
 *
 * 
 *
 * @author generator
 */
public class BoSearch extends BoSearchHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(bo search)}
                                                                                                                                                /**
    * Setter for the property 'email'.
    */
    public void setEmail(String email) {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }

    /**
    * Returns the property 'email'.
    *
    * 
    *
    */
    public String getEmail() {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _id;
        protected transient Long _count;
        protected Integer _type;
        protected de.smava.webapp.account.domain.Account _account;
        protected String _phone;
        protected String _phone2;
        protected String _email;
        protected Date _birthDate;
        protected String _lastName;
        protected String _firstName;
        
                                    
    /**
     * Setter for the property 'id'.
     *
     * 
     *
     */
    public void setId(Long id) {
        _id = id;
    }
            
    /**
     * Returns the property 'id'.
     *
     * 
     *
     */
    public Long getId() {
        return _id;
    }
                                            
    /**
     * Setter for the property 'count'.
     *
     * 
     *
     */
    public void setCount(Long count) {
        _count = count;
    }
            
    /**
     * Returns the property 'count'.
     *
     * 
     *
     */
    public Long getCount() {
        return _count;
    }
                                            
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(Integer type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public Integer getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'phone'.
     *
     * 
     *
     */
    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = getPhone();
        }
        registerChange("phone", _phoneInitVal, phone);
        _phone = phone;
    }
                        
    /**
     * Returns the property 'phone'.
     *
     * 
     *
     */
    public String getPhone() {
        return _phone;
    }
                                    /**
     * Setter for the property 'phone2'.
     *
     * 
     *
     */
    public void setPhone2(String phone2) {
        if (!_phone2IsSet) {
            _phone2IsSet = true;
            _phone2InitVal = getPhone2();
        }
        registerChange("phone2", _phone2InitVal, phone2);
        _phone2 = phone2;
    }
                        
    /**
     * Returns the property 'phone2'.
     *
     * 
     *
     */
    public String getPhone2() {
        return _phone2;
    }
                                                /**
     * Setter for the property 'birth date'.
     *
     * 
     *
     */
    public void setBirthDate(Date birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;
            _birthDateInitVal = getBirthDate();
        }
        registerChange("birth date", _birthDateInitVal, birthDate);
        _birthDate = birthDate;
    }
                        
    /**
     * Returns the property 'birth date'.
     *
     * 
     *
     */
    public Date getBirthDate() {
        return _birthDate;
    }
                                    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("last name", _lastNameInitVal, lastName);
        _lastName = lastName;
    }
                        
    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    public String getLastName() {
        return _lastName;
    }
                                    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("first name", _firstNameInitVal, firstName);
        _firstName = firstName;
    }
                        
    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    public String getFirstName() {
        return _firstName;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BoSearch.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _phone=").append(_phone);
            builder.append("\n    _phone2=").append(_phone2);
            builder.append("\n    _email=").append(_email);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n}");
        } else {
            builder.append(BoSearch.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BoSearch asBoSearch() {
        return this;
    }
}
