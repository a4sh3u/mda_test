//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(age group)}

import de.smava.webapp.account.domain.AgeGroup;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'AgeGroups'.
 *
 * @author generator
 */
public interface AgeGroupDao extends BaseDao<AgeGroup>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the age group identified by the given id.
     *
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    AgeGroup getAgeGroup(Long id);

    /**
     * Saves the age group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao<>#save(T)} instead.
     */
    Long saveAgeGroup(AgeGroup ageGroup);

    /**
     * Deletes an age group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the age group
     */
    void deleteAgeGroup(Long id);

    /**
     * Retrieves all 'AgeGroup' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<AgeGroup> getAgeGroupList();

    /**
     * Retrieves a page of 'AgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     */
    Collection<AgeGroup> getAgeGroupList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'AgeGroup' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<AgeGroup> getAgeGroupList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'AgeGroup' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<AgeGroup> getAgeGroupList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'AgeGroup' instances.
     */
    long getAgeGroupCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(age group)}
    //
    Collection<AgeGroup> getActiveAgeGroups();
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
