//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(category image)}

import de.smava.webapp.account.dao.CategoryImageDao;
import de.smava.webapp.account.domain.CategoryImage;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'CategoryImages'.
 *
 * @author generator
 */
@Repository(value = "categoryImageDao")
public class JdoCategoryImageDao extends JdoBaseDao implements CategoryImageDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCategoryImageDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(category image)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the category image identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public CategoryImage load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImage - start: id=" + id);
        }
        CategoryImage result = getEntity(CategoryImage.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImage - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public CategoryImage getCategoryImage(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	CategoryImage entity = findUniqueEntity(CategoryImage.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the category image.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(CategoryImage categoryImage) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCategoryImage: " + categoryImage);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(category image)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(categoryImage);
    }

    /**
     * @deprecated Use {@link #save(CategoryImage) instead}
     */
    public Long saveCategoryImage(CategoryImage categoryImage) {
        return save(categoryImage);
    }

    /**
     * Deletes an category image, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the category image
     */
    public void deleteCategoryImage(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteCategoryImage: " + id);
        }
        deleteEntity(CategoryImage.class, id);
    }

    /**
     * Retrieves all 'CategoryImage' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<CategoryImage> getCategoryImageList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - start");
        }
        Collection<CategoryImage> result = getEntities(CategoryImage.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'CategoryImage' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<CategoryImage> getCategoryImageList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - start: pageable=" + pageable);
        }
        Collection<CategoryImage> result = getEntities(CategoryImage.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'CategoryImage' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<CategoryImage> getCategoryImageList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - start: sortable=" + sortable);
        }
        Collection<CategoryImage> result = getEntities(CategoryImage.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CategoryImage' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<CategoryImage> getCategoryImageList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<CategoryImage> result = getEntities(CategoryImage.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'CategoryImage' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CategoryImage> findCategoryImageList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - start: whereClause=" + whereClause);
        }
        Collection<CategoryImage> result = findEntities(CategoryImage.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'CategoryImage' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<CategoryImage> findCategoryImageList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<CategoryImage> result = findEntities(CategoryImage.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'CategoryImage' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CategoryImage> findCategoryImageList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<CategoryImage> result = findEntities(CategoryImage.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'CategoryImage' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CategoryImage> findCategoryImageList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<CategoryImage> result = findEntities(CategoryImage.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CategoryImage' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CategoryImage> findCategoryImageList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<CategoryImage> result = findEntities(CategoryImage.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CategoryImage' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CategoryImage> findCategoryImageList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<CategoryImage> result = findEntities(CategoryImage.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryImageList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'CategoryImage' instances.
     */
    public long getCategoryImageCount() {
        long result = getEntityCount(CategoryImage.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'CategoryImage' instances which match the given whereClause.
     */
    public long getCategoryImageCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(CategoryImage.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'CategoryImage' instances which match the given whereClause together with params specified in object array.
     */
    public long getCategoryImageCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(CategoryImage.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryImageCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(category image)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
