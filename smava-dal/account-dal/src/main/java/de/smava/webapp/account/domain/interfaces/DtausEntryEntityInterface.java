package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.DtausEntryTransaction;
import de.smava.webapp.account.domain.DtausFile;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'DtausEntrys'.
 *
 * @author generator
 */
public interface DtausEntryEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'transaction date'.
     *
     * 
     *
     */
    void setTransactionDate(Date transactionDate);

    /**
     * Returns the property 'transaction date'.
     *
     * 
     *
     */
    Date getTransactionDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    void setKey(String key);

    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    String getKey();
    /**
     * Setter for the property 'return reason'.
     *
     * 
     *
     */
    void setReturnReason(String returnReason);

    /**
     * Returns the property 'return reason'.
     *
     * 
     *
     */
    String getReturnReason();
    /**
     * Setter for the property 'credit account name'.
     *
     * 
     *
     */
    void setCreditAccountName(String creditAccountName);

    /**
     * Returns the property 'credit account name'.
     *
     * 
     *
     */
    String getCreditAccountName();
    /**
     * Setter for the property 'credit account bank code'.
     *
     * 
     *
     */
    void setCreditAccountBankCode(String creditAccountBankCode);

    /**
     * Returns the property 'credit account bank code'.
     *
     * 
     *
     */
    String getCreditAccountBankCode();
    /**
     * Setter for the property 'credit account bank'.
     *
     * 
     *
     */
    void setCreditAccountBank(String creditAccountBank);

    /**
     * Returns the property 'credit account bank'.
     *
     * 
     *
     */
    String getCreditAccountBank();
    /**
     * Setter for the property 'credit account number'.
     *
     * 
     *
     */
    void setCreditAccountNumber(String creditAccountNumber);

    /**
     * Returns the property 'credit account number'.
     *
     * 
     *
     */
    String getCreditAccountNumber();
    /**
     * Setter for the property 'credit account iban'.
     *
     * 
     *
     */
    void setCreditAccountIban(String creditAccountIban);

    /**
     * Returns the property 'credit account iban'.
     *
     * 
     *
     */
    String getCreditAccountIban();
    /**
     * Setter for the property 'credit account bic'.
     *
     * 
     *
     */
    void setCreditAccountBic(String creditAccountBic);

    /**
     * Returns the property 'credit account bic'.
     *
     * 
     *
     */
    String getCreditAccountBic();
    /**
     * Setter for the property 'debit account name'.
     *
     * 
     *
     */
    void setDebitAccountName(String debitAccountName);

    /**
     * Returns the property 'debit account name'.
     *
     * 
     *
     */
    String getDebitAccountName();
    /**
     * Setter for the property 'debit account bank code'.
     *
     * 
     *
     */
    void setDebitAccountBankCode(String debitAccountBankCode);

    /**
     * Returns the property 'debit account bank code'.
     *
     * 
     *
     */
    String getDebitAccountBankCode();
    /**
     * Setter for the property 'debit account bank'.
     *
     * 
     *
     */
    void setDebitAccountBank(String debitAccountBank);

    /**
     * Returns the property 'debit account bank'.
     *
     * 
     *
     */
    String getDebitAccountBank();
    /**
     * Setter for the property 'debit account number'.
     *
     * 
     *
     */
    void setDebitAccountNumber(String debitAccountNumber);

    /**
     * Returns the property 'debit account number'.
     *
     * 
     *
     */
    String getDebitAccountNumber();
    /**
     * Setter for the property 'debit account iban'.
     *
     * 
     *
     */
    void setDebitAccountIban(String debitAccountIban);

    /**
     * Returns the property 'debit account iban'.
     *
     * 
     *
     */
    String getDebitAccountIban();
    /**
     * Setter for the property 'debit account bic'.
     *
     * 
     *
     */
    void setDebitAccountBic(String debitAccountBic);

    /**
     * Returns the property 'debit account bic'.
     *
     * 
     *
     */
    String getDebitAccountBic();
    /**
     * Setter for the property 'debit note date'.
     *
     * 
     *
     */
    void setDebitNoteDate(Date debitNoteDate);

    /**
     * Returns the property 'debit note date'.
     *
     * 
     *
     */
    Date getDebitNoteDate();
    /**
     * Setter for the property 'mandate id'.
     *
     * 
     *
     */
    void setMandateId(String mandateId);

    /**
     * Returns the property 'mandate id'.
     *
     * 
     *
     */
    String getMandateId();
    /**
     * Setter for the property 'eref'.
     *
     * 
     *
     */
    void setEref(String eref);

    /**
     * Returns the property 'eref'.
     *
     * 
     *
     */
    String getEref();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    double getAmount();
    /**
     * Setter for the property 'related transaction id list'.
     *
     * 
     *
     */
    void setRelatedTransactionIdList(String relatedTransactionIdList);

    /**
     * Returns the property 'related transaction id list'.
     *
     * 
     *
     */
    String getRelatedTransactionIdList();
    /**
     * Setter for the property 'dtaus file'.
     *
     * 
     *
     */
    void setDtausFile(DtausFile dtausFile);

    /**
     * Returns the property 'dtaus file'.
     *
     * 
     *
     */
    DtausFile getDtausFile();
    /**
     * Setter for the property 'dtaus index'.
     *
     * 
     *
     */
    void setDtausIndex(int dtausIndex);

    /**
     * Returns the property 'dtaus index'.
     *
     * 
     *
     */
    int getDtausIndex();
    /**
     * Setter for the property 'dtaus transaction index'.
     *
     * 
     *
     */
    void setDtausTransactionIndex(int dtausTransactionIndex);

    /**
     * Returns the property 'dtaus transaction index'.
     *
     * 
     *
     */
    int getDtausTransactionIndex();
    /**
     * Setter for the property 'dtaus entry transactions'.
     *
     * 
     *
     */
    void setDtausEntryTransactions(Set<DtausEntryTransaction> dtausEntryTransactions);

    /**
     * Returns the property 'dtaus entry transactions'.
     *
     * 
     *
     */
    Set<DtausEntryTransaction> getDtausEntryTransactions();
    /**
     * Setter for the property 'error code'.
     *
     * 
     *
     */
    void setErrorCode(String errorCode);

    /**
     * Returns the property 'error code'.
     *
     * 
     *
     */
    String getErrorCode();
    /**
     * Setter for the property 'external transaction id'.
     *
     * 
     *
     */
    void setExternalTransactionId(String externalTransactionId);

    /**
     * Returns the property 'external transaction id'.
     *
     * 
     *
     */
    String getExternalTransactionId();

}
