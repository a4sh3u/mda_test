//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(address)}

import de.smava.webapp.account.domain.history.AddressHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Addresss'.
 *
 * 
 *
 * @author generator
 */
public class Address extends AddressHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(address)}
    public Address() {
        setCreationDate(CurrentDate.getDate());
    }

    /**
     * Copies street, zip, city, country, term of residence. Set new creation date.
     * Doesn't copy expiration date.
     */
    public Address(Address address) {
        this();
        setType(address.getType());
        setStreet(address.getStreet());
        setStreetNumber(address.getStreetNumber());
        setZipCode(address.getZipCode());
        setCity(address.getCity());
        setCountry(address.getCountry());
//        setTermOfResidence(address.getTermOfResidence());
        setLivingThereSince(address.getLivingThereSince());
    }

    public Address(String type, String street, String streetNumber, String zipCode, String city, String country) {
        this();
        setType(type);
        setStreet(street);
        setStreetNumber(streetNumber);
        setZipCode(zipCode);
        setCity(city);
        setCountry(country);
//        setTermOfResidence(false);
    }

    /**
     * Setter for the property 'enabled'.
     */
    public void setEnabled(boolean enabled) {
        // enabled := address has no expiration date
        // disabled := address has expiration date
        _expirationDate = enabled ? null : CurrentDate.getDate();
    }
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _type;
        protected String _street;
        protected String _streetNumber;
        protected String _zipCode;
        protected String _city;
        protected String _country;
        protected Date _creationDate;
        protected Date _expirationDate;
        protected Date _livingThereSince;
        
                            /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'street'.
     *
     * 
     *
     */
    public void setStreet(String street) {
        if (!_streetIsSet) {
            _streetIsSet = true;
            _streetInitVal = getStreet();
        }
        registerChange("street", _streetInitVal, street);
        _street = street;
    }
                        
    /**
     * Returns the property 'street'.
     *
     * 
     *
     */
    public String getStreet() {
        return _street;
    }
                                    /**
     * Setter for the property 'street number'.
     *
     * 
     *
     */
    public void setStreetNumber(String streetNumber) {
        if (!_streetNumberIsSet) {
            _streetNumberIsSet = true;
            _streetNumberInitVal = getStreetNumber();
        }
        registerChange("street number", _streetNumberInitVal, streetNumber);
        _streetNumber = streetNumber;
    }
                        
    /**
     * Returns the property 'street number'.
     *
     * 
     *
     */
    public String getStreetNumber() {
        return _streetNumber;
    }
                                    /**
     * Setter for the property 'zip code'.
     *
     * 
     *
     */
    public void setZipCode(String zipCode) {
        if (!_zipCodeIsSet) {
            _zipCodeIsSet = true;
            _zipCodeInitVal = getZipCode();
        }
        registerChange("zip code", _zipCodeInitVal, zipCode);
        _zipCode = zipCode;
    }
                        
    /**
     * Returns the property 'zip code'.
     *
     * 
     *
     */
    public String getZipCode() {
        return _zipCode;
    }
                                    /**
     * Setter for the property 'city'.
     *
     * 
     *
     */
    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = getCity();
        }
        registerChange("city", _cityInitVal, city);
        _city = city;
    }
                        
    /**
     * Returns the property 'city'.
     *
     * 
     *
     */
    public String getCity() {
        return _city;
    }
                                    /**
     * Setter for the property 'country'.
     *
     * 
     *
     */
    public void setCountry(String country) {
        if (!_countryIsSet) {
            _countryIsSet = true;
            _countryInitVal = getCountry();
        }
        registerChange("country", _countryInitVal, country);
        _country = country;
    }
                        
    /**
     * Returns the property 'country'.
     *
     * 
     *
     */
    public String getCountry() {
        return _country;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'living there since'.
     *
     * 
     *
     */
    public void setLivingThereSince(Date livingThereSince) {
        if (!_livingThereSinceIsSet) {
            _livingThereSinceIsSet = true;
            _livingThereSinceInitVal = getLivingThereSince();
        }
        registerChange("living there since", _livingThereSinceInitVal, livingThereSince);
        _livingThereSince = livingThereSince;
    }
                        
    /**
     * Returns the property 'living there since'.
     *
     * 
     *
     */
    public Date getLivingThereSince() {
        return _livingThereSince;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Address.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _street=").append(_street);
            builder.append("\n    _streetNumber=").append(_streetNumber);
            builder.append("\n    _zipCode=").append(_zipCode);
            builder.append("\n    _city=").append(_city);
            builder.append("\n    _country=").append(_country);
            builder.append("\n}");
        } else {
            builder.append(Address.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Address asAddress() {
        return this;
    }
}
