//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\temp\mda\model.xml
//
//    Or you can change the template:
//
//    P:\temp\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(person)}

import de.smava.webapp.account.domain.Person;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Persons'.
 *
 * @author generator
 */
public interface PersonDao extends BaseDao<Person>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the person identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Person getPerson(Long id);

    /**
     * Saves the person.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long savePerson(Person person);

    /**
     * Deletes an person, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the person
     */
    void deletePerson(Long id);

    /**
     * Retrieves all 'Person' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Person> getPersonList();

    /**
     * Retrieves a page of 'Person' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Person> getPersonList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Person' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Person> getPersonList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Person' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Person> getPersonList(Pageable pageable, Sortable sortable);


    /**
     * Returns the number of 'Person' instances.
     */
    long getPersonCount();


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(person)}


    Collection<Person> findPerson(final String firstName, final String lastName, final Timestamp birthDate, final Long currentAccountId);

    Collection<Person> findActivePersonsByAccountIds(List<Long> accountIds);


    // !!!!!!!! End of insert code section !!!!!!!!
}
