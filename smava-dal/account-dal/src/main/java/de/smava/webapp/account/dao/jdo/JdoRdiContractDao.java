//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi contract)}

import de.smava.webapp.account.dao.RdiContractDao;
import de.smava.webapp.account.domain.RdiContract;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.datastore.Sequence;
import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RdiContracts'.
 *
 * @author generator
 */
@Repository(value = "rdiContractDao")
public class JdoRdiContractDao extends JdoBaseDao implements RdiContractDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRdiContractDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rdi contract)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the rdi contract identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RdiContract load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContract - start: id=" + id);
        }
        RdiContract result = getEntity(RdiContract.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContract - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RdiContract getRdiContract(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RdiContract entity = findUniqueEntity(RdiContract.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the rdi contract.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RdiContract rdiContract) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRdiContract: " + rdiContract);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rdi contract)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(rdiContract);
    }

    /**
     * @deprecated Use {@link #save(RdiContract) instead}
     */
    public Long saveRdiContract(RdiContract rdiContract) {
        return save(rdiContract);
    }

    /**
     * Deletes an rdi contract, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi contract
     */
    public void deleteRdiContract(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRdiContract: " + id);
        }
        deleteEntity(RdiContract.class, id);
    }

    /**
     * Retrieves all 'RdiContract' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RdiContract> getRdiContractList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - start");
        }
        Collection<RdiContract> result = getEntities(RdiContract.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RdiContract' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RdiContract> getRdiContractList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - start: pageable=" + pageable);
        }
        Collection<RdiContract> result = getEntities(RdiContract.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiContract' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RdiContract> getRdiContractList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - start: sortable=" + sortable);
        }
        Collection<RdiContract> result = getEntities(RdiContract.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiContract' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RdiContract> getRdiContractList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiContract> result = getEntities(RdiContract.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiContract' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiContract> findRdiContractList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - start: whereClause=" + whereClause);
        }
        Collection<RdiContract> result = findEntities(RdiContract.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RdiContract' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RdiContract> findRdiContractList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RdiContract> result = findEntities(RdiContract.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RdiContract' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiContract> findRdiContractList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RdiContract> result = findEntities(RdiContract.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RdiContract' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiContract> findRdiContractList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RdiContract> result = findEntities(RdiContract.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiContract' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiContract> findRdiContractList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiContract> result = findEntities(RdiContract.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RdiContract' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RdiContract> findRdiContractList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RdiContract> result = findEntities(RdiContract.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRdiContractList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RdiContract' instances.
     */
    public long getRdiContractCount() {
        long result = getEntityCount(RdiContract.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiContract' instances which match the given whereClause.
     */
    public long getRdiContractCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RdiContract.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RdiContract' instances which match the given whereClause together with params specified in object array.
     */
    public long getRdiContractCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RdiContract.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRdiContractCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi contract)}
    //
    // insert custom methods here
    //
    @Override
    public Long getRdiContractNumber() {
    	Sequence seq = getPersistenceManager().getSequence("de.smava.webapp.account.domain.RDI_CONTRACT_NUMBER");
        return (Long) seq.next();
	}
    @Override
    public Collection<RdiContract> getRdiContractsInProcessing() {
        return findRdiContractList(
                "_expirationDate == null && _state == '" + RdiContract.STATE_PROCESSING + "' "
        );
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
