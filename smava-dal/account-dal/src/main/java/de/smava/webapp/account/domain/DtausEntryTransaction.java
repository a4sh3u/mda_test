package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.DtausEntryTransactionHistory;

/**
 * The domain object that represents 'DtausEntryTransactions'.
 */
public class DtausEntryTransaction extends DtausEntryTransactionHistory  {

    public DtausEntryTransaction(DtausEntry DtausEntry, Transaction Transaction, String type) {
        _dtausEntry = DtausEntry;
        _transaction = Transaction;
        _type = type;
    }

        protected DtausEntry _dtausEntry;
        protected Transaction _transaction;
        protected String _type;

    /**
     * Setter for the property 'dtaus entry'.
     */
    public void setDtausEntry(DtausEntry dtausEntry) {
        _dtausEntry = dtausEntry;
    }
            
    /**
     * Returns the property 'dtaus entry'.
     */
    public DtausEntry getDtausEntry() {
        return _dtausEntry;
    }
                                            
    /**
     * Setter for the property 'transaction'.
     */
    public void setTransaction(Transaction transaction) {
        _transaction = transaction;
    }
            
    /**
     * Returns the property 'transaction'.
     */
    public Transaction getTransaction() {
        return _transaction;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DtausEntryTransaction.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(DtausEntryTransaction.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
