package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.Document;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'ConsolidatedDebts'.
 *
 * @author generator
 */
public interface ConsolidatedDebtEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'monthly installment'.
     *
     * 
     *
     */
    void setMonthlyInstallment(Double monthlyInstallment);

    /**
     * Returns the property 'monthly installment'.
     *
     * 
     *
     */
    Double getMonthlyInstallment();
    /**
     * Setter for the property 'months remaining'.
     *
     * 
     *
     */
    void setMonthsRemaining(Integer monthsRemaining);

    /**
     * Returns the property 'months remaining'.
     *
     * 
     *
     */
    Integer getMonthsRemaining();
    /**
     * Setter for the property 'interest rate'.
     *
     * 
     *
     */
    void setInterestRate(Double interestRate);

    /**
     * Returns the property 'interest rate'.
     *
     * 
     *
     */
    Double getInterestRate();
    /**
     * Setter for the property 'loan id'.
     *
     * 
     *
     */
    void setLoanId(String loanId);

    /**
     * Returns the property 'loan id'.
     *
     * 
     *
     */
    String getLoanId();
    /**
     * Setter for the property 'debt amount'.
     *
     * 
     *
     */
    void setDebtAmount(Double debtAmount);

    /**
     * Returns the property 'debt amount'.
     *
     * 
     *
     */
    Double getDebtAmount();
    /**
     * Setter for the property 'due date'.
     *
     * 
     *
     */
    void setDueDate(Date dueDate);

    /**
     * Returns the property 'due date'.
     *
     * 
     *
     */
    Date getDueDate();
    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    void setStartDate(Date startDate);

    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    Date getStartDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'credit type'.
     *
     * 
     *
     */
    void setCreditType(String creditType);

    /**
     * Returns the property 'credit type'.
     *
     * 
     *
     */
    String getCreditType();
    /**
     * Setter for the property 'consolidation'.
     *
     * 
     *
     */
    void setConsolidation(boolean consolidation);

    /**
     * Returns the property 'consolidation'.
     *
     * 
     *
     */
    boolean getConsolidation();
    /**
     * Setter for the property 'bank accounts'.
     *
     * 
     *
     */
    void setBankAccounts(Collection<BankAccount> bankAccounts);

    /**
     * Returns the property 'bank accounts'.
     *
     * 
     *
     */
    Collection<BankAccount> getBankAccounts();
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    void setDocuments(Collection<Document> documents);

    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    Collection<Document> getDocuments();

}
