package de.smava.webapp.account.todo.web;

import org.apache.commons.lang.StringUtils;

/**
 * base command to support sorting and ordering.
 * 
 * @author rfiedler
 *
 */
public class SortableCommand {

    private String _orderBy;
    private boolean _sortDesc;

    public SortableCommand() {
        super();
    }
    
    /**
     * Set default values for sort and order.
     */
    public void prepareSortAndOrder(String sortDesc,String orderBy) {
        if (StringUtils.isEmpty(sortDesc)) {
        	setSortDesc(true);
        }
        if (StringUtils.isEmpty(orderBy)) {
        	setOrderBy("activationDate");
        }
    }

    public String getOrderBy() {
        return _orderBy;
    }

    public void setOrderBy(String orderBy) {
        this._orderBy = orderBy;
    }

    public boolean isSortDesc() {
        return _sortDesc;
    }

    public void setSortDesc(boolean sortDesc) {
        this._sortDesc = sortDesc;
    }

}