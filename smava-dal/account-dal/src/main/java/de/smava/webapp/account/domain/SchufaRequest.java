package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.SchufaRequestHistory;

import java.util.Date;

/**
 * The domain object that represents 'SchufaRequests'.
 */
public class SchufaRequest extends SchufaRequestHistory  {

        protected String _requestXml;
        protected String _responseXml;
        protected Date _creationDate;
        protected SchufaScoreRequestData _data;
        protected String _state;
        protected String _type;
        protected Account _account;
        protected String _accountNumber;
        protected String _schufaPersonKey;
        protected Long _personId;
        
                            /**
     * Setter for the property 'request xml'.
     */
    public void setRequestXml(String requestXml) {
        if (!_requestXmlIsSet) {
            _requestXmlIsSet = true;
            _requestXmlInitVal = getRequestXml();
        }
        registerChange("request xml", _requestXmlInitVal, requestXml);
        _requestXml = requestXml;
    }
                        
    /**
     * Returns the property 'request xml'.
     */
    public String getRequestXml() {
        return _requestXml;
    }
                                    /**
     * Setter for the property 'response xml'.
     */
    public void setResponseXml(String responseXml) {
        if (!_responseXmlIsSet) {
            _responseXmlIsSet = true;
            _responseXmlInitVal = getResponseXml();
        }
        registerChange("response xml", _responseXmlInitVal, responseXml);
        _responseXml = responseXml;
    }
                        
    /**
     * Returns the property 'response xml'.
     */
    public String getResponseXml() {
        return _responseXml;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'data'.
     */
    public void setData(SchufaScoreRequestData data) {
        _data = data;
    }
            
    /**
     * Returns the property 'data'.
     */
    public SchufaScoreRequestData getData() {
        return _data;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'account number'.
     */
    public void setAccountNumber(String accountNumber) {
        if (!_accountNumberIsSet) {
            _accountNumberIsSet = true;
            _accountNumberInitVal = getAccountNumber();
        }
        registerChange("account number", _accountNumberInitVal, accountNumber);
        _accountNumber = accountNumber;
    }
                        
    /**
     * Returns the property 'account number'.
     */
    public String getAccountNumber() {
        return _accountNumber;
    }
                                    /**
     * Setter for the property 'schufa person key'.
     */
    public void setSchufaPersonKey(String schufaPersonKey) {
        if (!_schufaPersonKeyIsSet) {
            _schufaPersonKeyIsSet = true;
            _schufaPersonKeyInitVal = getSchufaPersonKey();
        }
        registerChange("schufa person key", _schufaPersonKeyInitVal, schufaPersonKey);
        _schufaPersonKey = schufaPersonKey;
    }
                        
    /**
     * Returns the property 'schufa person key'.
     */
    public String getSchufaPersonKey() {
        return _schufaPersonKey;
    }
                                            
    /**
     * Setter for the property 'person id'.
     */
    public void setPersonId(Long personId) {
        _personId = personId;
    }
            
    /**
     * Returns the property 'person id'.
     */
    public Long getPersonId() {
        return _personId;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaRequest.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _requestXml=").append(_requestXml);
            builder.append("\n    _responseXml=").append(_responseXml);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _accountNumber=").append(_accountNumber);
            builder.append("\n    _schufaPersonKey=").append(_schufaPersonKey);
            builder.append("\n}");
        } else {
            builder.append(SchufaRequest.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
