//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk-createOrderFlow\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk-createOrderFlow\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rdi response)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;


import de.smava.webapp.account.domain.RdiResponse;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RdiResponses'.
 *
 * @author generator
 */
public interface RdiResponseDao extends BaseDao<RdiResponse>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the rdi response identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RdiResponse getRdiResponse(Long id);

    /**
     * Saves the rdi response.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRdiResponse(RdiResponse rdiResponse);

    /**
     * Deletes an rdi response, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rdi response
     */
    void deleteRdiResponse(Long id);

    /**
     * Retrieves all 'RdiResponse' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RdiResponse> getRdiResponseList();

    /**
     * Retrieves a page of 'RdiResponse' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RdiResponse> getRdiResponseList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RdiResponse' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RdiResponse> getRdiResponseList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RdiResponse' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RdiResponse> getRdiResponseList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'RdiResponse' instances.
     */
    long getRdiResponseCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rdi response)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
