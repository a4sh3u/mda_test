package de.smava.webapp.account.domain.interfaces;



import de.smava.webapp.account.domain.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The domain object that represents 'Transactions'.
 *
 * @author generator
 */
public interface TransactionEntityInterface {

    /**
     * Setter for the property 'transaction id'.
     *
     * 
     *
     */
    void setTransactionId(Long transactionId);

    /**
     * Returns the property 'transaction id'.
     *
     * 
     *
     */
    Long getTransactionId();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'transaction date'.
     *
     * 
     *
     */
    void setTransactionDate(Date transactionDate);

    /**
     * Returns the property 'transaction date'.
     *
     * 
     *
     */
    Date getTransactionDate();
    /**
     * Setter for the property 'due date'.
     *
     * 
     *
     */
    void setDueDate(Date dueDate);

    /**
     * Returns the property 'due date'.
     *
     * 
     *
     */
    Date getDueDate();
    /**
     * Setter for the property 'modified date'.
     *
     * 
     *
     */
    void setModifiedDate(Date modifiedDate);

    /**
     * Returns the property 'modified date'.
     *
     * 
     *
     */
    Date getModifiedDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'credit account'.
     *
     * 
     *
     */
    void setCreditAccount(BankAccount creditAccount);

    /**
     * Returns the property 'credit account'.
     *
     * 
     *
     */
    BankAccount getCreditAccount();
    /**
     * Setter for the property 'debit account'.
     *
     * 
     *
     */
    void setDebitAccount(BankAccount debitAccount);

    /**
     * Returns the property 'debit account'.
     *
     * 
     *
     */
    BankAccount getDebitAccount();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'bookings'.
     *
     * 
     *
     */
    void setBookings(List<Booking> bookings);

    /**
     * Returns the property 'bookings'.
     *
     * 
     *
     */
    List<Booking> getBookings();
    /**
     * Setter for the property 'dtaus file'.
     *
     * 
     *
     */
    void setDtausFile(DtausFile dtausFile);

    /**
     * Returns the property 'dtaus file'.
     *
     * 
     *
     */
    DtausFile getDtausFile();
    /**
     * Setter for the property 'dtaus entry transactions'.
     *
     * 
     *
     */
    void setDtausEntryTransactions(Set<DtausEntryTransaction> dtausEntryTransactions);

    /**
     * Returns the property 'dtaus entry transactions'.
     *
     * 
     *
     */
    Set<DtausEntryTransaction> getDtausEntryTransactions();
    /**
     * Setter for the property 'reminder states'.
     *
     * 
     *
     */
    void setReminderStates(List<ReminderState> reminderStates);

    /**
     * Returns the property 'reminder states'.
     *
     * 
     *
     */
    List<ReminderState> getReminderStates();
    /**
     * Setter for the property 'external transaction id'.
     *
     * 
     *
     */
    void setExternalTransactionId(String externalTransactionId);

    /**
     * Returns the property 'external transaction id'.
     *
     * 
     *
     */
    String getExternalTransactionId();
    /**
     * Setter for the property 'dtausFileOutTransactions'.
     *
     * 
     *
     */
    void setDtausFileOutTransactions(List<DtausFileOutTransaction> dtausFileOutTransactions);

    /**
     * Returns the property 'dtausFileOutTransactions'.
     *
     * 
     *
     */
    List<DtausFileOutTransaction> getDtausFileOutTransactions();

}
