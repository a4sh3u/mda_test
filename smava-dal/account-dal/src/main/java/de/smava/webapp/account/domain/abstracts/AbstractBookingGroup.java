//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking group)}

import de.smava.webapp.account.domain.interfaces.BookingGroupEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.util.FormatUtils;
import org.apache.log4j.Logger;

import java.util.Locale;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BookingGroups'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBookingGroup
    extends KreditPrivatEntity implements BookingGroupEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBookingGroup.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(booking group)}

    private static final long serialVersionUID = 8850788672985427833L;

    public static final String TYPE_UNKNOWN = "???";
    public static final String TYPE_CREDIT_PAYOUT = "CREDIT_PAYOUT";
    public static final String TYPE_INSURANCE_GROUP = "INSURANCE";
    public static final String TYPE_SMAVA_GROUP = "SMAVA_GROUP";
    public static final String TYPE_INVITATION_BONUS = "INVITATION";
    public static final String TYPE_CONTRACT_CANCELLATION = "CONTRACT_CANCELLATION";
    public static final String TYPE_REPAYMENT_BREAK = "REPAYMENT_BREAK";



    public String getViewName() {
        Locale german = new Locale("de", "DE");
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(" ");
        sb.append(FormatUtils.formatMonthDateFormat(getDate(), german));
        sb.append(" ");
        if (TYPE_INSURANCE_GROUP.equals(getType())) {
            sb.append(getMarket());
        } else if (TYPE_INVITATION_BONUS.equals(getType())) {
            sb.append(getSpecifier());
        } else if (TYPE_SMAVA_GROUP.equals(getType())) {
            sb.append(getSpecifier());
        } else {
            sb.append(getName());
        }
        return sb.toString();
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

