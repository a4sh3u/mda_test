//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head15/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head15/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(repayment break)}

import de.smava.webapp.account.domain.*;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RepaymentBreaks'.
 *
 * @author generator
 */
public interface RepaymentBreakDao extends BaseDao<RepaymentBreak>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the repayment break identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RepaymentBreak getRepaymentBreak(Long id);

    /**
     * Saves the repayment break.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRepaymentBreak(RepaymentBreak repaymentBreak);

    /**
     * Deletes an repayment break, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the repayment break
     */
    void deleteRepaymentBreak(Long id);

    /**
     * Retrieves all 'RepaymentBreak' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RepaymentBreak> getRepaymentBreakList();

    /**
     * Retrieves a page of 'RepaymentBreak' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RepaymentBreak> getRepaymentBreakList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RepaymentBreak' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RepaymentBreak> getRepaymentBreakList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RepaymentBreak' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RepaymentBreak> getRepaymentBreakList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'RepaymentBreak' instances.
     */
    long getRepaymentBreakCount();


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(repayment break)}
    //
    Map<Date, Double> getRequestedMonthAndSum(Long repaymentBreakId);
    Map<Date, Double> getRequestedMonthAndSum(RepaymentBreak repaymentBreak);
    
    Map<Date, Double> getConfirmedMonthAndSum(Long repaymentBreakId);
    Map<Date, Double> getConfirmedMonthAndSum(RepaymentBreak repaymentBreak);
    
    
    Map<Long, RepaymentBreakMonthData> getCurrentMonthlyData(Account account, Collection<BankAccount> interimAccounts, Set<Contract> contracts, Date month);
    
    int getConfirmedRepaymentBreakCount(Account account);
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
