package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'BankAccountProviders'.
 *
 * @author generator
 */
public interface BankAccountProviderEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'schufaUsername'.
     *
     * 
     *
     */
    void setSchufaUsername(String schufaUsername);

    /**
     * Returns the property 'schufaUsername'.
     *
     * 
     *
     */
    String getSchufaUsername();
    /**
     * Setter for the property 'schufaPassword'.
     *
     * 
     *
     */
    void setSchufaPassword(String schufaPassword);

    /**
     * Returns the property 'schufaPassword'.
     *
     * 
     *
     */
    String getSchufaPassword();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.account.domain.BankAccountProviderType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccountProviderType getType();

}
