package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractMailLink;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MailLinks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MailLinkHistory extends AbstractMailLink {

    protected transient int _contentIdInitVal;
    protected transient boolean _contentIdIsSet;
    protected transient String _targetUrlInitVal;
    protected transient boolean _targetUrlIsSet;
    protected transient String _paramsInitVal;
    protected transient boolean _paramsIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _clickDateInitVal;
    protected transient boolean _clickDateIsSet;
    protected transient int _clickCountInitVal;
    protected transient boolean _clickCountIsSet;
    protected transient String _mailTypeInitVal;
    protected transient boolean _mailTypeIsSet;
    protected transient String _linkNameInitVal;
    protected transient boolean _linkNameIsSet;
    protected transient String _remoteAddressInitVal;
    protected transient boolean _remoteAddressIsSet;


	
    /**
     * Returns the initial value of the property 'content id'.
     */
    public int contentIdInitVal() {
        int result;
        if (_contentIdIsSet) {
            result = _contentIdInitVal;
        } else {
            result = getContentId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'content id'.
     */
    public boolean contentIdIsDirty() {
        return !valuesAreEqual(contentIdInitVal(), getContentId());
    }

    /**
     * Returns true if the setter method was called for the property 'content id'.
     */
    public boolean contentIdIsSet() {
        return _contentIdIsSet;
    }
	
    /**
     * Returns the initial value of the property 'target url'.
     */
    public String targetUrlInitVal() {
        String result;
        if (_targetUrlIsSet) {
            result = _targetUrlInitVal;
        } else {
            result = getTargetUrl();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'target url'.
     */
    public boolean targetUrlIsDirty() {
        return !valuesAreEqual(targetUrlInitVal(), getTargetUrl());
    }

    /**
     * Returns true if the setter method was called for the property 'target url'.
     */
    public boolean targetUrlIsSet() {
        return _targetUrlIsSet;
    }
	
    /**
     * Returns the initial value of the property 'params'.
     */
    public String paramsInitVal() {
        String result;
        if (_paramsIsSet) {
            result = _paramsInitVal;
        } else {
            result = getParams();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'params'.
     */
    public boolean paramsIsDirty() {
        return !valuesAreEqual(paramsInitVal(), getParams());
    }

    /**
     * Returns true if the setter method was called for the property 'params'.
     */
    public boolean paramsIsSet() {
        return _paramsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'click date'.
     */
    public Date clickDateInitVal() {
        Date result;
        if (_clickDateIsSet) {
            result = _clickDateInitVal;
        } else {
            result = getClickDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'click date'.
     */
    public boolean clickDateIsDirty() {
        return !valuesAreEqual(clickDateInitVal(), getClickDate());
    }

    /**
     * Returns true if the setter method was called for the property 'click date'.
     */
    public boolean clickDateIsSet() {
        return _clickDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'click count'.
     */
    public int clickCountInitVal() {
        int result;
        if (_clickCountIsSet) {
            result = _clickCountInitVal;
        } else {
            result = getClickCount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'click count'.
     */
    public boolean clickCountIsDirty() {
        return !valuesAreEqual(clickCountInitVal(), getClickCount());
    }

    /**
     * Returns true if the setter method was called for the property 'click count'.
     */
    public boolean clickCountIsSet() {
        return _clickCountIsSet;
    }
			
    /**
     * Returns the initial value of the property 'mail type'.
     */
    public String mailTypeInitVal() {
        String result;
        if (_mailTypeIsSet) {
            result = _mailTypeInitVal;
        } else {
            result = getMailType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'mail type'.
     */
    public boolean mailTypeIsDirty() {
        return !valuesAreEqual(mailTypeInitVal(), getMailType());
    }

    /**
     * Returns true if the setter method was called for the property 'mail type'.
     */
    public boolean mailTypeIsSet() {
        return _mailTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'link name'.
     */
    public String linkNameInitVal() {
        String result;
        if (_linkNameIsSet) {
            result = _linkNameInitVal;
        } else {
            result = getLinkName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'link name'.
     */
    public boolean linkNameIsDirty() {
        return !valuesAreEqual(linkNameInitVal(), getLinkName());
    }

    /**
     * Returns true if the setter method was called for the property 'link name'.
     */
    public boolean linkNameIsSet() {
        return _linkNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'remote address'.
     */
    public String remoteAddressInitVal() {
        String result;
        if (_remoteAddressIsSet) {
            result = _remoteAddressInitVal;
        } else {
            result = getRemoteAddress();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'remote address'.
     */
    public boolean remoteAddressIsDirty() {
        return !valuesAreEqual(remoteAddressInitVal(), getRemoteAddress());
    }

    /**
     * Returns true if the setter method was called for the property 'remote address'.
     */
    public boolean remoteAddressIsSet() {
        return _remoteAddressIsSet;
    }

}
