package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BankAccountClearingRunHistory;

import java.util.Date;

/**
 * The domain object that represents 'BankAccountClearingRuns'.
 */
public class BankAccountClearingRun extends BankAccountClearingRunHistory  {

	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = -2929318990025512615L;

        protected BankAccount _bankAccount;
        protected ClearingRun _clearingRun;
        protected Long _transactionId;
        protected Date _deliveryDate;
        
                                    
    /**
     * Setter for the property 'bank account'.
     */
    public void setBankAccount(BankAccount bankAccount) {
        _bankAccount = bankAccount;
    }
            
    /**
     * Returns the property 'bank account'.
     */
    public BankAccount getBankAccount() {
        return _bankAccount;
    }
                                            
    /**
     * Setter for the property 'clearing run'.
     */
    public void setClearingRun(ClearingRun clearingRun) {
        _clearingRun = clearingRun;
    }
            
    /**
     * Returns the property 'clearing run'.
     */
    public ClearingRun getClearingRun() {
        return _clearingRun;
    }
                                            
    /**
     * Setter for the property 'transaction id'.
     */
    public void setTransactionId(Long transactionId) {
        _transactionId = transactionId;
    }
            
    /**
     * Returns the property 'transaction id'.
     */
    public Long getTransactionId() {
        return _transactionId;
    }
                                    /**
     * Setter for the property 'delivery date'.
     */
    public void setDeliveryDate(Date deliveryDate) {
        if (!_deliveryDateIsSet) {
            _deliveryDateIsSet = true;
            _deliveryDateInitVal = getDeliveryDate();
        }
        registerChange("delivery date", _deliveryDateInitVal, deliveryDate);
        _deliveryDate = deliveryDate;
    }
                        
    /**
     * Returns the property 'delivery date'.
     */
    public Date getDeliveryDate() {
        return _deliveryDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BankAccountClearingRun.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BankAccountClearingRun.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
