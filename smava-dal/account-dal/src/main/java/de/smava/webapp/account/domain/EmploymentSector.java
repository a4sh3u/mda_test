package de.smava.webapp.account.domain;

/**
 * String holder for the employment sector type.
 * @author Dimitar Robev
 *
 */
public class EmploymentSector {
	
	private EmploymentSector() {
		super();
	}
	public static final String PUBLIC_INSTITUTION = "Administracja państwowa";
	public static final String FINANCE = "Finanse, bankowość, ubezpieczenia itp.";
	public static final String HEALTHCARE = "Opieka medyczna";
	public static final String CIVIL_SERVANTS = "Służba mundurowa";
	public static final String MANUFACTORING = "Produkcja";
	public static final String TRADE = "Handel";
	public static final String OTHER = "Inna, każda nie wymieniona powyżej";
}
