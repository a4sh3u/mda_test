package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBookingPerformance;




/**
 * The domain object that has all history aggregation related fields for 'BookingPerformances'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BookingPerformanceHistory extends AbstractBookingPerformance {



			
}
