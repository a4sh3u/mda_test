package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.MatchSorting;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'MatchSortingRuns'.
 *
 * @author generator
 */
public interface MatchSortingRunEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'market name'.
     *
     * 
     *
     */
    void setMarketName(String marketName);

    /**
     * Returns the property 'market name'.
     *
     * 
     *
     */
    String getMarketName();
    /**
     * Setter for the property 'match sortings'.
     *
     * 
     *
     */
    void setMatchSortings(List<MatchSorting> matchSortings);

    /**
     * Returns the property 'match sortings'.
     *
     * 
     *
     */
    List<MatchSorting> getMatchSortings();

}
