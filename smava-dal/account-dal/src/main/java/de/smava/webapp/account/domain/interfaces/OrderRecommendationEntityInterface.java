package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Order;

import java.util.Date;


/**
 * The domain object that represents 'OrderRecommendations'.
 *
 * @author generator
 */
public interface OrderRecommendationEntityInterface {

    /**
     * Setter for the property 'text'.
     *
     * 
     *
     */
    void setText(String text);

    /**
     * Returns the property 'text'.
     *
     * 
     *
     */
    String getText();
    /**
     * Setter for the property 'author'.
     *
     * 
     *
     */
    void setAuthor(Account author);

    /**
     * Returns the property 'author'.
     *
     * 
     *
     */
    Account getAuthor();
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    void setOrder(Order order);

    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    Order getOrder();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();

}
