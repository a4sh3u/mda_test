package de.smava.webapp.account.domain;

/**
 * String holder for the employment experience type.
 * @author Dimitar Robev
 *
 */
public class EmploymentExperience {
	
	
	private EmploymentExperience() {
		super();
	}
	public static final String UP_TO_THREE_YEARS = "UP_TO_THREE_YEARS";
	public static final String BETWEEN_THREE_AND_TEN_YEARS = "BETWEEN_THREE_AND_TEN_YEARS";
	public static final String ABOVE_TEN_YEARS = "ABOVE_TEN_YEARS";
	public static final String NOT_APPLICABLE = "NOT_APPLICABLE";
}
