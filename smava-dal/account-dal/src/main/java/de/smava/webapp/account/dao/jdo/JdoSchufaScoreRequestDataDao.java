//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score request data)}

import de.smava.webapp.account.dao.SchufaScoreRequestDataDao;
import de.smava.webapp.account.domain.SchufaScoreRequestData;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'SchufaScoreRequestDatas'.
 *
 * @author generator
 */
@Repository(value = "schufaScoreRequestDataDao")
public class JdoSchufaScoreRequestDataDao extends JdoBaseDao implements SchufaScoreRequestDataDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSchufaScoreRequestDataDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(schufa score request data)}
    private static final long serialVersionUID = 7807755744892676131L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the schufa score request data identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public SchufaScoreRequestData load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestData - start: id=" + id);
        }
        SchufaScoreRequestData result = getEntity(SchufaScoreRequestData.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestData - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public SchufaScoreRequestData getSchufaScoreRequestData(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	SchufaScoreRequestData entity = findUniqueEntity(SchufaScoreRequestData.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the schufa score request data.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(SchufaScoreRequestData schufaScoreRequestData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveSchufaScoreRequestData: " + schufaScoreRequestData);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(schufa score request data)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(schufaScoreRequestData);
    }

    /**
     * @deprecated Use {@link #save(SchufaScoreRequestData) instead}
     */
    public Long saveSchufaScoreRequestData(SchufaScoreRequestData schufaScoreRequestData) {
        return save(schufaScoreRequestData);
    }

    /**
     * Deletes an schufa score request data, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the schufa score request data
     */
    public void deleteSchufaScoreRequestData(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteSchufaScoreRequestData: " + id);
        }
        deleteEntity(SchufaScoreRequestData.class, id);
    }

    /**
     * Retrieves all 'SchufaScoreRequestData' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<SchufaScoreRequestData> getSchufaScoreRequestDataList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - start");
        }
        Collection<SchufaScoreRequestData> result = getEntities(SchufaScoreRequestData.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'SchufaScoreRequestData' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<SchufaScoreRequestData> getSchufaScoreRequestDataList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - start: pageable=" + pageable);
        }
        Collection<SchufaScoreRequestData> result = getEntities(SchufaScoreRequestData.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScoreRequestData' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<SchufaScoreRequestData> getSchufaScoreRequestDataList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - start: sortable=" + sortable);
        }
        Collection<SchufaScoreRequestData> result = getEntities(SchufaScoreRequestData.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreRequestData' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<SchufaScoreRequestData> getSchufaScoreRequestDataList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreRequestData> result = getEntities(SchufaScoreRequestData.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScoreRequestData' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreRequestData> findSchufaScoreRequestDataList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - start: whereClause=" + whereClause);
        }
        Collection<SchufaScoreRequestData> result = findEntities(SchufaScoreRequestData.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'SchufaScoreRequestData' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<SchufaScoreRequestData> findSchufaScoreRequestDataList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<SchufaScoreRequestData> result = findEntities(SchufaScoreRequestData.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'SchufaScoreRequestData' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreRequestData> findSchufaScoreRequestDataList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<SchufaScoreRequestData> result = findEntities(SchufaScoreRequestData.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'SchufaScoreRequestData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreRequestData> findSchufaScoreRequestDataList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<SchufaScoreRequestData> result = findEntities(SchufaScoreRequestData.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreRequestData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreRequestData> findSchufaScoreRequestDataList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreRequestData> result = findEntities(SchufaScoreRequestData.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'SchufaScoreRequestData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<SchufaScoreRequestData> findSchufaScoreRequestDataList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<SchufaScoreRequestData> result = findEntities(SchufaScoreRequestData.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findSchufaScoreRequestDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreRequestData' instances.
     */
    public long getSchufaScoreRequestDataCount() {
        long result = getEntityCount(SchufaScoreRequestData.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreRequestData' instances which match the given whereClause.
     */
    public long getSchufaScoreRequestDataCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(SchufaScoreRequestData.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'SchufaScoreRequestData' instances which match the given whereClause together with params specified in object array.
     */
    public long getSchufaScoreRequestDataCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(SchufaScoreRequestData.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getSchufaScoreRequestDataCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(schufa score request data)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
