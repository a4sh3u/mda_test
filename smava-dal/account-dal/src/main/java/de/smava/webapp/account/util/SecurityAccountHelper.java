package de.smava.webapp.account.util;

import de.smava.webapp.account.security.SmavaUserDetails;
import de.smava.webapp.account.security.SmavaUserDetailsIf;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created with IntelliJ IDEA.
 * User: aakhmerov
 * Date: 11/27/13
 * Time: 4:15 PM
 * [WEBSITE-11988]
 * wrapper around spring security implementation to grab user
 */
public final class SecurityAccountHelper {

    private SecurityAccountHelper() {
    }

    /**
     * Returns the id of the currently logged in user.
     */
    public static Long getCurrentUserId() {
        return getCurrentSmavaId();
    }

    public static Long getCurrentSmavaId() {
        return getSmavaIdFromSecurityContext(SecurityContextHolder.getContext());
    }


    public static Long getSmavaIdFromSecurityContext(final SecurityContext securityContext) {
        Long userId = null;
        if (securityContext != null) {
            final Authentication auth = securityContext.getAuthentication();
            if (auth != null) {
                Object principal = auth.getPrincipal();
                if (principal instanceof SmavaUserDetailsIf) {
                    SmavaUserDetailsIf smavaPrincipal = (SmavaUserDetailsIf) principal;
                    if (smavaPrincipal.getSmavaId() != null) {
                        userId = smavaPrincipal.getSmavaId();
                    }
                }
            }
        }
        return userId;
    }

}
