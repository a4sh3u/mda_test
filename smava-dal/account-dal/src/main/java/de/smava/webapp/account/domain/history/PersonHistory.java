package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractPerson;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Persons'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class PersonHistory extends AbstractPerson {

    protected transient String _titleInitVal;
    protected transient boolean _titleIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _salutationInitVal;
    protected transient boolean _salutationIsSet;
    protected transient String _firstNameInitVal;
    protected transient boolean _firstNameIsSet;
    protected transient String _lastNameInitVal;
    protected transient boolean _lastNameIsSet;
    protected transient String _birthNameInitVal;
    protected transient boolean _birthNameIsSet;
    protected transient Date _dateOfBirthInitVal;
    protected transient boolean _dateOfBirthIsSet;
    protected transient String _countryOfBirthInitVal;
    protected transient boolean _countryOfBirthIsSet;
    protected transient String _placeOfBirthInitVal;
    protected transient boolean _placeOfBirthIsSet;
    protected transient String _citizenshipInitVal;
    protected transient boolean _citizenshipIsSet;
    protected transient String _familyStatusInitVal;
    protected transient boolean _familyStatusIsSet;
    protected transient String _peselInitVal;
    protected transient boolean _peselIsSet;
    protected transient String _identityCardNumberInitVal;
    protected transient boolean _identityCardNumberIsSet;
    protected transient String _nipInitVal;
    protected transient boolean _nipIsSet;
    protected transient String _educationEarnedInitVal;
    protected transient boolean _educationEarnedIsSet;
    protected transient Date _validUntilInitVal;
    protected transient boolean _validUntilIsSet;
    protected transient int _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient Date _postidentFeeInitVal;
    protected transient boolean _postidentFeeIsSet;


	
    /**
     * Returns the initial value of the property 'title'.
     */
    public String titleInitVal() {
        String result;
        if (_titleIsSet) {
            result = _titleInitVal;
        } else {
            result = getTitle();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'title'.
     */
    public boolean titleIsDirty() {
        return !valuesAreEqual(titleInitVal(), getTitle());
    }

    /**
     * Returns true if the setter method was called for the property 'title'.
     */
    public boolean titleIsSet() {
        return _titleIsSet;
    }
		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'salutation'.
     */
    public String salutationInitVal() {
        String result;
        if (_salutationIsSet) {
            result = _salutationInitVal;
        } else {
            result = getSalutation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'salutation'.
     */
    public boolean salutationIsDirty() {
        return !valuesAreEqual(salutationInitVal(), getSalutation());
    }

    /**
     * Returns true if the setter method was called for the property 'salutation'.
     */
    public boolean salutationIsSet() {
        return _salutationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'first name'.
     */
    public String firstNameInitVal() {
        String result;
        if (_firstNameIsSet) {
            result = _firstNameInitVal;
        } else {
            result = getFirstName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first name'.
     */
    public boolean firstNameIsDirty() {
        return !valuesAreEqual(firstNameInitVal(), getFirstName());
    }

    /**
     * Returns true if the setter method was called for the property 'first name'.
     */
    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last name'.
     */
    public String lastNameInitVal() {
        String result;
        if (_lastNameIsSet) {
            result = _lastNameInitVal;
        } else {
            result = getLastName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last name'.
     */
    public boolean lastNameIsDirty() {
        return !valuesAreEqual(lastNameInitVal(), getLastName());
    }

    /**
     * Returns true if the setter method was called for the property 'last name'.
     */
    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'birth name'.
     */
    public String birthNameInitVal() {
        String result;
        if (_birthNameIsSet) {
            result = _birthNameInitVal;
        } else {
            result = getBirthName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birth name'.
     */
    public boolean birthNameIsDirty() {
        return !valuesAreEqual(birthNameInitVal(), getBirthName());
    }

    /**
     * Returns true if the setter method was called for the property 'birth name'.
     */
    public boolean birthNameIsSet() {
        return _birthNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'date of birth'.
     */
    public Date dateOfBirthInitVal() {
        Date result;
        if (_dateOfBirthIsSet) {
            result = _dateOfBirthInitVal;
        } else {
            result = getDateOfBirth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date of birth'.
     */
    public boolean dateOfBirthIsDirty() {
        return !valuesAreEqual(dateOfBirthInitVal(), getDateOfBirth());
    }

    /**
     * Returns true if the setter method was called for the property 'date of birth'.
     */
    public boolean dateOfBirthIsSet() {
        return _dateOfBirthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'country of birth'.
     */
    public String countryOfBirthInitVal() {
        String result;
        if (_countryOfBirthIsSet) {
            result = _countryOfBirthInitVal;
        } else {
            result = getCountryOfBirth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'country of birth'.
     */
    public boolean countryOfBirthIsDirty() {
        return !valuesAreEqual(countryOfBirthInitVal(), getCountryOfBirth());
    }

    /**
     * Returns true if the setter method was called for the property 'country of birth'.
     */
    public boolean countryOfBirthIsSet() {
        return _countryOfBirthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'place of birth'.
     */
    public String placeOfBirthInitVal() {
        String result;
        if (_placeOfBirthIsSet) {
            result = _placeOfBirthInitVal;
        } else {
            result = getPlaceOfBirth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'place of birth'.
     */
    public boolean placeOfBirthIsDirty() {
        return !valuesAreEqual(placeOfBirthInitVal(), getPlaceOfBirth());
    }

    /**
     * Returns true if the setter method was called for the property 'place of birth'.
     */
    public boolean placeOfBirthIsSet() {
        return _placeOfBirthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'citizenship'.
     */
    public String citizenshipInitVal() {
        String result;
        if (_citizenshipIsSet) {
            result = _citizenshipInitVal;
        } else {
            result = getCitizenship();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'citizenship'.
     */
    public boolean citizenshipIsDirty() {
        return !valuesAreEqual(citizenshipInitVal(), getCitizenship());
    }

    /**
     * Returns true if the setter method was called for the property 'citizenship'.
     */
    public boolean citizenshipIsSet() {
        return _citizenshipIsSet;
    }
	
    /**
     * Returns the initial value of the property 'family status'.
     */
    public String familyStatusInitVal() {
        String result;
        if (_familyStatusIsSet) {
            result = _familyStatusInitVal;
        } else {
            result = getFamilyStatus();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'family status'.
     */
    public boolean familyStatusIsDirty() {
        return !valuesAreEqual(familyStatusInitVal(), getFamilyStatus());
    }

    /**
     * Returns true if the setter method was called for the property 'family status'.
     */
    public boolean familyStatusIsSet() {
        return _familyStatusIsSet;
    }
	
    /**
     * Returns the initial value of the property 'pesel'.
     */
    public String peselInitVal() {
        String result;
        if (_peselIsSet) {
            result = _peselInitVal;
        } else {
            result = getPesel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'pesel'.
     */
    public boolean peselIsDirty() {
        return !valuesAreEqual(peselInitVal(), getPesel());
    }

    /**
     * Returns true if the setter method was called for the property 'pesel'.
     */
    public boolean peselIsSet() {
        return _peselIsSet;
    }
	
    /**
     * Returns the initial value of the property 'identity card number'.
     */
    public String identityCardNumberInitVal() {
        String result;
        if (_identityCardNumberIsSet) {
            result = _identityCardNumberInitVal;
        } else {
            result = getIdentityCardNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'identity card number'.
     */
    public boolean identityCardNumberIsDirty() {
        return !valuesAreEqual(identityCardNumberInitVal(), getIdentityCardNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'identity card number'.
     */
    public boolean identityCardNumberIsSet() {
        return _identityCardNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'nip'.
     */
    public String nipInitVal() {
        String result;
        if (_nipIsSet) {
            result = _nipInitVal;
        } else {
            result = getNip();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'nip'.
     */
    public boolean nipIsDirty() {
        return !valuesAreEqual(nipInitVal(), getNip());
    }

    /**
     * Returns true if the setter method was called for the property 'nip'.
     */
    public boolean nipIsSet() {
        return _nipIsSet;
    }
	
    /**
     * Returns the initial value of the property 'education earned'.
     */
    public String educationEarnedInitVal() {
        String result;
        if (_educationEarnedIsSet) {
            result = _educationEarnedInitVal;
        } else {
            result = getEducationEarned();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'education earned'.
     */
    public boolean educationEarnedIsDirty() {
        return !valuesAreEqual(educationEarnedInitVal(), getEducationEarned());
    }

    /**
     * Returns true if the setter method was called for the property 'education earned'.
     */
    public boolean educationEarnedIsSet() {
        return _educationEarnedIsSet;
    }
		
    /**
     * Returns the initial value of the property 'valid until'.
     */
    public Date validUntilInitVal() {
        Date result;
        if (_validUntilIsSet) {
            result = _validUntilInitVal;
        } else {
            result = getValidUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid until'.
     */
    public boolean validUntilIsDirty() {
        return !valuesAreEqual(validUntilInitVal(), getValidUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'valid until'.
     */
    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public int typeInitVal() {
        int result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'postident fee'.
     */
    public Date postidentFeeInitVal() {
        Date result;
        if (_postidentFeeIsSet) {
            result = _postidentFeeInitVal;
        } else {
            result = getPostidentFee();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'postident fee'.
     */
    public boolean postidentFeeIsDirty() {
        return !valuesAreEqual(postidentFeeInitVal(), getPostidentFee());
    }

    /**
     * Returns true if the setter method was called for the property 'postident fee'.
     */
    public boolean postidentFeeIsSet() {
        return _postidentFeeIsSet;
    }
				
}
