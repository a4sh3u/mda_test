package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.SchufaScoreAddress;
import de.smava.webapp.account.domain.SchufaScoreRequestData;
import de.smava.webapp.account.domain.interfaces.SchufaScoreRequestDataEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;

/**
 * The domain object that represents 'SchufaScoreRequestDatas'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractSchufaScoreRequestData
    extends KreditPrivatEntity implements SchufaScoreRequestDataEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaScoreRequestData.class);

    private static final long serialVersionUID = -7998158679444801734L;

    public void setAddress(SchufaScoreAddress address) {

        if (getAddresses() == null) {
            setAddresses(new LinkedList<SchufaScoreAddress>());
        }

        if (address.getRequestData() != this) {
            address.setRequestData(this.instance());
        }

        getAddresses().add(address);

    }

    public SchufaScoreAddress getAddress() {
        SchufaScoreAddress result = null;
        if (getAddresses() != null && !getAddresses().isEmpty()) {
            result = getAddresses().get(getAddresses().size() - 1);
        }

        return result;
    }

    public abstract SchufaScoreRequestData instance();

}

