package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.ContractEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'Contracts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractContract
    extends KreditPrivatEntity implements MarketingTrackingEntity,ContractEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractContract.class);

    public static final String STATE_NEW = "NEW";   // offer matched order
    public static final String STATE_PENDING = "PENDING"; // order is completely financed
    public static final String STATE_COMPLETE = "COMPLETE"; // contract amount is paid back in the usual way
    public static final String STATE_DELETED = "DELETED"; // either order is not financed or borrower did not subscribed the contract
    public static final String STATE_OBJECTED = "OBJECTED"; // borrower objects contract after complete financing or during payback period
    public static final String STATE_TO_BE_OBJECTED = "TO_BE_OBJECTED"; // borrower objects contract, but he still doesn't pay the money back
    public static final String STATE_REPAY = "REPAY"; // first rate is transfered successfully
    public static final String STATE_LATE = "LATE"; // borrower pays monthly rate too late or never
    public static final String STATE_DEFAULT = "DEFAULT"; // contract is send to inkasso
    public static final String STATE_CANCELED = "CANCELED"; // contract amount is paid back preterm
    public static final String STATE_TO_BE_CANCELED = "TO_BE_CANCELED"; //

    //running state from the lender point of view (defaulted contracts are not longer running for a lender, because he does not own it anymore)
    public static final Set<String> LENDER_RUNNING_CONTRACT_STATES = new HashSet<String>();

    public static final Set<String> STATE_NAMES = new HashSet<String>();

    static {
        STATE_NAMES.add(STATE_NEW);
        STATE_NAMES.add(STATE_PENDING);
        STATE_NAMES.add(STATE_COMPLETE);
        STATE_NAMES.add(STATE_DELETED);
        STATE_NAMES.add(STATE_OBJECTED);
        STATE_NAMES.add(STATE_REPAY);
        STATE_NAMES.add(STATE_LATE);
        STATE_NAMES.add(STATE_DEFAULT);
        STATE_NAMES.add(STATE_CANCELED);

        STATE_NAMES.add(STATE_TO_BE_CANCELED);
        STATE_NAMES.add(STATE_TO_BE_OBJECTED);

        LENDER_RUNNING_CONTRACT_STATES.add(STATE_REPAY);
        LENDER_RUNNING_CONTRACT_STATES.add(STATE_LATE);
    }
    public static final Collection<String> ACTIVE_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[] {STATE_PENDING, STATE_REPAY, STATE_LATE}));
    public static final Collection<String> SCHUFA_REQUEST_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[] {
            STATE_REPAY, 	// in r�ckzahlung
            STATE_LATE, 	// zahlungsr�ckstand
            STATE_TO_BE_CANCELED, // in k�ndigung (WS-3131: STATE_CANCELED)
            STATE_TO_BE_OBJECTED, // in widerspruch (WS-3131: STATE_OBJECTED)
            STATE_DEFAULT, 	// ausgefallen (inkasso)
            STATE_COMPLETE 	// beendet (erfolgreich)
    }));

    public static final Set<String> REPAY_RELEVANT_CONTRACTSTATES
            = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(Contract.STATE_REPAY, Contract.STATE_LATE, Contract.STATE_TO_BE_CANCELED, Contract.STATE_TO_BE_OBJECTED)));

    public static final Collection<String> OBJECTION_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[] {STATE_CANCELED, STATE_OBJECTED, STATE_TO_BE_CANCELED, STATE_TO_BE_OBJECTED}));

    public static final Set<String> INTERNAL_DEBT_CONSOLIDATEIN_STATES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {STATE_REPAY, STATE_TO_BE_CANCELED, STATE_LATE})));

    //updated roi per contract should be updated in this states
    public static final Set<String> UPDATED_ROI_RELEVANT_STATES
            = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(
            STATE_REPAY, STATE_LATE, STATE_CANCELED, STATE_TO_BE_CANCELED, STATE_DEFAULT)));

    //updated roi per account is based on this states
    public static final Set<String> UPDATED_ROI_LENDER_RELEVANT_STATES
            = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(
            STATE_REPAY, STATE_LATE, STATE_CANCELED, STATE_TO_BE_CANCELED, STATE_DEFAULT, STATE_COMPLETE)));

    private transient double _totalChargeAmount = 0.0;

    public Document getLastCreditAgreementDocument() {
        Document result = null;
        for (Document document : getDocuments()) {
            if (Document.DOCUMENT_TYPE_CREDIT_AGREEMENT.equals(document.getType()) && (result == null || document.getCreationDate().after(result.getCreationDate()))) {
                result = document;
            }
        }
        return result;
    }

    public void addDocument(Document document) {
        if (getDocuments() == null) {
            setDocuments(new LinkedList<Document>());
        }

        getDocuments().add(document);
    }

    public void removeDocument(Document document) {
        if (getDocuments() != null) {
            getDocuments().remove(document);
        }
    }

    public void removeBooking(Booking booking) {
        if (getBookings() != null) {
            getBookings().remove(booking);
        }
    }

    public double getAnnualPercentageInterest() {
        return getEffectiveYieldBorrower();
    }

    public double getNominalInterest() {
        return getRate();
    }


    public double getTotalChargeAmount() {
        return _totalChargeAmount;
    }

    public void setTotalChargeAmount(double totalChargeAmount) {
        _totalChargeAmount = totalChargeAmount;
    }



    /**
     * Can return null.
     * @return the credit procurement document for this contract.
     */
    public Document getCreditProcurementDocument() {
        Document result = null;
        for (Document document : getDocuments()) {
            if (Document.DOCUMENT_TYPE_CREDIT_PROCUREMENT.equals(document.getType())) {
                result = document;
                break;
            }
        }

        return result;
    }

    /*
     * Overrides super.registerChange() to block writing of history entry
     * Cannot change setter of setUpdatedRoi() method because all entity subclasses
     * are generated by apertoGenerator - meaning changes would be overwritten.
     *
     * (non-Javadoc)
     * @see de.smava.webapp.commons.domain.Entity#registerChange(java.lang.String, java.lang.Object, java.lang.Object)
     */
    @Override
    protected void registerChange(String key, Object oldValue, Object newValue) {

        if(!"updated roi".equals(key)){
            super.registerChange(key, oldValue, newValue);
        }

    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        if (getOrder() != null && getOrder().getAccount() != null) {
            list.add(getOrder().getAccount());
        }
        if (getOffer() != null && getOffer().getAccount() != null) {
            list.add(getOffer().getAccount());
        }
        return list;
    }

}

