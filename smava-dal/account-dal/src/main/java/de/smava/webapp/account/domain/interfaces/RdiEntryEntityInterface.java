package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'RdiEntrys'.
 *
 * @author generator
 */
public interface RdiEntryEntityInterface {

    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    void setKey(String key);

    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    String getKey();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();
    /**
     * Setter for the property 'error'.
     *
     * 
     *
     */
    void setError(String error);

    /**
     * Returns the property 'error'.
     *
     * 
     *
     */
    String getError();

}
