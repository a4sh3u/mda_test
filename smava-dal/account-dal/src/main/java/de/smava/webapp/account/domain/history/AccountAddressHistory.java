package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractAccountAddress;




/**
 * The domain object that has all history aggregation related fields for 'AccountAddresss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AccountAddressHistory extends AbstractAccountAddress {



	
}
