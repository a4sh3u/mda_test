//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(address)}

import de.smava.webapp.account.dao.AddressDao;
import de.smava.webapp.account.domain.Address;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Addresss'.
 *
 * @author generator
 */
@Repository(value = "addressDao")
public class JdoAddressDao extends JdoBaseDao implements AddressDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAddressDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(address)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the address identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Address load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddress - start: id=" + id);
        }
        Address result = getEntity(Address.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddress - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Address getAddress(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Address entity = findUniqueEntity(Address.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the address.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Address address) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAddress: " + address);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(address)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(address);
    }

    /**
     * @deprecated Use {@link #save(Address) instead}
     */
    public Long saveAddress(Address address) {
        return save(address);
    }

    /**
     * Deletes an address, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the address
     */
    public void deleteAddress(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAddress: " + id);
        }
        deleteEntity(Address.class, id);
    }

    /**
     * Retrieves all 'Address' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Address> getAddressList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - start");
        }
        Collection<Address> result = getEntities(Address.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Address' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Address> getAddressList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - start: pageable=" + pageable);
        }
        Collection<Address> result = getEntities(Address.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Address' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Address> getAddressList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - start: sortable=" + sortable);
        }
        Collection<Address> result = getEntities(Address.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Address' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Address> getAddressList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Address> result = getEntities(Address.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Address' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Address> findAddressList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - start: whereClause=" + whereClause);
        }
        Collection<Address> result = findEntities(Address.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Address' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Address> findAddressList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Address> result = findEntities(Address.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Address' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Address> findAddressList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Address> result = findEntities(Address.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Address' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Address> findAddressList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Address> result = findEntities(Address.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Address' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Address> findAddressList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Address> result = findEntities(Address.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Address' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Address> findAddressList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Address> result = findEntities(Address.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findAddressList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Address' instances.
     */
    public long getAddressCount() {
        long result = getEntityCount(Address.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Address' instances which match the given whereClause.
     */
    public long getAddressCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Address.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Address' instances which match the given whereClause together with params specified in object array.
     */
    public long getAddressCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Address.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getAddressCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(address)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
