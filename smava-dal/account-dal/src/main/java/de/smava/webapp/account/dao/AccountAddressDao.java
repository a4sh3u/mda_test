//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\wks\maven2\smava-webapp-parent\account\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\wks\maven2\smava-webapp-parent\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account address)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;


import de.smava.webapp.account.domain.AccountAddress;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'AccountAddresss'.
 *
 * @author generator
 */
public interface AccountAddressDao extends BaseDao<AccountAddress> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the account address identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    AccountAddress getAccountAddress(Long id);

    /**
     * Saves the account address.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveAccountAddress(AccountAddress accountAddress);

    /**
     * Deletes an account address, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account address
     */
    void deleteAccountAddress(Long id);

    /**
     * Retrieves all 'AccountAddress' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<AccountAddress> getAccountAddressList();

    /**
     * Retrieves a page of 'AccountAddress' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<AccountAddress> getAccountAddressList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'AccountAddress' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<AccountAddress> getAccountAddressList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'AccountAddress' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<AccountAddress> getAccountAddressList(Pageable pageable, Sortable sortable);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(account address)}
    /**
     * returns all addresses for the given account
     * @param accountId
     * @param pageable
     * @param sortable
     * @return
     */
    Collection<AccountAddress> getAddressList(Long accountId, Pageable pageable, Sortable sortable);
    // !!!!!!!! End of insert code section !!!!!!!!
}
