package de.smava.webapp.account.dao.jdo;

import de.smava.webapp.account.dao.HistoryEntryDao;
import de.smava.webapp.account.domain.history.HistoryEntry;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.util.ResourceBundleEnum;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * DAO implementation for the domain object 'HistoryEntrys'.
 *
 * @author generator
 */
@Repository(value = "historyEntryDao")
public class JdoHistoryEntryDao extends JdoBaseDao implements HistoryEntryDao {

    private static final Logger LOGGER = Logger.getLogger(JdoHistoryEntryDao.class);

    private ResourceBundleEnum _historyEntryTypes;

    /**
     * Returns an attached copy of the history entry identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public HistoryEntry getHistoryEntry(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntry - start: id=" + id);
        }
        HistoryEntry result = getEntity(HistoryEntry.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntry - result: " + result);
        }
        return result;
    }

    /**
     * Saves the history entry.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long saveHistoryEntry(HistoryEntry historyEntry) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveHistoryEntry: " + historyEntry);
        }
        return saveEntity(historyEntry);
    }

    /**
     * Deletes an history entry, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the history entry
     */
    public void deleteHistoryEntry(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteHistoryEntry: " + id);
        }
        deleteEntity(HistoryEntry.class, id);
    }

    /**
     * Retrieves all 'HistoryEntry' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<HistoryEntry> getHistoryEntryList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - start");
        }
        Collection<HistoryEntry> result = getEntities(HistoryEntry.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'HistoryEntry' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<HistoryEntry> getHistoryEntryList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - start: pageable=" + pageable);
        }
        Collection<HistoryEntry> result = getEntities(HistoryEntry.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'HistoryEntry' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<HistoryEntry> getHistoryEntryList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - start: sortable=" + sortable);
        }
        Collection<HistoryEntry> result = getEntities(HistoryEntry.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'HistoryEntry' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<HistoryEntry> getHistoryEntryList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<HistoryEntry> result = getEntities(HistoryEntry.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'HistoryEntry' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<HistoryEntry> findHistoryEntryList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - start: whereClause=" + whereClause);
        }
        Collection<HistoryEntry> result = findEntities(HistoryEntry.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'HistoryEntry' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<HistoryEntry> findHistoryEntryList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<HistoryEntry> result = findEntities(HistoryEntry.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'HistoryEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<HistoryEntry> findHistoryEntryList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<HistoryEntry> result = findEntities(HistoryEntry.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'HistoryEntry' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<HistoryEntry> findHistoryEntryList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<HistoryEntry> result = findEntities(HistoryEntry.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findHistoryEntryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'HistoryEntry' instances.
     */
    public long getHistoryEntryCount() {
        long result = getEntityCount(HistoryEntry.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'HistoryEntry' instances which match the given whereClause.
     */
    public long getHistoryEntryCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(HistoryEntry.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getHistoryEntryCount() - result: count=" + result);
        }
        return result;
    }

    public ResourceBundleEnum getHistoryEntryTypes() {
        return _historyEntryTypes;
    }

    public void setHistoryEntryTypes(ResourceBundleEnum historyEntryTypes) {
        _historyEntryTypes = historyEntryTypes;
    }

    public Collection<HistoryEntry> findHistoryEntryListForAccountId(Long accountId, Pageable pageable, Sortable sortable) {
        return findHistoryEntryList("_accountId == " + accountId, pageable, sortable);
    }

    public long countEntryListForAccountId(Long accountId) {
        return getHistoryEntryCount("_accountId == " + accountId);
    }
}