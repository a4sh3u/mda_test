package de.smava.webapp.account.exception;

import de.smava.webapp.commons.exception.RemoteServiceException;

/**
 * Base class for all schufa related errors.
 * @author aherr
 *
 */
public abstract class SchufaException extends RemoteServiceException {

}
