//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(id card)}

import de.smava.webapp.account.domain.IdCard;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'IdCards'.
 *
 * @author generator
 */
public interface IdCardDao extends BaseDao<IdCard>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the id card identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    IdCard getIdCard(Long id);

    /**
     * Saves the id card.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveIdCard(IdCard idCard);

    /**
     * Deletes an id card, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the id card
     */
    void deleteIdCard(Long id);

    /**
     * Retrieves all 'IdCard' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<IdCard> getIdCardList();

    /**
     * Retrieves a page of 'IdCard' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<IdCard> getIdCardList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'IdCard' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<IdCard> getIdCardList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'IdCard' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<IdCard> getIdCardList(Pageable pageable, Sortable sortable);




    /**
     * Returns the number of 'IdCard' instances.
     */
    long getIdCardCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(id card)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
