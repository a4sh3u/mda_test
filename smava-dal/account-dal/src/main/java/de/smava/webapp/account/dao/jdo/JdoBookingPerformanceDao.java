//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking performance)}

import de.smava.webapp.account.dao.BookingPerformanceDao;
import de.smava.webapp.account.domain.BookingPerformance;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'BookingPerformances'.
 *
 * @author generator
 */
@Repository(value = "bookingPerformanceDao")
public class JdoBookingPerformanceDao extends JdoBaseDao implements BookingPerformanceDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBookingPerformanceDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(booking performance)}
    private static final long serialVersionUID = 3928006408474611480L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the booking performance identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BookingPerformance load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformance - start: id=" + id);
        }
        BookingPerformance result = getEntity(BookingPerformance.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformance - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BookingPerformance getBookingPerformance(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BookingPerformance entity = findUniqueEntity(BookingPerformance.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the booking performance.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BookingPerformance bookingPerformance) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBookingPerformance: " + bookingPerformance);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(booking performance)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bookingPerformance);
    }

    /**
     * @deprecated Use {@link #save(BookingPerformance) instead}
     */
    public Long saveBookingPerformance(BookingPerformance bookingPerformance) {
        return save(bookingPerformance);
    }

    /**
     * Deletes an booking performance, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the booking performance
     */
    public void deleteBookingPerformance(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBookingPerformance: " + id);
        }
        deleteEntity(BookingPerformance.class, id);
    }

    /**
     * Retrieves all 'BookingPerformance' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BookingPerformance> getBookingPerformanceList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - start");
        }
        Collection<BookingPerformance> result = getEntities(BookingPerformance.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BookingPerformance' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BookingPerformance> getBookingPerformanceList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - start: pageable=" + pageable);
        }
        Collection<BookingPerformance> result = getEntities(BookingPerformance.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BookingPerformance' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BookingPerformance> getBookingPerformanceList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - start: sortable=" + sortable);
        }
        Collection<BookingPerformance> result = getEntities(BookingPerformance.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingPerformance' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BookingPerformance> getBookingPerformanceList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingPerformance> result = getEntities(BookingPerformance.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BookingPerformance' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingPerformance> findBookingPerformanceList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - start: whereClause=" + whereClause);
        }
        Collection<BookingPerformance> result = findEntities(BookingPerformance.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BookingPerformance' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BookingPerformance> findBookingPerformanceList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BookingPerformance> result = findEntities(BookingPerformance.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BookingPerformance' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingPerformance> findBookingPerformanceList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BookingPerformance> result = findEntities(BookingPerformance.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BookingPerformance' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingPerformance> findBookingPerformanceList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BookingPerformance> result = findEntities(BookingPerformance.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingPerformance' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingPerformance> findBookingPerformanceList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingPerformance> result = findEntities(BookingPerformance.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BookingPerformance' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BookingPerformance> findBookingPerformanceList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BookingPerformance> result = findEntities(BookingPerformance.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBookingPerformanceList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BookingPerformance' instances.
     */
    public long getBookingPerformanceCount() {
        long result = getEntityCount(BookingPerformance.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BookingPerformance' instances which match the given whereClause.
     */
    public long getBookingPerformanceCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BookingPerformance.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BookingPerformance' instances which match the given whereClause together with params specified in object array.
     */
    public long getBookingPerformanceCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BookingPerformance.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBookingPerformanceCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(booking performance)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
