package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.AttachmentHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'Attachments'.
 */
public class Attachment extends AttachmentHistory  {

        protected Date _creationDate;
        protected Long _attachmentId;
        protected String _name;
        protected String _description;
        protected String _contentType;
        protected String _location;
        protected de.smava.webapp.account.domain.Document _document;
        protected Collection<de.smava.webapp.account.domain.DocumentAttachmentContainer> _documentAttachmentContainer;

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'attachment id'.
     */
    public void setAttachmentId(Long attachmentId) {
        _attachmentId = attachmentId;
    }
            
    /**
     * Returns the property 'attachment id'.
     */
    public Long getAttachmentId() {
        return _attachmentId;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'content type'.
     */
    public void setContentType(String contentType) {
        if (!_contentTypeIsSet) {
            _contentTypeIsSet = true;
            _contentTypeInitVal = getContentType();
        }
        registerChange("content type", _contentTypeInitVal, contentType);
        _contentType = contentType;
    }
                        
    /**
     * Returns the property 'content type'.
     */
    public String getContentType() {
        return _contentType;
    }
                                    /**
     * Setter for the property 'location'.
     */
    public void setLocation(String location) {
        if (!_locationIsSet) {
            _locationIsSet = true;
            _locationInitVal = getLocation();
        }
        registerChange("location", _locationInitVal, location);
        _location = location;
    }
                        
    /**
     * Returns the property 'location'.
     */
    public String getLocation() {
        return _location;
    }
                                            
    /**
     * Setter for the property 'document'.
     */
    public void setDocument(de.smava.webapp.account.domain.Document document) {
        _document = document;
    }
            
    /**
     * Returns the property 'document'.
     */
    public de.smava.webapp.account.domain.Document getDocument() {
        return _document;
    }
                                            
    /**
     * Setter for the property 'document attachment container'.
     */
    public void setDocumentAttachmentContainer(Collection<de.smava.webapp.account.domain.DocumentAttachmentContainer> documentAttachmentContainer) {
        _documentAttachmentContainer = documentAttachmentContainer;
    }
            
    /**
     * Returns the property 'document attachment container'.
     */
    public Collection<de.smava.webapp.account.domain.DocumentAttachmentContainer> getDocumentAttachmentContainer() {
        return _documentAttachmentContainer;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Attachment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _contentType=").append(_contentType);
            builder.append("\n    _location=").append(_location);
            builder.append("\n}");
        } else {
            builder.append(Attachment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
