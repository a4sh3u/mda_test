package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.ApprovalHistory;

import java.util.Date;

/**
 * The domain object that represents 'Approvals'.
 */
public class Approval extends ApprovalHistory  implements DateComparable {

        protected AccountRole _accountRole;
        protected Account _approver;
        protected Date _approvalDate;
        protected String _comment;
        protected int _vote;
        protected RepaymentBreak _repaymentBreak;

    /**
     * Setter for the property 'account role'.
     */
    public void setAccountRole(AccountRole accountRole) {
        _accountRole = accountRole;
    }
            
    /**
     * Returns the property 'account role'.
     */
    public AccountRole getAccountRole() {
        return _accountRole;
    }
                                            
    /**
     * Setter for the property 'approver'.
     */
    public void setApprover(Account approver) {
        _approver = approver;
    }
            
    /**
     * Returns the property 'approver'.
     */
    public Account getApprover() {
        return _approver;
    }
                                    /**
     * Setter for the property 'approval date'.
     */
    public void setApprovalDate(Date approvalDate) {
        if (!_approvalDateIsSet) {
            _approvalDateIsSet = true;
            _approvalDateInitVal = getApprovalDate();
        }
        registerChange("approval date", _approvalDateInitVal, approvalDate);
        _approvalDate = approvalDate;
    }
                        
    /**
     * Returns the property 'approval date'.
     */
    public Date getApprovalDate() {
        return _approvalDate;
    }
                                    /**
     * Setter for the property 'comment'.
     */
    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = getComment();
        }
        registerChange("comment", _commentInitVal, comment);
        _comment = comment;
    }
                        
    /**
     * Returns the property 'comment'.
     */
    public String getComment() {
        return _comment;
    }
                                    /**
     * Setter for the property 'vote'.
     */
    public void setVote(int vote) {
        if (!_voteIsSet) {
            _voteIsSet = true;
            _voteInitVal = getVote();
        }
        registerChange("vote", _voteInitVal, vote);
        _vote = vote;
    }
                        
    /**
     * Returns the property 'vote'.
     */
    public int getVote() {
        return _vote;
    }
                                            
    /**
     * Setter for the property 'repayment break'.
     */
    public void setRepaymentBreak(RepaymentBreak repaymentBreak) {
        _repaymentBreak = repaymentBreak;
    }
            
    /**
     * Returns the property 'repayment break'.
     */
    public RepaymentBreak getRepaymentBreak() {
        return _repaymentBreak;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Approval.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _comment=").append(_comment);
            builder.append("\n    _vote=").append(_vote);
            builder.append("\n}");
        } else {
            builder.append(Approval.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
