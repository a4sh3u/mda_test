package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.interfaces.CreditScoreEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The domain object that represents 'CreditScores'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractCreditScore
    extends KreditPrivatEntity implements CreditScoreEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCreditScore.class);

    public static final String MAIN_BORROWER = "MAIN_BORROWER";
    public static final String CO_BORROWER = "CO_BORROWER";

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        if (getAccount() != null) {
            list.add(getAccount());
        }
        return list;
    }

}

