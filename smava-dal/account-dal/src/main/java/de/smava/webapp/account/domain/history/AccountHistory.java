package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractAccount;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Accounts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AccountHistory extends AbstractAccount {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient Date _lockDateInitVal;
    protected transient boolean _lockDateIsSet;
    protected transient String _usernameInitVal;
    protected transient boolean _usernameIsSet;
    protected transient String _emailInitVal;
    protected transient boolean _emailIsSet;
    protected transient String _phoneInitVal;
    protected transient boolean _phoneIsSet;
    protected transient String _phoneCodeInitVal;
    protected transient boolean _phoneCodeIsSet;
    protected transient String _phone2InitVal;
    protected transient boolean _phone2IsSet;
    protected transient String _phone2CodeInitVal;
    protected transient boolean _phone2CodeIsSet;
    protected transient Date _assetManagerGuidelineDateInitVal;
    protected transient boolean _assetManagerGuidelineDateIsSet;
    protected transient Date _acceptanceDateInitVal;
    protected transient boolean _acceptanceDateIsSet;
    protected transient Date _lastLoginInitVal;
    protected transient boolean _lastLoginIsSet;
    protected transient boolean _newsletterSubscriptionInitVal;
    protected transient boolean _newsletterSubscriptionIsSet;
    protected transient Long _marketingPlacementIdInitVal;
    protected transient boolean _marketingPlacementIdIsSet;
    protected transient String _preferredRoleInitVal;
    protected transient boolean _preferredRoleIsSet;
    protected transient String _externalReferenceIdInitVal;
    protected transient boolean _externalReferenceIdIsSet;
    protected transient Date _paymentProfilShowDateInitVal;
    protected transient boolean _paymentProfilShowDateIsSet;
    protected transient double _averageUpdatedRoiInitVal;
    protected transient boolean _averageUpdatedRoiIsSet;
    protected transient boolean _isManagedInitVal;
    protected transient boolean _isManagedIsSet;
    protected transient String _activationStringInitVal;
    protected transient boolean _activationStringIsSet;
    protected transient String _marketingTestInitVal;
    protected transient boolean _marketingTestIsSet;
    protected transient Date _callAgreementDateInitVal;
    protected transient boolean _callAgreementDateIsSet;
    protected transient String _phoneEmployerInitVal;
    protected transient boolean _phoneEmployerIsSet;
    protected transient String _phoneCodeEmployerInitVal;
    protected transient boolean _phoneCodeEmployerIsSet;
    protected transient String _phoneEmployerCoBorrowerInitVal;
    protected transient boolean _phoneEmployerCoBorrowerIsSet;
    protected transient String _phoneCodeEmployerCoBorrowerInitVal;
    protected transient boolean _phoneCodeEmployerCoBorrowerIsSet;
    protected transient Date _lastFrontendChangesInitVal;
    protected transient boolean _lastFrontendChangesIsSet;
    protected transient Date _emailValidatedDateInitVal;
    protected transient boolean _emailValidatedDateIsSet;
    protected transient String _payoutModeInitVal;
    protected transient boolean _payoutModeIsSet;
    protected transient Date _papLeadTrackedInitVal;
    protected transient boolean _papLeadTrackedIsSet;
    protected transient Date _receiveLoanOffersInitVal;
    protected transient boolean _receiveLoanOffersIsSet;
    protected transient Date _receiveConsolidationOffersInitVal;
    protected transient boolean _receiveConsolidationOffersIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'lock date'.
     */
    public Date lockDateInitVal() {
        Date result;
        if (_lockDateIsSet) {
            result = _lockDateInitVal;
        } else {
            result = getLockDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'lock date'.
     */
    public boolean lockDateIsDirty() {
        return !valuesAreEqual(lockDateInitVal(), getLockDate());
    }

    /**
     * Returns true if the setter method was called for the property 'lock date'.
     */
    public boolean lockDateIsSet() {
        return _lockDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'username'.
     */
    public String usernameInitVal() {
        String result;
        if (_usernameIsSet) {
            result = _usernameInitVal;
        } else {
            result = getUsername();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'username'.
     */
    public boolean usernameIsDirty() {
        return !valuesAreEqual(usernameInitVal(), getUsername());
    }

    /**
     * Returns true if the setter method was called for the property 'username'.
     */
    public boolean usernameIsSet() {
        return _usernameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone'.
     */
    public String phoneInitVal() {
        String result;
        if (_phoneIsSet) {
            result = _phoneInitVal;
        } else {
            result = getPhone();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone'.
     */
    public boolean phoneIsDirty() {
        return !valuesAreEqual(phoneInitVal(), getPhone());
    }

    /**
     * Returns true if the setter method was called for the property 'phone'.
     */
    public boolean phoneIsSet() {
        return _phoneIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone code'.
     */
    public String phoneCodeInitVal() {
        String result;
        if (_phoneCodeIsSet) {
            result = _phoneCodeInitVal;
        } else {
            result = getPhoneCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone code'.
     */
    public boolean phoneCodeIsDirty() {
        return !valuesAreEqual(phoneCodeInitVal(), getPhoneCode());
    }

    /**
     * Returns true if the setter method was called for the property 'phone code'.
     */
    public boolean phoneCodeIsSet() {
        return _phoneCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone2'.
     */
    public String phone2InitVal() {
        String result;
        if (_phone2IsSet) {
            result = _phone2InitVal;
        } else {
            result = getPhone2();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone2'.
     */
    public boolean phone2IsDirty() {
        return !valuesAreEqual(phone2InitVal(), getPhone2());
    }

    /**
     * Returns true if the setter method was called for the property 'phone2'.
     */
    public boolean phone2IsSet() {
        return _phone2IsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone2 code'.
     */
    public String phone2CodeInitVal() {
        String result;
        if (_phone2CodeIsSet) {
            result = _phone2CodeInitVal;
        } else {
            result = getPhone2Code();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone2 code'.
     */
    public boolean phone2CodeIsDirty() {
        return !valuesAreEqual(phone2CodeInitVal(), getPhone2Code());
    }

    /**
     * Returns true if the setter method was called for the property 'phone2 code'.
     */
    public boolean phone2CodeIsSet() {
        return _phone2CodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'asset manager guideline date'.
     */
    public Date assetManagerGuidelineDateInitVal() {
        Date result;
        if (_assetManagerGuidelineDateIsSet) {
            result = _assetManagerGuidelineDateInitVal;
        } else {
            result = getAssetManagerGuidelineDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'asset manager guideline date'.
     */
    public boolean assetManagerGuidelineDateIsDirty() {
        return !valuesAreEqual(assetManagerGuidelineDateInitVal(), getAssetManagerGuidelineDate());
    }

    /**
     * Returns true if the setter method was called for the property 'asset manager guideline date'.
     */
    public boolean assetManagerGuidelineDateIsSet() {
        return _assetManagerGuidelineDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'acceptance date'.
     */
    public Date acceptanceDateInitVal() {
        Date result;
        if (_acceptanceDateIsSet) {
            result = _acceptanceDateInitVal;
        } else {
            result = getAcceptanceDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'acceptance date'.
     */
    public boolean acceptanceDateIsDirty() {
        return !valuesAreEqual(acceptanceDateInitVal(), getAcceptanceDate());
    }

    /**
     * Returns true if the setter method was called for the property 'acceptance date'.
     */
    public boolean acceptanceDateIsSet() {
        return _acceptanceDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last login'.
     */
    public Date lastLoginInitVal() {
        Date result;
        if (_lastLoginIsSet) {
            result = _lastLoginInitVal;
        } else {
            result = getLastLogin();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last login'.
     */
    public boolean lastLoginIsDirty() {
        return !valuesAreEqual(lastLoginInitVal(), getLastLogin());
    }

    /**
     * Returns true if the setter method was called for the property 'last login'.
     */
    public boolean lastLoginIsSet() {
        return _lastLoginIsSet;
    }
	
    /**
     * Returns the initial value of the property 'newsletter subscription'.
     */
    public boolean newsletterSubscriptionInitVal() {
        boolean result;
        if (_newsletterSubscriptionIsSet) {
            result = _newsletterSubscriptionInitVal;
        } else {
            result = getNewsletterSubscription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'newsletter subscription'.
     */
    public boolean newsletterSubscriptionIsDirty() {
        return !valuesAreEqual(newsletterSubscriptionInitVal(), getNewsletterSubscription());
    }

    /**
     * Returns true if the setter method was called for the property 'newsletter subscription'.
     */
    public boolean newsletterSubscriptionIsSet() {
        return _newsletterSubscriptionIsSet;
    }
															
    /**
     * Returns the initial value of the property 'marketing placement id'.
     */
    public Long marketingPlacementIdInitVal() {
        Long result;
        if (_marketingPlacementIdIsSet) {
            result = _marketingPlacementIdInitVal;
        } else {
            result = getMarketingPlacementId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'marketing placement id'.
     */
    public boolean marketingPlacementIdIsDirty() {
        return !valuesAreEqual(marketingPlacementIdInitVal(), getMarketingPlacementId());
    }

    /**
     * Returns true if the setter method was called for the property 'marketing placement id'.
     */
    public boolean marketingPlacementIdIsSet() {
        return _marketingPlacementIdIsSet;
    }
				
    /**
     * Returns the initial value of the property 'preferred role'.
     */
    public String preferredRoleInitVal() {
        String result;
        if (_preferredRoleIsSet) {
            result = _preferredRoleInitVal;
        } else {
            result = getPreferredRole();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'preferred role'.
     */
    public boolean preferredRoleIsDirty() {
        return !valuesAreEqual(preferredRoleInitVal(), getPreferredRole());
    }

    /**
     * Returns true if the setter method was called for the property 'preferred role'.
     */
    public boolean preferredRoleIsSet() {
        return _preferredRoleIsSet;
    }
			
    /**
     * Returns the initial value of the property 'external reference id'.
     */
    public String externalReferenceIdInitVal() {
        String result;
        if (_externalReferenceIdIsSet) {
            result = _externalReferenceIdInitVal;
        } else {
            result = getExternalReferenceId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external reference id'.
     */
    public boolean externalReferenceIdIsDirty() {
        return !valuesAreEqual(externalReferenceIdInitVal(), getExternalReferenceId());
    }

    /**
     * Returns true if the setter method was called for the property 'external reference id'.
     */
    public boolean externalReferenceIdIsSet() {
        return _externalReferenceIdIsSet;
    }
				
    /**
     * Returns the initial value of the property 'payment profil show date'.
     */
    public Date paymentProfilShowDateInitVal() {
        Date result;
        if (_paymentProfilShowDateIsSet) {
            result = _paymentProfilShowDateInitVal;
        } else {
            result = getPaymentProfilShowDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'payment profil show date'.
     */
    public boolean paymentProfilShowDateIsDirty() {
        return !valuesAreEqual(paymentProfilShowDateInitVal(), getPaymentProfilShowDate());
    }

    /**
     * Returns true if the setter method was called for the property 'payment profil show date'.
     */
    public boolean paymentProfilShowDateIsSet() {
        return _paymentProfilShowDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'average updated roi'.
     */
    public double averageUpdatedRoiInitVal() {
        double result;
        if (_averageUpdatedRoiIsSet) {
            result = _averageUpdatedRoiInitVal;
        } else {
            result = getAverageUpdatedRoi();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'average updated roi'.
     */
    public boolean averageUpdatedRoiIsDirty() {
        return !valuesAreEqual(averageUpdatedRoiInitVal(), getAverageUpdatedRoi());
    }

    /**
     * Returns true if the setter method was called for the property 'average updated roi'.
     */
    public boolean averageUpdatedRoiIsSet() {
        return _averageUpdatedRoiIsSet;
    }
	
    /**
     * Returns the initial value of the property 'is managed'.
     */
    public boolean isManagedInitVal() {
        boolean result;
        if (_isManagedIsSet) {
            result = _isManagedInitVal;
        } else {
            result = getIsManaged();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'is managed'.
     */
    public boolean isManagedIsDirty() {
        return !valuesAreEqual(isManagedInitVal(), getIsManaged());
    }

    /**
     * Returns true if the setter method was called for the property 'is managed'.
     */
    public boolean isManagedIsSet() {
        return _isManagedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'activation string'.
     */
    public String activationStringInitVal() {
        String result;
        if (_activationStringIsSet) {
            result = _activationStringInitVal;
        } else {
            result = getActivationString();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'activation string'.
     */
    public boolean activationStringIsDirty() {
        return !valuesAreEqual(activationStringInitVal(), getActivationString());
    }

    /**
     * Returns true if the setter method was called for the property 'activation string'.
     */
    public boolean activationStringIsSet() {
        return _activationStringIsSet;
    }
	
    /**
     * Returns the initial value of the property 'marketing test'.
     */
    public String marketingTestInitVal() {
        String result;
        if (_marketingTestIsSet) {
            result = _marketingTestInitVal;
        } else {
            result = getMarketingTest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'marketing test'.
     */
    public boolean marketingTestIsDirty() {
        return !valuesAreEqual(marketingTestInitVal(), getMarketingTest());
    }

    /**
     * Returns true if the setter method was called for the property 'marketing test'.
     */
    public boolean marketingTestIsSet() {
        return _marketingTestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'call agreement date'.
     */
    public Date callAgreementDateInitVal() {
        Date result;
        if (_callAgreementDateIsSet) {
            result = _callAgreementDateInitVal;
        } else {
            result = getCallAgreementDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'call agreement date'.
     */
    public boolean callAgreementDateIsDirty() {
        return !valuesAreEqual(callAgreementDateInitVal(), getCallAgreementDate());
    }

    /**
     * Returns true if the setter method was called for the property 'call agreement date'.
     */
    public boolean callAgreementDateIsSet() {
        return _callAgreementDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'phone employer'.
     */
    public String phoneEmployerInitVal() {
        String result;
        if (_phoneEmployerIsSet) {
            result = _phoneEmployerInitVal;
        } else {
            result = getPhoneEmployer();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone employer'.
     */
    public boolean phoneEmployerIsDirty() {
        return !valuesAreEqual(phoneEmployerInitVal(), getPhoneEmployer());
    }

    /**
     * Returns true if the setter method was called for the property 'phone employer'.
     */
    public boolean phoneEmployerIsSet() {
        return _phoneEmployerIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone code employer'.
     */
    public String phoneCodeEmployerInitVal() {
        String result;
        if (_phoneCodeEmployerIsSet) {
            result = _phoneCodeEmployerInitVal;
        } else {
            result = getPhoneCodeEmployer();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone code employer'.
     */
    public boolean phoneCodeEmployerIsDirty() {
        return !valuesAreEqual(phoneCodeEmployerInitVal(), getPhoneCodeEmployer());
    }

    /**
     * Returns true if the setter method was called for the property 'phone code employer'.
     */
    public boolean phoneCodeEmployerIsSet() {
        return _phoneCodeEmployerIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone employer co borrower'.
     */
    public String phoneEmployerCoBorrowerInitVal() {
        String result;
        if (_phoneEmployerCoBorrowerIsSet) {
            result = _phoneEmployerCoBorrowerInitVal;
        } else {
            result = getPhoneEmployerCoBorrower();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone employer co borrower'.
     */
    public boolean phoneEmployerCoBorrowerIsDirty() {
        return !valuesAreEqual(phoneEmployerCoBorrowerInitVal(), getPhoneEmployerCoBorrower());
    }

    /**
     * Returns true if the setter method was called for the property 'phone employer co borrower'.
     */
    public boolean phoneEmployerCoBorrowerIsSet() {
        return _phoneEmployerCoBorrowerIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone code employer co borrower'.
     */
    public String phoneCodeEmployerCoBorrowerInitVal() {
        String result;
        if (_phoneCodeEmployerCoBorrowerIsSet) {
            result = _phoneCodeEmployerCoBorrowerInitVal;
        } else {
            result = getPhoneCodeEmployerCoBorrower();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone code employer co borrower'.
     */
    public boolean phoneCodeEmployerCoBorrowerIsDirty() {
        return !valuesAreEqual(phoneCodeEmployerCoBorrowerInitVal(), getPhoneCodeEmployerCoBorrower());
    }

    /**
     * Returns true if the setter method was called for the property 'phone code employer co borrower'.
     */
    public boolean phoneCodeEmployerCoBorrowerIsSet() {
        return _phoneCodeEmployerCoBorrowerIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last frontend changes'.
     */
    public Date lastFrontendChangesInitVal() {
        Date result;
        if (_lastFrontendChangesIsSet) {
            result = _lastFrontendChangesInitVal;
        } else {
            result = getLastFrontendChanges();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last frontend changes'.
     */
    public boolean lastFrontendChangesIsDirty() {
        return !valuesAreEqual(lastFrontendChangesInitVal(), getLastFrontendChanges());
    }

    /**
     * Returns true if the setter method was called for the property 'last frontend changes'.
     */
    public boolean lastFrontendChangesIsSet() {
        return _lastFrontendChangesIsSet;
    }
			
    /**
     * Returns the initial value of the property 'email validated date'.
     */
    public Date emailValidatedDateInitVal() {
        Date result;
        if (_emailValidatedDateIsSet) {
            result = _emailValidatedDateInitVal;
        } else {
            result = getEmailValidatedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email validated date'.
     */
    public boolean emailValidatedDateIsDirty() {
        return !valuesAreEqual(emailValidatedDateInitVal(), getEmailValidatedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'email validated date'.
     */
    public boolean emailValidatedDateIsSet() {
        return _emailValidatedDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'payout mode'.
     */
    public String payoutModeInitVal() {
        String result;
        if (_payoutModeIsSet) {
            result = _payoutModeInitVal;
        } else {
            result = getPayoutMode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'payout mode'.
     */
    public boolean payoutModeIsDirty() {
        return !valuesAreEqual(payoutModeInitVal(), getPayoutMode());
    }

    /**
     * Returns true if the setter method was called for the property 'payout mode'.
     */
    public boolean payoutModeIsSet() {
        return _payoutModeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'pap lead tracked'.
     */
    public Date papLeadTrackedInitVal() {
        Date result;
        if (_papLeadTrackedIsSet) {
            result = _papLeadTrackedInitVal;
        } else {
            result = getPapLeadTracked();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'pap lead tracked'.
     */
    public boolean papLeadTrackedIsDirty() {
        return !valuesAreEqual(papLeadTrackedInitVal(), getPapLeadTracked());
    }

    /**
     * Returns true if the setter method was called for the property 'pap lead tracked'.
     */
    public boolean papLeadTrackedIsSet() {
        return _papLeadTrackedIsSet;
    }
		
    /**
     * Returns the initial value of the property 'receive loan offers'.
     */
    public Date receiveLoanOffersInitVal() {
        Date result;
        if (_receiveLoanOffersIsSet) {
            result = _receiveLoanOffersInitVal;
        } else {
            result = getReceiveLoanOffers();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'receive loan offers'.
     */
    public boolean receiveLoanOffersIsDirty() {
        return !valuesAreEqual(receiveLoanOffersInitVal(), getReceiveLoanOffers());
    }

    /**
     * Returns true if the setter method was called for the property 'receive loan offers'.
     */
    public boolean receiveLoanOffersIsSet() {
        return _receiveLoanOffersIsSet;
    }
	
    /**
     * Returns the initial value of the property 'receive consolidation offers'.
     */
    public Date receiveConsolidationOffersInitVal() {
        Date result;
        if (_receiveConsolidationOffersIsSet) {
            result = _receiveConsolidationOffersInitVal;
        } else {
            result = getReceiveConsolidationOffers();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'receive consolidation offers'.
     */
    public boolean receiveConsolidationOffersIsDirty() {
        return !valuesAreEqual(receiveConsolidationOffersInitVal(), getReceiveConsolidationOffers());
    }

    /**
     * Returns true if the setter method was called for the property 'receive consolidation offers'.
     */
    public boolean receiveConsolidationOffersIsSet() {
        return _receiveConsolidationOffersIsSet;
    }

}
