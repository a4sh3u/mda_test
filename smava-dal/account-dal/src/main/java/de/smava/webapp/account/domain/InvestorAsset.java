package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.InvestorAssetHistory;

import java.util.*;

/**
 * The domain object that represents 'InvestorAssets'.
 */
public class InvestorAsset extends InvestorAssetHistory  {

        protected double _amount;
        protected Date _creationDate;
        protected boolean _active;
        protected Account _account;

                            /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'active'.
     *
     * 
     *
     */
    public void setActive(boolean active) {
        if (!_activeIsSet) {
            _activeIsSet = true;
            _activeInitVal = getActive();
        }
        registerChange("active", _activeInitVal, active);
        _active = active;
    }
                        
    /**
     * Returns the property 'active'.
     *
     * 
     *
     */
    public boolean getActive() {
        return _active;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public Account getAccount() {
        return _account;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(InvestorAsset.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _active=").append(_active);
            builder.append("\n}");
        } else {
            builder.append(InvestorAsset.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
