//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(config value)}

import de.smava.webapp.account.dao.ConfigValueDao;
import de.smava.webapp.account.domain.ConfigValue;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

/**
 * DAO implementation for the domain object 'ConfigValues'.
 *
 * @author generator
 */
@Repository(value = "configValueDao")
public class JdoConfigValueDao extends JdoBaseDao implements ConfigValueDao {

    private static final Logger LOGGER = Logger.getLogger(JdoConfigValueDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(config value)}
        //
        // insert custom fields here
        //
    
    
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the config value identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public ConfigValue load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValue - start: id=" + id);
        }
        ConfigValue result = getEntity(ConfigValue.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValue - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public ConfigValue getConfigValue(Long id) {
        return load(id);
    }

    /**
     * Saves the config value.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(ConfigValue configValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveConfigValue: " + configValue);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(config value)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(configValue);
    }

    /**
     * @deprecated Use {@link #save(ConfigValue) instead}
     */
    public Long saveConfigValue(ConfigValue configValue) {
        return save(configValue);
    }

    /**
     * Deletes an config value, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the config value
     */
    public void deleteConfigValue(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteConfigValue: " + id);
        }
        deleteEntity(ConfigValue.class, id);
    }

    /**
     * Retrieves all 'ConfigValue' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<ConfigValue> getConfigValueList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - start");
        }
        Collection<ConfigValue> result = getEntities(ConfigValue.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'ConfigValue' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<ConfigValue> getConfigValueList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - start: pageable=" + pageable);
        }
        Collection<ConfigValue> result = getEntities(ConfigValue.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ConfigValue' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<ConfigValue> getConfigValueList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - start: sortable=" + sortable);
        }
        Collection<ConfigValue> result = getEntities(ConfigValue.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ConfigValue' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<ConfigValue> getConfigValueList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<ConfigValue> result = getEntities(ConfigValue.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ConfigValue' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ConfigValue> findConfigValueList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - start: whereClause=" + whereClause);
        }
        Collection<ConfigValue> result = findEntities(ConfigValue.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ConfigValue' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<ConfigValue> findConfigValueList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<ConfigValue> result = findEntities(ConfigValue.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'ConfigValue' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ConfigValue> findConfigValueList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<ConfigValue> result = findEntities(ConfigValue.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ConfigValue' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ConfigValue> findConfigValueList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<ConfigValue> result = findEntities(ConfigValue.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ConfigValue' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ConfigValue> findConfigValueList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<ConfigValue> result = findEntities(ConfigValue.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ConfigValue' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ConfigValue> findConfigValueList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<ConfigValue> result = findEntities(ConfigValue.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findConfigValueList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'ConfigValue' instances.
     */
    public long getConfigValueCount() {
        long result = getEntityCount(ConfigValue.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ConfigValue' instances which match the given whereClause.
     */
    public long getConfigValueCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(ConfigValue.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ConfigValue' instances which match the given whereClause together with params specified in object array.
     */
    public long getConfigValueCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(ConfigValue.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(config value)}
    //
    // insert custom methods here
    //
    public ConfigValue getConfigValueByKey(String key) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueByKey() - start: key=" + key);
        }
        ConfigValue result = findUniqueEntity(ConfigValue.class, OqlTerm.newTerm().equals("_key", key).toString());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getConfigValueByKey() - result: " + result);
        }
        return result;
    }

    public Collection<ConfigValue> getConfigValuesByGroup(String group) {
        return findConfigValueList("_group == '" + group + "'");
    }
    // !!!!!!!! End of insert code section !!!!!!!!

	public boolean exists(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
}
