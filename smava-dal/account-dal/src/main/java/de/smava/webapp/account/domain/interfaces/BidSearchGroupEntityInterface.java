package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.BidSearch;

import java.util.Collection;


/**
 * The domain object that represents 'BidSearchGroups'.
 *
 * @author generator
 */
public interface BidSearchGroupEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'bid searches'.
     *
     * 
     *
     */
    void setBidSearches(Collection<BidSearch> bidSearches);

    /**
     * Returns the property 'bid searches'.
     *
     * 
     *
     */
    Collection<BidSearch> getBidSearches();

}
