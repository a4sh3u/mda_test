package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Rating;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BorrowerPromotions'.
 *
 * @author generator
 */
public interface BorrowerPromotionEntityInterface {

    /**
     * Setter for the property 'min amount'.
     *
     * 
     *
     */
    void setMinAmount(Double minAmount);

    /**
     * Returns the property 'min amount'.
     *
     * 
     *
     */
    Double getMinAmount();
    /**
     * Setter for the property 'max amount'.
     *
     * 
     *
     */
    void setMaxAmount(Double maxAmount);

    /**
     * Returns the property 'max amount'.
     *
     * 
     *
     */
    Double getMaxAmount();
    /**
     * Setter for the property 'interest'.
     *
     * 
     *
     */
    void setInterest(Float interest);

    /**
     * Returns the property 'interest'.
     *
     * 
     *
     */
    Float getInterest();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(String duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    String getDuration();
    /**
     * Setter for the property 'min score'.
     *
     * 
     *
     */
    void setMinScore(Integer minScore);

    /**
     * Returns the property 'min score'.
     *
     * 
     *
     */
    Integer getMinScore();
    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    void setStartDate(Date startDate);

    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    Date getStartDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'summatePromotionAmounts'.
     *
     * 
     *
     */
    void setSummatePromotionAmounts(Boolean summatePromotionAmounts);

    /**
     * Returns the property 'summatePromotionAmounts'.
     *
     * 
     *
     */
    Boolean getSummatePromotionAmounts();
    /**
     * Setter for the property 'ratings'.
     *
     * 
     *
     */
    void setRatings(Set<Rating> ratings);

    /**
     * Returns the property 'ratings'.
     *
     * 
     *
     */
    Set<Rating> getRatings();

}
