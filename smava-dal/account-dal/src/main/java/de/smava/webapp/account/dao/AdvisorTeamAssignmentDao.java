package de.smava.webapp.account.dao;

import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.account.domain.AdvisorTeamAssignment;

/**
 * DAO interface for the domain object 'AdvisorTeamAssignments'.
 */
public interface AdvisorTeamAssignmentDao extends BaseDao<AdvisorTeamAssignment> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Deletes an advisor team assignment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the advisor team assignment
     */
    void deleteAdvisorTeamAssignment(Long id);

    /**
     * Retrieves all 'AdvisorTeamAssignment' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<AdvisorTeamAssignment> getAdvisorTeamAssignmentList();

    /**
     * Retrieves a page of 'AdvisorTeamAssignment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<AdvisorTeamAssignment> getAdvisorTeamAssignmentList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'AdvisorTeamAssignment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<AdvisorTeamAssignment> getAdvisorTeamAssignmentList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'AdvisorTeamAssignment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<AdvisorTeamAssignment> getAdvisorTeamAssignmentList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'AdvisorTeamAssignment' instances.
     */
    long getAdvisorTeamAssignmentCount();

    AdvisorTeamAssignment findOpenAssignmentForTeamAndAdvisor(Long teamId, Long advisorId);

}
