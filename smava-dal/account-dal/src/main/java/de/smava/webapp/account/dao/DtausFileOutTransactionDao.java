//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(dtaus file out transaction)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.account.domain.DtausFile;
import de.smava.webapp.account.domain.DtausFileOutTransaction;
import de.smava.webapp.account.domain.Transaction;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'DtausFileOutTransactions'.
 *
 * @author generator
 */
public interface DtausFileOutTransactionDao extends BaseDao<DtausFileOutTransaction> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the dtaus file out transaction identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    DtausFileOutTransaction getDtausFileOutTransaction(Long id);

    /**
     * Saves the dtaus file out transaction.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveDtausFileOutTransaction(DtausFileOutTransaction dtausFileOutTransaction);

    /**
     * Deletes an dtaus file out transaction, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the dtaus file out transaction
     */
    void deleteDtausFileOutTransaction(Long id);

    /**
     * Retrieves all 'DtausFileOutTransaction' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<DtausFileOutTransaction> getDtausFileOutTransactionList();

    /**
     * Retrieves a page of 'DtausFileOutTransaction' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<DtausFileOutTransaction> getDtausFileOutTransactionList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'DtausFileOutTransaction' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<DtausFileOutTransaction> getDtausFileOutTransactionList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'DtausFileOutTransaction' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<DtausFileOutTransaction> getDtausFileOutTransactionList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'DtausFileOutTransaction' instances.
     */
    long getDtausFileOutTransactionCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(dtaus file out transaction)}
    //
    // insert custom methods here
    //

    /**
     * Returns a DtausFileOutTransaction with the given transaction and dtaus file where type is OUT
     * 
     * @param transaction the 
     * @param dtausFile
     * @return
     */
    DtausFileOutTransaction getDtausFileOutTransactionFromTransactionAndDtausFile(Transaction transaction, DtausFile dtausFile);


    // !!!!!!!! End of insert code section !!!!!!!!
}
