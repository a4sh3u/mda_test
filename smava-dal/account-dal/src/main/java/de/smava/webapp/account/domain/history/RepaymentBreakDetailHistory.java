package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractRepaymentBreakDetail;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'RepaymentBreakDetails'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RepaymentBreakDetailHistory extends AbstractRepaymentBreakDetail {

    protected transient double _requestedAmountInitVal;
    protected transient boolean _requestedAmountIsSet;
    protected transient Date _requestedDateInitVal;
    protected transient boolean _requestedDateIsSet;
    protected transient double _confirmedAmountInitVal;
    protected transient boolean _confirmedAmountIsSet;
    protected transient Date _confirmedDateInitVal;
    protected transient boolean _confirmedDateIsSet;


		
    /**
     * Returns the initial value of the property 'requested amount'.
     */
    public double requestedAmountInitVal() {
        double result;
        if (_requestedAmountIsSet) {
            result = _requestedAmountInitVal;
        } else {
            result = getRequestedAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested amount'.
     */
    public boolean requestedAmountIsDirty() {
        return !valuesAreEqual(requestedAmountInitVal(), getRequestedAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'requested amount'.
     */
    public boolean requestedAmountIsSet() {
        return _requestedAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested date'.
     */
    public Date requestedDateInitVal() {
        Date result;
        if (_requestedDateIsSet) {
            result = _requestedDateInitVal;
        } else {
            result = getRequestedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested date'.
     */
    public boolean requestedDateIsDirty() {
        return !valuesAreEqual(requestedDateInitVal(), getRequestedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'requested date'.
     */
    public boolean requestedDateIsSet() {
        return _requestedDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'confirmed amount'.
     */
    public double confirmedAmountInitVal() {
        double result;
        if (_confirmedAmountIsSet) {
            result = _confirmedAmountInitVal;
        } else {
            result = getConfirmedAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'confirmed amount'.
     */
    public boolean confirmedAmountIsDirty() {
        return !valuesAreEqual(confirmedAmountInitVal(), getConfirmedAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'confirmed amount'.
     */
    public boolean confirmedAmountIsSet() {
        return _confirmedAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'confirmed date'.
     */
    public Date confirmedDateInitVal() {
        Date result;
        if (_confirmedDateIsSet) {
            result = _confirmedDateInitVal;
        } else {
            result = getConfirmedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'confirmed date'.
     */
    public boolean confirmedDateIsDirty() {
        return !valuesAreEqual(confirmedDateInitVal(), getConfirmedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'confirmed date'.
     */
    public boolean confirmedDateIsSet() {
        return _confirmedDateIsSet;
    }
			
}
