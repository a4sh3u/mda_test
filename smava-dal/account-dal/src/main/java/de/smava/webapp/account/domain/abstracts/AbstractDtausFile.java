package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.interfaces.DtausFileEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.util.FormatUtils;
import org.apache.log4j.Logger;

import java.util.Set;

/**
 * The domain object that represents 'DtausFiles'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractDtausFile
    extends KreditPrivatEntity implements DtausFileEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDtausFile.class);

    private static final long serialVersionUID = 2172176581236526449L;

    public static final String DIRECTION_IN = "IN";
    public static final String DIRECTION_OUT = "OUT";

    public static final String STATE_OPEN = "OPEN";
    public static final String STATE_PROCESSED = "PROCESSED";
    public static final String STATE_RELEASED = "RELEASED";
    public static final String STATE_ERROR = "ERROR";
    public static final String STATE_ERROR_CHECKED = "ERROR_CHECKED";
    public static final String STATE_DELETED = "DELETED";

    public static final Set<String> STATES = ConstCollector.getAll("STATE_");

    public void addDescription(String text) {
        String description = getDescription();
        String delim = "\n";
        if (description == null) {
            description = "";
            delim = "";
        }
        description += delim + FormatUtils.formatShortDateTimeFormat(CurrentDate.getDate()) + ": " + text;
        int maxLen = 2048;
        if (description.length() > maxLen) {
            description = "..." + description.substring(description.length() - maxLen + 3);
        }
        setDescription(description);
    }

    public boolean isOpen() {
        return STATE_OPEN.equals(getState());
    }

    public boolean isProcessed() {
        return STATE_PROCESSED.equals(getState());
    }

    public boolean isReleased() {
        return STATE_RELEASED.equals(getState());
    }

    public boolean isIn() {
        return DIRECTION_IN.equals(getDirection());
    }

    public boolean isOut() {
        return DIRECTION_OUT.equals(getDirection());
    }

}

