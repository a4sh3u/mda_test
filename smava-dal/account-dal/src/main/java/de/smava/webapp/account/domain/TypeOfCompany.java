package de.smava.webapp.account.domain;

/**
 * String holder for the type of company type.
 * @author Dimitar Robev
 *
 */
public class TypeOfCompany {
	
	private TypeOfCompany() {
		super();
	}
	public static final String PUBLIC = "PUBLIC";
	public static final String STOCK = "STOCK";
	public static final String STATE = "STATE";
	public static final String LTD = "LTD";
	public static final String NOINFO = "NOINFO";
	public static final String OTHER = "OTHER";
}
