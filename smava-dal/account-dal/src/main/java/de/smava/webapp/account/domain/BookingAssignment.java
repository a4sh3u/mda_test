package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BookingAssignmentHistory;

/**
 * The domain object that represents 'BookingAssignments'.
 */
public class BookingAssignment extends BookingAssignmentHistory  {

    private static final long serialVersionUID = -3649166139065575848L;

    public BookingAssignment(BookingGroup bookingGroup, Booking booking, boolean isTarget) {
        _bookingGroup = bookingGroup;
        _booking = booking;
        _isTarget = isTarget; // true=Sollbuchung, false=Habenbuchung
    }

        protected boolean _isTarget;
        protected BookingGroup _bookingGroup;
        protected Booking _booking;
        
                            /**
     * Setter for the property 'is target'.
     */
    public void setIsTarget(boolean isTarget) {
        if (!_isTargetIsSet) {
            _isTargetIsSet = true;
            _isTargetInitVal = getIsTarget();
        }
        registerChange("is target", _isTargetInitVal, isTarget);
        _isTarget = isTarget;
    }
                        
    /**
     * Returns the property 'is target'.
     */
    public boolean getIsTarget() {
        return _isTarget;
    }
                                            
    /**
     * Setter for the property 'booking group'.
     */
    public void setBookingGroup(BookingGroup bookingGroup) {
        _bookingGroup = bookingGroup;
    }
            
    /**
     * Returns the property 'booking group'.
     */
    public BookingGroup getBookingGroup() {
        return _bookingGroup;
    }
                                            
    /**
     * Setter for the property 'booking'.
     */
    public void setBooking(Booking booking) {
        _booking = booking;
    }
            
    /**
     * Returns the property 'booking'.
     */
    public Booking getBooking() {
        return _booking;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BookingAssignment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _isTarget=").append(_isTarget);
            builder.append("\n}");
        } else {
            builder.append(BookingAssignment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
