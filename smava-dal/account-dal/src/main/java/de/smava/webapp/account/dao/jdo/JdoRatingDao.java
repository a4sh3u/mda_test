//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rating)}

import de.smava.webapp.account.domain.Rating;
import de.smava.webapp.account.todo.rating.dao.RatingDao;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Ratings'.
 *
 * @author generator
 */
@Repository(value = "ratingDao")
public class JdoRatingDao extends JdoBaseDao implements RatingDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRatingDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rating)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the rating identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Rating load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRating - start: id=" + id);
        }
        Rating result = getEntity(Rating.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRating - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Rating getRating(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Rating entity = findUniqueEntity(Rating.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the rating.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Rating rating) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRating: " + rating);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rating)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(rating);
    }

    /**
     * @deprecated Use {@link #save(Rating) instead}
     */
    public Long saveRating(Rating rating) {
        return save(rating);
    }

    /**
     * Deletes an rating, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rating
     */
    public void deleteRating(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRating: " + id);
        }
        deleteEntity(Rating.class, id);
    }

    /**
     * Retrieves all 'Rating' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Rating> getRatingList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - start");
        }
        Collection<Rating> result = getEntities(Rating.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Rating' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Rating> getRatingList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - start: pageable=" + pageable);
        }
        Collection<Rating> result = getEntities(Rating.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Rating' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Rating> getRatingList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - start: sortable=" + sortable);
        }
        Collection<Rating> result = getEntities(Rating.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Rating' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Rating> getRatingList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Rating> result = getEntities(Rating.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Rating' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Rating> findRatingList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - start: whereClause=" + whereClause);
        }
        Collection<Rating> result = findEntities(Rating.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Rating' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Rating> findRatingList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Rating> result = findEntities(Rating.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Rating' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Rating> findRatingList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Rating> result = findEntities(Rating.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Rating' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Rating> findRatingList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Rating> result = findEntities(Rating.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Rating' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Rating> findRatingList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Rating> result = findEntities(Rating.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Rating' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Rating> findRatingList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Rating> result = findEntities(Rating.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRatingList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Rating' instances.
     */
    public long getRatingCount() {
        long result = getEntityCount(Rating.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Rating' instances which match the given whereClause.
     */
    public long getRatingCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Rating.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Rating' instances which match the given whereClause together with params specified in object array.
     */
    public long getRatingCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Rating.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRatingCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rating)}
    //
    public Rating getRatingByName(String ratingLetter) {
        Rating rating;
        Collection<Rating> ratings = findRatingList("_name == '" + ratingLetter + "'");
        if (ratings.size() == 1) {
            rating = ratings.iterator().next();
        } else {
            throw new IllegalStateException("Too few/many ratings found for rating " + ratingLetter + " (" + ratings.size() + ")");
        }
        return rating;
    }

    @Override
    public Collection<Rating> findRatingsOrdered(){
        return findRatingList("_id > 0 order by _name ascending"); // _id>0 = dummy clause for jdo
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
