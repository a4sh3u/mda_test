//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(registration route)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.account.domain.RegistrationRoute;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RegistrationRoutes'.
 *
 * @author generator
 */
public interface RegistrationRouteDao extends BaseDao<RegistrationRoute> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the registration route identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RegistrationRoute getRegistrationRoute(Long id);

    /**
     * Saves the registration route.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRegistrationRoute(RegistrationRoute registrationRoute);

    /**
     * Deletes an registration route, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the registration route
     */
    void deleteRegistrationRoute(Long id);

    /**
     * Retrieves all 'RegistrationRoute' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RegistrationRoute> getRegistrationRouteList();

    /**
     * Retrieves a page of 'RegistrationRoute' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RegistrationRoute> getRegistrationRouteList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RegistrationRoute' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RegistrationRoute> getRegistrationRouteList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RegistrationRoute' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RegistrationRoute> getRegistrationRouteList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'RegistrationRoute' instances.
     */
    long getRegistrationRouteCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(registration route)}
    //
    // insert custom methods here

    RegistrationRoute getRegistrationRouteByName(String name);

    RegistrationRoute getRegistrationRouteByShortId(Integer shortId);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
