//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(id card)}

import de.smava.webapp.account.dao.IdCardDao;
import de.smava.webapp.account.domain.IdCard;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'IdCards'.
 *
 * @author generator
 */
@Repository(value = "idCardDao")
public class JdoIdCardDao extends JdoBaseDao implements IdCardDao {

    private static final Logger LOGGER = Logger.getLogger(JdoIdCardDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(id card)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the id card identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public IdCard load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCard - start: id=" + id);
        }
        IdCard result = getEntity(IdCard.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCard - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public IdCard getIdCard(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	IdCard entity = findUniqueEntity(IdCard.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the id card.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(IdCard idCard) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveIdCard: " + idCard);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(id card)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(idCard);
    }

    /**
     * @deprecated Use {@link #save(IdCard) instead}
     */
    public Long saveIdCard(IdCard idCard) {
        return save(idCard);
    }

    /**
     * Deletes an id card, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the id card
     */
    public void deleteIdCard(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteIdCard: " + id);
        }
        deleteEntity(IdCard.class, id);
    }

    /**
     * Retrieves all 'IdCard' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<IdCard> getIdCardList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - start");
        }
        Collection<IdCard> result = getEntities(IdCard.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'IdCard' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<IdCard> getIdCardList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - start: pageable=" + pageable);
        }
        Collection<IdCard> result = getEntities(IdCard.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdCard' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<IdCard> getIdCardList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - start: sortable=" + sortable);
        }
        Collection<IdCard> result = getEntities(IdCard.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdCard' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<IdCard> getIdCardList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<IdCard> result = getEntities(IdCard.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdCard' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdCard> findIdCardList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - start: whereClause=" + whereClause);
        }
        Collection<IdCard> result = findEntities(IdCard.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdCard' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<IdCard> findIdCardList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<IdCard> result = findEntities(IdCard.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'IdCard' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdCard> findIdCardList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<IdCard> result = findEntities(IdCard.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdCard' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdCard> findIdCardList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<IdCard> result = findEntities(IdCard.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdCard' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdCard> findIdCardList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<IdCard> result = findEntities(IdCard.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdCard' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdCard> findIdCardList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<IdCard> result = findEntities(IdCard.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findIdCardList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'IdCard' instances.
     */
    public long getIdCardCount() {
        long result = getEntityCount(IdCard.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdCard' instances which match the given whereClause.
     */
    public long getIdCardCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(IdCard.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdCard' instances which match the given whereClause together with params specified in object array.
     */
    public long getIdCardCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(IdCard.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getIdCardCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(id card)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
