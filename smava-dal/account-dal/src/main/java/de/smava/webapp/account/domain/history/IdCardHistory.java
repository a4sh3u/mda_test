package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractIdCard;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'IdCards'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IdCardHistory extends AbstractIdCard {

    protected transient String _bkzInitVal;
    protected transient boolean _bkzIsSet;
    protected transient Date _issueDateInitVal;
    protected transient boolean _issueDateIsSet;
    protected transient Date _expiryDateInitVal;
    protected transient boolean _expiryDateIsSet;
    protected transient Date _bkzCheckInitVal;
    protected transient boolean _bkzCheckIsSet;
    protected transient Date _checksumCheckInitVal;
    protected transient boolean _checksumCheckIsSet;
    protected transient Date _birthdateCheckInitVal;
    protected transient boolean _birthdateCheckIsSet;
    protected transient Date _expiryDateCheckInitVal;
    protected transient boolean _expiryDateCheckIsSet;
    protected transient Date _addressCheckInitVal;
    protected transient boolean _addressCheckIsSet;
    protected transient Date _validityDateInitVal;
    protected transient boolean _validityDateIsSet;
    protected transient Date _validUntilInitVal;
    protected transient boolean _validUntilIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _skippedDateInitVal;
    protected transient boolean _skippedDateIsSet;
    protected transient Date _birthdayCheckIdcardInitVal;
    protected transient boolean _birthdayCheckIdcardIsSet;
    protected transient String _descriptionsInitVal;
    protected transient boolean _descriptionsIsSet;


	
    /**
     * Returns the initial value of the property 'bkz'.
     */
    public String bkzInitVal() {
        String result;
        if (_bkzIsSet) {
            result = _bkzInitVal;
        } else {
            result = getBkz();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bkz'.
     */
    public boolean bkzIsDirty() {
        return !valuesAreEqual(bkzInitVal(), getBkz());
    }

    /**
     * Returns true if the setter method was called for the property 'bkz'.
     */
    public boolean bkzIsSet() {
        return _bkzIsSet;
    }
	
    /**
     * Returns the initial value of the property 'issue date'.
     */
    public Date issueDateInitVal() {
        Date result;
        if (_issueDateIsSet) {
            result = _issueDateInitVal;
        } else {
            result = getIssueDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'issue date'.
     */
    public boolean issueDateIsDirty() {
        return !valuesAreEqual(issueDateInitVal(), getIssueDate());
    }

    /**
     * Returns true if the setter method was called for the property 'issue date'.
     */
    public boolean issueDateIsSet() {
        return _issueDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiry date'.
     */
    public Date expiryDateInitVal() {
        Date result;
        if (_expiryDateIsSet) {
            result = _expiryDateInitVal;
        } else {
            result = getExpiryDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiry date'.
     */
    public boolean expiryDateIsDirty() {
        return !valuesAreEqual(expiryDateInitVal(), getExpiryDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiry date'.
     */
    public boolean expiryDateIsSet() {
        return _expiryDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bkz check'.
     */
    public Date bkzCheckInitVal() {
        Date result;
        if (_bkzCheckIsSet) {
            result = _bkzCheckInitVal;
        } else {
            result = getBkzCheck();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bkz check'.
     */
    public boolean bkzCheckIsDirty() {
        return !valuesAreEqual(bkzCheckInitVal(), getBkzCheck());
    }

    /**
     * Returns true if the setter method was called for the property 'bkz check'.
     */
    public boolean bkzCheckIsSet() {
        return _bkzCheckIsSet;
    }
	
    /**
     * Returns the initial value of the property 'checksum check'.
     */
    public Date checksumCheckInitVal() {
        Date result;
        if (_checksumCheckIsSet) {
            result = _checksumCheckInitVal;
        } else {
            result = getChecksumCheck();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'checksum check'.
     */
    public boolean checksumCheckIsDirty() {
        return !valuesAreEqual(checksumCheckInitVal(), getChecksumCheck());
    }

    /**
     * Returns true if the setter method was called for the property 'checksum check'.
     */
    public boolean checksumCheckIsSet() {
        return _checksumCheckIsSet;
    }
	
    /**
     * Returns the initial value of the property 'birthdate check'.
     */
    public Date birthdateCheckInitVal() {
        Date result;
        if (_birthdateCheckIsSet) {
            result = _birthdateCheckInitVal;
        } else {
            result = getBirthdateCheck();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birthdate check'.
     */
    public boolean birthdateCheckIsDirty() {
        return !valuesAreEqual(birthdateCheckInitVal(), getBirthdateCheck());
    }

    /**
     * Returns true if the setter method was called for the property 'birthdate check'.
     */
    public boolean birthdateCheckIsSet() {
        return _birthdateCheckIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiry date check'.
     */
    public Date expiryDateCheckInitVal() {
        Date result;
        if (_expiryDateCheckIsSet) {
            result = _expiryDateCheckInitVal;
        } else {
            result = getExpiryDateCheck();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiry date check'.
     */
    public boolean expiryDateCheckIsDirty() {
        return !valuesAreEqual(expiryDateCheckInitVal(), getExpiryDateCheck());
    }

    /**
     * Returns true if the setter method was called for the property 'expiry date check'.
     */
    public boolean expiryDateCheckIsSet() {
        return _expiryDateCheckIsSet;
    }
	
    /**
     * Returns the initial value of the property 'address check'.
     */
    public Date addressCheckInitVal() {
        Date result;
        if (_addressCheckIsSet) {
            result = _addressCheckInitVal;
        } else {
            result = getAddressCheck();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'address check'.
     */
    public boolean addressCheckIsDirty() {
        return !valuesAreEqual(addressCheckInitVal(), getAddressCheck());
    }

    /**
     * Returns true if the setter method was called for the property 'address check'.
     */
    public boolean addressCheckIsSet() {
        return _addressCheckIsSet;
    }
	
    /**
     * Returns the initial value of the property 'validity date'.
     */
    public Date validityDateInitVal() {
        Date result;
        if (_validityDateIsSet) {
            result = _validityDateInitVal;
        } else {
            result = getValidityDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'validity date'.
     */
    public boolean validityDateIsDirty() {
        return !valuesAreEqual(validityDateInitVal(), getValidityDate());
    }

    /**
     * Returns true if the setter method was called for the property 'validity date'.
     */
    public boolean validityDateIsSet() {
        return _validityDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid until'.
     */
    public Date validUntilInitVal() {
        Date result;
        if (_validUntilIsSet) {
            result = _validUntilInitVal;
        } else {
            result = getValidUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid until'.
     */
    public boolean validUntilIsDirty() {
        return !valuesAreEqual(validUntilInitVal(), getValidUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'valid until'.
     */
    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'skipped date'.
     */
    public Date skippedDateInitVal() {
        Date result;
        if (_skippedDateIsSet) {
            result = _skippedDateInitVal;
        } else {
            result = getSkippedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'skipped date'.
     */
    public boolean skippedDateIsDirty() {
        return !valuesAreEqual(skippedDateInitVal(), getSkippedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'skipped date'.
     */
    public boolean skippedDateIsSet() {
        return _skippedDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'birthday check idcard'.
     */
    public Date birthdayCheckIdcardInitVal() {
        Date result;
        if (_birthdayCheckIdcardIsSet) {
            result = _birthdayCheckIdcardInitVal;
        } else {
            result = getBirthdayCheckIdcard();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birthday check idcard'.
     */
    public boolean birthdayCheckIdcardIsDirty() {
        return !valuesAreEqual(birthdayCheckIdcardInitVal(), getBirthdayCheckIdcard());
    }

    /**
     * Returns true if the setter method was called for the property 'birthday check idcard'.
     */
    public boolean birthdayCheckIdcardIsSet() {
        return _birthdayCheckIdcardIsSet;
    }
	
    /**
     * Returns the initial value of the property 'descriptions'.
     */
    public String descriptionsInitVal() {
        String result;
        if (_descriptionsIsSet) {
            result = _descriptionsInitVal;
        } else {
            result = getDescriptions();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'descriptions'.
     */
    public boolean descriptionsIsDirty() {
        return !valuesAreEqual(descriptionsInitVal(), getDescriptions());
    }

    /**
     * Returns true if the setter method was called for the property 'descriptions'.
     */
    public boolean descriptionsIsSet() {
        return _descriptionsIsSet;
    }

}
