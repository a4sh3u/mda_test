package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.EconomicalData;
import de.smava.webapp.account.domain.SchufaScore;

import java.util.Date;


/**
 * The domain object that represents 'CreditScores'.
 *
 * @author generator
 */
public interface CreditScoreEntityInterface {

    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    void setScore(Long score);

    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    Long getScore();
    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    String getRating();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'schufa score'.
     *
     * 
     *
     */
    void setSchufaScore(SchufaScore schufaScore);

    /**
     * Returns the property 'schufa score'.
     *
     * 
     *
     */
    SchufaScore getSchufaScore();
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    void setEconomicalData(EconomicalData economicalData);

    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    EconomicalData getEconomicalData();
    /**
     * Setter for the property 'change date'.
     *
     * 
     *
     */
    void setChangeDate(Date changeDate);

    /**
     * Returns the property 'change date'.
     *
     * 
     *
     */
    Date getChangeDate();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();

}
