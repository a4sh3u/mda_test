//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(booking performance)}

import de.smava.webapp.account.domain.history.BookingPerformanceHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BookingPerformances'.
 *
 * @author generator
 */
public class BookingPerformance extends BookingPerformanceHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(booking performance)}
    private static final long serialVersionUID = 52670471455273672L;

    public BookingPerformance() {
        _successfulRate = 0L;
        _failedRate = 0L;
        _delayedRate = 0L;
    }

    // !!!!!!!! End of insert code section !!!!!!!!


            protected Long _successfulRate;
            protected Long _failedRate;
            protected Long _delayedRate;
        
                        /**
            * Setter for the property 'successful rate'.
            */
            public void setSuccessfulRate(Long successfulRate) {
            _successfulRate = successfulRate;
            }

            /**
            * Returns the property 'successful rate'.
            */
            public Long getSuccessfulRate() {
            return _successfulRate;
            }
                                /**
            * Setter for the property 'failed rate'.
            */
            public void setFailedRate(Long failedRate) {
            _failedRate = failedRate;
            }

            /**
            * Returns the property 'failed rate'.
            */
            public Long getFailedRate() {
            return _failedRate;
            }
                                /**
            * Setter for the property 'delayed rate'.
            */
            public void setDelayedRate(Long delayedRate) {
            _delayedRate = delayedRate;
            }

            /**
            * Returns the property 'delayed rate'.
            */
            public Long getDelayedRate() {
            return _delayedRate;
            }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
    StringBuilder builder = new StringBuilder();
    if (LOGGER.isDebugEnabled()) {
    builder.append(BookingPerformance.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
    } else {
    builder.append(BookingPerformance.class.getName()).append("(id=)").append(getId()).append(")");
    }
    return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BookingPerformance asBookingPerformance() {
        return this;
    }
}
