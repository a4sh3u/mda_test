package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.interfaces.AdvisorTeamAssignmentEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'AdvisorTeamAssignments'.
 */
public class AdvisorTeamAssignment extends KreditPrivatEntity
        implements AdvisorTeamAssignmentEntityInterface<Advisor, Team> {

    protected static final Logger LOGGER = Logger.getLogger(AdvisorTeamAssignment.class);

    protected Advisor _advisor;
    protected Team _team;
    protected Date _creationDate;
    protected Date _expirationDate;

    /**
     * Setter for the property 'advisor'.
     */
    public void setAdvisor(Advisor advisor) {
        _advisor = advisor;
    }

    /**
     * Returns the property 'advisor'.
     */
    public Advisor getAdvisor() {
        return _advisor;
    }

    /**
     * Setter for the property 'team'.
     */
    public void setTeam(Team team) {
        _team = team;
    }

    /**
     * Returns the property 'team'.
     */
    public Team getTeam() {
        return _team;
    }

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }

    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }

    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AdvisorTeamAssignment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AdvisorTeamAssignment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
