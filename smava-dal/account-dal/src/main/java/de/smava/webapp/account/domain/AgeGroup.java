package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.AgeGroupHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'AgeGroups'.
 */
public class AgeGroup extends AgeGroupHistory  {

	private static final long serialVersionUID = -1201626052559530628L;

    public AgeGroup(Integer minAge, Integer maxAge, Date validFrom, Date validUntil) {
    	_maxAge = maxAge;
    	_minAge = minAge;
    	_validFrom = validFrom;
    	_validUntil = validUntil;
    }

        protected Integer _minAge;
        protected Integer _maxAge;
        protected Date _validFrom;
        protected Date _validUntil;
        protected Collection<BidSearchAgeGroup> _bidSearches;

    /**
     * Setter for the property 'min age'.
     */
    public void setMinAge(Integer minAge) {
        _minAge = minAge;
    }
            
    /**
     * Returns the property 'min age'.
     */
    public Integer getMinAge() {
        return _minAge;
    }
                                            
    /**
     * Setter for the property 'max age'.
     */
    public void setMaxAge(Integer maxAge) {
        _maxAge = maxAge;
    }
            
    /**
     * Returns the property 'max age'.
     */
    public Integer getMaxAge() {
        return _maxAge;
    }
                                    /**
     * Setter for the property 'valid from'.
     */
    public void setValidFrom(Date validFrom) {
        if (!_validFromIsSet) {
            _validFromIsSet = true;
            _validFromInitVal = getValidFrom();
        }
        registerChange("valid from", _validFromInitVal, validFrom);
        _validFrom = validFrom;
    }
                        
    /**
     * Returns the property 'valid from'.
     */
    public Date getValidFrom() {
        return _validFrom;
    }
                                    /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }
                                            
    /**
     * Setter for the property 'bid searches'.
     */
    public void setBidSearches(Collection<BidSearchAgeGroup> bidSearches) {
        _bidSearches = bidSearches;
    }
            
    /**
     * Returns the property 'bid searches'.
     */
    public Collection<BidSearchAgeGroup> getBidSearches() {
        return _bidSearches;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AgeGroup.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AgeGroup.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
