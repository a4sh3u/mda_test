//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(mail link)}

import de.smava.webapp.account.dao.MailLinkDao;
import com.aperto.webkit.utils.StringTools;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.MailLink;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.event.Events;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'MailLinks'.
 *
 * @author generator
 */
@Repository(value = "mailLinkDao")
public class JdoMailLinkDao extends JdoBaseDao implements MailLinkDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMailLinkDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(mail link)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the mail link identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MailLink load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLink - start: id=" + id);
        }
        MailLink result = getEntity(MailLink.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLink - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MailLink getMailLink(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MailLink entity = findUniqueEntity(MailLink.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the mail link.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MailLink mailLink) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMailLink: " + mailLink);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(mail link)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(mailLink);
    }

    /**
     * @deprecated Use {@link #save(MailLink) instead}
     */
    public Long saveMailLink(MailLink mailLink) {
        return save(mailLink);
    }

    /**
     * Deletes an mail link, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the mail link
     */
    public void deleteMailLink(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMailLink: " + id);
        }
        deleteEntity(MailLink.class, id);
    }

    /**
     * Retrieves all 'MailLink' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MailLink> getMailLinkList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - start");
        }
        Collection<MailLink> result = getEntities(MailLink.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MailLink' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MailLink> getMailLinkList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - start: pageable=" + pageable);
        }
        Collection<MailLink> result = getEntities(MailLink.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MailLink' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MailLink> getMailLinkList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - start: sortable=" + sortable);
        }
        Collection<MailLink> result = getEntities(MailLink.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MailLink' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MailLink> getMailLinkList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MailLink> result = getEntities(MailLink.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MailLink' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MailLink> findMailLinkList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - start: whereClause=" + whereClause);
        }
        Collection<MailLink> result = findEntities(MailLink.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MailLink' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MailLink> findMailLinkList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MailLink> result = findEntities(MailLink.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MailLink' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MailLink> findMailLinkList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MailLink> result = findEntities(MailLink.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MailLink' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MailLink> findMailLinkList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MailLink> result = findEntities(MailLink.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MailLink' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MailLink> findMailLinkList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MailLink> result = findEntities(MailLink.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MailLink' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MailLink> findMailLinkList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MailLink> result = findEntities(MailLink.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMailLinkList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MailLink' instances.
     */
    public long getMailLinkCount() {
        long result = getEntityCount(MailLink.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MailLink' instances which match the given whereClause.
     */
    public long getMailLinkCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MailLink.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MailLink' instances which match the given whereClause together with params specified in object array.
     */
    public long getMailLinkCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MailLink.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMailLinkCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(mail link)}
    //

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<MailLink> findInvitationMailLinks(Account invited) {
        // findRecipientsForType
        Query query = getPersistenceManager().newNamedQuery(MailLink.class, "findRecipientsForType");
        query.setOrdering("_creationDate ASC");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("mailType", Events.EVENT_INVITEMEMBERFORM);
        params.put("recipient", invited);
        @SuppressWarnings("unchecked")
        Collection<MailLink> mailLinks = (Collection<MailLink>) query.executeWithMap(params);

        return mailLinks;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<MailLink> getMailLinks(Account account, boolean isSender, boolean respectIsSender) {
        Collection<MailLink> result = new ArrayList<MailLink>();
        if (account != null) {
            if (!respectIsSender) {
                result = findMailLinkList("_sender._id == " + account.getId() + " || _recipient._id == " + account.getId());
            } else {
                if (isSender) {
                    result = findMailLinkList("_sender._id == " + account.getId());
                } else {
                    result = findMailLinkList("_recipient._id == " + account.getId());
                }
            }
        }
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<MailLink> getInvitationMailLinks(Account account, boolean isSender, boolean respectIsSender, Date begin, Date end) {
        Collection<MailLink> result = new ArrayList<MailLink>();
        String whereClause = "_mailType == '" + Events.EVENT_INVITEMEMBERFORM + "'";
        if (account != null) {
            if (!respectIsSender) {
                whereClause += " && (_sender._id == " + account.getId() + " || _recipient._id == " + account.getId() + ")";
            } else {
                if (isSender) {
                    whereClause += " && _sender._id == " + account.getId();
                } else {
                    whereClause += " && _recipient._id == " + account.getId();
                }
            }
            if (begin != null && end != null) {
                whereClause += " && this._creationDate >= param1 && this._creationDate <= param2 ";
                whereClause += " parameters java.util.Date param1,java.util.Date param2";
                result = findMailLinkList(whereClause, begin, end);
            } else {
                result = findMailLinkList(whereClause);
            }

        }
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<MailLink> getInvitationMailLinks(String remoteAddress, Date begin, Date end) {
        Collection<MailLink> result = new ArrayList<MailLink>();
        String whereClause = "_mailType == '" + Events.EVENT_INVITEMEMBERFORM + "'";
        if (remoteAddress != null) {
            whereClause += " && _remoteAddress == '" + remoteAddress + "'";

            if (begin != null && end != null) {
                whereClause += " && this._creationDate >= param1 && this._creationDate <= param2 ";
                whereClause += " parameters java.util.Date param1,java.util.Date param2";
                result = findMailLinkList(whereClause, begin, end);
            } else {
                result = findMailLinkList(whereClause);
            }

        }
        return result;
    }

    public String getWhereClause(String searchLinkName, String searchMailType,
                          Account account, Date fromDateAsDate, Date toDateAsDate) {
        Collection<OqlTerm> terms = new ArrayList<OqlTerm>(5);

        OqlTerm term = OqlTerm.newTerm();

        // Link name types...
        if (!StringTools.isEmpty(searchLinkName)) {
            terms.add(term.equals("_linkName", searchLinkName));
        }

        // Mail types...
        if (!StringTools.isEmpty(searchMailType)) {
            terms.add(term.equals("_mailType", searchMailType));
        }

        if (account != null) {
            terms.add(term.equals("_recipient", account));
        }

        if (fromDateAsDate != null) {
            terms.add(term.greaterThanEquals("_creationDate", fromDateAsDate));
        }
        if (toDateAsDate != null) {
            terms.add(term.lessThan("_creationDate", toDateAsDate));
        }

        final String where;
        if (terms.size() == 1) {
            where = term.toString();
        } else if (terms.size() > 1) {
            term.and(terms.toArray(new OqlTerm[terms.size()]));
            where = term.toString();
        } else {
            where = "";
        }

        LOGGER.debug("************ where " + where);
        return where;
    }


    public Collection<MailLink> getMailLinks(String term, List<Object> paramsList) {
        return this.findMailLinkList(
                term,
                paramsList.toArray()
        );
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
