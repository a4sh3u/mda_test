//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rating)}

import de.smava.webapp.account.domain.interfaces.RatingEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Ratings'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractRating
    extends KreditPrivatEntity implements RatingEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRating.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(rating)}
    private static final long serialVersionUID = 6092176482847853143L;

    private static Map<String, Map<Integer, Double>> c_interestReductions = new HashMap<String, Map<Integer, Double>>();
    static {
        //todo: store this in DB... like creditDiscount!!
        c_interestReductions.put("A", new HashMap<Integer, Double>());
        c_interestReductions.put("B", new HashMap<Integer, Double>());
        c_interestReductions.put("C", new HashMap<Integer, Double>());
        c_interestReductions.put("D", new HashMap<Integer, Double>());
        c_interestReductions.put("E", new HashMap<Integer, Double>());
        c_interestReductions.put("F", new HashMap<Integer, Double>());
        c_interestReductions.put("G", new HashMap<Integer, Double>());
        c_interestReductions.put("H", new HashMap<Integer, Double>());
        c_interestReductions.put("I", new HashMap<Integer, Double>());
        c_interestReductions.put("J", new HashMap<Integer, Double>());
        c_interestReductions.put("K", new HashMap<Integer, Double>());
        c_interestReductions.put("L", new HashMap<Integer, Double>());
        c_interestReductions.put("M", new HashMap<Integer, Double>());

    }

    static {
        c_interestReductions.get("A").put(3, 0.15);
        c_interestReductions.get("A").put(6, 0.15);
        c_interestReductions.get("A").put(9, 0.15);
        c_interestReductions.get("A").put(12, 0.15);
        c_interestReductions.get("A").put(15, 0.15);
        c_interestReductions.get("A").put(18, 0.15);
        c_interestReductions.get("A").put(21, 0.16);
        c_interestReductions.get("A").put(24, 0.16);
        c_interestReductions.get("A").put(27, 0.16);
        c_interestReductions.get("A").put(30, 0.16);
        c_interestReductions.get("A").put(33, 0.16);
        c_interestReductions.get("A").put(36, 0.16);
        c_interestReductions.get("A").put(42, 0.12);
        c_interestReductions.get("A").put(48, 0.12);
        c_interestReductions.get("A").put(60, 0.12);

    }

    static {
        c_interestReductions.get("B").put(3, 0.17);
        c_interestReductions.get("B").put(6, 0.17);
        c_interestReductions.get("B").put(9, 0.17);
        c_interestReductions.get("B").put(12, 0.17);
        c_interestReductions.get("B").put(15, 0.17);
        c_interestReductions.get("B").put(18, 0.17);
        c_interestReductions.get("B").put(21, 0.17);
        c_interestReductions.get("B").put(24, 0.17);
        c_interestReductions.get("B").put(27, 0.18);
        c_interestReductions.get("B").put(30, 0.18);
        c_interestReductions.get("B").put(33, 0.18);
        c_interestReductions.get("B").put(36, 0.18);
        c_interestReductions.get("B").put(42, 0.155);
        c_interestReductions.get("B").put(48, 0.155);
        c_interestReductions.get("B").put(60, 0.155);

    }
    static {
        c_interestReductions.get("C").put(3, 0.18);
        c_interestReductions.get("C").put(6, 0.19);
        c_interestReductions.get("C").put(9, 0.19);
        c_interestReductions.get("C").put(12, 0.19);
        c_interestReductions.get("C").put(15, 0.19);
        c_interestReductions.get("C").put(18, 0.19);
        c_interestReductions.get("C").put(21, 0.19);
        c_interestReductions.get("C").put(24, 0.19);
        c_interestReductions.get("C").put(27, 0.19);
        c_interestReductions.get("C").put(30, 0.19);
        c_interestReductions.get("C").put(33, 0.20);
        c_interestReductions.get("C").put(36, 0.20);
        c_interestReductions.get("C").put(42, 0.19);
        c_interestReductions.get("C").put(48, 0.19);
        c_interestReductions.get("C").put(60, 0.19);

    }
    static {
        c_interestReductions.get("D").put(3, 0.20);
        c_interestReductions.get("D").put(6, 0.20);
        c_interestReductions.get("D").put(9, 0.20);
        c_interestReductions.get("D").put(12, 0.21);
        c_interestReductions.get("D").put(15, 0.21);
        c_interestReductions.get("D").put(18, 0.21);
        c_interestReductions.get("D").put(21, 0.21);
        c_interestReductions.get("D").put(24, 0.21);
        c_interestReductions.get("D").put(27, 0.21);
        c_interestReductions.get("D").put(30, 0.21);
        c_interestReductions.get("D").put(33, 0.21);
        c_interestReductions.get("D").put(36, 0.22);
        c_interestReductions.get("D").put(42, 0.22);
        c_interestReductions.get("D").put(48, 0.22);
        c_interestReductions.get("D").put(60, 0.22);

    }
    static {
        c_interestReductions.get("E").put(3, 0.22);
        c_interestReductions.get("E").put(6, 0.22);
        c_interestReductions.get("E").put(9, 0.22);
        c_interestReductions.get("E").put(12, 0.22);
        c_interestReductions.get("E").put(15, 0.22);
        c_interestReductions.get("E").put(18, 0.23);
        c_interestReductions.get("E").put(21, 0.23);
        c_interestReductions.get("E").put(24, 0.23);
        c_interestReductions.get("E").put(27, 0.23);
        c_interestReductions.get("E").put(30, 0.23);
        c_interestReductions.get("E").put(33, 0.23);
        c_interestReductions.get("E").put(36, 0.23);
        c_interestReductions.get("E").put(42, 0.255);
        c_interestReductions.get("E").put(48, 0.255);
        c_interestReductions.get("E").put(60, 0.255);
    }
    static {
        c_interestReductions.get("F").put(3, 0.24);
        c_interestReductions.get("F").put(6, 0.24);
        c_interestReductions.get("F").put(9, 0.24);
        c_interestReductions.get("F").put(12, 0.24);
        c_interestReductions.get("F").put(15, 0.24);
        c_interestReductions.get("F").put(18, 0.24);
        c_interestReductions.get("F").put(21, 0.25);
        c_interestReductions.get("F").put(24, 0.25);
        c_interestReductions.get("F").put(27, 0.25);
        c_interestReductions.get("F").put(30, 0.25);
        c_interestReductions.get("F").put(33, 0.25);
        c_interestReductions.get("F").put(36, 0.25);
        c_interestReductions.get("F").put(42, 0.29);
        c_interestReductions.get("F").put(48, 0.29);
        c_interestReductions.get("F").put(60, 0.29);
    }
    static {
        c_interestReductions.get("G").put(3, 0.25);
        c_interestReductions.get("G").put(6, 0.25);
        c_interestReductions.get("G").put(9, 0.26);
        c_interestReductions.get("G").put(12, 0.26);
        c_interestReductions.get("G").put(15, 0.26);
        c_interestReductions.get("G").put(18, 0.26);
        c_interestReductions.get("G").put(21, 0.26);
        c_interestReductions.get("G").put(24, 0.26);
        c_interestReductions.get("G").put(27, 0.27);
        c_interestReductions.get("G").put(30, 0.27);
        c_interestReductions.get("G").put(33, 0.27);
        c_interestReductions.get("G").put(36, 0.27);
        c_interestReductions.get("G").put(42, 0.325);
        c_interestReductions.get("G").put(48, 0.325);
        c_interestReductions.get("G").put(60, 0.325);
    }
    static {
        c_interestReductions.get("H").put(3, 0.27);
        c_interestReductions.get("H").put(6, 0.27);
        c_interestReductions.get("H").put(9, 0.27);
        c_interestReductions.get("H").put(12, 0.28);
        c_interestReductions.get("H").put(15, 0.28);
        c_interestReductions.get("H").put(18, 0.28);
        c_interestReductions.get("H").put(21, 0.28);
        c_interestReductions.get("H").put(24, 0.28);
        c_interestReductions.get("H").put(27, 0.28);
        c_interestReductions.get("H").put(30, 0.29);
        c_interestReductions.get("H").put(33, 0.29);
        c_interestReductions.get("H").put(36, 0.29);
        c_interestReductions.get("H").put(42, 0.38);
        c_interestReductions.get("H").put(48, 0.38);
        c_interestReductions.get("H").put(60, 0.38);

    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

