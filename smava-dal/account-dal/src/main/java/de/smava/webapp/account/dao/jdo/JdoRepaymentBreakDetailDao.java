//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head15/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head15/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(repayment break detail)}

import de.smava.webapp.account.dao.RepaymentBreakDetailDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.RepaymentBreakDetail;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'RepaymentBreakDetails'.
 *
 * @author generator
 */
@Repository(value = "repaymentBreakDetailDao")
public class JdoRepaymentBreakDetailDao extends JdoBaseDao implements RepaymentBreakDetailDao {

	private static final Logger LOGGER = Logger.getLogger(JdoRepaymentBreakDetailDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(repayment break detail)}
	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 6584396494311664973L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the repayment break detail identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RepaymentBreakDetail load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetail - start: id=" + id);
        }
        RepaymentBreakDetail result = getEntity(RepaymentBreakDetail.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetail - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RepaymentBreakDetail getRepaymentBreakDetail(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RepaymentBreakDetail entity = findUniqueEntity(RepaymentBreakDetail.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the repayment break detail.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RepaymentBreakDetail repaymentBreakDetail) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRepaymentBreakDetail: " + repaymentBreakDetail);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(repayment break detail)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(repaymentBreakDetail);
    }

    /**
     * @deprecated Use {@link #save(RepaymentBreakDetail) instead}
     */
    public Long saveRepaymentBreakDetail(RepaymentBreakDetail repaymentBreakDetail) {
        return save(repaymentBreakDetail);
    }

    /**
     * Deletes an repayment break detail, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the repayment break detail
     */
    public void deleteRepaymentBreakDetail(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRepaymentBreakDetail: " + id);
        }
        deleteEntity(RepaymentBreakDetail.class, id);
    }

    /**
     * Retrieves all 'RepaymentBreakDetail' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RepaymentBreakDetail> getRepaymentBreakDetailList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - start");
        }
        Collection<RepaymentBreakDetail> result = getEntities(RepaymentBreakDetail.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RepaymentBreakDetail' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RepaymentBreakDetail> getRepaymentBreakDetailList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - start: pageable=" + pageable);
        }
        Collection<RepaymentBreakDetail> result = getEntities(RepaymentBreakDetail.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RepaymentBreakDetail' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RepaymentBreakDetail> getRepaymentBreakDetailList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - start: sortable=" + sortable);
        }
        Collection<RepaymentBreakDetail> result = getEntities(RepaymentBreakDetail.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RepaymentBreakDetail' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RepaymentBreakDetail> getRepaymentBreakDetailList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RepaymentBreakDetail> result = getEntities(RepaymentBreakDetail.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RepaymentBreakDetail' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreakDetail> findRepaymentBreakDetailList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - start: whereClause=" + whereClause);
        }
        Collection<RepaymentBreakDetail> result = findEntities(RepaymentBreakDetail.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RepaymentBreakDetail' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RepaymentBreakDetail> findRepaymentBreakDetailList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<RepaymentBreakDetail> result = findEntities(RepaymentBreakDetail.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RepaymentBreakDetail' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreakDetail> findRepaymentBreakDetailList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<RepaymentBreakDetail> result = findEntities(RepaymentBreakDetail.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RepaymentBreakDetail' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreakDetail> findRepaymentBreakDetailList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<RepaymentBreakDetail> result = findEntities(RepaymentBreakDetail.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RepaymentBreakDetail' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreakDetail> findRepaymentBreakDetailList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RepaymentBreakDetail> result = findEntities(RepaymentBreakDetail.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RepaymentBreakDetail' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RepaymentBreakDetail> findRepaymentBreakDetailList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<RepaymentBreakDetail> result = findEntities(RepaymentBreakDetail.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findRepaymentBreakDetailList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RepaymentBreakDetail' instances.
     */
    public long getRepaymentBreakDetailCount() {
        long result = getEntityCount(RepaymentBreakDetail.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RepaymentBreakDetail' instances which match the given whereClause.
     */
    public long getRepaymentBreakDetailCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(RepaymentBreakDetail.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RepaymentBreakDetail' instances which match the given whereClause together with params specified in object array.
     */
    public long getRepaymentBreakDetailCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(RepaymentBreakDetail.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getRepaymentBreakDetailCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(repayment break detail)}
    //
    // insert custom methods here


    public List<RepaymentBreakDetail> getDetailsForBreakId(final Long repaymentBreakId) {
        return (List<RepaymentBreakDetail>) findRepaymentBreakDetailList("this._repaymentBreak._id == :breakId order by this._confirmedDate ascending", repaymentBreakId);
    }

    public RepaymentBreakDetail getLatestForAccount(Account account){
        StringBuilder whereClause = new StringBuilder();
        whereClause.append("_affectedInsuranceBookingGroup != null ");
        whereClause.append("&& _repaymentBreak._account == param1 ");
        whereClause.append("PARAMETERS de.smava.webapp.account.domain.Account param1 ");
        whereClause.append("ORDER BY _confirmedDate DESCENDING");

        RepaymentBreakDetail lastRepayment = null;

        Collection<RepaymentBreakDetail> details = findRepaymentBreakDetailList(whereClause.toString(), account);
        if (!details.isEmpty()) {
            lastRepayment = details.iterator().next();
        }
        return lastRepayment;
    }

    public Collection<RepaymentBreakDetail> getRepaymentBreakDetails(Long repaymentBreakId) {
        String whereClause = "_repaymentBreak._id == " + repaymentBreakId;
        return this.findRepaymentBreakDetailList(whereClause);
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
