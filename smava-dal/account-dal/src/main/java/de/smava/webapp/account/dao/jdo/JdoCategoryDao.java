//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(category)}

import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.CategoryOrder;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.account.todo.dao.CategoryDao;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Categorys'.
 *
 * @author generator
 */
@Repository(value = "categoryDao")
public class JdoCategoryDao extends JdoBaseDao implements CategoryDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCategoryDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(category)}
    private static final long serialVersionUID = -1063395066344941652L;
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the category identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Category load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategory - start: id=" + id);
        }
        Category result = getEntity(Category.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategory - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Category getCategory(Long id) {
        return load(id);
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Category entity = findUniqueEntity(Category.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the category.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Category category) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCategory: " + category);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(category)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(category);
    }

    /**
     * @deprecated Use {@link #save(Category) instead}
     */
    public Long saveCategory(Category category) {
        return save(category);
    }

    /**
     * Deletes an category, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the category
     */
    public void deleteCategory(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteCategory: " + id);
        }
        deleteEntity(Category.class, id);
    }

    /**
     * Retrieves all 'Category' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Category> getCategoryList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - start");
        }
        Collection<Category> result = getEntities(Category.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Category' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Category> getCategoryList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - start: pageable=" + pageable);
        }
        Collection<Category> result = getEntities(Category.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Category' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Category> getCategoryList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - start: sortable=" + sortable);
        }
        Collection<Category> result = getEntities(Category.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Category' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Category> getCategoryList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Category> result = getEntities(Category.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Category' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Category> findCategoryList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - start: whereClause=" + whereClause);
        }
        Collection<Category> result = findEntities(Category.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Category' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Category> findCategoryList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Category> result = findEntities(Category.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'Category' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Category> findCategoryList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Category> result = findEntities(Category.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Category' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Category> findCategoryList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Category> result = findEntities(Category.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Category' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Category> findCategoryList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Category> result = findEntities(Category.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Category' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Category> findCategoryList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Category> result = findEntities(Category.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findCategoryList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Category' instances.
     */
    public long getCategoryCount() {
        long result = getEntityCount(Category.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Category' instances which match the given whereClause.
     */
    public long getCategoryCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Category.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Category' instances which match the given whereClause together with params specified in object array.
     */
    public long getCategoryCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Category.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getCategoryCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(category)}
    //
    /**
     * Attaches a Order to the Category instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    @Override
    public void attachOrder(Category category, Order order) {

        saveEntity(category);
        saveEntity(order);

        CategoryOrder co = new CategoryOrder(category, order);
        category.getCategoryOrders().add(co);
        order.getCategoryOrders().add(co);

        saveEntity(co);

    }

    /**
     {@inheritDoc}
     */
    @Override
    public void attachOrder(Category category, Order order, boolean main) {
        saveEntity(category);
        saveEntity(order);

        CategoryOrder co = new CategoryOrder(category, order, main);
        category.getCategoryOrders().add(co);
        order.getCategoryOrders().add(co);

        saveEntity(co);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public void detachOrder(Category category, Order order) {
        saveEntity(order);
        saveEntity(category);

        Collection<CategoryOrder> deletableCategoryOrders = new HashSet<CategoryOrder>();

        for (CategoryOrder co : category.getCategoryOrders()) {
            if (co.getOrder().equals(order) && co.getCategory().equals(category)) {
                deletableCategoryOrders.add(co);
            }
        }
        for (CategoryOrder categoryOrder : deletableCategoryOrders) {
            deleteEntity(CategoryOrder.class, categoryOrder.getId());
        }

        category.getCategoryOrders().removeAll(deletableCategoryOrders);
        order.getCategoryOrders().removeAll(deletableCategoryOrders);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Category> getRootCategories() {
        OqlTerm oql = OqlTerm.newTerm();
        oql.isNull("_parent");
        oql.setSortBy("_name");
        return findCategoryList(oql.toString());
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Category> getShortlistedCategories() {
        return findCategoryList("_shortlisted == true");
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<Category> getChildren(Category parent) {
        Collection<Category> result;
        if (parent == null) {
            result = new ArrayList<Category>(0);
        } else {
            OqlTerm oql = OqlTerm.newTerm();
            oql.equals("_parent", parent);
            oql.setSortBy("_name");
            result = findCategoryList(oql.toString(), parent);
        }
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public void attachCategory(Category parent, Category child) {
        parent.getChildren().add(child);
        saveEntity(parent);
    }

    /**
     {@inheritDoc}
     */
    @Override
    public void detachCategory(Category parent, Category child) {
        parent.getChildren().remove(child);
        saveEntity(parent);
    }



    /**
     {@inheritDoc}
     */
    @Override
    public Long getCategoryIdByName(String name) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);

        Long id = executeNamedQuery(Category.class, "getCategoryIdByName", params, Long.class, true);

        return id;
    }

    @Override
    public Collection<Category> getOrderedCategories() {
        Query query = getPersistenceManager().newQuery(Category.class);

        query.setOrdering("_name" + " ASC");
        return (Collection<Category>) query.execute();
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
