//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account provider)}
import de.smava.webapp.account.domain.history.BankAccountProviderHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankAccountProviders'.
 *
 * @author generator
 */
public class BankAccountProvider extends BankAccountProviderHistory  implements Comparable<BankAccountProvider> {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(bank account provider)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected String _schufaUsername;
        protected String _schufaPassword;
        protected de.smava.webapp.account.domain.BankAccountProviderType _type;
        
                            /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'schufaUsername'.
     */
    public void setSchufaUsername(String schufaUsername) {
        if (!_schufaUsernameIsSet) {
            _schufaUsernameIsSet = true;
            _schufaUsernameInitVal = getSchufaUsername();
        }
        registerChange("schufaUsername", _schufaUsernameInitVal, schufaUsername);
        _schufaUsername = schufaUsername;
    }
                        
    /**
     * Returns the property 'schufaUsername'.
     */
    public String getSchufaUsername() {
        return _schufaUsername;
    }
                                    /**
     * Setter for the property 'schufaPassword'.
     */
    public void setSchufaPassword(String schufaPassword) {
        if (!_schufaPasswordIsSet) {
            _schufaPasswordIsSet = true;
            _schufaPasswordInitVal = getSchufaPassword();
        }
        registerChange("schufaPassword", _schufaPasswordInitVal, schufaPassword);
        _schufaPassword = schufaPassword;
    }
                        
    /**
     * Returns the property 'schufaPassword'.
     */
    public String getSchufaPassword() {
        return _schufaPassword;
    }
                                            
    /**
     * Setter for the property 'type'.
     */
    public void setType(de.smava.webapp.account.domain.BankAccountProviderType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     */
    public de.smava.webapp.account.domain.BankAccountProviderType getType() {
        return _type;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BankAccountProvider.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _schufaUsername=").append(_schufaUsername);
            builder.append("\n    _schufaPassword=").append(_schufaPassword);
            builder.append("\n}");
        } else {
            builder.append(BankAccountProvider.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BankAccountProvider asBankAccountProvider() {
        return this;
    }
}
