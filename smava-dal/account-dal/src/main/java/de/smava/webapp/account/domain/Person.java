package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.PersonHistory;

import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'Persons'.
 */
public class Person extends PersonHistory  {

    public Person() {
        super();
        _type = PRIMARY_PERSON_TYPE;
    }

    public Person(int type) {
        super();
        _type = type;
    }

        protected String _title;
        protected Account _account;
        protected Date _creationDate;
        protected String _salutation;
        protected String _firstName;
        protected String _lastName;
        protected String _birthName;
        protected Date _dateOfBirth;
        protected String _countryOfBirth;
        protected String _placeOfBirth;
        protected String _citizenship;
        protected String _familyStatus;
        protected String _pesel;
        protected String _identityCardNumber;
        protected String _nip;
        protected String _educationEarned;
        protected Collection<IdCard> _idCardHistory;
        protected Date _validUntil;
        protected int _type;
        protected Date _postidentFee;
        protected Integer _numberOfOtherInHouseholdCheck;
        protected Integer _confession;
        protected Boolean _taxComplianceAct;
        protected Boolean _taxesPaidInUsa;
        
                            /**
     * Setter for the property 'title'.
     *
     * 
     *
     */
    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = getTitle();
        }
        registerChange("title", _titleInitVal, title);
        _title = title;
    }
                        
    /**
     * Returns the property 'title'.
     *
     * 
     *
     */
    public String getTitle() {
        return _title;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'salutation'.
     *
     * 
     *
     */
    public void setSalutation(String salutation) {
        if (!_salutationIsSet) {
            _salutationIsSet = true;
            _salutationInitVal = getSalutation();
        }
        registerChange("salutation", _salutationInitVal, salutation);
        _salutation = salutation;
    }
                        
    /**
     * Returns the property 'salutation'.
     *
     * 
     *
     */
    public String getSalutation() {
        return _salutation;
    }
                                    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("first name", _firstNameInitVal, firstName);
        _firstName = firstName;
    }
                        
    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    public String getFirstName() {
        return _firstName;
    }
                                    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("last name", _lastNameInitVal, lastName);
        _lastName = lastName;
    }
                        
    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    public String getLastName() {
        return _lastName;
    }
                                    /**
     * Setter for the property 'birth name'.
     *
     * 
     *
     */
    public void setBirthName(String birthName) {
        if (!_birthNameIsSet) {
            _birthNameIsSet = true;
            _birthNameInitVal = getBirthName();
        }
        registerChange("birth name", _birthNameInitVal, birthName);
        _birthName = birthName;
    }
                        
    /**
     * Returns the property 'birth name'.
     *
     * 
     *
     */
    public String getBirthName() {
        return _birthName;
    }
                                    /**
     * Setter for the property 'date of birth'.
     *
     * 
     *
     */
    public void setDateOfBirth(Date dateOfBirth) {
        if (!_dateOfBirthIsSet) {
            _dateOfBirthIsSet = true;
            _dateOfBirthInitVal = getDateOfBirth();
        }
        registerChange("date of birth", _dateOfBirthInitVal, dateOfBirth);
        _dateOfBirth = dateOfBirth;
    }
                        
    /**
     * Returns the property 'date of birth'.
     *
     * 
     *
     */
    public Date getDateOfBirth() {
        return _dateOfBirth;
    }
                                    /**
     * Setter for the property 'country of birth'.
     *
     * 
     *
     */
    public void setCountryOfBirth(String countryOfBirth) {
        if (!_countryOfBirthIsSet) {
            _countryOfBirthIsSet = true;
            _countryOfBirthInitVal = getCountryOfBirth();
        }
        registerChange("country of birth", _countryOfBirthInitVal, countryOfBirth);
        _countryOfBirth = countryOfBirth;
    }
                        
    /**
     * Returns the property 'country of birth'.
     *
     * 
     *
     */
    public String getCountryOfBirth() {
        return _countryOfBirth;
    }
                                    /**
     * Setter for the property 'place of birth'.
     *
     * 
     *
     */
    public void setPlaceOfBirth(String placeOfBirth) {
        if (!_placeOfBirthIsSet) {
            _placeOfBirthIsSet = true;
            _placeOfBirthInitVal = getPlaceOfBirth();
        }
        registerChange("place of birth", _placeOfBirthInitVal, placeOfBirth);
        _placeOfBirth = placeOfBirth;
    }
                        
    /**
     * Returns the property 'place of birth'.
     *
     * 
     *
     */
    public String getPlaceOfBirth() {
        return _placeOfBirth;
    }
                                    /**
     * Setter for the property 'citizenship'.
     *
     * 
     *
     */
    public void setCitizenship(String citizenship) {
        if (!_citizenshipIsSet) {
            _citizenshipIsSet = true;
            _citizenshipInitVal = getCitizenship();
        }
        registerChange("citizenship", _citizenshipInitVal, citizenship);
        _citizenship = citizenship;
    }
                        
    /**
     * Returns the property 'citizenship'.
     *
     * 
     *
     */
    public String getCitizenship() {
        return _citizenship;
    }
                                    /**
     * Setter for the property 'family status'.
     *
     * 
     *
     */
    public void setFamilyStatus(String familyStatus) {
        if (!_familyStatusIsSet) {
            _familyStatusIsSet = true;
            _familyStatusInitVal = getFamilyStatus();
        }
        registerChange("family status", _familyStatusInitVal, familyStatus);
        _familyStatus = familyStatus;
    }
                        
    /**
     * Returns the property 'family status'.
     *
     * 
     *
     */
    public String getFamilyStatus() {
        return _familyStatus;
    }
                                    /**
     * Setter for the property 'pesel'.
     *
     * 
     *
     */
    public void setPesel(String pesel) {
        if (!_peselIsSet) {
            _peselIsSet = true;
            _peselInitVal = getPesel();
        }
        registerChange("pesel", _peselInitVal, pesel);
        _pesel = pesel;
    }
                        
    /**
     * Returns the property 'pesel'.
     *
     * 
     *
     */
    public String getPesel() {
        return _pesel;
    }
                                    /**
     * Setter for the property 'identity card number'.
     *
     * 
     *
     */
    public void setIdentityCardNumber(String identityCardNumber) {
        if (!_identityCardNumberIsSet) {
            _identityCardNumberIsSet = true;
            _identityCardNumberInitVal = getIdentityCardNumber();
        }
        registerChange("identity card number", _identityCardNumberInitVal, identityCardNumber);
        _identityCardNumber = identityCardNumber;
    }
                        
    /**
     * Returns the property 'identity card number'.
     *
     * 
     *
     */
    public String getIdentityCardNumber() {
        return _identityCardNumber;
    }
                                    /**
     * Setter for the property 'nip'.
     *
     * 
     *
     */
    public void setNip(String nip) {
        if (!_nipIsSet) {
            _nipIsSet = true;
            _nipInitVal = getNip();
        }
        registerChange("nip", _nipInitVal, nip);
        _nip = nip;
    }
                        
    /**
     * Returns the property 'nip'.
     *
     * 
     *
     */
    public String getNip() {
        return _nip;
    }
                                    /**
     * Setter for the property 'education earned'.
     *
     * 
     *
     */
    public void setEducationEarned(String educationEarned) {
        if (!_educationEarnedIsSet) {
            _educationEarnedIsSet = true;
            _educationEarnedInitVal = getEducationEarned();
        }
        registerChange("education earned", _educationEarnedInitVal, educationEarned);
        _educationEarned = educationEarned;
    }
                        
    /**
     * Returns the property 'education earned'.
     *
     * 
     *
     */
    public String getEducationEarned() {
        return _educationEarned;
    }
                                            
    /**
     * Setter for the property 'id card history'.
     *
     * 
     *
     */
    public void setIdCardHistory(Collection<IdCard> idCardHistory) {
        _idCardHistory = idCardHistory;
    }
            
    /**
     * Returns the property 'id card history'.
     *
     * 
     *
     */
    public Collection<IdCard> getIdCardHistory() {
        return _idCardHistory;
    }
                                    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    public Date getValidUntil() {
        return _validUntil;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(int type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public int getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'postident fee'.
     *
     * 
     *
     */
    public void setPostidentFee(Date postidentFee) {
        if (!_postidentFeeIsSet) {
            _postidentFeeIsSet = true;
            _postidentFeeInitVal = getPostidentFee();
        }
        registerChange("postident fee", _postidentFeeInitVal, postidentFee);
        _postidentFee = postidentFee;
    }
                        
    /**
     * Returns the property 'postident fee'.
     *
     * 
     *
     */
    public Date getPostidentFee() {
        return _postidentFee;
    }
                                            
    /**
     * Setter for the property 'number of other in household check'.
     *
     * 
     *
     */
    public void setNumberOfOtherInHouseholdCheck(Integer numberOfOtherInHouseholdCheck) {
        _numberOfOtherInHouseholdCheck = numberOfOtherInHouseholdCheck;
    }
            
    /**
     * Returns the property 'number of other in household check'.
     *
     * 
     *
     */
    public Integer getNumberOfOtherInHouseholdCheck() {
        return _numberOfOtherInHouseholdCheck;
    }
                                            
    /**
     * Setter for the property 'confession'.
     *
     * 
     *
     */
    public void setConfession(Integer confession) {
        _confession = confession;
    }
            
    /**
     * Returns the property 'confession'.
     *
     * 
     *
     */
    public Integer getConfession() {
        return _confession;
    }
                                            
    /**
     * Setter for the property 'tax compliance act'.
     *
     * 
     *
     */
    public void setTaxComplianceAct(Boolean taxComplianceAct) {
        _taxComplianceAct = taxComplianceAct;
    }
            
    /**
     * Returns the property 'tax compliance act'.
     *
     * 
     *
     */
    public Boolean getTaxComplianceAct() {
        return _taxComplianceAct;
    }
                                            
    /**
     * Setter for the property 'taxes paid in usa'.
     *
     * 
     *
     */
    public void setTaxesPaidInUsa(Boolean taxesPaidInUsa) {
        _taxesPaidInUsa = taxesPaidInUsa;
    }
            
    /**
     * Returns the property 'taxes paid in usa'.
     *
     * 
     *
     */
    public Boolean getTaxesPaidInUsa() {
        return _taxesPaidInUsa;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Person.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _title=").append(_title);
            builder.append("\n    _salutation=").append(_salutation);
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _birthName=").append(_birthName);
            builder.append("\n    _dateOfBirth=").append(_dateOfBirth);
            builder.append("\n    _countryOfBirth=").append(_countryOfBirth);
            builder.append("\n    _placeOfBirth=").append(_placeOfBirth);
            builder.append("\n    _citizenship=").append(_citizenship);
            builder.append("\n    _familyStatus=").append(_familyStatus);
            builder.append("\n    _pesel=").append(_pesel);
            builder.append("\n    _identityCardNumber=").append(_identityCardNumber);
            builder.append("\n    _nip=").append(_nip);
            builder.append("\n    _educationEarned=").append(_educationEarned);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(Person.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
