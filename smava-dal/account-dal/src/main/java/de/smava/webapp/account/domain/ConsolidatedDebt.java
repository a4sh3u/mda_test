package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.ConsolidatedDebtHistory;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;

/**
 * The domain object that represents 'ConsolidatedDebts'.
 */
public class ConsolidatedDebt extends ConsolidatedDebtHistory  {

    public ConsolidatedDebt() {
        super();
        _bankAccounts = new LinkedHashSet<BankAccount>();
        _documents = new LinkedHashSet<Document>();
    }

        protected Date _creationDate;
        protected Double _monthlyInstallment;
        protected Integer _monthsRemaining;
        protected Double _interestRate;
        protected String _loanId;
        protected Double _debtAmount;
        protected Date _dueDate;
        protected Date _startDate;
        protected String _state;
        protected String _creditType;
        protected boolean _consolidation;
        protected Collection<BankAccount> _bankAccounts;
        protected Collection<Document> _documents;

                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'monthly installment'.
     */
    public void setMonthlyInstallment(Double monthlyInstallment) {
        _monthlyInstallment = monthlyInstallment;
    }
            
    /**
     * Returns the property 'monthly installment'.
     */
    public Double getMonthlyInstallment() {
        return _monthlyInstallment;
    }
                                            
    /**
     * Setter for the property 'months remaining'.
     */
    public void setMonthsRemaining(Integer monthsRemaining) {
        _monthsRemaining = monthsRemaining;
    }
            
    /**
     * Returns the property 'months remaining'.
     */
    public Integer getMonthsRemaining() {
        return _monthsRemaining;
    }
                                            
    /**
     * Setter for the property 'interest rate'.
     */
    public void setInterestRate(Double interestRate) {
        _interestRate = interestRate;
    }
            
    /**
     * Returns the property 'interest rate'.
     */
    public Double getInterestRate() {
        return _interestRate;
    }
                                    /**
     * Setter for the property 'loan id'.
     */
    public void setLoanId(String loanId) {
        if (!_loanIdIsSet) {
            _loanIdIsSet = true;
            _loanIdInitVal = getLoanId();
        }
        registerChange("loan id", _loanIdInitVal, loanId);
        _loanId = loanId;
    }
                        
    /**
     * Returns the property 'loan id'.
     */
    public String getLoanId() {
        return _loanId;
    }
                                            
    /**
     * Setter for the property 'debt amount'.
     */
    public void setDebtAmount(Double debtAmount) {
        _debtAmount = debtAmount;
    }
            
    /**
     * Returns the property 'debt amount'.
     */
    public Double getDebtAmount() {
        return _debtAmount;
    }
                                    /**
     * Setter for the property 'due date'.
     */
    public void setDueDate(Date dueDate) {
        if (!_dueDateIsSet) {
            _dueDateIsSet = true;
            _dueDateInitVal = getDueDate();
        }
        registerChange("due date", _dueDateInitVal, dueDate);
        _dueDate = dueDate;
    }
                        
    /**
     * Returns the property 'due date'.
     */
    public Date getDueDate() {
        return _dueDate;
    }
                                    /**
     * Setter for the property 'start date'.
     */
    public void setStartDate(Date startDate) {
        if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = getStartDate();
        }
        registerChange("start date", _startDateInitVal, startDate);
        _startDate = startDate;
    }
                        
    /**
     * Returns the property 'start date'.
     */
    public Date getStartDate() {
        return _startDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'credit type'.
     */
    public void setCreditType(String creditType) {
        if (!_creditTypeIsSet) {
            _creditTypeIsSet = true;
            _creditTypeInitVal = getCreditType();
        }
        registerChange("credit type", _creditTypeInitVal, creditType);
        _creditType = creditType;
    }
                        
    /**
     * Returns the property 'credit type'.
     */
    public String getCreditType() {
        return _creditType;
    }
                                    /**
     * Setter for the property 'consolidation'.
     */
    public void setConsolidation(boolean consolidation) {
        if (!_consolidationIsSet) {
            _consolidationIsSet = true;
            _consolidationInitVal = getConsolidation();
        }
        registerChange("consolidation", _consolidationInitVal, consolidation);
        _consolidation = consolidation;
    }
                        
    /**
     * Returns the property 'consolidation'.
     */
    public boolean getConsolidation() {
        return _consolidation;
    }
                                            
    /**
     * Setter for the property 'bank accounts'.
     */
    public void setBankAccounts(Collection<BankAccount> bankAccounts) {
        _bankAccounts = bankAccounts;
    }
            
    /**
     * Returns the property 'bank accounts'.
     */
    public Collection<BankAccount> getBankAccounts() {
        return _bankAccounts;
    }
                                            
    /**
     * Setter for the property 'documents'.
     */
    public void setDocuments(Collection<Document> documents) {
        _documents = documents;
    }
            
    /**
     * Returns the property 'documents'.
     */
    public Collection<Document> getDocuments() {
        return _documents;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ConsolidatedDebt.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _loanId=").append(_loanId);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _creditType=").append(_creditType);
            builder.append("\n    _consolidation=").append(_consolidation);
            builder.append("\n}");
        } else {
            builder.append(ConsolidatedDebt.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
