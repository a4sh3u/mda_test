package de.smava.webapp.account.domain.abstracts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.IdCard;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.account.domain.SchufaScore;
import de.smava.webapp.account.domain.interfaces.PersonEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.util.PersonConstants;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import java.util.*;

/**
 * The domain object that represents 'Persons'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractPerson
    extends KreditPrivatEntity implements PersonEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractPerson.class);

    public static final String HIGHER_EDUCATION = "HIGHER";
    public static final String MEDIUM_EDUCATION = "MEDIUM";
    public static final String PROFESSIONAL_EDUCATION = "PROFESSIONAL";
    public static final String BASIC_EDUCATION = "BASIC";

    public static final String BELOW_25 = "BELOW_25";
    public static final String BTW25_30 = "BTW25_30";
    public static final String ABOVE_30 = "ABOVE_30";

    public static final String MARRIED = "MARRIED";
    public static final String WIDOWED = "WIDOWED";
    public static final String UNMARRIED = "UNMARRIED";
    public static final String DIVORCED = "DIVORCED";
    public static final String COHABITATION = "COHABITATION";
    public static final String SEPARATED = "SEPARATED";

    public static final String NO_TITLE = "NO_TITLE";
    public static final String DR_TITLE = "DR";
    public static final String PROF_TITLE = "PROF";
    public static final String PROF_DR_TITLE = "PROF_DR";

    public static final int PRIMARY_PERSON_TYPE = 1;
    public static final int SECONDARY_PERSON_TYPE = 2;

    public static final String DEFAULT_CITIZENSHIP = "default_DE";

    public static final String SEPARATOR = " ";

    @JsonIgnore
    public String getSchufaGenderName() {
        String sex = SchufaScore.GENDER_UNKOWN;
        if (PersonConstants.MALE_SALUTATION.equals(getSalutation())) {
            sex = SchufaScore.GENDER_MALE;
        } else if (PersonConstants.FEMALE_SALUTATION.equals(getSalutation())) {
            sex = SchufaScore.GENDER_FEMALE;
        }

        return sex;
    }

    public IdCard getCurrentIdCard() {
        IdCard result = null;
        if (getIdCardHistory() != null) {
            for (IdCard idCard : getIdCardHistory()) {
                if (idCard.getValidUntil() == null) {
                    result = idCard;
                    break;
                }
            }
        }
        return result;
    }

    public void setCurrentIdCard(IdCard idCard) {
        if (getCurrentIdCard() != null) {
            getCurrentIdCard().setValidUntil(CurrentDate.getDate());
        }
        if (getIdCardHistory() == null) {
            setIdCardHistory(new HashSet<IdCard>());
        }
        getIdCardHistory().add(idCard);
    }

    public boolean isCoBorrower() {
        return SECONDARY_PERSON_TYPE == getType();
    }

    public String getFullName() {
        return (getFirstName() + " " + getLastName()).trim();
    }

    public String getInitials() {
        final StringBuilder sb = new StringBuilder();
        sb.append(getFirstName() == null ? "" : getFirstName().charAt(0));
        sb.append(getLastName() == null ? "" : getLastName().charAt(0));
        return sb.toString();
    }

    /**Subject to change : compare persons semantically
     *
     * @param otherPerson
     * @return
     */
    public boolean functionallyEqual(Person otherPerson){

        if ( otherPerson == null){
            return false;
        }
        if(super.equals(otherPerson)) return true;

        boolean functionallyEqual;
        try {
            functionallyEqual = true;

            functionallyEqual = functionallyEqual && getSalutation().equals(otherPerson.getSalutation());
            functionallyEqual = functionallyEqual && getFirstName().equals(otherPerson.getFirstName());
            functionallyEqual = functionallyEqual && getLastName().equals(otherPerson.getLastName());
            if (StringUtils.isNotBlank(getBirthName())) {
                functionallyEqual = functionallyEqual && getBirthName().equals(otherPerson.getBirthName());
            } else {
                functionallyEqual = functionallyEqual && (StringUtils.isBlank(otherPerson.getBirthName()));
            }
            functionallyEqual = functionallyEqual && getCitizenship().equals(otherPerson.getCitizenship());
            functionallyEqual = functionallyEqual && getCountryOfBirth().equals(otherPerson.getCountryOfBirth());
            functionallyEqual = functionallyEqual && getPlaceOfBirth().equals(otherPerson.getPlaceOfBirth());
            functionallyEqual = functionallyEqual && getCitizenship().equals(otherPerson.getCitizenship());
            functionallyEqual = functionallyEqual && getFamilyStatus().equals(otherPerson.getFamilyStatus());

            //do vague date comparison using joda
            DateTime thisBirthDate = new DateTime(getDateOfBirth());
            DateTime otherPersonBirthDate = new DateTime(otherPerson.getDateOfBirth());

            LocalDate thisLD = thisBirthDate.toLocalDate();
            LocalDate otherLD = otherPersonBirthDate.toLocalDate();

            functionallyEqual = functionallyEqual && (thisLD.compareTo(otherLD) == 0);
        } catch (Exception e) {
            functionallyEqual = false;
        }

        return functionallyEqual;
    }

    /**Subject to change : compare persons semantically
     *
     * @param otherPerson
     * @return
     */
    public boolean basicallyEqual(Person otherPerson){

        if ( otherPerson == null){
            return false;
        }
        if(super.equals(otherPerson)) return true;

        boolean basicallyEqual;
        try {
            basicallyEqual = true;

            basicallyEqual = basicallyEqual && getSalutation().equals(otherPerson.getSalutation());
            basicallyEqual = basicallyEqual && getFirstName().equals(otherPerson.getFirstName());
            basicallyEqual = basicallyEqual && getLastName().equals(otherPerson.getLastName());
            DateTime thisBirthDate = new DateTime(getDateOfBirth());
            DateTime otherPersonBirthDate = new DateTime(otherPerson.getDateOfBirth());

            LocalDate thisLD = thisBirthDate.toLocalDate();
            LocalDate otherLD = otherPersonBirthDate.toLocalDate();

            basicallyEqual = basicallyEqual && (thisLD.compareTo(otherLD) == 0);
        } catch (Exception e) {
            basicallyEqual = false;
        }

        return basicallyEqual;
    }

    public boolean hasBirthday() {
        Date dateOfBirth = getDateOfBirth();

        if (dateOfBirth == null) {
            return false;
        }

        Calendar now = CurrentDate.getCalendar();
        Calendar birthday = Calendar.getInstance();
        birthday.setTime(dateOfBirth);

        return now.get(Calendar.MONTH) == birthday.get(Calendar.MONTH)
                && now.get(Calendar.DAY_OF_MONTH) == birthday.get(Calendar.DAY_OF_MONTH);
    }

    public int getAge() {
        LocalDate now = new LocalDate(CurrentDate.getTime());
        LocalDate birthday = new LocalDate(getDateOfBirth());

        Years ys = Years.yearsBetween(birthday, now);
        return ys.getYears();
    }

    public String getShortFirstName() {
        return StringUtils.substringBefore(getFirstName(), SEPARATOR);
    }

    public String getMiddleName() {
        return StringUtils.substringAfter(getFirstName(), SEPARATOR);
    }

    public List<? extends Entity> getModifier() {
        List<Account> accounts = new LinkedList<Account>();
        accounts.add(this.getAccount());
        return accounts;
    }

}

