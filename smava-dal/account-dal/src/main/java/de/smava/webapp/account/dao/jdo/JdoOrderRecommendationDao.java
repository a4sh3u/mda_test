//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(order recommendation)}

import de.smava.webapp.account.dao.OrderRecommendationDao;
import de.smava.webapp.account.domain.OrderRecommendation;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'OrderRecommendations'.
 *
 * @author generator
 */
@Repository(value = "orderRecommendationDao")
public class JdoOrderRecommendationDao extends JdoBaseDao implements OrderRecommendationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoOrderRecommendationDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(order recommendation)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the order recommendation identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public OrderRecommendation load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendation - start: id=" + id);
        }
        OrderRecommendation result = getEntity(OrderRecommendation.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendation - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public OrderRecommendation getOrderRecommendation(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	OrderRecommendation entity = findUniqueEntity(OrderRecommendation.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the order recommendation.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(OrderRecommendation orderRecommendation) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveOrderRecommendation: " + orderRecommendation);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(order recommendation)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(orderRecommendation);
    }

    /**
     * @deprecated Use {@link #save(OrderRecommendation) instead}
     */
    public Long saveOrderRecommendation(OrderRecommendation orderRecommendation) {
        return save(orderRecommendation);
    }

    /**
     * Deletes an order recommendation, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the order recommendation
     */
    public void deleteOrderRecommendation(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteOrderRecommendation: " + id);
        }
        deleteEntity(OrderRecommendation.class, id);
    }

    /**
     * Retrieves all 'OrderRecommendation' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<OrderRecommendation> getOrderRecommendationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - start");
        }
        Collection<OrderRecommendation> result = getEntities(OrderRecommendation.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'OrderRecommendation' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<OrderRecommendation> getOrderRecommendationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - start: pageable=" + pageable);
        }
        Collection<OrderRecommendation> result = getEntities(OrderRecommendation.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'OrderRecommendation' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<OrderRecommendation> getOrderRecommendationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - start: sortable=" + sortable);
        }
        Collection<OrderRecommendation> result = getEntities(OrderRecommendation.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'OrderRecommendation' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<OrderRecommendation> getOrderRecommendationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<OrderRecommendation> result = getEntities(OrderRecommendation.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'OrderRecommendation' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<OrderRecommendation> findOrderRecommendationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - start: whereClause=" + whereClause);
        }
        Collection<OrderRecommendation> result = findEntities(OrderRecommendation.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'OrderRecommendation' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<OrderRecommendation> findOrderRecommendationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<OrderRecommendation> result = findEntities(OrderRecommendation.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'OrderRecommendation' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<OrderRecommendation> findOrderRecommendationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<OrderRecommendation> result = findEntities(OrderRecommendation.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'OrderRecommendation' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<OrderRecommendation> findOrderRecommendationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<OrderRecommendation> result = findEntities(OrderRecommendation.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'OrderRecommendation' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<OrderRecommendation> findOrderRecommendationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<OrderRecommendation> result = findEntities(OrderRecommendation.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'OrderRecommendation' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<OrderRecommendation> findOrderRecommendationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<OrderRecommendation> result = findEntities(OrderRecommendation.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderRecommendationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'OrderRecommendation' instances.
     */
    public long getOrderRecommendationCount() {
        long result = getEntityCount(OrderRecommendation.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'OrderRecommendation' instances which match the given whereClause.
     */
    public long getOrderRecommendationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(OrderRecommendation.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'OrderRecommendation' instances which match the given whereClause together with params specified in object array.
     */
    public long getOrderRecommendationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(OrderRecommendation.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getOrderRecommendationCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(order recommendation)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
