package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractCategoryImage;




/**
 * The domain object that has all history aggregation related fields for 'CategoryImages'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CategoryImageHistory extends AbstractCategoryImage {

    protected transient String _imageInitVal;
    protected transient boolean _imageIsSet;
    protected transient int _imageHeightInitVal;
    protected transient boolean _imageHeightIsSet;
    protected transient int _imageWidthInitVal;
    protected transient boolean _imageWidthIsSet;


	
    /**
     * Returns the initial value of the property 'image'.
     */
    public String imageInitVal() {
        String result;
        if (_imageIsSet) {
            result = _imageInitVal;
        } else {
            result = getImage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image'.
     */
    public boolean imageIsDirty() {
        return !valuesAreEqual(imageInitVal(), getImage());
    }

    /**
     * Returns true if the setter method was called for the property 'image'.
     */
    public boolean imageIsSet() {
        return _imageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image height'.
     */
    public int imageHeightInitVal() {
        int result;
        if (_imageHeightIsSet) {
            result = _imageHeightInitVal;
        } else {
            result = getImageHeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image height'.
     */
    public boolean imageHeightIsDirty() {
        return !valuesAreEqual(imageHeightInitVal(), getImageHeight());
    }

    /**
     * Returns true if the setter method was called for the property 'image height'.
     */
    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image width'.
     */
    public int imageWidthInitVal() {
        int result;
        if (_imageWidthIsSet) {
            result = _imageWidthInitVal;
        } else {
            result = getImageWidth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image width'.
     */
    public boolean imageWidthIsDirty() {
        return !valuesAreEqual(imageWidthInitVal(), getImageWidth());
    }

    /**
     * Returns true if the setter method was called for the property 'image width'.
     */
    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }

}
