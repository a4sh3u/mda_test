package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.FeedbackHistory;

import java.util.Date;
import java.util.Set;

/**
 * The domain object that represents 'Feedbacks'.
 */
public class Feedback extends FeedbackHistory  {

        protected Date _creationDate;
        protected Date _releaseApprovalDate;
        protected String _text;
        protected String _type;
        protected Boolean _release;
        protected Set<Order> _orders;

                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'release approval date'.
     */
    public void setReleaseApprovalDate(Date releaseApprovalDate) {
        if (!_releaseApprovalDateIsSet) {
            _releaseApprovalDateIsSet = true;
            _releaseApprovalDateInitVal = getReleaseApprovalDate();
        }
        registerChange("release approval date", _releaseApprovalDateInitVal, releaseApprovalDate);
        _releaseApprovalDate = releaseApprovalDate;
    }
                        
    /**
     * Returns the property 'release approval date'.
     */
    public Date getReleaseApprovalDate() {
        return _releaseApprovalDate;
    }
                                    /**
     * Setter for the property 'text'.
     */
    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = getText();
        }
        registerChange("text", _textInitVal, text);
        _text = text;
    }
                        
    /**
     * Returns the property 'text'.
     */
    public String getText() {
        return _text;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'release'.
     */
    public void setRelease(Boolean release) {
        _release = release;
    }
            
    /**
     * Returns the property 'release'.
     */
    public Boolean getRelease() {
        return _release;
    }
                                            
    /**
     * Setter for the property 'orders'.
     */
    public void setOrders(Set<Order> orders) {
        _orders = orders;
    }
            
    /**
     * Returns the property 'orders'.
     */
    public Set<Order> getOrders() {
        return _orders;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Feedback.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _text=").append(_text);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(Feedback.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
