/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package de.smava.webapp.account.dao;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rfiedler
 */
public class GenericQueryResult {

    private List<String> _columnNames = new ArrayList<String>();
    private List<List<Object>> _values = new ArrayList<List<Object>>();

    /**
     * @return the columnNames
     */
    public List<String> getColumnNames() {
        return _columnNames;
    }

    /**
     * @param columnNames the columnNames to set
     */
    public void setColumnNames(List<String> columnNames) {
        this._columnNames = columnNames;
    }

    /**
     * @return the values
     */
    public List<List<Object>> getValues() {
        return _values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(List<List<Object>> values) {
        this._values = values;
    }




}
