package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.InsurancePoolPayoutRunEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'InsurancePoolPayoutRuns'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractInsurancePoolPayoutRun
    extends KreditPrivatEntity implements InsurancePoolPayoutRunEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractInsurancePoolPayoutRun.class);

}

