package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BidSearchHistory;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * The domain object that represents 'BidSearchs'.
 */
public class BidSearch extends BidSearchHistory  {

    public BidSearch() {
        if (getAgeGroups() == null) {
            setAgeGroups(new HashSet<BidSearchAgeGroup>());
        }
    }

    @Override
    public void setAmount(double amount) {
        _amount = amount;
    }

    @Override
    public double getAmount() {
        // TODO Auto-generated method stub
        return _amount;
    }

        protected Date _validUntil;
        protected Account _account;
        protected double _interestMin;
        protected double _interestMax;
        protected double _amountMin;
        protected double _amountMax;
        protected boolean _includeFinishedOrders;
        protected Double _roiMin;
        protected Double _roiMax;
        protected int _creditRateIndicator;
        protected int _matchedRate;
        protected String _searchPhrase;
        protected String _employment;
        protected boolean _isMailNotificationWanted;
        protected Collection<Integer> _integerEmployments;
        protected Integer _ageRangeMin;
        protected Integer _ageRangeMax;
        protected Boolean _exceptLatePayer;
        protected String _type;
        protected double _minInvestmentLimitPerBorrower;
        protected double _investmentLimitPerBorrower;
        protected Collection<BidSearchGroup> _bidSearchGroups;
        protected Integer _gender;
        protected de.smava.webapp.account.domain.BankAccountProvider _serviceProviderRestriction;
        protected Collection<BidSearchAgeGroup> _ageGroups;
        
                            /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'interest min'.
     */
    public void setInterestMin(double interestMin) {
        if (!_interestMinIsSet) {
            _interestMinIsSet = true;
            _interestMinInitVal = getInterestMin();
        }
        registerChange("interest min", _interestMinInitVal, interestMin);
        _interestMin = interestMin;
    }
                        
    /**
     * Returns the property 'interest min'.
     */
    public double getInterestMin() {
        return _interestMin;
    }
                                    /**
     * Setter for the property 'interest max'.
     */
    public void setInterestMax(double interestMax) {
        if (!_interestMaxIsSet) {
            _interestMaxIsSet = true;
            _interestMaxInitVal = getInterestMax();
        }
        registerChange("interest max", _interestMaxInitVal, interestMax);
        _interestMax = interestMax;
    }
                        
    /**
     * Returns the property 'interest max'.
     */
    public double getInterestMax() {
        return _interestMax;
    }
                                    /**
     * Setter for the property 'amount min'.
     */
    public void setAmountMin(double amountMin) {
        if (!_amountMinIsSet) {
            _amountMinIsSet = true;
            _amountMinInitVal = getAmountMin();
        }
        registerChange("amount min", _amountMinInitVal, amountMin);
        _amountMin = amountMin;
    }
                        
    /**
     * Returns the property 'amount min'.
     */
    public double getAmountMin() {
        return _amountMin;
    }
                                    /**
     * Setter for the property 'amount max'.
     */
    public void setAmountMax(double amountMax) {
        if (!_amountMaxIsSet) {
            _amountMaxIsSet = true;
            _amountMaxInitVal = getAmountMax();
        }
        registerChange("amount max", _amountMaxInitVal, amountMax);
        _amountMax = amountMax;
    }
                        
    /**
     * Returns the property 'amount max'.
     */
    public double getAmountMax() {
        return _amountMax;
    }
                                    /**
     * Setter for the property 'include finished orders'.
     */
    public void setIncludeFinishedOrders(boolean includeFinishedOrders) {
        if (!_includeFinishedOrdersIsSet) {
            _includeFinishedOrdersIsSet = true;
            _includeFinishedOrdersInitVal = getIncludeFinishedOrders();
        }
        registerChange("include finished orders", _includeFinishedOrdersInitVal, includeFinishedOrders);
        _includeFinishedOrders = includeFinishedOrders;
    }
                        
    /**
     * Returns the property 'include finished orders'.
     */
    public boolean getIncludeFinishedOrders() {
        return _includeFinishedOrders;
    }
                                            
    /**
     * Setter for the property 'roi min'.
     */
    public void setRoiMin(Double roiMin) {
        _roiMin = roiMin;
    }
            
    /**
     * Returns the property 'roi min'.
     */
    public Double getRoiMin() {
        return _roiMin;
    }
                                            
    /**
     * Setter for the property 'roi max'.
     */
    public void setRoiMax(Double roiMax) {
        _roiMax = roiMax;
    }
            
    /**
     * Returns the property 'roi max'.
     */
    public Double getRoiMax() {
        return _roiMax;
    }
                                    /**
     * Setter for the property 'credit rate indicator'.
     */
    public void setCreditRateIndicator(int creditRateIndicator) {
        if (!_creditRateIndicatorIsSet) {
            _creditRateIndicatorIsSet = true;
            _creditRateIndicatorInitVal = getCreditRateIndicator();
        }
        registerChange("credit rate indicator", _creditRateIndicatorInitVal, creditRateIndicator);
        _creditRateIndicator = creditRateIndicator;
    }
                        
    /**
     * Returns the property 'credit rate indicator'.
     */
    public int getCreditRateIndicator() {
        return _creditRateIndicator;
    }
                                    /**
     * Setter for the property 'matched rate'.
     */
    public void setMatchedRate(int matchedRate) {
        if (!_matchedRateIsSet) {
            _matchedRateIsSet = true;
            _matchedRateInitVal = getMatchedRate();
        }
        registerChange("matched rate", _matchedRateInitVal, matchedRate);
        _matchedRate = matchedRate;
    }
                        
    /**
     * Returns the property 'matched rate'.
     */
    public int getMatchedRate() {
        return _matchedRate;
    }
                                    /**
     * Setter for the property 'search phrase'.
     */
    public void setSearchPhrase(String searchPhrase) {
        if (!_searchPhraseIsSet) {
            _searchPhraseIsSet = true;
            _searchPhraseInitVal = getSearchPhrase();
        }
        registerChange("search phrase", _searchPhraseInitVal, searchPhrase);
        _searchPhrase = searchPhrase;
    }
                        
    /**
     * Returns the property 'search phrase'.
     */
    public String getSearchPhrase() {
        return _searchPhrase;
    }
                                    /**
     * Setter for the property 'employment'.
     */
    public void setEmployment(String employment) {
        if (!_employmentIsSet) {
            _employmentIsSet = true;
            _employmentInitVal = getEmployment();
        }
        registerChange("employment", _employmentInitVal, employment);
        _employment = employment;
    }
                        
    /**
     * Returns the property 'employment'.
     */
    public String getEmployment() {
        return _employment;
    }
                                    /**
     * Setter for the property 'is mail notification wanted'.
     */
    public void setIsMailNotificationWanted(boolean isMailNotificationWanted) {
        if (!_isMailNotificationWantedIsSet) {
            _isMailNotificationWantedIsSet = true;
            _isMailNotificationWantedInitVal = getIsMailNotificationWanted();
        }
        registerChange("is mail notification wanted", _isMailNotificationWantedInitVal, isMailNotificationWanted);
        _isMailNotificationWanted = isMailNotificationWanted;
    }
                        
    /**
     * Returns the property 'is mail notification wanted'.
     */
    public boolean getIsMailNotificationWanted() {
        return _isMailNotificationWanted;
    }
                                            
    /**
     * Setter for the property 'integer employments'.
     */
    public void setIntegerEmployments(Collection<Integer> integerEmployments) {
        _integerEmployments = integerEmployments;
    }
            
    /**
     * Returns the property 'integer employments'.
     */
    public Collection<Integer> getIntegerEmployments() {
        return _integerEmployments;
    }
                                            
    /**
     * Setter for the property 'age range min'.
     */
    public void setAgeRangeMin(Integer ageRangeMin) {
        _ageRangeMin = ageRangeMin;
    }
            
    /**
     * Returns the property 'age range min'.
     */
    public Integer getAgeRangeMin() {
        return _ageRangeMin;
    }
                                            
    /**
     * Setter for the property 'age range max'.
     */
    public void setAgeRangeMax(Integer ageRangeMax) {
        _ageRangeMax = ageRangeMax;
    }
            
    /**
     * Returns the property 'age range max'.
     */
    public Integer getAgeRangeMax() {
        return _ageRangeMax;
    }
                                            
    /**
     * Setter for the property 'except late payer'.
     */
    public void setExceptLatePayer(Boolean exceptLatePayer) {
        _exceptLatePayer = exceptLatePayer;
    }
            
    /**
     * Returns the property 'except late payer'.
     */
    public Boolean getExceptLatePayer() {
        return _exceptLatePayer;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'min investment limit per borrower'.
     */
    public void setMinInvestmentLimitPerBorrower(double minInvestmentLimitPerBorrower) {
        if (!_minInvestmentLimitPerBorrowerIsSet) {
            _minInvestmentLimitPerBorrowerIsSet = true;
            _minInvestmentLimitPerBorrowerInitVal = getMinInvestmentLimitPerBorrower();
        }
        registerChange("min investment limit per borrower", _minInvestmentLimitPerBorrowerInitVal, minInvestmentLimitPerBorrower);
        _minInvestmentLimitPerBorrower = minInvestmentLimitPerBorrower;
    }
                        
    /**
     * Returns the property 'min investment limit per borrower'.
     */
    public double getMinInvestmentLimitPerBorrower() {
        return _minInvestmentLimitPerBorrower;
    }
                                    /**
     * Setter for the property 'investment limit per borrower'.
     */
    public void setInvestmentLimitPerBorrower(double investmentLimitPerBorrower) {
        if (!_investmentLimitPerBorrowerIsSet) {
            _investmentLimitPerBorrowerIsSet = true;
            _investmentLimitPerBorrowerInitVal = getInvestmentLimitPerBorrower();
        }
        registerChange("investment limit per borrower", _investmentLimitPerBorrowerInitVal, investmentLimitPerBorrower);
        _investmentLimitPerBorrower = investmentLimitPerBorrower;
    }
                        
    /**
     * Returns the property 'investment limit per borrower'.
     */
    public double getInvestmentLimitPerBorrower() {
        return _investmentLimitPerBorrower;
    }
                                            
    /**
     * Setter for the property 'bid search groups'.
     */
    public void setBidSearchGroups(Collection<BidSearchGroup> bidSearchGroups) {
        _bidSearchGroups = bidSearchGroups;
    }
            
    /**
     * Returns the property 'bid search groups'.
     */
    public Collection<BidSearchGroup> getBidSearchGroups() {
        return _bidSearchGroups;
    }
                                            
    /**
     * Setter for the property 'gender'.
     */
    public void setGender(Integer gender) {
        _gender = gender;
    }
            
    /**
     * Returns the property 'gender'.
     */
    public Integer getGender() {
        return _gender;
    }
                                            
    /**
     * Setter for the property 'service provider restriction'.
     */
    public void setServiceProviderRestriction(de.smava.webapp.account.domain.BankAccountProvider serviceProviderRestriction) {
        _serviceProviderRestriction = serviceProviderRestriction;
    }
            
    /**
     * Returns the property 'service provider restriction'.
     */
    public de.smava.webapp.account.domain.BankAccountProvider getServiceProviderRestriction() {
        return _serviceProviderRestriction;
    }

    /**
     * Setter for the property 'age groups'.
     */
    public void setAgeGroups(Collection<BidSearchAgeGroup> ageGroups) {
        _ageGroups = ageGroups;
    }
            
    /**
     * Returns the property 'age groups'.
     */
    public Collection<BidSearchAgeGroup> getAgeGroups() {
        return _ageGroups;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BidSearch.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _interestMin=").append(_interestMin);
            builder.append("\n    _interestMax=").append(_interestMax);
            builder.append("\n    _amountMin=").append(_amountMin);
            builder.append("\n    _amountMax=").append(_amountMax);
            builder.append("\n    _includeFinishedOrders=").append(_includeFinishedOrders);
            builder.append("\n    _creditRateIndicator=").append(_creditRateIndicator);
            builder.append("\n    _matchedRate=").append(_matchedRate);
            builder.append("\n    _searchPhrase=").append(_searchPhrase);
            builder.append("\n    _employment=").append(_employment);
            builder.append("\n    _isMailNotificationWanted=").append(_isMailNotificationWanted);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _minInvestmentLimitPerBorrower=").append(_minInvestmentLimitPerBorrower);
            builder.append("\n    _investmentLimitPerBorrower=").append(_investmentLimitPerBorrower);
            builder.append("\n}");
        } else {
            builder.append(BidSearch.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
