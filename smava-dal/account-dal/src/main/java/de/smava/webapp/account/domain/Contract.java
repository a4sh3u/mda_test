package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.ContractHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * The domain object that represents 'Contracts'.
 */
public class Contract extends ContractHistory  implements MarketingTrackingEntity {

    private static final long serialVersionUID = 2245240585443286209L;

    public Contract() {
        final Date creationDate = CurrentDate.getDate();
        _creationDate = creationDate;
        _state = Contract.STATE_NEW;
        _bookings = new LinkedHashSet<Booking>();
        _encashmentRate = 0.0;
    }

    public float getRate() {
        return _rate;
    }

    public void setRate(float rate) {
        if (!_rateIsSet) {
            _rateIsSet = true;
            _rateInitVal = getRate();
        }
        registerChange("rate", _rateInitVal, rate);
        _rate = rate;
    }

        protected String _contractNumber;
        protected String _state;
        protected Offer _offer;
        protected Order _order;
        protected Date _creationDate;
        protected Date _sealDate;
        protected Date _startDate;
        protected Date _endDate;
        protected double _amount;
        protected float _rate;
        protected String _marketName;
        protected float _effectiveYieldBorrower;
        protected double _totalContractRepayment;
        protected Collection<Booking> _bookings;
        protected List<Document> _documents;
        protected double _encashmentRate;
        protected Long _marketingPlacementId;
        protected double _updatedRoi;
        protected Double _provisionRateFixLender;
        protected Double _provisionRateLender;
        protected Double _provisionRateBorrower;
        protected Double _provisionRateFixBorrower;
        protected Double _provisionRefundRate;
        protected Integer _refundPeriod;
        protected double _expectedOrderRoi;
        protected Boolean _sharedLoan;
        protected Integer _encashmentPartnerNo;
        
                            /**
     * Setter for the property 'contract number'.
     *
     * 
     *
     */
    public void setContractNumber(String contractNumber) {
        if (!_contractNumberIsSet) {
            _contractNumberIsSet = true;
            _contractNumberInitVal = getContractNumber();
        }
        registerChange("contract number", _contractNumberInitVal, contractNumber);
        _contractNumber = contractNumber;
    }
                        
    /**
     * Returns the property 'contract number'.
     *
     * 
     *
     */
    public String getContractNumber() {
        return _contractNumber;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public String getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'offer'.
     *
     * 
     *
     */
    public void setOffer(Offer offer) {
        _offer = offer;
    }
            
    /**
     * Returns the property 'offer'.
     *
     * 
     *
     */
    public Offer getOffer() {
        return _offer;
    }
                                            
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    public void setOrder(Order order) {
        _order = order;
    }
            
    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    public Order getOrder() {
        return _order;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'seal date'.
     *
     * 
     *
     */
    public void setSealDate(Date sealDate) {
        if (!_sealDateIsSet) {
            _sealDateIsSet = true;
            _sealDateInitVal = getSealDate();
        }
        registerChange("seal date", _sealDateInitVal, sealDate);
        _sealDate = sealDate;
    }
                        
    /**
     * Returns the property 'seal date'.
     *
     * 
     *
     */
    public Date getSealDate() {
        return _sealDate;
    }
                                    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    public void setStartDate(Date startDate) {
        if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = getStartDate();
        }
        registerChange("start date", _startDateInitVal, startDate);
        _startDate = startDate;
    }
                        
    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    public Date getStartDate() {
        return _startDate;
    }
                                    /**
     * Setter for the property 'end date'.
     *
     * 
     *
     */
    public void setEndDate(Date endDate) {
        if (!_endDateIsSet) {
            _endDateIsSet = true;
            _endDateInitVal = getEndDate();
        }
        registerChange("end date", _endDateInitVal, endDate);
        _endDate = endDate;
    }
                        
    /**
     * Returns the property 'end date'.
     *
     * 
     *
     */
    public Date getEndDate() {
        return _endDate;
    }
                                    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public double getAmount() {
        return _amount;
    }
                                                /**
     * Setter for the property 'market name'.
     *
     * 
     *
     */
    public void setMarketName(String marketName) {
        if (!_marketNameIsSet) {
            _marketNameIsSet = true;
            _marketNameInitVal = getMarketName();
        }
        registerChange("market name", _marketNameInitVal, marketName);
        _marketName = marketName;
    }
                        
    /**
     * Returns the property 'market name'.
     *
     * 
     *
     */
    public String getMarketName() {
        return _marketName;
    }
                                    /**
     * Setter for the property 'effective yield borrower'.
     *
     * 
     *
     */
    public void setEffectiveYieldBorrower(float effectiveYieldBorrower) {
        if (!_effectiveYieldBorrowerIsSet) {
            _effectiveYieldBorrowerIsSet = true;
            _effectiveYieldBorrowerInitVal = getEffectiveYieldBorrower();
        }
        registerChange("effective yield borrower", _effectiveYieldBorrowerInitVal, effectiveYieldBorrower);
        _effectiveYieldBorrower = effectiveYieldBorrower;
    }
                        
    /**
     * Returns the property 'effective yield borrower'.
     *
     * 
     *
     */
    public float getEffectiveYieldBorrower() {
        return _effectiveYieldBorrower;
    }
                                    /**
     * Setter for the property 'total contract repayment'.
     *
     * 
     *
     */
    public void setTotalContractRepayment(double totalContractRepayment) {
        if (!_totalContractRepaymentIsSet) {
            _totalContractRepaymentIsSet = true;
            _totalContractRepaymentInitVal = getTotalContractRepayment();
        }
        registerChange("total contract repayment", _totalContractRepaymentInitVal, totalContractRepayment);
        _totalContractRepayment = totalContractRepayment;
    }
                        
    /**
     * Returns the property 'total contract repayment'.
     *
     * 
     *
     */
    public double getTotalContractRepayment() {
        return _totalContractRepayment;
    }
                                            
    /**
     * Setter for the property 'bookings'.
     *
     * 
     *
     */
    public void setBookings(Collection<Booking> bookings) {
        _bookings = bookings;
    }
            
    /**
     * Returns the property 'bookings'.
     *
     * 
     *
     */
    public Collection<Booking> getBookings() {
        return _bookings;
    }
                                            
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    public void setDocuments(List<Document> documents) {
        _documents = documents;
    }
            
    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    public List<Document> getDocuments() {
        return _documents;
    }
                                    /**
     * Setter for the property 'encashment rate'.
     *
     * 
     *
     */
    public void setEncashmentRate(double encashmentRate) {
        if (!_encashmentRateIsSet) {
            _encashmentRateIsSet = true;
            _encashmentRateInitVal = getEncashmentRate();
        }
        registerChange("encashment rate", _encashmentRateInitVal, encashmentRate);
        _encashmentRate = encashmentRate;
    }
                        
    /**
     * Returns the property 'encashment rate'.
     *
     * 
     *
     */
    public double getEncashmentRate() {
        return _encashmentRate;
    }
                                            
    /**
     * Setter for the property 'marketing placement id'.
     *
     * 
     *
     */
    public void setMarketingPlacementId(Long marketingPlacementId) {
        _marketingPlacementId = marketingPlacementId;
    }
            
    /**
     * Returns the property 'marketing placement id'.
     *
     * 
     *
     */
    public Long getMarketingPlacementId() {
        return _marketingPlacementId;
    }
                                    /**
     * Setter for the property 'updated roi'.
     *
     * 
     *
     */
    public void setUpdatedRoi(double updatedRoi) {
        if (!_updatedRoiIsSet) {
            _updatedRoiIsSet = true;
            _updatedRoiInitVal = getUpdatedRoi();
        }
        registerChange("updated roi", _updatedRoiInitVal, updatedRoi);
        _updatedRoi = updatedRoi;
    }
                        
    /**
     * Returns the property 'updated roi'.
     *
     * 
     *
     */
    public double getUpdatedRoi() {
        return _updatedRoi;
    }
                                            
    /**
     * Setter for the property 'provision rate fix lender'.
     *
     * 
     *
     */
    public void setProvisionRateFixLender(Double provisionRateFixLender) {
        _provisionRateFixLender = provisionRateFixLender;
    }
            
    /**
     * Returns the property 'provision rate fix lender'.
     *
     * 
     *
     */
    public Double getProvisionRateFixLender() {
        return _provisionRateFixLender;
    }
                                            
    /**
     * Setter for the property 'provision rate lender'.
     *
     * 
     *
     */
    public void setProvisionRateLender(Double provisionRateLender) {
        _provisionRateLender = provisionRateLender;
    }
            
    /**
     * Returns the property 'provision rate lender'.
     *
     * 
     *
     */
    public Double getProvisionRateLender() {
        return _provisionRateLender;
    }
                                            
    /**
     * Setter for the property 'provision rate borrower'.
     *
     * 
     *
     */
    public void setProvisionRateBorrower(Double provisionRateBorrower) {
        _provisionRateBorrower = provisionRateBorrower;
    }
            
    /**
     * Returns the property 'provision rate borrower'.
     *
     * 
     *
     */
    public Double getProvisionRateBorrower() {
        return _provisionRateBorrower;
    }
                                            
    /**
     * Setter for the property 'provision rate fix borrower'.
     *
     * 
     *
     */
    public void setProvisionRateFixBorrower(Double provisionRateFixBorrower) {
        _provisionRateFixBorrower = provisionRateFixBorrower;
    }
            
    /**
     * Returns the property 'provision rate fix borrower'.
     *
     * 
     *
     */
    public Double getProvisionRateFixBorrower() {
        return _provisionRateFixBorrower;
    }
                                            
    /**
     * Setter for the property 'provision refund rate'.
     *
     * 
     *
     */
    public void setProvisionRefundRate(Double provisionRefundRate) {
        _provisionRefundRate = provisionRefundRate;
    }
            
    /**
     * Returns the property 'provision refund rate'.
     *
     * 
     *
     */
    public Double getProvisionRefundRate() {
        return _provisionRefundRate;
    }
                                            
    /**
     * Setter for the property 'refund period'.
     *
     * 
     *
     */
    public void setRefundPeriod(Integer refundPeriod) {
        _refundPeriod = refundPeriod;
    }
            
    /**
     * Returns the property 'refund period'.
     *
     * 
     *
     */
    public Integer getRefundPeriod() {
        return _refundPeriod;
    }
                                    /**
     * Setter for the property 'expected order roi'.
     *
     * 
     *
     */
    public void setExpectedOrderRoi(double expectedOrderRoi) {
        if (!_expectedOrderRoiIsSet) {
            _expectedOrderRoiIsSet = true;
            _expectedOrderRoiInitVal = getExpectedOrderRoi();
        }
        registerChange("expected order roi", _expectedOrderRoiInitVal, expectedOrderRoi);
        _expectedOrderRoi = expectedOrderRoi;
    }
                        
    /**
     * Returns the property 'expected order roi'.
     *
     * 
     *
     */
    public double getExpectedOrderRoi() {
        return _expectedOrderRoi;
    }
                                            
    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    public void setSharedLoan(Boolean sharedLoan) {
        _sharedLoan = sharedLoan;
    }
            
    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    public Boolean getSharedLoan() {
        return _sharedLoan;
    }
                                            
    /**
     * Setter for the property 'encashment partner no'.
     *
     * 
     *
     */
    public void setEncashmentPartnerNo(Integer encashmentPartnerNo) {
        _encashmentPartnerNo = encashmentPartnerNo;
    }
            
    /**
     * Returns the property 'encashment partner no'.
     *
     * 
     *
     */
    public Integer getEncashmentPartnerNo() {
        return _encashmentPartnerNo;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Contract.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _contractNumber=").append(_contractNumber);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _rate=").append(_rate);
            builder.append("\n    _marketName=").append(_marketName);
            builder.append("\n    _effectiveYieldBorrower=").append(_effectiveYieldBorrower);
            builder.append("\n    _totalContractRepayment=").append(_totalContractRepayment);
            builder.append("\n    _encashmentRate=").append(_encashmentRate);
            builder.append("\n    _updatedRoi=").append(_updatedRoi);
            builder.append("\n    _expectedOrderRoi=").append(_expectedOrderRoi);
            builder.append("\n}");
        } else {
            builder.append(Contract.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
