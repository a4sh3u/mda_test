//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(mail link)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.MailLink;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'MailLinks'.
 *
 * @author generator
 */
public interface MailLinkDao extends BaseDao<MailLink>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the mail link identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MailLink getMailLink(Long id);

    /**
     * Saves the mail link.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMailLink(MailLink mailLink);

    /**
     * Deletes an mail link, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the mail link
     */
    void deleteMailLink(Long id);

    /**
     * Retrieves all 'MailLink' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MailLink> getMailLinkList();

    /**
     * Retrieves a page of 'MailLink' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MailLink> getMailLinkList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MailLink' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MailLink> getMailLinkList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MailLink' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MailLink> getMailLinkList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'MailLink' instances.
     */
    long getMailLinkCount();



    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(mail link)}
    //

    Collection<MailLink> findInvitationMailLinks(Account invited);


    public Collection<MailLink> getMailLinks(Account account, boolean isSender, boolean respectIsSender);


    public Collection<MailLink> getInvitationMailLinks(Account account, boolean isSender, boolean respectIsSender, Date begin, Date end) ;


    public Collection<MailLink> getInvitationMailLinks(String remoteAddress, Date begin, Date end);

    String getWhereClause(String searchLinkName, String searchMailType,
                          Account boAccountInfo, Date fromDateAsDate, Date toDateAsDate);

    Collection<MailLink> getMailLinks(String term, List<Object> paramsList);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
