package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.MatchSortingRun;


/**
 * The domain object that represents 'MatchSortings'.
 *
 * @author generator
 */
public interface MatchSortingEntityInterface {

    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    double getAmount();
    /**
     * Setter for the property 'position'.
     *
     * 
     *
     */
    void setPosition(int position);

    /**
     * Returns the property 'position'.
     *
     * 
     *
     */
    int getPosition();
    /**
     * Setter for the property 'result number'.
     *
     * 
     *
     */
    void setResultNumber(int resultNumber);

    /**
     * Returns the property 'result number'.
     *
     * 
     *
     */
    int getResultNumber();
    /**
     * Setter for the property 'offer sorting'.
     *
     * 
     *
     */
    void setOfferSorting(String offerSorting);

    /**
     * Returns the property 'offer sorting'.
     *
     * 
     *
     */
    String getOfferSorting();
    /**
     * Setter for the property 'order sorting'.
     *
     * 
     *
     */
    void setOrderSorting(String orderSorting);

    /**
     * Returns the property 'order sorting'.
     *
     * 
     *
     */
    String getOrderSorting();
    /**
     * Setter for the property 'match sorting run'.
     *
     * 
     *
     */
    void setMatchSortingRun(MatchSortingRun matchSortingRun);

    /**
     * Returns the property 'match sorting run'.
     *
     * 
     *
     */
    MatchSortingRun getMatchSortingRun();

}
