package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'RegistrationRoutes'.
 *
 * @author generator
 */
public interface RegistrationRouteEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'json config'.
     *
     * 
     *
     */
    void setJsonConfig(String jsonConfig);

    /**
     * Returns the property 'json config'.
     *
     * 
     *
     */
    String getJsonConfig();
    /**
     * Setter for the property 'short id'.
     *
     * 
     *
     */
    void setShortId(Integer shortId);

    /**
     * Returns the property 'short id'.
     *
     * 
     *
     */
    Integer getShortId();

}
