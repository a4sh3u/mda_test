//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(insurance pool payout run)}

import de.smava.webapp.account.dao.InsurancePoolPayoutRunDao;
import de.smava.webapp.account.domain.Booking;
import de.smava.webapp.account.domain.BookingGroup;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.InsurancePoolPayoutRun;
import de.smava.webapp.commons.dao.DefaultSortable;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'InsurancePoolPayoutRuns'.
 *
 * @author generator
 */
@Repository(value = "insurancePoolPayoutRunDao")
public class JdoInsurancePoolPayoutRunDao extends JdoBaseDao implements InsurancePoolPayoutRunDao {

    private static final Logger LOGGER = Logger.getLogger(JdoInsurancePoolPayoutRunDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(insurance pool payout run)}
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the insurance pool payout run identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public InsurancePoolPayoutRun load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRun - start: id=" + id);
        }
        InsurancePoolPayoutRun result = getEntity(InsurancePoolPayoutRun.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRun - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public InsurancePoolPayoutRun getInsurancePoolPayoutRun(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	InsurancePoolPayoutRun entity = findUniqueEntity(InsurancePoolPayoutRun.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the insurance pool payout run.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(InsurancePoolPayoutRun insurancePoolPayoutRun) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveInsurancePoolPayoutRun: " + insurancePoolPayoutRun);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(insurance pool payout run)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(insurancePoolPayoutRun);
    }

    /**
     * @deprecated Use {@link #save(InsurancePoolPayoutRun) instead}
     */
    public Long saveInsurancePoolPayoutRun(InsurancePoolPayoutRun insurancePoolPayoutRun) {
        return save(insurancePoolPayoutRun);
    }

    /**
     * Deletes an insurance pool payout run, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the insurance pool payout run
     */
    public void deleteInsurancePoolPayoutRun(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteInsurancePoolPayoutRun: " + id);
        }
        deleteEntity(InsurancePoolPayoutRun.class, id);
    }

    /**
     * Retrieves all 'InsurancePoolPayoutRun' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - start");
        }
        Collection<InsurancePoolPayoutRun> result = getEntities(InsurancePoolPayoutRun.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'InsurancePoolPayoutRun' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - start: pageable=" + pageable);
        }
        Collection<InsurancePoolPayoutRun> result = getEntities(InsurancePoolPayoutRun.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'InsurancePoolPayoutRun' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - start: sortable=" + sortable);
        }
        Collection<InsurancePoolPayoutRun> result = getEntities(InsurancePoolPayoutRun.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'InsurancePoolPayoutRun' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<InsurancePoolPayoutRun> result = getEntities(InsurancePoolPayoutRun.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'InsurancePoolPayoutRun' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InsurancePoolPayoutRun> findInsurancePoolPayoutRunList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - start: whereClause=" + whereClause);
        }
        Collection<InsurancePoolPayoutRun> result = findEntities(InsurancePoolPayoutRun.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'InsurancePoolPayoutRun' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<InsurancePoolPayoutRun> findInsurancePoolPayoutRunList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<InsurancePoolPayoutRun> result = findEntities(InsurancePoolPayoutRun.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'InsurancePoolPayoutRun' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InsurancePoolPayoutRun> findInsurancePoolPayoutRunList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<InsurancePoolPayoutRun> result = findEntities(InsurancePoolPayoutRun.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'InsurancePoolPayoutRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InsurancePoolPayoutRun> findInsurancePoolPayoutRunList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<InsurancePoolPayoutRun> result = findEntities(InsurancePoolPayoutRun.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'InsurancePoolPayoutRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InsurancePoolPayoutRun> findInsurancePoolPayoutRunList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<InsurancePoolPayoutRun> result = findEntities(InsurancePoolPayoutRun.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'InsurancePoolPayoutRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<InsurancePoolPayoutRun> findInsurancePoolPayoutRunList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<InsurancePoolPayoutRun> result = findEntities(InsurancePoolPayoutRun.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findInsurancePoolPayoutRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'InsurancePoolPayoutRun' instances.
     */
    public long getInsurancePoolPayoutRunCount() {
        long result = getEntityCount(InsurancePoolPayoutRun.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'InsurancePoolPayoutRun' instances which match the given whereClause.
     */
    public long getInsurancePoolPayoutRunCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(InsurancePoolPayoutRun.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'InsurancePoolPayoutRun' instances which match the given whereClause together with params specified in object array.
     */
    public long getInsurancePoolPayoutRunCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(InsurancePoolPayoutRun.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getInsurancePoolPayoutRunCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(insurance pool payout run)}
    //

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRun(BookingGroup bookingGroup, Date endDate, Date startDate) {
        Query q = null;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bookingGroup", bookingGroup);

        if (endDate != null && startDate != null) {
            q = getPersistenceManager().newNamedQuery(InsurancePoolPayoutRun.class, "getPayoutRunByBookingGroupInTimeframe");
            params.put("endDate", endDate);
            params.put("startDate", startDate);
        } else {
            q = getPersistenceManager().newNamedQuery(InsurancePoolPayoutRun.class, "getPayoutRunByBookingGroup");
        }
        
        Collection<InsurancePoolPayoutRun> result = (Collection<InsurancePoolPayoutRun>) q.executeWithMap(params);
        return result;
    }

    /**
     {@inheritDoc}
     */
    @Override
    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRunOrdered( ){
        return findInsurancePoolPayoutRunList(" 1==1 order by _creationDate desc");
    }


    public Collection<InsurancePoolPayoutRun> getInsurancePoolPayoutRuns(BookingGroup bookingGroup) {
        Sortable sorting = new DefaultSortable("_creationDate", true);
        return this.findInsurancePoolPayoutRunList("_bookingGroup._id == " + bookingGroup.getId(), sorting);
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
