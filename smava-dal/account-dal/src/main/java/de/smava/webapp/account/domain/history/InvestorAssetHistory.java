package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractInvestorAsset;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'InvestorAssets'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class InvestorAssetHistory extends AbstractInvestorAsset {

    protected transient double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient boolean _activeInitVal;
    protected transient boolean _activeIsSet;


	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'active'.
     */
    public boolean activeInitVal() {
        boolean result;
        if (_activeIsSet) {
            result = _activeInitVal;
        } else {
            result = getActive();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'active'.
     */
    public boolean activeIsDirty() {
        return !valuesAreEqual(activeInitVal(), getActive());
    }

    /**
     * Returns true if the setter method was called for the property 'active'.
     */
    public boolean activeIsSet() {
        return _activeIsSet;
    }
	
}
