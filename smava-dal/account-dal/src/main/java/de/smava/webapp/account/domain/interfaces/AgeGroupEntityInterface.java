package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.BidSearchAgeGroup;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'AgeGroups'.
 *
 * @author generator
 */
public interface AgeGroupEntityInterface {

    /**
     * Setter for the property 'min age'.
     *
     * 
     *
     */
    void setMinAge(Integer minAge);

    /**
     * Returns the property 'min age'.
     *
     * 
     *
     */
    Integer getMinAge();
    /**
     * Setter for the property 'max age'.
     *
     * 
     *
     */
    void setMaxAge(Integer maxAge);

    /**
     * Returns the property 'max age'.
     *
     * 
     *
     */
    Integer getMaxAge();
    /**
     * Setter for the property 'valid from'.
     *
     * 
     *
     */
    void setValidFrom(Date validFrom);

    /**
     * Returns the property 'valid from'.
     *
     * 
     *
     */
    Date getValidFrom();
    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Setter for the property 'bid searches'.
     *
     * 
     *
     */
    void setBidSearches(Collection<BidSearchAgeGroup> bidSearches);

    /**
     * Returns the property 'bid searches'.
     *
     * 
     *
     */
    Collection<BidSearchAgeGroup> getBidSearches();

}
