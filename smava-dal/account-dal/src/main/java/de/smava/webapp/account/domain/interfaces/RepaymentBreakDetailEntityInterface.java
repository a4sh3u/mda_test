package de.smava.webapp.account.domain.interfaces;


import java.util.Date;


/**
 * The domain object that represents 'RepaymentBreakDetails'.
 *
 * @author generator
 */
public interface RepaymentBreakDetailEntityInterface {

    /**
     * Setter for the property 'repayment break'.
     *
     * 
     *
     */
    void setRepaymentBreak(de.smava.webapp.account.domain.RepaymentBreak repaymentBreak);

    /**
     * Returns the property 'repayment break'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.RepaymentBreak getRepaymentBreak();
    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    void setRequestedAmount(double requestedAmount);

    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    double getRequestedAmount();
    /**
     * Setter for the property 'requested date'.
     *
     * 
     *
     */
    void setRequestedDate(Date requestedDate);

    /**
     * Returns the property 'requested date'.
     *
     * 
     *
     */
    Date getRequestedDate();
    /**
     * Setter for the property 'confirmed amount'.
     *
     * 
     *
     */
    void setConfirmedAmount(double confirmedAmount);

    /**
     * Returns the property 'confirmed amount'.
     *
     * 
     *
     */
    double getConfirmedAmount();
    /**
     * Setter for the property 'confirmed date'.
     *
     * 
     *
     */
    void setConfirmedDate(Date confirmedDate);

    /**
     * Returns the property 'confirmed date'.
     *
     * 
     *
     */
    Date getConfirmedDate();
    /**
     * Setter for the property 'main order'.
     *
     * 
     *
     */
    void setMainOrder(de.smava.webapp.account.domain.Order mainOrder);

    /**
     * Returns the property 'main order'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Order getMainOrder();
    /**
     * Setter for the property 'affected insurance booking group'.
     *
     * 
     *
     */
    void setAffectedInsuranceBookingGroup(de.smava.webapp.account.domain.BookingGroup affectedInsuranceBookingGroup);

    /**
     * Returns the property 'affected insurance booking group'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BookingGroup getAffectedInsuranceBookingGroup();
    /**
     * Setter for the property 'repayment break booking group'.
     *
     * 
     *
     */
    void setRepaymentBreakBookingGroup(de.smava.webapp.account.domain.BookingGroup repaymentBreakBookingGroup);

    /**
     * Returns the property 'repayment break booking group'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BookingGroup getRepaymentBreakBookingGroup();

}
