package de.smava.webapp.account.dao.jdo;

import de.smava.webapp.account.dao.HistoryEntryDetailDao;
import de.smava.webapp.account.domain.history.HistoryEntryDetail;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;

/**
 * JDO implementation of HistoryEntryDetailDao.
 * 
 * @author rfiedler
 *
 */
public class JdoHistoryEntryDetailDao extends JdoBaseDao implements HistoryEntryDetailDao {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public HistoryEntryDetail load(Long id) {
        return load(id);
    }

    public Long save(HistoryEntryDetail message) {
        return saveEntity(message);
    }

	public boolean exists(Long id) {
		// TODO Auto-generated method stub
		return false;
	}
    
    


}
