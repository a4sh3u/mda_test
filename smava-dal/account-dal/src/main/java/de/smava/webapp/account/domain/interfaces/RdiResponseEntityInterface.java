package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.RdiEntry;
import de.smava.webapp.account.domain.RdiRequest;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'RdiResponses'.
 *
 * @author generator
 */
public interface RdiResponseEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'error message'.
     *
     * 
     *
     */
    void setErrorMessage(String errorMessage);

    /**
     * Returns the property 'error message'.
     *
     * 
     *
     */
    String getErrorMessage();
    /**
     * Setter for the property 'entries'.
     *
     * 
     *
     */
    void setEntries(Collection<RdiEntry> entries);

    /**
     * Returns the property 'entries'.
     *
     * 
     *
     */
    Collection<RdiEntry> getEntries();
    /**
     * Setter for the property 'error entries'.
     *
     * 
     *
     */
    void setErrorEntries(Collection<RdiEntry> errorEntries);

    /**
     * Returns the property 'error entries'.
     *
     * 
     *
     */
    Collection<RdiEntry> getErrorEntries();
    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    void setRequest(RdiRequest request);

    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    RdiRequest getRequest();

}
