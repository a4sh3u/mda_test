package de.smava.webapp.account.domain.interfaces;


import de.smava.webapp.account.domain.Booking;
import de.smava.webapp.account.domain.BookingGroup;


/**
 * The domain object that represents 'BookingAssignments'.
 *
 * @author generator
 */
public interface BookingAssignmentEntityInterface {

    /**
     * Setter for the property 'is target'.
     *
     * 
     *
     */
    void setIsTarget(boolean isTarget);

    /**
     * Returns the property 'is target'.
     *
     * 
     *
     */
    boolean getIsTarget();
    /**
     * Setter for the property 'booking group'.
     *
     * 
     *
     */
    void setBookingGroup(BookingGroup bookingGroup);

    /**
     * Returns the property 'booking group'.
     *
     * 
     *
     */
    BookingGroup getBookingGroup();
    /**
     * Setter for the property 'booking'.
     *
     * 
     *
     */
    void setBooking(Booking booking);

    /**
     * Returns the property 'booking'.
     *
     * 
     *
     */
    Booking getBooking();

}
