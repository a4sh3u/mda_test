package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'ConfigValues'.
 *
 * @author generator
 */
public interface ConfigValueEntityInterface {

    /**
     * Setter for the property 'group'.
     *
     * 
     *
     */
    void setGroup(String group);

    /**
     * Returns the property 'group'.
     *
     * 
     *
     */
    String getGroup();
    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    void setKey(String key);

    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    String getKey();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();

}
