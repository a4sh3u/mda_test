package de.smava.webapp.account.domain.abstracts;

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.interfaces.DtausEntryEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.commons.util.FormatUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'DtausEntrys'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractDtausEntry
    extends KreditPrivatEntity implements DtausEntryEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDtausEntry.class);

    private static final long serialVersionUID = -8473769622006688353L;

    public static final String STATE_OPEN = "OPEN";
    public static final String STATE_SUCCESSFUL = "SUCCESSFUL";
    public static final String STATE_DELETED = "DELETED";
    public static final Set STATES = ConstCollector.getAll("STATE_");

    // Same as Transaction.TYPE... (same resources are used in front end display)
    // So if you nvent new states or types
    public static final String TYPE_TRANSFER = "TRANSFER";
    //    2)	Lastschrifteinzug
    public static final String TYPE_DIRECT_DEBIT = "DIRECT_DEBIT";
    //    3)	R�cklastschrift (RLS)
    public static final String TYPE_RETURN_DEBIT = "RETURN_DEBIT";
    //    4)	R�ck�berweisung
    public static final String TYPE_RETURN_TRANSFER = "RETURN_TRANSFER";

    public static final Set TYPES = ConstCollector.getAll("TYPE_");


    public static final String RETURN_REASON_NO_INFO = "NO INFO";
    public static final String RETURN_REASON_ACCOUNT_EXPIRED = "EXPIRED";
    public static final String RETURN_REASON_WRONG_ACCOUNT = "WRONG ACCOUNT";
    public static final String RETURN_REASON_NO_PERMISSION = "NO PERMISSION";
    public static final String RETURN_REASON_CALL_BACK = "CALL BACK";
    public static final String RETURN_REASON_OBJECTION = "OBJECTION";
    public static final String RETURN_REASON_CHARGE_BACK = "CHARGE BACK EDC";
    public static final String RETURN_REASON_NO_GSE = "NO GSE";
    public static final String RETURN_REASON_WRONG_NAME = "NAME AND NUMBER";
    public static final String RETURN_REASON_CONTRACT_FULFILLED = "FULFILLED";
    public static final String RETURN_REASON_UNKNOWN = "UNKNOWN";

    public static final String ERRROR_CODE_BANK_ACCOUNT_OWNER_NOT_EQUAL = "BANK_ACCOUNT_OWNER_NOT_EQUAL";
    public static final String ERRROR_CODE_AMOUNT_NOT_EQUAL = "AMOUNT_NOT_EQUAL";

    /**
     * Return debit codes for dtaus.
     */
    public static final Map<String, String> MAP_RETURN_REASON_0905 = new HashMap<String, String>();

    /**
     * Return transfer codes for dtaus.
     */
    public static final Map<String, String> MAP_RETURN_REASON_59 = new HashMap<String, String>();


    /**
     * Return transfer codes for xcom.
     */
    public static final Map<String, String> MAP_RETURN_REASON_XCOM_TRANSFER = new HashMap<String, String>();

    /**
     * Return debit codes for xcom.
     */
    public static final Map<String, String> MAP_RETURN_REASON_XCOM_DEBIT = new HashMap<String, String>();

    static {
        /*
            "0" f�r keine Angabe
            "1" f�r "KONTO ERLOSCHEN",
            "2" f�r "KONTO-NR./BLZ FALSCH",
            "3" f�r Keine Einzugserm�chtigung
            "4" f�r R�ckruf der LS
            "5" f�r Wiederspruch
            "6" f�r Chargeback edc
            "7" f�r "NICHTVORLAGE GSE-PAPIER"
        */
        MAP_RETURN_REASON_0905.put("0", RETURN_REASON_NO_INFO);
        MAP_RETURN_REASON_0905.put("1", RETURN_REASON_ACCOUNT_EXPIRED);
        MAP_RETURN_REASON_0905.put("2", RETURN_REASON_WRONG_ACCOUNT);
        MAP_RETURN_REASON_0905.put("3", RETURN_REASON_NO_PERMISSION);
        MAP_RETURN_REASON_0905.put("4", RETURN_REASON_CALL_BACK);
        MAP_RETURN_REASON_0905.put("5", RETURN_REASON_OBJECTION);
        MAP_RETURN_REASON_0905.put("6", RETURN_REASON_CHARGE_BACK);
        MAP_RETURN_REASON_0905.put("7", RETURN_REASON_NO_GSE);

        /*
            59	YYZ	R�ck�berweisung
            Die Buchstaben "YY" sind durch den Ursprungstextschl�ssel der DTA-�berweisung zu ersetzen.
            Der Buchstabe "Z" ist durch die jeweilige Ziffer des verschl�sselten R�ckgabegrundes zu ersetzen:
            "l" f�r "KONTO ERLOSCHEN",
            "2" f�r "KONTO-NR./BLZ FALSCH".
            "3" f�r "VERTRAG ERF�LLT",
            "4" f�r "R�CKRUF",
            "5" f�r "KTO-NR./NAME NICHT IDENTISCH".
        */
        MAP_RETURN_REASON_59.put("0", RETURN_REASON_NO_INFO);
        MAP_RETURN_REASON_59.put("1", RETURN_REASON_ACCOUNT_EXPIRED);
        MAP_RETURN_REASON_59.put("2", RETURN_REASON_WRONG_ACCOUNT);
        MAP_RETURN_REASON_59.put("3", RETURN_REASON_CONTRACT_FULFILLED);
        MAP_RETURN_REASON_59.put("4", RETURN_REASON_CALL_BACK);
        MAP_RETURN_REASON_59.put("5", RETURN_REASON_WRONG_NAME);

        /*
            Lastschriftr�ckgabe aus Einzugserm�chtigungsverfahren:
            51 - Konto erloschen
            52 - Konto/BLZ falsch
            53 - Vertrag erf�llt bzw. unterbrochen bzw. Gutschrift unzul�ssig
            54 - R�ckruf
            55 = WEGEN WIDERSPRUCHS
            56 = R�CKGABE/CHARGEBACK Z.B.EDC
        */
        MAP_RETURN_REASON_XCOM_DEBIT.put("40", RETURN_REASON_NO_INFO);
        MAP_RETURN_REASON_XCOM_DEBIT.put("41", RETURN_REASON_ACCOUNT_EXPIRED);
        MAP_RETURN_REASON_XCOM_DEBIT.put("42", RETURN_REASON_WRONG_ACCOUNT);
        MAP_RETURN_REASON_XCOM_DEBIT.put("43", RETURN_REASON_CONTRACT_FULFILLED);
        MAP_RETURN_REASON_XCOM_DEBIT.put("44", RETURN_REASON_CALL_BACK);
        MAP_RETURN_REASON_XCOM_DEBIT.put("45", RETURN_REASON_OBJECTION);
        MAP_RETURN_REASON_XCOM_DEBIT.put("46", RETURN_REASON_CHARGE_BACK);
        MAP_RETURN_REASON_XCOM_DEBIT.put("50", RETURN_REASON_NO_INFO);
        MAP_RETURN_REASON_XCOM_DEBIT.put("51", RETURN_REASON_ACCOUNT_EXPIRED);
        MAP_RETURN_REASON_XCOM_DEBIT.put("52", RETURN_REASON_WRONG_ACCOUNT);
        MAP_RETURN_REASON_XCOM_DEBIT.put("53", RETURN_REASON_CONTRACT_FULFILLED);
        MAP_RETURN_REASON_XCOM_DEBIT.put("54", RETURN_REASON_CALL_BACK);
        MAP_RETURN_REASON_XCOM_DEBIT.put("55", RETURN_REASON_OBJECTION);
        MAP_RETURN_REASON_XCOM_DEBIT.put("56", RETURN_REASON_CHARGE_BACK);

        /*
            R�ck�berweisung
            aus �berweisung
            511 = KONTO ERLOSCHEN
            512 = KONTO/BLZ FALSCH
            513 = VERTRAG ERF�LLT
            514 = R�CKRUF
            515 = KTO.NR.-NAME NICHT IDENT
        */
        MAP_RETURN_REASON_XCOM_TRANSFER.put("0", RETURN_REASON_NO_INFO);
        MAP_RETURN_REASON_XCOM_TRANSFER.put("1", RETURN_REASON_ACCOUNT_EXPIRED);
        MAP_RETURN_REASON_XCOM_TRANSFER.put("2", RETURN_REASON_WRONG_ACCOUNT);
        MAP_RETURN_REASON_XCOM_TRANSFER.put("3", RETURN_REASON_CONTRACT_FULFILLED);
        MAP_RETURN_REASON_XCOM_TRANSFER.put("4", RETURN_REASON_OBJECTION);
        MAP_RETURN_REASON_XCOM_TRANSFER.put("5", RETURN_REASON_WRONG_NAME);

    }

    /**
     * Get reason code for R�cklastschrift.
     * @param number last character of the dtaus return reason code.
     * @return The appropriate return reason string, see constants above
     */
    public static String getReasonForDtausReturnDebit0905 (String number) {
        String reason = RETURN_REASON_UNKNOWN;
        if (MAP_RETURN_REASON_0905.containsKey(number)) {
            reason = MAP_RETURN_REASON_0905.get(number);
        }
        return reason;
    }

    /**
     * Get reason code for R�ck�berweisung.
     * @param number last character of the dtaus return reason code.
     * @return The appropriate return reason string, see constants above
     */
    public static String getReasonForDtausReturnTransfer59 (String number) {
        String reason = RETURN_REASON_UNKNOWN;
        if (MAP_RETURN_REASON_59.containsKey(number)) {
            reason = MAP_RETURN_REASON_59.get(number);
        }
        return reason;
    }

    /**
     * Get reason code for R�cklastschrift.
     * @param number last character of the dtaus return reason code.
     * @return The appropriate return reason string, see constants above
     */
    public static String getReasonForXcomReturnDebit (String number) {
        String reason = RETURN_REASON_UNKNOWN;
        if (MAP_RETURN_REASON_XCOM_DEBIT.containsKey(number)) {
            reason = MAP_RETURN_REASON_XCOM_DEBIT.get(number);
        }
        return reason;
    }

    /**
     * Get reason code for R�ck�berweisung.
     * @param number last character of the dtaus return reason code.
     * @return The appropriate return reason string, see constants above
     */
    public static String getReasonForXcomReturnTransfer (String number) {
        String reason = RETURN_REASON_UNKNOWN;
        if (MAP_RETURN_REASON_XCOM_TRANSFER.containsKey(number)) {
            reason = MAP_RETURN_REASON_XCOM_TRANSFER.get(number);
        }
        return reason;
    }


    public boolean isOpen () {
        return DtausEntry.STATE_OPEN.equals(getState());
    }

    public boolean isSuccessful () {
        return DtausEntry.STATE_SUCCESSFUL.equals(getState());
    }

    public boolean isTransfer () {
        return DtausEntry.TYPE_TRANSFER.equals(getType());
    }

    public boolean isReturnDebit () {
        return DtausEntry.TYPE_RETURN_DEBIT.equals(getType());
    }

    public boolean isReturnTransfer () {
        return DtausEntry.TYPE_RETURN_TRANSFER.equals(getType());
    }

    public boolean isDirectDebit () {
        return DtausEntry.TYPE_DIRECT_DEBIT.equals(getType());
    }

    public void appendDescription (String description) {
        if (getDescription() == null) {
            setDescription(description);
        } else if (description != null && !"".equals(description)) {
            setDescription(getDescription() + " " + description);
        }
    }

    /**
     * Get all transactions connected with realtion IN CONFIRMED.
     */
    public List<Transaction> getInConfirmedTransactions() {
        return DtausEntryTransaction.getRelatedTransactions(this.instance(), DtausEntryTransaction.TYPE_IN_CONFIRMED);
    }

    /**
     * Get all transactions connected with realtion IN RELATED.
     */
    public List<Transaction> getInRelatedTransactions() {
        return DtausEntryTransaction.getRelatedTransactions(this.instance(), DtausEntryTransaction.TYPE_IN_RELATED);
    }

    /**
     * Get all transactions connected with realtion IN FOLLOW.
     */
    public List<Transaction> getInFollowTransactions() {
        return DtausEntryTransaction.getRelatedTransactions(this.instance(), DtausEntryTransaction.TYPE_IN_FOLLOW);
    }

    /**
     * Get all transactions connected with realtion IN CONFIRMED or IN RELATED.
     */
    public List<Transaction> getInRelatedAndConfirmedTransactions() {
        List<Transaction> list = DtausEntryTransaction.getRelatedTransactions(this.instance(), DtausEntryTransaction.TYPE_IN_RELATED);
        list.addAll(DtausEntryTransaction.getRelatedTransactions(this.instance(), DtausEntryTransaction.TYPE_IN_CONFIRMED));
        return list;
    }

    public void addInConfirmedTransaction(Transaction transaction) {
        DtausEntryTransaction.connect(this.instance(), transaction, DtausEntryTransaction.TYPE_IN_CONFIRMED);
    }

    public void addInRelatedTransaction(Transaction transaction) {
        DtausEntryTransaction.connect(this.instance(), transaction, DtausEntryTransaction.TYPE_IN_RELATED);
    }

    public void addInFollowTransaction(Transaction transaction) {
        DtausEntryTransaction.connect(this.instance(), transaction, DtausEntryTransaction.TYPE_IN_FOLLOW);
    }

    public void removeInConfirmedTransaction(Transaction transaction) {
        DtausEntryTransaction.disconnect(this.instance(), transaction, DtausEntryTransaction.TYPE_IN_CONFIRMED);
    }

    public void removeInRelatedTransaction(Transaction transaction) {
        DtausEntryTransaction.disconnect(this.instance(), transaction, DtausEntryTransaction.TYPE_IN_RELATED);
    }

    private static Collection<String> notSmavaAccountTypes = new ArrayList<String>(4);
    static {
        notSmavaAccountTypes.add(BankAccount.TYPE_BORROWER_ACCOUNT);
        notSmavaAccountTypes.add(BankAccount.TYPE_LENDER_ACCOUNT);
        notSmavaAccountTypes.add(BankAccount.TYPE_REFRENCE_ACCOUNT);
        notSmavaAccountTypes.add(BankAccount.TYPE_OTHER_ACCOUNT);
    }

    public boolean hasErrors() {
        this.setErrorCode(StringUtils.EMPTY);
        boolean result = false;
        Set<DtausEntryTransaction> dtausEntryTransactions =  getDtausEntryTransactions();
        if (!dtausEntryTransactions.isEmpty()) {
            for (DtausEntryTransaction dtausEntryTransaction : dtausEntryTransactions) {
                Transaction transaction = dtausEntryTransaction.getTransaction();
                BankAccount debitAccount = transaction.getDebitAccount();
                BankAccount creditAccount = transaction.getCreditAccount();

                boolean rightAccountType = notSmavaAccountTypes.contains(debitAccount.getType()) && notSmavaAccountTypes.contains(creditAccount.getType());

                if (rightAccountType) {
                    result = isAmountUnequal(transaction);
                    result = result || areBankAccountsUnequal(transaction);
                }

                if (!result) {
                    this.setErrorCode("");
                }
            }
        } else {
            this.setErrorCode("");
        }

        return result;
    }

    private boolean areBankAccountsUnequal(Transaction transaction) {
        boolean result = false;
        BankAccount debitAccount = transaction.getDebitAccount();
        BankAccount creditAccount = transaction.getCreditAccount();

        if (!creditAccount.getAccount().equals(debitAccount.getAccount())) {
            this.setErrorCode(DtausEntry.ERRROR_CODE_BANK_ACCOUNT_OWNER_NOT_EQUAL);
            result = true;
        }

        return result;
    }

    private boolean isAmountUnequal(Transaction transaction) {
        boolean result = false;
        double transactionAmount = FormatUtils.roundAmount(transaction.getAmount());
        double entryAmount = FormatUtils.roundAmount(this.getAmount());
        if (transactionAmount != entryAmount) {
            this.setErrorCode(DtausEntry.ERRROR_CODE_AMOUNT_NOT_EQUAL);
            result = true;
        }

        return result;
    }

    public abstract DtausEntry instance();
}

