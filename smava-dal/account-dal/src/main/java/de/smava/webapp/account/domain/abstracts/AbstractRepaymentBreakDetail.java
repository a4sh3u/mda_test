package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.RepaymentBreakDetailEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'RepaymentBreakDetails'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractRepaymentBreakDetail
    extends KreditPrivatEntity implements RepaymentBreakDetailEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRepaymentBreakDetail.class);

    /**
     * generated serial.
     */
    private static final long serialVersionUID = 1L;

}

