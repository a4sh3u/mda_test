package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractCategory;




/**
 * The domain object that has all history aggregation related fields for 'Categorys'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CategoryHistory extends AbstractCategory {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient boolean _shortlistedInitVal;
    protected transient boolean _shortlistedIsSet;
    protected transient String _imageInitVal;
    protected transient boolean _imageIsSet;
    protected transient int _imageHeightInitVal;
    protected transient boolean _imageHeightIsSet;
    protected transient int _imageWidthInitVal;
    protected transient boolean _imageWidthIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
					
    /**
     * Returns the initial value of the property 'shortlisted'.
     */
    public boolean shortlistedInitVal() {
        boolean result;
        if (_shortlistedIsSet) {
            result = _shortlistedInitVal;
        } else {
            result = getShortlisted();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'shortlisted'.
     */
    public boolean shortlistedIsDirty() {
        return !valuesAreEqual(shortlistedInitVal(), getShortlisted());
    }

    /**
     * Returns true if the setter method was called for the property 'shortlisted'.
     */
    public boolean shortlistedIsSet() {
        return _shortlistedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image'.
     */
    public String imageInitVal() {
        String result;
        if (_imageIsSet) {
            result = _imageInitVal;
        } else {
            result = getImage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image'.
     */
    public boolean imageIsDirty() {
        return !valuesAreEqual(imageInitVal(), getImage());
    }

    /**
     * Returns true if the setter method was called for the property 'image'.
     */
    public boolean imageIsSet() {
        return _imageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image height'.
     */
    public int imageHeightInitVal() {
        int result;
        if (_imageHeightIsSet) {
            result = _imageHeightInitVal;
        } else {
            result = getImageHeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image height'.
     */
    public boolean imageHeightIsDirty() {
        return !valuesAreEqual(imageHeightInitVal(), getImageHeight());
    }

    /**
     * Returns true if the setter method was called for the property 'image height'.
     */
    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image width'.
     */
    public int imageWidthInitVal() {
        int result;
        if (_imageWidthIsSet) {
            result = _imageWidthInitVal;
        } else {
            result = getImageWidth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image width'.
     */
    public boolean imageWidthIsDirty() {
        return !valuesAreEqual(imageWidthInitVal(), getImageWidth());
    }

    /**
     * Returns true if the setter method was called for the property 'image width'.
     */
    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }

}
