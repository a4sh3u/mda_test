//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head11/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head11/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document attachment container)}

import de.smava.webapp.account.domain.Attachment;
import de.smava.webapp.account.domain.DocumentAttachmentContainer;
import de.smava.webapp.account.domain.DocumentContainer;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'DocumentAttachmentContainers'.
 *
 * @author generator
 */
public interface DocumentAttachmentContainerDao extends BaseDao<DocumentAttachmentContainer>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the document attachment container identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    DocumentAttachmentContainer getDocumentAttachmentContainer(Long id);

    /**
     * Saves the document attachment container.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveDocumentAttachmentContainer(DocumentAttachmentContainer documentAttachmentContainer);

    /**
     * Deletes an document attachment container, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document attachment container
     */
    void deleteDocumentAttachmentContainer(Long id);

    /**
     * Retrieves all 'DocumentAttachmentContainer' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList();

    /**
     * Retrieves a page of 'DocumentAttachmentContainer' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'DocumentAttachmentContainer' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'DocumentAttachmentContainer' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'DocumentAttachmentContainer' instances.
     */
    long getDocumentAttachmentContainerCount();


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document attachment container)}
    //
    
    Collection<DocumentAttachmentContainer> getDocumentAttachmentContainerByName(String name);
    Collection<DocumentAttachmentContainer> getOpenOutgoingDocumentAttachmentContainer();
    
    /**
     * loads a Map with {@link Attachment} to {@link DocumentContainer} for sending outgoing mail.
     * If there is more then one {@link DocumentContainer} linked to an {@link Attachment} the newest
     * with type {@link DocumentContainer#TYPE_LETTER_COMPILATION} wins.
     * 
     * @param dac, to load the map for
     * @return the map
     */
    Map<Attachment, DocumentContainer> getAttachmentToDocumentContainerMap(DocumentAttachmentContainer dac); 

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
