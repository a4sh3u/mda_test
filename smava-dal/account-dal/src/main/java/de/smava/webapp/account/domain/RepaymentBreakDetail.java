package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.RepaymentBreakDetailHistory;

import java.util.Date;

/**
 * The domain object that represents 'RepaymentBreakDetails'.
 */
public class RepaymentBreakDetail extends RepaymentBreakDetailHistory  {

        protected de.smava.webapp.account.domain.RepaymentBreak _repaymentBreak;
        protected double _requestedAmount;
        protected Date _requestedDate;
        protected double _confirmedAmount;
        protected Date _confirmedDate;
        protected de.smava.webapp.account.domain.Order _mainOrder;
        protected de.smava.webapp.account.domain.BookingGroup _affectedInsuranceBookingGroup;
        protected de.smava.webapp.account.domain.BookingGroup _repaymentBreakBookingGroup;

    /**
     * Setter for the property 'repayment break'.
     */
    public void setRepaymentBreak(de.smava.webapp.account.domain.RepaymentBreak repaymentBreak) {
        _repaymentBreak = repaymentBreak;
    }
            
    /**
     * Returns the property 'repayment break'.
     */
    public de.smava.webapp.account.domain.RepaymentBreak getRepaymentBreak() {
        return _repaymentBreak;
    }
                                    /**
     * Setter for the property 'requested amount'.
     */
    public void setRequestedAmount(double requestedAmount) {
        if (!_requestedAmountIsSet) {
            _requestedAmountIsSet = true;
            _requestedAmountInitVal = getRequestedAmount();
        }
        registerChange("requested amount", _requestedAmountInitVal, requestedAmount);
        _requestedAmount = requestedAmount;
    }
                        
    /**
     * Returns the property 'requested amount'.
     */
    public double getRequestedAmount() {
        return _requestedAmount;
    }
                                    /**
     * Setter for the property 'requested date'.
     */
    public void setRequestedDate(Date requestedDate) {
        if (!_requestedDateIsSet) {
            _requestedDateIsSet = true;
            _requestedDateInitVal = getRequestedDate();
        }
        registerChange("requested date", _requestedDateInitVal, requestedDate);
        _requestedDate = requestedDate;
    }
                        
    /**
     * Returns the property 'requested date'.
     */
    public Date getRequestedDate() {
        return _requestedDate;
    }
                                    /**
     * Setter for the property 'confirmed amount'.
     */
    public void setConfirmedAmount(double confirmedAmount) {
        if (!_confirmedAmountIsSet) {
            _confirmedAmountIsSet = true;
            _confirmedAmountInitVal = getConfirmedAmount();
        }
        registerChange("confirmed amount", _confirmedAmountInitVal, confirmedAmount);
        _confirmedAmount = confirmedAmount;
    }
                        
    /**
     * Returns the property 'confirmed amount'.
     */
    public double getConfirmedAmount() {
        return _confirmedAmount;
    }
                                    /**
     * Setter for the property 'confirmed date'.
     */
    public void setConfirmedDate(Date confirmedDate) {
        if (!_confirmedDateIsSet) {
            _confirmedDateIsSet = true;
            _confirmedDateInitVal = getConfirmedDate();
        }
        registerChange("confirmed date", _confirmedDateInitVal, confirmedDate);
        _confirmedDate = confirmedDate;
    }
                        
    /**
     * Returns the property 'confirmed date'.
     */
    public Date getConfirmedDate() {
        return _confirmedDate;
    }
                                            
    /**
     * Setter for the property 'main order'.
     */
    public void setMainOrder(de.smava.webapp.account.domain.Order mainOrder) {
        _mainOrder = mainOrder;
    }
            
    /**
     * Returns the property 'main order'.
     */
    public de.smava.webapp.account.domain.Order getMainOrder() {
        return _mainOrder;
    }
                                            
    /**
     * Setter for the property 'affected insurance booking group'.
     */
    public void setAffectedInsuranceBookingGroup(de.smava.webapp.account.domain.BookingGroup affectedInsuranceBookingGroup) {
        _affectedInsuranceBookingGroup = affectedInsuranceBookingGroup;
    }
            
    /**
     * Returns the property 'affected insurance booking group'.
     */
    public de.smava.webapp.account.domain.BookingGroup getAffectedInsuranceBookingGroup() {
        return _affectedInsuranceBookingGroup;
    }
                                            
    /**
     * Setter for the property 'repayment break booking group'.
     */
    public void setRepaymentBreakBookingGroup(de.smava.webapp.account.domain.BookingGroup repaymentBreakBookingGroup) {
        _repaymentBreakBookingGroup = repaymentBreakBookingGroup;
    }
            
    /**
     * Returns the property 'repayment break booking group'.
     */
    public de.smava.webapp.account.domain.BookingGroup getRepaymentBreakBookingGroup() {
        return _repaymentBreakBookingGroup;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RepaymentBreakDetail.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _requestedAmount=").append(_requestedAmount);
            builder.append("\n    _confirmedAmount=").append(_confirmedAmount);
            builder.append("\n}");
        } else {
            builder.append(RepaymentBreakDetail.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
