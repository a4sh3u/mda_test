//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Tools\workspace\smava-webapp-parent\account\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Tools\workspace\smava-webapp-parent\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account clearing run)}
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.account.dao.BankAccountClearingRunDao;
import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.BankAccountClearingRun;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BankAccountClearingRuns'.
 *
 * @author generator
 */
@Repository(value = "bankAccountClearingRunDao")
public class JdoBankAccountClearingRunDao extends JdoBaseDao implements BankAccountClearingRunDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBankAccountClearingRunDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bank account clearing run)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bank account clearing run identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BankAccountClearingRun load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRun - start: id=" + id);
        }
        BankAccountClearingRun result = getEntity(BankAccountClearingRun.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRun - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BankAccountClearingRun getBankAccountClearingRun(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BankAccountClearingRun entity = findUniqueEntity(BankAccountClearingRun.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the bank account clearing run.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BankAccountClearingRun bankAccountClearingRun) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBankAccountClearingRun: " + bankAccountClearingRun);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bank account clearing run)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bankAccountClearingRun);
    }

    /**
     * @deprecated Use {@link #save(BankAccountClearingRun) instead}
     */
    public Long saveBankAccountClearingRun(BankAccountClearingRun bankAccountClearingRun) {
        return save(bankAccountClearingRun);
    }

    /**
     * Deletes an bank account clearing run, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bank account clearing run
     */
    public void deleteBankAccountClearingRun(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBankAccountClearingRun: " + id);
        }
        deleteEntity(BankAccountClearingRun.class, id);
    }

    /**
     * Retrieves all 'BankAccountClearingRun' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BankAccountClearingRun> getBankAccountClearingRunList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - start");
        }
        Collection<BankAccountClearingRun> result = getEntities(BankAccountClearingRun.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BankAccountClearingRun' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BankAccountClearingRun> getBankAccountClearingRunList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - start: pageable=" + pageable);
        }
        Collection<BankAccountClearingRun> result = getEntities(BankAccountClearingRun.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BankAccountClearingRun' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BankAccountClearingRun> getBankAccountClearingRunList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - start: sortable=" + sortable);
        }
        Collection<BankAccountClearingRun> result = getEntities(BankAccountClearingRun.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankAccountClearingRun' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BankAccountClearingRun> getBankAccountClearingRunList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BankAccountClearingRun> result = getEntities(BankAccountClearingRun.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BankAccountClearingRun' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccountClearingRun> findBankAccountClearingRunList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - start: whereClause=" + whereClause);
        }
        Collection<BankAccountClearingRun> result = findEntities(BankAccountClearingRun.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BankAccountClearingRun' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BankAccountClearingRun> findBankAccountClearingRunList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<BankAccountClearingRun> result = findEntities(BankAccountClearingRun.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BankAccountClearingRun' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccountClearingRun> findBankAccountClearingRunList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<BankAccountClearingRun> result = findEntities(BankAccountClearingRun.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BankAccountClearingRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccountClearingRun> findBankAccountClearingRunList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<BankAccountClearingRun> result = findEntities(BankAccountClearingRun.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankAccountClearingRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccountClearingRun> findBankAccountClearingRunList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BankAccountClearingRun> result = findEntities(BankAccountClearingRun.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankAccountClearingRun' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankAccountClearingRun> findBankAccountClearingRunList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<BankAccountClearingRun> result = findEntities(BankAccountClearingRun.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBankAccountClearingRunList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BankAccountClearingRun' instances.
     */
    public long getBankAccountClearingRunCount() {
        long result = getEntityCount(BankAccountClearingRun.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BankAccountClearingRun' instances which match the given whereClause.
     */
    public long getBankAccountClearingRunCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(BankAccountClearingRun.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BankAccountClearingRun' instances which match the given whereClause together with params specified in object array.
     */
    public long getBankAccountClearingRunCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(BankAccountClearingRun.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBankAccountClearingRunCount() - result: count=" + result);
        }
        return result;
    }

	


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bank account clearing run)}
    //
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Long> findUnsentBankAccountClearingRunIds() {
    	final Query query = getPersistenceManager().newNamedQuery(BankAccountClearingRun.class, "findUnsentBankAccountClearingRunIds");
    	query.setResultClass(Long.class);
    	final Collection<Long> rawResult = (Collection<Long>) query.execute();
    	final Set<Long> result = new LinkedHashSet<Long>(rawResult);
		return result;
	}
    
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsUnsentClearingRun( BankAccount ba) {
    	final Query query = getPersistenceManager().newNamedQuery(BankAccountClearingRun.class, "findUnsentBankAccountClearingRunByBankAccount");
    	Map<Integer, Object> params = new HashMap<Integer, Object>();
        params.put(1, ba.getId());
    	query.setResultClass(Long.class);
    	final Collection<Long> rawResult = (Collection<Long>) query.executeWithMap(params);
    	final Set<Long> result = new LinkedHashSet<Long>(rawResult);
		return result.size() > 0;
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
