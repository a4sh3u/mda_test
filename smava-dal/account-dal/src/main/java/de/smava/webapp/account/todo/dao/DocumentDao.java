//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\temp\mda\model.xml
//
//    Or you can change the template:
//
//    P:\temp\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.account.todo.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Contract;
import de.smava.webapp.account.domain.Document;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.Serializable;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Documents'.
 *
 * @author generator
 */
public interface DocumentDao extends BaseDao<Document>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the document identified by the given id.
     *
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Document getDocument(Long id);

    /**
     * Saves the document.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link de.smava.webapp.commons.dao.BaseDao<>#save(T)} instead.
     */
    Long saveDocument(Document document);

    /**
     * Deletes an document, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document
     */
    void deleteDocument(Long id);

    /**
     * Retrieves all 'Document' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Document> getDocumentList();

    /**
     * Retrieves a page of 'Document' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     */
    Collection<Document> getDocumentList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Document' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<Document> getDocumentList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Document' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    Collection<Document> getDocumentList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'Document' instances.
     */
    long getDocumentCount();

    /**
     * Attaches a Account to the Document instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachAccount(Document document, Account account);

    /**
     * Attaches a Contract to the Document instance.
     * Both instances will be persistent and thus will be in hollow state after this operation.
     */
    void attachContract(Document document, Contract contract);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document)}
    //


    void detachContract(Document document, Contract contract);

    Collection<Document> findIncomingDocumentWithAttachmentsList(Account account, Pageable pageable, Sortable sortable);
    Collection<Document> findOutgoingDocumentWithAttachmentsList(Account account, Pageable pageable, Sortable sortable);
    Collection<Document> findOpenOutgoingDocumentListWithAttachmentsOrderByCreation(Account account, Pageable pageable, Sortable sortable);



    Collection<Document> findCreditPayoutDocuments(Pageable pageable, Sortable sortable);
    Document findLastDocument(Account account, String type, String direction, String state);
    Collection<Document> findDocumentsByAccountAndType(Long accountId, String documentType, Collection<String> typeOfDispatch, String direction);

    
    Collection<Document> findDocumentsByAccountAndTypeAndState(Long accountId, Set<String> types, Set<String> typeOfDispatch, String direction, Set<String> states);
    Collection<Document> findDocumentsByAccountAndTypeAndStateWithAttachmentsList(Long accountId, Set<String> types, Set<String> typeOfDispatch, String direction, Set<String> states);
    Collection<Document> getRepaymentBreakCalls(Account account);

    Collection<Document> findDocuments(Account account, Set<String> documentTypes, Set<String> states);
    
    Document findFirstLastDomument(final Account owner, final Set<String> types, final Set<String> states, final Set<String> directions, final boolean last);

    
    /** WEBSITE-10809
     * For account matching requirements, find smava documents of defined types with attachments to be deleted 
     * 
     * @param accountId
     * @return
     */
    public Collection<Document> retrieveDocumentsWithDeletableFiles(Long accountId);

    public Collection<Document> retrieveDocumentsWithDeletableFilesShortPeriod();

    public Collection<Document> retrieveBrokerageBanksDocumentsWithDeletableFiles();

    
	public Collection<Document> findAllDocuments(final Account owner, final Set<String> types, final Set<String> states, final Set<String> directions);

    public Collection<Document> getOpenIncomingDocuments(Account account);

    Map<Account, List<Document>> getMissingDocumentsGroupedByAccount(int days);

    Collection<Document> findDocumentsByAccount(Account account, String direction);

    Collection<Document> findDocumentsWithContract(long contractId);

    Collection<Document> getDocumentsOrderedById(List<Long> ids);

    /**
     * WEBSITE-14068
     *
     * get documents corresponding to beratung_nicht_notwendig
     * search withing 3 days from LA creation for document with specified title
     *
     * @param subject - document search
     * @param a - account
     * @param laCreation - loan application creation date for specified account and documentsearch
     * @return
     */
    Collection<Document> getDocumentsBySubject(String subject, Account a, Date laCreation);
    //
    // !!!!!!!! End of insert code section !!!!!!!!


}
