package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.interfaces.MessageConfigEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The domain object that represents 'MessageConfigs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractMessageConfig
    extends KreditPrivatEntity implements MessageConfigEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMessageConfig.class);

    private static final long serialVersionUID = 2813255745216543940L;

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        list.add(getAccount());
        return list;
    }

}

