package de.smava.webapp.account.domain.interfaces;


/**
 * The domain object that represents 'EncashmentRates'.
 *
 * @author generator
 */
public interface EncashmentRateEntityInterface {

    /**
     * Setter for the property 'market name'.
     *
     * 
     *
     */
    void setMarketName(String marketName);

    /**
     * Returns the property 'market name'.
     *
     * 
     *
     */
    String getMarketName();
    /**
     * Setter for the property 'rate'.
     *
     * 
     *
     */
    void setRate(Double rate);

    /**
     * Returns the property 'rate'.
     *
     * 
     *
     */
    Double getRate();
    /**
     * Setter for the property 'borrower percental charge'.
     *
     * 
     *
     */
    void setBorrowerPercentalCharge(Float borrowerPercentalCharge);

    /**
     * Returns the property 'borrower percental charge'.
     *
     * 
     *
     */
    Float getBorrowerPercentalCharge();
    /**
     * Setter for the property 'borrower fix charge'.
     *
     * 
     *
     */
    void setBorrowerFixCharge(Float borrowerFixCharge);

    /**
     * Returns the property 'borrower fix charge'.
     *
     * 
     *
     */
    Float getBorrowerFixCharge();
    /**
     * Setter for the property 'lender fix charge'.
     *
     * 
     *
     */
    void setLenderFixCharge(Float lenderFixCharge);

    /**
     * Returns the property 'lender fix charge'.
     *
     * 
     *
     */
    Float getLenderFixCharge();
    /**
     * Setter for the property 'lender percental charge'.
     *
     * 
     *
     */
    void setLenderPercentalCharge(Float lenderPercentalCharge);

    /**
     * Returns the property 'lender percental charge'.
     *
     * 
     *
     */
    Float getLenderPercentalCharge();

}
