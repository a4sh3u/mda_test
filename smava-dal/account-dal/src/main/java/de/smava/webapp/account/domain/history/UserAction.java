package de.smava.webapp.account.domain.history;

import java.util.Map;

import de.smava.webapp.commons.domain.Change;

/**
 * Interface for actions taken by users.
 * These actions have to be logged.
 *
 * @author michael.schwalbe (21.10.2005)
 */
public interface UserAction {
    /**
     * Get the changed data, where old values are mapped to new values of the data that was modified.
     * If data was initially set, only a key can be set.
     *
     * @return Map a mapping where old values are mapped to new values of the data that was modified
     */
    Map<String, Change> getChangeData();
}