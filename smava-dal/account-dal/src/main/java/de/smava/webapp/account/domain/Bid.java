package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BidHistory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * The domain object that represents 'Bids'.
 */
public class Bid extends BidHistory  {

    /**
     * Default constructor (would have been blocked by copy constructor).
     */
    public Bid() {
        _state = STATE_QUEUE;
    }

    /**
     * copy constructor for matching algorithm and contract creation.
     * uses groupId to mark as related.
     * if no groupId is set, bid.getId() will be used for new groupId for BOTH (old AND new bid)
     */
    public Bid(Bid bid) {
        setId(bid.getId()); //TODO: Check if this is necessary (matching test suite)
        _title = bid.getTitle();
        _description = bid.getDescription();
        _amount = bid.getAmount();
        if (bid.getCreationDate() != null) {
            _creationDate = new Date(bid.getCreationDate().getTime());
        }
        if (bid.getUpdateDate() != null) {
            _updateDate = new Date(bid.getUpdateDate().getTime());
        }

        _bidInterests = new ArrayList<BidInterest>();
        for (BidInterest bidInterest : bid.getBidInterests()) {
            String marketName = bidInterest.getMarketName();
            if (marketName.length() == 2) {
                marketName = marketName.charAt(0) + "0" + marketName.charAt(1);
            }
            putRateForMarket(marketName, bidInterest.getRate());
        }

        _state = bid.getState();

        if (bid.getGroupId() == 0) {
            _groupId = bid.getId();
            bid.setGroupId(bid.getId());
        } else {
            _groupId = bid.getGroupId();
        }
        if (bid.getContract() != null) {
            _contract = bid.getContract();
        }

    }

        protected String _title;
        protected String _description;
        protected double _amount;
        protected Date _creationDate;
        protected Date _updateDate;
        protected String _state;
        protected long _groupId;
        protected Collection<BidInterest> _bidInterests;
        protected Contract _contract;
        protected Date _lastMarketplaceDate;

                            /**
     * Setter for the property 'title'.
     */
    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = getTitle();
        }
        registerChange("title", _titleInitVal, title);
        _title = title;
    }

    /**
     * Returns the property 'title'.
     */
    public String getTitle() {
        return _title;
    }
                                    /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }

    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     */
    public double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'update date'.
     */
    public void setUpdateDate(Date updateDate) {
        if (!_updateDateIsSet) {
            _updateDateIsSet = true;
            _updateDateInitVal = getUpdateDate();
        }
        registerChange("update date", _updateDateInitVal, updateDate);
        _updateDate = updateDate;
    }
                        
    /**
     * Returns the property 'update date'.
     */
    public Date getUpdateDate() {
        return _updateDate;
    }
                                    /**
     * Setter for the property 'state'.
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     */
    public String getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'group id'.
     */
    public void setGroupId(long groupId) {
        _groupId = groupId;
    }
            
    /**
     * Returns the property 'group id'.
     */
    public long getGroupId() {
        return _groupId;
    }
                                            
    /**
     * Setter for the property 'bid interests'.
     */
    public void setBidInterests(Collection<BidInterest> bidInterests) {
        _bidInterests = bidInterests;
    }
            
    /**
     * Returns the property 'bid interests'.
     */
    public Collection<BidInterest> getBidInterests() {
        return _bidInterests;
    }
                                            
    /**
     * Setter for the property 'contract'.
     */
    public void setContract(Contract contract) {
        _contract = contract;
    }
            
    /**
     * Returns the property 'contract'.
     */
    public Contract getContract() {
        return _contract;
    }
                                    /**
     * Setter for the property 'last marketplace date'.
     */
    public void setLastMarketplaceDate(Date lastMarketplaceDate) {
        if (!_lastMarketplaceDateIsSet) {
            _lastMarketplaceDateIsSet = true;
            _lastMarketplaceDateInitVal = getLastMarketplaceDate();
        }
        registerChange("last marketplace date", _lastMarketplaceDateInitVal, lastMarketplaceDate);
        _lastMarketplaceDate = lastMarketplaceDate;
    }
                        
    /**
     * Returns the property 'last marketplace date'.
     */
    public Date getLastMarketplaceDate() {
        return _lastMarketplaceDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Bid.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _title=").append(_title);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _state=").append(_state);
            builder.append("\n}");
        } else {
            builder.append(Bid.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Bid instance() {
        return this;
    }
}
