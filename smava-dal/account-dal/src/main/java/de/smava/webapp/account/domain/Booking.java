package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.history.BookingHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.util.FormatUtils;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * The domain object that represents 'Bookings'.
 *
 * @author generator
 */
public class Booking extends BookingHistory  {

    private static final long serialVersionUID = 6665136137550786296L;

    public static class PoolPerformanceBookingInformation{

        public boolean _successful;
        public boolean _open;
        public long _contractId;
        public BookingType _bookingType;
        public double _amount;
        public Date _transactionDate;
        public boolean _target;
        public boolean _toInterim;
        public long _lenderBankAccount;

        public PoolPerformanceBookingInformation( boolean open,
                                                  boolean successful,
                                                  long contractId,
                                                  BookingType bookingType,
                                                  Date trxDate,
                                                  double amount,
                                                  boolean target,
                                                  boolean toInterim,
                                                  long lenderBankAccount){

            _successful = successful;
            _open = open;
            _contractId = contractId;
            _bookingType = bookingType;
            _transactionDate = trxDate;
            _amount = amount;
            _target = target;
            _toInterim = toInterim;
            _lenderBankAccount = lenderBankAccount;
        }

    }

    // Note: If you invent new Booking types then update SmavaDtausUtils.BOOKING_TYPE_NAMES, too!!!!
    // Note: TransactionHelper.copyCreateBooking may switch rolenames based on stringreplacement,
    // so every first occurence of "BORROWER" or "LENDER" may be replaced with the opposite

    private transient BookingType _enumType;

    public Booking() {
        _bookingAssignments = new HashSet<BookingAssignment>();
        _description = FormatUtils.formatShortDateTimeFormat(CurrentDate.getDate());
    }

    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }

    /**
     * Returns the property 'amount'.
     */
    public double getAmount() {
        return cutAmount(_amount);
    }

    /**
     * Returns the initial value of the property 'amount'.
     */
    public double amountInitVal() {
        double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }

    /**
     * Setter for the property 'type'.
     */
    public void setType(final BookingType type) {
        if (type == null) {
            setIntegerType(null);
        } else {
            setIntegerType(type.getDbValue());
        }
        _enumType = type;
    }

    /**
     * Returns the property 'type'.
     */
    public BookingType getType() {
        if (_enumType == null && _integerType != null) {
            _enumType = BookingType.enumFromDbValue(_integerType);
        }
        return _enumType;
    }

    public boolean isBorrowerInterest() {
        return BookingType.BORROWER_INTEREST.equals(getType());
    }

    public boolean isBorrowerAmortisation() {
        return BookingType.BORROWER_AMORTISATION.equals(getType());
    }

    public boolean isLenderInterest() {
        return BookingType.LENDER_INTEREST.equals(getType());
    }

    public boolean isLenderAmortisation() {
        return BookingType.LENDER_AMORTISATION.equals(getType());
    }

        protected Integer _integerType;
        protected double _amount;
        protected String _description;
        protected Transaction _transaction;
        protected Contract _contract;
        protected Collection<BookingAssignment> _bookingAssignments;
        
                                    
    /**
     * Setter for the property 'integer type'.
     */
    public void setIntegerType(Integer integerType) {
        _integerType = integerType;
    }
            
    /**
     * Returns the property 'integer type'.
     */
    public Integer getIntegerType() {
        return _integerType;
    }
                                                /**
     * Setter for the property 'description'.
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     */
    public String getDescription() {
        return _description;
    }
                                            
    /**
     * Setter for the property 'transaction'.
     */
    public void setTransaction(Transaction transaction) {
        _transaction = transaction;
    }
            
    /**
     * Returns the property 'transaction'.
     */
    public Transaction getTransaction() {
        return _transaction;
    }
                                            
    /**
     * Setter for the property 'contract'.
     */
    public void setContract(Contract contract) {
        _contract = contract;
    }
            
    /**
     * Returns the property 'contract'.
     */
    public Contract getContract() {
        return _contract;
    }
                                            
    /**
     * Setter for the property 'booking assignments'.
     */
    public void setBookingAssignments(Collection<BookingAssignment> bookingAssignments) {
        _bookingAssignments = bookingAssignments;
    }
            
    /**
     * Returns the property 'booking assignments'.
     */
    public Collection<BookingAssignment> getBookingAssignments() {
        return _bookingAssignments;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Booking.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _description=").append(_description);
            builder.append("\n}");
        } else {
            builder.append(Booking.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
