package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractSector;




/**
 * The domain object that has all history aggregation related fields for 'Sectors'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SectorHistory extends AbstractSector {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _codeInitVal;
    protected transient boolean _codeIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'code'.
     */
    public String codeInitVal() {
        String result;
        if (_codeIsSet) {
            result = _codeInitVal;
        } else {
            result = getCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'code'.
     */
    public boolean codeIsDirty() {
        return !valuesAreEqual(codeInitVal(), getCode());
    }

    /**
     * Returns true if the setter method was called for the property 'code'.
     */
    public boolean codeIsSet() {
        return _codeIsSet;
    }
					
}
