package de.smava.webapp.backoffice.domain.interfaces;



import de.smava.webapp.backoffice.domain.BoSearch;

import java.util.Date;


/**
 * The domain object that represents 'BoSearchs'.
 *
 * @author generator
 */
public interface BoSearchEntityInterface {

    /**
     * Setter for the property 'id'.
     *
     * 
     *
     */
    void setId(Long id);

    /**
     * Returns the property 'id'.
     *
     * 
     *
     */
    Long getId();
    /**
     * Setter for the property 'count'.
     *
     * 
     *
     */
    void setCount(Long count);

    /**
     * Returns the property 'count'.
     *
     * 
     *
     */
    Long getCount();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(Integer type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    Integer getType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'phone'.
     *
     * 
     *
     */
    void setPhone(String phone);

    /**
     * Returns the property 'phone'.
     *
     * 
     *
     */
    String getPhone();
    /**
     * Setter for the property 'phone2'.
     *
     * 
     *
     */
    void setPhone2(String phone2);

    /**
     * Returns the property 'phone2'.
     *
     * 
     *
     */
    String getPhone2();
    /**
     * Setter for the property 'email'.
     *
     * 
     *
     */
    void setEmail(String email);

    /**
     * Returns the property 'email'.
     *
     * 
     *
     */
    String getEmail();
    /**
     * Setter for the property 'birth date'.
     *
     * 
     *
     */
    void setBirthDate(Date birthDate);

    /**
     * Returns the property 'birth date'.
     *
     * 
     *
     */
    Date getBirthDate();
    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    void setLastName(String lastName);

    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    String getLastName();
    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    void setFirstName(String firstName);

    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    String getFirstName();
    /**
     * Helper method to get reference of this object as model type.
     */
    BoSearch asBoSearch();
}
