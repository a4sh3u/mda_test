package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.interfaces.MailLinkEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

/**
 * The domain object that represents 'MailLinks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractMailLink
    extends KreditPrivatEntity implements MailLinkEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMailLink.class);

    private static final long serialVersionUID = -5476137446716665764L;
}

