//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(economical data)}

import de.smava.webapp.account.dao.EconomicalDataDao;
import de.smava.webapp.account.domain.EconomicalData;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'EconomicalDatas'.
 *
 * @author generator
 */
@Repository(value = "economicalDataDao")
public class JdoEconomicalDataDao extends JdoBaseDao implements EconomicalDataDao {

    private static final Logger LOGGER = Logger.getLogger(JdoEconomicalDataDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(economical data)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the economical data identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public EconomicalData load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalData - start: id=" + id);
        }
        EconomicalData result = getEntity(EconomicalData.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalData - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public EconomicalData getEconomicalData(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	EconomicalData entity = findUniqueEntity(EconomicalData.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the economical data.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(EconomicalData economicalData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveEconomicalData: " + economicalData);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(economical data)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(economicalData);
    }

    /**
     * @deprecated Use {@link #save(EconomicalData) instead}
     */
    public Long saveEconomicalData(EconomicalData economicalData) {
        return save(economicalData);
    }

    /**
     * Deletes an economical data, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the economical data
     */
    public void deleteEconomicalData(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteEconomicalData: " + id);
        }
        deleteEntity(EconomicalData.class, id);
    }

    /**
     * Retrieves all 'EconomicalData' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<EconomicalData> getEconomicalDataList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - start");
        }
        Collection<EconomicalData> result = getEntities(EconomicalData.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'EconomicalData' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<EconomicalData> getEconomicalDataList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - start: pageable=" + pageable);
        }
        Collection<EconomicalData> result = getEntities(EconomicalData.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'EconomicalData' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<EconomicalData> getEconomicalDataList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - start: sortable=" + sortable);
        }
        Collection<EconomicalData> result = getEntities(EconomicalData.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'EconomicalData' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<EconomicalData> getEconomicalDataList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<EconomicalData> result = getEntities(EconomicalData.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'EconomicalData' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EconomicalData> findEconomicalDataList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - start: whereClause=" + whereClause);
        }
        Collection<EconomicalData> result = findEntities(EconomicalData.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'EconomicalData' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<EconomicalData> findEconomicalDataList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<EconomicalData> result = findEntities(EconomicalData.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'EconomicalData' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EconomicalData> findEconomicalDataList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<EconomicalData> result = findEntities(EconomicalData.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'EconomicalData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EconomicalData> findEconomicalDataList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<EconomicalData> result = findEntities(EconomicalData.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'EconomicalData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EconomicalData> findEconomicalDataList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<EconomicalData> result = findEntities(EconomicalData.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'EconomicalData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<EconomicalData> findEconomicalDataList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<EconomicalData> result = findEntities(EconomicalData.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findEconomicalDataList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'EconomicalData' instances.
     */
    public long getEconomicalDataCount() {
        long result = getEntityCount(EconomicalData.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataCount() - result: count=" + result);
        }
        return result;
    }

    @Override
    public Collection<EconomicalData> findAllByAccountIds(List<Long> borrowerIds) {
        Query query = getPersistenceManager().newQuery(EconomicalData.class);
        query.setFilter(":borrowerIds.contains(this._account._id)");
        return (Collection<EconomicalData>) query.execute(borrowerIds);
    }

    /**
     * Returns the number of 'EconomicalData' instances which match the given whereClause.
     */
    public long getEconomicalDataCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(EconomicalData.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'EconomicalData' instances which match the given whereClause together with params specified in object array.
     */
    public long getEconomicalDataCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(EconomicalData.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEconomicalDataCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(economical data)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
