package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractApproval;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Approvals'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ApprovalHistory extends AbstractApproval {

    protected transient Date _approvalDateInitVal;
    protected transient boolean _approvalDateIsSet;
    protected transient String _commentInitVal;
    protected transient boolean _commentIsSet;
    protected transient int _voteInitVal;
    protected transient boolean _voteIsSet;


			
    /**
     * Returns the initial value of the property 'approval date'.
     */
    public Date approvalDateInitVal() {
        Date result;
        if (_approvalDateIsSet) {
            result = _approvalDateInitVal;
        } else {
            result = getApprovalDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'approval date'.
     */
    public boolean approvalDateIsDirty() {
        return !valuesAreEqual(approvalDateInitVal(), getApprovalDate());
    }

    /**
     * Returns true if the setter method was called for the property 'approval date'.
     */
    public boolean approvalDateIsSet() {
        return _approvalDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'comment'.
     */
    public String commentInitVal() {
        String result;
        if (_commentIsSet) {
            result = _commentInitVal;
        } else {
            result = getComment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comment'.
     */
    public boolean commentIsDirty() {
        return !valuesAreEqual(commentInitVal(), getComment());
    }

    /**
     * Returns true if the setter method was called for the property 'comment'.
     */
    public boolean commentIsSet() {
        return _commentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'vote'.
     */
    public int voteInitVal() {
        int result;
        if (_voteIsSet) {
            result = _voteInitVal;
        } else {
            result = getVote();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vote'.
     */
    public boolean voteIsDirty() {
        return !valuesAreEqual(voteInitVal(), getVote());
    }

    /**
     * Returns true if the setter method was called for the property 'vote'.
     */
    public boolean voteIsSet() {
        return _voteIsSet;
    }
	
}
