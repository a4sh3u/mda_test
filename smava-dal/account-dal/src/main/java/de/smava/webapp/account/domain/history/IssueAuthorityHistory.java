package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractIssueAuthority;




/**
 * The domain object that has all history aggregation related fields for 'IssueAuthoritys'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IssueAuthorityHistory extends AbstractIssueAuthority {

    protected transient String _bkzInitVal;
    protected transient boolean _bkzIsSet;
    protected transient String _zipInitVal;
    protected transient boolean _zipIsSet;
    protected transient String _cityInitVal;
    protected transient boolean _cityIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;


	
    /**
     * Returns the initial value of the property 'bkz'.
     */
    public String bkzInitVal() {
        String result;
        if (_bkzIsSet) {
            result = _bkzInitVal;
        } else {
            result = getBkz();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bkz'.
     */
    public boolean bkzIsDirty() {
        return !valuesAreEqual(bkzInitVal(), getBkz());
    }

    /**
     * Returns true if the setter method was called for the property 'bkz'.
     */
    public boolean bkzIsSet() {
        return _bkzIsSet;
    }
	
    /**
     * Returns the initial value of the property 'zip'.
     */
    public String zipInitVal() {
        String result;
        if (_zipIsSet) {
            result = _zipInitVal;
        } else {
            result = getZip();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'zip'.
     */
    public boolean zipIsDirty() {
        return !valuesAreEqual(zipInitVal(), getZip());
    }

    /**
     * Returns true if the setter method was called for the property 'zip'.
     */
    public boolean zipIsSet() {
        return _zipIsSet;
    }
	
    /**
     * Returns the initial value of the property 'city'.
     */
    public String cityInitVal() {
        String result;
        if (_cityIsSet) {
            result = _cityInitVal;
        } else {
            result = getCity();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'city'.
     */
    public boolean cityIsDirty() {
        return !valuesAreEqual(cityInitVal(), getCity());
    }

    /**
     * Returns true if the setter method was called for the property 'city'.
     */
    public boolean cityIsSet() {
        return _cityIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }

}
