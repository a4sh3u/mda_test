package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.account.domain.abstracts.AbstractCoBorrowerEconomicalData;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CoBorrowerEconomicalDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CoBorrowerEconomicalDataHistory extends AbstractCoBorrowerEconomicalData {

    protected transient String _coBorrowerEmploymentInitVal;
    protected transient boolean _coBorrowerEmploymentIsSet;
    protected transient double _coBorrowerIncomeInitVal;
    protected transient boolean _coBorrowerIncomeIsSet;
    protected transient Date _coBorrowerStartOfOccupationInitVal;
    protected transient boolean _coBorrowerStartOfOccupationIsSet;
    protected transient boolean _coBorrowerTempEmploymentInitVal;
    protected transient boolean _coBorrowerTempEmploymentIsSet;
    protected transient Date _coBorrowerEndOfTempEmploymentInitVal;
    protected transient boolean _coBorrowerEndOfTempEmploymentIsSet;
    protected transient Sector _coBorrowerSectorInitVal;
    protected transient boolean _coBorrowerSectorIsSet;
    protected transient String _coBorrowerSectorDescriptionInitVal;
    protected transient boolean _coBorrowerSectorDescriptionIsSet;
    protected transient String _coBorrowerEmployerNameInitVal;
    protected transient boolean _coBorrowerEmployerNameIsSet;


	
    /**
     * Returns the initial value of the property 'co borrower employment'.
     */
    public String coBorrowerEmploymentInitVal() {
        String result;
        if (_coBorrowerEmploymentIsSet) {
            result = _coBorrowerEmploymentInitVal;
        } else {
            result = getCoBorrowerEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower employment'.
     */
    public boolean coBorrowerEmploymentIsDirty() {
        return !valuesAreEqual(coBorrowerEmploymentInitVal(), getCoBorrowerEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower employment'.
     */
    public boolean coBorrowerEmploymentIsSet() {
        return _coBorrowerEmploymentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower income'.
     */
    public double coBorrowerIncomeInitVal() {
        double result;
        if (_coBorrowerIncomeIsSet) {
            result = _coBorrowerIncomeInitVal;
        } else {
            result = getCoBorrowerIncome();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower income'.
     */
    public boolean coBorrowerIncomeIsDirty() {
        return !valuesAreEqual(coBorrowerIncomeInitVal(), getCoBorrowerIncome());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower income'.
     */
    public boolean coBorrowerIncomeIsSet() {
        return _coBorrowerIncomeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower start of occupation'.
     */
    public Date coBorrowerStartOfOccupationInitVal() {
        Date result;
        if (_coBorrowerStartOfOccupationIsSet) {
            result = _coBorrowerStartOfOccupationInitVal;
        } else {
            result = getCoBorrowerStartOfOccupation();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower start of occupation'.
     */
    public boolean coBorrowerStartOfOccupationIsDirty() {
        return !valuesAreEqual(coBorrowerStartOfOccupationInitVal(), getCoBorrowerStartOfOccupation());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower start of occupation'.
     */
    public boolean coBorrowerStartOfOccupationIsSet() {
        return _coBorrowerStartOfOccupationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower temp employment'.
     */
    public boolean coBorrowerTempEmploymentInitVal() {
        boolean result;
        if (_coBorrowerTempEmploymentIsSet) {
            result = _coBorrowerTempEmploymentInitVal;
        } else {
            result = getCoBorrowerTempEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower temp employment'.
     */
    public boolean coBorrowerTempEmploymentIsDirty() {
        return !valuesAreEqual(coBorrowerTempEmploymentInitVal(), getCoBorrowerTempEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower temp employment'.
     */
    public boolean coBorrowerTempEmploymentIsSet() {
        return _coBorrowerTempEmploymentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower end of temp employment'.
     */
    public Date coBorrowerEndOfTempEmploymentInitVal() {
        Date result;
        if (_coBorrowerEndOfTempEmploymentIsSet) {
            result = _coBorrowerEndOfTempEmploymentInitVal;
        } else {
            result = getCoBorrowerEndOfTempEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower end of temp employment'.
     */
    public boolean coBorrowerEndOfTempEmploymentIsDirty() {
        return !valuesAreEqual(coBorrowerEndOfTempEmploymentInitVal(), getCoBorrowerEndOfTempEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower end of temp employment'.
     */
    public boolean coBorrowerEndOfTempEmploymentIsSet() {
        return _coBorrowerEndOfTempEmploymentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower sector'.
     */
    public Sector coBorrowerSectorInitVal() {
        Sector result;
        if (_coBorrowerSectorIsSet) {
            result = _coBorrowerSectorInitVal;
        } else {
            result = getCoBorrowerSector();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower sector'.
     */
    public boolean coBorrowerSectorIsDirty() {
        return !valuesAreEqual(coBorrowerSectorInitVal(), getCoBorrowerSector());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower sector'.
     */
    public boolean coBorrowerSectorIsSet() {
        return _coBorrowerSectorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower sector description'.
     */
    public String coBorrowerSectorDescriptionInitVal() {
        String result;
        if (_coBorrowerSectorDescriptionIsSet) {
            result = _coBorrowerSectorDescriptionInitVal;
        } else {
            result = getCoBorrowerSectorDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower sector description'.
     */
    public boolean coBorrowerSectorDescriptionIsDirty() {
        return !valuesAreEqual(coBorrowerSectorDescriptionInitVal(), getCoBorrowerSectorDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower sector description'.
     */
    public boolean coBorrowerSectorDescriptionIsSet() {
        return _coBorrowerSectorDescriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'co borrower employer name'.
     */
    public String coBorrowerEmployerNameInitVal() {
        String result;
        if (_coBorrowerEmployerNameIsSet) {
            result = _coBorrowerEmployerNameInitVal;
        } else {
            result = getCoBorrowerEmployerName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'co borrower employer name'.
     */
    public boolean coBorrowerEmployerNameIsDirty() {
        return !valuesAreEqual(coBorrowerEmployerNameInitVal(), getCoBorrowerEmployerName());
    }

    /**
     * Returns true if the setter method was called for the property 'co borrower employer name'.
     */
    public boolean coBorrowerEmployerNameIsSet() {
        return _coBorrowerEmployerNameIsSet;
    }

}
