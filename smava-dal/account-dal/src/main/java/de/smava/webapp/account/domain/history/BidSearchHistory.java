package de.smava.webapp.account.domain.history;



import de.smava.webapp.account.domain.abstracts.AbstractBidSearch;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BidSearchs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BidSearchHistory extends AbstractBidSearch {

    protected transient Date _validUntilInitVal;
    protected transient boolean _validUntilIsSet;
    protected transient double _interestMinInitVal;
    protected transient boolean _interestMinIsSet;
    protected transient double _interestMaxInitVal;
    protected transient boolean _interestMaxIsSet;
    protected transient double _amountMinInitVal;
    protected transient boolean _amountMinIsSet;
    protected transient double _amountMaxInitVal;
    protected transient boolean _amountMaxIsSet;
    protected transient boolean _includeFinishedOrdersInitVal;
    protected transient boolean _includeFinishedOrdersIsSet;
    protected transient int _creditRateIndicatorInitVal;
    protected transient boolean _creditRateIndicatorIsSet;
    protected transient int _matchedRateInitVal;
    protected transient boolean _matchedRateIsSet;
    protected transient String _searchPhraseInitVal;
    protected transient boolean _searchPhraseIsSet;
    protected transient String _employmentInitVal;
    protected transient boolean _employmentIsSet;
    protected transient boolean _isMailNotificationWantedInitVal;
    protected transient boolean _isMailNotificationWantedIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient double _minInvestmentLimitPerBorrowerInitVal;
    protected transient boolean _minInvestmentLimitPerBorrowerIsSet;
    protected transient double _investmentLimitPerBorrowerInitVal;
    protected transient boolean _investmentLimitPerBorrowerIsSet;


	
    /**
     * Returns the initial value of the property 'valid until'.
     */
    public Date validUntilInitVal() {
        Date result;
        if (_validUntilIsSet) {
            result = _validUntilInitVal;
        } else {
            result = getValidUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid until'.
     */
    public boolean validUntilIsDirty() {
        return !valuesAreEqual(validUntilInitVal(), getValidUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'valid until'.
     */
    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }
		
    /**
     * Returns the initial value of the property 'interest min'.
     */
    public double interestMinInitVal() {
        double result;
        if (_interestMinIsSet) {
            result = _interestMinInitVal;
        } else {
            result = getInterestMin();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'interest min'.
     */
    public boolean interestMinIsDirty() {
        return !valuesAreEqual(interestMinInitVal(), getInterestMin());
    }

    /**
     * Returns true if the setter method was called for the property 'interest min'.
     */
    public boolean interestMinIsSet() {
        return _interestMinIsSet;
    }
	
    /**
     * Returns the initial value of the property 'interest max'.
     */
    public double interestMaxInitVal() {
        double result;
        if (_interestMaxIsSet) {
            result = _interestMaxInitVal;
        } else {
            result = getInterestMax();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'interest max'.
     */
    public boolean interestMaxIsDirty() {
        return !valuesAreEqual(interestMaxInitVal(), getInterestMax());
    }

    /**
     * Returns true if the setter method was called for the property 'interest max'.
     */
    public boolean interestMaxIsSet() {
        return _interestMaxIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount min'.
     */
    public double amountMinInitVal() {
        double result;
        if (_amountMinIsSet) {
            result = _amountMinInitVal;
        } else {
            result = getAmountMin();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount min'.
     */
    public boolean amountMinIsDirty() {
        return !valuesAreEqual(amountMinInitVal(), getAmountMin());
    }

    /**
     * Returns true if the setter method was called for the property 'amount min'.
     */
    public boolean amountMinIsSet() {
        return _amountMinIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount max'.
     */
    public double amountMaxInitVal() {
        double result;
        if (_amountMaxIsSet) {
            result = _amountMaxInitVal;
        } else {
            result = getAmountMax();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount max'.
     */
    public boolean amountMaxIsDirty() {
        return !valuesAreEqual(amountMaxInitVal(), getAmountMax());
    }

    /**
     * Returns true if the setter method was called for the property 'amount max'.
     */
    public boolean amountMaxIsSet() {
        return _amountMaxIsSet;
    }
	
    /**
     * Returns the initial value of the property 'include finished orders'.
     */
    public boolean includeFinishedOrdersInitVal() {
        boolean result;
        if (_includeFinishedOrdersIsSet) {
            result = _includeFinishedOrdersInitVal;
        } else {
            result = getIncludeFinishedOrders();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'include finished orders'.
     */
    public boolean includeFinishedOrdersIsDirty() {
        return !valuesAreEqual(includeFinishedOrdersInitVal(), getIncludeFinishedOrders());
    }

    /**
     * Returns true if the setter method was called for the property 'include finished orders'.
     */
    public boolean includeFinishedOrdersIsSet() {
        return _includeFinishedOrdersIsSet;
    }
			
    /**
     * Returns the initial value of the property 'credit rate indicator'.
     */
    public int creditRateIndicatorInitVal() {
        int result;
        if (_creditRateIndicatorIsSet) {
            result = _creditRateIndicatorInitVal;
        } else {
            result = getCreditRateIndicator();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'credit rate indicator'.
     */
    public boolean creditRateIndicatorIsDirty() {
        return !valuesAreEqual(creditRateIndicatorInitVal(), getCreditRateIndicator());
    }

    /**
     * Returns true if the setter method was called for the property 'credit rate indicator'.
     */
    public boolean creditRateIndicatorIsSet() {
        return _creditRateIndicatorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'matched rate'.
     */
    public int matchedRateInitVal() {
        int result;
        if (_matchedRateIsSet) {
            result = _matchedRateInitVal;
        } else {
            result = getMatchedRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'matched rate'.
     */
    public boolean matchedRateIsDirty() {
        return !valuesAreEqual(matchedRateInitVal(), getMatchedRate());
    }

    /**
     * Returns true if the setter method was called for the property 'matched rate'.
     */
    public boolean matchedRateIsSet() {
        return _matchedRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'search phrase'.
     */
    public String searchPhraseInitVal() {
        String result;
        if (_searchPhraseIsSet) {
            result = _searchPhraseInitVal;
        } else {
            result = getSearchPhrase();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'search phrase'.
     */
    public boolean searchPhraseIsDirty() {
        return !valuesAreEqual(searchPhraseInitVal(), getSearchPhrase());
    }

    /**
     * Returns true if the setter method was called for the property 'search phrase'.
     */
    public boolean searchPhraseIsSet() {
        return _searchPhraseIsSet;
    }
	
    /**
     * Returns the initial value of the property 'employment'.
     */
    public String employmentInitVal() {
        String result;
        if (_employmentIsSet) {
            result = _employmentInitVal;
        } else {
            result = getEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employment'.
     */
    public boolean employmentIsDirty() {
        return !valuesAreEqual(employmentInitVal(), getEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'employment'.
     */
    public boolean employmentIsSet() {
        return _employmentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'is mail notification wanted'.
     */
    public boolean isMailNotificationWantedInitVal() {
        boolean result;
        if (_isMailNotificationWantedIsSet) {
            result = _isMailNotificationWantedInitVal;
        } else {
            result = getIsMailNotificationWanted();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'is mail notification wanted'.
     */
    public boolean isMailNotificationWantedIsDirty() {
        return !valuesAreEqual(isMailNotificationWantedInitVal(), getIsMailNotificationWanted());
    }

    /**
     * Returns true if the setter method was called for the property 'is mail notification wanted'.
     */
    public boolean isMailNotificationWantedIsSet() {
        return _isMailNotificationWantedIsSet;
    }
					
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'min investment limit per borrower'.
     */
    public double minInvestmentLimitPerBorrowerInitVal() {
        double result;
        if (_minInvestmentLimitPerBorrowerIsSet) {
            result = _minInvestmentLimitPerBorrowerInitVal;
        } else {
            result = getMinInvestmentLimitPerBorrower();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'min investment limit per borrower'.
     */
    public boolean minInvestmentLimitPerBorrowerIsDirty() {
        return !valuesAreEqual(minInvestmentLimitPerBorrowerInitVal(), getMinInvestmentLimitPerBorrower());
    }

    /**
     * Returns true if the setter method was called for the property 'min investment limit per borrower'.
     */
    public boolean minInvestmentLimitPerBorrowerIsSet() {
        return _minInvestmentLimitPerBorrowerIsSet;
    }
	
    /**
     * Returns the initial value of the property 'investment limit per borrower'.
     */
    public double investmentLimitPerBorrowerInitVal() {
        double result;
        if (_investmentLimitPerBorrowerIsSet) {
            result = _investmentLimitPerBorrowerInitVal;
        } else {
            result = getInvestmentLimitPerBorrower();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'investment limit per borrower'.
     */
    public boolean investmentLimitPerBorrowerIsDirty() {
        return !valuesAreEqual(investmentLimitPerBorrowerInitVal(), getInvestmentLimitPerBorrower());
    }

    /**
     * Returns true if the setter method was called for the property 'investment limit per borrower'.
     */
    public boolean investmentLimitPerBorrowerIsSet() {
        return _investmentLimitPerBorrowerIsSet;
    }
				
}
