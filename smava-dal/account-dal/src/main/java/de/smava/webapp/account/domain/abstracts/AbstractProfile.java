package de.smava.webapp.account.domain.abstracts;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.ImageAwareEntity;
import de.smava.webapp.account.domain.interfaces.ProfileEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

/**
 * The domain object that represents 'Profiles'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 */
public abstract class AbstractProfile
    extends KreditPrivatEntity implements ImageAwareEntity,ProfileEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractProfile.class);

    private static final long serialVersionUID = -8181289784682044489L;

    public static final String STATE_OK = "OK";
    public static final String STATE_LOCKED = "LOCKED";

    public String getFullImagePath(String imagePath) {
        return imagePath + "/" + getAccount().getId();
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        list.add(getAccount());
        return list;
    }

}

