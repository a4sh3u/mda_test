//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.account.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid)}

import de.smava.webapp.account.dao.BidDao;
import de.smava.webapp.account.domain.Bid;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.util.Arrays;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO implementation for the domain object 'Bids'.
 *
 * @author generator
 */
@Repository(value = "bidDao")
public class JdoBidDao extends JdoBaseDao implements BidDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBidDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bid)}
    //
    // insert custom fields here
    //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the bid identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Bid load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBid - start: id=" + id);
        }
        Bid result = getEntity(Bid.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBid - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Bid getBid(Long id) {
        return load(id);
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            Bid entity = findUniqueEntity(Bid.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the bid.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Bid bid) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBid: " + bid);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bid)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bid);
    }

    /**
     * @deprecated Use {@link #save(Bid) instead}
     */
    public Long saveBid(Bid bid) {
        return save(bid);
    }

    /**
     * Deletes an bid, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid
     */
    public void deleteBid(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBid: " + id);
        }
        deleteEntity(Bid.class, id);
    }

    /**
     * Retrieves all 'Bid' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Bid> getBidList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - start");
        }
        Collection<Bid> result = getEntities(Bid.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Bid' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Bid> getBidList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - start: pageable=" + pageable);
        }
        Collection<Bid> result = getEntities(Bid.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Bid' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Bid> getBidList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - start: sortable=" + sortable);
        }
        Collection<Bid> result = getEntities(Bid.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Bid' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Bid> getBidList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Bid> result = getEntities(Bid.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Bid' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Bid> findBidList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - start: whereClause=" + whereClause);
        }
        Collection<Bid> result = findEntities(Bid.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'Bid' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<Bid> findBidList(String whereClause, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<Bid> result = findEntities(Bid.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - result: size=" + result.size());
        }
        return result;
    }


    /**
     * Retrieves a page of 'Bid' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Bid> findBidList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<Bid> result = findEntities(Bid.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Bid' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Bid> findBidList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<Bid> result = findEntities(Bid.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Bid' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Bid> findBidList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Bid> result = findEntities(Bid.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Bid' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<Bid> findBidList(String whereClause, Pageable pageable, Sortable sortable, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<Bid> result = findEntities(Bid.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findBidList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Bid' instances.
     */
    public long getBidCount() {
        long result = getEntityCount(Bid.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Bid' instances which match the given whereClause.
     */
    private long getBidCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(Bid.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'Bid' instances which match the given whereClause together with params specified in object array.
     */
    private long getBidCount(String whereClause, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(Bid.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBidCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid)}

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Bid> findQueuedBids() {
        final PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(pm.getExtent(Bid.class, true));
        query.setFilter("_state == '"+Bid.STATE_QUEUE+"'");
        query.setOrdering("_creationDate ascending");
        return (Collection<Bid>) query.execute();
    }


    /**
     * A JDO callback for finding queued bids.
     *
     * @author Daniel Doubleday
     */

    // !!!!!!!! End of insert code section !!!!!!!!
}
