package de.smava.webapp.account.domain;

import junit.framework.Assert;

import org.junit.Test;

import de.smava.test.TestDataCreator;

public class BankAccountTest {

	@Test
	public void functionallyEqualsTest(){
		BankAccount original = TestDataCreator.createRefBankAccount(TestDataCreator.createBorrower());
		
		BankAccount dif = TestDataCreator.createRefBankAccount(TestDataCreator.createBorrower());
		
		Assert.assertTrue( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		dif.setCustomerNumber("99fdsfdsdsfd");
		Assert.assertTrue( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		dif.setIban( dif.getIban() +"001");
		
		Assert.assertFalse( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		dif.setIban(null);
		
		Assert.assertFalse( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		dif.setAccountNo("99fdsfdsdsfd");
		Assert.assertFalse( original.functionallyEqual(dif, true));
		Assert.assertFalse( original.functionallyEqual(dif, false));
		
		
		//reset dif
		dif = TestDataCreator.createRefBankAccount(TestDataCreator.createBorrower());		
		Assert.assertTrue( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		
		dif.setBic(null);
		
		Assert.assertFalse( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		dif.setBic(dif.getBic() + "anotherBic");
		Assert.assertFalse( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
		dif.setBankCode("fdfdsfd");
		Assert.assertFalse( original.functionallyEqual(dif, true));
		Assert.assertFalse( original.functionallyEqual(dif, false));
		
	}
	
	
	@Test
	public void functionallyEqualsLeading000Test(){
		BankAccount original = TestDataCreator.createRefBankAccount(TestDataCreator.createBorrower());
		
		BankAccount dif = TestDataCreator.createRefBankAccount(TestDataCreator.createBorrower());
		
		dif.setAccountNo("000"+ dif.getAccountNo());
		
		Assert.assertTrue( original.functionallyEqual(dif, true));
		Assert.assertTrue( original.functionallyEqual(dif, false));
		
	
		
	}
}
