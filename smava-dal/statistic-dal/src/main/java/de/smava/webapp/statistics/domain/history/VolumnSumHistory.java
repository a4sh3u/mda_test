package de.smava.webapp.statistics.domain.history;



import de.smava.webapp.statistics.domain.abstracts.AbstractVolumnSum;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'VolumnSums'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class VolumnSumHistory extends AbstractVolumnSum {

    protected transient String _ratingInitVal;
    protected transient boolean _ratingIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient Date _monthInitVal;
    protected transient boolean _monthIsSet;


	
    /**
     * Returns the initial value of the property 'rating'.
     */
    public String ratingInitVal() {
        String result;
        if (_ratingIsSet) {
            result = _ratingInitVal;
        } else {
            result = getRating();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rating'.
     */
    public boolean ratingIsDirty() {
        return !valuesAreEqual(ratingInitVal(), getRating());
    }

    /**
     * Returns true if the setter method was called for the property 'rating'.
     */
    public boolean ratingIsSet() {
        return _ratingIsSet;
    }
		
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
							
    /**
     * Returns the initial value of the property 'month'.
     */
    public Date monthInitVal() {
        Date result;
        if (_monthIsSet) {
            result = _monthInitVal;
        } else {
            result = getMonth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'month'.
     */
    public boolean monthIsDirty() {
        return !valuesAreEqual(monthInitVal(), getMonth());
    }

    /**
     * Returns true if the setter method was called for the property 'month'.
     */
    public boolean monthIsSet() {
        return _monthIsSet;
    }

}
