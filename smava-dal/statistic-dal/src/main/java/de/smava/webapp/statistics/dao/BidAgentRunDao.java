//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid agent run)}
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.statistics.domain.BidAgentRun;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BidAgentRuns'.
 *
 * @author generator
 */
public interface BidAgentRunDao extends BaseDao<BidAgentRun> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bid agent run identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BidAgentRun getBidAgentRun(Long id);

    /**
     * Saves the bid agent run.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBidAgentRun(BidAgentRun bidAgentRun);

    /**
     * Deletes an bid agent run, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bid agent run
     */
    void deleteBidAgentRun(Long id);

    /**
     * Retrieves all 'BidAgentRun' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BidAgentRun> getBidAgentRunList();

    /**
     * Retrieves a page of 'BidAgentRun' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BidAgentRun> getBidAgentRunList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BidAgentRun' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BidAgentRun> getBidAgentRunList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BidAgentRun' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BidAgentRun> getBidAgentRunList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BidAgentRun' instances.
     */
    long getBidAgentRunCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bid agent run)}
    //
    // insert custom methods here

    /**
     * Returns all BidAgentRun that are not finished
     * @param orderId
     * @return
     */
    List<BidAgentRun> findOpenBidAgentRunforOrder(Long orderId);

    /**
     * Returns all BidAgentRun that are not started.
     * @param orderId
     * @return
     */
    List<BidAgentRun> findUnstartedBidAgentRunforOrder(Long orderId);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
