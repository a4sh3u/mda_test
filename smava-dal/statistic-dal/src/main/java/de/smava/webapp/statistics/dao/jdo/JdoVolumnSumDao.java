//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(volumn sum)}
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.statistics.dao.VolumnSumDao;
import de.smava.webapp.statistics.domain.VolumnSum;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'VolumnSums'.
 *
 * @author generator
 */
@Repository(value = "volumnSumDao")
public class JdoVolumnSumDao extends JdoBaseDao implements VolumnSumDao {

    private static final Logger LOGGER = Logger.getLogger(JdoVolumnSumDao.class);

    private static final String CLASS_NAME = "VolumnSum";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(volumn sum)}
        //
    private static final String TYPE_PLACEHOLDER = "##type##";
    
    private static final String VOLUMNDATA_SQL 
    	= "select \n" + 
			"	'" + TYPE_PLACEHOLDER + "' as \"type\", \n" + 
			"	to_number(c.market_name,'99V9') as \"duration\",\n" + 
			"	substring(c.market_name, 1, 1) as \"rating\",\n" + 
			"   sum(c.amount) as \"volumn\", " +
			"	sum(\n" + 
			"		case \n" + 
			"			when b.id = b.\"groupId\" then date_part('month', age(:forMonth, date_trunc('month', c.start_date))) + date_part('year',age(:forMonth, date_trunc('month', c.start_date)))*12 " + 
			"			else 0\n" + 
			"		end\n" + 
			"		) / count(distinct b.\"groupId\") as \"avarageCreditprojectAge\",\n" + 
			"	count(c.id) as \"contractCount\", " +    			
			"	count(distinct od.account_id) as \"borrowerCount\", " +
			"	count(distinct o.account_id) as \"lenderCount\", " +
			"	count(distinct b.\"groupId\") as \"creditprojectCount\" " + 
			"from contract c\n" + 
			"inner join \"order\" od on od.id = c.order_id\n" + 
			"inner join bid b on b.id = od.id\n" + 
			"inner join offer o on o.id = c.offer_id\n" + 
			"where 1 = 1 " + 
			"and c.start_date is not null " +
			"and c.start_date < :forMonth " +
			"and c.state in (:contractStates) " + 
			"group by " + 
			"	c.market_name ";
    
    private static final String POOLDATA_SQL
	    = "select \n" + 
		"	'" + TYPE_PLACEHOLDER + "' as \"type\", \n" + 
		"	to_number(c.market_name,'99V9') as \"duration\",\n" + 
		"	substring(c.market_name, 1, 1) as \"rating\",\n" + 
		"   sum(c.amount) as \"volumn\", " +
		"	sum(\n" + 
		"		case \n" + 
		"			when b.id = b.\"groupId\" then date_part('month', age(:forMonth, date_trunc('month', c.start_date))) + date_part('year',age(:forMonth, date_trunc('month', c.start_date)))*12 " + 
		"			else 0\n" + 
		"		end\n" + 
		"		) / count(distinct b.\"groupId\") as \"avarageCreditprojectAge\",\n" + 
		"	count(c.id) as \"contractCount\", " +    			
		"	count(distinct od.account_id) as \"borrowerCount\", " +
		"	count(distinct o.account_id) as \"lenderCount\", " +
		"	count(distinct b.\"groupId\") as \"creditprojectCount\" " + 
		"from contract c\n" + 
		"inner join \"order\" od on od.id = c.order_id\n" + 
		"inner join bid b on b.id = od.id\n" + 
		"inner join offer o on o.id = c.offer_id\n" + 
		"where 1 = 1 " + 
		"and c.start_date is not null " +
		"and c.start_date < :forMonth " +
		"and (c.state in (:contractStates) and date_part('month', age(:forMonth, date_trunc('month', c.start_date))) + date_part('year',age(:forMonth, date_trunc('month', c.start_date)))*12 < to_number(c.market_name,'99V9') + 6) " + 
		"group by " + 
		"	c.market_name ";
    
    
    private static final String DEFAULT_DATA_SQL 
    	= "select " +
			"substring(market, 1, 1), to_number(market,'99V9'), " +
			"loan_sum, borr_arm, intrum_sum, sum_tmp.base_sum from \n" + 
			"(\n" + 
			"select market, sum(borr_arm) as borr_arm, sum(intrum_sum) as intrum_sum, sum(loan_sum) as loan_sum \n" + 
			"\n" + 
			"from (select \n" + 
			"c.id, \n" +
			"c.amount as loan_sum, \n" +
			"c.market_name as market, \n" +  
			"sum(case when (credit.type = 'smava interim account in' and debit.type='KNK') then b.amount else 0 end) - sum(case when (debit.type = 'smava interim account in' and credit.type='KNK') then b.amount else 0 end) as borr_arm, \n" + 
			"sum(case when (credit.type = 'smava interim account in' and debit.type in ('encashment interim account','encashment account')) then b.amount else 0 end) as intrum_sum\n" + 
			" " + 
			"from contract c,\n" + 
			"bid bd,\n" + 
			"transaction t, \n" + 
			"booking b, \n" + 
			"bank_account credit, \n" + 
			"bank_account debit \n" + 
			" \n" + 
			"where 1=1 \n" + 
			"and c.order_id=bd.id \n" + 
			"and b.transaction_id=t.id \n" + 
			"and b.contract_id=c.id \n" + 
			"and t.debit_account=debit.id \n" + 
			"and t.credit_account=credit.id \n" + 
			"and c.state = 'DEFAULT' \n" + 
			"and date_trunc('month',start_date) + interval '20 days' + interval '3 months' < :now \n" +
			"and b.type_new in (7,26) " + 
			"and t.state in ('SUCCESSFUL','OPEN') " + 
			"and (credit.id in (:interimBankAccountIds) or debit.id in (:interimBankAccountIds)) " + 
			"and credit.type in ('smava interim account in', 'smava interim account out','KNK') " + 
			"and debit.type in ('smava interim account in', 'smava interim account out','KNK','encashment interim account','encashment account') \n" + 
			" \n" + 
			"group by c.id, c.amount, c.market_name \n" + 
			") tmp1 \n" + 
			"group by market \n" + 
			") tmp2," +
			"( \n" + 
			"select c1.market_name as base_sum_market, sum(c1.amount) as base_sum \n" + 
			"from contract c1 \n" + 
			"where c1.state in (:baseSumContractStates) \n" + 
			"and c1.start_date is not null \n" + 
			"and date_trunc('month',start_date) + interval '20 days' + interval '3 months' < :now \n" + 
			"group by c1.market_name \n" + 
			") as sum_tmp \n" + 
			"where sum_tmp.base_sum_market = tmp2.market";
    
    private static final Map<String, String> TYPE_SQL_MAP;
    
    static {
    	final Map<String, String> typeSqlMap = new LinkedHashMap<String, String>(VolumnSum.VOLUMN_TYPES.size() + VolumnSum.POOL_TYPES.size());
    	for (String volumnType : VolumnSum.VOLUMN_TYPES) {
			typeSqlMap.put(volumnType, VOLUMNDATA_SQL.replace(TYPE_PLACEHOLDER, volumnType));
		}
    	
    	for (String poolType : VolumnSum.POOL_TYPES) {
			typeSqlMap.put(poolType, POOLDATA_SQL.replace(TYPE_PLACEHOLDER, poolType));
		}
    	
    	TYPE_SQL_MAP = Collections.unmodifiableMap(typeSqlMap);
    }
    
    
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the volumn sum identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public VolumnSum load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        VolumnSum result = getEntity(VolumnSum.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public VolumnSum getVolumnSum(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	VolumnSum entity = findUniqueEntity(VolumnSum.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the volumn sum.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(VolumnSum volumnSum) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveVolumnSum: " + volumnSum);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(volumn sum)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(volumnSum);
    }

    /**
     * @deprecated Use {@link #save(VolumnSum) instead}
     */
    public Long saveVolumnSum(VolumnSum volumnSum) {
        return save(volumnSum);
    }

    /**
     * Deletes an volumn sum, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the volumn sum
     */
    public void deleteVolumnSum(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteVolumnSum: " + id);
        }
        deleteEntity(VolumnSum.class, id);
    }

    /**
     * Retrieves all 'VolumnSum' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<VolumnSum> getVolumnSumList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<VolumnSum> result = getEntities(VolumnSum.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'VolumnSum' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<VolumnSum> getVolumnSumList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<VolumnSum> result = getEntities(VolumnSum.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'VolumnSum' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<VolumnSum> getVolumnSumList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<VolumnSum> result = getEntities(VolumnSum.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'VolumnSum' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<VolumnSum> getVolumnSumList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<VolumnSum> result = getEntities(VolumnSum.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'VolumnSum' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<VolumnSum> findVolumnSumList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<VolumnSum> result = findEntities(VolumnSum.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'VolumnSum' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<VolumnSum> findVolumnSumList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<VolumnSum> result = findEntities(VolumnSum.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'VolumnSum' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<VolumnSum> findVolumnSumList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<VolumnSum> result = findEntities(VolumnSum.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'VolumnSum' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<VolumnSum> findVolumnSumList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<VolumnSum> result = findEntities(VolumnSum.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'VolumnSum' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<VolumnSum> findVolumnSumList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<VolumnSum> result = findEntities(VolumnSum.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'VolumnSum' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<VolumnSum> findVolumnSumList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<VolumnSum> result = findEntities(VolumnSum.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'VolumnSum' instances.
     */
    public long getVolumnSumCount() {
        long result = getEntityCount(VolumnSum.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'VolumnSum' instances which match the given whereClause.
     */
    public long getVolumnSumCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(VolumnSum.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'VolumnSum' instances which match the given whereClause together with params specified in object array.
     */
    public long getVolumnSumCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(VolumnSum.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(volumn sum)}
    //
    
    public Collection<Object[]> getVolumnsFromData(final String type, final Set<String> contractStates, final Date forMonth) {
    	final String typeSql = TYPE_SQL_MAP.get(type);
    	if (StringUtils.isEmpty(typeSql)) {
    		throw new IllegalArgumentException("unkwoen type '" + type + "'");
    	}
    	final Map<String, Object> params = new LinkedHashMap<String, Object>();
    	params.put("forMonth", new Timestamp(forMonth.getTime()));
    	params.put("contractStates", contractStates);    	
		final String sql = replaceCollectionTypes(params, typeSql);
		final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
		return (Collection<Object[]>) query.executeWithMap(params);
	}

	public void deleteMonth(Date monthToDelete) {
		//ensure table existence
		final long count = getVolumnSumCount();
		if (count > 0) {
			final String sql = "delete from stats_volumn_sums where month = ?";
			final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
			query.execute(new Timestamp(monthToDelete.getTime()));
		}
	}

	public Date getMaxMonthForType(final String type) {
		final Date result;
		//ensure table existence
		final long count = getVolumnSumCount();
		if (count > 0) {
			final String sql = "select max(svs.month) from stats_volumn_sums svs where svs.type = :type";
			final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
			query.setUnique(true);
			query.setResultClass(Date.class);
			result = (Date) query.executeWithMap(getSingleParamMap("type", type));
		} else {
			result = null;
		}
		return result;
	}
	
	public List<Object[]> getDeafultData(final Date now, final Set<Long> interimBankAccountIds, final Set<String> baseSumContractStates) {
		final Map<String, Object> params = new LinkedHashMap<String, Object>(2);
		params.put("now", new Timestamp(now.getTime()));
		params.put("interimBankAccountIds", interimBankAccountIds);
		params.put("baseSumContractStates", baseSumContractStates);
		final String sql = replaceCollectionTypes(params, DEFAULT_DATA_SQL);
		final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
		return (List<Object[]>) query.executeWithMap(params);
	}
	
	public List<VolumnSum> findVolumnsForDuration(final Date currentMonth, final Set<String> types, final Integer duration) {
		final Map<String, Object> params = new LinkedHashMap<String, Object>(3);
		params.put("currentMonth", currentMonth);
		params.put("types", types);
		params.put("duration", duration);
		final Query query = getPersistenceManager().newNamedQuery(VolumnSum.class, "findVolumnsForDuration");
		return (List<VolumnSum>) query.executeWithMap(params);
	}
	
	public Collection<VolumnSum> getActualPoolSumsForDuration(
			Collection<String> types, int performanceStatsDuration, Date now) {
		return findVolumnsForDuration(now, new LinkedHashSet<String>(types), performanceStatsDuration);
	}
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
