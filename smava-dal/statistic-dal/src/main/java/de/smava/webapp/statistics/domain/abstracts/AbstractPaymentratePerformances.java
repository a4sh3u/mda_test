//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.statistics.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(paymentrate performances)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.statistics.domain.interfaces.PaymentratePerformancesEntityInterface;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PaymentratePerformancess'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractPaymentratePerformances
    extends KreditPrivatEntity implements PaymentratePerformancesEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractPaymentratePerformances.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(paymentrate performances)}
    /**
     * generated serial.
     */
    private static final long serialVersionUID = 4530489338049627493L;

    public static final String TYPE_ROI						 = "ROI";
    public static final String TYPE_EXEMPLAR_INTEREST		 = "EXEMPLAR_INTEREST";
    public static final String TYPE_EXPECTED_PAYMENT_RATE	 = "EXPECTED_PAYMENT_RATE";
    public static final String TYPE_AVERAGE_PAYMENT_RATE	 = "AVERAGE_PAYMENT_RATE";
    public static final String TYPE_LAST_FINALE_PAYMENT_RATE = "LAST_FINALE_PAYMENT_RATE";


    // !!!!!!!! End of insert code section !!!!!!!!
}

