package de.smava.webapp.statistics.domain.history;



import de.smava.webapp.statistics.domain.abstracts.AbstractAverageInterest;




/**
 * The domain object that has all history aggregation related fields for 'AverageInterests'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AverageInterestHistory extends AbstractAverageInterest {

    protected transient String _ratingInitVal;
    protected transient boolean _ratingIsSet;
    protected transient int _durationInitVal;
    protected transient boolean _durationIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient double _valueInitVal;
    protected transient boolean _valueIsSet;
    protected transient int _calculationTermInitVal;
    protected transient boolean _calculationTermIsSet;


	
    /**
     * Returns the initial value of the property 'rating'.
     */
    public String ratingInitVal() {
        String result;
        if (_ratingIsSet) {
            result = _ratingInitVal;
        } else {
            result = getRating();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rating'.
     */
    public boolean ratingIsDirty() {
        return !valuesAreEqual(ratingInitVal(), getRating());
    }

    /**
     * Returns true if the setter method was called for the property 'rating'.
     */
    public boolean ratingIsSet() {
        return _ratingIsSet;
    }
	
    /**
     * Returns the initial value of the property 'duration'.
     */
    public int durationInitVal() {
        int result;
        if (_durationIsSet) {
            result = _durationInitVal;
        } else {
            result = getDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'duration'.
     */
    public boolean durationIsDirty() {
        return !valuesAreEqual(durationInitVal(), getDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'duration'.
     */
    public boolean durationIsSet() {
        return _durationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'value'.
     */
    public double valueInitVal() {
        double result;
        if (_valueIsSet) {
            result = _valueInitVal;
        } else {
            result = getValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'value'.
     */
    public boolean valueIsDirty() {
        return !valuesAreEqual(valueInitVal(), getValue());
    }

    /**
     * Returns true if the setter method was called for the property 'value'.
     */
    public boolean valueIsSet() {
        return _valueIsSet;
    }
	
    /**
     * Returns the initial value of the property 'calculation term'.
     */
    public int calculationTermInitVal() {
        int result;
        if (_calculationTermIsSet) {
            result = _calculationTermInitVal;
        } else {
            result = getCalculationTerm();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'calculation term'.
     */
    public boolean calculationTermIsDirty() {
        return !valuesAreEqual(calculationTermInitVal(), getCalculationTerm());
    }

    /**
     * Returns true if the setter method was called for the property 'calculation term'.
     */
    public boolean calculationTermIsSet() {
        return _calculationTermIsSet;
    }

}
