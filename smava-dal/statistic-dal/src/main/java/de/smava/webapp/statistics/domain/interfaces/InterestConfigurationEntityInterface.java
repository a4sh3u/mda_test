package de.smava.webapp.statistics.domain.interfaces;



import de.smava.webapp.statistics.domain.InterestConfiguration;


/**
 * The domain object that represents 'InterestConfigurations'.
 *
 * @author generator
 */
public interface InterestConfigurationEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(Double value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    Double getValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    InterestConfiguration asInterestConfiguration();
}
