package de.smava.webapp.statistics.domain.interfaces;



import de.smava.webapp.statistics.domain.PaymentratePerformances;


/**
 * The domain object that represents 'PaymentratePerformancess'.
 *
 * @author generator
 */
public interface PaymentratePerformancesEntityInterface {

    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    String getRating();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(int duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    int getDuration();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(double value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    double getValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    PaymentratePerformances asPaymentratePerformances();
}
