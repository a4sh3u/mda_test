package de.smava.webapp.statistics.domain.history;



import de.smava.webapp.statistics.domain.abstracts.AbstractInterestConfiguration;




/**
 * The domain object that has all history aggregation related fields for 'InterestConfigurations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class InterestConfigurationHistory extends AbstractInterestConfiguration {

    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;


	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
}
