package de.smava.webapp.statistics.domain.interfaces;



import de.smava.webapp.statistics.domain.BidAgentRun;

import java.util.Date;


/**
 * The domain object that represents 'BidAgentRuns'.
 *
 * @author generator
 */
public interface BidAgentRunEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    void setStartDate(Date startDate);

    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    Date getStartDate();
    /**
     * Setter for the property 'end date'.
     *
     * 
     *
     */
    void setEndDate(Date endDate);

    /**
     * Returns the property 'end date'.
     *
     * 
     *
     */
    Date getEndDate();
    /**
     * Setter for the property 'order id'.
     *
     * 
     *
     */
    void setOrderId(Long orderId);

    /**
     * Returns the property 'order id'.
     *
     * 
     *
     */
    Long getOrderId();
    /**
     * Setter for the property 'server name'.
     *
     * 
     *
     */
    void setServerName(String serverName);

    /**
     * Returns the property 'server name'.
     *
     * 
     *
     */
    String getServerName();
    /**
     * Helper method to get reference of this object as model type.
     */
    BidAgentRun asBidAgentRun();
}
