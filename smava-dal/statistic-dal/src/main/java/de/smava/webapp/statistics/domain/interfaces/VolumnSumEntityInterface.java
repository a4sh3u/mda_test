package de.smava.webapp.statistics.domain.interfaces;



import de.smava.webapp.statistics.domain.VolumnSum;

import java.util.Date;


/**
 * The domain object that represents 'VolumnSums'.
 *
 * @author generator
 */
public interface VolumnSumEntityInterface {

    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    String getRating();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'volumn'.
     *
     * 
     *
     */
    void setVolumn(Double volumn);

    /**
     * Returns the property 'volumn'.
     *
     * 
     *
     */
    Double getVolumn();
    /**
     * Setter for the property 'contract count'.
     *
     * 
     *
     */
    void setContractCount(Long contractCount);

    /**
     * Returns the property 'contract count'.
     *
     * 
     *
     */
    Long getContractCount();
    /**
     * Setter for the property 'creditproject count'.
     *
     * 
     *
     */
    void setCreditprojectCount(Long creditprojectCount);

    /**
     * Returns the property 'creditproject count'.
     *
     * 
     *
     */
    Long getCreditprojectCount();
    /**
     * Setter for the property 'avarage creditproject age'.
     *
     * 
     *
     */
    void setAvarageCreditprojectAge(Double avarageCreditprojectAge);

    /**
     * Returns the property 'avarage creditproject age'.
     *
     * 
     *
     */
    Double getAvarageCreditprojectAge();
    /**
     * Setter for the property 'lender count'.
     *
     * 
     *
     */
    void setLenderCount(Long lenderCount);

    /**
     * Returns the property 'lender count'.
     *
     * 
     *
     */
    Long getLenderCount();
    /**
     * Setter for the property 'borrower count'.
     *
     * 
     *
     */
    void setBorrowerCount(Long borrowerCount);

    /**
     * Returns the property 'borrower count'.
     *
     * 
     *
     */
    Long getBorrowerCount();
    /**
     * Setter for the property 'month'.
     *
     * 
     *
     */
    void setMonth(Date month);

    /**
     * Returns the property 'month'.
     *
     * 
     *
     */
    Date getMonth();
    /**
     * Helper method to get reference of this object as model type.
     */
    VolumnSum asVolumnSum();
}
