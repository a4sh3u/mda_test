//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(task execution)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.statistics.domain.DatabaseTrxLocks;
import de.smava.webapp.statistics.domain.TaskExecution;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'TaskExecutions'.
 *
 * @author generator
 */
public interface TaskExecutionDao extends BaseDao<TaskExecution> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the task execution identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    TaskExecution getTaskExecution(Long id);

    /**
     * Saves the task execution.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveTaskExecution(TaskExecution taskExecution);

    /**
     * Deletes an task execution, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the task execution
     */
    void deleteTaskExecution(Long id);

    /**
     * Retrieves all 'TaskExecution' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<TaskExecution> getTaskExecutionList();

    /**
     * Retrieves a page of 'TaskExecution' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<TaskExecution> getTaskExecutionList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'TaskExecution' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<TaskExecution> getTaskExecutionList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'TaskExecution' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<TaskExecution> getTaskExecutionList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'TaskExecution' instances.
     */
    long getTaskExecutionCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(task execution)}
    //
    // insert custom methods here
    
    Collection<TaskExecution> findTaskExecutionListForLastMinutes(int minutes);
    Collection<TaskExecution> findTaskExecutionListForLastMinutes(int minutes, String taskName);
    public Collection<TaskExecution> findUnfinishedTasksStartedBefore(int minutes, int maxMinutes);
    
    /**
     * Queries the database to find current locks. Check the file query_state if that is to old something severe happend
     * 
     * @return
     */
    public Collection<DatabaseTrxLocks> getDatabaseTrxLocksOverview ();
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
