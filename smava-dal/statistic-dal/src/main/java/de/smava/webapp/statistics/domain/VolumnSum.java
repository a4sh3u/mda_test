//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.statistics.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(volumn sum)}
import de.smava.webapp.statistics.domain.history.VolumnSumHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'VolumnSums'.
 *
 * @author generator
 */
public class VolumnSum extends VolumnSumHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(volumn sum)}
    public VolumnSum() {
    	super();
    }

    public VolumnSum(Integer duration, Double volumn, Long contractCount, Long creditprojectCount, Long lenderCount, Long borrowerCount) {
    	super();
    	setDuration(duration);
    	setVolumn(volumn);
    	setContractCount(contractCount);
    	setCreditprojectCount(creditprojectCount);
    	setLenderCount(lenderCount);
    	setBorrowerCount(borrowerCount);
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _rating;
        protected Integer _duration;
        protected String _type;
        protected Double _volumn;
        protected Long _contractCount;
        protected Long _creditprojectCount;
        protected Double _avarageCreditprojectAge;
        protected Long _lenderCount;
        protected Long _borrowerCount;
        protected Date _month;
        
                            /**
     * Setter for the property 'rating'.
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     */
    public String getRating() {
        return _rating;
    }
                                            
    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration) {
        _duration = duration;
    }
            
    /**
     * Returns the property 'duration'.
     */
    public Integer getDuration() {
        return _duration;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'volumn'.
     */
    public void setVolumn(Double volumn) {
        _volumn = volumn;
    }
            
    /**
     * Returns the property 'volumn'.
     */
    public Double getVolumn() {
        return _volumn;
    }
                                            
    /**
     * Setter for the property 'contract count'.
     */
    public void setContractCount(Long contractCount) {
        _contractCount = contractCount;
    }
            
    /**
     * Returns the property 'contract count'.
     */
    public Long getContractCount() {
        return _contractCount;
    }
                                            
    /**
     * Setter for the property 'creditproject count'.
     */
    public void setCreditprojectCount(Long creditprojectCount) {
        _creditprojectCount = creditprojectCount;
    }
            
    /**
     * Returns the property 'creditproject count'.
     */
    public Long getCreditprojectCount() {
        return _creditprojectCount;
    }
                                            
    /**
     * Setter for the property 'avarage creditproject age'.
     */
    public void setAvarageCreditprojectAge(Double avarageCreditprojectAge) {
        _avarageCreditprojectAge = avarageCreditprojectAge;
    }
            
    /**
     * Returns the property 'avarage creditproject age'.
     */
    public Double getAvarageCreditprojectAge() {
        return _avarageCreditprojectAge;
    }
                                            
    /**
     * Setter for the property 'lender count'.
     */
    public void setLenderCount(Long lenderCount) {
        _lenderCount = lenderCount;
    }
            
    /**
     * Returns the property 'lender count'.
     */
    public Long getLenderCount() {
        return _lenderCount;
    }
                                            
    /**
     * Setter for the property 'borrower count'.
     */
    public void setBorrowerCount(Long borrowerCount) {
        _borrowerCount = borrowerCount;
    }
            
    /**
     * Returns the property 'borrower count'.
     */
    public Long getBorrowerCount() {
        return _borrowerCount;
    }
                                    /**
     * Setter for the property 'month'.
     */
    public void setMonth(Date month) {
        if (!_monthIsSet) {
            _monthIsSet = true;
            _monthInitVal = getMonth();
        }
        registerChange("month", _monthInitVal, month);
        _month = month;
    }
                        
    /**
     * Returns the property 'month'.
     */
    public Date getMonth() {
        return _month;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(VolumnSum.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(VolumnSum.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public VolumnSum asVolumnSum() {
        return this;
    }
}
