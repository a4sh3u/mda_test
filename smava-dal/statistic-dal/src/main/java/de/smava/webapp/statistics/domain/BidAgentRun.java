//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.statistics.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bid agent run)}
import de.smava.webapp.statistics.domain.history.BidAgentRunHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BidAgentRuns'.
 *
 * @author generator
 */
public class BidAgentRun extends BidAgentRunHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(bid agent run)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _startDate;
        protected Date _endDate;
        protected Long _orderId;
        protected String _serverName;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'start date'.
     */
    public void setStartDate(Date startDate) {
        if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = getStartDate();
        }
        registerChange("start date", _startDateInitVal, startDate);
        _startDate = startDate;
    }
                        
    /**
     * Returns the property 'start date'.
     */
    public Date getStartDate() {
        return _startDate;
    }
                                    /**
     * Setter for the property 'end date'.
     */
    public void setEndDate(Date endDate) {
        if (!_endDateIsSet) {
            _endDateIsSet = true;
            _endDateInitVal = getEndDate();
        }
        registerChange("end date", _endDateInitVal, endDate);
        _endDate = endDate;
    }
                        
    /**
     * Returns the property 'end date'.
     */
    public Date getEndDate() {
        return _endDate;
    }
                                            
    /**
     * Setter for the property 'order id'.
     */
    public void setOrderId(Long orderId) {
        _orderId = orderId;
    }
            
    /**
     * Returns the property 'order id'.
     */
    public Long getOrderId() {
        return _orderId;
    }
                                    /**
     * Setter for the property 'server name'.
     */
    public void setServerName(String serverName) {
        if (!_serverNameIsSet) {
            _serverNameIsSet = true;
            _serverNameInitVal = getServerName();
        }
        registerChange("server name", _serverNameInitVal, serverName);
        _serverName = serverName;
    }
                        
    /**
     * Returns the property 'server name'.
     */
    public String getServerName() {
        return _serverName;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BidAgentRun.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _serverName=").append(_serverName);
            builder.append("\n}");
        } else {
            builder.append(BidAgentRun.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BidAgentRun asBidAgentRun() {
        return this;
    }
}
