//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.statistics.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(interest configuration)}
import de.smava.webapp.statistics.domain.history.InterestConfigurationHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'InterestConfigurations'.
 *
 * @author generator
 */
public class InterestConfiguration extends InterestConfigurationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(interest configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _type;
        protected Double _value;
        
                            /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'value'.
     */
    public void setValue(Double value) {
        _value = value;
    }
            
    /**
     * Returns the property 'value'.
     */
    public Double getValue() {
        return _value;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(InterestConfiguration.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(InterestConfiguration.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public InterestConfiguration asInterestConfiguration() {
        return this;
    }
}
