//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(paymentrate performances)}
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.statistics.dao.PaymentratePerformancesDao;
import de.smava.webapp.statistics.domain.PaymentratePerformances;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'PaymentratePerformancess'.
 *
 * @author generator
 */
@Repository(value = "paymentratePerformancesDao")
public class JdoPaymentratePerformancesDao extends JdoBaseDao implements PaymentratePerformancesDao {

    private static final Logger LOGGER = Logger.getLogger(JdoPaymentratePerformancesDao.class);

    private static final String CLASS_NAME = "PaymentratePerformances";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(paymentrate performances)}

	private static final String TRUNCATE_SQL = "DELETE FROM stats_paymentrate_performances;";
	private static final String FINAL_PAYMENT_RATE_FROM_SQL
		= "FROM insurancepool_payout_run ipr INNER JOIN booking_group bg ON bg.id = ipr.booking_group_id \n"
		+ "WHERE bg.market = :market \n"
		+ "AND bg.date = \n"
		+ "		(SELECT bg.date - INTERVAL '3 month' FROM booking_group bg \n"
		+ "		 INNER JOIN insurancepool_payout_run ipr ON bg.id = ipr.booking_group_id \n"
		+ "		 WHERE bg.market = :market \n"
		+ "		 ORDER BY bg.date DESC LIMIT 1) \n"
		+ "ORDER BY ipr.creation_date DESC LIMIT 1;";

	private static final String FINAL_PAYMENT_RATE_SELECT_SQL
		= "SELECT ipr.payment_rate \n";

	private static final String FINAL_PAYMENT_RATE_DATE_SELECT_SQL
		= "SELECT ipr.creation_date \n";

	private static final String AVERAGE_PAYMENTRATES_SQL
		= "select tmp.market, avg(tmp.c2) "
		+ "from ( "
		+ "		select bg.market as market, "
		+ "		(select ipr_in.payment_rate from booking_group bg_in "
		+ "			inner join insurancepool_payout_run ipr_in on bg_in.id = ipr_in.booking_group_id "
		+ "			where bg_in.market = bg.market and bg_in.date = bg.date  "
		+ "			order by ipr_in.creation_date desc "
		+ "			limit 1 "
		+ "			) as c2 "
		+ "			from booking_group bg "
		+ "			inner join insurancepool_payout_run ipr on bg.id = ipr.booking_group_id "
		+ "			group by bg.date, bg.market "
		+ "			order by  "
		+ "			bg.date desc "
		+ "			) as tmp "
		+ "group by tmp.market "
		+ "order by tmp.market;";

	private static final String DURATION_BASED_ROI_DATA 
		= "SELECT id FROM stats_paymentrate_performances "
		+ "WHERE duration = :duration "
		+ "AND type IN (:types);";

    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the paymentrate performances identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public PaymentratePerformances load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        PaymentratePerformances result = getEntity(PaymentratePerformances.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public PaymentratePerformances getPaymentratePerformances(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	PaymentratePerformances entity = findUniqueEntity(PaymentratePerformances.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the paymentrate performances.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(PaymentratePerformances paymentratePerformances) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("savePaymentratePerformances: " + paymentratePerformances);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(paymentrate performances)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(paymentratePerformances);
    }

    /**
     * @deprecated Use {@link #save(PaymentratePerformances) instead}
     */
    public Long savePaymentratePerformances(PaymentratePerformances paymentratePerformances) {
        return save(paymentratePerformances);
    }

    /**
     * Deletes an paymentrate performances, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the paymentrate performances
     */
    public void deletePaymentratePerformances(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deletePaymentratePerformances: " + id);
        }
        deleteEntity(PaymentratePerformances.class, id);
    }

    /**
     * Retrieves all 'PaymentratePerformances' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<PaymentratePerformances> getPaymentratePerformancesList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<PaymentratePerformances> result = getEntities(PaymentratePerformances.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'PaymentratePerformances' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<PaymentratePerformances> getPaymentratePerformancesList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<PaymentratePerformances> result = getEntities(PaymentratePerformances.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'PaymentratePerformances' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<PaymentratePerformances> getPaymentratePerformancesList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<PaymentratePerformances> result = getEntities(PaymentratePerformances.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'PaymentratePerformances' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<PaymentratePerformances> getPaymentratePerformancesList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<PaymentratePerformances> result = getEntities(PaymentratePerformances.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'PaymentratePerformances' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<PaymentratePerformances> findPaymentratePerformancesList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<PaymentratePerformances> result = findEntities(PaymentratePerformances.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'PaymentratePerformances' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<PaymentratePerformances> findPaymentratePerformancesList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<PaymentratePerformances> result = findEntities(PaymentratePerformances.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'PaymentratePerformances' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<PaymentratePerformances> findPaymentratePerformancesList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<PaymentratePerformances> result = findEntities(PaymentratePerformances.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'PaymentratePerformances' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<PaymentratePerformances> findPaymentratePerformancesList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<PaymentratePerformances> result = findEntities(PaymentratePerformances.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'PaymentratePerformances' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<PaymentratePerformances> findPaymentratePerformancesList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<PaymentratePerformances> result = findEntities(PaymentratePerformances.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'PaymentratePerformances' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<PaymentratePerformances> findPaymentratePerformancesList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<PaymentratePerformances> result = findEntities(PaymentratePerformances.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'PaymentratePerformances' instances.
     */
    public long getPaymentratePerformancesCount() {
        long result = getEntityCount(PaymentratePerformances.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'PaymentratePerformances' instances which match the given whereClause.
     */
    public long getPaymentratePerformancesCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(PaymentratePerformances.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'PaymentratePerformances' instances which match the given whereClause together with params specified in object array.
     */
    public long getPaymentratePerformancesCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(PaymentratePerformances.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(paymentrate performances)}

    @Override
    public void truncateTable() {
//    	in case that the table does not exist we 'tell' the OR-mapper to create it.
    	long numberOfRows = getPaymentratePerformancesCount();

//		truncate the table
    	if (numberOfRows > 0) {
	    	final Query query = getPersistenceManager().newQuery(Query.SQL, TRUNCATE_SQL);
	    	query.execute();
    	}
    }

	@Override
	public Date findLastFinalPaymentRateDate(String market) {
		Query query = getPersistenceManager().newQuery(Query.SQL, FINAL_PAYMENT_RATE_DATE_SELECT_SQL + FINAL_PAYMENT_RATE_FROM_SQL);
    	query.setUnique(true);
    	query.setResultClass(Date.class);

    	Map<String, String> parameters = new HashMap<String, String>();
    	parameters.put("market", market);
//    	for testing reasons
    	Object resultObject = query.executeWithMap(parameters);
    	Date result = (Date) resultObject;

		return result;
	}

	@Override
	public double findLastFinalPaymentRate(String market) {
		Query query = getPersistenceManager().newQuery(Query.SQL, FINAL_PAYMENT_RATE_SELECT_SQL + FINAL_PAYMENT_RATE_FROM_SQL);
    	query.setUnique(true);
    	query.setResultClass(Double.class);

    	Map<String, String> parameters = new HashMap<String, String>();
    	parameters.put("market", market);

    	Object resultObject = query.executeWithMap(parameters);
    	double result = (Double) (resultObject != null ? resultObject : 0.0);

		return result;
	}

	@Override
	public Collection<Object[]> findAvaragePaymentRates() {
		final Query query = getPersistenceManager().newQuery(Query.SQL, AVERAGE_PAYMENTRATES_SQL);
		return (Collection<Object[]>) query.execute();
	}

	@Override
	public Collection<PaymentratePerformances> getPaymentratePerformancesData(Collection<String> types,
			int performanceStatsDuration) {
		final Map<String, Object> params = new LinkedHashMap<String, Object>(2);
		params.put("duration", performanceStatsDuration);
		params.put("types", types);

		final String sql = replaceCollectionTypes(params, DURATION_BASED_ROI_DATA);
		final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
		query.setClass(PaymentratePerformances.class);

		return (Collection<PaymentratePerformances>) query.executeWithMap(params);
	}

    // !!!!!!!! End of insert code section !!!!!!!!
}
