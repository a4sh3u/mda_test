package de.smava.webapp.statistics.domain.interfaces;



import de.smava.webapp.statistics.domain.TaskExecution;

import java.util.Date;


/**
 * The domain object that represents 'TaskExecutions'.
 *
 * @author generator
 */
public interface TaskExecutionEntityInterface {

    /**
     * Setter for the property 'start date'.
     *
     * 
     *
     */
    void setStartDate(Date startDate);

    /**
     * Returns the property 'start date'.
     *
     * 
     *
     */
    Date getStartDate();
    /**
     * Setter for the property 'end date'.
     *
     * 
     *
     */
    void setEndDate(Date endDate);

    /**
     * Returns the property 'end date'.
     *
     * 
     *
     */
    Date getEndDate();
    /**
     * Setter for the property 'task name'.
     *
     * 
     *
     */
    void setTaskName(String taskName);

    /**
     * Returns the property 'task name'.
     *
     * 
     *
     */
    String getTaskName();
    /**
     * Setter for the property 'server name'.
     *
     * 
     *
     */
    void setServerName(String serverName);

    /**
     * Returns the property 'server name'.
     *
     * 
     *
     */
    String getServerName();
    /**
     * Helper method to get reference of this object as model type.
     */
    TaskExecution asTaskExecution();
}
