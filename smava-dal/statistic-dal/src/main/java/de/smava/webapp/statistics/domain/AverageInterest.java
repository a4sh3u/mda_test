//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.statistics.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(average interest)}
import de.smava.webapp.statistics.domain.history.AverageInterestHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AverageInterests'.
 *
 * @author generator
 */
public class AverageInterest extends AverageInterestHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(average interest)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _rating;
        protected int _duration;
        protected String _type;
        protected double _value;
        protected int _calculationTerm;
        
                            /**
     * Setter for the property 'rating'.
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     */
    public String getRating() {
        return _rating;
    }
                                    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(int duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = getDuration();
        }
        registerChange("duration", _durationInitVal, duration);
        _duration = duration;
    }
                        
    /**
     * Returns the property 'duration'.
     */
    public int getDuration() {
        return _duration;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'value'.
     */
    public void setValue(double value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     */
    public double getValue() {
        return _value;
    }
                                    /**
     * Setter for the property 'calculation term'.
     */
    public void setCalculationTerm(int calculationTerm) {
        if (!_calculationTermIsSet) {
            _calculationTermIsSet = true;
            _calculationTermInitVal = getCalculationTerm();
        }
        registerChange("calculation term", _calculationTermInitVal, calculationTerm);
        _calculationTerm = calculationTerm;
    }
                        
    /**
     * Returns the property 'calculation term'.
     */
    public int getCalculationTerm() {
        return _calculationTerm;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AverageInterest.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _duration=").append(_duration);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _value=").append(_value);
            builder.append("\n    _calculationTerm=").append(_calculationTerm);
            builder.append("\n}");
        } else {
            builder.append(AverageInterest.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AverageInterest asAverageInterest() {
        return this;
    }
}
