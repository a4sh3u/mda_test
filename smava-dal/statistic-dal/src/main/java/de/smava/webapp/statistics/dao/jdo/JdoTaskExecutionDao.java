//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(task execution)}
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.statistics.dao.TaskExecutionDao;
import de.smava.webapp.statistics.domain.DatabaseTrxLocks;
import de.smava.webapp.statistics.domain.TaskExecution;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'TaskExecutions'.
 *
 * @author generator
 */
@Repository(value = "taskExecutionDao")
public class JdoTaskExecutionDao extends JdoBaseDao implements TaskExecutionDao {

    private static final Logger LOGGER = Logger.getLogger(JdoTaskExecutionDao.class);

    private static final String CLASS_NAME = "TaskExecution";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(task execution)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the task execution identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public TaskExecution load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        TaskExecution result = getEntity(TaskExecution.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public TaskExecution getTaskExecution(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	TaskExecution entity = findUniqueEntity(TaskExecution.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the task execution.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(TaskExecution taskExecution) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveTaskExecution: " + taskExecution);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(task execution)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(taskExecution);
    }

    /**
     * @deprecated Use {@link #save(TaskExecution) instead}
     */
    public Long saveTaskExecution(TaskExecution taskExecution) {
        return save(taskExecution);
    }

    /**
     * Deletes an task execution, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the task execution
     */
    public void deleteTaskExecution(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteTaskExecution: " + id);
        }
        deleteEntity(TaskExecution.class, id);
    }

    /**
     * Retrieves all 'TaskExecution' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<TaskExecution> getTaskExecutionList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<TaskExecution> result = getEntities(TaskExecution.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'TaskExecution' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<TaskExecution> getTaskExecutionList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<TaskExecution> result = getEntities(TaskExecution.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'TaskExecution' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<TaskExecution> getTaskExecutionList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<TaskExecution> result = getEntities(TaskExecution.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'TaskExecution' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<TaskExecution> getTaskExecutionList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<TaskExecution> result = getEntities(TaskExecution.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'TaskExecution' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<TaskExecution> findTaskExecutionList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<TaskExecution> result = findEntities(TaskExecution.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'TaskExecution' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<TaskExecution> findTaskExecutionList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<TaskExecution> result = findEntities(TaskExecution.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'TaskExecution' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<TaskExecution> findTaskExecutionList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<TaskExecution> result = findEntities(TaskExecution.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'TaskExecution' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<TaskExecution> findTaskExecutionList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<TaskExecution> result = findEntities(TaskExecution.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'TaskExecution' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<TaskExecution> findTaskExecutionList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<TaskExecution> result = findEntities(TaskExecution.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'TaskExecution' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<TaskExecution> findTaskExecutionList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<TaskExecution> result = findEntities(TaskExecution.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'TaskExecution' instances.
     */
    public long getTaskExecutionCount() {
        long result = getEntityCount(TaskExecution.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'TaskExecution' instances which match the given whereClause.
     */
    public long getTaskExecutionCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(TaskExecution.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'TaskExecution' instances which match the given whereClause together with params specified in object array.
     */
    public long getTaskExecutionCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(TaskExecution.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(task execution)}
    //
    // insert custom methods here
    
    public Collection<TaskExecution> findTaskExecutionListForLastMinutes(int minutes){
    	Collection<Object> params = new ArrayList<Object>();
    	
    	Calendar c = CurrentDate.getCalendar();
    	c.add(Calendar.MINUTE, minutes * -1);
    	Date start = c.getTime();
    	params.add( start);
    	
        OqlTerm oqlTerm = OqlTerm.newTerm();
       
        oqlTerm.greaterThanEquals("_startDate", start);
                    
        
        Collection<TaskExecution> result = findEntities(TaskExecution.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }
    
    public Collection<TaskExecution> findTaskExecutionListForLastMinutes(int minutes, String taskName){
    	Collection<Object> params = new ArrayList<Object>();
    	
    	Calendar c = CurrentDate.getCalendar();
    	c.add(Calendar.MINUTE, minutes * -1);
    	Date start = c.getTime();
    	params.add( start);
        OqlTerm oqlTerm = OqlTerm.newTerm();
       
        oqlTerm.and( oqlTerm.greaterThanEquals("_startDate", start), oqlTerm.equals("_taskName", taskName));
                    
        
        Collection<TaskExecution> result = findEntities(TaskExecution.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }
    
    public Collection<TaskExecution> findUnfinishedTasksStartedBefore(int minutes, int maxMinutes){
    	Collection<Object> params = new ArrayList<Object>();
    	
    	Calendar c = CurrentDate.getCalendar();
    	c.add(Calendar.MINUTE, maxMinutes * -1);
    	Date start = c.getTime();
    	params.add( start);
    	
    	c = CurrentDate.getCalendar();
    	c.add(Calendar.MINUTE, minutes * -1);
    	Date startLatest = c.getTime();
    	params.add( startLatest);
    	
        OqlTerm oqlTerm = OqlTerm.newTerm();
       
        oqlTerm.and( oqlTerm.greaterThanEquals("_startDate", start), oqlTerm.lessThanEquals("_startDate", startLatest), oqlTerm.isNull("_endDate"));
                    
        
        Collection<TaskExecution> result = findEntities(TaskExecution.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }
    
    
    @Override
    public Collection<DatabaseTrxLocks> getDatabaseTrxLocksOverview () {
        String sql = "SELECT" +
                "  a.datname," +
                "  l.transactionid," +
                "  l.mode," +
                "  l.granted," +
                "  a.usename," +
                "  a.query," +
                "  a.query_start," +
                "  a.pid" +
                " FROM pg_stat_activity a" +
                "  JOIN pg_locks l ON l.pid = a.pid" +
                "  JOIN pg_class C ON C.oid = l.relation" +
                " GROUP BY a.pid, l.transactionid, a.datname, l.mode, l.granted, a.usename, a.query, a.query_start " +
                "ORDER BY a.query_start;";
    	Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        
        query.setResultClass(DatabaseTrxLocks.class);
        
        Collection<DatabaseTrxLocks> queryResult = (Collection<DatabaseTrxLocks>) query.execute();
        
      
        return queryResult;
        
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
