package de.smava.webapp.statistics.domain.history;



import de.smava.webapp.statistics.domain.abstracts.AbstractTaskExecution;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'TaskExecutions'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class TaskExecutionHistory extends AbstractTaskExecution {

    protected transient Date _startDateInitVal;
    protected transient boolean _startDateIsSet;
    protected transient Date _endDateInitVal;
    protected transient boolean _endDateIsSet;
    protected transient String _taskNameInitVal;
    protected transient boolean _taskNameIsSet;
    protected transient String _serverNameInitVal;
    protected transient boolean _serverNameIsSet;


	
    /**
     * Returns the initial value of the property 'start date'.
     */
    public Date startDateInitVal() {
        Date result;
        if (_startDateIsSet) {
            result = _startDateInitVal;
        } else {
            result = getStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start date'.
     */
    public boolean startDateIsDirty() {
        return !valuesAreEqual(startDateInitVal(), getStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'start date'.
     */
    public boolean startDateIsSet() {
        return _startDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end date'.
     */
    public Date endDateInitVal() {
        Date result;
        if (_endDateIsSet) {
            result = _endDateInitVal;
        } else {
            result = getEndDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end date'.
     */
    public boolean endDateIsDirty() {
        return !valuesAreEqual(endDateInitVal(), getEndDate());
    }

    /**
     * Returns true if the setter method was called for the property 'end date'.
     */
    public boolean endDateIsSet() {
        return _endDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'task name'.
     */
    public String taskNameInitVal() {
        String result;
        if (_taskNameIsSet) {
            result = _taskNameInitVal;
        } else {
            result = getTaskName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'task name'.
     */
    public boolean taskNameIsDirty() {
        return !valuesAreEqual(taskNameInitVal(), getTaskName());
    }

    /**
     * Returns true if the setter method was called for the property 'task name'.
     */
    public boolean taskNameIsSet() {
        return _taskNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'server name'.
     */
    public String serverNameInitVal() {
        String result;
        if (_serverNameIsSet) {
            result = _serverNameInitVal;
        } else {
            result = getServerName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'server name'.
     */
    public boolean serverNameIsDirty() {
        return !valuesAreEqual(serverNameInitVal(), getServerName());
    }

    /**
     * Returns true if the setter method was called for the property 'server name'.
     */
    public boolean serverNameIsSet() {
        return _serverNameIsSet;
    }

}
