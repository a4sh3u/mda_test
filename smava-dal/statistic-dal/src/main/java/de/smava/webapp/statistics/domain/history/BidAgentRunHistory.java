package de.smava.webapp.statistics.domain.history;



import de.smava.webapp.statistics.domain.abstracts.AbstractBidAgentRun;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BidAgentRuns'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BidAgentRunHistory extends AbstractBidAgentRun {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _startDateInitVal;
    protected transient boolean _startDateIsSet;
    protected transient Date _endDateInitVal;
    protected transient boolean _endDateIsSet;
    protected transient String _serverNameInitVal;
    protected transient boolean _serverNameIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'start date'.
     */
    public Date startDateInitVal() {
        Date result;
        if (_startDateIsSet) {
            result = _startDateInitVal;
        } else {
            result = getStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start date'.
     */
    public boolean startDateIsDirty() {
        return !valuesAreEqual(startDateInitVal(), getStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'start date'.
     */
    public boolean startDateIsSet() {
        return _startDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end date'.
     */
    public Date endDateInitVal() {
        Date result;
        if (_endDateIsSet) {
            result = _endDateInitVal;
        } else {
            result = getEndDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end date'.
     */
    public boolean endDateIsDirty() {
        return !valuesAreEqual(endDateInitVal(), getEndDate());
    }

    /**
     * Returns true if the setter method was called for the property 'end date'.
     */
    public boolean endDateIsSet() {
        return _endDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'server name'.
     */
    public String serverNameInitVal() {
        String result;
        if (_serverNameIsSet) {
            result = _serverNameInitVal;
        } else {
            result = getServerName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'server name'.
     */
    public boolean serverNameIsDirty() {
        return !valuesAreEqual(serverNameInitVal(), getServerName());
    }

    /**
     * Returns true if the setter method was called for the property 'server name'.
     */
    public boolean serverNameIsSet() {
        return _serverNameIsSet;
    }

}
