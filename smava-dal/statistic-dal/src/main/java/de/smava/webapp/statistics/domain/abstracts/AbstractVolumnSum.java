//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.statistics.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(volumn sum)}

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.statistics.domain.interfaces.VolumnSumEntityInterface;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.Set;

                                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'VolumnSums'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractVolumnSum
    extends KreditPrivatEntity implements VolumnSumEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractVolumnSum.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(volumn sum)}

    private static final long serialVersionUID = 1L;

    public static final String TYPE_VOLUMN_ALL 				= "VOLUMN_ALL";

    public static final String TYPE_VOLUMN_RUNNING	 		= "VOLUMN_RUNNING";
    public static final String TYPE_VOLUMN_RUNNING_INTIME 	= "VOLUMN_RUNNING_INTIME";
    public static final String TYPE_VOLUMN_RUNNING_LATE 	= "VOLUMN_RUNNING_LATE";

    public static final String TYPE_VOLUMN_ENDED 			= "VOLUMN_ENDED";
    public static final String TYPE_VOLUMN_ENDED_PRIOR		= "VOLUMN_ENDED_PRIOR";
    public static final String TYPE_VOLUMN_ENDED_NORMAL 	= "VOLUMN_ENDED_NORMAL";

    public static final String TYPE_VOLUMN_DEFAULTED		= "VOLUMN_DEFAULTED";
    public static final String TYPE_VOLUMN_DEFAULTED_SUM	= "VOLUMN_DEFAULTED_SUM";
    public static final String TYPE_VOLUMN_DEFAULTED_RATE	= "VOLUMN_DEFAULTED_RATE";

    public static final String TYPE_POOL_ALL 				= "POOL_ALL";
    public static final String TYPE_POOL_INTIME 			= "POOL_INTIME";
    public static final String TYPE_POOL_LATE 				= "POOL_LATE";
    public static final String TYPE_POOL_DEFAULTED			= "POOL_DEFAULTED";

    @SuppressWarnings("unchecked")
    public static final Set<String> TYPES = Collections.unmodifiableSet(ConstCollector.getAll("TYPE_"));

    @SuppressWarnings("unchecked")
    public static final Set<String> VOLUMN_TYPES = Collections.unmodifiableSet(ConstCollector.getAll("TYPE_VOLUMN_"));

    @SuppressWarnings("unchecked")
    public static final Set<String> POOL_TYPES = Collections.unmodifiableSet(ConstCollector.getAll("TYPE_POOL"));

    // !!!!!!!! End of insert code section !!!!!!!!
}

