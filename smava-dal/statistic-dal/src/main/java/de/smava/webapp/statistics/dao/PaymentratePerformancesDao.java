//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(paymentrate performances)}
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.statistics.domain.PaymentratePerformances;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'PaymentratePerformancess'.
 *
 * @author generator
 */
public interface PaymentratePerformancesDao extends BaseDao<PaymentratePerformances> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the paymentrate performances identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    PaymentratePerformances getPaymentratePerformances(Long id);

    /**
     * Saves the paymentrate performances.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long savePaymentratePerformances(PaymentratePerformances paymentratePerformances);

    /**
     * Deletes an paymentrate performances, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the paymentrate performances
     */
    void deletePaymentratePerformances(Long id);

    /**
     * Retrieves all 'PaymentratePerformances' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<PaymentratePerformances> getPaymentratePerformancesList();

    /**
     * Retrieves a page of 'PaymentratePerformances' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<PaymentratePerformances> getPaymentratePerformancesList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'PaymentratePerformances' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<PaymentratePerformances> getPaymentratePerformancesList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'PaymentratePerformances' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<PaymentratePerformances> getPaymentratePerformancesList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'PaymentratePerformances' instances.
     */
    long getPaymentratePerformancesCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(paymentrate performances)}
    void truncateTable();
    double findLastFinalPaymentRate(String market);
    Collection<Object[]> findAvaragePaymentRates();

    Collection<PaymentratePerformances> getPaymentratePerformancesData(Collection<String> types,
                                                                       int performanceStatsDuration);

    Date findLastFinalPaymentRateDate(String market);
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
