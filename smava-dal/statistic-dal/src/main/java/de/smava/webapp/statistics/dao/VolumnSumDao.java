//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.statistics.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(volumn sum)}

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.statistics.domain.VolumnSum;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'VolumnSums'.
 *
 * @author generator
 */
public interface VolumnSumDao extends BaseDao<VolumnSum> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the volumn sum identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    VolumnSum getVolumnSum(Long id);

    /**
     * Saves the volumn sum.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveVolumnSum(VolumnSum volumnSum);

    /**
     * Deletes an volumn sum, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the volumn sum
     */
    void deleteVolumnSum(Long id);

    /**
     * Retrieves all 'VolumnSum' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<VolumnSum> getVolumnSumList();

    /**
     * Retrieves a page of 'VolumnSum' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<VolumnSum> getVolumnSumList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'VolumnSum' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<VolumnSum> getVolumnSumList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'VolumnSum' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<VolumnSum> getVolumnSumList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'VolumnSum' instances.
     */
    long getVolumnSumCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(volumn sum)}
    //
    // insert custom methods here
    
    Collection<Object[]> getVolumnsFromData(String type, Set<String> contractStates, Date forMonth);
    
    Date getMaxMonthForType(String type);
    
    void deleteMonth(Date monthToDelete);
    
    List<Object[]> getDeafultData(final Date now, final Set<Long> interimBankAccountIds, Set<String> loanSumContractStates);

	Collection<VolumnSum> getActualPoolSumsForDuration(Collection<String> types,
			int performanceStatsDuration, Date now);
    
    List<VolumnSum> findVolumnsForDuration(final Date currentMonth, final Set<String> types, final Integer duration);
    
    // !!!!!!!! End of insert code section !!!!!!!!
}
