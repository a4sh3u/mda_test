package de.smava.webapp.statistics.domain;

import java.util.Date;
/**
 * 
 * @author dkeller
 *
 */
public class DatabaseTrxLocks {
	private String _datname;
	private Long _transactionid;
	private String _mode;
	private Boolean _granted;
	private String _usename;
	private String query;
	private Date _query_start;
	private Long pid;
	public String getDatname() {
		return _datname;
	}
	public void setDatname(String datname) {
		_datname = datname;
	}
	public Long getTransactionid() {
		return _transactionid;
	}
	public void setTransactionid(Long transactionid) {
		_transactionid = transactionid;
	}
	public String getMode() {
		return _mode;
	}
	public void setMode(String mode) {
		_mode = mode;
	}
	public Boolean getGranted() {
		return _granted;
	}
	public void setGranted(Boolean granted) {
		_granted = granted;
	}
	public String getUsename() {
		return _usename;
	}
	public void setUsename(String usename) {
		_usename = usename;
	}
	public Date getQuery_start() {
		return _query_start;
	}
	public void setQuery_start(Date query_start) {
		_query_start = query_start;
	}

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
}
