//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.statistics.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(interest configuration)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.statistics.domain.interfaces.InterestConfigurationEntityInterface;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'InterestConfigurations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractInterestConfiguration
    extends KreditPrivatEntity implements InterestConfigurationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractInterestConfiguration.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(interest configuration)}
    public final static String DEFAULT_INTEREST_SLIDER = "DEFAULT_INTEREST_SLIDER";
    public final static String ADJUSTED_INTEREST_SLIDER ="ADJUSTED_INTEREST_SLIDER";
    // !!!!!!!! End of insert code section !!!!!!!!
}

