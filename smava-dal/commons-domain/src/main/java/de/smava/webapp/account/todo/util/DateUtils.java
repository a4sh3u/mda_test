package de.smava.webapp.account.todo.util;

import de.smava.webapp.commons.currentdate.CurrentDate;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Utilities for dates.
 * <p/>
 * Date: 18.09.2006
 *
 * @author ingmar.decker
 */
public final class DateUtils {

    public static final DateFormat DEFAULT_FORMAT = new SimpleDateFormat("hh:mm dd.MM.yyyy");

    private DateUtils() {
    }

    public static String format(XMLGregorianCalendar cal) {
        if (cal == null) {
            return null;
        }
        return format(cal.toGregorianCalendar());
    }

    public static String format(Calendar cal) {
        if (cal == null) {
            return null;
        }
        return DEFAULT_FORMAT.format(cal.getTime());
    }

    /**
     * Add a month to given date.
     * Leaves hours etc. unchanged.
     */
    public static void setToEndOfNextMonth(Calendar calendar) {
        // (Problem: 30.4 + 1 month = 30.5.)
        calendar.add(Calendar.MONTH, 2);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
    }
    
    /**
     * Get the last day of month of given date (hours, minutes etc = 0).
     */
    public static Date getDateEndOfPreviousMonth(Date date) {
        Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);

        return clean(calendar.getTime());
    }

    /**
     * Get the last day of month of given date (hours, minutes etc = 0).
     */
    public static Date getDateEndOfMonth(Date date) {
        Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);

        return clean(calendar.getTime());
    }

    /**
     * Get the first day of month of given date (hours, minutes etc = 0).
     */
    public static Date getDateStartOfMonth(Date date) {
        Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return clean(calendar.getTime());
    }
    
    public static Date getDateStartOfNextMonth(Date date) {
    	Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, 1);
        return clean(calendar.getTime());
    }

    /**
     * Get the first day of month of given date (hours, minutes etc = 0).
     */
    public static Date getDateStartOfWeek(Date date) {
        Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return clean(calendar.getTime());
    }

    /**
     * Get the last day of next month of given date (hours, minutes etc = 0).
     */
    public static Date getDateEndOfNextMonth(Date date) {
        Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        setToEndOfNextMonth(calendar);

        return clean(calendar.getTime());
    }

    /**
     * Create date.
     */
    public static Date createDate(int year, int month, int day) {
        Calendar cal = CurrentDate.getCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);

        return clean(cal.getTime());
    }

    public static Date addMinutes(Date date, int minutes) {
    	Calendar cal = CurrentDate.getCalendar();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        return cal.getTime();
    }
    
    /**
     * Get a new date where days days are added.
     */
    public static Date addDays(Date date, int days) {
        Calendar cal = CurrentDate.getCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return cal.getTime();
    }
    
    /**
     * Get a new date where month are added.
     */
    public static Date addMonth(Date date, int month) {
        Calendar cal = CurrentDate.getCalendar();
        cal.setTime(date);
        cal.add(Calendar.MONTH, month);
        return cal.getTime();
    }
    
    public static Calendar addMonth(Calendar date, int month) {
        Calendar cal = CurrentDate.getCalendar();
        cal.setTime(date.getTime());
        cal.add(Calendar.MONTH, month);
        return cal;
    }
    
    /**
     * Get a new date where year are added.
     */
    public static Date addYear(Date date, int year) {
        Calendar cal = CurrentDate.getCalendar();
        cal.setTime(date);
        cal.add(Calendar.YEAR, year);
        return cal.getTime();
    }
    

    /**
     * Cleans date to same day but midnight (0:00:00).
     */
    public static Date clean(Date date) {
        return org.apache.commons.lang.time.DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
    }
    
    /**
     * Returns the first weekday beginning with the third day of next month.
     * If the third is sunday, monday the fourth will be returned.
     */
    public static Date getThirdOfNextMonth(Date date) {
    	Calendar cal = CurrentDate.getCalendar();
        cal.setTime(date);
        cal.add(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 3);
        
        while (cal.get(Calendar.DAY_OF_WEEK) == cal.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == cal.SATURDAY) {
        	cal.add(Calendar.DATE, 1);
        }
        
        return cal.getTime();
    }

    /**
     * Gets the number of years in the time frame between the given from date and now + given months.
     * @param from date from
     * @param months date to (now + number of months)
     * @return number of years between
     */
    public static int getYearsBetween(Date from, int months) {
        Calendar end = CurrentDate.getCalendar();
        end.add(Calendar.MONTH, months);
        Calendar start = Calendar.getInstance();
        start.setTime(from);
        return end.get(Calendar.YEAR) - start.get(Calendar.YEAR);
    }

    /**
     * Checks whether the given Date is not older than x month,
     * @param date date to check
     * @param month the x in x month
     * @return true if not older
     */
    public static boolean isOlderThan(Date date, int month) {
        Calendar now = CurrentDate.getCalendar();
        now.add(Calendar.MONTH, -month);
        return date.before(now.getTime());
    }

    
    public static int getAllowedFreelancerMonths() {
    	return getAllowedFreelancerMonths(CurrentDate.getCalendar());
    }
    
    public static int getAllowedFreelancerMonths(final Calendar now) {
    	final int currentMonth = now.get(Calendar.MONTH);
    	final int result = 10 + (currentMonth % 3); 
    	return result;
    }

    public static int getYearAsInt(final Date date) {
        Calendar calendar = CurrentDate.getCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

	public static String getYear(final Date date) {
		int yearAsInt = getYearAsInt(date);
		final String result = String.valueOf(yearAsInt);
		return result;
	}

	public static String getDayOfMonth(Date date) {
		Calendar calendar = CurrentDate.getCalendar();
		calendar.setTime(date);
		final String result = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));

		return result;
	}
	
	/**
	 * get the current date + x month
	 */
	public static Date getInMonthFromNow(int months) {
		Calendar calendar = CurrentDate.getCalendar();
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + months);
		return calendar.getTime();
	}
	
	public static boolean isBeforeConfigDate(Date toCheck, Date configDate) {
		return configDate != null && (toCheck == null || toCheck.before(configDate));
	}

	public static boolean equalsIgnoreTime(Date date1, Date date2){
        return getZeroTimeDate(date1).compareTo(getZeroTimeDate(date2)) == 0;
    }

    public static Date getZeroTimeDate(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Date getTheEndOfTheDay(Date date) {
        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }


}