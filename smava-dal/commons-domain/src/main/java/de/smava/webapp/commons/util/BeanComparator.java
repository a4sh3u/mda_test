package de.smava.webapp.commons.util;

import org.springframework.beans.BeanWrapperImpl;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Locale;

/**
 * Comparator for sorting list of entity beans using an "order by" like expression,
 * see {@link BeanComparator#BeanComparator(String)}.      
 * Beans are sorted by bean property defined in constructor - and if those values
 * are equal the id propety is compared, or (if set) the equalsBeanComparator
 * is applied.
 * <p/>
 * Date: 12.09.2006
 *
 * @author ingmar.decker
 */
public class BeanComparator implements Comparator<Object>, Serializable {

    private static final long serialVersionUID = 1526197460686737337L;

    private int _modifier;
    private String _field;
    private BeanComparator _equalsCaseComparator = null;

    /**
     * @param orderByClause &lt;bean property name&gt; [asc|desc], e.g. "id desc" or "_id desc".
     */
    public BeanComparator(String orderByClause) {
        String obc = orderByClause.replaceAll("_", "");
        _modifier = 1;
        _field = "id";
        int split = obc.indexOf(' ');
        if (split > 0) {
            _field = obc.substring(0, split).trim();
            String direction = obc.substring(split).trim().toLowerCase(Locale.getDefault());
            if (direction.equals("desc") || direction.equals("descending")) {
                _modifier = -1;
            } else {
                _modifier = 1;
            }
        } else if (obc.length() > 0) {
            _field = obc;
        }
        if (_field.startsWith("_")) {
            _field = _field.substring(1); 
        }
    }

    /**
     * @param orderByClause  &lt;bean property name&gt; [asc|desc], e.g. "id desc" or "_id desc".
     * @param equalsCaseComparator Second comparator in case that compared values
     * are equal - them the equalsCaseComparator is applied.
     */
    public BeanComparator(String orderByClause, BeanComparator equalsCaseComparator) {
        this(orderByClause);
        _equalsCaseComparator = equalsCaseComparator;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public int compare(Object a, Object b) {
        int rc;
        BeanWrapperImpl bwa = new BeanWrapperImpl(a);
        BeanWrapperImpl bwb = new BeanWrapperImpl(b);
        Object ca = bwa.getPropertyValue(_field);
        Object cb = bwb.getPropertyValue(_field);
        if (ca == null || cb == null) {
            if (ca == null && cb == null) {
                rc = 0;
            } else if (ca == null) {
                rc = -1;
            } else {
                rc = 1;
            }
        } else {
            if (ca instanceof Comparable) {
                rc = ((Comparable) ca).compareTo(cb);
            } else {
                throw new UnsupportedOperationException("Unsupported type for ordering: " + ca.getClass().getName());
            }
        }
        if (rc == 0) {
            if (_equalsCaseComparator != null) {
                rc = _equalsCaseComparator.compare(a, b);
            } else {
                Long ida = (Long) bwa.getPropertyValue("id");
                Long idb = (Long) bwb.getPropertyValue("id");
                rc = compareId(ida, idb);
            }
        }
        return rc * _modifier;
    }

    private int compareId(Long ida, Long idb) {
        int rc = 0;
        if (ida != null && idb != null) {
            rc = ida.compareTo(idb);
        } else if (ida == null && idb != null) {
            rc = -1;
        } else if (ida != null) {
            rc = 1;
        }
        return rc;
    }

}