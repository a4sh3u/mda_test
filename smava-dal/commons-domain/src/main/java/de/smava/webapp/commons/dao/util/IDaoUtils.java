package de.smava.webapp.commons.dao.util;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.smava.webapp.commons.web.ListFilter;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.util.BeanComparator;

public interface IDaoUtils {

	public <T extends Entity> void save(T entity);

	public <T extends Entity> BaseDao<T> getDao(Class<T> clazz);

	public <T extends Entity> BaseDao<T> getDao(String type);

	/**
	 * Get entity list (calls find[type]List(String, Object...) method of appropriate dao).
	 * @param type Simple class name of type to search for
	 * @param term Oql term, might contain parameter names ("... == param1 ... PARAMETERS java.lang.String param1 ..."
	 * @param params Object array for query evaluation
	 * @param orderByTerm like "id desc"...
	 * @return Collection of found objects
	 */
	public <T extends Entity> Collection<T> findEntityList(String type,
			String term, String orderByTerm, List<Object> params);

	public <T extends Entity> Collection<T> findEntityList(String type,
			String term, Pageable pageable, Sortable sortable,
			List<Object> params);

	public long countEntityList(String type, String term, List<Object> params);

	/**
	 * Filter objects out of allObjects respecting pageable (optional), sortable (optional).
	 */
	public <T extends Entity> Collection<T> getEntityList(
			Collection<T> allObjects, Pageable pageable, Sortable sortable);

	/**
	 * Filter objects out of allObjects respecting orderByTerm (not null but might be empty, otherwise containing "order by")
	 * ans optional pageable.
	 * @param allObjects List of objects
	 * @param orderByTerm not null, an order by clause. Examples: "" (no ordering), "a == b" (no ordering), "order by a" (ordering by
	 * a), "a == b order by a" (ordering by a).
	 * @param pageable Optional pageable
	 */
	public <T extends Entity> Collection<T> getEntityList(
			Collection<T> allObjects, String orderByTerm, Pageable pageable);

	/**
	 * Sort list of (bean) objects according to term
	 * using the {@link BeanComparator}.
	 * @param term order term with "order by"
	 */
	public <T extends Entity> void sort(List<T> list, String term);

	public String getIdList(final Collection<? extends Entity> entities);
	
	public List<Long> getIdsAsList(final Collection<? extends Entity> entities);

	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException;

    <T extends Entity> String createQueryTerm(Class<T> targetClass, ListFilter filter, Map<String, String[]> valuesMap, List<Object> parameters);
    <T extends Entity> String createQueryTerm(String targetType, ListFilter filter, Map<String, String[]> valuesMap, List<Object> parameters);
}