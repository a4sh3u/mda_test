package de.smava.webapp.commons.util;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

/**
 * Implements an Enumeration that combines elements from a Set and
 * an Enumeration. Used by ListResourceBundle and PropertyResourceBundle.
 *
 * @author (michael.schwalbe)
 */
class ArtResourceBundleEnumeration implements Enumeration<String> {
    private final Set<String> _set;
    private final Iterator<String> _iterator;
    private final Enumeration<String> _enumeration; // may remain null

    /**
     * Constructs a resource bundle enumeration.
     * @param set an set providing some elements of the enumeration
     * @param enumeration an enumeration providing more elements of the enumeration.
     *        enumeration may be null.
     */
    ArtResourceBundleEnumeration(Set<String> set, Enumeration<String> enumeration) {
        _set = set;
        _iterator = set.iterator();
        _enumeration = enumeration;
    }

    private String _next = null;

    public boolean hasMoreElements() {
        final boolean hasNext;
        if (_next == null) {
            if (_iterator.hasNext()) {
                _next = _iterator.next();
            } else if (_enumeration != null) {
                while (_next == null && _enumeration.hasMoreElements()) {
                    _next = _enumeration.nextElement();
                    if (_set.contains(_next)) {
                        _next = null;
                    }
                }
            }
            hasNext = _next != null;
        } else {
            hasNext = true;
        }
        return hasNext;
    }

    public String nextElement() {
        if (hasMoreElements()) {
            String result = _next;
            _next = null;
            return result;
        } else {
            throw new NoSuchElementException();
        }
    }
}