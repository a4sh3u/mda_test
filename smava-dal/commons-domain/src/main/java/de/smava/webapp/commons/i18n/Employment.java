package de.smava.webapp.commons.i18n;

import com.aperto.webkit.utils.ConstCollector;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Daniel Doubleday
 */
public final class Employment {
    public static final String EMPLOYEE = "EMPLOYEE";
    public static final String FREELANCER = "FREELANCER";
    public static final String TRADESMAN = "TRADESMAN";
    public static final String STUDENT = "STUDENT";
    public static final String RETIREE = "RETIREE";
    public static final String TRAINEE = "TRAINEE";
    public static final String OTHER   = "OTHER";
    public static final String CIVIL_CERVANT   = "CIVIL_CERVANT";
    public static final String CEO = "CEO";
    public static final String ANNUITANT = "ANNUITANT";
    public static final String LABOURER = "LABOURER";
    public static final String MANAGER = "MANAGER";
    public static final String PENSIONAR = "PENSIONAR";

    /**
     * Includes freelancer and tradesman as combined entry for the select boxes.
     */
    public static final String GENERAL_FREELANCER = "GENERAL_FREELANCER";

	public static final Set<String> FREELANCER_EMPLOYMENTS = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {FREELANCER, TRADESMAN, GENERAL_FREELANCER})));
	/**
	 * Employee and Labourer are often handled simultaneous, so they are handled in this set.
	 */
	public static final Set<String> EMPLOYEE_EMPLOYMENTS = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(new String[] {EMPLOYEE, LABOURER})));
    @SuppressWarnings("unchecked")
	public static final Set<String> EMPLOYMENTS = ConstCollector.getAll("");
    private Employment() {
        // Only used as a container for constants
    }
}