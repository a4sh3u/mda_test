/**
 * 
 */
package de.smava.webapp.commons.domain;



/**
 * @author bvoss
 * 
 * @param <T> the entity defining this template
 *
 */
public abstract class AbstractImageInfo<T extends Entity> extends AbstractEntityInfo<T> {
	
	private String _image;
	private Integer _imageWidth;
	private Integer _imageHeight;
	
	protected AbstractImageInfo(final Class<T> infoForDomainClass) {
		super(infoForDomainClass);
	}
	public String getImage() {
		return _image;
	}
	public void setImage(final String image) {
		_image = image;
	}
	public Integer getImageWidth() {
		return _imageWidth;
	}
	public void setImageWidth(final Integer imageWidth) {
		_imageWidth = imageWidth;
	}
	public Integer getImageHeight() {
		return _imageHeight;
	}
	public void setImageHeight(final Integer imageHeight) {
		_imageHeight = imageHeight;
	}
}
