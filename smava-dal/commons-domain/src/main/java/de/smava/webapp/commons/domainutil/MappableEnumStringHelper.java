/**
 * 
 */
package de.smava.webapp.commons.domainutil;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.Assert;

import de.smava.webapp.commons.domain.MappableEnumString;


/**
 * maps database IDs to enums.
 *
 * @author bvoss
 *
 */
public final class MappableEnumStringHelper {

	private static final Map<Class<?>, Map<String, Object>> DBVALUE_MAP = new HashMap<Class<?>, Map<String, Object>>();

	private MappableEnumStringHelper() {
		super();
	}

	public static <T> T getEnumFromDbValue(final Class<?> enumClass, final String dbValue) {
		final T result;
		if (dbValue == null) {
			result = null;
		} else {
			Assert.isAssignable(Enum.class, enumClass);
			Assert.isAssignable(MappableEnumString.class, enumClass);
			Map<String, Object> enumMap = DBVALUE_MAP.get(enumClass);
			if (enumMap == null) {
				synchronized (DBVALUE_MAP) {
					enumMap = DBVALUE_MAP.get(enumClass);
					if (enumMap == null) {
						enumMap = new HashMap<String, Object>();
						for (Object enumElement : enumClass.getEnumConstants()) {
							enumMap.put(((MappableEnumString<?>) enumElement).getDbValue(), enumElement);
						}
						DBVALUE_MAP.put(enumClass, enumMap);
					}
				}
			}

			result = (T) enumMap.get(dbValue);
			if (result == null) {
				throw new IllegalArgumentException("unknown " + dbValue + " for " + enumClass.getName());
			}
		}

		return result;
	}
}
