package de.smava.webapp.commons.dao.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import de.smava.webapp.commons.web.ListFilter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.util.BeanComparator;

/**
 * Helper for daos.
 * <p/>
 * Date: 19.09.2006
 *
 * @author ingmar.decker
 */
@Component(value = "daoUtils")
public class DaoUtils implements ApplicationContextAware, IDaoUtils {

    public static final Object[] EMPTY_OBJECT_ARRAY = new Object[]{};

    private static final String SQL_DELIM = ",";

    private ApplicationContext _applicationContext;
    
    private static final String CANT_FIND_ENTITY_LIST_FOR = "Can't find entity list for ";
    
    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#save(T)
	 */
    @Override
	public <T extends Entity> void save(T entity) {
        BaseDao<T> dao = (BaseDao<T>) getDao(entity.getClass());
        dao.save(entity);
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#getDao(java.lang.Class)
	 */
    @Override
	public <T extends Entity> BaseDao<T> getDao(Class<T> clazz) {
        if (clazz == null) {
            throw new IllegalArgumentException("Class argument must not be null.");
        }
        return getDao(clazz.getSimpleName());
    }


    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#getDao(java.lang.String)
	 */
    @Override
	public <T extends Entity> BaseDao<T> getDao(String type) {
        final String s1 = type.substring(0, 1);
        @SuppressWarnings("unchecked")
        BaseDao<T> dao = (BaseDao<T>) _applicationContext.getBean(s1.toLowerCase() + type.substring(1) + "Dao", BaseDao.class);

        return dao;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#findEntityList(java.lang.String, java.lang.String, java.lang.String, java.util.List)
	 */
    @Override
	public <T extends Entity> Collection<T> findEntityList(String type, String term, String orderByTerm, List<Object> params) {
        List<T> list = new ArrayList<T>();
        try {
            Object dao = getDao(type);
            Method listGetter = dao.getClass().getMethod(getMethodName("find", type, "List"), new Class[]{String.class, Object[].class});
            list.addAll((Collection<T>) listGetter.invoke(dao, new Object[]{term, params.toArray(EMPTY_OBJECT_ARRAY)}));
            Collections.sort(list, new BeanComparator(orderByTerm));
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e.getClass().getSimpleName() + " Error in findEntityList() for type '" + type + "', term '" + term + "', orderByTerm '" + orderByTerm + "' - " + e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(e.getClass().getSimpleName() + " Error in findEntityList() for type '" + type + "', term '" + term + "', orderByTerm '" + orderByTerm + "' - " + e.getMessage(), e);
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException(e.getClass().getSimpleName() + " Error in findEntityList() for type '" + type + "', term '" + term + "', orderByTerm '" + orderByTerm + "' - " + e.getMessage(), e);
        }
        return list;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#findEntityList(java.lang.String, java.lang.String, de.smava.webapp.commons.pagination.Pageable, de.smava.webapp.commons.pagination.Sortable, java.util.List)
	 */
    @Override
	public <T extends Entity> Collection<T> findEntityList(String type, String term, Pageable pageable, Sortable sortable, List<Object> params) {
        List<T> list = new ArrayList<T>();
        try {
            Object dao = getDao(type);
            Method listGetter = dao.getClass().getMethod(getMethodName("find", type, "List"), new Class[]{String.class, Pageable.class, Sortable.class, Object[].class});
            list.addAll((Collection<T>) listGetter.invoke(dao, new Object[]{term, pageable, sortable, params.toArray(EMPTY_OBJECT_ARRAY)}));
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(CANT_FIND_ENTITY_LIST_FOR + type, e);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(CANT_FIND_ENTITY_LIST_FOR + type, e);
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException(CANT_FIND_ENTITY_LIST_FOR + type, e);
        }
        return list;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#countEntityList(java.lang.String, java.lang.String, java.util.List)
	 */
    @Override
	public long countEntityList(String type, String term, List<Object> params) {
        long result = 0;
        try {
            Object dao = getDao(type);
            Method listGetter = dao.getClass().getMethod(getMethodName("get", type, "Count"), new Class[]{String.class, Object[].class});
            result = (Long) listGetter.invoke(dao, new Object[]{term, params.toArray(EMPTY_OBJECT_ARRAY)});
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(CANT_FIND_ENTITY_LIST_FOR + type, e);
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException(CANT_FIND_ENTITY_LIST_FOR + type, e);
        } catch (InvocationTargetException e) {
            throw new IllegalArgumentException(CANT_FIND_ENTITY_LIST_FOR + type, e);
        }
        return result;
    }

    
    /**
     * Returns method name: prefix + first letter uppercase property + suffix.
     */
    public static String getMethodName(String prefix, String property, String suffix) {
        String propName = property.substring(0, 1).toUpperCase(Locale.getDefault());
        propName += property.substring(1);
        return prefix + propName + suffix;
    }

    /**
     * Returns getter method name for property.
     */
    public static String getGetterName(String property) {
        return getMethodName("get", property, "");
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#getEntityList(java.util.Collection, de.smava.webapp.commons.pagination.Pageable, de.smava.webapp.commons.pagination.Sortable)
	 */
    @Override
	public <T extends Entity> Collection<T> getEntityList(Collection<T> allObjects, Pageable pageable, Sortable sortable) {
        List<T> sortList = new ArrayList<T>();
        sortList.addAll(allObjects);

        // Check order by (sortable)...
        String orderBy = "";
        if (sortable != null && sortable.hasSort()) {
            if (sortable.sortDescending()) {
                orderBy = "order by " + sortable.getSort() + " " + "descending";
            } else {
                orderBy = "order by " + sortable.getSort() + " " + "ascending";
            }
        }
        return getEntityList(sortList, orderBy, pageable);
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#getEntityList(java.util.Collection, java.lang.String, de.smava.webapp.commons.pagination.Pageable)
	 */
    @Override
	public <T extends Entity> Collection<T> getEntityList(Collection<T> allObjects, String orderByTerm, Pageable pageable) {
        // Sort using order by...
        List<T> sortList = new ArrayList<T>();
        sortList.addAll(allObjects);

        sort(sortList, orderByTerm);

        Collection<T> resultList = new ArrayList<T>();

        // Copy list and respect pageable...
        long fromInclusive = 0;
        long uptoExclusive = allObjects.size();
        if (pageable != null) {
            fromInclusive = pageable.getOffset();
            uptoExclusive = pageable.getOffset() + pageable.getItemsPerPage();
        }
        long i = 0;
        for (T o : sortList) {
            if (i >= fromInclusive && i < uptoExclusive) {
                resultList.add(o);
            }
            i++;
        }
        return resultList;
    }


    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#sort(java.util.List, java.lang.String)
	 */
    @Override
	public <T extends Entity> void sort(List<T> list, String term) {
        String orderByTerm = "order by";
        int orderbyIndex = term.toLowerCase(Locale.getDefault()).indexOf(orderByTerm);
        String orderBy;
        if (orderbyIndex >= 0) {
            orderBy = term.substring(orderbyIndex + orderByTerm.length()).trim();
            Collections.sort(list, new BeanComparator(orderBy));
        }
    }
    
    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#getIdList(java.util.Collection)
	 */
    @Override
	public String getIdList(final Collection<? extends Entity> entities) {
    	final String result;
	    if (CollectionUtils.isNotEmpty(entities)) {
	    	final StringBuilder inIdsStringBuffer = new StringBuilder();
			for (Entity entity : entities) {
				inIdsStringBuffer.append(entity.getId());
				inIdsStringBuffer.append(SQL_DELIM);
			}
			inIdsStringBuffer.deleteCharAt(inIdsStringBuffer.length() - 1);
			result = inIdsStringBuffer.toString();
	    } else {
	    	result = StringUtils.EMPTY;
	    }
		return result;
    }
    
    public List<Long> getIdsAsList(final Collection<? extends Entity> entities){
    	List<Long> result = new ArrayList<Long>();
    	if (CollectionUtils.isNotEmpty(entities)) {
    		for (Entity entity : entities) {
    			result.add(entity.getId());
    		}
    	}
    	return result;
    }

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.dao.util.IDaoUtils#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		_applicationContext = applicationContext;
	}

    /**
     * Returns an oql query (in braces) string with parameter placeholders if necessary and appends
     * parameter objects to given list.
     * @param valuesMap Values map, usualy the http servlet request's parameters
     * @param parameters List not null to add parameter objects to for calling dao's find methods with parameter array
     */
    @Override
    public <T extends Entity> String createQueryTerm(Class<T> targetClass, ListFilter filter, Map<String, String[]> valuesMap, List<Object> parameters) {
        String concat = "";
        BaseDao<T> dao = getDao(targetClass);
        return dao.createQueryTerm(filter.getFilterElements(), valuesMap, parameters, concat);
    }

    @Override
    public <T extends Entity> String createQueryTerm(String targetType, ListFilter filter, Map<String, String[]> valuesMap, List<Object> parameters) {
        String concat = "";
        BaseDao<T> dao = getDao(targetType);
        return dao.createQueryTerm(filter.getFilterElements(), valuesMap, parameters, concat);
    }
}