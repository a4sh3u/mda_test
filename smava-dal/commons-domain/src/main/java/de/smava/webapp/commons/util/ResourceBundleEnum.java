package de.smava.webapp.commons.util;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * @author Daniel Doubleday
 */
public class ResourceBundleEnum implements Serializable, ValueProvider {
    private static final long serialVersionUID = 4128342259731237572L;

    private static final Logger LOGGER = Logger.getLogger(ResourceBundleEnum.class);

    private final Map<Locale, ResourceBundle> _bundles = new HashMap<Locale, ResourceBundle>();

    private String _resourceBundleBaseName = null;
    
    private String _encoding = "UTF-8";
    
    public String getResourceBundleBaseName() {
        return _resourceBundleBaseName;
    }

    public void setResourceBundleBaseName(String resourceBundleBaseName) {
        _resourceBundleBaseName = resourceBundleBaseName;
    }

    public String getValue(String key) {
        try {
            ResourceBundle bundle = getResourceBundle(Locale.getDefault());
            return bundle.getString(key);
        } catch (MissingResourceException e) {
            LOGGER.error("Can't find resource bundle for key '" + key + "' (" + _resourceBundleBaseName + ")", e);
            // For a slightly better error message:
            throw new MissingResourceException("Can't find resource bundle for key '" + key + "' (" + _resourceBundleBaseName + ")", this.getClass().getSimpleName(), key);
        }
    }

    public Enumeration<String> getKeys() {
        return getResourceBundle(Locale.getDefault()).getKeys();
    }

    public Map<String, String> asMap() {
        return asMap(false);
    }

    public Map<String, String> asMap(boolean addEmptyOption) {
        Map<String, String> result = new LinkedHashMap<String, String>();
        if (addEmptyOption) {
            result.put("", "");
        }
        Enumeration<String> keys = getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            result.put(key, getValue(key));
        }
        return result;
    }

    private ResourceBundle getResourceBundle(Locale locale) {
        synchronized (_bundles) {
            ResourceBundle result = _bundles.get(locale);
            if (result == null) {
                try {
                    Reader resourceStream = getResourceStream(locale);
                    result = new ArtPropertyResourceBundle(resourceStream);
                    _bundles.put(locale, result);
                } catch (Exception e) {
                    throw new MissingResourceException("Can't find bundle for base name "
                            + _resourceBundleBaseName
                            + ", locale " + locale, _resourceBundleBaseName + "_" + locale, "");
                }
            }
            return result;
        }
    }

    protected Reader getResourceStream(Locale locale) {
        StringBuilder bundleName = new StringBuilder();
        bundleName.append(_resourceBundleBaseName.replaceAll("\\.", "/"));
        //Try to get a stream for the file <basename>_<complete locale string>
        final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        InputStream resourceStream = classLoader.getResourceAsStream(bundleName.toString() + ".properties");
        //if no resource stream is available, try to get a stream for the file <basename>_<language>
        if (resourceStream == null && locale != null && locale.toString().length() > 0) {
            bundleName.append("_");
            bundleName.append(locale.getLanguage());
            resourceStream = classLoader.getResourceAsStream(bundleName.toString() + ".properties");
        }
        //if no resource stream is available, try to get a stream for the file <basename>
        if (resourceStream == null) {
            if (locale != null && locale.toString().length() > 0) {
                bundleName.append("_");
                bundleName.append(locale.getCountry());
                resourceStream = classLoader.getResourceAsStream(bundleName.toString() + ".properties");
            }
            resourceStream = classLoader.getResourceAsStream(bundleName.toString());
        }
        InputStreamReader result = null;
        try {
            result = new InputStreamReader(resourceStream, _encoding);
        } catch (UnsupportedEncodingException e) {
            // never happens
            throw new IllegalStateException(e);
        }
        return result;
    }

	public final String getEncoding() {
		return _encoding;
	}

	public final void setEncoding(String encoding) {
		_encoding = encoding;
	}
}