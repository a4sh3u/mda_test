/**
 * Numeric constants.
 */
package de.smava.webapp.commons.constants;

/**
 * @author Dimitar Robev
 *
 */
public final class NumericalData {

	private NumericalData() {
	}

	public static final long NANO_TO_MILLI = 1000000;

}
