/**
 * 
 */
package de.smava.webapp.commons.domain;


/**
 * @author bvoss
 *
 * @param <T> the entity defining this template
 */
public abstract class AbstractEntityInfo<T extends Entity> {
	
	private final Class<T> _infoForDomainClass;
	
	private Long _entityId;

	protected AbstractEntityInfo(final Class<T> infoForDomainClass) {
		super();
		_infoForDomainClass = infoForDomainClass;
	}

	public Long getEntityId() {
		return _entityId;
	}

	public void setEntityId(final Long id) {
		_entityId = id;
	}

	public Class<T> getInfoForDomainClass() {
		return _infoForDomainClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((_entityId == null) ? 0 : _entityId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractEntityInfo other = (AbstractEntityInfo) obj;
		if (_entityId == null) {
			if (other._entityId != null) {
				return false;
			}
		} else if (!_entityId.equals(other._entityId)) {
			return false;
		}
		return true;
	}

	
	
}
