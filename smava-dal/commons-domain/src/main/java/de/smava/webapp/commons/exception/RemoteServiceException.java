package de.smava.webapp.commons.exception;

/**
 * Indicating errors while calling external services.
 * 
 * Service-specific exceptions should extend this error.
 * 
 * @author aherr
 *
 */
public abstract class RemoteServiceException extends SmavaException {

	public RemoteServiceException() {
		super();
	}

	public RemoteServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public RemoteServiceException(String message) {
		super(message);
	}

	public RemoteServiceException(Throwable cause) {
		super(cause);
	}

}
