package de.smava.webapp.commons.domainutil.jdo;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import de.smava.webapp.commons.pagination.Sortable;

/**
 * @author Daniel Doubleday
 */
public class OqlTerm {

    private static final String CONDITION_EQUALS = "==";
    private static final String CONDITION_NOT_EQUALS = "!=";
    private static final String CONDITION_LESS_THAN = "<";
    private static final String CONDITION_GREATER_THAN = ">";
    private static final String CONDITION_LESS_THAN_EQUALS = "<=";
    private static final String CONDITION_GREATER_THAN_EQUALS = ">=";

    private static final String BOOLEAN_COMBINE_AND = "&&";
    private static final String BOOLEAN_COMBINE_OR = "||";

    private RootTerm _rootTerm;

    public static OqlTerm newTerm() {
        return new RootTerm();
    }

    protected OqlTerm(RootTerm rootTerm) {
        _rootTerm = rootTerm;
        if (_rootTerm != null) {
            _rootTerm.setTerm(this);
        }
    }

    RootTerm getRootTerm() {
        return _rootTerm;
    }

    void setRootTerm(RootTerm rootTerm) {
        _rootTerm = rootTerm;
    }

    public void setSortBy(String sortBy) {
        getRootTerm().setSortBy(sortBy);
    }

    public void setGroupBy(String groupBy) {
        getRootTerm().setGroupBy(groupBy);
    }

    public void setRange(long start, long end) {
        getRootTerm().setRange(start, end);
    }

    public void setSortDescending(boolean value) {
        getRootTerm().setSortDescending(value);
    }

    public OqlTerm and(OqlTerm ... terms) {
        return new CompoundTerm(getRootTerm(), BOOLEAN_COMBINE_AND, terms);
    }

    public OqlTerm or(OqlTerm ... terms) {
        return new CompoundTerm(getRootTerm(), BOOLEAN_COMBINE_OR, terms);
    }

    public OqlTerm equals(String fieldName, Object value) {
        return new ConditionalTerm(getRootTerm(), CONDITION_EQUALS, fieldName, value);
    }

    public OqlTerm notEquals(String fieldName, Object value) {
        return new ConditionalTerm(getRootTerm(), CONDITION_NOT_EQUALS, fieldName, value);
    }

    public OqlTerm isNull(String fieldName) {
        return new ConditionalTerm(getRootTerm(), CONDITION_EQUALS, fieldName, null);
    }

    public OqlTerm notNull(String fieldName) {
        return new ConditionalTerm(getRootTerm(), CONDITION_NOT_EQUALS, fieldName, null);
    }


    public OqlTerm lessThan(String fieldName, Object value) {
        return new ConditionalTerm(getRootTerm(), CONDITION_LESS_THAN, fieldName, value);
    }

    public OqlTerm lessThanEquals(String fieldName, Object value) {
        return new ConditionalTerm(getRootTerm(), CONDITION_LESS_THAN_EQUALS, fieldName, value);
    }

    public OqlTerm greaterThan(String fieldName, Object value) {
        return new ConditionalTerm(getRootTerm(), CONDITION_GREATER_THAN, fieldName, value);
    }

    public OqlTerm greaterThanEquals(String fieldName, Object value) {
        return new ConditionalTerm(getRootTerm(), CONDITION_GREATER_THAN_EQUALS, fieldName, value);
    }

    /**
     * The root term of the oql query.
     */
    static class RootTerm extends OqlTerm implements Sortable {
        private OqlTerm _term;
        private final Map<String, Class<?>> _params = new TreeMap<String, Class<?>>();
        private String _sortBy;
        private String _groupBy;
        private boolean _sortDescending;
        private long _start = -1;
        private long _end = -1;

        RootTerm() {
            super(null);
            setRootTerm(this);
        }

        void setTerm(OqlTerm term) {
            _term = term;
        }

        String createParameter(Object value) {
            Class<? extends Object> clazz = value.getClass();
            String name = getNextParamName();
            _params.put(name, clazz);
            return name;
        }

        String getNextParamName() {
        	
        	String s = ""+(_params.size() + 1);
        	if ( s.length() == 1){
        		s = "0" +s;
        	}
        	
            return "param" + s;
        }

        public String toString() {
            StringBuilder result = new StringBuilder();
            result.append(_term.toString());
            appendParamsTerm(result);
            appendOrderByTerm(result);
            appendGroupByTerm(result);
            appendRangeTerm(result);
            return result.toString();
        }

        private void appendParamsTerm(StringBuilder result) {
            if (!_params.isEmpty()) {
                result.append(" PARAMETERS");
                Set<Map.Entry<String, Class<?>>> entries = _params.entrySet();
                boolean first = true;
                for (Map.Entry<String, Class<?>> entry : entries) {
                    if (first) {
                        result.append(' ');
                        first = false;
                    } else {
                        result.append(", ");
                    }
                    result.append(entry.getValue().getName());
                    result.append(' ');
                    result.append(entry.getKey());
                }
            }
        }

        private void appendOrderByTerm(StringBuilder builder) {
            if (hasSort()) {
                builder.append(" ORDER BY ");
                builder.append(getSort());
                builder.append(" ");
                if (sortDescending()) {
                    builder.append("descending");
                } else {
                    builder.append("ascending");
                }
            }
        }

        private void appendGroupByTerm(StringBuilder builder) {
            if (hasGroup()) {
                builder.append(" GROUP BY ");
                builder.append(getGroup());
            }
        }

        private void appendRangeTerm(StringBuilder builder) {
            if (hasRange()) {
                builder.append(" RANGE ");
                builder.append(getStart());
                builder.append(" , ");
                builder.append(getEnd());
            }
        }

        public void setRange(long start, long end) {
            _start = start;
            _end = end;
        }

        public boolean hasRange() {
            return _start != -1 && _end != -1;
        }

        public long getStart() {
            return _start;
        }

        public long getEnd() {
            return _end;
        }

        public String getSort() {
            return _sortBy;
        }

        public void setSortBy(String sortBy) {
            _sortBy = sortBy;
        }

        public boolean hasSort() {
            return StringUtils.isNotEmpty(_sortBy);
        }

        public String getGroup() {
            return _groupBy;
        }

        public void setGroupBy(String groupBy) {
            _groupBy = groupBy;
        }

        public boolean hasGroup() {
            return StringUtils.isNotEmpty(_groupBy);
        }

        public boolean sortDescending() {
            return _sortDescending;
        }

        public void setSortDescending(boolean sortDescending) {
            _sortDescending = sortDescending;
        }
    }

    /**
     * Term which represents conditional terms.
     */
    static class ConditionalTerm extends OqlTerm {
        private final String _type;
        private final String _fieldName;
        private final Object _value;
        private String _toString;

        public ConditionalTerm(RootTerm rootTerm, String type, String fieldName, Object value) {
            super(rootTerm);
            _type = type;
            _fieldName = fieldName;
            _value = value;
        }

        public String toString() {
            if (_toString == null) {
                StringBuilder builder = new StringBuilder();
                builder.append(_fieldName);
                builder.append(' ');
                builder.append(_type);
                builder.append(' ');
                if (_value == null) {
                    builder.append("null");
                } else {
                    if (_value instanceof Number) {
                        builder.append(_value);
                    } else if (_value instanceof String) {
                        String value = (String) _value;
                        builder.append('\'');
                        builder.append(value.replaceAll("'", "\\\\'"));
                        builder.append('\'');
                    } else {
                        builder.append(getRootTerm().createParameter(_value));
                    }
                }
                _toString = builder.toString();
            }
            return _toString;
        }
    }

    /**
     * Term which represents compound terms.
     */
    static class CompoundTerm extends OqlTerm {
        private final String _type;
        private final OqlTerm[] _terms;
        private String _toString;

        public CompoundTerm(RootTerm rootTerm, String type, OqlTerm ... terms) {
            super(rootTerm);
            if (terms.length < 2) {
                throw new IllegalArgumentException("Compound term must consist of at least 2 terms");
            }
            _type = type;
            _terms = terms;
        }

        public String toString() {
            if (_toString == null) {
                StringBuilder builder = new StringBuilder();
                builder.append('(');
                builder.append(_terms[0]);
                for (int i = 1; i < _terms.length; i++) {
                    builder.append(' ');
                    builder.append(_type);
                    builder.append(' ');
                    builder.append(_terms[i]);
                }
                builder.append(')');
                _toString = builder.toString();
            }
            return _toString;
        }
    }
}