package de.smava.webapp.commons.domain;

import java.io.Serializable;

/**
 * @author Daniel Doubleday
 */
public class Change implements Serializable {

    private static final long serialVersionUID = 6365996931673834504L;
	
    private String _oldValue;
    private String _newValue;

    public Change() {
    }

    public Change(Object oldValue, Object newValue) {
        if (oldValue != null) {
            if (oldValue instanceof Entity) {
                Entity entity = (Entity) oldValue;
                _oldValue = entity.getClass().getSimpleName() + "=" + entity.getId();
            } else {
                _oldValue = oldValue.toString();
            }
        } else {
            _oldValue = null;
        }

        if (newValue != null) {
            if (newValue instanceof Entity) {
                Entity entity = (Entity) newValue;
                _newValue = entity.getClass().getSimpleName() + "=" + entity.getId();
            } else {
                _newValue = newValue.toString();
            }
        } else {
            _newValue = null;
        }
    }

    public String getOldValue() {
        return _oldValue;
    }

    public void setOldValue(String oldValue) {
        _oldValue = oldValue;
    }

    public String getNewValue() {
        return _newValue;
    }

    public void setNewValue(String newValue) {
        _newValue = newValue;
    }
}