package de.smava.webapp.commons.exception;

/**
 * Created by aherr on 20.11.14.
 */
public class SmavaWebException extends SmavaException {

    public SmavaWebException(String msg) {
        super(msg);
    }

    public SmavaWebException(Exception ex) {
        super(ex);
    }

}
