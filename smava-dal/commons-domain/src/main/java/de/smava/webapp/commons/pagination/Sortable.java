package de.smava.webapp.commons.pagination;

/**
 * Interface for classes being sortable after a specified field.
 *
 * @author michael.schwalbe (17.10.2005)
 */
public interface Sortable {
    /**
     * Gets the sorting key.
     */
    String getSort();

    /**
     * Gets the sorting state.
     *
     * @return True if there is a sorting.
     */
    boolean hasSort();

    /**
     * Gets the sorting direction.
     *
     * @return True if the sorting is descending.
     */
    boolean sortDescending();
}