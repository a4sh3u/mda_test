package de.smava.webapp.commons.domainutil;

import com.aperto.webkit.utils.ExceptionEater;
import de.smava.webapp.commons.dao.util.DaoUtils;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.util.ResourceBundleEnum;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.MissingResourceException;

/**
 * Provider for all resources. Also provides a method to get the resource based translation of
 * a value for a bean and property ({@link ResourceProvider#getResourceValue(de.smava.webapp.commons.domain.Entity, String)},
 * {@link ResourceProvider#getInstance(org.springframework.context.ApplicationContext)}.<br />
 * The latter is useful for JSP to get this provider and translates values e.g. in list views.
 * <br />Example:<br />
 * <pre>

 &lt;%
     ResourceProvider resourceProvider = ResourceProvider.getInstance(pageContext);
 %&gt;
 &lt;!-- ... --&gt;
                     &lt;td&gt;&lt;%= resourceProvider.getResourceValue(order, "state") %&gt;&lt;/td&gt;


   </pre>
 * <br />
 * A resourceProvider bean must be defined in application context with id "resourceProvider".
 * <br />
 *
 * Date: 20.09.2006
 *
 * @author ingmar.decker
 *
 * @author aakhmerov
 * WEBSITE-11988
 * isolate in DAL, refactor to be depending on spring context only, without any binding to web components
 */
public class ResourceProvider implements IResourceProvider {
    private static final Logger LOGGER = Logger.getLogger(ResourceProvider.class);

    private ResourceBundleEnum _academicTitleResource;
    private ResourceBundleEnum _salutationResource;
    private ResourceBundleEnum _employmentResource;
    private ResourceBundleEnum _familyStatusResource;
    private ResourceBundleEnum _confessionResource;
    private ResourceBundleEnum _numberOfChildrenResource;
    private ResourceBundleEnum _numberOfOtherInHouseholdResource;
    private ResourceBundleEnum _numberOfPeopleInHouseholdResource;
    private ResourceBundleEnum _securityQuestionsResource;
    private ResourceBundleEnum _transactionStatesResource;
    private ResourceBundleEnum _transactionTypesResource;
    private ResourceBundleEnum _smavaGroupTypesResource;
    private ResourceBundleEnum _smavaGroupStatesResource;
    private ResourceBundleEnum _bookingTypesResource;
    private ResourceBundleEnum _customerBookingTypesResource;
    private ResourceBundleEnum _bookingGroupTypesResource;
    private ResourceBundleEnum _durationOfOccupationResource;
    private ResourceBundleEnum _expensesCarResource;
    private ResourceBundleEnum _modeOfAccommodationResource;
    private ResourceBundleEnum _numOfCarsResource;
    private ResourceBundleEnum _userStatesResource;
    private ResourceBundleEnum _intendedUseResource;
    private ResourceBundleEnum _documentTypeResource;
    private ResourceBundleEnum _documentTypeOfDispatchResource;
    private ResourceBundleEnum _documentDirectionResource;
    private ResourceBundleEnum _smavaGroupMemberStatesResource;
    private ResourceBundleEnum _bidStatesResource;
    private ResourceBundleEnum _dtausFileStatesResource;
    private ResourceBundleEnum _dtausEntryTypesResource;
    private ResourceBundleEnum _dtausEntryStatesResource;
    private ResourceBundleEnum _contractStatesResource;
    private ResourceBundleEnum _documentStateResource;
    private ResourceBundleEnum _dealTypesResource;
    private ResourceBundleEnum _smavaGroupInterestingResource;
    private ResourceBundleEnum _entaxStatesResource;
    private ResourceBundleEnum _bookingPerformanceStatesResource;
    private ResourceBundleEnum _isoCitizenshipResource;
    private ResourceBundleEnum _isoCountryResource;
    private ResourceBundleEnum _rdiContractTypesResource;
    private ResourceBundleEnum _rdiContractTypesShortResource;
    private ResourceBundleEnum _rdiContractTypesBrokerageResource;
    private ResourceBundleEnum _rdiContractStatesResource;
    private ResourceBundleEnum _rdiPaymentStatesResource;
    private ResourceBundleEnum _rdiPaymentTypesResource;
    private ResourceBundleEnum _rdiRequestStatesResource;
    private ResourceBundleEnum _rdiResponseStatesResource;
    private ResourceBundleEnum _documentApprovalStateResource;
    private ResourceBundleEnum _typeOfCompanyResource;
    private ResourceBundleEnum _educationEarnedResource;
    private ResourceBundleEnum _documentContainerNamesResource;
    private ResourceBundleEnum _documentAttachmentContainerStatesResource;
    private ResourceBundleEnum _creditProjectSorterResource;
    private ResourceBundleEnum _sharedLoanResource;
    private ResourceBundleEnum _kindOfCreditResource;
    private ResourceBundleEnum _kindOfUserResource;
    private ResourceBundleEnum _miscEarnTypeResource;
    private ResourceBundleEnum _creditCardTypesResource;
    private ResourceBundleEnum _propertyOwnedTypesResource;
    private ResourceBundleEnum _documentTypeOfProviderResource;
    private ResourceBundleEnum _payoutModeResource;
    private ResourceBundleEnum _advisoryFailedResource;
    private ResourceBundleEnum _caJournalSubjectResource;
    private ResourceBundleEnum _vehicleTypeResource;
    private ResourceBundleEnum smsResource;
    private ResourceBundleEnum _selfemployedProfession;
    private ResourceBundleEnum estateTypeResource;
    private ResourceBundleEnum childBenefitResource;
    private ResourceBundleEnum numOfBikesResource;
    private ResourceBundleEnum livingConditionResource;
    private ResourceBundleEnum salariedStaffTypeResource;
    private ResourceBundleEnum pensionSupplementTypeResource;
    private ResourceBundleEnum additionalIncomeTypeResource;
    private ResourceBundleEnum salariedStaffSoldierResource;
    private ResourceBundleEnum pensionTypeResource;
    private ResourceBundleEnum lettedEstateResource;
    private ResourceBundleEnum salariedStaffCivilCervantResource;
    private ResourceBundleEnum genderResource;
    private ResourceBundleEnum casiAncillaryEmploymentType;
    private ResourceBundleEnum casiEmploymentType;
    private ResourceBundleEnum casiSalariedStaffType;
    private ResourceBundleEnum casiOfficalServiceGradeType;
    private ResourceBundleEnum casiIndustrySectorType;
    private ResourceBundleEnum casiFreelancerOccupationType;
    private ResourceBundleEnum casiLoanBorrowerType;
    private ResourceBundleEnum casiThirdPartyLoanType;
    private ResourceBundleEnum casidebtCreditsCardType;
    private ResourceBundleEnum casiApplicantsRelationshipType;
    private ResourceBundleEnum casiAccommodationType;
    private ResourceBundleEnum casiEstateType;
    private ResourceBundleEnum casiOtherPensionIncomeType;
    private ResourceBundleEnum casiOtherRevenueType;
    private ResourceBundleEnum casiMaritalStateType;
    private ResourceBundleEnum casiOtherExpenseType;
    private ResourceBundleEnum partTimeJobType;
    private ResourceBundleEnum borrowerRelationshipTypeResource;
    private ResourceBundleEnum selfEmployedType;

    private Map<String, ResourceBundleEnum> _entityPropertyResourceMap;

    /**
     * Get resource provider bean from application context.
     */
    public static ResourceProvider getInstance(ApplicationContext applicationContext ) {

        return (ResourceProvider) applicationContext.getBean("resourceProvider");
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getResourceValue(de.smava.webapp.commons.domain.Entity, java.lang.String)
	 */
    @Override
	public String getResourceValue(Entity entity, String propertyName) {
        String value = "";
        if (entity == null) {
            throw new IllegalArgumentException("Entity must not be null.");
        }
        if (propertyName == null) {
            throw new IllegalArgumentException("Property name must not be null.");
        }
        Map<String, ResourceBundleEnum> map = getEntityPropertyResourceMap();
        if (map == null) {
            throw new IllegalStateException("No property entityPropertyResourceMap set.");
        }
        String getValue = null;
        try {
            String resourceKey = entity.getClass().getName() + "._" + propertyName;
            ResourceBundleEnum resourceBundle = map.get(resourceKey);
            if (resourceBundle == null) {
            	// In case of there is no property on the resource bundle for the given key, try it again with the superclass
            	resourceKey = entity.getClass().getSuperclass().getName() + "._" + propertyName;
            	resourceBundle = map.get(resourceKey);
            	if (resourceBundle == null) {
            		throw new IllegalStateException("No resource found for " + resourceKey + ". None found in entityPropertyResourceMap. Forgot to define the mapping in application context?");
            	}
            }

            final Object getValueObj = PropertyUtils.getProperty(entity, propertyName);
            if (getValueObj != null) {
            	if (getValueObj instanceof String) {
            		getValue = (String) getValueObj;
				} else if (getValueObj instanceof Enum) {
					final Enum<?> enumVal = (Enum<?>) getValueObj;
					getValue = enumVal.name();
				} else {
					getValue = getValueObj.toString();
				}
            }
            if (getValue != null) {
                value = resourceBundle.getValue(getValue);
            }
        } catch (MissingResourceException e) {
            LOGGER.error(e.getMessage(), e);
            value = "[" + propertyName + "]";
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException("Cannot access property value for " + propertyName + " (value: " + getValue + ") of " + entity.getClass().getSimpleName(), e);
        } catch (NoSuchMethodException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException("Can't find property value for " + propertyName + " (value: " + getValue + ") of " + entity.getClass().getSimpleName(), e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException("Can't find property value for " + propertyName + " (value: " + getValue + ") of " + entity.getClass().getSimpleName(), e);
        }
        return value;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getResourceForName(java.lang.String)
	 */
    @Override
	public ResourceBundleEnum getResourceForName(String name) {
        ResourceBundleEnum res = null;
        try {
            res = (ResourceBundleEnum) getClass().getMethod(DaoUtils.getGetterName(name)).invoke(this);
        } catch (IllegalAccessException e) {
            LOGGER.error("Can't find resource for name " + name, new RuntimeException());
        } catch (NoSuchMethodException e) {
            LOGGER.error("Can't find methode for name " + name, new RuntimeException());
        } catch (InvocationTargetException e) {
            LOGGER.error("Can't find resourcre for name " + name, new RuntimeException());
        }
        return res;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#hasResourceForName(java.lang.String)
	 */
    @Override
	public boolean hasResourceForName(String name) {
        ResourceBundleEnum res = null;
        try {
            res = (ResourceBundleEnum) getClass().getMethod(DaoUtils.getGetterName(name)).invoke(this);
        } catch (Exception e) {
            ExceptionEater.eat(e);
        }
        return res != null;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getEntityPropertyResourceMap()
	 */
    @Override
	public Map<String, ResourceBundleEnum> getEntityPropertyResourceMap() {
        return _entityPropertyResourceMap;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setEntityPropertyResourceMap(java.util.Map)
	 */
    @Override
	public void setEntityPropertyResourceMap(Map<String, ResourceBundleEnum> entityPropertyResourceMap) {
        _entityPropertyResourceMap = entityPropertyResourceMap;
    }


    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getAcademicTitleResource()
	 */
    @Override
	public ResourceBundleEnum getAcademicTitleResource() {
        return _academicTitleResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setAcademicTitleResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setAcademicTitleResource(ResourceBundleEnum academicTitleResource) {
        _academicTitleResource = academicTitleResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSalutationResource()
	 */
    @Override
	public ResourceBundleEnum getSalutationResource() {
        return _salutationResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSalutationResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setSalutationResource(ResourceBundleEnum salutationResource) {
        _salutationResource = salutationResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getEmploymentResource()
	 */
    @Override
	public ResourceBundleEnum getEmploymentResource() {
        return _employmentResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setEmploymentResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setEmploymentResource(ResourceBundleEnum employmentResource) {
        _employmentResource = employmentResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getFamilyStatusResource()
	 */
    @Override
	public ResourceBundleEnum getFamilyStatusResource() {
        return _familyStatusResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setFamilyStatusResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setFamilyStatusResource(ResourceBundleEnum familyStatusResource) {
        _familyStatusResource = familyStatusResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getConfessionResource()
	 */
    @Override
	public ResourceBundleEnum getConfessionResource() {
        return _confessionResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setConfessionResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setConfessionResource(ResourceBundleEnum confessionResource) {
        _confessionResource = confessionResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getNumberOfChildrenResource()
	 */
    @Override
	public ResourceBundleEnum getNumberOfChildrenResource() {
        return _numberOfChildrenResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setNumberOfChildrenResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setNumberOfChildrenResource(ResourceBundleEnum numberOfChildrenResource) {
        _numberOfChildrenResource = numberOfChildrenResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getNumberOfOtherInHouseholdResource()
	 */
    @Override
	public ResourceBundleEnum getNumberOfOtherInHouseholdResource() {
        return _numberOfOtherInHouseholdResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setNumberOfOtherInHouseholdResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setNumberOfOtherInHouseholdResource(ResourceBundleEnum numberOfOtherInHouseholdResource) {
    	_numberOfOtherInHouseholdResource = numberOfOtherInHouseholdResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getNumberOfPeopleInHouseholdResource()
	 */
    @Override
	public ResourceBundleEnum getNumberOfPeopleInHouseholdResource() {
        return _numberOfPeopleInHouseholdResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setNumberOfPeopleInHouseholdResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setNumberOfPeopleInHouseholdResource(ResourceBundleEnum numberOfPeopleInHouseholdResource) {
    	_numberOfPeopleInHouseholdResource = numberOfPeopleInHouseholdResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSecurityQuestionsResource()
	 */
    @Override
	public ResourceBundleEnum getSecurityQuestionsResource() {
        return _securityQuestionsResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSecurityQuestionsResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setSecurityQuestionsResource(ResourceBundleEnum securityQuestionsResource) {
        _securityQuestionsResource = securityQuestionsResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getTransactionStatesResource()
	 */
    @Override
	public ResourceBundleEnum getTransactionStatesResource() {
        return _transactionStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setTransactionStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setTransactionStatesResource(ResourceBundleEnum transactionStatesResource) {
        _transactionStatesResource = transactionStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getTransactionTypesResource()
	 */
    @Override
	public ResourceBundleEnum getTransactionTypesResource() {
        return _transactionTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setTransactionTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setTransactionTypesResource(ResourceBundleEnum transactionTypesResource) {
        _transactionTypesResource = transactionTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSmavaGroupTypesResource()
	 */
    @Override
	public ResourceBundleEnum getSmavaGroupTypesResource() {
        return _smavaGroupTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSmavaGroupTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setSmavaGroupTypesResource(ResourceBundleEnum smavaGroupTypesResource) {
        _smavaGroupTypesResource = smavaGroupTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSmavaGroupStatesResource()
	 */
    @Override
	public ResourceBundleEnum getSmavaGroupStatesResource() {
        return _smavaGroupStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSmavaGroupStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setSmavaGroupStatesResource(ResourceBundleEnum smavaGroupStatesResource) {
        _smavaGroupStatesResource = smavaGroupStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getBookingTypesResource()
	 */
    @Override
	public ResourceBundleEnum getBookingTypesResource() {
        return _bookingTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setBookingTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setBookingTypesResource(ResourceBundleEnum bookingTypesResource) {
        _bookingTypesResource = bookingTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDurationOfOccupationResource()
	 */
    @Override
	public ResourceBundleEnum getDurationOfOccupationResource() {
        return _durationOfOccupationResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDurationOfOccupationResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDurationOfOccupationResource(ResourceBundleEnum durationOfOccupationResource) {
        _durationOfOccupationResource = durationOfOccupationResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getExpensesCarResource()
	 */
    @Override
	public ResourceBundleEnum getExpensesCarResource() {
        return _expensesCarResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setExpensesCarResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setExpensesCarResource(ResourceBundleEnum expensesCarResource) {
        _expensesCarResource = expensesCarResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getModeOfAccommodationResource()
	 */
    @Override
	public ResourceBundleEnum getModeOfAccommodationResource() {
        return _modeOfAccommodationResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setModeOfAccommodationResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setModeOfAccommodationResource(ResourceBundleEnum modeOfAccommodationResource) {
        _modeOfAccommodationResource = modeOfAccommodationResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getNumOfCarsResource()
	 */
    @Override
	public ResourceBundleEnum getNumOfCarsResource() {
        return _numOfCarsResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setNumOfCarsResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setNumOfCarsResource(ResourceBundleEnum numOfCarsResource) {
        _numOfCarsResource = numOfCarsResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getUserStatesResource()
	 */
    @Override
	public ResourceBundleEnum getUserStatesResource() {
        return _userStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setUserStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setUserStatesResource(ResourceBundleEnum userStatesResource) {
        _userStatesResource = userStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getIntendedUseResource()
	 */
    @Override
	public ResourceBundleEnum getIntendedUseResource() {
        return _intendedUseResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setIntendedUseResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setIntendedUseResource(ResourceBundleEnum intendedUseResource) {
        _intendedUseResource = intendedUseResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getTypeResource()
	 */
    @Override
	public ResourceBundleEnum getTypeResource() {
        return _documentTypeResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setTypeResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setTypeResource(ResourceBundleEnum typeResource) {
        _documentTypeResource = typeResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getTypeOfDispatchResource()
	 */
    @Override
	public ResourceBundleEnum getTypeOfDispatchResource() {
        return _documentTypeOfDispatchResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setTypeOfDispatchResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setTypeOfDispatchResource(ResourceBundleEnum typeOfDispatchResource) {
        _documentTypeOfDispatchResource = typeOfDispatchResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDirectionResource()
	 */
    @Override
	public ResourceBundleEnum getDirectionResource() {
        return _documentDirectionResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDirectionResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDirectionResource(ResourceBundleEnum directionResource) {
        _documentDirectionResource = directionResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentTypeResource()
	 */
    @Override
	public ResourceBundleEnum getDocumentTypeResource() {
        return _documentTypeResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentTypeResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDocumentTypeResource(ResourceBundleEnum documentTypeResource) {
        _documentTypeResource = documentTypeResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentTypeOfDispatchResource()
	 */
    @Override
	public ResourceBundleEnum getDocumentTypeOfDispatchResource() {
        return _documentTypeOfDispatchResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentTypeOfDispatchResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDocumentTypeOfDispatchResource(ResourceBundleEnum documentTypeOfDispatchResource) {
        _documentTypeOfDispatchResource = documentTypeOfDispatchResource;
    }
    

    @Override
	public ResourceBundleEnum getDocumentTypeOfProviderResource() {
        return _documentTypeOfProviderResource;
    }


    @Override
	public void setDocumentTypeOfProviderResource(ResourceBundleEnum documentTypeOfDispatchResource) {
        _documentTypeOfProviderResource = documentTypeOfDispatchResource;
    }
    


    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentDirectionResource()
	 */
    @Override
	public ResourceBundleEnum getDocumentDirectionResource() {
        return _documentDirectionResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentDirectionResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDocumentDirectionResource(ResourceBundleEnum documentDirectionResource) {
        _documentDirectionResource = documentDirectionResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSmavaGroupMemberStatesResource()
	 */
    @Override
	public ResourceBundleEnum getSmavaGroupMemberStatesResource() {
        return _smavaGroupMemberStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSmavaGroupMemberStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setSmavaGroupMemberStatesResource(ResourceBundleEnum smavaGroupMemberStatesResource) {
        _smavaGroupMemberStatesResource = smavaGroupMemberStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getBidStatesResource()
	 */
    @Override
	public ResourceBundleEnum getBidStatesResource() {
        return _bidStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setBidStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setBidStatesResource(ResourceBundleEnum bidStatesResource) {
        _bidStatesResource = bidStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDtausFileStatesResource()
	 */
    @Override
	public ResourceBundleEnum getDtausFileStatesResource() {
        return _dtausFileStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDtausFileStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDtausFileStatesResource(ResourceBundleEnum dtausFileStatesResource) {
        _dtausFileStatesResource = dtausFileStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDtausEntryTypesResource()
	 */
    @Override
	public ResourceBundleEnum getDtausEntryTypesResource() {
        return _dtausEntryTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDtausEntryTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDtausEntryTypesResource(ResourceBundleEnum dtausEntryTypesResource) {
        _dtausEntryTypesResource = dtausEntryTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDtausEntryStatesResource()
	 */
    @Override
	public ResourceBundleEnum getDtausEntryStatesResource() {
        return _dtausEntryStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDtausEntryStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDtausEntryStatesResource(ResourceBundleEnum dtausEntryStatesResource) {
        _dtausEntryStatesResource = dtausEntryStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getBookingGroupTypesResource()
	 */
    @Override
	public ResourceBundleEnum getBookingGroupTypesResource() {
        return _bookingGroupTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setBookingGroupTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setBookingGroupTypesResource(ResourceBundleEnum bookingGroupTypesResource) {
        _bookingGroupTypesResource = bookingGroupTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getContractStatesResource()
	 */
    @Override
	public ResourceBundleEnum getContractStatesResource() {
        return _contractStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setContractStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setContractStatesResource(ResourceBundleEnum contractStatesResource) {
        _contractStatesResource = contractStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentStateResource()
	 */
    @Override
	public ResourceBundleEnum getDocumentStateResource() {
        return _documentStateResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentStateResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDocumentStateResource(ResourceBundleEnum documentStateResource) {
        _documentStateResource = documentStateResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDealTypesResource()
	 */
    @Override
	public ResourceBundleEnum getDealTypesResource() {
        return _dealTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDealTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDealTypesResource(ResourceBundleEnum dealTypesResource) {
        _dealTypesResource = dealTypesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSmavaGroupInterestingResource()
	 */
    @Override
	public ResourceBundleEnum getSmavaGroupInterestingResource() {
        return _smavaGroupInterestingResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSmavaGroupInterestingResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setSmavaGroupInterestingResource(ResourceBundleEnum smavaGroupInterestingResource) {
        _smavaGroupInterestingResource = smavaGroupInterestingResource;
    }

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getEntaxStatesResource()
	 */
	@Override
	public ResourceBundleEnum getEntaxStatesResource() {
		return _entaxStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setEntaxStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setEntaxStatesResource(ResourceBundleEnum entaxStatesResource) {
		_entaxStatesResource = entaxStatesResource;
	}

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getBookingPerformanceStatesResource()
	 */
    @Override
	public ResourceBundleEnum getBookingPerformanceStatesResource() {
        return _bookingPerformanceStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setBookingPerformanceStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setBookingPerformanceStatesResource(ResourceBundleEnum bookingPerformanceStatesResource) {
        this._bookingPerformanceStatesResource = bookingPerformanceStatesResource;
    }

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getIsoCitizenshipResource()
	 */
	@Override
	public ResourceBundleEnum getIsoCitizenshipResource() {
		return _isoCitizenshipResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setIsoCitizenshipResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setIsoCitizenshipResource(ResourceBundleEnum isoCitizenshipResource) {
		_isoCitizenshipResource = isoCitizenshipResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getIsoCountryResource()
	 */
	@Override
	public ResourceBundleEnum getIsoCountryResource() {
		return _isoCountryResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setIsoCountryResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setIsoCountryResource(ResourceBundleEnum isoCountryResource) {
		_isoCountryResource = isoCountryResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiContractTypesResource()
	 */
	@Override
	public ResourceBundleEnum getRdiContractTypesResource() {
		return _rdiContractTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiContractTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiContractTypesResource(ResourceBundleEnum rdiContractTypesResource) {
		_rdiContractTypesResource = rdiContractTypesResource;
	}
	
	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiContractTypesShortResource()
	 */
	@Override
	public ResourceBundleEnum getRdiContractTypesShortResource() {
		return _rdiContractTypesShortResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiContractTypesShortResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiContractTypesShortResource(
			ResourceBundleEnum rdiContractTypesShortResource) {
		_rdiContractTypesShortResource = rdiContractTypesShortResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiContractStatesResource()
	 */
	@Override
	public ResourceBundleEnum getRdiContractStatesResource() {
		return _rdiContractStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiContractStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiContractStatesResource(
			ResourceBundleEnum rdiContractStatesResource) {
		_rdiContractStatesResource = rdiContractStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiPaymentStatesResource()
	 */
	@Override
	public ResourceBundleEnum getRdiPaymentStatesResource() {
		return _rdiPaymentStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiPaymentStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiPaymentStatesResource(
			ResourceBundleEnum rdiPaymentStatesResource) {
		_rdiPaymentStatesResource = rdiPaymentStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiPaymentTypesResource()
	 */
	@Override
	public ResourceBundleEnum getRdiPaymentTypesResource() {
		return _rdiPaymentTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiPaymentTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiPaymentTypesResource(
			ResourceBundleEnum rdiPaymentTypesResource) {
		_rdiPaymentTypesResource = rdiPaymentTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiRequestStatesResource()
	 */
	@Override
	public ResourceBundleEnum getRdiRequestStatesResource() {
		return _rdiRequestStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiRequestStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiRequestStatesResource(
			ResourceBundleEnum rdiRequestStatesResource) {
		_rdiRequestStatesResource = rdiRequestStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiResponseStatesResource()
	 */
	@Override
	public ResourceBundleEnum getRdiResponseStatesResource() {
		return _rdiResponseStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiResponseStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiResponseStatesResource(
			ResourceBundleEnum rdiResponseStatesResource) {
		_rdiResponseStatesResource = rdiResponseStatesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getCustomerBookingTypesResource()
	 */
	@Override
	public ResourceBundleEnum getCustomerBookingTypesResource() {
		return _customerBookingTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setCustomerBookingTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setCustomerBookingTypesResource(ResourceBundleEnum customerBookingTypesResource) {
		_customerBookingTypesResource = customerBookingTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentApprovalStateResource()
	 */
	@Override
	public ResourceBundleEnum getDocumentApprovalStateResource() {
		return _documentApprovalStateResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentApprovalStateResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setDocumentApprovalStateResource(ResourceBundleEnum documentApprovalStateResource) {
		_documentApprovalStateResource = documentApprovalStateResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getTypeOfCompanyResource()
	 */
	@Override
	public ResourceBundleEnum getTypeOfCompanyResource() {
		return _typeOfCompanyResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setTypeOfCompanyResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setTypeOfCompanyResource(ResourceBundleEnum typeOfCompanyResource) {
		_typeOfCompanyResource = typeOfCompanyResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getEducationEarnedResource()
	 */
	@Override
	public ResourceBundleEnum getEducationEarnedResource() {
		return _educationEarnedResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setEducationEarnedResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setEducationEarnedResource(ResourceBundleEnum educationEarnedResource) {
		_educationEarnedResource = educationEarnedResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentContainerNamesResource()
	 */
	@Override
	public ResourceBundleEnum getDocumentContainerNamesResource() {
		return _documentContainerNamesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentContainerNamesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setDocumentContainerNamesResource(ResourceBundleEnum documentContainerNamesResource) {
		_documentContainerNamesResource = documentContainerNamesResource;
	}

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getDocumentAttachmentContainerStatesResource()
	 */
    @Override
	public ResourceBundleEnum getDocumentAttachmentContainerStatesResource() {
        return _documentAttachmentContainerStatesResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setDocumentAttachmentContainerStatesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setDocumentAttachmentContainerStatesResource(ResourceBundleEnum documentAttachmentContainerStateResource) {
        this._documentAttachmentContainerStatesResource = documentAttachmentContainerStateResource;
    }

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getCreditProjectSorterResource()
	 */
	@Override
	public ResourceBundleEnum getCreditProjectSorterResource() {
		return _creditProjectSorterResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setCreditProjectSorterResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setCreditProjectSorterResource(
			ResourceBundleEnum creditProjectSorterResource) {
		_creditProjectSorterResource = creditProjectSorterResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getSharedLoanResource()
	 */
	@Override
	public ResourceBundleEnum getSharedLoanResource() {
		return _sharedLoanResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setSharedLoanResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setSharedLoanResource(ResourceBundleEnum sharedLoanResource) {
		_sharedLoanResource = sharedLoanResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setKindOfUserResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setKindOfCreditResource(ResourceBundleEnum kindOfCreditResource) {
		_kindOfCreditResource = kindOfCreditResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getKindOfUserResource()
	 */
	@Override
	public ResourceBundleEnum getKindOfCreditResource() {
		return _kindOfCreditResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setKindOfUserResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setKindOfUserResource(ResourceBundleEnum kindOfUserResource) {
		_kindOfUserResource = kindOfUserResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getKindOfUserResource()
	 */
	@Override
	public ResourceBundleEnum getKindOfUserResource() {
		return _kindOfUserResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getMiscEarnTypeResource()
	 */
	@Override
	public ResourceBundleEnum getMiscEarnTypeResource() {
		return _miscEarnTypeResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setMiscEarnTypeResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setMiscEarnTypeResource(ResourceBundleEnum miscEarnTypesResource) {
		_miscEarnTypeResource = miscEarnTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getCreditCardTypesResource()
	 */
	@Override
	public ResourceBundleEnum getCreditCardTypesResource() {
		return _creditCardTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setCreditCardTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setCreditCardTypesResource(
			ResourceBundleEnum creditCardTypesResource) {
		_creditCardTypesResource = creditCardTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getPropertyOwnedTypesResource()
	 */
	@Override
	public ResourceBundleEnum getPropertyOwnedTypesResource() {
		return _propertyOwnedTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setPropertyOwnedTypesResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setPropertyOwnedTypesResource(
			ResourceBundleEnum propertyOwnedTypesResource) {
		_propertyOwnedTypesResource = propertyOwnedTypesResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getRdiContractTypesBrokerageResource()
	 */
	@Override
	public ResourceBundleEnum getRdiContractTypesBrokerageResource() {
		return _rdiContractTypesBrokerageResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setRdiContractTypesBrokerageResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setRdiContractTypesBrokerageResource(
			ResourceBundleEnum rdiContractTypesBrokerageResource) {
		_rdiContractTypesBrokerageResource = rdiContractTypesBrokerageResource;
	}

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#getPayoutModeResource()
	 */
    @Override
	public ResourceBundleEnum getPayoutModeResource() {
        return _payoutModeResource;
    }

    /* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setPayoutModeResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
    @Override
	public void setPayoutModeResource(ResourceBundleEnum payoutModeResource) {
        _payoutModeResource = payoutModeResource;
    }

    /* (non-Javadoc)
     * @see de.smava.webapp.commons.domainutil.IResourceProvider#getAdvisoryFailedResource()
     */
    @Override
    public ResourceBundleEnum getAdvisoryFailedResource() {
    	return _advisoryFailedResource;
    }
    
    /* (non-Javadoc)
     * @see de.smava.webapp.commons.domainutil.IResourceProvider#setAdvisoryFailedResource(de.smava.webapp.commons.util.ResourceBundleEnum)
     */
    @Override
    public void setAdvisoryFailedResource(ResourceBundleEnum advisoryFailedResource) {
    	_advisoryFailedResource = advisoryFailedResource;
    }
    
    /* (non-Javadoc)
     * @see de.smava.webapp.commons.domainutil.IResourceProvider#getCaJournalSubjectResource()
     */
    @Override
    public ResourceBundleEnum getCaJournalSubjectResource() {
    	return _caJournalSubjectResource;
    }
    
    /* (non-Javadoc)
     * @see de.smava.webapp.commons.domainutil.IResourceProvider#setCaJournalSubjectResource(de.smava.webapp.commons.util.ResourceBundleEnum)
     */
    @Override
    public void setCaJournalSubjectResource(ResourceBundleEnum caJournalSubjectResource) {
    	_caJournalSubjectResource = caJournalSubjectResource;
    }

    /* (non-Javadoc)
     * @see de.smava.webapp.commons.domainutil.IResourceProvider#getVehicleTypeResource()
     */
    @Override
	public ResourceBundleEnum getVehicleTypeResource() {
		return _vehicleTypeResource;
	}

	/* (non-Javadoc)
	 * @see de.smava.webapp.commons.domainutil.IResourceProvider#setVehicleTypeResource(de.smava.webapp.commons.util.ResourceBundleEnum)
	 */
	@Override
	public void setVehicleTypeResource(ResourceBundleEnum vehicleTypeResource) {
		_vehicleTypeResource = vehicleTypeResource;
	}

    @Override
    public ResourceBundleEnum getSmsResource() {
        return smsResource;
    }
    @Override
    public void setSmsResource(ResourceBundleEnum smsResource) {
        this.smsResource = smsResource;
    }

	@Override
	public ResourceBundleEnum getSelfemployedProfessionResource() {
		return _selfemployedProfession;
	}

	@Override
	public void setSelfemployedProfessionResource(ResourceBundleEnum selfemployedProfession) {
		_selfemployedProfession = selfemployedProfession;
	}

    @Override
    public ResourceBundleEnum getEstateTypeResource() {
        return estateTypeResource;
    }

    @Override
    public void setEstateTypeResource(ResourceBundleEnum estateTypeResource) {
        this.estateTypeResource = estateTypeResource;
    }

    @Override
    public ResourceBundleEnum getChildBenefitResource() {
        return childBenefitResource;
    }

    @Override
    public void setChildBenefitResource(ResourceBundleEnum childBenefitResource) {
        this.childBenefitResource = childBenefitResource;
    }

    @Override
    public ResourceBundleEnum getNumOfBikesResource() {
        return numOfBikesResource;
    }

    @Override
    public void setNumOfBikesResource(ResourceBundleEnum numOfBikesResource) {
        this.numOfBikesResource = numOfBikesResource;
    }

    public ResourceBundleEnum getLivingConditionResource() {
        return livingConditionResource;
    }

    @Override
    public void setLivingConditionResource(ResourceBundleEnum livingConditionResource) {
        this.livingConditionResource = livingConditionResource;
    }

    @Override
    public ResourceBundleEnum getSalariedStaffTypeResource() {
        return salariedStaffTypeResource;
    }

    @Override
    public void setSalariedStaffTypeResource(ResourceBundleEnum salariedStaffTypeResource) {
        this.salariedStaffTypeResource = salariedStaffTypeResource;
    }

    @Override
    public ResourceBundleEnum getSalariedStaffSoldierResource() {
        return salariedStaffSoldierResource;
    }

    @Override
    public void setSalariedStaffSoldierResource(ResourceBundleEnum salariedStaffSoldierResource) {
        this.salariedStaffSoldierResource = salariedStaffSoldierResource;
    }

    @Override
    public ResourceBundleEnum getLettedEstateResource() {
        return lettedEstateResource;
    }

    @Override
    public void setLettedEstateResource(ResourceBundleEnum lettedEstateResource) {
        this.lettedEstateResource = lettedEstateResource;
    }

    @Override
    public ResourceBundleEnum getSalariedStaffCivilCervantResource() {
        return salariedStaffCivilCervantResource;
    }

    @Override
    public void setSalariedStaffCivilCervantResource(ResourceBundleEnum salariedStaffCivilCervantResource) {
        this.salariedStaffCivilCervantResource = salariedStaffCivilCervantResource;
    }

    @Override
    public ResourceBundleEnum getGenderResource() {
        return genderResource;
    }

    @Override
    public void setGenderResource(ResourceBundleEnum genderResource) {
        this.genderResource = genderResource;
    }

    @Override
    public ResourceBundleEnum getCasiAncillaryEmploymentTypeResource() {
        return casiAncillaryEmploymentType;
    }

    @Override
    public void setCasiAncillaryEmploymentTypeResource(ResourceBundleEnum casiAncillaryEmploymentType) {
        this.casiAncillaryEmploymentType = casiAncillaryEmploymentType;
    }

    @Override
    public ResourceBundleEnum getCasiEmploymentTypeResource() {
        return casiEmploymentType;
    }

    @Override
    public void setCasiEmploymentTypeResource(ResourceBundleEnum casiEmploymentType) {
        this.casiEmploymentType = casiEmploymentType;
    }

    @Override
    public void setCasiSalariedStaffTypeResource(ResourceBundleEnum casiSalariedStaffType) {
        this.casiSalariedStaffType = casiSalariedStaffType;
    }

    @Override
    public ResourceBundleEnum getCasiSalariedStaffTypeResource() {
        return casiSalariedStaffType;
    }

    @Override
    public void setCasiOfficialServiceGradeTypeResource(ResourceBundleEnum casiOfficalServiceGradeType) {
        this.casiOfficalServiceGradeType = casiOfficalServiceGradeType;
    }

    @Override
    public ResourceBundleEnum getCasiOfficialServiceGradeTypeResource() {
        return casiOfficalServiceGradeType;
    }

    @Override
    public void setCasiIndustrySectorTypeResource(ResourceBundleEnum casiIndustrySectorType) {
        this.casiIndustrySectorType = casiIndustrySectorType;
    }

    @Override
    public ResourceBundleEnum getCasiIndustrySectorTypeResource() {
        return casiIndustrySectorType;
    }

    @Override
    public void setCasiFreelancerOccupationTypeResource(ResourceBundleEnum casiFreelancerOccupationType) {
        this.casiFreelancerOccupationType = casiFreelancerOccupationType;
    }

    @Override
    public ResourceBundleEnum getCasiFreelancerOccupationTypeResource() {
        return casiFreelancerOccupationType;
    }

    @Override
    public ResourceBundleEnum getCasiLoanBorrowerTypeResource() {
        return casiLoanBorrowerType;
    }

    @Override
    public void setCasiLoanBorrowerTypeResource(ResourceBundleEnum casiLoanBorrowerType) {
        this.casiLoanBorrowerType = casiLoanBorrowerType;
    }

    @Override
    public ResourceBundleEnum getCasiThirdPartyLoanTypeResource() {
        return casiThirdPartyLoanType;
    }

    @Override
    public void setCasiThirdPartyLoanTypeResource(ResourceBundleEnum casiThirdPartyLoanType) {
        this.casiThirdPartyLoanType = casiThirdPartyLoanType;
    }

    @Override
    public ResourceBundleEnum getCasidebtCreditsCardTypeResource() {
        return casidebtCreditsCardType;
    }

    @Override
    public void setCasidebtCreditsCardTypeResource(ResourceBundleEnum casidebtCreditsCardType) {
        this.casidebtCreditsCardType = casidebtCreditsCardType;
    }

    @Override
    public ResourceBundleEnum getCasiApplicantsRelationshipTypeResource() {
        return casiApplicantsRelationshipType;
    }

    @Override
    public void setCasiApplicantsRelationshipTypeResource(ResourceBundleEnum casiApplicantsRelationshipType) {
        this.casiApplicantsRelationshipType = casiApplicantsRelationshipType;
    }

    @Override
    public ResourceBundleEnum getCasiAccomodationTypeResource() {
        return casiAccommodationType;
    }

    @Override
    public void setCasiAccommodationTypeResource(ResourceBundleEnum casiAccommodationType) {
        this.casiAccommodationType = casiAccommodationType;
    }

    @Override
    public ResourceBundleEnum getCasiEstateTypeResource() {
        return casiEstateType;
    }

    @Override
    public void setCasiEstateTypeResource(ResourceBundleEnum casiEstateType) {
        this.casiEstateType = casiEstateType;
    }

    @Override
    public ResourceBundleEnum getCasiOtherPensionIncomeTypeResource() {
        return casiOtherPensionIncomeType;
    }

    @Override
    public void setCasiOtherPensionIncomeTypeResource(ResourceBundleEnum casiOtherPensionIncomeType) {
        this.casiOtherPensionIncomeType = casiOtherPensionIncomeType;
    }

    @Override
    public ResourceBundleEnum getCasiOtherRevenueTypeResource() {
        return casiOtherRevenueType;
    }

    @Override
    public void setCasiOtherRevenueTypeResource(ResourceBundleEnum casiOtherRevenueType) {
        this.casiOtherRevenueType = casiOtherRevenueType;
    }

    @Override
    public ResourceBundleEnum getCasiMaritalStateTypeResource() {
        return casiMaritalStateType;
    }

    @Override
    public void setCasiMaritalStateTypeResource(ResourceBundleEnum casiMaritalStateType) {
        this.casiMaritalStateType = casiMaritalStateType;
    }

    @Override
    public ResourceBundleEnum getCasiOtherExpenseTypeResource() {
        return casiOtherExpenseType;
    }

    @Override
    public void setCasiOtherExpenseTypeResource(ResourceBundleEnum casiOtherExpenseType) {
        this.casiOtherExpenseType = casiOtherExpenseType;
    }

    @Override
    public ResourceBundleEnum getPartTimeJobTypeResource() {
        return partTimeJobType;
    }

    @Override
    public void setPartTimeJobTypeResource(ResourceBundleEnum partTimeJobType) {
        this.partTimeJobType = partTimeJobType;
    }

    @Override
    public ResourceBundleEnum getBorrowerRelationshipTypeResource() {
        return borrowerRelationshipTypeResource;
    }

    @Override
    public void setBorrowerRelationshipTypeResource(ResourceBundleEnum borrowerRelationshipTypeResource) {
        this.borrowerRelationshipTypeResource = borrowerRelationshipTypeResource;
    }

    @Override
    public ResourceBundleEnum getAdditionalIncomeTypeResource() {
        return additionalIncomeTypeResource;
    }

    @Override
    public void setAdditionalIncomeTypeResource(ResourceBundleEnum additionalIncomeTypeResource) {
        this.additionalIncomeTypeResource = additionalIncomeTypeResource;
    }

    @Override
    public ResourceBundleEnum getPensionSupplementTypeResource() {
        return pensionSupplementTypeResource;
    }

    @Override
    public void setPensionSupplementTypeResource(ResourceBundleEnum pensionSupplementTypeResource) {
        this.pensionSupplementTypeResource = pensionSupplementTypeResource;
    }

    @Override
    public ResourceBundleEnum getPensionTypeResource() {
        return pensionTypeResource;
    }

    @Override
    public void setPensionTypeResource(ResourceBundleEnum pensionTypeResource) {
        this.pensionTypeResource = pensionTypeResource;
    }

    @Override
    public ResourceBundleEnum getSelfEmployedTypeResource() {
        return selfEmployedType;
    }

    @Override
    public void setSelfEmployedTypeResource(ResourceBundleEnum selfEmployedTypeResource) {
        this.selfEmployedType = selfEmployedTypeResource;
    }
}