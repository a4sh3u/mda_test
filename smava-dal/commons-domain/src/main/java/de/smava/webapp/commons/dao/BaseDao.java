package de.smava.webapp.commons.dao;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * A generified dao interface.
 * <p/>
 * Date: 14.08.2006 16:55:31
 *
 * @param <T> The entity class for which the dao is working
 *
 * @author joern.stampehl
 */
public interface BaseDao<T extends BaseEntity> {
    /**
     * Returns an attached copy of the entity identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    T load(Long id);

    /**
     * Saves the entity.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    Long save(T message);
    
    void forceFlush();
    
    boolean exists(Long id);

    String createQueryTerm(Collection elements, Map valuesMap, List parameters, String concat);

    /**
     * Gets the highest given identifier in the table managed clazz.
     *
     * @param clazz
     * @return identifier or null if no entity was present in table
     */
    Long getMaxId(Class clazz);
}