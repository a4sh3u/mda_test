package de.smava.webapp.commons.dao.jdo;

import de.smava.webapp.commons.domain.BaseEntity;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.MappableEnum;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.commons.web.ListFilterElement;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.datanucleus.exceptions.NucleusObjectNotFoundException;
import org.springframework.orm.jdo.PersistenceManagerFactoryUtils;
import org.springframework.util.Assert;

import javax.jdo.*;
import javax.transaction.Synchronization;
import java.util.*;
import java.util.Map.Entry;

/**
 * @author Daniel Doubleday
 */
public abstract class JdoBaseDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBaseDao.class);

    private PersistenceManagerFactory persistenceManagerFactory;

    public void setSynchronization(Synchronization synchronization) {
        Transaction transaction = getPersistenceManager().currentTransaction();
        if (transaction.isActive()) {
            transaction.setSynchronization(synchronization);
        }
        else {
            throw new IllegalStateException("Transaction is not active");
        }
    }

    public void forceFlush() {
        getPersistenceManager().flush();
    }

    public void evictAll() {
        getPersistenceManager().evictAll();
    }

    public Long saveEntity(BaseEntity entity) {
        //entity.isNew checks, if there is an id available
        if (entity.isNew()) {
            //JDOHelper checks, if this instance already has been made persistent
            //this subsequent check is necessary, because after the first "makepersistent" call the entity will be marked as new for JDO,
            //but not id is assigned, so entity.isNew() will return true wich would lead to more "makepersistent" calls
            if (!JDOHelper.isNew(entity)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Make entity persistent: " + entity);
                }
                getPersistenceManager().makePersistent(entity);
            }
        }
        else {
            if (JDOHelper.isDetached(entity)) {
                throw new IllegalStateException("Detaching entites is not supported!");
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Flush entity: " + entity);
            }
            if (!JDOHelper.isPersistent(entity)) {
                throw new IllegalStateException("Cannot save a non persistent object.");
            }
        }
        return entity.getId();
    }

    public <T extends Entity> void deleteEntity(final Class<T> clazz, final Long id) {
        final PersistenceManager pm = getPersistenceManager();
        Entity entity = (Entity) pm.getObjectById(clazz, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Deleting entity: " + entity);
        }

        pm.deletePersistent(entity);
        pm.evict(entity);
        pm.evictAll();
    }

    public <T extends Entity> T getEntity(final Class<T> clazz, final Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEntity: " + clazz + " id=" + id);
        }
        // use persistence manager directly to avoid unnecessary reflection magic in spring
        T entity = getPersistenceManager().getObjectById(clazz, id);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Loaded entity: " + entity);
        }
        return entity;
    }

    /**
     * Retrieves all 'clazz' instances and returns them attached to
     * the current persistence manager.
     */
    public <T extends Entity> Collection<T> getEntities(Class<T> clazz) {
        return (Collection<T>) getPersistenceManager().newQuery(clazz).execute();
    }

    /**
     * Retrieves a page of 'clazz' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public <T extends Entity> Collection<T> getEntities(Class<T> clazz, Pageable pageable) {
        return getEntities(clazz, pageable, null);
    }

    /**
     * Retrieves a sorted collection of 'class' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public <T extends Entity> Collection<T> getEntities(Class<T> clazz, Sortable sortable) {
        return getEntities(clazz, null, sortable);
    }

    /**
     * Retrieves a sorted page of 'clazz' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see de.smava.webapp.commons.pagination.Pageable
     * @see de.smava.webapp.commons.pagination.Sortable
     */
    public <T extends Entity> Collection<T> getEntities(Class<T> clazz, Pageable pageable, Sortable sortable) {
        return findEntities(clazz, null, pageable, sortable);
    }

    /**
     * Retrieves a collection of 'clazz' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause) {
        return findEntities(clazz, whereClause, (Pageable) null, (Sortable) null);
    }

    /**
     * Retrieves a page of 'clazz' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause, Pageable pageable) {
        return findEntities(clazz, whereClause, pageable, null);
    }

    /**
     * Retrieves a sorted collection of 'clazz' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause, Sortable sortable) {
        return findEntities(clazz, whereClause, (Pageable) null, sortable);
    }

    /**
     * Retrieves a sorted page of 'clazz' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause, Pageable pageable, Sortable sortable) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));

        // evaluate Pageable information
        if (pageable != null) {
            long start = pageable.getOffset();
            long end = start + pageable.getItemsPerPage();
            query.setRange(start, end);
        }

        // evaluate Sortable information if present
        if (sortable != null && sortable.hasSort()) {
            StringBuilder sortStringBuilder = new StringBuilder(sortable.getSort());
            sortStringBuilder.append(" ");
            if (sortable.sortDescending()) {
                sortStringBuilder.append("descending");
            }
            else {
                sortStringBuilder.append("ascending");
            }
            query.setOrdering(sortStringBuilder.toString());
        }

        // execute query
        query.setResult("distinct this");
        return (List<T>) query.execute();
    }

    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause, Pageable pageable, Sortable sortable, Object[] params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));

        // evaluate Pageable information
        if (pageable != null) {
            long start = pageable.getOffset();
            long end = start + pageable.getItemsPerPage();
            query.setRange(start, end);
        }

        // evaluate Sortable information if present
        if (sortable != null && sortable.hasSort()) {
            StringBuilder sortStringBuilder = new StringBuilder(sortable.getSort());
            sortStringBuilder.append(" ");
            if (sortable.sortDescending()) {
                sortStringBuilder.append("descending");
            }
            else {
                sortStringBuilder.append("ascending");
            }
            query.setOrdering(sortStringBuilder.toString());
        }

        // execute query
        query.setResult("distinct this");
        return (List<T>) query.executeWithArray(params);
    }

    /**
     * Retrieves a sorted page of 'clazz' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause, Object... params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));
        // execute query
        query.setResult("distinct this");
        return (List<T>) query.executeWithArray(params);
    }

    public <T extends Entity> Collection<T> findEntities(Class<T> clazz, String whereClause, Sortable sortable, Object... params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));

        // evaluate Sortable information if present
        if (sortable != null && sortable.hasSort()) {
            StringBuilder sortStringBuilder = new StringBuilder(sortable.getSort());
            sortStringBuilder.append(" ");
            if (sortable.sortDescending()) {
                sortStringBuilder.append("descending");
            }
            else {
                sortStringBuilder.append("ascending");
            }
            query.setOrdering(sortStringBuilder.toString());
        }

        // execute query
        query.setResult("distinct this");
        return (List<T>) query.executeWithArray(params);
    }

    /**
     * Returns the number of 'clazz' entities.
     */
    public <T extends Entity> long getEntityCount(Class<T> clazz) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(clazz);
        return getEntityCount(query);
    }

    /**
     * Returns the number of 'clazz' entities which match the given whereClause.
     */
    public <T extends Entity> long getEntityCount(Class<T> clazz, String whereClause) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));
        return getEntityCount(query);
    }

    /**
     * Returns the number of 'clazz' entities which match the given whereClause.
     */
    public <T extends Entity> long getEntityCount(Class<T> clazz, String whereClause, Object... params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));

        query.setResult("count(this)");
        return (Long) query.executeWithArray(params);
    }

    /**
     * Returns the number of entities which are returned by the given query.
     */
    private long getEntityCount(Query query) {
        query.setResult("count(this)");
        return (Long) query.execute();
    }

    /**
     * Helper to construct a query string. The whereClause can be blank.
     */
    protected <T extends Entity> String createQueryString(Class<T> clazz, String whereClause) {
        StringBuilder builder = new StringBuilder("select from ").append(clazz.getName());
        if (StringUtils.isNotBlank(whereClause)) {
            builder.append(" where ").append(whereClause);
        }
        return builder.toString();
    }

    public <T extends Entity> T findUniqueEntity(Class<T> clazz, String whereClause) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(clazz, whereClause));
        query.setUnique(true);
        return (T) query.execute();
    }

    public <T extends Entity> boolean exists(Class<T> clazz, Long id) {
        boolean result = false;
        if (id != null) {
            T entity = findUniqueEntity(clazz, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    public <T extends Entity> T getById(Class<T> clazz, Long id) {
        final PersistenceManager pm = getPersistenceManager();
        final Query query = pm.newQuery(createQueryString(clazz, "_id == :id"));
        query.setUnique(true);
        return (T) query.executeWithMap(Collections.singletonMap("id", id));
    }

    public <T extends Entity> Object getAggregateByNamedQuery(final Class<T> clazz, final String queryName, final Map<String, ?> params, final String aggregate, final String ordering) {
        Query query = getPersistenceManager().newNamedQuery(clazz, queryName);
        if (StringUtils.isNotBlank(aggregate)) {
            query.setResult(aggregate);
        }
        if (StringUtils.isNotBlank(ordering)) {
            query.setOrdering(ordering);
        }
        return query.executeWithMap(params);
    }

    public <T extends Entity> Collection<T> findByNamedQuery(Class<T> clazz, String queryName, Map<String, ?> params) {
        return (Collection<T>) getAggregateByNamedQuery(clazz, queryName, params, null, null);
    }

    public Long getMaxId(Class clazz) {
        final Query query = getPersistenceManager().newQuery("select max(_id) from " + clazz.getName());
        Long l = (Long) query.execute();
        return l;
    }

    /**
     * @param <I>                type of result class
     * @param sql                - sql to execute WITHOUT order, offset and limit part at the end!
     * @param resultClass        - rasult class object
     * @param allowedSortColumns - set of allowed column names to check
     * @param sortColumn         - the column to sort, will be checked against <code>allowedSortColumns</code>
     * @param ascending          - if true sort will be ascending, otherwise descending
     * @param params             - parameters to use in the sql. IMPORTANT: the last tow parameters have to be numbers for offset and pagecount!
     * @return list of I as returned by sql
     */
    protected <I> List<I> getListBySinglePagableQuery(final String sql, final Class<I> resultClass, final Set<String> allowedSortColumns, final String sortColumn, final boolean ascending, final Object... params) {
        Assert.hasText(sql);
        Assert.notNull(resultClass);
        Assert.notEmpty(allowedSortColumns);
        if (!allowedSortColumns.contains(sortColumn)) {
            StringBuilder msg = new StringBuilder("illegal sort column '");
            msg.append(sortColumn);
            msg.append("'. allowed columns are [");
            for (String string : allowedSortColumns) {
                msg.append(string);
                msg.append(",");
            }
            msg.deleteCharAt(msg.length() - 1);
            msg.append("]");
            throw new IllegalArgumentException(msg.toString());
        }
        final Query q = getPersistenceManager().newQuery(Query.SQL, appendOrderAndOffset(sql, sortColumn, ascending));
        q.setResultClass(resultClass);
        return (List<I>) q.executeWithArray(params);
    }

    private String appendOrderAndOffset(final String sql, final String sortColumn, final boolean ascending) {
        final StringBuilder sb = new StringBuilder(sql);
        sb.append(" ORDER BY \"");
        sb.append(sortColumn);
        sb.append("\" ");
        sb.append(BooleanUtils.toString(ascending, "asc", "desc"));
        sb.append(", \"entityId\" asc OFFSET ? LIMIT ?");
        return sb.toString();
    }

    protected <T extends Entity> Collection<T> findByIds(final Class<T> resultType, final Collection<Long> ids) {
        final Collection<T> result;
        if (CollectionUtils.isNotEmpty(ids)) {
            result = findEntities(resultType, "ids.contains(this._id) PARAMETERS java.util.Collection ids", ids);
        }
        else {
            result = new LinkedList<T>();
        }
        return result;
    }

    /**
     * replaces named parameters of type {@link Collection} and arrays in the given sql query to use named parameter as part of in clauses.<br/>
     * Example:<br/>
     * 'select sum(b.amount) from booking b where b.type_new in (:bookingTypes)' <br/>
     * will be transformed to<br/>
     * 'select sum(b.amount) from booking b where b.type_new in (8,7)'<br/>
     * <br/>
     * <strong>Note:</strong> only parameters contained in the map will be replaced
     *
     * @param input    - parameters to replace
     * @param inputSql - the original sql
     * @return sql with replaced named parameters
     */
    protected String replaceCollectionTypes(final Map<String, Object> input, final String inputSql) {
        String newSql = inputSql;
        if (MapUtils.isNotEmpty(input)) {
            for (Entry<String, Object> element : input.entrySet()) {
                final Object value = element.getValue();
                if (value instanceof Iterable<?>) {
                    newSql = StringUtils.replace(newSql, ":" + element.getKey(), expand((Iterable<?>) value));
                }
                else if (value instanceof Object[]) {
                    newSql = StringUtils.replace(newSql, ":" + element.getKey(), expand((Object[]) value));
                }
            }
        }
        return newSql;
    }

    protected String expand(final Object[] obArray) {
        final StringBuilder expanded = new StringBuilder();
        if (obArray.length > 0) {
            for (Object object : obArray) {
                addElement(expanded, object, true);
            }
            expanded.deleteCharAt(expanded.length() - 1);
        }
        return expanded.toString();
    }

    protected String expand(final Iterable<?> iterable) {
        final StringBuilder expanded = new StringBuilder();
        final Iterator<?> it = iterable.iterator();
        while (it.hasNext()) {
            addElement(expanded, it.next(), it.hasNext());
        }
        return expanded.toString();
    }

    private void addElement(final StringBuilder sb, Object element, final boolean appendDelim) {
        final boolean isNumber = (element instanceof Number);
        final boolean isMappableEnum = (element instanceof MappableEnum<?>);
        final boolean isEntity = (element instanceof Entity);
        final boolean isNoNumber = !isNumber && !isMappableEnum && !isEntity;
        if (isNoNumber) {
            sb.append("'");
        }

        if (isMappableEnum) {
            sb.append(((MappableEnum<?>) element).getDbValue());
        }
        else if (isNumber) {
            sb.append(element);
        }
        else if (isEntity) {
            sb.append(((Entity) element).getId());
        }
        else {
            sb.append(StringEscapeUtils.escapeSql(ObjectUtils.toString(element)));
        }

        if (isNoNumber) {
            sb.append("'");
        }
        if (appendDelim) {
            sb.append(",");
        }
    }


    protected <K, V> Map<K, V> executeForMap(final String sql, Object... params) {
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        q.setResultClass(Object[].class);
        final List<Object[]> sqlResult = (List<Object[]>) q.executeWithArray(params);
        final Map<K, V> result = new LinkedHashMap<K, V>();
        for (Object[] row : sqlResult) {
            result.put((K) row[0], (V) row[1]);
        }
        return result;
    }

    protected <T> T executeWithParamsUnique(final String sqlInput, final Map<String, Object> params, final Class<T> returnType) {
        final String sql = replaceCollectionTypes(params, sqlInput);
        final Query q = getPersistenceManager().newQuery(Query.SQL, sql);
        return executeQuery(q, params, returnType, true);
    }

    protected <T> T executeQuery(final Query q, final Map<String, Object> params, final Class<T> returnType, final boolean unique) {
        if (returnType != null) {
            q.setResultClass(returnType);
        }
        if (unique) {
            q.setUnique(true);
        }
        return (T) q.executeWithMap(params);
    }

    protected <T> T executeNamedQuery(final Class<?> type, final String name, final Map<String, Object> params, final Class<T> returnType, final boolean unique) {
        final Query q = getPersistenceManager().newNamedQuery(type, name);
        return executeQuery(q, params, returnType, unique);
    }

    protected Collection<Long> getIds(Collection<? extends Entity> entities) {
        Collection<Long> result = new ArrayList<Long>(entities.size());
        for (Entity entity : entities) {
            result.add(entity.getId());
        }
        return result;
    }

    protected Map<String, Object> getSingleParamMap(final String name, final Object value) {
        return Collections.singletonMap(name, value);
    }


    public PersistenceManager getPersistenceManager() {
        return PersistenceManagerFactoryUtils.getPersistenceManager(persistenceManagerFactory, true);
    }

    public void setPersistenceManagerFactory(PersistenceManagerFactory persistenceManagerFactory) {
        this.persistenceManagerFactory = persistenceManagerFactory;
    }

    //-------- Begin List Filter Stuff -------

    public String createQueryTerm(Collection elements, Map valuesMap, List parameters, String concat) {
        StringBuilder term = new StringBuilder();
        String defaultConcat = " && ";
        boolean isRootFilterElement = false;
        for (Object obj : elements) {
            ListFilterElement elem = (ListFilterElement) obj;
            String elemTerm = elem.createQueryTerm(valuesMap, parameters);
            if (StringUtils.isNotEmpty(elemTerm)) {

                if (elements.contains(elem)) {
                    isRootFilterElement = true;
                }

                if (StringUtils.isNotEmpty(concat) || !isRootFilterElement) {
                    concat = elem.getConcatenationOperator();
                }

                term.append(concat).append(isRootFilterElement ? "(" : " ").append(elemTerm);

                if (elem.getChildren() != null) {
                    String childrenTerm = createQueryTerm(elem.getChildren(), valuesMap, parameters, concat);
                    if (StringUtils.isNotEmpty(childrenTerm)) {
                        term.append(childrenTerm);
                    }
                }
                term.append(isRootFilterElement ? ")" : " ");

                concat = defaultConcat;
            }
        }

        String queryTerm;
        if (term.length() > 0) {
            if (isRootFilterElement) {
                queryTerm = "(" + term.toString() + ")";
            }
            else {
                queryTerm = term.toString();
            }
        }
        else {
            queryTerm = "";
        }

        concat = "";
        StringBuilder paramsString = new StringBuilder();
        for (int i = 0; i < parameters.size(); i++) {
            Object object = parameters.get(i);
            paramsString.append(concat + object.getClass().getName() + " param" + (i + 1));
            concat = ", ";
        }
        if (paramsString.length() > 0) {
            queryTerm += " PARAMETERS " + paramsString;
        }
        LOGGER.debug("Filter term: '" + queryTerm + "'");
        return queryTerm;
    }

    public <T> T findOne(Class<T> clazz, Long id) {
        try {
            return getPersistenceManager().getObjectById(clazz, id);
        } catch (NucleusObjectNotFoundException e) {
            return null;
        }
    }

    public <T> void delete(Class<T> clazz, Long id){
        T obj = getPersistenceManager().getObjectById(clazz, id);
        getPersistenceManager().deletePersistent(obj);
    }
}