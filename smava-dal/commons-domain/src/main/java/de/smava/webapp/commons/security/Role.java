package de.smava.webapp.commons.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;


/**
 * Enumeration of all System-Roles.
 * @author Daniel Doubleday
 */
public enum Role {
    /**
     * The permission tag for the anonymous user.
     */
    ROLE_ANONYMOUS,

    /**
     * The role is for unregistered inviters.
     */
    ROLE_GUEST,

    /**
     * A member is registered but has not decided if he is a borrower or a lender.
     */
    ROLE_MEMBER,

    /**
     * The lender role which is used to give access to lender specific
     * funtionality.
     */
    ROLE_LENDER,

    /**
     * The borrower role which is used to give access to borrower specific
     * funtionality.
     */
    ROLE_BORROWER,

    /**
     * Special role for scheduling.
     */
    ROLE_TASK_USER,

    /**
     * The regular backoffice user.
     */
    ROLE_BO_USER,

    ROLE_BANK,
    
    ROLE_BANK_BIW,
    
    ROLE_BANK_FIDOR,

    ROLE_MARKETING,

    ROLE_SUPPORT,

    ROLE_CLERK,

    ROLE_CREDIT_ADVISOR,

    ROLE_CREDIT_ADVISORY_ADMIN,

    ROLE_CASI_GUEST,

    /**
     * CASI role for data analyst
     */
    ROLE_CASI_ANALYTICS,

    /**
     * Smava friend.
     */
    ROLE_BORROWER_FRIEND,
    
    /**
     * Vermoegensverwalter-Kontakt .
     */
    ROLE_ASSET_MANAGER,
    
    /**
     * Bisheriger Marketing-Partner, der kein smava-Agent-Kontakt ist .
     */
    ROLE_LEAD_PARTNER,
    
    /**
     * Smava-Agent-Kontakt .
     */
    ROLE_SMAVA_AGENT,

    /**
     * God. At least in the backoffice.
     */
    ROLE_SUPERUSER,
    
    /**
     * Nutzer der sich noch nicht selbst eingeloggt hat, aber schon identifiziert ist und zB im Zuge des 
     * Registrierungsprozesses Aktionen (zB KP erstllen) ausführen darf.
     */
    ROLE_TEMPORARY_LOGGEDIN_USER,

    ROLE_TOKEN_LOGGED_IN,

    ROLE_TEAM_LEAD;

    private final GrantedAuthority _authority;

    Role() {
		_authority = new SimpleGrantedAuthority(name());
	}

	public GrantedAuthority getAuthority() {
        return _authority;
    }

    public static Set<Role> translateStringSet(Set<String> roles) {
        Set<Role> result = EnumSet.noneOf(Role.class);
        for (String s : roles) {
            result.add(Enum.valueOf(Role.class, s));
        }
        return result;
    }

    public static Set<String> translateRoleSet(Set<Role> roles) {
        Set<String> result = new HashSet<String>(roles.size());
        for (Role role : roles) {
            result.add(role.name());
        }
        return result;
    }

    public static Set<Role> storefrontRoles() {
        return EnumSet.of(ROLE_MEMBER, ROLE_BORROWER, ROLE_LENDER);
    }

    public static Set<Role> casiRoles() {
    	return EnumSet.of(ROLE_SUPERUSER, ROLE_CREDIT_ADVISOR, ROLE_CREDIT_ADVISORY_ADMIN, ROLE_CASI_GUEST, ROLE_CASI_ANALYTICS, ROLE_TEAM_LEAD);
    }

    public static Set<Role> backofficeRoles() {
        return EnumSet.of(ROLE_BO_USER, ROLE_SUPERUSER, ROLE_BANK_BIW, ROLE_BANK_FIDOR, ROLE_MARKETING, ROLE_CLERK, ROLE_SUPPORT, ROLE_CREDIT_ADVISOR, ROLE_CREDIT_ADVISORY_ADMIN, ROLE_CASI_GUEST, ROLE_CASI_ANALYTICS, ROLE_TEAM_LEAD);
    }
    
    public static Set<Role> partnerRoles() {
    	return EnumSet.of(ROLE_SMAVA_AGENT, ROLE_LEAD_PARTNER, ROLE_ASSET_MANAGER);
    }
}