package de.smava.webapp.account.security;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public interface SmavaUserDetailsIf {


	Long getSmavaId();
	Collection<? extends GrantedAuthority> getAuthorities();
}
