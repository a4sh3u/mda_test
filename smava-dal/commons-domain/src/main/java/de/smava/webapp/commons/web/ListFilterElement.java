package de.smava.webapp.commons.web;

import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.util.FormatUtils;
import de.smava.webapp.commons.util.ResourceBundleEnum;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Filter element for backoffice list views. Holds information for filter jsp (e.g. widget type and resource bundle
 * for option lists) and for query generating.
 * <p/>
 *
 * name: Request parameter name; widgetType: type of form widget ("text", "select", "checkboxlist");
 * candidates: comma seperated query like info about all fields to search (see below); query: if false don't
 * query, label: Label to show in form; options: comma seperated list of values to be used if no resource is given
 *
 * <p/>
 * Candidates format: comma seperated list of ([simple java type name] [entity's field name] [operator]) triples
 * to be combined by OR operator.
 * <br /> E.g.:<br />
 * "Date _creationDate >=, Date _transactionDate >=".
 * <p/>
 * Date: 11.09.2006
 *
 * @author ingmar.decker
 */
public class ListFilterElement {

    private String _name = "";
    private String _widgetType = "text";
    private String _candidates = "";
    private ResourceBundleEnum _resourceBundleEnum = null;
    private boolean _query = true;
    private String _label = "";
    private String _options = "";
    private boolean _show = true;
    private List<String> _variables = new ArrayList<String>();
    private String _concatenationOperator = " && "; 
    private Collection<ListFilterElement> _children;


    /**
     * Get resource bundle as map if present, otherwise empty map.
     */
    public Map<String, String> getReferenceData() {
        SortedMap<String, String> map = new TreeMap<String, String>();
        if (getResourceBundleEnum() == null) {
            String optionsString = getOptions();
            if (optionsString != null) {
                String[] optionsSplit = optionsString.split(", ?");
                for (String option : optionsSplit) {
                    String value = option;
                    String label = option;
                    String[] valueLabel = option.split(":");
                    if (valueLabel.length == 2) {
                        value = valueLabel[0];
                        label = valueLabel[1];
                    }
                    map.put(value, label);
                }
            }
        } else {
            Map<String, String> resource = getResourceBundleEnum().asMap();
            map.put("", "");
            map.putAll(resource);
        }
        return map;
    }

    /**
     * Create query term (oql where clause) using values in valuesMap (usually containing the request parameter values).
     * @param valuesMap Current values to fill query with
     * @param parameters List of parameters (not null!) for later usage in dao's find...List method;
     * this method will add patameters if necessary (e.g. for dates). If necessary this method will create a query
     * term with oql placeholders like "param[n]" and append the parameter value to parameters list, where n is (new)
     * size of parameters list.
     * @return Query string for this element.
     */
    public String createQueryTerm(Map<String, String[]> valuesMap, List<Object> parameters) {
        StringBuilder term = new StringBuilder("");
        String defaultConcatOp = " || ";
        String concatOp = "";
        List<String> candidates = getCandidatesAsList();

        if (!candidates.isEmpty() && isQuery()) {
            for (String candidate : candidates) {
            	if (!StringUtils.isEmpty(candidate)) {
	                String[] candidateInfo = getCandidateInfo(candidate);
	
	                String javaType = candidateInfo[0];
	                String fieldName = candidateInfo[1];
	                String operator = candidateInfo[2];
	                String expression = null;
	                String[] values = valuesMap.get(getName());
	                if (values != null) {
	                    if (values.length > 1) {
	                        expression = getExpression(javaType, fieldName, operator, values, parameters);
	                    } else if (values.length == 1) {
	                        String value = values[0];
	                        expression = getExpression(javaType, fieldName, operator, value, parameters);
	                    }
	                }
	                if (expression != null && !"".equals(expression)) {
	                    term.append(concatOp).append(expression);
	                    concatOp = defaultConcatOp;
	                }
            	}
            }
        }
        String queryTerm = term.toString();
        return queryTerm;
    }

    /**
     * Split candidate string and returns array with three elements: 1. java type name, 2. query fields expression,
     * 3. operator.
     */
    private String[] getCandidateInfo(String candidate) {
        String[] split = candidate.split(" +");
        String[] result = split;
        if (result.length > 3) {
            result = new String[]{"", "", ""};
            result[0] = split[0];
            result[2] = split[split.length - 1];
            StringBuilder info = new StringBuilder();
            String delim = "";
            for (int i = 1; i < split.length - 1; i++) {
                info.append(delim).append(split[i]);
                delim = " ";
            }
            result[1] = info.toString();
        } else if (result.length < 3) {
            throw new IllegalArgumentException("Wrong syntax for candidates (\"" + candidate + "\"). Forgot blanks in front of operator?");
        }
        return result;
    }

    /**
     * Get expression for given properties.
     * @param javaType Simple java type name like "String", "Date"...
     * @param fieldName Name of field to query like "_creationDate"
     * @param operator Operator like "==", "<="... Special: "like" is converted to appropriate oql match expression
     * @param values Current values to use in query (request parameter values for this element's name). If array has more than 1
     * elements then query is repeated for all values concatenated with OR.
     * @param parameters Parameter list for later use in dao's find...List method, will be extended here if necessary
     */
    private String getExpression(String javaType, String fieldName, String operator, String[] values, List<Object> parameters) {
        String concatOp = "";
        String defaultConcatOp = " || ";
        StringBuilder term = new StringBuilder();
        // If fieldName looks like [^ ]+ [^ ]+ (i.e. contains blank), then split it and use only last part for
        // expression generation. Example: <property name="candidates" value="String _bookings.contains(b) &amp;&amp; b._type ==" />
        // here we have to repeat "b._type ==" with all values. If fieldName matches this pattern, parts[1] will contain last part...
        String[] parts = splitFieldExpression(fieldName);
        for (String value : values) {
            String expression;
            if (parts.length <= 1) {
                // Normal expression:
                expression = getExpression(javaType, fieldName, operator, value, parameters);
            } else {
                // Special expression with more than one parts...
                expression = getExpression(javaType, parts[1], operator, value, parameters);
            }
            if (expression != null && !"".equals(expression)) {
                term.append(concatOp).append(expression);
                concatOp = defaultConcatOp;
            }
        }
        String result = term.toString();
        if (parts.length == 2) {
            result = parts[0] + "(" + result + ")";
        }
        return result;
    }

    /**
     * Split given fieldExpression at last blank(s). If there is such a blank character sequence, this returns
     * an array with the both parts, otherwise an array with only one element: the fieldExpression.
     * @param fieldExpression String to split
     */
    private String[] splitFieldExpression(String fieldExpression) {
        String[] result;
        Pattern pattern = Pattern.compile("^(.*) +([^ ]+)$");
        Matcher matcher = pattern.matcher(fieldExpression);
        if (matcher.matches()) {
            result = new String[]{matcher.group(1), matcher.group(2)};
        } else {
            result = new String[]{fieldExpression};
        }
        return result;
    }

    /**
     * Get expression for given properties.
     * @param javaType Simple java type name like "String", "Date", Integer, "Double"
     * @param fieldName Name of field to query like "_creationDate"
     * @param operator Operator like "==", "&lt;="... Special: "like" is converted to appropriate oql match expression
     * @param curentValue Current value to use in query (request parameter value for this element's name).
     * @param parameters Parameter list for later use in dao's find...List method, will be extended here if necessary
     */
    private String getExpression(String javaType, String fieldName, String operator, String curentValue, List<Object> parameters) {
        String value = curentValue.trim();
        String term = "";
        if (StringUtils.isNotEmpty(value)) {
            if ("String".equals(javaType)) {
                if ("like".equals(operator)) {
                    term = fieldName + ".toLowerCase().matches('.*" + value.toLowerCase(Locale.getDefault()) + ".*')";
                } else {
                    term = fieldName + " " + operator + " '" + value + "'";
                }
            } else if ("Date".equals(javaType)) {
                Date date = FormatUtils.asDateTime(value, CurrentDate.getDate());
                if ("==".equals(operator)) {
                    Date startDate = DateUtils.clean(date);
                    Date endDate = DateUtils.addDays(startDate, 1);
                    parameters.add(startDate);
                    term += "(" + fieldName + " >= param" + parameters.size();
                    parameters.add(endDate);
                    term += " && " + fieldName + " < param" + parameters.size() + ")";
                } else if ("<=".equals(operator) || "&lt;=".equals(operator)) {
                    Date startDate = DateUtils.clean(date);
                    Date endDate = DateUtils.addDays(startDate, 1);
                    parameters.add(endDate);
                    term += fieldName + " < param" + parameters.size();
                } else {
                    parameters.add(date);
                    term = fieldName + operator + " param" + parameters.size();
                }
            } else if ("Integer".equals(javaType) || "Long".equals(javaType)) {
                Integer number = NumberUtils.toInt(value, 0);
                term = fieldName + " " + operator + " " + number;
            } else if ("Double".equals(javaType)  || "Float".equals(javaType)) {
                Double number = NumberUtils.toDouble(value, 0);
                term = fieldName + " " + operator + " " + number;
            } else if ("boolean".equals(javaType.toLowerCase(Locale.getDefault()))) {
                Boolean b = Boolean.parseBoolean(value);
                term = fieldName + " " + operator + " " + b.toString();
            } else {
                throw new UnsupportedOperationException("Unsupported type " + javaType);
            }
        }
        return term;
    }

    private List<String> getCandidatesAsList() {
        String[] candidates = StringUtils.stripAll(getCandidates().split(","));
        return Arrays.asList(candidates);
    }

    /**
     * Get label property to show in form.
     */
    public String getLabel() {
        return _label;
    }

    public void setLabel(String label) {
        _label = label;
    }

    /**
     * Get name property, i.e. the form parameter name.
     */
    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    /**
     * Get widget type property, i.e. the form element type.
     */
    public String getWidgetType() {
        return _widgetType;
    }

    public void setWidgetType(String widgetType) {
        _widgetType = widgetType;
    }

    /**
     * Get candidates property, i.e. the comma seperated list of expressions that have to
     * be checked connected by OR operator, see {@link ListFilterElement} description.
     */
    public String getCandidates() {
        return _candidates;
    }

    public void setCandidates(String candidates) {
        _candidates = candidates;
    }

    /**
     * Get resource bundle property (might be null).
     */
    public ResourceBundleEnum getResourceBundleEnum() {
        return _resourceBundleEnum;
    }

    public void setResourceBundleEnum(ResourceBundleEnum resourceBundleEnum) {
        _resourceBundleEnum = resourceBundleEnum;
    }

    /**
     * Get query property: if true this element should be used in query, otherwise not
     * (means for example only for display).
     */
    public boolean isQuery() {
        return _query;
    }

    public void setQuery(boolean query) {
        _query = query;
    }

    /**
     * Get options property, i.e. comma seperated list of options to display in select widget
     * (makes sense / is queries only for select widgets).
     */
    public String getOptions() {
        return _options;
    }

    public void setOptions(String options) {
        _options = options;
    }

    /**
     * Get show property: if true this element should be shown in form, otherwise not
     * (like a hidden parameter). Filter template should
     */
    public boolean isShow() {
        return _show;
    }

    public void setShow(boolean show) {
        _show = show;
    }

    public List<String> getVariables() {
        return _variables;
    }

    public void setVariables(List<String> variables) {
        _variables = variables;
    }

	public String getConcatenationOperator() {
		return _concatenationOperator;
	}

	public void setConcatenationOperator(String concatenationOperator) {
		_concatenationOperator = concatenationOperator;
	}

	public Collection<ListFilterElement> getChildren() {
		return _children;
	}

	public void setChildren(Collection<ListFilterElement> children) {
		_children = children;
	}
}