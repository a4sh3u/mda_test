package de.smava.webapp.commons.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;
import java.util.*;


/**
 * @author Daniel Doubleday
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@Id")
public abstract class KreditPrivatEntity implements Serializable, Entity {
    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    private final transient Map<String, Change> _changeMap = new HashMap<String, Change>();

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public boolean isNew() {
        return getId() == null;
    }

    public void copyFromOldEntity(Entity oldEntity) {
        // do nothing, method will be overwritten by generated classes
    }

    public boolean functionallyEquals(Entity otherEntity) {
        return true;
    }

    public Set<String> getChangeSet() {
        return _changeMap.keySet();
    }

    protected void registerChange(String key, Object oldValue, Object newValue) {
        if (valuesAreEqual(oldValue, newValue)) {
            _changeMap.remove(key);
        } else {
            Change change = _changeMap.get(key);
            if (change != null && valuesAreEqual(change.getOldValue(), newValue)) {
                _changeMap.remove(key);
            } else {
                _changeMap.put(key, new Change(oldValue, newValue));
            }
        }
    }

    /**
     * Return a list of accounts that changed values of the entity.
     *
     * @return A list of accounts that can be empty but must never be null.
     */
    public List<? extends Entity> getModifier() {
        return Collections.EMPTY_LIST;
    }

    public void clearChanges() {
        _changeMap.clear();
    }

    public Map<String, Change> getChangeMap() {
        return Collections.unmodifiableMap(_changeMap);
    }

    protected boolean valuesAreEqual(Object val1, Object val2) {
        final boolean result;
        // special handling of string comparison
        if (val1 instanceof String || val2 instanceof String) {
            result = valuesAreEqual(String.valueOf(val1), String.valueOf(val2));
        } else {
            if (val1 == val2) {
                result = true;
            } else if (val1 == null || val2 == null) {
                result = false;
            } else {
                result = val1.equals(val2);
            }
        }
        return result;
    }

    /**
     * Very special string comparison which considers null and blank strings equal.
     */
    protected boolean valuesAreEqual(String val1, String val2) {
        final boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null) {
            result = StringUtils.isBlank(val2);
        } else if (val2 == null) {
            result = StringUtils.isBlank(val1);
        } else {
            result = val1.equals(val2);
        }
        return result;
    }

    protected boolean valuesAreEqual(double val1, double val2) {
        return Math.abs(val2 - val1) < 0.001;
    }

    @Override
	public boolean equals(Object o) {
        final boolean result;
        if (this == o) {
            result = true;
        } else if (o == null || getClass() != o.getClass()) {
            result = false;
        } else {
            final Entity entity = (Entity) o;
            if (_id == null) {
                result = entity.getId() == null;
            } else {
                result = _id.equals(entity.getId());
            }
        }
        return result;
    }

    @Override
	public int hashCode() {
        final int hashCode;
        if (_id == null) {
            hashCode = super.hashCode();
        } else {
            hashCode = _id.hashCode();
        }
        return hashCode;
    }

    @Override
	public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}