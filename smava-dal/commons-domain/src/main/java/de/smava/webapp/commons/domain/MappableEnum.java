/**
 * 
 */
package de.smava.webapp.commons.domain;

/**
 * @param <T> type of implementing {@link Enum}
 * 
 * @author bvoss
 *
 */
public interface MappableEnum<T extends Enum<T>> {

	Number getDbValue();
	
	T getEnumFromDbValue(Number dbValue);
	
	/**
	 * for better use in jsps und EL.
	 * 
	 * @return {@link Enum#name()}
	 */
	String getName();
}
