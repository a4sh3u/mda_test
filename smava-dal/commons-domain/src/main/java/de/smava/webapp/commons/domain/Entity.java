package de.smava.webapp.commons.domain;

import java.util.Map;

public interface Entity extends BaseEntity {

    @Deprecated
    void copyFromOldEntity(Entity oldEntity);

    @Deprecated
    boolean functionallyEquals(Entity otherEntity);

    @Deprecated
    void clearChanges();

    @Deprecated
    Map<String, Change> getChangeMap();
}