package de.smava.webapp.commons.exception;

import java.io.IOException;

/**
 * Special error while generating pdfs.
 * The error indicates, that the accessing an external resource while writing or reading a pdf failed.
 *
 * Created by aherr on 21.11.14.
 */
public class PdfIOException extends PdfException {

    public PdfIOException(IOException ioe) {
       super(ioe);
    }

}
