package de.smava.webapp.commons.exception;

public class ValidationException extends ModelException {

	public ValidationException(String type, String constraint) {
		super(type+":"+constraint+" is invalid");
	}
	
	public ValidationException(String type, String constraint, String value) {
		super(type+":"+constraint+"='"+value+"' is invalid");
	}
	
}
