package de.smava.webapp.commons.domainutil;

import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.util.ResourceBundleEnum;

import java.util.Map;

public interface IResourceProvider {

	/**
	 * Get a resource based translation of an entity's property value. This method
	 * looks for a resource with key "[entity's full class name]._[propertyName]"
	 * in entityPropertyResourceMap. If there is such a resourceBundleEnum the value
	 * of the map is sought.
	 * @param entity Entity to check propertyName's property value
	 * @param propertyName Name of property to get value that is used as key to search the resourceBundleEnum
	 */
	public String getResourceValue(Entity entity, String propertyName);

	/**
	 * Get resource bundle enum for name or null if none found.
	 */
	public ResourceBundleEnum getResourceForName(String name);

	/**
	 * Get resource bundle enum for name or null if none found.
	 */
	public boolean hasResourceForName(String name);

	public Map<String, ResourceBundleEnum> getEntityPropertyResourceMap();

	public void setEntityPropertyResourceMap(
			Map<String, ResourceBundleEnum> entityPropertyResourceMap);

	public ResourceBundleEnum getKindOfCreditResource();

	public void setKindOfCreditResource(ResourceBundleEnum kindOfCreditResource);

	public ResourceBundleEnum getAcademicTitleResource();

	public void setAcademicTitleResource(ResourceBundleEnum academicTitleResource);

	public ResourceBundleEnum getSalutationResource();

	public void setSalutationResource(ResourceBundleEnum salutationResource);

	public ResourceBundleEnum getEmploymentResource();

	public void setEmploymentResource(ResourceBundleEnum employmentResource);

	public ResourceBundleEnum getFamilyStatusResource();

	public void setFamilyStatusResource(ResourceBundleEnum familyStatusResource);

	public ResourceBundleEnum getConfessionResource();

	public void setConfessionResource(ResourceBundleEnum confessionResource);

	public ResourceBundleEnum getNumberOfChildrenResource();

	public void setNumberOfChildrenResource(
			ResourceBundleEnum numberOfChildrenResource);

	public ResourceBundleEnum getNumberOfOtherInHouseholdResource();

	public void setNumberOfOtherInHouseholdResource(
			ResourceBundleEnum numberOfOtherInHouseholdResource);

	public ResourceBundleEnum getNumberOfPeopleInHouseholdResource();

	public void setNumberOfPeopleInHouseholdResource(
			ResourceBundleEnum numberOfPeopleInHouseholdResource);

	public ResourceBundleEnum getSecurityQuestionsResource();

	public void setSecurityQuestionsResource(
			ResourceBundleEnum securityQuestionsResource);

	public ResourceBundleEnum getTransactionStatesResource();

	public void setTransactionStatesResource(
			ResourceBundleEnum transactionStatesResource);

	public ResourceBundleEnum getTransactionTypesResource();

	public void setTransactionTypesResource(
			ResourceBundleEnum transactionTypesResource);

	public ResourceBundleEnum getSmavaGroupTypesResource();

	public void setSmavaGroupTypesResource(
			ResourceBundleEnum smavaGroupTypesResource);

	public ResourceBundleEnum getSmavaGroupStatesResource();

	public void setSmavaGroupStatesResource(
			ResourceBundleEnum smavaGroupStatesResource);

	public ResourceBundleEnum getBookingTypesResource();

	public void setBookingTypesResource(ResourceBundleEnum bookingTypesResource);

	public ResourceBundleEnum getDurationOfOccupationResource();

	public void setDurationOfOccupationResource(
			ResourceBundleEnum durationOfOccupationResource);

	public ResourceBundleEnum getExpensesCarResource();

	public void setExpensesCarResource(ResourceBundleEnum expensesCarResource);

	public ResourceBundleEnum getModeOfAccommodationResource();

	public void setModeOfAccommodationResource(
			ResourceBundleEnum modeOfAccommodationResource);

	public ResourceBundleEnum getNumOfCarsResource();

	public void setNumOfCarsResource(ResourceBundleEnum numOfCarsResource);

	public ResourceBundleEnum getUserStatesResource();

	public void setUserStatesResource(ResourceBundleEnum userStatesResource);

	public ResourceBundleEnum getIntendedUseResource();

	public void setIntendedUseResource(ResourceBundleEnum intendedUseResource);

	/** @deprecated Use document... */
	public ResourceBundleEnum getTypeResource();

	/** @deprecated Use document... */
	public void setTypeResource(ResourceBundleEnum typeResource);

	/** @deprecated Use document... */
	public ResourceBundleEnum getTypeOfDispatchResource();

	/** @deprecated Use document... */
	public void setTypeOfDispatchResource(
			ResourceBundleEnum typeOfDispatchResource);

	/** @deprecated Use document... */
	public ResourceBundleEnum getDirectionResource();

	/** @deprecated Use document... */
	public void setDirectionResource(ResourceBundleEnum directionResource);

	public ResourceBundleEnum getDocumentTypeResource();

	public void setDocumentTypeResource(ResourceBundleEnum documentTypeResource);

	public ResourceBundleEnum getDocumentTypeOfDispatchResource();

	public void setDocumentTypeOfDispatchResource(
			ResourceBundleEnum documentTypeOfDispatchResource);

	public ResourceBundleEnum getDocumentDirectionResource();

	public void setDocumentDirectionResource(
			ResourceBundleEnum documentDirectionResource);

	public ResourceBundleEnum getSmavaGroupMemberStatesResource();

	public void setSmavaGroupMemberStatesResource(
			ResourceBundleEnum smavaGroupMemberStatesResource);

	public ResourceBundleEnum getBidStatesResource();

	public void setBidStatesResource(ResourceBundleEnum bidStatesResource);

	public ResourceBundleEnum getDtausFileStatesResource();

	public void setDtausFileStatesResource(
			ResourceBundleEnum dtausFileStatesResource);

	public ResourceBundleEnum getDtausEntryTypesResource();

	public void setDtausEntryTypesResource(
			ResourceBundleEnum dtausEntryTypesResource);

	public ResourceBundleEnum getDtausEntryStatesResource();

	public void setDtausEntryStatesResource(
			ResourceBundleEnum dtausEntryStatesResource);

	public ResourceBundleEnum getBookingGroupTypesResource();

	public void setBookingGroupTypesResource(
			ResourceBundleEnum bookingGroupTypesResource);

	public ResourceBundleEnum getContractStatesResource();

	public void setContractStatesResource(
			ResourceBundleEnum contractStatesResource);

	public ResourceBundleEnum getDocumentStateResource();

	public void setDocumentStateResource(
			ResourceBundleEnum documentStateResource);

	public ResourceBundleEnum getDealTypesResource();

	public void setDealTypesResource(ResourceBundleEnum dealTypesResource);

	public ResourceBundleEnum getSmavaGroupInterestingResource();

	public void setSmavaGroupInterestingResource(
			ResourceBundleEnum smavaGroupInterestingResource);

	public ResourceBundleEnum getEntaxStatesResource();

	public void setEntaxStatesResource(ResourceBundleEnum entaxStatesResource);

	public ResourceBundleEnum getBookingPerformanceStatesResource();

	public void setBookingPerformanceStatesResource(
			ResourceBundleEnum bookingPerformanceStatesResource);

	public ResourceBundleEnum getIsoCitizenshipResource();

	public void setIsoCitizenshipResource(
			ResourceBundleEnum isoCitizenshipResource);

	public ResourceBundleEnum getIsoCountryResource();

	public void setIsoCountryResource(ResourceBundleEnum isoCountryResource);

	public ResourceBundleEnum getRdiContractTypesResource();

	public void setRdiContractTypesResource(
			ResourceBundleEnum rdiContractTypesResource);

	public ResourceBundleEnum getRdiContractTypesShortResource();

	public void setRdiContractTypesShortResource(
			ResourceBundleEnum rdiContractTypesShortResource);

	public ResourceBundleEnum getRdiContractStatesResource();

	public void setRdiContractStatesResource(
			ResourceBundleEnum rdiContractStatesResource);

	public ResourceBundleEnum getRdiPaymentStatesResource();

	public void setRdiPaymentStatesResource(
			ResourceBundleEnum rdiPaymentStatesResource);

	public ResourceBundleEnum getRdiPaymentTypesResource();

	public void setRdiPaymentTypesResource(
			ResourceBundleEnum rdiPaymentTypesResource);

	public ResourceBundleEnum getRdiRequestStatesResource();

	public void setRdiRequestStatesResource(
			ResourceBundleEnum rdiRequestStatesResource);

	public ResourceBundleEnum getRdiResponseStatesResource();

	public void setRdiResponseStatesResource(
			ResourceBundleEnum rdiResponseStatesResource);

	public ResourceBundleEnum getCustomerBookingTypesResource();

	public void setCustomerBookingTypesResource(
			ResourceBundleEnum customerBookingTypesResource);

	public ResourceBundleEnum getDocumentApprovalStateResource();

	public void setDocumentApprovalStateResource(
			ResourceBundleEnum documentApprovalStateResource);

	public ResourceBundleEnum getTypeOfCompanyResource();

	public void setTypeOfCompanyResource(
			ResourceBundleEnum typeOfCompanyResource);

	public ResourceBundleEnum getEducationEarnedResource();

	public void setEducationEarnedResource(
			ResourceBundleEnum educationEarnedResource);

	public ResourceBundleEnum getDocumentContainerNamesResource();

	public void setDocumentContainerNamesResource(
			ResourceBundleEnum documentContainerNamesResource);

	public ResourceBundleEnum getDocumentAttachmentContainerStatesResource();

	public void setDocumentAttachmentContainerStatesResource(
			ResourceBundleEnum documentAttachmentContainerStateResource);

	public ResourceBundleEnum getCreditProjectSorterResource();

	public void setCreditProjectSorterResource(
			ResourceBundleEnum creditProjectSorterResource);

	public ResourceBundleEnum getSharedLoanResource();

	public void setSharedLoanResource(ResourceBundleEnum sharedLoanResource);

	public void setKindOfUserResource(ResourceBundleEnum kindOfUserResource);

	public ResourceBundleEnum getKindOfUserResource();

	public ResourceBundleEnum getMiscEarnTypeResource();

	public void setMiscEarnTypeResource(ResourceBundleEnum miscEarnTypesResource);

	public ResourceBundleEnum getCreditCardTypesResource();

	public void setCreditCardTypesResource(
			ResourceBundleEnum creditCardTypesResource);

	public ResourceBundleEnum getPropertyOwnedTypesResource();

	public void setPropertyOwnedTypesResource(
			ResourceBundleEnum propertyOwnedTypesResource);

	public ResourceBundleEnum getRdiContractTypesBrokerageResource();

	public void setRdiContractTypesBrokerageResource(
			ResourceBundleEnum rdiContractTypesBrokerageResource);

	
	public ResourceBundleEnum getDocumentTypeOfProviderResource();
	public void setDocumentTypeOfProviderResource(ResourceBundleEnum documentTypeOfDispatchResource);

	ResourceBundleEnum getPayoutModeResource();

	void setPayoutModeResource(ResourceBundleEnum payoutModeResource);

	ResourceBundleEnum getAdvisoryFailedResource();

	void setAdvisoryFailedResource(ResourceBundleEnum advisoryFailedResource);

	ResourceBundleEnum getCaJournalSubjectResource();

	void setCaJournalSubjectResource(ResourceBundleEnum caJournalSubjectResource);
	
	ResourceBundleEnum getVehicleTypeResource();

	void setVehicleTypeResource(ResourceBundleEnum vehicleTypeResource);

    ResourceBundleEnum getSmsResource();

    void setSmsResource(ResourceBundleEnum smsResource);

    public ResourceBundleEnum getSelfemployedProfessionResource();

	void setSelfemployedProfessionResource(ResourceBundleEnum selfemployedProfessionResource);

    ResourceBundleEnum getEstateTypeResource();

	void setEstateTypeResource(ResourceBundleEnum estateTypeResource);

    ResourceBundleEnum getChildBenefitResource();

	void setChildBenefitResource(ResourceBundleEnum childBenefitResource);

    ResourceBundleEnum getLivingConditionResource();

	void setLivingConditionResource(ResourceBundleEnum livingConditionResource);

    ResourceBundleEnum getSalariedStaffTypeResource();

	void setSalariedStaffTypeResource(ResourceBundleEnum salariedStaffTypeResource);

	ResourceBundleEnum getSalariedStaffSoldierResource();

	void setSalariedStaffSoldierResource(ResourceBundleEnum salariedStaffSoldierResource);

    public ResourceBundleEnum getNumOfBikesResource();

	void setNumOfBikesResource(ResourceBundleEnum numOfBikesResource);

    public ResourceBundleEnum getLettedEstateResource();

	void setLettedEstateResource(ResourceBundleEnum lettedEstateResource);

    public ResourceBundleEnum getSalariedStaffCivilCervantResource();

	void setSalariedStaffCivilCervantResource(ResourceBundleEnum salariedStaffCivilCervantResource);

	/**
	 * @see de.smava.webapp.applicant.type.Gender
	 *
	 * @return
	 */
	ResourceBundleEnum getGenderResource();

	void setGenderResource(ResourceBundleEnum genderResource);



	/**
	 * @see de.smava.webapp.casi.account.i18n.AncillaryEmploymentType
	 *
	 * @return
	 */
	ResourceBundleEnum getCasiAncillaryEmploymentTypeResource();

	void setCasiAncillaryEmploymentTypeResource(ResourceBundleEnum casiAncillaryEmploymentType);




	/**
	 * @see de.smava.webapp.casi.account.i18n.EmploymentType
	 *
	 * @return
	 */
	ResourceBundleEnum getCasiEmploymentTypeResource();

	void setCasiEmploymentTypeResource(ResourceBundleEnum casiEmploymentType);





	/**
	 * @see de.smava.webapp.casi.account.i18n.SalariedStaffType
	 *
	 * @return
	 */
	ResourceBundleEnum getCasiSalariedStaffTypeResource();

	void setCasiSalariedStaffTypeResource(ResourceBundleEnum casiSalariedStaffType);




	/**
	 * @see de.smava.webapp.casi.account.i18n.OfficialServiceGradeType
	 *
	 * @return
	 */
	ResourceBundleEnum getCasiOfficialServiceGradeTypeResource();

	void setCasiOfficialServiceGradeTypeResource(ResourceBundleEnum casiOfficialServiceGrade);


	/**
	 * @see de.smava.webapp.casi.account.i18n.IndustrySectorType
	 *
	 * @return
	 */
	ResourceBundleEnum getCasiIndustrySectorTypeResource();

	void setCasiIndustrySectorTypeResource(ResourceBundleEnum casiIndustrySectorType);




	/**
	 * @see de.smava.webapp.casi.account.i18n.FreelancerOccupationType
	 *
	 * @return
	 */
	ResourceBundleEnum getCasiFreelancerOccupationTypeResource();

	void setCasiFreelancerOccupationTypeResource(ResourceBundleEnum casiFreelancerOccupationType);

	ResourceBundleEnum getCasiLoanBorrowerTypeResource();

	void setCasiLoanBorrowerTypeResource(ResourceBundleEnum casiLoanBorrowerType);

	ResourceBundleEnum getCasiThirdPartyLoanTypeResource();

	void setCasiThirdPartyLoanTypeResource(ResourceBundleEnum casiThirdPartyLoanType);

	ResourceBundleEnum getCasidebtCreditsCardTypeResource();

	void setCasidebtCreditsCardTypeResource(ResourceBundleEnum casidebtCreditsCardType);

	ResourceBundleEnum getCasiApplicantsRelationshipTypeResource();

	void setCasiApplicantsRelationshipTypeResource(ResourceBundleEnum casiApplicantsRelationshipType);

	ResourceBundleEnum getCasiAccomodationTypeResource();

	void setCasiAccommodationTypeResource(ResourceBundleEnum casiAccommodationType);

	ResourceBundleEnum getCasiEstateTypeResource();

	void setCasiEstateTypeResource(ResourceBundleEnum casiEstateType);

	ResourceBundleEnum getCasiOtherPensionIncomeTypeResource();

	void setCasiOtherPensionIncomeTypeResource(ResourceBundleEnum casiOtherPensionIncomeType);

	ResourceBundleEnum getCasiOtherRevenueTypeResource();

	void setCasiOtherRevenueTypeResource(ResourceBundleEnum casiOtherRevenueType);

	ResourceBundleEnum getCasiMaritalStateTypeResource();

	void setCasiMaritalStateTypeResource(ResourceBundleEnum casiMaritalStateType);

	ResourceBundleEnum getCasiOtherExpenseTypeResource();

	void setCasiOtherExpenseTypeResource(ResourceBundleEnum casiOtherExpenseType);

	ResourceBundleEnum getPartTimeJobTypeResource();

	void setPartTimeJobTypeResource(ResourceBundleEnum partTimeJobType);

	ResourceBundleEnum getBorrowerRelationshipTypeResource();

	void setBorrowerRelationshipTypeResource(ResourceBundleEnum borrowerRelationshipType);

	ResourceBundleEnum getAdditionalIncomeTypeResource();

	void setAdditionalIncomeTypeResource(ResourceBundleEnum additionalIncomeTypeResource);

	ResourceBundleEnum getPensionSupplementTypeResource();

	void setPensionSupplementTypeResource(ResourceBundleEnum pensionSupplementTypeResource);

	ResourceBundleEnum getPensionTypeResource();

	void setPensionTypeResource(ResourceBundleEnum pensionTypeResource);

	public ResourceBundleEnum getSelfEmployedTypeResource();

	void setSelfEmployedTypeResource(ResourceBundleEnum selfEmployedTypeResource);
}