package de.smava.webapp.commons.util;

public interface ValueProvider {

	String getValue(String key);
}
