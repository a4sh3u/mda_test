package de.smava.webapp.commons.exception;

/**
 * Created by pvitic on 07.09.15.
 */
public class MappingException extends Exception {

    public MappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MappingException(String message) {
        super(message);
    }

    public MappingException(Throwable cause) {
        super(cause);
    }
}
