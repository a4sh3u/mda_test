package de.smava.webapp.commons.domain;

import java.io.Serializable;

public interface BaseEntity extends Serializable{

    Long getId();

    void setId(Long id);

    boolean isNew();

}
