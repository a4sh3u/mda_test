/**
 * 
 */
package de.smava.webapp.commons.constants;

/**
 * @author bvoss
 *
 */
public interface SqlConstants {
	
	final String CONTRACT_SUM_FOR_LENDER_BORROWER = "SELECT " + 
			"sum(c.amount) " + 
			"from contract c " + 
			"inner join \"order\" o on o.id = c.order_id " + 
			"inner join offer on offer.id = c.offer_id " + 
			"where o.account_id = :borrowerId and offer.account_id = :lenderId " + 
			"and c.state in (:states)";

}
