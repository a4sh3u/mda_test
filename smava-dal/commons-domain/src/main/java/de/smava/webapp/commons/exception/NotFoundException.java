package de.smava.webapp.commons.exception;

/**
 * Occurs of a value was required but not found.
 * @author aherr
 *
 */
public class NotFoundException extends ModelException {

	public NotFoundException(String type, String constraint) {
		super(type+":"+constraint+" not found");
	}
	
	public NotFoundException(String type, String constraint, Object value) {
		super(type+":"+constraint+"='"+value+"' not found");
	}	
}
