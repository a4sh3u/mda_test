package de.smava.webapp.commons.exception;

/**
 * Created by dsuszczynski on 02.02.15.
 */
public class SmavaRuntimeException extends RuntimeException {

    public SmavaRuntimeException() {
    }

    public SmavaRuntimeException(String message) {
        super(message);
    }

    public SmavaRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public SmavaRuntimeException(Throwable cause) {
        super(cause);
    }
}
