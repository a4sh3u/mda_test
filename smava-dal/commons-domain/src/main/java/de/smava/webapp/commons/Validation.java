package de.smava.webapp.commons;

/**
 * Created by aherr on 25.09.14.
 */
public final class Validation {

    private Validation() {
    }

    /**
     * Helper method used for validation, useful for validating argument values before putting them to the request
     * context or whatever.
     *
     * After validation, use the return value instead of the given one.
     *
     * If the given value is longer than the specified length, the validation doesnt fail but cut the string to
     * ensure the specified length.
     *
     * @param length maximum length of the request value
     * @param required is the value for that parameter required?
     * @param parameter request parameter name
     * @param value request parameter value
     * @return the given value (maybe changed by checking)
     */
    public static String validate(int length, boolean required, String parameter, String value) throws IllegalArgumentException {
        if (value == null) {
            if (required) {
                throw new IllegalArgumentException(parameter + " is required");
            } else {
                return value;
            }
        }


        return validate(length, value);
    }

    /**
     * Helper method ensuring that the given is not longer than the given length.
     *
     * In contrast to #validate(int, boolean, String, String) the method also accpets null values
     *
     * @param length max number of chars allowed
     * @param value value to check, can be null
     * @return value, maybe trimmed of original was to long
     */
    public static String validate(int length, String value) {
        if (value == null) {
            return null;
        }
        if (value.length() > length) {
            // WEBSITE-12834 [P12]
            value = value.trim().substring(0, length);
        }
        return value;
    }

}
