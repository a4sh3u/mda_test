package de.smava.webapp.commons.exception;

/**
 * Created by aherr on 17.09.14.
 */
public class SecurityException extends SmavaException {

    public SecurityException(String message) {
        super(message);
    }
}
