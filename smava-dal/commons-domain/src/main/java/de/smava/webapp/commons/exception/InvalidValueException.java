package de.smava.webapp.commons.exception;

/**
 * Occurs while a internally if a values is expected but not provided.
 */
public class InvalidValueException extends CoreException {

    public InvalidValueException(String message) {
        super(message);
    }

    public InvalidValueException(Exception cause) {
        super(cause);
    }
}
