package de.smava.webapp.commons.dao;

import de.smava.webapp.commons.pagination.Sortable;

import java.io.Serializable;

/**
 * Default implementation for Sortable.
 * @author robert
 *
 */
public class DefaultSortable implements Sortable, Serializable {

	private String _sort = null;
	private boolean _isDescending = false;
	
	public DefaultSortable(String sort, boolean isDescending) {
		_sort = sort;
		_isDescending = isDescending;
	}
	
	public String getSort() {
		return _sort;
	}

	public boolean hasSort() {
		return _sort != null;
	}

	public boolean sortDescending() {
		return _isDescending;
	}

}
