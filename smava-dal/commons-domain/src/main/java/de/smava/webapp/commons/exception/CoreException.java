package de.smava.webapp.commons.exception;

/**
 * Abstract error indicating violation of business constraints.
 * @author aherr
 *
 */
public abstract class CoreException extends SmavaException {

	public CoreException() {
		super();
	}

	public CoreException(String message, Throwable cause) {
		super(message, cause);
	}

	public CoreException(String message) {
		super(message);
	}

	public CoreException(Throwable cause) {
		super(cause);
	}

}
