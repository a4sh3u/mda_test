package de.smava.webapp.commons.exception;

/**
 * Basic exception for all exceptions of the platform.
 * @author aherr
 *
 */
public abstract class SmavaException extends Exception {

	public SmavaException() {
		super();
	}

	public SmavaException(String message, Throwable cause) {
		super(message, cause);
	}

	public SmavaException(String message) {
		super(message);
	}

	public SmavaException(Throwable cause) {
		super(cause);
	}
}
