package de.smava.webapp.commons.exception;

/**
 * An error occured while generating a pdf documents
 * Created by aherr on 20.11.14.
 */
public class PdfException extends CoreException {

    public PdfException() {
        super();
    }

    public PdfException(Exception cause) {
        super(cause);
    }

    public PdfException(String msg, Exception cause) {
        super(msg, cause);
    }

    public PdfException(String message) {
        super(message);
    }

}
