package de.smava.webapp.commons.pagination;

import java.util.Collection;

/**
 * This interface is used for the collection of data entities. So only part of the whole result can be fetched from the
 * database.
 *
 * @author jan.groth
 */
public interface Pageable {

    long getPrevious();

    long getNext();

    int getItemsPerPage();

    long getOffset();

    boolean hasPrevious();

    boolean hasNext();

    Collection<?> getPageContent();

    long getSize();
}