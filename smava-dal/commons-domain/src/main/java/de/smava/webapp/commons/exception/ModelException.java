package de.smava.webapp.commons.exception;

/**
 * Base exception for all violated model constraints.
 * @author aherr
 *
 */
public abstract class ModelException extends SmavaException {

	public ModelException() {
		super();
	}

	public ModelException(String message, Throwable cause) {
		super(message, cause);
	}

	public ModelException(String message) {
		super(message);
	}

	public ModelException(Throwable cause) {
		super(cause);
	}

}
