package de.smava.webapp.commons.web;

import com.aperto.webkit.utils.StringTools;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import java.util.Collection;

/**
 * Base class for Commands holding a list which should support paging.
 *
 * @param <T> The class of the page content objects
 * @author michael.schwalbe (18.10.2005)
 */
public class AbstractListCommand<T> implements Pageable, Sortable {
    /* BEGIN STATIC BLOCK */
    public static final String OFFSET = "offset";

    public static final String SORT = "sort";
    public static final String SORT_DESCENDING = "sortDescending";
    /* END STATIC BLOCK */

    private long _offset;
    private int _itemsPerPage = 20;
    private long _size;
    private Collection<T> _pageContent;
    private long _lastPageOffset;

    private String _sortBy;
    private boolean _sortDescending;

    public boolean sortDescending() {
        return _sortDescending;
    }

    public void setSortDescending(boolean sortDescending) {
        _sortDescending = sortDescending;
    }

    public String getSort() {
        return _sortBy;
    }

    public void setSort(String sortBy) {
        _sortBy = sortBy;
    }

    public boolean hasSort() {
        return !StringTools.isEmpty(_sortBy);
    }

    public boolean hasNext() {
        return _offset + _itemsPerPage < _size;
    }

    public boolean hasPrevious() {
        return _offset > 0L;
    }

    public int getItemsPerPage() {
        return _itemsPerPage;
    }

    public long getOffset() {
        return _offset;
    }

    public long getPrevious() {
        return _offset - _itemsPerPage;
    }

    public long getNext() {
        return _offset + _itemsPerPage;
    }

    public Collection<T> getPageContent() {
        return _pageContent;
    }

    /**
     * Method for setting the current page�s content as a collection holding the list items.
     *
     * @param pageContent a collection holding the list items
     */
    public void setPageContent(Collection<T> pageContent) {
        _pageContent = pageContent;
    }

    public long getSize() {
        return _size;
    }

    /**
     * Method for setting the items to be displayed per page.
     *
     * @param itemsPerPage an int specifying the items to be displayed per page
     */
    public void setItemsPerPage(int itemsPerPage) {
        _itemsPerPage = itemsPerPage;
    }

    /**
     * Method for setting the current offset in the list, i.e. at which offset the current page starts.
     *
     * @param offSet an int specifying the offset
     */
    public void setOffset(long offSet) {
        _offset = offSet;
    }

    /**
     * Method for setting the size of the whole result, which is displayed over one or several pages.
     * This value is necessary for calucating the pager information
     *
     * @param size a long specifying the current result�s size.
     */
    public void setSize(long size) {
        _size = size;
    }

    public long getLastPageOffset() {
        return _lastPageOffset;
    }

    public void setLastPageOffset(long lastPageOffset) {
        _lastPageOffset = lastPageOffset;
    }
}