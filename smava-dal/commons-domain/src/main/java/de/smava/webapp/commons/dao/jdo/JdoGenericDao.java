package de.smava.webapp.commons.dao.jdo;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.domain.BaseEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
import org.datanucleus.exceptions.NucleusObjectNotFoundException;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

public abstract class JdoGenericDao<T extends BaseEntity> extends JdoBaseDao implements BaseDao<T> {

    private PersistenceManagerFactory persistenceManagerFactory;

    private static final Logger LOGGER = Logger.getLogger(JdoGenericDao.class);

    public <E extends BaseEntity> void saveInNewThread(E entity) {
        if (entity != null) {
            JdoNewThreadPersister persister = new JdoNewThreadPersister(persistenceManagerFactory, entity);
            Thread thread = new Thread(persister);
            thread.start();
        }
    }

    public <T extends Entity> void deleteEntity(final Class<T> clazz, final Long id) {
        final PersistenceManager pm = getPersistenceManager();
        T entity = pm.getObjectById(clazz, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Deleting entity: " + entity);
        }

        pm.deletePersistent(entity);
        pm.evict(entity);
    }

    public <T extends Entity> T getEntity(final Class<T> clazz, final Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEntity: " + clazz + " id=" + id);
        }

        T entity = getPersistenceManager().getObjectById(clazz, id);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Loaded entity: " + entity);
        }
        return entity;
    }

    public <T> T findOne(Class<T> clazz, Long id) {
        try {
            return getPersistenceManager().getObjectById(clazz, id);
        } catch (NucleusObjectNotFoundException e) {
            return null;
        }
    }

    public <T> void delete(Class<T> clazz, Long id){
        T obj = getPersistenceManager().getObjectById(clazz, id);
        getPersistenceManager().deletePersistent(obj);
    }

    @Override
    public void setPersistenceManagerFactory(PersistenceManagerFactory persistenceManagerFactory) {
        this.persistenceManagerFactory = persistenceManagerFactory;
        super.setPersistenceManagerFactory(persistenceManagerFactory);
    }
}