/*
 * @(#)PropertyResourceBundle.java	1.27 04/05/05
 *
 * Copyright 2004 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

/*
 * (C) Copyright Taligent, Inc. 1996, 1997 - All Rights Reserved
 * (C) Copyright IBM Corp. 1996 - 1998 - All Rights Reserved
 *
 * The original version of this source code and documentation
 * is copyrighted and owned by Taligent, Inc., a wholly-owned
 * subsidiary of IBM. These materials are provided under terms
 * of a License Agreement between Taligent and Sun. This technology
 * is protected by multiple US and International patents.
 *
 * This notice and attribution to Taligent may not be removed.
 * Taligent is a registered trademark of Taligent, Inc.
 */

package de.smava.webapp.commons.util;

import com.aperto.webkit.utils.IoTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;

/**
 * A specialized PropertyResourceBundle implementation holding the properties in a sorted map,
 * to ensure the order of elements being the same as in the properties file.
 *
 * @author (michael.schwalbe)
 */
public class ArtPropertyResourceBundle extends ResourceBundle {
    /* BEGIN STATIC BLOCK */
    private static final String WHITESPACE_CHARS = " \t\r\n\f";
    private static final String KEY_VALUE_SEPARATORS = "=: \t\r\n\f";
    private static final String STRICT_KEY_VALUE_SEPARATORS = "=:";

    private static final Map<Character, Integer> LOOKUP = new HashMap<Character, Integer>(22);
    static {
        for (char character = '0'; character <= '9'; character++) {
            LOOKUP.put(character, 1);
        }
        for (char character = 'a'; character <= 'f'; character++) {
            LOOKUP.put(character, 2);
        }
        for (char character = 'A'; character <= 'F'; character++) {
            LOOKUP.put(character, 3);
        }
    }

    private static Map<String, String> readFileToMap(Reader isoReader) throws IOException {
        Map<String, String> result = new LinkedHashMap<String, String>();
        try {
            BufferedReader in = new BufferedReader(isoReader);
            try {
                String line = in.readLine();
                while (line != null) {
                    // Ignore empty lines ...
                    if (line.length() != 0) {
                        // Find start of key ...
                        int n = line.length();
                        int keyStart;
                        for (keyStart = 0; keyStart < n; ++keyStart) {
                            if (WHITESPACE_CHARS.indexOf(line.charAt(keyStart)) == -1) {
                                // Continue lines that end in slashes if they are not comments ...
                                checkContinueLine(line, keyStart, in, n, result);
                                break;
                            }
                        }
                    }
                    // Get the next line...
                    line = in.readLine();
                }
            } finally {
                IoTools.closeQuietly(in);
            }
        } finally {
            IoTools.closeQuietly(isoReader);
        }
        return result;
    }

    private static void checkContinueLine(String line, int keyStart, BufferedReader in, int n, Map<String, String> result) throws IOException {
        char firstChar = line.charAt(keyStart);
        if (firstChar != '#' && firstChar != '!') {
            while (continueLine(line)) {
                String nextLine = in.readLine();
                if (nextLine == null) {
                    nextLine = "";
                }
                String loppedLine = line.substring(0, n - 1);
                // Advance beyond whitespace on new line ...
                int startIndex;
                startIndex = getValueIndex(0, nextLine.length(), nextLine);
                nextLine = nextLine.substring(startIndex, nextLine.length());
                line = loppedLine + nextLine;
                n = line.length();
            }
            // Find separation between key and value ...
            int separatorIndex;
            separatorIndex = getSeparatorIndex(keyStart, n, line);
            // Skip over whitespace after key if any ...
            int valueIndex;
            valueIndex = getValueIndex(separatorIndex, n, line);
            // Skip over one non whitespace key value separators if any ...
            if (valueIndex < n && STRICT_KEY_VALUE_SEPARATORS.indexOf(line.charAt(valueIndex)) != -1) {
                valueIndex++;
            }
            // Skip over white space after other separators if any ...
            while (valueIndex < n) {
                if (WHITESPACE_CHARS.indexOf(line.charAt(valueIndex)) == -1) {
                    break;
                }
                valueIndex++;
            }
            String key = line.substring(keyStart, separatorIndex);
            String value;
            if (separatorIndex < n) {
                value = line.substring(valueIndex, n);
            } else {
                value = "";
            }
            // Convert, then store key and value ...
            key = convert(key);
            value = convert(value);
            result.put(key, value);
        }
    }

    private static int getValueIndex(int separatorIndex, int n, String line) {
        int valueIndex;
        for (valueIndex = separatorIndex; valueIndex < n; valueIndex++) {
            if (WHITESPACE_CHARS.indexOf(line.charAt(valueIndex)) == -1) {
                break;
            }
        }
        return valueIndex;
    }

    private static int getSeparatorIndex(int keyStart, int n, String line) {
        int separatorIndex;
        for (separatorIndex = keyStart; separatorIndex < n; separatorIndex++) {
            char currentChar = line.charAt(separatorIndex);
            if (currentChar == '\\') {
                separatorIndex++;
            } else if (KEY_VALUE_SEPARATORS.indexOf(currentChar) != -1) {
                break;
            }
        }
        return separatorIndex;
    }

    /**
     * Returns <code>true</code> if the given line is a line,
     * that must be appended to the next line.
     */
    /**
     * Returns <code>true</code> if the given line is a line,
     * that must be appended to the next line.
     * @param line the string to be checked
     * @return true if the line is to be continued, false otherwise
     */
    private static boolean continueLine(String line) {
        int slashCount = 0;
        int index = line.length() - 1;
        while (index >= 0 && line.charAt(index--) == '\\') {
            slashCount++;
        }
        return slashCount % 2 != 0;
    }

    /*
    * Converts encoded &#92;uxxxx to unicode chars
    * and changes special saved chars to their original forms.
    */

    /**
     * Converts encoded &#92;uxxxx to unicode chars
     * and changes special saved chars to their original forms.
     * @param s the string to be converted
     * @return the converted string
     */
    private static String convert(String s) {
        char c;
        int n = s.length();
        StringBuffer sb = new StringBuffer(n);
        int x = 0;
        while (x < n) {
            c = s.charAt(x);
            x++;
            if (c == '\\') {
                c = s.charAt(x);
                x++;
                if (c == 'u') {
                    // Read the xxxx ...
                    int value = 0;
                    for (int i = 0; i < 4; ++i) {
                        c = s.charAt(x);
                        x++;
                        value = getValueForChar(c, value);
                    }
                    sb.append((char) value);
                } else {
                    c = getWhitespaceChar(c);
                    sb.append(c);
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static char getWhitespaceChar(char c) {
        if (c == 't') {
            c = '\t';
        } else if (c == 'r') {
            c = '\r';
        } else if (c == 'n') {
            c = '\n';
        } else if (c == 'f') {
            c = '\f';
        }
        return c;
    }

    private static int getValueForChar(char c, int value) {
        int lookupValue = LOOKUP.get(c);
        switch (lookupValue) {
            case 1:
                value = (value << 4) + c - '0';
                break;
            case 2:
                value = (value << 4) + 10 + c - 'a';
                break;
            case 3:
                value = (value << 4) + 10 + c - 'A';
                break;
            default:
                throw new IllegalArgumentException("Malformed \\uxxxx encoding.");
        }
        return value;
    }

    /* END STATIC BLOCK */

    private final Map<String, String> _lookup;

    /**
     * Creates a property resource bundle.
     * @param reader property file to read from.
     */
    public ArtPropertyResourceBundle(Reader reader) throws IOException {
        _lookup = readFileToMap(reader);
    }

    // Implements java.util.ResourceBundle.handleGetObject; inherits javadoc specification.
    public Object handleGetObject(String key) {
        if (key == null) {
            throw new IllegalArgumentException("The parameter key must not be null!");
        }
        return _lookup.get(key);
    }

    /**
     * Implementation of ResourceBundle.getKeys.
     */
    public Enumeration<String> getKeys() {
        ResourceBundle parentBundle = this.parent;
        Enumeration<String> keys = null;
        if (parentBundle != null) {
            keys = parentBundle.getKeys();
        }
        return new ArtResourceBundleEnumeration(_lookup.keySet(), keys);
    }
}