/**
 * 
 */
package de.smava.webapp.commons.util;

/**
 * @author bvoss
 *
 */
public final class MathUtils {
	
	public static final double PRECISION = Math.pow(10, 8);
	
	public static final double TOLERANCE = 1 / PRECISION;
	
	private MathUtils() {
		super();
	}
	
	public static boolean nearlySame(final double d1, final double d2) {
		return Math.abs(d1 - d2) < TOLERANCE;
	}
	
	public static boolean nearlySero(final double value) {
		return Math.abs(value) < TOLERANCE;
	}
	
	/**
     * rounds the given value to 8 after comma digits. 
     * 
     * @param amount the amount
     * @return the rounded amount
     */
    public static double roundAmount(double amount) {
        double result = amount;        
        result = result * PRECISION;
        result = Math.round(result) / PRECISION;
        return result;        
    }

}
