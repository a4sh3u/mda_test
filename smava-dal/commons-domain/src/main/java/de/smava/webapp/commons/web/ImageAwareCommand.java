package de.smava.webapp.commons.web;

/**
 * An interface for the command objects that handles images.
 * <p/>
 * Date: 09.01.2007 15:46:52
 *
 * @author joern.stampehl
 */
public interface ImageAwareCommand {
	
	String IMAGE_CATEGORY_GROUP = "group";
	String IMAGE_CATEGORY_PROFILE = "profile";
	String IMAGE_CATEGORY_ORDER = "orders";
	String IMAGE_CATEGORY_MARKETING = "marketing/adformats";
	String IMAGE_CATEGORY_CATEGORY = "category";
	
    String getNoImageExpression();
    String getImageFileExpression();
    String getImageCategory();
    byte[] getFile();
    void updateNoImage();
    void resetForm();

    /**
     * current image data.
     */
    String getImage();
    void setImage(String imageName);
    
    int getImageHeight();
    void setImageHeight(int height);
    
    int getImageWidth();
    void setImageWidth(int width);

	boolean isNewUpload();
	void setNewUpload(boolean isNewUpload);
}
