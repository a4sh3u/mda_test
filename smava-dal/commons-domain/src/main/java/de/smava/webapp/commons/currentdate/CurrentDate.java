package de.smava.webapp.commons.currentdate;

import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Date;


/**
 * provides static access to the current date.
 *
 * @author rfiedler
 *
 */
public final class CurrentDate {

	private static final Logger LOGGER = Logger.getLogger(CurrentDate.class);

	private static volatile boolean initDone = false;

	private static boolean allowFakeDate = false;
	private static volatile boolean useFakeDate = false;
	private static volatile boolean useFakeTime = false;

	private static volatile Date fakeDate;

	private CurrentDate() {

	}


	public static Date getDate() {
		final Date result;
		if (useFakeDate()) {
			result = getFake().getTime();
		} else {
			result = new Date();
		}
		return result;
	}

	public static long getTime() {
		final long result;
		if (useFakeDate()) {
			result = getFake().getTimeInMillis();
		} else {
			result = System.currentTimeMillis();
		}
		return result;
	}

	public static Calendar getCalendar() {
		final Calendar result;
		if (useFakeDate()) {
			result = getFake();
		} else {
			result = Calendar.getInstance();
		}
		return result;
	}

	private static boolean useFakeDate() {
		if (!initDone) {
			LOGGER.trace("##################################### using CurrentDate before init done!!!");
		}
		return allowFakeDate && useFakeDate && fakeDate != null;
	}

	private static Calendar getFake() {
		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		int millis = cal.get(Calendar.MILLISECOND);
		cal.setTime(fakeDate);
		if(!useFakeTime) {
			cal.set(Calendar.HOUR_OF_DAY, hour);
			cal.set(Calendar.MINUTE, minute);
			cal.set(Calendar.SECOND, second);
			cal.set(Calendar.MILLISECOND, millis);
		}
		return cal;
	}


	static void setAllowFakeDate(final boolean allowFakeDate) {
		CurrentDate.allowFakeDate = allowFakeDate;
		initDone = true;
	}


	static void setUseFakeDate(final boolean useFakeDate) {
		CurrentDate.useFakeDate = useFakeDate;
	}
	static void setUseFakeTime(final boolean useFakeTime) {
		CurrentDate.useFakeTime = useFakeTime;
	}

	static void setFakeDate(final Date fakeDate) {
		CurrentDate.fakeDate = fakeDate;
	}
}
