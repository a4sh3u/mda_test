/**
 *
 */
package de.smava.webapp.commons.util;

import java.math.RoundingMode;
import java.text.*;
import java.util.Currency;

/**
 * Thread safe subclass of {@link java.text.DecimalFormat} which is optimized for use in
 * multithreaded environments that use thread pools.
 *
 * Uses internally a {@link ThreadLocal} so syncronisation isn't need and new
 * insatnaces of {@link java.text.DecimalFormat} are only created once per {@link Thread}
 *
 *
 * @author bvoss
 *
 */
public class ThreadLocalDecimalFormat extends DecimalFormat implements Cloneable {

	private static final long serialVersionUID = 1L;

	private static final String UNSUPPORTED_MESSAGE = ThreadLocalDecimalFormat.class + " can't be changed.";
	private final DecimalFormatThreadLocal _localDf;

	private boolean _initFinished = false;

	public ThreadLocalDecimalFormat() {
		this(null, null);
	}

	public ThreadLocalDecimalFormat(final String pattern, final DecimalFormatSymbols symbols) {
		_localDf = new DecimalFormatThreadLocal(pattern, symbols);
		_initFinished = true;
	}

	public ThreadLocalDecimalFormat(final String pattern) {
		this(pattern, null);
	}



	@Override
	public void applyLocalizedPattern(final String pattern) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.applyLocalizedPattern(pattern);
		}
	}

	@Override
	public void applyPattern(final String pattern) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.applyPattern(pattern);
		}
	}

	@Override
	public Object clone() {
		throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
	}

	@Override
	public boolean equals(final Object obj) {
		return _localDf.get().equals(obj);
	}

	@Override
	public StringBuffer format(final double number, final StringBuffer result,
			final FieldPosition fieldPosition) {
		return _localDf.get().format(number, result, fieldPosition);
	}

	@Override
	public StringBuffer format(final long number, final StringBuffer result,
			final FieldPosition fieldPosition) {
		return _localDf.get().format(number, result, fieldPosition);
	}

	@Override
	public AttributedCharacterIterator formatToCharacterIterator(final Object obj) {
		return _localDf.get().formatToCharacterIterator(obj);
	}

	@Override
	public Currency getCurrency() {
		return _localDf.get().getCurrency();
	}

	@Override
	public DecimalFormatSymbols getDecimalFormatSymbols() {
		return _localDf.get().getDecimalFormatSymbols();
	}

	@Override
	public int getGroupingSize() {
		return _localDf.get().getGroupingSize();
	}

	@Override
	public int getMaximumFractionDigits() {
		return _localDf.get().getMaximumFractionDigits();
	}

	@Override
	public int getMaximumIntegerDigits() {
		return _localDf.get().getMaximumIntegerDigits();
	}

	@Override
	public int getMinimumFractionDigits() {
		return _localDf.get().getMinimumFractionDigits();
	}

	@Override
	public int getMinimumIntegerDigits() {
		return _localDf.get().getMinimumIntegerDigits();
	}

	@Override
	public int getMultiplier() {
		return _localDf.get().getMultiplier();
	}

	@Override
	public String getNegativePrefix() {
		return _localDf.get().getNegativePrefix();
	}

	@Override
	public String getNegativeSuffix() {
		return _localDf.get().getNegativeSuffix();
	}

	@Override
	public String getPositivePrefix() {
		return _localDf.get().getPositivePrefix();
	}

	@Override
	public String getPositiveSuffix() {
		return _localDf.get().getPositiveSuffix();
	}

	@Override
	public RoundingMode getRoundingMode() {
		return _localDf.get().getRoundingMode();
	}

	@Override
	public int hashCode() {
		return _localDf.get().hashCode();
	}

	@Override
	public boolean isDecimalSeparatorAlwaysShown() {
		return _localDf.get().isDecimalSeparatorAlwaysShown();
	}

	@Override
	public boolean isParseBigDecimal() {
		return _localDf.get().isParseBigDecimal();
	}

	@Override
	public Number parse(final String text, final ParsePosition pos) {
		return _localDf.get().parse(text, pos);
	}

	@Override
	public void setCurrency(final Currency currency) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setCurrency(currency);
		}
	}

	@Override
	public void setDecimalFormatSymbols(final DecimalFormatSymbols newSymbols) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setDecimalFormatSymbols(newSymbols);
		}
	}

	@Override
	public void setDecimalSeparatorAlwaysShown(final boolean newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setDecimalSeparatorAlwaysShown(newValue);
		}
	}

	@Override
	public void setGroupingSize(final int newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setGroupingSize(newValue);
		}
	}

	@Override
	public void setMaximumFractionDigits(final int newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setMaximumFractionDigits(newValue);
		}
	}

	@Override
	public void setMaximumIntegerDigits(final int newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setMaximumIntegerDigits(newValue);
		}
	}

	@Override
	public void setMinimumFractionDigits(final int newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setMinimumFractionDigits(newValue);
		}
	}

	@Override
	public void setMinimumIntegerDigits(final int newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setMinimumIntegerDigits(newValue);
		}
	}

	@Override
	public void setMultiplier(final int newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setMultiplier(newValue);
		}
	}

	@Override
	public void setNegativePrefix(final String newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setNegativePrefix(newValue);
		}
	}

	@Override
	public void setNegativeSuffix(final String newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setNegativeSuffix(newValue);
		}
	}

	@Override
	public void setParseBigDecimal(final boolean newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setParseBigDecimal(newValue);
		}
	}

	@Override
	public void setPositivePrefix(final String newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setPositivePrefix(newValue);
		}
	}

	@Override
	public void setPositiveSuffix(final String newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setPositiveSuffix(newValue);
		}
	}

	@Override
	public void setRoundingMode(final RoundingMode roundingMode) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setRoundingMode(roundingMode);
		}
	}

	@Override
	public String toLocalizedPattern() {
		return _localDf.get().toLocalizedPattern();
	}

	@Override
	public String toPattern() {
		return _localDf.get().toPattern();
	}

	@Override
	public boolean isGroupingUsed() {
		return _localDf.get().isGroupingUsed();
	}

	@Override
	public boolean isParseIntegerOnly() {
		return _localDf.get().isParseIntegerOnly();
	}

	@Override
	public Number parse(final String source) throws ParseException {
		return _localDf.get().parse(source);
	}

	@Override
	public void setGroupingUsed(final boolean newValue) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setGroupingUsed(newValue);
		}
	}

	@Override
	public void setParseIntegerOnly(final boolean value) {
		if (_initFinished) {
			throw new UnsupportedOperationException(UNSUPPORTED_MESSAGE);
		} else {
			super.setParseIntegerOnly(value);
		}
	}



	/**
	 * @author bvoss
	 *
	 */
	private static class DecimalFormatThreadLocal extends ThreadLocal<DecimalFormat> {

		private final String _pattern;
		private final DecimalFormatSymbols _symbols;

		public DecimalFormatThreadLocal(final String pattern,
				final DecimalFormatSymbols symbols) {
			_pattern = pattern;
			_symbols = symbols;
		}

		@Override
		protected DecimalFormat initialValue() {
			final DecimalFormat returnVal;
			if (_pattern != null) {
				if (_symbols != null) {
					returnVal = new DecimalFormat(_pattern, _symbols);
				} else {
					returnVal = new DecimalFormat(_pattern);
				}
			} else {
				returnVal = new DecimalFormat();
			}
			return returnVal;
		}



	}

}
