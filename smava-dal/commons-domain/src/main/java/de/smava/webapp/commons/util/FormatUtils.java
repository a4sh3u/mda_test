package de.smava.webapp.commons.util;

import com.aperto.webkit.utils.ExceptionEater;
import de.smava.webapp.commons.exception.SmavaRuntimeException;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Minutes;

import java.text.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for parsing and formatting.
 *
 * @author Daniel Doubleday
 */
public final class FormatUtils {
    private static final String CANNOT_PARSE_BLANK_STRINGS = "Cannot parse blank strings";

    private static final String OVERLAY_CHARACTER = "X";
    private static final int MIN_NUMBER_OF_CHARS = 2;
    public static final DateFormat SHORT_DATE_FORMAT = SimpleDateFormat.getDateInstance(DateFormat.SHORT, Locale.GERMANY);
    public static final DateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMANY);
    public static final DateFormat WITHOUT_YEAR_DATE_FORMAT = new SimpleDateFormat("dd.MM", Locale.GERMANY);
    public static final DateFormat MONTH_YEAR_DATE_FORMAT = new SimpleDateFormat("MM/yyyy", Locale.GERMANY);
    public static final DateFormat LONG_DATE_FORMAT = SimpleDateFormat.getDateInstance(DateFormat.LONG, Locale.GERMANY);
    public static final Map<Locale, DateFormat> LONG_DATE_FORMATS = new HashMap<Locale, DateFormat>();
    public static final Map<Locale, DateFormat> MONTH_FORMATS = new HashMap<Locale, DateFormat>();
    public static final Map<Locale, DateFormat> SIMPLE_MONTH_FORMATS = new HashMap<Locale, DateFormat>();
    public static final DateFormat SIMPLE_MONTH_FORMAT = new SimpleDateFormat("MMMM", Locale.GERMANY);
    public static final DateFormat MONTH_DATE_FORMAT = new SimpleDateFormat("MMMM yyyy", Locale.GERMANY);
    public static final DateFormat MEDIUM_DATE_FORMAT = new SimpleDateFormat("EEEE, dd.MM.yyyy", Locale.GERMANY);
    public static final DateFormat ELIXIR_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd", Locale.GERMANY);
    public static final DateFormat IDCARD_DATE_FORMAT = new SimpleDateFormat("yyMMdd", Locale.GERMANY);
    public static final DateFormat DB_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.GERMANY);

    public static final Map<Locale, DateFormat> DATE_TIME_FORMATS = new HashMap<Locale, DateFormat>();
    public static final DateFormat DATE_TIME_FORMAT = SimpleDateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, Locale.GERMANY);
    public static final DateFormat DATE_TIME_FORMAT_MEDIUM = SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT, Locale.GERMANY);
    public static final DateFormat DATE_TIME_FORMAT_SHORT = SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.GERMANY);

    public static final NumberFormat RATE_FORMAT = new ThreadLocalDecimalFormat("0.0", new DecimalFormatSymbols(Locale.GERMANY));
    public static final NumberFormat LONG_RATE_FORMAT = new ThreadLocalDecimalFormat("0.00", new DecimalFormatSymbols(Locale.GERMANY));
    /**
     * AMOUNT_FORMAT - round up third digit after point
     */
    public static final NumberFormat AMOUNT_FORMAT = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(Locale.GERMANY));
    public static final NumberFormat SHORT_AMOUNT_FORMAT = new ThreadLocalDecimalFormat("#,##0", new DecimalFormatSymbols(Locale.GERMANY));
    public static final NumberFormat DOUBLE_FORMAT = new ThreadLocalDecimalFormat("#,##0.#", new DecimalFormatSymbols(Locale.GERMANY));
    public static final NumberFormat DOUBLE_LONG_FORMAT = new ThreadLocalDecimalFormat("#,##0.##", new DecimalFormatSymbols(Locale.GERMANY));
    public static final NumberFormat DOUBLE_SHORT_FORMAT = new ThreadLocalDecimalFormat("#0", new DecimalFormatSymbols(Locale.GERMANY));
    public static final NumberFormat CUSTOMER_NUMBER_FORMAT = new ThreadLocalDecimalFormat("0000000");

    public static final Pattern DATE_TIME_PATTERN = Pattern.compile("([0-3]?[0-9])\\.([0-1]?[0-9])\\.([1-2][0-9]{3})(\\s([0-1][0-9]|2[0-3]):([0-5][0-9]))?");
    public static final Pattern DATE_PATTERN = Pattern.compile("([0-3]?[0-9])\\.([0-1]?[0-9])\\.([1-2][0-9]{3})");

    static {
        Locale polish = new Locale("pl", "PL");
        Locale german = new Locale("de", "DE");

        LONG_DATE_FORMATS.put(german, SimpleDateFormat.getDateInstance(DateFormat.LONG, german));
        LONG_DATE_FORMATS.put(polish, SimpleDateFormat.getDateInstance(DateFormat.LONG, polish));

        MONTH_FORMATS.put(german, new SimpleDateFormat("MMMM yyyy", german));
        MONTH_FORMATS.put(polish, new SimpleDateFormat("MMMM yyyy", polish));

        SIMPLE_MONTH_FORMATS.put(german, new SimpleDateFormat("MMMM", german));
        SIMPLE_MONTH_FORMATS.put(polish, new SimpleDateFormat("MMMM", polish));

        DATE_TIME_FORMATS.put(german, SimpleDateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, german));
        DATE_TIME_FORMATS.put(polish, SimpleDateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, polish));

    }

    public static final int HIGHEST_SPECIAL = '>';
    public static final char[][] SPECIAL_CHARACTERS_REPRESENTATION = new char[HIGHEST_SPECIAL + 1][];

    static {
        SPECIAL_CHARACTERS_REPRESENTATION['&'] = "&amp;".toCharArray();
        SPECIAL_CHARACTERS_REPRESENTATION['<'] = "&lt;".toCharArray();
        SPECIAL_CHARACTERS_REPRESENTATION['>'] = "&gt;".toCharArray();
        SPECIAL_CHARACTERS_REPRESENTATION['"'] = "&#034;".toCharArray();
        SPECIAL_CHARACTERS_REPRESENTATION['\''] = "&#039;".toCharArray();
    }

    private static final String EMPTY_STRING = "";

    private FormatUtils() {
    }

    public static String formatLong(final Long l) {
        String s = "-1";
        if (l != null) {
            s = Long.toString(l);
        }
        return s;
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseShortDateFormat(final String date) throws ParseException {
        // convert npe tp ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        synchronized (SHORT_DATE_FORMAT) {
            return SHORT_DATE_FORMAT.parse(date.trim());
        }
    }

    public static String formatCustomerNumber(final Long number) {
        String cn = CUSTOMER_NUMBER_FORMAT.format(0L);
        if (number != null) {
            cn = CUSTOMER_NUMBER_FORMAT.format(number);
        }
        return cn;
    }

    public static String formatDbDateFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (DB_DATE_FORMAT) {
                result = DB_DATE_FORMAT.format(date);
            }
        } else {
            result = EMPTY_STRING;
        }
        return result;
    }

    /**
     * Formats a date instance according to short german format. Null dates are formatted as
     * empty strings.
     */
    public static String formatShortDateFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (SHORT_DATE_FORMAT) {
                result = SHORT_DATE_FORMAT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Formats a date instance according to short german format without year. Null dates are formatted as
     * empty strings.
     */
    public static String formatDateWithoutYearFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (WITHOUT_YEAR_DATE_FORMAT) {
                result = WITHOUT_YEAR_DATE_FORMAT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Formats a date instance according to short german format with only month and year. Null dates are formatted as
     * empty strings.
     */
    public static String formatMonthYearDateFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (MONTH_YEAR_DATE_FORMAT) {
                result = MONTH_YEAR_DATE_FORMAT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseDefaultDateFormat(final String date) throws ParseException {
        // convert npe tp ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        synchronized (DEFAULT_DATE_FORMAT) {
            return DEFAULT_DATE_FORMAT.parse(date.trim());
        }
    }

    /**
     * Formats a date instance according to short german format with long year. Null dates are formatted as
     * empty strings.
     */
    public static String formatDefaultDateFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (DEFAULT_DATE_FORMAT) {
                result = DEFAULT_DATE_FORMAT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseElixirDateFormat(final String date) throws ParseException {
        // convert npe to ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        synchronized (ELIXIR_DATE_FORMAT) {
            return ELIXIR_DATE_FORMAT.parse(date.trim());
        }
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseIdCardDateFormat(final String date) throws ParseException {
        // convert npe to ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        synchronized (IDCARD_DATE_FORMAT) {
            return IDCARD_DATE_FORMAT.parse(date.trim());
        }
    }


    /**
     * Formats a date instance according to short german format with long year. Null dates are formatted as
     * empty strings.
     */
    public static String formatElixirDateFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (ELIXIR_DATE_FORMAT) {
                result = ELIXIR_DATE_FORMAT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    public static String formatMediumDateFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (MEDIUM_DATE_FORMAT) {
                result = MEDIUM_DATE_FORMAT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Formats a date instance according to long german format. Null dates are formatted as
     * empty strings.
     */
    public static String formatLongDateFormat(final Date date, final Locale locale) {
        final String result;
        if (date != null) {
            DateFormat dateFormat = LONG_DATE_FORMATS.get(locale);
            if (dateFormat == null) {
                dateFormat = LONG_DATE_FORMAT;
            }
            synchronized (dateFormat) {
                result = dateFormat.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Formats a date instance according to short german format with long year. Null dates are formatted as
     * empty strings.
     */
    public static String formatMonthDateFormat(final Date date, final Locale locale) {
        final String result;
        if (date != null) {
            DateFormat dateFormat = MONTH_FORMATS.get(locale);
            if (dateFormat == null) {
                dateFormat = MONTH_DATE_FORMAT;
            }
            synchronized (dateFormat) {
                result = dateFormat.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    public static String formatSimpleMonthFormat(final Date date, final Locale locale) {
        final String result;
        if (date != null) {
            DateFormat dateFormat = SIMPLE_MONTH_FORMATS.get(locale);
            if (dateFormat == null) {
                dateFormat = SIMPLE_MONTH_FORMAT;
            }
            synchronized (dateFormat) {
                result = dateFormat.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    public static Date parseMonthDateFormat(final String monthAndYear) {
        Date date;
        try {
            synchronized (MONTH_DATE_FORMAT) {
                date = MONTH_DATE_FORMAT.parse(monthAndYear);
            }
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseDateTimeFormat(final String date, final Locale locale) throws ParseException {
        // convert npe tp ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        DateFormat format = DATE_TIME_FORMATS.get(locale);
        if (format == null) {
            format = DATE_TIME_FORMAT;
        }

        synchronized (format) {
            return format.parse(date.trim());
        }
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseMediumDateTimeFormat(final String date) throws ParseException {
        // convert npe tp ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        synchronized (DATE_TIME_FORMAT_MEDIUM) {
            return DATE_TIME_FORMAT_MEDIUM.parse(date.trim());
        }
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static Date parseShortDateTimeFormat(final String date) throws ParseException {
        // convert npe tp ParseException
        if (date == null || date.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        synchronized (DATE_TIME_FORMAT_SHORT) {
            return DATE_TIME_FORMAT_SHORT.parse(date.trim());
        }
    }

    /**
     * Formats a date instance according to short german format with long year. Null dates are formatted as
     * empty strings.
     */
    public static String formatDateTimeFormat(final Date date, final Locale locale) {
        final String result;
        if (date != null) {
            DateFormat format = DATE_TIME_FORMATS.get(locale);
            if (format == null) {
                format = DATE_TIME_FORMAT;
            }
            synchronized (format) {
                result = format.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Formats a date instance according to short german format with long year. Null dates are formatted as
     * empty strings.
     */
    public static String formatShortDateTimeFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (DATE_TIME_FORMAT_SHORT) {
                result = DATE_TIME_FORMAT_SHORT.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    public static String formatMediumDateTimeFormat(final Date date) {
        final String result;
        if (date != null) {
            synchronized (DATE_TIME_FORMAT_MEDIUM) {
                result = DATE_TIME_FORMAT_MEDIUM.format(date);
            }
        } else {
            result = "";
        }
        return result;
    }

    /**
     * Formats the given double to a string. E.g. 1000000 = 1 Mio. 1000 = 1 Tsd.
     * The numbers over 1 Mio. are rounded down in a 100000 steps. E.g. 1560000 = 1.5 Mio.
     * Numbers over 1000 are rounded down in 1000 steps. E.g. 1500 = 1 Tsd.
     *
     * @param amount the double to format
     * @return the formated number.
     */
    public static String formatVolumeTickerOld(double amount) {
        String result = null;
        if (amount >= 1000000000) { // optimistic ;)
            result = formatOverMrd(amount);
        } else if (amount >= 1000000) {
            result = formatOverMio(amount);
        } else {
            result = formatUnderMio(amount);
        }

        return result;
    }

    public static String formatVolumeTicker(double amount) {
        final String result;
        final long divider;
        final String suffix;
        if (amount >= 1000000000) { // optimistic ;)
            divider = 1000000000;
            suffix = " Mrd.";
            result = formatRate(amount / divider) + suffix;
        } else if (amount >= 1000000) {
            divider = 1000000;
            suffix = " Mio.";
            result = formatRate(amount / divider) + suffix;
        } else {
            divider = 1000;
            suffix = " Tsd.";
            double tmpAmount = Math.round(amount / 1000);
            result = formatUnderMio(tmpAmount * 1000);
        }
        return result;
    }

    private static String formatOverMrd(double amount) {
        int mrd = 1000000000;
        int hundredMio = 100000000;
        StringBuilder sb = new StringBuilder();
        int mrds = (int) amount / mrd;
        sb.append(mrds);

        double mio = amount % mrd;
        int hundredMios = (int) (mio / hundredMio);
        if (hundredMios > 0) {
            sb.append(",").append(hundredMios);
        }

        sb.append(" Mrd.");

        return sb.toString();
    }

    private static String formatUnderMio(double amount) {
        int tousend = 1000;
        StringBuilder sb = new StringBuilder();
        int tousends = (int) amount / tousend;

        sb.append(tousends).append(" Tsd.");

        return sb.toString();
    }

    private static String formatOverMio(double amount) {
        int mio = 1000000;
        int hundredTousend = 100000;
        StringBuilder sb = new StringBuilder();
        int mios = (int) amount / mio;
        sb.append(mios);

        double tsd = amount % mio;
        int hundredTousends = (int) (tsd / hundredTousend);
        if (hundredTousends > 0) {
            sb.append(",").append(hundredTousends);
        }

        sb.append(" Mio.");

        return sb.toString();
    }

    /**
     * Parses the given argument which is expected to be a double string.
     */
    public static double parseRate(final String rate) throws ParseException {
        // convert npe to ParseException
        if (rate == null || rate.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        return RATE_FORMAT.parse(rate.trim()).doubleValue();
    }

    /**
     * Parses the given argument which is expected to be a double string.
     */
    public static double parseLongRate(final String rate) throws ParseException {
        // convert npe to ParseException
        if (rate == null || rate.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        return LONG_RATE_FORMAT.parse(rate.trim()).doubleValue();
    }

    /**
     * Formats a number instance according to decimal format with one fraction number.
     */
    public static String formatRate(final double number) {
        return RATE_FORMAT.format(number);
    }

    /**
     * Formats number as rate with two digits.
     */
    public static String formatRateLong(final double number) {
        return LONG_RATE_FORMAT.format(number);
    }

    public static double parseAmountBlankToSero(String rate) throws ParseException {
        final double result;
        if (rate == null || rate.isEmpty()) {
            result = 0.0d;
        } else {
            result = parseAmount(rate);
        }
        return result;
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static double parseAmount(String rate) throws ParseException {
        // convert npe to ParseException
        if (rate == null || rate.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        rate = rate.trim();
        if (!ValidationUtils.isPriceFormat(rate) && !ValidationUtils.isNegativePriceFormat(rate)) {
            throw new ParseException("Wrong price format '" + rate + "'", 0);
        }
        if (Pattern.compile("[0-9]+([\\.\\,][0-9]{1,2})?").matcher(rate).matches()) {
            rate = rate.replaceAll("\\.", ",");
        }

        Number nf = AMOUNT_FORMAT.parse(rate.trim());
        double amount = nf.doubleValue();
        return amount;
    }

    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static double parseAmountAndRound(String rate) throws ParseException {
        // convert npe to ParseException
        if (rate == null || rate.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        rate = rate.trim();
        if (!ValidationUtils.isPriceFormat(rate) && !ValidationUtils.isNegativePriceFormat(rate)) {
            throw new ParseException("Wrong price format", 0);
        }
        // accept any double or german amount
        if (Pattern.compile("[0-9]+([\\.\\,][0-9]*)?").matcher(rate).matches()) {
            rate = rate.replaceAll("\\.", ",");
        }

        // round and format as amount
        Number nf = AMOUNT_FORMAT.parse(rate.trim());
        double amount = nf.doubleValue();
        return amount;
    }


    /**
     * Parses the given argument which is expected to be in short german date formatted string.
     */
    public static double parseDouble(String rate) throws ParseException {
        // convert npe tp ParseException
        if (rate == null || rate.isEmpty()) {
            throw new ParseException(CANNOT_PARSE_BLANK_STRINGS, 0);
        }
        rate = rate.trim();
        if (!ValidationUtils.isDouble(rate)) {
            throw new ParseException("Wrong price format", 0);
        }
        if (Pattern.compile("[0-9]+([\\.\\,][0-9]*)?").matcher(rate).matches()) {
            rate = rate.replaceAll("\\.", ",");
        }

        Number nf = DOUBLE_FORMAT.parse(rate.trim());
        double amount = nf.doubleValue();
        return amount;
    }

    /**
     * Calls {@link #parseAmount(String)}, throws RuntimeException if parse exception.
     */
    public static double asAmount(final String value) {
        double d;
        try {
            d = parseAmount(value);
        } catch (ParseException e) {
            throw new SmavaRuntimeException("ParseException. Should have been handled before by validator!", e);
        }
        return d;
    }

    public static double asRate(final String value) {
        try {
            return parseRate(value);
        } catch (ParseException e) {
            throw new RuntimeException("ParseException. Should have been handled before by validator!", e);
        }
    }


    /**
     * Formats a number instance according to decimal format with two fraction numbers.
     * @throws ArithmeticException as thrown by java.text.NumberFormat.format(double)
     */
    public static String formatAmount(final double number) throws ArithmeticException {
        return AMOUNT_FORMAT.format(number);
    }

    public static String formatAmountSeroToEmpty(final double number) {
        final String result;
        if (number == 0.0d) {
            result = EMPTY_STRING;
        } else {
            result = formatAmount(number);
        }
        return result;
    }

    public static String roundAndformatShortAmount(final double number) {
        return SHORT_AMOUNT_FORMAT.format(roundAmount(number));
    }

    public static String formatShortAmount(final double number) {
        return SHORT_AMOUNT_FORMAT.format(number);
    }

    public static String formatDoubleAmount(final double number) {
        return DOUBLE_LONG_FORMAT.format(number);
    }

    public static String formatShortDoubleAmount(final double number) {
        return DOUBLE_SHORT_FORMAT.format(number);
    }

    /**
     * Formats number with sign.
     */
    public static String formatSignedAmount(final double number) {
        String text = AMOUNT_FORMAT.format(number);
        if (Math.abs(number) < 0.0001) {
            if (text.length() > 0 && text.charAt(0) == '-') {
                text = text.substring(1);
            }
        } else {
            if (number > 0.0) {
                text = "+" + text;
            }
        }
        return text;
    }

    public static String formatAmount(final String number) {
        String s = number;
        try {
            double a = asAmount(number);
            s = formatAmount(a);
        } catch (Exception e) {
            ExceptionEater.eat(e);
        }
        return s;
    }

    /**
     * Search for (regex) needlePattern in haystack and return it. If needlePattern
     * contains "()" then the "embraced" part is returned.
     *
     * @param needlePattern Pattern to search for
     * @param haystack      Text to search
     * @return Found match or null
     */
    public static String grep(final String needlePattern, final String haystack) {
        String rs = null;
        if (needlePattern != null && haystack != null) {
            Pattern pattern = Pattern.compile(needlePattern);
            Matcher matcher = pattern.matcher(haystack);
            if (matcher.find()) {
                rs = matcher.group();
                if (needlePattern.indexOf('(') >= 0) {
                    rs = matcher.group(1);
                }
            }
        }
        return rs;
    }

    public static String formatList(final List<?> values, final String connectionWord) {
        int index = 0;
        int lastIndex = values.size() - 1;
        StringBuilder formatedList = new StringBuilder();
        for (Object value : values) {
            if (index != 0) {
                if (index != lastIndex) {
                    formatedList.append(", ");
                } else if (index == lastIndex) {
                    if (connectionWord != null) {
                        formatedList.append(' ').append(connectionWord).append(' '); // 'und' 'oder' '&' 'bzw.'
                    } else {
                        formatedList.append(", ");
                    }
                }
            }

            formatedList.append(value.toString());
            index++;
        }

        return formatedList.toString();
    }



    /**
     * Performs the following substring replacements
     * (to facilitate output to XML/HTML pages):
     * <p/>
     * & -> &amp;
     * < -> &lt;
     * > -> &gt;
     * " -> &#034;
     * ' -> &#039;
     * <p/>
     * See also OutSupport.writeEscapedXml().
     */
    public static String escapeXml(final String buffer) {
        int start = 0;
        int length = buffer.length();
        char[] arrayBuffer = buffer.toCharArray();
        StringBuffer escapedBuffer = null;
        String result = null;

        for (int i = 0; i < length; i++) {
            char c = arrayBuffer[i];
            if (c <= HIGHEST_SPECIAL) {
                char[] escaped = SPECIAL_CHARACTERS_REPRESENTATION[c];
                if (escaped != null) {
                    // create StringBuffer to hold escaped xml string
                    if (start == 0) {
                        escapedBuffer = new StringBuffer(length + 5);
                    }
                    // add unescaped portion
                    if (start < i) {
                        escapedBuffer.append(arrayBuffer, start, i - start);
                    }
                    start = i + 1;
                    // add escaped xml
                    escapedBuffer.append(escaped);
                }
            }
        }
        // no xml escaping was necessary
        if (start == 0) {
            result = buffer;

            // add rest of unescaped portion
        } else if (escapedBuffer != null && start < length) {
            escapedBuffer.append(arrayBuffer, start, length - start);
            result = escapedBuffer.toString();
        }

        return result;
    }


    public static long getValidMinuets(final Date activationDate, final Date now, final int maxOrderDuration) {
        DateTime dateTime = new DateTime(org.apache.commons.lang.time.DateUtils.truncate(now, Calendar.MINUTE));

        DateTime activationDateTime = new DateTime(org.apache.commons.lang.time.DateUtils.truncate(activationDate, Calendar.MINUTE));

        DateTime date = dateTime.minusDays((int) maxOrderDuration);

        long minutes = (long) Minutes.minutesBetween(date, activationDateTime).getMinutes();

        if (minutes < 0) {
            minutes = 0;
        }
        return minutes;
    }

    public static double roundRate(final double value) {
        return (double) Math.round(value * 10) / (double) 10;
    }

    public static float roundRate(final float value) {
        return (float) Math.round(value * 10) / (float) 10;
    }

    public static double roundAmount(final double amount) {
        return (double) Math.round(amount * 100) / (double) 100;
    }

    public static double roundAmountToThousand(final double amount) {
        return (double) Math.round(amount / 1000) * (double) 1000;
    }

    public static double roundAmountToThousandWithMax(final double amount, final double max) {
        final double rounded = (double) Math.round(amount / 1000) * (double) 1000;
        return rounded > max ? max : rounded;
    }

    public static Date asDateTime(String value, Date fallback) {
        Date date = fallback;
        if (value != null) {
            Matcher matcher = DATE_TIME_PATTERN.matcher(value);
            if (matcher.matches()) {
                String sDay = matcher.group(1);
                String sMonth = matcher.group(2);
                String sYear = matcher.group(3);
                String sHour = (matcher.group(5) == null) ? "00" : matcher.group(5);
                String sMinute = (matcher.group(6) == null) ? "00" : matcher.group(6);
                Calendar cal = new GregorianCalendar(Integer.parseInt(sYear), Integer.parseInt(sMonth) - 1, Integer.parseInt(sDay), Integer.parseInt(sHour), Integer.parseInt(sMinute));
                date = cal.getTime();
            }
        }
        return date;
    }

    public static Date asDate(String value, Date fallback) {
        Date date = fallback;

        if (value != null) {
            Matcher matcher = DATE_PATTERN.matcher(value);
            if (matcher.matches()) {
                String sDay = matcher.group(1);
                String sMonth = matcher.group(2);
                String sYear = matcher.group(3);
                Calendar cal = new GregorianCalendar(Integer.parseInt(sYear), Integer.parseInt(sMonth) - 1, Integer.parseInt(sDay));
                date = cal.getTime();
            }
        }

        return date;
    }

    /**
     * Replace the last 4 numbers with X. At least 2 starting digits are shown.
     * If the given number is smaller or equals than 4 numbers then nothing is replaced.
     * E.g. 12345678 -> 1234XXXX. 12345 -> 1XXXX. 1234 -> 12XX. 123 -> 12X.
     *
     * @param accountNo   the number to replace
     * @param numberCount how many numbers are to be replaced
     * @param side        of which side are the 'X' to be placed
     * @return replaced number
     */
    public static String formatSecureBankNumber(final String accountNo, final int numberCount, final String side) {
        String result = null;
        final String trimmedAccountNo = accountNo.trim();
        final int accountNoLength = trimmedAccountNo.length();
        if (accountNoLength > MIN_NUMBER_OF_CHARS) {
            int index = getNumberCount(accountNoLength, numberCount);
            String overlay = getOverlay(index);
            if ("right".equals(side)) {
                result = StringUtils.overlay(trimmedAccountNo, overlay, accountNoLength - index, accountNoLength);
            } else if ("left".equals(side)) {
                result = StringUtils.overlay(trimmedAccountNo, overlay, 0, index);
            } else {
                throw new IllegalArgumentException("Wrong parameters. Parameter side needs to be one of [left|right] but it's '" + side + "'");
            }
        } else {
            result = accountNo.trim();
        }

        return result;
    }

    private static String getOverlay(int index) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < index; i++) {
            sb.append(OVERLAY_CHARACTER);
        }
        return sb.toString();
    }

    private static int getNumberCount(int accountNoLength, int numberCount) {
        int result = numberCount;
        if ((numberCount + MIN_NUMBER_OF_CHARS) > accountNoLength) {
            result = accountNoLength - MIN_NUMBER_OF_CHARS;
        }

        return result;
    }
}