package de.smava.webapp.commons.web;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Filter for database queries. Can collect values from http request or explicitely passed map
 * and create a query term to query daos for filtered queriess.
 * Is used for backoffice list views (see {@link de.smava.webapp.baseweb.backoffice.BoArtBaseListController})
 * and for filteres database queries like in {@link com.aperto.smava.service.BoDtausFileServiceImpl}
 * <p/>
 * Date: 11.09.2006
 *
 * @author ingmar.decker
 */
public class ListFilter implements Serializable {
    private static final long serialVersionUID = -7561048415298812263L;

    private static final Logger LOGGER = Logger.getLogger(ListFilter.class);

    private List<ListFilterElement> _filterElements = new ArrayList<ListFilterElement>();
    
    private String _queryTermPrefix = null;
    private String _queryTermSuffix = null;


    public List<ListFilterElement> getFilterElements() {
        return Collections.unmodifiableList(_filterElements);
    }

    public void setFilterElements(List<ListFilterElement> filterElements) {
        _filterElements = filterElements;
    }

    public void addFilterElement(ListFilterElement elem) {
        _filterElements.add(elem);
    }

    public ListFilterElement getFilterElementByName(String name) {
        ListFilterElement found = null;
        for (ListFilterElement elem : _filterElements) {
            if (elem.getName().equals(name)) {
                found = elem;
                break;
            }
        }
        return found;
    }

    public String getQueryTermPrefix() {
        return _queryTermPrefix;
    }

    public void setQueryTermPrefix(String queryTermPrefix) {
        this._queryTermPrefix = queryTermPrefix;
    }

    public String getQueryTermSuffix() {
        return _queryTermSuffix;
    }

    public void setQueryTermSuffix(String queryTermSuffix) {
        this._queryTermSuffix = queryTermSuffix;
    }
    
    
}