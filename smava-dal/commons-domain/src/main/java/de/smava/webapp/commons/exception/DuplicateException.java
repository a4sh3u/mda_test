package de.smava.webapp.commons.exception;

/**
 * A unique constraint in the model was violated.
 * 
 * That typically occurs while creating new data.
 * 
 * @author aherr
 *
 */
public class DuplicateException extends ModelException {

	public DuplicateException(String type, String constraint) {
		super(type+":"+constraint+" already exists");
	}
	
	public DuplicateException(String type, String constraint, String value) {
		super(type+":"+constraint+"='"+value+"' already exists");
	}	
}
