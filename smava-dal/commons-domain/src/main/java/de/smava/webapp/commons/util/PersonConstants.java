package de.smava.webapp.commons.util;

public interface PersonConstants {
    String MALE_SALUTATION = "MR";
    String FEMALE_SALUTATION = "MRS";
}
