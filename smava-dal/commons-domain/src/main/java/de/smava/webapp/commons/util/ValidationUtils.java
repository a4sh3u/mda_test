package de.smava.webapp.commons.util;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import java.util.Collection;
import java.util.regex.Pattern;

/**
 * @author Daniel Doubleday
 */
public class ValidationUtils extends org.springframework.validation.ValidationUtils {

    public static final Logger LOGGER = Logger.getLogger(ValidationUtils.class);
    
    public static final Pattern ALPHA_NUMERIC = Pattern.compile("^([a-zA-Z0-9]+)");
    public static final Pattern NUMERIC = Pattern.compile("\\d+");
    public static final Pattern EMAIL_PATTERN = Pattern.compile("^[a-zA-Z0-9][\\w\\.-]*@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$");
    public static final Pattern AMOUNT_PATTERN = Pattern.compile("[0-9]+([\\.\\,][0-9]{2})?|[0-9]+(\\.[0-9]{3})*(,[0-9]{2})?");
    public static final Pattern ZERO_AMOUNT_PATTERN = Pattern.compile("[0]*([\\.\\,][0]{2})?|[0]+(\\.[0]{3})*(,[0]{2})?");
    public static final Pattern SECURITY_ANSWER_PATTERN = Pattern.compile("[\\S]{3,}");
    public static final Pattern PHONE_PATTERN = Pattern.compile("^[0-9][0-9\\- ]+$");
    public static final Pattern DOUBLE_PATTERN = Pattern.compile("[-]?[0-9]+([\\.\\,][0-9]*)?|[-]?[0-9]+(\\.[0-9]{3})*(,[0-9]*)?");
    public static final Pattern ONLY_WORDS_AND_MINUS_PATTERN = Pattern.compile("[[a-z\u00e4\u00f6\u00fc\u00dfA-Z\u00c4\u00d6\u00dc_0-9\\-]\\s+]*"); //"[[a-zäöüßA-ZÖÄÜ_0-9\\-]\\s+]*"
    public static final Pattern INTEREST_PATTERN = Pattern.compile("[0-9]+([\\.\\,][0-9])?");
    public static final Pattern POSITIV_INTEREST_PATTERN = Pattern.compile("([0-9]+[\\.\\,][1-9]{1})|([1-9]+)");
    public static final Pattern MOBILE_PHONE_CODE_PATTERN = PHONE_PATTERN;
    public static final Pattern MOBILE_PHONE_PATTERN = PHONE_PATTERN;

    /** Checks if rate format (10,0 or 10.0). */
    public static boolean isRateFormat(String value) {
        return INTEREST_PATTERN.matcher(value).matches();
    }

    public static boolean isPositivRateFormat(String value) {
        return POSITIV_INTEREST_PATTERN.matcher(value).matches();
    }

    /** Checks positive price format (only). */
    public static boolean isPriceFormat(String value) {
        return AMOUNT_PATTERN.matcher(value).matches();
    }

    public static boolean isZero(String value) {
    	return ZERO_AMOUNT_PATTERN.matcher(value).matches();
    }

    /** Checks if given value is a valid double . */
    public static boolean isDouble(String value) {
        return DOUBLE_PATTERN.matcher(value).matches();
    }
    
    /** Checks negative price format (only). */
    public static boolean isNegativePriceFormat(String amount) {
        boolean rc = false;
        if (amount != null && amount.length() > 0 && amount.charAt(0) == '-') {
            rc = isPriceFormat(amount.substring(1));
        }
        return rc;
    }

    public static boolean isEmailFormat(String value) {
        return EMAIL_PATTERN.matcher(value).matches();
    }

    public static boolean isSecurityAnswerFormat(String value) {
        return SECURITY_ANSWER_PATTERN.matcher(value).matches();
    }

    public static boolean isPhoneFormat(String value) {
        return PHONE_PATTERN.matcher(value).matches();
    }

    public static boolean isMobilePhoneCodeFormat(String value) {
    	return MOBILE_PHONE_CODE_PATTERN.matcher(value).matches();
    }

    public static boolean isMobilePhoneFormat(String value) {
    	return MOBILE_PHONE_PATTERN.matcher(value).matches();
    }

    /**
     * Rejects the value if the value has not been filled out.  Uses the blank error message.
     */
    public static void rejectIfNotPriceFormat(Errors errors, String field, String blankMessage) {
        if (StringUtils.isEmpty((String) errors.getFieldValue(field)) || !isPriceFormat((String) errors.getFieldValue(field))) {
            errors.rejectValue(field, blankMessage);
        }
    }
    
    public static void rejectIfNotEmptyAndNotPriceFormat(Errors errors, String field, String message) {
    	final String fieldValue = (String) errors.getFieldValue(field);
    	if (StringUtils.isNotEmpty(fieldValue) && !isPriceFormat(fieldValue)) {
            errors.rejectValue(field, message);
        }
    }
    
    public static void rejectIfNotRateFormat(final Errors errors, final String field, final String errorCode) {
    	if (!isRateFormat((String) errors.getFieldValue(field))) {
    		errors.rejectValue(field, errorCode);
    	}
    }

    public static void rejectIfNotPositivRateFormat(final Errors errors, final String field, final String errorCode) {
    	if (!isPositivRateFormat((String) errors.getFieldValue(field))) {
    		errors.rejectValue(field, errorCode);
    	}
    }

    /**
     * Rejects the value if the value has not been filled out or if it has been filled out and the value is
     * not of the right format.
     */
    public static void rejectIfNotPriceFormat(Errors errors, String field, String blankMessage, String formatMessage) {
        if (StringUtils.isEmpty((String) errors.getFieldValue(field))) {
            errors.rejectValue(field, blankMessage);
        } else if (!isPriceFormat((String) errors.getFieldValue(field))) {
            errors.rejectValue(field, formatMessage);
        }
    }

    public static void rejectIfZero(Errors errors, String field, String errorMessage) {
        if (isZero((String) errors.getFieldValue(field))) {
            errors.rejectValue(field, errorMessage);
        }
    }

    public static void rejectIfNotPriceFormat(Errors errors, String field, String blankMessage, String formatMessage, Object[] blankParameters, Object[] formatParameters) {
        if (StringUtils.isEmpty((String) errors.getFieldValue(field))) {
            errors.rejectValue(field, blankMessage, blankParameters, "");
        } else if (!isPriceFormat((String) errors.getFieldValue(field))) {
            errors.rejectValue(field, formatMessage, formatParameters, "");
        }
    }

    public static void rejectIfNotEqual(Errors errors, String field1, String field2, String errorCode) {
        boolean fieldsMatch;
        Object field1Value = errors.getFieldValue(field1);
        Object field2Value = errors.getFieldValue(field2);
        if (field1Value == null) {
            fieldsMatch = field2Value == null;
        } else {
            fieldsMatch = field1Value.equals(field2Value);
        }

        if (!fieldsMatch) {
            errors.rejectValue(field2, errorCode);
        }
    }

    public static void rejectIfNotTrue(Errors errors, String field, String errorCode) {
        Object fieldValue = errors.getFieldValue(field);
        boolean boolValue;
        if (fieldValue == null) {
            boolValue = false;
        } else if (fieldValue instanceof Boolean) {
            boolValue = (Boolean) fieldValue;
        } else {
            boolValue = Boolean.parseBoolean(fieldValue.toString());
        }

        if (!boolValue) {
            errors.rejectValue(field, errorCode);
        }
    }
    
    public static void rejectIfNotNipFormat(Errors errors, String field, String errorCode) {
    	Object fieldValue = errors.getFieldValue(field);
    	if (fieldValue != null) {
        	String nip = fieldValue.toString();
        	boolean format = true;
        	
        	format = Pattern.matches("\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d", nip);			// XXXXXXXXXX
    		if (!format) {
    			format = Pattern.matches("\\d\\d\\d-\\d\\d\\d-\\d\\d-\\d\\d", nip);		// XXX-XXX-XX-XX
    		}
    		if (!format) {
    			format = Pattern.matches("\\d\\d\\d-\\d\\d-\\d\\d-\\d\\d\\d", nip);		// XXX-XX-XX-XXX
    		}
        	if (!format) {
        		errors.rejectValue(field, errorCode);
        	}
    	}
    }
    
    public static void rejectIfIsNotAlphaNumeric(Errors errors, String field, String errorCode) {
    	if (field != null && errors.getFieldValue(field) != null) {
	    	if (!ALPHA_NUMERIC.matcher((String) errors.getFieldValue(field)).matches()) {
	    		errors.rejectValue(field, errorCode);
	    	}
    	}
    }

    public static void rejectIfIsNotTitleFormat(Errors errors, String field, String errorCode) {
    	if (field != null && errors.getFieldValue(field) != null) {
	    	if (!ONLY_WORDS_AND_MINUS_PATTERN.matcher((String) errors.getFieldValue(field)).matches()) {
	    		errors.rejectValue(field, errorCode);
	    	}
    	}
    }

    public static void rejectIfNotIntegerFormat(Errors errors, String field, String errorCode) {
        Object fieldValue = errors.getFieldValue(field);
        if (fieldValue != null && StringUtils.isNotBlank(fieldValue.toString())) {
            try {
                Integer.parseInt(fieldValue.toString());
            } catch (NumberFormatException e) {
                errors.rejectValue(field, errorCode);
            }
        } else {
            errors.rejectValue(field, errorCode);
        }
    }
    
    public static void rejectIfNotLongFormat(Errors errors, String field, String errorCode) {
        Object fieldValue = errors.getFieldValue(field);
        if (fieldValue != null && StringUtils.isNotBlank(fieldValue.toString())) {
            try {
                Long.valueOf(fieldValue.toString());
            } catch (NumberFormatException e) {
                errors.rejectValue(field, errorCode);
            }
        } else {
            errors.rejectValue(field, errorCode);
        }
    }
    
    public static void rejectIfNotPattern(Pattern pattern, Errors errors, String field, String errorCode) {
    	rejectIfNotPattern(pattern, errors, field, errorCode, null);
    }

    public static void rejectIfNotPattern(final Pattern pattern, final Errors errors, final String field, final String errorCode, final Object[] msgParams) {
        boolean result = false;
        Object fieldValue = errors.getFieldValue(field);
        if (fieldValue != null && pattern.matcher(fieldValue.toString()).matches()) {
            result = true;
        }
        if (!result) {
            errors.rejectValue(field, errorCode, msgParams, null);
        }
    }

	public static void rejectIfEmptyCollection(Errors errors, String field, String errorCode) {
        Object fieldValue = errors.getFieldValue(field);
        if (fieldValue == null) {
            errors.rejectValue(field, errorCode);
        } else if (fieldValue instanceof Collection) {
            Collection<?> collection = (Collection<?>) fieldValue;
            if (collection.isEmpty()) {
                errors.rejectValue(field, errorCode);
            }
        }
    }

	public static void rejectIfEmptyCollection(Errors errors, String field, String errorCode, Object[] objects, String defaultMessage) {
        Object fieldValue = errors.getFieldValue(field);
        if (fieldValue == null) {
            errors.rejectValue(field, errorCode, objects, defaultMessage);
        } else if (fieldValue instanceof Collection) {
            Collection<?> collection = (Collection<?>) fieldValue;
            if (collection.isEmpty()) {
                errors.rejectValue(field, errorCode, objects, defaultMessage);
            }
        }
    }
    
    public static void rejectIfNoDateFormat(final Errors errors, final String field, final String errorCode) {
    	final Object fieldValue = errors.getFieldValue(field);
    	if (!isDateFormat(ObjectUtils.toString(fieldValue))) {
    		errors.rejectValue(field, errorCode);
    	}
    }

    public static boolean isDateFormat(String value) {
        return FormatUtils.DATE_PATTERN.matcher(value).matches();
    }

    public static boolean isDateTimeFormat(String value) {
        return FormatUtils.DATE_TIME_PATTERN.matcher(value).matches();
    }

    public static void rejectIfNotOnlyNumeric(final Errors errors, final String field, final String errorCode) {
    	rejectIfNotPattern(NUMERIC, errors, field, errorCode);
    }
    
    public static void rejectIfNotEmptyAndTooLong(final Errors errors, final String field, final String errorCode, final int maxLength) {
    	final Object fieldValue = errors.getFieldValue(field);
    	final String strValue = ObjectUtils.toString(fieldValue);
    	if (StringUtils.isNotEmpty(strValue) && strValue.length() > maxLength) {
    		errors.rejectValue(field, errorCode, new Object[] {maxLength}, "Only {0} chars");
    	}
    }
    
	public static boolean getNipAlgorithmCheck(String nip) {
		boolean result = false;
		nip = nip.replaceAll("-", "");
        
    	if (nip.length() == 10) {            
        	// start algorithm check ...
        	char[] charArray = nip.trim().toCharArray();
        	
        	int resultValue = 0;
        	
        	resultValue += charArray[0] * 6;
        	resultValue += charArray[1] * 5;
        	resultValue += charArray[2] * 7;
        	resultValue += charArray[3] * 2;
        	resultValue += charArray[4] * 3;
        	resultValue += charArray[5] * 4;
        	resultValue += charArray[6] * 5;
        	resultValue += charArray[7] * 6;
        	resultValue += charArray[8] * 7;
        	resultValue += charArray[9] * -1;
        	
        	result = (resultValue % 11 == 0.0);
        }
    	
    	return result;
	}
	
	/**
	 * checks for valid pesel syntax.
	 * 
	 * @param errors binding errors
	 * @param expression field expression
	 * @param requiredMessage message if value is empty
	 * @param formatMessage message if its not a number
	 * @param invalidMessage message if it's not a valid pesel
	 * 
	 * @return <code>true</code> if the value was rejected, <code>false</code> otherwise
	 */
	public static boolean rejectIfInvalidPesel(Errors errors, String expression, String requiredMessage, String formatMessage, String invalidMessage) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, expression, requiredMessage);
				
		ValidationUtils.rejectIfNotLongFormat(errors, expression, formatMessage);
		
		String pesel = String.valueOf(errors.getFieldValue(expression));
        
		// validate pesel
		final boolean result; 
        if (!errors.hasFieldErrors(expression)) {
        	if (!getPeselAlgorithmCheck(pesel)) {
        		errors.rejectValue(expression, invalidMessage);
        		result = true;
        	} else {
        		result = false;
        	}
        } else {
        	result = true;
        }
        return result;
	}

	public static boolean getPeselAlgorithmCheck(String pesel) {
		boolean result = false;

		if (pesel.length() == 11) {            
        	// start algorithm check ...
        	char[] charArray = pesel.trim().toCharArray();

        	int resultValue = 0;

        	resultValue += charArray[0] * 1;
        	resultValue += charArray[1] * 3;
        	resultValue += charArray[2] * 7;
        	resultValue += charArray[3] * 9;
        	resultValue += charArray[4] * 1;
        	resultValue += charArray[5] * 3;
        	resultValue += charArray[6] * 7;
        	resultValue += charArray[7] * 9;
        	resultValue += charArray[8] * 1;
        	resultValue += charArray[9] * 3;
        	resultValue += charArray[10] * 1;

        	result = (resultValue % 10 == 0.0);
        }

		return result;
	}
}