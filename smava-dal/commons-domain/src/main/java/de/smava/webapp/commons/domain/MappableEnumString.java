/**
 * 
 */
package de.smava.webapp.commons.domain;

/**
 * @param <T> type of implementing {@link Enum}
 * 
 * @author bvoss
 *
 */
public interface MappableEnumString<T extends Enum<T>> {

	String getDbValue();
	
	T getEnumFromDbValue(String dbValue);
	
	/**
	 * for better use in jsps und EL.
	 * 
	 * @return {@link Enum#name()}
	 */
	String getName();
}
