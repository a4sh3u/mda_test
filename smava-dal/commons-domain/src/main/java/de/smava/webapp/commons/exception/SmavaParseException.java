package de.smava.webapp.commons.exception;

import java.text.ParseException;

/**
 * Used if parsing a text failed for any reason.
 *
 * Created by aherr on 20.11.14.
 */
public class SmavaParseException extends InvalidValueException {

    private int errorOffset;

    public SmavaParseException(String s, int errorOffset) {
        super(s);
        this.errorOffset = errorOffset;
    }

    public SmavaParseException(ParseException ex) {
        super(ex);
    }
}
