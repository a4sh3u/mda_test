package de.smava.webapp.commons.dao.jdo;

import de.smava.webapp.commons.domain.BaseEntity;
import org.apache.log4j.Logger;
import org.springframework.orm.jdo.PersistenceManagerFactoryUtils;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;

public class JdoNewThreadPersister implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(JdoNewThreadPersister.class);

    private PersistenceManagerFactory persistenceManagerFactory;
    private BaseEntity persistingEntity;

    public JdoNewThreadPersister(PersistenceManagerFactory persistenceManagerFactory, BaseEntity persistingEntity) {
        this.persistenceManagerFactory = persistenceManagerFactory;
        this.persistingEntity = persistingEntity;
    }

    @Override
    public void run() {
        try {
            getPersistenceManager().makePersistent(persistingEntity);
        }
        catch (Exception e) {
            LOGGER.error("Can't persist in separate thread an entity " + persistingEntity, e);
        }

    }

    private PersistenceManager getPersistenceManager() {
        return PersistenceManagerFactoryUtils.getPersistenceManager(persistenceManagerFactory, true);
    }

}
