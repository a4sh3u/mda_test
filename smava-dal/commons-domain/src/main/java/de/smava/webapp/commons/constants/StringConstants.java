/**
 * 
 */
package de.smava.webapp.commons.constants;

/**
 * @author bvoss
 *
 */
public interface StringConstants {

	final String RDI_CONTRACT_DENIAL_KEY = "noRdiContract";

	final String MARKETINGFILTER_ATTRIBUTE_CURRENT_PLACEMENT_ID = "currentPlacementId";

	final String REQUEST_KEY_SUCCESS_VIEW = "Controller.SUCCESSVIEW";

	final String CACHING_SERVICE = "cachingService";
	
	final String PERSONAL_MAIN_DATA_INPUT_COUNTER = "de.smava.webapp.PERSONAL_MAIN_DATA_INPUT_COUNTER";
	final String PERSONAL_ADDRES_DATA_INPUT_COUNTER = "de.smava.webapp.PERSONAL_ADDRESS_DATA_INPUT_COUNTER";

	final String CREDIT_WORTHINESS_EMPLOYMENT_JAVASCRIPT_BLOCK = "creditWorthinessEmploymentJavaScriptBlock";

	final String CREDIT_WORTHINESS_INCOME_JAVASCRIPT_BLOCK = "creditWorthinessIncomeJavaScriptBlock";

	final String CREDIT_WORTHINESS_EXPENSES_JAVASCRIPT_BLOCK = "creditWorthinessExpensesJavaScriptBlock";

	final String CREDIT_WORTHINESS_HOUSEHOLD_JAVASCRIPT_BLOCK = "creditWorthinessHouseholdJavaScriptBlock";
	
	final String LOGIN_DATA_COUNTER = "de.smava.webapp.LOGIN_DATA_COUNTER";
	
	final String FEED_VIEW_PARAMETER = "feedView";
	final String FEED_CONTENTID_PARAMETER = "feedContentId";
	final String FEED_SELECTION_PARAMETER = "feedSelection";
	
	
	final String ACCOUNT_FORM_DATA = "accountFormData";
	final String COMMAND_ACCOUNT_FORM_DATA = "command." + ACCOUNT_FORM_DATA;
	
	final String ACCOUNT_FORM_DATA_REF = ACCOUNT_FORM_DATA + ".";
	final String ACCOUNT_FORM_DATA2_REF = ACCOUNT_FORM_DATA + "2" + ".";

	final String DEBT_CONSOLIDATION_COUNTER = "de.smava.webapp.DEBT_CONSOLIDATION_COUNTER";
	
	
	//Session Attribute Names
	final String BO_MARKETING_TEST_PLACMENT_LIST_ATTRIBUTE_NAME = "de.smava.webapp.backoffice.marketing.PLACEMENT_LIST";

//	additional captions
	final String KEY_ADDITONAL_CAPTIONS = "additionalCaptions";
	final String KEY_ORDER_ADDITONAL_CAPTION = "orderAdditionalCaption";

	final String SHORTLEAD = "SHORTLEAD";

    /**
     * Nutzer, die die Kontrolle über Ihren Account an smava-Telefonvertrieb Mitarbeiter für die Registrierung abgegeben haben
     */
	final String SALESLEAD = "SALESLEAD";
	
	final String LOGIN_EMAIL_REQUEST_PARAMETER = "loginEmail";
	
	final String YUI_EVENT_SCRIPT_SNIP = "<script src=\"js/yui/yahoo-dom-event/yahoo-dom-event.js\" type=\"text/javascript\" language=\"JavaScript\"></script>\n";
	final String YUI_EVENT_INSERT_ATTRIBUTE = "de.smava.webapp.web.YUI_EVENT_INSERT_ATTRIBUTE";
	
	final String INDEXTOOLS_PAGENAME_ATTRIBUTE_NAME = "de.smava.webapp.YUI_TRACKING_PAGENAME";
	final String INDEXTOOLS_PAGENAME_SUFFIX_ATTRIBUTE_NAME = "de.smava.webapp.YUI_TRACKING_PAGENAME_SUFFIX";
	
	final String UPDATE_CONSOLIDATION_DEPT = "updateCD";
	final String NEW_CONSOLIDATION_DEPT = "newCD";
}
