/**
 *
 */
package de.smava.webapp.commons.currentdate;

import java.util.Date;

/**
 * Only used by ConfigService the change the fake date settings in {@link CurrentDate}
 * so that the original setters are only package visible and nobody can call them accidentally.  
 *
 *
 * @author bvoss
 *
 */
public final class SetterForCurrentDate {

	private SetterForCurrentDate() {
	}

	public static void setAllowFakeDate(final boolean allowFakeDate) {
		CurrentDate.setAllowFakeDate(allowFakeDate);
	}

	public static void setUseFakeDate(final boolean useFakeDate) {
		CurrentDate.setUseFakeDate(useFakeDate);
	}
	public static void setUseFakeTime(final boolean useFakeTime) {
		CurrentDate.setUseFakeTime(useFakeTime);
	}

	public static void setFakeDate(final Date fakeDate) {
		CurrentDate.setFakeDate(fakeDate);
	}

}
