package de.smava.webapp.commons.dao.util;


import static org.easymock.EasyMock.expect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/test-context.xml"})
public class DaoUtilsTest  {
    @Resource
	private DaoUtils daoUtils;

	private static final Logger LOGGER = Logger.getLogger(DaoUtilsTest.class);

	class Fake extends KreditPrivatEntity{

		@Override
		public List<? extends Entity> getModifier() {
			return null;
		}
		
	}

	class FakeDao implements BaseDao<Fake>{

		@Override
		public Fake load(Long id) {
			return null;
		}

		@Override
		public Long save(Fake message) {
			return null;
		}

		@Override
		public void forceFlush() {
		}

		@Override
		public boolean exists(Long id) {
			return false;
		}

        @Override
        public String createQueryTerm(Collection elements, Map valuesMap, List parameters, String concat) {
            return null;
        }

        @Override
        public Long getMaxId(Class clazz) {
            return null;
        }
    }
	
	class EasySort implements Sortable{

		boolean decending;
		public EasySort(boolean decending){
			this.decending = decending;
		}
		@Override
		public String getSort() {
			return "_id";
		}

		@Override
		public boolean hasSort() {
			return true;
		}

		@Override
		public boolean sortDescending() {
			return decending;
		}
		
	}
	
	public class EasyPaging implements Pageable{

		int size;
		
		public EasyPaging(int size){
			this.size = size;
		}
		@Override
		public long getPrevious() {
			return 0;
		}

		@Override
		public long getNext() {
			return 3;
		}

		@Override
		public int getItemsPerPage() {
			return 2;
		}

		@Override
		public long getOffset() {
			return 0;
		}

		@Override
		public boolean hasPrevious() {
			return false;
		}

		@Override
		public boolean hasNext() {
			return true;
		}

		@Override
		public Collection<?> getPageContent() {
			return null;
		}

		@Override
		public long getSize() {
			return size;
		}
		
	}
	private List<Fake> getColection(){
		Fake f1 = new Fake();
		Fake f2 = new Fake();
		Fake f3 = new Fake();
		
		f1.setId(1l);
		f2.setId(2l);
		f3.setId(3l);
		
		List<Fake> col = new ArrayList<Fake>();
		col.add(f1);
		col.add(f2);
		col.add(f3);
		return col;
	}
	@Test
	public void testGetIdsAsList(){
		
		List<Long> res = daoUtils.getIdsAsList(getColection());
		
		Assert.assertEquals(3, res.size());
		
		res = daoUtils.getIdsAsList(new ArrayList<Fake>());
		Assert.assertEquals(0, res.size());
	}
	
	@Test
	public void testGetIdList(){
		
		String s = daoUtils.getIdList(getColection());
		LOGGER.info(s);
		Assert.assertEquals("1,2,3", s);
		
		s = daoUtils.getIdList(new ArrayList<Fake>());
		Assert.assertEquals("", s);
	}
	
	@Test
	public void testGetEntityListSorted(){
		Fake f4 = new Fake();
		f4.setId(4l);
		List<Fake> col = getColection();
		col.add(0, f4);
		List<Fake> res = (List<Fake>) daoUtils.getEntityList(col, null, new EasySort(false));
		

		Assert.assertEquals(1l, res.get(0).getId().longValue());
		Assert.assertEquals(4l, res.get(3).getId().longValue());
		
	}
	
	@Test
	public void testGetEntityListSortedDecending(){
		Fake f4 = new Fake();
		f4.setId(4l);
		List<Fake> col = getColection();
		col.add(0, f4);
		List<Fake> res = (List<Fake>) daoUtils.getEntityList(col, null, new EasySort(true));
		
	
		Assert.assertEquals(4l, res.get(0).getId().longValue());
		Assert.assertEquals(1l, res.get(3).getId().longValue());
		
	}
	
	@Test
	public void testGetEntityListPagedSorted(){
		Fake f4 = new Fake();
		f4.setId(4l);
		List<Fake> col = getColection();
		col.add(0, f4);
		List<Fake> res = (List<Fake>) daoUtils.getEntityList(col, new EasyPaging(col.size()), new EasySort(false));
		
		Assert.assertEquals(2, res.size());
		Assert.assertEquals(1l, res.get(0).getId().longValue());
		Assert.assertEquals(2l, res.get(1).getId().longValue());
		
	}
	
	@Test
	public void testGetDao(){
		
		ApplicationContext context = EasyMock.createMock(ApplicationContext.class);
		
		expect(context.getBean(EasyMock.eq("fakeDao"), EasyMock.notNull(Class.class))).andReturn(new FakeDao());
		EasyMock.replay(context);
		daoUtils.setApplicationContext(context);
		BaseDao<Fake> res = daoUtils.getDao(Fake.class);
		
		Assert.assertNotNull(res);
		
	}
}
