package de.smava.webapp.commons.dao;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import de.smava.database.restore.DatabaseRestore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.io.IOException;

public class DataSourceConfigurer {

    @Value("${de.smava.bitbucket.dumps.url:https://bitbucket.smava.de/projects/QA/repos/dumps/}")
    private String storageUrl;

    @Value("${de.smava.bitbucket.encodedAuthToken}")
    private String bitBucketEncodedAuthToken;

    @Bean(destroyMethod = "close")
    @Primary
    public EmbeddedPostgres createEmbeddedPostgres() throws IOException {
        return EmbeddedPostgres
                .builder()
                .start();
    }

    @Bean
    @Primary
    public DataSource dataSource(EmbeddedPostgres embeddedPostgres) {
        return DatabaseRestore.restoreDatabase(embeddedPostgres,
                storageUrl,
                bitBucketEncodedAuthToken);
    }
}
