package de.smava.webapp.commons.dao.jdo;

import de.smava.webapp.commons.domain.BaseEntity;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;

@Repository
public class JdoTestEntityDao extends JdoBaseDao {

    public <T extends BaseEntity> Long save(T entity) {
        return getPersistenceManager().makePersistent(entity).getId();
    }

    public <T extends BaseEntity> T getObjectById(Class<T> clazz, Long id) {
        return getPersistenceManager().getObjectById(clazz, id);
    }

    @SuppressWarnings("unchecked")
    public <T extends BaseEntity> T getLastInsertedObject(Class<T> clazz) {
        Query query = getPersistenceManager().newQuery(clazz);
        query.setOrdering("_id desc");
        query.setRange(0L, 1L);
        query.setUnique(true);
        return (T) query.execute();
    }

}
