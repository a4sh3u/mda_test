package de.smava.webapp.brokerage.loancheck.domain.interfaces;



import de.smava.webapp.brokerage.loancheck.domain.LoanCheck;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequest;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheckType;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'LoanChecks'.
 *
 * @author generator
 */
public interface LoanCheckEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(LoanCheckType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    LoanCheckType getType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'completion date'.
     *
     * 
     *
     */
    void setCompletionDate(Date completionDate);

    /**
     * Returns the property 'completion date'.
     *
     * 
     *
     */
    Date getCompletionDate();
    /**
     * Setter for the property 'loan check requests'.
     *
     * 
     *
     */
    void setLoanCheckRequests(Set<LoanCheckRequest> loanCheckRequests);

    /**
     * Returns the property 'loan check requests'.
     *
     * 
     *
     */
    Set<LoanCheckRequest> getLoanCheckRequests();
    /**
     * Helper method to get reference of this object as model type.
     */
    LoanCheck asLoanCheck();
}
