/**
 * 
 */
package de.smava.webapp.brokerage.domain;

/**
 * @author dkeller
 *
 */
public interface RemoteBankRequestDataIf {

	public Long getId();
	public BrokerageBank getBrokerageBank();
	public de.smava.webapp.account.domain.EconomicalData getEconomicalData();
	public de.smava.webapp.account.domain.Person getFirstPerson();
	public double getRequestedAmount();
	public int getRequestedDuration();
	public de.smava.webapp.account.domain.Account getAccount();

	public String getRequestedRdiType();
	public de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType();
	public de.smava.webapp.account.domain.Person getSecondPerson();

	public void setBrokerageApplicationFilterReason(BrokerageApplicationFilterReason brokerageApplicationFilterReason);

}
