//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.loancheck.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan check)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheck;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'LoanChecks'.
 *
 * @author generator
 */
public interface LoanCheckDao extends BaseDao<LoanCheck> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the loan check identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    LoanCheck getLoanCheck(Long id);

    /**
     * Saves the loan check.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveLoanCheck(LoanCheck loanCheck);

    /**
     * Deletes an loan check, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the loan check
     */
    void deleteLoanCheck(Long id);

    /**
     * Retrieves all 'LoanCheck' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<LoanCheck> getLoanCheckList();

    /**
     * Retrieves a page of 'LoanCheck' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<LoanCheck> getLoanCheckList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'LoanCheck' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<LoanCheck> getLoanCheckList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'LoanCheck' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<LoanCheck> getLoanCheckList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'LoanCheck' instances.
     */
    long getLoanCheckCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(loan check)}
    //
    // insert custom methods here

    Collection<LoanCheck> getLoanChecksForAccount(Account account);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
