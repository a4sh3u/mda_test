package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokeragePayoutProbabilityPredictionData;




/**
 * The domain object that has all history aggregation related fields for 'BrokeragePayoutProbabilityPredictionDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokeragePayoutProbabilityPredictionDataHistory extends AbstractBrokeragePayoutProbabilityPredictionData {



												
}
