//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document type placeholder)}
import de.smava.webapp.brokerage.domain.history.DocumentTypePlaceholderHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentTypePlaceholders'.
 *
 * 
 *
 * @author generator
 */
public class DocumentTypePlaceholder extends DocumentTypePlaceholderHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(document type placeholder)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected DocumentType _documentType;
        protected DocumentPlaceholder _documentPlaceholder;
        
                                    
    /**
     * Setter for the property 'document type'.
     *
     * 
     *
     */
    public void setDocumentType(DocumentType documentType) {
        _documentType = documentType;
    }
            
    /**
     * Returns the property 'document type'.
     *
     * 
     *
     */
    public DocumentType getDocumentType() {
        return _documentType;
    }
                                            
    /**
     * Setter for the property 'document placeholder'.
     *
     * 
     *
     */
    public void setDocumentPlaceholder(DocumentPlaceholder documentPlaceholder) {
        _documentPlaceholder = documentPlaceholder;
    }
            
    /**
     * Returns the property 'document placeholder'.
     *
     * 
     *
     */
    public DocumentPlaceholder getDocumentPlaceholder() {
        return _documentPlaceholder;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentTypePlaceholder.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(DocumentTypePlaceholder.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DocumentTypePlaceholder asDocumentTypePlaceholder() {
        return this;
    }
}
