//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application guarantee)}
import de.smava.webapp.brokerage.domain.history.BrokerageApplicationGuaranteeHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationGuarantees'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageApplicationGuarantee extends BrokerageApplicationGuaranteeHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application guarantee)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageApplication _brokerageApplication;
        protected LoanApplication _loanApplication;
        protected Date _topChance;
        protected Date _guarantee;
        protected String _reason;
        
                                    
    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }
                                            
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                    /**
     * Setter for the property 'top chance'.
     *
     * 
     *
     */
    public void setTopChance(Date topChance) {
        if (!_topChanceIsSet) {
            _topChanceIsSet = true;
            _topChanceInitVal = getTopChance();
        }
        registerChange("top chance", _topChanceInitVal, topChance);
        _topChance = topChance;
    }
                        
    /**
     * Returns the property 'top chance'.
     *
     * 
     *
     */
    public Date getTopChance() {
        return _topChance;
    }
                                    /**
     * Setter for the property 'guarantee'.
     *
     * 
     *
     */
    public void setGuarantee(Date guarantee) {
        if (!_guaranteeIsSet) {
            _guaranteeIsSet = true;
            _guaranteeInitVal = getGuarantee();
        }
        registerChange("guarantee", _guaranteeInitVal, guarantee);
        _guarantee = guarantee;
    }
                        
    /**
     * Returns the property 'guarantee'.
     *
     * 
     *
     */
    public Date getGuarantee() {
        return _guarantee;
    }
                                    /**
     * Setter for the property 'reason'.
     *
     * 
     *
     */
    public void setReason(String reason) {
        if (!_reasonIsSet) {
            _reasonIsSet = true;
            _reasonInitVal = getReason();
        }
        registerChange("reason", _reasonInitVal, reason);
        _reason = reason;
    }
                        
    /**
     * Returns the property 'reason'.
     *
     * 
     *
     */
    public String getReason() {
        return _reason;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplicationGuarantee.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _reason=").append(_reason);
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplicationGuarantee.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplicationGuarantee asBrokerageApplicationGuarantee() {
        return this;
    }
}
