package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageBucketConfiguration;
import de.smava.webapp.brokerage.domain.BrokerageBucketConfigurationWildcardBank;
import de.smava.webapp.brokerage.domain.BrokerageBucketWildcardExclusionRule;

import java.util.Set;


/**
 * The domain object that represents 'BrokerageBucketConfigurationWildcardBanks'.
 *
 * @author generator
 */
public interface BrokerageBucketConfigurationWildcardBankEntityInterface {

    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    void setBrokerageBucketConfiguration(BrokerageBucketConfiguration brokerageBucketConfiguration);

    /**
     * Returns the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    BrokerageBucketConfiguration getBrokerageBucketConfiguration();
    /**
     * Setter for the property 'brokerage bucket wildcard exclusion rules'.
     *
     * 
     *
     */
    void setBrokerageBucketWildcardExclusionRules(Set<BrokerageBucketWildcardExclusionRule> brokerageBucketWildcardExclusionRules);

    /**
     * Returns the property 'brokerage bucket wildcard exclusion rules'.
     *
     * 
     *
     */
    Set<BrokerageBucketWildcardExclusionRule> getBrokerageBucketWildcardExclusionRules();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBucketConfigurationWildcardBank asBrokerageBucketConfigurationWildcardBank();
}
