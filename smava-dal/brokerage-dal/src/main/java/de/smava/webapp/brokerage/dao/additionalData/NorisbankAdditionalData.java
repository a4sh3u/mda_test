package de.smava.webapp.brokerage.dao.additionalData;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonFilter(NorisbankAdditionalData.NORISBANK_FILTER)
public class NorisbankAdditionalData extends AbstractAdditionalData implements Serializable {

    private static final long serialVersionUID = 1943863879098413411L;

    public static final String NORISBANK_FILTER = "norisbankFilter";

    public static final String RETURN_CODE = "returnCode";
    public static final String LOAN_OFFER_ID = "loanOfferId";
    public static final String UPSELLING_OFFER = "upsellingOffer";
    public static final String REQUEST_STATE_TEXT = "requestStateText";
    public static final String REASON_FOR_REPAIRS = "reasonForRepairs";
    public static final String REASON_FOR_REFUSALS = "reasonForRefusals";
    public static final String FMKV = "fmkv";
    public static final String AMOUNT_LAST_INSTALLMENT = "schlussrate";
    public static final String NUMBER_INSTALLMENTS = "anzahlRaten";
    public static final String INTEREST_RATE = "sollzins";
    public static final String AMOUNT_INTERESTS = "gesamtzinsen";
    public static final String TOTAL_LOAN_AMOUNT = "gesamtkreditbetrag";
    public static final String DATE_FIRST_INSTALLMENT = "ersteRateZum";
    public static final String ALTERNATIVE_REQUESTED_AMOUNT = "alternativeRequestedAmount";
    public static final String ALTERNATIVE_REQUESTED_DURATION = "alternativeRequestedDuration";
    public static final String TOTAL_INSURANCE_PREMIUM = "totalInsurancePremium";
    public static final String INSURED_AMOUNT_RKV = "insuredAmountRKV";
    public static final String INSURED_AMOUNT_AUV = "insuredAmountAUV";
    public static final String INSURED_AMOUNT_ALV = "insuredAmountALV";
    public static final String INSURANCE_DURATION = "insuranceDuration";
    public static final String DOCUMENT_REQUEST_ERROR_CODE = "documentRequestErrorCode";
    public static final String MESSAGES = "messages";
    public static final String ORDER_REPRINT_DATE = "orderReprintDate";
    public static final String TRANSACTION_ID = "transactionId";
    public static final String REQUEST_ID = "requestId";
	
    public static final String UPSELLING_OFFER_AMOUNT = "Kreditbetrag";
    public static final String UPSELLING_OFFER_DURATION = "Laufzeit";
    public static final String UPSELLING_OFFER_EFFECTIVE_INTEREST = "Eff. Zins";
    public static final String UPSELLING_OFFER_MONTHLY_RATE = "Rate";
    public static final String UPSELLING_OFFER_RDI_TYPE = "RKV";


    @JsonProperty(RETURN_CODE)
    private Integer returnCode;

    @JsonProperty(LOAN_OFFER_ID)
    private String loanOfferId;

    @JsonProperty(UPSELLING_OFFER)
    private Map<String, String> upsellingOffer;

    @JsonProperty(REQUEST_STATE_TEXT)
    private String requestStateText;

    @JsonProperty(REASON_FOR_REPAIRS)
    private List<String> reasonForRepairs;

    @JsonProperty(REASON_FOR_REFUSALS)
    private List<String> reasonForRefusals;

    @JsonProperty(FMKV)
    private BigDecimal fmkv;

    @JsonProperty(AMOUNT_LAST_INSTALLMENT)
    private BigDecimal amountLastInstallment;

    @JsonProperty(NUMBER_INSTALLMENTS)
    private Integer numberInstallments;

    @JsonProperty(INTEREST_RATE)
    private BigDecimal interestRate;

    @JsonProperty(AMOUNT_INTERESTS)
    private BigDecimal amountInterests;

    @JsonProperty(TOTAL_LOAN_AMOUNT)
    private BigDecimal totalLoanAmount;

    @JsonProperty(DATE_FIRST_INSTALLMENT)
    private Date dateFirstInstallment;

    @JsonProperty(ALTERNATIVE_REQUESTED_AMOUNT)
    private Double alternativeRequestedAmount;

    @JsonProperty(ALTERNATIVE_REQUESTED_DURATION)
    private Integer alternativeRequestedDuration;

    @JsonProperty(TOTAL_INSURANCE_PREMIUM)
    private BigDecimal totalInsurancePremium;

    @JsonProperty(INSURED_AMOUNT_RKV)
    private BigDecimal insuredAmountRKV;

    @JsonProperty(INSURED_AMOUNT_AUV)
    private BigDecimal insuredAmountAUV;

    @JsonProperty(INSURED_AMOUNT_ALV)
    private BigDecimal insuredAmountALV;

    @JsonProperty(INSURANCE_DURATION)
    private Integer insuranceDuration;

    @JsonProperty(DOCUMENT_REQUEST_ERROR_CODE)
    private String documentRequestErrorCode;

    @JsonProperty(MESSAGES)
    protected String messages;

    @JsonProperty(ORDER_REPRINT_DATE)
    protected Date orderReprintDate;

    @JsonProperty(TRANSACTION_ID)
    private String transactionId;

    @JsonProperty(REQUEST_ID)
    private String requestId;

    @Override
    public String determineFilterName() {
        return NORISBANK_FILTER;
    }


    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getLoanOfferId() {
        return loanOfferId;
    }

    public void setLoanOfferId(String loanOfferId) {
        this.loanOfferId = loanOfferId;
    }

    public Map<String, String> getUpsellingOffer() {
        return upsellingOffer;
    }

    public void setUpsellingOffer(Map<String, String> upsellingOffer) {
        this.upsellingOffer = upsellingOffer;
    }

    public String getRequestStateText() {
        return requestStateText;
    }

    public void setRequestStateText(String requestStateText) {
        this.requestStateText = requestStateText;
    }

    public List<String> getReasonForRepairs() {
        return reasonForRepairs;
    }

    public void setReasonForRepairs(List<String> reasonForRepairs) {
        this.reasonForRepairs = reasonForRepairs;
    }

    public List<String> getReasonForRefusals() {
        return reasonForRefusals;
    }

    public void setReasonForRefusals(List<String> reasonForRefusals) {
        this.reasonForRefusals = reasonForRefusals;
    }

    public BigDecimal getFmkv() {
        return fmkv;
    }

    public void setFmkv(BigDecimal fmkv) {
        this.fmkv = fmkv;
    }

    public void setAmountLastInstallment(BigDecimal amountLastInstallment) {
        this.amountLastInstallment = amountLastInstallment;
    }

    public Integer getNumberInstallments() {
        return numberInstallments;
    }

    public void setNumberInstallments(Integer numberInstallments) {
        this.numberInstallments = numberInstallments;
    }

    public BigDecimal getAmountLastInstallment() {
        return amountLastInstallment;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setAmountInterests(BigDecimal amountInterests) {
        this.amountInterests = amountInterests;
    }

    public BigDecimal getAmountInterests() {
        return amountInterests;
    }

    public void setTotalLoanAmount(BigDecimal totalLoanAmount) {
        this.totalLoanAmount = totalLoanAmount;
    }

    public BigDecimal getTotalLoanAmount() {
        return totalLoanAmount;
    }

    public void setDateFirstInstallment(Date dateFirstInstallment) {
        this.dateFirstInstallment = dateFirstInstallment;
    }

    public Date getDateFirstInstallment() {
        return dateFirstInstallment;
    }

    public void setAlternativeRequestedAmount(Double alternativeRequestedAmount) {
        this.alternativeRequestedAmount = alternativeRequestedAmount;
    }

    public Double getAlternativeRequestedAmount() {
        return alternativeRequestedAmount;
    }

    public void setAlternativeRequestedDuration(Integer alternativeRequestedDuration) {
        this.alternativeRequestedDuration = alternativeRequestedDuration;
    }

    public Integer getAlternativeRequestedDuration() {
        return alternativeRequestedDuration;
    }

    public void setTotalInsurancePremium(BigDecimal totalInsurancePremium) {
        this.totalInsurancePremium = totalInsurancePremium;
    }

    public BigDecimal getTotalInsurancePremium() {
        return totalInsurancePremium;
    }

    public void setInsuredAmountRKV(BigDecimal insuredAmountRKV) {
        this.insuredAmountRKV = insuredAmountRKV;
    }

    public BigDecimal getInsuredAmountRKV() {
        return insuredAmountRKV;
    }

    public void setInsuredAmountAUV(BigDecimal insuredAmountAUV) {
        this.insuredAmountAUV = insuredAmountAUV;
    }

    public BigDecimal getInsuredAmountAUV() {
        return insuredAmountAUV;
    }

    public void setInsuredAmountALV(BigDecimal insuredAmountALV) {
        this.insuredAmountALV = insuredAmountALV;
    }

    public BigDecimal getInsuredAmountALV() {
        return insuredAmountALV;
    }

    public void setInsuranceDuration(Integer insuranceDuration) {
        this.insuranceDuration = insuranceDuration;
    }

    public Integer getInsuranceDuration() {
        return insuranceDuration;
    }

    public String getDocumentRequestErrorCode() {
        return documentRequestErrorCode;
    }

    public void setDocumentRequestErrorCode(String documentRequestErrorCode) {
        this.documentRequestErrorCode = documentRequestErrorCode;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getMessages() {
        return messages;
    }

    public void setOrderReprintDate(Date orderReprintDate) {
        this.orderReprintDate = orderReprintDate;
    }

    public Date getOrderReprintDate() {
        return orderReprintDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public HashMap<String, Object> readAdditionalData() {
        return readAdditionalData(NORISBANK_FILTER, RETURN_CODE, UPSELLING_OFFER, REQUEST_STATE_TEXT, REASON_FOR_REPAIRS,
                REASON_FOR_REFUSALS, AMOUNT_LAST_INSTALLMENT, NUMBER_INSTALLMENTS, INTEREST_RATE, AMOUNT_INTERESTS,
                TOTAL_LOAN_AMOUNT, DATE_FIRST_INSTALLMENT, MESSAGES, ORDER_REPRINT_DATE, TOTAL_INSURANCE_PREMIUM, INSURED_AMOUNT_RKV,
                INSURED_AMOUNT_AUV, INSURED_AMOUNT_ALV, INSURANCE_DURATION, DOCUMENT_REQUEST_ERROR_CODE, TRANSACTION_ID, REQUEST_ID);
    }
}
