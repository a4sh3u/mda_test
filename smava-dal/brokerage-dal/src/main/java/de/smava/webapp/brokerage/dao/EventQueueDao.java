package de.smava.webapp.brokerage.dao;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.brokerage.domain.EventQueue;
import de.smava.webapp.brokerage.domain.EventQueueState;
import de.smava.webapp.brokerage.domain.EventQueueType;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.List;

/**
 * DAO interface for the domain object 'EventQueues'.
 */
public interface EventQueueDao extends BaseDao<EventQueue> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    Collection<EventQueue> findCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states);
    
    Collection<EventQueue> findCurrentEventQueueList(Account account, Collection<EventQueueType> types, Collection<EventQueueState> states);

    /**
     * Finds all types of event queues for a borrower whose state is not VOID
     * @param account the account of borrower
     * @param current if true only those event queues whose due date in the past and expiry date in the future are returned
     * @param offset the offset
     * @param limit the limit
     * @param sortBy sort by if null sorted according to due date descending
     * @param descending sort direction if sortBy is null than this argument is not used
     * @return list of event queues
     */
    Collection<EventQueue> findBorrowerEventQueues(Account account, Boolean current, Integer offset, Integer limit, String sortBy, Boolean descending);

    /**
     * Finds latest borrower event queus
     * @param account the account of borrower
     * @param current if true only those event queues whose due date in the past and expiry date in the future are returned
     * @return null if none found
     */
    EventQueue findLastBorrowerEventQueue(Account account, Boolean current);

    Collection<EventQueue> findAllEventQueueList(LoanApplication la, Collection<EventQueueType> types, Collection<EventQueueState> states);
    
    Collection<EventQueue> findAllEventQueueList(Account account, Collection<EventQueueType> types, Collection<EventQueueState> states);

    Collection<EventQueue> findAllEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset,
                                                 Integer limit, String sortBy, Boolean descending, boolean assigned);

    
    Collection<EventQueue> findCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor);

    Collection<EventQueue> findCurrentEventQueueList(
            Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset, Integer limit, String sortBy, Boolean descending);

    Collection<EventQueue> findCurrentEventQueueList(
            Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset,
            Integer limit, String sortBy, Boolean descending, boolean assigned);

    long getNumberOfCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states);

    long getNumberOfCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Long advisorId, boolean assigned);

    long getNumberOfEventQueueList(Collection<EventQueueState> states, Long advisorId, boolean assigned);

    /**
     * Find events with provided types and states using restrictive adviser search and
     * limit of 100 most recent records.
     *
     * @param types
     * @param states
     * @param banks
     * @return
     */
    Collection<EventQueue> findLastEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Collection<Long> advisers, List<String> banks);

    /**
     * Find last state-independent event for borrower
     *
     * @param account Borrower Account
     * @return Event queue
     */
    EventQueue findLastStateIndependentBorrowerEventQueues(Account account);

    EventQueue findLatestEventQueueForLoanApplication(LoanApplication la);

    Collection<EventQueue> loadEventQueueByIds(Collection<Long> eventQueueIds);

    Collection<Long> findCurrentEventQueueIds(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset, Integer limit, final String sortBy, final Boolean descending, final String alternativeSortBy, final Boolean alternativeDescending, boolean assigned, CustomerProcessState customerProcessState);

    Collection<EventQueue> findCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset, Integer limit, final String sortBy, final Boolean descending, final String alternativeSortBy, final Boolean alternativeDescending, boolean assigned, CustomerProcessState customerProcessState);

    Long countCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset, Integer limit, final String sortBy, final Boolean descending, final String alternativeSortBy, final Boolean alternativeDescending, boolean assigned, CustomerProcessState customerProcessState);
}
