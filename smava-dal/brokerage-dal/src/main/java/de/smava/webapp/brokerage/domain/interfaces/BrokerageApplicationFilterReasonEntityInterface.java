package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageApplicationFilterReason;
import de.smava.webapp.brokerage.domain.FilterReasonCode;


/**
 * The domain object that represents 'BrokerageApplicationFilterReasons'.
 *
 * @author generator
 */
public interface BrokerageApplicationFilterReasonEntityInterface {

    /**
     * Setter for the property 'reason code'.
     *
     * 
     *
     */
    void setReasonCode(FilterReasonCode reasonCode);

    /**
     * Returns the property 'reason code'.
     *
     * 
     *
     */
    FilterReasonCode getReasonCode();
    /**
     * Setter for the property 'reason description'.
     *
     * 
     *
     */
    void setReasonDescription(String reasonDescription);

    /**
     * Returns the property 'reason description'.
     *
     * 
     *
     */
    String getReasonDescription();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationFilterReason asBrokerageApplicationFilterReason();
}
