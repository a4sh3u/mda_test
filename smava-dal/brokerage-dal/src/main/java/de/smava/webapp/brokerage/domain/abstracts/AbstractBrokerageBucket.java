//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket)}

import de.smava.webapp.brokerage.domain.interfaces.BrokerageBucketEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBuckets'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * A brokerage bucket is a virtual collection of brokerage applications that are requested at a the same time.
 *
 * @author generator
 */
public abstract class AbstractBrokerageBucket
    extends KreditPrivatEntity implements BrokerageBucketEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageBucket.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage bucket)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

