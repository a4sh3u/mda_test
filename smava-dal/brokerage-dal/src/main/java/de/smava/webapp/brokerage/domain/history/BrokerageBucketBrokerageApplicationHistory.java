package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBucketBrokerageApplication;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBucketBrokerageApplications'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBucketBrokerageApplicationHistory extends AbstractBrokerageBucketBrokerageApplication {



							
}
