package de.smava.webapp.brokerage.domain;

import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.brokerage.domain.history.AdvisorAssignmentLogHistory;

import java.util.Date;

public class AdvisorAssignmentLog extends AdvisorAssignmentLogHistory {

    protected Advisor _creditAdvisor;
    protected Long _customerNumber;
    protected Long _loanApplicationId;
    protected Date _assignmentDate;
    protected AdvisorAssignmentType _customerType;
    protected Advisor _createdBy;

    public void setCreditAdvisor(Advisor creditAdvisor) {
        _creditAdvisor = creditAdvisor;
    }

    public Advisor getCreditAdvisor() {
        return _creditAdvisor;
    }

    @Override
    public void setCustomerNumber(Long customerNumber) {
        this._customerNumber = customerNumber;
    }

    @Override
    public Long getCustomerNumber() {
        return _customerNumber;
    }

    public Long getLoanApplicationId() {
        return _loanApplicationId;
    }

    public void setLoanApplicationId(Long loanApplicationId) {
        this._loanApplicationId = loanApplicationId;
    }

    public void setAssignmentDate(Date assignmentDate) {
        if (!_assignmentDateIsSet) {
            _assignmentDateIsSet = true;
            _assignmentDateInitVal = getAssignmentDate();
        }
        registerChange("assignment date", _assignmentDateInitVal, assignmentDate);
        _assignmentDate = assignmentDate;
    }

    public Date getAssignmentDate() {
        return _assignmentDate;
    }

    public void setCustomerType(AdvisorAssignmentType customerType) {
        _customerType = customerType;
    }

    public AdvisorAssignmentType getCustomerType() {
        return _customerType;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AdvisorAssignmentLog.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AdvisorAssignmentLog.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public Advisor getCreatedBy() {
        return _createdBy;
    }

    public void setCreatedBy(Advisor createdBy) {
        this._createdBy = createdBy;
    }
}
