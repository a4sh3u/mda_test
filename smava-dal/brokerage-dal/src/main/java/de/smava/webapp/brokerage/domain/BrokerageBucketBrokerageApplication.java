//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket brokerage application)}
import de.smava.webapp.brokerage.domain.history.BrokerageBucketBrokerageApplicationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBucketBrokerageApplications'.
 *
 * A brokerage bucket is a virtual collection of brokerage applications that are requested at a the same time.
 *
 * @author generator
 */
public class BrokerageBucketBrokerageApplication extends BrokerageBucketBrokerageApplicationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bucket brokerage application)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageApplication _brokerageApplication;
        protected BrokerageBucket _brokerageBucket;
        protected BrokerageBucketConfiguration _brokerageBucketConfiguration;
        protected BrokerageInterestRatePredictionData _brokerageInterestRatePredictionData;
        protected BrokeragePayoutProbabilityPredictionData _brokeragePayoutProbabilityPredictionData;
        protected BrokerageInterestRateDetailsData _brokerageInterestRateDetailsData;
        protected BrokerageBucketReason _reason;
        
                                    
    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket'.
     *
     * 
     *
     */
    public void setBrokerageBucket(BrokerageBucket brokerageBucket) {
        _brokerageBucket = brokerageBucket;
    }
            
    /**
     * Returns the property 'brokerage bucket'.
     *
     * 
     *
     */
    public BrokerageBucket getBrokerageBucket() {
        return _brokerageBucket;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    public void setBrokerageBucketConfiguration(BrokerageBucketConfiguration brokerageBucketConfiguration) {
        _brokerageBucketConfiguration = brokerageBucketConfiguration;
    }
            
    /**
     * Returns the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    public BrokerageBucketConfiguration getBrokerageBucketConfiguration() {
        return _brokerageBucketConfiguration;
    }
                                            
    /**
     * Setter for the property 'brokerage interest rate prediction data'.
     *
     * 
     *
     */
    public void setBrokerageInterestRatePredictionData(BrokerageInterestRatePredictionData brokerageInterestRatePredictionData) {
        _brokerageInterestRatePredictionData = brokerageInterestRatePredictionData;
    }
            
    /**
     * Returns the property 'brokerage interest rate prediction data'.
     *
     * 
     *
     */
    public BrokerageInterestRatePredictionData getBrokerageInterestRatePredictionData() {
        return _brokerageInterestRatePredictionData;
    }
                                            
    /**
     * Setter for the property 'brokerage payout probability prediction data'.
     *
     * 
     *
     */
    public void setBrokeragePayoutProbabilityPredictionData(BrokeragePayoutProbabilityPredictionData brokeragePayoutProbabilityPredictionData) {
        _brokeragePayoutProbabilityPredictionData = brokeragePayoutProbabilityPredictionData;
    }
            
    /**
     * Returns the property 'brokerage payout probability prediction data'.
     *
     * 
     *
     */
    public BrokeragePayoutProbabilityPredictionData getBrokeragePayoutProbabilityPredictionData() {
        return _brokeragePayoutProbabilityPredictionData;
    }
                                            
    /**
     * Setter for the property 'brokerage interest rate details data'.
     *
     * 
     *
     */
    public void setBrokerageInterestRateDetailsData(BrokerageInterestRateDetailsData brokerageInterestRateDetailsData) {
        _brokerageInterestRateDetailsData = brokerageInterestRateDetailsData;
    }
            
    /**
     * Returns the property 'brokerage interest rate details data'.
     *
     * 
     *
     */
    public BrokerageInterestRateDetailsData getBrokerageInterestRateDetailsData() {
        return _brokerageInterestRateDetailsData;
    }
                                            
    /**
     * Setter for the property 'reason'.
     *
     * 
     *
     */
    public void setReason(BrokerageBucketReason reason) {
        _reason = reason;
    }
            
    /**
     * Returns the property 'reason'.
     *
     * 
     *
     */
    public BrokerageBucketReason getReason() {
        return _reason;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBucketBrokerageApplication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageBucketBrokerageApplication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBucketBrokerageApplication asBrokerageBucketBrokerageApplication() {
        return this;
    }
}
