package de.smava.webapp.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author asaadat
 * @since 02.08.2017.
 */
public class CustomerDocToBank {

    @JsonProperty
    private Long brokerageRefId;
    @JsonProperty
    private Long bankDocId;

    public Long getBrokerageRefId() {
        return brokerageRefId;
    }

    public void setBrokerageRefId(Long brokerageRefId) {
        this.brokerageRefId = brokerageRefId;
    }

    public Long getBankDocId() {
        return bankDocId;
    }

    public void setBankDocId(Long bankDocId) {
        this.bankDocId = bankDocId;
    }
}
