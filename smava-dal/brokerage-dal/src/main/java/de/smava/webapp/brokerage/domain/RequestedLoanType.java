package de.smava.webapp.brokerage.domain;

public enum RequestedLoanType {

	STANDARD,
	CAR,
	HOME,
	CONSOLIDATION;

}
