package de.smava.webapp.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public class SchufaInformation {

    @JsonProperty
    private Integer applicant;

    @JsonProperty
    private String partialScore;

    @JsonProperty
    private String riskCategory;

    @JsonProperty
    private List<SchufaAttribute> schufaAttributes;

    public Integer getApplicant() {
        return applicant;
    }

    public void setApplicant(Integer applicant) {
        this.applicant = applicant;
    }

    public String getPartialScore() {
        return partialScore;
    }

    public void setPartialScore(String partialScore) {
        this.partialScore = partialScore;
    }

    public String getRiskCategory() {
        return riskCategory;
    }

    public void setRiskCategory(String riskCategory) {
        this.riskCategory = riskCategory;
    }

    public List<SchufaAttribute> getSchufaAttributes() {
        if (this.schufaAttributes == null) {
            this.schufaAttributes = new ArrayList<SchufaAttribute>();
        }

        return this.schufaAttributes;
    }
}
