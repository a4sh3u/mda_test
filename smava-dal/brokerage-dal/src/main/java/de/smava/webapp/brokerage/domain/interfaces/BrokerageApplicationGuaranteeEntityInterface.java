package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.brokerage.domain.BrokerageApplicationGuarantee;
import de.smava.webapp.brokerage.domain.LoanApplication;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageApplicationGuarantees'.
 *
 * @author generator
 */
public interface BrokerageApplicationGuaranteeEntityInterface {

    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    LoanApplication getLoanApplication();
    /**
     * Setter for the property 'top chance'.
     *
     * 
     *
     */
    void setTopChance(Date topChance);

    /**
     * Returns the property 'top chance'.
     *
     * 
     *
     */
    Date getTopChance();
    /**
     * Setter for the property 'guarantee'.
     *
     * 
     *
     */
    void setGuarantee(Date guarantee);

    /**
     * Returns the property 'guarantee'.
     *
     * 
     *
     */
    Date getGuarantee();
    /**
     * Setter for the property 'reason'.
     *
     * 
     *
     */
    void setReason(String reason);

    /**
     * Returns the property 'reason'.
     *
     * 
     *
     */
    String getReason();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationGuarantee asBrokerageApplicationGuarantee();
}
