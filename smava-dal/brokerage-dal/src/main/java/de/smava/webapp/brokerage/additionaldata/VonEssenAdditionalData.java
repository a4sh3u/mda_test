package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Mikolaj Zymon
 * @since 18.01.2017
 */
public class VonEssenAdditionalData extends AbstractAdditionalData {
    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_VONESSEN;
    }
}
