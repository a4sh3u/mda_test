package de.smava.webapp.brokerage.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import de.smava.webapp.brokerage.domain.interfaces.EventQueueEntityInterface;
import de.smava.webapp.commons.domain.Change;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * The domain object that represents 'EventQueues'.
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@Id")
public class EventQueue implements EventQueueEntityInterface, Serializable, Entity {

    private static final Logger LOGGER = Logger.getLogger(EventQueue.class);

    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    protected Date _creationDate;
    protected de.smava.webapp.account.domain.Account _account;
    protected de.smava.webapp.brokerage.domain.LoanApplication _loanApplication;
    protected de.smava.webapp.brokerage.domain.EventQueueType _type;
    protected de.smava.webapp.brokerage.domain.EventQueueState _state;
    protected Date _dueDate;
    protected Date _expirationDate;
    protected de.smava.webapp.brokerage.domain.BrokerageBank _bank;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }

    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }

    /**
     * Setter for the property 'loan application'.
     */
    public void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }

    /**
     * Returns the property 'loan application'.
     */
    public de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }

    /**
     * Setter for the property 'type'.
     */
    public void setType(de.smava.webapp.brokerage.domain.EventQueueType type) {
        _type = type;
    }

    /**
     * Returns the property 'type'.
     */
    public de.smava.webapp.brokerage.domain.EventQueueType getType() {
        return _type;
    }

    /**
     * Setter for the property 'state'.
     */
    public void setState(de.smava.webapp.brokerage.domain.EventQueueState state) {
        _state = state;
    }

    /**
     * Returns the property 'state'.
     */
    public de.smava.webapp.brokerage.domain.EventQueueState getState() {
        return _state;
    }

    /**
     * Setter for the property 'due date'.
     */
    public void setDueDate(Date dueDate) {
        _dueDate = dueDate;
    }

    /**
     * Returns the property 'due date'.
     */
    public Date getDueDate() {
        return _dueDate;
    }

    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }

    /**
     * Setter for the property 'bank'.
     */
    public void setBank(de.smava.webapp.brokerage.domain.BrokerageBank bank) {
        _bank = bank;
    }

    /**
     * Returns the property 'bank'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBank() {
        return _bank;
    }

    public Double getCustomerValue() {
        return this.getLoanApplication().getCustomerValue();
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    @Deprecated
    @Override
    public void copyFromOldEntity(Entity oldEntity) {
    }

    @Deprecated
    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        return false;
    }

    @Deprecated
    @Override
    public void clearChanges() {
    }

    @Deprecated
    @Override
    public Map<String, Change> getChangeMap() {
        return null;
    }

    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(EventQueue.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(EventQueue.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
