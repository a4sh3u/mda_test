package de.smava.webapp.brokerage.domain.interfaces;

import de.smava.webapp.account.domain.Advisor;

import java.util.Date;

public interface AdvisorAssignmentLogEntityInterface {

    void setCreditAdvisor(de.smava.webapp.account.domain.Advisor creditAdvisor);

    de.smava.webapp.account.domain.Advisor getCreditAdvisor();

    void setCustomerNumber(Long customerNumber);

    Long getCustomerNumber();

    void setLoanApplicationId(Long loanApplicationId);

    Long getLoanApplicationId();

    void setAssignmentDate(Date assignmentDate);

    Date getAssignmentDate();

    void setCustomerType(de.smava.webapp.brokerage.domain.AdvisorAssignmentType customerType);

    de.smava.webapp.brokerage.domain.AdvisorAssignmentType getCustomerType();

    Advisor getCreatedBy();

    void setCreatedBy(Advisor createdBy);
}
