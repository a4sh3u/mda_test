package de.smava.webapp.brokerage.domain;

import de.smava.webapp.brokerage.domain.type.CustomerProcessReason;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.commons.domain.Change;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class CasiEventQueue implements Serializable, Entity {

    protected static final Logger LOGGER = Logger.getLogger(CasiEventQueue.class);

    public EventQueueType getType() {
        return EventQueueType.valueOf(_eventQueueType);
    }

    private Long _id;
    protected Date _eventQueueCreationDate;
    protected de.smava.webapp.account.domain.Account _account;
    protected de.smava.webapp.marketing.affiliate.domain.AffiliateInformation _saleAffiliateInfo;
    protected de.smava.webapp.brokerage.domain.LoanApplication _loanApplication;
    protected String _eventQueueType;
    protected String _eventQueueState;
    protected Date _eventQueueDueDate;
    protected Date _eventQueueExpirationDate;
    protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
    protected Integer _hasDocuments;
    protected boolean _eventQueueIsSchufaPositive;
    protected Long _customerStatusId;
    protected Date _customerStatusCreationDate;
    protected CustomerProcessState _customerStatusState;
    protected CustomerProcessReason _customerStatusReason;
    protected String _customerStatusCreatedBy;
    protected Double _customerValue;

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public void setEventQueueCreationDate(Date eventQueueCreationDate) {
        _eventQueueCreationDate = eventQueueCreationDate;
    }

    public Date getEventQueueCreationDate() {
        return _eventQueueCreationDate;
    }

    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }

    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }

    public void setSaleAffiliateInfo(de.smava.webapp.marketing.affiliate.domain.AffiliateInformation saleAffiliateInfo) {
        _saleAffiliateInfo = saleAffiliateInfo;
    }

    public de.smava.webapp.marketing.affiliate.domain.AffiliateInformation getSaleAffiliateInfo() {
        return _saleAffiliateInfo;
    }

    public void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }

    public de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }

    public void setEventQueueType(String eventQueueType) {
        _eventQueueType = eventQueueType;
    }

    public String getEventQueueType() {
        return _eventQueueType;
    }

    public void setEventQueueState(String eventQueueState) {
        _eventQueueState = eventQueueState;
    }

    public String getEventQueueState() {
        return _eventQueueState;
    }

    public void setEventQueueDueDate(Date eventQueueDueDate) {
        _eventQueueDueDate = eventQueueDueDate;
    }

    public Date getEventQueueDueDate() {
        return _eventQueueDueDate;
    }

    public void setEventQueueExpirationDate(Date eventQueueExpirationDate) {
        _eventQueueExpirationDate = eventQueueExpirationDate;
    }

    public Date getEventQueueExpirationDate() {
        return _eventQueueExpirationDate;
    }

    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }

    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }

    public void setHasDocuments(Integer hasDocuments) {
        _hasDocuments = hasDocuments;
    }

    public Integer getHasDocuments() {
        return _hasDocuments;
    }

    public void setEventQueueIsSchufaPositive(boolean eventQueueIsSchufaPositive) {
        _eventQueueIsSchufaPositive = eventQueueIsSchufaPositive;
    }

    public boolean getEventQueueIsSchufaPositive() {
        return _eventQueueIsSchufaPositive;
    }

    public void setCustomerStatusId(Long customerStatusId) {
        _customerStatusId = customerStatusId;
    }

    public Long getCustomerStatusId() {
        return _customerStatusId;
    }

    public void setCustomerStatusCreationDate(Date customerStatusCreationDate) {
        _customerStatusCreationDate = customerStatusCreationDate;
    }

    public Date getCustomerStatusCreationDate() {
        return _customerStatusCreationDate;
    }

    public void setCustomerStatusState(CustomerProcessState customerStatusState) {
        _customerStatusState = customerStatusState;
    }

    public CustomerProcessState getCustomerStatusState() {
        return _customerStatusState;
    }

    public void setCustomerStatusReason(CustomerProcessReason customerStatusReason) {
        _customerStatusReason = customerStatusReason;
    }

    public CustomerProcessReason getCustomerStatusReason() {
        return _customerStatusReason;
    }

    public void setCustomerStatusCreatedBy(String customerStatusCreatedBy) {
        _customerStatusCreatedBy = customerStatusCreatedBy;
    }

    public String getCustomerStatusCreatedBy() {
        return _customerStatusCreatedBy;
    }

    public void setCustomerValue(Double customerValue) {
        _customerValue = customerValue;
    }

    public Double getCustomerValue() {
        return _customerValue;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    @Deprecated
    @Override
    public void copyFromOldEntity(Entity oldEntity) {
    }

    @Deprecated
    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        return false;
    }

    @Deprecated
    @Override
    public void clearChanges() {
    }

    @Deprecated
    @Override
    public Map<String, Change> getChangeMap() {
        return null;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CasiEventQueue.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _eventQueueType=").append(_eventQueueType);
            builder.append("\n    _eventQueueState=").append(_eventQueueState);
            builder.append("\n    _eventQueueIsSchufaPositive=").append(_eventQueueIsSchufaPositive);
            builder.append("\n    _customerStatusCreatedBy=").append(_customerStatusCreatedBy);
            builder.append("\n}");
        } else {
            builder.append(CasiEventQueue.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
