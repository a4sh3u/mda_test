//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application guarantee)}
import java.util.Collection;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.brokerage.domain.BrokerageApplicationGuarantee;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageApplicationGuarantees'.
 *
 * @author generator
 */
public interface BrokerageApplicationGuaranteeDao extends BaseDao<BrokerageApplicationGuarantee> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage application guarantee identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageApplicationGuarantee getBrokerageApplicationGuarantee(Long id);

    /**
     * Saves the brokerage application guarantee.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageApplicationGuarantee(BrokerageApplicationGuarantee brokerageApplicationGuarantee);

    /**
     * Deletes an brokerage application guarantee, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application guarantee
     */
    void deleteBrokerageApplicationGuarantee(Long id);

    /**
     * Retrieves all 'BrokerageApplicationGuarantee' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageApplicationGuarantee> getBrokerageApplicationGuaranteeList();

    /**
     * Retrieves a page of 'BrokerageApplicationGuarantee' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageApplicationGuarantee> getBrokerageApplicationGuaranteeList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageApplicationGuarantee' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageApplicationGuarantee> getBrokerageApplicationGuaranteeList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageApplicationGuarantee' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageApplicationGuarantee> getBrokerageApplicationGuaranteeList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageApplicationGuarantee' instances.
     */
    long getBrokerageApplicationGuaranteeCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application guarantee)}
    //
    // insert custom methods here
    
    public BrokerageApplicationGuarantee getBrokerageApplicationGuarantee(BrokerageApplication ba);
    List<BrokerageApplicationGuarantee> getBrokerageApplicationGuarantee(LoanApplication la);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
