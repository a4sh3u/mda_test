package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageMappingConverter;
import de.smava.webapp.brokerage.domain.ConverterType;


/**
 * The domain object that represents 'BrokerageMappingConverters'.
 *
 * @author generator
 */
public interface BrokerageMappingConverterEntityInterface {

    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(ConverterType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    ConverterType getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageMappingConverter asBrokerageMappingConverter();
}
