//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.loancheck.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan check)}
import de.smava.webapp.brokerage.loancheck.domain.history.LoanCheckHistory;

import java.util.Date;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanChecks'.
 *
 * @author generator
 */
public class LoanCheck extends LoanCheckHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(loan check)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected LoanCheckType _type;
        protected de.smava.webapp.account.domain.Account _account;
        protected Date _completionDate;
        protected Set<LoanCheckRequest> _loanCheckRequests;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'type'.
     */
    public void setType(LoanCheckType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     */
    public LoanCheckType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'completion date'.
     */
    public void setCompletionDate(Date completionDate) {
        if (!_completionDateIsSet) {
            _completionDateIsSet = true;
            _completionDateInitVal = getCompletionDate();
        }
        registerChange("completion date", _completionDateInitVal, completionDate);
        _completionDate = completionDate;
    }
                        
    /**
     * Returns the property 'completion date'.
     */
    public Date getCompletionDate() {
        return _completionDate;
    }
                                            
    /**
     * Setter for the property 'loan check requests'.
     */
    public void setLoanCheckRequests(Set<LoanCheckRequest> loanCheckRequests) {
        _loanCheckRequests = loanCheckRequests;
    }
            
    /**
     * Returns the property 'loan check requests'.
     */
    public Set<LoanCheckRequest> getLoanCheckRequests() {
        return _loanCheckRequests;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LoanCheck.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(LoanCheck.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LoanCheck asLoanCheck() {
        return this;
    }
}
