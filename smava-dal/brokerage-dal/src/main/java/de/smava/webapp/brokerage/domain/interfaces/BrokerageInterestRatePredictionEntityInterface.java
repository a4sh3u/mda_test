package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageInterestRatePrediction;
import de.smava.webapp.brokerage.domain.BrokerageInterestRatePredictionData;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageInterestRatePredictions'.
 *
 * @author generator
 */
public interface BrokerageInterestRatePredictionEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'activation date'.
     *
     * 
     *
     */
    void setActivationDate(Date activationDate);

    /**
     * Returns the property 'activation date'.
     *
     * 
     *
     */
    Date getActivationDate();
    /**
     * Setter for the property 'brokerage interest rate prediction datas'.
     *
     * 
     *
     */
    void setBrokerageInterestRatePredictionDatas(Set<BrokerageInterestRatePredictionData> brokerageInterestRatePredictionDatas);

    /**
     * Returns the property 'brokerage interest rate prediction datas'.
     *
     * 
     *
     */
    Set<BrokerageInterestRatePredictionData> getBrokerageInterestRatePredictionDatas();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageInterestRatePrediction asBrokerageInterestRatePrediction();
}
