package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokeragePayoutProbabilityPrediction;
import de.smava.webapp.brokerage.domain.BrokeragePayoutProbabilityPredictionData;


/**
 * The domain object that represents 'BrokeragePayoutProbabilityPredictionDatas'.
 *
 * @author generator
 */
public interface BrokeragePayoutProbabilityPredictionDataEntityInterface {

    /**
     * Setter for the property 'brokerage payout probability prediction'.
     *
     * 
     *
     */
    void setBrokeragePayoutProbabilityPrediction(BrokeragePayoutProbabilityPrediction brokeragePayoutProbabilityPrediction);

    /**
     * Returns the property 'brokerage payout probability prediction'.
     *
     * 
     *
     */
    BrokeragePayoutProbabilityPrediction getBrokeragePayoutProbabilityPrediction();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'requested amount min'.
     *
     * 
     *
     */
    void setRequestedAmountMin(Double requestedAmountMin);

    /**
     * Returns the property 'requested amount min'.
     *
     * 
     *
     */
    Double getRequestedAmountMin();
    /**
     * Setter for the property 'requested amount max'.
     *
     * 
     *
     */
    void setRequestedAmountMax(Double requestedAmountMax);

    /**
     * Returns the property 'requested amount max'.
     *
     * 
     *
     */
    Double getRequestedAmountMax();
    /**
     * Setter for the property 'requested duration min'.
     *
     * 
     *
     */
    void setRequestedDurationMin(Integer requestedDurationMin);

    /**
     * Returns the property 'requested duration min'.
     *
     * 
     *
     */
    Integer getRequestedDurationMin();
    /**
     * Setter for the property 'requested duration max'.
     *
     * 
     *
     */
    void setRequestedDurationMax(Integer requestedDurationMax);

    /**
     * Returns the property 'requested duration max'.
     *
     * 
     *
     */
    Integer getRequestedDurationMax();
    /**
     * Setter for the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    void setIncomeMin(Double incomeMin);

    /**
     * Returns the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    Double getIncomeMin();
    /**
     * Setter for the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    void setIncomeMax(Double incomeMax);

    /**
     * Returns the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    Double getIncomeMax();
    /**
     * Setter for the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    void setSchufaAvailable(Boolean schufaAvailable);

    /**
     * Returns the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    Boolean getSchufaAvailable();
    /**
     * Setter for the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    void setSchufaMin(Integer schufaMin);

    /**
     * Returns the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    Integer getSchufaMin();
    /**
     * Setter for the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    void setSchufaMax(Integer schufaMax);

    /**
     * Returns the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    Integer getSchufaMax();
    /**
     * Setter for the property 'payout'.
     *
     * 
     *
     */
    void setPayout(Double payout);

    /**
     * Returns the property 'payout'.
     *
     * 
     *
     */
    Double getPayout();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokeragePayoutProbabilityPredictionData asBrokeragePayoutProbabilityPredictionData();
}
