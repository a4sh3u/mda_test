package de.smava.webapp.brokerage.domain;

public enum EventQueueType {
	/**
	 * created by task : handleRecentlyActiveBorrowersJob
	 * 
	 * requirements: 
	 * - Latest LoanApplication is in state APPLICATION_INCOMPLETE
	 * - Latest EconomicalData of type EconomicalDataType.CUSTOMER less than 3 days old (no minimal age)
	 * - Latest LoanApplication has InitiatorType LoanApplicationInitiatorType.CUSTOMER
	 * - The created event would not be expired (CreationDate of LoanApplication + validity <= now)
	 * 
	 */
	BORROWER_CREDIT_CONSULTING_FIRST_CALL_SHORTLEAD("BORROWER_CREDIT_CONSULTING_FIRST_CALL_SHORTLEAD"),
	
	/**
	 * created by task : handleRecentlyActiveBorrowersJob
	 * 
	 * requirements: 
	 * - Latest LoanApplication is in state APPLICATION_APPLIED
	 * - Latest LoanApplication has a minimum number of BrokerageApplications in state APPLICATION_APPLIED (BO configurable)
	 * - Latest LoanApplication has am minimum requested amount (BO configurable)
	 * - Latest LoanApplication is not initiated by a CreditAdvisor
	 * - Latest EconomicalData of type EconomicalDataType.CUSTOMER less than 3 days old (no minimal age)
	 * - Latest LoanApplication has InitiatorType LoanApplicationInitiatorType.CUSTOMER
	 * - The created event would not be expired (CreationDate of LoanApplication + validity <= now)
	 * 
	 */
	BORROWER_CREDIT_CONSULTING_FIRST_CALL("BORROWER_CREDIT_CONSULTING_FIRST_CALL"),
	
	/**
	 * created by a credit advisor action CUSTOMER_NOT_AVAILABLE
	 * 
	 * requirements: 
	 * - The customer was found in CASI by a BORROWER_CREDIT_CONSULTING_FIRST_CALL_SHORTLEAD event
	 */
	BORROWER_CREDIT_CONSULTING_SECOND_CALL_SHORTLEAD("BORROWER_CREDIT_CONSULTING_SECOND_CALL_SHORTLEAD"),
	
	/**
	 * created by a credit advisor action CUSTOMER_NOT_AVAILABLE
	 * 
	 * requirements: 
	 * - The customer was found in CASI by a BORROWER_CREDIT_CONSULTING_FIRST_CALL event
	 */
	BORROWER_CREDIT_CONSULTING_SECOND_CALL("BORROWER_CREDIT_CONSULTING_SECOND_CALL"),
	
	/**
	 * created by task : performPeriodicalLoanChecksJob
	 * 
	 * requirements: 
	 * - The first payout notice (BARD in state APPLICATION_PAYOUT) exactly 720 + ( n *90) days old
	 * - At least one LoanCheckRequest in state APPLICATION_PRECHECK_GREEN or APPLICATION_PRECHECK_YELLOW
	 * - The original BrokerageApplication (the one to consolidate) has a minimum remaining duration of 12 months
	 * - The total saving from the new offer against the original BrokerageApplication is minimum 50 Euros
	 * 
	 */
	BORROWER_CREDIT_CONSULTING_CONSOLIDATION_CHECK("BORROWER_CREDIT_CONSULTING_CONSOLIDATION_CHECK"),
	
	/**
	 * created by a status update message. BrokerageApplicationRequestData with type TYPE_STATE_REQUEST
	 * 
	  * requirements: 
	 * - State of BrokerageApplication is switching to APPLICATION_DOCUMENTS_MISSING (from any other)
	 */
	BORROWER_CREDIT_CONSULTING_DOCUMENTS_MISSING("BORROWER_CREDIT_CONSULTING_DOCUMENTS_MISSING"),
	
	
	/**
	 * created by task :customerDidNotsendDocumentsJob
	 * 
	 * requirements:
	 * - The customer has a successful advisory with a send in until date that is expired
	 * - The BrokerageApplication is in state BrokerageState.APPLICATION_APPLIED or BrokerageState.APPLICATION_PENDING
	 */
	BORROWER_CREDIT_CONSULTING_DOCUMENTS_NOT_SENT("BORROWER_CREDIT_CONSULTING_DOCUMENTS_NOT_SENT"),
	
	/**
	 * created by a status update message. BrokerageApplicationRequestData with type TYPE_STATE_REQUEST
	 * 
	  * requirements: 
	 * - State of BrokerageApplication is switching to APPLICATION_DENIED or APPLICATION_DENIED_OFFLINE (from any other)
	 * - The customer has a successful advisory for this BrokerageApplication
	 */
	BORROWER_CREDIT_CONSULTING_FAILED_PAYOUT("BORROWER_CREDIT_CONSULTING_FAILED_PAYOUT"),
	
	/**
	 * created by a status update message. BrokerageApplicationRequestData with type TYPE_STATE_REQUEST
	 * 
	  * requirements: 
	 * - Postbank only response code that identifies that there is an alternative offer for this customer
	 */
	BORROWER_CREDIT_CONSULTING_MANUAL_ALTERNATIVE_OFFER("BORROWER_CREDIT_CONSULTING_MANUAL_ALTERNATIVE_OFFER"),

    /**
     * created by task : handleRecentlyActiveBorrowersJob
     *
     * requirements:
     * - {@link de.smava.webapp.brokerage.domain.LoanApplication#_state} == {@link de.smava.webapp.brokerage.domain.BrokerageState#APPLICATION_INCOMPLETE}
     * - {@link de.smava.webapp.brokerage.domain.LoanApplication#_creationDate} > now - x minutes (BO configurable)
     * - {@link de.smava.webapp.brokerage.domain.LoanApplication#_initiatorType} == {@link de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType#CUSTOMER}
     * - The created event would not be expired (CreationDate of LoanApplication + validity <= now)
     */
    BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT("BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT"),

    /**
     * WEBSITE-14068
     * created by task : handleRecentlyActiveBorrowersJob
     *
     * requirements: http://intranet.smava.de/wiki/index.php/BIRT_-_SQL_queries#Call_Us_report
     */
    BORROWER_CREDIT_CONSULTING_ADVISORY_NO_OFFER("BORROWER_CREDIT_CONSULTING_ADVISORY_NO_OFFER"),

    /**
     * WEBSITE-14068
     * created on journal entry processing if CUSTOMER_NOT_AVAILABLE
     *
     */
    BORROWER_CREDIT_CONSULTING_ADVISORY_NO_OFFER_SECONDARY_CALL("BORROWER_CREDIT_CONSULTING_ADVISORY_NO_OFFER_SECONDARY_CALL"),

    /**
     * created by a credit advisor action CUSTOMER_NOT_AVAILABLE
     *
     * requirements:
     * - The customer was found in CASI by a BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT event
     */
    BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT_ADDITIONAL_CALL("BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT_ADDITIONAL_CALL");

    private final String value;

    EventQueueType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EventQueueType fromValue(String v) {
        for (EventQueueType c : EventQueueType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
