package de.smava.webapp.brokerage.domain.interfaces;

import de.smava.webapp.brokerage.domain.DocumentPlaceholder;
import de.smava.webapp.brokerage.domain.DocumentType;
import de.smava.webapp.brokerage.domain.DocumentTypePlaceholder;


/**
 * The domain object that represents 'DocumentTypePlaceholders'.
 *
 * @author generator
 */
public interface DocumentTypePlaceholderEntityInterface {

    /**
     * Setter for the property 'document type'.
     *
     * 
     *
     */
    public void setDocumentType(DocumentType documentType);

    /**
     * Returns the property 'document type'.
     *
     * 
     *
     */
    public DocumentType getDocumentType();
    /**
     * Setter for the property 'document placeholder'.
     *
     * 
     *
     */
    public void setDocumentPlaceholder(DocumentPlaceholder documentPlaceholder);

    /**
     * Returns the property 'document placeholder'.
     *
     * 
     *
     */
    public DocumentPlaceholder getDocumentPlaceholder();
    /**
     * Helper method to get reference of this object as model type.
     */
    public DocumentTypePlaceholder asDocumentTypePlaceholder();
}
