package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author pobrebski
 * @since 21.11.2016.
 */
public class AbkAdditionalData extends AbstractAdditionalData {

    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_ABK;
    }
}
