//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}

import de.smava.webapp.brokerage.domain.history.BrokerageApplicationHistory;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplications'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageApplication extends BrokerageApplicationHistory  implements de.smava.webapp.brokerage.domain.RemoteBankRequestDataIf {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application)}
    public BrokerageApplication(){

    }
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected BrokerageBank _brokerageBank;
        protected String _extRefNumber;
        protected String _prevExtRefNumber;
        protected BrokerageState _state;
        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.EconomicalData _economicalData;
        protected de.smava.webapp.account.domain.Person _firstPerson;
        protected de.smava.webapp.account.domain.Person _secondPerson;
        protected de.smava.webapp.account.domain.BankAccount _referenceBankAccount;
        protected Set<de.smava.webapp.account.domain.Address> _addresses;
        protected String _requestedRdiType;
        protected double _requestedAmount;
        protected de.smava.webapp.account.domain.Category _reqeustedCategory;
        protected int _requestedDuration;
        protected Double _monthlyRate;
        protected Double _effectiveInterest;
        protected Double _amount;
        protected Integer _duration;
        protected String _rdiType;
        protected BrokerageApplicationFilterReason _brokerageApplicationFilterReason;
        protected de.smava.webapp.brokerage.domain.LoanApplication _loanApplication;
        protected de.smava.webapp.brokerage.domain.RequestedLoanType _requestedLoanType;
        protected Date _papSaleTrackedDate;
        protected Date _lastStateRequest;
        protected Date _lastStateChange;
        protected de.smava.webapp.account.domain.DocumentContainer _documentContainer;
        protected Date _emailCreated;
        protected String _emailType;
        protected Date _documentsRequested;
        protected Date _documentsSent;
        protected de.smava.webapp.brokerage.domain.BrokerageCarFinance _brokerageCarFinance;
        protected String _additionalData;
        protected Date _payoutDate;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'ext ref number'.
     *
     * 
     *
     */
    public void setExtRefNumber(String extRefNumber) {
        if (!_extRefNumberIsSet) {
            _extRefNumberIsSet = true;
            _extRefNumberInitVal = getExtRefNumber();
        }
        registerChange("ext ref number", _extRefNumberInitVal, extRefNumber);
        _extRefNumber = extRefNumber;
    }
                        
    /**
     * Returns the property 'ext ref number'.
     *
     * 
     *
     */
    public String getExtRefNumber() {
        return _extRefNumber;
    }
                                    /**
     * Setter for the property 'prev ext ref number'.
     *
     * 
     *
     */
    public void setPrevExtRefNumber(String prevExtRefNumber) {
        if (!_prevExtRefNumberIsSet) {
            _prevExtRefNumberIsSet = true;
            _prevExtRefNumberInitVal = getPrevExtRefNumber();
        }
        registerChange("prev ext ref number", _prevExtRefNumberInitVal, prevExtRefNumber);
        _prevExtRefNumber = prevExtRefNumber;
    }
                        
    /**
     * Returns the property 'prev ext ref number'.
     *
     * 
     *
     */
    public String getPrevExtRefNumber() {
        return _prevExtRefNumber;
    }
                                            
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(BrokerageState state) {
        _state = state;
    }
            
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public BrokerageState getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    public void setEconomicalData(de.smava.webapp.account.domain.EconomicalData economicalData) {
        _economicalData = economicalData;
    }
            
    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.EconomicalData getEconomicalData() {
        return _economicalData;
    }
                                            
    /**
     * Setter for the property 'first person'.
     *
     * 
     *
     */
    public void setFirstPerson(de.smava.webapp.account.domain.Person firstPerson) {
        _firstPerson = firstPerson;
    }
            
    /**
     * Returns the property 'first person'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Person getFirstPerson() {
        return _firstPerson;
    }
                                            
    /**
     * Setter for the property 'second person'.
     *
     * 
     *
     */
    public void setSecondPerson(de.smava.webapp.account.domain.Person secondPerson) {
        _secondPerson = secondPerson;
    }
            
    /**
     * Returns the property 'second person'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Person getSecondPerson() {
        return _secondPerson;
    }
                                            
    /**
     * Setter for the property 'reference bank account'.
     *
     * 
     *
     */
    public void setReferenceBankAccount(de.smava.webapp.account.domain.BankAccount referenceBankAccount) {
        _referenceBankAccount = referenceBankAccount;
    }
            
    /**
     * Returns the property 'reference bank account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.BankAccount getReferenceBankAccount() {
        return _referenceBankAccount;
    }
                                            
    /**
     * Setter for the property 'addresses'.
     *
     * 
     *
     */
    public void setAddresses(Set<de.smava.webapp.account.domain.Address> addresses) {
        _addresses = addresses;
    }
            
    /**
     * Returns the property 'addresses'.
     *
     * 
     *
     */
    public Set<de.smava.webapp.account.domain.Address> getAddresses() {
        return _addresses;
    }
                                    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    public void setRequestedRdiType(String requestedRdiType) {
        if (!_requestedRdiTypeIsSet) {
            _requestedRdiTypeIsSet = true;
            _requestedRdiTypeInitVal = getRequestedRdiType();
        }
        registerChange("requested rdi type", _requestedRdiTypeInitVal, requestedRdiType);
        _requestedRdiType = requestedRdiType;
    }
                        
    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    public String getRequestedRdiType() {
        return _requestedRdiType;
    }
                                    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    public void setRequestedAmount(double requestedAmount) {
        if (!_requestedAmountIsSet) {
            _requestedAmountIsSet = true;
            _requestedAmountInitVal = getRequestedAmount();
        }
        registerChange("requested amount", _requestedAmountInitVal, requestedAmount);
        _requestedAmount = requestedAmount;
    }
                        
    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    public double getRequestedAmount() {
        return _requestedAmount;
    }
                                            
    /**
     * Setter for the property 'reqeusted category'.
     *
     * 
     *
     */
    public void setReqeustedCategory(de.smava.webapp.account.domain.Category reqeustedCategory) {
        _reqeustedCategory = reqeustedCategory;
    }
            
    /**
     * Returns the property 'reqeusted category'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Category getReqeustedCategory() {
        return _reqeustedCategory;
    }
                                    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    public void setRequestedDuration(int requestedDuration) {
        if (!_requestedDurationIsSet) {
            _requestedDurationIsSet = true;
            _requestedDurationInitVal = getRequestedDuration();
        }
        registerChange("requested duration", _requestedDurationInitVal, requestedDuration);
        _requestedDuration = requestedDuration;
    }
                        
    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    public int getRequestedDuration() {
        return _requestedDuration;
    }
                                            
    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    public void setMonthlyRate(Double monthlyRate) {
        _monthlyRate = monthlyRate;
    }
            
    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    public Double getMonthlyRate() {
        return _monthlyRate;
    }
                                            
    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    public void setEffectiveInterest(Double effectiveInterest) {
        _effectiveInterest = effectiveInterest;
    }
            
    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    public Double getEffectiveInterest() {
        return _effectiveInterest;
    }
                                            
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(Double amount) {
        _amount = amount;
    }
            
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public Double getAmount() {
        return _amount;
    }
                                            
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    public void setDuration(Integer duration) {
        _duration = duration;
    }
            
    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    public Integer getDuration() {
        return _duration;
    }
                                    /**
     * Setter for the property 'rdi type'.
     *
     * 
     *
     */
    public void setRdiType(String rdiType) {
        if (!_rdiTypeIsSet) {
            _rdiTypeIsSet = true;
            _rdiTypeInitVal = getRdiType();
        }
        registerChange("rdi type", _rdiTypeInitVal, rdiType);
        _rdiType = rdiType;
    }
                        
    /**
     * Returns the property 'rdi type'.
     *
     * 
     *
     */
    public String getRdiType() {
        return _rdiType;
    }
                                            
    /**
     * Setter for the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    public void setBrokerageApplicationFilterReason(BrokerageApplicationFilterReason brokerageApplicationFilterReason) {
        _brokerageApplicationFilterReason = brokerageApplicationFilterReason;
    }
            
    /**
     * Returns the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    public BrokerageApplicationFilterReason getBrokerageApplicationFilterReason() {
        return _brokerageApplicationFilterReason;
    }
                                            
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                            
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    public void setRequestedLoanType(de.smava.webapp.brokerage.domain.RequestedLoanType requestedLoanType) {
        _requestedLoanType = requestedLoanType;
    }
            
    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType() {
        return _requestedLoanType;
    }
                                    /**
     * Setter for the property 'pap sale tracked date'.
     *
     * 
     *
     */
    public void setPapSaleTrackedDate(Date papSaleTrackedDate) {
        if (!_papSaleTrackedDateIsSet) {
            _papSaleTrackedDateIsSet = true;
            _papSaleTrackedDateInitVal = getPapSaleTrackedDate();
        }
        registerChange("pap sale tracked date", _papSaleTrackedDateInitVal, papSaleTrackedDate);
        _papSaleTrackedDate = papSaleTrackedDate;
    }
                        
    /**
     * Returns the property 'pap sale tracked date'.
     *
     * 
     *
     */
    public Date getPapSaleTrackedDate() {
        return _papSaleTrackedDate;
    }
                                    /**
     * Setter for the property 'last state request'.
     *
     * 
     *
     */
    public void setLastStateRequest(Date lastStateRequest) {
        if (!_lastStateRequestIsSet) {
            _lastStateRequestIsSet = true;
            _lastStateRequestInitVal = getLastStateRequest();
        }
        registerChange("last state request", _lastStateRequestInitVal, lastStateRequest);
        _lastStateRequest = lastStateRequest;
    }
                        
    /**
     * Returns the property 'last state request'.
     *
     * 
     *
     */
    public Date getLastStateRequest() {
        return _lastStateRequest;
    }
                                    /**
     * Setter for the property 'last state change'.
     *
     * 
     *
     */
    public void setLastStateChange(Date lastStateChange) {
        if (!_lastStateChangeIsSet) {
            _lastStateChangeIsSet = true;
            _lastStateChangeInitVal = getLastStateChange();
        }
        registerChange("last state change", _lastStateChangeInitVal, lastStateChange);
        _lastStateChange = lastStateChange;
    }
                        
    /**
     * Returns the property 'last state change'.
     *
     * 
     *
     */
    public Date getLastStateChange() {
        return _lastStateChange;
    }
                                            
    /**
     * Setter for the property 'document container'.
     *
     * 
     *
     */
    public void setDocumentContainer(de.smava.webapp.account.domain.DocumentContainer documentContainer) {
        _documentContainer = documentContainer;
    }
            
    /**
     * Returns the property 'document container'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.DocumentContainer getDocumentContainer() {
        return _documentContainer;
    }
                                    /**
     * Setter for the property 'email created'.
     *
     * 
     *
     */
    public void setEmailCreated(Date emailCreated) {
        if (!_emailCreatedIsSet) {
            _emailCreatedIsSet = true;
            _emailCreatedInitVal = getEmailCreated();
        }
        registerChange("email created", _emailCreatedInitVal, emailCreated);
        _emailCreated = emailCreated;
    }
                        
    /**
     * Returns the property 'email created'.
     *
     * 
     *
     */
    public Date getEmailCreated() {
        return _emailCreated;
    }
                                    /**
     * Setter for the property 'email type'.
     *
     * 
     *
     */
    public void setEmailType(String emailType) {
        if (!_emailTypeIsSet) {
            _emailTypeIsSet = true;
            _emailTypeInitVal = getEmailType();
        }
        registerChange("email type", _emailTypeInitVal, emailType);
        _emailType = emailType;
    }
                        
    /**
     * Returns the property 'email type'.
     *
     * 
     *
     */
    public String getEmailType() {
        return _emailType;
    }
                                    /**
     * Setter for the property 'documents requested'.
     *
     * 
     *
     */
    public void setDocumentsRequested(Date documentsRequested) {
        if (!_documentsRequestedIsSet) {
            _documentsRequestedIsSet = true;
            _documentsRequestedInitVal = getDocumentsRequested();
        }
        registerChange("documents requested", _documentsRequestedInitVal, documentsRequested);
        _documentsRequested = documentsRequested;
    }
                        
    /**
     * Returns the property 'documents requested'.
     *
     * 
     *
     */
    public Date getDocumentsRequested() {
        return _documentsRequested;
    }
                                    /**
     * Setter for the property 'documents sent'.
     *
     * 
     *
     */
    public void setDocumentsSent(Date documentsSent) {
        if (!_documentsSentIsSet) {
            _documentsSentIsSet = true;
            _documentsSentInitVal = getDocumentsSent();
        }
        registerChange("documents sent", _documentsSentInitVal, documentsSent);
        _documentsSent = documentsSent;
    }
                        
    /**
     * Returns the property 'documents sent'.
     *
     * 
     *
     */
    public Date getDocumentsSent() {
        return _documentsSent;
    }
                                            
    /**
     * Setter for the property 'brokerage car finance'.
     *
     * 
     *
     */
    public void setBrokerageCarFinance(de.smava.webapp.brokerage.domain.BrokerageCarFinance brokerageCarFinance) {
        _brokerageCarFinance = brokerageCarFinance;
    }
            
    /**
     * Returns the property 'brokerage car finance'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.BrokerageCarFinance getBrokerageCarFinance() {
        return _brokerageCarFinance;
    }
                                    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    public void setAdditionalData(String additionalData) {
        if (!_additionalDataIsSet) {
            _additionalDataIsSet = true;
            _additionalDataInitVal = getAdditionalData();
        }
        registerChange("additional data", _additionalDataInitVal, additionalData);
        _additionalData = additionalData;
    }
                        
    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    public String getAdditionalData() {
        return _additionalData;
    }
                                    /**
     * Setter for the property 'payout date'.
     *
     * 
     *
     */
    public void setPayoutDate(Date payoutDate) {
        if (!_payoutDateIsSet) {
            _payoutDateIsSet = true;
            _payoutDateInitVal = getPayoutDate();
        }
        registerChange("payout date", _payoutDateInitVal, payoutDate);
        _payoutDate = payoutDate;
    }
                        
    /**
     * Returns the property 'payout date'.
     *
     * 
     *
     */
    public Date getPayoutDate() {
        return _payoutDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _extRefNumber=").append(_extRefNumber);
            builder.append("\n    _prevExtRefNumber=").append(_prevExtRefNumber);
            builder.append("\n    _requestedRdiType=").append(_requestedRdiType);
            builder.append("\n    _requestedAmount=").append(_requestedAmount);
            builder.append("\n    _requestedDuration=").append(_requestedDuration);
            builder.append("\n    _rdiType=").append(_rdiType);
            builder.append("\n    _emailType=").append(_emailType);
            builder.append("\n    _additionalData=").append(_additionalData);
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplication asBrokerageApplication() {
        return this;
    }
}
