package de.smava.webapp.brokerage.domain.type;

import java.util.Arrays;
import java.util.List;

public enum CustomerProcessState {

    BEI_BANK(new CustomerProcessReason[]{
            CustomerProcessReason.LAUT_BANK,
            CustomerProcessReason.LAUT_KUNDE,
            CustomerProcessReason.FEHLENDE_UNTERLAGEN
    }),

    ERREICHT(new CustomerProcessReason[]{
            CustomerProcessReason.REICHT_EIN
    }),

    IN_KLARUNG(new CustomerProcessReason[] {
            CustomerProcessReason.WEITERER_ANRUFVERSUCH
    }),

    ABGESCHLOSSEN(new CustomerProcessReason[]{
            CustomerProcessReason.AUSGEZAHLT,
            CustomerProcessReason.KEIN_BEDARF,
            CustomerProcessReason.BESSERES_ANGEBOT,
            CustomerProcessReason.NICHT_ERREICHT,
            CustomerProcessReason.KUNDE_UNZUFRIEDEN,
            CustomerProcessReason.BERUF,
            CustomerProcessReason.HHR,
            CustomerProcessReason.SCHUFA,
            CustomerProcessReason.FAKE,
            CustomerProcessReason.SONSTIGES
    }),

    GENEHMIGT(new CustomerProcessReason[]{
            CustomerProcessReason.LAUT_BANK,
            CustomerProcessReason.LAUT_KUNDE,
            CustomerProcessReason.MISSING_CASES
    }),

    NICHT_ERREICHT(new CustomerProcessReason[]{
            CustomerProcessReason.SHORT_LEAD,
            CustomerProcessReason.ANTRAG_VERSCHICKT
    }),

    OFFEN(new CustomerProcessReason[]{
            CustomerProcessReason.BESTANDSKUNDE,
            CustomerProcessReason.NEUKUNDE
    }),

    VERTRETUNG(new CustomerProcessReason[]{
            CustomerProcessReason.VERTRETUNG
    });

    private final List<CustomerProcessReason> reasons;

    CustomerProcessState(CustomerProcessReason[] reasons) {
        this.reasons = Arrays.asList(reasons);
    }


    public List<CustomerProcessReason> getReasons(){
        return reasons;
    }
}