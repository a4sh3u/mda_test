//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage interest rate details)}

import de.smava.webapp.brokerage.dao.BrokerageInterestRateDetailsDao;
import de.smava.webapp.brokerage.domain.BrokerageInterestRateDetails;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import javax.transaction.Synchronization;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BrokerageInterestRateDetailss'.
 *
 * @author generator
 */
@Repository(value = "brokerageInterestRateDetailsDao")
public class JdoBrokerageInterestRateDetailsDao extends JdoBaseDao implements BrokerageInterestRateDetailsDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageInterestRateDetailsDao.class);

    private static final String CLASS_NAME = "BrokerageInterestRateDetails";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(brokerage interest rate details)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the brokerage interest rate details identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BrokerageInterestRateDetails load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BrokerageInterestRateDetails result = getEntity(BrokerageInterestRateDetails.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BrokerageInterestRateDetails getBrokerageInterestRateDetails(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BrokerageInterestRateDetails entity = findUniqueEntity(BrokerageInterestRateDetails.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the brokerage interest rate details.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BrokerageInterestRateDetails brokerageInterestRateDetails) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageInterestRateDetails: " + brokerageInterestRateDetails);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(brokerage interest rate details)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(brokerageInterestRateDetails);
    }

    /**
     * @deprecated Use {@link #save(BrokerageInterestRateDetails) instead}
     */
    public Long saveBrokerageInterestRateDetails(BrokerageInterestRateDetails brokerageInterestRateDetails) {
        return save(brokerageInterestRateDetails);
    }

    /**
     * Deletes an brokerage interest rate details, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage interest rate details
     */
    public void deleteBrokerageInterestRateDetails(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBrokerageInterestRateDetails: " + id);
        }
        deleteEntity(BrokerageInterestRateDetails.class, id);
    }

    /**
     * Retrieves all 'BrokerageInterestRateDetails' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BrokerageInterestRateDetails> result = getEntities(BrokerageInterestRateDetails.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BrokerageInterestRateDetails' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageInterestRateDetails> result = getEntities(BrokerageInterestRateDetails.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageInterestRateDetails' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageInterestRateDetails> result = getEntities(BrokerageInterestRateDetails.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageInterestRateDetails' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageInterestRateDetails> result = getEntities(BrokerageInterestRateDetails.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageInterestRateDetails' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageInterestRateDetails> findBrokerageInterestRateDetailsList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BrokerageInterestRateDetails> result = findEntities(BrokerageInterestRateDetails.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageInterestRateDetails' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BrokerageInterestRateDetails> findBrokerageInterestRateDetailsList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BrokerageInterestRateDetails> result = findEntities(BrokerageInterestRateDetails.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BrokerageInterestRateDetails' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageInterestRateDetails> findBrokerageInterestRateDetailsList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageInterestRateDetails> result = findEntities(BrokerageInterestRateDetails.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageInterestRateDetails' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageInterestRateDetails> findBrokerageInterestRateDetailsList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageInterestRateDetails> result = findEntities(BrokerageInterestRateDetails.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageInterestRateDetails' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageInterestRateDetails> findBrokerageInterestRateDetailsList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageInterestRateDetails> result = findEntities(BrokerageInterestRateDetails.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageInterestRateDetails' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageInterestRateDetails> findBrokerageInterestRateDetailsList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageInterestRateDetails> result = findEntities(BrokerageInterestRateDetails.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageInterestRateDetails' instances.
     */
    public long getBrokerageInterestRateDetailsCount() {
        long result = getEntityCount(BrokerageInterestRateDetails.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageInterestRateDetails' instances which match the given whereClause.
     */
    public long getBrokerageInterestRateDetailsCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BrokerageInterestRateDetails.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageInterestRateDetails' instances which match the given whereClause together with params specified in object array.
     */
    public long getBrokerageInterestRateDetailsCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BrokerageInterestRateDetails.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage interest rate details)}
    @Override
    public BrokerageInterestRateDetails findCurrentlyActiveBrokerageInterestRateDetails(){
    	BrokerageInterestRateDetails retbird = null;
    	
    	Query query = getPersistenceManager().newNamedQuery(BrokerageInterestRateDetails.class, "findCurrentlyActiveBrokerageInterestRateDetails");
    	Map<Integer, Object> params = new HashMap<Integer, Object>();
    	
    	params.put(1, new Timestamp( CurrentDate.getDate().getTime()));
    	
    	@SuppressWarnings("unchecked")
        Collection<BrokerageInterestRateDetails> bird = (Collection<BrokerageInterestRateDetails>) query.executeWithMap(params);
    	
    	if(bird.size() == 1){
            retbird = bird.iterator().next();
        }
    	

    	return retbird;
    }

    @Override
    public BrokerageInterestRateDetails findCurrentlyActiveInterestRateDetails(Date currentDate){
        Query query = getPersistenceManager().newNamedQuery(BrokerageInterestRateDetails.class, "findCurrentlyActiveBrokerageInterestRateDetails");
        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, new java.sql.Timestamp(currentDate.getTime()));
        List<BrokerageInterestRateDetails> results = (List<BrokerageInterestRateDetails>)query.executeWithMap(parameters);

        BrokerageInterestRateDetails finalResult = null;

        if(results.size() == 1){
            finalResult = results.iterator().next();
        }

        return finalResult;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
