//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage interest rate prediction data)}
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Synchronization;

import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageInterestRatePrediction;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.brokerage.domain.BrokerageInterestRatePredictionData;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageInterestRatePredictionDatas'.
 *
 * @author generator
 */
public interface BrokerageInterestRatePredictionDataDao extends BaseDao<BrokerageInterestRatePredictionData> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage interest rate prediction data identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageInterestRatePredictionData getBrokerageInterestRatePredictionData(Long id);

    /**
     * Saves the brokerage interest rate prediction data.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageInterestRatePredictionData(BrokerageInterestRatePredictionData brokerageInterestRatePredictionData);

    /**
     * Deletes an brokerage interest rate prediction data, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage interest rate prediction data
     */
    void deleteBrokerageInterestRatePredictionData(Long id);

    /**
     * Retrieves all 'BrokerageInterestRatePredictionData' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageInterestRatePredictionData> getBrokerageInterestRatePredictionDataList();

    /**
     * Retrieves a page of 'BrokerageInterestRatePredictionData' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageInterestRatePredictionData> getBrokerageInterestRatePredictionDataList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageInterestRatePredictionData' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageInterestRatePredictionData> getBrokerageInterestRatePredictionDataList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageInterestRatePredictionData' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageInterestRatePredictionData> getBrokerageInterestRatePredictionDataList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageInterestRatePredictionData' instances.
     */
    long getBrokerageInterestRatePredictionDataCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage interest rate prediction data)}

    /**
     * Returns a collection of currently active {@link BrokerageInterestRatePredictionData} with given
     * {@link BrokerageInterestRatePrediction} and {@link BrokerageBank}
     * @param prediction is the given {@link BrokerageInterestRatePrediction}
     * @param brokerageBank the given {@link BrokerageBank}
     * @return the resulting {@link BrokerageInterestRatePredictionData} collection
     */
    Collection<BrokerageInterestRatePredictionData> getCurrentlyActiveByBrokerageBank(BrokerageInterestRatePrediction prediction, BrokerageBank brokerageBank);

    // !!!!!!!! End of insert code section !!!!!!!!
}
