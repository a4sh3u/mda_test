package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBankFilter;
import de.smava.webapp.brokerage.domain.FilterReasonCode;


/**
 * The domain object that represents 'BrokerageBankFilters'.
 *
 * @author generator
 */
public interface BrokerageBankFilterEntityInterface {

    /**
     * Setter for the property 'condition'.
     *
     * 
     *
     */
    void setCondition(String condition);

    /**
     * Returns the property 'condition'.
     *
     * 
     *
     */
    String getCondition();
    /**
     * Setter for the property 'filter reason'.
     *
     * 
     *
     */
    void setFilterReason(FilterReasonCode filterReason);

    /**
     * Returns the property 'filter reason'.
     *
     * 
     *
     */
    FilterReasonCode getFilterReason();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBankFilter asBrokerageBankFilter();
}
