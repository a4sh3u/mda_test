//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage car finance)}
import de.smava.webapp.brokerage.domain.history.BrokerageCarFinanceHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageCarFinances'.
 *
 * @author generator
 */
public class BrokerageCarFinance extends BrokerageCarFinanceHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage car finance)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected Date _creationDate;
        protected de.smava.webapp.brokerage.domain.VehicleType _vehicleType;
        protected String _vehicleBrand;
        protected String _vehicleModel;
        protected Integer _vehiclePowerKw;
        protected Date _vehicleRegistrationYear;
        protected Integer _vehicleKm;
        protected Double _vehiclePrice;
        protected Double _initialPayment;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'vehicle type'.
     */
    public void setVehicleType(de.smava.webapp.brokerage.domain.VehicleType vehicleType) {
        _vehicleType = vehicleType;
    }
            
    /**
     * Returns the property 'vehicle type'.
     */
    public de.smava.webapp.brokerage.domain.VehicleType getVehicleType() {
        return _vehicleType;
    }
                                    /**
     * Setter for the property 'vehicle brand'.
     */
    public void setVehicleBrand(String vehicleBrand) {
        if (!_vehicleBrandIsSet) {
            _vehicleBrandIsSet = true;
            _vehicleBrandInitVal = getVehicleBrand();
        }
        registerChange("vehicle brand", _vehicleBrandInitVal, vehicleBrand);
        _vehicleBrand = vehicleBrand;
    }
                        
    /**
     * Returns the property 'vehicle brand'.
     */
    public String getVehicleBrand() {
        return _vehicleBrand;
    }
                                    /**
     * Setter for the property 'vehicle model'.
     */
    public void setVehicleModel(String vehicleModel) {
        if (!_vehicleModelIsSet) {
            _vehicleModelIsSet = true;
            _vehicleModelInitVal = getVehicleModel();
        }
        registerChange("vehicle model", _vehicleModelInitVal, vehicleModel);
        _vehicleModel = vehicleModel;
    }
                        
    /**
     * Returns the property 'vehicle model'.
     */
    public String getVehicleModel() {
        return _vehicleModel;
    }
                                            
    /**
     * Setter for the property 'vehicle power kw'.
     */
    public void setVehiclePowerKw(Integer vehiclePowerKw) {
        _vehiclePowerKw = vehiclePowerKw;
    }
            
    /**
     * Returns the property 'vehicle power kw'.
     */
    public Integer getVehiclePowerKw() {
        return _vehiclePowerKw;
    }
                                    /**
     * Setter for the property 'vehicle registration year'.
     */
    public void setVehicleRegistrationYear(Date vehicleRegistrationYear) {
        if (!_vehicleRegistrationYearIsSet) {
            _vehicleRegistrationYearIsSet = true;
            _vehicleRegistrationYearInitVal = getVehicleRegistrationYear();
        }
        registerChange("vehicle registration year", _vehicleRegistrationYearInitVal, vehicleRegistrationYear);
        _vehicleRegistrationYear = vehicleRegistrationYear;
    }
                        
    /**
     * Returns the property 'vehicle registration year'.
     */
    public Date getVehicleRegistrationYear() {
        return _vehicleRegistrationYear;
    }
                                            
    /**
     * Setter for the property 'vehicle km'.
     */
    public void setVehicleKm(Integer vehicleKm) {
        _vehicleKm = vehicleKm;
    }
            
    /**
     * Returns the property 'vehicle km'.
     */
    public Integer getVehicleKm() {
        return _vehicleKm;
    }
                                            
    /**
     * Setter for the property 'vehicle price'.
     */
    public void setVehiclePrice(Double vehiclePrice) {
        _vehiclePrice = vehiclePrice;
    }
            
    /**
     * Returns the property 'vehicle price'.
     */
    public Double getVehiclePrice() {
        return _vehiclePrice;
    }
                                            
    /**
     * Setter for the property 'initial payment'.
     */
    public void setInitialPayment(Double initialPayment) {
        _initialPayment = initialPayment;
    }
            
    /**
     * Returns the property 'initial payment'.
     */
    public Double getInitialPayment() {
        return _initialPayment;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageCarFinance.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _vehicleBrand=").append(_vehicleBrand);
            builder.append("\n    _vehicleModel=").append(_vehicleModel);
            builder.append("\n}");
        } else {
            builder.append(BrokerageCarFinance.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageCarFinance asBrokerageCarFinance() {
        return this;
    }
}
