package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageApplicationGuarantee;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationGuarantees'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationGuaranteeHistory extends AbstractBrokerageApplicationGuarantee {

    protected transient Date _topChanceInitVal;
    protected transient boolean _topChanceIsSet;
    protected transient Date _guaranteeInitVal;
    protected transient boolean _guaranteeIsSet;
    protected transient String _reasonInitVal;
    protected transient boolean _reasonIsSet;


			
    /**
     * Returns the initial value of the property 'top chance'.
     */
    public Date topChanceInitVal() {
        Date result;
        if (_topChanceIsSet) {
            result = _topChanceInitVal;
        } else {
            result = getTopChance();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'top chance'.
     */
    public boolean topChanceIsDirty() {
        return !valuesAreEqual(topChanceInitVal(), getTopChance());
    }

    /**
     * Returns true if the setter method was called for the property 'top chance'.
     */
    public boolean topChanceIsSet() {
        return _topChanceIsSet;
    }
	
    /**
     * Returns the initial value of the property 'guarantee'.
     */
    public Date guaranteeInitVal() {
        Date result;
        if (_guaranteeIsSet) {
            result = _guaranteeInitVal;
        } else {
            result = getGuarantee();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'guarantee'.
     */
    public boolean guaranteeIsDirty() {
        return !valuesAreEqual(guaranteeInitVal(), getGuarantee());
    }

    /**
     * Returns true if the setter method was called for the property 'guarantee'.
     */
    public boolean guaranteeIsSet() {
        return _guaranteeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'reason'.
     */
    public String reasonInitVal() {
        String result;
        if (_reasonIsSet) {
            result = _reasonInitVal;
        } else {
            result = getReason();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'reason'.
     */
    public boolean reasonIsDirty() {
        return !valuesAreEqual(reasonInitVal(), getReason());
    }

    /**
     * Returns true if the setter method was called for the property 'reason'.
     */
    public boolean reasonIsSet() {
        return _reasonIsSet;
    }

}
