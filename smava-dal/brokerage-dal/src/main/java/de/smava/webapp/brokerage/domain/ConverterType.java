/**
 * 
 */
package de.smava.webapp.brokerage.domain;

/**
 * @author bvoss
 *
 */
public enum ConverterType {
	
	MAP, EL, BEAN;

}
