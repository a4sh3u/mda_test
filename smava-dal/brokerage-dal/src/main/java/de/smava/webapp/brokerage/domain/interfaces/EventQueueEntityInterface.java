package de.smava.webapp.brokerage.domain.interfaces;

import java.util.Date;

/**
 * The domain object that represents 'EventQueues'.
 */
public interface EventQueueEntityInterface {

    Long getId();

    void setId(Long id);

    /**
     * Setter for the property 'creation date'.
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     */
    Date getCreationDate();

    /**
     * Setter for the property 'account'.
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'loan application'.
     *
     */
    void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     */
    de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication();

    /**
     * Setter for the property 'type'.
     *
     */
    void setType(de.smava.webapp.brokerage.domain.EventQueueType type);

    /**
     * Returns the property 'type'.
     *
     */
    de.smava.webapp.brokerage.domain.EventQueueType getType();

    /**
     * Setter for the property 'state'.
     *
     */
    void setState(de.smava.webapp.brokerage.domain.EventQueueState state);

    /**
     * Returns the property 'state'.
     *
     */
    de.smava.webapp.brokerage.domain.EventQueueState getState();

    /**
     * Setter for the property 'due date'.
     *
     */
    void setDueDate(Date dueDate);

    /**
     * Returns the property 'due date'.
     *
     */

    Date getDueDate();
    /**
     * Setter for the property 'expiration date'.
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'bank'.
     *
     */
    void setBank(de.smava.webapp.brokerage.domain.BrokerageBank bank);

    /**
     * Returns the property 'bank'.
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBank();

}
