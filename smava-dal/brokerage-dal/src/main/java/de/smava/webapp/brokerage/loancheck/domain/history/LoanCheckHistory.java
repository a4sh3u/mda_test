package de.smava.webapp.brokerage.loancheck.domain.history;



import de.smava.webapp.brokerage.loancheck.domain.abstracts.AbstractLoanCheck;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'LoanChecks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class LoanCheckHistory extends AbstractLoanCheck {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _completionDateInitVal;
    protected transient boolean _completionDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'completion date'.
     */
    public Date completionDateInitVal() {
        Date result;
        if (_completionDateIsSet) {
            result = _completionDateInitVal;
        } else {
            result = getCompletionDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'completion date'.
     */
    public boolean completionDateIsDirty() {
        return !valuesAreEqual(completionDateInitVal(), getCompletionDate());
    }

    /**
     * Returns true if the setter method was called for the property 'completion date'.
     */
    public boolean completionDateIsSet() {
        return _completionDateIsSet;
    }
	
}
