package de.smava.webapp.brokerage.domain;

public enum VehicleType {
	CAR,
	BIKE,
	CARAVAN,
	MOTOCARAVAN;
}
