package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author KF
 * @since 20.03.2017
 */
public class AuxmoneyAdditionalData extends AbstractAdditionalData {
    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_AUXMONEY;
    }
}
