/**
 * 
 */
package de.smava.webapp.brokerage.domain;

/**
 * @author bvoss
 *
 */
public enum MappingDirection {
	
	IN, OUT;

}
