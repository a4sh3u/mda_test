package de.smava.webapp.brokerage.dao.jdo;

import de.smava.webapp.account.dao.ConfigValueDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.ConfigValue;
import de.smava.webapp.brokerage.dao.EventQueueDao;
import de.smava.webapp.brokerage.domain.EventQueue;
import de.smava.webapp.brokerage.domain.EventQueueState;
import de.smava.webapp.brokerage.domain.EventQueueType;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.datanucleus.store.rdbms.query.ForwardQueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'EventQueues'.
 */
@Repository(value = "eventQueueDao")
public class JdoEventQueueDao extends JdoBaseDao implements EventQueueDao {

    private static final Logger LOGGER = Logger.getLogger(JdoEventQueueDao.class);

    private static final String CLASS_NAME = "EventQueue";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    private static final String TIME_FACTOR_TIMEGAP6_KEY = "casi.leadAssignment.timeFactor.timegap6";
    private static final String RELEVANCE_FACTOR_QUALIFIED_LEAD_KEY = "casi.leadAssignment.relevanceFactor.qualifiedLead";
    private static final String RELEVANCE_FACTOR_LEAD_NO_OFFER_KEY = "casi.leadAssignment.relevanceFactor.leadNoOffer";
    private static final String RELEVANCE_FACTOR_LEAD_KDF_KEY = "casi.leadAssignment.relevanceFactor.leadKdf";
    private static final String RELEVANCE_FACTOR_SHORT_LEAD_KEY = "casi.leadAssignment.relevanceFactor.shortLead";
    private static final String RELEVANCE_FACTOR_LEAD_SCHUFA_NEGATIVE_KEY = "casi.leadAssignment.relevanceFactor.leadSchufaNegative";
    private static final String RELEVANCE_FACTOR_SPECIAL_FACTOR_KEY = "casi.leadAssignment.relevanceFactor.specialFactor";
    private static final String TIME_FACTOR_TIMEGAP0_KEY = "casi.leadAssignment.timeFactor.timegap0";
    private static final String TIME_FACTOR_TIMEVALUE0_KEY = "casi.leadAssignment.timeFactor.timevalue0";
    private static final String TIME_FACTOR_TIMEVALUE_REMAINING_KEY = "casi.leadAssignment.timeFactor.timevalueRemaining";

    private static final long LIMIT = 500;

    @Autowired
    private ConfigValueDao configValueDao;

    private final Sortable mostCurrent = new Sortable() {
        @Override
        public String getSort() {
            return "_dueDate";
        }

        @Override
        public boolean hasSort() {
            return true;
        }

        @Override
        public boolean sortDescending() {
            return true;
        }
    };

    /**
     * Returns an attached copy of the event queue identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public EventQueue load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        EventQueue result = getEntity(EventQueue.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	EventQueue entity = findUniqueEntity(EventQueue.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the event queue.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persistence manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(EventQueue eventQueue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveEventQueue: " + eventQueue);
        }
        return saveEntity(eventQueue);
    }

    /**
     * Gets the states term for finding types
     */
    private OqlTerm getTypesTerm(OqlTerm oqlTerm,
            Collection<EventQueueType> types) {
        OqlTerm result;
        if (types == null || types.size() == 0) {
            result = oqlTerm.notNull("_type");
        } else if (types.size() > 1) {
            OqlTerm[] orStates = new OqlTerm[types.size()];
            int i = 0;
            for (EventQueueType state : types) {
                orStates[i] = oqlTerm.equals("_type", state);
                i++;
            }
            result = oqlTerm.or(orStates);
        } else { // size should be 1 here
            result = oqlTerm.equals("_type", types.iterator().next());
        }
        return result;
    }

    /**
     * Gets the states term for finding types
     */
    private OqlTerm getStatesTerm(OqlTerm oqlTerm,
            Collection<EventQueueState> states) {
        OqlTerm result;
        if (states.size() > 1) {
            OqlTerm[] orStates = new OqlTerm[states.size()];
            int i = 0;
            for (EventQueueState state : states) {
                orStates[i] = oqlTerm.equals("_state", state);
                i++;
            }
            result = oqlTerm.or(orStates);
        } else if (states.size() == 1) {
            result = oqlTerm.equals("_state", states.iterator().next());
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }
        return result;
    }

    @Override
    public Collection<EventQueue> findCurrentEventQueueList(
            Collection<EventQueueType> types, Collection<EventQueueState> states) {
        Date now = CurrentDate.getDate();
        Collection<Object> params = new ArrayList<Object>();
        params.add(now);
        params.add(now);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
        oqlTerm.and(oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types));
            if (types!=null) {
                params.addAll(types);
            }
        } else {
            oqlTerm.and(oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states));

            if (types!=null) {
                params.addAll(types);
            }
            params.addAll(states);
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), mostCurrent, params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;

    }

    @Override
    public long getNumberOfCurrentEventQueueList(
            Collection<EventQueueType> types, Collection<EventQueueState> states) {
        return getNumberOfCurrentEventQueueList(types, states, null, false);
    }

    @Override
    public long getNumberOfCurrentEventQueueList(
            Collection<EventQueueType> types, Collection<EventQueueState> states, Long advisorId, boolean assigned) {

        Date now = CurrentDate.getDate();
        Collection<Object> params = new ArrayList<Object>();
        params.add(now);
        params.add(now);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
            oqlTerm.and(oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types),
                    getAdvisorTerm(oqlTerm, advisorId, assigned));
            if (types != null) {
                params.addAll(types);
            }
        } else {
            oqlTerm.and(oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states),
                    getAdvisorTerm(oqlTerm, advisorId, assigned));

            if (types != null) {
                params.addAll(types);
            }
            params.addAll(states);
        }

        return getEntityCount(EventQueue.class, oqlTerm.toString(),
                params.toArray());
    }

    @Override
    public long getNumberOfEventQueueList(Collection<EventQueueState> states, Long advisorId, boolean assigned) {
        Collection<Object> params = new ArrayList<Object>();

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
            oqlTerm.and(getAdvisorTerm(oqlTerm, advisorId, assigned));
        } else {
            oqlTerm.and(
                    getStatesTerm(oqlTerm, states),
                    getAdvisorTerm(oqlTerm, advisorId, assigned));
            params.addAll(states);
        }

        return getEntityCount(EventQueue.class, oqlTerm.toString(),
                params.toArray());
    }

    public Collection<EventQueue> findCurrentEventQueueList( Account account, Collection<EventQueueType> types, Collection<EventQueueState> states){
        Date now = CurrentDate.getDate();

        Collection<Object> params = new ArrayList<Object>();
        params.add(account);
        params.add(now);
        params.add(now);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
            oqlTerm.and(oqlTerm.equals("_account", account),
                    oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types));
            if (types != null) {
                params.addAll(types);
            }
        } else {
            oqlTerm.and(oqlTerm.equals("_account", account),
                    oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states));
            if (types != null) {
                params.addAll(types);
            }

            params.addAll(states);
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), mostCurrent, params.toArray());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<EventQueue> findBorrowerEventQueues(Account account, Boolean current, Integer offset, Integer limit,
                                                          final String sortBy, final Boolean descending) {

        OqlTerm oqlTerm = OqlTerm.newTerm();
        Collection<Object> params = new ArrayList<Object>();
        params.add(account);

        if (current){
            Date now = CurrentDate.getDate();
            params.add(now);
            params.add(now);

            oqlTerm.and(oqlTerm.notEquals("_state", "VOID"),
                    oqlTerm.equals("_account", account),
                    oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now));
        } else {
            oqlTerm.and(oqlTerm.notEquals("_state", "VOID"),
                    oqlTerm.equals("_account", account));
        }

        Sortable sortable = mostCurrent;
        if (sortBy != null) {
            sortable = new Sortable() {
                @Override
                public String getSort() {
                    return sortBy;
                }

                @Override
                public boolean hasSort() {
                    return true;
                }

                @Override
                public boolean sortDescending() {
                    return BooleanUtils.isTrue(descending);
                }
            };
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), sortable, params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;

    }

    @Override
    public EventQueue findLastBorrowerEventQueue(Account account, Boolean current) {
        Collection<EventQueue> list = findBorrowerEventQueues(account, current, 0, 1, null, null);
        if (list!=null && list.size()!=0){
            return list.iterator().next();
        }
        return null;
    }

    @Override
    public Collection<EventQueue> findAllEventQueueList( LoanApplication la, Collection<EventQueueType> types, Collection<EventQueueState> states){

        Collection<Object> params = new ArrayList<Object>();
        params.add(la);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
            oqlTerm.and(oqlTerm.equals("_loanApplication", la),
                    getTypesTerm(oqlTerm, types));
            if (types != null) {
                params.addAll(types);
            }
        } else {
            oqlTerm.and(oqlTerm.equals("_loanApplication", la),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states));
            if (types != null) {
                params.addAll(types);
            }

            params.addAll(states);
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }
    
    @Override
    public Collection<EventQueue> findAllEventQueueList( Account account, Collection<EventQueueType> types, Collection<EventQueueState> states) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(account);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
            oqlTerm.and(oqlTerm.equals("_account", account),
                    getTypesTerm(oqlTerm, types));
            if (types != null) {
                params.addAll(types);
            }
        } else {
            oqlTerm.and(oqlTerm.equals("_account", account),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states));
            if (types != null) {
                params.addAll(types);
            }
            params.addAll(states);
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }


    @Override
    public Collection<EventQueue> findAllEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset,
                                                        Integer limit, final String sortBy, final Boolean descending, boolean assigned){
        Collection<Object> params = new ArrayList<Object>();

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
            oqlTerm.and(getAdvisorTerm(oqlTerm, personalAdvisor, assigned),
                    getTypesTerm(oqlTerm, types));
            if (types!=null) {
                params.addAll(types);
            }
        }
        else {
            oqlTerm.and(getAdvisorTerm(oqlTerm, personalAdvisor, assigned),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states));
            if (types!=null) {
                params.addAll(types);
            }
            params.addAll(states);
        }

        if (offset != null && limit != null) {
            oqlTerm.setRange(offset, offset + limit);
        }

        Sortable sortable = mostCurrent;
        if (sortBy != null) {
            sortable = new Sortable() {
                @Override
                public String getSort() {
                    return sortBy;
                }

                @Override
                public boolean hasSort() {
                    return true;
                }

                @Override
                public boolean sortDescending() {
                    return BooleanUtils.isTrue(descending);
                }
            };
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), sortable, params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    private OqlTerm getAdvisorTerm(OqlTerm oqlTerm, Long advisorId, boolean assigned) {

        OqlTerm orTerm = OqlTerm.newTerm();
        if (advisorId != null) {
            return oqlTerm.equals("_account._personalAdvisorId", advisorId);
        } else {
            if (assigned) {
                return orTerm.notNull("_account._personalAdvisorId");
            } else {
                return orTerm.isNull("_account._personalAdvisorId");
            }
        }
    }

    /**
     * Restrictive method to search for credit adviser omitting null values in final select statement
     *
     * @param adviserIds
     * @return
     */
    private OqlTerm getAdvisorTermStrict(Collection<Long> adviserIds, OqlTerm oqlTerm) {


        OqlTerm result;
        if (adviserIds.size() > 1) {
            OqlTerm[] orTerm = new OqlTerm[adviserIds.size()];
            int i = 0;
            for (Long adv : adviserIds) {
                if (adv < 0) {
                    orTerm[i] = oqlTerm.isNull("_account._personalAdvisorId");
                } else{
                    orTerm[i] = oqlTerm.equals("_account._personalAdvisorId", adv);
                }
                i++;
            }
            result = oqlTerm.or(orTerm);
        } else if (adviserIds.size() == 1) {
            Long adv =  adviserIds.iterator().next();
            if (adv < 0) {
                result = oqlTerm.isNull("_account._personalAdvisorId");
            } else {
                result = oqlTerm.equals("_account._personalAdvisorId", adv);
            }
        } else {
            throw new IllegalArgumentException("adviserIds must not be empty");
        }
        return result;

    }

    public Collection<EventQueue> findCurrentEventQueueList(
            Collection<EventQueueType> types,
            Collection<EventQueueState> states,
            Long personalAdvisor) {
        return findCurrentEventQueueList(types, states, personalAdvisor, null, null, null, null, false);
    }

    @Override
    public Collection<EventQueue> findCurrentEventQueueList(
            Collection<EventQueueType> types,
            Collection<EventQueueState> states,
            Long personalAdvisor,
            Integer offset,
            Integer limit,
            final String sortBy,
            final Boolean descending) {
        return findCurrentEventQueueList(types, states, personalAdvisor, offset, limit, sortBy, descending, false);
    }

    public Collection<EventQueue> findCurrentEventQueueList(
            Collection<EventQueueType> types,
            Collection<EventQueueState> states,
            Long personalAdvisor,
            Integer offset,
            Integer limit,
            final String sortBy,
            final Boolean descending, boolean assigned) {

        Date now = CurrentDate.getDate();

        Collection<Object> params = new ArrayList<Object>();
        params.add(now);
        params.add(now);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        if (states.size() == 0) {
        oqlTerm.and(getAdvisorTerm(oqlTerm, personalAdvisor, assigned),
                oqlTerm.lessThan("_dueDate", now),
                oqlTerm.greaterThanEquals("_expirationDate", now),
                getTypesTerm(oqlTerm, types));
            if (types != null) {
                params.addAll(types);
            }
        }
        else {
            oqlTerm.and(getAdvisorTerm(oqlTerm, personalAdvisor, assigned),
                    oqlTerm.lessThan("_dueDate", now),
                    oqlTerm.greaterThanEquals("_expirationDate", now),
                    getTypesTerm(oqlTerm, types),
                    getStatesTerm(oqlTerm, states));
            if (types != null) {
                params.addAll(types);
            }
            params.addAll(states);
        }

        if (offset != null && limit != null) {
            oqlTerm.setRange(offset, offset + limit);
        }

        Sortable sortable = mostCurrent;
        if (sortBy != null) {
            sortable = new Sortable() {
                @Override
                public String getSort() {
                    return sortBy;
                }

                @Override
                public boolean hasSort() {
                    return true;
                }

                @Override
                public boolean sortDescending() {
                    return BooleanUtils.isTrue(descending);
                }
            };
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), sortable, params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Find events with provided types and states using restrictive adviser search and
     * limit of 100 most recent records.
     *
     * @param types
     * @param states
     * @param banks
     * @return
     */
    @Override
    public Collection<EventQueue> findLastEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states, Collection<Long> advisers, List<String> banks) {
        Date now = CurrentDate.getDate();

        Collection<Object> params = new ArrayList<Object>();
        params.add(now);
        params.add(now);

        OqlTerm oqlTerm = OqlTerm.newTerm();

        oqlTerm.setSortBy("_dueDate");
        oqlTerm.setRange(0, LIMIT);
        if (states.size() == 0) {
            if (banks != null && banks.size() > 0) {
                oqlTerm.and(getAdvisorTermStrict(advisers, oqlTerm),
                        oqlTerm.lessThan("_dueDate", now),
                        oqlTerm.greaterThanEquals("_expirationDate", now),
                        getTypesTerm(oqlTerm, types),
                        getBanksTerm(oqlTerm, banks));
                if (types != null) {
                    params.addAll(types);
                }
            } else {
                oqlTerm.and(getAdvisorTermStrict(advisers, oqlTerm),
                        oqlTerm.lessThan("_dueDate", now),
                        oqlTerm.greaterThanEquals("_expirationDate", now),
                        getTypesTerm(oqlTerm, types));
                if (types != null) {
                    params.addAll(types);
                }
            }
        } else {
            if (banks != null && banks.size() > 0) {
                oqlTerm.and(getAdvisorTermStrict(advisers, oqlTerm),
                        oqlTerm.lessThan("_dueDate", now),
                        oqlTerm.greaterThanEquals("_expirationDate", now),
                        getTypesTerm(oqlTerm, types),
                        getStatesTerm(oqlTerm, states),
                        getBanksTerm(oqlTerm, banks));
                if (types != null) {
                    params.addAll(types);
                }
                params.addAll(states);
            } else {
                oqlTerm.and(getAdvisorTermStrict(advisers, oqlTerm),
                        oqlTerm.lessThan("_dueDate", now),
                        oqlTerm.greaterThanEquals("_expirationDate", now),
                        getTypesTerm(oqlTerm, types),
                        getStatesTerm(oqlTerm, states));
                if (types != null) {
                    params.addAll(types);
                }
                params.addAll(states);
            }
        }

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    private OqlTerm getBanksTerm(OqlTerm oqlTerm, List<String> banks) {
        OqlTerm result;
        if (banks.size() > 1) {
            OqlTerm[] orStates = new OqlTerm[banks.size()];
            int i = 0;
            for (String bank : banks) {
                orStates[i] = oqlTerm.equals("_bank._name", bank);
                i++;
            }
            result = oqlTerm.or(orStates);
        } else if (banks.size() == 1) {
            result = oqlTerm.equals("_bank._name", banks.iterator().next());
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }
        return result;
    }

    @Override
    public EventQueue findLastStateIndependentBorrowerEventQueues(Account account) {

        OqlTerm oqlTerm = OqlTerm.newTerm();
        Collection<Object> params = new ArrayList<Object>();
        params.add(account);

        Date now = CurrentDate.getDate();
        params.add(now);

        oqlTerm.and(oqlTerm.equals("_account", account),
                oqlTerm.lessThan("_creationDate", now));

        Sortable sortable = mostCurrent;

        Collection<EventQueue> result = findEntities(EventQueue.class,
                oqlTerm.toString(), sortable, params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findLastStateIndependentBorrowerEventQueues() - result: size=" + result.size());
        }

        EventQueue singleResult = null;
        if (result != null && result.size() > 0) {
            singleResult = result.iterator().next();
        }

        return singleResult;
    }

    @Override
    @SuppressWarnings("unchecked")
    public EventQueue findLatestEventQueueForLoanApplication(LoanApplication la) {
        String sql = "select id as id from event_queue where loan_application_id=" +
                la.getId() +
                " order by creation_date DESC limit 1";

        Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setClass(EventQueue.class);
        ForwardQueryResult execute = (ForwardQueryResult) query.execute();
        return execute.isEmpty() ? null : (EventQueue) execute.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    @Deprecated
    public Collection<EventQueue> loadEventQueueByIds(Collection<Long> eventQueueIds) {
        Query query = getPersistenceManager().newQuery(EventQueue.class);
        // WARNING this filter could possible throw StackOverflow exception for large collections like size > 1000
        query.setFilter(":eventQueueIds.contains(this._id)");
        Collection<EventQueue> eventQueues = (Collection<EventQueue>) query.execute(eventQueueIds);
        return sortByIdsOrder(eventQueues, eventQueueIds);
    }

    private Collection<EventQueue> sortByIdsOrder(Collection<EventQueue> eventQueues, Collection<Long> eventQueueIds) {
        List<EventQueue> sorterEventQueues = new ArrayList<EventQueue>(eventQueues.size());
        for (Long id : eventQueueIds) {
            sorterEventQueues.add(getEventQueueById(eventQueues, id));
        }
        return sorterEventQueues;
    }

    private EventQueue getEventQueueById(Collection<EventQueue> eventQueues, Long id) {
        for (EventQueue eventQueue : eventQueues) {
            if (eventQueue.getId().equals(id)) {
                return eventQueue;
            }
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Long> findCurrentEventQueueIds(Collection<EventQueueType> types, Collection<EventQueueState> states,
                                                     Long personalAdvisor, Integer offset, Integer limit, String sortBy,
                                                     Boolean descending, String alternativeSortBy, Boolean alternativeDescending,
                                                     boolean assigned, CustomerProcessState customerProcessState) {
        Map<String, String> configFactors = prepareConfigFactors();
        Date now = CurrentDate.getDate();

        StringBuffer sql = generateBaseQuery(types, states, personalAdvisor, assigned, customerProcessState, configFactors, now);
        appendOrderingAndPagination(sql, offset, limit, sortBy, descending, alternativeSortBy, alternativeDescending);

        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        return  (Collection<Long>) query.execute();
    }

    public Collection<EventQueue> findCurrentEventQueueList(Collection<EventQueueType> types,
                                                            Collection<EventQueueState> states, Long personalAdvisor,
                                                            Integer offset, Integer limit, final String sortBy,
                                                            final Boolean descending, final String alternativeSortBy,
                                                            final Boolean alternativeDescending, boolean assigned,
                                                            CustomerProcessState customerProcessState) {

        Collection<Long> eventQueueIds = findCurrentEventQueueIds(types, states, personalAdvisor, offset, limit, sortBy,
                descending, alternativeSortBy, alternativeDescending, assigned, customerProcessState);

        return loadEventQueueByIds(eventQueueIds);
    }

    @SuppressWarnings("unchecked")
    public Long countCurrentEventQueueList(Collection<EventQueueType> types, Collection<EventQueueState> states,
                                           Long personalAdvisor, Integer offset, Integer limit, final String sortBy,
                                           final Boolean descending, final String alternativeSortBy,
                                           final Boolean alternativeDescending, boolean assigned,
                                           CustomerProcessState customerProcessState) {

        Map<String, String> configFactors = prepareConfigFactors();
        Date now = CurrentDate.getDate();

        StringBuffer sql = generateBaseQuery(types, states, personalAdvisor, assigned, customerProcessState, configFactors, now);
        String countQuery = wrapQueryWithSelectCount(sql);
        Query query = getPersistenceManager().newQuery(Query.SQL, countQuery);
        query.setUnique(true);

        return (Long) query.execute();
    }

    private String wrapQueryWithSelectCount(StringBuffer sql) {
        return "select count(*) from (" + sql.toString() + ") AS casieq where customer_value_with_factors > 0) sel";
    }

    private Map<String, String> prepareConfigFactors() {
        List<String> keysToFetch = new ArrayList<String>();
        keysToFetch.add(RELEVANCE_FACTOR_QUALIFIED_LEAD_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_LEAD_NO_OFFER_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_LEAD_KDF_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_SHORT_LEAD_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_LEAD_SCHUFA_NEGATIVE_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_SPECIAL_FACTOR_KEY);

        keysToFetch.add(TIME_FACTOR_TIMEGAP0_KEY);
        keysToFetch.add(TIME_FACTOR_TIMEGAP6_KEY);
        keysToFetch.add(TIME_FACTOR_TIMEVALUE0_KEY);
        keysToFetch.add(TIME_FACTOR_TIMEVALUE_REMAINING_KEY);

        Map<String, String> configFactors = new HashMap<String, String>();
        for (String key : keysToFetch) {
            ConfigValue configValue = configValueDao.getConfigValueByKey(key);
            if (configValue == null) {
                throw new IllegalStateException("Can't fetch config value for key " + key);
            }
            configFactors.put(key, configValue.getValue());
        }

        return configFactors;
    }

    private StringBuffer generateBaseQuery(Collection<EventQueueType> types, Collection<EventQueueState> states,
                                           Long personalAdvisorId, boolean assigned,
                                           CustomerProcessState customerProcessState, Map<String, String> configFactors,
                                           Date now) {

        StringBuffer sql = new StringBuffer();
        sql.append("select id from (");

        sql.append("select ceq.id as id, ");

        sql.append("calculate_customer_value_timefactor(");
        sql.append("calculate_customer_value_relevancefactor_verivox(");
        sql.append("old_la.customer_value, ");
        sql.append("new_la.id, ");
        sql.append(configFactors.get(RELEVANCE_FACTOR_QUALIFIED_LEAD_KEY)).append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_LEAD_NO_OFFER_KEY)).append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_LEAD_KDF_KEY)) .append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_SHORT_LEAD_KEY)).append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_LEAD_SCHUFA_NEGATIVE_KEY)).append(",");
        sql.append("ceq.event_queue_is_schufa_positive").append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_SPECIAL_FACTOR_KEY)).append(",");
        sql.append("ai.external_affiliate_id");
        sql.append("), ");
        sql.append("old_la.creation_date, ");
        sql.append("TIMESTAMPTZ '");
        sql.append(new java.sql.Timestamp(now.getTime())).append("',");
        sql.append("old_la.state, ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEGAP0_KEY)).append(", ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEGAP6_KEY)).append(", ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEVALUE0_KEY)).append(", ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEVALUE_REMAINING_KEY));
        sql.append(") as customer_value_with_factors ");

        sql.append("from ");
        sql.append("casi_event_queue ceq ");
        sql.append("inner join account a ON ceq.account_id = a.id ");
        sql.append("inner join loan_application old_la on ceq.loan_application_id = old_la.id ");
        sql.append("left join brokerage_entity_map map on old_la.id = map.kredit_privat_id ");
        sql.append("left join brokerage.loan_application new_la on map.brokerage_id=new_la.id ");
        sql.append("inner join public.affiliate_information ai on ceq.sale_affiliate_info_id = ai.id ");

        sql.append("where ceq.customer_value > 0 and ");

        sql.append(getAdvisorTermSQL(personalAdvisorId, assigned));

        sql.append("and ceq.event_queue_due_date < TIMESTAMPTZ '");
        sql.append(new java.sql.Timestamp(now.getTime()));
        sql.append("' ");

        sql.append("and ceq.event_queue_expiration_date > TIMESTAMPTZ '");
        sql.append(new java.sql.Timestamp(now.getTime()));
        sql.append("' ");

        if (states != null && states.size() > 0) {
            sql.append(getStatesTermSQL(states));
        }

        if (types != null) {
            sql.append(getTypesTermSQL(types));
        }

        sql.append((customerProcessState != null) ? " AND ceq.customer_status_state='" + customerProcessState.name() + "' " : "");

        return sql;
    }

    private StringBuffer getAdvisorTermSQL(Long advisorId, boolean assigned) {
        StringBuffer sql = new StringBuffer();

        if (advisorId != null) {
            sql.append("ceq.account_id = a.id and a.personal_advisor = '");
            sql.append(advisorId);
            sql.append("' ");
        } else {
            if (assigned) {
                sql.append("ceq.account_id = a.id and a.personal_advisor is not null ");
            } else {
                sql.append("ceq.account_id = a.id and a.personal_advisor is null ");
            }
        }

        return sql;
    }

    private StringBuffer getTypesTermSQL(Collection<EventQueueType> types) {
        StringBuffer sql = new StringBuffer();

        if (types != null && types.size() != 0) {
            if (types.size() > 1) {
                sql.append("and (");
                boolean started = false;
                for (EventQueueType type : types) {
                    if (started) {
                        sql.append("or ");
                    }
                    started = true;

                    sql.append("ceq.event_queue_type = '");
                    sql.append(type);
                    sql.append("' ");
                }
                sql.append(") ");
            } else {
                sql.append("and ceq.event_queue_type = '");
                sql.append(types.iterator().next());
                sql.append("' ");
            }
        } else {
            sql.append("and ceq.event_queue_type is not null ");
        }

        return sql;
    }

    private StringBuffer getStatesTermSQL(Collection<EventQueueState> states) {
        StringBuffer sql = new StringBuffer();

        if (states != null && states.size() != 0) {
            if (states.size() > 1) {
                sql.append("and (");
                boolean started = false;
                for (EventQueueState state : states) {
                    if (started) {
                        sql.append("or ");
                    }
                    started = true;

                    sql.append("ceq.event_queue_state = ");
                    sql.append(state);
                    sql.append(" ");
                }
                sql.append(") ");
            } else {
                sql.append("and ceq.event_queue_state = '");
                sql.append(states.iterator().next());
                sql.append("' ");
            }
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }

        return sql;
    }

    private EventQueue getEntity(final Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getEntity: " + EventQueue.class + " id=" + id);
        }
        EventQueue entity = getPersistenceManager().getObjectById(EventQueue.class, id);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Loaded entity: " + entity);
        }
        return entity;
    }

    private EventQueue findUniqueEntity(String whereClause) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(whereClause));
        query.setUnique(true);
        return (EventQueue) query.execute();
    }

    private String createQueryString(String whereClause) {
        StringBuilder builder = new StringBuilder("select from ").append(EventQueue.class.getName());
        if (StringUtils.isNotBlank(whereClause)) {
            builder.append(" where ").append(whereClause);
        }
        return builder.toString();
    }

    private Long saveEntity(EventQueue entity) {
        if (entity.getId() == null) {
            if (!JDOHelper.isNew(entity)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Make entity persistent: " + entity);
                }
                getPersistenceManager().makePersistent(entity);
            }
        }
        else {
            if (JDOHelper.isDetached(entity)) {
                throw new IllegalStateException("Detaching entites is not supported!");
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Flush entity: " + entity);
            }
            if (!JDOHelper.isPersistent(entity)) {
                throw new IllegalStateException("Cannot save a non persistent object.");
            }
        }
        return entity.getId();
    }

    /**
     * Returns the number of 'clazz' entities which match the given whereClause.
     */
    private long getEntityCount(String whereClause, Object... params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(whereClause));

        query.setResult("count(this)");
        return (Long) query.executeWithArray(params);
    }

    public Collection<EventQueue> findEntities(String whereClause, Object... params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(whereClause));
        // execute query
        query.setResult("distinct this");
        return (List<EventQueue>) query.executeWithArray(params);
    }

    public Collection<EventQueue> findEntities(String whereClause, Sortable sortable, Object... params) {
        PersistenceManager pm = getPersistenceManager();
        Query query = pm.newQuery(createQueryString(whereClause));

        // evaluate Sortable information if present
        if (sortable != null && sortable.hasSort()) {
            StringBuilder sortStringBuilder = new StringBuilder(sortable.getSort());
            sortStringBuilder.append(" ");
            if (sortable.sortDescending()) {
                sortStringBuilder.append("descending");
            }
            else {
                sortStringBuilder.append("ascending");
            }
            query.setOrdering(sortStringBuilder.toString());
        }

        // execute query
        query.setResult("distinct this");
        return (List<EventQueue>) query.executeWithArray(params);
    }

    private void appendOrderingAndPagination(StringBuffer sql, Integer offset, Integer limit, String sortBy,
                                             Boolean descending, String alternativeSortBy, Boolean alternativeDescending) {
        sql.append(" order by ");
        sql.append(sortBy);
        sql.append(" ");

        if (descending) {
            sql.append("DESC ");
        } else {
            sql.append("ASC ");
        }
        sql.append("NULLS LAST ");

        sql.append(", ");
        sql.append(alternativeSortBy);
        sql.append(" ");

        if (alternativeDescending) {
            sql.append("DESC ");
        } else {
            sql.append("ASC ");
        }
        sql.append("NULLS LAST ");

        sql.append(") AS casieq where customer_value_with_factors > 0");

        if (limit != null) {
            sql.append("limit ");
            sql.append(limit.intValue());
        }
        if (offset != null) {
            sql.append(" offset ");
            sql.append(offset.intValue());
        }
    }

}
