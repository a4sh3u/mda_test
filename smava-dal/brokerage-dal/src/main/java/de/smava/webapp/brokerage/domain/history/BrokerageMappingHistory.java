package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageMapping;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageMappings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageMappingHistory extends AbstractBrokerageMapping {

    protected transient String _smavaValueInitVal;
    protected transient boolean _smavaValueIsSet;
    protected transient String _bankValueInitVal;
    protected transient boolean _bankValueIsSet;


		
    /**
     * Returns the initial value of the property 'smava value'.
     */
    public String smavaValueInitVal() {
        String result;
        if (_smavaValueIsSet) {
            result = _smavaValueInitVal;
        } else {
            result = getSmavaValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'smava value'.
     */
    public boolean smavaValueIsDirty() {
        return !valuesAreEqual(smavaValueInitVal(), getSmavaValue());
    }

    /**
     * Returns true if the setter method was called for the property 'smava value'.
     */
    public boolean smavaValueIsSet() {
        return _smavaValueIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank value'.
     */
    public String bankValueInitVal() {
        String result;
        if (_bankValueIsSet) {
            result = _bankValueInitVal;
        } else {
            result = getBankValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank value'.
     */
    public boolean bankValueIsDirty() {
        return !valuesAreEqual(bankValueInitVal(), getBankValue());
    }

    /**
     * Returns true if the setter method was called for the property 'bank value'.
     */
    public boolean bankValueIsSet() {
        return _bankValueIsSet;
    }
	
}
