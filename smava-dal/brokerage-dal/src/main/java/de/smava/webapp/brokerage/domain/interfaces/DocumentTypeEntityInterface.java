package de.smava.webapp.brokerage.domain.interfaces;


import de.smava.webapp.brokerage.domain.DocumentType;
import de.smava.webapp.brokerage.domain.DocumentTypePlaceholder;

import java.util.Set;

/**
 * The domain object that represents 'DocumentTypes'.
 *
 * @author generator
 */
public interface DocumentTypeEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType();
    /**
     * Setter for the property 'document type placeholders'.
     *
     * 
     *
     */
    public void setDocumentTypePlaceholders(Set<DocumentTypePlaceholder> documentTypePlaceholders);

    /**
     * Returns the property 'document type placeholders'.
     *
     * 
     *
     */
    public Set<DocumentTypePlaceholder> getDocumentTypePlaceholders();
    /**
     * Helper method to get reference of this object as model type.
     */
    public DocumentType asDocumentType();
}
