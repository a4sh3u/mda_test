package de.smava.webapp.brokerage.dao.additionalData;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;

@JsonFilter(CreditEuropeAdditionalData.CREDIT_EUROPE_FILTER)
public class CreditEuropeAdditionalData extends AbstractAdditionalData implements Serializable {

	private static final long serialVersionUID = 6547651065145305145L;

	public static final String CREDIT_EUROPE_FILTER = "creditEuropeFilter";
	
	public static final String RDI_INFO = "rdiInfo";
	public static final String TOTAL_AMOUNT = "totalAmount";

	@JsonProperty(RDI_INFO)
	private String rdiInfo;

	@JsonProperty(TOTAL_AMOUNT)
	private String totalAmount;

	@Override
	public String determineFilterName() {
		return CREDIT_EUROPE_FILTER;
	}

	public String getRdiInfo() {
		return rdiInfo;
	}

	public void setRdiInfo(String rdiInfo) {
		this.rdiInfo = rdiInfo;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public HashMap<String, Object> readAdditionalData() {
		return readAdditionalData(CREDIT_EUROPE_FILTER, RDI_INFO, TOTAL_AMOUNT);
	}
}
