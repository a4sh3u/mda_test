package de.smava.webapp.brokerage.dao;

import de.smava.webapp.brokerage.domain.CustomerAssignment;
import de.smava.webapp.commons.dao.BaseDao;

/**
 * DAO interface for the domain object 'CustomerAssignment'.
 *
 */
public interface CustomerAssignmentDao extends BaseDao<CustomerAssignment> {

    /**
     * Saves the customer assignment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveCustomerAssignment(CustomerAssignment customerAssignment);

    CustomerAssignment findCurrentCustomerAssignment(Long customerNumber);

    void removeAssignment(Long customerId);
}
