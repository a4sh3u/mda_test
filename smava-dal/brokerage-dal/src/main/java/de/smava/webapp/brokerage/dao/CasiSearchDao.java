//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(casi search)}
import de.smava.webapp.brokerage.domain.CasiSearch;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'CasiSearchs'.
 *
 * @author generator
 */
public interface CasiSearchDao extends BaseDao<CasiSearch> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the casi search identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    CasiSearch getCasiSearch(Long id);

    /**
     * Saves the casi search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveCasiSearch(CasiSearch casiSearch);

    /**
     * Deletes an casi search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the casi search
     */
    void deleteCasiSearch(Long id);

    /**
     * Retrieves all 'CasiSearch' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<CasiSearch> getCasiSearchList();

    /**
     * Retrieves a page of 'CasiSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<CasiSearch> getCasiSearchList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'CasiSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<CasiSearch> getCasiSearchList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'CasiSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<CasiSearch> getCasiSearchList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'CasiSearch' instances.
     */
    long getCasiSearchCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(casi search)}
    //
    // insert custom methods here
    //

    Collection<CasiSearch> retrieveCasiCustomers(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate,
                                                 Integer offset, Integer limit, String orderBy, boolean desc);

    Collection<CasiSearch> retrieveCasiCustomers(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate,
                                                 Integer offset, Integer limit, String orderBy, boolean desc, boolean withCount);


    Long countRetrieveCasiCustomers(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate);
    // !!!!!!!! End of insert code section !!!!!!!!
}
