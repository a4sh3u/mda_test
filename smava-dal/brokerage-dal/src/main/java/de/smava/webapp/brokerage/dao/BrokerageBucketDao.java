//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket)}
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.brokerage.domain.BrokerageInterestRatePrediction;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.brokerage.domain.BrokerageBucket;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageBuckets'.
 *
 * @author generator
 */
public interface BrokerageBucketDao extends BaseDao<BrokerageBucket> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage bucket identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageBucket getBrokerageBucket(Long id);

    /**
     * Saves the brokerage bucket.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageBucket(BrokerageBucket brokerageBucket);

    /**
     * Deletes an brokerage bucket, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage bucket
     */
    void deleteBrokerageBucket(Long id);

    /**
     * Retrieves all 'BrokerageBucket' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageBucket> getBrokerageBucketList();

    /**
     * Retrieves a page of 'BrokerageBucket' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageBucket> getBrokerageBucketList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageBucket' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageBucket> getBrokerageBucketList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageBucket' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageBucket> getBrokerageBucketList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageBucket' instances.
     */
    long getBrokerageBucketCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage bucket)}

    /**
     * Retrieves all buckets with same loan application and number less than given number and
     * sorts them in bucket number ascending order
     * @param loanApplication that all buckets refer to
     * @return all buckets with same loan application
     */
    List<BrokerageBucket> listByLoanApplication(LoanApplication loanApplication, Integer number);

    /**
     * Retrieves all buckets with same loan application and
     * sorts them in bucket number ascending order
     * @param loanApplication that all buckets refer to
     * @return all buckets with same loan application
     */
    List<BrokerageBucket> listAllByLoanApplication(LoanApplication loanApplication);


    // !!!!!!!! End of insert code section !!!!!!!!
}
