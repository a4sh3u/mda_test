package de.smava.webapp.brokerage.dao.jdo;

import de.smava.webapp.brokerage.dao.CustomerAssignmentDao;
import de.smava.webapp.brokerage.domain.CustomerAssignment;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;


/**
 * DAO implementation for the domain object 'AdvisorAssignmentLogs'.
 *
 */
@Repository(value = "customerAssignment")
public class JdoCustomerAssignmentDao extends JdoBaseDao implements CustomerAssignmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCustomerAssignmentDao.class);

    private static final String CLASS_NAME = "CustomerAssignment";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    @Override
    public CustomerAssignment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        CustomerAssignment result = getEntity(CustomerAssignment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    public Long save(CustomerAssignment customerAssignment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCustomerAssignment: " + customerAssignment);
        }
        Long creditAdvisorId = customerAssignment.getCreditAdvisorId();
        CustomerAssignment existedAssignment = findCurrentCustomerAssignment(customerAssignment.getCustomerNumber());
        if(existedAssignment != null && creditAdvisorId.equals(existedAssignment.getCreditAdvisorId())){
            return existedAssignment.getId();
        }

        Long savedEntityId = saveEntity(customerAssignment);
        getPersistenceManager().flush();
        return savedEntityId;
    }

    @Override
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            CustomerAssignment entity = findUniqueEntity(CustomerAssignment.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    @Override
    public Long saveCustomerAssignment(CustomerAssignment customerAssignment) {
        return save(customerAssignment);
    }

    @Override
    public CustomerAssignment findCurrentCustomerAssignment(Long customerNumber) {
        if (customerNumber == null) {
           return null;
        }
        return findUniqueEntity(CustomerAssignment.class, "_customerNumber == " + customerNumber);
    }

    @Override
    public void removeAssignment(Long customerNumber) {
        CustomerAssignment customerAssignment = findCurrentCustomerAssignment(customerNumber);
        if(customerAssignment != null){
            super.deleteEntity(CustomerAssignment.class, customerAssignment.getId());
        }
    }
}
