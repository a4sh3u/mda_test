//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket configuration wildcard bank)}
import de.smava.webapp.brokerage.domain.history.BrokerageBucketConfigurationWildcardBankHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBucketConfigurationWildcardBanks'.
 *
 * A brokerage bucket configuration is a virtual collection of condition base on which brokerage application in linked to specific bucket.
 *
 * @author generator
 */
public class BrokerageBucketConfigurationWildcardBank extends BrokerageBucketConfigurationWildcardBankHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bucket configuration wildcard bank)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageBank _brokerageBank;
        protected BrokerageBucketConfiguration _brokerageBucketConfiguration;
        protected Set<BrokerageBucketWildcardExclusionRule> _brokerageBucketWildcardExclusionRules;
        
                                    
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    public void setBrokerageBucketConfiguration(BrokerageBucketConfiguration brokerageBucketConfiguration) {
        _brokerageBucketConfiguration = brokerageBucketConfiguration;
    }
            
    /**
     * Returns the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    public BrokerageBucketConfiguration getBrokerageBucketConfiguration() {
        return _brokerageBucketConfiguration;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket wildcard exclusion rules'.
     *
     * 
     *
     */
    public void setBrokerageBucketWildcardExclusionRules(Set<BrokerageBucketWildcardExclusionRule> brokerageBucketWildcardExclusionRules) {
        _brokerageBucketWildcardExclusionRules = brokerageBucketWildcardExclusionRules;
    }
            
    /**
     * Returns the property 'brokerage bucket wildcard exclusion rules'.
     *
     * 
     *
     */
    public Set<BrokerageBucketWildcardExclusionRule> getBrokerageBucketWildcardExclusionRules() {
        return _brokerageBucketWildcardExclusionRules;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBucketConfigurationWildcardBank.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageBucketConfigurationWildcardBank.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBucketConfigurationWildcardBank asBrokerageBucketConfigurationWildcardBank() {
        return this;
    }
}
