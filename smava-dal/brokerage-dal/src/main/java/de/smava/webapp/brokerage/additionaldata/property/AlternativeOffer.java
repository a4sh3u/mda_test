package de.smava.webapp.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public class AlternativeOffer {

    @JsonProperty
    private Double loanAmount;

    @JsonProperty
    private Double effInterestRate;

    @JsonProperty
    private Integer duration;

    @JsonProperty
    private Double monthlyRate;

    @JsonProperty
    private Double lastRate;

    @JsonProperty
    private Double interestAmount;

    @JsonProperty
    private String rdi;

    @JsonProperty
    private Double rdiAmount;

    @JsonProperty
    private Double serviceFee;

    @JsonProperty
    private Double loanAmountTotal;

    public Double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(Double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Double getEffInterestRate() {
        return effInterestRate;
    }

    public void setEffInterestRate(Double effInterestRate) {
        this.effInterestRate = effInterestRate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Double getMonthlyRate() {
        return monthlyRate;
    }

    public void setMonthlyRate(Double monthlyRate) {
        this.monthlyRate = monthlyRate;
    }

    public Double getLastRate() {
        return lastRate;
    }

    public void setLastRate(Double lastRate) {
        this.lastRate = lastRate;
    }

    public Double getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(Double interestAmount) {
        this.interestAmount = interestAmount;
    }

    public String getRdi() {
        return rdi;
    }

    public void setRdi(String rdi) {
        this.rdi = rdi;
    }

    public Double getRdiAmount() {
        return rdiAmount;
    }

    public void setRdiAmount(Double rdiAmount) {
        this.rdiAmount = rdiAmount;
    }

    public Double getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(Double serviceFee) {
        this.serviceFee = serviceFee;
    }

    public Double getLoanAmountTotal() {
        return loanAmountTotal;
    }

    public void setLoanAmountTotal(Double loanAmountTotal) {
        this.loanAmountTotal = loanAmountTotal;
    }
}
