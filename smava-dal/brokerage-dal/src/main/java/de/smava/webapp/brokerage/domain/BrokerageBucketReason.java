package de.smava.webapp.brokerage.domain;

/**
 * Created by dkeller on 18.05.15.
 */
public enum BrokerageBucketReason {

    WILDCARD,
    USER_SELECTED,
    CALCULATION,
    FOLLOWING,
    CASI;
}
