//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement bank filter)}
import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.MarketingPlacementBankFilter;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'MarketingPlacementBankFilters'.
 *
 * @author generator
 */
public interface MarketingPlacementBankFilterDao extends BaseDao<MarketingPlacementBankFilter> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing placement bank filter identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MarketingPlacementBankFilter getMarketingPlacementBankFilter(Long id);

    /**
     * Saves the marketing placement bank filter.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMarketingPlacementBankFilter(MarketingPlacementBankFilter marketingPlacementBankFilter);

    /**
     * Deletes an marketing placement bank filter, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing placement bank filter
     */
    void deleteMarketingPlacementBankFilter(Long id);

    /**
     * Retrieves all 'MarketingPlacementBankFilter' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList();

    /**
     * Retrieves a page of 'MarketingPlacementBankFilter' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingPlacementBankFilter' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingPlacementBankFilter' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'MarketingPlacementBankFilter' instances.
     */
    long getMarketingPlacementBankFilterCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing placement bank filter)}
    //
    // insert custom methods here
    
    /**
     * returns whether the the given external affiliate id is on the blacklist for a certain brokerage bank
     * @param externalAffiliateId
     * @param bb
     * @return
     */
    public boolean isFilteredExternalAffiliateId(String externalAffiliateId, BrokerageBank bb);

    /**
     * get all 'INCLUDE' marketing filters for a bank.
     * @param bb bank
     * @return include filters
     */
    Collection<MarketingPlacementBankFilter> findIncludeFilters(BrokerageBank bb);

    /**
     * get all 'INCLUDE' marketing filters for a external affiliate.
     * @param externalAffiliateId the base 64 encoded external affiliate id
     * @return include filters
     * @throws
     */
    Collection<MarketingPlacementBankFilter> findIncludeFilters(String externalAffiliateId) throws UnsupportedEncodingException;

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
