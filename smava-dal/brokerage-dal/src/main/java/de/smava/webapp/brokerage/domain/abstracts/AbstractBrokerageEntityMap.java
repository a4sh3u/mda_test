//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage entity map)}

import de.smava.webapp.brokerage.domain.interfaces.BrokerageEntityMapEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageEntityMaps'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * Temporary entity that maps redundant entities between brokerage and kredit privat.
 *
 * @author generator
 */
public abstract class AbstractBrokerageEntityMap
    extends KreditPrivatEntity implements BrokerageEntityMapEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageEntityMap.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage entity map)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

