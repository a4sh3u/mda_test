//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.smava.webapp.account.domain.Address;
import de.smava.webapp.account.domain.Document;
import de.smava.webapp.account.domain.DocumentContainer;
import de.smava.webapp.brokerage.additionaldata.AdditionalData;
import de.smava.webapp.brokerage.additionaldata.AdditionalDataFactory;
import de.smava.webapp.brokerage.domain.AdditionalBrokerageData;
import de.smava.webapp.brokerage.domain.BrokerageState;
import de.smava.webapp.brokerage.domain.interfaces.BrokerageApplicationEntityInterface;
import de.smava.webapp.brokerage.exception.BrokerageAdditionalDataException;
import de.smava.webapp.brokerage.util.MappingUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.StringWriter;
import java.util.*;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplications'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageApplication
    extends KreditPrivatEntity implements de.smava.webapp.brokerage.domain.RemoteBankRequestDataIf,BrokerageApplicationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageApplication.class);

    // !!!!!!!! You can insert code here: !!!!!!!!

    public int getRemainingDurationOfPayedoutCredit(){

        if ( getPayoutDate() != null && getDuration() != null){


            return getDuration() -  MappingUtils.getNumberOfMonthsOfPeriod(getPayoutDate(), CurrentDate.getDate());
        }
        return -1;
    }

    private transient Map<String, Address> _addressMap;

    public final Map<String, Address> getAddressMap() {
        return _addressMap;
    }

    public final void setAddressMap(Map<String, Address> addressMap) {
        setAddresses(new LinkedHashSet<Address>(addressMap.values()));
        _addressMap = addressMap;
    }

    public final boolean hasAttachment() {
        boolean hasAttachments = false;

        if (null != this.getDocumentContainer()
                && CollectionUtils.isNotEmpty(this.getDocumentContainer().getDocuments())
                && DocumentContainer.TYPE_REGISTRATION_COMPILATION.equals(this.getDocumentContainer().getType())) {

            for (Document doc : this.getDocumentContainer().getDocuments()) {
                if (CollectionUtils.isNotEmpty(doc.getAttachments())) {
                    hasAttachments = true;
                    break;
                }
            }
        }

        return hasAttachments;
    }

	/* ########### end of brokerage sending ################ */



    public String getTokenizedExtRefNumber() {
        if (StringUtils.isNotBlank(this.getExtRefNumber())) {
            StringTokenizer refNumberTokenizer = new StringTokenizer(this.getExtRefNumber(), ";");

            if (refNumberTokenizer.countTokens() <= 2) {
                return this.getExtRefNumber();
            } else {
                return refNumberTokenizer.nextToken() + ";" + refNumberTokenizer.nextToken();
            }
        }
        return "";
    }


    /**
     * @deprecated due to the change of Additional Data schema, use {@link #writeAdditionalData(AdditionalData)} instead.
     */
    @Deprecated
    public void writeAdditionalData(AdditionalBrokerageData value) {
        if (value == null){
            setAdditionalData(null);
        } else{
            JsonFactory fac = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter out = new StringWriter();

            try {
                // Create pretty printer:
                JsonGenerator gen = fac.createJsonGenerator(out);
                gen.useDefaultPrettyPrinter();

                FilterProvider filters =
                        new SimpleFilterProvider().addFilter(value.determineFilterName(),
                                SimpleBeanPropertyFilter.serializeAllExcept());

                // Now write:
                mapper.writer(filters).writeValue(gen, value);

                setAdditionalData(mapper.writer(filters).writeValueAsString(value));
            } catch (Exception e) {
                LOGGER.info("problem formatting additional data: ", e);
            }
        }
    }

    /**
     * Writes to database additional data serialized from the proper object to JSON string
     *
     * @param additionalData {@link AdditionalData}
     */
    public void writeAdditionalData(AdditionalData additionalData) {
        if (additionalData != null) {
            if (getBrokerageBank().getName().equals(additionalData.retrieveBankName())) {

                setAdditionalData(additionalData.asJson());

            } else {
                LOGGER.error(
                        String.format(
                                "Problem writing additional data for ba (id = %s): object for %s expected, %s given",
                                getId(),
                                getBrokerageBank().getName(),
                                additionalData.retrieveBankName()
                        )
                );
            }
        }
    }
    
    /**
     * Implementation of the method is quite error prone und should not be used
     * anymore. Use {@link #readAdditionalData(Class)} instead.
     * @param className
     * @return
     */
    @Deprecated
    public AdditionalBrokerageData readAdditionalData( AdditionalBrokerageData className ) {
        return readAdditionalData(className.getClass());
    }

    /**
     * @deprecated due to the change of Additional Data schema, use {@link #writeAdditionalData(AdditionalData)} instead.
     */
    @Deprecated    
    public <T extends AdditionalBrokerageData> T readAdditionalData( Class<T> clazz ) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            if (getAdditionalData() != null) {
                return mapper.readValue(getAdditionalData(), clazz);
            } else {
                return clazz.newInstance();
            }
        } catch (Exception e) {
            LOGGER.error("problem reading additional data ba(id="+getId()+"): ", e);
        }

        return null;
    }

    /**
     * Reads additional data from database and deserialize it to the proper object
     *
     * @return {@link AdditionalData}
     */
    public AdditionalData readAdditionalData() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            AdditionalData additionalData = AdditionalDataFactory.create(getBrokerageBank().getName());

            if (additionalData != null && getAdditionalData() != null) {
                return mapper.readValue(getAdditionalData(), additionalData.getClass());
            } else {
                return additionalData;
            }
        } catch (Exception e) {
            LOGGER.error("Problem getting additional data for ba (id = " + getId() + "): ", e);
        }

        return null;
    }
    
    /**
     * WARNING: It is not possible to set additionalData manually!
     * Use {@link #writeAdditionalData(AdditionalBrokerageData) writeAdditionalData} instead!
     * @param additionalData
     */
    @Override
    @Deprecated
    public void setAdditionalData(String additionalData) {
        LOGGER.error("Unallowed operation setAdditionalData(AdditionalBrokerageData): Use writeAdditionalData(AdditionalBrokerageData) instead! ",
                new BrokerageAdditionalDataException());
    }

    public Address getAddress(String addressType) {
        Address address = null;
        Set<Address> addresses = getAddresses();
        if (addresses != null && !addresses.isEmpty()) {

            // backward address list iterator to find last address
            Iterator<Address> addressIter = addresses.iterator();
            while (addressIter.hasNext()) {
                Address candidate = (Address) addressIter.next();
                if (addressType.equals(candidate.getType())) {
                    address = candidate;
                    break;
                }
            }
        }
        return address;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

