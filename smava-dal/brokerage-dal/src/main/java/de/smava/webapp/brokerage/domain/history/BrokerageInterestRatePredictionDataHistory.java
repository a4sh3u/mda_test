package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageInterestRatePredictionData;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageInterestRatePredictionDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageInterestRatePredictionDataHistory extends AbstractBrokerageInterestRatePredictionData {

    protected transient String _employmentInitVal;
    protected transient boolean _employmentIsSet;


								
    /**
     * Returns the initial value of the property 'employment'.
     */
    public String employmentInitVal() {
        String result;
        if (_employmentIsSet) {
            result = _employmentInitVal;
        } else {
            result = getEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employment'.
     */
    public boolean employmentIsDirty() {
        return !valuesAreEqual(employmentInitVal(), getEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'employment'.
     */
    public boolean employmentIsSet() {
        return _employmentIsSet;
    }
								
}
