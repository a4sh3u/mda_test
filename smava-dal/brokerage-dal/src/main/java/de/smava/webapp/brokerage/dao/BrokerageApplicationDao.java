
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageState;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.exception.InvalidValueException;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageApplications'.
 *
 * @author generator
 */
public interface BrokerageApplicationDao extends BaseDao<BrokerageApplication> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage application identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageApplication getBrokerageApplication(Long id);

    /**
     * Saves the brokerage application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageApplication(BrokerageApplication brokerageApplication);

    /**
     * Deletes an brokerage application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application
     */
    void deleteBrokerageApplication(Long id);

    /**
     * Retrieves all 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageApplication> getBrokerageApplicationList();

    /**
     * Retrieves a page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageApplication> getBrokerageApplicationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageApplication' instances.
     */
    long getBrokerageApplicationCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application)}
    /**
     * Returns'BrokerageApplication' instances for a given account and bank with given states.
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, BrokerageBank bank, Collection<BrokerageState> states);

    /**
     * Returns'BrokerageApplication' instances for a given account with given states.
     */
    Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, Collection<BrokerageState> states, Pageable pageable, Sortable sortable);

    /**
     * Returns'BrokerageApplication' instances for a given account with given states.
     */
    Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, Collection<BrokerageState> states);

    /**
     * Returns'BrokerageApplication' instances for a given account.
     */
    Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower);

    /**
     * Returns'BrokerageApplication' of the given bank that are created after the given date.
     * @param bank
     * @param states
     * @param creationDate
     * @param endDate
     * @return
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(BrokerageBank bank, Collection<BrokerageState> states, Date creationDate, Date endDate);

    /**
     * Returns 'BrokerageApplication' of the given bank that has the given extRefNumber.
     * @param bank
     * @param extRefNumber
     * @return
     */
    public BrokerageApplication findBrokerageApplicationList(BrokerageBank bank, String extRefNumber) throws InvalidValueException;

    /**
     * Returns 'BrokerageApplication' of the given bank that perfectly matches the given extRefNumber.
     * @param bank
     * @param extRefNumber
     * @return
     */
    public BrokerageApplication findBrokerageApplicationListWithMatchingExtRef(BrokerageBank bank, String extRefNumber) throws InvalidValueException;

    /**
     *
     * @param bank
     * @param states
     * @param minDate
     * @param maxDate
     * @return
     * @throws Exception
     */
    public Collection<BrokerageApplication> findBrokerageApplicationListWithoutEmail(BrokerageBank bank, Collection<BrokerageState> states, Date minDate, Date maxDate);

    /**
     *
     * @param borrower
     * @param bank
     * @param states
     * @param startSearch
     * @param endSearch
     * @return
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, BrokerageBank bank, Collection<BrokerageState> states, Date startSearch, Date endSearch);

    /**
     * latest brokerage application for account with one of the given states and not
     * older than <code>maxAgeInDays</code> days.
     * @param account
     * @param states
     * @param maxAgeInDays
     * @param onlyVisible only from loan applications that are visible
     * @return
     */
    public Collection<BrokerageApplication> getNewestBrokerageApplicationPerBank(Account account, Collection<BrokerageState> states, int maxAgeInDays, boolean onlyVisible);

    /**
     * Blk search applications based on list of ids.
     *
     * @param brokeageApplications - list of BA ids
     * @return list of matching BAs
     */
    List<BrokerageApplication> findApplications(List<Long> brokeageApplications);

    /**
     * Returns the latest BrokerageApplication which match criteria:
     * - BA was created for certain Person
     * - BA is related to certain BrokerageBank
     *
     * First check Person given in parameter as FIRST person, if won't find BA, try as SECOND person
     *
     * @param person
     * @param brokerageBank
     * @return
     */
    public BrokerageApplication getLatestBrokerageApplicationByPersonAndBrokerageBank(Person person, BrokerageBank brokerageBank);
    Collection<BrokerageApplication> fetchBrokerageApplicationBankIdAndByExtRef(Long bankId, String extRefNumber);

    Collection<BrokerageApplication> fetchBrokerageApplicationByAccountAndBankInAndStateInDescOrder(Long accountId,
                                                                                                    List<BrokerageBank> brokerageBanks,
                                                                                                    List<BrokerageState> brokerageStates);

    /**
     * Returns a list of BrokerageApplications searched for with accounitId and brokerageState
     *
     * @param accountId
     * @param brokerageState
     * @return  Collection<BrokerageApplication>
     */
    Collection<BrokerageApplication> getBrokerageApplicationListByAccountIdAndState(Long accountId, String brokerageState);

    /**
     * Returns last inserted Brokerage Application
     * @return {@link BrokerageApplication}
     */
    public BrokerageApplication getLastInsertedBrokerageApplication() throws InvalidValueException;

    /**
     * Get brokerage applications for specified loan applications and states
     * @param loanApplications collection of loan applications
     * @param brokerageStates
     * @return collection of brokerage applications for specified parameters.
     * If loanApplications or states are null or empty returns empty collection
     */
    Collection<BrokerageApplication> findBrokerageApplicationListForLAs(Collection<LoanApplication> loanApplications, Collection<BrokerageState> brokerageStates);

    BrokerageApplication findSuccessfulBAByAccount( Account account );

}
