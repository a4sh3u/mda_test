//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document type)}
import de.smava.webapp.brokerage.domain.history.DocumentTypeHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentTypes'.
 *
 * 
 *
 * @author generator
 */
public class DocumentType extends DocumentTypeHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(document type)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _type;
        protected Set<DocumentTypePlaceholder> _documentTypePlaceholders;
        
                            /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'document type placeholders'.
     *
     * 
     *
     */
    public void setDocumentTypePlaceholders(Set<DocumentTypePlaceholder> documentTypePlaceholders) {
        _documentTypePlaceholders = documentTypePlaceholders;
    }
            
    /**
     * Returns the property 'document type placeholders'.
     *
     * 
     *
     */
    public Set<DocumentTypePlaceholder> getDocumentTypePlaceholders() {
        return _documentTypePlaceholders;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentType.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(DocumentType.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DocumentType asDocumentType() {
        return this;
    }
}
