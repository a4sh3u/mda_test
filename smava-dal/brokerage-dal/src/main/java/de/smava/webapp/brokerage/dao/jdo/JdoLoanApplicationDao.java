package de.smava.webapp.brokerage.dao.jdo;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.brokerage.dao.LoanApplicationDao;
import de.smava.webapp.brokerage.domain.BrokerageState;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.sql.Timestamp;
import java.util.*;

/**
 * DAO implementation for the domain object 'LoanApplications'.
 */
@Repository(value = "loanApplicationDao")
public class JdoLoanApplicationDao extends JdoBaseDao implements LoanApplicationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoLoanApplicationDao.class);

    private static final String CLASS_NAME = "LoanApplication";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";


    /**
     * Returns an attached copy of the loan application identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public LoanApplication load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        LoanApplication result = getEntity(LoanApplication.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public LoanApplication getLoanApplication(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	LoanApplication entity = findUniqueEntity(LoanApplication.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the loan application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(LoanApplication loanApplication) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveLoanApplication: " + loanApplication);
        }

        return saveEntity(loanApplication);
    }

    /**
     * @deprecated Use {@link #save(LoanApplication) instead}
     */
    public Long saveLoanApplication(LoanApplication loanApplication) {
        return save(loanApplication);
    }

    /**
     * Deletes an loan application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the loan application
     */
    public void deleteLoanApplication(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteLoanApplication: " + id);
        }
        deleteEntity(LoanApplication.class, id);
    }

    /**
     * Retrieves all 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<LoanApplication> getLoanApplicationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<LoanApplication> getLoanApplicationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'LoanApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<LoanApplication> getLoanApplicationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<LoanApplication> getLoanApplicationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'LoanApplication' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'LoanApplication' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'LoanApplication' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'LoanApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'LoanApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'LoanApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'LoanApplication' instances.
     */
    public long getLoanApplicationCount() {
        long result = getEntityCount(LoanApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'LoanApplication' instances which match the given whereClause.
     */
    public long getLoanApplicationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(LoanApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'LoanApplication' instances which match the given whereClause together with params specified in object array.
     */
    public long getLoanApplicationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(LoanApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    public LoanApplication findCurrentLoanApplicationForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentLoanApplicationForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() == 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    public LoanApplication findCurrentVisibleLoanApplicationForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentVisibleLoanApplicationForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() >= 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    public LoanApplication findCurrentLoanApplicationWithCarFinanceForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentLoanApplicationWithCarFinanceForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);
        params.put(2, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() == 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<LoanApplication> findAllLoanApplicationsBetweenTwoDatesForAccount(Long accountId, Date fromDate, Date toDate) {
        Query query = getPersistenceManager().newQuery(LoanApplication.class);
        query.setFilter("this._account._id == :accountId && this._creationDate >= :fromDate && this._creationDate <= :toDate");

        return (Collection<LoanApplication>) query.execute(accountId, fromDate, toDate);
    }

    public Collection<LoanApplication> findLoanApplicationsByAccount(Long accountId) {

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findLoanApplicationByAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);
        params.put(2, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);
        
        return loanApps;
    }

    @Override
    public LoanApplication findRecentLoanApplicationByInitiatorType(Long accountId, LoanApplicationInitiatorType initiatorType) {

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findLoanApplicationByAccountAndInitiatorType");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);
        params.put(2, initiatorType.name());

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);
        if (!loanApps.isEmpty()) {
            return loanApps.iterator().next();
        }
        return null;
    }

    /**
     * Gets the states term for finding offers and orders.
     */
    private OqlTerm getStatesTerm(OqlTerm oqlTerm, Collection<BrokerageState> states) {
        OqlTerm result;
        if (states.size() > 1) {
            OqlTerm [] orStates = new OqlTerm[states.size()];
            int i = 0;
            for (BrokerageState state : states) {
                orStates[i] = oqlTerm.equals("_state", state.name());
                i++;
            }
            result = oqlTerm.or(orStates);
        } else if (states.size() == 1) {
            result = oqlTerm.equals("_state", states.iterator().next());
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }
        return result;
    }
    
    public Collection<LoanApplication> findLoanApplication(Account borrower, BrokerageState state) {

        OqlTerm oqlTerm = OqlTerm.newTerm();
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    oqlTerm.equals("_state", state));
      
        
        Collection<LoanApplication> result = findEntities(LoanApplication.class, oqlTerm.toString(), borrower, state);
        
        
        return result;
    }

    @Override
    public Collection<LoanApplication> findLoanApplicationsByCreationDateAndStates(Date date, Collection<BrokerageState> allowedStates, Collection<BrokerageState> deniedStates) {
        Date startDate = getStartTimeFromDate(date);
        Date endDate = getEndTimeFromDate(date);

        StringBuilder sb = new StringBuilder();

        sb.append("SELECT DISTINCT(la.id) AS id ");
        sb.append("FROM (SELECT *, RANK() ");
        sb.append("OVER (PARTITION BY account_id ");
        sb.append("ORDER BY creation_date DESC) dest_rank ");
        sb.append("FROM loan_application) as la ");
        sb.append("WHERE creation_date ");
        sb.append("BETWEEN '");
        sb.append(new Timestamp(startDate.getTime()));
        sb.append("' AND '");
        sb.append(new Timestamp(endDate.getTime()));
        sb.append("' ");
        sb.append("AND dest_rank = 1 ");
        sb.append("AND state IN (");

        int i = 0;
        for (BrokerageState state : allowedStates) {
            sb.append("'");
            sb.append(state);
            sb.append("'");
            if (++i < allowedStates.size()) {
                sb.append(", ");
            }
        }

        sb.append(") ");
        sb.append("AND account_id NOT IN (SELECT account_id ");
        sb.append("FROM loan_application ");
        sb.append("WHERE state IN (");

        int j = 0;
        for (BrokerageState state : deniedStates) {
            sb.append("'");
            sb.append(state);
            sb.append("'");
            if (++j < deniedStates.size()) {
                sb.append(", ");
            }
        }

        sb.append("))");

        Query query = getPersistenceManager().newQuery(Query.SQL, sb.toString());

        query.setClass(LoanApplication.class);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> result = (Collection<LoanApplication>) query.execute();

        return result;
    }

    @Override
    public Collection<Long> findLoanApplicationsDuringLastDays(int days) {
        Date now = CurrentDate.getDate();
        Date limitDateBottom = DateUtils.addDays(now, -days);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        StringBuffer sql =  new StringBuffer();
        sql.append("select max(la.id) as id "
                + "from "
                + "loan_application la "
                + "where la.creation_date BETWEEN :bottom AND :top "
                + "group by la.account_id");
        Map<String, Object> parameters = new HashMap<String, Object>(2);
        parameters.put("bottom", new Timestamp(limitDateBottom.getTime()));
        parameters.put("top", new Timestamp(now.getTime()));

        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(LoanApplication.class);
        Collection<LoanApplication> las = (Collection<LoanApplication>) query.executeWithMap(parameters);
        Collection<Long> ids = new ArrayList<Long>();
        for ( LoanApplication a : las) {
            ids.add(a.getId());
        }
        return ids;
    }

    public Collection<LoanApplication> findLoanApplicationsCreatedOnDate(Date date, Collection<BrokerageState> states){
        Date startDate = getStartTimeFromDate(date);
        Date endDate = getEndTimeFromDate(date);

        Collection<Object> params = new ArrayList<Object>();
        params.add(startDate);
        params.add(endDate);

        OqlTerm oqlTerm = OqlTerm.newTerm();
         
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.greaterThanEquals("_creationDate", startDate),
                    oqlTerm.lessThan("_creationDate", endDate),
                    getStatesTerm(oqlTerm, states));
        } else {
             oqlTerm.and(
                     oqlTerm.greaterThanEquals("_creationDate", startDate),
                     oqlTerm.lessThan("_creationDate", endDate));
        }

         Collection<LoanApplication> result = findEntities(LoanApplication.class, oqlTerm.toString(),  params.toArray());
         if (LOGGER.isDebugEnabled()) {
             LOGGER.debug("findOrderList() - result: size=" + result.size());
         }
         return result;
    }

    private Date getStartTimeFromDate(Date date) {
        Calendar start = CurrentDate.getCalendar();
        start.setTime(date);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        return start.getTime();
    }

    private Date getEndTimeFromDate(Date date) {
        Calendar end = CurrentDate.getCalendar();
        end.setTime(date);
        end.set(Calendar.HOUR_OF_DAY, 23);
        end.set(Calendar.MINUTE, 29);
        end.set(Calendar.SECOND, 59);
        end.set(Calendar.MILLISECOND,999);
        return end.getTime();
    }

}
