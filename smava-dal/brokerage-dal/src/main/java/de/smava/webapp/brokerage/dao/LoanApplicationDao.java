package de.smava.webapp.brokerage.dao;

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.brokerage.domain.BrokerageState;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;

/**
 * DAO interface for the domain object 'LoanApplications'.
 */
public interface LoanApplicationDao extends BaseDao<LoanApplication> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the loan application identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    LoanApplication getLoanApplication(Long id);

    /**
     * Saves the loan application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveLoanApplication(LoanApplication loanApplication);

    /**
     * Deletes an loan application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the loan application
     */
    void deleteLoanApplication(Long id);

    /**
     * Retrieves all 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<LoanApplication> getLoanApplicationList();

    /**
     * Retrieves a page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<LoanApplication> getLoanApplicationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'LoanApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<LoanApplication> getLoanApplicationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<LoanApplication> getLoanApplicationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'LoanApplication' instances.
     */
    long getLoanApplicationCount();

    LoanApplication findCurrentLoanApplicationForAccount(Long accountId);

    LoanApplication findCurrentVisibleLoanApplicationForAccount(Long accountId);

    LoanApplication findCurrentLoanApplicationWithCarFinanceForAccount(Long accountId);

    Collection<LoanApplication> findAllLoanApplicationsBetweenTwoDatesForAccount(Long accountId, Date fromDate, Date toDate);

    Collection<LoanApplication> findLoanApplication(Account borrower, BrokerageState states);

    Collection<LoanApplication> findLoanApplicationsByAccount(Long accountId);

    /**
     * Returns a list of borrower accounts that were active during the last day.
     *
     * Activity is defined in terms of new created loan application. That means that
     * the latest loan application for each account is returned for the case, that
     * the loan application was created during the last x days.
     *
     * @param days the number of days
     * @return loan application ids
     */
    Collection<Long> findLoanApplicationsDuringLastDays(int days);

    Collection<LoanApplication> findLoanApplicationsCreatedOnDate(Date date, Collection<BrokerageState> states);

    Collection<LoanApplication> findLoanApplicationsByCreationDateAndStates(
            Date date,
            Collection<BrokerageState> allowedStates,
            Collection<BrokerageState> deniedStates);

    LoanApplication findRecentLoanApplicationByInitiatorType(Long accountId, LoanApplicationInitiatorType initiatorType);

}
