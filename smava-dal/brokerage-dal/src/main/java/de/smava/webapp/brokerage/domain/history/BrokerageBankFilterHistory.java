package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBankFilter;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBankFilters'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankFilterHistory extends AbstractBrokerageBankFilter {

    protected transient String _conditionInitVal;
    protected transient boolean _conditionIsSet;


	
    /**
     * Returns the initial value of the property 'condition'.
     */
    public String conditionInitVal() {
        String result;
        if (_conditionIsSet) {
            result = _conditionInitVal;
        } else {
            result = getCondition();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'condition'.
     */
    public boolean conditionIsDirty() {
        return !valuesAreEqual(conditionInitVal(), getCondition());
    }

    /**
     * Returns true if the setter method was called for the property 'condition'.
     */
    public boolean conditionIsSet() {
        return _conditionIsSet;
    }
	
}
