package de.smava.webapp.brokerage.additionaldata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.smava.webapp.brokerage.additionaldata.property.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public abstract class AbstractAdditionalData implements AdditionalData {

    private static final Logger LOGGER = Logger.getLogger(AbstractAdditionalData.class);

    @JsonProperty
    private List<Reference> references;

    @JsonProperty
    private MissingDocuments missingDocuments;

    @JsonProperty
    private List<String> messages;

    @JsonProperty
    private List<String> information;

    @JsonProperty
    private Scoring scoring;

    @JsonProperty
    private List<SchufaInformation> schufaInformation;

    @JsonProperty
    private DocumentInformation documentInformation;

    @JsonProperty
    private List<AlternativeOffer> alternativeOffers;

    @JsonProperty
    private Map<String, Object> technicalData;

    @JsonIgnore
    private String videoIdentInformation;

    @JsonProperty
    private Boolean digitalLoan;

    /**
     * {@link AdditionalData#getReferences()}
     */
    @Override
    public List<Reference> getReferences() {
        if (this.references == null) {
            this.references = new ArrayList<Reference>();
        }

        return this.references;
    }

    /**
     * {@link AdditionalData#getMissingDocuments()}
     */
    @Override
    public MissingDocuments getMissingDocuments() {
        return missingDocuments;
    }

    /**
     * {@link AdditionalData#setMissingDocuments(MissingDocuments)}
     */
    @Override
    public void setMissingDocuments(MissingDocuments missingDocuments) {
        this.missingDocuments = missingDocuments;
    }

    /**
     * {@link AdditionalData#getMessages()}
     */
    @Override
    public List<String> getMessages() {
        if (this.messages == null) {
            this.messages = new ArrayList<String>();
        }

        return this.messages;
    }

    /**
     * {@link AdditionalData#getInformation()}
     */
    @Override
    public List<String> getInformation() {
        if (this.information == null) {
            this.information = new ArrayList<String>();
        }

        return this.information;
    }

    /**
     * {@link AdditionalData#getVideoIdentInformation()}
     */
    @Override
    public String getVideoIdentInformation() {
        if (this.information != null && !this.information.isEmpty()) {
            for (String info : this.information) {
                if (info.toLowerCase().trim().startsWith("1. videoident:")) {
                    return info.trim().substring("1. videoident:".length()).trim();
                } else if (info.toLowerCase().trim().startsWith("videoident:")) {
                    return info.trim().substring("videoident:".length()).trim();
                }
            }
        }

        return null;
    }

    public void setVideoIdentInformation(final String videoIdentInformation) {
        this.videoIdentInformation = videoIdentInformation;
    }

    /**
     * {@link AdditionalData#getScoring()}
     */
    @Override
    public Scoring getScoring() {
        return scoring;
    }

    /**
     * {@link AdditionalData#setScoring(Scoring)}
     */
    @Override
    public void setScoring(Scoring scoring) {
        this.scoring = scoring;
    }

    /**
     * {@link AdditionalData#isDigitalLoan()}
     */
    @Override
    public Boolean isDigitalLoan() {
        return digitalLoan;
    }

    /**
     * {@link AdditionalData#setDigitalLoan(Boolean)}
     */
    @Override
    public void setDigitalLoan(Boolean digitalLoan) {
        this.digitalLoan = digitalLoan;
    }

    /**
     * {@link AdditionalData#getSchufaInformation()}
     */
    @Override
    public List<SchufaInformation> getSchufaInformation() {
        if (this.schufaInformation == null) {
            this.schufaInformation = new ArrayList<SchufaInformation>();
        }

        return this.schufaInformation;
    }

    /**
     * {@link AdditionalData#getDocumentInformation()}
     */
    @Override
    public DocumentInformation getDocumentInformation() {
        return this.documentInformation;
    }

    /**
     * {@link AdditionalData#setDocumentInformation(DocumentInformation)}
     */
    public void setDocumentInformation(DocumentInformation documentInformation) {
        this.documentInformation = documentInformation;
    }

    /**
     * {@link AdditionalData#getAlternativeOffers()}
     */
    @Override
    public List<AlternativeOffer> getAlternativeOffers() {
        if (this.alternativeOffers == null) {
            this.alternativeOffers = new ArrayList<AlternativeOffer>();
        }

        return this.alternativeOffers;
    }

    /**
     * {@link AdditionalData#getTechnicalData()}
     */
    @Override
    public Map<String, Object> getTechnicalData() {
        if (technicalData == null) {
            technicalData = new HashMap<String, Object>();
        }

        return technicalData;
    }

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return null;
    }

    /**
     * {@link AdditionalData#asJson()}
     */
    @Override
    public String asJson() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            LOGGER.error("Cannot read additional data: ", e);
        }

        return null;
    }

    /**
     * {@link AdditionalData#read()}
     */
    @Override
    public Map<String, Object> read() {
        try {
            return new ObjectMapper().readValue(asJson(), HashMap.class);
        } catch (Exception e) {
            LOGGER.error("Cannot read additional data: ", e);
        }

        return null;
    }
}
