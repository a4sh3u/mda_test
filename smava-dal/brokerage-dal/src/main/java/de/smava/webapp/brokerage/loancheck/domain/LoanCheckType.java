/**
 * 
 */
package de.smava.webapp.brokerage.loancheck.domain;

/**
 * @author dkeller
 *
 */
public enum LoanCheckType {

	PERDIODICAL_CHECK,
	CONSOLIDATION_CHECK;
}
