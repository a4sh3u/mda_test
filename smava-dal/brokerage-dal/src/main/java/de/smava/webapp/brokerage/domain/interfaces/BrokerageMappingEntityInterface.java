package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageMapping;
import de.smava.webapp.brokerage.domain.BrokerageMappingConverter;
import de.smava.webapp.brokerage.domain.MappingDirection;


/**
 * The domain object that represents 'BrokerageMappings'.
 *
 * @author generator
 */
public interface BrokerageMappingEntityInterface {

    /**
     * Setter for the property 'converter'.
     *
     * 
     *
     */
    void setConverter(BrokerageMappingConverter converter);

    /**
     * Returns the property 'converter'.
     *
     * 
     *
     */
    BrokerageMappingConverter getConverter();
    /**
     * Setter for the property 'smava value'.
     *
     * 
     *
     */
    void setSmavaValue(String smavaValue);

    /**
     * Returns the property 'smava value'.
     *
     * 
     *
     */
    String getSmavaValue();
    /**
     * Setter for the property 'bank value'.
     *
     * 
     *
     */
    void setBankValue(String bankValue);

    /**
     * Returns the property 'bank value'.
     *
     * 
     *
     */
    String getBankValue();
    /**
     * Setter for the property 'direction'.
     *
     * 
     *
     */
    void setDirection(MappingDirection direction);

    /**
     * Returns the property 'direction'.
     *
     * 
     *
     */
    MappingDirection getDirection();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageMapping asBrokerageMapping();
}
