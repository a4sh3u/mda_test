package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractMarketingPlacementBankFilter;




/**
 * The domain object that has all history aggregation related fields for 'MarketingPlacementBankFilters'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingPlacementBankFilterHistory extends AbstractMarketingPlacementBankFilter {

    protected transient String _externalAffiliateIdInitVal;
    protected transient boolean _externalAffiliateIdIsSet;


	
    /**
     * Returns the initial value of the property 'external affiliate id'.
     */
    public String externalAffiliateIdInitVal() {
        String result;
        if (_externalAffiliateIdIsSet) {
            result = _externalAffiliateIdInitVal;
        } else {
            result = getExternalAffiliateId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external affiliate id'.
     */
    public boolean externalAffiliateIdIsDirty() {
        return !valuesAreEqual(externalAffiliateIdInitVal(), getExternalAffiliateId());
    }

    /**
     * Returns true if the setter method was called for the property 'external affiliate id'.
     */
    public boolean externalAffiliateIdIsSet() {
        return _externalAffiliateIdIsSet;
    }
		
}
