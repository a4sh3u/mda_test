package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBankGuaranteeConfiguration;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBankGuaranteeConfigurations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankGuaranteeConfigurationHistory extends AbstractBrokerageBankGuaranteeConfiguration {

    protected transient boolean _forceTopChanceInitVal;
    protected transient boolean _forceTopChanceIsSet;


		
    /**
     * Returns the initial value of the property 'force top chance'.
     */
    public boolean forceTopChanceInitVal() {
        boolean result;
        if (_forceTopChanceIsSet) {
            result = _forceTopChanceInitVal;
        } else {
            result = getForceTopChance();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'force top chance'.
     */
    public boolean forceTopChanceIsDirty() {
        return !valuesAreEqual(forceTopChanceInitVal(), getForceTopChance());
    }

    /**
     * Returns true if the setter method was called for the property 'force top chance'.
     */
    public boolean forceTopChanceIsSet() {
        return _forceTopChanceIsSet;
    }

}
