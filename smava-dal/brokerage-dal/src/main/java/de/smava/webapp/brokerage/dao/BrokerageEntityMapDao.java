//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage entity map)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.brokerage.domain.BrokerageEntityMap;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageEntityMaps'.
 *
 * @author generator
 */
public interface BrokerageEntityMapDao extends BaseDao<BrokerageEntityMap> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage entity map identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageEntityMap getBrokerageEntityMap(Long id);

    /**
     * Saves the brokerage entity map.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageEntityMap(BrokerageEntityMap brokerageEntityMap);

    /**
     * Deletes an brokerage entity map, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage entity map
     */
    void deleteBrokerageEntityMap(Long id);

    /**
     * Retrieves all 'BrokerageEntityMap' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageEntityMap> getBrokerageEntityMapList();

    /**
     * Retrieves a page of 'BrokerageEntityMap' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageEntityMap> getBrokerageEntityMapList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageEntityMap' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageEntityMap> getBrokerageEntityMapList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageEntityMap' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageEntityMap> getBrokerageEntityMapList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageEntityMap' instances.
     */
    long getBrokerageEntityMapCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage entity map)}
    //

    /**
     * Get latest map
     * @param kreditPrivatId old entity id
     * @param brokerageId new entity id
     * @param type of entity
     * @return null if not exists
     */
    BrokerageEntityMap getLatestMap(Long kreditPrivatId, Long brokerageId, String type);

    /**
     * Returns map given
     * @param brokerageId new entity id
     * @param type of entity
     * @return null if not exists
     */
    BrokerageEntityMap getByNewEntity(Long brokerageId, String type);

    /**
     * Returns map given
     * @param kreditPrivatId old entity id
     * @param type of entity
     * @return null if not exists
     */
    BrokerageEntityMap getByOldEntity(Long kreditPrivatId, String type);

    /**
     * Returns the id of the entity in kredit privat domain
     * @param brokerageId the id of the entity in brokerage domain
     * @param type short name of object type
     * @return the the id of the entity in kredit privat domain
     */
    Long getOldEntityId(Long brokerageId, String type);

    /**
     * Returns the id of the entity in brokerage domain
     * @param kreditPrivatId the id of the entity kredit privat
     * @param type short name of object type
     * @return the the id of the entity in brokerage domain
     */
    Long getNewEntityId(Long kreditPrivatId, String type);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
