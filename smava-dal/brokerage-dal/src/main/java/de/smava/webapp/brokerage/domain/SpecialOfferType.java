package de.smava.webapp.brokerage.domain;

public enum SpecialOfferType {
    BEST_INTEREST, TOP_INTEREST, SUPER_INTEREST;
}
