package de.smava.webapp.brokerage.domain;

import de.smava.webapp.brokerage.domain.history.LoanApplicationHistory;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'LoanApplications'.
 *
 * 
 *
 * @author generator
 */
public class LoanApplication extends LoanApplicationHistory  {

    protected Date _creationDate;
    protected BrokerageState _state;
    protected de.smava.webapp.account.domain.Account _account;
    protected String _requestedRdiType;
    protected double _requestedAmount;
    protected de.smava.webapp.account.domain.Category _reqeustedCategory;
    protected int _requestedDuration;
    protected boolean _sharedLoan;
    protected Set<BrokerageApplication> _brokerageApplications;
    protected de.smava.webapp.brokerage.domain.RequestedLoanType _requestedLoanType;
    protected de.smava.webapp.brokerage.domain.BrokerageCarFinance _brokerageCarFinance;
    protected de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType _initiatorType;
    protected String _initiatorTool;
    protected de.smava.webapp.account.domain.EconomicalData _economicalData;
    protected boolean _reachedEmailTimeout;
    protected BrokerageBank _selectedBank;
    protected boolean _visible;
    protected Double _customerValue;
    protected Long _initiatorSmavaId;

     /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(BrokerageState state) {
        _state = state;
    }
            
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public BrokerageState getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    public void setRequestedRdiType(String requestedRdiType) {
        if (!_requestedRdiTypeIsSet) {
            _requestedRdiTypeIsSet = true;
            _requestedRdiTypeInitVal = getRequestedRdiType();
        }
        registerChange("requested rdi type", _requestedRdiTypeInitVal, requestedRdiType);
        _requestedRdiType = requestedRdiType;
    }
                        
    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    public String getRequestedRdiType() {
        return _requestedRdiType;
    }
                                    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    public void setRequestedAmount(double requestedAmount) {
        if (!_requestedAmountIsSet) {
            _requestedAmountIsSet = true;
            _requestedAmountInitVal = getRequestedAmount();
        }
        registerChange("requested amount", _requestedAmountInitVal, requestedAmount);
        _requestedAmount = requestedAmount;
    }
                        
    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    public double getRequestedAmount() {
        return _requestedAmount;
    }
                                            
    /**
     * Setter for the property 'reqeusted category'.
     *
     * 
     *
     */
    public void setReqeustedCategory(de.smava.webapp.account.domain.Category reqeustedCategory) {
        _reqeustedCategory = reqeustedCategory;
    }
            
    /**
     * Returns the property 'reqeusted category'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Category getReqeustedCategory() {
        return _reqeustedCategory;
    }
                                    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    public void setRequestedDuration(int requestedDuration) {
        if (!_requestedDurationIsSet) {
            _requestedDurationIsSet = true;
            _requestedDurationInitVal = getRequestedDuration();
        }
        registerChange("requested duration", _requestedDurationInitVal, requestedDuration);
        _requestedDuration = requestedDuration;
    }
                        
    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    public int getRequestedDuration() {
        return _requestedDuration;
    }
                                    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    public void setSharedLoan(boolean sharedLoan) {
        if (!_sharedLoanIsSet) {
            _sharedLoanIsSet = true;
            _sharedLoanInitVal = getSharedLoan();
        }
        registerChange("shared loan", _sharedLoanInitVal, sharedLoan);
        _sharedLoan = sharedLoan;
    }
                        
    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    public boolean getSharedLoan() {
        return _sharedLoan;
    }
                                            
    /**
     * Setter for the property 'brokerage applications'.
     *
     * 
     *
     */
    public void setBrokerageApplications(Set<BrokerageApplication> brokerageApplications) {
        _brokerageApplications = brokerageApplications;
    }
            
    /**
     * Returns the property 'brokerage applications'.
     *
     * 
     *
     */
    public Set<BrokerageApplication> getBrokerageApplications() {
        return _brokerageApplications;
    }
                                            
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    public void setRequestedLoanType(de.smava.webapp.brokerage.domain.RequestedLoanType requestedLoanType) {
        _requestedLoanType = requestedLoanType;
    }
            
    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType() {
        return _requestedLoanType;
    }
                                            
    /**
     * Setter for the property 'brokerage car finance'.
     *
     * 
     *
     */
    public void setBrokerageCarFinance(de.smava.webapp.brokerage.domain.BrokerageCarFinance brokerageCarFinance) {
        _brokerageCarFinance = brokerageCarFinance;
    }
            
    /**
     * Returns the property 'brokerage car finance'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.BrokerageCarFinance getBrokerageCarFinance() {
        return _brokerageCarFinance;
    }
                                            
    /**
     * Setter for the property 'initiator type'.
     *
     * 
     *
     */
    public void setInitiatorType(de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType initiatorType) {
        _initiatorType = initiatorType;
    }
            
    /**
     * Returns the property 'initiator type'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType getInitiatorType() {
        return _initiatorType;
    }
                                    /**
     * Setter for the property 'initiator tool'.
     *
     * 
     *
     */
    public void setInitiatorTool(String initiatorTool) {
        if (!_initiatorToolIsSet) {
            _initiatorToolIsSet = true;
            _initiatorToolInitVal = getInitiatorTool();
        }
        registerChange("initiator tool", _initiatorToolInitVal, initiatorTool);
        _initiatorTool = initiatorTool;
    }
                        
    /**
     * Returns the property 'initiator tool'.
     *
     * 
     *
     */
    public String getInitiatorTool() {
        return _initiatorTool;
    }
                                            
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    public void setEconomicalData(de.smava.webapp.account.domain.EconomicalData economicalData) {
        _economicalData = economicalData;
    }
            
    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.EconomicalData getEconomicalData() {
        return _economicalData;
    }
                                    /**
     * Setter for the property 'reached email timeout'.
     *
     * 
     *
     */
    public void setReachedEmailTimeout(boolean reachedEmailTimeout) {
        if (!_reachedEmailTimeoutIsSet) {
            _reachedEmailTimeoutIsSet = true;
            _reachedEmailTimeoutInitVal = getReachedEmailTimeout();
        }
        registerChange("reached email timeout", _reachedEmailTimeoutInitVal, reachedEmailTimeout);
        _reachedEmailTimeout = reachedEmailTimeout;
    }
                        
    /**
     * Returns the property 'reached email timeout'.
     *
     * 
     *
     */
    public boolean getReachedEmailTimeout() {
        return _reachedEmailTimeout;
    }
                                            
    /**
     * Setter for the property 'selected bank'.
     *
     * 
     *
     */
    public void setSelectedBank(BrokerageBank selectedBank) {
        _selectedBank = selectedBank;
    }
            
    /**
     * Returns the property 'selected bank'.
     *
     * 
     *
     */
    public BrokerageBank getSelectedBank() {
        return _selectedBank;
    }
                                    /**
     * Setter for the property 'visible'.
     *
     * 
     *
     */
    public void setVisible(boolean visible) {
        if (!_visibleIsSet) {
            _visibleIsSet = true;
            _visibleInitVal = getVisible();
        }
        registerChange("visible", _visibleInitVal, visible);
        _visible = visible;
    }
                        
    /**
     * Returns the property 'visible'.
     *
     * 
     *
     */
    public boolean getVisible() {
        return _visible;
    }
                                            
    /**
     * Setter for the property 'customer value'.
     *
     * 
     *
     */
    public void setCustomerValue(Double customerValue) {
        _customerValue = customerValue;
    }
            
    /**
     * Returns the property 'customer value'.
     *
     * 
     *
     */
    public Double getCustomerValue() {
        return _customerValue;
    }


    /**
     * Setter for the property '_initiatorSmavaId'.
     *
     *
     *
     */
    public void setInitiatorSmavaId(Long _initiatorSmavaId) {
        this._initiatorSmavaId = _initiatorSmavaId;
    }

    /**
     * Returns the property '_initiatorSmavaId'.
     *
     *
     *
     */
    public Long getInitiatorSmavaId() {
        return _initiatorSmavaId;
    }

    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LoanApplication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _requestedRdiType=").append(_requestedRdiType);
            builder.append("\n    _requestedAmount=").append(_requestedAmount);
            builder.append("\n    _requestedDuration=").append(_requestedDuration);
            builder.append("\n    _sharedLoan=").append(_sharedLoan);
            builder.append("\n    _initiatorTool=").append(_initiatorTool);
            builder.append("\n    _reachedEmailTimeout=").append(_reachedEmailTimeout);
            builder.append("\n    _visible=").append(_visible);
            builder.append("\n}");
        } else {
            builder.append(LoanApplication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LoanApplication asLoanApplication() {
        return this;
    }
}
