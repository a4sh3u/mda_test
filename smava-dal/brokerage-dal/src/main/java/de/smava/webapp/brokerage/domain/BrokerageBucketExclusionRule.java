//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket exclusion rule)}
import de.smava.webapp.brokerage.domain.history.BrokerageBucketExclusionRuleHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBucketExclusionRules'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBucketExclusionRule extends BrokerageBucketExclusionRuleHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bucket exclusion rule)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.brokerage.domain.BrokerageBucketConfiguration _brokerageBucketConfiguration;
        protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
        protected String _exclusionRule;
        protected String _description;
        
                                    
    /**
     * Setter for the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    public void setBrokerageBucketConfiguration(de.smava.webapp.brokerage.domain.BrokerageBucketConfiguration brokerageBucketConfiguration) {
        _brokerageBucketConfiguration = brokerageBucketConfiguration;
    }
            
    /**
     * Returns the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.BrokerageBucketConfiguration getBrokerageBucketConfiguration() {
        return _brokerageBucketConfiguration;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'exclusion rule'.
     *
     * 
     *
     */
    public void setExclusionRule(String exclusionRule) {
        if (!_exclusionRuleIsSet) {
            _exclusionRuleIsSet = true;
            _exclusionRuleInitVal = getExclusionRule();
        }
        registerChange("exclusion rule", _exclusionRuleInitVal, exclusionRule);
        _exclusionRule = exclusionRule;
    }
                        
    /**
     * Returns the property 'exclusion rule'.
     *
     * 
     *
     */
    public String getExclusionRule() {
        return _exclusionRule;
    }
                                    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    public String getDescription() {
        return _description;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBucketExclusionRule.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _exclusionRule=").append(_exclusionRule);
            builder.append("\n    _description=").append(_description);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBucketExclusionRule.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBucketExclusionRule asBrokerageBucketExclusionRule() {
        return this;
    }
}
