package de.smava.webapp.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * @author KF
 * @since 23.06.2016.
 */
public class SchufaAttribute {

    @JsonProperty
    private String code;

    @JsonProperty
    @JsonFormat(locale = "de", shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "CET")
    private Date date;

    @JsonProperty
    private String currency;

    @JsonProperty
    private Double amount;

    @JsonProperty
    private Integer numberOfRates;

    @JsonProperty
    private String typeOfRates;

    @JsonProperty
    private String text;

    @JsonProperty
    private Boolean done;

    @JsonProperty
    @JsonFormat(locale = "de", shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "CET")
    private Date doneDate;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getNumberOfRates() {
        return numberOfRates;
    }

    public void setNumberOfRates(Integer numberOfRates) {
        this.numberOfRates = numberOfRates;
    }

    public String getTypeOfRates() {
        return typeOfRates;
    }

    public void setTypeOfRates(String typeOfRates) {
        this.typeOfRates = typeOfRates;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Date getDoneDate() {
        return doneDate;
    }

    public void setDoneDate(Date doneDate) {
        this.doneDate = doneDate;
    }
}
