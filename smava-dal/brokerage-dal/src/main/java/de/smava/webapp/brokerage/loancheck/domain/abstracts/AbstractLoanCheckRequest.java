//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.brokerage.loancheck.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan check request)}

import de.smava.webapp.brokerage.domain.BrokerageApplicationFilterReason;
import de.smava.webapp.brokerage.domain.RequestedLoanType;
import de.smava.webapp.brokerage.loancheck.domain.interfaces.LoanCheckRequestEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

                                                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanCheckRequests'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractLoanCheckRequest
    extends KreditPrivatEntity implements de.smava.webapp.brokerage.domain.RemoteBankRequestDataIf,LoanCheckRequestEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractLoanCheckRequest.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(loan check request)}

    public void setBrokerageApplicationFilterReason(BrokerageApplicationFilterReason brokerageApplicationFilterReason){
        LOGGER.info("filtred by reason: " + brokerageApplicationFilterReason);
    }

    public de.smava.webapp.account.domain.Account getAccount(){
        return getLoanCheck().getAccount();
    }

    public de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType(){
        return RequestedLoanType.STANDARD;
    }

    /**
     * Setter for the property 'effective interest'.
     */
    public void setEffectiveInterest(Double effectiveInterest, boolean propagate) {
        setEffectiveInterest(effectiveInterest);
    }

    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(Double amount, boolean propagate) {
        setAmount(amount);
    }

    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration, boolean propagate) {
        setDuration(duration);
    }

    /**
     * Setter for the property 'monthly rate'.
     */
    public void setMonthlyRate(Double monthlyRate, boolean propagate) {
        setMonthlyRate(monthlyRate);
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

