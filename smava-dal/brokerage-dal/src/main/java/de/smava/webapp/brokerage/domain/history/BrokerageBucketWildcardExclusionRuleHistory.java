package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBucketWildcardExclusionRule;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBucketWildcardExclusionRules'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBucketWildcardExclusionRuleHistory extends AbstractBrokerageBucketWildcardExclusionRule {

    protected transient String _exclusionRuleInitVal;
    protected transient boolean _exclusionRuleIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;


		
    /**
     * Returns the initial value of the property 'exclusion rule'.
     */
    public String exclusionRuleInitVal() {
        String result;
        if (_exclusionRuleIsSet) {
            result = _exclusionRuleInitVal;
        } else {
            result = getExclusionRule();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'exclusion rule'.
     */
    public boolean exclusionRuleIsDirty() {
        return !valuesAreEqual(exclusionRuleInitVal(), getExclusionRule());
    }

    /**
     * Returns true if the setter method was called for the property 'exclusion rule'.
     */
    public boolean exclusionRuleIsSet() {
        return _exclusionRuleIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }

}
