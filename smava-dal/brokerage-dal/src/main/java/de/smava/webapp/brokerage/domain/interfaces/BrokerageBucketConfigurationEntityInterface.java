package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.*;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageBucketConfigurations'.
 *
 * @author generator
 */
public interface BrokerageBucketConfigurationEntityInterface {

    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    void setRequestedLoanType(RequestedLoanType requestedLoanType);

    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    RequestedLoanType getRequestedLoanType();
    /**
     * Setter for the property 'wildcard brokerage banks'.
     *
     * Brokerage bank which is included in the bucket regardless of other conditions.
     *
     */
    void setWildcardBrokerageBanks(Set<BrokerageBucketConfigurationWildcardBank> wildcardBrokerageBanks);

    /**
     * Returns the property 'wildcard brokerage banks'.
     *
     * Brokerage bank which is included in the bucket regardless of other conditions.
     *
     */
    Set<BrokerageBucketConfigurationWildcardBank> getWildcardBrokerageBanks();
    /**
     * Setter for the property 'marketing placement id'.
     *
     * Marketing placement ID - it is not directly related with an object in purpose (to avoid dependence).
     *
     */
    void setMarketingPlacementId(Long marketingPlacementId);

    /**
     * Returns the property 'marketing placement id'.
     *
     * Marketing placement ID - it is not directly related with an object in purpose (to avoid dependence).
     *
     */
    Long getMarketingPlacementId();
    /**
     * Setter for the property 'bucket number'.
     *
     * 
     *
     */
    void setBucketNumber(Integer bucketNumber);

    /**
     * Returns the property 'bucket number'.
     *
     * 
     *
     */
    Integer getBucketNumber();
    /**
     * Setter for the property 'number of interest rate'.
     *
     * Number of brokerage applications with best interest rates which can be processed under this bucket.
     *
     */
    void setNumberOfInterestRate(Integer numberOfInterestRate);

    /**
     * Returns the property 'number of interest rate'.
     *
     * Number of brokerage applications with best interest rates which can be processed under this bucket.
     *
     */
    Integer getNumberOfInterestRate();
    /**
     * Setter for the property 'number of payout'.
     *
     * Number of brokerage applications with highest payout probability which can be processed under this bucket.
     *
     */
    void setNumberOfPayout(Integer numberOfPayout);

    /**
     * Returns the property 'number of payout'.
     *
     * Number of brokerage applications with highest payout probability which can be processed under this bucket.
     *
     */
    Integer getNumberOfPayout();
    /**
     * Setter for the property 'payout probability'.
     *
     * Minimal payout probability which brokerage application need to reach to become a part of certain bucket.
     *
     */
    void setPayoutProbability(Double payoutProbability);

    /**
     * Returns the property 'payout probability'.
     *
     * Minimal payout probability which brokerage application need to reach to become a part of certain bucket.
     *
     */
    Double getPayoutProbability();
    /**
     * Setter for the property 'delay'.
     *
     * Time in seconds after which next bucket should start processing.
     *
     */
    void setDelay(Long delay);

    /**
     * Returns the property 'delay'.
     *
     * Time in seconds after which next bucket should start processing.
     *
     */
    Long getDelay();
    /**
     * Setter for the property 'sufficient applications number'.
     *
     * Common value for all buckets - specify number of brokerage applications with status applied which have to be reached to stop processing (stop running another bucket).
     *
     */
    void setSufficientApplicationsNumber(Integer sufficientApplicationsNumber);

    /**
     * Returns the property 'sufficient applications number'.
     *
     * Common value for all buckets - specify number of brokerage applications with status applied which have to be reached to stop processing (stop running another bucket).
     *
     */
    Integer getSufficientApplicationsNumber();
    /**
     * Setter for the property 'expiration date'.
     *
     * Configuration with expiration date are not considered anymore (historical usage only).
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * Configuration with expiration date are not considered anymore (historical usage only).
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    void setBrokerageBucketBrokerageApplications(Set<BrokerageBucketBrokerageApplication> brokerageBucketBrokerageApplications);

    /**
     * Returns the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    Set<BrokerageBucketBrokerageApplication> getBrokerageBucketBrokerageApplications();
    /**
     * Setter for the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    void setBrokerageBucketExclusionRules(Set<BrokerageBucketExclusionRule> brokerageBucketExclusionRules);

    /**
     * Returns the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    Set<BrokerageBucketExclusionRule> getBrokerageBucketExclusionRules();
    /**
     * Setter for the property 'bucket type'.
     *
     * 
     *
     */
    void setBucketType(BucketType bucketType);

    /**
     * Returns the property 'bucket type'.
     *
     * 
     *
     */
    BucketType getBucketType();

    /**
     * Setter for the property 'Brokerage Bucket Following Configuration'.
     *
     * @return {@link FollowingBucketType}
     */
    FollowingBucketType getFollowingBucketType() ;

    /**
     * Returns the property 'Brokerage Bucket Following Configuration'.
     *
     * @param followingBucketType Following Bucket Type {@link FollowingBucketType}
     */
    void setFollowingBucketType(FollowingBucketType followingBucketType);

    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBucketConfiguration asBrokerageBucketConfiguration();
}
