package de.smava.webapp.brokerage.dto;

public class AdvisorPickingCounterTO {
    Long advisorId;
    Long pickingCounter;

    public AdvisorPickingCounterTO() {
    }

    public Long getAdvisorId() {
        return advisorId;
    }

    public void setAdvisorId(Long advisorId) {
        this.advisorId = advisorId;
    }

    public Long getPickingCounter() {
        return pickingCounter;
    }

    public void setPickingCounter(Long pickingCounter) {
        this.pickingCounter = pickingCounter;
    }
}
