package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBank;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBanks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankHistory extends AbstractBrokerageBank {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient boolean _validInitVal;
    protected transient boolean _validIsSet;
    protected transient boolean _comparisonOnlyInitVal;
    protected transient boolean _comparisonOnlyIsSet;
    protected transient String _identificationCompanyIdInitVal;
    protected transient boolean _identificationCompanyIdIsSet;


		
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid'.
     */
    public boolean validInitVal() {
        boolean result;
        if (_validIsSet) {
            result = _validInitVal;
        } else {
            result = getValid();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid'.
     */
    public boolean validIsDirty() {
        return !valuesAreEqual(validInitVal(), getValid());
    }

    /**
     * Returns true if the setter method was called for the property 'valid'.
     */
    public boolean validIsSet() {
        return _validIsSet;
    }
	
    /**
     * Returns the initial value of the property 'comparison only'.
     */
    public boolean comparisonOnlyInitVal() {
        boolean result;
        if (_comparisonOnlyIsSet) {
            result = _comparisonOnlyInitVal;
        } else {
            result = getComparisonOnly();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comparison only'.
     */
    public boolean comparisonOnlyIsDirty() {
        return !valuesAreEqual(comparisonOnlyInitVal(), getComparisonOnly());
    }

    /**
     * Returns true if the setter method was called for the property 'comparison only'.
     */
    public boolean comparisonOnlyIsSet() {
        return _comparisonOnlyIsSet;
    }
		
    /**
     * Returns the initial value of the property 'identification company id'.
     */
    public String identificationCompanyIdInitVal() {
        String result;
        if (_identificationCompanyIdIsSet) {
            result = _identificationCompanyIdInitVal;
        } else {
            result = getIdentificationCompanyId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'identification company id'.
     */
    public boolean identificationCompanyIdIsDirty() {
        return !valuesAreEqual(identificationCompanyIdInitVal(), getIdentificationCompanyId());
    }

    /**
     * Returns true if the setter method was called for the property 'identification company id'.
     */
    public boolean identificationCompanyIdIsSet() {
        return _identificationCompanyIdIsSet;
    }
					
}
