package de.smava.webapp.brokerage.dao;

import de.smava.webapp.brokerage.domain.AdvisorAssignmentLog;
import de.smava.webapp.brokerage.dto.AdvisorPickingCounterTO;
import de.smava.webapp.commons.dao.BaseDao;

import javax.transaction.Synchronization;
import java.util.Date;
import java.util.List;

/**
 * DAO interface for the domain object 'AdvisorAssignmentLogs'.
 */
public interface AdvisorAssignmentLogDao extends BaseDao<AdvisorAssignmentLog> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    AdvisorAssignmentLog findCurrentAdvisorAssignmentLogForBorrower(Long customerNumber);

    Long countFirstCallsByAdvisorAndDate(Long advisorId, Date date);

    List<Long> findPickedCustomersByAdvisorAndDate(Long advisorId, Date date);

    List<AdvisorPickingCounterTO> countFirstCallForAllAdvisors(Date date);
}
