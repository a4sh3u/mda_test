package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Adam Tomecki
 * @since 27.12.2016
 */
public class KreditPrivatAdditionalData extends AbstractAdditionalData {

    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_KREDIT_PRIVAT;
    }
}
