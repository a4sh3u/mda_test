package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Jakub Janus
 * @since 08.11.2016.
 */
public class OnlinekreditAdditionalData extends SwkAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_ONLINEKREDIT;
    }
}
