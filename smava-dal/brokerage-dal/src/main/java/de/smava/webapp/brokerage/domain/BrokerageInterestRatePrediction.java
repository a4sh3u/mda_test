//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage interest rate prediction)}
import de.smava.webapp.brokerage.domain.history.BrokerageInterestRatePredictionHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageInterestRatePredictions'.
 *
 * A brokerage interest rate prediction is a virtual collection of uploaded BrokerageInterestRatePredictionData.
 *
 * @author generator
 */
public class BrokerageInterestRatePrediction extends BrokerageInterestRatePredictionHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage interest rate prediction)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected Date _creationDate;
        protected Date _activationDate;
        protected Set<BrokerageInterestRatePredictionData> _brokerageInterestRatePredictionDatas;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'activation date'.
     *
     * 
     *
     */
    public void setActivationDate(Date activationDate) {
        if (!_activationDateIsSet) {
            _activationDateIsSet = true;
            _activationDateInitVal = getActivationDate();
        }
        registerChange("activation date", _activationDateInitVal, activationDate);
        _activationDate = activationDate;
    }
                        
    /**
     * Returns the property 'activation date'.
     *
     * 
     *
     */
    public Date getActivationDate() {
        return _activationDate;
    }
                                            
    /**
     * Setter for the property 'brokerage interest rate prediction datas'.
     *
     * 
     *
     */
    public void setBrokerageInterestRatePredictionDatas(Set<BrokerageInterestRatePredictionData> brokerageInterestRatePredictionDatas) {
        _brokerageInterestRatePredictionDatas = brokerageInterestRatePredictionDatas;
    }
            
    /**
     * Returns the property 'brokerage interest rate prediction datas'.
     *
     * 
     *
     */
    public Set<BrokerageInterestRatePredictionData> getBrokerageInterestRatePredictionDatas() {
        return _brokerageInterestRatePredictionDatas;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageInterestRatePrediction.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageInterestRatePrediction.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageInterestRatePrediction asBrokerageInterestRatePrediction() {
        return this;
    }
}
