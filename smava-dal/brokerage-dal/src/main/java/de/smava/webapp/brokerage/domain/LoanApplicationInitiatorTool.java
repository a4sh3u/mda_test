package de.smava.webapp.brokerage.domain;

public enum LoanApplicationInitiatorTool {
	CASI, SOAP, MY_SMAVA;
}
