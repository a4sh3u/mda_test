//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.loancheck.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan check request)}
import de.smava.webapp.brokerage.loancheck.domain.history.LoanCheckRequestHistory;

import java.util.Date;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanCheckRequests'.
 *
 * @author generator
 */
public class LoanCheckRequest extends LoanCheckRequestHistory  implements de.smava.webapp.brokerage.domain.RemoteBankRequestDataIf {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(loan check request)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
        protected de.smava.webapp.brokerage.domain.BrokerageState _state;
        protected de.smava.webapp.account.domain.EconomicalData _economicalData;
        protected de.smava.webapp.account.domain.Person _firstPerson;
        protected de.smava.webapp.account.domain.Person _secondPerson;
        protected double _requestedAmount;
        protected int _requestedDuration;
        protected String _requestedRdiType;
        protected Double _monthlyRate;
        protected Double _effectiveInterest;
        protected Double _amount;
        protected Integer _duration;
        protected LoanCheck _loanCheck;
        protected Set<LoanCheckRequestData> _loanCheckRequestDatas;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     */
    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'state'.
     */
    public void setState(de.smava.webapp.brokerage.domain.BrokerageState state) {
        _state = state;
    }
            
    /**
     * Returns the property 'state'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageState getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'economical data'.
     */
    public void setEconomicalData(de.smava.webapp.account.domain.EconomicalData economicalData) {
        _economicalData = economicalData;
    }
            
    /**
     * Returns the property 'economical data'.
     */
    public de.smava.webapp.account.domain.EconomicalData getEconomicalData() {
        return _economicalData;
    }
                                            
    /**
     * Setter for the property 'first person'.
     */
    public void setFirstPerson(de.smava.webapp.account.domain.Person firstPerson) {
        _firstPerson = firstPerson;
    }
            
    /**
     * Returns the property 'first person'.
     */
    public de.smava.webapp.account.domain.Person getFirstPerson() {
        return _firstPerson;
    }
                                            
    /**
     * Setter for the property 'second person'.
     */
    public void setSecondPerson(de.smava.webapp.account.domain.Person secondPerson) {
        _secondPerson = secondPerson;
    }
            
    /**
     * Returns the property 'second person'.
     */
    public de.smava.webapp.account.domain.Person getSecondPerson() {
        return _secondPerson;
    }
                                    /**
     * Setter for the property 'requested amount'.
     */
    public void setRequestedAmount(double requestedAmount) {
        if (!_requestedAmountIsSet) {
            _requestedAmountIsSet = true;
            _requestedAmountInitVal = getRequestedAmount();
        }
        registerChange("requested amount", _requestedAmountInitVal, requestedAmount);
        _requestedAmount = requestedAmount;
    }
                        
    /**
     * Returns the property 'requested amount'.
     */
    public double getRequestedAmount() {
        return _requestedAmount;
    }
                                    /**
     * Setter for the property 'requested duration'.
     */
    public void setRequestedDuration(int requestedDuration) {
        if (!_requestedDurationIsSet) {
            _requestedDurationIsSet = true;
            _requestedDurationInitVal = getRequestedDuration();
        }
        registerChange("requested duration", _requestedDurationInitVal, requestedDuration);
        _requestedDuration = requestedDuration;
    }
                        
    /**
     * Returns the property 'requested duration'.
     */
    public int getRequestedDuration() {
        return _requestedDuration;
    }
                                    /**
     * Setter for the property 'requested rdi type'.
     */
    public void setRequestedRdiType(String requestedRdiType) {
        if (!_requestedRdiTypeIsSet) {
            _requestedRdiTypeIsSet = true;
            _requestedRdiTypeInitVal = getRequestedRdiType();
        }
        registerChange("requested rdi type", _requestedRdiTypeInitVal, requestedRdiType);
        _requestedRdiType = requestedRdiType;
    }
                        
    /**
     * Returns the property 'requested rdi type'.
     */
    public String getRequestedRdiType() {
        return _requestedRdiType;
    }
                                            
    /**
     * Setter for the property 'monthly rate'.
     */
    public void setMonthlyRate(Double monthlyRate) {
        _monthlyRate = monthlyRate;
    }
            
    /**
     * Returns the property 'monthly rate'.
     */
    public Double getMonthlyRate() {
        return _monthlyRate;
    }
                                            
    /**
     * Setter for the property 'effective interest'.
     */
    public void setEffectiveInterest(Double effectiveInterest) {
        _effectiveInterest = effectiveInterest;
    }
            
    /**
     * Returns the property 'effective interest'.
     */
    public Double getEffectiveInterest() {
        return _effectiveInterest;
    }
                                            
    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(Double amount) {
        _amount = amount;
    }
            
    /**
     * Returns the property 'amount'.
     */
    public Double getAmount() {
        return _amount;
    }
                                            
    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration) {
        _duration = duration;
    }
            
    /**
     * Returns the property 'duration'.
     */
    public Integer getDuration() {
        return _duration;
    }
                                            
    /**
     * Setter for the property 'loan check'.
     */
    public void setLoanCheck(LoanCheck loanCheck) {
        _loanCheck = loanCheck;
    }
            
    /**
     * Returns the property 'loan check'.
     */
    public LoanCheck getLoanCheck() {
        return _loanCheck;
    }
                                            
    /**
     * Setter for the property 'loan check request datas'.
     */
    public void setLoanCheckRequestDatas(Set<LoanCheckRequestData> loanCheckRequestDatas) {
        _loanCheckRequestDatas = loanCheckRequestDatas;
    }
            
    /**
     * Returns the property 'loan check request datas'.
     */
    public Set<LoanCheckRequestData> getLoanCheckRequestDatas() {
        return _loanCheckRequestDatas;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LoanCheckRequest.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _requestedAmount=").append(_requestedAmount);
            builder.append("\n    _requestedDuration=").append(_requestedDuration);
            builder.append("\n    _requestedRdiType=").append(_requestedRdiType);
            builder.append("\n}");
        } else {
            builder.append(LoanCheckRequest.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LoanCheckRequest asLoanCheckRequest() {
        return this;
    }
}
