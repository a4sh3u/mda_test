//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank guarantee configuration)}
import de.smava.webapp.brokerage.domain.history.BrokerageBankGuaranteeConfigurationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBankGuaranteeConfigurations'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBankGuaranteeConfiguration extends BrokerageBankGuaranteeConfigurationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bank guarantee configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageBank _brokerageBank;
        protected boolean _forceTopChance;
        
                                    
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'force top chance'.
     *
     * 
     *
     */
    public void setForceTopChance(boolean forceTopChance) {
        if (!_forceTopChanceIsSet) {
            _forceTopChanceIsSet = true;
            _forceTopChanceInitVal = getForceTopChance();
        }
        registerChange("force top chance", _forceTopChanceInitVal, forceTopChance);
        _forceTopChance = forceTopChance;
    }
                        
    /**
     * Returns the property 'force top chance'.
     *
     * 
     *
     */
    public boolean getForceTopChance() {
        return _forceTopChance;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBankGuaranteeConfiguration.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _forceTopChance=").append(_forceTopChance);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBankGuaranteeConfiguration.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBankGuaranteeConfiguration asBrokerageBankGuaranteeConfiguration() {
        return this;
    }
}
