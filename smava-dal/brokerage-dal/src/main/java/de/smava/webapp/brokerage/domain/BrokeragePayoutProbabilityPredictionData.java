//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage payout probability prediction data)}
import de.smava.webapp.brokerage.domain.history.BrokeragePayoutProbabilityPredictionDataHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokeragePayoutProbabilityPredictionDatas'.
 *
 * A brokerage payout probability prediction data is a virtual collection of data which indicate (based of certain criteria) a probability of brokerage (loan) payout.
 *
 * @author generator
 */
public class BrokeragePayoutProbabilityPredictionData extends BrokeragePayoutProbabilityPredictionDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage payout probability prediction data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokeragePayoutProbabilityPrediction _brokeragePayoutProbabilityPrediction;
        protected BrokerageBank _brokerageBank;
        protected Double _requestedAmountMin;
        protected Double _requestedAmountMax;
        protected Integer _requestedDurationMin;
        protected Integer _requestedDurationMax;
        protected Double _incomeMin;
        protected Double _incomeMax;
        protected Boolean _schufaAvailable;
        protected Integer _schufaMin;
        protected Integer _schufaMax;
        protected Double _payout;
        
                                    
    /**
     * Setter for the property 'brokerage payout probability prediction'.
     *
     * 
     *
     */
    public void setBrokeragePayoutProbabilityPrediction(BrokeragePayoutProbabilityPrediction brokeragePayoutProbabilityPrediction) {
        _brokeragePayoutProbabilityPrediction = brokeragePayoutProbabilityPrediction;
    }
            
    /**
     * Returns the property 'brokerage payout probability prediction'.
     *
     * 
     *
     */
    public BrokeragePayoutProbabilityPrediction getBrokeragePayoutProbabilityPrediction() {
        return _brokeragePayoutProbabilityPrediction;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'requested amount min'.
     *
     * 
     *
     */
    public void setRequestedAmountMin(Double requestedAmountMin) {
        _requestedAmountMin = requestedAmountMin;
    }
            
    /**
     * Returns the property 'requested amount min'.
     *
     * 
     *
     */
    public Double getRequestedAmountMin() {
        return _requestedAmountMin;
    }
                                            
    /**
     * Setter for the property 'requested amount max'.
     *
     * 
     *
     */
    public void setRequestedAmountMax(Double requestedAmountMax) {
        _requestedAmountMax = requestedAmountMax;
    }
            
    /**
     * Returns the property 'requested amount max'.
     *
     * 
     *
     */
    public Double getRequestedAmountMax() {
        return _requestedAmountMax;
    }
                                            
    /**
     * Setter for the property 'requested duration min'.
     *
     * 
     *
     */
    public void setRequestedDurationMin(Integer requestedDurationMin) {
        _requestedDurationMin = requestedDurationMin;
    }
            
    /**
     * Returns the property 'requested duration min'.
     *
     * 
     *
     */
    public Integer getRequestedDurationMin() {
        return _requestedDurationMin;
    }
                                            
    /**
     * Setter for the property 'requested duration max'.
     *
     * 
     *
     */
    public void setRequestedDurationMax(Integer requestedDurationMax) {
        _requestedDurationMax = requestedDurationMax;
    }
            
    /**
     * Returns the property 'requested duration max'.
     *
     * 
     *
     */
    public Integer getRequestedDurationMax() {
        return _requestedDurationMax;
    }
                                            
    /**
     * Setter for the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    public void setIncomeMin(Double incomeMin) {
        _incomeMin = incomeMin;
    }
            
    /**
     * Returns the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    public Double getIncomeMin() {
        return _incomeMin;
    }
                                            
    /**
     * Setter for the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    public void setIncomeMax(Double incomeMax) {
        _incomeMax = incomeMax;
    }
            
    /**
     * Returns the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    public Double getIncomeMax() {
        return _incomeMax;
    }
                                            
    /**
     * Setter for the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    public void setSchufaAvailable(Boolean schufaAvailable) {
        _schufaAvailable = schufaAvailable;
    }
            
    /**
     * Returns the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    public Boolean getSchufaAvailable() {
        return _schufaAvailable;
    }
                                            
    /**
     * Setter for the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    public void setSchufaMin(Integer schufaMin) {
        _schufaMin = schufaMin;
    }
            
    /**
     * Returns the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    public Integer getSchufaMin() {
        return _schufaMin;
    }
                                            
    /**
     * Setter for the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    public void setSchufaMax(Integer schufaMax) {
        _schufaMax = schufaMax;
    }
            
    /**
     * Returns the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    public Integer getSchufaMax() {
        return _schufaMax;
    }
                                            
    /**
     * Setter for the property 'payout'.
     *
     * 
     *
     */
    public void setPayout(Double payout) {
        _payout = payout;
    }
            
    /**
     * Returns the property 'payout'.
     *
     * 
     *
     */
    public Double getPayout() {
        return _payout;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokeragePayoutProbabilityPredictionData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokeragePayoutProbabilityPredictionData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokeragePayoutProbabilityPredictionData asBrokeragePayoutProbabilityPredictionData() {
        return this;
    }
}
