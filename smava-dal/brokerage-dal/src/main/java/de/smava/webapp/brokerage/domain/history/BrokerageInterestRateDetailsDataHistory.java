package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageInterestRateDetailsData;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageInterestRateDetailsDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageInterestRateDetailsDataHistory extends AbstractBrokerageInterestRateDetailsData {

    protected transient String _bankNameInitVal;
    protected transient boolean _bankNameIsSet;


		
    /**
     * Returns the initial value of the property 'bank name'.
     */
    public String bankNameInitVal() {
        String result;
        if (_bankNameIsSet) {
            result = _bankNameInitVal;
        } else {
            result = getBankName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank name'.
     */
    public boolean bankNameIsDirty() {
        return !valuesAreEqual(bankNameInitVal(), getBankName());
    }

    /**
     * Returns true if the setter method was called for the property 'bank name'.
     */
    public boolean bankNameIsSet() {
        return _bankNameIsSet;
    }
										
}
