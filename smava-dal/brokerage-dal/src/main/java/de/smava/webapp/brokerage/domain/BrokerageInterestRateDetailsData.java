//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage interest rate details data)}
import de.smava.webapp.brokerage.domain.history.BrokerageInterestRateDetailsDataHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageInterestRateDetailsDatas'.
 *
 * @author generator
 */
public class BrokerageInterestRateDetailsData extends BrokerageInterestRateDetailsDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage interest rate details data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageInterestRateDetails _brokerageInterestRateDetails;
        protected String _bankName;
        protected de.smava.webapp.brokerage.domain.RequestedLoanType _requestedLoanType;
        protected Integer _duration;
        protected Double _minAmount;
        protected Double _maxAmount;
        protected Double _minEffectiveInterest;
        protected Double _maxEffectiveInterest;
        protected Double _representativeEffectiveInterest;
        protected Double _representativeDebtInterest;
        protected Boolean _eitherOrEffectiveInterest;
        protected de.smava.webapp.brokerage.domain.SpecialOfferType _specialOfferType;
        protected Boolean _fullyDigitalOffer;
        
                                    
    /**
     * Setter for the property 'brokerage interest rate details'.
     */
    public void setBrokerageInterestRateDetails(BrokerageInterestRateDetails brokerageInterestRateDetails) {
        _brokerageInterestRateDetails = brokerageInterestRateDetails;
    }
            
    /**
     * Returns the property 'brokerage interest rate details'.
     */
    public BrokerageInterestRateDetails getBrokerageInterestRateDetails() {
        return _brokerageInterestRateDetails;
    }
                                    /**
     * Setter for the property 'bank name'.
     */
    public void setBankName(String bankName) {
        if (!_bankNameIsSet) {
            _bankNameIsSet = true;
            _bankNameInitVal = getBankName();
        }
        registerChange("bank name", _bankNameInitVal, bankName);
        _bankName = bankName;
    }
                        
    /**
     * Returns the property 'bank name'.
     */
    public String getBankName() {
        return _bankName;
    }
                                            
    /**
     * Setter for the property 'requested loan type'.
     */
    public void setRequestedLoanType(de.smava.webapp.brokerage.domain.RequestedLoanType requestedLoanType) {
        _requestedLoanType = requestedLoanType;
    }
            
    /**
     * Returns the property 'requested loan type'.
     */
    public de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType() {
        return _requestedLoanType;
    }
                                            
    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration) {
        _duration = duration;
    }
            
    /**
     * Returns the property 'duration'.
     */
    public Integer getDuration() {
        return _duration;
    }
                                            
    /**
     * Setter for the property 'min amount'.
     */
    public void setMinAmount(Double minAmount) {
        _minAmount = minAmount;
    }
            
    /**
     * Returns the property 'min amount'.
     */
    public Double getMinAmount() {
        return _minAmount;
    }
                                            
    /**
     * Setter for the property 'max amount'.
     */
    public void setMaxAmount(Double maxAmount) {
        _maxAmount = maxAmount;
    }
            
    /**
     * Returns the property 'max amount'.
     */
    public Double getMaxAmount() {
        return _maxAmount;
    }
                                            
    /**
     * Setter for the property 'min effective interest'.
     */
    public void setMinEffectiveInterest(Double minEffectiveInterest) {
        _minEffectiveInterest = minEffectiveInterest;
    }
            
    /**
     * Returns the property 'min effective interest'.
     */
    public Double getMinEffectiveInterest() {
        return _minEffectiveInterest;
    }
                                            
    /**
     * Setter for the property 'max effective interest'.
     */
    public void setMaxEffectiveInterest(Double maxEffectiveInterest) {
        _maxEffectiveInterest = maxEffectiveInterest;
    }
            
    /**
     * Returns the property 'max effective interest'.
     */
    public Double getMaxEffectiveInterest() {
        return _maxEffectiveInterest;
    }
                                            
    /**
     * Setter for the property 'representative effective interest'.
     */
    public void setRepresentativeEffectiveInterest(Double representativeEffectiveInterest) {
        _representativeEffectiveInterest = representativeEffectiveInterest;
    }
            
    /**
     * Returns the property 'representative effective interest'.
     */
    public Double getRepresentativeEffectiveInterest() {
        return _representativeEffectiveInterest;
    }
                                            
    /**
     * Setter for the property 'representative debt interest'.
     */
    public void setRepresentativeDebtInterest(Double representativeDebtInterest) {
        _representativeDebtInterest = representativeDebtInterest;
    }
            
    /**
     * Returns the property 'representative debt interest'.
     */
    public Double getRepresentativeDebtInterest() {
        return _representativeDebtInterest;
    }
                                            
    /**
     * Setter for the property 'either or effective interest'.
     */
    public void setEitherOrEffectiveInterest(Boolean eitherOrEffectiveInterest) {
        _eitherOrEffectiveInterest = eitherOrEffectiveInterest;
    }
            
    /**
     * Returns the property 'either or effective interest'.
     */
    public Boolean getEitherOrEffectiveInterest() {
        return _eitherOrEffectiveInterest;
    }
                                            
    /**
     * Setter for the property 'special offer type'.
     */
    public void setSpecialOfferType(de.smava.webapp.brokerage.domain.SpecialOfferType specialOfferType) {
        _specialOfferType = specialOfferType;
    }
            
    /**
     * Returns the property 'special offer type'.
     */
    public de.smava.webapp.brokerage.domain.SpecialOfferType getSpecialOfferType() {
        return _specialOfferType;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageInterestRateDetailsData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _bankName=").append(_bankName);
            builder.append("\n}");
        } else {
            builder.append(BrokerageInterestRateDetailsData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageInterestRateDetailsData asBrokerageInterestRateDetailsData() {
        return this;
    }

    /**
     * Returns the property 'fully digital offer'.
     */
    public Boolean getFullyDigitalOffer() {
        return _fullyDigitalOffer;
    }

    /**
     * sets the property 'fully digital offer'.
     */
    public void setFullyDigitalOffer(Boolean fullyDigitalOffer) {
        this._fullyDigitalOffer = fullyDigitalOffer;
    }
}
