//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage car finance)}

import de.smava.webapp.brokerage.domain.BrokerageCarFinance;
import de.smava.webapp.brokerage.domain.interfaces.BrokerageCarFinanceEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

                                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageCarFinances'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageCarFinance
    extends KreditPrivatEntity implements BrokerageCarFinanceEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageCarFinance.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage car finance)}
    // taken from here: http://www.schnelle-online.info/Umrechnung/KW-PS.html
    private static final double KW_TO_PS_FACTOR = 1.359621617;

    public Double getVehiclePowerPs() {
        Integer base = getVehiclePowerKw();
        if (base != null) {
            return base * KW_TO_PS_FACTOR;
        }
        return 0d;
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageCarFinance)) {
            equals = false;
        }


        equals = equals && valuesAreEqual(this.getVehicleType(), ((BrokerageCarFinance) otherEntity).getVehicleType());
        equals = equals && valuesAreEqual(this.getVehicleBrand(), ((BrokerageCarFinance) otherEntity).getVehicleBrand());
        equals = equals && valuesAreEqual(this.getVehicleModel(), ((BrokerageCarFinance) otherEntity).getVehicleModel());
        equals = equals && valuesAreEqual(this.getVehiclePowerKw(), ((BrokerageCarFinance) otherEntity).getVehiclePowerKw());
        equals = equals && valuesAreEqual(this.getVehicleRegistrationYear(), ((BrokerageCarFinance) otherEntity).getVehicleRegistrationYear());
        equals = equals && valuesAreEqual(this.getVehicleKm(), ((BrokerageCarFinance) otherEntity).getVehicleKm());
        equals = equals && valuesAreEqual(this.getVehiclePrice(), ((BrokerageCarFinance) otherEntity).getVehiclePrice());
        equals = equals && valuesAreEqual(this.getInitialPayment(), ((BrokerageCarFinance) otherEntity).getInitialPayment());

        return equals;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

