package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractCasiSearch;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CasiSearchs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CasiSearchHistory extends AbstractCasiSearch {

    protected transient String _phoneInitVal;
    protected transient boolean _phoneIsSet;
    protected transient String _phone2InitVal;
    protected transient boolean _phone2IsSet;
    protected transient String _emailInitVal;
    protected transient boolean _emailIsSet;
    protected transient Date _birthDateInitVal;
    protected transient boolean _birthDateIsSet;
    protected transient String _lastNameInitVal;
    protected transient boolean _lastNameIsSet;
    protected transient String _firstNameInitVal;
    protected transient boolean _firstNameIsSet;


					
    /**
     * Returns the initial value of the property 'phone'.
     */
    public String phoneInitVal() {
        String result;
        if (_phoneIsSet) {
            result = _phoneInitVal;
        } else {
            result = getPhone();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone'.
     */
    public boolean phoneIsDirty() {
        return !valuesAreEqual(phoneInitVal(), getPhone());
    }

    /**
     * Returns true if the setter method was called for the property 'phone'.
     */
    public boolean phoneIsSet() {
        return _phoneIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone2'.
     */
    public String phone2InitVal() {
        String result;
        if (_phone2IsSet) {
            result = _phone2InitVal;
        } else {
            result = getPhone2();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone2'.
     */
    public boolean phone2IsDirty() {
        return !valuesAreEqual(phone2InitVal(), getPhone2());
    }

    /**
     * Returns true if the setter method was called for the property 'phone2'.
     */
    public boolean phone2IsSet() {
        return _phone2IsSet;
    }
	
    /**
     * Returns the initial value of the property 'birth date'.
     */
    public Date birthDateInitVal() {
        Date result;
        if (_birthDateIsSet) {
            result = _birthDateInitVal;
        } else {
            result = getBirthDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birth date'.
     */
    public boolean birthDateIsDirty() {
        return !valuesAreEqual(birthDateInitVal(), getBirthDate());
    }

    /**
     * Returns true if the setter method was called for the property 'birth date'.
     */
    public boolean birthDateIsSet() {
        return _birthDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last name'.
     */
    public String lastNameInitVal() {
        String result;
        if (_lastNameIsSet) {
            result = _lastNameInitVal;
        } else {
            result = getLastName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last name'.
     */
    public boolean lastNameIsDirty() {
        return !valuesAreEqual(lastNameInitVal(), getLastName());
    }

    /**
     * Returns true if the setter method was called for the property 'last name'.
     */
    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'first name'.
     */
    public String firstNameInitVal() {
        String result;
        if (_firstNameIsSet) {
            result = _firstNameInitVal;
        } else {
            result = getFirstName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first name'.
     */
    public boolean firstNameIsDirty() {
        return !valuesAreEqual(firstNameInitVal(), getFirstName());
    }

    /**
     * Returns true if the setter method was called for the property 'first name'.
     */
    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }
		
}
