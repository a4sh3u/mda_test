package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * Implements DSL specific Additional Data structure
 *
 * @author Kamil
 * @since 07.09.2016.
 */
public class DslAdditionalData extends AbstractAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_DSL;
    }
}