package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBucketWildcardExclusionRule;


/**
 * The domain object that represents 'BrokerageBucketWildcardExclusionRules'.
 *
 * @author generator
 */
public interface BrokerageBucketWildcardExclusionRuleEntityInterface {

    /**
     * Setter for the property 'brokerage bucket configuration wildcard bank'.
     *
     * 
     *
     */
    void setBrokerageBucketConfigurationWildcardBank(de.smava.webapp.brokerage.domain.BrokerageBucketConfigurationWildcardBank brokerageBucketConfigurationWildcardBank);

    /**
     * Returns the property 'brokerage bucket configuration wildcard bank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBucketConfigurationWildcardBank getBrokerageBucketConfigurationWildcardBank();
    /**
     * Setter for the property 'exclusion rule'.
     *
     * 
     *
     */
    void setExclusionRule(String exclusionRule);

    /**
     * Returns the property 'exclusion rule'.
     *
     * 
     *
     */
    String getExclusionRule();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBucketWildcardExclusionRule asBrokerageBucketWildcardExclusionRule();
}
