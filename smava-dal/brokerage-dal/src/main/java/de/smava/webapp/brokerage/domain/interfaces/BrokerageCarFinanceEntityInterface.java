package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageCarFinance;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageCarFinances'.
 *
 * @author generator
 */
public interface BrokerageCarFinanceEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'vehicle type'.
     *
     * 
     *
     */
    void setVehicleType(de.smava.webapp.brokerage.domain.VehicleType vehicleType);

    /**
     * Returns the property 'vehicle type'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.VehicleType getVehicleType();
    /**
     * Setter for the property 'vehicle brand'.
     *
     * 
     *
     */
    void setVehicleBrand(String vehicleBrand);

    /**
     * Returns the property 'vehicle brand'.
     *
     * 
     *
     */
    String getVehicleBrand();
    /**
     * Setter for the property 'vehicle model'.
     *
     * 
     *
     */
    void setVehicleModel(String vehicleModel);

    /**
     * Returns the property 'vehicle model'.
     *
     * 
     *
     */
    String getVehicleModel();
    /**
     * Setter for the property 'vehicle power kw'.
     *
     * 
     *
     */
    void setVehiclePowerKw(Integer vehiclePowerKw);

    /**
     * Returns the property 'vehicle power kw'.
     *
     * 
     *
     */
    Integer getVehiclePowerKw();
    /**
     * Setter for the property 'vehicle registration year'.
     *
     * 
     *
     */
    void setVehicleRegistrationYear(Date vehicleRegistrationYear);

    /**
     * Returns the property 'vehicle registration year'.
     *
     * 
     *
     */
    Date getVehicleRegistrationYear();
    /**
     * Setter for the property 'vehicle km'.
     *
     * 
     *
     */
    void setVehicleKm(Integer vehicleKm);

    /**
     * Returns the property 'vehicle km'.
     *
     * 
     *
     */
    Integer getVehicleKm();
    /**
     * Setter for the property 'vehicle price'.
     *
     * 
     *
     */
    void setVehiclePrice(Double vehiclePrice);

    /**
     * Returns the property 'vehicle price'.
     *
     * 
     *
     */
    Double getVehiclePrice();
    /**
     * Setter for the property 'initial payment'.
     *
     * 
     *
     */
    void setInitialPayment(Double initialPayment);

    /**
     * Returns the property 'initial payment'.
     *
     * 
     *
     */
    Double getInitialPayment();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageCarFinance asBrokerageCarFinance();
}
