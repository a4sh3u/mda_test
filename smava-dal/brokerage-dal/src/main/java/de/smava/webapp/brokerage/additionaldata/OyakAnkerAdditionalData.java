package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author KF
 * @since 13.06.2017
 */
public class OyakAnkerAdditionalData extends AbstractAdditionalData {
    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_OYAKANKER;
    }
}
