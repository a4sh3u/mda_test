package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.*;

import java.util.Set;


/**
 * The domain object that represents 'BrokerageBanks'.
 *
 * @author generator
 */
public interface BrokerageBankEntityInterface {

    /**
     * Setter for the property 'marketing partner id'.
     *
     * 
     *
     */
    void setMarketingPartnerId(Long marketingPartnerId);

    /**
     * Returns the property 'marketing partner id'.
     *
     * 
     *
     */
    Long getMarketingPartnerId();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'valid'.
     *
     * 
     *
     */
    void setValid(boolean valid);

    /**
     * Returns the property 'valid'.
     *
     * 
     *
     */
    boolean getValid();
    /**
     * Setter for the property 'comparison only'.
     *
     * 
     *
     */
    void setComparisonOnly(boolean comparisonOnly);

    /**
     * Returns the property 'comparison only'.
     *
     * 
     *
     */
    boolean getComparisonOnly();
    /**
     * Setter for the property 'send in offset'.
     *
     * 
     *
     */
    void setSendInOffset(Integer sendInOffset);

    /**
     * Returns the property 'send in offset'.
     *
     * 
     *
     */
    Integer getSendInOffset();
    /**
     * Setter for the property 'identification company id'.
     *
     * 
     *
     */
    void setIdentificationCompanyId(String identificationCompanyId);

    /**
     * Returns the property 'identification company id'.
     *
     * 
     *
     */
    String getIdentificationCompanyId();
    /**
     * Setter for the property 'identification expiration date in days'.
     *
     * 
     *
     */
    void setIdentificationExpirationDateInDays(Integer identificationExpirationDateInDays);

    /**
     * Returns the property 'identification expiration date in days'.
     *
     * 
     *
     */
    Integer getIdentificationExpirationDateInDays();
    /**
     * Setter for the property 'filter'.
     *
     * 
     *
     */
    void setFilter(Set<BrokerageBankFilter> filter);

    /**
     * Returns the property 'filter'.
     *
     * 
     *
     */
    Set<BrokerageBankFilter> getFilter();
    /**
     * Setter for the property 'mappings'.
     *
     * 
     *
     */
    void setMappings(Set<BrokerageMapping> mappings);

    /**
     * Returns the property 'mappings'.
     *
     * 
     *
     */
    Set<BrokerageMapping> getMappings();
    /**
     * Setter for the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    void setBrokerageBucketExclusionRules(Set<BrokerageBucketExclusionRule> brokerageBucketExclusionRules);

    /**
     * Returns the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    Set<BrokerageBucketExclusionRule> getBrokerageBucketExclusionRules();

    /**
     * Setter for the property 'bucket type'.
     *
     * 
     *
     */
    void setBucketType(BucketType bucketType);

    /**
     * Returns the property 'bucket type'.
     *
     * 
     *
     */
    BucketType getBucketType();

    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBank asBrokerageBank();
}
