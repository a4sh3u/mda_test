package de.smava.webapp.brokerage.additionaldata;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Mateusz Zyla
 * @since 06.04.2018
 */
public class BarclaysAdditionalData extends AbstractAdditionalData {

    @JsonProperty
    private String lastPushStatus;

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_BARCLAYS;
    }

    /**
     * Get lastPushStatus
     * @return
     */
    public String getLastPushStatus() {
        return lastPushStatus;
    }

    /**
     * Set lastPushStatus
     * @param lastPushStatus
     */
    public void setLastPushStatus(String lastPushStatus) {
        this.lastPushStatus = lastPushStatus;
    }
}
