package de.smava.webapp.brokerage.domain;

/**
 * Created by aherr on 17.12.14.
 */
public enum FilterType {
    INCLUDE, EXCLUDE;
}
