package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author kTomczyk
 * @since 12.10.2016.
 */
public class CarCreditAdditionalData extends AbstractAdditionalData {

    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_CARCREDIT;
    }
}
