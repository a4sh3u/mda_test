package de.smava.webapp.brokerage.domain;

/**
 * Filter reasons for brokerage applications we don't want to send
 *
 * @author dkeller
 */
public enum FilterReasonCode {
    /**
     * account has a placement that should not be brokered.
     */
    NO_BROKERAGE_PLACEMENT,
    /**
     * something with the borrowers employment doesn't match
     */
    FORBIDDEN_EMPLOYMENT,
    /**
     * borrower is employed in a sector the bank doesn't support
     */
    FORBIDDEN_SECTOR,
    /**
     * the borrowers nationality is not allowed by the bank
     */
    FORBIDDEN_CITIZENSHIP,
    /**
     * amount is smaller than the minimum amount for the bank
     */
    AMOUNT_TOO_LOW,
    /**
     * amount is larger than the maximum amount for the bank
     */
    AMOUNT_TOO_HIGH,
    /**
     * the borrower's the income is too low
     */
    INCOME_TOO_LOW,
    /**
     * the borrower's the income is too high
     */
    INCOME_TOO_HIGH,
    /**
     * the request's combination duration/amount is not supported by the bank
     */
    FORBIDDEN_COMBINATION_DURATION_AMOUNT,
    /**
     * the request's combination employment/amount is not supported by the bank
     */
    FORBIDDEN_COMBINATION_EMPLOYMENT_AMOUNT,
    /**
     * borrower is temporary employed
     */
    TEMPORARY_EMPLOYED,
    /**
     * the borrower is not employed for a minimum amount of time at his employer
     */
    WORKING_PERIOD_TOO_SHORT,
    /**
     * the type RDI: full is not supported by the bank
     */
    RDI_FULL_NOT_ALLOWED,
    /**
     * the type RDI: double is not supported by the bank
     */
    RDI_DOUBLE_NOT_ALLOWED,
    /**
     * the type RDI: normal is not supported by the bank
     */
    RDI_NORMAL_NOT_ALLOWED,
    /**
     * the SCHUFA rating is not sufficient
     */
    FORBIDDEN_SCHUFA_RATING,
    /**
     * the credit calculation for this borrower is not high enough
     */
    INSUFFICIENT_CREDIT_RATE,
    /**
     * the consolation for the debts is too expensive
     */
    CONSOLIDATION_TOO_HIGH,

    /**
     * the borrower has previous defaulted contracts at smava
     */
    DEFAULTED_CONTRACTS,

    /**
     * the first borrower is too young for this bank
     */
    FIRST_PERSON_TOO_YOUNG,
    /**
     * second borrower is too young for this bank
     */
    SECOND_PERSON_TOO_YOUNG,

    /**
     * the first borrower is too old for this bank
     */
    FIRST_PERSON_TOO_OLD,

    /**
     * the co borrower is too old
     */
    SECOND_PERSON_TOO_OLD,

    /**
     * the borrower has a valid credit check at smava
     */
    SMAVA_PRIVAT_CREDIT_CHECK_STILL_VALID,
    /**
     * the borrower has an active project on the smava marketplace
     */
    SMAVA_PRIVAT_CURRENTLY_ON_MARKETPLACE,

    /**
     * ???
     */
    OPEN_DEBTS_NOT_ALLOWED,
    /**
     * ???
     */
    DEBTS_REQUESTED_FOR_CONSOLIDATION_FORBIDDEN,
    /**
     * there is currently an offer in creation.(smava privat only)
     */
    OFFER_CURRENTLY_IN_CREATION,
    /**
     * the category is not allowed for this bank
     */
    FORBIDDEN_CATEGORY,
    /**
     * the amount of the consolidation is higher that the maximum amount offered by this bank
     */
    REQUESTED_CONSOLIDATION_AMOUNT_BIGGER_THAN_BANK_MAX_AMOUNT,
    /**
     * Smava is waiting for the scoring for this borrower. the legendary SCHUFA after treatment
     */
    WAITING_FOR_SCORE,
    /**
     * the borrower is temporary employed but no end date is provided
     */
    TEMPORARY_EMPLOYMENT_END_DATE_MISSING,
    /**
     * the borrower is temporary employed for a too long periode of time
     */
    TEMPORARY_EMPLOYMENT_TOO_LONG,
    /**
     * the borrower is temporary employed for a too short period of time
     */
    TEMPORARY_EMPLOYMENT_TOO_SHORT,
    /**
     * the co-borrower is temporary employed but no end date is provided
     */
    CO_BORROWER_TEMPORARY_EMPLOYMENT_END_DATE_MISSING,
    /**
     * the co-borrower is temporary employed for a too long periode of time
     */
    CO_BORROWER_TEMPORARY_EMPLOYMENT_TOO_LONG,
    /**
     * a co-borrower is required at this bank
     */
    SECOND_PERSON_NEEDED,
    /**
     * the duration of the loan is too long for a temporary employed borrower
     */
    LOAN_DURATION_TOO_LONG_FOR_TEMPORARY_EMPLOYMENT,
    /**
     * possibility to deactivate a certain bank for a limited amount of time
     */
    TEMPORARILY_DEACTIVATED,
    /**
     * Filtered due an entry on the placement/bank blacklist
     */
    AFFILIATE_FILTER,
    /**
     * The borrower has to may schufa credits
     */
    TOO_MANY_SCHUFA_CREDITS,
    /**
     * the duration is too long
     */
    DURATION_TOO_LONG,
    /**
     * the duration is too short
     */
    DURATION_TOO_SHORT,
    /**
     * the arvato score failed
     */
    ARVATO_SCORE_FAILED,
    /**
     * the household check failed
     */
    HOUSEHOLD_CHECK_FAILED,
    /**
     * the indebtness check failed
     */
    INDEBTEDNESS_CHECK_FAILED,

    /**
     * main bank account has no IBAN defined
     */
    NO_IBAN_MAIN_ACCOUNT,

    /**
     * main bank account has no BIC defined
     */
    NO_BIC_MAIN_ACCOUNT,

    /**
     * bank account different then main has no IBAN defined
     */
    NO_IBAN_CONSOLIDATED_ACCOUNT,

    /**
     * bank account different then main has no BIC defined
     */
    NO_BIC_CONSOLIDATED_ACCOUNT,

    /**
     * the iban not starts with "DE"
     */
    IBAN_NOT_GERMAN,

    /**
     * the car (for which borrower whats a load) is too old
     */
    CAR_TOO_OLD,

    /**
     * the data of an old application not mapped to new data yet
     */
    OLD_DATA_NOT_MAPPED,

    /**
     * the co-borrower nationality is not allowed by the bank
     */
    CO_BORROWER_FORBIDDEN_CITIZENSHIP,

    /**
     * something with the co-borrowe emplyoment doesn't match
     */
    CO_BORROWER_FORBIDDEN_EMPLOYMENT,

    /**
     * the borrowers householde state is not allowed by the bank
     */
    SHARED_HOUSEHOLD_FALSE,

    /**
     * the co-borrower is not employed for a minimum amount of time at his employer
     */
    CO_BORROWER_WORKING_PERIOD_TOO_SHORT,

    /**
     * Required applicant data is missing
     */
    APPLICANT_DATA_MISSING,
    /**
     * default filter reason for 1st applicant
     */
    UNDEFINED,

    /**
     * default filter reason for 2nd applicant
     */
    CO_BORROWER_UNDEFINED,

    /**
     * borrower is not married
     */
    NOT_MARRIED,

    /**
     * co-borrower is not married
     */
    CO_BORROWER_NOT_MARRIED,

    /**
     * borrower's country of residence is not allowed by the bank
     */
    FORBIDDEN_COUNTRY_OF_RESIDENCE,

    /**
     * co-borrower's country of residence is not allowed by the bank
     */
    CO_BORROWER_FORBIDDEN_COUNTRY_OF_RESIDENCE,

    /**
     * borrowers relationship is not sufficient for a shared loan application.
     */
    FORBIDDEN_RELATIONSHIP,

    /**
     * Amount from third party loans is too high.
     */
    THIRD_PARTY_LOAN_TOO_HIGH,

    /**
     * Residence permition is too short
     */
    RESIDENCE_PERMIT_TOO_SHORT,

    /**
     * Residence permition is too short for second applicant
     */
    CO_BORROWER_RESIDENCE_PERMIT_TOO_SHORT,

    /**
     * Too many third party loans for first applicant
     */
    TOO_MANY_THIRD_PARTY_LOANS,

    /**
     * Too many third party loans for second applicant
     */
    CO_BORROWER_TOO_MANY_THIRD_PARTY_LOANS,

    /**
     * Residence is forbidden for first applicant
     */
    FORBIDDEN_RESIDENCE,

    /**
     * Residence is forbidden for second applicant
     */
    CO_BORROWER_FORBIDDEN_RESIDENCE,

    /**
     * Income is too low for second applicant
     */
    CO_BORROWER_INCOME_TOO_LOW,

    /**
     * household calculation filter reason for too low income
     */
    FREE_HOUSEHOLD_INCOME_TOO_LOW,

    /**
     * household calculation filter reason for too high indebtedness
     */
    INDEBTEDNESS_TOO_HIGH,

    /**
     * mothly rate is too low for bank
     */
    MONTHLY_RATE_TOO_LOW,

    /**
     * Usage not permitted
     */
    LOAN_CATEGORY,

    /**
     * vehicle too old
     */
    VEHICLE_TOO_OLD,

    /**
     * 2nd applicant is forbidden
     */
    SECOND_APPLICANT_FORBIDDEN,

    /**
     * incorrect credit rating
     */
    RATING_TOO_BAD,

    /**
     * household calculation filter reason for too young applicant
     */
    APPLICANT_TOO_YOUNG,

    /**
     * household calculation filter reason for too old applicant
     */
    APPLICANT_TOO_OLD,

    /**
     * household calculation filter reason for applicant with wrong citizenship
     */
    CITIZENSHIP,

    /**
     * household calculation filter reason for applicant with not german bank account
     */
    BANK_ACCOUNT_NOT_GERMAN,

    /**
     * household calculation filter reason for applicant with incorrect residence address
     */
    RESIDENCE,

    /**
     * household calculation filter reason for applicant with incorrect employment
     */
    EMPLOYMENT,

    /**
     * household calculation filter reason when duration is longer then temporary income
     */
    DURATION_TOO_LONG_FOR_TEMPORARY_INCOME,

    /**
     * invalid BIC code
     */
    BIC_INVALID,

    /**
     * employment period too short
     */
    EMPLOYMENT_PERIOD_TOO_SHORT,

    /**
     * probation period
     */
    EMPLOYMENT_PROBATION_PERIOD,

    /**
     * temporary employed
     */
    EMPLOYMENT_TEMPORARY,

    /**
     * invalid IBAN code
     */
    IBAN_INVALID,

    /**
     * insufficient KDF
     */
    INSUFFICIENT_KDF,

    /**
     * not enough residence permit
     */
    RESIDENCE_PERMIT,

    /**
     * second applicant needed
     */
    SECOND_APPLICANT_NEEDED,

    /**
     * shared household loan reason
     */
    SHARED_LOAN_HOUSEHOLD,

    /**
     * shared relationship loan reason
     */
    SHARED_LOAN_RELATIONSHIP,

    /**
     * too many loans reason
     */
    TOO_MANY_LOANS,

    /**
     * approval probability too low
     */
    APPROVAL_PROBABILITY_TOO_LOW,

    /**
     * amount is to high for household
     */
    AMOUNT_TOO_HIGH_FOR_HOUSEHOLD,

    /**
     * no comparison permitted
     */
    NO_COMPARISON_PERMITTED,

    /**
     * request allowed only by advisor
     */
    REQUEST_ONLY_BY_ADVISOR,

    /**
     * no RDI requested
     */
    NO_RDI_REQUESTED,

    /**
     * email extension is not allowed
     */
    EMAIL_NOT_ALLOWED,

    /**
     * reference bank account missing
     */
    MISSING_BANK_ACCOUNT,

    /**
     * returning customer with multiple accounts
     */
    RETURNING_CUSTOMER_WITH_MULTIPLE_ACCOUNTS,

    /**
     * special campaign
     */
    SPECIAL_CAMPAIGN,

    /**
     * no employer specified
     */
    APPLICANT_EMPLOYMENT_OTHER,
    
    /**
     * customer number based traffic limit exceeded
     */
    CUSTOMER_TRAFFIC_CONTROL,

    /**
     * Too Low payout
     */
    CONVERSION_TOO_LOW,
    
    /**
     * too low amount for conversion
     */
    CONVERSION_TOO_LOW_FOR_AMOUNT,
    
    /**
     * too low amount and duration for conversion
     */
    CONVERSION_TOO_LOW_FOR_AMOUNT_AND_DURATION,
    
    /**
     * forbidden category for conversion
     */
    CONVERSION_TOO_LOW_FOR_LOAN_CATEGORY,
    
    /**
     * Too low payout ratio - replacement of third-party loans recommended
     */
    CONVERSION_TOO_LOW_CONSOLIDATION_RECOMMENDED
}
