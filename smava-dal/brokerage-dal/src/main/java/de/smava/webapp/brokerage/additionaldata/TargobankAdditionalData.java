package de.smava.webapp.brokerage.additionaldata;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.smava.webapp.brokerage.domain.BrokerageBank;
import org.apache.commons.lang.StringUtils;

/**
 * Targobank additional data
 *
 * @author Adam Tomecki
 * @since 27.12.2016
 */
public class TargobankAdditionalData extends AbstractAdditionalData {

    private static final String OCI_MAX_LOAN_LABEL = "ociMaxLoan: ";
    private static final String OCI_STATUS_LABEL = "ociStatus: ";

    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_TARGOBANK;
    }

    /**
     * Retrieves max OCI loan from the information
     *
     * @return max OCI loan {@link String}
     */
    @JsonIgnore
    public String getOciMaxLoan() {
        return getInformationByLabel(OCI_MAX_LOAN_LABEL);
    }

    /**
     * Retrieves OCI status from the information
     *
     * @return OCI status {@link String}
     */
    @JsonIgnore
    public String getOciStatus() {
        return getInformationByLabel(OCI_STATUS_LABEL);
    }

    /**
     * Retrieves piece of information from the list by given prefix label
     *
     * @param label prefix label {@link String}
     * @return piece of information {@link String}
     */
    private String getInformationByLabel(final String label) {
        for (String info : getInformation()) {

            if (info != null && info.startsWith(label)) {

                String result = StringUtils.substringAfter(info, label);

                if (StringUtils.isNotBlank(result)) {
                    return result;
                }
            }
        }

        return null;
    }
}
