//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application filter reason)}
import de.smava.webapp.brokerage.domain.history.BrokerageApplicationFilterReasonHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationFilterReasons'.
 *
 * @author generator
 */
public class BrokerageApplicationFilterReason extends BrokerageApplicationFilterReasonHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application filter reason)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected FilterReasonCode _reasonCode;
        protected String _reasonDescription;
        
                                    
    /**
     * Setter for the property 'reason code'.
     */
    public void setReasonCode(FilterReasonCode reasonCode) {
        _reasonCode = reasonCode;
    }
            
    /**
     * Returns the property 'reason code'.
     */
    public FilterReasonCode getReasonCode() {
        return _reasonCode;
    }
                                    /**
     * Setter for the property 'reason description'.
     */
    public void setReasonDescription(String reasonDescription) {
        if (!_reasonDescriptionIsSet) {
            _reasonDescriptionIsSet = true;
            _reasonDescriptionInitVal = getReasonDescription();
        }
        registerChange("reason description", _reasonDescriptionInitVal, reasonDescription);
        _reasonDescription = reasonDescription;
    }
                        
    /**
     * Returns the property 'reason description'.
     */
    public String getReasonDescription() {
        return _reasonDescription;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplicationFilterReason.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _reasonDescription=").append(_reasonDescription);
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplicationFilterReason.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplicationFilterReason asBrokerageApplicationFilterReason() {
        return this;
    }
}
