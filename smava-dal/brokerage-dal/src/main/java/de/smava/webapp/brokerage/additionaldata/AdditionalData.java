package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.additionaldata.property.*;

import java.util.List;
import java.util.Map;

/**
 * Defines Additional Data structure
 *
 * @author Adam
 * @since 25.04.2016.
 */
public interface AdditionalData {

    /**
     * Get Bank Name for which objects is dedicated
     *
     * @return {@link String} as bank name
     */
    String retrieveBankName();

    /**
     * Get in JSON format
     *
     * @return {@link String} data formatted as JSON
     */
    String asJson();

    /**
     * @return
     */
    Map<String, Object> read();

    /**
     * Get list of References
     *
     * @return List of {@link Reference}
     */
    List<Reference> getReferences();

    /**
     * Get Missing Documents object
     *
     * @return {@link MissingDocuments}
     */
    MissingDocuments getMissingDocuments();

    void setMissingDocuments(MissingDocuments missingDocuments);

    /**
     * Get Information data
     *
     * @return List of {@link String}
     */
    List<String> getInformation();

    /**
     * Get VideoIdent Information data
     *
     * @return {@link String}
     */
    String getVideoIdentInformation();

    /**
     * Get Scoring data
     *
     * @return {@link Scoring}
     */
    Scoring getScoring();

    /**
     * Set Scorring data
     *
     * @param scoring
     */
    void setScoring(Scoring scoring);

    /**
     * Gets value
     *
     * @return {@link Boolean}
     */
    public Boolean isDigitalLoan();

    /**
     * Sets value
     *
     * @param digitalLoan
     */
    public void setDigitalLoan(Boolean digitalLoan);

    /**
     * @return List of {@link SchufaInformation}
     */
    List<SchufaInformation> getSchufaInformation();

    /**
     * @return {@link DocumentInformation}
     */
    DocumentInformation getDocumentInformation();

    /**
     * Set DocumentInformation data
     *
     * @param documentInformation
     */
    void setDocumentInformation(DocumentInformation documentInformation);

    /**
     * Get list of AlternativeOffers
     *
     * @return List of {@link AlternativeOffer}
     */
    List<AlternativeOffer> getAlternativeOffers();

    /**
     * Get map technical object for BA
     *
     * @return Maps
     */
    Map<String, Object> getTechnicalData();

    /**
     * Gel ist of Messages
     *
     * @return List of {@link String}
     */
    List<String> getMessages();
}
