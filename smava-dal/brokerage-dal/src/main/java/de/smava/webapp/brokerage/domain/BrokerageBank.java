//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank)}

import de.smava.webapp.brokerage.domain.history.BrokerageBankHistory;

import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBanks'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBank extends BrokerageBankHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bank)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _marketingPartnerId;
        protected String _name;
        protected boolean _valid;
        protected boolean _comparisonOnly;
        protected Integer _sendInOffset;
        protected String _identificationCompanyId;
        protected Integer _identificationExpirationDateInDays;
        protected Set<BrokerageBankFilter> _filter;
        protected Set<BrokerageMapping> _mappings;
        protected Set<BrokerageBucketExclusionRule> _brokerageBucketExclusionRules;
        protected de.smava.webapp.brokerage.domain.BucketType _bucketType;
        
                                    
    /**
     * Setter for the property 'marketing partner id'.
     *
     * 
     *
     */
    public void setMarketingPartnerId(Long marketingPartnerId) {
        _marketingPartnerId = marketingPartnerId;
    }
            
    /**
     * Returns the property 'marketing partner id'.
     *
     * 
     *
     */
    public Long getMarketingPartnerId() {
        return _marketingPartnerId;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'valid'.
     *
     * 
     *
     */
    public void setValid(boolean valid) {
        if (!_validIsSet) {
            _validIsSet = true;
            _validInitVal = getValid();
        }
        registerChange("valid", _validInitVal, valid);
        _valid = valid;
    }
                        
    /**
     * Returns the property 'valid'.
     *
     * 
     *
     */
    public boolean getValid() {
        return _valid;
    }
                                    /**
     * Setter for the property 'comparison only'.
     *
     * 
     *
     */
    public void setComparisonOnly(boolean comparisonOnly) {
        if (!_comparisonOnlyIsSet) {
            _comparisonOnlyIsSet = true;
            _comparisonOnlyInitVal = getComparisonOnly();
        }
        registerChange("comparison only", _comparisonOnlyInitVal, comparisonOnly);
        _comparisonOnly = comparisonOnly;
    }
                        
    /**
     * Returns the property 'comparison only'.
     *
     * 
     *
     */
    public boolean getComparisonOnly() {
        return _comparisonOnly;
    }
                                            
    /**
     * Setter for the property 'send in offset'.
     *
     * 
     *
     */
    public void setSendInOffset(Integer sendInOffset) {
        _sendInOffset = sendInOffset;
    }
            
    /**
     * Returns the property 'send in offset'.
     *
     * 
     *
     */
    public Integer getSendInOffset() {
        return _sendInOffset;
    }
                                    /**
     * Setter for the property 'identification company id'.
     *
     * 
     *
     */
    public void setIdentificationCompanyId(String identificationCompanyId) {
        if (!_identificationCompanyIdIsSet) {
            _identificationCompanyIdIsSet = true;
            _identificationCompanyIdInitVal = getIdentificationCompanyId();
        }
        registerChange("identification company id", _identificationCompanyIdInitVal, identificationCompanyId);
        _identificationCompanyId = identificationCompanyId;
    }
                        
    /**
     * Returns the property 'identification company id'.
     *
     * 
     *
     */
    public String getIdentificationCompanyId() {
        return _identificationCompanyId;
    }
                                            
    /**
     * Setter for the property 'identification expiration date in days'.
     *
     * 
     *
     */
    public void setIdentificationExpirationDateInDays(Integer identificationExpirationDateInDays) {
        _identificationExpirationDateInDays = identificationExpirationDateInDays;
    }
            
    /**
     * Returns the property 'identification expiration date in days'.
     *
     * 
     *
     */
    public Integer getIdentificationExpirationDateInDays() {
        return _identificationExpirationDateInDays;
    }
                                            
    /**
     * Setter for the property 'filter'.
     *
     * 
     *
     */
    public void setFilter(Set<BrokerageBankFilter> filter) {
        _filter = filter;
    }
            
    /**
     * Returns the property 'filter'.
     *
     * 
     *
     */
    public Set<BrokerageBankFilter> getFilter() {
        return _filter;
    }
                                            
    /**
     * Setter for the property 'mappings'.
     *
     * 
     *
     */
    public void setMappings(Set<BrokerageMapping> mappings) {
        _mappings = mappings;
    }
            
    /**
     * Returns the property 'mappings'.
     *
     * 
     *
     */
    public Set<BrokerageMapping> getMappings() {
        return _mappings;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    public void setBrokerageBucketExclusionRules(Set<BrokerageBucketExclusionRule> brokerageBucketExclusionRules) {
        _brokerageBucketExclusionRules = brokerageBucketExclusionRules;
    }
            
    /**
     * Returns the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    public Set<BrokerageBucketExclusionRule> getBrokerageBucketExclusionRules() {
        return _brokerageBucketExclusionRules;
    }
                                            
    /**
     * Setter for the property 'bucket type'.
     *
     * 
     *
     */
    public void setBucketType(de.smava.webapp.brokerage.domain.BucketType bucketType) {
        _bucketType = bucketType;
    }
            
    /**
     * Returns the property 'bucket type'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.BucketType getBucketType() {
        return _bucketType;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBank.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _valid=").append(_valid);
            builder.append("\n    _comparisonOnly=").append(_comparisonOnly);
            builder.append("\n    _identificationCompanyId=").append(_identificationCompanyId);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBank.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBank asBrokerageBank() {
        return this;
    }
}
