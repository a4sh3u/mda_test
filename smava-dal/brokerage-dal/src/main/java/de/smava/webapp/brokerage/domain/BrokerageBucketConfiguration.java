//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket configuration)}
import de.smava.webapp.brokerage.domain.history.BrokerageBucketConfigurationHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBucketConfigurations'.
 *
 * A brokerage bucket configuration is a virtual collection of condition base on which brokerage application in linked to specific bucket.
 *
 * @author generator
 */
public class BrokerageBucketConfiguration extends BrokerageBucketConfigurationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bucket configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected RequestedLoanType _requestedLoanType;
        protected Set<BrokerageBucketConfigurationWildcardBank> _wildcardBrokerageBanks;
        protected Long _marketingPlacementId;
        protected Integer _bucketNumber;
        protected Integer _numberOfInterestRate;
        protected Integer _numberOfPayout;
        protected Double _payoutProbability;
        protected Long _delay;
        protected Integer _sufficientApplicationsNumber;
        protected Date _expirationDate;
        protected Set<BrokerageBucketBrokerageApplication> _brokerageBucketBrokerageApplications;
        protected Set<BrokerageBucketExclusionRule> _brokerageBucketExclusionRules;
        protected de.smava.webapp.brokerage.domain.BucketType _bucketType;
        protected de.smava.webapp.brokerage.domain.FollowingBucketType _followingBucketType;
                                    
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    public void setRequestedLoanType(RequestedLoanType requestedLoanType) {
        _requestedLoanType = requestedLoanType;
    }
            
    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    public RequestedLoanType getRequestedLoanType() {
        return _requestedLoanType;
    }
                                            
    /**
     * Setter for the property 'wildcard brokerage banks'.
     *
     * Brokerage bank which is included in the bucket regardless of other conditions.
     *
     */
    public void setWildcardBrokerageBanks(Set<BrokerageBucketConfigurationWildcardBank> wildcardBrokerageBanks) {
        _wildcardBrokerageBanks = wildcardBrokerageBanks;
    }
            
    /**
     * Returns the property 'wildcard brokerage banks'.
     *
     * Brokerage bank which is included in the bucket regardless of other conditions.
     *
     */
    public Set<BrokerageBucketConfigurationWildcardBank> getWildcardBrokerageBanks() {
        return _wildcardBrokerageBanks;
    }
                                            
    /**
     * Setter for the property 'marketing placement id'.
     *
     * Marketing placement ID - it is not directly related with an object in purpose (to avoid dependence).
     *
     */
    public void setMarketingPlacementId(Long marketingPlacementId) {
        _marketingPlacementId = marketingPlacementId;
    }
            
    /**
     * Returns the property 'marketing placement id'.
     *
     * Marketing placement ID - it is not directly related with an object in purpose (to avoid dependence).
     *
     */
    public Long getMarketingPlacementId() {
        return _marketingPlacementId;
    }
                                            
    /**
     * Setter for the property 'bucket number'.
     *
     * 
     *
     */
    public void setBucketNumber(Integer bucketNumber) {
        _bucketNumber = bucketNumber;
    }
            
    /**
     * Returns the property 'bucket number'.
     *
     * 
     *
     */
    public Integer getBucketNumber() {
        return _bucketNumber;
    }
                                            
    /**
     * Setter for the property 'number of interest rate'.
     *
     * Number of brokerage applications with best interest rates which can be processed under this bucket.
     *
     */
    public void setNumberOfInterestRate(Integer numberOfInterestRate) {
        _numberOfInterestRate = numberOfInterestRate;
    }
            
    /**
     * Returns the property 'number of interest rate'.
     *
     * Number of brokerage applications with best interest rates which can be processed under this bucket.
     *
     */
    public Integer getNumberOfInterestRate() {
        return _numberOfInterestRate;
    }
                                            
    /**
     * Setter for the property 'number of payout'.
     *
     * Number of brokerage applications with highest payout probability which can be processed under this bucket.
     *
     */
    public void setNumberOfPayout(Integer numberOfPayout) {
        _numberOfPayout = numberOfPayout;
    }
            
    /**
     * Returns the property 'number of payout'.
     *
     * Number of brokerage applications with highest payout probability which can be processed under this bucket.
     *
     */
    public Integer getNumberOfPayout() {
        return _numberOfPayout;
    }
                                            
    /**
     * Setter for the property 'payout probability'.
     *
     * Minimal payout probability which brokerage application need to reach to become a part of certain bucket.
     *
     */
    public void setPayoutProbability(Double payoutProbability) {
        _payoutProbability = payoutProbability;
    }
            
    /**
     * Returns the property 'payout probability'.
     *
     * Minimal payout probability which brokerage application need to reach to become a part of certain bucket.
     *
     */
    public Double getPayoutProbability() {
        return _payoutProbability;
    }
                                            
    /**
     * Setter for the property 'delay'.
     *
     * Time in seconds after which next bucket should start processing.
     *
     */
    public void setDelay(Long delay) {
        _delay = delay;
    }
            
    /**
     * Returns the property 'delay'.
     *
     * Time in seconds after which next bucket should start processing.
     *
     */
    public Long getDelay() {
        return _delay;
    }
                                            
    /**
     * Setter for the property 'sufficient applications number'.
     *
     * Common value for all buckets - specify number of brokerage applications with status applied which have to be reached to stop processing (stop running another bucket).
     *
     */
    public void setSufficientApplicationsNumber(Integer sufficientApplicationsNumber) {
        _sufficientApplicationsNumber = sufficientApplicationsNumber;
    }
            
    /**
     * Returns the property 'sufficient applications number'.
     *
     * Common value for all buckets - specify number of brokerage applications with status applied which have to be reached to stop processing (stop running another bucket).
     *
     */
    public Integer getSufficientApplicationsNumber() {
        return _sufficientApplicationsNumber;
    }
                                    /**
     * Setter for the property 'expiration date'.
     *
     * Configuration with expiration date are not considered anymore (historical usage only).
     *
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     *
     * Configuration with expiration date are not considered anymore (historical usage only).
     *
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    public void setBrokerageBucketBrokerageApplications(Set<BrokerageBucketBrokerageApplication> brokerageBucketBrokerageApplications) {
        _brokerageBucketBrokerageApplications = brokerageBucketBrokerageApplications;
    }
            
    /**
     * Returns the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    public Set<BrokerageBucketBrokerageApplication> getBrokerageBucketBrokerageApplications() {
        return _brokerageBucketBrokerageApplications;
    }
                                            
    /**
     * Setter for the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    public void setBrokerageBucketExclusionRules(Set<BrokerageBucketExclusionRule> brokerageBucketExclusionRules) {
        _brokerageBucketExclusionRules = brokerageBucketExclusionRules;
    }
            
    /**
     * Returns the property 'brokerage bucket exclusion rules'.
     *
     * 
     *
     */
    public Set<BrokerageBucketExclusionRule> getBrokerageBucketExclusionRules() {
        return _brokerageBucketExclusionRules;
    }
                                            
    /**
     * Setter for the property 'bucket type'.
     *
     * 
     *
     */
    public void setBucketType(de.smava.webapp.brokerage.domain.BucketType bucketType) {
        _bucketType = bucketType;
    }
            
    /**
     * Returns the property 'bucket type'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.BucketType getBucketType() {
        return _bucketType;
    }

    /**
     * Setter for the property 'Brokerage Bucket Following Configuration'.
     *
     * @return {@link Long}
     */
    public de.smava.webapp.brokerage.domain.FollowingBucketType getFollowingBucketType() {
        return _followingBucketType;
    }

    /**
     * Returns the property 'Brokerage Bucket Following Configuration'.
     *
     * @param followingBucketType {@link de.smava.webapp.brokerage.domain.FollowingBucketType}
     */
    public void setFollowingBucketType(de.smava.webapp.brokerage.domain.FollowingBucketType followingBucketType) {
        this._followingBucketType = followingBucketType;
    }

    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBucketConfiguration.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageBucketConfiguration.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBucketConfiguration asBrokerageBucketConfiguration() {
        return this;
    }
}
