package de.smava.webapp.brokerage.domain;

/**
 * @author Mikolaj Zymon
 * @since 30.05.2017
 */
public enum BucketType {
    STANDARD,
    SUBPRIME,
    FOLLOWING,
    INITIAL;

    /**
     * Retrieve bank type based on Bucket Type
     *
     * @return {@ling BucketType}
     */
    public BucketType retrieveBankType() {

        if(BucketType.INITIAL.equals(this)
                || BucketType.FOLLOWING.equals(this)) {

            return BucketType.STANDARD;
        }

        return this;
    }

}
