package de.smava.webapp.brokerage.loancheck.domain.interfaces;



import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequest;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequestData;

import java.util.Date;


/**
 * The domain object that represents 'LoanCheckRequestDatas'.
 *
 * @author generator
 */
public interface LoanCheckRequestDataEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'loan check request'.
     *
     * 
     *
     */
    void setLoanCheckRequest(LoanCheckRequest loanCheckRequest);

    /**
     * Returns the property 'loan check request'.
     *
     * 
     *
     */
    LoanCheckRequest getLoanCheckRequest();
    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    void setRequest(String request);

    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    String getRequest();
    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    void setResponse(String response);

    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    String getResponse();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(de.smava.webapp.brokerage.domain.BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageState getState();
    /**
     * Helper method to get reference of this object as model type.
     */
    LoanCheckRequestData asLoanCheckRequestData();
}
