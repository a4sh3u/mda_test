//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage payout probability prediction)}
import de.smava.webapp.brokerage.domain.history.BrokeragePayoutProbabilityPredictionHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokeragePayoutProbabilityPredictions'.
 *
 * A brokerage payout probability prediction is a virtual collection of uploaded BrokeragePayoutProbabilityPredictionData.
 *
 * @author generator
 */
public class BrokeragePayoutProbabilityPrediction extends BrokeragePayoutProbabilityPredictionHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage payout probability prediction)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected Date _creationDate;
        protected Date _activationDate;
        protected Set<BrokeragePayoutProbabilityPredictionData> _brokeragePayoutProbabilityPredictionDatas;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'activation date'.
     *
     * 
     *
     */
    public void setActivationDate(Date activationDate) {
        if (!_activationDateIsSet) {
            _activationDateIsSet = true;
            _activationDateInitVal = getActivationDate();
        }
        registerChange("activation date", _activationDateInitVal, activationDate);
        _activationDate = activationDate;
    }
                        
    /**
     * Returns the property 'activation date'.
     *
     * 
     *
     */
    public Date getActivationDate() {
        return _activationDate;
    }
                                            
    /**
     * Setter for the property 'brokerage payout probability prediction datas'.
     *
     * 
     *
     */
    public void setBrokeragePayoutProbabilityPredictionDatas(Set<BrokeragePayoutProbabilityPredictionData> brokeragePayoutProbabilityPredictionDatas) {
        _brokeragePayoutProbabilityPredictionDatas = brokeragePayoutProbabilityPredictionDatas;
    }
            
    /**
     * Returns the property 'brokerage payout probability prediction datas'.
     *
     * 
     *
     */
    public Set<BrokeragePayoutProbabilityPredictionData> getBrokeragePayoutProbabilityPredictionDatas() {
        return _brokeragePayoutProbabilityPredictionDatas;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokeragePayoutProbabilityPrediction.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokeragePayoutProbabilityPrediction.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokeragePayoutProbabilityPrediction asBrokeragePayoutProbabilityPrediction() {
        return this;
    }
}
