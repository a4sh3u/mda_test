package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.*;


/**
 * The domain object that represents 'BrokerageBucketBrokerageApplications'.
 *
 * @author generator
 */
public interface BrokerageBucketBrokerageApplicationEntityInterface {

    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'brokerage bucket'.
     *
     * 
     *
     */
    void setBrokerageBucket(BrokerageBucket brokerageBucket);

    /**
     * Returns the property 'brokerage bucket'.
     *
     * 
     *
     */
    BrokerageBucket getBrokerageBucket();
    /**
     * Setter for the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    void setBrokerageBucketConfiguration(BrokerageBucketConfiguration brokerageBucketConfiguration);

    /**
     * Returns the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    BrokerageBucketConfiguration getBrokerageBucketConfiguration();
    /**
     * Setter for the property 'brokerage interest rate prediction data'.
     *
     * 
     *
     */
    void setBrokerageInterestRatePredictionData(BrokerageInterestRatePredictionData brokerageInterestRatePredictionData);

    /**
     * Returns the property 'brokerage interest rate prediction data'.
     *
     * 
     *
     */
    BrokerageInterestRatePredictionData getBrokerageInterestRatePredictionData();
    /**
     * Setter for the property 'brokerage payout probability prediction data'.
     *
     * 
     *
     */
    void setBrokeragePayoutProbabilityPredictionData(BrokeragePayoutProbabilityPredictionData brokeragePayoutProbabilityPredictionData);

    /**
     * Returns the property 'brokerage payout probability prediction data'.
     *
     * 
     *
     */
    BrokeragePayoutProbabilityPredictionData getBrokeragePayoutProbabilityPredictionData();
    /**
     * Setter for the property 'brokerage interest rate details data'.
     *
     * 
     *
     */
    void setBrokerageInterestRateDetailsData(BrokerageInterestRateDetailsData brokerageInterestRateDetailsData);

    /**
     * Returns the property 'brokerage interest rate details data'.
     *
     * 
     *
     */
    BrokerageInterestRateDetailsData getBrokerageInterestRateDetailsData();
    /**
     * Setter for the property 'reason'.
     *
     * 
     *
     */
    void setReason(BrokerageBucketReason reason);

    /**
     * Returns the property 'reason'.
     *
     * 
     *
     */
    BrokerageBucketReason getReason();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBucketBrokerageApplication asBrokerageBucketBrokerageApplication();
}
