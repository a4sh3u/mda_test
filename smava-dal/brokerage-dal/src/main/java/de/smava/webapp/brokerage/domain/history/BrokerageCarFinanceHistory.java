package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageCarFinance;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageCarFinances'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageCarFinanceHistory extends AbstractBrokerageCarFinance {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _vehicleBrandInitVal;
    protected transient boolean _vehicleBrandIsSet;
    protected transient String _vehicleModelInitVal;
    protected transient boolean _vehicleModelIsSet;
    protected transient Date _vehicleRegistrationYearInitVal;
    protected transient boolean _vehicleRegistrationYearIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'vehicle brand'.
     */
    public String vehicleBrandInitVal() {
        String result;
        if (_vehicleBrandIsSet) {
            result = _vehicleBrandInitVal;
        } else {
            result = getVehicleBrand();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vehicle brand'.
     */
    public boolean vehicleBrandIsDirty() {
        return !valuesAreEqual(vehicleBrandInitVal(), getVehicleBrand());
    }

    /**
     * Returns true if the setter method was called for the property 'vehicle brand'.
     */
    public boolean vehicleBrandIsSet() {
        return _vehicleBrandIsSet;
    }
	
    /**
     * Returns the initial value of the property 'vehicle model'.
     */
    public String vehicleModelInitVal() {
        String result;
        if (_vehicleModelIsSet) {
            result = _vehicleModelInitVal;
        } else {
            result = getVehicleModel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vehicle model'.
     */
    public boolean vehicleModelIsDirty() {
        return !valuesAreEqual(vehicleModelInitVal(), getVehicleModel());
    }

    /**
     * Returns true if the setter method was called for the property 'vehicle model'.
     */
    public boolean vehicleModelIsSet() {
        return _vehicleModelIsSet;
    }
		
    /**
     * Returns the initial value of the property 'vehicle registration year'.
     */
    public Date vehicleRegistrationYearInitVal() {
        Date result;
        if (_vehicleRegistrationYearIsSet) {
            result = _vehicleRegistrationYearInitVal;
        } else {
            result = getVehicleRegistrationYear();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vehicle registration year'.
     */
    public boolean vehicleRegistrationYearIsDirty() {
        return !valuesAreEqual(vehicleRegistrationYearInitVal(), getVehicleRegistrationYear());
    }

    /**
     * Returns true if the setter method was called for the property 'vehicle registration year'.
     */
    public boolean vehicleRegistrationYearIsSet() {
        return _vehicleRegistrationYearIsSet;
    }
			
}
