package de.smava.webapp.brokerage.dao.additionalData;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;

@JsonFilter(EasyCreditAdditionalData.EASY_CREDIT_FILTER)
public class EasyCreditAdditionalData extends AbstractAdditionalData implements Serializable {

	private static final long serialVersionUID = -1850546546543567564L;

	public static final String EASY_CREDIT_FILTER = "easyCreditFilter";

	public static final String ADDITIONAL_INFO = "additionalInfo";

	@JsonProperty(ADDITIONAL_INFO)
	private String additionalInfo;

	@Override
	public String determineFilterName() {
		return EASY_CREDIT_FILTER;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	@Override
	public HashMap<String, Object> readAdditionalData() {
		return readAdditionalData(EASY_CREDIT_FILTER, ADDITIONAL_INFO);
	}
}
