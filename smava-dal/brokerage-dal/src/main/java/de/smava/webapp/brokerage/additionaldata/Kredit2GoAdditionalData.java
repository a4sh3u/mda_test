package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * Extension of AdditionalData class for Kredit2Go
 * 
 * @author KF
 * @since 26.08.2016.
 */
public class Kredit2GoAdditionalData extends AbstractAdditionalData {

    /**
     *  @see AbstractAdditionalData#retrieveBankName()
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_KREDIT2GO;
    }
}
