//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document placeholder)}
import de.smava.webapp.brokerage.domain.history.DocumentPlaceholderHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentPlaceholders'.
 *
 * 
 *
 * @author generator
 */
public class DocumentPlaceholder extends DocumentPlaceholderHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(document placeholder)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _key;
        protected String _value;
        protected Set<DocumentTypePlaceholder> _documentTypePlaceholders;
        
                            /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    public void setKey(String key) {
        if (!_keyIsSet) {
            _keyIsSet = true;
            _keyInitVal = getKey();
        }
        registerChange("key", _keyInitVal, key);
        _key = key;
    }
                        
    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    public String getKey() {
        return _key;
    }
                                    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    public String getValue() {
        return _value;
    }
                                            
    /**
     * Setter for the property 'document type placeholders'.
     *
     * 
     *
     */
    public void setDocumentTypePlaceholders(Set<DocumentTypePlaceholder> documentTypePlaceholders) {
        _documentTypePlaceholders = documentTypePlaceholders;
    }
            
    /**
     * Returns the property 'document type placeholders'.
     *
     * 
     *
     */
    public Set<DocumentTypePlaceholder> getDocumentTypePlaceholders() {
        return _documentTypePlaceholders;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentPlaceholder.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _key=").append(_key);
            builder.append("\n    _value=").append(_value);
            builder.append("\n}");
        } else {
            builder.append(DocumentPlaceholder.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DocumentPlaceholder asDocumentPlaceholder() {
        return this;
    }
}
