package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * Implements Wunschkredit specific Additional Data structure
 * <p/>
 * Created by Jakub on 09.09.2016.
 */
public class WunschkreditAdditionalData extends DslAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_WUNSCHKREDIT;
    }
}
