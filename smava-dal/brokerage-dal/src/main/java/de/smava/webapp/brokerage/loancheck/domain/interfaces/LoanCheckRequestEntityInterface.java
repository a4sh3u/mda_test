package de.smava.webapp.brokerage.loancheck.domain.interfaces;



import de.smava.webapp.brokerage.loancheck.domain.LoanCheck;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequest;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequestData;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'LoanCheckRequests'.
 *
 * @author generator
 */
public interface LoanCheckRequestEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(de.smava.webapp.brokerage.domain.BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageState getState();
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    void setEconomicalData(de.smava.webapp.account.domain.EconomicalData economicalData);

    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.EconomicalData getEconomicalData();
    /**
     * Setter for the property 'first person'.
     *
     * 
     *
     */
    void setFirstPerson(de.smava.webapp.account.domain.Person firstPerson);

    /**
     * Returns the property 'first person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getFirstPerson();
    /**
     * Setter for the property 'second person'.
     *
     * 
     *
     */
    void setSecondPerson(de.smava.webapp.account.domain.Person secondPerson);

    /**
     * Returns the property 'second person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getSecondPerson();
    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    void setRequestedAmount(double requestedAmount);

    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    double getRequestedAmount();
    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    void setRequestedDuration(int requestedDuration);

    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    int getRequestedDuration();
    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    void setRequestedRdiType(String requestedRdiType);

    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    String getRequestedRdiType();
    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    void setMonthlyRate(Double monthlyRate);

    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    Double getMonthlyRate();
    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    void setEffectiveInterest(Double effectiveInterest);

    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    Double getEffectiveInterest();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Double getAmount();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'loan check'.
     *
     * 
     *
     */
    void setLoanCheck(LoanCheck loanCheck);

    /**
     * Returns the property 'loan check'.
     *
     * 
     *
     */
    LoanCheck getLoanCheck();
    /**
     * Setter for the property 'loan check request datas'.
     *
     * 
     *
     */
    void setLoanCheckRequestDatas(Set<LoanCheckRequestData> loanCheckRequestDatas);

    /**
     * Returns the property 'loan check request datas'.
     *
     * 
     *
     */
    Set<LoanCheckRequestData> getLoanCheckRequestDatas();
    /**
     * Helper method to get reference of this object as model type.
     */
    LoanCheckRequest asLoanCheckRequest();
}
