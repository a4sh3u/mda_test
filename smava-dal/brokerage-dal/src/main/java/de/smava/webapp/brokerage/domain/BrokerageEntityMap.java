//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage entity map)}
import de.smava.webapp.brokerage.domain.history.BrokerageEntityMapHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageEntityMaps'.
 *
 * Temporary entity that maps redundant entities between brokerage and kredit privat.
 *
 * @author generator
 */
public class BrokerageEntityMap extends BrokerageEntityMapHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage entity map)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _brokerageId;
        protected Long _kreditPrivatId;
        protected String _type;
        
                                    
    /**
     * Setter for the property 'brokerage id'.
     *
     * 
     *
     */
    public void setBrokerageId(Long brokerageId) {
        _brokerageId = brokerageId;
    }
            
    /**
     * Returns the property 'brokerage id'.
     *
     * 
     *
     */
    public Long getBrokerageId() {
        return _brokerageId;
    }
                                            
    /**
     * Setter for the property 'kredit privat id'.
     *
     * 
     *
     */
    public void setKreditPrivatId(Long kreditPrivatId) {
        _kreditPrivatId = kreditPrivatId;
    }
            
    /**
     * Returns the property 'kredit privat id'.
     *
     * 
     *
     */
    public Long getKreditPrivatId() {
        return _kreditPrivatId;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageEntityMap.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(BrokerageEntityMap.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageEntityMap asBrokerageEntityMap() {
        return this;
    }
}
