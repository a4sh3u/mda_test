package de.smava.webapp.brokerage.dao.additionalData;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.smava.webapp.brokerage.domain.AdditionalBrokerageData;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * Created by pharbert on 09.12.15.
 */
public abstract class AbstractAdditionalData implements AdditionalBrokerageData {

    private static final Logger LOGGER = Logger.getLogger(AdditionalBrokerageData.class);

    public HashMap<String, Object> readAdditionalData(String filterName, String... filteredFields) {
        ObjectMapper om = new ObjectMapper();

        try {
            String json;
            if (filterName != null && filteredFields != null) {
                FilterProvider filters =
                        new SimpleFilterProvider().addFilter(filterName,
                                SimpleBeanPropertyFilter.filterOutAllExcept(filteredFields));
                json = om.writer(filters).writeValueAsString(this);
            } else {
                json = om.writeValueAsString(this);
            }
            HashMap<String, Object> result = om.readValue(json, HashMap.class);
            return result;
        } catch (Exception e) {
            LOGGER.error("Cannot read additional data: ", e);
        }

        return null;
    }

    @Override
    public HashMap<String, Object> readAdditionalData() {
        return null;
    }

    @Override
    public HashMap<String, Object> readAdditionalErrorData() {
        return null;
    }
}
