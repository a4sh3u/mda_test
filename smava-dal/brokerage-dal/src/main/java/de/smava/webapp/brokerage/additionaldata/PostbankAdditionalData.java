package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public class PostbankAdditionalData extends AbstractAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_POSTBANK;
    }
}
