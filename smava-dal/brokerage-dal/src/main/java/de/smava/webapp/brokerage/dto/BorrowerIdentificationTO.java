package de.smava.webapp.brokerage.dto;

public class BorrowerIdentificationTO {
    protected Long id;
    protected Long customerNumber;

    public BorrowerIdentificationTO() {
    }

    public BorrowerIdentificationTO(Long id, Long customerNumber) {
        this.id = id;
        this.customerNumber = customerNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(Long customerNumber) {
        this.customerNumber = customerNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BorrowerIdentificationTO that = (BorrowerIdentificationTO) o;

        if (!id.equals(that.id)) return false;
        return customerNumber.equals(that.customerNumber);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + customerNumber.hashCode();
        return result;
    }
}
