package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageInterestRateDetails;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageInterestRateDetailss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageInterestRateDetailsHistory extends AbstractBrokerageInterestRateDetails {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _activationDateInitVal;
    protected transient boolean _activationDateIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'activation date'.
     */
    public Date activationDateInitVal() {
        Date result;
        if (_activationDateIsSet) {
            result = _activationDateInitVal;
        } else {
            result = getActivationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'activation date'.
     */
    public boolean activationDateIsDirty() {
        return !valuesAreEqual(activationDateInitVal(), getActivationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'activation date'.
     */
    public boolean activationDateIsSet() {
        return _activationDateIsSet;
    }
	
}
