package de.smava.webapp.brokerage.domain.interfaces;


import de.smava.webapp.brokerage.domain.DocumentPlaceholder;
import de.smava.webapp.brokerage.domain.DocumentTypePlaceholder;

import java.util.Set;


/**
 * The domain object that represents 'DocumentPlaceholders'.
 *
 * @author generator
 */
public interface DocumentPlaceholderEntityInterface {

    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    public void setKey(String key);

    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    public String getKey();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    public void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    public String getValue();
    /**
     * Setter for the property 'document type placeholders'.
     *
     * 
     *
     */
    public void setDocumentTypePlaceholders(Set<DocumentTypePlaceholder> documentTypePlaceholders);

    /**
     * Returns the property 'document type placeholders'.
     *
     * 
     *
     */
    public Set<DocumentTypePlaceholder> getDocumentTypePlaceholders();
    /**
     * Helper method to get reference of this object as model type.
     */
    public DocumentPlaceholder asDocumentPlaceholder();
}
