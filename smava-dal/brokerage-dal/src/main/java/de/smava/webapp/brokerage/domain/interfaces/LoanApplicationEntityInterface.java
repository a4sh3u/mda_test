package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageState;
import de.smava.webapp.brokerage.domain.LoanApplication;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'LoanApplications'.
 *
 * @author generator
 */
public interface LoanApplicationEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    BrokerageState getState();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    void setRequestedRdiType(String requestedRdiType);

    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    String getRequestedRdiType();
    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    void setRequestedAmount(double requestedAmount);

    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    double getRequestedAmount();
    /**
     * Setter for the property 'reqeusted category'.
     *
     * 
     *
     */
    void setReqeustedCategory(de.smava.webapp.account.domain.Category reqeustedCategory);

    /**
     * Returns the property 'reqeusted category'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Category getReqeustedCategory();
    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    void setRequestedDuration(int requestedDuration);

    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    int getRequestedDuration();
    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    void setSharedLoan(boolean sharedLoan);

    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    boolean getSharedLoan();
    /**
     * Setter for the property 'brokerage applications'.
     *
     * 
     *
     */
    void setBrokerageApplications(Set<BrokerageApplication> brokerageApplications);

    /**
     * Returns the property 'brokerage applications'.
     *
     * 
     *
     */
    Set<BrokerageApplication> getBrokerageApplications();
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    void setRequestedLoanType(de.smava.webapp.brokerage.domain.RequestedLoanType requestedLoanType);

    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType();
    /**
     * Setter for the property 'brokerage car finance'.
     *
     * 
     *
     */
    void setBrokerageCarFinance(de.smava.webapp.brokerage.domain.BrokerageCarFinance brokerageCarFinance);

    /**
     * Returns the property 'brokerage car finance'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageCarFinance getBrokerageCarFinance();
    /**
     * Setter for the property 'initiator type'.
     *
     * 
     *
     */
    void setInitiatorType(de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType initiatorType);

    /**
     * Returns the property 'initiator type'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.LoanApplicationInitiatorType getInitiatorType();
    /**
     * Setter for the property 'initiator tool'.
     *
     * 
     *
     */
    void setInitiatorTool(String initiatorTool);

    /**
     * Returns the property 'initiator tool'.
     *
     * 
     *
     */
    String getInitiatorTool();
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    void setEconomicalData(de.smava.webapp.account.domain.EconomicalData economicalData);

    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.EconomicalData getEconomicalData();
    /**
     * Setter for the property 'reached email timeout'.
     *
     * 
     *
     */
    void setReachedEmailTimeout(boolean reachedEmailTimeout);

    /**
     * Returns the property 'reached email timeout'.
     *
     * 
     *
     */
    boolean getReachedEmailTimeout();
    /**
     * Setter for the property 'selected bank'.
     *
     * 
     *
     */
    void setSelectedBank(BrokerageBank selectedBank);

    /**
     * Returns the property 'selected bank'.
     *
     * 
     *
     */
    BrokerageBank getSelectedBank();
    /**
     * Setter for the property 'visible'.
     *
     * 
     *
     */
    void setVisible(boolean visible);

    /**
     * Returns the property 'visible'.
     *
     * 
     *
     */
    boolean getVisible();
    /**
     * Setter for the property 'customer value'.
     *
     * 
     *
     */
    void setCustomerValue(Double customerValue);

    /**
     * Returns the property 'customer value'.
     *
     * 
     *
     */
    Double getCustomerValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    LoanApplication asLoanApplication();
}
