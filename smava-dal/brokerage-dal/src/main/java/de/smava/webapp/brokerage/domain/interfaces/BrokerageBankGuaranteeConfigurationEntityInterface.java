package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageBankGuaranteeConfiguration;


/**
 * The domain object that represents 'BrokerageBankGuaranteeConfigurations'.
 *
 * @author generator
 */
public interface BrokerageBankGuaranteeConfigurationEntityInterface {

    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'force top chance'.
     *
     * 
     *
     */
    void setForceTopChance(boolean forceTopChance);

    /**
     * Returns the property 'force top chance'.
     *
     * 
     *
     */
    boolean getForceTopChance();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBankGuaranteeConfiguration asBrokerageBankGuaranteeConfiguration();
}
