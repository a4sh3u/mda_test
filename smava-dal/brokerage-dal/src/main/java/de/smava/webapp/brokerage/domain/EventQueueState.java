package de.smava.webapp.brokerage.domain;

public enum EventQueueState {
	OPEN,
	SUCCESSFUL,
	STATE_FAILED,
	VOID,
	DELETED,
	DRAFT;
}
