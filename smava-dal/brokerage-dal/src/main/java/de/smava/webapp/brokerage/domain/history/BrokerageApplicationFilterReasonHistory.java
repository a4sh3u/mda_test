package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageApplicationFilterReason;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationFilterReasons'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationFilterReasonHistory extends AbstractBrokerageApplicationFilterReason {

    protected transient String _reasonDescriptionInitVal;
    protected transient boolean _reasonDescriptionIsSet;


		
    /**
     * Returns the initial value of the property 'reason description'.
     */
    public String reasonDescriptionInitVal() {
        String result;
        if (_reasonDescriptionIsSet) {
            result = _reasonDescriptionInitVal;
        } else {
            result = getReasonDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'reason description'.
     */
    public boolean reasonDescriptionIsDirty() {
        return !valuesAreEqual(reasonDescriptionInitVal(), getReasonDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'reason description'.
     */
    public boolean reasonDescriptionIsSet() {
        return _reasonDescriptionIsSet;
    }

}
