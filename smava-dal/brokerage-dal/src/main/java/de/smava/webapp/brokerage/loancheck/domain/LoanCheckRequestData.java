//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.loancheck.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan check request data)}
import de.smava.webapp.brokerage.loancheck.domain.history.LoanCheckRequestDataHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanCheckRequestDatas'.
 *
 * @author generator
 */
public class LoanCheckRequestData extends LoanCheckRequestDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(loan check request data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected LoanCheckRequest _loanCheckRequest;
        protected String _request;
        protected String _response;
        protected de.smava.webapp.brokerage.domain.BrokerageState _state;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'loan check request'.
     */
    public void setLoanCheckRequest(LoanCheckRequest loanCheckRequest) {
        _loanCheckRequest = loanCheckRequest;
    }
            
    /**
     * Returns the property 'loan check request'.
     */
    public LoanCheckRequest getLoanCheckRequest() {
        return _loanCheckRequest;
    }
                                    /**
     * Setter for the property 'request'.
     */
    public void setRequest(String request) {
        if (!_requestIsSet) {
            _requestIsSet = true;
            _requestInitVal = getRequest();
        }
        registerChange("request", _requestInitVal, request);
        _request = request;
    }
                        
    /**
     * Returns the property 'request'.
     */
    public String getRequest() {
        return _request;
    }
                                    /**
     * Setter for the property 'response'.
     */
    public void setResponse(String response) {
        if (!_responseIsSet) {
            _responseIsSet = true;
            _responseInitVal = getResponse();
        }
        registerChange("response", _responseInitVal, response);
        _response = response;
    }
                        
    /**
     * Returns the property 'response'.
     */
    public String getResponse() {
        return _response;
    }
                                            
    /**
     * Setter for the property 'state'.
     */
    public void setState(de.smava.webapp.brokerage.domain.BrokerageState state) {
        _state = state;
    }
            
    /**
     * Returns the property 'state'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageState getState() {
        return _state;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LoanCheckRequestData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _request=").append(_request);
            builder.append("\n    _response=").append(_response);
            builder.append("\n}");
        } else {
            builder.append(LoanCheckRequestData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LoanCheckRequestData asLoanCheckRequestData() {
        return this;
    }
}
