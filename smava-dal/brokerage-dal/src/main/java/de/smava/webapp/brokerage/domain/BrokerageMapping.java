//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage mapping)}
import de.smava.webapp.brokerage.domain.history.BrokerageMappingHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMappings'.
 *
 * @author generator
 */
public class BrokerageMapping extends BrokerageMappingHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage mapping)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageMappingConverter _converter;
        protected String _smavaValue;
        protected String _bankValue;
        protected MappingDirection _direction;
        
                                    
    /**
     * Setter for the property 'converter'.
     */
    public void setConverter(BrokerageMappingConverter converter) {
        _converter = converter;
    }
            
    /**
     * Returns the property 'converter'.
     */
    public BrokerageMappingConverter getConverter() {
        return _converter;
    }
                                    /**
     * Setter for the property 'smava value'.
     */
    public void setSmavaValue(String smavaValue) {
        if (!_smavaValueIsSet) {
            _smavaValueIsSet = true;
            _smavaValueInitVal = getSmavaValue();
        }
        registerChange("smava value", _smavaValueInitVal, smavaValue);
        _smavaValue = smavaValue;
    }
                        
    /**
     * Returns the property 'smava value'.
     */
    public String getSmavaValue() {
        return _smavaValue;
    }
                                    /**
     * Setter for the property 'bank value'.
     */
    public void setBankValue(String bankValue) {
        if (!_bankValueIsSet) {
            _bankValueIsSet = true;
            _bankValueInitVal = getBankValue();
        }
        registerChange("bank value", _bankValueInitVal, bankValue);
        _bankValue = bankValue;
    }
                        
    /**
     * Returns the property 'bank value'.
     */
    public String getBankValue() {
        return _bankValue;
    }
                                            
    /**
     * Setter for the property 'direction'.
     */
    public void setDirection(MappingDirection direction) {
        _direction = direction;
    }
            
    /**
     * Returns the property 'direction'.
     */
    public MappingDirection getDirection() {
        return _direction;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageMapping.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _smavaValue=").append(_smavaValue);
            builder.append("\n    _bankValue=").append(_bankValue);
            builder.append("\n}");
        } else {
            builder.append(BrokerageMapping.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageMapping asBrokerageMapping() {
        return this;
    }
}
