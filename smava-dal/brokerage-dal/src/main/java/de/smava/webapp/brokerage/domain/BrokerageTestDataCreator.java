package de.smava.webapp.brokerage.domain;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.smava.test.TestDataCreator;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Address;
import de.smava.webapp.account.domain.Category;
import de.smava.webapp.account.domain.CoBorrowerEconomicalData;
import de.smava.webapp.account.domain.CustomerAgreement;
import de.smava.webapp.account.domain.CustomerAgreementType;
import de.smava.webapp.account.domain.EconomicalData;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheck;
import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequest;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.i18n.Employment;
import de.smava.webapp.commons.util.PersonConstants;

public class BrokerageTestDataCreator {


    public static BrokerageApplication createDefaultBa(Account account){
        Long brokerageApplicationId = 2437473L;

        BrokerageApplication brokerageApplication = new BrokerageApplication();
        brokerageApplication.setId(brokerageApplicationId);
        brokerageApplication.setEmailCreated(null);
        brokerageApplication.setCreationDate(new Date());
        brokerageApplication.setAccount(account);
        brokerageApplication.setEconomicalData(TestDataCreator.createEconomicalData(account));

        brokerageApplication.setRequestedRdiType("double");
        brokerageApplication.setRdiType("double");
        brokerageApplication.setRequestedDuration(60);
        brokerageApplication.setRequestedLoanType(RequestedLoanType.STANDARD);
        brokerageApplication.setRequestedAmount(5000.0);
        brokerageApplication.setState(BrokerageState.APPLICATION_ACCEPTED);
        brokerageApplication.setReferenceBankAccount(account.getBankAccount());

        HashMap<String, Address> addressMap = createAddressMap();
        brokerageApplication.setAddressMap(addressMap);

        Category requestedCategory = createDefaultRequestedCategory();
        brokerageApplication.setReqeustedCategory(requestedCategory);

        BrokerageBank brokerageBank = createDefaultBrokerageBank();
        brokerageApplication.setBrokerageBank(brokerageBank);

        Person person = account.getPerson();
        if(person == null) {
            person = TestDataCreator.createPerson(account);
        }
        brokerageApplication.setFirstPerson(person);

        LoanApplication loanApplication = createDefaultLa(account);
        brokerageApplication.setLoanApplication(loanApplication);

        return brokerageApplication;
    }

    public static BrokerageBank createDefaultBrokerageBank(){
        final String bankName = "some_bank";
        BrokerageBank brokerageBank = new BrokerageBank();
        brokerageBank.setId(33L);
        brokerageBank.setName(bankName);
        brokerageBank.setValid(true);
        brokerageBank.setMarketingPartnerId(1L);
        return brokerageBank;
    }

    public static Category createDefaultRequestedCategory(){
        Category requestedCategory = new Category();
        requestedCategory.setId(3216L);
        return requestedCategory;
    }

    private static void addCoBorrowerEconomicalData( EconomicalData economicalData) {

        Calendar startOfEmployment = CurrentDate.getCalendar();
        startOfEmployment.add(Calendar.YEAR, -1);
        startOfEmployment.set(Calendar.DAY_OF_MONTH, 1);

        Sector sector = new Sector();
        sector.setId(4869L);

        if ( economicalData instanceof CoBorrowerEconomicalData){
            CoBorrowerEconomicalData coEd = (CoBorrowerEconomicalData)economicalData;
            coEd.setCoBorrowerSector(sector);
            coEd.setCoBorrowerEmployment(Employment.EMPLOYEE);
            coEd.setCoBorrowerEmployerName("The Co Company");
            coEd.setCoBorrowerTempEmployment(false);
            coEd.setCoBorrowerStartOfOccupation(startOfEmployment.getTime());
            coEd.setCoBorrowerEndOfTempEmployment(null);
            coEd.setType(EconomicalData.TYPE_SHARED_LOAN);
            coEd.setCoBorrowerIncome(2000d);
        }
    }

    private static HashMap<String, Address> createAddressMap() {
        HashMap<String, Address> addressMap = new HashMap<String, Address>();
        Address mAdd = new Address();
        Address emplAdd = new Address();

        Calendar startDate = CurrentDate.getCalendar();
        startDate.add(Calendar.YEAR, -1);
        startDate.set(Calendar.DAY_OF_MONTH, 1);

        mAdd.setStreet("Main Address Street");
        mAdd.setStreetNumber("123");
        mAdd.setZipCode("55555");
        mAdd.setCity("Main City");
        mAdd.setCountry("DE");
        mAdd.setLivingThereSince(startDate.getTime());
        mAdd.setType(Address.TYPE_MAIN_ADDRESS);
        addressMap.put(Address.TYPE_MAIN_ADDRESS, mAdd);

        emplAdd.setStreet("Employer Address Street");
        emplAdd.setStreetNumber("456");
        emplAdd.setZipCode("33333");
        emplAdd.setCity("Employer City");
        emplAdd.setCountry("DE");
        emplAdd.setLivingThereSince(startDate.getTime());
        emplAdd.setType(Address.TYPE_BORROWER_EMPLOYER_ADDRESS);
        addressMap.put(Address.TYPE_BORROWER_EMPLOYER_ADDRESS, emplAdd);

        return addressMap;
    }

    public static LoanCheckRequest createDefaultLcr(Account a){

        LoanCheckRequest lcr = new LoanCheckRequest();
        LoanCheck lc = new LoanCheck();
        lc.setAccount(a);
        lcr.setLoanCheck(lc);
        lcr.setCreationDate(new Date());
        lcr.setEconomicalData( TestDataCreator.createEconomicalData(a));
        Long brokerageApplicationId = 2437473L;
        lcr.setId(brokerageApplicationId);

        lcr.setFirstPerson(a.getPerson());

        lcr.setRequestedDuration(60);
        lcr.setRequestedAmount(5000.0);


        BrokerageBank bb = new BrokerageBank();
        bb.setId(33L);
        lcr.setBrokerageBank(bb);

        return lcr;
    }




    public static BrokerageApplication createDefaultBa(){
        Account a = TestDataCreator.createBorrower();

        CustomerAgreement ca = new CustomerAgreement();
        ca.setAgreementDate(new Date());
        ca.setType(CustomerAgreementType.BROKERAGE_AGREEMENT);
        a.getCustomerAgreements().add(ca);
        return createDefaultBa(a);
    }

    public static BrokerageApplication createDefaultCarLoanBa(){
        BrokerageApplication ba = createDefaultBa(TestDataCreator.createBorrower());
        BrokerageCarFinance bcf = new BrokerageCarFinance();
        bcf.setAccount(ba.getAccount());
        bcf.setCreationDate( new Date());
        bcf.setInitialPayment(4000.0);
        bcf.setVehicleBrand("Rennwagen");
        bcf.setVehicleKm(12000);
        bcf.setVehiclePowerKw(40);
        bcf.setVehicleModel("Brumm");
        bcf.setVehiclePrice(14000.0);
        bcf.setVehicleType(VehicleType.CAR);
        Calendar c = CurrentDate.getCalendar();
        c.set(Calendar.YEAR, 2010);
        bcf.setVehicleRegistrationYear(c.getTime());
        ba.setBrokerageCarFinance(bcf);
        return ba;
    }

    private static Person createCoBorrowerPerson(Account a) {
        Person p = new Person();
        p.setId(2L);
        p.setAccount(a);
        p.setSalutation(PersonConstants.FEMALE_SALUTATION);
        p.setFirstName("Maria");
        p.setLastName("Meier");
        p.setBirthName("Hansen");
        p.setPlaceOfBirth("Geburtsstadt");
        p.setCitizenship("DE");
        p.setFamilyStatus(Person.MARRIED);
        Calendar birthday = Calendar.getInstance();
        birthday.set(1980, 11, 3, 0, 0);
        p.setDateOfBirth(birthday.getTime());
        p.setType(Person.SECONDARY_PERSON_TYPE);
        return p;
    }

    private static void  addCoBorrowerAddresses( Map<String, Address> addressMap){

        Address emplAdd = new Address();

        Calendar startDate = CurrentDate.getCalendar();
        startDate.add(Calendar.YEAR, -1);
        startDate.set(Calendar.DAY_OF_MONTH, 1);



        emplAdd.setStreet("Co Employer Address Street");
        emplAdd.setStreetNumber("456");
        emplAdd.setZipCode("99999");
        emplAdd.setCity("Co Employer City");
        emplAdd.setCountry("DE");
        emplAdd.setLivingThereSince(startDate.getTime());
        emplAdd.setType(Address.TYPE_CO_BORROWER_EMPLOYER_ADDRESS);
        addressMap.put(Address.TYPE_CO_BORROWER_EMPLOYER_ADDRESS, emplAdd);

    }

    public static BrokerageApplication createSharedLoanBa(){
        BrokerageApplication ba =  createDefaultBa(TestDataCreator.createBorrower());
        ba.setSecondPerson( createCoBorrowerPerson(ba.getAccount()));
        addCoBorrowerAddresses( ba.getAddressMap());
        addCoBorrowerEconomicalData(ba.getEconomicalData());
        return ba;

    }

    public static LoanCheckRequest createDefaultLcr(){
        return createDefaultLcr(TestDataCreator.createBorrower());
    }

    public static LoanApplication createDefaultLa(Account a){
        Long loanApplicationId = 999L;
        LoanApplication loanApplication = new LoanApplication();
        loanApplication.setId(loanApplicationId);
        loanApplication.setAccount(a);
        loanApplication.setSharedLoan(false);
        loanApplication.setReachedEmailTimeout(false);
        loanApplication.setCreationDate(CurrentDate.getDate());
        loanApplication.setState(BrokerageState.APPLICATION_ACCEPTED);
        loanApplication.setRequestedRdiType("noRdiContract");
        loanApplication.setRequestedLoanType(RequestedLoanType.STANDARD);

        Set<BrokerageApplication> set = new HashSet<BrokerageApplication>();

        loanApplication.setBrokerageApplications(set);

        Category category = new Category();
        category.setId(1234L);
        loanApplication.setReqeustedCategory(category);

        return loanApplication;
    }

}