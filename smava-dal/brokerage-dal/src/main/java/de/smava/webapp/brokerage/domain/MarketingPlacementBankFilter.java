//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement bank filter)}
import de.smava.webapp.brokerage.domain.history.MarketingPlacementBankFilterHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPlacementBankFilters'.
 *
 * @author generator
 */
public class MarketingPlacementBankFilter extends MarketingPlacementBankFilterHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing placement bank filter)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _externalAffiliateId;
        protected BrokerageBank _brokerageBank;
        protected de.smava.webapp.brokerage.domain.FilterType _filterType;
        
                            /**
     * Setter for the property 'external affiliate id'.
     */
    public void setExternalAffiliateId(String externalAffiliateId) {
        if (!_externalAffiliateIdIsSet) {
            _externalAffiliateIdIsSet = true;
            _externalAffiliateIdInitVal = getExternalAffiliateId();
        }
        registerChange("external affiliate id", _externalAffiliateIdInitVal, externalAffiliateId);
        _externalAffiliateId = externalAffiliateId;
    }
                        
    /**
     * Returns the property 'external affiliate id'.
     */
    public String getExternalAffiliateId() {
        return _externalAffiliateId;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'filter type'.
     */
    public void setFilterType(de.smava.webapp.brokerage.domain.FilterType filterType) {
        _filterType = filterType;
    }
            
    /**
     * Returns the property 'filter type'.
     */
    public de.smava.webapp.brokerage.domain.FilterType getFilterType() {
        return _filterType;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingPlacementBankFilter.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _externalAffiliateId=").append(_externalAffiliateId);
            builder.append("\n}");
        } else {
            builder.append(MarketingPlacementBankFilter.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingPlacementBankFilter asMarketingPlacementBankFilter() {
        return this;
    }
}
