package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageInterestRateDetails;
import de.smava.webapp.brokerage.domain.BrokerageInterestRateDetailsData;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageInterestRateDetailss'.
 *
 * @author generator
 */
public interface BrokerageInterestRateDetailsEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'activation date'.
     *
     * 
     *
     */
    void setActivationDate(Date activationDate);

    /**
     * Returns the property 'activation date'.
     *
     * 
     *
     */
    Date getActivationDate();
    /**
     * Setter for the property 'brokerage interest rate details datas'.
     *
     * 
     *
     */
    void setBrokerageInterestRateDetailsDatas(Set<BrokerageInterestRateDetailsData> brokerageInterestRateDetailsDatas);

    /**
     * Returns the property 'brokerage interest rate details datas'.
     *
     * 
     *
     */
    Set<BrokerageInterestRateDetailsData> getBrokerageInterestRateDetailsDatas();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageInterestRateDetails asBrokerageInterestRateDetails();
}
