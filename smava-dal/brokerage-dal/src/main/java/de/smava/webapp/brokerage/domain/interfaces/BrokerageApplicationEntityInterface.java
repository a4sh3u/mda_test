package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.*;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageApplications'.
 *
 * @author generator
 */
public interface BrokerageApplicationEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'ext ref number'.
     *
     * 
     *
     */
    void setExtRefNumber(String extRefNumber);

    /**
     * Returns the property 'ext ref number'.
     *
     * 
     *
     */
    String getExtRefNumber();
    /**
     * Setter for the property 'prev ext ref number'.
     *
     * 
     *
     */
    void setPrevExtRefNumber(String prevExtRefNumber);

    /**
     * Returns the property 'prev ext ref number'.
     *
     * 
     *
     */
    String getPrevExtRefNumber();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    BrokerageState getState();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'economical data'.
     *
     * 
     *
     */
    void setEconomicalData(de.smava.webapp.account.domain.EconomicalData economicalData);

    /**
     * Returns the property 'economical data'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.EconomicalData getEconomicalData();
    /**
     * Setter for the property 'first person'.
     *
     * 
     *
     */
    void setFirstPerson(de.smava.webapp.account.domain.Person firstPerson);

    /**
     * Returns the property 'first person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getFirstPerson();
    /**
     * Setter for the property 'second person'.
     *
     * 
     *
     */
    void setSecondPerson(de.smava.webapp.account.domain.Person secondPerson);

    /**
     * Returns the property 'second person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getSecondPerson();
    /**
     * Setter for the property 'reference bank account'.
     *
     * 
     *
     */
    void setReferenceBankAccount(de.smava.webapp.account.domain.BankAccount referenceBankAccount);

    /**
     * Returns the property 'reference bank account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccount getReferenceBankAccount();
    /**
     * Setter for the property 'addresses'.
     *
     * 
     *
     */
    void setAddresses(Set<de.smava.webapp.account.domain.Address> addresses);

    /**
     * Returns the property 'addresses'.
     *
     * 
     *
     */
    Set<de.smava.webapp.account.domain.Address> getAddresses();
    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    void setRequestedRdiType(String requestedRdiType);

    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    String getRequestedRdiType();
    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    void setRequestedAmount(double requestedAmount);

    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    double getRequestedAmount();
    /**
     * Setter for the property 'reqeusted category'.
     *
     * 
     *
     */
    void setReqeustedCategory(de.smava.webapp.account.domain.Category reqeustedCategory);

    /**
     * Returns the property 'reqeusted category'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Category getReqeustedCategory();
    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    void setRequestedDuration(int requestedDuration);

    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    int getRequestedDuration();
    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    void setMonthlyRate(Double monthlyRate);

    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    Double getMonthlyRate();
    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    void setEffectiveInterest(Double effectiveInterest);

    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    Double getEffectiveInterest();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Double getAmount();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'rdi type'.
     *
     * 
     *
     */
    void setRdiType(String rdiType);

    /**
     * Returns the property 'rdi type'.
     *
     * 
     *
     */
    String getRdiType();
    /**
     * Setter for the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    void setBrokerageApplicationFilterReason(BrokerageApplicationFilterReason brokerageApplicationFilterReason);

    /**
     * Returns the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    BrokerageApplicationFilterReason getBrokerageApplicationFilterReason();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication();

    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    void setRequestedLoanType(de.smava.webapp.brokerage.domain.RequestedLoanType requestedLoanType);

    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType();
    /**
     * Setter for the property 'pap sale tracked date'.
     *
     * 
     *
     */
    void setPapSaleTrackedDate(Date papSaleTrackedDate);

    /**
     * Returns the property 'pap sale tracked date'.
     *
     * 
     *
     */
    Date getPapSaleTrackedDate();
    /**
     * Setter for the property 'last state request'.
     *
     * 
     *
     */
    void setLastStateRequest(Date lastStateRequest);

    /**
     * Returns the property 'last state request'.
     *
     * 
     *
     */
    Date getLastStateRequest();
    /**
     * Setter for the property 'last state change'.
     *
     * 
     *
     */
    void setLastStateChange(Date lastStateChange);

    /**
     * Returns the property 'last state change'.
     *
     * 
     *
     */
    Date getLastStateChange();
    /**
     * Setter for the property 'document container'.
     *
     * 
     *
     */
    void setDocumentContainer(de.smava.webapp.account.domain.DocumentContainer documentContainer);

    /**
     * Returns the property 'document container'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.DocumentContainer getDocumentContainer();
    /**
     * Setter for the property 'email created'.
     *
     * 
     *
     */
    void setEmailCreated(Date emailCreated);

    /**
     * Returns the property 'email created'.
     *
     * 
     *
     */
    Date getEmailCreated();
    /**
     * Setter for the property 'email type'.
     *
     * 
     *
     */
    void setEmailType(String emailType);

    /**
     * Returns the property 'email type'.
     *
     * 
     *
     */
    String getEmailType();
    /**
     * Setter for the property 'documents requested'.
     *
     * 
     *
     */
    void setDocumentsRequested(Date documentsRequested);

    /**
     * Returns the property 'documents requested'.
     *
     * 
     *
     */
    Date getDocumentsRequested();
    /**
     * Setter for the property 'documents sent'.
     *
     * 
     *
     */
    void setDocumentsSent(Date documentsSent);

    /**
     * Returns the property 'documents sent'.
     *
     * 
     *
     */
    Date getDocumentsSent();
    /**
     * Setter for the property 'brokerage car finance'.
     *
     * 
     *
     */
    void setBrokerageCarFinance(de.smava.webapp.brokerage.domain.BrokerageCarFinance brokerageCarFinance);

    /**
     * Returns the property 'brokerage car finance'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageCarFinance getBrokerageCarFinance();
    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    void setAdditionalData(String additionalData);

    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    String getAdditionalData();
    /**
     * Setter for the property 'payout date'.
     *
     * 
     *
     */
    void setPayoutDate(Date payoutDate);

    /**
     * Returns the property 'payout date'.
     *
     * 
     *
     */
    Date getPayoutDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplication asBrokerageApplication();
}
