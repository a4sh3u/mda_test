package de.smava.webapp.brokerage.loancheck.domain.history;



import de.smava.webapp.brokerage.loancheck.domain.abstracts.AbstractLoanCheckRequest;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'LoanCheckRequests'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class LoanCheckRequestHistory extends AbstractLoanCheckRequest {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient double _requestedAmountInitVal;
    protected transient boolean _requestedAmountIsSet;
    protected transient int _requestedDurationInitVal;
    protected transient boolean _requestedDurationIsSet;
    protected transient String _requestedRdiTypeInitVal;
    protected transient boolean _requestedRdiTypeIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
						
    /**
     * Returns the initial value of the property 'requested amount'.
     */
    public double requestedAmountInitVal() {
        double result;
        if (_requestedAmountIsSet) {
            result = _requestedAmountInitVal;
        } else {
            result = getRequestedAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested amount'.
     */
    public boolean requestedAmountIsDirty() {
        return !valuesAreEqual(requestedAmountInitVal(), getRequestedAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'requested amount'.
     */
    public boolean requestedAmountIsSet() {
        return _requestedAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested duration'.
     */
    public int requestedDurationInitVal() {
        int result;
        if (_requestedDurationIsSet) {
            result = _requestedDurationInitVal;
        } else {
            result = getRequestedDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested duration'.
     */
    public boolean requestedDurationIsDirty() {
        return !valuesAreEqual(requestedDurationInitVal(), getRequestedDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'requested duration'.
     */
    public boolean requestedDurationIsSet() {
        return _requestedDurationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested rdi type'.
     */
    public String requestedRdiTypeInitVal() {
        String result;
        if (_requestedRdiTypeIsSet) {
            result = _requestedRdiTypeInitVal;
        } else {
            result = getRequestedRdiType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested rdi type'.
     */
    public boolean requestedRdiTypeIsDirty() {
        return !valuesAreEqual(requestedRdiTypeInitVal(), getRequestedRdiType());
    }

    /**
     * Returns true if the setter method was called for the property 'requested rdi type'.
     */
    public boolean requestedRdiTypeIsSet() {
        return _requestedRdiTypeIsSet;
    }
						
}
