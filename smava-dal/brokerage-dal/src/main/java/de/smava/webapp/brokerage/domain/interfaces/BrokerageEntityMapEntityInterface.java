package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageEntityMap;


/**
 * The domain object that represents 'BrokerageEntityMaps'.
 *
 * @author generator
 */
public interface BrokerageEntityMapEntityInterface {

    /**
     * Setter for the property 'brokerage id'.
     *
     * 
     *
     */
    void setBrokerageId(Long brokerageId);

    /**
     * Returns the property 'brokerage id'.
     *
     * 
     *
     */
    Long getBrokerageId();
    /**
     * Setter for the property 'kredit privat id'.
     *
     * 
     *
     */
    void setKreditPrivatId(Long kreditPrivatId);

    /**
     * Returns the property 'kredit privat id'.
     *
     * 
     *
     */
    Long getKreditPrivatId();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageEntityMap asBrokerageEntityMap();
}
