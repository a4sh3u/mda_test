//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage mapping converter)}
import de.smava.webapp.brokerage.domain.history.BrokerageMappingConverterHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMappingConverters'.
 *
 * @author generator
 */
public class BrokerageMappingConverter extends BrokerageMappingConverterHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage mapping converter)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _value;
        protected ConverterType _type;
        
                            /**
     * Setter for the property 'value'.
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     */
    public String getValue() {
        return _value;
    }
                                            
    /**
     * Setter for the property 'type'.
     */
    public void setType(ConverterType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     */
    public ConverterType getType() {
        return _type;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageMappingConverter.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _value=").append(_value);
            builder.append("\n}");
        } else {
            builder.append(BrokerageMappingConverter.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageMappingConverter asBrokerageMappingConverter() {
        return this;
    }
}
