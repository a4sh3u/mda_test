package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageInterestRatePrediction;
import de.smava.webapp.brokerage.domain.BrokerageInterestRatePredictionData;
import de.smava.webapp.brokerage.domain.RequestedLoanType;


/**
 * The domain object that represents 'BrokerageInterestRatePredictionDatas'.
 *
 * @author generator
 */
public interface BrokerageInterestRatePredictionDataEntityInterface {

    /**
     * Setter for the property 'brokerage interest rate prediction'.
     *
     * 
     *
     */
    void setBrokerageInterestRatePrediction(BrokerageInterestRatePrediction brokerageInterestRatePrediction);

    /**
     * Returns the property 'brokerage interest rate prediction'.
     *
     * 
     *
     */
    BrokerageInterestRatePrediction getBrokerageInterestRatePrediction();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    void setRequestedLoanType(RequestedLoanType requestedLoanType);

    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    RequestedLoanType getRequestedLoanType();
    /**
     * Setter for the property 'requested amount min'.
     *
     * 
     *
     */
    void setRequestedAmountMin(Double requestedAmountMin);

    /**
     * Returns the property 'requested amount min'.
     *
     * 
     *
     */
    Double getRequestedAmountMin();
    /**
     * Setter for the property 'requested amount max'.
     *
     * 
     *
     */
    void setRequestedAmountMax(Double requestedAmountMax);

    /**
     * Returns the property 'requested amount max'.
     *
     * 
     *
     */
    Double getRequestedAmountMax();
    /**
     * Setter for the property 'requested duration min'.
     *
     * 
     *
     */
    void setRequestedDurationMin(Integer requestedDurationMin);

    /**
     * Returns the property 'requested duration min'.
     *
     * 
     *
     */
    Integer getRequestedDurationMin();
    /**
     * Setter for the property 'requested duration max'.
     *
     * 
     *
     */
    void setRequestedDurationMax(Integer requestedDurationMax);

    /**
     * Returns the property 'requested duration max'.
     *
     * 
     *
     */
    Integer getRequestedDurationMax();
    /**
     * Setter for the property 'employment'.
     *
     * First applicant employment.
     *
     */
    void setEmployment(String employment);

    /**
     * Returns the property 'employment'.
     *
     * First applicant employment.
     *
     */
    String getEmployment();
    /**
     * Setter for the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    void setIncomeMin(Double incomeMin);

    /**
     * Returns the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    Double getIncomeMin();
    /**
     * Setter for the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    void setIncomeMax(Double incomeMax);

    /**
     * Returns the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    Double getIncomeMax();
    /**
     * Setter for the property 'leverage min'.
     *
     * First applicant leverage MIN (Verschuldungsgrad).
     *
     */
    void setLeverageMin(Double leverageMin);

    /**
     * Returns the property 'leverage min'.
     *
     * First applicant leverage MIN (Verschuldungsgrad).
     *
     */
    Double getLeverageMin();
    /**
     * Setter for the property 'leverage max'.
     *
     * First applicant leverage MAX (Verschuldungsgrad).
     *
     */
    void setLeverageMax(Double leverageMax);

    /**
     * Returns the property 'leverage max'.
     *
     * First applicant leverage MAX (Verschuldungsgrad).
     *
     */
    Double getLeverageMax();
    /**
     * Setter for the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    void setSchufaAvailable(Boolean schufaAvailable);

    /**
     * Returns the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    Boolean getSchufaAvailable();
    /**
     * Setter for the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    void setSchufaMin(Integer schufaMin);

    /**
     * Returns the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    Integer getSchufaMin();
    /**
     * Setter for the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    void setSchufaMax(Integer schufaMax);

    /**
     * Returns the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    Integer getSchufaMax();
    /**
     * Setter for the property 'factor'.
     *
     * Factor.
     *
     */
    void setFactor(Double factor);

    /**
     * Returns the property 'factor'.
     *
     * Factor.
     *
     */
    Double getFactor();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageInterestRatePredictionData asBrokerageInterestRatePredictionData();
}
