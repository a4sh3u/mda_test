package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBucketExclusionRule;


/**
 * The domain object that represents 'BrokerageBucketExclusionRules'.
 *
 * @author generator
 */
public interface BrokerageBucketExclusionRuleEntityInterface {

    /**
     * Setter for the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    void setBrokerageBucketConfiguration(de.smava.webapp.brokerage.domain.BrokerageBucketConfiguration brokerageBucketConfiguration);

    /**
     * Returns the property 'brokerage bucket configuration'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBucketConfiguration getBrokerageBucketConfiguration();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'exclusion rule'.
     *
     * 
     *
     */
    void setExclusionRule(String exclusionRule);

    /**
     * Returns the property 'exclusion rule'.
     *
     * 
     *
     */
    String getExclusionRule();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBucketExclusionRule asBrokerageBucketExclusionRule();
}
