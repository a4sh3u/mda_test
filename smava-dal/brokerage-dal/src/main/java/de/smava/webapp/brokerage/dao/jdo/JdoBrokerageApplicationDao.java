//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.brokerage.dao.BrokerageApplicationDao;
import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.brokerage.domain.BrokerageState;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.exception.InvalidValueException;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BrokerageApplications'.
 *
 * @author generator
 */
@Repository(value = "brokerageApplicationDao")
public class JdoBrokerageApplicationDao extends JdoBaseDao implements BrokerageApplicationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageApplicationDao.class);

    private static final String CLASS_NAME = "BrokerageApplication";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(brokerage application)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the brokerage application identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BrokerageApplication load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BrokerageApplication result = getEntity(BrokerageApplication.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BrokerageApplication getBrokerageApplication(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BrokerageApplication entity = findUniqueEntity(BrokerageApplication.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the brokerage application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BrokerageApplication brokerageApplication) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageApplication: " + brokerageApplication);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(brokerage application)}
        
        LOGGER.info("saving bank: " + brokerageApplication.getBrokerageBank().getName());
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(brokerageApplication);
    }

    /**
     * @deprecated Use {@link #save(BrokerageApplication) instead}
     */
    public Long saveBrokerageApplication(BrokerageApplication brokerageApplication) {
        return save(brokerageApplication);
    }

    /**
     * Deletes an brokerage application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application
     */
    public void deleteBrokerageApplication(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBrokerageApplication: " + id);
        }
        deleteEntity(BrokerageApplication.class, id);
    }

    /**
     * Retrieves all 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageApplication' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageApplication' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BrokerageApplication' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplication' instances.
     */
    public long getBrokerageApplicationCount() {
        long result = getEntityCount(BrokerageApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplication' instances which match the given whereClause.
     */
    public long getBrokerageApplicationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BrokerageApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplication' instances which match the given whereClause together with params specified in object array.
     */
    public long getBrokerageApplicationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BrokerageApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application)}
    //
    // insert custom methods here

    public Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, Collection<BrokerageState> states, Pageable pageable, Sortable sortable) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(borrower);
        params.addAll(states);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    getStatesTerm(oqlTerm, states));
        } else {
            oqlTerm.equals("_account", borrower);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + borrower.getUsername());
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(), pageable, sortable, params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }
    
    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, Collection<BrokerageState> states) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(borrower);
        params.addAll(states);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    getStatesTerm(oqlTerm, states));
        } else {
             oqlTerm.and(
                     oqlTerm.equals("_account", borrower));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + borrower.getUsername());
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    public Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, BrokerageBank bank, Collection<BrokerageState> states) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(borrower);
        params.add( bank);
        params.addAll(states);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    oqlTerm.equals("_brokerageBank", bank),
                    getStatesTerm(oqlTerm, states));
        } else {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    oqlTerm.equals("_brokerageBank", bank));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + borrower.getUsername());
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationList(Account borrower, BrokerageBank bank, Collection<BrokerageState> states, Date startSearch, Date endSearch) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(borrower);
        params.add( bank);
        params.addAll(states);
        params.add( startSearch);
        params.add(endSearch);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    oqlTerm.equals("_brokerageBank", bank),
                    getStatesTerm(oqlTerm, states),
                    oqlTerm.greaterThanEquals("_creationDate", startSearch),
                    oqlTerm.lessThan("_creationDate", endSearch));
        } else {
            oqlTerm.and(
                    oqlTerm.equals("_account", borrower),
                    oqlTerm.equals("_brokerageBank", bank),
                    oqlTerm.greaterThanEquals("_creationDate", startSearch),
                    oqlTerm.lessThan("_creationDate", endSearch));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + borrower.getUsername());
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    public Collection<BrokerageApplication> findBrokerageApplicationList(BrokerageBank bank, Collection<BrokerageState> states, Date creationDate, Date endDate) {

        Collection<Object> params = new ArrayList<Object>();
        params.add( bank);
        params.add(creationDate);
        params.add( endDate);
        params.addAll(states);

        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_brokerageBank", bank),
                    oqlTerm.greaterThanEquals("_creationDate", creationDate),
                    oqlTerm.lessThan("_creationDate", endDate),
                    getStatesTerm(oqlTerm, states));
        } else {
             oqlTerm.and(
                     oqlTerm.equals("_brokerageBank", bank),
                     oqlTerm.greaterThanEquals("_creationDate", creationDate),
                     oqlTerm.lessThan("_creationDate", endDate));
        }
        
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }
    
    /**
     * Gets the states term for finding offers and orders.
     */
    private OqlTerm getStatesTerm(OqlTerm oqlTerm, Collection<BrokerageState> states) {
        OqlTerm result;
        if (states.size() > 1) {
            OqlTerm [] orStates = new OqlTerm[states.size()];
            int i = 0;
            for (BrokerageState state : states) {
                orStates[i] = oqlTerm.equals("_state", state);
                i++;
            }
            result = oqlTerm.or(orStates);
        } else if (states.size() == 1) {
            result = oqlTerm.equals("_state", states.iterator().next());
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }
        return result;
    }

    
    public BrokerageApplication findBrokerageApplicationList(BrokerageBank bank, String extRefNumber) throws InvalidValueException {

        final Map<String, Object> params = new HashMap<String, Object>(0);

        String sql = "select distinct ba.id as id from brokerage_application ba where ba.brokerage_bank_id = " +  bank.getId() 
                            + " and ba.ext_ref_number like '%" + extRefNumber+ "%'";
        
        
        Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setClass(BrokerageApplication.class);
        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> bas = (Collection<BrokerageApplication>) query.executeWithMap(params);
        
        
        BrokerageApplication ret = null;
        for ( BrokerageApplication ba :  bas){

            if (ret != null){
                throw new InvalidValueException("multiple entries. extRefNumber" + extRefNumber);
            }
            ret = ba;
        }
        
        return ret;
       
    }

    public BrokerageApplication findBrokerageApplicationListWithMatchingExtRef(BrokerageBank bank, String extRefNumber) throws InvalidValueException {

        final Map<String, Object> params = new HashMap<String, Object>(0);

        String sql = "select distinct ba.id as id from brokerage_application ba where ba.brokerage_bank_id = " +  bank.getId()
                + " and ba.ext_ref_number = '" + extRefNumber+ "'";


        Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setClass(BrokerageApplication.class);
        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> bas = (Collection<BrokerageApplication>) query.executeWithMap(params);


        BrokerageApplication ret = null;
        for ( BrokerageApplication ba :  bas){

            if (ret != null){
                throw new InvalidValueException("multiple entries. extRefNumber" + extRefNumber);
            }
            ret = ba;
        }

        return ret;
    }
    
    
    public Collection<BrokerageApplication> findBrokerageApplicationListWithoutEmail(BrokerageBank bank, Collection<BrokerageState> states, Date minDate, Date maxDate) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(bank);
        params.add(minDate);
        params.add(maxDate);
        params.addAll(states);

        OqlTerm oqlTerm = OqlTerm.newTerm();
        
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.isNull("_emailCreated"),
                    oqlTerm.equals("_brokerageBank", bank),
                    oqlTerm.greaterThanEquals("_creationDate", minDate),
                    oqlTerm.lessThan("_creationDate", maxDate),
                    getStatesTerm(oqlTerm, states));
        } else {
             oqlTerm.and(
                     oqlTerm.isNull("_emailCreated"),
                     oqlTerm.equals("_brokerageBank", bank),
                     oqlTerm.greaterThanEquals("_creationDate", minDate),
                     oqlTerm.lessThan("_creationDate", maxDate));
        }
        
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }


    @Override
    public Collection<BrokerageApplication> getNewestBrokerageApplicationPerBank( Account account, Collection<BrokerageState> states, int maxAgeInDays, boolean onlyVisibleOnes ){
        StringBuffer sql =  new StringBuffer();

        String statesinSql = "";

        String visibility ="";
        if ( onlyVisibleOnes){
            visibility = " and visible = true ";
        }
        if ( states!= null){

            for (BrokerageState state:states){
                if ( statesinSql.length() == 0){
                    statesinSql = " and brokerage_application.state in(" + "'" +state+"'";
                }
                statesinSql += ",'" +state+"'";
            }

            statesinSql += ")";
        }
        sql.append("SELECT DISTINCT(ba.id) as id FROM ( "
                + " SELECT brokerage_application.id as id, "

                + " RANK() OVER (PARTITION BY brokerage_bank_id ORDER BY brokerage_application.creation_date DESC) dest_rank"
                + " FROM brokerage_application left JOIN loan_application as la on brokerage_application.loan_application_id = la.id"
                + " where  brokerage_application.creation_date >= :maxAge "
                + statesinSql
                + visibility
                + " and brokerage_application.account_id= " + account.getId() + ")"
                + "as ba where dest_rank = 1 ");

       
        Date now = CurrentDate.getDate();
        
        Date maxAge = DateUtils.addDays(now, -maxAgeInDays);
       
        
        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(BrokerageApplication.class);

        Map<String, Object> parameters = new HashMap<String, Object>(2);
        parameters.put("maxAge", new Timestamp(maxAge.getTime()));


        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> bas = (Collection<BrokerageApplication>) query.executeWithMap(parameters);
        

        return bas;
    }

    /**
     * OqlTerm based implementation of bulk lookup by ids.
     *
     * @param brokerageApplications - list of BA ids
     * @return list of matching BAs
     */
    @Override
    public List<BrokerageApplication> findApplications(List<Long> brokerageApplications) {
        OqlTerm oqlTerm = OqlTerm.newTerm();
        List <OqlTerm> terms = new ArrayList<OqlTerm>();
        if (brokerageApplications.size() > 1) {
            for (Long id : brokerageApplications) {
                terms.add(oqlTerm.equals("_id",id));
            }
            oqlTerm.or(terms.toArray(new OqlTerm[terms.size()]));
        } else if (brokerageApplications.size() == 1) {
            oqlTerm.equals("_id",brokerageApplications.get(0));
        }

        return (List<BrokerageApplication>) findEntities(BrokerageApplication.class, oqlTerm.toString());
    }

    public Collection<BrokerageApplication> findBrokerageApplicationList(Account account) {
        String term = " _account != null && _account._id == " + account.getId();

        return findBrokerageApplicationList(term);
    }

    @Override
    public BrokerageApplication getLatestBrokerageApplicationByPersonAndBrokerageBank(Person person, BrokerageBank brokerageBank) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "findLatestByFirstPerson");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("personId", person.getId());
        parameters.put("brokerageBankId", brokerageBank.getId());
        List<BrokerageApplication> result = (List<BrokerageApplication>)query.executeWithMap(parameters);

        BrokerageApplication ret = null;
        if (result.size() > 0) {
            ret = result.get(0);
        } else {
            Query query2 = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "findLatestBySecondPerson");
            List<BrokerageApplication> result2 = (List<BrokerageApplication>)query2.executeWithMap(parameters);

            if (result2.size() > 0) {
                ret = result2.get(0);
            }
        }

        return ret;
    }

    @Override
    public Collection<BrokerageApplication> getBrokerageApplicationListByAccountIdAndState(Long accountId, String brokerageState) {

        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "getBrokerageApplicationListByAccountIdAndState");

        //Map<String, Object> params = new HashMap<String, Object>();
        //params.put("accountId", accountId);
        //params.put("brokerageState", brokerageState);

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, accountId);
        parameters.put(2, brokerageState);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> queryResult = (List<BrokerageApplication>) query.executeWithMap(parameters);
        return queryResult;
    }

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplicationBankIdAndByExtRef(Long brokerageBankId, String extRefNumber) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationBankIdAndByExtRef");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageBankId", brokerageBankId);
        params.put("extRefNumber", extRefNumber);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> brokerageApplications = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return brokerageApplications;
    }

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplicationByAccountAndBankInAndStateInDescOrder(Long accountId,
                                                                                                           List<BrokerageBank> brokerageBanks,
                                                                                                           List<BrokerageState> brokerageStates) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationByAccountAndBankInAndStateInDescOrder");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("accountId", accountId);
        params.put("brokerageBanks", brokerageBanks);
        params.put("brokerageStates", brokerageStates);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> customerUploadedDocs = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    @Override
    public BrokerageApplication getLastInsertedBrokerageApplication() throws InvalidValueException {

        final Map<String, Object> params = new HashMap<String, Object>(0);
        String sql = "SELECT * FROM brokerage_application ORDER BY id DESC LIMIT 1";
        Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setClass(BrokerageApplication.class);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> bas = (Collection<BrokerageApplication>) query.executeWithMap(params);

        if(bas.isEmpty()){
            throw new  InvalidValueException("Last inserted Brokerage Application was not found");
        }

        BrokerageApplication returned = null;

        for (BrokerageApplication ba :  bas){

            if (returned != null){
                throw new InvalidValueException("multiple entries for last id");
            }
            returned = ba;
        }

        return returned;
    }

    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationListForLAs(Collection<LoanApplication> loanApplications, Collection<BrokerageState> brokerageStates) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationByLaInAndStateInDescOrder");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("las", loanApplications);
        params.put("brokerageStates", brokerageStates);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> result = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return result;
    }


    @Override
    public BrokerageApplication findSuccessfulBAByAccount(Account account) {
        String sql = "SELECT ba.id as id , ba.creation_date as \"creationDate\", ba.brokerage_bank_id as \"_brokerageBank\", ba.ext_ref_number as \"_extRefNumber\", ba.state as _state, \n" +
                " ba.account_id as _account, ba.economical_data_id as \"_economicalData\", ba.first_person_id as \"_firstPerson\", ba.second_person_id as \"_secondPerson\", \n" +
                " ba.reference_bank_account_id as \"_referenceBankAccount\", ba.requested_rdi_type as \"_requestedRdiType\", ba.requested_amount as \"_requestedAmount\", \n" +
                " ba.reqeusted_category_id as \"_reqeustedCategory\", ba.requested_duration as \"_requestedDuration\", ba.monthly_rate as \"_monthlyRate\", ba.effective_interest as \"_effectiveInterest\", \n" +
                " ba.amount as _amount, ba.duration as _duration, ba.rdi_type as \"_rdiType\", ba.brokerage_application_filter_reason_id as \"_brokerageApplicationFilterReason\", \n" +
                " ba.loan_application_id as \"_loanApplication\", ba.requested_loan_type as \"_requestedLoanType\", ba.pap_sale_tracked_date as \"_papSaleTrackedDate\", \n" +
                " ba.last_state_request as \"_lastStateRequest\", ba.last_state_change as \"_lastStateChange\", ba.document_container_id as \"_documentContainer\", \n" +
                " ba.email_created as \"_emailCreated\", ba.brokerage_car_finance_id as \"_brokerageCarFinance\", ba.prev_ext_ref_number as \"_prevExtRefNumber\", \n" +
                " ba.info as \"_additionalData\", ba.documents_requested as \"_documentsRequested\", ba.documents_sent as \"_documentsSent\", ba.email_type as \"_emailType\", ba.payout_date as \"_payoutDate\"" +
                " from public.brokerage_application ba \n" +
                "JOIN brokerage.brokerage_journal ON brokerage.brokerage_journal.brokerage_application_id = ba.id\n" +
                "AND brokerage.brokerage_journal.type = 'ADVISORY_SUCCESSFUL'\n" +
                "AND ba.state != 'APPLICATION_DENIED'\n" +
                "AND ba.account_id= " + account.getCustomerNumber() + "  " +
                "ORDER BY brokerage.brokerage_journal.creation_date DESC " +
                "LIMIT 1 ;";
        Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setClass(BrokerageApplication.class);
        Collection<BrokerageApplication> brokerageAppList = (Collection<BrokerageApplication>)query.execute();
        if (brokerageAppList!=null && brokerageAppList.size()>0){
            return brokerageAppList.iterator().next();
        }
        return null;
    }

}
