//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bucket)}
import de.smava.webapp.brokerage.domain.history.BrokerageBucketHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBuckets'.
 *
 * A brokerage bucket is a virtual collection of brokerage applications that are requested at a the same time.
 *
 * @author generator
 */
public class BrokerageBucket extends BrokerageBucketHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bucket)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _executionDate;
        protected Boolean _processed;
        protected Integer _number;
        protected Set<BrokerageBucketBrokerageApplication> _brokerageBucketBrokerageApplications;
        protected LoanApplication _loanApplication;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'execution date'.
     *
     * 
     *
     */
    public void setExecutionDate(Date executionDate) {
        if (!_executionDateIsSet) {
            _executionDateIsSet = true;
            _executionDateInitVal = getExecutionDate();
        }
        registerChange("execution date", _executionDateInitVal, executionDate);
        _executionDate = executionDate;
    }
                        
    /**
     * Returns the property 'execution date'.
     *
     * 
     *
     */
    public Date getExecutionDate() {
        return _executionDate;
    }
                                            
    /**
     * Setter for the property 'number'.
     *
     * 
     *
     */
    public void setNumber(Integer number) {
        _number = number;
    }
            
    /**
     * Returns the property 'number'.
     *
     * 
     *
     */
    public Integer getNumber() {
        return _number;
    }

    /**
     * Setter for the property 'processed'.
     *
     *
     *
     */
    public void setProcessed(Boolean processed) {
        _processed = processed;
    }
    
    /**
     * Returns the property 'processed'.
     *
     *
     *
     */    
    public Boolean isPocessed() {
        return _processed;
    }

    /**
     * Setter for the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    public void setBrokerageBucketBrokerageApplications(Set<BrokerageBucketBrokerageApplication> brokerageBucketBrokerageApplications) {
        _brokerageBucketBrokerageApplications = brokerageBucketBrokerageApplications;
    }
            
    /**
     * Returns the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    public Set<BrokerageBucketBrokerageApplication> getBrokerageBucketBrokerageApplications() {
        return _brokerageBucketBrokerageApplications;
    }
                                            
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public LoanApplication getLoanApplication() {
        return _loanApplication;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBucket.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageBucket.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBucket asBrokerageBucket() {
        return this;
    }
}
