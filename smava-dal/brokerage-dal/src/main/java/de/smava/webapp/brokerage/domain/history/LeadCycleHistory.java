package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractLeadCycle;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'LeadCycles'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class LeadCycleHistory extends AbstractLeadCycle {

    protected transient Date _lastTouchpointInitVal;
    protected transient boolean _lastTouchpointIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _stateChangedDateInitVal;
    protected transient boolean _stateChangedDateIsSet;


				
    /**
     * Returns the initial value of the property 'last touchpoint'.
     */
    public Date lastTouchpointInitVal() {
        Date result;
        if (_lastTouchpointIsSet) {
            result = _lastTouchpointInitVal;
        } else {
            result = getLastTouchpoint();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last touchpoint'.
     */
    public boolean lastTouchpointIsDirty() {
        return !valuesAreEqual(lastTouchpointInitVal(), getLastTouchpoint());
    }

    /**
     * Returns true if the setter method was called for the property 'last touchpoint'.
     */
    public boolean lastTouchpointIsSet() {
        return _lastTouchpointIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state changed date'.
     */
    public Date stateChangedDateInitVal() {
        Date result;
        if (_stateChangedDateIsSet) {
            result = _stateChangedDateInitVal;
        } else {
            result = getStateChangedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state changed date'.
     */
    public boolean stateChangedDateIsDirty() {
        return !valuesAreEqual(stateChangedDateInitVal(), getStateChangedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'state changed date'.
     */
    public boolean stateChangedDateIsSet() {
        return _stateChangedDateIsSet;
    }
		
}
