//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.loancheck.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan check request)}

import de.smava.webapp.brokerage.loancheck.domain.LoanCheckRequest;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'LoanCheckRequests'.
 *
 * @author generator
 */
public interface LoanCheckRequestDao extends BaseDao<LoanCheckRequest> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the loan check request identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    LoanCheckRequest getLoanCheckRequest(Long id);

    /**
     * Saves the loan check request.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveLoanCheckRequest(LoanCheckRequest loanCheckRequest);

    /**
     * Deletes an loan check request, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the loan check request
     */
    void deleteLoanCheckRequest(Long id);

    /**
     * Retrieves all 'LoanCheckRequest' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<LoanCheckRequest> getLoanCheckRequestList();

    /**
     * Retrieves a page of 'LoanCheckRequest' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<LoanCheckRequest> getLoanCheckRequestList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'LoanCheckRequest' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<LoanCheckRequest> getLoanCheckRequestList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'LoanCheckRequest' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<LoanCheckRequest> getLoanCheckRequestList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'LoanCheckRequest' instances.
     */
    long getLoanCheckRequestCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(loan check request)}
    //
    // insert custom methods here
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
