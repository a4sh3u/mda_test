//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage entity map)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.brokerage.dao.BrokerageEntityMapDao;
import de.smava.webapp.brokerage.domain.BrokerageEntityMap;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BrokerageEntityMaps'.
 *
 * @author generator
 */
@Repository(value = "brokerageEntityMapDao")
public class JdoBrokerageEntityMapDao extends JdoBaseDao implements BrokerageEntityMapDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageEntityMapDao.class);

    private static final String CLASS_NAME = "BrokerageEntityMap";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(brokerage entity map)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the brokerage entity map identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BrokerageEntityMap load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BrokerageEntityMap result = getEntity(BrokerageEntityMap.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BrokerageEntityMap getBrokerageEntityMap(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BrokerageEntityMap entity = findUniqueEntity(BrokerageEntityMap.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the brokerage entity map.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BrokerageEntityMap brokerageEntityMap) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageEntityMap: " + brokerageEntityMap);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(brokerage entity map)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(brokerageEntityMap);
    }

    /**
     * @deprecated Use {@link #save(BrokerageEntityMap) instead}
     */
    public Long saveBrokerageEntityMap(BrokerageEntityMap brokerageEntityMap) {
        return save(brokerageEntityMap);
    }

    /**
     * Deletes an brokerage entity map, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage entity map
     */
    public void deleteBrokerageEntityMap(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBrokerageEntityMap: " + id);
        }
        deleteEntity(BrokerageEntityMap.class, id);
    }

    /**
     * Retrieves all 'BrokerageEntityMap' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BrokerageEntityMap> getBrokerageEntityMapList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BrokerageEntityMap> result = getEntities(BrokerageEntityMap.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BrokerageEntityMap' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BrokerageEntityMap> getBrokerageEntityMapList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageEntityMap> result = getEntities(BrokerageEntityMap.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageEntityMap' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BrokerageEntityMap> getBrokerageEntityMapList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageEntityMap> result = getEntities(BrokerageEntityMap.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageEntityMap' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BrokerageEntityMap> getBrokerageEntityMapList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageEntityMap> result = getEntities(BrokerageEntityMap.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageEntityMap' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageEntityMap> findBrokerageEntityMapList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BrokerageEntityMap> result = findEntities(BrokerageEntityMap.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageEntityMap' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BrokerageEntityMap> findBrokerageEntityMapList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BrokerageEntityMap> result = findEntities(BrokerageEntityMap.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BrokerageEntityMap' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageEntityMap> findBrokerageEntityMapList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageEntityMap> result = findEntities(BrokerageEntityMap.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageEntityMap' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageEntityMap> findBrokerageEntityMapList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageEntityMap> result = findEntities(BrokerageEntityMap.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageEntityMap' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageEntityMap> findBrokerageEntityMapList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageEntityMap> result = findEntities(BrokerageEntityMap.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageEntityMap' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageEntityMap> findBrokerageEntityMapList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageEntityMap> result = findEntities(BrokerageEntityMap.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageEntityMap' instances.
     */
    public long getBrokerageEntityMapCount() {
        long result = getEntityCount(BrokerageEntityMap.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageEntityMap' instances which match the given whereClause.
     */
    public long getBrokerageEntityMapCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BrokerageEntityMap.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageEntityMap' instances which match the given whereClause together with params specified in object array.
     */
    public long getBrokerageEntityMapCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BrokerageEntityMap.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage entity map)}
    //

    @Override
    @SuppressWarnings("unchecked")
    public BrokerageEntityMap getLatestMap(Long kreditPrivatId, Long brokerageId, String type) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageEntityMap.class, "getLatestMap");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("kreditPrivatId", kreditPrivatId);
        parameters.put("brokerageId", brokerageId);
        parameters.put("type", type);

        Collection<BrokerageEntityMap> brokerageEntityMaps = (Collection<BrokerageEntityMap>)query.executeWithMap(parameters);
        if (brokerageEntityMaps!= null && !brokerageEntityMaps.isEmpty()) {
            if (brokerageEntityMaps.size()> 1){
                LOGGER.fatal("There are multiple mappings for " +
                        type +
                        " entity with either kredir privat id " +
                        kreditPrivatId +
                        " or brokerage id " +
                        brokerageId +
                        ". Returning the latest one by db id.");
            }
            return brokerageEntityMaps.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public BrokerageEntityMap getByNewEntity(Long brokerageId, String type) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageEntityMap.class, "getOldEntityId");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("brokerageId", brokerageId);
        parameters.put("type", type);

        Collection<BrokerageEntityMap> brokerageEntityMaps = (Collection<BrokerageEntityMap>)query.executeWithMap(parameters);
        if (brokerageEntityMaps!= null && !brokerageEntityMaps.isEmpty()) {
            if (brokerageEntityMaps.size()> 1){
                LOGGER.fatal("There are multiple mappings for " +
                        type +
                        " entity with brokerage id " +
                        brokerageId +
                        ". Returning the latest one by db id.");
            }
            return brokerageEntityMaps.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public BrokerageEntityMap getByOldEntity(Long kreditPrivatId, String type) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageEntityMap.class, "getNewEntityId");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("kreditPrivatId", kreditPrivatId);
        parameters.put("type", type);

        Collection<BrokerageEntityMap> brokerageEntityMaps = (Collection<BrokerageEntityMap>)query.executeWithMap(parameters);
        if (brokerageEntityMaps!= null && !brokerageEntityMaps.isEmpty()) {
            if (brokerageEntityMaps.size()> 1){
                LOGGER.fatal("There are multiple mappings for " +
                        type +
                        " entity with kreditprivat id " +
                        kreditPrivatId +
                        ". Returning the latest one by db id.");
            }
            return brokerageEntityMaps.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Long getOldEntityId(Long brokerageId, String type) {
        BrokerageEntityMap brokerageEntityMap = getByNewEntity(brokerageId, type);
        if (brokerageEntityMap!= null) {
            return brokerageEntityMap.getKreditPrivatId();
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Long getNewEntityId(Long kreditPrivatId, String type) {
        BrokerageEntityMap brokerageEntityMap = getByOldEntity(kreditPrivatId, type);
        if (brokerageEntityMap!= null) {
            return brokerageEntityMap.getBrokerageId();
        } else {
            return null;
        }
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
