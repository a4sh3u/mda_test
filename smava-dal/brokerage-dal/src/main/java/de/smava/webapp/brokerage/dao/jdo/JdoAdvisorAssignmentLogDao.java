package de.smava.webapp.brokerage.dao.jdo;

import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.brokerage.dao.AdvisorAssignmentLogDao;
import de.smava.webapp.brokerage.domain.AdvisorAssignmentLog;
import de.smava.webapp.brokerage.dto.AdvisorPickingCounterTO;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * DAO implementation for the domain object 'AdvisorAssignmentLogs'.
 */
@Repository
public class JdoAdvisorAssignmentLogDao extends JdoGenericDao<AdvisorAssignmentLog> implements AdvisorAssignmentLogDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAdvisorAssignmentLogDao.class);

    private static final String CLASS_NAME = "AdvisorAssignmentLog";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    /**
     * Returns an attached copy of the advisor assignment log identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public AdvisorAssignmentLog load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        AdvisorAssignmentLog result = getEntity(AdvisorAssignmentLog.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	AdvisorAssignmentLog entity = findUniqueEntity(AdvisorAssignmentLog.class, "_id == " + id);
	    	result = entity != null;

	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the advisor assignment log.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(AdvisorAssignmentLog advisorAssignmentLog) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAdvisorAssignmentLog: " + advisorAssignmentLog);
        }
        return saveEntity(advisorAssignmentLog);
    }

    @Override
    @SuppressWarnings("unchecked")
    public AdvisorAssignmentLog findCurrentAdvisorAssignmentLogForBorrower(Long customerNumber) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorAssignmentLog.class, "findCurrentAdvisorAssignmentLogForBorrower");

        Timestamp timestamp = new Timestamp(CurrentDate.getTime());

        Collection<AdvisorAssignmentLog> queryResult =
                (Collection<AdvisorAssignmentLog>) query.execute(customerNumber, timestamp);
        if (!queryResult.isEmpty()) {
            return queryResult.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    public Long countFirstCallsByAdvisorAndDate(Long advisorId, Date date) {
        Date begin = DateUtils.getZeroTimeDate(date);
        Date end = DateUtils.getTheEndOfTheDay(date);
        Query query = buildFirstCallsQueryWithAdvisorAndDateParameters();
        query.setResult("count(this)");
        return (Long) query.execute(advisorId, begin, end);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Long> findPickedCustomersByAdvisorAndDate(Long advisorId, Date date) {
        Date begin = DateUtils.getZeroTimeDate(date);
        Date end = DateUtils.getTheEndOfTheDay(date);
        Query query = buildFirstCallsQueryWithAdvisorAndDateParameters();
        query.setResult("this._customerNumber");
        query.setResultClass(Long.class);
        return (List<Long>) query.execute(advisorId, begin, end);
    }

    private Query buildFirstCallsQueryWithAdvisorAndDateParameters(){
        Query query = getPersistenceManager().newQuery(AdvisorAssignmentLog.class);
        query.setFilter("this._creditAdvisor._id == :advisorId && this._customerType == 'FIRST_CALL'" +
                " && this._assignmentDate > :begin && this._assignmentDate <= :end");
        return query;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<AdvisorPickingCounterTO> countFirstCallForAllAdvisors(Date date) {
        Date begin = DateUtils.getZeroTimeDate(date);
        Date end = DateUtils.getTheEndOfTheDay(date);
        Query query = getPersistenceManager().newQuery(AdvisorAssignmentLog.class);
        query.setFilter("this._customerType == 'FIRST_CALL' && this._assignmentDate > :begin && this._assignmentDate <= :end");
        query.setGrouping("this._creditAdvisor._id");
        query.setResult("this._creditAdvisor._id as advisorId, count(this._customerNumber) as pickingCounter");
        query.setResultClass(AdvisorPickingCounterTO.class);
        return (List<AdvisorPickingCounterTO>) query.execute(begin, end);
    }
}
