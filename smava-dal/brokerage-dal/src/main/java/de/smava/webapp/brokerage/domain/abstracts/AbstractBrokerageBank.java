//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank)}

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.brokerage.domain.interfaces.BrokerageBankEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBanks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 *
 *
 * @author generator
 */
public abstract class AbstractBrokerageBank
        extends KreditPrivatEntity implements BrokerageBankEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageBank.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage bank)}

    public static final String BANK_SMAVA_PRIVAT = "smavaprivat";
    public static final String BANK_BARCLAYS = "barclays";
    public static final String BANK_SWK = "swk";
    public static final String BANK_POSTBANK = "postbank";
    public static final String BANK_PSD = "psd";
    public static final String BANK_KREDIT2GO = "kredit2go";
    public static final String BANK_TARGOBANK = "targobank";
    public static final String BANK_CREDITEUROPE = "crediteurope";
    public static final String BANK_EASYCREDIT = "easycredit";
    public static final String BANK_INGDIBA = "ingdiba";
    public static final String BANK_CREDITPLUS = "creditplus";
    public static final String BANK_CARCREDIT = "santander";
    public static final String BANK_SANTANDER_BESTCREDIT = "santanderbestcredit";
    public static final String BANK_ONLINEKREDIT = "onlinekredit";
    public static final String BANK_DKB = "dkb";
    public static final String BANK_DSL = "dsl";
    public static final String BANK_SKG = "skg";
    public static final String BANK_DEUTSCHEBANK = "deutschebank";
    public static final String BANK_NORISBANK = "norisbank";
    public static final String BANK_ABK = "abk";
    public static final String BANK_WUNSCHKREDIT = "wunschkredit";
    public static final String BANK_BANKOFSCOTLAND = "bankofscotland";
    public static final String BANK_KREDIT_PRIVAT = "kreditprivat";
    public static final String BANK_OYAKANKER = "oyakankerbank";
    public static final String BANK_11 = "bank11";
    public static final String BANK_AUXMONEY = "auxmoney";
    public static final String BANK_VONESSEN = "vonessen";
    public static final String BANK_POSTBANK_BUSINESS_CREDIT = "postbankbusinesscredit";
    public static final String BANK_VONESSEN_SUBPRIME = "vonessensubprime";
    public static final String BANK_ADAC = "adac";
    public static final String BANK_KREDIT2DAY = "kredit2day";

    @SuppressWarnings("unchecked")
    public static final Set<String> BANKS = ConstCollector.getAll("BANK_");

    public static final List<String> EXTERNAL_BANKS_LIST = Arrays.asList(BANK_11, BANK_OYAKANKER, BANK_AUXMONEY, BANK_VONESSEN, BANK_VONESSEN_SUBPRIME);

    // !!!!!!!! End of insert code section !!!!!!!!
}
