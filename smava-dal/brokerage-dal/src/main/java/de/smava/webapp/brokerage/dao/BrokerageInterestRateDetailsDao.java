//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage interest rate details)}

import de.smava.webapp.brokerage.domain.BrokerageInterestRateDetails;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageInterestRateDetailss'.
 *
 * @author generator
 */
public interface BrokerageInterestRateDetailsDao extends BaseDao<BrokerageInterestRateDetails> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage interest rate details identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageInterestRateDetails getBrokerageInterestRateDetails(Long id);

    /**
     * Saves the brokerage interest rate details.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageInterestRateDetails(BrokerageInterestRateDetails brokerageInterestRateDetails);

    /**
     * Deletes an brokerage interest rate details, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage interest rate details
     */
    void deleteBrokerageInterestRateDetails(Long id);

    /**
     * Retrieves all 'BrokerageInterestRateDetails' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList();

    /**
     * Retrieves a page of 'BrokerageInterestRateDetails' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageInterestRateDetails' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageInterestRateDetails' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageInterestRateDetails> getBrokerageInterestRateDetailsList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageInterestRateDetails' instances.
     */
    long getBrokerageInterestRateDetailsCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage interest rate details)}

    public BrokerageInterestRateDetails findCurrentlyActiveBrokerageInterestRateDetails();

    BrokerageInterestRateDetails findCurrentlyActiveInterestRateDetails(Date currentDate);

    // !!!!!!!! End of insert code section !!!!!!!!
}
