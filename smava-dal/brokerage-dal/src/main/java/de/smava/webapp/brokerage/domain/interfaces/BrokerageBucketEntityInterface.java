package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageBucket;
import de.smava.webapp.brokerage.domain.BrokerageBucketBrokerageApplication;
import de.smava.webapp.brokerage.domain.LoanApplication;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageBuckets'.
 *
 * @author generator
 */
public interface BrokerageBucketEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'execution date'.
     *
     * 
     *
     */
    void setExecutionDate(Date executionDate);

    /**
     * Returns the property 'execution date'.
     *
     * 
     *
     */
    Date getExecutionDate();
    /**
     * Setter for the property 'number'.
     *
     * 
     *
     */
    void setNumber(Integer number);

    /**
     * Returns the property 'number'.
     *
     * 
     *
     */
    Integer getNumber();
    /**
     * Setter for the property 'processed'.
     *
     *
     *
     */
    void setProcessed(Boolean processed);

    /**
     * Returns the property 'processed'.
     *
     *
     *
     */
    Boolean isPocessed();
    
    /**
     * Setter for the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    void setBrokerageBucketBrokerageApplications(Set<BrokerageBucketBrokerageApplication> brokerageBucketBrokerageApplications);

    /**
     * Returns the property 'brokerage bucket brokerage applications'.
     *
     * 
     *
     */
    Set<BrokerageBucketBrokerageApplication> getBrokerageBucketBrokerageApplications();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    LoanApplication getLoanApplication();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBucket asBrokerageBucket();
}
