package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageMappingConverter;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageMappingConverters'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageMappingConverterHistory extends AbstractBrokerageMappingConverter {

    protected transient String _valueInitVal;
    protected transient boolean _valueIsSet;


	
    /**
     * Returns the initial value of the property 'value'.
     */
    public String valueInitVal() {
        String result;
        if (_valueIsSet) {
            result = _valueInitVal;
        } else {
            result = getValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'value'.
     */
    public boolean valueIsDirty() {
        return !valuesAreEqual(valueInitVal(), getValue());
    }

    /**
     * Returns true if the setter method was called for the property 'value'.
     */
    public boolean valueIsSet() {
        return _valueIsSet;
    }
	
}
