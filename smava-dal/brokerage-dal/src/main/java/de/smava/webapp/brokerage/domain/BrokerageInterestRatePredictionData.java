//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage interest rate prediction data)}
import de.smava.webapp.brokerage.domain.history.BrokerageInterestRatePredictionDataHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageInterestRatePredictionDatas'.
 *
 * A brokerage interest rate prediction data is a virtual collection of data which indicate (based of certain criteria) a rate of brokerage (loan) interest.
 *
 * @author generator
 */
public class BrokerageInterestRatePredictionData extends BrokerageInterestRatePredictionDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage interest rate prediction data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageInterestRatePrediction _brokerageInterestRatePrediction;
        protected BrokerageBank _brokerageBank;
        protected RequestedLoanType _requestedLoanType;
        protected Double _requestedAmountMin;
        protected Double _requestedAmountMax;
        protected Integer _requestedDurationMin;
        protected Integer _requestedDurationMax;
        protected String _employment;
        protected Double _incomeMin;
        protected Double _incomeMax;
        protected Double _leverageMin;
        protected Double _leverageMax;
        protected Boolean _schufaAvailable;
        protected Integer _schufaMin;
        protected Integer _schufaMax;
        protected Double _factor;
        
                                    
    /**
     * Setter for the property 'brokerage interest rate prediction'.
     *
     * 
     *
     */
    public void setBrokerageInterestRatePrediction(BrokerageInterestRatePrediction brokerageInterestRatePrediction) {
        _brokerageInterestRatePrediction = brokerageInterestRatePrediction;
    }
            
    /**
     * Returns the property 'brokerage interest rate prediction'.
     *
     * 
     *
     */
    public BrokerageInterestRatePrediction getBrokerageInterestRatePrediction() {
        return _brokerageInterestRatePrediction;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    public void setRequestedLoanType(RequestedLoanType requestedLoanType) {
        _requestedLoanType = requestedLoanType;
    }
            
    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    public RequestedLoanType getRequestedLoanType() {
        return _requestedLoanType;
    }
                                            
    /**
     * Setter for the property 'requested amount min'.
     *
     * 
     *
     */
    public void setRequestedAmountMin(Double requestedAmountMin) {
        _requestedAmountMin = requestedAmountMin;
    }
            
    /**
     * Returns the property 'requested amount min'.
     *
     * 
     *
     */
    public Double getRequestedAmountMin() {
        return _requestedAmountMin;
    }
                                            
    /**
     * Setter for the property 'requested amount max'.
     *
     * 
     *
     */
    public void setRequestedAmountMax(Double requestedAmountMax) {
        _requestedAmountMax = requestedAmountMax;
    }
            
    /**
     * Returns the property 'requested amount max'.
     *
     * 
     *
     */
    public Double getRequestedAmountMax() {
        return _requestedAmountMax;
    }
                                            
    /**
     * Setter for the property 'requested duration min'.
     *
     * 
     *
     */
    public void setRequestedDurationMin(Integer requestedDurationMin) {
        _requestedDurationMin = requestedDurationMin;
    }
            
    /**
     * Returns the property 'requested duration min'.
     *
     * 
     *
     */
    public Integer getRequestedDurationMin() {
        return _requestedDurationMin;
    }
                                            
    /**
     * Setter for the property 'requested duration max'.
     *
     * 
     *
     */
    public void setRequestedDurationMax(Integer requestedDurationMax) {
        _requestedDurationMax = requestedDurationMax;
    }
            
    /**
     * Returns the property 'requested duration max'.
     *
     * 
     *
     */
    public Integer getRequestedDurationMax() {
        return _requestedDurationMax;
    }
                                    /**
     * Setter for the property 'employment'.
     *
     * First applicant employment.
     *
     */
    public void setEmployment(String employment) {
        if (!_employmentIsSet) {
            _employmentIsSet = true;
            _employmentInitVal = getEmployment();
        }
        registerChange("employment", _employmentInitVal, employment);
        _employment = employment;
    }
                        
    /**
     * Returns the property 'employment'.
     *
     * First applicant employment.
     *
     */
    public String getEmployment() {
        return _employment;
    }
                                            
    /**
     * Setter for the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    public void setIncomeMin(Double incomeMin) {
        _incomeMin = incomeMin;
    }
            
    /**
     * Returns the property 'income min'.
     *
     * First applicant income MIN.
     *
     */
    public Double getIncomeMin() {
        return _incomeMin;
    }
                                            
    /**
     * Setter for the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    public void setIncomeMax(Double incomeMax) {
        _incomeMax = incomeMax;
    }
            
    /**
     * Returns the property 'income max'.
     *
     * First applicant income MAX.
     *
     */
    public Double getIncomeMax() {
        return _incomeMax;
    }
                                            
    /**
     * Setter for the property 'leverage min'.
     *
     * First applicant leverage MIN (Verschuldungsgrad).
     *
     */
    public void setLeverageMin(Double leverageMin) {
        _leverageMin = leverageMin;
    }
            
    /**
     * Returns the property 'leverage min'.
     *
     * First applicant leverage MIN (Verschuldungsgrad).
     *
     */
    public Double getLeverageMin() {
        return _leverageMin;
    }
                                            
    /**
     * Setter for the property 'leverage max'.
     *
     * First applicant leverage MAX (Verschuldungsgrad).
     *
     */
    public void setLeverageMax(Double leverageMax) {
        _leverageMax = leverageMax;
    }
            
    /**
     * Returns the property 'leverage max'.
     *
     * First applicant leverage MAX (Verschuldungsgrad).
     *
     */
    public Double getLeverageMax() {
        return _leverageMax;
    }
                                            
    /**
     * Setter for the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    public void setSchufaAvailable(Boolean schufaAvailable) {
        _schufaAvailable = schufaAvailable;
    }
            
    /**
     * Returns the property 'schufa available'.
     *
     * First applicant schufa available.
     *
     */
    public Boolean getSchufaAvailable() {
        return _schufaAvailable;
    }
                                            
    /**
     * Setter for the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    public void setSchufaMin(Integer schufaMin) {
        _schufaMin = schufaMin;
    }
            
    /**
     * Returns the property 'schufa min'.
     *
     * First applicant schufa MIN.
     *
     */
    public Integer getSchufaMin() {
        return _schufaMin;
    }
                                            
    /**
     * Setter for the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    public void setSchufaMax(Integer schufaMax) {
        _schufaMax = schufaMax;
    }
            
    /**
     * Returns the property 'schufa max'.
     *
     * First applicant schufa MAX.
     *
     */
    public Integer getSchufaMax() {
        return _schufaMax;
    }
                                            
    /**
     * Setter for the property 'factor'.
     *
     * Factor.
     *
     */
    public void setFactor(Double factor) {
        _factor = factor;
    }
            
    /**
     * Returns the property 'factor'.
     *
     * Factor.
     *
     */
    public Double getFactor() {
        return _factor;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageInterestRatePredictionData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _employment=").append(_employment);
            builder.append("\n}");
        } else {
            builder.append(BrokerageInterestRatePredictionData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageInterestRatePredictionData asBrokerageInterestRatePredictionData() {
        return this;
    }
}
