package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokeragePayoutProbabilityPrediction;
import de.smava.webapp.brokerage.domain.BrokeragePayoutProbabilityPredictionData;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokeragePayoutProbabilityPredictions'.
 *
 * @author generator
 */
public interface BrokeragePayoutProbabilityPredictionEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'activation date'.
     *
     * 
     *
     */
    void setActivationDate(Date activationDate);

    /**
     * Returns the property 'activation date'.
     *
     * 
     *
     */
    Date getActivationDate();
    /**
     * Setter for the property 'brokerage payout probability prediction datas'.
     *
     * 
     *
     */
    void setBrokeragePayoutProbabilityPredictionDatas(Set<BrokeragePayoutProbabilityPredictionData> brokeragePayoutProbabilityPredictionDatas);

    /**
     * Returns the property 'brokerage payout probability prediction datas'.
     *
     * 
     *
     */
    Set<BrokeragePayoutProbabilityPredictionData> getBrokeragePayoutProbabilityPredictionDatas();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokeragePayoutProbabilityPrediction asBrokeragePayoutProbabilityPrediction();
}
