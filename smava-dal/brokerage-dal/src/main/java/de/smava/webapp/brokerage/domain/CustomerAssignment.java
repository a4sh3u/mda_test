package de.smava.webapp.brokerage.domain;

import de.smava.webapp.commons.domain.KreditPrivatEntity;

public class CustomerAssignment extends KreditPrivatEntity {

    private Long _customerNumber;
    private Long _creditAdvisorId;

    public CustomerAssignment() {
    }

    public CustomerAssignment(Long _customerNumber, Long _creditAdvisorId) {
        this._customerNumber = _customerNumber;
        this._creditAdvisorId = _creditAdvisorId;
    }

    public Long getCustomerNumber() {
        return _customerNumber;
    }

    public Long getCreditAdvisorId() {
        return _creditAdvisorId;
    }
}
