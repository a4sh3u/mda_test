package de.smava.webapp.brokerage.domain.interfaces;



import de.smava.webapp.brokerage.domain.BrokerageInterestRateDetails;
import de.smava.webapp.brokerage.domain.BrokerageInterestRateDetailsData;


/**
 * The domain object that represents 'BrokerageInterestRateDetailsDatas'.
 *
 * @author generator
 */
public interface BrokerageInterestRateDetailsDataEntityInterface {

    /**
     * Setter for the property 'brokerage interest rate details'.
     *
     * 
     *
     */
    void setBrokerageInterestRateDetails(BrokerageInterestRateDetails brokerageInterestRateDetails);

    /**
     * Returns the property 'brokerage interest rate details'.
     *
     * 
     *
     */
    BrokerageInterestRateDetails getBrokerageInterestRateDetails();
    /**
     * Setter for the property 'bank name'.
     *
     * 
     *
     */
    void setBankName(String bankName);

    /**
     * Returns the property 'bank name'.
     *
     * 
     *
     */
    String getBankName();
    /**
     * Setter for the property 'requested loan type'.
     *
     * 
     *
     */
    void setRequestedLoanType(de.smava.webapp.brokerage.domain.RequestedLoanType requestedLoanType);

    /**
     * Returns the property 'requested loan type'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.RequestedLoanType getRequestedLoanType();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'min amount'.
     *
     * 
     *
     */
    void setMinAmount(Double minAmount);

    /**
     * Returns the property 'min amount'.
     *
     * 
     *
     */
    Double getMinAmount();
    /**
     * Setter for the property 'max amount'.
     *
     * 
     *
     */
    void setMaxAmount(Double maxAmount);

    /**
     * Returns the property 'max amount'.
     *
     * 
     *
     */
    Double getMaxAmount();
    /**
     * Setter for the property 'min effective interest'.
     *
     * 
     *
     */
    void setMinEffectiveInterest(Double minEffectiveInterest);

    /**
     * Returns the property 'min effective interest'.
     *
     * 
     *
     */
    Double getMinEffectiveInterest();
    /**
     * Setter for the property 'max effective interest'.
     *
     * 
     *
     */
    void setMaxEffectiveInterest(Double maxEffectiveInterest);

    /**
     * Returns the property 'max effective interest'.
     *
     * 
     *
     */
    Double getMaxEffectiveInterest();
    /**
     * Setter for the property 'representative effective interest'.
     *
     * 
     *
     */
    void setRepresentativeEffectiveInterest(Double representativeEffectiveInterest);

    /**
     * Returns the property 'representative effective interest'.
     *
     * 
     *
     */
    Double getRepresentativeEffectiveInterest();
    /**
     * Setter for the property 'representative debt interest'.
     *
     * 
     *
     */
    void setRepresentativeDebtInterest(Double representativeDebtInterest);

    /**
     * Returns the property 'representative debt interest'.
     *
     * 
     *
     */
    Double getRepresentativeDebtInterest();
    /**
     * Setter for the property 'either or effective interest'.
     *
     * 
     *
     */
    void setEitherOrEffectiveInterest(Boolean eitherOrEffectiveInterest);

    /**
     * Returns the property 'either or effective interest'.
     *
     * 
     *
     */
    Boolean getEitherOrEffectiveInterest();
    /**
     * Setter for the property 'special offer type'.
     *
     * 
     *
     */
    void setSpecialOfferType(de.smava.webapp.brokerage.domain.SpecialOfferType specialOfferType);

    /**
     * Returns the property 'special offer type'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.SpecialOfferType getSpecialOfferType();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageInterestRateDetailsData asBrokerageInterestRateDetailsData();
}
