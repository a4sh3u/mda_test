package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractAdvisorAssignmentLog;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'AdvisorAssignmentLogs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AdvisorAssignmentLogHistory extends AbstractAdvisorAssignmentLog {

    protected transient Date _assignmentDateInitVal;
    protected transient boolean _assignmentDateIsSet;


				
    /**
     * Returns the initial value of the property 'assignment date'.
     */
    public Date assignmentDateInitVal() {
        Date result;
        if (_assignmentDateIsSet) {
            result = _assignmentDateInitVal;
        } else {
            result = getAssignmentDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'assignment date'.
     */
    public boolean assignmentDateIsDirty() {
        return !valuesAreEqual(assignmentDateInitVal(), getAssignmentDate());
    }

    /**
     * Returns true if the setter method was called for the property 'assignment date'.
     */
    public boolean assignmentDateIsSet() {
        return _assignmentDateIsSet;
    }
	
}
