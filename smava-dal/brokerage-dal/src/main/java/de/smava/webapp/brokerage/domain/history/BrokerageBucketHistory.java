package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBucket;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBuckets'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBucketHistory extends AbstractBrokerageBucket {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _executionDateInitVal;
    protected transient boolean _executionDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'execution date'.
     */
    public Date executionDateInitVal() {
        Date result;
        if (_executionDateIsSet) {
            result = _executionDateInitVal;
        } else {
            result = getExecutionDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'execution date'.
     */
    public boolean executionDateIsDirty() {
        return !valuesAreEqual(executionDateInitVal(), getExecutionDate());
    }

    /**
     * Returns true if the setter method was called for the property 'execution date'.
     */
    public boolean executionDateIsSet() {
        return _executionDateIsSet;
    }
			
}
