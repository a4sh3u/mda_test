package de.smava.webapp.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Adam
 * @since 28.07.2016.
 */
public class DkbAdditionalData extends AbstractAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_DKB;
    }
}
