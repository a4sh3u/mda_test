package de.smava.webapp.brokerage.domain.history;



import de.smava.webapp.brokerage.domain.abstracts.AbstractBrokerageBucketConfiguration;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBucketConfigurations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBucketConfigurationHistory extends AbstractBrokerageBucketConfiguration {

    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;


										
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
			
}
