//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(casi search)}
import de.smava.webapp.brokerage.dao.CasiSearchDao;
import de.smava.webapp.brokerage.domain.CasiSearch;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'CasiSearchs'.
 *
 * @author generator
 */
@Repository(value = "casiSearchDao")
public class JdoCasiSearchDao extends JdoBaseDao implements CasiSearchDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCasiSearchDao.class);

    private static final String CLASS_NAME = "CasiSearch";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(casi search)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the casi search identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public CasiSearch load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        CasiSearch result = getEntity(CasiSearch.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public CasiSearch getCasiSearch(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	CasiSearch entity = findUniqueEntity(CasiSearch.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the casi search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(CasiSearch casiSearch) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCasiSearch: " + casiSearch);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(casi search)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(casiSearch);
    }

    /**
     * @deprecated Use {@link #save(CasiSearch) instead}
     */
    public Long saveCasiSearch(CasiSearch casiSearch) {
        return save(casiSearch);
    }

    /**
     * Deletes an casi search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the casi search
     */
    public void deleteCasiSearch(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteCasiSearch: " + id);
        }
        deleteEntity(CasiSearch.class, id);
    }

    /**
     * Retrieves all 'CasiSearch' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<CasiSearch> getCasiSearchList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<CasiSearch> result = getEntities(CasiSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'CasiSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<CasiSearch> getCasiSearchList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<CasiSearch> result = getEntities(CasiSearch.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'CasiSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<CasiSearch> getCasiSearchList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<CasiSearch> result = getEntities(CasiSearch.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CasiSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<CasiSearch> getCasiSearchList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<CasiSearch> result = getEntities(CasiSearch.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'CasiSearch' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CasiSearch> findCasiSearchList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<CasiSearch> result = findEntities(CasiSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'CasiSearch' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<CasiSearch> findCasiSearchList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<CasiSearch> result = findEntities(CasiSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'CasiSearch' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CasiSearch> findCasiSearchList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<CasiSearch> result = findEntities(CasiSearch.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'CasiSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CasiSearch> findCasiSearchList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<CasiSearch> result = findEntities(CasiSearch.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CasiSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CasiSearch> findCasiSearchList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<CasiSearch> result = findEntities(CasiSearch.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CasiSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CasiSearch> findCasiSearchList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<CasiSearch> result = findEntities(CasiSearch.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'CasiSearch' instances.
     */
    public long getCasiSearchCount() {
        long result = getEntityCount(CasiSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'CasiSearch' instances which match the given whereClause.
     */
    public long getCasiSearchCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(CasiSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'CasiSearch' instances which match the given whereClause together with params specified in object array.
     */
    public long getCasiSearchCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(CasiSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(casi search)}
    //
    // insert custom methods here
    //
    @Override
    public Collection<CasiSearch> retrieveCasiCustomers(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate,
                                                        Integer offset, Integer limit, String orderBy, boolean desc) {
        LOGGER.debug("try to find accounts for names: " + firstName + ", " + lastName);
        Collection<CasiSearch> result;
        if (checkSearchValues(email, firstName, lastName, phone, birth, advisorId, loanApplicationDate)) {
            Query query = this.getPersistenceManager().newQuery(CasiSearch.class);
            List<String> andFilters = setCasiCustomersFilter(email, firstName, lastName, phone, birth, advisorId, loanApplicationDate);

            if (!StringUtils.isEmpty(orderBy)){
                if (desc){
                    query.setOrdering(orderBy + " DESC");
                } else {
                    query.setOrdering(orderBy +" ASC");
                }
            }


            if (offset != null && limit != null) {
                query.setRange(offset, offset + limit);
            }

            query.setFilter(and(andFilters));

            result = (Collection<CasiSearch>) query.execute();

        } else {
            result = this.getCasiSearchList();
        }
//      dirty hack because of duplications in result set

        return result;
    }

    @Override
    public Collection<CasiSearch> retrieveCasiCustomers(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate,
                                                        Integer offset, Integer limit, String orderBy, boolean desc, boolean withCount) {
        Collection<CasiSearch> result = retrieveCasiCustomers(email, firstName, lastName, phone, birth, advisorId, loanApplicationDate,
                offset, limit, orderBy, desc);

        if (result != null && result.size() > 0 && withCount){
            if (result.size() == 1) {
                result.iterator().next().setCount(1L);
            } else {
                // add count to first item
                Long count = countRetrieveCasiCustomers(email, firstName, lastName, phone, birth, advisorId, loanApplicationDate);
                result.iterator().next().setCount(count);
            }
        }

        return result;
    }


    @Override
    public Long countRetrieveCasiCustomers(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate) {
        Long result = null;

        if (checkSearchValues(email, firstName, lastName, phone, birth, advisorId, loanApplicationDate)) {
            List<String> andFilters = setCasiCustomersFilter(email, firstName, lastName, phone, birth, advisorId, loanApplicationDate);
            Query query = getPersistenceManager().newQuery("SELECT count(this) FROM " + CasiSearch.class.getName() + " WHERE " + and(andFilters));
            result = (Long) query.execute();
        }

        return result;
    }

    public boolean checkSearchValues(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate) {
        return !StringUtils.isEmpty(firstName) || !StringUtils.isEmpty(email) || !StringUtils.isEmpty(lastName) || !StringUtils.isEmpty(phone) || birth != null || advisorId != null || loanApplicationDate != null;
    }

    public List<String> setCasiCustomersFilter(String email, String firstName, String lastName, String phone, Date birth, Long advisorId, Date loanApplicationDate) {
        List <String> andFilters = new ArrayList<String>();
        if (!StringUtils.isEmpty(firstName)) {
            andFilters.add(like("_firstName", firstName));
        }

        if (!StringUtils.isEmpty(lastName)) {
            andFilters.add(like("_lastName", lastName));
        }

        if (!StringUtils.isEmpty(email)) {
            andFilters.add(like("_email", email));
        }

        if (!StringUtils.isEmpty(phone)) {
            List<String> phones = new ArrayList<String>();
            phones.add(like("_phone", phone));
            phones.add(like("_phone2", phone));
            andFilters.add(or(phones));
        }
        if (birth != null ) {
            Calendar c = Calendar.getInstance();
            c.setTime(birth);
            andFilters.add(eq("_birthDate.getDay()", c.get(Calendar.DAY_OF_MONTH)));
            andFilters.add(eq("_birthDate.getMonth()", c.get(Calendar.MONTH)));
            andFilters.add(eq("_birthDate.getYear()", c.get(Calendar.YEAR)));

        }
        if (advisorId != null) {
            andFilters.add(eq("_advisor._id", advisorId));
        }
        if (loanApplicationDate != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(loanApplicationDate);
            andFilters.add(eq("_loanApplication._creationDate.getDay()", c.get(Calendar.DAY_OF_MONTH)));
            andFilters.add(eq("_loanApplication._creationDate.getMonth()", c.get(Calendar.MONTH)));
            andFilters.add(eq("_loanApplication._creationDate.getYear()", c.get(Calendar.YEAR)));

        }
        return andFilters;
    }

    private String and(List<String> andFilters) {
        String result = "";
        for (String filter : andFilters) {
            result = result + "(" +filter + ") && ";
        }
        if (!result.isEmpty()) {
            result = result.substring(0, result.lastIndexOf(" && "));
        }
        return result;
    }

    private String eq(String field, Object value) {
        return field + " == " + value.toString();
    }

    private String or(List<String> filters) {
        String result = "";
        for (String filter : filters) {
            result = result + "(" + filter + ") || ";
        }
        if (!result.isEmpty()) {
            result = result.substring(0, result.lastIndexOf(" || "));
        }
        return result;
    }

    private String like(String field, String value) {
        return field + ".toLowerCase().matches('.*" + value.toLowerCase(Locale.GERMANY) + ".*') == true";
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
