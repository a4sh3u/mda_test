//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document type)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.brokerage.dao.DocumentTypeDao;
import de.smava.webapp.brokerage.domain.DocumentType;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;


import java.util.*;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'DocumentTypes'.
 *
 * @author generator
 */
@Repository(value = "documentTypeDao")
public class JdoDocumentTypeDao extends JdoBaseDao implements DocumentTypeDao {

    private static final Logger LOGGER = Logger.getLogger(JdoDocumentTypeDao.class);

    private static final String CLASS_NAME = "DocumentType";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(document type)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the document type identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public DocumentType load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        DocumentType result = getEntity(DocumentType.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public DocumentType getDocumentType(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	DocumentType entity = findUniqueEntity(DocumentType.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the document type.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(DocumentType documentType) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveDocumentType: " + documentType);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(document type)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(documentType);
    }

    /**
     * @deprecated Use {@link #save(DocumentType) instead}
     */
    public Long saveDocumentType(DocumentType documentType) {
        return save(documentType);
    }

    /**
     * Deletes an document type, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the document type
     */
    public void deleteDocumentType(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteDocumentType: " + id);
        }
        deleteEntity(DocumentType.class, id);
    }

    /**
     * Retrieves all 'DocumentType' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<DocumentType> getDocumentTypeList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<DocumentType> result = getEntities(DocumentType.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'DocumentType' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<DocumentType> getDocumentTypeList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<DocumentType> result = getEntities(DocumentType.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentType' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<DocumentType> getDocumentTypeList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<DocumentType> result = getEntities(DocumentType.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentType' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<DocumentType> getDocumentTypeList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<DocumentType> result = getEntities(DocumentType.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentType' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentType> findDocumentTypeList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<DocumentType> result = findEntities(DocumentType.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'DocumentType' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<DocumentType> findDocumentTypeList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<DocumentType> result = findEntities(DocumentType.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'DocumentType' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentType> findDocumentTypeList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<DocumentType> result = findEntities(DocumentType.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'DocumentType' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentType> findDocumentTypeList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<DocumentType> result = findEntities(DocumentType.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentType' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentType> findDocumentTypeList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<DocumentType> result = findEntities(DocumentType.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'DocumentType' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<DocumentType> findDocumentTypeList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<DocumentType> result = findEntities(DocumentType.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentType' instances.
     */
    public long getDocumentTypeCount() {
        long result = getEntityCount(DocumentType.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentType' instances which match the given whereClause.
     */
    public long getDocumentTypeCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(DocumentType.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'DocumentType' instances which match the given whereClause together with params specified in object array.
     */
    public long getDocumentTypeCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(DocumentType.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(document type)}
    //
    // insert custom methods here

    @Override
    public DocumentType fetchDocumentType(String type) {
        Query query = getPersistenceManager().newNamedQuery(DocumentType.class, "fetchDocumentType");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("documentType", type);

        @SuppressWarnings("unchecked")
        List<DocumentType> documentTypeList = (List<DocumentType>) query.executeWithMap(params);

        if (documentTypeList != null && !documentTypeList.isEmpty()) {
            return documentTypeList.get(0);
        } else {
            return null;
        }
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
