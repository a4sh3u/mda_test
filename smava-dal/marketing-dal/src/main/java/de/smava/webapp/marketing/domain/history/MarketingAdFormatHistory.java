package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingAdFormat;




/**
 * The domain object that has all history aggregation related fields for 'MarketingAdFormats'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingAdFormatHistory extends AbstractMarketingAdFormat {

    protected transient String _textInitVal;
    protected transient boolean _textIsSet;
    protected transient int _imageWidthInitVal;
    protected transient boolean _imageWidthIsSet;
    protected transient int _imageHeightInitVal;
    protected transient boolean _imageHeightIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _imageInitVal;
    protected transient boolean _imageIsSet;


	
    /**
     * Returns the initial value of the property 'text'.
     */
    public String textInitVal() {
        String result;
        if (_textIsSet) {
            result = _textInitVal;
        } else {
            result = getText();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'text'.
     */
    public boolean textIsDirty() {
        return !valuesAreEqual(textInitVal(), getText());
    }

    /**
     * Returns true if the setter method was called for the property 'text'.
     */
    public boolean textIsSet() {
        return _textIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image width'.
     */
    public int imageWidthInitVal() {
        int result;
        if (_imageWidthIsSet) {
            result = _imageWidthInitVal;
        } else {
            result = getImageWidth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image width'.
     */
    public boolean imageWidthIsDirty() {
        return !valuesAreEqual(imageWidthInitVal(), getImageWidth());
    }

    /**
     * Returns true if the setter method was called for the property 'image width'.
     */
    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image height'.
     */
    public int imageHeightInitVal() {
        int result;
        if (_imageHeightIsSet) {
            result = _imageHeightInitVal;
        } else {
            result = getImageHeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image height'.
     */
    public boolean imageHeightIsDirty() {
        return !valuesAreEqual(imageHeightInitVal(), getImageHeight());
    }

    /**
     * Returns true if the setter method was called for the property 'image height'.
     */
    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image'.
     */
    public String imageInitVal() {
        String result;
        if (_imageIsSet) {
            result = _imageInitVal;
        } else {
            result = getImage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image'.
     */
    public boolean imageIsDirty() {
        return !valuesAreEqual(imageInitVal(), getImage());
    }

    /**
     * Returns true if the setter method was called for the property 'image'.
     */
    public boolean imageIsSet() {
        return _imageIsSet;
    }
		
}
