package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingAdvertisement;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingAdvertisements'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingAdvertisementHistory extends AbstractMarketingAdvertisement {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _commentInitVal;
    protected transient boolean _commentIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'comment'.
     */
    public String commentInitVal() {
        String result;
        if (_commentIsSet) {
            result = _commentInitVal;
        } else {
            result = getComment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comment'.
     */
    public boolean commentIsDirty() {
        return !valuesAreEqual(commentInitVal(), getComment());
    }

    /**
     * Returns true if the setter method was called for the property 'comment'.
     */
    public boolean commentIsSet() {
        return _commentIsSet;
    }
	
}
