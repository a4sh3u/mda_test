package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.EkomiData;

import java.util.Date;


/**
 * The domain object that represents 'EkomiDatas'.
 *
 * @author generator
 */
public interface EkomiDataEntityInterface {

    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    void setBrokerageApplicationId(Long brokerageApplicationId);

    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    Long getBrokerageApplicationId();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'url'.
     *
     * 
     *
     */
    void setUrl(String url);

    /**
     * Returns the property 'url'.
     *
     * 
     *
     */
    String getUrl();
    /**
     * Helper method to get reference of this object as model type.
     */
    EkomiData asEkomiData();
}
