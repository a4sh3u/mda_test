//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing deal)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;


import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.domain.MarketingDeal;

// !!!!!!!! End of insert code section !!!!!!!!
/**
 * DAO interface for the domain object 'MarketingDeals'.
 *
 * @author generator
 */
public interface MarketingDealDao extends BaseDao<MarketingDeal>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing deal identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MarketingDeal getMarketingDeal(Long id);

    /**
     * Saves the marketing deal.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMarketingDeal(MarketingDeal marketingDeal);

    /**
     * Deletes an marketing deal, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing deal
     */
    void deleteMarketingDeal(Long id);

    /**
     * Retrieves all 'MarketingDeal' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingDeal> getMarketingDealList();

    /**
     * Retrieves a page of 'MarketingDeal' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingDeal> getMarketingDealList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingDeal' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingDeal> getMarketingDealList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingDeal' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingDeal> getMarketingDealList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'MarketingDeal' instances.
     */
    long getMarketingDealCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing deal)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
