package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.EmailCoupon;

import java.util.Date;


/**
 * The domain object that represents 'EmailCoupons'.
 *
 * @author generator
 */
public interface EmailCouponEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'account id'.
     *
     * 
     *
     */
    void setAccountId(Long accountId);

    /**
     * Returns the property 'account id'.
     *
     * 
     *
     */
    Long getAccountId();
    /**
     * Setter for the property 'loan application id'.
     *
     * 
     *
     */
    void setLoanApplicationId(Long loanApplicationId);

    /**
     * Returns the property 'loan application id'.
     *
     * 
     *
     */
    Long getLoanApplicationId();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Integer amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Integer getAmount();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'payout'.
     *
     * 
     *
     */
    void setPayout(Boolean payout);

    /**
     * Returns the property 'payout'.
     *
     * 
     *
     */
    Boolean getPayout();
    /**
     * Setter for the property 'payout validation date'.
     *
     * 
     *
     */
    void setPayoutValidationDate(Date payoutValidationDate);

    /**
     * Returns the property 'payout validation date'.
     *
     * 
     *
     */
    Date getPayoutValidationDate();
    /**
     * Setter for the property 'coupon generated'.
     *
     * 
     *
     */
    void setCouponGenerated(Boolean couponGenerated);

    /**
     * Returns the property 'coupon generated'.
     *
     * 
     *
     */
    Boolean getCouponGenerated();
    /**
     * Setter for the property 'coupon generated date'.
     *
     * 
     *
     */
    void setCouponGeneratedDate(Date couponGeneratedDate);

    /**
     * Returns the property 'coupon generated date'.
     *
     * 
     *
     */
    Date getCouponGeneratedDate();
    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    void setBrokerageApplicationId(Long brokerageApplicationId);

    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    Long getBrokerageApplicationId();
    /**
     * Helper method to get reference of this object as model type.
     */
    EmailCoupon asEmailCoupon();
}
