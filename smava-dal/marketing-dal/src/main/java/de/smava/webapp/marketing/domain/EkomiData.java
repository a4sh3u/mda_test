//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(ekomi data)}
import de.smava.webapp.marketing.domain.history.EkomiDataHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'EkomiDatas'.
 *
 * @author generator
 */
public class EkomiData extends EkomiDataHistory  implements java.io.Serializable {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(ekomi data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _brokerageApplicationId;
        protected Date _creationDate;
        protected String _url;
        
                                    
    /**
     * Setter for the property 'brokerage application id'.
     */
    public void setBrokerageApplicationId(Long brokerageApplicationId) {
        _brokerageApplicationId = brokerageApplicationId;
    }
            
    /**
     * Returns the property 'brokerage application id'.
     */
    public Long getBrokerageApplicationId() {
        return _brokerageApplicationId;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'url'.
     */
    public void setUrl(String url) {
        if (!_urlIsSet) {
            _urlIsSet = true;
            _urlInitVal = getUrl();
        }
        registerChange("url", _urlInitVal, url);
        _url = url;
    }
                        
    /**
     * Returns the property 'url'.
     */
    public String getUrl() {
        return _url;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(EkomiData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _url=").append(_url);
            builder.append("\n}");
        } else {
            builder.append(EkomiData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public EkomiData asEkomiData() {
        return this;
    }
}
