//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing tracking hit)}
import de.smava.webapp.marketing.domain.history.MarketingTrackingHitHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingTrackingHits'.
 *
 * @author generator
 */
public class MarketingTrackingHit extends MarketingTrackingHitHistory  implements de.smava.webapp.account.domain.MarketingTrackingEntity {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing tracking hit)}

    public MarketingTrackingHit(Date date, Long placementId) {
        super();
        _creationDate = date;
        _marketingPlacementId = placementId;
    }

    public MarketingTrackingHit(Date date, Long placementId, String marketingTest) {
        super();
        _creationDate = date;
        _marketingPlacementId = placementId;
        _marketingTest = marketingTest;
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Long _marketingPlacementId;
        protected String _marketingTest;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'marketing placement id'.
     */
    public void setMarketingPlacementId(Long marketingPlacementId) {
        _marketingPlacementId = marketingPlacementId;
    }
            
    /**
     * Returns the property 'marketing placement id'.
     */
    public Long getMarketingPlacementId() {
        return _marketingPlacementId;
    }
                                    /**
     * Setter for the property 'marketing test'.
     */
    public void setMarketingTest(String marketingTest) {
        if (!_marketingTestIsSet) {
            _marketingTestIsSet = true;
            _marketingTestInitVal = getMarketingTest();
        }
        registerChange("marketing test", _marketingTestInitVal, marketingTest);
        _marketingTest = marketingTest;
    }
                        
    /**
     * Returns the property 'marketing test'.
     */
    public String getMarketingTest() {
        return _marketingTest;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingTrackingHit.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _marketingTest=").append(_marketingTest);
            builder.append("\n}");
        } else {
            builder.append(MarketingTrackingHit.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingTrackingHit asMarketingTrackingHit() {
        return this;
    }
}
