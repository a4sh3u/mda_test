package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingPartner;
import de.smava.webapp.marketing.domain.MarketingPayment;
import de.smava.webapp.marketing.domain.MarketingPlacement;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'MarketingPayments'.
 *
 * @author generator
 */
public interface MarketingPaymentEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Float amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Float getAmount();
    /**
     * Setter for the property 'from'.
     *
     * 
     *
     */
    void setFrom(Date from);

    /**
     * Returns the property 'from'.
     *
     * 
     *
     */
    Date getFrom();
    /**
     * Setter for the property 'until'.
     *
     * 
     *
     */
    void setUntil(Date until);

    /**
     * Returns the property 'until'.
     *
     * 
     *
     */
    Date getUntil();
    /**
     * Setter for the property 'marketing partner'.
     *
     * 
     *
     */
    void setMarketingPartner(MarketingPartner marketingPartner);

    /**
     * Returns the property 'marketing partner'.
     *
     * 
     *
     */
    MarketingPartner getMarketingPartner();
    /**
     * Setter for the property 'placements'.
     *
     * 
     *
     */
    void setPlacements(List<MarketingPlacement> placements);

    /**
     * Returns the property 'placements'.
     *
     * 
     *
     */
    List<MarketingPlacement> getPlacements();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingPayment asMarketingPayment();
}
