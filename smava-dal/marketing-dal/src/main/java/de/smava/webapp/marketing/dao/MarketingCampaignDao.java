//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing campaign)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.domain.MarketingCampaign;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO interface for the domain object 'MarketingCampaigns'.
 *
 * @author generator
 */
public interface MarketingCampaignDao extends BaseDao<MarketingCampaign>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing campaign identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MarketingCampaign getMarketingCampaign(Long id);

    /**
     * Saves the marketing campaign.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMarketingCampaign(MarketingCampaign marketingCampaign);

    /**
     * Deletes an marketing campaign, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing campaign
     */
    void deleteMarketingCampaign(Long id);

    /**
     * Retrieves all 'MarketingCampaign' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingCampaign> getMarketingCampaignList();

    /**
     * Retrieves a page of 'MarketingCampaign' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingCampaign> getMarketingCampaignList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingCampaign' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingCampaign> getMarketingCampaignList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingCampaign' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingCampaign> getMarketingCampaignList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'MarketingCampaign' instances.
     */
    long getMarketingCampaignCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing campaign)}
    //
    MarketingCampaign getById(Long id);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
