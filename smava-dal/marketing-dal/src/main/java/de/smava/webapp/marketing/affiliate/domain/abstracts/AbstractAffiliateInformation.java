//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.marketing.affiliate.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(affiliate information)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.marketing.affiliate.domain.AffiliateInformation;
import de.smava.webapp.marketing.affiliate.domain.interfaces.AffiliateInformationEntityInterface;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AffiliateInformations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAffiliateInformation
    extends KreditPrivatEntity implements AffiliateInformationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAffiliateInformation.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(affiliate information)}
    public static final int MAX_AFFILIATE_CHARS = 512;
    public static final int MAX_SUB_AFFILIATE_CHARS = 512;
    public static final int MAX_CHANNEL_CHARS = 512;

    private static final String UTF8 = "utf-8";

    /**
     * encodes the given value as base64 with UFT-8
     *
     * @param value
     * @return
     */
    protected String encode( String value){
        try{
            return new String (Base64.encodeBase64(value.getBytes(UTF8)));
        } catch( Exception e){
            LOGGER.error("problem with encoding", e);
        }
        return null;
    }

    /**
     * decodes the given value with base64 UTF8
     * @param value
     * @return
     */
    protected String decode( String value){
        try{
            return new String(Base64.decodeBase64(value), UTF8);
        } catch( Exception e){
            LOGGER.error("problem with encoding", e);
        }
        return null;
    }


    public boolean functionallyEquals(AffiliateInformation o){
        if (this == o) {
            return true;
        }

        AffiliateInformation that = (AffiliateInformation) o;

        if (getAccount() != null ? !getAccount().equals(that.getAccount()) : that.getAccount() != null) {
            return false;
        }
        if (getExternalAffiliateChannel() != null ? !getExternalAffiliateChannel().equals(that.getExternalAffiliateChannel()) : that.getExternalAffiliateChannel() != null) {
            return false;
        }
        if (getExternalAffiliateId() != null ? !getExternalAffiliateId().equals(that.getExternalAffiliateId()) : that.getExternalAffiliateId() != null) {
            return false;
        }
        if (getExternalAffiliateSubId() != null ? !getExternalAffiliateSubId().equals(that.getExternalAffiliateSubId()) : that.getExternalAffiliateSubId() != null) {
            return false;
        }
        if (getMarketingPlacement() != null ? !getMarketingPlacement().equals(that.getMarketingPlacement()) : that.getMarketingPlacement() != null) {
            return false;
        }

        return true;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

