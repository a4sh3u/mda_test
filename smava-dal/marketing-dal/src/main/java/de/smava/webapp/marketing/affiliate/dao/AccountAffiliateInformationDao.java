//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.marketing.affiliate.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account affiliate information)}

import de.smava.webapp.account.domain.Account;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.affiliate.domain.AccountAffiliateInformation;

import javax.transaction.Synchronization;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'AccountAffiliateInformations'.
 *
 * @author generator
 */
public interface AccountAffiliateInformationDao extends BaseDao<AccountAffiliateInformation> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the account affiliate information identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    AccountAffiliateInformation getAccountAffiliateInformation(Long id);

    /**
     * Saves the account affiliate information.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveAccountAffiliateInformation(AccountAffiliateInformation accountAffiliateInformation);

    /**
     * Deletes an account affiliate information, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account affiliate information
     */
    void deleteAccountAffiliateInformation(Long id);

    /**
     * Retrieves all 'AccountAffiliateInformation' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<AccountAffiliateInformation> getAccountAffiliateInformationList();

    /**
     * Retrieves a page of 'AccountAffiliateInformation' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<AccountAffiliateInformation> getAccountAffiliateInformationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'AccountAffiliateInformation' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<AccountAffiliateInformation> getAccountAffiliateInformationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'AccountAffiliateInformation' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<AccountAffiliateInformation> getAccountAffiliateInformationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'AccountAffiliateInformation' instances.
     */
    long getAccountAffiliateInformationCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(account affiliate information)}
    //
    // insert custom methods here

    /**
     * returns the latest AccountAffiliateInformation for a given account
     *
     * @param a
     * @return
     */
    AccountAffiliateInformation getAffiliateInformationForAccount(Account a);

    /**
     * returns the latest AccountAffiliateInformation for a given account
     * @param aId
     * @return
     */
    AccountAffiliateInformation getCurrentAffiliateInformationForAccountId(long aId);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
