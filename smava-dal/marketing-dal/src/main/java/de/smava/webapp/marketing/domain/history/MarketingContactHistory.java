package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingContact;




/**
 * The domain object that has all history aggregation related fields for 'MarketingContacts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingContactHistory extends AbstractMarketingContact {



				
}
