//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing advertisement)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingAdvertisementDao;
import de.smava.webapp.marketing.domain.MarketingAdvertisement;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingAdvertisements'.
 *
 * @author generator
 */
@Repository(value = "marketingAdvertisementDao")
public class JdoMarketingAdvertisementDao extends JdoBaseDao implements MarketingAdvertisementDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingAdvertisementDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing advertisement)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing advertisement identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingAdvertisement load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisement - start: id=" + id);
        }
        MarketingAdvertisement result = getEntity(MarketingAdvertisement.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisement - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingAdvertisement getMarketingAdvertisement(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingAdvertisement entity = findUniqueEntity(MarketingAdvertisement.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing advertisement.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingAdvertisement marketingAdvertisement) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingAdvertisement: " + marketingAdvertisement);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing advertisement)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingAdvertisement);
    }

    /**
     * @deprecated Use {@link #save(MarketingAdvertisement) instead}
     */
    public Long saveMarketingAdvertisement(MarketingAdvertisement marketingAdvertisement) {
        return save(marketingAdvertisement);
    }

    /**
     * Deletes an marketing advertisement, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing advertisement
     */
    public void deleteMarketingAdvertisement(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingAdvertisement: " + id);
        }
        deleteEntity(MarketingAdvertisement.class, id);
    }

    /**
     * Retrieves all 'MarketingAdvertisement' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingAdvertisement> getMarketingAdvertisementList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - start");
        }
        Collection<MarketingAdvertisement> result = getEntities(MarketingAdvertisement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingAdvertisement' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingAdvertisement> getMarketingAdvertisementList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - start: pageable=" + pageable);
        }
        Collection<MarketingAdvertisement> result = getEntities(MarketingAdvertisement.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingAdvertisement' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingAdvertisement> getMarketingAdvertisementList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - start: sortable=" + sortable);
        }
        Collection<MarketingAdvertisement> result = getEntities(MarketingAdvertisement.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingAdvertisement' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingAdvertisement> getMarketingAdvertisementList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingAdvertisement> result = getEntities(MarketingAdvertisement.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingAdvertisement' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdvertisement> findMarketingAdvertisementList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingAdvertisement> result = findEntities(MarketingAdvertisement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingAdvertisement' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingAdvertisement> findMarketingAdvertisementList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingAdvertisement> result = findEntities(MarketingAdvertisement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingAdvertisement' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdvertisement> findMarketingAdvertisementList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingAdvertisement> result = findEntities(MarketingAdvertisement.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingAdvertisement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdvertisement> findMarketingAdvertisementList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingAdvertisement> result = findEntities(MarketingAdvertisement.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingAdvertisement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdvertisement> findMarketingAdvertisementList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingAdvertisement> result = findEntities(MarketingAdvertisement.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingAdvertisement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdvertisement> findMarketingAdvertisementList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingAdvertisement> result = findEntities(MarketingAdvertisement.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdvertisementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingAdvertisement' instances.
     */
    public long getMarketingAdvertisementCount() {
        long result = getEntityCount(MarketingAdvertisement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingAdvertisement' instances which match the given whereClause.
     */
    public long getMarketingAdvertisementCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingAdvertisement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingAdvertisement' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingAdvertisementCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingAdvertisement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdvertisementCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing advertisement)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
