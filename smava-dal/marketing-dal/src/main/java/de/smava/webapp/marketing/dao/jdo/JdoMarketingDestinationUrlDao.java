//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.marketing.domain.MarketingPlacement;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingDestinationUrlDao;
import de.smava.webapp.marketing.domain.MarketingDestinationUrl;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingDestinationUrls'.
 *
 * @author generator
 */
@Repository(value = "marketingDestinationUrlDao")
public class JdoMarketingDestinationUrlDao extends JdoBaseDao implements MarketingDestinationUrlDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingDestinationUrlDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing destination url)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing destination url identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingDestinationUrl load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrl - start: id=" + id);
        }
        MarketingDestinationUrl result = getEntity(MarketingDestinationUrl.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrl - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingDestinationUrl getMarketingDestinationUrl(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingDestinationUrl entity = findUniqueEntity(MarketingDestinationUrl.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing destination url.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingDestinationUrl marketingDestinationUrl) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingDestinationUrl: " + marketingDestinationUrl);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing destination url)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingDestinationUrl);
    }

    /**
     * @deprecated Use {@link #save(MarketingDestinationUrl) instead}
     */
    public Long saveMarketingDestinationUrl(MarketingDestinationUrl marketingDestinationUrl) {
        return save(marketingDestinationUrl);
    }

    /**
     * Deletes an marketing destination url, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing destination url
     */
    public void deleteMarketingDestinationUrl(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingDestinationUrl: " + id);
        }
        deleteEntity(MarketingDestinationUrl.class, id);
    }

    /**
     * Retrieves all 'MarketingDestinationUrl' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingDestinationUrl> getMarketingDestinationUrlList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - start");
        }
        Collection<MarketingDestinationUrl> result = getEntities(MarketingDestinationUrl.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingDestinationUrl' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingDestinationUrl> getMarketingDestinationUrlList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - start: pageable=" + pageable);
        }
        Collection<MarketingDestinationUrl> result = getEntities(MarketingDestinationUrl.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingDestinationUrl' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingDestinationUrl> getMarketingDestinationUrlList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - start: sortable=" + sortable);
        }
        Collection<MarketingDestinationUrl> result = getEntities(MarketingDestinationUrl.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrl' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingDestinationUrl> getMarketingDestinationUrlList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrl> result = getEntities(MarketingDestinationUrl.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingDestinationUrl' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrl> findMarketingDestinationUrlList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingDestinationUrl> result = findEntities(MarketingDestinationUrl.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingDestinationUrl' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrl> findMarketingDestinationUrlList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingDestinationUrl> result = findEntities(MarketingDestinationUrl.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingDestinationUrl' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrl> findMarketingDestinationUrlList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingDestinationUrl> result = findEntities(MarketingDestinationUrl.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingDestinationUrl' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrl> findMarketingDestinationUrlList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrl> result = findEntities(MarketingDestinationUrl.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrl' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrl> findMarketingDestinationUrlList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrl> result = findEntities(MarketingDestinationUrl.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrl' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrl> findMarketingDestinationUrlList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrl> result = findEntities(MarketingDestinationUrl.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDestinationUrl' instances.
     */
    public long getMarketingDestinationUrlCount() {
        long result = getEntityCount(MarketingDestinationUrl.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDestinationUrl' instances which match the given whereClause.
     */
    public long getMarketingDestinationUrlCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingDestinationUrl.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDestinationUrl' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingDestinationUrlCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingDestinationUrl.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlCount() - result: count=" + result);
        }
        return result;
    }
	


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing destination url)}
    //
    @Override
	public Collection<MarketingDestinationUrl> findByIds(Collection<Long> ids) {
		return findByIds(MarketingDestinationUrl.class, ids);
	}

    @Override
    public long getValidUrlsCount(MarketingPlacement placement) {
        String whereClause = "this._marketingPlacement == marketingPlacement && this._validUntil == null"
                + " parameters de.smava.webapp.marketing.domain.MarketingDestinationUrlPlacement marketingPlacement";
        return getMarketingDestinationUrlCount(whereClause, new Object[] {placement});
    }
    @Override
    public MarketingDestinationUrl findMarketingDestinationUrlByUrl(String url){
        MarketingDestinationUrl result = null;
        final Object[] oqlParams = new Object[] {url};
        Collection<MarketingDestinationUrl> urlList = findMarketingDestinationUrlList("_url == :url", oqlParams);
        if (urlList != null && !urlList.isEmpty()) {
            result = urlList.iterator().next();
        }

        return result;
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
