//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement)}
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.transaction.Synchronization;

import de.smava.webapp.account.todo.dao.PlacementView;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.domain.MarketingPlacement;
import de.smava.webapp.marketing.domain.MarketingPublication;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'MarketingPlacements'.
 *
 * @author generator
 */
public interface MarketingPlacementDao extends BaseDao<MarketingPlacement>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing placement identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MarketingPlacement getMarketingPlacement(Long id);

    /**
     * Saves the marketing placement.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMarketingPlacement(MarketingPlacement marketingPlacement);

    /**
     * Deletes an marketing placement, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing placement
     */
    void deleteMarketingPlacement(Long id);

    /**
     * Retrieves all 'MarketingPlacement' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingPlacement> getMarketingPlacementList();

    /**
     * Retrieves a page of 'MarketingPlacement' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingPlacement> getMarketingPlacementList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingPlacement' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingPlacement> getMarketingPlacementList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingPlacement' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingPlacement> getMarketingPlacementList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'MarketingPlacement' instances.
     */
    long getMarketingPlacementCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing placement)}
    //
    // insert custom methods here
    //
    /**
     * Looks for a unique marketing placement that corresponds to the given invitation code.
	 * @param invitationCode possible placement code
	 * @return marketing placement for the given code or null
	 */
	MarketingPlacement findMarketingPlacementByAlternateCode(String invitationCode);
	
    boolean exists(Long placementId);
    
    MarketingPlacement getById(Long id);
    
    List<PlacementView> findAllForTest(Set<Long> ids, Long campaignId, Long partnerId, Long publication);
    
    List<PlacementView> findAllForTest(String testName);
    
    Collection<MarketingPlacement> findByIds(Collection<Long> ids);


    Collection<MarketingPlacement> findMarketingPlacementList(List<MarketingPublication> publications);


    Collection<MarketingPlacement> getMarketingPlacementsWithoutCampain();
    // !!!!!!!! End of insert code section !!!!!!!!
	
}
