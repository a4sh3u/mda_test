package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingTrackingHit;

import java.util.Date;


/**
 * The domain object that represents 'MarketingTrackingHits'.
 *
 * @author generator
 */
public interface MarketingTrackingHitEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'marketing placement id'.
     *
     * 
     *
     */
    void setMarketingPlacementId(Long marketingPlacementId);

    /**
     * Returns the property 'marketing placement id'.
     *
     * 
     *
     */
    Long getMarketingPlacementId();
    /**
     * Setter for the property 'marketing test'.
     *
     * 
     *
     */
    void setMarketingTest(String marketingTest);

    /**
     * Returns the property 'marketing test'.
     *
     * 
     *
     */
    String getMarketingTest();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingTrackingHit asMarketingTrackingHit();
}
