package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingContact;
import de.smava.webapp.marketing.domain.MarketingContactPlacement;
import de.smava.webapp.marketing.domain.MarketingPlacement;


/**
 * The domain object that represents 'MarketingContactPlacements'.
 *
 * @author generator
 */
public interface MarketingContactPlacementEntityInterface {

    /**
     * Setter for the property 'marketing placement'.
     *
     * 
     *
     */
    void setMarketingPlacement(MarketingPlacement marketingPlacement);

    /**
     * Returns the property 'marketing placement'.
     *
     * 
     *
     */
    MarketingPlacement getMarketingPlacement();
    /**
     * Setter for the property 'marketing contact'.
     *
     * 
     *
     */
    void setMarketingContact(MarketingContact marketingContact);

    /**
     * Returns the property 'marketing contact'.
     *
     * 
     *
     */
    MarketingContact getMarketingContact();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingContactPlacement asMarketingContactPlacement();
}
