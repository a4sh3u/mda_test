//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing tracking hit)}
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingTrackingHitDao;
import de.smava.webapp.marketing.domain.MarketingPlacement;
import de.smava.webapp.marketing.domain.MarketingTrackingHit;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingTrackingHits'.
 *
 * @author generator
 */
@Repository(value = "marketingTrackingHitDao")
public class JdoMarketingTrackingHitDao extends JdoBaseDao implements MarketingTrackingHitDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingTrackingHitDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing tracking hit)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing tracking hit identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingTrackingHit load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHit - start: id=" + id);
        }
        MarketingTrackingHit result = getEntity(MarketingTrackingHit.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHit - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingTrackingHit getMarketingTrackingHit(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingTrackingHit entity = findUniqueEntity(MarketingTrackingHit.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing tracking hit.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingTrackingHit marketingTrackingHit) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingTrackingHit: " + marketingTrackingHit);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing tracking hit)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingTrackingHit);
    }

    /**
     * @deprecated Use {@link #save(MarketingTrackingHit) instead}
     */
    public Long saveMarketingTrackingHit(MarketingTrackingHit marketingTrackingHit) {
        return save(marketingTrackingHit);
    }

    /**
     * Deletes an marketing tracking hit, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing tracking hit
     */
    public void deleteMarketingTrackingHit(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingTrackingHit: " + id);
        }
        deleteEntity(MarketingTrackingHit.class, id);
    }

    /**
     * Retrieves all 'MarketingTrackingHit' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingTrackingHit> getMarketingTrackingHitList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - start");
        }
        Collection<MarketingTrackingHit> result = getEntities(MarketingTrackingHit.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingTrackingHit' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingTrackingHit> getMarketingTrackingHitList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - start: pageable=" + pageable);
        }
        Collection<MarketingTrackingHit> result = getEntities(MarketingTrackingHit.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingTrackingHit' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingTrackingHit> getMarketingTrackingHitList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - start: sortable=" + sortable);
        }
        Collection<MarketingTrackingHit> result = getEntities(MarketingTrackingHit.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingTrackingHit' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingTrackingHit> getMarketingTrackingHitList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingTrackingHit> result = getEntities(MarketingTrackingHit.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingTrackingHit' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingTrackingHit> findMarketingTrackingHitList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingTrackingHit> result = findEntities(MarketingTrackingHit.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingTrackingHit' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingTrackingHit> findMarketingTrackingHitList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingTrackingHit> result = findEntities(MarketingTrackingHit.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingTrackingHit' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingTrackingHit> findMarketingTrackingHitList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingTrackingHit> result = findEntities(MarketingTrackingHit.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingTrackingHit' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingTrackingHit> findMarketingTrackingHitList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingTrackingHit> result = findEntities(MarketingTrackingHit.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingTrackingHit' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingTrackingHit> findMarketingTrackingHitList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingTrackingHit> result = findEntities(MarketingTrackingHit.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingTrackingHit' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingTrackingHit> findMarketingTrackingHitList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingTrackingHit> result = findEntities(MarketingTrackingHit.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingTrackingHitList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingTrackingHit' instances.
     */
    public long getMarketingTrackingHitCount() {
        long result = getEntityCount(MarketingTrackingHit.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingTrackingHit' instances which match the given whereClause.
     */
    public long getMarketingTrackingHitCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingTrackingHit.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingTrackingHit' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingTrackingHitCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingTrackingHit.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingTrackingHitCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing tracking hit)}
    //

    @SuppressWarnings("unchecked")
    public Collection<MarketingTrackingHit> getMarketingTrackingHits(Date from, Date until) {
        Query q = getPersistenceManager().newNamedQuery(MarketingTrackingHit.class, "hitsForTimeFrame");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("begin", from);
        params.put("end", until);
        return (Collection<MarketingTrackingHit>) q.executeWithMap(params);
    }


    @SuppressWarnings("unchecked")
    public Collection<MarketingTrackingHit> getMarketingTrackingHits(Date from, Date until, Collection<MarketingPlacement> placements) {
        Query q = getPersistenceManager().newNamedQuery(MarketingTrackingHit.class, "hitsForTimeFrameAndPlacement");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("begin", from);
        params.put("end", until);
        params.put("placements", placements);
        return (Collection<MarketingTrackingHit>) q.executeWithMap(params);
    }

    
    @SuppressWarnings("unchecked")
    public long getMarketingTrackingHitCount(Date from, Date until) {
        Query q = getPersistenceManager().newNamedQuery(MarketingTrackingHit.class, "hitsForTimeFrame");
        q.setResult("count(this)");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("begin", from);
        params.put("end", until);
        return (Long) q.executeWithMap(params);
    }

    @SuppressWarnings("unchecked")
    public long getMarketingTrackingHitCount(Date from, Date until, Collection<MarketingPlacement> placements) {
        Query q = getPersistenceManager().newNamedQuery(MarketingTrackingHit.class, "hitsForTimeFrameAndPlacement");
        q.setResult("count(this)");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("begin", from);
        params.put("end", until);
        params.put("placements", placements);
        return (Long) q.executeWithMap(params);
    }
    
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
