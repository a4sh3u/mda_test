package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractEmailCoupon;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'EmailCoupons'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EmailCouponHistory extends AbstractEmailCoupon {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient Date _payoutValidationDateInitVal;
    protected transient boolean _payoutValidationDateIsSet;
    protected transient Date _couponGeneratedDateInitVal;
    protected transient boolean _couponGeneratedDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
		
    /**
     * Returns the initial value of the property 'payout validation date'.
     */
    public Date payoutValidationDateInitVal() {
        Date result;
        if (_payoutValidationDateIsSet) {
            result = _payoutValidationDateInitVal;
        } else {
            result = getPayoutValidationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'payout validation date'.
     */
    public boolean payoutValidationDateIsDirty() {
        return !valuesAreEqual(payoutValidationDateInitVal(), getPayoutValidationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'payout validation date'.
     */
    public boolean payoutValidationDateIsSet() {
        return _payoutValidationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'coupon generated date'.
     */
    public Date couponGeneratedDateInitVal() {
        Date result;
        if (_couponGeneratedDateIsSet) {
            result = _couponGeneratedDateInitVal;
        } else {
            result = getCouponGeneratedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'coupon generated date'.
     */
    public boolean couponGeneratedDateIsDirty() {
        return !valuesAreEqual(couponGeneratedDateInitVal(), getCouponGeneratedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'coupon generated date'.
     */
    public boolean couponGeneratedDateIsSet() {
        return _couponGeneratedDateIsSet;
    }
	
}
