//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing campaign)}

import de.smava.webapp.marketing.domain.history.MarketingCampaignHistory;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingCampaigns'.
 *
 * @author generator
 */
public class MarketingCampaign extends MarketingCampaignHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing campaign)}

    public void addMarketingPlacement(MarketingPlacement placement) {
        if (getPlacements() == null) {
            setPlacements(new TreeSet<MarketingPlacement>());
        }

        getPlacements().add(placement);
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected String _name;
        protected String _comment;
        protected Set<MarketingPlacement> _placements;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'comment'.
     */
    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = getComment();
        }
        registerChange("comment", _commentInitVal, comment);
        _comment = comment;
    }
                        
    /**
     * Returns the property 'comment'.
     */
    public String getComment() {
        return _comment;
    }
                                            
    /**
     * Setter for the property 'placements'.
     */
    public void setPlacements(Set<MarketingPlacement> placements) {
        _placements = placements;
    }
            
    /**
     * Returns the property 'placements'.
     */
    public Set<MarketingPlacement> getPlacements() {
        return _placements;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingCampaign.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _comment=").append(_comment);
            builder.append("\n}");
        } else {
            builder.append(MarketingCampaign.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingCampaign asMarketingCampaign() {
        return this;
    }
}
