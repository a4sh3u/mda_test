//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(sale tracking)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.marketing.domain.SaleTracking;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'SaleTrackings'.
 *
 * @author generator
 */
public interface SaleTrackingDao extends BaseDao<SaleTracking> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the sale tracking identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    SaleTracking getSaleTracking(Long id);

    /**
     * Saves the sale tracking.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveSaleTracking(SaleTracking saleTracking);

    /**
     * Deletes an sale tracking, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the sale tracking
     */
    void deleteSaleTracking(Long id);

    /**
     * Retrieves all 'SaleTracking' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<SaleTracking> getSaleTrackingList();

    /**
     * Retrieves a page of 'SaleTracking' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<SaleTracking> getSaleTrackingList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'SaleTracking' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<SaleTracking> getSaleTrackingList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'SaleTracking' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<SaleTracking> getSaleTrackingList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'SaleTracking' instances.
     */
    long getSaleTrackingCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(sale tracking)}
    //
    // insert custom methods here

    /**
     * Returns all sales that are tracked for a given BrokerageApplication ID
     * @param baId
     * @return
     */
    Collection<SaleTracking> getExistingSaleTrackingsForBrokerageApplication(Long baId);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
