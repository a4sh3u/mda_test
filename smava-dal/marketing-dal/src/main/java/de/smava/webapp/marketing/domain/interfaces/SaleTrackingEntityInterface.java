package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.SaleTracking;
import de.smava.webapp.marketing.domain.SaleType;

import java.util.Date;


/**
 * The domain object that represents 'SaleTrackings'.
 *
 * @author generator
 */
public interface SaleTrackingEntityInterface {

    /**
     * Setter for the property 'tracking date'.
     *
     * 
     *
     */
    void setTrackingDate(Date trackingDate);

    /**
     * Returns the property 'tracking date'.
     *
     * 
     *
     */
    Date getTrackingDate();
    /**
     * Setter for the property 'sale type'.
     *
     * 
     *
     */
    void setSaleType(SaleType saleType);

    /**
     * Returns the property 'sale type'.
     *
     * 
     *
     */
    SaleType getSaleType();
    /**
     * Setter for the property 'affiliate information'.
     *
     * 
     *
     */
    void setAffiliateInformation(de.smava.webapp.marketing.affiliate.domain.AffiliateInformation affiliateInformation);

    /**
     * Returns the property 'affiliate information'.
     *
     * 
     *
     */
    de.smava.webapp.marketing.affiliate.domain.AffiliateInformation getAffiliateInformation();
    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    void setBrokerageApplicationId(Long brokerageApplicationId);

    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    Long getBrokerageApplicationId();
    /**
     * Helper method to get reference of this object as model type.
     */
    SaleTracking asSaleTracking();
}
