//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing deal)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingDealDao;
import de.smava.webapp.marketing.domain.MarketingDeal;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingDeals'.
 *
 * @author generator
 */
@Repository(value = "marketingDealDao")
public class JdoMarketingDealDao extends JdoBaseDao implements MarketingDealDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingDealDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing deal)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing deal identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingDeal load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDeal - start: id=" + id);
        }
        MarketingDeal result = getEntity(MarketingDeal.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDeal - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingDeal getMarketingDeal(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingDeal entity = findUniqueEntity(MarketingDeal.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing deal.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingDeal marketingDeal) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingDeal: " + marketingDeal);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing deal)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingDeal);
    }

    /**
     * @deprecated Use {@link #save(MarketingDeal) instead}
     */
    public Long saveMarketingDeal(MarketingDeal marketingDeal) {
        return save(marketingDeal);
    }

    /**
     * Deletes an marketing deal, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing deal
     */
    public void deleteMarketingDeal(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingDeal: " + id);
        }
        deleteEntity(MarketingDeal.class, id);
    }

    /**
     * Retrieves all 'MarketingDeal' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingDeal> getMarketingDealList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - start");
        }
        Collection<MarketingDeal> result = getEntities(MarketingDeal.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingDeal' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingDeal> getMarketingDealList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - start: pageable=" + pageable);
        }
        Collection<MarketingDeal> result = getEntities(MarketingDeal.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingDeal' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingDeal> getMarketingDealList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - start: sortable=" + sortable);
        }
        Collection<MarketingDeal> result = getEntities(MarketingDeal.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDeal' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingDeal> getMarketingDealList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDeal> result = getEntities(MarketingDeal.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingDeal' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDeal> findMarketingDealList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingDeal> result = findEntities(MarketingDeal.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingDeal' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingDeal> findMarketingDealList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingDeal> result = findEntities(MarketingDeal.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingDeal' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDeal> findMarketingDealList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingDeal> result = findEntities(MarketingDeal.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingDeal' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDeal> findMarketingDealList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingDeal> result = findEntities(MarketingDeal.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDeal' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDeal> findMarketingDealList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDeal> result = findEntities(MarketingDeal.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDeal' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDeal> findMarketingDealList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDeal> result = findEntities(MarketingDeal.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDealList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDeal' instances.
     */
    public long getMarketingDealCount() {
        long result = getEntityCount(MarketingDeal.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDeal' instances which match the given whereClause.
     */
    public long getMarketingDealCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingDeal.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDeal' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingDealCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingDeal.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDealCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing deal)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
