package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingDestinationUrl;




/**
 * The domain object that has all history aggregation related fields for 'MarketingDestinationUrls'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingDestinationUrlHistory extends AbstractMarketingDestinationUrl {

    protected transient String _urlInitVal;
    protected transient boolean _urlIsSet;


	
    /**
     * Returns the initial value of the property 'url'.
     */
    public String urlInitVal() {
        String result;
        if (_urlIsSet) {
            result = _urlInitVal;
        } else {
            result = getUrl();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'url'.
     */
    public boolean urlIsDirty() {
        return !valuesAreEqual(urlInitVal(), getUrl());
    }

    /**
     * Returns true if the setter method was called for the property 'url'.
     */
    public boolean urlIsSet() {
        return _urlIsSet;
    }
	
}
