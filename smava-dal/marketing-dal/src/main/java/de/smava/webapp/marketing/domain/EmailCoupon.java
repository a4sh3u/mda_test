//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(email coupon)}
import de.smava.webapp.marketing.domain.history.EmailCouponHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'EmailCoupons'.
 *
 * 
 *
 * @author generator
 */
public class EmailCoupon extends EmailCouponHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(email coupon)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _expirationDate;
        protected Long _accountId;
        protected Long _loanApplicationId;
        protected Integer _amount;
        protected String _type;
        protected Boolean _payout;
        protected Date _payoutValidationDate;
        protected Boolean _couponGenerated;
        protected Date _couponGeneratedDate;
        protected Long _brokerageApplicationId;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                            
    /**
     * Setter for the property 'account id'.
     *
     * 
     *
     */
    public void setAccountId(Long accountId) {
        _accountId = accountId;
    }
            
    /**
     * Returns the property 'account id'.
     *
     * 
     *
     */
    public Long getAccountId() {
        return _accountId;
    }
                                            
    /**
     * Setter for the property 'loan application id'.
     *
     * 
     *
     */
    public void setLoanApplicationId(Long loanApplicationId) {
        _loanApplicationId = loanApplicationId;
    }
            
    /**
     * Returns the property 'loan application id'.
     *
     * 
     *
     */
    public Long getLoanApplicationId() {
        return _loanApplicationId;
    }
                                            
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(Integer amount) {
        _amount = amount;
    }
            
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public Integer getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'payout'.
     *
     * 
     *
     */
    public void setPayout(Boolean payout) {
        _payout = payout;
    }
            
    /**
     * Returns the property 'payout'.
     *
     * 
     *
     */
    public Boolean getPayout() {
        return _payout;
    }
                                    /**
     * Setter for the property 'payout validation date'.
     *
     * 
     *
     */
    public void setPayoutValidationDate(Date payoutValidationDate) {
        if (!_payoutValidationDateIsSet) {
            _payoutValidationDateIsSet = true;
            _payoutValidationDateInitVal = getPayoutValidationDate();
        }
        registerChange("payout validation date", _payoutValidationDateInitVal, payoutValidationDate);
        _payoutValidationDate = payoutValidationDate;
    }
                        
    /**
     * Returns the property 'payout validation date'.
     *
     * 
     *
     */
    public Date getPayoutValidationDate() {
        return _payoutValidationDate;
    }
                                            
    /**
     * Setter for the property 'coupon generated'.
     *
     * 
     *
     */
    public void setCouponGenerated(Boolean couponGenerated) {
        _couponGenerated = couponGenerated;
    }
            
    /**
     * Returns the property 'coupon generated'.
     *
     * 
     *
     */
    public Boolean getCouponGenerated() {
        return _couponGenerated;
    }
                                    /**
     * Setter for the property 'coupon generated date'.
     *
     * 
     *
     */
    public void setCouponGeneratedDate(Date couponGeneratedDate) {
        if (!_couponGeneratedDateIsSet) {
            _couponGeneratedDateIsSet = true;
            _couponGeneratedDateInitVal = getCouponGeneratedDate();
        }
        registerChange("coupon generated date", _couponGeneratedDateInitVal, couponGeneratedDate);
        _couponGeneratedDate = couponGeneratedDate;
    }
                        
    /**
     * Returns the property 'coupon generated date'.
     *
     * 
     *
     */
    public Date getCouponGeneratedDate() {
        return _couponGeneratedDate;
    }
                                            
    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    public void setBrokerageApplicationId(Long brokerageApplicationId) {
        _brokerageApplicationId = brokerageApplicationId;
    }
            
    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    public Long getBrokerageApplicationId() {
        return _brokerageApplicationId;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(EmailCoupon.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(EmailCoupon.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public EmailCoupon asEmailCoupon() {
        return this;
    }
}
