//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing ad format)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingAdFormatDao;
import de.smava.webapp.marketing.domain.MarketingAdFormat;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingAdFormats'.
 *
 * @author generator
 */
@Repository(value = "marketingAdFormatDao")
public class JdoMarketingAdFormatDao extends JdoBaseDao implements MarketingAdFormatDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingAdFormatDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing ad format)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing ad format identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingAdFormat load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormat - start: id=" + id);
        }
        MarketingAdFormat result = getEntity(MarketingAdFormat.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormat - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingAdFormat getMarketingAdFormat(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingAdFormat entity = findUniqueEntity(MarketingAdFormat.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing ad format.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingAdFormat marketingAdFormat) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingAdFormat: " + marketingAdFormat);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing ad format)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingAdFormat);
    }

    /**
     * @deprecated Use {@link #save(MarketingAdFormat) instead}
     */
    public Long saveMarketingAdFormat(MarketingAdFormat marketingAdFormat) {
        return save(marketingAdFormat);
    }

    /**
     * Deletes an marketing ad format, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing ad format
     */
    public void deleteMarketingAdFormat(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingAdFormat: " + id);
        }
        deleteEntity(MarketingAdFormat.class, id);
    }

    /**
     * Retrieves all 'MarketingAdFormat' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingAdFormat> getMarketingAdFormatList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - start");
        }
        Collection<MarketingAdFormat> result = getEntities(MarketingAdFormat.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingAdFormat' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingAdFormat> getMarketingAdFormatList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - start: pageable=" + pageable);
        }
        Collection<MarketingAdFormat> result = getEntities(MarketingAdFormat.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingAdFormat' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingAdFormat> getMarketingAdFormatList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - start: sortable=" + sortable);
        }
        Collection<MarketingAdFormat> result = getEntities(MarketingAdFormat.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingAdFormat' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingAdFormat> getMarketingAdFormatList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingAdFormat> result = getEntities(MarketingAdFormat.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingAdFormat' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdFormat> findMarketingAdFormatList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingAdFormat> result = findEntities(MarketingAdFormat.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingAdFormat' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingAdFormat> findMarketingAdFormatList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingAdFormat> result = findEntities(MarketingAdFormat.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingAdFormat' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdFormat> findMarketingAdFormatList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingAdFormat> result = findEntities(MarketingAdFormat.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingAdFormat' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdFormat> findMarketingAdFormatList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingAdFormat> result = findEntities(MarketingAdFormat.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingAdFormat' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdFormat> findMarketingAdFormatList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingAdFormat> result = findEntities(MarketingAdFormat.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingAdFormat' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingAdFormat> findMarketingAdFormatList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingAdFormat> result = findEntities(MarketingAdFormat.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingAdFormatList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingAdFormat' instances.
     */
    public long getMarketingAdFormatCount() {
        long result = getEntityCount(MarketingAdFormat.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingAdFormat' instances which match the given whereClause.
     */
    public long getMarketingAdFormatCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingAdFormat.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingAdFormat' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingAdFormatCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingAdFormat.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingAdFormatCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing ad format)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
