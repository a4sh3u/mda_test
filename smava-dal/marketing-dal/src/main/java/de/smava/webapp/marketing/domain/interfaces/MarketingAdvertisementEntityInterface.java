package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingAdFormat;
import de.smava.webapp.marketing.domain.MarketingAdvertisement;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'MarketingAdvertisements'.
 *
 * @author generator
 */
public interface MarketingAdvertisementEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    void setComment(String comment);

    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    String getComment();
    /**
     * Setter for the property 'formats'.
     *
     * 
     *
     */
    void setFormats(List<MarketingAdFormat> formats);

    /**
     * Returns the property 'formats'.
     *
     * 
     *
     */
    List<MarketingAdFormat> getFormats();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingAdvertisement asMarketingAdvertisement();
}
