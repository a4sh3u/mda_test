//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing publication)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingPublicationDao;
import de.smava.webapp.marketing.domain.MarketingPublication;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingPublications'.
 *
 * @author generator
 */
@Repository(value = "marketingPublicationDao")
public class JdoMarketingPublicationDao extends JdoBaseDao implements MarketingPublicationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingPublicationDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing publication)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing publication identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingPublication load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublication - start: id=" + id);
        }
        MarketingPublication result = getEntity(MarketingPublication.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublication - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingPublication getMarketingPublication(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingPublication entity = findUniqueEntity(MarketingPublication.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing publication.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingPublication marketingPublication) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingPublication: " + marketingPublication);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing publication)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingPublication);
    }

    /**
     * @deprecated Use {@link #save(MarketingPublication) instead}
     */
    public Long saveMarketingPublication(MarketingPublication marketingPublication) {
        return save(marketingPublication);
    }

    /**
     * Deletes an marketing publication, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing publication
     */
    public void deleteMarketingPublication(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingPublication: " + id);
        }
        deleteEntity(MarketingPublication.class, id);
    }

    /**
     * Retrieves all 'MarketingPublication' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingPublication> getMarketingPublicationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - start");
        }
        Collection<MarketingPublication> result = getEntities(MarketingPublication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingPublication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingPublication> getMarketingPublicationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - start: pageable=" + pageable);
        }
        Collection<MarketingPublication> result = getEntities(MarketingPublication.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPublication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingPublication> getMarketingPublicationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - start: sortable=" + sortable);
        }
        Collection<MarketingPublication> result = getEntities(MarketingPublication.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPublication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingPublication> getMarketingPublicationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPublication> result = getEntities(MarketingPublication.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPublication' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPublication> findMarketingPublicationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingPublication> result = findEntities(MarketingPublication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPublication' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingPublication> findMarketingPublicationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingPublication> result = findEntities(MarketingPublication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingPublication' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPublication> findMarketingPublicationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingPublication> result = findEntities(MarketingPublication.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPublication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPublication> findMarketingPublicationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingPublication> result = findEntities(MarketingPublication.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPublication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPublication> findMarketingPublicationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPublication> result = findEntities(MarketingPublication.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPublication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPublication> findMarketingPublicationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPublication> result = findEntities(MarketingPublication.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPublicationList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPublication' instances.
     */
    public long getMarketingPublicationCount() {
        long result = getEntityCount(MarketingPublication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPublication' instances which match the given whereClause.
     */
    public long getMarketingPublicationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingPublication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPublication' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingPublicationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingPublication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPublicationCount() - result: count=" + result);
        }
        return result;
    }

	
    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing publication)}
    //
    @Override
    public MarketingPublication getById(Long id) {
		return getById(MarketingPublication.class, id);
	}


    @Override
    public Collection<MarketingPublication> getPublicationsByPartner(Long partnerId) {
        return findMarketingPublicationList("_marketingPartner._id == " + partnerId);
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
