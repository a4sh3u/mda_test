//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing campaign)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingCampaignDao;
import de.smava.webapp.marketing.domain.MarketingCampaign;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingCampaigns'.
 *
 * @author generator
 */
@Repository(value = "marketingCampaignDao")
public class JdoMarketingCampaignDao extends JdoBaseDao implements MarketingCampaignDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingCampaignDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing campaign)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing campaign identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingCampaign load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaign - start: id=" + id);
        }
        MarketingCampaign result = getEntity(MarketingCampaign.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaign - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingCampaign getMarketingCampaign(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingCampaign entity = findUniqueEntity(MarketingCampaign.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing campaign.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingCampaign marketingCampaign) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingCampaign: " + marketingCampaign);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing campaign)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingCampaign);
    }

    /**
     * @deprecated Use {@link #save(MarketingCampaign) instead}
     */
    public Long saveMarketingCampaign(MarketingCampaign marketingCampaign) {
        return save(marketingCampaign);
    }

    /**
     * Deletes an marketing campaign, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing campaign
     */
    public void deleteMarketingCampaign(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingCampaign: " + id);
        }
        deleteEntity(MarketingCampaign.class, id);
    }

    /**
     * Retrieves all 'MarketingCampaign' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingCampaign> getMarketingCampaignList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - start");
        }
        Collection<MarketingCampaign> result = getEntities(MarketingCampaign.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingCampaign' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingCampaign> getMarketingCampaignList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - start: pageable=" + pageable);
        }
        Collection<MarketingCampaign> result = getEntities(MarketingCampaign.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingCampaign' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingCampaign> getMarketingCampaignList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - start: sortable=" + sortable);
        }
        Collection<MarketingCampaign> result = getEntities(MarketingCampaign.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingCampaign' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingCampaign> getMarketingCampaignList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingCampaign> result = getEntities(MarketingCampaign.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingCampaign' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingCampaign> findMarketingCampaignList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingCampaign> result = findEntities(MarketingCampaign.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingCampaign' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingCampaign> findMarketingCampaignList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingCampaign> result = findEntities(MarketingCampaign.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingCampaign' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingCampaign> findMarketingCampaignList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingCampaign> result = findEntities(MarketingCampaign.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingCampaign' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingCampaign> findMarketingCampaignList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingCampaign> result = findEntities(MarketingCampaign.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingCampaign' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingCampaign> findMarketingCampaignList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingCampaign> result = findEntities(MarketingCampaign.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingCampaign' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingCampaign> findMarketingCampaignList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingCampaign> result = findEntities(MarketingCampaign.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingCampaignList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingCampaign' instances.
     */
    public long getMarketingCampaignCount() {
        long result = getEntityCount(MarketingCampaign.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingCampaign' instances which match the given whereClause.
     */
    public long getMarketingCampaignCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingCampaign.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingCampaign' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingCampaignCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingCampaign.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingCampaignCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing campaign)}
    //
    @Override
	public MarketingCampaign getById(Long id) {
		return getById(MarketingCampaign.class, id);
	}
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
