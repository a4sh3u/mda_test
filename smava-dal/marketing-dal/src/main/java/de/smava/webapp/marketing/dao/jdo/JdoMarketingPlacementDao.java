//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement)}

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import de.smava.webapp.marketing.domain.MarketingPublication;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import de.smava.webapp.account.todo.dao.PlacementView;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingPlacementDao;
import de.smava.webapp.marketing.domain.MarketingPlacement;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingPlacements'.
 *
 * @author generator
 */
@Repository(value = "marketingPlacementDao")
public class JdoMarketingPlacementDao extends JdoBaseDao implements MarketingPlacementDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingPlacementDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing placement)}

    private static final String FIND_ALL_FOR_TEST_SELECT
            = "select "
            + "	pl.id as \"id\", "
            + "	mpu.name as \"publicationName\", "
            + "	mpa.name as \"partnerName\", "
            + "	mc.name as \"campaignName\" "
            + "from marketing_placement pl "
            + "left outer join marketing_publication mpu on pl.publication = mpu.id "
            + "left outer join marketing_partner mpa on mpu.marketing_partner = mpa.id "
            + "left outer join marketing_campaign mc on pl.campaign = mc.id ";

    private static final String FIND_ALL_FOR_TEST_SQL
            = FIND_ALL_FOR_TEST_SELECT
            + "where "
            + "	pl.publication in (select pu.id from marketing_publication pu where pu.marketing_partner = :partnerId) "
            + "	or pl.publication = :publicationId "
            + "	or pl.campaign = :campaignId ";

    private static final String WHERE_IDSIN_SNIP = "or pl.id in (:placementIds) ";

    private static final String GROUP_ORDER_SNIP = "group by pl.id, mpu.name, mpa.name, mc.name order by mc.name, mpa.name, mpu.name, pl.id";

    private static final String FIND_ALL_FOR_TEST_BY_NAME2
            = "select "
            + "	pl.id as \"id\", "
            + "	mpu.name as \"publicationName\", "
            + "	mpa.name as \"partnerName\", "
            + "	mc.name as \"campaignName\", "
            + " mdup.valid_until as \"validUntil\" "
            + "from marketing_placement pl "
            + "inner join marketing_destination_url_placement mdup on pl.id = mdup.placement_id "
            + "left outer join marketing_publication mpu on pl.publication = mpu.id "
            + "left outer join marketing_partner mpa on mpu.marketing_partner = mpa.id "
            + "left outer join marketing_campaign mc on pl.campaign = mc.id "
            + "where mdup.marketing_test_label = :testLabel "
            + "order by mc.name, mpa.name, mpu.name, pl.id";

    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing placement identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingPlacement load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacement - start: id=" + id);
        }
        MarketingPlacement result = getEntity(MarketingPlacement.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacement - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingPlacement getMarketingPlacement(Long id) {
        return load(id);
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            MarketingPlacement entity = findUniqueEntity(MarketingPlacement.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the marketing placement.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingPlacement marketingPlacement) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingPlacement: " + marketingPlacement);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing placement)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingPlacement);
    }

    /**
     * @deprecated Use {@link #save(MarketingPlacement) instead}
     */
    public Long saveMarketingPlacement(MarketingPlacement marketingPlacement) {
        return save(marketingPlacement);
    }

    /**
     * Deletes an marketing placement, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing placement
     */
    public void deleteMarketingPlacement(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingPlacement: " + id);
        }
        deleteEntity(MarketingPlacement.class, id);
    }

    /**
     * Retrieves all 'MarketingPlacement' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingPlacement> getMarketingPlacementList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - start");
        }
        Collection<MarketingPlacement> result = getEntities(MarketingPlacement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingPlacement' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingPlacement> getMarketingPlacementList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - start: pageable=" + pageable);
        }
        Collection<MarketingPlacement> result = getEntities(MarketingPlacement.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPlacement' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingPlacement> getMarketingPlacementList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - start: sortable=" + sortable);
        }
        Collection<MarketingPlacement> result = getEntities(MarketingPlacement.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPlacement' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingPlacement> getMarketingPlacementList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPlacement> result = getEntities(MarketingPlacement.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPlacement' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacement> findMarketingPlacementList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingPlacement> result = findEntities(MarketingPlacement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPlacement' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingPlacement> findMarketingPlacementList(String whereClause, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingPlacement> result = findEntities(MarketingPlacement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }


    /**
     * Retrieves a page of 'MarketingPlacement' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacement> findMarketingPlacementList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingPlacement> result = findEntities(MarketingPlacement.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacement> findMarketingPlacementList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingPlacement> result = findEntities(MarketingPlacement.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacement> findMarketingPlacementList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPlacement> result = findEntities(MarketingPlacement.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacement> findMarketingPlacementList(String whereClause, Pageable pageable, Sortable sortable, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPlacement> result = findEntities(MarketingPlacement.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPlacement' instances.
     */
    public long getMarketingPlacementCount() {
        long result = getEntityCount(MarketingPlacement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPlacement' instances which match the given whereClause.
     */
    public long getMarketingPlacementCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingPlacement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPlacement' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingPlacementCount(String whereClause, Object... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingPlacement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPlacementCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing placement)}
    //
    // insert custom methods here
    //

    /**
     * {@inheritDoc}
     */
    @Override
    public MarketingPlacement findMarketingPlacementByAlternateCode(String invitationCode) {

        MarketingPlacement result = null;
        if (invitationCode != null && StringUtils.isNotEmpty(invitationCode)) {
            result = findUniqueEntity(MarketingPlacement.class, "_alternateCode == '" + invitationCode + "'");
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PlacementView> findAllForTest(Set<Long> ids,
                                              Long campaignId, Long partnerId, Long publicationId) {
        final Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("placementIds", ids);
        params.put("campaignId", campaignId);
        params.put("partnerId", partnerId);
        params.put("publicationId", publicationId);
        String sql = FIND_ALL_FOR_TEST_SQL;
        if (CollectionUtils.isNotEmpty(ids)) {
            sql += WHERE_IDSIN_SNIP;
            sql = replaceCollectionTypes(params, sql);
        }
        sql += GROUP_ORDER_SNIP;
        final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
        query.setResultClass(PlacementView.class);
        return (List<PlacementView>) query.executeWithMap(params);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PlacementView> findAllForTest(final String testName) {
        final Query query = getPersistenceManager().newQuery(Query.SQL, FIND_ALL_FOR_TEST_BY_NAME2);
        query.setResultClass(PlacementView.class);
        return (List<PlacementView>) query.executeWithMap(getSingleParamMap("testLabel", testName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MarketingPlacement getById(Long id) {
        return getById(MarketingPlacement.class, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MarketingPlacement> findByIds(Collection<Long> ids) {
        return findByIds(MarketingPlacement.class, ids);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MarketingPlacement> findMarketingPlacementList(
            List<MarketingPublication> publications) {
        StringBuilder term = new StringBuilder();
        String delim = "";
        if (publications != null) {
            for (MarketingPublication publication : publications) {
                term.append(delim).append("_publication._id == ").append(publication.getId());
                if (!delim.equals(" || ")) {
                    delim = " || ";
                }
            }
        }

        return findMarketingPlacementList(term.toString());
    }


    @Override
    public Collection<MarketingPlacement> getMarketingPlacementsWithoutCampain() {

        return findMarketingPlacementList("_campaign == null");
    }
    
    
    // !!!!!!!! End of insert code section !!!!!!!!
}
