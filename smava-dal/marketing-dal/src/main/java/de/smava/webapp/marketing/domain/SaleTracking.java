//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(sale tracking)}
import de.smava.webapp.marketing.domain.history.SaleTrackingHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SaleTrackings'.
 *
 * @author generator
 */
public class SaleTracking extends SaleTrackingHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(sale tracking)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _trackingDate;
        protected SaleType _saleType;
        protected de.smava.webapp.marketing.affiliate.domain.AffiliateInformation _affiliateInformation;
        protected Long _brokerageApplicationId;
        
                            /**
     * Setter for the property 'tracking date'.
     */
    public void setTrackingDate(Date trackingDate) {
        if (!_trackingDateIsSet) {
            _trackingDateIsSet = true;
            _trackingDateInitVal = getTrackingDate();
        }
        registerChange("tracking date", _trackingDateInitVal, trackingDate);
        _trackingDate = trackingDate;
    }
                        
    /**
     * Returns the property 'tracking date'.
     */
    public Date getTrackingDate() {
        return _trackingDate;
    }
                                            
    /**
     * Setter for the property 'sale type'.
     */
    public void setSaleType(SaleType saleType) {
        _saleType = saleType;
    }
            
    /**
     * Returns the property 'sale type'.
     */
    public SaleType getSaleType() {
        return _saleType;
    }
                                            
    /**
     * Setter for the property 'affiliate information'.
     */
    public void setAffiliateInformation(de.smava.webapp.marketing.affiliate.domain.AffiliateInformation affiliateInformation) {
        _affiliateInformation = affiliateInformation;
    }
            
    /**
     * Returns the property 'affiliate information'.
     */
    public de.smava.webapp.marketing.affiliate.domain.AffiliateInformation getAffiliateInformation() {
        return _affiliateInformation;
    }
                                            
    /**
     * Setter for the property 'brokerage application id'.
     */
    public void setBrokerageApplicationId(Long brokerageApplicationId) {
        _brokerageApplicationId = brokerageApplicationId;
    }
            
    /**
     * Returns the property 'brokerage application id'.
     */
    public Long getBrokerageApplicationId() {
        return _brokerageApplicationId;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SaleTracking.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(SaleTracking.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SaleTracking asSaleTracking() {
        return this;
    }
}
