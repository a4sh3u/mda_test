package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingAdFormat;
import de.smava.webapp.marketing.domain.MarketingAdvertisement;
import de.smava.webapp.marketing.domain.MarketingPlacement;

import java.util.List;


/**
 * The domain object that represents 'MarketingAdFormats'.
 *
 * @author generator
 */
public interface MarketingAdFormatEntityInterface {

    /**
     * Setter for the property 'text'.
     *
     * 
     *
     */
    void setText(String text);

    /**
     * Returns the property 'text'.
     *
     * 
     *
     */
    String getText();
    /**
     * Setter for the property 'image width'.
     *
     * 
     *
     */
    void setImageWidth(int imageWidth);

    /**
     * Returns the property 'image width'.
     *
     * 
     *
     */
    int getImageWidth();
    /**
     * Setter for the property 'image height'.
     *
     * 
     *
     */
    void setImageHeight(int imageHeight);

    /**
     * Returns the property 'image height'.
     *
     * 
     *
     */
    int getImageHeight();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'image'.
     *
     * 
     *
     */
    void setImage(String image);

    /**
     * Returns the property 'image'.
     *
     * 
     *
     */
    String getImage();
    /**
     * Setter for the property 'marketing advertisement'.
     *
     * 
     *
     */
    void setMarketingAdvertisement(MarketingAdvertisement marketingAdvertisement);

    /**
     * Returns the property 'marketing advertisement'.
     *
     * 
     *
     */
    MarketingAdvertisement getMarketingAdvertisement();
    /**
     * Setter for the property 'placements'.
     *
     * 
     *
     */
    void setPlacements(List<MarketingPlacement> placements);

    /**
     * Returns the property 'placements'.
     *
     * 
     *
     */
    List<MarketingPlacement> getPlacements();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingAdFormat asMarketingAdFormat();
}
