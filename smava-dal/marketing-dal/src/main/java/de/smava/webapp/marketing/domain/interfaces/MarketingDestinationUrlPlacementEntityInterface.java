package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingDestinationUrl;
import de.smava.webapp.marketing.domain.MarketingDestinationUrlPlacement;
import de.smava.webapp.marketing.domain.MarketingPlacement;

import java.util.Date;


/**
 * The domain object that represents 'MarketingDestinationUrlPlacements'.
 *
 * @author generator
 */
public interface MarketingDestinationUrlPlacementEntityInterface {

    /**
     * Setter for the property 'marketing placement'.
     *
     * 
     *
     */
    void setMarketingPlacement(MarketingPlacement marketingPlacement);

    /**
     * Returns the property 'marketing placement'.
     *
     * 
     *
     */
    MarketingPlacement getMarketingPlacement();
    /**
     * Setter for the property 'marketing destination url'.
     *
     * 
     *
     */
    void setMarketingDestinationUrl(MarketingDestinationUrl marketingDestinationUrl);

    /**
     * Returns the property 'marketing destination url'.
     *
     * 
     *
     */
    MarketingDestinationUrl getMarketingDestinationUrl();
    /**
     * Setter for the property 'marketing test label'.
     *
     * 
     *
     */
    void setMarketingTestLabel(String marketingTestLabel);

    /**
     * Returns the property 'marketing test label'.
     *
     * 
     *
     */
    String getMarketingTestLabel();
    /**
     * Setter for the property 'valid from'.
     *
     * 
     *
     */
    void setValidFrom(Date validFrom);

    /**
     * Returns the property 'valid from'.
     *
     * 
     *
     */
    Date getValidFrom();
    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingDestinationUrlPlacement asMarketingDestinationUrlPlacement();
}
