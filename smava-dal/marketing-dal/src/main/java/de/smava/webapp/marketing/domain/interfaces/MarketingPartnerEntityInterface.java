package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingContact;
import de.smava.webapp.marketing.domain.MarketingPartner;
import de.smava.webapp.marketing.domain.MarketingPayment;
import de.smava.webapp.marketing.domain.MarketingPublication;

import java.util.Collection;
import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'MarketingPartners'.
 *
 * @author generator
 */
public interface MarketingPartnerEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'image'.
     *
     * 
     *
     */
    void setImage(String image);

    /**
     * Returns the property 'image'.
     *
     * 
     *
     */
    String getImage();
    /**
     * Setter for the property 'image width'.
     *
     * 
     *
     */
    void setImageWidth(int imageWidth);

    /**
     * Returns the property 'image width'.
     *
     * 
     *
     */
    int getImageWidth();
    /**
     * Setter for the property 'image height'.
     *
     * 
     *
     */
    void setImageHeight(int imageHeight);

    /**
     * Returns the property 'image height'.
     *
     * 
     *
     */
    int getImageHeight();
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    void setBankAccount(de.smava.webapp.account.domain.BankAccount bankAccount);

    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccount getBankAccount();
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(de.smava.webapp.account.domain.Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Address getAddress();
    /**
     * Setter for the property 'payments'.
     *
     * 
     *
     */
    void setPayments(List<MarketingPayment> payments);

    /**
     * Returns the property 'payments'.
     *
     * 
     *
     */
    List<MarketingPayment> getPayments();
    /**
     * Setter for the property 'publications'.
     *
     * 
     *
     */
    void setPublications(List<MarketingPublication> publications);

    /**
     * Returns the property 'publications'.
     *
     * 
     *
     */
    List<MarketingPublication> getPublications();
    /**
     * Setter for the property 'contacts'.
     *
     * 
     *
     */
    void setContacts(Collection<MarketingContact> contacts);

    /**
     * Returns the property 'contacts'.
     *
     * 
     *
     */
    Collection<MarketingContact> getContacts();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingPartner asMarketingPartner();
}
