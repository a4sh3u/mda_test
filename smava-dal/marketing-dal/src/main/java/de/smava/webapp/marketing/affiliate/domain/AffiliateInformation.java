//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.affiliate.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(affiliate information)}
import de.smava.webapp.commons.Validation;
import de.smava.webapp.marketing.affiliate.domain.history.AffiliateInformationHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AffiliateInformations'.
 *
 * @author generator
 */
public class AffiliateInformation extends AffiliateInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(affiliate information)}



    public AffiliateInformation() {

    }



    /**
     * Setter for the property 'external affiliate id'.
     */
    public void setExternalAffiliateId(String externalAffiliateId) {
        if (!_externalAffiliateIdIsSet) {
            _externalAffiliateIdIsSet = true;
            _externalAffiliateIdInitVal = getExternalAffiliateId();
        }
        registerChange("external affiliate id", _externalAffiliateIdInitVal, externalAffiliateId);
        if ( externalAffiliateId != null){
            _externalAffiliateId = encode(externalAffiliateId);
            _externalAffiliateId = Validation.validate(MAX_AFFILIATE_CHARS, _externalAffiliateId);

        } else {
            _externalAffiliateChannel = externalAffiliateId;
        }
    }

    /**
     * Returns the property 'external affiliate id'.
     */
    public String getExternalAffiliateId() {
        if ( _externalAffiliateId == null){
            return null;
        }
        return decode(_externalAffiliateId);
    }

    /**
     * Returns the initial value of the property 'external affiliate id'.
     */
    public String externalAffiliateIdInitVal() {
        String result;
        if (_externalAffiliateIdIsSet) {
            result = _externalAffiliateIdInitVal;
        } else {
            result = getExternalAffiliateId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external affiliate id'.
     */
    public boolean externalAffiliateIdIsDirty() {
        return !valuesAreEqual(externalAffiliateIdInitVal(), getExternalAffiliateId());
    }

    /**
     * Returns true if the setter method was called for the property 'external affiliate id'.
     */
    public boolean externalAffiliateIdIsSet() {
        return _externalAffiliateIdIsSet;
    }
    /**
     * Setter for the property 'external affiliate sub id'.
     */
    public void setExternalAffiliateSubId(String externalAffiliateSubId) {
        if (!_externalAffiliateSubIdIsSet) {
            _externalAffiliateSubIdIsSet = true;
            _externalAffiliateSubIdInitVal = getExternalAffiliateSubId();
        }
        registerChange("external affiliate sub id", _externalAffiliateSubIdInitVal, externalAffiliateSubId);

        if ( externalAffiliateSubId != null){
            _externalAffiliateSubId = encode(externalAffiliateSubId);
            _externalAffiliateSubId=Validation.validate(MAX_SUB_AFFILIATE_CHARS, _externalAffiliateSubId);
        } else {
            _externalAffiliateSubId = externalAffiliateSubId;
        }
    }

    /**
     * Returns the property 'external affiliate sub id'.
     */
    public String getExternalAffiliateSubId() {
        if ( _externalAffiliateSubId == null){
            return null;
        }
        return decode(_externalAffiliateSubId);
    }

    /**
     * Returns the initial value of the property 'external affiliate sub id'.
     */
    public String externalAffiliateSubIdInitVal() {
        String result;
        if (_externalAffiliateSubIdIsSet) {
            result = _externalAffiliateSubIdInitVal;
        } else {
            result = getExternalAffiliateSubId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external affiliate sub id'.
     */
    public boolean externalAffiliateSubIdIsDirty() {
        return !valuesAreEqual(externalAffiliateSubIdInitVal(), getExternalAffiliateSubId());
    }

    /**
     * Returns true if the setter method was called for the property 'external affiliate sub id'.
     */
    public boolean externalAffiliateSubIdIsSet() {
        return _externalAffiliateSubIdIsSet;
    }
    /**
     * Setter for the property 'external affiliate channel'.
     */
    public void setExternalAffiliateChannel(String externalAffiliateChannel) {
        if (!_externalAffiliateChannelIsSet) {
            _externalAffiliateChannelIsSet = true;
            _externalAffiliateChannelInitVal = getExternalAffiliateChannel();
        }
        registerChange("external affiliate channel", _externalAffiliateChannelInitVal, externalAffiliateChannel);
        if ( externalAffiliateChannel != null){
            _externalAffiliateChannel = encode(externalAffiliateChannel);
            _externalAffiliateChannel = Validation.validate(MAX_CHANNEL_CHARS, _externalAffiliateChannel);
        } else {
            _externalAffiliateChannel = externalAffiliateChannel;
        }
    }

    /**
     * Returns the property 'external affiliate channel'.
     */
    public String getExternalAffiliateChannel() {
        if ( _externalAffiliateChannel == null){
            return null;
        }
        return decode(_externalAffiliateChannel);
    }

    /**
     * Returns the initial value of the property 'external affiliate channel'.
     */
    public String externalAffiliateChannelInitVal() {
        String result;
        if (_externalAffiliateChannelIsSet) {
            result = _externalAffiliateChannelInitVal;
        } else {
            result = getExternalAffiliateChannel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external affiliate channel'.
     */
    public boolean externalAffiliateChannelIsDirty() {
        return !valuesAreEqual(externalAffiliateChannelInitVal(), getExternalAffiliateChannel());
    }

    /**
     * Returns true if the setter method was called for the property 'external affiliate channel'.
     */
    public boolean externalAffiliateChannelIsSet() {
        return _externalAffiliateChannelIsSet;
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _expirationDate;
        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.marketing.domain.MarketingPlacement _marketingPlacement;
        protected String _externalAffiliateId;
        protected String _externalAffiliateSubId;
        protected String _externalAffiliateChannel;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'marketing placement'.
     */
    public void setMarketingPlacement(de.smava.webapp.marketing.domain.MarketingPlacement marketingPlacement) {
        _marketingPlacement = marketingPlacement;
    }
            
    /**
     * Returns the property 'marketing placement'.
     */
    public de.smava.webapp.marketing.domain.MarketingPlacement getMarketingPlacement() {
        return _marketingPlacement;
    }
                                                
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AffiliateInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _externalAffiliateId=").append(_externalAffiliateId);
            builder.append("\n    _externalAffiliateSubId=").append(_externalAffiliateSubId);
            builder.append("\n    _externalAffiliateChannel=").append(_externalAffiliateChannel);
            builder.append("\n}");
        } else {
            builder.append(AffiliateInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AffiliateInformation asAffiliateInformation() {
        return this;
    }
}
