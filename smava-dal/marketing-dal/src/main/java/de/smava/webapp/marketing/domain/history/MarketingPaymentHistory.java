package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingPayment;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingPayments'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingPaymentHistory extends AbstractMarketingPayment {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _fromInitVal;
    protected transient boolean _fromIsSet;
    protected transient Date _untilInitVal;
    protected transient boolean _untilIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'from'.
     */
    public Date fromInitVal() {
        Date result;
        if (_fromIsSet) {
            result = _fromInitVal;
        } else {
            result = getFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'from'.
     */
    public boolean fromIsDirty() {
        return !valuesAreEqual(fromInitVal(), getFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'from'.
     */
    public boolean fromIsSet() {
        return _fromIsSet;
    }
	
    /**
     * Returns the initial value of the property 'until'.
     */
    public Date untilInitVal() {
        Date result;
        if (_untilIsSet) {
            result = _untilInitVal;
        } else {
            result = getUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'until'.
     */
    public boolean untilIsDirty() {
        return !valuesAreEqual(untilInitVal(), getUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'until'.
     */
    public boolean untilIsSet() {
        return _untilIsSet;
    }
		
}
