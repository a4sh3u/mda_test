//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Tools\workspace\smava-webapp-parent\account\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Tools\workspace\smava-webapp-parent\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url placement)}
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import javax.transaction.Synchronization;


import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.domain.MarketingDestinationUrl;
import de.smava.webapp.marketing.domain.MarketingDestinationUrlPlacement;
import de.smava.webapp.marketing.domain.MarketingPlacement;
//!!!!!!!! End of insert code section !!!!!!!!
/**
 * DAO interface for the domain object 'MarketingDestinationUrlPlacements'.
 *
 * @author generator
 */
public interface MarketingDestinationUrlPlacementDao extends BaseDao<MarketingDestinationUrlPlacement> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing destination url placement identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    @Deprecated
	MarketingDestinationUrlPlacement getMarketingDestinationUrlPlacement(Long id);

    /**
     * Saves the marketing destination url placement.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    @Deprecated
	Long saveMarketingDestinationUrlPlacement(MarketingDestinationUrlPlacement marketingDestinationUrlPlacement);

    /**
     * Deletes an marketing destination url placement, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing destination url placement
     */
    void deleteMarketingDestinationUrlPlacement(Long id);

    /**
     * Retrieves all 'MarketingDestinationUrlPlacement' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList();

    /**
     * Retrieves a page of 'MarketingDestinationUrlPlacement' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingDestinationUrlPlacement' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrlPlacement' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'MarketingDestinationUrlPlacement' instances.
     */
    long getMarketingDestinationUrlPlacementCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing destination url placement)}
    //
    Collection<MarketingUrlPlacementBean> getMarketingUrlPlacementBeanList(Sortable sortable, Pageable pageable, boolean validOnly);
    Collection<MarketingUrlPlacementBean> getMarketingUrlPlacementBeanList(MarketingDestinationUrl marketingDestinationUrl);
    long getCountMarketingUrlPlacementBeanList(Sortable sortable, Pageable pageable, boolean validOnly);

	MarketingDestinationUrlPlacement findByPlacementIdAndTestLabel(long placementId, String testLabel, boolean validOnly);
	String getMarketingTestLabel(long placementId, String marketingDestinationUrl);
	
	boolean isMarketingTestLabelPresent(String testLabel);
	
	void updateInvalidateForPlacements(Collection<Long> placementIds);
	
	List<MarketingTestView> findAllMarketingTests(Pageable pageable);
	
	long getCountForAllMarketingTests();
	
	long getCountRunningTests(Timestamp now);
	
	MarketingDestinationUrlPlacement getOldMapping(MarketingDestinationUrlPlacement urlPlacement);

	List<MarketingDestinationUrlPlacement> getSortedUrlPlacements(MarketingPlacement placement);
	Collection<MarketingDestinationUrlPlacement> findByIds(Collection<Long> ids);

    Collection<MarketingDestinationUrlPlacement> findValidMarketingDestinationUrlPlacementList();
    long getValidUrlsCount(MarketingPlacement placement);
    //
    // !!!!!!!! End of insert code section !!!!!!!!


}