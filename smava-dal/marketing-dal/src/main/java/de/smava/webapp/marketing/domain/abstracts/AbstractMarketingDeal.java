//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing deal)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.marketing.domain.interfaces.MarketingDealEntityInterface;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingDeals'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingDeal
    extends KreditPrivatEntity implements MarketingDealEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingDeal.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing deal)}
    public static final String TYPE_LENDER_AGIO = "LENDER_AGIO";
    public static final String TYPE_LENDER_PROV_1YEAR = "LENDER_PROV_1YEAR";
    public static final String TYPE_LENDER_PROV_2YEAR = "LENDER_PROV_2YEAR";
    public static final String TYPE_LENDER_PROV_3YEAR = "LENDER_PROV_3YEAR";
    public static final String TYPE_LENDER_PROV_4YEAR = "LENDER_PROV_4YEAR";
    public static final String TYPE_PER_HIT = "PER_HIT";
    public static final String TYPE_PER_OFFER = "PER_OFFER";
    public static final String TYPE_PER_ORDER = "PER_ORDER";
    public static final String TYPE_PER_ACCOUNT = "PER_ACCOUNT";
    public static final String TYPE_PER_LENDER = "PER_LENDER";
    public static final String TYPE_PER_BORROWER = "PER_BORROWER";
    public static final String TYPE_PER_CONTRACT = "PER_CONTRACT";
    public static final String TYPE_MAKLER_COURTAGE = "MAKLER_COURTAGE"; // WEBSITE-3026
    // for offline placements:
    public static final String TYPE_PER_CUSTOMER = "PER_CUSTOMER";
    // !!!!!!!! End of insert code section !!!!!!!!
}

