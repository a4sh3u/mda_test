package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingDestinationUrl;
import de.smava.webapp.marketing.domain.MarketingDestinationUrlPlacement;

import java.util.Collection;


/**
 * The domain object that represents 'MarketingDestinationUrls'.
 *
 * @author generator
 */
public interface MarketingDestinationUrlEntityInterface {

    /**
     * Setter for the property 'url'.
     *
     * 
     *
     */
    void setUrl(String url);

    /**
     * Returns the property 'url'.
     *
     * 
     *
     */
    String getUrl();
    /**
     * Setter for the property 'marketing destination url placements'.
     *
     * 
     *
     */
    void setMarketingDestinationUrlPlacements(Collection<MarketingDestinationUrlPlacement> marketingDestinationUrlPlacements);

    /**
     * Returns the property 'marketing destination url placements'.
     *
     * 
     *
     */
    Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacements();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingDestinationUrl asMarketingDestinationUrl();
}
