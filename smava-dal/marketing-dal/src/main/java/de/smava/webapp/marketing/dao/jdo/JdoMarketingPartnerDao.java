//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing partner)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingPartnerDao;
import de.smava.webapp.marketing.domain.MarketingPartner;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingPartners'.
 *
 * @author generator
 */
@Repository(value = "marketingPartnerDao")
public class JdoMarketingPartnerDao extends JdoBaseDao implements MarketingPartnerDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingPartnerDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing partner)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing partner identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingPartner load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartner - start: id=" + id);
        }
        MarketingPartner result = getEntity(MarketingPartner.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartner - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingPartner getMarketingPartner(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingPartner entity = findUniqueEntity(MarketingPartner.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing partner.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingPartner marketingPartner) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingPartner: " + marketingPartner);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing partner)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingPartner);
    }

    /**
     * @deprecated Use {@link #save(MarketingPartner) instead}
     */
    public Long saveMarketingPartner(MarketingPartner marketingPartner) {
        return save(marketingPartner);
    }

    /**
     * Deletes an marketing partner, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing partner
     */
    public void deleteMarketingPartner(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingPartner: " + id);
        }
        deleteEntity(MarketingPartner.class, id);
    }

    /**
     * Retrieves all 'MarketingPartner' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingPartner> getMarketingPartnerList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - start");
        }
        Collection<MarketingPartner> result = getEntities(MarketingPartner.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingPartner' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingPartner> getMarketingPartnerList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - start: pageable=" + pageable);
        }
        Collection<MarketingPartner> result = getEntities(MarketingPartner.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPartner' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingPartner> getMarketingPartnerList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - start: sortable=" + sortable);
        }
        Collection<MarketingPartner> result = getEntities(MarketingPartner.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPartner' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingPartner> getMarketingPartnerList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPartner> result = getEntities(MarketingPartner.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPartner' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPartner> findMarketingPartnerList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingPartner> result = findEntities(MarketingPartner.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPartner' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingPartner> findMarketingPartnerList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingPartner> result = findEntities(MarketingPartner.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingPartner' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPartner> findMarketingPartnerList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingPartner> result = findEntities(MarketingPartner.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPartner' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPartner> findMarketingPartnerList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingPartner> result = findEntities(MarketingPartner.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPartner' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPartner> findMarketingPartnerList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPartner> result = findEntities(MarketingPartner.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPartner' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPartner> findMarketingPartnerList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPartner> result = findEntities(MarketingPartner.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPartnerList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPartner' instances.
     */
    public long getMarketingPartnerCount() {
        long result = getEntityCount(MarketingPartner.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPartner' instances which match the given whereClause.
     */
    public long getMarketingPartnerCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingPartner.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPartner' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingPartnerCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingPartner.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPartnerCount() - result: count=" + result);
        }
        return result;
    }

	


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing partner)}
    //
    public MarketingPartner getById(Long id) {
		return getById(MarketingPartner.class, id);
	}
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
