//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact placement)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingContactPlacementDao;
import de.smava.webapp.marketing.domain.MarketingContactPlacement;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingContactPlacements'.
 *
 * @author generator
 */
@Repository(value = "marketingContactPlacementDao")
public class JdoMarketingContactPlacementDao extends JdoBaseDao implements MarketingContactPlacementDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingContactPlacementDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing contact placement)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing contact placement identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingContactPlacement load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacement - start: id=" + id);
        }
        MarketingContactPlacement result = getEntity(MarketingContactPlacement.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacement - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingContactPlacement getMarketingContactPlacement(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingContactPlacement entity = findUniqueEntity(MarketingContactPlacement.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing contact placement.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingContactPlacement marketingContactPlacement) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingContactPlacement: " + marketingContactPlacement);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing contact placement)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingContactPlacement);
    }

    /**
     * @deprecated Use {@link #save(MarketingContactPlacement) instead}
     */
    public Long saveMarketingContactPlacement(MarketingContactPlacement marketingContactPlacement) {
        return save(marketingContactPlacement);
    }

    /**
     * Deletes an marketing contact placement, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing contact placement
     */
    public void deleteMarketingContactPlacement(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingContactPlacement: " + id);
        }
        deleteEntity(MarketingContactPlacement.class, id);
    }

    /**
     * Retrieves all 'MarketingContactPlacement' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacementList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - start");
        }
        Collection<MarketingContactPlacement> result = getEntities(MarketingContactPlacement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingContactPlacement' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacementList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - start: pageable=" + pageable);
        }
        Collection<MarketingContactPlacement> result = getEntities(MarketingContactPlacement.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingContactPlacement' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacementList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - start: sortable=" + sortable);
        }
        Collection<MarketingContactPlacement> result = getEntities(MarketingContactPlacement.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingContactPlacement' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacementList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingContactPlacement> result = getEntities(MarketingContactPlacement.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingContactPlacement' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContactPlacement> findMarketingContactPlacementList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingContactPlacement> result = findEntities(MarketingContactPlacement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingContactPlacement' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingContactPlacement> findMarketingContactPlacementList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingContactPlacement> result = findEntities(MarketingContactPlacement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingContactPlacement' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContactPlacement> findMarketingContactPlacementList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingContactPlacement> result = findEntities(MarketingContactPlacement.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingContactPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContactPlacement> findMarketingContactPlacementList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingContactPlacement> result = findEntities(MarketingContactPlacement.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingContactPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContactPlacement> findMarketingContactPlacementList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingContactPlacement> result = findEntities(MarketingContactPlacement.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingContactPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContactPlacement> findMarketingContactPlacementList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingContactPlacement> result = findEntities(MarketingContactPlacement.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingContactPlacement' instances.
     */
    public long getMarketingContactPlacementCount() {
        long result = getEntityCount(MarketingContactPlacement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingContactPlacement' instances which match the given whereClause.
     */
    public long getMarketingContactPlacementCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingContactPlacement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingContactPlacement' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingContactPlacementCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingContactPlacement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactPlacementCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing contact placement)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
