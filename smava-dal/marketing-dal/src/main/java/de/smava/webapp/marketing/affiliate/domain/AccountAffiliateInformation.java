//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.affiliate.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account affiliate information)}
import de.smava.webapp.marketing.affiliate.domain.history.AccountAffiliateInformationHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AccountAffiliateInformations'.
 *
 * @author generator
 */
public class AccountAffiliateInformation extends AccountAffiliateInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(account affiliate information)}
    // !!!!!!!! End of insert code section !!!!!!!!


        protected de.smava.webapp.account.domain.Account _account;
        protected AffiliateInformation _firstAffiliate;
        protected AffiliateInformation _saleAffiliate;
        
                /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                        /**
     * Setter for the property 'first affiliate'.
     */
    public void setFirstAffiliate(AffiliateInformation firstAffiliate) {
        _firstAffiliate = firstAffiliate;
    }

    /**
     * Returns the property 'first affiliate'.
     */
    public AffiliateInformation getFirstAffiliate() {
        return _firstAffiliate;
    }
                        /**
     * Setter for the property 'sale affiliate'.
     */
    public void setSaleAffiliate(AffiliateInformation saleAffiliate) {
        _saleAffiliate = saleAffiliate;
    }

    /**
     * Returns the property 'sale affiliate'.
     */
    public AffiliateInformation getSaleAffiliate() {
        return _saleAffiliate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AccountAffiliateInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AccountAffiliateInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AccountAffiliateInformation asAccountAffiliateInformation() {
        return this;
    }
}
