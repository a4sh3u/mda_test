//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing payment)}
import java.util.Arrays;
import java.util.Collection;

import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingPaymentDao;
import de.smava.webapp.marketing.domain.MarketingPayment;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingPayments'.
 *
 * @author generator
 */
@Repository(value = "marketingPaymentDao")
public class JdoMarketingPaymentDao extends JdoBaseDao implements MarketingPaymentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingPaymentDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing payment)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing payment identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingPayment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPayment - start: id=" + id);
        }
        MarketingPayment result = getEntity(MarketingPayment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPayment - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingPayment getMarketingPayment(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingPayment entity = findUniqueEntity(MarketingPayment.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing payment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingPayment marketingPayment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingPayment: " + marketingPayment);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing payment)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingPayment);
    }

    /**
     * @deprecated Use {@link #save(MarketingPayment) instead}
     */
    public Long saveMarketingPayment(MarketingPayment marketingPayment) {
        return save(marketingPayment);
    }

    /**
     * Deletes an marketing payment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing payment
     */
    public void deleteMarketingPayment(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingPayment: " + id);
        }
        deleteEntity(MarketingPayment.class, id);
    }

    /**
     * Retrieves all 'MarketingPayment' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingPayment> getMarketingPaymentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - start");
        }
        Collection<MarketingPayment> result = getEntities(MarketingPayment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingPayment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingPayment> getMarketingPaymentList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - start: pageable=" + pageable);
        }
        Collection<MarketingPayment> result = getEntities(MarketingPayment.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPayment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingPayment> getMarketingPaymentList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - start: sortable=" + sortable);
        }
        Collection<MarketingPayment> result = getEntities(MarketingPayment.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPayment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingPayment> getMarketingPaymentList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPayment> result = getEntities(MarketingPayment.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPayment' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPayment> findMarketingPaymentList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingPayment> result = findEntities(MarketingPayment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPayment' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingPayment> findMarketingPaymentList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingPayment> result = findEntities(MarketingPayment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingPayment' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPayment> findMarketingPaymentList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingPayment> result = findEntities(MarketingPayment.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPayment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPayment> findMarketingPaymentList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingPayment> result = findEntities(MarketingPayment.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPayment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPayment> findMarketingPaymentList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPayment> result = findEntities(MarketingPayment.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPayment' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPayment> findMarketingPaymentList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingPayment> result = findEntities(MarketingPayment.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingPaymentList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPayment' instances.
     */
    public long getMarketingPaymentCount() {
        long result = getEntityCount(MarketingPayment.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPayment' instances which match the given whereClause.
     */
    public long getMarketingPaymentCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingPayment.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPayment' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingPaymentCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingPayment.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingPaymentCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing payment)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
