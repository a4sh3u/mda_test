//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Tools\workspace\smava-webapp-parent\account\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Tools\workspace\smava-webapp-parent\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url placement)}
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingDestinationUrlPlacementDao;
import de.smava.webapp.marketing.dao.MarketingTestView;
import de.smava.webapp.marketing.dao.MarketingUrlPlacementBean;
import de.smava.webapp.marketing.domain.MarketingDestinationUrl;
import de.smava.webapp.marketing.domain.MarketingDestinationUrlPlacement;
import de.smava.webapp.marketing.domain.MarketingPlacement;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingDestinationUrlPlacements'.
 *
 * @author generator
 */
@Repository(value = "marketingDestinationUrlPlacementDao")
public class JdoMarketingDestinationUrlPlacementDao extends JdoBaseDao implements MarketingDestinationUrlPlacementDao {



    private static final Logger LOGGER = Logger.getLogger(JdoMarketingDestinationUrlPlacementDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing destination url placement)}
        //
    private static final String SELECT_URL_SQL
        = "select id as \"id\" "
        + "from marketing_destination_url_placement "
        + "where placement_id = :marketingPlacementId "
        + "and marketing_test_label = :marketingTestLabel ";

    private static final String INVALIDATE_MAPPINGS_UPDATE_SQL
        = "update marketing_destination_url_placement "
        + "set valid_until = :now "
        + "where valid_until is null and placement_id in (:placementIds)";
    
    private static final String MARKETING_TEST_VIEW_SELECT
        = "select "
        + "	mdup.marketing_test_label as \"testLabel\", "
        + "	min(mdup.valid_from) as \"validFrom\", "
        + "	(select  "
        + "		case  "
        + "			when mdup2.valid_until is null then '3000-01-01 00:00:00' "
        + "			else mdup2.valid_until "
        + "		end as until2 "
        + "	from marketing_destination_url_placement mdup2 where mdup2.marketing_test_label = mdup.marketing_test_label order by until2 desc limit 1 "
        + "	) as \"validUntil\", 	 "
        + "	count(mdup.id) as \"placementCount\", 	 "
        + "	mdu.url as \"destinationUrl\"  "
        + "from marketing_destination_url_placement mdup  "
        + "inner join marketing_destination_url mdu on mdu.id = mdup.destination_url_id  "
        + "where mdup.marketing_test_label is not null  "
        + "group by mdup.marketing_test_label, mdu.url  "
        + "order by \"validUntil\" desc, mdup.marketing_test_label "
        + "OFFSET :offset LIMIT :limit";

    private static final String MARKETING_TEST_VIEW_COUNT
        = "select count(distinct mdup.marketing_test_label) from marketing_destination_url_placement mdup "
        + "where mdup.marketing_test_label is not null ";
    
    private static final String MARKETING_TEST_RUNNING_COUNT
        = MARKETING_TEST_VIEW_COUNT
        + " and (mdup.valid_until is null or mdup.valid_until > :now)";

    private static final String MARKETING_URL_PLACEMENT_COLUMNS 
        = "mdup.id AS \"marketingDestinationUrlPlacementId\", "
        + "mdu.url AS \"marketingDestinationUrlLink\", "
        + "mdu.id AS \"marketingDestinationUrlId\", "
        + "mpl.id AS \"marketingPlacementId\", "
        + "mdup.valid_from AS \"placementValidFrom\", "
        + "mpu.name AS \"publicationName\", "
        + "mp.name AS \"partnerName\", "
        + "mc.name AS \"campaignName\" ";

    private static final String MARKETING_URL_OVERVIEW_SELECT_COLUMNS_NULLS
        = "null AS \"marketingDestinationUrlPlacementId\", "
        + "mdu.url AS \"marketingDestinationUrlLink\", "
        + "mdu.id AS \"marketingDestinationUrlId\", "
        + "null AS \"marketingPlacementId\", "
        + "null AS \"placementValidFrom\", "
        + "null AS \"publicationName\", "
        + "null AS \"partnerName\", "
        + "null AS \"campaignName\" ";

    private static final String MARKETING_URL_PLACEMENT_FROM 
        = "FROM "
        + "marketing_destination_url mdu LEFT JOIN marketing_destination_url_placement mdup ON mdu.id = mdup.destination_url_id "
        + "LEFT JOIN marketing_placement mpl ON mpl.id = mdup.placement_id "
        + "LEFT JOIN marketing_publication mpu ON mpl.publication = mpu.id "
        + "LEFT JOIN marketing_campaign mc ON mc.id = mpl.campaign "
        + "LEFT JOIN marketing_partner mp ON mp.id = mpu.marketing_partner ";
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    @Override
    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing destination url placement identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingDestinationUrlPlacement load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacement - start: id=" + id);
        }
        MarketingDestinationUrlPlacement result = getEntity(MarketingDestinationUrlPlacement.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacement - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    @Deprecated
    public MarketingDestinationUrlPlacement getMarketingDestinationUrlPlacement(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            MarketingDestinationUrlPlacement entity = findUniqueEntity(MarketingDestinationUrlPlacement.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the marketing destination url placement.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingDestinationUrlPlacement marketingDestinationUrlPlacement) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingDestinationUrlPlacement: " + marketingDestinationUrlPlacement);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing destination url placement)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingDestinationUrlPlacement);
    }

    /**
     * @deprecated Use {@link #save(MarketingDestinationUrlPlacement) instead}
     */
    @Deprecated
    public Long saveMarketingDestinationUrlPlacement(MarketingDestinationUrlPlacement marketingDestinationUrlPlacement) {
        return save(marketingDestinationUrlPlacement);
    }

    /**
     * Deletes an marketing destination url placement, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing destination url placement
     */
    public void deleteMarketingDestinationUrlPlacement(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingDestinationUrlPlacement: " + id);
        }
        deleteEntity(MarketingDestinationUrlPlacement.class, id);
    }

    /**
     * Retrieves all 'MarketingDestinationUrlPlacement' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - start");
        }
        Collection<MarketingDestinationUrlPlacement> result = getEntities(MarketingDestinationUrlPlacement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingDestinationUrlPlacement' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - start: pageable=" + pageable);
        }
        Collection<MarketingDestinationUrlPlacement> result = getEntities(MarketingDestinationUrlPlacement.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingDestinationUrlPlacement' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - start: sortable=" + sortable);
        }
        Collection<MarketingDestinationUrlPlacement> result = getEntities(MarketingDestinationUrlPlacement.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrlPlacement' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacementList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrlPlacement> result = getEntities(MarketingDestinationUrlPlacement.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingDestinationUrlPlacement' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrlPlacement> findMarketingDestinationUrlPlacementList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingDestinationUrlPlacement> result = findEntities(MarketingDestinationUrlPlacement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingDestinationUrlPlacement' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrlPlacement> findMarketingDestinationUrlPlacementList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingDestinationUrlPlacement> result = findEntities(MarketingDestinationUrlPlacement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingDestinationUrlPlacement' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrlPlacement> findMarketingDestinationUrlPlacementList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingDestinationUrlPlacement> result = findEntities(MarketingDestinationUrlPlacement.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingDestinationUrlPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrlPlacement> findMarketingDestinationUrlPlacementList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrlPlacement> result = findEntities(MarketingDestinationUrlPlacement.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrlPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrlPlacement> findMarketingDestinationUrlPlacementList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrlPlacement> result = findEntities(MarketingDestinationUrlPlacement.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrlPlacement' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingDestinationUrlPlacement> findMarketingDestinationUrlPlacementList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingDestinationUrlPlacement> result = findEntities(MarketingDestinationUrlPlacement.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingDestinationUrlPlacementList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDestinationUrlPlacement' instances.
     */
    public long getMarketingDestinationUrlPlacementCount() {
        long result = getEntityCount(MarketingDestinationUrlPlacement.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDestinationUrlPlacement' instances which match the given whereClause.
     */
    public long getMarketingDestinationUrlPlacementCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingDestinationUrlPlacement.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingDestinationUrlPlacement' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingDestinationUrlPlacementCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingDestinationUrlPlacement.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingDestinationUrlPlacementCount() - result: count=" + result);
        }
        return result;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing destination url placement)}
    //
    public Collection<MarketingUrlPlacementBean> getMarketingUrlPlacementBeanList(final Sortable sortable, final Pageable pageable, boolean validOnly) {
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append(MARKETING_URL_PLACEMENT_COLUMNS);

        sql.append(getSqlFrom(sortable, pageable, validOnly, false));

        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        query.setResultClass(MarketingUrlPlacementBean.class);

        Collection<MarketingUrlPlacementBean> result = (Collection<MarketingUrlPlacementBean>) query.execute();

        return result;
    }

    public Collection<MarketingUrlPlacementBean> getMarketingUrlPlacementBeanList(MarketingDestinationUrl marketingDestinationUrl) {
        StringBuilder sql = new StringBuilder("SELECT ");
        sql.append(MARKETING_URL_PLACEMENT_COLUMNS);
        sql.append(MARKETING_URL_PLACEMENT_FROM);
        sql.append("WHERE mdu.id = :id AND mdup.valid_until IS NULL");

        Map<String, Long> parameters = new HashMap<String, Long>(1);
        parameters.put("id", marketingDestinationUrl.getId());
        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        query.setResultClass(MarketingUrlPlacementBean.class);

        Collection<MarketingUrlPlacementBean> result = (Collection<MarketingUrlPlacementBean>) query.executeWithMap(parameters);

        return result;
    }

    public long getCountMarketingUrlPlacementBeanList(Sortable sortable, Pageable pageable, boolean validOnly) {
        StringBuilder sql = new StringBuilder("SELECT count(*) FROM (SELECT mdu.id, mpl.id ");
        sql.append(getSqlFrom(sortable, pageable, validOnly, true));

        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        query.setResultClass(Long.class);
        query.setUnique(true);

        Long result = (Long) query.execute();

        return result;
    }

    private String getSqlFrom(final Sortable sortable, final Pageable pageable, 
            boolean validOnly, boolean forCount) {
        StringBuilder sql = new StringBuilder(MARKETING_URL_PLACEMENT_FROM);

        if (validOnly) {
            sql.append("WHERE mdup.valid_until IS NULL ");
            sql.append("UNION ");
            sql.append("SELECT ");
            if (forCount) {
                sql.append("mdu.id, null ");
            } else {
                sql.append(MARKETING_URL_OVERVIEW_SELECT_COLUMNS_NULLS);
        }
            sql.append(" FROM marketing_destination_url mdu where (select count(mdup2.id) from marketing_destination_url_placement mdup2 where mdup2.destination_url_id = mdu.id) = (select count(mdup2.id) from marketing_destination_url_placement mdup2 where mdup2.destination_url_id = mdu.id and mdup2.valid_until IS NOT NULL) ");
            if (forCount) {
                sql.append(") as tmp ");
            }
        }


        if (sortable != null) {
            sql.append("ORDER BY ").append(sortable.getSort()).append(sortable.sortDescending() ? " DESC" : " ASC");
        }

        if (pageable != null) {
            sql.append(" LIMIT ").append(pageable.getItemsPerPage());
            sql.append(" OFFSET ").append(pageable.getOffset());
        }

        return sql.toString();
    }

    @Override
    public MarketingDestinationUrlPlacement findByPlacementIdAndTestLabel(
            long placementId, String testLabel, boolean validOnly) {
        StringBuilder sql = new StringBuilder(SELECT_URL_SQL);
        if (validOnly) {
            sql.append("and valid_until is null");
        }

        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        query.setUnique(true);
        query.setClass(MarketingDestinationUrlPlacement.class);

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("marketingPlacementId", placementId);
        params.put("marketingTestLabel", testLabel);
        MarketingDestinationUrlPlacement result = (MarketingDestinationUrlPlacement) query.executeWithMap(params);

        return result;
    }

    @Override
    public String getMarketingTestLabel(long placementId,
            String marketingDestinationUrl) {
        Query query = getPersistenceManager().newNamedQuery(MarketingDestinationUrlPlacement.class, "findLabelByPlacementIdAndMarketingUrl");
        query.setUnique(true);
        Map<String, Object> params = new HashMap<String, Object>(3);
        params.put("marketingDestinationUrl", marketingDestinationUrl);
        params.put("marketingPlacementId", placementId);

        String result = null;
        MarketingDestinationUrlPlacement urlPlacement = (MarketingDestinationUrlPlacement) query.executeWithMap(params);
        if (urlPlacement != null) {
            result = urlPlacement.getMarketingTestLabel();
        }

        return result;
    }

    public boolean isMarketingTestLabelPresent(String testLabel) {
        final Query query = getPersistenceManager().newNamedQuery(MarketingDestinationUrlPlacement.class, "findTestLabel");
        final Collection<MarketingDestinationUrlPlacement> result = (Collection<MarketingDestinationUrlPlacement>) query.executeWithMap(getSingleParamMap("testLabel", testLabel));
        return !result.isEmpty();
    }

    public void updateInvalidateForPlacements(Collection<Long> placementIds) {
        if (CollectionUtils.isNotEmpty(placementIds)) {
            final Map<String, Object> params = new LinkedHashMap<String, Object>();
            params.put("now", new Timestamp(CurrentDate.getTime()));
            params.put("placementIds", placementIds);
            final String sql = replaceCollectionTypes(params, INVALIDATE_MAPPINGS_UPDATE_SQL);
            final Query query = getPersistenceManager().newQuery(Query.SQL, sql);
            query.executeWithMap(params);
        }
    }

    public List<MarketingTestView> findAllMarketingTests(Pageable pageable) {
        final Map<String, Object> params = new LinkedHashMap<String, Object>(2);
        params.put("offset", pageable.getOffset());
        params.put("limit", pageable.getItemsPerPage());
        final Query query = getPersistenceManager().newQuery(Query.SQL, MARKETING_TEST_VIEW_SELECT);
        query.setResultClass(MarketingTestView.class);
        return (List<MarketingTestView>) query.executeWithMap(params);
    }

    public long getCountForAllMarketingTests() {
        final Long result = (Long) executeWithParamsUnique(MARKETING_TEST_VIEW_COUNT, Collections.EMPTY_MAP, Long.class);
        return result != null ? result : 0;
    }

    public long getCountRunningTests(final Timestamp now) {
        final Long result = executeWithParamsUnique(MARKETING_TEST_RUNNING_COUNT, getSingleParamMap("now", now), Long.class);
        return result != null ? result : 0;
    }

    @Override
    public MarketingDestinationUrlPlacement getOldMapping(final MarketingDestinationUrlPlacement urlPlacement) {
        final Map<String, Object> params = new LinkedHashMap<String, Object>(2);
        params.put("placementId", urlPlacement.getMarketingPlacement().getId());
        return executeNamedQuery(MarketingDestinationUrlPlacement.class, "findOldMapping", params, null, true);
    }

    public List<MarketingDestinationUrlPlacement> getSortedUrlPlacements(
            MarketingPlacement placement) {
        final Query query = getPersistenceManager().newNamedQuery(MarketingDestinationUrlPlacement.class, "getSortedElements");
        final Map<String, Object> params = new LinkedHashMap<String, Object>(1);
        params.put("marketingPlacement", placement);

        return (List<MarketingDestinationUrlPlacement>) query.executeWithMap(params);
    }


    @Override
    public Collection<MarketingDestinationUrlPlacement> findByIds(
            Collection<Long> ids) {
        return findByIds(MarketingDestinationUrlPlacement.class, ids);
    }

    @Override
    public Collection<MarketingDestinationUrlPlacement> findValidMarketingDestinationUrlPlacementList(){
        return findMarketingDestinationUrlPlacementList("_validUntil == null && _marketingTestLabel != null");
    }

    @Override
    public long getValidUrlsCount(MarketingPlacement placement){
        String whereClause = "this._marketingPlacement == marketingPlacement && this._validUntil == null"
                + " parameters de.smava.webapp.marketing.domain.MarketingPlacement marketingPlacement";
        return getMarketingDestinationUrlPlacementCount( whereClause, new Object[] {placement});
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!



}
