//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing payment)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;


import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.domain.MarketingPayment;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * DAO interface for the domain object 'MarketingPayments'.
 *
 * @author generator
 */
public interface MarketingPaymentDao extends BaseDao<MarketingPayment>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing payment identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MarketingPayment getMarketingPayment(Long id);

    /**
     * Saves the marketing payment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMarketingPayment(MarketingPayment marketingPayment);

    /**
     * Deletes an marketing payment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing payment
     */
    void deleteMarketingPayment(Long id);

    /**
     * Retrieves all 'MarketingPayment' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingPayment> getMarketingPaymentList();

    /**
     * Retrieves a page of 'MarketingPayment' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingPayment> getMarketingPaymentList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingPayment' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingPayment> getMarketingPaymentList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingPayment' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingPayment> getMarketingPaymentList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'MarketingPayment' instances.
     */
    long getMarketingPaymentCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing payment)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
