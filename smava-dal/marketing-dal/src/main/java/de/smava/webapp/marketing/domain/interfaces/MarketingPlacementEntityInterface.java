package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The domain object that represents 'MarketingPlacements'.
 *
 * @author generator
 */
public interface MarketingPlacementEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'quantity'.
     *
     * 
     *
     */
    void setQuantity(int quantity);

    /**
     * Returns the property 'quantity'.
     *
     * 
     *
     */
    int getQuantity();
    /**
     * Setter for the property 'position'.
     *
     * 
     *
     */
    void setPosition(String position);

    /**
     * Returns the property 'position'.
     *
     * 
     *
     */
    String getPosition();
    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    void setComment(String comment);

    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    String getComment();
    /**
     * Setter for the property 'publication'.
     *
     * 
     *
     */
    void setPublication(MarketingPublication publication);

    /**
     * Returns the property 'publication'.
     *
     * 
     *
     */
    MarketingPublication getPublication();
    /**
     * Setter for the property 'ad format'.
     *
     * 
     *
     */
    void setAdFormat(MarketingAdFormat adFormat);

    /**
     * Returns the property 'ad format'.
     *
     * 
     *
     */
    MarketingAdFormat getAdFormat();
    /**
     * Setter for the property 'campaign'.
     *
     * 
     *
     */
    void setCampaign(MarketingCampaign campaign);

    /**
     * Returns the property 'campaign'.
     *
     * 
     *
     */
    MarketingCampaign getCampaign();
    /**
     * Setter for the property 'marketing destination url placements'.
     *
     * 
     *
     */
    void setMarketingDestinationUrlPlacements(Set<MarketingDestinationUrlPlacement> marketingDestinationUrlPlacements);

    /**
     * Returns the property 'marketing destination url placements'.
     *
     * 
     *
     */
    Set<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacements();
    /**
     * Setter for the property 'payments'.
     *
     * 
     *
     */
    void setPayments(List<MarketingPayment> payments);

    /**
     * Returns the property 'payments'.
     *
     * 
     *
     */
    List<MarketingPayment> getPayments();
    /**
     * Setter for the property 'deals'.
     *
     * 
     *
     */
    void setDeals(List<MarketingDeal> deals);

    /**
     * Returns the property 'deals'.
     *
     * 
     *
     */
    List<MarketingDeal> getDeals();
    /**
     * Setter for the property 'marketing contact placements'.
     *
     * 
     *
     */
    void setMarketingContactPlacements(Collection<MarketingContactPlacement> marketingContactPlacements);

    /**
     * Returns the property 'marketing contact placements'.
     *
     * 
     *
     */
    Collection<MarketingContactPlacement> getMarketingContactPlacements();
    /**
     * Setter for the property 'alternate code'.
     *
     * 
     *
     */
    void setAlternateCode(String alternateCode);

    /**
     * Returns the property 'alternate code'.
     *
     * 
     *
     */
    String getAlternateCode();
    /**
     * Setter for the property 'no brokerage'.
     *
     * 
     *
     */
    void setNoBrokerage(Boolean noBrokerage);

    /**
     * Returns the property 'no brokerage'.
     *
     * 
     *
     */
    Boolean getNoBrokerage();
    /**
     * Setter for the property 'user data sharing'.
     *
     * 
     *
     */
    void setUserDataSharing(Boolean userDataSharing);

    /**
     * Returns the property 'user data sharing'.
     *
     * 
     *
     */
    Boolean getUserDataSharing();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingPlacement asMarketingPlacement();
}
