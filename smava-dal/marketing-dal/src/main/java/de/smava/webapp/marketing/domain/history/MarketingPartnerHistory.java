package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingPartner;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingPartners'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingPartnerHistory extends AbstractMarketingPartner {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _imageInitVal;
    protected transient boolean _imageIsSet;
    protected transient int _imageWidthInitVal;
    protected transient boolean _imageWidthIsSet;
    protected transient int _imageHeightInitVal;
    protected transient boolean _imageHeightIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image'.
     */
    public String imageInitVal() {
        String result;
        if (_imageIsSet) {
            result = _imageInitVal;
        } else {
            result = getImage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image'.
     */
    public boolean imageIsDirty() {
        return !valuesAreEqual(imageInitVal(), getImage());
    }

    /**
     * Returns true if the setter method was called for the property 'image'.
     */
    public boolean imageIsSet() {
        return _imageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image width'.
     */
    public int imageWidthInitVal() {
        int result;
        if (_imageWidthIsSet) {
            result = _imageWidthInitVal;
        } else {
            result = getImageWidth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image width'.
     */
    public boolean imageWidthIsDirty() {
        return !valuesAreEqual(imageWidthInitVal(), getImageWidth());
    }

    /**
     * Returns true if the setter method was called for the property 'image width'.
     */
    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'image height'.
     */
    public int imageHeightInitVal() {
        int result;
        if (_imageHeightIsSet) {
            result = _imageHeightInitVal;
        } else {
            result = getImageHeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'image height'.
     */
    public boolean imageHeightIsDirty() {
        return !valuesAreEqual(imageHeightInitVal(), getImageHeight());
    }

    /**
     * Returns true if the setter method was called for the property 'image height'.
     */
    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }
					
}
