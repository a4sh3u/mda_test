//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing partner)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.marketing.domain.MarketingContact;
import de.smava.webapp.marketing.domain.interfaces.MarketingPartnerEntityInterface;
import org.apache.log4j.Logger;

                                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPartners'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingPartner
    extends KreditPrivatEntity implements de.smava.webapp.account.domain.ImageAwareEntity,MarketingPartnerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingPartner.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing partner)}

    public static final String STATE_ACTIVE = "active";
    public static final String STATE_DELETED = "deleted";



    public String getFullImagePath(String imagePath) {
        return imagePath + "/" + getId();
    }

    public MarketingContact getAgentAdminContact() {
        MarketingContact contact = null;
        if (!getContacts().isEmpty()) {
            for (MarketingContact mc : getContacts()) {
                if (mc.getIsAgentAdmin()) {
                    contact = mc;
                }
            }
        }
        return contact;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

