//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url placement)}
import de.smava.webapp.marketing.domain.history.MarketingDestinationUrlPlacementHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingDestinationUrlPlacements'.
 *
 * @author generator
 */
public class MarketingDestinationUrlPlacement extends MarketingDestinationUrlPlacementHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing destination url placement)}

    public MarketingDestinationUrlPlacement(
            MarketingPlacement marketingPlacement,
            MarketingDestinationUrl marketingDestinationUrl, Date validFrom) {
        super();
        _marketingPlacement = marketingPlacement;
        _marketingDestinationUrl = marketingDestinationUrl;
        _validFrom = validFrom;
    }

    public MarketingDestinationUrlPlacement() {
        super();
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected MarketingPlacement _marketingPlacement;
        protected MarketingDestinationUrl _marketingDestinationUrl;
        protected String _marketingTestLabel;
        protected Date _validFrom;
        protected Date _validUntil;
        
                                    
    /**
     * Setter for the property 'marketing placement'.
     */
    public void setMarketingPlacement(MarketingPlacement marketingPlacement) {
        _marketingPlacement = marketingPlacement;
    }
            
    /**
     * Returns the property 'marketing placement'.
     */
    public MarketingPlacement getMarketingPlacement() {
        return _marketingPlacement;
    }
                                            
    /**
     * Setter for the property 'marketing destination url'.
     */
    public void setMarketingDestinationUrl(MarketingDestinationUrl marketingDestinationUrl) {
        _marketingDestinationUrl = marketingDestinationUrl;
    }
            
    /**
     * Returns the property 'marketing destination url'.
     */
    public MarketingDestinationUrl getMarketingDestinationUrl() {
        return _marketingDestinationUrl;
    }
                                    /**
     * Setter for the property 'marketing test label'.
     */
    public void setMarketingTestLabel(String marketingTestLabel) {
        if (!_marketingTestLabelIsSet) {
            _marketingTestLabelIsSet = true;
            _marketingTestLabelInitVal = getMarketingTestLabel();
        }
        registerChange("marketing test label", _marketingTestLabelInitVal, marketingTestLabel);
        _marketingTestLabel = marketingTestLabel;
    }
                        
    /**
     * Returns the property 'marketing test label'.
     */
    public String getMarketingTestLabel() {
        return _marketingTestLabel;
    }
                                    /**
     * Setter for the property 'valid from'.
     */
    public void setValidFrom(Date validFrom) {
        if (!_validFromIsSet) {
            _validFromIsSet = true;
            _validFromInitVal = getValidFrom();
        }
        registerChange("valid from", _validFromInitVal, validFrom);
        _validFrom = validFrom;
    }
                        
    /**
     * Returns the property 'valid from'.
     */
    public Date getValidFrom() {
        return _validFrom;
    }
                                    /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingDestinationUrlPlacement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _marketingTestLabel=").append(_marketingTestLabel);
            builder.append("\n}");
        } else {
            builder.append(MarketingDestinationUrlPlacement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingDestinationUrlPlacement asMarketingDestinationUrlPlacement() {
        return this;
    }
}
