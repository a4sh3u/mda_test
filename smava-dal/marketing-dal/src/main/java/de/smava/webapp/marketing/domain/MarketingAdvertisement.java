//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing advertisement)}
import de.smava.webapp.marketing.domain.history.MarketingAdvertisementHistory;

import java.util.Date;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingAdvertisements'.
 *
 * @author generator
 */
public class MarketingAdvertisement extends MarketingAdvertisementHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing advertisement)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected String _name;
        protected String _comment;
        protected List<MarketingAdFormat> _formats;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'comment'.
     */
    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = getComment();
        }
        registerChange("comment", _commentInitVal, comment);
        _comment = comment;
    }
                        
    /**
     * Returns the property 'comment'.
     */
    public String getComment() {
        return _comment;
    }
                                            
    /**
     * Setter for the property 'formats'.
     */
    public void setFormats(List<MarketingAdFormat> formats) {
        _formats = formats;
    }
            
    /**
     * Returns the property 'formats'.
     */
    public List<MarketingAdFormat> getFormats() {
        return _formats;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingAdvertisement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _comment=").append(_comment);
            builder.append("\n}");
        } else {
            builder.append(MarketingAdvertisement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingAdvertisement asMarketingAdvertisement() {
        return this;
    }
}
