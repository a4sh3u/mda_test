//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head9/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head9/mda/templates/dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact)}
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.dao.MarketingContactDao;
import de.smava.webapp.marketing.domain.MarketingContact;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingContacts'.
 *
 * @author generator
 */
@Repository(value = "marketingContactDao")
public class JdoMarketingContactDao extends JdoBaseDao implements MarketingContactDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingContactDao.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing contact)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    public void setSynchronization(Synchronization synchronization) {
        super.setSynchronization(synchronization);
    }

    /**
     * Returns an attached copy of the marketing contact identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingContact load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContact - start: id=" + id);
        }
        MarketingContact result = getEntity(MarketingContact.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContact - result: " + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingContact getMarketingContact(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            MarketingContact entity = findUniqueEntity(MarketingContact.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the marketing contact.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingContact marketingContact) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingContact: " + marketingContact);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing contact)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingContact);
    }

    /**
     * @deprecated Use {@link #save(MarketingContact) instead}
     */
    public Long saveMarketingContact(MarketingContact marketingContact) {
        return save(marketingContact);
    }

    /**
     * Deletes an marketing contact, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing contact
     */
    public void deleteMarketingContact(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingContact: " + id);
        }
        deleteEntity(MarketingContact.class, id);
    }

    /**
     * Retrieves all 'MarketingContact' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingContact> getMarketingContactList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - start");
        }
        Collection<MarketingContact> result = getEntities(MarketingContact.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingContact' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingContact> getMarketingContactList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - start: pageable=" + pageable);
        }
        Collection<MarketingContact> result = getEntities(MarketingContact.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingContact' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingContact> getMarketingContactList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - start: sortable=" + sortable);
        }
        Collection<MarketingContact> result = getEntities(MarketingContact.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingContact' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingContact> getMarketingContactList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - start: pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingContact> result = getEntities(MarketingContact.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingContact' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContact> findMarketingContactList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - start: whereClause=" + whereClause);
        }
        Collection<MarketingContact> result = findEntities(MarketingContact.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingContact' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingContact> findMarketingContactList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        Collection<MarketingContact> result = findEntities(MarketingContact.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingContact' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContact> findMarketingContactList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - start: whereClause=" + whereClause + " pageable=" + pageable);
        }
        Collection<MarketingContact> result = findEntities(MarketingContact.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingContact' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContact> findMarketingContactList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - start: whereClause=" + whereClause + " sortable=" + sortable);
        }
        Collection<MarketingContact> result = findEntities(MarketingContact.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingContact' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContact> findMarketingContactList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingContact> result = findEntities(MarketingContact.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingContact' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingContact> findMarketingContactList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - start: whereClause=" + whereClause + " pageable=" + pageable + " sortable=" + sortable);
        }
        Collection<MarketingContact> result = findEntities(MarketingContact.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findMarketingContactList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingContact' instances.
     */
    public long getMarketingContactCount() {
        long result = getEntityCount(MarketingContact.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingContact' instances which match the given whereClause.
     */
    public long getMarketingContactCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactCount() - start: whereClause=" + whereClause);
        }
        long result = getEntityCount(MarketingContact.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactCount() - result: count=" + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingContact' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingContactCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactCount() - start: whereClause=" + whereClause + " params=" + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingContact.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getMarketingContactCount() - result: count=" + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing contact)}

    public long getPlacementIdForAccount(Account account) {
        Long result = null;

        StringBuilder sql = new StringBuilder("select mcp.marketing_placement_id ");
        sql.append("from marketing_contact mc ");
        sql.append("left outer join marketing_contact_placement mcp on mcp.marketing_contact_id = mc.id ");
        sql.append("left outer join marketing_placement mp on mcp.marketing_placement_id = mp.id ");
        sql.append("where	mc.account_id = ");
        sql.append(account.getId());
        sql.append(" and	mcp.\"type\" = 'MAIN'");
        Query q = getPersistenceManager().newQuery("javax.jdo.query.SQL", sql.toString());
        q.setResultClass(Long.class);

        result = (Long) ((List) q.execute()).iterator().next();

        return result;
    }

    @Override
    public Collection<MarketingContact> findAdminMarketingContactList(){
        return findMarketingContactList("_isAgentAdmin == true");
    }

    @Override
    public Collection<MarketingContact> getAllContacts(Long partnerId) {
        Collection<MarketingContact> result = new ArrayList<MarketingContact>();
        if (partnerId != null) {
            String where = "this._marketingPartner._id == " + partnerId;
            result = findMarketingContactList(where);
        }
        return result;
    }

    @Override
    public Collection<MarketingContact> findMarketingContactsForAccount(Account account) {
        return findMarketingContactList(" _account._accountId == " + account.getAccountId());
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
