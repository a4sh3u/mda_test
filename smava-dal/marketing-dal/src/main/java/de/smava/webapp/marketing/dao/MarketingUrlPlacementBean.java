/**
 * 
 */
package de.smava.webapp.marketing.dao;

import java.io.Serializable;
import java.util.Date;

/**
 * Simple data holder.
 * @author Dimitar Robev
 *
 */
public class MarketingUrlPlacementBean implements Serializable {


	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 2628705224950182974L;

	private Long _marketingDestinationUrlPlacementId;
	private String _marketingDestinationUrlLink;
	private Long _marketingDestinationUrlId;
	private Long _marketingPlacementId;
	private Date _placementValidFrom;
	private String _publicationName;
	private String _partnerName;
	private String _campaignName;

	public MarketingUrlPlacementBean() {
		
	}

	public MarketingUrlPlacementBean(String marketingDestinationUrlLink,
			Long marketingDestinationUrlId, Long marketingPlacementId,
			Date placementValidFrom, String publicationName,
			String partnerName, String campaignName) {
		super();
		_marketingDestinationUrlLink = marketingDestinationUrlLink;
		_marketingDestinationUrlId = marketingDestinationUrlId;
		_marketingPlacementId = marketingPlacementId;
		_placementValidFrom = placementValidFrom;
		_publicationName = publicationName;
		_partnerName = partnerName;
		_campaignName = campaignName;
	}

	public Long getMarketingDestinationUrlPlacementId() {
		return _marketingDestinationUrlPlacementId;
	}

	public void setMarketingDestinationUrlPlacementId(
			Long marketingDestinationUrlPlacementId) {
		_marketingDestinationUrlPlacementId = marketingDestinationUrlPlacementId;
	}

	public String getMarketingDestinationUrlLink() {
		return _marketingDestinationUrlLink;
	}

	public void setMarketingDestinationUrlLink(String marketingDestinationUrlLink) {
		_marketingDestinationUrlLink = marketingDestinationUrlLink;
	}

	public Long getMarketingDestinationUrlId() {
		return _marketingDestinationUrlId;
	}

	public void setMarketingDestinationUrlId(Long marketingDestinationUrlId) {
		_marketingDestinationUrlId = marketingDestinationUrlId;
	}

	public Long getMarketingPlacementId() {
		return _marketingPlacementId;
	}

	public void setMarketingPlacementId(Long marketingPlacementId) {
		_marketingPlacementId = marketingPlacementId;
	}

	public Date getPlacementValidFrom() {
		return _placementValidFrom;
	}

	public void setPlacementValidFrom(Date placementValidFrom) {
		_placementValidFrom = placementValidFrom;
	}

	public String getPublicationName() {
		return _publicationName;
	}

	public void setPublicationName(String publicationName) {
		_publicationName = publicationName;
	}

	public String getPartnerName() {
		return _partnerName;
	}

	public void setPartnerName(String partnerName) {
		_partnerName = partnerName;
	}

	public String getCampaignName() {
		return _campaignName;
	}

	public void setCampaignName(String campaignName) {
		_campaignName = campaignName;
	}
}
