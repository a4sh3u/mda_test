package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingDestinationUrlPlacement;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingDestinationUrlPlacements'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingDestinationUrlPlacementHistory extends AbstractMarketingDestinationUrlPlacement {

    protected transient String _marketingTestLabelInitVal;
    protected transient boolean _marketingTestLabelIsSet;
    protected transient Date _validFromInitVal;
    protected transient boolean _validFromIsSet;
    protected transient Date _validUntilInitVal;
    protected transient boolean _validUntilIsSet;


			
    /**
     * Returns the initial value of the property 'marketing test label'.
     */
    public String marketingTestLabelInitVal() {
        String result;
        if (_marketingTestLabelIsSet) {
            result = _marketingTestLabelInitVal;
        } else {
            result = getMarketingTestLabel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'marketing test label'.
     */
    public boolean marketingTestLabelIsDirty() {
        return !valuesAreEqual(marketingTestLabelInitVal(), getMarketingTestLabel());
    }

    /**
     * Returns true if the setter method was called for the property 'marketing test label'.
     */
    public boolean marketingTestLabelIsSet() {
        return _marketingTestLabelIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid from'.
     */
    public Date validFromInitVal() {
        Date result;
        if (_validFromIsSet) {
            result = _validFromInitVal;
        } else {
            result = getValidFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid from'.
     */
    public boolean validFromIsDirty() {
        return !valuesAreEqual(validFromInitVal(), getValidFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'valid from'.
     */
    public boolean validFromIsSet() {
        return _validFromIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid until'.
     */
    public Date validUntilInitVal() {
        Date result;
        if (_validUntilIsSet) {
            result = _validUntilInitVal;
        } else {
            result = getValidUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid until'.
     */
    public boolean validUntilIsDirty() {
        return !valuesAreEqual(validUntilInitVal(), getValidUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'valid until'.
     */
    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }

}
