//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\dao.tpl
//
//
//
//
package de.smava.webapp.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.marketing.domain.MarketingDestinationUrl;
import de.smava.webapp.marketing.domain.MarketingPlacement;

// !!!!!!!! End of insert code section !!!!!!!!
/**
 * DAO interface for the domain object 'MarketingDestinationUrls'.
 *
 * @author generator
 */
public interface MarketingDestinationUrlDao extends BaseDao<MarketingDestinationUrl>, Serializable {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the marketing destination url identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    MarketingDestinationUrl getMarketingDestinationUrl(Long id);

    /**
     * Saves the marketing destination url.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMarketingDestinationUrl(MarketingDestinationUrl marketingDestinationUrl);

    /**
     * Deletes an marketing destination url, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing destination url
     */
    void deleteMarketingDestinationUrl(Long id);

    /**
     * Retrieves all 'MarketingDestinationUrl' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MarketingDestinationUrl> getMarketingDestinationUrlList();

    /**
     * Retrieves a page of 'MarketingDestinationUrl' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MarketingDestinationUrl> getMarketingDestinationUrlList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MarketingDestinationUrl' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MarketingDestinationUrl> getMarketingDestinationUrlList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MarketingDestinationUrl' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MarketingDestinationUrl> getMarketingDestinationUrlList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'MarketingDestinationUrl' instances.
     */
    long getMarketingDestinationUrlCount();

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing destination url)}
    //
    Collection<MarketingDestinationUrl> findByIds(Collection<Long> ids);


    long getValidUrlsCount(MarketingPlacement placement);


    public MarketingDestinationUrl findMarketingDestinationUrlByUrl(String url);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
