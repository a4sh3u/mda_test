package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingPublication;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingPublications'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingPublicationHistory extends AbstractMarketingPublication {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _rangeInitVal;
    protected transient boolean _rangeIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'range'.
     */
    public String rangeInitVal() {
        String result;
        if (_rangeIsSet) {
            result = _rangeInitVal;
        } else {
            result = getRange();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'range'.
     */
    public boolean rangeIsDirty() {
        return !valuesAreEqual(rangeInitVal(), getRange());
    }

    /**
     * Returns true if the setter method was called for the property 'range'.
     */
    public boolean rangeIsSet() {
        return _rangeIsSet;
    }
		
}
