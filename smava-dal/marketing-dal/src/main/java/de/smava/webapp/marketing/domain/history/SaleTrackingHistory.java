package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractSaleTracking;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'SaleTrackings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SaleTrackingHistory extends AbstractSaleTracking {

    protected transient Date _trackingDateInitVal;
    protected transient boolean _trackingDateIsSet;


	
    /**
     * Returns the initial value of the property 'tracking date'.
     */
    public Date trackingDateInitVal() {
        Date result;
        if (_trackingDateIsSet) {
            result = _trackingDateInitVal;
        } else {
            result = getTrackingDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'tracking date'.
     */
    public boolean trackingDateIsDirty() {
        return !valuesAreEqual(trackingDateInitVal(), getTrackingDate());
    }

    /**
     * Returns true if the setter method was called for the property 'tracking date'.
     */
    public boolean trackingDateIsSet() {
        return _trackingDateIsSet;
    }
			
}
