package de.smava.webapp.marketing.affiliate.domain.interfaces;



import de.smava.webapp.marketing.affiliate.domain.AccountAffiliateInformation;
import de.smava.webapp.marketing.affiliate.domain.AffiliateInformation;


/**
 * The domain object that represents 'AccountAffiliateInformations'.
 *
 * @author generator
 */
public interface AccountAffiliateInformationEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'first affiliate'.
     *
     * 
     *
     */
    void setFirstAffiliate(AffiliateInformation firstAffiliate);

    /**
     * Returns the property 'first affiliate'.
     *
     * 
     *
     */
    AffiliateInformation getFirstAffiliate();
    /**
     * Setter for the property 'sale affiliate'.
     *
     * 
     *
     */
    void setSaleAffiliate(AffiliateInformation saleAffiliate);

    /**
     * Returns the property 'sale affiliate'.
     *
     * 
     *
     */
    AffiliateInformation getSaleAffiliate();
    /**
     * Helper method to get reference of this object as model type.
     */
    AccountAffiliateInformation asAccountAffiliateInformation();
}
