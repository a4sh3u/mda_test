package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingCampaign;
import de.smava.webapp.marketing.domain.MarketingPlacement;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'MarketingCampaigns'.
 *
 * @author generator
 */
public interface MarketingCampaignEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    void setComment(String comment);

    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    String getComment();
    /**
     * Setter for the property 'placements'.
     *
     * 
     *
     */
    void setPlacements(Set<MarketingPlacement> placements);

    /**
     * Returns the property 'placements'.
     *
     * 
     *
     */
    Set<MarketingPlacement> getPlacements();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingCampaign asMarketingCampaign();
}
