package de.smava.webapp.marketing.domain.history;



import de.smava.webapp.marketing.domain.abstracts.AbstractMarketingTrackingHit;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingTrackingHits'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingTrackingHitHistory extends AbstractMarketingTrackingHit {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _marketingTestInitVal;
    protected transient boolean _marketingTestIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'marketing test'.
     */
    public String marketingTestInitVal() {
        String result;
        if (_marketingTestIsSet) {
            result = _marketingTestInitVal;
        } else {
            result = getMarketingTest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'marketing test'.
     */
    public boolean marketingTestIsDirty() {
        return !valuesAreEqual(marketingTestInitVal(), getMarketingTest());
    }

    /**
     * Returns true if the setter method was called for the property 'marketing test'.
     */
    public boolean marketingTestIsSet() {
        return _marketingTestIsSet;
    }

}
