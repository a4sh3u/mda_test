//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing payment)}
import de.smava.webapp.marketing.domain.history.MarketingPaymentHistory;

import java.util.Date;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPayments'.
 *
 * @author generator
 */
public class MarketingPayment extends MarketingPaymentHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing payment)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Float _amount;
        protected Date _from;
        protected Date _until;
        protected MarketingPartner _marketingPartner;
        protected List<MarketingPlacement> _placements;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(Float amount) {
        _amount = amount;
    }
            
    /**
     * Returns the property 'amount'.
     */
    public Float getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'from'.
     */
    public void setFrom(Date from) {
        if (!_fromIsSet) {
            _fromIsSet = true;
            _fromInitVal = getFrom();
        }
        registerChange("from", _fromInitVal, from);
        _from = from;
    }
                        
    /**
     * Returns the property 'from'.
     */
    public Date getFrom() {
        return _from;
    }
                                    /**
     * Setter for the property 'until'.
     */
    public void setUntil(Date until) {
        if (!_untilIsSet) {
            _untilIsSet = true;
            _untilInitVal = getUntil();
        }
        registerChange("until", _untilInitVal, until);
        _until = until;
    }
                        
    /**
     * Returns the property 'until'.
     */
    public Date getUntil() {
        return _until;
    }
                                            
    /**
     * Setter for the property 'marketing partner'.
     */
    public void setMarketingPartner(MarketingPartner marketingPartner) {
        _marketingPartner = marketingPartner;
    }
            
    /**
     * Returns the property 'marketing partner'.
     */
    public MarketingPartner getMarketingPartner() {
        return _marketingPartner;
    }
                                            
    /**
     * Setter for the property 'placements'.
     */
    public void setPlacements(List<MarketingPlacement> placements) {
        _placements = placements;
    }
            
    /**
     * Returns the property 'placements'.
     */
    public List<MarketingPlacement> getPlacements() {
        return _placements;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingPayment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(MarketingPayment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingPayment asMarketingPayment() {
        return this;
    }
}
