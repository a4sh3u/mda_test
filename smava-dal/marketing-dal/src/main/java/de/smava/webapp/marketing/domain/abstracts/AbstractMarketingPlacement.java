//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.marketing.domain.*;
import de.smava.webapp.marketing.domain.interfaces.MarketingPlacementEntityInterface;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

                                                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPlacements'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingPlacement
    extends KreditPrivatEntity implements MarketingPlacementEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingPlacement.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing placement)}

    public String getName() {
        String name = getPosition();
        if (getPublication() != null) {
            name = getPublication().getName() + ": " + name;
        }
        return name + " (" + getId() + ")";
    }

    public MarketingDestinationUrl getLastMarketingDestinationUrl() {
        MarketingDestinationUrl url = null;

        /* TODO: muss umgebaut werden, da von 1:N zum N:M uebergegangen wurde
        if (getMarketingDestinationUrls() != null && !getMarketingDestinationUrls().isEmpty()) {
            url = getMarketingDestinationUrls().get(getMarketingDestinationUrls().size() - 1);
        }*/
        return url;
    }


    public void add(MarketingDeal entity) {
        List<MarketingDeal> entities = getDeals();
        if (entities == null) {
            entities = new ArrayList<MarketingDeal>();
        }
        entities.add(entity);
        setDeals(entities);
        entity.setPlacement(this.asMarketingPlacement());
    }

    public void add(MarketingPayment entity) {
        List<MarketingPayment> entities = getPayments();
        if (entities == null) {
            entities = new ArrayList<MarketingPayment>();
        }
        entities.add(entity);
        setPayments(entities);

        List<MarketingPlacement> placements = entity.getPlacements();
        if (placements == null) {
            placements = new ArrayList<MarketingPlacement>();
        }
        placements.add(this.asMarketingPlacement());
        entity.setPlacements(placements);
    }

    public List<MarketingDestinationUrl> getMarketingDestinationUrls() {
        ArrayList<MarketingDestinationUrl> result = new ArrayList<MarketingDestinationUrl>();
        if (getMarketingDestinationUrlPlacements() != null && !getMarketingDestinationUrlPlacements().isEmpty()) {
            for (MarketingDestinationUrlPlacement marketingDestinationUrlPlacement : getMarketingDestinationUrlPlacements()) {
                result.add(marketingDestinationUrlPlacement.getMarketingDestinationUrl());
            }
        }

        return result;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

