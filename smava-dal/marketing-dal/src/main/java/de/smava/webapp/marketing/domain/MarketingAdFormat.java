//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing ad format)}
import de.smava.webapp.marketing.domain.history.MarketingAdFormatHistory;

import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingAdFormats'.
 *
 * @author generator
 */
public class MarketingAdFormat extends MarketingAdFormatHistory  implements de.smava.webapp.account.domain.ImageAwareEntity {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing ad format)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _text;
        protected int _imageWidth;
        protected int _imageHeight;
        protected String _type;
        protected String _image;
        protected MarketingAdvertisement _marketingAdvertisement;
        protected List<MarketingPlacement> _placements;
        
                            /**
     * Setter for the property 'text'.
     */
    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = getText();
        }
        registerChange("text", _textInitVal, text);
        _text = text;
    }
                        
    /**
     * Returns the property 'text'.
     */
    public String getText() {
        return _text;
    }
                                    /**
     * Setter for the property 'image width'.
     */
    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = getImageWidth();
        }
        registerChange("image width", _imageWidthInitVal, imageWidth);
        _imageWidth = imageWidth;
    }
                        
    /**
     * Returns the property 'image width'.
     */
    public int getImageWidth() {
        return _imageWidth;
    }
                                    /**
     * Setter for the property 'image height'.
     */
    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = getImageHeight();
        }
        registerChange("image height", _imageHeightInitVal, imageHeight);
        _imageHeight = imageHeight;
    }
                        
    /**
     * Returns the property 'image height'.
     */
    public int getImageHeight() {
        return _imageHeight;
    }
                                    /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'image'.
     */
    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = getImage();
        }
        registerChange("image", _imageInitVal, image);
        _image = image;
    }
                        
    /**
     * Returns the property 'image'.
     */
    public String getImage() {
        return _image;
    }
                                            
    /**
     * Setter for the property 'marketing advertisement'.
     */
    public void setMarketingAdvertisement(MarketingAdvertisement marketingAdvertisement) {
        _marketingAdvertisement = marketingAdvertisement;
    }
            
    /**
     * Returns the property 'marketing advertisement'.
     */
    public MarketingAdvertisement getMarketingAdvertisement() {
        return _marketingAdvertisement;
    }
                                            
    /**
     * Setter for the property 'placements'.
     */
    public void setPlacements(List<MarketingPlacement> placements) {
        _placements = placements;
    }
            
    /**
     * Returns the property 'placements'.
     */
    public List<MarketingPlacement> getPlacements() {
        return _placements;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingAdFormat.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _text=").append(_text);
            builder.append("\n    _imageWidth=").append(_imageWidth);
            builder.append("\n    _imageHeight=").append(_imageHeight);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _image=").append(_image);
            builder.append("\n}");
        } else {
            builder.append(MarketingAdFormat.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingAdFormat asMarketingAdFormat() {
        return this;
    }
}
