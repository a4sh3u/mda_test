//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact)}
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.marketing.domain.history.MarketingContactHistory;

import java.util.Collection;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingContacts'.
 *
 * @author generator
 */
public class MarketingContact extends MarketingContactHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing contact)}

    public static final String TYPE_MAIN = "MAIN";

    public MarketingContact() {
        setIsAgentAdmin(false);				// constructor for setting a default value
    }

    public String getPrefixPhone1() {
        String result = "";
        if (getAccount() != null) {
            result = getAccount().getPhone();
        }
        return result;
    }
    public String getPrefixPhone2() {
        String result = "";
        if (getAccount() != null) {
            result = getAccount().getPhone2();
        }
        return result;
    }

    public void setPrefixPhone1(String phoneCode) {
        if (getAccount() != null) {
            getAccount().setPhoneCode(phoneCode);
        }
    }
    public void setPrefixPhone2(String phone2Code) {
        if (getAccount() != null) {
            getAccount().setPhone2Code(phone2Code);
        }
    }

    public String getEmail() {
        String email = "";
        if (getAccount() != null) {
            email = getAccount().getEmail();
        }
        return email;
    }

    public void setEmail(String email) {
        if (getAccount() != null) {
            getAccount().setEmail(email);
        }
    }

    public String getLastName() {
        String result = "";
        if (getAccount() != null) {
            result = getAccount().getPerson().getLastName();
        }
        return result;
    }

    public void setLastName(String lastName) {
        if (getAccount() != null) {
            getAccount().getPerson().setLastName(lastName);
        }
    }

    public String getName() {
        String result = "";
        if (getAccount() != null) {
            result = getAccount().getPerson().getFirstName();
        }
        return result;
    }

    public void setName(String name) {
        if (getAccount() != null) {
            getAccount().getPerson().setFirstName(name);
        }
    }

    public String getPhone1() {
        String result = "";
        if (getAccount() != null) {
            result = getAccount().getPhone();
        }
        return result;
    }

    public void setPhone1(String phone1) {
        if (getAccount() != null) {
            getAccount().setPhone(phone1);
        }
    }

    public String getPhone2() {
        String result = "";
        if (getAccount() != null) {
            result = getAccount().getPhone2();
        }
        return result;
    }

    public void setPhone2(String phone2) {
        if (getAccount() != null) {
            getAccount().setPhone2(phone2);
        }
    }

    public Date getCreationDate() {
        Date result = CurrentDate.getDate();
        if (getAccount() != null) {
            result = getAccount().getCreationDate();
        }
        return result;
    }

    public void setCreationDate(Date date) {
        if (getAccount() != null) {
            getAccount().setCreationDate(date);
        }
    }

    // !!!!!!!! End of insert code section !!!!!!!!


        protected MarketingPartner _marketingPartner;
        protected Collection<MarketingContactPlacement> _marketingContactPlacements;
        protected de.smava.webapp.account.domain.Account _account;
        protected Boolean _isAgentAdmin;
        
                /**
     * Setter for the property 'marketing partner'.
     */
    public void setMarketingPartner(MarketingPartner marketingPartner) {
        _marketingPartner = marketingPartner;
    }

    /**
     * Returns the property 'marketing partner'.
     */
    public MarketingPartner getMarketingPartner() {
        return _marketingPartner;
    }
                        /**
     * Setter for the property 'marketing contact placements'.
     */
    public void setMarketingContactPlacements(Collection<MarketingContactPlacement> marketingContactPlacements) {
        _marketingContactPlacements = marketingContactPlacements;
    }

    /**
     * Returns the property 'marketing contact placements'.
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacements() {
        return _marketingContactPlacements;
    }
                        /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                        /**
     * Setter for the property 'is agent admin'.
     */
    public void setIsAgentAdmin(Boolean isAgentAdmin) {
        _isAgentAdmin = isAgentAdmin;
    }

    /**
     * Returns the property 'is agent admin'.
     */
    public Boolean getIsAgentAdmin() {
        return _isAgentAdmin;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingContact.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(MarketingContact.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingContact asMarketingContact() {
        return this;
    }
}
