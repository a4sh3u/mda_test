//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing deal)}
import de.smava.webapp.marketing.domain.history.MarketingDealHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingDeals'.
 *
 * @author generator
 */
public class MarketingDeal extends MarketingDealHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing deal)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _type;
        protected Double _amount;
        protected MarketingPlacement _placement;
        
                            /**
     * Setter for the property 'type'.
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(Double amount) {
        _amount = amount;
    }
            
    /**
     * Returns the property 'amount'.
     */
    public Double getAmount() {
        return _amount;
    }
                                            
    /**
     * Setter for the property 'placement'.
     */
    public void setPlacement(MarketingPlacement placement) {
        _placement = placement;
    }
            
    /**
     * Returns the property 'placement'.
     */
    public MarketingPlacement getPlacement() {
        return _placement;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingDeal.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(MarketingDeal.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingDeal asMarketingDeal() {
        return this;
    }
}
