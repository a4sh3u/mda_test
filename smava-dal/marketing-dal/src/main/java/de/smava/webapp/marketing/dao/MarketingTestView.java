/**
 * 
 */
package de.smava.webapp.marketing.dao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

/**
 * @author bvoss
 *
 */
public class MarketingTestView implements Serializable {
	
	private static final Timestamp PSEUDO_NULL = new Timestamp(new GregorianCalendar(3000, 0, 1).getTimeInMillis());
	
	private String _testLabel;
	private long _placementCount;
	private String _destinationUrl;
	private Timestamp _validFrom;
	private Timestamp _validUntil;
	public final String getTestLabel() {
		return _testLabel;
	}
	public final void setTestLabel(String testLabel) {
		_testLabel = testLabel;
	}
	public final String getDestinationUrl() {
		return _destinationUrl;
	}
	public final void setDestinationUrl(String destinationUrl) {
		_destinationUrl = destinationUrl;
	}
	public final Timestamp getValidFrom() {
		return _validFrom;
	}
	public final void setValidFrom(Timestamp validFrom) {
		_validFrom = validFrom;
	}
	public final Timestamp getValidUntil() {
		final Timestamp result;
		if (PSEUDO_NULL.equals(_validUntil)) {
			result = null;
		} else {
			result = _validUntil;
		}
		return result;
	}
	public final void setValidUntil(Timestamp validUntil) {
		_validUntil = validUntil;
	}
	public final long getPlacementCount() {
		return _placementCount;
	}
	public final void setPlacementCount(long placementCount) {
		_placementCount = placementCount;
	}
}
