package de.smava.webapp.marketing.domain.interfaces;



import de.smava.webapp.marketing.domain.MarketingDeal;
import de.smava.webapp.marketing.domain.MarketingPlacement;


/**
 * The domain object that represents 'MarketingDeals'.
 *
 * @author generator
 */
public interface MarketingDealEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Double getAmount();
    /**
     * Setter for the property 'placement'.
     *
     * 
     *
     */
    void setPlacement(MarketingPlacement placement);

    /**
     * Returns the property 'placement'.
     *
     * 
     *
     */
    MarketingPlacement getPlacement();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingDeal asMarketingDeal();
}
