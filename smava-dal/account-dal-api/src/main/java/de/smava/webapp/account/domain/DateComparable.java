package de.smava.webapp.account.domain;

import java.util.Date;

/**
 * 
 * @author Dimitar Robev
 *
 */
public interface DateComparable {

	Date getDateToCompare();
}
