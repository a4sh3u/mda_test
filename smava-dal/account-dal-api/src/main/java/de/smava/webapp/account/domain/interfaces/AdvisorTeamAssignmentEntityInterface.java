package de.smava.webapp.account.domain.interfaces;

import java.util.Date;

/**
 * The domain object that represents 'AdvisorTeamAssignments'.
 */
public interface AdvisorTeamAssignmentEntityInterface<A extends AdvisorEntityInterface, T extends TeamEntityInterface> {

    /**
     * Setter for the property 'advisor'.
     */
    public void setAdvisor(A advisor);

    /**
     * Returns the property 'advisor'.
     */
    public A getAdvisor();

    /**
     * Setter for the property 'team'.
     */
    public void setTeam(T team);

    /**
     * Returns the property 'team'.
     */
    public T getTeam();

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate();

    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate();

}
