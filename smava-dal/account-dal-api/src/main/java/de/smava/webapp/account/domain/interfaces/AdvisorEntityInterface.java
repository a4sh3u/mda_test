package de.smava.webapp.account.domain.interfaces;

import de.smava.webapp.account.domain.AdvisorLocationType;
import de.smava.webapp.commons.domain.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * The domain object that represents 'Advisors'.
 */
public interface AdvisorEntityInterface<T extends TeamEntityInterface> extends BaseEntity {

    /**
     * Setter for the property 'blocked date'.
     */
    void setBlockedDate(Date blockedDate);

    /**
     * Returns the property 'blocked date'.
     */
    Date getBlockedDate();

    /**
     * Setter for the property 'unlimited picking date'.
     */
    void setUnlimitedPickingDate(Date unlimitedPickingDate);

    /**
     * Returns the property 'unlimited picking date'.
     */
    Date getUnlimitedPickingDate();

    /**
     * Setter for the property 'max customer per day'.
     */
    void setMaxCustomerPerDay(Integer maxCustomerPerDay);

    /**
     * Returns the property 'max customer per day'.
     */
    Integer getMaxCustomerPerDay();

    /**
     * Setter for the property 'location'.
     */
    void setLocation(AdvisorLocationType location);

    /**
     * Returns the property 'location'.
     */
    AdvisorLocationType getLocation();

    /**
     * Setter for the property 'team'.
     */
    void setTeam(T team);

    /**
     * Returns the property 'team'.
     */
    T getTeam();

    void setSip(String sip);

    String getSip();

}
