package de.smava.webapp.account.domain;

import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.BaseEntity;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The domain object that represents 'AccountRoleStates'.
 */
public class AccountRoleState implements DateComparable, BaseEntity {

    private static final Logger LOGGER = Logger.getLogger(AccountRoleState.class);
    private static final long serialVersionUID = 738781691927432906L;

    public static final String STATE_APPLIED = "APPLIED";
    public static final String STATE_ACCEPTED = "ACCEPTED";
    public static final String STATE_DENIED = "DENIED";
    public static final String STATE_VOID = "VOID";
    public static final String STATE_EXPIRED = "EXPIRED";

    public static final Set<String> FINAL_STATES = new HashSet<String>();

    static {
        FINAL_STATES.add(STATE_ACCEPTED);
        FINAL_STATES.add(STATE_DENIED);
    }

    protected Long _id;
    protected String _name;
    protected Date _validFrom;
    protected Date _validUntil;

    public AccountRoleState() {
        super();
    }

    public AccountRoleState(String _name, Date _validFrom) {
        this._name = _name;
        this._validFrom = _validFrom;
        this._validUntil = null;
    }

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }

    /**
     * Setter for the property 'valid from'.
     */
    public void setValidFrom(Date validFrom) {
        _validFrom = validFrom;
    }

    /**
     * Returns the property 'valid from'.
     */
    public Date getValidFrom() {
        return _validFrom;
    }

    /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        _validUntil = validUntil;
    }

    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }

    public Date getDateToCompare() {
        return getValidFrom();
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AccountRoleState.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(AccountRoleState.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public static AccountRoleState createAppliedAccountRoleState(Date creationDate) {
        AccountRoleState result;

        if (creationDate != null) {
            result = new AccountRoleState(STATE_APPLIED, creationDate);
        } else {
            result = new AccountRoleState(STATE_APPLIED, CurrentDate.getDate());
        }

        return result;
    }

    public static AccountRoleState createAppliedAccountRoleState() {
        return createAppliedAccountRoleState(CurrentDate.getDate());
    }

    public static AccountRoleState createAcceptedAccountRoleState() {
        return new AccountRoleState(STATE_ACCEPTED, CurrentDate.getDate());
    }

    public static AccountRoleState createVoidAccountRoleState() {
        return new AccountRoleState(STATE_VOID, CurrentDate.getDate());
    }

    public static AccountRoleState createDeniedAccountRoleState() {
        return new AccountRoleState(STATE_DENIED, CurrentDate.getDate());
    }

}
