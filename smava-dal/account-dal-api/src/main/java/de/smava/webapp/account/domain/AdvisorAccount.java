package de.smava.webapp.account.domain;

import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.BaseEntity;
import de.smava.webapp.commons.security.Role;

import java.util.*;

public class AdvisorAccount implements BaseEntity {

    private Long _id;
    private Long _smavaId;
    private Date _creationDate;
    private String _state;
    private String _username;
    private String _email;
    private String _phone;
    private String _phoneCode;
    private AdvisorPerson _person;
    private Collection<AdvisorRole> _accountRoles;

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public Long getSmavaId() {
        return _smavaId;
    }

    public void setSmavaId(Long smavaRefId) {
        this._smavaId = smavaRefId;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this._creationDate = creationDate;
    }

    public String getState() {
        return _state;
    }

    public void setState(String state) {
        this._state = state;
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        this._username = username;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        this._email = email;
    }

    public String getPhone() {
        return _phone;
    }

    public void setPhone(String phone) {
        this._phone = phone;
    }

    public String getPhoneCode() {
        return _phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this._phoneCode = phoneCode;
    }

    public AdvisorPerson getPerson() {
        return _person;
    }

    public void setPerson(AdvisorPerson person) {
        this._person = person;
    }

    public Collection<AdvisorRole> getAccountRoles() {
        return _accountRoles;
    }

    public void setAccountRoles(Collection<AdvisorRole> accountRoles) {
        this._accountRoles = accountRoles;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    public Set<Role> getActiveRoles() {
        Set<Role> result = new HashSet<Role>();

        Collection<AdvisorRole> accountRoles = getAccountRoles();
        if (accountRoles != null) {
            for (AdvisorRole accountRole : accountRoles) {
                AdvisorRoleState state = accountRole.getCurrentAccountRoleState();
                if (state != null && state.getValidUntil() == null) {
                    result.add(Enum.valueOf(Role.class, accountRole.getName()));
                }
            }
        }

        return result;
    }

    public void setRoles(Set<Role> roles) {
        if (getAccountRoles() == null) {
            setAccountRoles(new HashSet<AdvisorRole>());
        }

        Date currentDate = CurrentDate.getDate();

        for (AdvisorRole currentRole : getActiveAccountRoles()) {
            if (currentRole.getCurrentAccountRoleState() != null) {
                if (currentRole.getCurrentAccountRoleState().getValidUntil() == null) {
                    currentRole.getCurrentAccountRoleState().setValidUntil(currentDate);
                }
            }
        }

        for (Role role : roles) {
            getRoles().add(role);
            AdvisorRole newRole = new AdvisorRole(role.name(), new HashSet<AdvisorRoleState>());
            AdvisorRoleState accountRoleState = AdvisorRoleState.createAppliedAdvisorRoleState(currentDate);
            newRole.getStates().add(accountRoleState);
            _accountRoles.add(newRole);
        }
    }

    private Set<Role> getRoles() {
        Set<Role> _roles = EnumSet.noneOf(Role.class);
        Collection<AdvisorRole> accountRoles = getAccountRoles();
        if (accountRoles != null) {
            for (AdvisorRole accountRole : accountRoles) {
                _roles.add(Enum.valueOf(Role.class, accountRole.getName()));
            }
        }
        return _roles;
    }

    private Set<AdvisorRole> getActiveAccountRoles() {
        Set<AdvisorRole> result = new HashSet<AdvisorRole>();
        Collection<AdvisorRole> accountRoles = getAccountRoles();
        if (accountRoles != null) {
            for (AdvisorRole accountRole : accountRoles) {
                AdvisorRoleState state = accountRole.getCurrentAccountRoleState();
                if (state != null && state.getValidUntil() == null) {
                    result.add(accountRole);
                }
            }
        }

        return result;
    }
}
