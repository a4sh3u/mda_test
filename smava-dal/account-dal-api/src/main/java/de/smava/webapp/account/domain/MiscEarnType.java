package de.smava.webapp.account.domain;

public enum MiscEarnType {
	
	SIDELINE, SELF_EMPLOYED, CAPITAL_INCOME, CHILD_BENEFIT, UNEMPLOYMENT_BENEFIT, MISC;	
	
}
