package de.smava.webapp.account.domain;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Collection;
import java.util.HashSet;

public class AdvisorRole implements BaseEntity {

    private Long _id;
    private String _name;
    private Collection<AdvisorRoleState> _states;

    public AdvisorRole() {
        _states = new HashSet<AdvisorRoleState>();
    }

    public AdvisorRole(String name, Collection<AdvisorRoleState> states) {
        this._name = name;
        this._states = states;
    }

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public Collection<AdvisorRoleState> getStates() {
        return _states;
    }

    public void setStates(Collection<AdvisorRoleState> states) {
        this._states = states;
    }

    public AdvisorRoleState getCurrentAccountRoleState() {
        AdvisorRoleState result = null;

        if (getStates() != null) {
            for (AdvisorRoleState state : getStates()) {
                if (state.getValidUntil() == null) {
                    result = state;
                    break;
                } else {
                    if (result == null) {
                        result = state;
                    } else if (result.getValidUntil() != null && state.getValidUntil().after(result.getValidUntil())) {
                        result = state;
                    }
                }
            }

        }
        return result;
    }

}
