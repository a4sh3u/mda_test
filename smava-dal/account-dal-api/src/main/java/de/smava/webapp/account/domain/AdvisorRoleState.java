package de.smava.webapp.account.domain;

import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.BaseEntity;
import org.apache.log4j.Logger;

import java.util.Date;

import static de.smava.webapp.account.domain.AccountRoleState.*;

public class AdvisorRoleState implements DateComparable, BaseEntity {

    private static final Logger LOGGER = Logger.getLogger(AdvisorRoleState.class);
    private static final long serialVersionUID = 738781691927432912L;

    private Long _id;
    private String _name;
    private Date _validFrom;
    private Date _validUntil;

    public AdvisorRoleState() {
    }

    public AdvisorRoleState(String name, Date validFrom) {
        this._name = name;
        this._validFrom = validFrom;
        this._validUntil = null;
    }

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }

    /**
     * Setter for the property 'valid from'.
     */
    public void setValidFrom(Date validFrom) {
        _validFrom = validFrom;
    }

    /**
     * Returns the property 'valid from'.
     */
    public Date getValidFrom() {
        return _validFrom;
    }

    /**
     * Setter for the property 'valid until'.
     */
    public void setValidUntil(Date validUntil) {
        _validUntil = validUntil;
    }

    /**
     * Returns the property 'valid until'.
     */
    public Date getValidUntil() {
        return _validUntil;
    }

    public Date getDateToCompare() {
        return getValidFrom();
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    public static AdvisorRoleState createAppliedAdvisorRoleState(Date creationDate) {
        AdvisorRoleState result;

        if (creationDate != null) {
            result = new AdvisorRoleState(STATE_APPLIED, creationDate);
        } else {
            result = new AdvisorRoleState(STATE_APPLIED, CurrentDate.getDate());
        }

        return result;
    }

    public static AdvisorRoleState createAppliedAdvisorRoleState() {
        return createAppliedAdvisorRoleState(CurrentDate.getDate());
    }

    public static AdvisorRoleState createAcceptedAdvisorRoleState() {
        return new AdvisorRoleState(STATE_ACCEPTED, CurrentDate.getDate());
    }

    public static AdvisorRoleState createVoidAdvisorRoleState() {
        return new AdvisorRoleState(STATE_VOID, CurrentDate.getDate());
    }

    public static AdvisorRoleState createDeniedAdvisorRoleState() {
        return new AdvisorRoleState(STATE_DENIED, CurrentDate.getDate());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AdvisorRoleState.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(AdvisorRoleState.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }
}
