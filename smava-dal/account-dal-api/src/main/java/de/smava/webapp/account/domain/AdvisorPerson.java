package de.smava.webapp.account.domain;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public class AdvisorPerson implements BaseEntity {

    private Long _id;
    private String _firstName;
    private String _lastName;
    private Date _creationDate;
    private AdvisorAccount _account;

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        this._firstName = firstName;
    }

    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        this._lastName = lastName;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this._creationDate = creationDate;
    }

    public AdvisorAccount getAccount() {
        return _account;
    }

    public void setAccount(AdvisorAccount account) {
        this._account = account;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    public String getFullName() {
        return (this.getFirstName() + " " + this.getLastName()).trim();
    }
}
