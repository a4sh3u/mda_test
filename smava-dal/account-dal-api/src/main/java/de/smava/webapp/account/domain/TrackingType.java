package de.smava.webapp.account.domain;

/**
 * @author dkeller
 *
 */
public enum TrackingType {

	BORROWER_LEAD,
	LENDER_LEAD,
	BORROWER_LEAD_PAP,
	LENDER_LEAD_PAP;
}
