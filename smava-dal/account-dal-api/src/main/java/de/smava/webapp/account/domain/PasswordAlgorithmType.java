package de.smava.webapp.account.domain;

public enum PasswordAlgorithmType {

	MD5,
	SHA256,
	PBKDF2
}
