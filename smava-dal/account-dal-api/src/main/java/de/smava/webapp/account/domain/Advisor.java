package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.interfaces.AdvisorEntityInterface;

import java.util.Date;

public class Advisor implements AdvisorEntityInterface<Team> {

    private static final long serialVersionUID = 236145197223517199L;
    public static final Integer DEFAULT_MAX_CUSTOMERS_PER_DAY = 50;

    protected Long _id;
    protected Date _blockedDate;
    protected Date _unlimitedPickingDate;
    protected Integer _maxCustomerPerDay;
    protected AdvisorLocationType _location;
    protected Team _team;
    protected String _sip;
    protected AdvisorAccount _account;

    public Advisor() {
        setLocation(AdvisorLocationType.UNKNOWN);
        setMaxCustomerPerDay(DEFAULT_MAX_CUSTOMERS_PER_DAY);
    }

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    @Override
    public boolean isNew() {
        return this._id == null;
    }

    public void setBlockedDate(Date blockedDate) {
        _blockedDate = blockedDate;
    }

    public Date getBlockedDate() {
        return _blockedDate;
    }

    public void setUnlimitedPickingDate(Date unlimitedPickingDate) {
        _unlimitedPickingDate = unlimitedPickingDate;
    }

    public Date getUnlimitedPickingDate() {
        return _unlimitedPickingDate;
    }

    public void setMaxCustomerPerDay(Integer maxCustomerPerDay) {
        _maxCustomerPerDay = maxCustomerPerDay;
    }

    public Integer getMaxCustomerPerDay() {
        return _maxCustomerPerDay;
    }

    public void setLocation(AdvisorLocationType location) {
        _location = location;
    }

    public AdvisorLocationType getLocation() {
        return _location;
    }

    public void setTeam(Team team) {
        _team = team;
    }

    public Team getTeam() {
        return _team;
    }

    public void setSip(String sip) {
        _sip = sip;
    }

    public String getSip() {
        return _sip;
    }

    public AdvisorAccount getAccount() {
        return _account;
    }

    public void setAccount(AdvisorAccount account) {
        this._account = account;
    }
}
