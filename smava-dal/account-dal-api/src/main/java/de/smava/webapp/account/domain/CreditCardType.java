package de.smava.webapp.account.domain;

public enum CreditCardType {
	NO_DEBIT_OR_CREDIT_CARD, DEBIT_CARD_ONLY, CREDIT_CARD_ONLY, DEBIT_AND_CREDIT_CARD;
}
