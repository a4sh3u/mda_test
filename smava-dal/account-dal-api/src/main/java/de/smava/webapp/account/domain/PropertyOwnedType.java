package de.smava.webapp.account.domain;

public enum PropertyOwnedType {
	NO_PROPERTY, SELF_USED_PROPERTY,LETTED_PROPERTY, SELF_USED_AND_LETTED_PROPERTY
}
