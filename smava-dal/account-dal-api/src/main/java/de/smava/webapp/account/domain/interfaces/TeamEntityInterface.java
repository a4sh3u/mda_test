package de.smava.webapp.account.domain.interfaces;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;
import java.util.Set;

/**
 * The domain object that represents 'Teams'.
 */
public interface TeamEntityInterface<A extends AdvisorEntityInterface> extends BaseEntity {

    /**
     * Setter for the property 'name'.
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     */
    String getName();

    /**
     * Setter for the property 'expiration date'.
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     */
    Date getExpirationDate();

    /**
     * Setter for the property 'team lead'.
     */
    void setTeamLead(A teamLead);

    /**
     * Returns the property 'team lead'.
     */
    A getTeamLead();

    /**
     * Setter for the property 'advisors'.
     */
    void setAdvisors(Set<A> advisors);

    /**
     * Returns the property 'advisors'.
     */
    Set<A> getAdvisors();

}
