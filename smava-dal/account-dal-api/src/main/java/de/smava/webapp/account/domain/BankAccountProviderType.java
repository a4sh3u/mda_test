package de.smava.webapp.account.domain;

/**
 * @author cdey
 *	Defines the technical type of a credit institute
 *	E_MONEY : electronic cash in general
 *	ALLOCATION : traditional offline bank accounts
 */
public enum BankAccountProviderType {
	E_MONEY, ALLOCATION;
}
