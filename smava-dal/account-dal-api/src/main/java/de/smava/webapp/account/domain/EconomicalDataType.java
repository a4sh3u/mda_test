package de.smava.webapp.account.domain;

/**
 * @author dkeller
 *
 */
public enum EconomicalDataType {
	CUSTOMER, CREDIT_ADVISOR, CREDIT_CHECK;
}
