package de.smava.webapp.account.domain;

/**
 * The different agreements a customer might have
 * 
 * @author dkeller
 *
 */
public enum CustomerAgreementType {

	FIDOR_AGREEMENT,
	TERMS_AND_CONDITIONS,
	PRIVACY_AGREEMENT,
	PARTNER_PRIVACY_AGREEMENT,
	BROKERAGE_AGREEMENT,
	SCHUFA_AGREEMENT;
}
