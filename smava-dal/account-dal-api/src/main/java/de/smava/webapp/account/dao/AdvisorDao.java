package de.smava.webapp.account.dao;

import de.smava.webapp.account.domain.Advisor;

import java.util.Collection;

public interface AdvisorDao {

    Collection<Advisor> getAdvisorList();

    Collection<Advisor> getActiveAdvisorList();

    Advisor loadActive(Long id);

    Long save(Advisor advisor);

    Advisor load(Long id);

    Advisor findPersonalAdvisorByCustomerNumber(Long customerNumber);

    Advisor findBySmavaRefId(Long advisorSmavaRefId);

    boolean checkIfAdvisorExistsForBorrower(Long customerNumber);
}
