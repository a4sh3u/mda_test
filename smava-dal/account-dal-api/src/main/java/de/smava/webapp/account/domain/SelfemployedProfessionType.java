package de.smava.webapp.account.domain;

public enum SelfemployedProfessionType {
    LAWYER,
    PHARMACIST,
    ARCHITECT,
    MD, //House MD
    TRANSLATER,
    IT_CONSULTANT,
    PHOTOGRAPHER,
    MIDWIFE,
    HEALER,
    ENGINEER,
    JOURNALIST,
    PHYSICAL_THERAPIST,
    NOTARY,
    PSYCHIATRIST,
    EXPERT,
    STRUCTURAL_ENGINEER,
    TAX_ADVISOR,
    VETERINARY,
    AUDITOR,
    DENTIST,
    OTHER,
    GERIATRIC_NURSE,
    MOBILE_NURSE,
    NURSE,
    GRAPHIC_DESIGNER,
    SPEECH_THERAPIST,
    CLASSIC_MUSICIAN,
    PHYSIOTHERAPIST,
    DIETICIAN, //Diätassistent
    NUTRITIONIST, //Ernährungsberater
    ERGOTHERAPIST, //Ergotherapeut
    MASSAGE_THERAPIST, //Heilmasseur
    MEDICAL_TECHNICAL_ASSISTENT, //Medizinisch-technischer Assistent
    DENTAL_TECHNICAL, //Zahntechniker
    DATA_PROTECTION_OFFICER, //Datenschutzbeauftragter
    LIQUIDATOR, //Insolvenzverwalter
    RECRUITMENT_CONSULTANT, //Personalberater
    EXECUTIVE_CONSULTANT, //Unternehmensberater
    CONSTRUCTION_SUPERVISOR, //Bauleiter
    GEOGRAPHER, //Geograf
    COMPUTER_SCIENTIST, //Informatiker
    DRAFTSMAN, //Konstrukteur
    URBAN_PLANNER, //Stadtplaner
    HISTORIAN, //Historiker
    BROADCASTER, //Rundfunksprecher
    MORTICIAN, //Bestatter
    ADORNER, //Dekorateur
    OPERA_SINGER, //Opernsänger
    INTERIOR_DECORATOR, //Raumausstatter

    /**
     * Don't confuse the name with the 'Grafiker' above. Not nice naming, but to avoid conflicts with already existing
     * one.
     *
     * see: http://intranet.smava.de/jira/browse/WEBSITE-12570
     */
    GRAPHIC_DESIGNER2 //Grafikerdesigner/in
}
