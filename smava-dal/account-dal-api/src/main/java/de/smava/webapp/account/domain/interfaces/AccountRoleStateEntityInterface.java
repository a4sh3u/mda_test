package de.smava.webapp.account.domain.interfaces;

import java.util.Date;

/**
 * The domain object that represents 'AccountRoleStates'.
 */
public interface AccountRoleStateEntityInterface {

    /**
     * Setter for the property 'name'.
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     */
    String getName();

    /**
     * Setter for the property 'valid from'.
     */
    void setValidFrom(Date validFrom);

    /**
     * Returns the property 'valid from'.
     */
    Date getValidFrom();

    /**
     * Setter for the property 'valid until'.
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     */
    Date getValidUntil();

    /**
     * Setter for the property 'account role ID'.
     */
    void setAccountRoleId(Long accountRoleId);

    /**
     * Returns the property 'account role'.
     */
    Long getAccountRoleId();

}
