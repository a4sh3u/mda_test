package de.smava.webapp.account.domain;

import de.smava.webapp.account.domain.interfaces.TeamEntityInterface;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'Teams'.
 */
public class Team implements TeamEntityInterface<Advisor> {

    protected static final Logger LOGGER = Logger.getLogger(Team.class);

    protected Long _id;
    protected String _name;
    protected Date _expirationDate;
    protected Advisor _teamLead;
    protected Set<Advisor> _advisors = new HashSet<Advisor>();


    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public void addAdvisor(Advisor advisor) {
        if (advisor != null) {
            advisor.setTeam(this);
            _advisors.add(advisor);
        }
    }

    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }

    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }

    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }

    /**
     * Setter for the property 'team lead'.
     */
    public void setTeamLead(Advisor teamLead) {
        _teamLead = teamLead;
    }

    /**
     * Returns the property 'team lead'.
     */
    public Advisor getTeamLead() {
        return _teamLead;
    }

    /**
     * Setter for the property 'advisors'.
     */
    public void setAdvisors(Set<Advisor> advisors) {
        _advisors = advisors;
    }

    /**
     * Returns the property 'advisors'.
     */
    public Set<Advisor> getAdvisors() {
        return _advisors;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Team.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(Team.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
