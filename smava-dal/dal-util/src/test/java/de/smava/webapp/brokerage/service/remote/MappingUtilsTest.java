package de.smava.webapp.brokerage.service.remote;

import de.smava.webapp.brokerage.util.MappingUtils;
import de.smava.webapp.commons.currentdate.SetterForCurrentDate;
import de.smava.webapp.commons.util.FormatUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: aakhmerov
 * Date: 11/25/13
 * Time: 10:56 AM
 * Fetched from @link {de.smava.webapp.brokerage.service.remote.BrokerageMappingUtilsTest} as part of DAL module refactorings
 */
public class MappingUtilsTest {

    @Test
    public void testAddMonth12() throws Exception {
        final Date pseudoNow = new GregorianCalendar(2011, 5, 17, 11, 48).getTime();
        final Date expected = new GregorianCalendar(2012, 5, 30, 0, 0).getTime();

        final Date result = MappingUtils.addMonthAndClean(pseudoNow, 12);
        assertEquals(expected, result);
    }

    @Test
    public void testAddMonth1() throws Exception {
        final Date pseudoNow = new GregorianCalendar(2011, 5, 17, 11, 48).getTime();
        final Date expected = new GregorianCalendar(2011, 6, 31, 0, 0).getTime();

        final Date result = MappingUtils.addMonthAndClean(pseudoNow, 1);
        assertEquals(expected, result);
    }

    @Test
    public void testAddMonthNull() throws Exception {
        final Date pseudoNow = new GregorianCalendar(2011, 5, 17, 11, 48).getTime();

        final Date result = MappingUtils.addMonthAndClean(pseudoNow, null);
        assertEquals(null, result);
    }

    @Test
    public void testAddMonthDateNull() throws Exception {
        final Date result = MappingUtils.addMonthAndClean(null, 3);
        assertEquals(null, result);
    }

    @Test
    public void testGetNumberOfMonthsOfPeriod() throws Exception {
        DateTime now = new DateTime(2011, 05, 01, 12, 0, 0, 0);
        DateTime date = new DateTime(2010, 01, 01, 12, 0, 0, 0);

        int result = MappingUtils.getNumberOfMonthsOfPeriod(date.toDate(), now.toDate());

        Assert.assertEquals(16, result);

    }

    @Test
    public void testReplaceTargoIllegalChars() throws Exception {
        String containsIllegal = "A bcDEf342xxxX(bb$dfdf\\";
        containsIllegal = containsIllegal.replaceAll("[^a-zA-Z\\-.üÜäÄöÖß\\s]+", " ");
        System.out.println(containsIllegal);
    }

    @Test
    public void testReplaceTargoIllegalChars2() throws Exception {
        String houseNo = "12 A";
        houseNo = houseNo.replace(" ", "");
        System.out.println(houseNo);

    }

    @Test
    public void testAddDoublePrimitive() throws Exception {
        String test = "40,55";
        String add = "1,99";

        double result = FormatUtils.asAmount(test) + FormatUtils.asAmount(add);

        Assert.assertEquals(42.54, result, 0.01);
    }

    @Test
    public void testRoundUpToStep() throws Exception {
        double result = MappingUtils.roundUpAmountToStep(2942.1545d, 500d);

        Assert.assertEquals(3000,result,0.005);

        double result2 = MappingUtils.roundUpAmountToStep(3001.1545d, 500d);
        Assert.assertEquals(3500,result2,0.005);

    }

    @Test
    public void testGetAge() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        Date creation = sdf.parse("31.01.2012");
        SetterForCurrentDate.setFakeDate(creation);
        SetterForCurrentDate.setUseFakeDate(true);
        SetterForCurrentDate.setAllowFakeDate(true);

        Date birthday = sdf.parse("01.01.1992");

        Assert.assertEquals(20, MappingUtils.getAge(birthday));
    }

    @Test
    public void testGetAgeByEndOfDuration() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

        Date creation = sdf.parse("31.01.2012");
        SetterForCurrentDate.setFakeDate(creation);
        SetterForCurrentDate.setUseFakeDate(true);
        SetterForCurrentDate.setAllowFakeDate(true);

        Date birthday = sdf.parse("01.01.1992");

        Assert.assertEquals(25, MappingUtils.getAgeByEndOfDuration(birthday, 60));
    }

    @Test
    public void testNearlySameAmount() throws Exception {
        double val1 = 0.12345;
        Double val2 = 0.12345;

        Assert.assertTrue(MappingUtils.nearlySameAmount(val1, val2));
    }

    @Test
    public void testCalculateChildBenefit() throws Exception {
        Assert.assertEquals(0d, MappingUtils.calculateChildBenefit(0), 0.0);
        Assert.assertEquals(194d, MappingUtils.calculateChildBenefit(1), 0.0);
        Assert.assertEquals(388.0, MappingUtils.calculateChildBenefit(2), 0.0);
        Assert.assertEquals(588.0, MappingUtils.calculateChildBenefit(3), 0.0);
        Assert.assertEquals(813d, MappingUtils.calculateChildBenefit(4), 0.0);
        Assert.assertEquals(1038d, MappingUtils.calculateChildBenefit(5), 0.0);
        Assert.assertEquals(1263d, MappingUtils.calculateChildBenefit(6), 0.0);
        Assert.assertEquals(1488d, MappingUtils.calculateChildBenefit(7), 0.0);
        Assert.assertEquals(1713d, MappingUtils.calculateChildBenefit(8), 0.0);
        Assert.assertEquals(1938d, MappingUtils.calculateChildBenefit(9), 0.0);
    }
}
