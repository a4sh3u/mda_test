/**
 * 
 */
package com.aperto.smava.util;

import java.util.Date;

/**
 * @author bvoss
 *
 */
public class IsNullDate extends Date {
	
	public static final IsNullDate INSTANCE = new IsNullDate();
	
	public IsNullDate() {
		super(0);
	}

}
