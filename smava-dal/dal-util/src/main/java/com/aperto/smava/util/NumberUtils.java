package com.aperto.smava.util;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * NumberUtils.
 * 
 * @author Ferry Trunschke
 */
public final class NumberUtils {
	
	private static final Logger LOGGER = Logger.getLogger(NumberUtils.class);

    private NumberUtils() {
    }

    public static double valueOf(Double nullableValue) {
    	return valueOf(nullableValue, 0);
    }
    
    public static double valueOf(Double nullableValue, double defaultValue) {
    	if (nullableValue == null) {
    		nullableValue = defaultValue;
    	}
    	return nullableValue;
    }
    
    public static double valueOf(final BigDecimal nullableValue) {
    	return valueOf(nullableValue, 0);
    }
    
    public static double valueOf(final BigDecimal nullableValue, final double defaultValue) {
    	return nullableValue == null ? defaultValue : nullableValue.doubleValue();
    }
    
    public static long valueOf(Long nullableValue, long defaultValue) {
    	if (nullableValue == null) {
    		nullableValue = defaultValue;
    	}
    	return nullableValue;
    }
    

    public static int valueOf(Integer nullableValue) {
    	return valueOf(nullableValue, 0);
    }

    public static int valueOf(Integer nullableValue, int defaultValue) {
    	if (nullableValue == null) {
    		nullableValue = defaultValue;
    	}
    	return nullableValue;
    }
    
    /**
     * Patiently parses the given {@link String} into a {@link Long}
     * <b>without</b> throwing a {@link NumberFormatException}. In this case <code>null</code> 
     * is returned.
     * 
     * @param str - string to parse
     * @return parsed long value or <code>null</code> if the string can't be parsed. 
     */
    public static Long patientToLong(final String str) {
    	Long result = null;
		if (StringUtils.isNotEmpty(str)) {
			try {
				result = Long.parseLong(str);
			} catch (NumberFormatException e) {
				//in this case just return null
				if (LOGGER.isTraceEnabled()) {
					LOGGER.trace("can't parse '" + str + "' to Long. returning null!", e);
				}
			}
		}
		return result;
    }

}