package de.smava.webapp.brokerage.util;

import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.joda.time.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

/**
 * @author bvoss
 *
 * This class in fact belongs to Brokerage module or even utils module free of any other dependencies that
 * can be fetched into commons.
 *
 */
public class MappingUtils {
	private static final DatatypeFactory DATATYPE_FACTORY;
	
	static {
		try {
			DATATYPE_FACTORY = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}
	}


    /**
     * List of EU countries
     */
    public static final Collection<String> EU_COUNTIRES;

    static {
        final Set<String> euCountries = new HashSet<String>();
        // Germany
        euCountries.add("DE");
        // Austria
        euCountries.add("AT");
        // Netherlands
        euCountries.add("NL");
        // Belgium
        euCountries.add("BE");
        // Luxembourg
        euCountries.add("LU");
        // France
        euCountries.add("FR");
        // Spain
        euCountries.add("ES");
        // Portugal
        euCountries.add("PT");
        // Italy
        euCountries.add("IT");
        // Greece
        euCountries.add("GR");
        // United Kingdom
        euCountries.add("GB");
        // Ireland
        euCountries.add("IE");
        // Danmark
        euCountries.add("DK");
        // Sweden
        euCountries.add("SE");
        // Finland
        euCountries.add("FI");
        // Estonia
        euCountries.add("EE");
        // Lithuania
        euCountries.add("LT");
        // Latvia
        euCountries.add("LV");
        // Poland
        euCountries.add("PL");
        // Czech Republic
        euCountries.add("CZ");
        // Slowakia
        euCountries.add("SK");
        // Slowenia
        euCountries.add("SI");
        // Romania
        euCountries.add("RO");
        // Bulgaria
        euCountries.add("BG");
        // Hungary
        euCountries.add("HU");
        // Cyprus
        euCountries.add("CY");
        // Malta
        euCountries.add("MT");
        // Croatia
        euCountries.add("HR");
        EU_COUNTIRES = Collections.unmodifiableSet(euCountries);
    }

	/**
	 * List of European Economic Areas whose citizens do not need a working/living permit
	 */
	public static final Collection<String> NON_EU_EEA_COUNTIRES;

	static {
		final Set<String> eeaCountries = new HashSet<String>();
		// Norway
		eeaCountries.add("NO");
		// Iceland
		eeaCountries.add("IS");
		// Liechtenstein
		eeaCountries.add("LI");

		NON_EU_EEA_COUNTIRES = Collections.unmodifiableSet(eeaCountries);
	}

	public static XMLGregorianCalendar dateToXmlGregorianCalendar(final Date source) {
		final GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(source);
		return DATATYPE_FACTORY.newXMLGregorianCalendar(gregorianCalendar);
	}

	public static int calcNumberOfCars(String numberOfCarsString) {
		int calcNumOfCars = 0;
		if (numberOfCarsString.equalsIgnoreCase("0_CARS")) {
			calcNumOfCars = 0;
		} else if (numberOfCarsString.equalsIgnoreCase("1_CAR")) {
			calcNumOfCars = 1;
		} else if (numberOfCarsString.equalsIgnoreCase("2_CARS")) {
			calcNumOfCars = 2;
		} else if (numberOfCarsString.equalsIgnoreCase("3_CARS")) {
			calcNumOfCars = 3;
		} else {
			calcNumOfCars = 4;
		}
		return calcNumOfCars;
	}

	public static Date addMonthAndClean(final Date date, final Integer months) {
		Date result = null;
		if (date != null && months != null) {
			result = DateUtils.clean(date);
			result = DateUtils.addMonth(result, months);
			result = DateUtils.getDateEndOfMonth(result);			
		}
		return result;		
	}
	
	public static int getNumberOfDaysOfPeriod(Date begin, Date end){
		
		DateTime beginDT = new DateTime(begin);
		DateTime endDT = new DateTime(end);
		
		Days d = Days.daysBetween(beginDT, endDT);
		return d.getDays();

	}

	public static int getNumberOfMonthsOfPeriod(Date begin, Date end){
		
		DateTime beginDT = new DateTime(begin);
		DateTime endDT = new DateTime(end);
		
		Months months = Months.monthsBetween(beginDT, endDT);
		return months.getMonths();

	}
	
	public static int getNumberOfYearsOfPeriod(Date begin, Date end){
		
		DateTime beginDT = new DateTime(begin);
		DateTime endDT = new DateTime(end);
		
		Years ys = Years.yearsBetween(beginDT, endDT);
		return ys.getYears();

	}
	
	public static double roundUpAmountToStep(double requestedCreditAmount, double step){
		if (requestedCreditAmount % step == 0){
			return requestedCreditAmount;
		}

		return (((int) (requestedCreditAmount / step)) + 1) * step;
	}
	
	public static int adjustDuration(List<Integer> durations, int duration, Integer passedMaxDuration) {
		int result = duration;

		if (durations.contains(duration)) {
			result = duration;
		} else {
			Collections.sort(durations);
			int chosenDuration = duration;
			int maxDuration;

			if (passedMaxDuration != null) {
				maxDuration = passedMaxDuration.intValue();
			} else {
				maxDuration = durations.get(durations.size() - 1);
			}

			if (chosenDuration > maxDuration) {
				result = maxDuration;
			} else {
				for (int term : durations) {
					if (chosenDuration < term) {
						result = term;
						break;
					}
				}
			}
		}

		return result;
	}

	/**
	 * class that map smava-only country codes to standardized country code
	 * (which is at the moment only default_DE)
	 * @param countryCode the country code to adjust
	 * @return the adjusted conutry code
	 */
	public static String adjustCountryCode(String countryCode) {
		return "default_DE".equals(countryCode) ? "DE" : countryCode;
	}


	/**
	 * calculate the age on a person for the given date of birth
	 * @param dateOfBirth the date of birth
	 * @return the calculated age
	 */
	public static int getAge(Date dateOfBirth) {
		LocalDate now = new LocalDate(CurrentDate.getTime());
		LocalDate birthday = new LocalDate(dateOfBirth);

		Years ys = Years.yearsBetween(birthday, now);
		return ys.getYears(); 
	}

	/**
	 * calculate the age by the end of an duration on a person for the given date of birth
	 * @param dateOfBirth the date of birth
	 * @param duration the duration
	 * @return the calculated age at the end of the duration
	 */
	public static int getAgeByEndOfDuration(Date dateOfBirth, int duration) {
		Calendar birthday = Calendar.getInstance();
		birthday.setTime(dateOfBirth);

		birthday.add(Calendar.MONTH, (-1) * duration);

		return getAge(birthday.getTime());
	}

	/**
	 * tests if two values have nearly the same amount
	 * @param d1 the first value
	 * @param d2 the second value
	 * @return true if the amounts are nearly the same
	 */
	public static boolean nearlySameAmount(final double d1, final Double d2) {
		if (d2 == null) {
			return false;
		}
		return Math.abs(d1 - d2) < 1.0;
	}

	/**
	 * Calculates the child benefit:
	 * 1st and 2nd 194 euro,
	 * 3rd child 200 euro,
	 * 4th and each other child 225 euro
	 * @param numberOfChildren the number of children
	 * @return the child benefit
	 */
	public static double calculateChildBenefit(int numberOfChildren) {
		if (numberOfChildren == 0) {
			return 0d;
		} else if (numberOfChildren == 1 || numberOfChildren == 2) {
			return 194.0d * numberOfChildren;
		} else if (numberOfChildren == 3) {
			return 194.0d + 194.0d + 200.0d;
		} else {
			return 194.0d + 194.0d + 200.0d + 225.0d + ((numberOfChildren - 4) * 225.0d);
		}
	}
}

