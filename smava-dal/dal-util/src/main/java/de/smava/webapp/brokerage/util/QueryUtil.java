package de.smava.webapp.brokerage.util;

import java.util.Collection;

/**
 * Helpers for preparing and reading queries.
 * Created by aherr on 26.03.15.
 */
public final class QueryUtil {

    /**
     * Helper for queries used for average calculation. The given result set
     * is a query result, where the single rows contains a number (as double).
     * The method will sum up those numbers and divide by total number of lines.
     *
     * @param queryResult as retrieved by datanucleus query
     * @param idx row index of the number column
     * @return average
     */
    public static double average(Collection<Object[]> queryResult, int idx) {
        if (queryResult == null || queryResult.size() == 0) {
            return 0d;
        }
        double totalSum = 0;
        for (Object[] row : queryResult) {
            totalSum += ((Number)row[idx]).doubleValue();
        }
        return totalSum / Double.valueOf(queryResult.size());
    }

}
