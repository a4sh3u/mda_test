package de.smava.webapp.arvato.domain;

public enum ArvatoRequestType {
	
	/**
	 * Address Verification (AV)
	 */
	@Deprecated
	ES0013,
	/**
	 * “behaviour” of the account (RPP-Check)
	 */
	@Deprecated
	ES0024,
	/**
	 * Scoring and “Bonität”
	 */
	@Deprecated
	ES0036,

	/**
	 * new BIG request type for SOAP webservice
	 */
	BIG_SCORE,


	BIG_ADDRESS
	
}
