package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoRppInfoToken;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoRppInfoTokens'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoRppInfoTokenHistory extends AbstractArvatoRppInfoToken {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _rppContentDescriptionInitVal;
    protected transient boolean _rppContentDescriptionIsSet;
    protected transient Date _firstNoticeDateInitVal;
    protected transient boolean _firstNoticeDateIsSet;
    protected transient Date _lastNoticeDateInitVal;
    protected transient boolean _lastNoticeDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'rpp content description'.
     */
    public String rppContentDescriptionInitVal() {
        String result;
        if (_rppContentDescriptionIsSet) {
            result = _rppContentDescriptionInitVal;
        } else {
            result = getRppContentDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rpp content description'.
     */
    public boolean rppContentDescriptionIsDirty() {
        return !valuesAreEqual(rppContentDescriptionInitVal(), getRppContentDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'rpp content description'.
     */
    public boolean rppContentDescriptionIsSet() {
        return _rppContentDescriptionIsSet;
    }
		
    /**
     * Returns the initial value of the property 'first notice date'.
     */
    public Date firstNoticeDateInitVal() {
        Date result;
        if (_firstNoticeDateIsSet) {
            result = _firstNoticeDateInitVal;
        } else {
            result = getFirstNoticeDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first notice date'.
     */
    public boolean firstNoticeDateIsDirty() {
        return !valuesAreEqual(firstNoticeDateInitVal(), getFirstNoticeDate());
    }

    /**
     * Returns true if the setter method was called for the property 'first notice date'.
     */
    public boolean firstNoticeDateIsSet() {
        return _firstNoticeDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last notice date'.
     */
    public Date lastNoticeDateInitVal() {
        Date result;
        if (_lastNoticeDateIsSet) {
            result = _lastNoticeDateInitVal;
        } else {
            result = getLastNoticeDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last notice date'.
     */
    public boolean lastNoticeDateIsDirty() {
        return !valuesAreEqual(lastNoticeDateInitVal(), getLastNoticeDate());
    }

    /**
     * Returns true if the setter method was called for the property 'last notice date'.
     */
    public boolean lastNoticeDateIsSet() {
        return _lastNoticeDateIsSet;
    }

}
