package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoAddressCheck;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoAddressChecks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoAddressCheckHistory extends AbstractArvatoAddressCheck {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _featureInitVal;
    protected transient boolean _featureIsSet;
    protected transient String _lastNameInitVal;
    protected transient boolean _lastNameIsSet;
    protected transient String _firstNameInitVal;
    protected transient boolean _firstNameIsSet;
    protected transient String _streetInitVal;
    protected transient boolean _streetIsSet;
    protected transient String _houseInitVal;
    protected transient boolean _houseIsSet;
    protected transient String _cityInitVal;
    protected transient boolean _cityIsSet;
    protected transient String _zipInitVal;
    protected transient boolean _zipIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
					
    /**
     * Returns the initial value of the property 'feature'.
     */
    public String featureInitVal() {
        String result;
        if (_featureIsSet) {
            result = _featureInitVal;
        } else {
            result = getFeature();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'feature'.
     */
    public boolean featureIsDirty() {
        return !valuesAreEqual(featureInitVal(), getFeature());
    }

    /**
     * Returns true if the setter method was called for the property 'feature'.
     */
    public boolean featureIsSet() {
        return _featureIsSet;
    }
	
    /**
     * Returns the initial value of the property 'lastName'.
     */
    public String lastNameInitVal() {
        String result;
        if (_lastNameIsSet) {
            result = _lastNameInitVal;
        } else {
            result = getLastName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'lastName'.
     */
    public boolean lastNameIsDirty() {
        return !valuesAreEqual(lastNameInitVal(), getLastName());
    }

    /**
     * Returns true if the setter method was called for the property 'lastName'.
     */
    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'firstName'.
     */
    public String firstNameInitVal() {
        String result;
        if (_firstNameIsSet) {
            result = _firstNameInitVal;
        } else {
            result = getFirstName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'firstName'.
     */
    public boolean firstNameIsDirty() {
        return !valuesAreEqual(firstNameInitVal(), getFirstName());
    }

    /**
     * Returns true if the setter method was called for the property 'firstName'.
     */
    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'street'.
     */
    public String streetInitVal() {
        String result;
        if (_streetIsSet) {
            result = _streetInitVal;
        } else {
            result = getStreet();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'street'.
     */
    public boolean streetIsDirty() {
        return !valuesAreEqual(streetInitVal(), getStreet());
    }

    /**
     * Returns true if the setter method was called for the property 'street'.
     */
    public boolean streetIsSet() {
        return _streetIsSet;
    }
	
    /**
     * Returns the initial value of the property 'house'.
     */
    public String houseInitVal() {
        String result;
        if (_houseIsSet) {
            result = _houseInitVal;
        } else {
            result = getHouse();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'house'.
     */
    public boolean houseIsDirty() {
        return !valuesAreEqual(houseInitVal(), getHouse());
    }

    /**
     * Returns true if the setter method was called for the property 'house'.
     */
    public boolean houseIsSet() {
        return _houseIsSet;
    }
	
    /**
     * Returns the initial value of the property 'city'.
     */
    public String cityInitVal() {
        String result;
        if (_cityIsSet) {
            result = _cityInitVal;
        } else {
            result = getCity();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'city'.
     */
    public boolean cityIsDirty() {
        return !valuesAreEqual(cityInitVal(), getCity());
    }

    /**
     * Returns true if the setter method was called for the property 'city'.
     */
    public boolean cityIsSet() {
        return _cityIsSet;
    }
	
    /**
     * Returns the initial value of the property 'zip'.
     */
    public String zipInitVal() {
        String result;
        if (_zipIsSet) {
            result = _zipInitVal;
        } else {
            result = getZip();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'zip'.
     */
    public boolean zipIsDirty() {
        return !valuesAreEqual(zipInitVal(), getZip());
    }

    /**
     * Returns true if the setter method was called for the property 'zip'.
     */
    public boolean zipIsSet() {
        return _zipIsSet;
    }

}
