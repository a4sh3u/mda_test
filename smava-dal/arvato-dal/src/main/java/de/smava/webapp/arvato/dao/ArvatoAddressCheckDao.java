//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.arvato.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvatoAddressCheck)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.arvato.domain.ArvatoAddressCheck;
import de.smava.webapp.arvato.domain.ArvatoRequest;
import de.smava.webapp.arvato.domain.ArvatoScore;
import de.smava.webapp.arvato.exception.ArvatoDataException;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'ArvatoAddressChecks'.
 *
 * @author generator
 */
public interface ArvatoAddressCheckDao extends BaseDao<ArvatoAddressCheck> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the arvatoAddressCheck identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    ArvatoAddressCheck getArvatoAddressCheck(Long id);

    /**
     * Saves the arvatoAddressCheck.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveArvatoAddressCheck(ArvatoAddressCheck arvatoAddressCheck);

    /**
     * Deletes an arvatoAddressCheck, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the arvatoAddressCheck
     */
    void deleteArvatoAddressCheck(Long id);

    /**
     * Retrieves all 'ArvatoAddressCheck' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<ArvatoAddressCheck> getArvatoAddressCheckList();

    /**
     * Retrieves a page of 'ArvatoAddressCheck' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<ArvatoAddressCheck> getArvatoAddressCheckList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'ArvatoAddressCheck' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<ArvatoAddressCheck> getArvatoAddressCheckList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'ArvatoAddressCheck' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<ArvatoAddressCheck> getArvatoAddressCheckList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'ArvatoAddressCheck' instances.
     */
    long getArvatoAddressCheckCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(arvatoAddressCheck)}
    /**
     * Finds all entities for this dao's managed type.
     * DO NOT use that method for production code. It is used for testing only! 
     * @return all entities
     */
    Collection<ArvatoAddressCheck> findAll();
    
    ArvatoAddressCheck findArvatoAddressCheck(final ArvatoRequest arvatoRequest) throws ArvatoDataException;
    // !!!!!!!! End of insert code section !!!!!!!!
}
