package de.smava.webapp.arvato.domain;

import de.smava.webapp.commons.domain.MappableEnum;
import de.smava.webapp.commons.domainutil.MappableEnumHelper;

public enum ArvatoRppContentType implements MappableEnum<ArvatoRppContentType> {

	/**
	 * offene RLS.
	 */
	OPEN_RLS(0), 
	
	/**
	 * erledigte RLS
	 */
	COMPLETED_RLS(1),
	
	/**
	 * historische RLS
	 */
	HISTORICAL_RLS(2),
	
	/**
	 * Institution/öffentliche Bankverbindung (NCA)
	 */
	NCA(3), 
	
	/**
	 * Mandantenbezogene Negativliste
	 */
	CLIENT_RELATED_NEGATIVE_LIST(5),
	
	/**
	 * Mandantenbezogene Positivliste
	 */
	CLIENT_RELATED_POSITIVE_LIST(6),
	
	/**
	 * Kartensperren
	 */
	CARD_BLOCKING(7), 
	
	/**
	 * Personal Account Protection (PAP)
	 */
	PAP(14); 
	
	ArvatoRppContentType(int dbValue) {
		this.dbValue = dbValue;
	}
	
	private int dbValue;

	/*public static ArvatoRppContentType fromValue(int value) {
		for(ArvatoRppContentType e: ArvatoRppContentType.values()) {
			if(e.value == value) {
				return e;
			}
		}
        throw new IllegalArgumentException(String.valueOf(value));
	}

    public static ArvatoRppContentType fromValue(String value) {
        int asInt = Integer.parseInt(value);
        return fromValue(asInt);
    }*/

	@Override
	public Number getDbValue() {
		return dbValue;
	}

	@Override
	public ArvatoRppContentType getEnumFromDbValue(Number dbValue) {
		return enumFromDbValue(dbValue);
	}
	
	public static ArvatoRppContentType enumFromDbValue(final String value) {
		int dbValue = Integer.parseInt(value);
		return enumFromDbValue(dbValue);
	}
	
	public static ArvatoRppContentType enumFromDbValue(final Number dbValue) {
		return MappableEnumHelper.getEnumFromDbValue(ArvatoRppContentType.class, dbValue);
	}

	@Override
	public String getName() {
		return name();
	}
}
