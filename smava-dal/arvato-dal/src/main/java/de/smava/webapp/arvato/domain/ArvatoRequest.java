//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato request)}
import de.smava.webapp.arvato.domain.history.ArvatoRequestHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoRequests'.
 *
 * 
 *
 * @author generator
 */
public class ArvatoRequest extends ArvatoRequestHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvato request)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected de.smava.webapp.arvato.domain.ArvatoRequestType _type;
        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.Person _person;
        protected de.smava.webapp.account.domain.BankAccount _bankAccount;
        protected de.smava.webapp.account.domain.Address _address;
        protected de.smava.webapp.account.domain.Address _previousAddress;
        protected Integer _returnCode;
        protected String _request;
        protected String _response;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.arvato.domain.ArvatoRequestType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.arvato.domain.ArvatoRequestType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'person'.
     *
     * 
     *
     */
    public void setPerson(de.smava.webapp.account.domain.Person person) {
        _person = person;
    }
            
    /**
     * Returns the property 'person'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Person getPerson() {
        return _person;
    }
                                            
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    public void setBankAccount(de.smava.webapp.account.domain.BankAccount bankAccount) {
        _bankAccount = bankAccount;
    }
            
    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.BankAccount getBankAccount() {
        return _bankAccount;
    }
                                            
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    public void setAddress(de.smava.webapp.account.domain.Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Address getAddress() {
        return _address;
    }
                                            
    /**
     * Setter for the property 'previous address'.
     *
     * 
     *
     */
    public void setPreviousAddress(de.smava.webapp.account.domain.Address previousAddress) {
        _previousAddress = previousAddress;
    }
            
    /**
     * Returns the property 'previous address'.
     *
     * 
     *
     */
    public de.smava.webapp.account.domain.Address getPreviousAddress() {
        return _previousAddress;
    }
                                            
    /**
     * Setter for the property 'return code'.
     *
     * 
     *
     */
    public void setReturnCode(Integer returnCode) {
        _returnCode = returnCode;
    }
            
    /**
     * Returns the property 'return code'.
     *
     * 
     *
     */
    public Integer getReturnCode() {
        return _returnCode;
    }
                                    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    public void setRequest(String request) {
        if (!_requestIsSet) {
            _requestIsSet = true;
            _requestInitVal = getRequest();
        }
        registerChange("request", _requestInitVal, request);
        _request = request;
    }
                        
    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    public String getRequest() {
        return _request;
    }
                                    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    public void setResponse(String response) {
        if (!_responseIsSet) {
            _responseIsSet = true;
            _responseInitVal = getResponse();
        }
        registerChange("response", _responseInitVal, response);
        _response = response;
    }
                        
    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    public String getResponse() {
        return _response;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoRequest.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _request=").append(_request);
            builder.append("\n    _response=").append(_response);
            builder.append("\n}");
        } else {
            builder.append(ArvatoRequest.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoRequest asArvatoRequest() {
        return this;
    }
}
