//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato rpp info token)}
import de.smava.webapp.arvato.domain.history.ArvatoRppInfoTokenHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoRppInfoTokens'.
 *
 * @author generator
 */
public class ArvatoRppInfoToken extends ArvatoRppInfoTokenHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvato rpp info token)}
    private Integer _rppContentType;
    private Integer _rppContentCode;
    /**
     * Setter for the property 'rpp content type'.
     */
    public void setRppContentType(ArvatoRppContentType rppContentType) {
        _rppContentType = (Integer) rppContentType.getDbValue();
    }
    public void setRppContentType(Integer rppContentType) {
        _rppContentType = rppContentType;
    }
    /**
     * Returns the property 'rpp content type'.
     */
    public ArvatoRppContentType getRppContentType() {
        return ArvatoRppContentType.enumFromDbValue(_rppContentType);
    }

    /**
     * Setter for the property 'rpp content code'.
     */
    public void setRppContentCode(ArvatoRppContentCode rppContentCode) {
        _rppContentCode = (Integer) rppContentCode.getDbValue();
    }
    public void setRppContentCode(Integer rppContentCode) {
        _rppContentCode = rppContentCode;
    }
    /**
     * Returns the property 'rpp content code'.
     */
    public ArvatoRppContentCode getRppContentCode() {
        return ArvatoRppContentCode.enumFromDbValue(_rppContentCode, getRppContentType());
    }


    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected ArvatoRppInfo _arvatoRppInfo;
        protected String _rppContentDescription;
        protected Integer _noOfMatches;
        protected Date _firstNoticeDate;
        protected Date _lastNoticeDate;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'arvato rpp info'.
     */
    public void setArvatoRppInfo(ArvatoRppInfo arvatoRppInfo) {
        _arvatoRppInfo = arvatoRppInfo;
    }
            
    /**
     * Returns the property 'arvato rpp info'.
     */
    public ArvatoRppInfo getArvatoRppInfo() {
        return _arvatoRppInfo;
    }
                                    /**
     * Setter for the property 'rpp content description'.
     */
    public void setRppContentDescription(String rppContentDescription) {
        if (!_rppContentDescriptionIsSet) {
            _rppContentDescriptionIsSet = true;
            _rppContentDescriptionInitVal = getRppContentDescription();
        }
        registerChange("rpp content description", _rppContentDescriptionInitVal, rppContentDescription);
        _rppContentDescription = rppContentDescription;
    }
                        
    /**
     * Returns the property 'rpp content description'.
     */
    public String getRppContentDescription() {
        return _rppContentDescription;
    }
                                            
    /**
     * Setter for the property 'no of matches'.
     */
    public void setNoOfMatches(Integer noOfMatches) {
        _noOfMatches = noOfMatches;
    }
            
    /**
     * Returns the property 'no of matches'.
     */
    public Integer getNoOfMatches() {
        return _noOfMatches;
    }
                                    /**
     * Setter for the property 'first notice date'.
     */
    public void setFirstNoticeDate(Date firstNoticeDate) {
        if (!_firstNoticeDateIsSet) {
            _firstNoticeDateIsSet = true;
            _firstNoticeDateInitVal = getFirstNoticeDate();
        }
        registerChange("first notice date", _firstNoticeDateInitVal, firstNoticeDate);
        _firstNoticeDate = firstNoticeDate;
    }
                        
    /**
     * Returns the property 'first notice date'.
     */
    public Date getFirstNoticeDate() {
        return _firstNoticeDate;
    }
                                    /**
     * Setter for the property 'last notice date'.
     */
    public void setLastNoticeDate(Date lastNoticeDate) {
        if (!_lastNoticeDateIsSet) {
            _lastNoticeDateIsSet = true;
            _lastNoticeDateInitVal = getLastNoticeDate();
        }
        registerChange("last notice date", _lastNoticeDateInitVal, lastNoticeDate);
        _lastNoticeDate = lastNoticeDate;
    }
                        
    /**
     * Returns the property 'last notice date'.
     */
    public Date getLastNoticeDate() {
        return _lastNoticeDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoRppInfoToken.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rppContentDescription=").append(_rppContentDescription);
            builder.append("\n}");
        } else {
            builder.append(ArvatoRppInfoToken.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoRppInfoToken asArvatoRppInfoToken() {
        return this;
    }
}
