package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoAddressCheck;
import de.smava.webapp.arvato.domain.ArvatoRequest;

import java.util.Date;


/**
 * The domain object that represents 'ArvatoAddressChecks'.
 *
 * @author generator
 */
public interface ArvatoAddressCheckEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'arvato request'.
     *
     * 
     *
     */
    void setArvatoRequest(ArvatoRequest arvatoRequest);

    /**
     * Returns the property 'arvato request'.
     *
     * 
     *
     */
    ArvatoRequest getArvatoRequest();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(de.smava.webapp.account.domain.Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Address getAddress();
    /**
     * Setter for the property 'previous address'.
     *
     * 
     *
     */
    void setPreviousAddress(de.smava.webapp.account.domain.Address previousAddress);

    /**
     * Returns the property 'previous address'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Address getPreviousAddress();
    /**
     * Setter for the property 'feature'.
     *
     * 
     *
     */
    void setFeature(String feature);

    /**
     * Returns the property 'feature'.
     *
     * 
     *
     */
    String getFeature();
    /**
     * Setter for the property 'lastName'.
     *
     * 
     *
     */
    void setLastName(String lastName);

    /**
     * Returns the property 'lastName'.
     *
     * 
     *
     */
    String getLastName();
    /**
     * Setter for the property 'firstName'.
     *
     * 
     *
     */
    void setFirstName(String firstName);

    /**
     * Returns the property 'firstName'.
     *
     * 
     *
     */
    String getFirstName();
    /**
     * Setter for the property 'street'.
     *
     * 
     *
     */
    void setStreet(String street);

    /**
     * Returns the property 'street'.
     *
     * 
     *
     */
    String getStreet();
    /**
     * Setter for the property 'house'.
     *
     * 
     *
     */
    void setHouse(String house);

    /**
     * Returns the property 'house'.
     *
     * 
     *
     */
    String getHouse();
    /**
     * Setter for the property 'city'.
     *
     * 
     *
     */
    void setCity(String city);

    /**
     * Returns the property 'city'.
     *
     * 
     *
     */
    String getCity();
    /**
     * Setter for the property 'zip'.
     *
     * 
     *
     */
    void setZip(String zip);

    /**
     * Returns the property 'zip'.
     *
     * 
     *
     */
    String getZip();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoAddressCheck asArvatoAddressCheck();
}
