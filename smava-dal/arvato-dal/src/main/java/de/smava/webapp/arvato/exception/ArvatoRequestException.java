package de.smava.webapp.arvato.exception;

/**
 * 
 * @author Daniel Suszczynski
 *
 */

public class ArvatoRequestException extends ArvatoException {

	public ArvatoRequestException(String message) {
		super(message);
	}

	public ArvatoRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
