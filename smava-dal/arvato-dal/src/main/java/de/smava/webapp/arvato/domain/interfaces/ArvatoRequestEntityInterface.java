package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoRequest;

import java.util.Date;


/**
 * The domain object that represents 'ArvatoRequests'.
 *
 * @author generator
 */
public interface ArvatoRequestEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.arvato.domain.ArvatoRequestType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.arvato.domain.ArvatoRequestType getType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'person'.
     *
     * 
     *
     */
    void setPerson(de.smava.webapp.account.domain.Person person);

    /**
     * Returns the property 'person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getPerson();
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    void setBankAccount(de.smava.webapp.account.domain.BankAccount bankAccount);

    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccount getBankAccount();
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(de.smava.webapp.account.domain.Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Address getAddress();
    /**
     * Setter for the property 'previous address'.
     *
     * 
     *
     */
    void setPreviousAddress(de.smava.webapp.account.domain.Address previousAddress);

    /**
     * Returns the property 'previous address'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Address getPreviousAddress();
    /**
     * Setter for the property 'return code'.
     *
     * 
     *
     */
    void setReturnCode(Integer returnCode);

    /**
     * Returns the property 'return code'.
     *
     * 
     *
     */
    Integer getReturnCode();
    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    void setRequest(String request);

    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    String getRequest();
    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    void setResponse(String response);

    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    String getResponse();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoRequest asArvatoRequest();
}
