package de.smava.webapp.arvato.domain;

import de.smava.webapp.commons.domain.MappableEnumString;
import de.smava.webapp.commons.domainutil.MappableEnumStringHelper;

public enum ArvatoEScoreValueType implements MappableEnumString<ArvatoEScoreValueType> {
	
	RED("R"),
	YELLOW("Y"),
	GREEN("G");
	
	private String dbValue;

	ArvatoEScoreValueType(String dbValue) {
		this.dbValue = dbValue;
	}

	/*public static ArvatoEScoreValueType fromValue(String value) {
		for(ArvatoEScoreValueType e: ArvatoEScoreValueType.values()) {
			if(e.value.equals(value)) {
				return e;
			}
		}
        throw new IllegalArgumentException(value);
	}*/

	@Override
	public String getDbValue() {
		return dbValue;
	}

	@Override
	public ArvatoEScoreValueType getEnumFromDbValue(String dbValue) {
		return enumFromDbValue(dbValue);
	}
	
	public static ArvatoEScoreValueType enumFromDbValue(final String dbValue) {
		return MappableEnumStringHelper.getEnumFromDbValue(ArvatoEScoreValueType.class, dbValue);
	}

	@Override
	public String getName() {
		return name();
	}
	
}
