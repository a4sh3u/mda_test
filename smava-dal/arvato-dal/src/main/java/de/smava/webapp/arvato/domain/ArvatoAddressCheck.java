//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvatoAddressCheck)}
import de.smava.webapp.arvato.domain.history.ArvatoAddressCheckHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoAddressChecks'.
 *
 * @author generator
 */
public class ArvatoAddressCheck extends ArvatoAddressCheckHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvatoAddressCheck)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected ArvatoRequest _arvatoRequest;
        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.Address _address;
        protected de.smava.webapp.account.domain.Address _previousAddress;
        protected String _feature;
        protected String _lastName;
        protected String _firstName;
        protected String _street;
        protected String _house;
        protected String _city;
        protected String _zip;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'arvato request'.
     */
    public void setArvatoRequest(ArvatoRequest arvatoRequest) {
        _arvatoRequest = arvatoRequest;
    }
            
    /**
     * Returns the property 'arvato request'.
     */
    public ArvatoRequest getArvatoRequest() {
        return _arvatoRequest;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'address'.
     */
    public void setAddress(de.smava.webapp.account.domain.Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     */
    public de.smava.webapp.account.domain.Address getAddress() {
        return _address;
    }
                                            
    /**
     * Setter for the property 'previous address'.
     */
    public void setPreviousAddress(de.smava.webapp.account.domain.Address previousAddress) {
        _previousAddress = previousAddress;
    }
            
    /**
     * Returns the property 'previous address'.
     */
    public de.smava.webapp.account.domain.Address getPreviousAddress() {
        return _previousAddress;
    }
                                    /**
     * Setter for the property 'feature'.
     */
    public void setFeature(String feature) {
        if (!_featureIsSet) {
            _featureIsSet = true;
            _featureInitVal = getFeature();
        }
        registerChange("feature", _featureInitVal, feature);
        _feature = feature;
    }
                        
    /**
     * Returns the property 'feature'.
     */
    public String getFeature() {
        return _feature;
    }
                                    /**
     * Setter for the property 'lastName'.
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("lastName", _lastNameInitVal, lastName);
        _lastName = lastName;
    }
                        
    /**
     * Returns the property 'lastName'.
     */
    public String getLastName() {
        return _lastName;
    }
                                    /**
     * Setter for the property 'firstName'.
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("firstName", _firstNameInitVal, firstName);
        _firstName = firstName;
    }
                        
    /**
     * Returns the property 'firstName'.
     */
    public String getFirstName() {
        return _firstName;
    }
                                    /**
     * Setter for the property 'street'.
     */
    public void setStreet(String street) {
        if (!_streetIsSet) {
            _streetIsSet = true;
            _streetInitVal = getStreet();
        }
        registerChange("street", _streetInitVal, street);
        _street = street;
    }
                        
    /**
     * Returns the property 'street'.
     */
    public String getStreet() {
        return _street;
    }
                                    /**
     * Setter for the property 'house'.
     */
    public void setHouse(String house) {
        if (!_houseIsSet) {
            _houseIsSet = true;
            _houseInitVal = getHouse();
        }
        registerChange("house", _houseInitVal, house);
        _house = house;
    }
                        
    /**
     * Returns the property 'house'.
     */
    public String getHouse() {
        return _house;
    }
                                    /**
     * Setter for the property 'city'.
     */
    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = getCity();
        }
        registerChange("city", _cityInitVal, city);
        _city = city;
    }
                        
    /**
     * Returns the property 'city'.
     */
    public String getCity() {
        return _city;
    }
                                    /**
     * Setter for the property 'zip'.
     */
    public void setZip(String zip) {
        if (!_zipIsSet) {
            _zipIsSet = true;
            _zipInitVal = getZip();
        }
        registerChange("zip", _zipInitVal, zip);
        _zip = zip;
    }
                        
    /**
     * Returns the property 'zip'.
     */
    public String getZip() {
        return _zip;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoAddressCheck.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _feature=").append(_feature);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n    _street=").append(_street);
            builder.append("\n    _house=").append(_house);
            builder.append("\n    _city=").append(_city);
            builder.append("\n    _zip=").append(_zip);
            builder.append("\n}");
        } else {
            builder.append(ArvatoAddressCheck.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoAddressCheck asArvatoAddressCheck() {
        return this;
    }
}
