package de.smava.webapp.arvato.exception;

import de.smava.webapp.commons.exception.RemoteServiceException;

/**
 * Base class for all errors related to arvato integration.
 * @author aherr
 *
 */
public abstract class ArvatoException extends RemoteServiceException {

	public ArvatoException(String message) {
		super(message);
	}

	public ArvatoException(String message, Throwable cause) {
		super(message, cause);
	}
}
