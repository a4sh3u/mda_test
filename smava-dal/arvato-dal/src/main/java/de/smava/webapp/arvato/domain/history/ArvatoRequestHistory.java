package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoRequest;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoRequests'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoRequestHistory extends AbstractArvatoRequest {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _requestInitVal;
    protected transient boolean _requestIsSet;
    protected transient String _responseInitVal;
    protected transient boolean _responseIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
								
    /**
     * Returns the initial value of the property 'request'.
     */
    public String requestInitVal() {
        String result;
        if (_requestIsSet) {
            result = _requestInitVal;
        } else {
            result = getRequest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'request'.
     */
    public boolean requestIsDirty() {
        return !valuesAreEqual(requestInitVal(), getRequest());
    }

    /**
     * Returns true if the setter method was called for the property 'request'.
     */
    public boolean requestIsSet() {
        return _requestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'response'.
     */
    public String responseInitVal() {
        String result;
        if (_responseIsSet) {
            result = _responseInitVal;
        } else {
            result = getResponse();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response'.
     */
    public boolean responseIsDirty() {
        return !valuesAreEqual(responseInitVal(), getResponse());
    }

    /**
     * Returns true if the setter method was called for the property 'response'.
     */
    public boolean responseIsSet() {
        return _responseIsSet;
    }

}
