package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoScoreToken;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoScoreTokens'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoScoreTokenHistory extends AbstractArvatoScoreToken {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _featureInitVal;
    protected transient boolean _featureIsSet;
    protected transient Date _featureDateInitVal;
    protected transient boolean _featureDateIsSet;
    protected transient String _docReferenceOfFeatureInitVal;
    protected transient boolean _docReferenceOfFeatureIsSet;
    protected transient Date _completionDateOfFeatureInitVal;
    protected transient boolean _completionDateOfFeatureIsSet;
    protected transient String _completionFlagInitVal;
    protected transient boolean _completionFlagIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'feature'.
     */
    public String featureInitVal() {
        String result;
        if (_featureIsSet) {
            result = _featureInitVal;
        } else {
            result = getFeature();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'feature'.
     */
    public boolean featureIsDirty() {
        return !valuesAreEqual(featureInitVal(), getFeature());
    }

    /**
     * Returns true if the setter method was called for the property 'feature'.
     */
    public boolean featureIsSet() {
        return _featureIsSet;
    }
	
    /**
     * Returns the initial value of the property 'feature date'.
     */
    public Date featureDateInitVal() {
        Date result;
        if (_featureDateIsSet) {
            result = _featureDateInitVal;
        } else {
            result = getFeatureDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'feature date'.
     */
    public boolean featureDateIsDirty() {
        return !valuesAreEqual(featureDateInitVal(), getFeatureDate());
    }

    /**
     * Returns true if the setter method was called for the property 'feature date'.
     */
    public boolean featureDateIsSet() {
        return _featureDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'doc reference of feature'.
     */
    public String docReferenceOfFeatureInitVal() {
        String result;
        if (_docReferenceOfFeatureIsSet) {
            result = _docReferenceOfFeatureInitVal;
        } else {
            result = getDocReferenceOfFeature();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'doc reference of feature'.
     */
    public boolean docReferenceOfFeatureIsDirty() {
        return !valuesAreEqual(docReferenceOfFeatureInitVal(), getDocReferenceOfFeature());
    }

    /**
     * Returns true if the setter method was called for the property 'doc reference of feature'.
     */
    public boolean docReferenceOfFeatureIsSet() {
        return _docReferenceOfFeatureIsSet;
    }
	
    /**
     * Returns the initial value of the property 'completion date of feature'.
     */
    public Date completionDateOfFeatureInitVal() {
        Date result;
        if (_completionDateOfFeatureIsSet) {
            result = _completionDateOfFeatureInitVal;
        } else {
            result = getCompletionDateOfFeature();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'completion date of feature'.
     */
    public boolean completionDateOfFeatureIsDirty() {
        return !valuesAreEqual(completionDateOfFeatureInitVal(), getCompletionDateOfFeature());
    }

    /**
     * Returns true if the setter method was called for the property 'completion date of feature'.
     */
    public boolean completionDateOfFeatureIsSet() {
        return _completionDateOfFeatureIsSet;
    }
	
    /**
     * Returns the initial value of the property 'completion flag'.
     */
    public String completionFlagInitVal() {
        String result;
        if (_completionFlagIsSet) {
            result = _completionFlagInitVal;
        } else {
            result = getCompletionFlag();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'completion flag'.
     */
    public boolean completionFlagIsDirty() {
        return !valuesAreEqual(completionFlagInitVal(), getCompletionFlag());
    }

    /**
     * Returns true if the setter method was called for the property 'completion flag'.
     */
    public boolean completionFlagIsSet() {
        return _completionFlagIsSet;
    }
	
}
