package de.smava.webapp.arvato.exception;

/**
 * 
 * @author Daniel Suszczynski
 *
 */

public class ArvatoDataException extends ArvatoException {

	public ArvatoDataException(String message) {
		super(message);
	}

}
