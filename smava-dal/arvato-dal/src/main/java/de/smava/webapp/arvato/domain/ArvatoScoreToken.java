//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato score token)}
import de.smava.webapp.arvato.domain.history.ArvatoScoreTokenHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoScoreTokens'.
 *
 * @author generator
 */
public class ArvatoScoreToken extends ArvatoScoreTokenHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvato score token)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected ArvatoScore _arvatoScore;
        protected String _feature;
        protected Date _featureDate;
        protected String _docReferenceOfFeature;
        protected Date _completionDateOfFeature;
        protected String _completionFlag;
        protected Double _principalClaim;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'arvato score'.
     */
    public void setArvatoScore(ArvatoScore arvatoScore) {
        _arvatoScore = arvatoScore;
    }
            
    /**
     * Returns the property 'arvato score'.
     */
    public ArvatoScore getArvatoScore() {
        return _arvatoScore;
    }
                                    /**
     * Setter for the property 'feature'.
     */
    public void setFeature(String feature) {
        if (!_featureIsSet) {
            _featureIsSet = true;
            _featureInitVal = getFeature();
        }
        registerChange("feature", _featureInitVal, feature);
        _feature = feature;
    }
                        
    /**
     * Returns the property 'feature'.
     */
    public String getFeature() {
        return _feature;
    }
                                    /**
     * Setter for the property 'feature date'.
     */
    public void setFeatureDate(Date featureDate) {
        if (!_featureDateIsSet) {
            _featureDateIsSet = true;
            _featureDateInitVal = getFeatureDate();
        }
        registerChange("feature date", _featureDateInitVal, featureDate);
        _featureDate = featureDate;
    }
                        
    /**
     * Returns the property 'feature date'.
     */
    public Date getFeatureDate() {
        return _featureDate;
    }
                                    /**
     * Setter for the property 'doc reference of feature'.
     */
    public void setDocReferenceOfFeature(String docReferenceOfFeature) {
        if (!_docReferenceOfFeatureIsSet) {
            _docReferenceOfFeatureIsSet = true;
            _docReferenceOfFeatureInitVal = getDocReferenceOfFeature();
        }
        registerChange("doc reference of feature", _docReferenceOfFeatureInitVal, docReferenceOfFeature);
        _docReferenceOfFeature = docReferenceOfFeature;
    }
                        
    /**
     * Returns the property 'doc reference of feature'.
     */
    public String getDocReferenceOfFeature() {
        return _docReferenceOfFeature;
    }
                                    /**
     * Setter for the property 'completion date of feature'.
     */
    public void setCompletionDateOfFeature(Date completionDateOfFeature) {
        if (!_completionDateOfFeatureIsSet) {
            _completionDateOfFeatureIsSet = true;
            _completionDateOfFeatureInitVal = getCompletionDateOfFeature();
        }
        registerChange("completion date of feature", _completionDateOfFeatureInitVal, completionDateOfFeature);
        _completionDateOfFeature = completionDateOfFeature;
    }
                        
    /**
     * Returns the property 'completion date of feature'.
     */
    public Date getCompletionDateOfFeature() {
        return _completionDateOfFeature;
    }
                                    /**
     * Setter for the property 'completion flag'.
     */
    public void setCompletionFlag(String completionFlag) {
        if (!_completionFlagIsSet) {
            _completionFlagIsSet = true;
            _completionFlagInitVal = getCompletionFlag();
        }
        registerChange("completion flag", _completionFlagInitVal, completionFlag);
        _completionFlag = completionFlag;
    }
                        
    /**
     * Returns the property 'completion flag'.
     */
    public String getCompletionFlag() {
        return _completionFlag;
    }
                                            
    /**
     * Setter for the property 'principal claim'.
     */
    public void setPrincipalClaim(Double principalClaim) {
        _principalClaim = principalClaim;
    }
            
    /**
     * Returns the property 'principal claim'.
     */
    public Double getPrincipalClaim() {
        return _principalClaim;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoScoreToken.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _feature=").append(_feature);
            builder.append("\n    _docReferenceOfFeature=").append(_docReferenceOfFeature);
            builder.append("\n    _completionFlag=").append(_completionFlag);
            builder.append("\n}");
        } else {
            builder.append(ArvatoScoreToken.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoScoreToken asArvatoScoreToken() {
        return this;
    }
}
