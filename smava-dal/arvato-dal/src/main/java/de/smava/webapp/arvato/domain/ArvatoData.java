//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato data)}
import de.smava.webapp.arvato.domain.history.ArvatoDataHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoDatas'.
 *
 * @author generator
 */
public class ArvatoData extends ArvatoDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvato data)}
    // !!!!!!!! End of insert code section !!!!!!!!


        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.Person _person;
        protected de.smava.webapp.arvato.domain.ArvatoRequest _latestArvatoScore;
        protected de.smava.webapp.arvato.domain.ArvatoRequest _latestArvatoRppInfo;
        protected de.smava.webapp.arvato.domain.ArvatoRequest _latestArvatoAddressCheck;
        
                /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                        /**
     * Setter for the property 'person'.
     */
    public void setPerson(de.smava.webapp.account.domain.Person person) {
        _person = person;
    }

    /**
     * Returns the property 'person'.
     */
    public de.smava.webapp.account.domain.Person getPerson() {
        return _person;
    }
                        /**
     * Setter for the property 'latest arvato score'.
     */
    public void setLatestArvatoScore(de.smava.webapp.arvato.domain.ArvatoRequest latestArvatoScore) {
        _latestArvatoScore = latestArvatoScore;
    }

    /**
     * Returns the property 'latest arvato score'.
     */
    public de.smava.webapp.arvato.domain.ArvatoRequest getLatestArvatoScore() {
        return _latestArvatoScore;
    }
                        /**
     * Setter for the property 'latest arvato rpp info'.
     */
    public void setLatestArvatoRppInfo(de.smava.webapp.arvato.domain.ArvatoRequest latestArvatoRppInfo) {
        _latestArvatoRppInfo = latestArvatoRppInfo;
    }

    /**
     * Returns the property 'latest arvato rpp info'.
     */
    public de.smava.webapp.arvato.domain.ArvatoRequest getLatestArvatoRppInfo() {
        return _latestArvatoRppInfo;
    }
                        /**
     * Setter for the property 'latest arvato address check'.
     */
    public void setLatestArvatoAddressCheck(de.smava.webapp.arvato.domain.ArvatoRequest latestArvatoAddressCheck) {
        _latestArvatoAddressCheck = latestArvatoAddressCheck;
    }

    /**
     * Returns the property 'latest arvato address check'.
     */
    public de.smava.webapp.arvato.domain.ArvatoRequest getLatestArvatoAddressCheck() {
        return _latestArvatoAddressCheck;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(ArvatoData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoData asArvatoData() {
        return this;
    }
}
