//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.arvato.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato data)}
import de.smava.webapp.account.domain.Address;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.arvato.domain.ArvatoData;
import de.smava.webapp.arvato.exception.ArvatoDataException;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'ArvatoDatas'.
 *
 * @author generator
 */
public interface ArvatoDataDao extends BaseDao<ArvatoData> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the arvato data identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    ArvatoData getArvatoData(Long id);

    /**
     * Saves the arvato data.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveArvatoData(ArvatoData arvatoData);

    /**
     * Deletes an arvato data, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the arvato data
     */
    void deleteArvatoData(Long id);

    /**
     * Retrieves all 'ArvatoData' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<ArvatoData> getArvatoDataList();

    /**
     * Retrieves a page of 'ArvatoData' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<ArvatoData> getArvatoDataList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'ArvatoData' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<ArvatoData> getArvatoDataList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'ArvatoData' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<ArvatoData> getArvatoDataList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'ArvatoData' instances.
     */
    long getArvatoDataCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(arvato data)}
    /**
     * Returns 'ArvatoData' with equal person will be found.
     * Otherwise return null.
     * Throws ArvatoDataException when found more than one ArvatoData for Person.
     * @throws ArvatoDataException
     */
    ArvatoData findArvatoData(final Person person) throws ArvatoDataException;

    ArvatoData findArvatoDataForScoreNotExceedingThreshold(Person person, Address address, int threshold);

    ArvatoData findArvatoDataForAddressVerificationNotExceedingThreshold(Person person, Address address, int threshold);


    // !!!!!!!! End of insert code section !!!!!!!!
}
