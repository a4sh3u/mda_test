package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoScore;
import de.smava.webapp.arvato.domain.ArvatoScoreToken;

import java.util.Date;


/**
 * The domain object that represents 'ArvatoScoreTokens'.
 *
 * @author generator
 */
public interface ArvatoScoreTokenEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'arvato score'.
     *
     * 
     *
     */
    void setArvatoScore(ArvatoScore arvatoScore);

    /**
     * Returns the property 'arvato score'.
     *
     * 
     *
     */
    ArvatoScore getArvatoScore();
    /**
     * Setter for the property 'feature'.
     *
     * 
     *
     */
    void setFeature(String feature);

    /**
     * Returns the property 'feature'.
     *
     * 
     *
     */
    String getFeature();
    /**
     * Setter for the property 'feature date'.
     *
     * 
     *
     */
    void setFeatureDate(Date featureDate);

    /**
     * Returns the property 'feature date'.
     *
     * 
     *
     */
    Date getFeatureDate();
    /**
     * Setter for the property 'doc reference of feature'.
     *
     * 
     *
     */
    void setDocReferenceOfFeature(String docReferenceOfFeature);

    /**
     * Returns the property 'doc reference of feature'.
     *
     * 
     *
     */
    String getDocReferenceOfFeature();
    /**
     * Setter for the property 'completion date of feature'.
     *
     * 
     *
     */
    void setCompletionDateOfFeature(Date completionDateOfFeature);

    /**
     * Returns the property 'completion date of feature'.
     *
     * 
     *
     */
    Date getCompletionDateOfFeature();
    /**
     * Setter for the property 'completion flag'.
     *
     * 
     *
     */
    void setCompletionFlag(String completionFlag);

    /**
     * Returns the property 'completion flag'.
     *
     * 
     *
     */
    String getCompletionFlag();
    /**
     * Setter for the property 'principal claim'.
     *
     * 
     *
     */
    void setPrincipalClaim(Double principalClaim);

    /**
     * Returns the property 'principal claim'.
     *
     * 
     *
     */
    Double getPrincipalClaim();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoScoreToken asArvatoScoreToken();
}
