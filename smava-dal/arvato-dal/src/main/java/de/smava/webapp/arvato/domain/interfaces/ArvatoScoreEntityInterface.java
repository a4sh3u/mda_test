package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoRequest;
import de.smava.webapp.arvato.domain.ArvatoScore;
import de.smava.webapp.arvato.domain.ArvatoScoreToken;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'ArvatoScores'.
 *
 * @author generator
 */
public interface ArvatoScoreEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'arvato request'.
     *
     * 
     *
     */
    void setArvatoRequest(ArvatoRequest arvatoRequest);

    /**
     * Returns the property 'arvato request'.
     *
     * 
     *
     */
    ArvatoRequest getArvatoRequest();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'person'.
     *
     * 
     *
     */
    void setPerson(de.smava.webapp.account.domain.Person person);

    /**
     * Returns the property 'person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getPerson();
    /**
     * Setter for the property 'score type'.
     *
     * 
     *
     */
    void setScoreType(String scoreType);

    /**
     * Returns the property 'score type'.
     *
     * 
     *
     */
    String getScoreType();
    /**
     * Setter for the property 'score value'.
     *
     * 
     *
     */
    void setScoreValue(Integer scoreValue);

    /**
     * Returns the property 'score value'.
     *
     * 
     *
     */
    Integer getScoreValue();
    /**
     * Setter for the property 'arvato score tokens'.
     *
     * 
     *
     */
    void setArvatoScoreTokens(Set<ArvatoScoreToken> arvatoScoreTokens);

    /**
     * Returns the property 'arvato score tokens'.
     *
     * 
     *
     */
    Set<ArvatoScoreToken> getArvatoScoreTokens();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoScore asArvatoScore();
}
