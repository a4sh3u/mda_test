package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoRppInfo;
import de.smava.webapp.arvato.domain.ArvatoRppInfoToken;

import java.util.Date;


/**
 * The domain object that represents 'ArvatoRppInfoTokens'.
 *
 * @author generator
 */
public interface ArvatoRppInfoTokenEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'arvato rpp info'.
     *
     * 
     *
     */
    void setArvatoRppInfo(ArvatoRppInfo arvatoRppInfo);

    /**
     * Returns the property 'arvato rpp info'.
     *
     * 
     *
     */
    ArvatoRppInfo getArvatoRppInfo();
    /**
     * Setter for the property 'rpp content description'.
     *
     * 
     *
     */
    void setRppContentDescription(String rppContentDescription);

    /**
     * Returns the property 'rpp content description'.
     *
     * 
     *
     */
    String getRppContentDescription();
    /**
     * Setter for the property 'no of matches'.
     *
     * 
     *
     */
    void setNoOfMatches(Integer noOfMatches);

    /**
     * Returns the property 'no of matches'.
     *
     * 
     *
     */
    Integer getNoOfMatches();
    /**
     * Setter for the property 'first notice date'.
     *
     * 
     *
     */
    void setFirstNoticeDate(Date firstNoticeDate);

    /**
     * Returns the property 'first notice date'.
     *
     * 
     *
     */
    Date getFirstNoticeDate();
    /**
     * Setter for the property 'last notice date'.
     *
     * 
     *
     */
    void setLastNoticeDate(Date lastNoticeDate);

    /**
     * Returns the property 'last notice date'.
     *
     * 
     *
     */
    Date getLastNoticeDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoRppInfoToken asArvatoRppInfoToken();
}
