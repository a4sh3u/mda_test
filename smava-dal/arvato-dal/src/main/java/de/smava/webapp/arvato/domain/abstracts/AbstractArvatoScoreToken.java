//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.arvato.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato score token)}

import de.smava.webapp.arvato.domain.interfaces.ArvatoScoreTokenEntityInterface;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;

                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoScoreTokens'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractArvatoScoreToken
    extends KreditPrivatEntity implements ArvatoScoreTokenEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractArvatoScoreToken.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(arvato score token)}

    public static final List<String> ARVATO_SOFT_FEATURES = Arrays.asList(
            "GM",
            "IA",
            "AM",
            "IE");

    public static final List<String> ARVATO_MEDIUM_FEATURES = Arrays.asList(
            "MB",
            "VB",
            "TR",
            "ZWA",
            "ZWI",
            "FRP",
            "LP",
            "UF",
            "UBV");

    public static final List<String> ARVATO_HARD_FEATURES = Arrays.asList(
            "HB",
            "HV",
            "SVV",
            "EV",
            "SAV",
            "SNZ",
            "EEV",
            "WEV",
            "IVE",
            "ISP",
            "IVS",
            "IVA",
            "IBE",
            "IBA",
            "IWP",
            "IRB",
            "IRV");

    // !!!!!!!! End of insert code section !!!!!!!!
}

