//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.arvato.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato score)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.arvato.dao.ArvatoScoreDao;
import de.smava.webapp.arvato.domain.ArvatoData;
import de.smava.webapp.arvato.domain.ArvatoRequest;
import de.smava.webapp.arvato.domain.ArvatoRppInfo;
import de.smava.webapp.arvato.domain.ArvatoScore;
import de.smava.webapp.arvato.domain.ArvatoScoreToken;
import de.smava.webapp.arvato.exception.ArvatoDataException;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'ArvatoScores'.
 *
 * @author generator
 */
@Repository(value = "arvatoScoreDao")
public class JdoArvatoScoreDao extends JdoBaseDao implements ArvatoScoreDao {

    private static final Logger LOGGER = Logger.getLogger(JdoArvatoScoreDao.class);

    private static final String CLASS_NAME = "ArvatoScore";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(arvato score)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the arvato score identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public ArvatoScore load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        ArvatoScore result = getEntity(ArvatoScore.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public ArvatoScore getArvatoScore(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	ArvatoScore entity = findUniqueEntity(ArvatoScore.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the arvato score.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(ArvatoScore arvatoScore) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveArvatoScore: " + arvatoScore);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(arvato score)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(arvatoScore);
    }

    /**
     * @deprecated Use {@link #save(ArvatoScore) instead}
     */
    public Long saveArvatoScore(ArvatoScore arvatoScore) {
        return save(arvatoScore);
    }

    /**
     * Deletes an arvato score, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the arvato score
     */
    public void deleteArvatoScore(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteArvatoScore: " + id);
        }
        deleteEntity(ArvatoScore.class, id);
    }

    /**
     * Retrieves all 'ArvatoScore' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<ArvatoScore> getArvatoScoreList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<ArvatoScore> result = getEntities(ArvatoScore.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'ArvatoScore' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<ArvatoScore> getArvatoScoreList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<ArvatoScore> result = getEntities(ArvatoScore.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ArvatoScore' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<ArvatoScore> getArvatoScoreList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoScore> result = getEntities(ArvatoScore.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ArvatoScore' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<ArvatoScore> getArvatoScoreList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoScore> result = getEntities(ArvatoScore.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ArvatoScore' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoScore> findArvatoScoreList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<ArvatoScore> result = findEntities(ArvatoScore.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ArvatoScore' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<ArvatoScore> findArvatoScoreList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<ArvatoScore> result = findEntities(ArvatoScore.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'ArvatoScore' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoScore> findArvatoScoreList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<ArvatoScore> result = findEntities(ArvatoScore.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ArvatoScore' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoScore> findArvatoScoreList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoScore> result = findEntities(ArvatoScore.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ArvatoScore' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoScore> findArvatoScoreList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoScore> result = findEntities(ArvatoScore.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ArvatoScore' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoScore> findArvatoScoreList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoScore> result = findEntities(ArvatoScore.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'ArvatoScore' instances.
     */
    public long getArvatoScoreCount() {
        long result = getEntityCount(ArvatoScore.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ArvatoScore' instances which match the given whereClause.
     */
    public long getArvatoScoreCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(ArvatoScore.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ArvatoScore' instances which match the given whereClause together with params specified in object array.
     */
    public long getArvatoScoreCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(ArvatoScore.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(arvato score)}
    @Override
    public Collection<ArvatoScore> findAll() {
    	return getEntities(ArvatoScore.class);
    }
    
    public ArvatoScore findArvatoScore(ArvatoRequest arvatoRequest) throws ArvatoDataException {
    	// fill parameters into query
    	Query query = getPersistenceManager().newNamedQuery(ArvatoScore.class, "findArvatoScoreByArvatoRequest");
    	Map<String, Object> params = new HashMap<String, Object>();
        params.put("arvatoRequestId", arvatoRequest.getId());

        // execute query
        @SuppressWarnings("unchecked")
        List<ArvatoScore> queryResult = (List<ArvatoScore>) query.executeWithMap(params);

        // just debug
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("hasArvatoRequest() arvatoRequestId=" + arvatoRequest.getId() + " results=" + queryResult.size());
        }

        // return result
        if(queryResult.size() > 0) {
        	if(queryResult.size() > 1) {
        		throw new ArvatoDataException("ArvatoScore table is corrupt, because there is more than one row for ArvatoRequest.");
        	}
        	return queryResult.get(0);
        }
        
        return null;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
