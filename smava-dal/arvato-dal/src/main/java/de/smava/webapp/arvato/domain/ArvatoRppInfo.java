//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato rpp info)}
import de.smava.webapp.arvato.domain.history.ArvatoRppInfoHistory;

import java.util.Date;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoRppInfos'.
 *
 * @author generator
 */
public class ArvatoRppInfo extends ArvatoRppInfoHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvato rpp info)}
    private String _eScoreValue;
    private Integer _bankAccountValidationResult;

    /**
     * Setter for the property 'e score value'.
     */
    public void setEScoreValue(ArvatoEScoreValueType eScoreValue) {
        _eScoreValue = eScoreValue.getDbValue();
    }
    public void setEScoreValue(String eScoreValue) {
        _eScoreValue = eScoreValue;
    }

    /**
     * Returns the property 'e score value'.
     */
    public ArvatoEScoreValueType getEScoreValue() {
        return ArvatoEScoreValueType.enumFromDbValue(_eScoreValue);
    }
    /**
     * Setter for the property 'bank account validation result'.
     */
    public void setBankAccountValidationResult(ArvatoBankAccountValidationResultType bankAccountValidationResult) {
        _bankAccountValidationResult = (Integer) bankAccountValidationResult.getDbValue();
    }
    public void setBankAccountValidationResult(Integer bankAccountValidationResult) {
        _bankAccountValidationResult = bankAccountValidationResult;
    }

    /**
     * Returns the property 'bank account validation result'.
     */
    public ArvatoBankAccountValidationResultType getBankAccountValidationResult() {
        return ArvatoBankAccountValidationResultType.enumFromDbValue(_bankAccountValidationResult);
    }


    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected ArvatoRequest _arvatoRequest;
        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.BankAccount _bankAccount;
        protected boolean _rppMatch;
        protected String _bankAccountValidationMessage;
        protected String _bankAccountNr;
        protected String _bankCode;
        protected String _bankName;
        protected String _bankIban;
        protected String _bankBic;
        protected String _country;
        protected String _errorCodeName;
        protected String _errorDescription;
        protected Set<ArvatoRppInfoToken> _arvatoRppInfoTokens;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'arvato request'.
     */
    public void setArvatoRequest(ArvatoRequest arvatoRequest) {
        _arvatoRequest = arvatoRequest;
    }
            
    /**
     * Returns the property 'arvato request'.
     */
    public ArvatoRequest getArvatoRequest() {
        return _arvatoRequest;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'bank account'.
     */
    public void setBankAccount(de.smava.webapp.account.domain.BankAccount bankAccount) {
        _bankAccount = bankAccount;
    }
            
    /**
     * Returns the property 'bank account'.
     */
    public de.smava.webapp.account.domain.BankAccount getBankAccount() {
        return _bankAccount;
    }
                                    /**
     * Setter for the property 'rpp match'.
     */
    public void setRppMatch(boolean rppMatch) {
        if (!_rppMatchIsSet) {
            _rppMatchIsSet = true;
            _rppMatchInitVal = getRppMatch();
        }
        registerChange("rpp match", _rppMatchInitVal, rppMatch);
        _rppMatch = rppMatch;
    }
                        
    /**
     * Returns the property 'rpp match'.
     */
    public boolean getRppMatch() {
        return _rppMatch;
    }
                                    /**
     * Setter for the property 'bank account validation message'.
     */
    public void setBankAccountValidationMessage(String bankAccountValidationMessage) {
        if (!_bankAccountValidationMessageIsSet) {
            _bankAccountValidationMessageIsSet = true;
            _bankAccountValidationMessageInitVal = getBankAccountValidationMessage();
        }
        registerChange("bank account validation message", _bankAccountValidationMessageInitVal, bankAccountValidationMessage);
        _bankAccountValidationMessage = bankAccountValidationMessage;
    }
                        
    /**
     * Returns the property 'bank account validation message'.
     */
    public String getBankAccountValidationMessage() {
        return _bankAccountValidationMessage;
    }
                                    /**
     * Setter for the property 'bank account nr'.
     */
    public void setBankAccountNr(String bankAccountNr) {
        if (!_bankAccountNrIsSet) {
            _bankAccountNrIsSet = true;
            _bankAccountNrInitVal = getBankAccountNr();
        }
        registerChange("bank account nr", _bankAccountNrInitVal, bankAccountNr);
        _bankAccountNr = bankAccountNr;
    }
                        
    /**
     * Returns the property 'bank account nr'.
     */
    public String getBankAccountNr() {
        return _bankAccountNr;
    }
                                    /**
     * Setter for the property 'bank code'.
     */
    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = getBankCode();
        }
        registerChange("bank code", _bankCodeInitVal, bankCode);
        _bankCode = bankCode;
    }
                        
    /**
     * Returns the property 'bank code'.
     */
    public String getBankCode() {
        return _bankCode;
    }
                                    /**
     * Setter for the property 'bank name'.
     */
    public void setBankName(String bankName) {
        if (!_bankNameIsSet) {
            _bankNameIsSet = true;
            _bankNameInitVal = getBankName();
        }
        registerChange("bank name", _bankNameInitVal, bankName);
        _bankName = bankName;
    }
                        
    /**
     * Returns the property 'bank name'.
     */
    public String getBankName() {
        return _bankName;
    }
                                    /**
     * Setter for the property 'bank iban'.
     */
    public void setBankIban(String bankIban) {
        if (!_bankIbanIsSet) {
            _bankIbanIsSet = true;
            _bankIbanInitVal = getBankIban();
        }
        registerChange("bank iban", _bankIbanInitVal, bankIban);
        _bankIban = bankIban;
    }
                        
    /**
     * Returns the property 'bank iban'.
     */
    public String getBankIban() {
        return _bankIban;
    }
                                    /**
     * Setter for the property 'bank bic'.
     */
    public void setBankBic(String bankBic) {
        if (!_bankBicIsSet) {
            _bankBicIsSet = true;
            _bankBicInitVal = getBankBic();
        }
        registerChange("bank bic", _bankBicInitVal, bankBic);
        _bankBic = bankBic;
    }
                        
    /**
     * Returns the property 'bank bic'.
     */
    public String getBankBic() {
        return _bankBic;
    }
                                    /**
     * Setter for the property 'country'.
     */
    public void setCountry(String country) {
        if (!_countryIsSet) {
            _countryIsSet = true;
            _countryInitVal = getCountry();
        }
        registerChange("country", _countryInitVal, country);
        _country = country;
    }
                        
    /**
     * Returns the property 'country'.
     */
    public String getCountry() {
        return _country;
    }
                                    /**
     * Setter for the property 'error code name'.
     */
    public void setErrorCodeName(String errorCodeName) {
        if (!_errorCodeNameIsSet) {
            _errorCodeNameIsSet = true;
            _errorCodeNameInitVal = getErrorCodeName();
        }
        registerChange("error code name", _errorCodeNameInitVal, errorCodeName);
        _errorCodeName = errorCodeName;
    }
                        
    /**
     * Returns the property 'error code name'.
     */
    public String getErrorCodeName() {
        return _errorCodeName;
    }
                                    /**
     * Setter for the property 'error description'.
     */
    public void setErrorDescription(String errorDescription) {
        if (!_errorDescriptionIsSet) {
            _errorDescriptionIsSet = true;
            _errorDescriptionInitVal = getErrorDescription();
        }
        registerChange("error description", _errorDescriptionInitVal, errorDescription);
        _errorDescription = errorDescription;
    }
                        
    /**
     * Returns the property 'error description'.
     */
    public String getErrorDescription() {
        return _errorDescription;
    }
                                            
    /**
     * Setter for the property 'arvato rpp info tokens'.
     */
    public void setArvatoRppInfoTokens(Set<ArvatoRppInfoToken> arvatoRppInfoTokens) {
        _arvatoRppInfoTokens = arvatoRppInfoTokens;
    }
            
    /**
     * Returns the property 'arvato rpp info tokens'.
     */
    public Set<ArvatoRppInfoToken> getArvatoRppInfoTokens() {
        return _arvatoRppInfoTokens;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoRppInfo.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rppMatch=").append(_rppMatch);
            builder.append("\n    _bankAccountValidationMessage=").append(_bankAccountValidationMessage);
            builder.append("\n    _bankAccountNr=").append(_bankAccountNr);
            builder.append("\n    _bankCode=").append(_bankCode);
            builder.append("\n    _bankName=").append(_bankName);
            builder.append("\n    _bankIban=").append(_bankIban);
            builder.append("\n    _bankBic=").append(_bankBic);
            builder.append("\n    _country=").append(_country);
            builder.append("\n    _errorCodeName=").append(_errorCodeName);
            builder.append("\n    _errorDescription=").append(_errorDescription);
            builder.append("\n}");
        } else {
            builder.append(ArvatoRppInfo.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoRppInfo asArvatoRppInfo() {
        return this;
    }
}
