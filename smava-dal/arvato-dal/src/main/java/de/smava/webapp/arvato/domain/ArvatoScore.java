//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.arvato.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvato score)}
import de.smava.webapp.arvato.domain.history.ArvatoScoreHistory;

import java.util.Date;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ArvatoScores'.
 *
 * @author generator
 */
public class ArvatoScore extends ArvatoScoreHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(arvato score)}

    private String _eScoreValue;

    /**
     * Setter for the property 'e score value'.
     */
    public void setEScoreValue(ArvatoEScoreValueType eScoreValue) {
        _eScoreValue = eScoreValue.getDbValue();
    }
    public void setEScoreValue(String eScoreValue) {
        _eScoreValue = eScoreValue;
    }

    /**
     * Returns the property 'e score value'.
     */
    public ArvatoEScoreValueType getEScoreValue() {
        return ArvatoEScoreValueType.enumFromDbValue(_eScoreValue);
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected ArvatoRequest _arvatoRequest;
        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.Person _person;
        protected String _scoreType;
        protected Integer _scoreValue;
        protected Set<ArvatoScoreToken> _arvatoScoreTokens;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'arvato request'.
     */
    public void setArvatoRequest(ArvatoRequest arvatoRequest) {
        _arvatoRequest = arvatoRequest;
    }
            
    /**
     * Returns the property 'arvato request'.
     */
    public ArvatoRequest getArvatoRequest() {
        return _arvatoRequest;
    }
                                            
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'person'.
     */
    public void setPerson(de.smava.webapp.account.domain.Person person) {
        _person = person;
    }
            
    /**
     * Returns the property 'person'.
     */
    public de.smava.webapp.account.domain.Person getPerson() {
        return _person;
    }
                                    /**
     * Setter for the property 'score type'.
     */
    public void setScoreType(String scoreType) {
        if (!_scoreTypeIsSet) {
            _scoreTypeIsSet = true;
            _scoreTypeInitVal = getScoreType();
        }
        registerChange("score type", _scoreTypeInitVal, scoreType);
        _scoreType = scoreType;
    }
                        
    /**
     * Returns the property 'score type'.
     */
    public String getScoreType() {
        return _scoreType;
    }
                                            
    /**
     * Setter for the property 'score value'.
     */
    public void setScoreValue(Integer scoreValue) {
        _scoreValue = scoreValue;
    }
            
    /**
     * Returns the property 'score value'.
     */
    public Integer getScoreValue() {
        return _scoreValue;
    }
                                            
    /**
     * Setter for the property 'arvato score tokens'.
     */
    public void setArvatoScoreTokens(Set<ArvatoScoreToken> arvatoScoreTokens) {
        _arvatoScoreTokens = arvatoScoreTokens;
    }
            
    /**
     * Returns the property 'arvato score tokens'.
     */
    public Set<ArvatoScoreToken> getArvatoScoreTokens() {
        return _arvatoScoreTokens;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ArvatoScore.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _scoreType=").append(_scoreType);
            builder.append("\n}");
        } else {
            builder.append(ArvatoScore.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ArvatoScore asArvatoScore() {
        return this;
    }
}
