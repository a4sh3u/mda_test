//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.arvato.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(arvatoAddressCheck)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.arvato.dao.ArvatoAddressCheckDao;
import de.smava.webapp.arvato.domain.ArvatoAddressCheck;
import de.smava.webapp.arvato.domain.ArvatoRequest;
import de.smava.webapp.arvato.domain.ArvatoRppInfo;
import de.smava.webapp.arvato.exception.ArvatoDataException;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'ArvatoAddressChecks'.
 *
 * @author generator
 */
@Repository(value = "arvatoAddressCheckDao")
public class JdoArvatoAddressCheckDao extends JdoBaseDao implements ArvatoAddressCheckDao {

    private static final Logger LOGGER = Logger.getLogger(JdoArvatoAddressCheckDao.class);

    private static final String CLASS_NAME = "ArvatoAddressCheck";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(arvatoAddressCheck)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the arvatoAddressCheck identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public ArvatoAddressCheck load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        ArvatoAddressCheck result = getEntity(ArvatoAddressCheck.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public ArvatoAddressCheck getArvatoAddressCheck(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	ArvatoAddressCheck entity = findUniqueEntity(ArvatoAddressCheck.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the arvatoAddressCheck.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(ArvatoAddressCheck arvatoAddressCheck) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveArvatoAddressCheck: " + arvatoAddressCheck);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(arvatoAddressCheck)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(arvatoAddressCheck);
    }

    /**
     * @deprecated Use {@link #save(ArvatoAddressCheck) instead}
     */
    public Long saveArvatoAddressCheck(ArvatoAddressCheck arvatoAddressCheck) {
        return save(arvatoAddressCheck);
    }

    /**
     * Deletes an arvatoAddressCheck, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the arvatoAddressCheck
     */
    public void deleteArvatoAddressCheck(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteArvatoAddressCheck: " + id);
        }
        deleteEntity(ArvatoAddressCheck.class, id);
    }

    /**
     * Retrieves all 'ArvatoAddressCheck' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<ArvatoAddressCheck> getArvatoAddressCheckList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<ArvatoAddressCheck> result = getEntities(ArvatoAddressCheck.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'ArvatoAddressCheck' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<ArvatoAddressCheck> getArvatoAddressCheckList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<ArvatoAddressCheck> result = getEntities(ArvatoAddressCheck.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ArvatoAddressCheck' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<ArvatoAddressCheck> getArvatoAddressCheckList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoAddressCheck> result = getEntities(ArvatoAddressCheck.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ArvatoAddressCheck' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<ArvatoAddressCheck> getArvatoAddressCheckList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoAddressCheck> result = getEntities(ArvatoAddressCheck.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ArvatoAddressCheck' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoAddressCheck> findArvatoAddressCheckList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<ArvatoAddressCheck> result = findEntities(ArvatoAddressCheck.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'ArvatoAddressCheck' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<ArvatoAddressCheck> findArvatoAddressCheckList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<ArvatoAddressCheck> result = findEntities(ArvatoAddressCheck.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'ArvatoAddressCheck' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoAddressCheck> findArvatoAddressCheckList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<ArvatoAddressCheck> result = findEntities(ArvatoAddressCheck.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'ArvatoAddressCheck' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoAddressCheck> findArvatoAddressCheckList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoAddressCheck> result = findEntities(ArvatoAddressCheck.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ArvatoAddressCheck' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoAddressCheck> findArvatoAddressCheckList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoAddressCheck> result = findEntities(ArvatoAddressCheck.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'ArvatoAddressCheck' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<ArvatoAddressCheck> findArvatoAddressCheckList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<ArvatoAddressCheck> result = findEntities(ArvatoAddressCheck.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'ArvatoAddressCheck' instances.
     */
    public long getArvatoAddressCheckCount() {
        long result = getEntityCount(ArvatoAddressCheck.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ArvatoAddressCheck' instances which match the given whereClause.
     */
    public long getArvatoAddressCheckCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(ArvatoAddressCheck.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'ArvatoAddressCheck' instances which match the given whereClause together with params specified in object array.
     */
    public long getArvatoAddressCheckCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(ArvatoAddressCheck.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(arvatoAddressCheck)}
    @Override
    public Collection<ArvatoAddressCheck> findAll() {
    	return getEntities(ArvatoAddressCheck.class);
    }
    
    public ArvatoAddressCheck findArvatoAddressCheck(ArvatoRequest arvatoRequest) throws ArvatoDataException {
    	// fill parameters into query
    	Query query = getPersistenceManager().newNamedQuery(ArvatoAddressCheck.class, "findArvatoAddressCheckByArvatoRequest");
    	Map<String, Object> params = new HashMap<String, Object>();
        params.put("arvatoRequestId", arvatoRequest.getId());

        // execute query
        @SuppressWarnings("unchecked")
        List<ArvatoAddressCheck> queryResult = (List<ArvatoAddressCheck>) query.executeWithMap(params);

        // just debug
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("hasArvatoRequest() arvatoRequestId=" + arvatoRequest.getId() + " results=" + queryResult.size());
        }

        // return result
        if(queryResult.size() > 0) {
        	if(queryResult.size() > 1) {
        		throw new ArvatoDataException("ArvatoAddressCheck table is corrupt, because there is more than one row for ArvatoRequest.");
        	}
        	return queryResult.get(0);
        }
        
        return null;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
