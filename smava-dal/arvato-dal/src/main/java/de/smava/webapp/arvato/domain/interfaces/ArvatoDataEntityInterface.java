package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoData;


/**
 * The domain object that represents 'ArvatoDatas'.
 *
 * @author generator
 */
public interface ArvatoDataEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'person'.
     *
     * 
     *
     */
    void setPerson(de.smava.webapp.account.domain.Person person);

    /**
     * Returns the property 'person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getPerson();
    /**
     * Setter for the property 'latest arvato score'.
     *
     * 
     *
     */
    void setLatestArvatoScore(de.smava.webapp.arvato.domain.ArvatoRequest latestArvatoScore);

    /**
     * Returns the property 'latest arvato score'.
     *
     * 
     *
     */
    de.smava.webapp.arvato.domain.ArvatoRequest getLatestArvatoScore();
    /**
     * Setter for the property 'latest arvato rpp info'.
     *
     * 
     *
     */
    void setLatestArvatoRppInfo(de.smava.webapp.arvato.domain.ArvatoRequest latestArvatoRppInfo);

    /**
     * Returns the property 'latest arvato rpp info'.
     *
     * 
     *
     */
    de.smava.webapp.arvato.domain.ArvatoRequest getLatestArvatoRppInfo();
    /**
     * Setter for the property 'latest arvato address check'.
     *
     * 
     *
     */
    void setLatestArvatoAddressCheck(de.smava.webapp.arvato.domain.ArvatoRequest latestArvatoAddressCheck);

    /**
     * Returns the property 'latest arvato address check'.
     *
     * 
     *
     */
    de.smava.webapp.arvato.domain.ArvatoRequest getLatestArvatoAddressCheck();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoData asArvatoData();
}
