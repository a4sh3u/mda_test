package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoData;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoDataHistory extends AbstractArvatoData {



					
}
