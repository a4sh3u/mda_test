package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoRppInfo;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoRppInfos'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoRppInfoHistory extends AbstractArvatoRppInfo {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient boolean _rppMatchInitVal;
    protected transient boolean _rppMatchIsSet;
    protected transient String _bankAccountValidationMessageInitVal;
    protected transient boolean _bankAccountValidationMessageIsSet;
    protected transient String _bankAccountNrInitVal;
    protected transient boolean _bankAccountNrIsSet;
    protected transient String _bankCodeInitVal;
    protected transient boolean _bankCodeIsSet;
    protected transient String _bankNameInitVal;
    protected transient boolean _bankNameIsSet;
    protected transient String _bankIbanInitVal;
    protected transient boolean _bankIbanIsSet;
    protected transient String _bankBicInitVal;
    protected transient boolean _bankBicIsSet;
    protected transient String _countryInitVal;
    protected transient boolean _countryIsSet;
    protected transient String _errorCodeNameInitVal;
    protected transient boolean _errorCodeNameIsSet;
    protected transient String _errorDescriptionInitVal;
    protected transient boolean _errorDescriptionIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'rpp match'.
     */
    public boolean rppMatchInitVal() {
        boolean result;
        if (_rppMatchIsSet) {
            result = _rppMatchInitVal;
        } else {
            result = getRppMatch();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rpp match'.
     */
    public boolean rppMatchIsDirty() {
        return !valuesAreEqual(rppMatchInitVal(), getRppMatch());
    }

    /**
     * Returns true if the setter method was called for the property 'rpp match'.
     */
    public boolean rppMatchIsSet() {
        return _rppMatchIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank account validation message'.
     */
    public String bankAccountValidationMessageInitVal() {
        String result;
        if (_bankAccountValidationMessageIsSet) {
            result = _bankAccountValidationMessageInitVal;
        } else {
            result = getBankAccountValidationMessage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank account validation message'.
     */
    public boolean bankAccountValidationMessageIsDirty() {
        return !valuesAreEqual(bankAccountValidationMessageInitVal(), getBankAccountValidationMessage());
    }

    /**
     * Returns true if the setter method was called for the property 'bank account validation message'.
     */
    public boolean bankAccountValidationMessageIsSet() {
        return _bankAccountValidationMessageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank account nr'.
     */
    public String bankAccountNrInitVal() {
        String result;
        if (_bankAccountNrIsSet) {
            result = _bankAccountNrInitVal;
        } else {
            result = getBankAccountNr();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank account nr'.
     */
    public boolean bankAccountNrIsDirty() {
        return !valuesAreEqual(bankAccountNrInitVal(), getBankAccountNr());
    }

    /**
     * Returns true if the setter method was called for the property 'bank account nr'.
     */
    public boolean bankAccountNrIsSet() {
        return _bankAccountNrIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank code'.
     */
    public String bankCodeInitVal() {
        String result;
        if (_bankCodeIsSet) {
            result = _bankCodeInitVal;
        } else {
            result = getBankCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank code'.
     */
    public boolean bankCodeIsDirty() {
        return !valuesAreEqual(bankCodeInitVal(), getBankCode());
    }

    /**
     * Returns true if the setter method was called for the property 'bank code'.
     */
    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank name'.
     */
    public String bankNameInitVal() {
        String result;
        if (_bankNameIsSet) {
            result = _bankNameInitVal;
        } else {
            result = getBankName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank name'.
     */
    public boolean bankNameIsDirty() {
        return !valuesAreEqual(bankNameInitVal(), getBankName());
    }

    /**
     * Returns true if the setter method was called for the property 'bank name'.
     */
    public boolean bankNameIsSet() {
        return _bankNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank iban'.
     */
    public String bankIbanInitVal() {
        String result;
        if (_bankIbanIsSet) {
            result = _bankIbanInitVal;
        } else {
            result = getBankIban();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank iban'.
     */
    public boolean bankIbanIsDirty() {
        return !valuesAreEqual(bankIbanInitVal(), getBankIban());
    }

    /**
     * Returns true if the setter method was called for the property 'bank iban'.
     */
    public boolean bankIbanIsSet() {
        return _bankIbanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank bic'.
     */
    public String bankBicInitVal() {
        String result;
        if (_bankBicIsSet) {
            result = _bankBicInitVal;
        } else {
            result = getBankBic();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank bic'.
     */
    public boolean bankBicIsDirty() {
        return !valuesAreEqual(bankBicInitVal(), getBankBic());
    }

    /**
     * Returns true if the setter method was called for the property 'bank bic'.
     */
    public boolean bankBicIsSet() {
        return _bankBicIsSet;
    }
	
    /**
     * Returns the initial value of the property 'country'.
     */
    public String countryInitVal() {
        String result;
        if (_countryIsSet) {
            result = _countryInitVal;
        } else {
            result = getCountry();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'country'.
     */
    public boolean countryIsDirty() {
        return !valuesAreEqual(countryInitVal(), getCountry());
    }

    /**
     * Returns true if the setter method was called for the property 'country'.
     */
    public boolean countryIsSet() {
        return _countryIsSet;
    }
	
    /**
     * Returns the initial value of the property 'error code name'.
     */
    public String errorCodeNameInitVal() {
        String result;
        if (_errorCodeNameIsSet) {
            result = _errorCodeNameInitVal;
        } else {
            result = getErrorCodeName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'error code name'.
     */
    public boolean errorCodeNameIsDirty() {
        return !valuesAreEqual(errorCodeNameInitVal(), getErrorCodeName());
    }

    /**
     * Returns true if the setter method was called for the property 'error code name'.
     */
    public boolean errorCodeNameIsSet() {
        return _errorCodeNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'error description'.
     */
    public String errorDescriptionInitVal() {
        String result;
        if (_errorDescriptionIsSet) {
            result = _errorDescriptionInitVal;
        } else {
            result = getErrorDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'error description'.
     */
    public boolean errorDescriptionIsDirty() {
        return !valuesAreEqual(errorDescriptionInitVal(), getErrorDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'error description'.
     */
    public boolean errorDescriptionIsSet() {
        return _errorDescriptionIsSet;
    }
	
}
