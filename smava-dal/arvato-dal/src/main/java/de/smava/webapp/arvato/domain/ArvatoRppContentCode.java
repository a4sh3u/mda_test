package de.smava.webapp.arvato.domain;

public enum ArvatoRppContentCode {
	
	/**
	 * ArvatoRppContentCodeCardBlockingType
	 * EC-Karte zur Bankverbindung wurde als gestohlen/verloren gemeldet
	 */
	DEBIT_CARD_FOR_THE_BANK_ACCOUNT_WAS_STOLEN_OR_REPORTED_LOST(0, ArvatoRppContentType.CARD_BLOCKING), 
	
	/**
	 * ArvatoRppContentCodeCardBlockingType
	 * Zur Bankverbindung gibt es einen Eintrag in der KUNO-Datenbank
	 */
	AT_BANK_THERE_IS_AN_ENTRY_IN_DATABASE_KUNO(1, ArvatoRppContentType.CARD_BLOCKING),
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * mangels Deckung / nicht bezahlt
	 */
	LACK_OF_FUNDS_OR_NOT_PAID(1, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}), 
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * Konto falsch
	 */
	WRONG_ACCOUNT(2, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}),
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * Widerspruch
	 */
	CONTRADICTION(3, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}),
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * Konto erloschen
	 */
	EXTINGUISHED_ACCOUNT(5, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}),
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * sonstiges
	 */
	OTHER(6, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}), 
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * Keine Information
	 */
	NO_INFORMATION(7, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}), 
	
	/**
	 * ArvatoRppContentCodeCompletedRlsType
	 * ArvatoRppContentCodeHistoricalRlsType
	 * ArvatoRppContentCodeOpenRlsType
	 * kein gültiges Mandat
	 */
	NO_VALID_MANDATE(8, new ArvatoRppContentType[]{ArvatoRppContentType.COMPLETED_RLS, ArvatoRppContentType.HISTORICAL_RLS, ArvatoRppContentType.OPEN_RLS}),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * unklassifiziert
	 */
	UNCLASSIFIED(0, ArvatoRppContentType.NCA), 
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Spenden und Hilfsorganisationen
	 */
	DONATIONS_AND_AID_ORGANIZATIONS(1, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Ämter und öffentliche Einrichtungen
	 */
	OFFICES_AND_PUBLIC_INSTITUTIONS(2, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}), 
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Vereine
	 */
	CLUBS(3, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}), 
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Bildungsinstitutionen und Kirche
	 */
	EDUCATIONAL_INSTITUTIONS_AND_CHURCH(4, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Freizeitinstitutionen & Touristik
	 */
	RECREATIONAL_INSTITUTIONS_AND_TOURISM(5, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Verbände und Stiftungen
	 */
	ASSOCIATIONS_AND_FOUNDATIONS(6, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ArvatoRppContentCodePapType
	 * Firmen
	 */
	COMPANY(7, new ArvatoRppContentType[]{ArvatoRppContentType.NCA, ArvatoRppContentType.PAP}), 
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * ebay-Seller
	 */
	EBAY_SELLER(9, ArvatoRppContentType.NCA),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * Privatperson
	 */
	PRIVATE_PERSON(12, ArvatoRppContentType.NCA),
	
	/**
	 * ArvatoRppContentCodeNcaType
	 * Kreditkonten
	 */
	CREDIT_ACCOUNTS(31, ArvatoRppContentType.NCA); 
	
	
	ArvatoRppContentCode(int dbValue, ArvatoRppContentType ... arvatoRppContentTypes) {
		this.dbValue = dbValue;
		this.arvatoRppContentTypes = arvatoRppContentTypes;
	}
	
	private int dbValue;
	private ArvatoRppContentType[] arvatoRppContentTypes;

	public int getDbValue() {
		return dbValue;
	}
	
	public static ArvatoRppContentCode enumFromDbValue(int dbValue, ArvatoRppContentType arvatoRppContentType) {
		for(ArvatoRppContentCode e1: ArvatoRppContentCode.values()) {
			if(e1.dbValue == dbValue) {
				for(ArvatoRppContentType e2: e1.arvatoRppContentTypes) {
					if(e2 == arvatoRppContentType) {
						return e1;
					}
				}
			}
		}
        throw new IllegalArgumentException(String.valueOf(dbValue) + " : " + arvatoRppContentType);
	}

}
