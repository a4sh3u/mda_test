package de.smava.webapp.arvato.domain.history;



import de.smava.webapp.arvato.domain.abstracts.AbstractArvatoScore;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'ArvatoScores'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ArvatoScoreHistory extends AbstractArvatoScore {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _scoreTypeInitVal;
    protected transient boolean _scoreTypeIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
				
    /**
     * Returns the initial value of the property 'score type'.
     */
    public String scoreTypeInitVal() {
        String result;
        if (_scoreTypeIsSet) {
            result = _scoreTypeInitVal;
        } else {
            result = getScoreType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'score type'.
     */
    public boolean scoreTypeIsDirty() {
        return !valuesAreEqual(scoreTypeInitVal(), getScoreType());
    }

    /**
     * Returns true if the setter method was called for the property 'score type'.
     */
    public boolean scoreTypeIsSet() {
        return _scoreTypeIsSet;
    }
		
}
