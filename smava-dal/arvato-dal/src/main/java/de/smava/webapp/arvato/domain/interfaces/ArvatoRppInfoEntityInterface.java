package de.smava.webapp.arvato.domain.interfaces;



import de.smava.webapp.arvato.domain.ArvatoRequest;
import de.smava.webapp.arvato.domain.ArvatoRppInfo;
import de.smava.webapp.arvato.domain.ArvatoRppInfoToken;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'ArvatoRppInfos'.
 *
 * @author generator
 */
public interface ArvatoRppInfoEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'arvato request'.
     *
     * 
     *
     */
    void setArvatoRequest(ArvatoRequest arvatoRequest);

    /**
     * Returns the property 'arvato request'.
     *
     * 
     *
     */
    ArvatoRequest getArvatoRequest();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    void setBankAccount(de.smava.webapp.account.domain.BankAccount bankAccount);

    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.BankAccount getBankAccount();
    /**
     * Setter for the property 'rpp match'.
     *
     * 
     *
     */
    void setRppMatch(boolean rppMatch);

    /**
     * Returns the property 'rpp match'.
     *
     * 
     *
     */
    boolean getRppMatch();
    /**
     * Setter for the property 'bank account validation message'.
     *
     * 
     *
     */
    void setBankAccountValidationMessage(String bankAccountValidationMessage);

    /**
     * Returns the property 'bank account validation message'.
     *
     * 
     *
     */
    String getBankAccountValidationMessage();
    /**
     * Setter for the property 'bank account nr'.
     *
     * 
     *
     */
    void setBankAccountNr(String bankAccountNr);

    /**
     * Returns the property 'bank account nr'.
     *
     * 
     *
     */
    String getBankAccountNr();
    /**
     * Setter for the property 'bank code'.
     *
     * 
     *
     */
    void setBankCode(String bankCode);

    /**
     * Returns the property 'bank code'.
     *
     * 
     *
     */
    String getBankCode();
    /**
     * Setter for the property 'bank name'.
     *
     * 
     *
     */
    void setBankName(String bankName);

    /**
     * Returns the property 'bank name'.
     *
     * 
     *
     */
    String getBankName();
    /**
     * Setter for the property 'bank iban'.
     *
     * 
     *
     */
    void setBankIban(String bankIban);

    /**
     * Returns the property 'bank iban'.
     *
     * 
     *
     */
    String getBankIban();
    /**
     * Setter for the property 'bank bic'.
     *
     * 
     *
     */
    void setBankBic(String bankBic);

    /**
     * Returns the property 'bank bic'.
     *
     * 
     *
     */
    String getBankBic();
    /**
     * Setter for the property 'country'.
     *
     * 
     *
     */
    void setCountry(String country);

    /**
     * Returns the property 'country'.
     *
     * 
     *
     */
    String getCountry();
    /**
     * Setter for the property 'error code name'.
     *
     * 
     *
     */
    void setErrorCodeName(String errorCodeName);

    /**
     * Returns the property 'error code name'.
     *
     * 
     *
     */
    String getErrorCodeName();
    /**
     * Setter for the property 'error description'.
     *
     * 
     *
     */
    void setErrorDescription(String errorDescription);

    /**
     * Returns the property 'error description'.
     *
     * 
     *
     */
    String getErrorDescription();
    /**
     * Setter for the property 'arvato rpp info tokens'.
     *
     * 
     *
     */
    void setArvatoRppInfoTokens(Set<ArvatoRppInfoToken> arvatoRppInfoTokens);

    /**
     * Returns the property 'arvato rpp info tokens'.
     *
     * 
     *
     */
    Set<ArvatoRppInfoToken> getArvatoRppInfoTokens();
    /**
     * Helper method to get reference of this object as model type.
     */
    ArvatoRppInfo asArvatoRppInfo();
}
