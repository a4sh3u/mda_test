package de.smava.webapp.arvato.domain;

import de.smava.webapp.commons.domain.MappableEnum;
import de.smava.webapp.commons.domainutil.MappableEnumHelper;

public enum ArvatoBankAccountValidationResultType implements MappableEnum<ArvatoBankAccountValidationResultType> {
	
	/**
	 * Hinweis: Die Bankverbindung ist valide.
	 */
	BANK_IS_VALID(0),
	
	/**
	 * Hinweis: Die Bankverbindung ist valide, jedoch entspricht die BLZ einer Filiale der Bundesbank.
	 */
	BANK_IS_VALID_HOWEVER_CORRESPONDS_TO_BLZ_BRANCH_OF_BUNDESBANK(1),
	
	/**
	 * Hinweis: Die als gelöscht gekennzeichnete BLZ wurde durch die Nachfolge-BLZ ersetzt.
	 */
	BLZ_MARKED_AS_DELETED_WAS_REPLACED_BY_SUCCESSOR_BLZ(2),
	
	/**
	 * Hinweis: Die BIC passt nicht zur BLZ, die in der IBAN enthalten ist.
	 */
	BIC_DOES_NOT_MATCH_BLZ_THAT_IS_INCLUDED_IN_IBAN(3),
	
	/**
	 * Hinweis: Die BLZ passt nicht zur BLZ, die in der IBAN enthalten ist.
	 */
	BLZ_DOES_NOT_MATCH_BLZ_THAT_IS_INCLUDED_IN_IBAN(4), 
	
	/**
	 * Hinweis: Die Kontonummer passt nicht zur Kontonummer , die in der IBAN enthalten ist.
	 */
	ACCOUNT_NUMBER_DOES_NOT_MATCH_ACCOUNT_NUMBER_THAT_IS_INCLUDED_IN_IBAN(5),
	
	/**
	 * Hinweis: Die Länderschlüssel passt nicht zur Länderschlüssel, die in der IBAN enthalten ist.
	 */
	COUNTRY_CODE_DOES_NOT_MATCH_COUNTRY_CODE_THAT_IS_INCLUDED_IN_IBAN(6), 
	
	/**
	 * Hinweis: Die IBAN konnte nicht eindeutig ermittelt bzw. die Kontonummer nicht eindeutig geprüft werden. Falls möglich, sollte die Bankverbindung manuell überprüft werden.
	 */
	IBAN_CANNOT_RESOLVED_UNIQUELY_OR_BANK_ACCOUNT_CANNOT_BE_CHECKED_UNIQUELY(7), 
	
	/**
	 * Hinweis: Der BIC konnte nicht ermittelt werden.
	 */
	BIC_COULD_NOT_BE_DETERMINED(8),
	
//	(9), //
	
	/**
	 * Die IBAN-Prüfsumme ist ungültig.
	 */
	IBAN_CHECKSUM_IS_INVALID(10), 
	
	/**
	 * Der Länderschlüssel in der IBAN ist ungültig.
	 */
	COUNTRY_CODE_IN_IBAN_IS_INVALID(11), 
	
	/**
	 * Das IBAN-Format ist ungültig.
	 */
	IBAN_FORMAT_IS_INVALID(12), 
	
	/**
	 * Der BIC ist ungültig.
	 */
	BIC_IS_INVALID(13), 
	
	/**
	 * Die BLZ ist ungültig.
	 */
	BLZ_IS_INVALID(14), 
	
	/**
	 * Die IBAN kann nicht ermittelt werden.
	 */
	IBAN_CAN_NOT_BE_DETERMINED(15), 
	
	/**
	 * Die Kontonummer ist ungültig.
	 */
	ACCOUNT_NUMBER_IS_INVALID(16); 

	ArvatoBankAccountValidationResultType(int dbValue) {
		this.dbValue = dbValue;
	}

	private int dbValue;

	/*public static ArvatoBankAccountValidationResultType fromValue(int value) {
		for(ArvatoBankAccountValidationResultType e: ArvatoBankAccountValidationResultType.values()) {
			if(e.value == value) {
				return e;
			}
		}
        throw new IllegalArgumentException(String.valueOf(value));
	}

    public static ArvatoBankAccountValidationResultType fromValue(String value) {
        int asInt = Integer.parseInt(value);
        return fromValue(asInt);
    }*/

	@Override
	public Number getDbValue() {
		return dbValue;
	}

	@Override
	public ArvatoBankAccountValidationResultType getEnumFromDbValue(Number dbValue) {
		return enumFromDbValue(dbValue);
	}
	
	public static ArvatoBankAccountValidationResultType enumFromDbValue(final String value) {
		int dbValue = Integer.parseInt(value);
		return enumFromDbValue(dbValue);
	}
	
	public static ArvatoBankAccountValidationResultType enumFromDbValue(final Number dbValue) {
		return MappableEnumHelper.getEnumFromDbValue(ArvatoBankAccountValidationResultType.class, dbValue);
	}

	@Override
	public String getName() {
		return name();
	}
}
