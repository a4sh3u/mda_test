//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result)}
import de.smava.webapp.ident.domain.history.IdentificationBrokerageBankResultHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationBrokerageBankResults'.
 *
 * @author generator
 */
public class IdentificationBrokerageBankResult extends IdentificationBrokerageBankResultHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(identification brokerage bank result)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
        protected IdentificationBrokerageBankResultType _type;
        protected IdentificationBrokerageBankResultState _state;
        protected String _responseData;
        protected Date _creationDate;
        protected Set<de.smava.webapp.account.domain.Person> _persons;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'brokerageBank'.
     */
    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerageBank'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'type'.
     */
    public void setType(IdentificationBrokerageBankResultType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     */
    public IdentificationBrokerageBankResultType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'state'.
     */
    public void setState(IdentificationBrokerageBankResultState state) {
        _state = state;
    }
            
    /**
     * Returns the property 'state'.
     */
    public IdentificationBrokerageBankResultState getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'responseData'.
     */
    public void setResponseData(String responseData) {
        if (!_responseDataIsSet) {
            _responseDataIsSet = true;
            _responseDataInitVal = getResponseData();
        }
        registerChange("responseData", _responseDataInitVal, responseData);
        _responseData = responseData;
    }
                        
    /**
     * Returns the property 'responseData'.
     */
    public String getResponseData() {
        return _responseData;
    }
                                    /**
     * Setter for the property 'creationDate'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creationDate", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creationDate'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'persons'.
     */
    public void setPersons(Set<de.smava.webapp.account.domain.Person> persons) {
        _persons = persons;
    }
            
    /**
     * Returns the property 'persons'.
     */
    public Set<de.smava.webapp.account.domain.Person> getPersons() {
        return _persons;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdentificationBrokerageBankResult.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _responseData=").append(_responseData);
            builder.append("\n}");
        } else {
            builder.append(IdentificationBrokerageBankResult.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IdentificationBrokerageBankResult asIdentificationBrokerageBankResult() {
        return this;
    }
}
