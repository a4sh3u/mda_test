//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ident.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(ident token)}

import de.smava.webapp.account.domain.Person;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ident.domain.IdentToken;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'IdentTokens'.
 *
 * @author generator
 */
public interface IdentTokenDao extends BaseDao<IdentToken> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the ident token identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    IdentToken getIdentToken(Long id);

    /**
     * Saves the ident token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveIdentToken(IdentToken identToken);

    /**
     * Deletes an ident token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the ident token
     */
    void deleteIdentToken(Long id);

    /**
     * Retrieves all 'IdentToken' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<IdentToken> getIdentTokenList();

    /**
     * Retrieves a page of 'IdentToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<IdentToken> getIdentTokenList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'IdentToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<IdentToken> getIdentTokenList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'IdentToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<IdentToken> getIdentTokenList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'IdentToken' instances.
     */
    long getIdentTokenCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(ident token)}
    //
    boolean isTransactionNumberExisting(String transactionNumber);
    boolean isExistingForPerson(Person person, Date currentDate);

    IdentToken getIdentTokenForPerson(long personId, Date currentDate);
    List<IdentToken> findUnexpiredAndUnidentifiedIdentTokenList(Date currentDate);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
