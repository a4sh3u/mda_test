package de.smava.webapp.ident.domain.history;



import de.smava.webapp.ident.domain.abstracts.AbstractIdentificationBrokerageBankToken;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'IdentificationBrokerageBankTokens'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IdentificationBrokerageBankTokenHistory extends AbstractIdentificationBrokerageBankToken {

    protected transient String _responseDataInitVal;
    protected transient boolean _responseDataIsSet;
    protected transient String _errorInitVal;
    protected transient boolean _errorIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


					
    /**
     * Returns the initial value of the property 'responseData'.
     */
    public String responseDataInitVal() {
        String result;
        if (_responseDataIsSet) {
            result = _responseDataInitVal;
        } else {
            result = getResponseData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'responseData'.
     */
    public boolean responseDataIsDirty() {
        return !valuesAreEqual(responseDataInitVal(), getResponseData());
    }

    /**
     * Returns true if the setter method was called for the property 'responseData'.
     */
    public boolean responseDataIsSet() {
        return _responseDataIsSet;
    }
	
    /**
     * Returns the initial value of the property 'error'.
     */
    public String errorInitVal() {
        String result;
        if (_errorIsSet) {
            result = _errorInitVal;
        } else {
            result = getError();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'error'.
     */
    public boolean errorIsDirty() {
        return !valuesAreEqual(errorInitVal(), getError());
    }

    /**
     * Returns true if the setter method was called for the property 'error'.
     */
    public boolean errorIsSet() {
        return _errorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expirationDate'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expirationDate'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expirationDate'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creationDate'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creationDate'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creationDate'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

}
