package de.smava.webapp.ident.domain.interfaces;



import de.smava.webapp.ident.domain.IdentificationBrokerageBankResult;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankResultState;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankResultType;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'IdentificationBrokerageBankResults'.
 *
 * @author generator
 */
public interface IdentificationBrokerageBankResultEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'brokerageBank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerageBank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(IdentificationBrokerageBankResultType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    IdentificationBrokerageBankResultType getType();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(IdentificationBrokerageBankResultState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    IdentificationBrokerageBankResultState getState();
    /**
     * Setter for the property 'responseData'.
     *
     * 
     *
     */
    void setResponseData(String responseData);

    /**
     * Returns the property 'responseData'.
     *
     * 
     *
     */
    String getResponseData();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'persons'.
     *
     * 
     *
     */
    void setPersons(Set<de.smava.webapp.account.domain.Person> persons);

    /**
     * Returns the property 'persons'.
     *
     * 
     *
     */
    Set<de.smava.webapp.account.domain.Person> getPersons();
    /**
     * Helper method to get reference of this object as model type.
     */
    IdentificationBrokerageBankResult asIdentificationBrokerageBankResult();
}
