package de.smava.webapp.ident.domain;

/**
 * @author dsuszczynski
 * @since 26.02.15
 */
public enum IdentificationBrokerageBankResultType {
    VIDEO,
    POSTIDENT,
    UNKNOWN,
    PERSONAL,
    OTHER
}
