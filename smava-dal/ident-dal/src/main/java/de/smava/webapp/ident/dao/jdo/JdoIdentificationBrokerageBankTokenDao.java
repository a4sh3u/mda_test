//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ident.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank token)}

import de.smava.webapp.account.domain.Person;
import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ident.dao.IdentificationBrokerageBankTokenDao;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankToken;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'IdentificationBrokerageBankTokens'.
 *
 * @author generator
 */
@Repository(value = "identificationBrokerageBankTokenDao")
public class JdoIdentificationBrokerageBankTokenDao extends JdoBaseDao implements IdentificationBrokerageBankTokenDao {

    private static final Logger LOGGER = Logger.getLogger(JdoIdentificationBrokerageBankTokenDao.class);

    private static final String CLASS_NAME = "IdentificationBrokerageBankToken";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(identification brokerage bank token)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the identification brokerage bank token identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public IdentificationBrokerageBankToken load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        IdentificationBrokerageBankToken result = getEntity(IdentificationBrokerageBankToken.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public IdentificationBrokerageBankToken getIdentificationBrokerageBankToken(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	IdentificationBrokerageBankToken entity = findUniqueEntity(IdentificationBrokerageBankToken.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the identification brokerage bank token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(IdentificationBrokerageBankToken identificationBrokerageBankToken) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveIdentificationBrokerageBankToken: " + identificationBrokerageBankToken);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(identification brokerage bank token)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(identificationBrokerageBankToken);
    }

    /**
     * @deprecated Use {@link #save(IdentificationBrokerageBankToken) instead}
     */
    public Long saveIdentificationBrokerageBankToken(IdentificationBrokerageBankToken identificationBrokerageBankToken) {
        return save(identificationBrokerageBankToken);
    }

    /**
     * Deletes an identification brokerage bank token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification brokerage bank token
     */
    public void deleteIdentificationBrokerageBankToken(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteIdentificationBrokerageBankToken: " + id);
        }
        deleteEntity(IdentificationBrokerageBankToken.class, id);
    }

    /**
     * Retrieves all 'IdentificationBrokerageBankToken' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<IdentificationBrokerageBankToken> result = getEntities(IdentificationBrokerageBankToken.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'IdentificationBrokerageBankToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationBrokerageBankToken> result = getEntities(IdentificationBrokerageBankToken.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankToken> result = getEntities(IdentificationBrokerageBankToken.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankToken> result = getEntities(IdentificationBrokerageBankToken.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationBrokerageBankToken' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankToken> findIdentificationBrokerageBankTokenList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<IdentificationBrokerageBankToken> result = findEntities(IdentificationBrokerageBankToken.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationBrokerageBankToken' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankToken> findIdentificationBrokerageBankTokenList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<IdentificationBrokerageBankToken> result = findEntities(IdentificationBrokerageBankToken.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'IdentificationBrokerageBankToken' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankToken> findIdentificationBrokerageBankTokenList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationBrokerageBankToken> result = findEntities(IdentificationBrokerageBankToken.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankToken> findIdentificationBrokerageBankTokenList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankToken> result = findEntities(IdentificationBrokerageBankToken.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankToken> findIdentificationBrokerageBankTokenList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankToken> result = findEntities(IdentificationBrokerageBankToken.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankToken> findIdentificationBrokerageBankTokenList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankToken> result = findEntities(IdentificationBrokerageBankToken.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankToken' instances.
     */
    public long getIdentificationBrokerageBankTokenCount() {
        long result = getEntityCount(IdentificationBrokerageBankToken.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankToken' instances which match the given whereClause.
     */
    public long getIdentificationBrokerageBankTokenCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(IdentificationBrokerageBankToken.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankToken' instances which match the given whereClause together with params specified in object array.
     */
    public long getIdentificationBrokerageBankTokenCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(IdentificationBrokerageBankToken.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification brokerage bank token)}

    @Override
    public boolean isUnexpired(Person person, BrokerageBank brokerageBank, Date currentDate) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationBrokerageBankToken.class, "findUnexpiredByPersonAndBrokerageBank");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("currentDate", currentDate);
        parameters.put("personId", person.getId());
        parameters.put("brokerageBankId", brokerageBank.getId());
        List<?> result = (List<?>)query.executeWithMap(parameters);
        return !result.isEmpty();
    }

    @Override
    public IdentificationBrokerageBankToken getByTransactionNumber(String transaction, BrokerageBank brokerageBank) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationBrokerageBankToken.class, "findByTransactionNumberAndBrokerageBank");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionNumber", transaction);
        parameters.put("brokerageBankId", brokerageBank.getId());
        List<IdentificationBrokerageBankToken> result = (List<IdentificationBrokerageBankToken>)query.executeWithMap(parameters);

        IdentificationBrokerageBankToken ret = null;
        for (IdentificationBrokerageBankToken ident : result){
            if ( ret == null ){
                ret = ident;
            } else if (ret.getExpirationDate().before(ident.getExpirationDate())){
                ret = ident;
            }
        }

        return ret;
    }

    @Override
    public IdentificationBrokerageBankToken getByPerson(Person person, BrokerageBank brokerageBank) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationBrokerageBankToken.class, "findByPersonAndBrokerageBank");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("personId", person.getId());
        parameters.put("brokerageBankId", brokerageBank.getId());
        List<IdentificationBrokerageBankToken> result = (List<IdentificationBrokerageBankToken>)query.executeWithMap(parameters);

        IdentificationBrokerageBankToken ret = null;
        for (IdentificationBrokerageBankToken ident : result){
            if ( ret == null ){
                ret = ident;
            } else if (ret.getExpirationDate().before(ident.getExpirationDate())){
                ret = ident;
            }
        }

        return ret;
    }

    @Override
    public List<IdentificationBrokerageBankToken> findUnexpired(Date currentDate) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationBrokerageBankToken.class, "findUnexpired");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("currentDate", currentDate);
        List<IdentificationBrokerageBankToken> result = (List<IdentificationBrokerageBankToken>)query.executeWithMap(parameters);

        return result;
    }

    @Override
    public List<IdentificationBrokerageBankToken> findUnexpired(BrokerageBank brokerageBank, Date currentDate) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationBrokerageBankToken.class, "findUnexpiredByBrokerageBank");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("currentDate", currentDate);
        parameters.put("brokerageBankId", brokerageBank.getId());
        List<IdentificationBrokerageBankToken> result = (List<IdentificationBrokerageBankToken>)query.executeWithMap(parameters);

        return result;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
