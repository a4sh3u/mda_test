package de.smava.webapp.ident.domain.interfaces;



import de.smava.webapp.ident.domain.IdentificationGeneralToken;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'IdentificationGeneralTokens'.
 *
 * @author generator
 */
public interface IdentificationGeneralTokenEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'transactionNumber'.
     *
     * 
     *
     */
    void setTransactionNumber(String transactionNumber);

    /**
     * Returns the property 'transactionNumber'.
     *
     * 
     *
     */
    String getTransactionNumber();
    /**
     * Setter for the property 'statusCode'.
     *
     * 
     *
     */
    void setStatusCode(Integer statusCode);

    /**
     * Returns the property 'statusCode'.
     *
     * 
     *
     */
    Integer getStatusCode();
    /**
     * Setter for the property 'requestData'.
     *
     * 
     *
     */
    void setRequestData(String requestData);

    /**
     * Returns the property 'requestData'.
     *
     * 
     *
     */
    String getRequestData();
    /**
     * Setter for the property 'responseData'.
     *
     * 
     *
     */
    void setResponseData(String responseData);

    /**
     * Returns the property 'responseData'.
     *
     * 
     *
     */
    String getResponseData();
    /**
     * Setter for the property 'error'.
     *
     * 
     *
     */
    void setError(String error);

    /**
     * Returns the property 'error'.
     *
     * 
     *
     */
    String getError();
    /**
     * Setter for the property 'expirationDate'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expirationDate'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'persons'.
     *
     * 
     *
     */
    void setPersons(Set<de.smava.webapp.account.domain.Person> persons);

    /**
     * Returns the property 'persons'.
     *
     * 
     *
     */
    Set<de.smava.webapp.account.domain.Person> getPersons();
    /**
     * Setter for the property 'identificationBrokerageBankTokens'.
     *
     * 
     *
     */
    void setIdentificationBrokerageBankTokens(Set<de.smava.webapp.ident.domain.IdentificationBrokerageBankToken> identificationBrokerageBankTokens);

    /**
     * Returns the property 'identificationBrokerageBankTokens'.
     *
     * 
     *
     */
    Set<de.smava.webapp.ident.domain.IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokens();
    /**
     * Helper method to get reference of this object as model type.
     */
    IdentificationGeneralToken asIdentificationGeneralToken();
}
