package de.smava.webapp.ident.domain.interfaces;



import de.smava.webapp.ident.domain.IdentificationBrokerageBankToken;

import java.util.Date;


/**
 * The domain object that represents 'IdentificationBrokerageBankTokens'.
 *
 * @author generator
 */
public interface IdentificationBrokerageBankTokenEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'identification general token'.
     *
     * 
     *
     */
    void setIdentificationGeneralToken(de.smava.webapp.ident.domain.IdentificationGeneralToken identificationGeneralToken);

    /**
     * Returns the property 'identification general token'.
     *
     * 
     *
     */
    de.smava.webapp.ident.domain.IdentificationGeneralToken getIdentificationGeneralToken();
    /**
     * Setter for the property 'brokerageBank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerageBank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'statusCode'.
     *
     * 
     *
     */
    void setStatusCode(Integer statusCode);

    /**
     * Returns the property 'statusCode'.
     *
     * 
     *
     */
    Integer getStatusCode();
    /**
     * Setter for the property 'responseData'.
     *
     * 
     *
     */
    void setResponseData(String responseData);

    /**
     * Returns the property 'responseData'.
     *
     * 
     *
     */
    String getResponseData();
    /**
     * Setter for the property 'error'.
     *
     * 
     *
     */
    void setError(String error);

    /**
     * Returns the property 'error'.
     *
     * 
     *
     */
    String getError();
    /**
     * Setter for the property 'expirationDate'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expirationDate'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    IdentificationBrokerageBankToken asIdentificationBrokerageBankToken();
}
