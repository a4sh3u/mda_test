//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ident.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification general token)}

import de.smava.webapp.account.domain.Person;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ident.dao.IdentificationGeneralTokenDao;
import de.smava.webapp.ident.domain.IdentificationGeneralToken;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'IdentificationGeneralTokens'.
 *
 * @author generator
 */
@Repository(value = "identificationGeneralTokenDao")
public class JdoIdentificationGeneralTokenDao extends JdoBaseDao implements IdentificationGeneralTokenDao {

    private static final Logger LOGGER = Logger.getLogger(JdoIdentificationGeneralTokenDao.class);

    private static final String CLASS_NAME = "IdentificationGeneralToken";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(identification general token)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the identification general token identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public IdentificationGeneralToken load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        IdentificationGeneralToken result = getEntity(IdentificationGeneralToken.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public IdentificationGeneralToken getIdentificationGeneralToken(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	IdentificationGeneralToken entity = findUniqueEntity(IdentificationGeneralToken.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the identification general token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(IdentificationGeneralToken identificationGeneralToken) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveIdentificationGeneralToken: " + identificationGeneralToken);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(identification general token)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(identificationGeneralToken);
    }

    /**
     * @deprecated Use {@link #save(IdentificationGeneralToken) instead}
     */
    public Long saveIdentificationGeneralToken(IdentificationGeneralToken identificationGeneralToken) {
        return save(identificationGeneralToken);
    }

    /**
     * Deletes an identification general token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification general token
     */
    public void deleteIdentificationGeneralToken(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteIdentificationGeneralToken: " + id);
        }
        deleteEntity(IdentificationGeneralToken.class, id);
    }

    /**
     * Retrieves all 'IdentificationGeneralToken' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<IdentificationGeneralToken> result = getEntities(IdentificationGeneralToken.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'IdentificationGeneralToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationGeneralToken> result = getEntities(IdentificationGeneralToken.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationGeneralToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationGeneralToken> result = getEntities(IdentificationGeneralToken.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationGeneralToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationGeneralToken> result = getEntities(IdentificationGeneralToken.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationGeneralToken' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationGeneralToken> findIdentificationGeneralTokenList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<IdentificationGeneralToken> result = findEntities(IdentificationGeneralToken.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationGeneralToken' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<IdentificationGeneralToken> findIdentificationGeneralTokenList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<IdentificationGeneralToken> result = findEntities(IdentificationGeneralToken.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'IdentificationGeneralToken' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationGeneralToken> findIdentificationGeneralTokenList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationGeneralToken> result = findEntities(IdentificationGeneralToken.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationGeneralToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationGeneralToken> findIdentificationGeneralTokenList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationGeneralToken> result = findEntities(IdentificationGeneralToken.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationGeneralToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationGeneralToken> findIdentificationGeneralTokenList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationGeneralToken> result = findEntities(IdentificationGeneralToken.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationGeneralToken' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationGeneralToken> findIdentificationGeneralTokenList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationGeneralToken> result = findEntities(IdentificationGeneralToken.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationGeneralToken' instances.
     */
    public long getIdentificationGeneralTokenCount() {
        long result = getEntityCount(IdentificationGeneralToken.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationGeneralToken' instances which match the given whereClause.
     */
    public long getIdentificationGeneralTokenCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(IdentificationGeneralToken.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationGeneralToken' instances which match the given whereClause together with params specified in object array.
     */
    public long getIdentificationGeneralTokenCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(IdentificationGeneralToken.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification general token)}

    @Override
    public boolean isTransactionNumberExists(String transactionNumber) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationGeneralToken.class, "findByTransactionNumber");
        List<?> result = (List<?>)query.execute(transactionNumber);
        return !result.isEmpty();
    }

    @Override
    public boolean isUnexpired(Person person, Date currentDate) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationGeneralToken.class, "findUnexpiredByPerson");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("currentDate", currentDate);
        parameters.put("personId", person.getId());
        List<?> result = (List<?>)query.executeWithMap(parameters);
        return !result.isEmpty();
    }

    @Override
    public IdentificationGeneralToken getByTransactionNumber(String transactionNumber) {
        Query query = getPersistenceManager().newNamedQuery(IdentificationGeneralToken.class, "findByTransactionNumber");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("transactionNumber", transactionNumber);
        List<IdentificationGeneralToken> result = (List<IdentificationGeneralToken>)query.executeWithMap(parameters);

        IdentificationGeneralToken ret = null;
        for (IdentificationGeneralToken ident : result){
            if ( ret == null ){
                ret = ident;
            } else if (ret.getExpirationDate().before(ident.getExpirationDate())) {
                LOGGER.warn("There are more than one Token with TAN: '" + transactionNumber + "'!");
                ret = ident;
            }
        }

        return ret;
    }

    @Override
    public IdentificationGeneralToken findUnexpiredForPerson (Person person) {
        IdentificationGeneralToken toReturn = null;
        Query query = getPersistenceManager().newNamedQuery(IdentificationGeneralToken.class, "findUnexpiredByPerson");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("personId", person.getId());
        Date currentDate = CurrentDate.getDate();
        parameters.put("currentDate", currentDate);
        List<?> result = (List<?>)query.executeWithMap(parameters);
        if (!result.isEmpty()) {
            toReturn = (IdentificationGeneralToken) result.get(0);
        }
        return toReturn;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}
