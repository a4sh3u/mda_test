//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification general token)}
import de.smava.webapp.ident.domain.history.IdentificationGeneralTokenHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationGeneralTokens'.
 *
 * @author generator
 */
public class IdentificationGeneralToken extends IdentificationGeneralTokenHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(identification general token)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected String _transactionNumber;
        protected Integer _statusCode;
        protected String _requestData;
        protected String _responseData;
        protected String _error;
        protected Date _expirationDate;
        protected Date _creationDate;
        protected Set<de.smava.webapp.account.domain.Person> _persons;
        protected Set<de.smava.webapp.ident.domain.IdentificationBrokerageBankToken> _identificationBrokerageBankTokens;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'transactionNumber'.
     */
    public void setTransactionNumber(String transactionNumber) {
        if (!_transactionNumberIsSet) {
            _transactionNumberIsSet = true;
            _transactionNumberInitVal = getTransactionNumber();
        }
        registerChange("transactionNumber", _transactionNumberInitVal, transactionNumber);
        _transactionNumber = transactionNumber;
    }
                        
    /**
     * Returns the property 'transactionNumber'.
     */
    public String getTransactionNumber() {
        return _transactionNumber;
    }
                                            
    /**
     * Setter for the property 'statusCode'.
     */
    public void setStatusCode(Integer statusCode) {
        _statusCode = statusCode;
    }
            
    /**
     * Returns the property 'statusCode'.
     */
    public Integer getStatusCode() {
        return _statusCode;
    }
                                    /**
     * Setter for the property 'requestData'.
     */
    public void setRequestData(String requestData) {
        if (!_requestDataIsSet) {
            _requestDataIsSet = true;
            _requestDataInitVal = getRequestData();
        }
        registerChange("requestData", _requestDataInitVal, requestData);
        _requestData = requestData;
    }
                        
    /**
     * Returns the property 'requestData'.
     */
    public String getRequestData() {
        return _requestData;
    }
                                    /**
     * Setter for the property 'responseData'.
     */
    public void setResponseData(String responseData) {
        if (!_responseDataIsSet) {
            _responseDataIsSet = true;
            _responseDataInitVal = getResponseData();
        }
        registerChange("responseData", _responseDataInitVal, responseData);
        _responseData = responseData;
    }
                        
    /**
     * Returns the property 'responseData'.
     */
    public String getResponseData() {
        return _responseData;
    }
                                    /**
     * Setter for the property 'error'.
     */
    public void setError(String error) {
        if (!_errorIsSet) {
            _errorIsSet = true;
            _errorInitVal = getError();
        }
        registerChange("error", _errorInitVal, error);
        _error = error;
    }
                        
    /**
     * Returns the property 'error'.
     */
    public String getError() {
        return _error;
    }
                                    /**
     * Setter for the property 'expirationDate'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expirationDate", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expirationDate'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'creationDate'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creationDate", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creationDate'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'persons'.
     */
    public void setPersons(Set<de.smava.webapp.account.domain.Person> persons) {
        _persons = persons;
    }
            
    /**
     * Returns the property 'persons'.
     */
    public Set<de.smava.webapp.account.domain.Person> getPersons() {
        return _persons;
    }
                                            
    /**
     * Setter for the property 'identificationBrokerageBankTokens'.
     */
    public void setIdentificationBrokerageBankTokens(Set<de.smava.webapp.ident.domain.IdentificationBrokerageBankToken> identificationBrokerageBankTokens) {
        _identificationBrokerageBankTokens = identificationBrokerageBankTokens;
    }
            
    /**
     * Returns the property 'identificationBrokerageBankTokens'.
     */
    public Set<de.smava.webapp.ident.domain.IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokens() {
        return _identificationBrokerageBankTokens;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdentificationGeneralToken.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _transactionNumber=").append(_transactionNumber);
            builder.append("\n    _requestData=").append(_requestData);
            builder.append("\n    _responseData=").append(_responseData);
            builder.append("\n    _error=").append(_error);
            builder.append("\n}");
        } else {
            builder.append(IdentificationGeneralToken.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IdentificationGeneralToken asIdentificationGeneralToken() {
        return this;
    }
}
