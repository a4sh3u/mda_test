//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ident.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification general token)}
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ident.domain.IdentificationGeneralToken;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'IdentificationGeneralTokens'.
 *
 * @author generator
 */
public interface IdentificationGeneralTokenDao extends BaseDao<IdentificationGeneralToken> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the identification general token identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    IdentificationGeneralToken getIdentificationGeneralToken(Long id);

    /**
     * Saves the identification general token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveIdentificationGeneralToken(IdentificationGeneralToken identificationGeneralToken);

    /**
     * Deletes an identification general token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification general token
     */
    void deleteIdentificationGeneralToken(Long id);

    /**
     * Retrieves all 'IdentificationGeneralToken' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList();

    /**
     * Retrieves a page of 'IdentificationGeneralToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'IdentificationGeneralToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'IdentificationGeneralToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<IdentificationGeneralToken> getIdentificationGeneralTokenList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'IdentificationGeneralToken' instances.
     */
    long getIdentificationGeneralTokenCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification general token)}

    /**
     * Returns TRUE if there is required Token (no matter if it is valid/unexpired or not).
     * @param transactionNumber
     * @return
     */
    boolean isTransactionNumberExists(String transactionNumber);

    /**
     * Returns TRUE if there is valid/unexpired Token for Person.
     * @param person
     * @param currentDate
     * @return
     */
    public boolean isUnexpired(Person person, Date currentDate);

    /**
     * Gives IdentificationGeneralToken which match to criteria.
     * @param transaction
     * @return
     */
    public IdentificationGeneralToken getByTransactionNumber(String transaction);

    /**
     * Used to get general token during email url attachment
     * @param person
     * @return
     */
    public IdentificationGeneralToken findUnexpiredForPerson (Person person);
    // !!!!!!!! End of insert code section !!!!!!!!
}
