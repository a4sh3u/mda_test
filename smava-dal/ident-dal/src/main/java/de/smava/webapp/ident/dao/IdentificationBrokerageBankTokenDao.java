//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ident.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank token)}
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankToken;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'IdentificationBrokerageBankTokens'.
 *
 * @author generator
 */
public interface IdentificationBrokerageBankTokenDao extends BaseDao<IdentificationBrokerageBankToken> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the identification brokerage bank token identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    IdentificationBrokerageBankToken getIdentificationBrokerageBankToken(Long id);

    /**
     * Saves the identification brokerage bank token.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveIdentificationBrokerageBankToken(IdentificationBrokerageBankToken identificationBrokerageBankToken);

    /**
     * Deletes an identification brokerage bank token, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification brokerage bank token
     */
    void deleteIdentificationBrokerageBankToken(Long id);

    /**
     * Retrieves all 'IdentificationBrokerageBankToken' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList();

    /**
     * Retrieves a page of 'IdentificationBrokerageBankToken' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankToken' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankToken' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<IdentificationBrokerageBankToken> getIdentificationBrokerageBankTokenList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'IdentificationBrokerageBankToken' instances.
     */
    long getIdentificationBrokerageBankTokenCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification brokerage bank token)}

    /**
     * Gives IdentificationBrokerageBankToken which match to criteria.
     * @param transaction
     * @param brokerageBank
     * @return
     */
    IdentificationBrokerageBankToken getByTransactionNumber(String transaction, BrokerageBank brokerageBank);

    /**
     * Gives IdentificationBrokerageBankToken which match to criteria.
     * @param person
     * @param brokerageBank
     * @return
     */
    IdentificationBrokerageBankToken getByPerson(Person person, BrokerageBank brokerageBank);

    /**
     * Returns TRUE if there is valid/unexpired Token for Person and BrokerageBank.
     * @param person
     * @param brokerageBank
     * @param currentDate
     * @return
     */
    boolean isUnexpired(Person person, BrokerageBank brokerageBank, Date currentDate);

    /**
     * Returns List of unexpired Tokens.
     * @param currentDate
     * @return
     */
    public List<IdentificationBrokerageBankToken> findUnexpired(Date currentDate);

    /**
     * Returns List of unexpired Tokens for BrokerageBank.
     * @param brokerageBank
     * @param currentDate
     * @return
     */
    public List<IdentificationBrokerageBankToken> findUnexpired(BrokerageBank brokerageBank, Date currentDate);
    // !!!!!!!! End of insert code section !!!!!!!!
}
