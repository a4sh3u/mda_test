package de.smava.webapp.ident.domain;

/**
 * @author dsuszczynski
 * @since 26.02.15
 */
public enum IdentificationBrokerageBankResultState {
    IDENTIFIED,
    IDENTIFIED_DATA_CHANGE,
    NOT_IDENTIFIED,
    UNKNOWN
}
