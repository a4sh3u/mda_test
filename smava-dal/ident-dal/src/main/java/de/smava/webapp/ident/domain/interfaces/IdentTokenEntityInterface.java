package de.smava.webapp.ident.domain.interfaces;



import de.smava.webapp.ident.domain.IdentToken;

import java.util.Date;


/**
 * The domain object that represents 'IdentTokens'.
 *
 * @author generator
 */
public interface IdentTokenEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'person'.
     *
     * 
     *
     */
    void setPerson(de.smava.webapp.account.domain.Person person);

    /**
     * Returns the property 'person'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Person getPerson();
    /**
     * Setter for the property 'transactionNumber'.
     *
     * 
     *
     */
    void setTransactionNumber(String transactionNumber);

    /**
     * Returns the property 'transactionNumber'.
     *
     * 
     *
     */
    String getTransactionNumber();
    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    void setResponse(String response);

    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    String getResponse();
    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    void setStatus(Integer status);

    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    Integer getStatus();
    /**
     * Setter for the property 'data'.
     *
     * 
     *
     */
    void setData(String data);

    /**
     * Returns the property 'data'.
     *
     * 
     *
     */
    String getData();
    /**
     * Setter for the property 'expire date'.
     *
     * 
     *
     */
    void setExpireDate(Date expireDate);

    /**
     * Returns the property 'expire date'.
     *
     * 
     *
     */
    Date getExpireDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    IdentToken asIdentToken();
}
