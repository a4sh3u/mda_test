package de.smava.webapp.ident.domain.history;



import de.smava.webapp.ident.domain.abstracts.AbstractIdentificationBrokerageBankResult;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'IdentificationBrokerageBankResults'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IdentificationBrokerageBankResultHistory extends AbstractIdentificationBrokerageBankResult {

    protected transient String _responseDataInitVal;
    protected transient boolean _responseDataIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


					
    /**
     * Returns the initial value of the property 'responseData'.
     */
    public String responseDataInitVal() {
        String result;
        if (_responseDataIsSet) {
            result = _responseDataInitVal;
        } else {
            result = getResponseData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'responseData'.
     */
    public boolean responseDataIsDirty() {
        return !valuesAreEqual(responseDataInitVal(), getResponseData());
    }

    /**
     * Returns true if the setter method was called for the property 'responseData'.
     */
    public boolean responseDataIsSet() {
        return _responseDataIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creationDate'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creationDate'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creationDate'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
}
