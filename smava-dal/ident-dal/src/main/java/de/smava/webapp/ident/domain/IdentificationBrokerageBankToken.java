//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank token)}
import de.smava.webapp.ident.domain.history.IdentificationBrokerageBankTokenHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationBrokerageBankTokens'.
 *
 * @author generator
 */
public class IdentificationBrokerageBankToken extends IdentificationBrokerageBankTokenHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(identification brokerage bank token)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.ident.domain.IdentificationGeneralToken _identificationGeneralToken;
        protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
        protected Integer _statusCode;
        protected String _responseData;
        protected String _error;
        protected Date _expirationDate;
        protected Date _creationDate;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'identification general token'.
     */
    public void setIdentificationGeneralToken(de.smava.webapp.ident.domain.IdentificationGeneralToken identificationGeneralToken) {
        _identificationGeneralToken = identificationGeneralToken;
    }
            
    /**
     * Returns the property 'identification general token'.
     */
    public de.smava.webapp.ident.domain.IdentificationGeneralToken getIdentificationGeneralToken() {
        return _identificationGeneralToken;
    }
                                            
    /**
     * Setter for the property 'brokerageBank'.
     */
    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerageBank'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'statusCode'.
     */
    public void setStatusCode(Integer statusCode) {
        _statusCode = statusCode;
    }
            
    /**
     * Returns the property 'statusCode'.
     */
    public Integer getStatusCode() {
        return _statusCode;
    }
                                    /**
     * Setter for the property 'responseData'.
     */
    public void setResponseData(String responseData) {
        if (!_responseDataIsSet) {
            _responseDataIsSet = true;
            _responseDataInitVal = getResponseData();
        }
        registerChange("responseData", _responseDataInitVal, responseData);
        _responseData = responseData;
    }
                        
    /**
     * Returns the property 'responseData'.
     */
    public String getResponseData() {
        return _responseData;
    }
                                    /**
     * Setter for the property 'error'.
     */
    public void setError(String error) {
        if (!_errorIsSet) {
            _errorIsSet = true;
            _errorInitVal = getError();
        }
        registerChange("error", _errorInitVal, error);
        _error = error;
    }
                        
    /**
     * Returns the property 'error'.
     */
    public String getError() {
        return _error;
    }
                                    /**
     * Setter for the property 'expirationDate'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expirationDate", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expirationDate'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'creationDate'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creationDate", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creationDate'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdentificationBrokerageBankToken.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _responseData=").append(_responseData);
            builder.append("\n    _error=").append(_error);
            builder.append("\n}");
        } else {
            builder.append(IdentificationBrokerageBankToken.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IdentificationBrokerageBankToken asIdentificationBrokerageBankToken() {
        return this;
    }
}
