package de.smava.webapp.ident.domain.history;



import de.smava.webapp.ident.domain.abstracts.AbstractIdentToken;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'IdentTokens'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IdentTokenHistory extends AbstractIdentToken {

    protected transient String _transactionNumberInitVal;
    protected transient boolean _transactionNumberIsSet;
    protected transient String _responseInitVal;
    protected transient boolean _responseIsSet;
    protected transient String _dataInitVal;
    protected transient boolean _dataIsSet;
    protected transient Date _expireDateInitVal;
    protected transient boolean _expireDateIsSet;


			
    /**
     * Returns the initial value of the property 'transactionNumber'.
     */
    public String transactionNumberInitVal() {
        String result;
        if (_transactionNumberIsSet) {
            result = _transactionNumberInitVal;
        } else {
            result = getTransactionNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'transactionNumber'.
     */
    public boolean transactionNumberIsDirty() {
        return !valuesAreEqual(transactionNumberInitVal(), getTransactionNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'transactionNumber'.
     */
    public boolean transactionNumberIsSet() {
        return _transactionNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'response'.
     */
    public String responseInitVal() {
        String result;
        if (_responseIsSet) {
            result = _responseInitVal;
        } else {
            result = getResponse();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response'.
     */
    public boolean responseIsDirty() {
        return !valuesAreEqual(responseInitVal(), getResponse());
    }

    /**
     * Returns true if the setter method was called for the property 'response'.
     */
    public boolean responseIsSet() {
        return _responseIsSet;
    }
		
    /**
     * Returns the initial value of the property 'data'.
     */
    public String dataInitVal() {
        String result;
        if (_dataIsSet) {
            result = _dataInitVal;
        } else {
            result = getData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'data'.
     */
    public boolean dataIsDirty() {
        return !valuesAreEqual(dataInitVal(), getData());
    }

    /**
     * Returns true if the setter method was called for the property 'data'.
     */
    public boolean dataIsSet() {
        return _dataIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expire date'.
     */
    public Date expireDateInitVal() {
        Date result;
        if (_expireDateIsSet) {
            result = _expireDateInitVal;
        } else {
            result = getExpireDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expire date'.
     */
    public boolean expireDateIsDirty() {
        return !valuesAreEqual(expireDateInitVal(), getExpireDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expire date'.
     */
    public boolean expireDateIsSet() {
        return _expireDateIsSet;
    }

}
