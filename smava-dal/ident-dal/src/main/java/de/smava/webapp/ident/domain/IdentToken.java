//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(ident token)}
import de.smava.webapp.ident.domain.history.IdentTokenHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentTokens'.
 *
 * @author generator
 */
public class IdentToken extends IdentTokenHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(ident token)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.account.domain.Account _account;
        protected de.smava.webapp.account.domain.Person _person;
        protected String _transactionNumber;
        protected String _response;
        protected Integer _status;
        protected String _data;
        protected Date _expireDate;
        
                                    
    /**
     * Setter for the property 'account'.
     */
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     */
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'person'.
     */
    public void setPerson(de.smava.webapp.account.domain.Person person) {
        _person = person;
    }
            
    /**
     * Returns the property 'person'.
     */
    public de.smava.webapp.account.domain.Person getPerson() {
        return _person;
    }
                                    /**
     * Setter for the property 'transactionNumber'.
     */
    public void setTransactionNumber(String transactionNumber) {
        if (!_transactionNumberIsSet) {
            _transactionNumberIsSet = true;
            _transactionNumberInitVal = getTransactionNumber();
        }
        registerChange("transactionNumber", _transactionNumberInitVal, transactionNumber);
        _transactionNumber = transactionNumber;
    }
                        
    /**
     * Returns the property 'transactionNumber'.
     */
    public String getTransactionNumber() {
        return _transactionNumber;
    }
                                    /**
     * Setter for the property 'response'.
     */
    public void setResponse(String response) {
        if (!_responseIsSet) {
            _responseIsSet = true;
            _responseInitVal = getResponse();
        }
        registerChange("response", _responseInitVal, response);
        _response = response;
    }
                        
    /**
     * Returns the property 'response'.
     */
    public String getResponse() {
        return _response;
    }
                                            
    /**
     * Setter for the property 'status'.
     */
    public void setStatus(Integer status) {
        _status = status;
    }
            
    /**
     * Returns the property 'status'.
     */
    public Integer getStatus() {
        return _status;
    }
                                    /**
     * Setter for the property 'data'.
     */
    public void setData(String data) {
        if (!_dataIsSet) {
            _dataIsSet = true;
            _dataInitVal = getData();
        }
        registerChange("data", _dataInitVal, data);
        _data = data;
    }
                        
    /**
     * Returns the property 'data'.
     */
    public String getData() {
        return _data;
    }
                                    /**
     * Setter for the property 'expire date'.
     */
    public void setExpireDate(Date expireDate) {
        if (!_expireDateIsSet) {
            _expireDateIsSet = true;
            _expireDateInitVal = getExpireDate();
        }
        registerChange("expire date", _expireDateInitVal, expireDate);
        _expireDate = expireDate;
    }
                        
    /**
     * Returns the property 'expire date'.
     */
    public Date getExpireDate() {
        return _expireDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdentToken.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _transactionNumber=").append(_transactionNumber);
            builder.append("\n    _response=").append(_response);
            builder.append("\n    _data=").append(_data);
            builder.append("\n}");
        } else {
            builder.append(IdentToken.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IdentToken asIdentToken() {
        return this;
    }
}
