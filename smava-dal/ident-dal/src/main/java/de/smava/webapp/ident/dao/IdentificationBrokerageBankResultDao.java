//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ident.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result)}
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankResult;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankResultState;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankResultType;
import de.smava.webapp.ident.domain.IdentificationBrokerageBankToken;

import javax.transaction.Synchronization;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'IdentificationBrokerageBankResults'.
 *
 * @author generator
 */
public interface IdentificationBrokerageBankResultDao extends BaseDao<IdentificationBrokerageBankResult> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the identification brokerage bank result identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    IdentificationBrokerageBankResult getIdentificationBrokerageBankResult(Long id);

    /**
     * Saves the identification brokerage bank result.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveIdentificationBrokerageBankResult(IdentificationBrokerageBankResult identificationBrokerageBankResult);

    /**
     * Deletes an identification brokerage bank result, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification brokerage bank result
     */
    void deleteIdentificationBrokerageBankResult(Long id);

    /**
     * Retrieves all 'IdentificationBrokerageBankResult' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList();

    /**
     * Retrieves a page of 'IdentificationBrokerageBankResult' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankResult' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResult' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'IdentificationBrokerageBankResult' instances.
     */
    long getIdentificationBrokerageBankResultCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification brokerage bank result)}
    /**
     * Returns latest results found by {@link de.smava.webapp.ident.domain.IdentificationBrokerageBankToken}
     *
     * @param brokerageBankToken
     * @return
     */
    public IdentificationBrokerageBankResult getByIdentificationBrokerageBankToken(IdentificationBrokerageBankToken brokerageBankToken);

    /**
     * Returns latest results found by {@link de.smava.webapp.ident.domain.IdentificationBrokerageBankToken}
     * for specific state {@link de.smava.webapp.ident.domain.IdentificationBrokerageBankResultState}
     *
     * @param brokerageBankToken
     * @param resultType
     * @return
     */
    public IdentificationBrokerageBankResult getByIdentificationBrokerageBankToken(IdentificationBrokerageBankToken brokerageBankToken, IdentificationBrokerageBankResultType resultType);

    /**
     * Returns latest results found by {@link de.smava.webapp.ident.domain.IdentificationBrokerageBankToken}
     * for specific state {@link de.smava.webapp.ident.domain.IdentificationBrokerageBankResultState}
     * and type {@link de.smava.webapp.ident.domain.IdentificationBrokerageBankResultType}
     *
     * @param brokerageBankToken
     * @param resultType
     * @param resultState
     * @return
     */
    public IdentificationBrokerageBankResult getByIdentificationBrokerageBankToken(IdentificationBrokerageBankToken brokerageBankToken, IdentificationBrokerageBankResultType resultType, IdentificationBrokerageBankResultState resultState);
    // !!!!!!!! End of insert code section !!!!!!!!
}
