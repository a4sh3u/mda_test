<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE jdo PUBLIC
        "-//Sun Microsystems, Inc.//DTD Java Data Objects Metadata 2.0//EN"
        "http://java.sun.com/dtd/jdo_2_0.dtd">
<jdo schema="public">
    <package name="de.smava.webapp.ident.domain" schema="public">
        <class name="IdentToken" identity-type="application" table="ident_token" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_account"><column name="account_id" allows-null="false" /></field>
            <field name="_person"><column name="person_id" /></field>
            <field name="_status"><column name="status" /></field>
            <field name="_transactionNumber" unique="true"><column name="transaction_number" /></field>
            <field name="_data"><column name="data" jdbc-type="LONGVARCHAR" /></field>
            <field name="_expireDate"><column name="expire_date" allows-null="false" /></field>
            <field name="_response"><column name="response" jdbc-type="LONGVARCHAR" /></field>

            <query name="findByTransactionNumber" language="javax.jdo.query.JDOQL">
                <![CDATA[
  	            	select
	            	from
	            		de.smava.webapp.ident.domain.IdentToken
	            	where
	            		this._transactionNumber == tan

	            	parameters
	            		java.lang.String tan
	            ]]>
            </query>
            <query name="findValidByPerson" language="javax.jdo.query.JDOQL">
                <![CDATA[
  	            	select
	            	from
	            		de.smava.webapp.ident.domain.IdentToken
	            	where
	            		this._person._id == personId &&
	            		this._expireDate > currentDate
	            	parameters
	            		java.lang.Long personId,java.util.Date currentDate
	            ]]>
            </query>
            <query name="findUnexpiredAndUnidentified" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentToken
                    where
                        this._expireDate > currentDate &&
                        this._response == null
                    parameters
                        java.util.Date currentDate
                ]]>
            </query>
        </class>

        <class name="IdentificationGeneralToken" identity-type="application" table="identification_general_token" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_account"><column name="account_id" allows-null="false" /></field>
            <field name="_transactionNumber" unique="true"><column name="transaction_number" allows-null="false" /></field>
            <field name="_statusCode"><column name="status_code" /></field>
            <field name="_requestData"><column name="request_data" jdbc-type="LONGVARCHAR" /></field>
            <field name="_responseData"><column name="response_data" jdbc-type="LONGVARCHAR" /></field>
            <field name="_error"><column name="error" jdbc-type="LONGVARCHAR" /></field>
            <field name="_expirationDate"><column name="expiration_date" allows-null="false" /></field>
            <field name="_creationDate"><column name="creation_date" allows-null="false" /></field>

            <field name="_identificationBrokerageBankTokens" mapped-by="_identificationGeneralToken">
                <collection element-type="de.smava.webapp.ident.domain.IdentificationBrokerageBankToken"/>
            </field>
            <field name="_persons" persistence-modifier="persistent" table="identification_general_token_person">
                <collection element-type="de.smava.webapp.account.domain.Person"/>
                <join>
                    <column name="identification_general_token_id" />
                </join>
                <element>
                    <column name="person_id"/>
                </element>
            </field>

            <query name="findByTransactionNumber" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationGeneralToken
                    where
                        this._transactionNumber == transactionNumber
                    parameters
                        java.lang.String transactionNumber
                ]]>
            </query>
            <query name="findUnexpiredByPerson" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationGeneralToken
                    where
                        this._expirationDate > currentDate &&
                        person._id == personId &&
                        this._persons.contains(person)
                    variables
                        de.smava.webapp.account.domain.Person person
                    parameters
                        java.lang.Long personId, java.util.Date currentDate
                ]]>
            </query>
            <query name="findByTransactionNumber" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationGeneralToken
                    where
                        this._transactionNumber == transactionNumber
                    parameters
                        java.lang.String transactionNumber
                ]]>
            </query>
        </class>

        <class name="IdentificationBrokerageBankToken" identity-type="application" table="identification_brokerage_bank_token" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_account"><column name="account_id" allows-null="false" /></field>
            <field name="_identificationGeneralToken"><column name="identification_general_token_id" allows-null="false" /></field>
            <field name="_brokerageBank"><column name="brokerage_bank_id" allows-null="false" /></field>
            <field name="_statusCode"><column name="status_code" /></field>
            <field name="_responseData"><column name="response_data" jdbc-type="LONGVARCHAR" /></field>
            <field name="_error"><column name="error" jdbc-type="LONGVARCHAR" /></field>
            <field name="_expirationDate"><column name="expiration_date" /></field>
            <field name="_creationDate"><column name="creation_date" allows-null="false" /></field>

            <query name="findUnexpiredByPersonAndBrokerageBank" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankToken
                    where
                        this._brokerageBank._id == brokerageBankId &&
                        (this._expirationDate == null || this._expirationDate > currentDate) &&
                        person._id == personId &&
                        this._identificationGeneralToken._persons.contains(person)
                    variables
                        de.smava.webapp.account.domain.Person person
                    parameters
                        java.lang.Long brokerageBankId, java.lang.Long personId, java.util.Date currentDate
                ]]>
            </query>
            <query name="findByTransactionNumberAndBrokerageBank" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankToken
                    where
                        this._identificationGeneralToken._transactionNumber == transactionNumber &&
                        this._brokerageBank._id == brokerageBankId
                    parameters
                        java.lang.String transactionNumber, java.lang.Long brokerageBankId
                ]]>
            </query>
            <query name="findByPersonAndBrokerageBank" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankToken
                    where
                        this._brokerageBank._id == brokerageBankId &&
                        person._id == personId &&
                        this._identificationGeneralToken._persons.contains(person)
                    variables
                        de.smava.webapp.account.domain.Person person
                    parameters
                        java.lang.Long brokerageBankId, java.lang.Long personId
                ]]>
            </query>
            <query name="findUnexpired" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankToken
                    where
                        (this._expirationDate == null || this._expirationDate > currentDate)
                    parameters
                        java.util.Date currentDate
                ]]>
            </query>
            <query name="findUnexpiredByBrokerageBank" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankToken
                    where
                        this._brokerageBank._id == brokerageBankId &&
                        (this._expirationDate == null || this._expirationDate > currentDate)
                    parameters
                        java.lang.Long brokerageBankId, java.util.Date currentDate
                ]]>
            </query>
        </class>

        <class name="IdentificationBrokerageBankResult" identity-type="application" table="identification_brokerage_bank_result" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_account"><column name="account_id" allows-null="false" /></field>
            <field name="_brokerageBank"><column name="brokerage_bank_id" allows-null="false" /></field>
            <field name="_type"><column name="type" /></field>
            <field name="_state"><column name="state" /></field>
            <field name="_responseData"><column name="response_data" jdbc-type="LONGVARCHAR" /></field>
            <field name="_creationDate"><column name="creation_date" allows-null="false" /></field>

            <field name="_persons" persistence-modifier="persistent" table="identification_brokerage_bank_result_person">
                <collection element-type="de.smava.webapp.account.domain.Person"/>
                <join>
                    <column name="identification_brokerage_bank_result_id" />
                </join>
                <element>
                    <column name="person_id"/>
                </element>
            </field>

            <query name="findByIdentificationBrokerageBankToken" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankResult
                    where
                        brokerageBankToken._id == brokerageBankTokenId &&
                        this._brokerageBank == brokerageBankToken._brokerageBank &&
                        person._id == personId &&
                        this._persons.contains(person)
                    variables
                        de.smava.webapp.account.domain.Person person; de.smava.webapp.ident.domain.IdentificationBrokerageBankToken brokerageBankToken
                    parameters
                        java.lang.Long brokerageBankTokenId, java.lang.Long personId
                ]]>
            </query>
            <query name="findByIdentificationBrokerageBankTokenAndType" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankResult
                    where
                        this._type == type &&
                        brokerageBankToken._id == brokerageBankTokenId &&
                        this._brokerageBank == brokerageBankToken._brokerageBank &&
                        person._id == personId &&
                        this._persons.contains(person)
                    variables
                        de.smava.webapp.account.domain.Person person; de.smava.webapp.ident.domain.IdentificationBrokerageBankToken brokerageBankToken
                    parameters
                        java.lang.Long brokerageBankTokenId, java.lang.Long personId, java.lang.String type
                ]]>
            </query>
            <query name="findByIdentificationBrokerageBankTokenAndTypeAndState" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.ident.domain.IdentificationBrokerageBankResult
                    where
                        this._type == type &&
                        this._state == state &&
                        brokerageBankToken._id == brokerageBankTokenId &&
                        this._brokerageBank == brokerageBankToken._brokerageBank &&
                        person._id == personId &&
                        this._persons.contains(person)
                    variables
                        de.smava.webapp.account.domain.Person person; de.smava.webapp.ident.domain.IdentificationBrokerageBankToken brokerageBankToken
                    parameters
                        java.lang.Long brokerageBankTokenId, java.lang.Long personId, java.lang.String type, java.lang.String state
                ]]>
            </query>
        </class>
    </package>
</jdo>