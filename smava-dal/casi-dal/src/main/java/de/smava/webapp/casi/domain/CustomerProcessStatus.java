package de.smava.webapp.casi.domain;

import de.smava.webapp.casi.domain.interfaces.CustomerProcessStatusEntityInterface;
import de.smava.webapp.brokerage.domain.type.CustomerProcessReason;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.commons.domain.KreditPrivatEntity;
import org.apache.log4j.Logger;

import java.util.Date;


public class CustomerProcessStatus extends KreditPrivatEntity implements CustomerProcessStatusEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(CustomerProcessStatus.class);

    protected Long _accountId;
    protected Long _accountSmavaId;
    protected CustomerProcessState _state;
    protected CustomerProcessReason _reason;
    protected String _createdBy;
    protected Date _creationDate;
    protected Date _expirationDate;


    public Long getAccountSmavaId() {
        return _accountSmavaId;
    }

    public void setAccountSmavaId(Long accountSmavaId) {
        this._accountSmavaId = accountSmavaId;
    }

    public void setState(CustomerProcessState state) {
        _state = state;
    }
            
    public CustomerProcessState getState() {
        return _state;
    }
                                            
    public void setReason(CustomerProcessReason reason) {
        _reason = reason;
    }
            
    public CustomerProcessReason getReason() {
        return _reason;
    }

    public void setCreatedBy(String createdBy) {
        _createdBy = createdBy;
    }
                        
    public String getCreatedBy() {
        return _createdBy;
    }

    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }
                        
    public Date getCreationDate() {
        return _creationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }
                        
    public Date getExpirationDate() {
        return _expirationDate;
    }
            
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CustomerProcessStatus.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _createdBy=").append(_createdBy);
            builder.append("\n}");
        } else {
            builder.append(CustomerProcessStatus.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public CustomerProcessStatus asCustomerProcessStatus() {
        return this;
    }

    public Long getAccountId() {
        return _accountId;
    }

    public void setAccountId(Long _accountId) {
        this._accountId = _accountId;
    }
}
