package de.smava.webapp.casi.domain.interfaces;



import de.smava.webapp.casi.domain.BorrowerSearch;
import de.smava.webapp.casi.domain.CustomerProcessStatus;


/**
 * The domain object that represents 'BorrowerSearchs'.
 *
 * @author generator
 */
public interface BorrowerSearchEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication();
    /**
     * Setter for the property 'customer process status'.
     *
     * 
     *
     */
    void setCustomerProcessStatus(CustomerProcessStatus customerProcessStatus);

    /**
     * Returns the property 'customer process status'.
     *
     * 
     *
     */
    CustomerProcessStatus getCustomerProcessStatus();
    /**
     * Helper method to get reference of this object as model type.
     */
    BorrowerSearch asBorrowerSearch();
}
