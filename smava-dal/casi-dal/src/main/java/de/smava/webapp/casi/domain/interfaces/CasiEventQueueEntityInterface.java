package de.smava.webapp.casi.domain.interfaces;



import de.smava.webapp.brokerage.domain.CasiEventQueue;
import de.smava.webapp.brokerage.domain.type.CustomerProcessReason;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;

import java.util.Date;


/**
 * The domain object that represents 'CasiEventQueues'.
 *
 * @author generator
 */
public interface CasiEventQueueEntityInterface {

    /**
     * Setter for the property 'event queue creation date'.
     *
     * 
     *
     */
    void setEventQueueCreationDate(Date eventQueueCreationDate);

    /**
     * Returns the property 'event queue creation date'.
     *
     * 
     *
     */
    Date getEventQueueCreationDate();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.account.domain.Account getAccount();
    /**
     * Setter for the property 'sale affiliate info'.
     *
     * 
     *
     */
    void setSaleAffiliateInfo(de.smava.webapp.marketing.affiliate.domain.AffiliateInformation saleAffiliateInfo);

    /**
     * Returns the property 'sale affiliate info'.
     *
     * 
     *
     */
    de.smava.webapp.marketing.affiliate.domain.AffiliateInformation getSaleAffiliateInfo();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication();
    /**
     * Setter for the property 'event queue type'.
     *
     * 
     *
     */
    void setEventQueueType(String eventQueueType);

    /**
     * Returns the property 'event queue type'.
     *
     * 
     *
     */
    String getEventQueueType();
    /**
     * Setter for the property 'event queue state'.
     *
     * 
     *
     */
    void setEventQueueState(String eventQueueState);

    /**
     * Returns the property 'event queue state'.
     *
     * 
     *
     */
    String getEventQueueState();
    /**
     * Setter for the property 'event queue due date'.
     *
     * 
     *
     */
    void setEventQueueDueDate(Date eventQueueDueDate);

    /**
     * Returns the property 'event queue due date'.
     *
     * 
     *
     */
    Date getEventQueueDueDate();
    /**
     * Setter for the property 'event queue expiration date'.
     *
     * 
     *
     */
    void setEventQueueExpirationDate(Date eventQueueExpirationDate);

    /**
     * Returns the property 'event queue expiration date'.
     *
     * 
     *
     */
    Date getEventQueueExpirationDate();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'has documents'.
     *
     * 
     *
     */
    void setHasDocuments(Integer hasDocuments);

    /**
     * Returns the property 'has documents'.
     *
     * 
     *
     */
    Integer getHasDocuments();
    /**
     * Setter for the property 'event queue is schufa positive'.
     *
     * 
     *
     */
    void setEventQueueIsSchufaPositive(boolean eventQueueIsSchufaPositive);

    /**
     * Returns the property 'event queue is schufa positive'.
     *
     * 
     *
     */
    boolean getEventQueueIsSchufaPositive();
    /**
     * Setter for the property 'customer status id'.
     *
     * 
     *
     */
    void setCustomerStatusId(Long customerStatusId);

    /**
     * Returns the property 'customer status id'.
     *
     * 
     *
     */
    Long getCustomerStatusId();
    /**
     * Setter for the property 'customer status creation date'.
     *
     * 
     *
     */
    void setCustomerStatusCreationDate(Date customerStatusCreationDate);

    /**
     * Returns the property 'customer status creation date'.
     *
     * 
     *
     */
    Date getCustomerStatusCreationDate();
    /**
     * Setter for the property 'customer status state'.
     *
     * 
     *
     */
    void setCustomerStatusState(CustomerProcessState customerStatusState);

    /**
     * Returns the property 'customer status state'.
     *
     * 
     *
     */
    CustomerProcessState getCustomerStatusState();
    /**
     * Setter for the property 'customer status reason'.
     *
     * 
     *
     */
    void setCustomerStatusReason(CustomerProcessReason customerStatusReason);

    /**
     * Returns the property 'customer status reason'.
     *
     * 
     *
     */
    CustomerProcessReason getCustomerStatusReason();
    /**
     * Setter for the property 'customer status created by'.
     *
     * 
     *
     */
    void setCustomerStatusCreatedBy(String customerStatusCreatedBy);

    /**
     * Returns the property 'customer status created by'.
     *
     * 
     *
     */
    String getCustomerStatusCreatedBy();
    /**
     * Setter for the property 'customer value'.
     *
     * 
     *
     */
    void setCustomerValue(Double customerValue);

    /**
     * Returns the property 'customer value'.
     *
     * 
     *
     */
    Double getCustomerValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    CasiEventQueue asCasiEventQueue();
}
