package de.smava.webapp.casi.domain.history;



import de.smava.webapp.casi.domain.abstracts.AbstractBorrowerSearch;




/**
 * The domain object that has all history aggregation related fields for 'BorrowerSearchs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BorrowerSearchHistory extends AbstractBorrowerSearch {



			
}
