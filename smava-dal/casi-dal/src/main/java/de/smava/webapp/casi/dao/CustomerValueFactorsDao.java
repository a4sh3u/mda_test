//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.casi.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer value factors)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.casi.domain.CustomerValueFactors;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'CustomerValueFactorss'.
 *
 * @author generator
 */
public interface CustomerValueFactorsDao extends BaseDao<CustomerValueFactors> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the customer value factors identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    CustomerValueFactors getCustomerValueFactors(Long id);

    /**
     * Saves the customer value factors.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveCustomerValueFactors(CustomerValueFactors customerValueFactors);

    /**
     * Deletes an customer value factors, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the customer value factors
     */
    void deleteCustomerValueFactors(Long id);

    /**
     * Retrieves all 'CustomerValueFactors' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<CustomerValueFactors> getCustomerValueFactorsList();

    /**
     * Retrieves a page of 'CustomerValueFactors' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<CustomerValueFactors> getCustomerValueFactorsList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'CustomerValueFactors' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<CustomerValueFactors> getCustomerValueFactorsList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'CustomerValueFactors' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<CustomerValueFactors> getCustomerValueFactorsList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'CustomerValueFactors' instances.
     */
    long getCustomerValueFactorsCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(customer value factors)}
    //
    CustomerValueFactors findByLoanApplication(LoanApplication loanApplication);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
