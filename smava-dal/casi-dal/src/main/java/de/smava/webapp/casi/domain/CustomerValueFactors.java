//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.casi.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer value factors)}
import de.smava.webapp.casi.domain.history.CustomerValueFactorsHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CustomerValueFactorss'.
 *
 * 
 *
 * @author generator
 */
public class CustomerValueFactors extends CustomerValueFactorsHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(customer value factors)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.brokerage.domain.LoanApplication _loanApplication;
        protected String _loanApplicationFactors;
        protected String _eventQueueFactors;
        protected Date _lastUpdate;
        
                                    
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                    /**
     * Setter for the property 'loan application factors'.
     *
     * 
     *
     */
    public void setLoanApplicationFactors(String loanApplicationFactors) {
        if (!_loanApplicationFactorsIsSet) {
            _loanApplicationFactorsIsSet = true;
            _loanApplicationFactorsInitVal = getLoanApplicationFactors();
        }
        registerChange("loan application factors", _loanApplicationFactorsInitVal, loanApplicationFactors);
        _loanApplicationFactors = loanApplicationFactors;
    }
                        
    /**
     * Returns the property 'loan application factors'.
     *
     * 
     *
     */
    public String getLoanApplicationFactors() {
        return _loanApplicationFactors;
    }
                                    /**
     * Setter for the property 'event queue factors'.
     *
     * 
     *
     */
    public void setEventQueueFactors(String eventQueueFactors) {
        if (!_eventQueueFactorsIsSet) {
            _eventQueueFactorsIsSet = true;
            _eventQueueFactorsInitVal = getEventQueueFactors();
        }
        registerChange("event queue factors", _eventQueueFactorsInitVal, eventQueueFactors);
        _eventQueueFactors = eventQueueFactors;
    }
                        
    /**
     * Returns the property 'event queue factors'.
     *
     * 
     *
     */
    public String getEventQueueFactors() {
        return _eventQueueFactors;
    }
                                    /**
     * Setter for the property 'last update'.
     *
     * 
     *
     */
    public void setLastUpdate(Date lastUpdate) {
        if (!_lastUpdateIsSet) {
            _lastUpdateIsSet = true;
            _lastUpdateInitVal = getLastUpdate();
        }
        registerChange("last update", _lastUpdateInitVal, lastUpdate);
        _lastUpdate = lastUpdate;
    }
                        
    /**
     * Returns the property 'last update'.
     *
     * 
     *
     */
    public Date getLastUpdate() {
        return _lastUpdate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CustomerValueFactors.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _loanApplicationFactors=").append(_loanApplicationFactors);
            builder.append("\n    _eventQueueFactors=").append(_eventQueueFactors);
            builder.append("\n}");
        } else {
            builder.append(CustomerValueFactors.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CustomerValueFactors asCustomerValueFactors() {
        return this;
    }
}
