package de.smava.webapp.casi.dao;

import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.casi.domain.CustomerProcessStatus;
import de.smava.webapp.commons.dao.BaseDao;

import javax.transaction.Synchronization;
import java.util.Collection;

/**
 * DAO interface for the domain object 'CustomerProcessStatuses'.
 */
public interface CustomerProcessStatusDao extends BaseDao<CustomerProcessStatus> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    Collection<CustomerProcessStatus> findNotExpiredBySmavaId(Long smavaId);

    Collection<CustomerProcessStatus> findBySmavaId(Long smavaId);

    CustomerProcessStatus findLatestBySmavaId(Long smavaId);

    CustomerProcessState findStateBeforeCurrentStatusBySmavaId(Long currentStatusId, Long smavaId);

    void saveAndFlush(CustomerProcessStatus status);

}
