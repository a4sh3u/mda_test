package de.smava.webapp.casi.domain.interfaces;


import de.smava.webapp.casi.domain.CustomerProcessStatus;
import de.smava.webapp.brokerage.domain.type.CustomerProcessReason;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;

import java.util.Date;


public interface CustomerProcessStatusEntityInterface {


    void setAccountSmavaId(Long accountSmavaId);

    Long getAccountSmavaId();

    void setState(CustomerProcessState state);

    CustomerProcessState getState();

    void setReason(CustomerProcessReason reason);

    CustomerProcessReason getReason();

    void setCreatedBy(String createdBy);

    String getCreatedBy();

    void setCreationDate(Date creationDate);

    Date getCreationDate();

    void setExpirationDate(Date expirationDate);

    Date getExpirationDate();

    CustomerProcessStatus asCustomerProcessStatus();
}
