package de.smava.webapp.casi.domain.history;



import de.smava.webapp.casi.domain.abstracts.AbstractCustomerValueFactors;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CustomerValueFactorss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CustomerValueFactorsHistory extends AbstractCustomerValueFactors {

    protected transient String _loanApplicationFactorsInitVal;
    protected transient boolean _loanApplicationFactorsIsSet;
    protected transient String _eventQueueFactorsInitVal;
    protected transient boolean _eventQueueFactorsIsSet;
    protected transient Date _lastUpdateInitVal;
    protected transient boolean _lastUpdateIsSet;


		
    /**
     * Returns the initial value of the property 'loan application factors'.
     */
    public String loanApplicationFactorsInitVal() {
        String result;
        if (_loanApplicationFactorsIsSet) {
            result = _loanApplicationFactorsInitVal;
        } else {
            result = getLoanApplicationFactors();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'loan application factors'.
     */
    public boolean loanApplicationFactorsIsDirty() {
        return !valuesAreEqual(loanApplicationFactorsInitVal(), getLoanApplicationFactors());
    }

    /**
     * Returns true if the setter method was called for the property 'loan application factors'.
     */
    public boolean loanApplicationFactorsIsSet() {
        return _loanApplicationFactorsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'event queue factors'.
     */
    public String eventQueueFactorsInitVal() {
        String result;
        if (_eventQueueFactorsIsSet) {
            result = _eventQueueFactorsInitVal;
        } else {
            result = getEventQueueFactors();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'event queue factors'.
     */
    public boolean eventQueueFactorsIsDirty() {
        return !valuesAreEqual(eventQueueFactorsInitVal(), getEventQueueFactors());
    }

    /**
     * Returns true if the setter method was called for the property 'event queue factors'.
     */
    public boolean eventQueueFactorsIsSet() {
        return _eventQueueFactorsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last update'.
     */
    public Date lastUpdateInitVal() {
        Date result;
        if (_lastUpdateIsSet) {
            result = _lastUpdateInitVal;
        } else {
            result = getLastUpdate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last update'.
     */
    public boolean lastUpdateIsDirty() {
        return !valuesAreEqual(lastUpdateInitVal(), getLastUpdate());
    }

    /**
     * Returns true if the setter method was called for the property 'last update'.
     */
    public boolean lastUpdateIsSet() {
        return _lastUpdateIsSet;
    }

}
