package de.smava.webapp.casi.dao.jdo;

import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.casi.dao.CustomerProcessStatusDao;
import de.smava.webapp.casi.domain.CustomerProcessStatus;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'CustomerProcessStatuses'.
 */
@Repository(value = "customerProcessStatusDao")
public class JdoCustomerProcessStatusDao extends JdoBaseDao implements CustomerProcessStatusDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCustomerProcessStatusDao.class);

    private static final String CLASS_NAME = "CustomerProcessStatus";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    /**
     * Returns an attached copy of the customer process status identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public CustomerProcessStatus load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        CustomerProcessStatus result = getEntity(CustomerProcessStatus.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        return exists(CustomerProcessStatus.class, id);
    }

    /**
     * Saves the customer process status.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persistence manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(CustomerProcessStatus customerProcessStatus) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCustomerProcessStatus: " + customerProcessStatus);
        }
        return saveEntity(customerProcessStatus);
    }

    @SuppressWarnings("unchecked")
    public Collection<CustomerProcessStatus> findNotExpiredBySmavaId(Long smavaId) {
        Query query = getPersistenceManager().newQuery(CustomerProcessStatus.class);
        query.setFilter("this._accountSmavaId == :smavaId && this._expirationDate == null");
        Collection<CustomerProcessStatus> results = (Collection<CustomerProcessStatus>) query.execute(smavaId);
        if (results != null && results.size() > 1) {
            LOGGER.warn("For borrower with smavaId=" + smavaId + " exist " + results.size()
                    + " active customer process statuses. Normally should be only one.");
        }
        return results;
    }

    @Override
    public Collection<CustomerProcessStatus> findBySmavaId(Long smavaId) {

        String where = OqlTerm.newTerm().equals("_accountSmavaId", smavaId).toString();
        return findEntities(CustomerProcessStatus.class, where, SORTABLE);
    }

    @Override
    public CustomerProcessStatus findLatestBySmavaId(Long smavaId) {
        List<OqlTerm> terms = new ArrayList<OqlTerm>();
        terms.add(OqlTerm.newTerm().equals("_accountSmavaId", smavaId));
        terms.add(OqlTerm.newTerm().isNull("_expirationDate"));
        OqlTerm term = OqlTerm.newTerm().and(terms.toArray(new OqlTerm[terms.size()]));

        Collection<CustomerProcessStatus> result = findEntities(CustomerProcessStatus.class, term.toString(), SORTABLE);
        if (result!=null && result.size()>0){
            return result.iterator().next();
        }
        return null;
    }

    @Override
    public void saveAndFlush(CustomerProcessStatus status) {
        save(status);
        forceFlush();
    }

    @Override
    public CustomerProcessState findStateBeforeCurrentStatusBySmavaId(Long currentStatusId, Long smavaId) {
        if (currentStatusId == null) {
            throw new IllegalArgumentException("Current status id is null");
        }

        if (smavaId == null) {
            throw new IllegalArgumentException("Current smava id is null");
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("smavaId", smavaId);
        params.put("currentStatusId", currentStatusId);
        String select =
                "SELECT tmp.previousState" +
                "  from (SELECT" +
                "               id AS selectedId," +
                "               LAG(state) OVER (ORDER BY creation_date) AS previousState" +
                "          FROM public.customer_process_status cps WHERE cps.account_smava_id = :smavaId) tmp" +
                " WHERE tmp.selectedId = :currentStatusId" +
                " LIMIT 1";
        Query query = getPersistenceManager().newQuery(Query.SQL, select);
        query.setResultClass(String.class);
        query.setUnique(true);
        String state = (String) query.executeWithMap(params);
        return state != null ? CustomerProcessState.valueOf(state.toUpperCase()) : null;

    }

    private static Sortable SORTABLE = new Sortable() {
        @Override
        public String getSort() {
            return "_creationDate";
        }
        @Override
        public boolean hasSort() {
            return true;
        }
        @Override
        public boolean sortDescending() {
            return true;
        }
    };
}
