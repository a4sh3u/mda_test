package de.smava.webapp.casi.domain.interfaces;



import de.smava.webapp.casi.domain.CustomerValueFactors;

import java.util.Date;


/**
 * The domain object that represents 'CustomerValueFactorss'.
 *
 * @author generator
 */
public interface CustomerValueFactorsEntityInterface {

    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication();
    /**
     * Setter for the property 'loan application factors'.
     *
     * 
     *
     */
    void setLoanApplicationFactors(String loanApplicationFactors);

    /**
     * Returns the property 'loan application factors'.
     *
     * 
     *
     */
    String getLoanApplicationFactors();
    /**
     * Setter for the property 'event queue factors'.
     *
     * 
     *
     */
    void setEventQueueFactors(String eventQueueFactors);

    /**
     * Returns the property 'event queue factors'.
     *
     * 
     *
     */
    String getEventQueueFactors();
    /**
     * Setter for the property 'last update'.
     *
     * 
     *
     */
    void setLastUpdate(Date lastUpdate);

    /**
     * Returns the property 'last update'.
     *
     * 
     *
     */
    Date getLastUpdate();
    /**
     * Helper method to get reference of this object as model type.
     */
    CustomerValueFactors asCustomerValueFactors();
}
