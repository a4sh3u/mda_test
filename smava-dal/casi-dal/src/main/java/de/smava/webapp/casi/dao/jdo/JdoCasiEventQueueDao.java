package de.smava.webapp.casi.dao.jdo;

import de.smava.webapp.account.dao.ConfigValueDao;
import de.smava.webapp.account.domain.ConfigValue;
import de.smava.webapp.brokerage.domain.*;
import de.smava.webapp.casi.dao.CasiEventQueueDao;
import de.smava.webapp.brokerage.domain.CasiEventQueue;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'CasiEventQueues'.
 */
@Repository(value = "casiEventQueueDao")
public class JdoCasiEventQueueDao extends JdoBaseDao implements CasiEventQueueDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCasiEventQueueDao.class);

    private static final String CLASS_NAME = "CasiEventQueue";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String TIME_FACTOR_TIMEGAP6_KEY = "casi.leadAssignment.timeFactor.timegap6";
    private static final String RELEVANCE_FACTOR_QUALIFIED_LEAD_KEY = "casi.leadAssignment.relevanceFactor.qualifiedLead";
    private static final String RELEVANCE_FACTOR_LEAD_NO_OFFER_KEY = "casi.leadAssignment.relevanceFactor.leadNoOffer";
    private static final String RELEVANCE_FACTOR_LEAD_KDF_KEY = "casi.leadAssignment.relevanceFactor.leadKdf";
    private static final String RELEVANCE_FACTOR_SHORT_LEAD_KEY = "casi.leadAssignment.relevanceFactor.shortLead";
    private static final String RELEVANCE_FACTOR_LEAD_SCHUFA_NEGATIVE_KEY = "casi.leadAssignment.relevanceFactor.leadSchufaNegative";
    private static final String RELEVANCE_FACTOR_SPECIAL_FACTOR_KEY = "casi.leadAssignment.relevanceFactor.specialFactor";
    private static final String TIME_FACTOR_TIMEGAP0_KEY = "casi.leadAssignment.timeFactor.timegap0";
    private static final String TIME_FACTOR_TIMEVALUE0_KEY = "casi.leadAssignment.timeFactor.timevalue0";
    private static final String TIME_FACTOR_TIMEVALUE_REMAINING_KEY = "casi.leadAssignment.timeFactor.timevalueRemaining";

    @Autowired
    private ConfigValueDao configValueDao;

    /**
     * Returns an attached copy of the casi event queue identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public CasiEventQueue load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        CasiEventQueue result = getEntity(CasiEventQueue.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            CasiEventQueue entity = findUniqueEntity(CasiEventQueue.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    /**
     * Saves the casi event queue.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(CasiEventQueue casiEventQueue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCasiEventQueue: " + casiEventQueue);
        }
        return saveEntity(casiEventQueue);
    }

    private StringBuffer generateBaseQuery(Collection<EventQueueType> types, Collection<EventQueueState> states,
                                           Long personalAdvisorId, boolean assigned,
                                           CustomerProcessState customerProcessState, Map<String, String> configFactors,
                                           Date now, BrokerageState loanAppState) {

        StringBuffer sql = new StringBuffer();
        sql.append("select id from (");

        sql.append("select ceq.id as id, ");

        sql.append("calculate_customer_value_timefactor(");
        sql.append("calculate_customer_value_relevancefactor_verivox(");
        sql.append("old_la.customer_value, ");
        sql.append("new_la.id, ");
        sql.append(configFactors.get(RELEVANCE_FACTOR_QUALIFIED_LEAD_KEY)).append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_LEAD_NO_OFFER_KEY)).append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_LEAD_KDF_KEY)) .append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_SHORT_LEAD_KEY)).append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_LEAD_SCHUFA_NEGATIVE_KEY)).append(",");
        sql.append("ceq.event_queue_is_schufa_positive").append(",");
        sql.append(configFactors.get(RELEVANCE_FACTOR_SPECIAL_FACTOR_KEY)).append(",");
        sql.append("ai.external_affiliate_id");
        sql.append("), ");
        sql.append("old_la.creation_date, ");
        sql.append("TIMESTAMPTZ '");
        sql.append(new java.sql.Timestamp(now.getTime())).append("',");
        sql.append("old_la.state, ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEGAP0_KEY)).append(", ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEGAP6_KEY)).append(", ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEVALUE0_KEY)).append(", ");
        sql.append(configFactors.get(TIME_FACTOR_TIMEVALUE_REMAINING_KEY));
        sql.append(") as customer_value_with_factors ");

        sql.append("from ");
        sql.append("casi_event_queue ceq ");
        sql.append("inner join account a ON ceq.account_id = a.id ");
        sql.append("inner join loan_application old_la on ceq.loan_application_id = old_la.id ");
        sql.append("left join brokerage_entity_map map on old_la.id = map.kredit_privat_id ");
        sql.append("left join brokerage.loan_application new_la on map.brokerage_id=new_la.id ");
        sql.append("inner join public.affiliate_information ai on ceq.sale_affiliate_info_id = ai.id ");

        sql.append("where ceq.customer_value > 0 and ");

        sql.append(getAdvisorTermSQL(personalAdvisorId, assigned));

        sql.append("and ceq.event_queue_due_date < TIMESTAMPTZ '");
        sql.append(new java.sql.Timestamp(now.getTime()));
        sql.append("' ");

        sql.append("and ceq.event_queue_expiration_date > TIMESTAMPTZ '");
        sql.append(new java.sql.Timestamp(now.getTime()));
        sql.append("' ");

        if (states != null && states.size() > 0) {
            sql.append(getStatesTermSQL(states));
        }

        if (types != null) {
            sql.append(getTypesTermSQL(types));
        }

        if (loanAppState != null) {
            sql.append("and old_la.state = '")
                    .append(loanAppState.name())
                    .append("' ");
        }

        sql.append((customerProcessState != null) ? " AND ceq.customer_status_state='" + customerProcessState.name() + "' " : "");

        return sql;
    }

    private Map<String, String> prepareConfigFactors() {
        List<String> keysToFetch = new ArrayList<String>();
        keysToFetch.add(RELEVANCE_FACTOR_QUALIFIED_LEAD_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_LEAD_NO_OFFER_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_LEAD_KDF_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_SHORT_LEAD_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_LEAD_SCHUFA_NEGATIVE_KEY);
        keysToFetch.add(RELEVANCE_FACTOR_SPECIAL_FACTOR_KEY);

        keysToFetch.add(TIME_FACTOR_TIMEGAP0_KEY);
        keysToFetch.add(TIME_FACTOR_TIMEGAP6_KEY);
        keysToFetch.add(TIME_FACTOR_TIMEVALUE0_KEY);
        keysToFetch.add(TIME_FACTOR_TIMEVALUE_REMAINING_KEY);

        Map<String, String> configFactors = new HashMap<String, String>();
        for (String key : keysToFetch) {
            ConfigValue configValue = configValueDao.getConfigValueByKey(key);
            if (configValue == null) {
                throw new IllegalStateException("Can't fetch config value for key " + key);
            }
            configFactors.put(key, configValue.getValue());
        }

        return configFactors;
    }

    private StringBuffer getAdvisorTermSQL(Long advisorId, boolean assigned) {
        StringBuffer sql = new StringBuffer();

        if (advisorId != null) {
            sql.append("ceq.account_id = a.id and a.personal_advisor = '");
            sql.append(advisorId);
            sql.append("' ");
        } else {
            if (assigned) {
                sql.append("ceq.account_id = a.id and a.personal_advisor is not null ");
            } else {
                sql.append("ceq.account_id = a.id and a.personal_advisor is null ");
            }
        }

        return sql;
    }

    private StringBuffer getTypesTermSQL(Collection<EventQueueType> types) {
        StringBuffer sql = new StringBuffer();

        if (types != null && types.size() != 0) {
            if (types.size() > 1) {
                sql.append("and (");
                boolean started = false;
                for (EventQueueType type : types) {
                    if (started) {
                        sql.append("or ");
                    }
                    started = true;

                    sql.append("ceq.event_queue_type = '");
                    sql.append(type);
                    sql.append("' ");
                }
                sql.append(") ");
            } else {
                sql.append("and ceq.event_queue_type = '");
                sql.append(types.iterator().next());
                sql.append("' ");
            }
        } else {
            sql.append("and ceq.event_queue_type is not null ");
        }

        return sql;
    }

    private StringBuffer getStatesTermSQL(Collection<EventQueueState> states) {
        StringBuffer sql = new StringBuffer();

        if (states != null && states.size() != 0) {
            if (states.size() > 1) {
                sql.append("and (");
                boolean started = false;
                for (EventQueueState state : states) {
                    if (started) {
                        sql.append("or ");
                    }
                    started = true;

                    sql.append("ceq.event_queue_state = ");
                    sql.append(state);
                    sql.append(" ");
                }
                sql.append(") ");
            } else {
                sql.append("and ceq.event_queue_state = '");
                sql.append(states.iterator().next());
                sql.append("' ");
            }
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }

        return sql;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Long> findCurrentEventQueueIds(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset, Integer limit, String sortBy, Boolean descending, String alternativeSortBy, Boolean alternativeDescending, boolean assigned, CustomerProcessState customerProcessState, BrokerageState loanAppState) {
        Map<String, String> configFactors = prepareConfigFactors();
        Date now = CurrentDate.getDate();

        StringBuffer sql = generateBaseQuery(types, states, personalAdvisor, assigned, customerProcessState, configFactors, now, loanAppState);
        appendOrderingAndPagination(sql, offset, limit, sortBy, descending, alternativeSortBy, alternativeDescending);

        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        return  (Collection<Long>) query.execute();
    }

    private void appendOrderingAndPagination(StringBuffer sql, Integer offset, Integer limit, String sortBy, Boolean descending, String alternativeSortBy, Boolean alternativeDescending) {
        sql.append(" order by ");
        sql.append(sortBy);
        sql.append(" ");

        if (descending) {
            sql.append("DESC ");
        } else {
            sql.append("ASC ");
        }
        sql.append("NULLS LAST ");

        sql.append(", ");
        sql.append(alternativeSortBy);
        sql.append(" ");

        if (alternativeDescending) {
            sql.append("DESC ");
        } else {
            sql.append("ASC ");
        }
        sql.append("NULLS LAST ");

        sql.append(") AS casieq where customer_value_with_factors > 0");

        if (limit != null) {
            sql.append("limit ");
            sql.append(limit.intValue());
        }
        if (offset != null) {
            sql.append(" offset ");
            sql.append(offset.intValue());
        }
    }

}
