//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.casi.domain;

import de.smava.webapp.casi.domain.history.BorrowerSearchHistory;


public class BorrowerSearch extends BorrowerSearchHistory  {

    protected de.smava.webapp.account.domain.Account _account;
    protected de.smava.webapp.brokerage.domain.LoanApplication _loanApplication;
    protected CustomerProcessStatus _customerProcessStatus;
        
                                    
    public void setAccount(de.smava.webapp.account.domain.Account account) {
        _account = account;
    }
            
    public de.smava.webapp.account.domain.Account getAccount() {
        return _account;
    }
                                            
    public void setLoanApplication(de.smava.webapp.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    public de.smava.webapp.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                            
    public void setCustomerProcessStatus(CustomerProcessStatus customerProcessStatus) {
        _customerProcessStatus = customerProcessStatus;
    }
            
    public CustomerProcessStatus getCustomerProcessStatus() {
        return _customerProcessStatus;
    }
            
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BorrowerSearch.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BorrowerSearch.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    public BorrowerSearch asBorrowerSearch() {
        return this;
    }
}
