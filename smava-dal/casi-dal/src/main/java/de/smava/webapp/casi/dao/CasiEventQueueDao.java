package de.smava.webapp.casi.dao;

import de.smava.webapp.brokerage.domain.*;
import de.smava.webapp.brokerage.domain.CasiEventQueue;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.commons.dao.BaseDao;

import java.util.Collection;

/**
 * DAO interface for the domain object 'CasiEventQueues'.
 */
public interface CasiEventQueueDao extends BaseDao<CasiEventQueue> {

    Collection<Long> findCurrentEventQueueIds(Collection<EventQueueType> types, Collection<EventQueueState> states, Long personalAdvisor, Integer offset, Integer limit, final String sortBy, final Boolean descending, final String alternativeSortBy, final Boolean alternativeDescending, boolean assigned, CustomerProcessState customerProcessState, BrokerageState loanAppState);

}
