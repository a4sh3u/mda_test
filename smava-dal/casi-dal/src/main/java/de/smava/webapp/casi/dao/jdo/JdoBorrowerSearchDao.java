//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.casi.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(borrower search)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.casi.dao.BorrowerSearchDao;
import de.smava.webapp.casi.domain.BorrowerSearch;

import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;


import java.util.*;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BorrowerSearchs'.
 *
 * @author generator
 */
@Repository(value = "borrowerSearchDao")
public class JdoBorrowerSearchDao extends JdoBaseDao implements BorrowerSearchDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBorrowerSearchDao.class);

    private static final String CLASS_NAME = "BorrowerSearch";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(borrower search)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the borrower search identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BorrowerSearch load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BorrowerSearch result = getEntity(BorrowerSearch.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BorrowerSearch getBorrowerSearch(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BorrowerSearch entity = findUniqueEntity(BorrowerSearch.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the borrower search.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BorrowerSearch borrowerSearch) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBorrowerSearch: " + borrowerSearch);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(borrower search)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(borrowerSearch);
    }

    /**
     * @deprecated Use {@link #save(BorrowerSearch) instead}
     */
    public Long saveBorrowerSearch(BorrowerSearch borrowerSearch) {
        return save(borrowerSearch);
    }

    /**
     * Deletes an borrower search, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the borrower search
     */
    public void deleteBorrowerSearch(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBorrowerSearch: " + id);
        }
        deleteEntity(BorrowerSearch.class, id);
    }

    /**
     * Retrieves all 'BorrowerSearch' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BorrowerSearch> getBorrowerSearchList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BorrowerSearch> result = getEntities(BorrowerSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BorrowerSearch' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BorrowerSearch> getBorrowerSearchList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BorrowerSearch> result = getEntities(BorrowerSearch.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BorrowerSearch' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BorrowerSearch> getBorrowerSearchList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BorrowerSearch> result = getEntities(BorrowerSearch.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BorrowerSearch' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BorrowerSearch> getBorrowerSearchList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BorrowerSearch> result = getEntities(BorrowerSearch.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BorrowerSearch' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BorrowerSearch> findBorrowerSearchList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BorrowerSearch> result = findEntities(BorrowerSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BorrowerSearch' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BorrowerSearch> findBorrowerSearchList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BorrowerSearch> result = findEntities(BorrowerSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BorrowerSearch' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BorrowerSearch> findBorrowerSearchList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BorrowerSearch> result = findEntities(BorrowerSearch.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BorrowerSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BorrowerSearch> findBorrowerSearchList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BorrowerSearch> result = findEntities(BorrowerSearch.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BorrowerSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BorrowerSearch> findBorrowerSearchList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BorrowerSearch> result = findEntities(BorrowerSearch.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BorrowerSearch' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BorrowerSearch> findBorrowerSearchList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BorrowerSearch> result = findEntities(BorrowerSearch.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BorrowerSearch' instances.
     */
    public long getBorrowerSearchCount() {
        long result = getEntityCount(BorrowerSearch.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BorrowerSearch' instances which match the given whereClause.
     */
    public long getBorrowerSearchCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BorrowerSearch.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BorrowerSearch' instances which match the given whereClause together with params specified in object array.
     */
    public long getBorrowerSearchCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BorrowerSearch.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(borrower search)}
    //

    @Override
    public Collection<BorrowerSearch> searchByCustomerProcessStatus(String state, String reason, String action, Long advisorId,
                                                                    final Integer offset, final Integer limit,
                                                                    final String sortBy, final Boolean descending) {

        Sortable sort = new Sortable() {
            @Override
            public String getSort() {
                return sortBy;
            }
            @Override
            public boolean hasSort() {
                return true;
            }
            @Override
            public boolean sortDescending() {
                return descending;
            }
        };

        List<OqlTerm> terms = new ArrayList<OqlTerm>();

        if (state != null) {
            terms.add(OqlTerm.newTerm().equals("_customerProcessStatus._state", state));
        }

        if (reason != null){
            terms.add(OqlTerm.newTerm().equals("_customerProcessStatus._reason", reason));
        }

        if (action != null){
            terms.add(OqlTerm.newTerm().equals("_customerProcessStatus._action", action));
        }

        if (advisorId != null){
            terms.add(OqlTerm.newTerm().equals("_account._personalAdvisorId", advisorId));
        }

        Pageable page = null;
        if (offset != null && limit != null) {
            page =new Pageable() {
                @Override
                public long getPrevious() {
                    return 0;
                }

                @Override
                public long getNext() {
                    return 0;
                }

                @Override
                public int getItemsPerPage() {
                    return limit;
                }

                @Override
                public long getOffset() {
                    return offset;
                }

                @Override
                public boolean hasPrevious() {
                    return false;
                }

                @Override
                public boolean hasNext() {
                    return false;
                }

                @Override
                public Collection<?> getPageContent() {
                    return null;
                }

                @Override
                public long getSize() {
                    return 0;
                }
            };
        }

        if (terms.size() == 1){
            if (page==null)
                return findBorrowerSearchList(terms.get(0).toString(), sort);
            else
                return findBorrowerSearchList(terms.get(0).toString(), page, sort);
        } else if (terms.size() > 1){
            OqlTerm term = OqlTerm.newTerm().and(terms.toArray(new OqlTerm[terms.size()]));
            if (page==null)
                return findBorrowerSearchList(term.toString(), sort);
            else
                return findBorrowerSearchList(term.toString(), page, sort);
        } else {
            throw new IllegalArgumentException("There must be at leas one where clause.");
        }
    }

    @Override
    public long countByCustomerProcessStatus(String state, String reason, String action, Long advisorId) {
        List<OqlTerm> terms = new ArrayList<OqlTerm>();
        if (state != null) {
            terms.add(OqlTerm.newTerm().equals("_customerProcessStatus._state", state));
        }

        if (reason != null){
            terms.add(OqlTerm.newTerm().equals("_customerProcessStatus._reason", reason));
        }

        if (action != null){
            terms.add(OqlTerm.newTerm().equals("_customerProcessStatus._action", action));
        }

        if (advisorId != null){
            terms.add(OqlTerm.newTerm().equals("_account._personalAdvisorId", advisorId));
        }

        if (terms.size() == 1){
            return getBorrowerSearchCount(terms.get(0).toString());
        } else if (terms.size() > 1){
            OqlTerm term = OqlTerm.newTerm().and(terms.toArray(new OqlTerm[terms.size()]));
            return getBorrowerSearchCount(term.toString());
        } else {
            throw new IllegalArgumentException("There must be at leas one where clause.");
        }
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
