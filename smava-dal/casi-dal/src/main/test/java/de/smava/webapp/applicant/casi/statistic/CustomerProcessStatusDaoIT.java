package de.smava.webapp.applicant.casi.statistic;

import de.smava.webapp.brokerage.domain.type.CustomerProcessReason;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.casi.dao.CustomerProcessStatusDao;
import de.smava.webapp.casi.domain.CustomerProcessStatus;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;

import static de.smava.webapp.brokerage.domain.type.CustomerProcessReason.*;
import static de.smava.webapp.brokerage.domain.type.CustomerProcessState.*;
import static de.smava.webapp.brokerage.domain.type.CustomerProcessState.NICHT_ERREICHT;

@ContextConfiguration("classpath*:dao-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager")
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerProcessStatusDaoIT {
    @Autowired
    private CustomerProcessStatusDao customerProcessStatusDao;
    
    private final long SMAVA_ID = 1;

    @Test
    public void testFindStateBeforeCurrentStatusBySmavaId() {
        Date tenMinutesAgo = getDate(10);
        CustomerProcessStatus statusTenMinutesAgo = createStatus(OFFEN, NEUKUNDE, SMAVA_ID, tenMinutesAgo);
        //save few status with different smava_id
        createStatus(GENEHMIGT, KUNDE_UNZUFRIEDEN, 8, tenMinutesAgo);
        createStatus(NICHT_ERREICHT, SHORT_LEAD, 8, tenMinutesAgo);
        Date fiveMinutesAgo = getDate(5);
        CustomerProcessStatus statusFiveMinutesAgo = createStatus(BEI_BANK, LAUT_BANK, SMAVA_ID, fiveMinutesAgo);
        Date now = new Date();
        CustomerProcessStatus statusNow = createStatus(ABGESCHLOSSEN, AUSGEZAHLT, SMAVA_ID, now);

        CustomerProcessState stateFiveMinutesAgo = customerProcessStatusDao.findStateBeforeCurrentStatusBySmavaId(statusNow.getId(), SMAVA_ID);
        Assert.assertEquals(statusFiveMinutesAgo.getState(), stateFiveMinutesAgo);

        CustomerProcessState stateTenMinutesAgo = customerProcessStatusDao.findStateBeforeCurrentStatusBySmavaId(statusFiveMinutesAgo.getId(), SMAVA_ID);
        Assert.assertEquals(statusTenMinutesAgo.getState(), stateTenMinutesAgo);
    }

    private CustomerProcessStatus createStatus(CustomerProcessState state, CustomerProcessReason reason, long smavaId, Date creationDate) {
        CustomerProcessStatus status = new CustomerProcessStatus();
        status.setCreatedBy("kunde");
        status.setCreationDate(creationDate);
        status.setAccountId(1L);
        status.setAccountSmavaId(smavaId);
        status.setState(state);
        status.setReason(reason);
        customerProcessStatusDao.save(status);
        return status;
    }

    private Date getDate(int minusMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, -minusMinutes);
        return calendar.getTime();
    }
}
