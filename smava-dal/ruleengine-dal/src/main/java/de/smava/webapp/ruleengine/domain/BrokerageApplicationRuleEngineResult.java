//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ruleengine.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application rule engine result)}
import de.smava.webapp.ruleengine.domain.history.BrokerageApplicationRuleEngineResultHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationRuleEngineResults'.
 *
 * @author generator
 */
public class BrokerageApplicationRuleEngineResult extends BrokerageApplicationRuleEngineResultHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application rule engine result)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.brokerage.domain.BrokerageApplication _brokerageApplication;
        protected RuleEngineModel _ruleEngineModel;
        protected Double _result;
        protected de.smava.webapp.ruleengine.model.ResultStatus _status;
        protected Date _creationDate;
        
                                    
    /**
     * Setter for the property 'brokerage application'.
     */
    public void setBrokerageApplication(de.smava.webapp.brokerage.domain.BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    /**
     * Returns the property 'brokerage application'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }
                                            
    /**
     * Setter for the property 'rule engine model'.
     */
    public void setRuleEngineModel(RuleEngineModel ruleEngineModel) {
        _ruleEngineModel = ruleEngineModel;
    }
            
    /**
     * Returns the property 'rule engine model'.
     */
    public RuleEngineModel getRuleEngineModel() {
        return _ruleEngineModel;
    }
                                            
    /**
     * Setter for the property 'result'.
     */
    public void setResult(Double result) {
        _result = result;
    }
            
    /**
     * Returns the property 'result'.
     */
    public Double getResult() {
        return _result;
    }
                                            
    /**
     * Setter for the property 'status'.
     */
    public void setStatus(de.smava.webapp.ruleengine.model.ResultStatus status) {
        _status = status;
    }
            
    /**
     * Returns the property 'status'.
     */
    public de.smava.webapp.ruleengine.model.ResultStatus getStatus() {
        return _status;
    }
                                    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplicationRuleEngineResult.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplicationRuleEngineResult.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplicationRuleEngineResult asBrokerageApplicationRuleEngineResult() {
        return this;
    }
}
