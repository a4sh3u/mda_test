package de.smava.webapp.ruleengine.domain.history;



import de.smava.webapp.ruleengine.domain.abstracts.AbstractRuleEngineVariable;




/**
 * The domain object that has all history aggregation related fields for 'RuleEngineVariables'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RuleEngineVariableHistory extends AbstractRuleEngineVariable {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;


		
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
}
