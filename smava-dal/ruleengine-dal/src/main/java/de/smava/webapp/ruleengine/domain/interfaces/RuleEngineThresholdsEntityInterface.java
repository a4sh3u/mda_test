package de.smava.webapp.ruleengine.domain.interfaces;



import de.smava.webapp.ruleengine.domain.RuleEngineModel;
import de.smava.webapp.ruleengine.domain.RuleEngineThresholds;


/**
 * The domain object that represents 'RuleEngineThresholdss'.
 *
 * @author generator
 */
public interface RuleEngineThresholdsEntityInterface {

    /**
     * Setter for the property 'rule engine model'.
     *
     * 
     *
     */
    void setRuleEngineModel(RuleEngineModel ruleEngineModel);

    /**
     * Returns the property 'rule engine model'.
     *
     * 
     *
     */
    RuleEngineModel getRuleEngineModel();
    /**
     * Setter for the property 'first'.
     *
     * 
     *
     */
    void setFirst(Double first);

    /**
     * Returns the property 'first'.
     *
     * 
     *
     */
    Double getFirst();
    /**
     * Setter for the property 'second'.
     *
     * 
     *
     */
    void setSecond(Double second);

    /**
     * Returns the property 'second'.
     *
     * 
     *
     */
    Double getSecond();
    /**
     * Setter for the property 'third'.
     *
     * 
     *
     */
    void setThird(Double third);

    /**
     * Returns the property 'third'.
     *
     * 
     *
     */
    Double getThird();
    /**
     * Setter for the property 'fourth'.
     *
     * 
     *
     */
    void setFourth(Double fourth);

    /**
     * Returns the property 'fourth'.
     *
     * 
     *
     */
    Double getFourth();
    /**
     * Helper method to get reference of this object as model type.
     */
    RuleEngineThresholds asRuleEngineThresholds();
}
