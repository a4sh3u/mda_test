package de.smava.webapp.ruleengine.domain.history;



import de.smava.webapp.ruleengine.domain.abstracts.AbstractRuleEngineRateClass;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'RuleEngineRateClasss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RuleEngineRateClassHistory extends AbstractRuleEngineRateClass {

    protected transient Date _dateMinInitVal;
    protected transient boolean _dateMinIsSet;
    protected transient Date _dateMaxInitVal;
    protected transient boolean _dateMaxIsSet;


						
    /**
     * Returns the initial value of the property 'date min'.
     */
    public Date dateMinInitVal() {
        Date result;
        if (_dateMinIsSet) {
            result = _dateMinInitVal;
        } else {
            result = getDateMin();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date min'.
     */
    public boolean dateMinIsDirty() {
        return !valuesAreEqual(dateMinInitVal(), getDateMin());
    }

    /**
     * Returns true if the setter method was called for the property 'date min'.
     */
    public boolean dateMinIsSet() {
        return _dateMinIsSet;
    }
	
    /**
     * Returns the initial value of the property 'date max'.
     */
    public Date dateMaxInitVal() {
        Date result;
        if (_dateMaxIsSet) {
            result = _dateMaxInitVal;
        } else {
            result = getDateMax();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date max'.
     */
    public boolean dateMaxIsDirty() {
        return !valuesAreEqual(dateMaxInitVal(), getDateMax());
    }

    /**
     * Returns true if the setter method was called for the property 'date max'.
     */
    public boolean dateMaxIsSet() {
        return _dateMaxIsSet;
    }
			
}
