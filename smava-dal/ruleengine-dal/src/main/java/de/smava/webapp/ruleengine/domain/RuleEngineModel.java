//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ruleengine.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine model)}
import de.smava.webapp.ruleengine.domain.history.RuleEngineModelHistory;
import java.util.Collection;
import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RuleEngineModels'.
 *
 * @author generator
 */
public class RuleEngineModel extends RuleEngineModelHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(rule engine model)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _expirationDate;
        protected int _version;
        protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
        protected String _acceptanceRule;
        protected String _descriptor;
        protected RuleEngineThresholds _ruleEngineThreshold;
        protected Collection<RuleEngineVariable> _ruleEngineVariables;
        protected boolean _usingArvato;
        
                            /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'version'.
     */
    public void setVersion(int version) {
        if (!_versionIsSet) {
            _versionIsSet = true;
            _versionInitVal = getVersion();
        }
        registerChange("version", _versionInitVal, version);
        _version = version;
    }
                        
    /**
     * Returns the property 'version'.
     */
    public int getVersion() {
        return _version;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     */
    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'acceptance rule'.
     */
    public void setAcceptanceRule(String acceptanceRule) {
        if (!_acceptanceRuleIsSet) {
            _acceptanceRuleIsSet = true;
            _acceptanceRuleInitVal = getAcceptanceRule();
        }
        registerChange("acceptance rule", _acceptanceRuleInitVal, acceptanceRule);
        _acceptanceRule = acceptanceRule;
    }
                        
    /**
     * Returns the property 'acceptance rule'.
     */
    public String getAcceptanceRule() {
        return _acceptanceRule;
    }
                                    /**
     * Setter for the property 'descriptor'.
     */
    public void setDescriptor(String descriptor) {
        if (!_descriptorIsSet) {
            _descriptorIsSet = true;
            _descriptorInitVal = getDescriptor();
        }
        registerChange("descriptor", _descriptorInitVal, descriptor);
        _descriptor = descriptor;
    }
                        
    /**
     * Returns the property 'descriptor'.
     */
    public String getDescriptor() {
        return _descriptor;
    }
                                            
    /**
     * Setter for the property 'rule engine threshold'.
     */
    public void setRuleEngineThreshold(RuleEngineThresholds ruleEngineThreshold) {
        _ruleEngineThreshold = ruleEngineThreshold;
    }
            
    /**
     * Returns the property 'rule engine threshold'.
     */
    public RuleEngineThresholds getRuleEngineThreshold() {
        return _ruleEngineThreshold;
    }
                                            
    /**
     * Setter for the property 'rule engine variables'.
     */
    public void setRuleEngineVariables(Collection<RuleEngineVariable> ruleEngineVariables) {
        _ruleEngineVariables = ruleEngineVariables;
    }
            
    /**
     * Returns the property 'rule engine variables'.
     */
    public Collection<RuleEngineVariable> getRuleEngineVariables() {
        return _ruleEngineVariables;
    }
                                    /**
     * Setter for the property 'using arvato'.
     */
    public void setUsingArvato(boolean usingArvato) {
        if (!_usingArvatoIsSet) {
            _usingArvatoIsSet = true;
            _usingArvatoInitVal = getUsingArvato();
        }
        registerChange("using arvato", _usingArvatoInitVal, usingArvato);
        _usingArvato = usingArvato;
    }
                        
    /**
     * Returns the property 'using arvato'.
     */
    public boolean getUsingArvato() {
        return _usingArvato;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RuleEngineModel.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _version=").append(_version);
            builder.append("\n    _acceptanceRule=").append(_acceptanceRule);
            builder.append("\n    _descriptor=").append(_descriptor);
            builder.append("\n    _usingArvato=").append(_usingArvato);
            builder.append("\n}");
        } else {
            builder.append(RuleEngineModel.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RuleEngineModel asRuleEngineModel() {
        return this;
    }
}
