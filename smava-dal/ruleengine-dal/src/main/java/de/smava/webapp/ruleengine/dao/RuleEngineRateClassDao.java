//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ruleengine.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine rate class)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.brokerage.domain.BrokerageBank;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.ruleengine.domain.RuleEngineRateClass;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'RuleEngineRateClasss'.
 *
 * @author generator
 */
public interface RuleEngineRateClassDao extends BaseDao<RuleEngineRateClass> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the rule engine rate class identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    RuleEngineRateClass getRuleEngineRateClass(Long id);

    /**
     * Saves the rule engine rate class.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRuleEngineRateClass(RuleEngineRateClass ruleEngineRateClass);

    /**
     * Deletes an rule engine rate class, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rule engine rate class
     */
    void deleteRuleEngineRateClass(Long id);

    /**
     * Retrieves all 'RuleEngineRateClass' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<RuleEngineRateClass> getRuleEngineRateClassList();

    /**
     * Retrieves a page of 'RuleEngineRateClass' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<RuleEngineRateClass> getRuleEngineRateClassList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'RuleEngineRateClass' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<RuleEngineRateClass> getRuleEngineRateClassList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'RuleEngineRateClass' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<RuleEngineRateClass> getRuleEngineRateClassList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'RuleEngineRateClass' instances.
     */
    long getRuleEngineRateClassCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rule engine rate class)}

    /**
     * Get all rate class definitions as defined for the given bank
     * @param bank the bank
     * @return list of rate classes
     */
    Collection<RuleEngineRateClass> findByBrokerageBank(BrokerageBank bank);

    /**
     * Get all rate class definitions as defined for the given bank
     * @param bankId the bank ID
     * @return list of rate classes
     */
    Collection<RuleEngineRateClass> findByBrokerageBankId(long bankId);
    // !!!!!!!! End of insert code section !!!!!!!!
}
