//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ruleengine.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine variable)}
import de.smava.webapp.ruleengine.domain.history.RuleEngineVariableHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RuleEngineVariables'.
 *
 * @author generator
 */
public class RuleEngineVariable extends RuleEngineVariableHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(rule engine variable)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected RuleEngineModel _ruleEngineModel;
        protected String _name;
        protected Double _coefficient;
        
                                    
    /**
     * Setter for the property 'rule engine model'.
     */
    public void setRuleEngineModel(RuleEngineModel ruleEngineModel) {
        _ruleEngineModel = ruleEngineModel;
    }
            
    /**
     * Returns the property 'rule engine model'.
     */
    public RuleEngineModel getRuleEngineModel() {
        return _ruleEngineModel;
    }
                                    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }
                                            
    /**
     * Setter for the property 'coefficient'.
     */
    public void setCoefficient(Double coefficient) {
        _coefficient = coefficient;
    }
            
    /**
     * Returns the property 'coefficient'.
     */
    public Double getCoefficient() {
        return _coefficient;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RuleEngineVariable.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(RuleEngineVariable.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RuleEngineVariable asRuleEngineVariable() {
        return this;
    }
}
