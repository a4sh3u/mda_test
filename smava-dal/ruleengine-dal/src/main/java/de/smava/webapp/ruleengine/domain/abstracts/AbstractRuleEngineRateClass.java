//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.ruleengine.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine rate class)}

import de.smava.webapp.commons.domain.KreditPrivatEntity;
import de.smava.webapp.ruleengine.domain.interfaces.RuleEngineRateClassEntityInterface;
import org.apache.log4j.Logger;

                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RuleEngineRateClasss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * We try to determine the risk class of the customer based on the effective rate sent by bank in first applied response.
 *
 * @author generator
 */
public abstract class AbstractRuleEngineRateClass
    extends KreditPrivatEntity implements RuleEngineRateClassEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRuleEngineRateClass.class);

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(rule engine rate class)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

