package de.smava.webapp.ruleengine.domain.interfaces;



import de.smava.webapp.ruleengine.domain.RuleEngineModel;
import de.smava.webapp.ruleengine.domain.RuleEngineThresholds;
import de.smava.webapp.ruleengine.domain.RuleEngineVariable;

import java.util.Collection;
import java.util.Date;


/**
 * The domain object that represents 'RuleEngineModels'.
 *
 * @author generator
 */
public interface RuleEngineModelEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'version'.
     *
     * 
     *
     */
    void setVersion(int version);

    /**
     * Returns the property 'version'.
     *
     * 
     *
     */
    int getVersion();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'acceptance rule'.
     *
     * 
     *
     */
    void setAcceptanceRule(String acceptanceRule);

    /**
     * Returns the property 'acceptance rule'.
     *
     * 
     *
     */
    String getAcceptanceRule();
    /**
     * Setter for the property 'descriptor'.
     *
     * 
     *
     */
    void setDescriptor(String descriptor);

    /**
     * Returns the property 'descriptor'.
     *
     * 
     *
     */
    String getDescriptor();
    /**
     * Setter for the property 'rule engine threshold'.
     *
     * 
     *
     */
    void setRuleEngineThreshold(RuleEngineThresholds ruleEngineThreshold);

    /**
     * Returns the property 'rule engine threshold'.
     *
     * 
     *
     */
    RuleEngineThresholds getRuleEngineThreshold();
    /**
     * Setter for the property 'rule engine variables'.
     *
     * 
     *
     */
    void setRuleEngineVariables(Collection<RuleEngineVariable> ruleEngineVariables);

    /**
     * Returns the property 'rule engine variables'.
     *
     * 
     *
     */
    Collection<RuleEngineVariable> getRuleEngineVariables();
    /**
     * Setter for the property 'using arvato'.
     *
     * 
     *
     */
    void setUsingArvato(boolean usingArvato);

    /**
     * Returns the property 'using arvato'.
     *
     * 
     *
     */
    boolean getUsingArvato();
    /**
     * Helper method to get reference of this object as model type.
     */
    RuleEngineModel asRuleEngineModel();
}
