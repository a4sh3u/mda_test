//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.ruleengine.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine variable)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.ruleengine.dao.RuleEngineVariableDao;
import de.smava.webapp.ruleengine.domain.RuleEngineModel;
import de.smava.webapp.ruleengine.domain.RuleEngineVariable;

import javax.jdo.Query;
import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'RuleEngineVariables'.
 *
 * @author generator
 */
@Repository(value = "ruleEngineVariableDao")
public class JdoRuleEngineVariableDao extends JdoBaseDao implements RuleEngineVariableDao {

    private static final Logger LOGGER = Logger.getLogger(JdoRuleEngineVariableDao.class);

    private static final String CLASS_NAME = "RuleEngineVariable";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(rule engine variable)}
    private static final String RE_VARIABLE_SQL = "select * from rule_engine_variable re where re.name = :name and re.rule_engine_model_id = :model";
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the rule engine variable identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public RuleEngineVariable load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        RuleEngineVariable result = getEntity(RuleEngineVariable.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public RuleEngineVariable getRuleEngineVariable(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	RuleEngineVariable entity = findUniqueEntity(RuleEngineVariable.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the rule engine variable.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(RuleEngineVariable ruleEngineVariable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveRuleEngineVariable: " + ruleEngineVariable);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(rule engine variable)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(ruleEngineVariable);
    }

    /**
     * @deprecated Use {@link #save(RuleEngineVariable) instead}
     */
    public Long saveRuleEngineVariable(RuleEngineVariable ruleEngineVariable) {
        return save(ruleEngineVariable);
    }

    /**
     * Deletes an rule engine variable, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the rule engine variable
     */
    public void deleteRuleEngineVariable(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteRuleEngineVariable: " + id);
        }
        deleteEntity(RuleEngineVariable.class, id);
    }

    /**
     * Retrieves all 'RuleEngineVariable' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<RuleEngineVariable> getRuleEngineVariableList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<RuleEngineVariable> result = getEntities(RuleEngineVariable.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'RuleEngineVariable' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<RuleEngineVariable> getRuleEngineVariableList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<RuleEngineVariable> result = getEntities(RuleEngineVariable.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RuleEngineVariable' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<RuleEngineVariable> getRuleEngineVariableList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<RuleEngineVariable> result = getEntities(RuleEngineVariable.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RuleEngineVariable' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<RuleEngineVariable> getRuleEngineVariableList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<RuleEngineVariable> result = getEntities(RuleEngineVariable.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RuleEngineVariable' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RuleEngineVariable> findRuleEngineVariableList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<RuleEngineVariable> result = findEntities(RuleEngineVariable.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'RuleEngineVariable' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<RuleEngineVariable> findRuleEngineVariableList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<RuleEngineVariable> result = findEntities(RuleEngineVariable.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'RuleEngineVariable' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RuleEngineVariable> findRuleEngineVariableList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<RuleEngineVariable> result = findEntities(RuleEngineVariable.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'RuleEngineVariable' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RuleEngineVariable> findRuleEngineVariableList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<RuleEngineVariable> result = findEntities(RuleEngineVariable.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RuleEngineVariable' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RuleEngineVariable> findRuleEngineVariableList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<RuleEngineVariable> result = findEntities(RuleEngineVariable.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'RuleEngineVariable' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<RuleEngineVariable> findRuleEngineVariableList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<RuleEngineVariable> result = findEntities(RuleEngineVariable.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'RuleEngineVariable' instances.
     */
    public long getRuleEngineVariableCount() {
        long result = getEntityCount(RuleEngineVariable.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RuleEngineVariable' instances which match the given whereClause.
     */
    public long getRuleEngineVariableCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(RuleEngineVariable.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'RuleEngineVariable' instances which match the given whereClause together with params specified in object array.
     */
    public long getRuleEngineVariableCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(RuleEngineVariable.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(rule engine variable)}
    public RuleEngineVariable findOne(String variableName, RuleEngineModel evaluated) {

        Query q = getPersistenceManager().newQuery(Query.SQL, RE_VARIABLE_SQL);
        q.setClass(RuleEngineVariable.class);

        Map<String, Object> parameters = new HashMap<String, Object>(2);
        parameters.put("name", variableName);
        parameters.put("model", evaluated.getId());
        Collection<RuleEngineVariable> result = (Collection<RuleEngineVariable>) q.executeWithMap(parameters);
        RuleEngineVariable resultVar = null;
        if (result.size() > 0) {
            result.iterator().next().getCoefficient();
            resultVar = result.iterator().next();
        }
        return resultVar;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}
