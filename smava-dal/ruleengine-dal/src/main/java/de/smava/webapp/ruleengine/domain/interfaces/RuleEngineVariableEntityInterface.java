package de.smava.webapp.ruleengine.domain.interfaces;



import de.smava.webapp.ruleengine.domain.RuleEngineModel;
import de.smava.webapp.ruleengine.domain.RuleEngineVariable;


/**
 * The domain object that represents 'RuleEngineVariables'.
 *
 * @author generator
 */
public interface RuleEngineVariableEntityInterface {

    /**
     * Setter for the property 'rule engine model'.
     *
     * 
     *
     */
    void setRuleEngineModel(RuleEngineModel ruleEngineModel);

    /**
     * Returns the property 'rule engine model'.
     *
     * 
     *
     */
    RuleEngineModel getRuleEngineModel();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'coefficient'.
     *
     * 
     *
     */
    void setCoefficient(Double coefficient);

    /**
     * Returns the property 'coefficient'.
     *
     * 
     *
     */
    Double getCoefficient();
    /**
     * Helper method to get reference of this object as model type.
     */
    RuleEngineVariable asRuleEngineVariable();
}
