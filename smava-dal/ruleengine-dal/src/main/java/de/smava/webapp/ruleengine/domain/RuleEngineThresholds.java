//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ruleengine.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine thresholds)}
import de.smava.webapp.ruleengine.domain.history.RuleEngineThresholdsHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RuleEngineThresholdss'.
 *
 * @author generator
 */
public class RuleEngineThresholds extends RuleEngineThresholdsHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(rule engine thresholds)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected RuleEngineModel _ruleEngineModel;
        protected Double _first;
        protected Double _second;
        protected Double _third;
        protected Double _fourth;
        
                /**
     * Setter for the property 'rule engine model'.
     */
    public void setRuleEngineModel(RuleEngineModel ruleEngineModel) {
        _ruleEngineModel = ruleEngineModel;
    }

    /**
     * Returns the property 'rule engine model'.
     */
    public RuleEngineModel getRuleEngineModel() {
        return _ruleEngineModel;
    }
                        /**
     * Setter for the property 'first'.
     */
    public void setFirst(Double first) {
        _first = first;
    }

    /**
     * Returns the property 'first'.
     */
    public Double getFirst() {
        return _first;
    }
                        /**
     * Setter for the property 'second'.
     */
    public void setSecond(Double second) {
        _second = second;
    }

    /**
     * Returns the property 'second'.
     */
    public Double getSecond() {
        return _second;
    }
                        /**
     * Setter for the property 'third'.
     */
    public void setThird(Double third) {
        _third = third;
    }

    /**
     * Returns the property 'third'.
     */
    public Double getThird() {
        return _third;
    }
                        /**
     * Setter for the property 'fourth'.
     */
    public void setFourth(Double fourth) {
        _fourth = fourth;
    }

    /**
     * Returns the property 'fourth'.
     */
    public Double getFourth() {
        return _fourth;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RuleEngineThresholds.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(RuleEngineThresholds.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RuleEngineThresholds asRuleEngineThresholds() {
        return this;
    }
}
