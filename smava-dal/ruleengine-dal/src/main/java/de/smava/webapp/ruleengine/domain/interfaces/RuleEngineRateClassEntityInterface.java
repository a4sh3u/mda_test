package de.smava.webapp.ruleengine.domain.interfaces;



import de.smava.webapp.ruleengine.domain.RuleEngineRateClass;

import java.util.Date;


/**
 * The domain object that represents 'RuleEngineRateClasss'.
 *
 * @author generator
 */
public interface RuleEngineRateClassEntityInterface {

    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'rate'.
     *
     * 
     *
     */
    void setRate(Double rate);

    /**
     * Returns the property 'rate'.
     *
     * 
     *
     */
    Double getRate();
    /**
     * Setter for the property 'amount min'.
     *
     * 
     *
     */
    void setAmountMin(Double amountMin);

    /**
     * Returns the property 'amount min'.
     *
     * 
     *
     */
    Double getAmountMin();
    /**
     * Setter for the property 'amount max'.
     *
     * 
     *
     */
    void setAmountMax(Double amountMax);

    /**
     * Returns the property 'amount max'.
     *
     * 
     *
     */
    Double getAmountMax();
    /**
     * Setter for the property 'risk class'.
     *
     * 
     *
     */
    void setRiskClass(Integer riskClass);

    /**
     * Returns the property 'risk class'.
     *
     * 
     *
     */
    Integer getRiskClass();
    /**
     * Setter for the property 'date min'.
     *
     * 
     *
     */
    void setDateMin(Date dateMin);

    /**
     * Returns the property 'date min'.
     *
     * 
     *
     */
    Date getDateMin();
    /**
     * Setter for the property 'date max'.
     *
     * 
     *
     */
    void setDateMax(Date dateMax);

    /**
     * Returns the property 'date max'.
     *
     * 
     *
     */
    Date getDateMax();
    /**
     * Setter for the property 'order'.
     *
     * 
     *
     */
    void setOrder(Integer order);

    /**
     * Returns the property 'order'.
     *
     * 
     *
     */
    Integer getOrder();
    /**
     * Setter for the property 'rate min'.
     *
     * 
     *
     */
    void setRateMin(Double rateMin);

    /**
     * Returns the property 'rate min'.
     *
     * 
     *
     */
    Double getRateMin();
    /**
     * Setter for the property 'rate max'.
     *
     * 
     *
     */
    void setRateMax(Double rateMax);

    /**
     * Returns the property 'rate max'.
     *
     * 
     *
     */
    Double getRateMax();
    /**
     * Helper method to get reference of this object as model type.
     */
    RuleEngineRateClass asRuleEngineRateClass();
}
