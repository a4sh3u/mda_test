//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.ruleengine.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(rule engine rate class)}
import de.smava.webapp.ruleengine.domain.history.RuleEngineRateClassHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RuleEngineRateClasss'.
 *
 * We try to determine the risk class of the customer based on the effective rate sent by bank in first applied response.
 *
 * @author generator
 */
public class RuleEngineRateClass extends RuleEngineRateClassHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(rule engine rate class)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.brokerage.domain.BrokerageBank _brokerageBank;
        protected Double _rate;
        protected Double _amountMin;
        protected Double _amountMax;
        protected Integer _riskClass;
        protected Date _dateMin;
        protected Date _dateMax;
        protected Integer _order;
        protected Double _rateMin;
        protected Double _rateMax;
        
                                    
    /**
     * Setter for the property 'brokerage bank'.
     */
    public void setBrokerageBank(de.smava.webapp.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     */
    public de.smava.webapp.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'rate'.
     */
    public void setRate(Double rate) {
        _rate = rate;
    }
            
    /**
     * Returns the property 'rate'.
     */
    public Double getRate() {
        return _rate;
    }
                                            
    /**
     * Setter for the property 'amount min'.
     */
    public void setAmountMin(Double amountMin) {
        _amountMin = amountMin;
    }
            
    /**
     * Returns the property 'amount min'.
     */
    public Double getAmountMin() {
        return _amountMin;
    }
                                            
    /**
     * Setter for the property 'amount max'.
     */
    public void setAmountMax(Double amountMax) {
        _amountMax = amountMax;
    }
            
    /**
     * Returns the property 'amount max'.
     */
    public Double getAmountMax() {
        return _amountMax;
    }
                                            
    /**
     * Setter for the property 'risk class'.
     */
    public void setRiskClass(Integer riskClass) {
        _riskClass = riskClass;
    }
            
    /**
     * Returns the property 'risk class'.
     */
    public Integer getRiskClass() {
        return _riskClass;
    }
                                    /**
     * Setter for the property 'date min'.
     */
    public void setDateMin(Date dateMin) {
        if (!_dateMinIsSet) {
            _dateMinIsSet = true;
            _dateMinInitVal = getDateMin();
        }
        registerChange("date min", _dateMinInitVal, dateMin);
        _dateMin = dateMin;
    }
                        
    /**
     * Returns the property 'date min'.
     */
    public Date getDateMin() {
        return _dateMin;
    }
                                    /**
     * Setter for the property 'date max'.
     */
    public void setDateMax(Date dateMax) {
        if (!_dateMaxIsSet) {
            _dateMaxIsSet = true;
            _dateMaxInitVal = getDateMax();
        }
        registerChange("date max", _dateMaxInitVal, dateMax);
        _dateMax = dateMax;
    }
                        
    /**
     * Returns the property 'date max'.
     */
    public Date getDateMax() {
        return _dateMax;
    }
                                            
    /**
     * Setter for the property 'order'.
     */
    public void setOrder(Integer order) {
        _order = order;
    }
            
    /**
     * Returns the property 'order'.
     */
    public Integer getOrder() {
        return _order;
    }
                                            
    /**
     * Setter for the property 'rate min'.
     */
    public void setRateMin(Double rateMin) {
        _rateMin = rateMin;
    }
            
    /**
     * Returns the property 'rate min'.
     */
    public Double getRateMin() {
        return _rateMin;
    }
                                            
    /**
     * Setter for the property 'rate max'.
     */
    public void setRateMax(Double rateMax) {
        _rateMax = rateMax;
    }
            
    /**
     * Returns the property 'rate max'.
     */
    public Double getRateMax() {
        return _rateMax;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RuleEngineRateClass.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(RuleEngineRateClass.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RuleEngineRateClass asRuleEngineRateClass() {
        return this;
    }
}
