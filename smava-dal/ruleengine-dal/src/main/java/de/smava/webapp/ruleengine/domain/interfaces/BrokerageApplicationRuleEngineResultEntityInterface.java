package de.smava.webapp.ruleengine.domain.interfaces;



import de.smava.webapp.ruleengine.domain.BrokerageApplicationRuleEngineResult;
import de.smava.webapp.ruleengine.domain.RuleEngineModel;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageApplicationRuleEngineResults'.
 *
 * @author generator
 */
public interface BrokerageApplicationRuleEngineResultEntityInterface {

    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    void setBrokerageApplication(de.smava.webapp.brokerage.domain.BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    de.smava.webapp.brokerage.domain.BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'rule engine model'.
     *
     * 
     *
     */
    void setRuleEngineModel(RuleEngineModel ruleEngineModel);

    /**
     * Returns the property 'rule engine model'.
     *
     * 
     *
     */
    RuleEngineModel getRuleEngineModel();
    /**
     * Setter for the property 'result'.
     *
     * 
     *
     */
    void setResult(Double result);

    /**
     * Returns the property 'result'.
     *
     * 
     *
     */
    Double getResult();
    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    void setStatus(de.smava.webapp.ruleengine.model.ResultStatus status);

    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    de.smava.webapp.ruleengine.model.ResultStatus getStatus();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationRuleEngineResult asBrokerageApplicationRuleEngineResult();
}
