package de.smava.webapp.ruleengine.domain.history;



import de.smava.webapp.ruleengine.domain.abstracts.AbstractRuleEngineThresholds;




/**
 * The domain object that has all history aggregation related fields for 'RuleEngineThresholdss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RuleEngineThresholdsHistory extends AbstractRuleEngineThresholds {



					
}
