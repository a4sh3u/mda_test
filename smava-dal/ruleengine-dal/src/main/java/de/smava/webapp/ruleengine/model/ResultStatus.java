package de.smava.webapp.ruleengine.model;

public enum ResultStatus {
	DENIED,
	ONE_MARK,
	TWO_MARKS,
	THREE_MARKS,
	UNCERTAIN;
}
