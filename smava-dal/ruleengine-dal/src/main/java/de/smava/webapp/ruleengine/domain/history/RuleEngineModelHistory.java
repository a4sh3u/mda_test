package de.smava.webapp.ruleengine.domain.history;



import de.smava.webapp.ruleengine.domain.abstracts.AbstractRuleEngineModel;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'RuleEngineModels'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RuleEngineModelHistory extends AbstractRuleEngineModel {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient int _versionInitVal;
    protected transient boolean _versionIsSet;
    protected transient String _acceptanceRuleInitVal;
    protected transient boolean _acceptanceRuleIsSet;
    protected transient String _descriptorInitVal;
    protected transient boolean _descriptorIsSet;
    protected transient boolean _usingArvatoInitVal;
    protected transient boolean _usingArvatoIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public Date expirationDateInitVal() {
        Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'version'.
     */
    public int versionInitVal() {
        int result;
        if (_versionIsSet) {
            result = _versionInitVal;
        } else {
            result = getVersion();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'version'.
     */
    public boolean versionIsDirty() {
        return !valuesAreEqual(versionInitVal(), getVersion());
    }

    /**
     * Returns true if the setter method was called for the property 'version'.
     */
    public boolean versionIsSet() {
        return _versionIsSet;
    }
		
    /**
     * Returns the initial value of the property 'acceptance rule'.
     */
    public String acceptanceRuleInitVal() {
        String result;
        if (_acceptanceRuleIsSet) {
            result = _acceptanceRuleInitVal;
        } else {
            result = getAcceptanceRule();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'acceptance rule'.
     */
    public boolean acceptanceRuleIsDirty() {
        return !valuesAreEqual(acceptanceRuleInitVal(), getAcceptanceRule());
    }

    /**
     * Returns true if the setter method was called for the property 'acceptance rule'.
     */
    public boolean acceptanceRuleIsSet() {
        return _acceptanceRuleIsSet;
    }
	
    /**
     * Returns the initial value of the property 'descriptor'.
     */
    public String descriptorInitVal() {
        String result;
        if (_descriptorIsSet) {
            result = _descriptorInitVal;
        } else {
            result = getDescriptor();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'descriptor'.
     */
    public boolean descriptorIsDirty() {
        return !valuesAreEqual(descriptorInitVal(), getDescriptor());
    }

    /**
     * Returns true if the setter method was called for the property 'descriptor'.
     */
    public boolean descriptorIsSet() {
        return _descriptorIsSet;
    }
			
    /**
     * Returns the initial value of the property 'using arvato'.
     */
    public boolean usingArvatoInitVal() {
        boolean result;
        if (_usingArvatoIsSet) {
            result = _usingArvatoInitVal;
        } else {
            result = getUsingArvato();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'using arvato'.
     */
    public boolean usingArvatoIsDirty() {
        return !valuesAreEqual(usingArvatoInitVal(), getUsingArvato());
    }

    /**
     * Returns true if the setter method was called for the property 'using arvato'.
     */
    public boolean usingArvatoIsSet() {
        return _usingArvatoIsSet;
    }

}
