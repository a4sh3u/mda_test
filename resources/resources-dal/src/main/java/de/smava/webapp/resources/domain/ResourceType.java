package de.smava.webapp.resources.domain;

/**
 * Created by dkeller on 14.03.16.
 */
public enum ResourceType {

    LINK,
    STRING,
    HTML,
    PATH,
    ENUM,
    FOLDER
}
