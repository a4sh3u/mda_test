//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.resources.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(Resource)}
import de.smava.webapp.resources.domain.history.ResourceHistory;

import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Resources'.
 *
 * 
 *
 * @author generator
 */
public class Resource extends ResourceHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(Resource)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _key;
        protected String _value;
        protected ResourceType _type;
        protected Set<Resource> _children;
        protected Resource _parent;
        
                            /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    public void setKey(String key) {
        if (!_keyIsSet) {
            _keyIsSet = true;
            _keyInitVal = getKey();
        }
        registerChange("key", _keyInitVal, key);
        _key = key;
    }
                        
    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    public String getKey() {
        return _key;
    }
                                    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    public String getValue() {
        return _value;
    }
                                            
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(ResourceType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public ResourceType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'children'.
     *
     * 
     *
     */
    public void setChildren(Set<Resource> children) {
        _children = children;
    }
            
    /**
     * Returns the property 'children'.
     *
     * 
     *
     */
    public Set<Resource> getChildren() {
        return _children;
    }
                                            
    /**
     * Setter for the property 'parent'.
     *
     * 
     *
     */
    public void setParent(Resource parent) {
        _parent = parent;
    }
            
    /**
     * Returns the property 'parent'.
     *
     * 
     *
     */
    public Resource getParent() {
        return _parent;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Resource.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _key=").append(_key);
            builder.append("\n    _value=").append(_value);
            builder.append("\n}");
        } else {
            builder.append(Resource.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Resource asResource() {
        return this;
    }
}
