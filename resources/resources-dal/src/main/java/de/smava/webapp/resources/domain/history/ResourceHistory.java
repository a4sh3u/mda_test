package de.smava.webapp.resources.domain.history;



import de.smava.webapp.resources.domain.abstracts.AbstractResource;




/**
 * The domain object that has all history aggregation related fields for 'Resources'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ResourceHistory extends AbstractResource {

    protected transient String _keyInitVal;
    protected transient boolean _keyIsSet;
    protected transient String _valueInitVal;
    protected transient boolean _valueIsSet;


	
    /**
     * Returns the initial value of the property 'key'.
     */
    public String keyInitVal() {
        String result;
        if (_keyIsSet) {
            result = _keyInitVal;
        } else {
            result = getKey();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'key'.
     */
    public boolean keyIsDirty() {
        return !valuesAreEqual(keyInitVal(), getKey());
    }

    /**
     * Returns true if the setter method was called for the property 'key'.
     */
    public boolean keyIsSet() {
        return _keyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'value'.
     */
    public String valueInitVal() {
        String result;
        if (_valueIsSet) {
            result = _valueInitVal;
        } else {
            result = getValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'value'.
     */
    public boolean valueIsDirty() {
        return !valuesAreEqual(valueInitVal(), getValue());
    }

    /**
     * Returns true if the setter method was called for the property 'value'.
     */
    public boolean valueIsSet() {
        return _valueIsSet;
    }
			
}
