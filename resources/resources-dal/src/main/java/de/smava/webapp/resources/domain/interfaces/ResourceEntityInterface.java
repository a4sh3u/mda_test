package de.smava.webapp.resources.domain.interfaces;



import de.smava.webapp.resources.domain.Resource;
import de.smava.webapp.resources.domain.ResourceType;

import java.util.Set;


/**
 * The domain object that represents 'Resources'.
 *
 * @author generator
 */
public interface ResourceEntityInterface {

    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    void setKey(String key);

    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    String getKey();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(ResourceType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    ResourceType getType();
    /**
     * Setter for the property 'children'.
     *
     * 
     *
     */
    void setChildren(Set<Resource> children);

    /**
     * Returns the property 'children'.
     *
     * 
     *
     */
    Set<Resource> getChildren();
    /**
     * Setter for the property 'parent'.
     *
     * 
     *
     */
    void setParent(Resource parent);

    /**
     * Returns the property 'parent'.
     *
     * 
     *
     */
    Resource getParent();
    /**
     * Helper method to get reference of this object as model type.
     */
    Resource asResource();
}
