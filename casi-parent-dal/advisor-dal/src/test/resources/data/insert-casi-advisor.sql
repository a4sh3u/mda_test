INSERT INTO public.account(
  id, account_id, creation_date, email, phone, phone_code, state, username, personal_advisor)
VALUES (nextval('"MAIN_SEQUENCE"'), nextval('"ACCOUNT_ID_SEQUENCE"'), now(),
        'mail' || currval('"MAIN_SEQUENCE"')::text || '@domain.com', '1234567', '030', 'ACTIVATED', 'user' || currval('"MAIN_SEQUENCE"')::text, null);

INSERT INTO public.account_role(id, account_id, name)
  SELECT nextval('"MAIN_SEQUENCE"'), max(id), 'ROLE_CREDIT_ADVISOR'
  FROM public.account ac;

INSERT INTO public.account_role_state(id, account_role_id, name, valid_from, valid_until)
  SELECT nextval('"MAIN_SEQUENCE"'), max(id), 'APPLIED', now(), NULL
  FROM public.account_role;

INSERT INTO public.person(id, account_id, creation_date, first_name, last_name, type)
  SELECT nextval('"MAIN_SEQUENCE"'), max(id), now(), 'Johnny', 'Smith', 1
  FROM public.account ac;

INSERT INTO public.advisor(
  id, location, team_id, max_customer_per_day, sip, account_id)
  SELECT nextval('"MAIN_SEQUENCE"'), 'UNKNOWN', NULL, 50, NULL, max(id)
  FROM public.account ac;

INSERT INTO casiadvisor.advisor(id, smava_ref_id, email, username, first_name, last_name, location, team_id,
                                blocked_picking_date, max_customer_per_day, unlimited_picking_date, sip, phone, state,
                                db_creation_date, db_modify_date) (
  SELECT nextval('casiadvisor.id_seq'), ac.account_id, ac.email, ac.username, p.first_name, p.last_name, ad.location,
    NULL, ad.blocked_date, ad.max_customer_per_day, ad.unlimited_picking_date, ad.sip,
    COALESCE(ac.phone_code, '') || COALESCE(ac.phone, ''), ac.state, ac.creation_date, ad.db_modify_date
  FROM public.advisor ad
    JOIN public.account ac ON ac.id = ad.account_id
    JOIN public.person p ON p.account_id = ac.id AND p.type = 1
  ORDER BY ad.id DESC
  LIMIT 1
);