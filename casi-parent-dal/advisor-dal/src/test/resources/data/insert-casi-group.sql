INSERT INTO brokerage.advisor_group(
  id, name, condition, expiration_date, priority)
VALUES (nextval('"MAIN_SEQUENCE"'), 'Test group', '#offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)', null, 1);

INSERT INTO casiadvisor.advisor_group(
  id, name, description, priority, properties, state, old_temp_id)
  SELECT nextval('casiadvisor.id_seq'), 'Test group', null, 1, '{"conditions":{"isFromOfflineAffiliate":true}}', 'ACTIVE', max(id)
  FROM brokerage.advisor_group bg;