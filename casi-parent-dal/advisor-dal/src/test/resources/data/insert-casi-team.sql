INSERT INTO public.team(id, name, expiration_date, team_lead_id)
VALUES (nextval('"MAIN_SEQUENCE"'), 'Test team', current_timestamp + 2 * interval '1 day', NULL);

INSERT INTO casiadvisor.team(
  id, name, expiration_date, team_lead_id, old_temp_id)
SELECT nextval('casiadvisor.id_seq'), 'Test team', current_timestamp + 2 * interval '1 day', NULL, max(id)
  FROM team t;