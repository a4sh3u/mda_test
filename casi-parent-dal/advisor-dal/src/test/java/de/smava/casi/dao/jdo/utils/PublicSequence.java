package de.smava.casi.dao.jdo.utils;

import org.springframework.orm.jdo.PersistenceManagerFactoryUtils;

import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

public class PublicSequence {

    private PersistenceManagerFactory persistenceManagerFactory;

    public PublicSequence(PersistenceManagerFactory persistenceManagerFactory) {
        this.persistenceManagerFactory = persistenceManagerFactory;
    }

    public long next() {
        PersistenceManager persistenceManager = PersistenceManagerFactoryUtils.getPersistenceManager(persistenceManagerFactory, true);
        Query query = persistenceManager.newQuery("javax.jdo.query.SQL",
                "SELECT nextval('public.\"MAIN_SEQUENCE\"')");
        query.setUnique(true);
        query.setResultClass(Long.class);
        return  (Long) query.execute();
    }
}
