package de.smava.casi.dao.jdo;

import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.brokerage.dao.AdvisorAssignmentLogDao;
import de.smava.webapp.brokerage.domain.AdvisorAssignmentLog;
import de.smava.webapp.brokerage.domain.AdvisorAssignmentType;
import de.smava.webapp.commons.dao.jdo.JdoTestEntityDao;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Random;

import static de.smava.webapp.brokerage.domain.AdvisorAssignmentType.FIRST_CALL;
import static de.smava.webapp.brokerage.domain.AdvisorAssignmentType.MANUAL_ADVISOR_CHANGE;
import static org.junit.Assert.assertNotNull;

@Ignore
@ContextConfiguration("classpath*:advisor-dao-test-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoHybridAdvisorAssignmentLogDaoIT {

    private static final Random RANDOM = new Random();
    private static final long CREDIT_ADVISOR_ID = 28176L;  // "credit_advisor@backoffice.de"
    private static final long CREDIT_ADVISORY_ADMIN_ID = 81162L; // "credit_advisory_admin@backoffice.de"

    @Autowired
    private AdvisorAssignmentLogDao advisorAssignmentLogDao;

    @Autowired
    private JdoTestEntityDao jdoTestEntityDao;

    @Test
    public void testSaveAdvisorAssignmentLogBySystem() {
        AdvisorAssignmentLog legacyLog = createAdvisorAssignmentLogStub(FIRST_CALL, RANDOM.nextLong());

        Advisor advisor = jdoTestEntityDao.getObjectById(Advisor.class, CREDIT_ADVISOR_ID);
        legacyLog.setCreditAdvisor(advisor);

        Long savedId = advisorAssignmentLogDao.save(legacyLog);
        assertNotNull(savedId);
    }

    @Test
    public void testSaveAdvisorAssignmentLogByAdmin() {
        AdvisorAssignmentLog legacyLog =
                createAdvisorAssignmentLog(FIRST_CALL, CREDIT_ADVISOR_ID, CREDIT_ADVISORY_ADMIN_ID, RANDOM.nextLong());

        Long savedId = advisorAssignmentLogDao.save(legacyLog);
        assertNotNull(savedId);
    }

    @Test
    public void testSaveAdvisorAssignmentLogByAdminWithReassignment() {
        AdvisorAssignmentLog legacyLog =
                createAdvisorAssignmentLog(FIRST_CALL, CREDIT_ADVISOR_ID, CREDIT_ADVISORY_ADMIN_ID, RANDOM.nextLong());
        Long usedCustomerNumber = legacyLog.getCustomerNumber();

        Long savedId = advisorAssignmentLogDao.save(legacyLog);
        assertNotNull(savedId);

        AdvisorAssignmentLog anotherLegacyLog = createAdvisorAssignmentLog(MANUAL_ADVISOR_CHANGE, CREDIT_ADVISORY_ADMIN_ID, CREDIT_ADVISORY_ADMIN_ID, usedCustomerNumber);
        Long anotherSavedId = advisorAssignmentLogDao.save(anotherLegacyLog);
        assertNotNull(anotherSavedId);
    }

    private AdvisorAssignmentLog createAdvisorAssignmentLog(AdvisorAssignmentType assignmentType, long advisorId, long createdById, long customerNumber) {
        AdvisorAssignmentLog legacyLog = createAdvisorAssignmentLogStub(assignmentType, customerNumber);

        Advisor advisor = jdoTestEntityDao.getObjectById(Advisor.class, advisorId);
        legacyLog.setCreditAdvisor(advisor);

        Advisor admin = jdoTestEntityDao.getObjectById(Advisor.class, createdById);
        legacyLog.setCreatedBy(admin);
        return legacyLog;
    }

    private AdvisorAssignmentLog createAdvisorAssignmentLogStub(AdvisorAssignmentType assignmentType, long customerNumber) {
        AdvisorAssignmentLog legacyLog = new AdvisorAssignmentLog();
        legacyLog.setAssignmentDate(new Date());
        legacyLog.setCustomerType(assignmentType);
        legacyLog.setCustomerNumber(customerNumber);
        legacyLog.setLoanApplicationId(123456L);
        return legacyLog;
    }
}
