package de.smava.casi.dao.jdo;

import de.smava.casi.data.DataFixtures;
import de.smava.webapp.account.dao.TeamDao;
import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.account.domain.Team;
import de.smava.webapp.commons.dao.jdo.JdoTestEntityDao;
import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Date;

import static de.smava.webapp.account.todo.util.DateUtils.addMonth;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Ignore
@ContextConfiguration("classpath*:advisor-dao-test-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoHybridTeamDaoIT {

    private static final Logger LOGGER = Logger.getLogger(JdoHybridTeamDaoIT.class);
    private static final Date NOW = new Date();

    @Autowired
    public DataSource dataSource;

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private JdoTestEntityDao jdoTestEntityDao;

    @Test
    public void testSaveWithoutTeamLead() {
        LOGGER.info("Testing of Team saving started");
        Team team = createTeam();

        Long savedId = teamDao.save(team);
        assertNotNull(savedId);
        LOGGER.info("Testing of Team saving finished");
    }

    @Test
    public void testSaveWithTeamLead() {
        LOGGER.info("Testing of Team saving started");

        Team team = createTeam();

        Advisor teamLead = jdoTestEntityDao.getObjectById(Advisor.class, 28176L); // "credit_advisor@backoffice.de"
        team.setTeamLead(teamLead);

        Long savedId = teamDao.save(team);
        assertNotNull(savedId);
        LOGGER.info("Testing of Team saving finished");
    }

    @Test
    public void testUpdateExistingTeam() {
        DataFixtures.executeScript(dataSource, "classpath:data/insert-casi-team.sql");
        Team existingTeam = jdoTestEntityDao.getLastInsertedObject(Team.class);
        assertNotNull(existingTeam);

        Long maxLegacyId = jdoTestEntityDao.getMaxId(Team.class);
        assertEquals(existingTeam.getId(), maxLegacyId);
        Long maxCasiId = jdoTestEntityDao.getMaxId(de.smava.casi.model.Team.class);

        String updatedName = "Test team - changed " + NOW;
        existingTeam.setName(updatedName);
        Long savedId = teamDao.save(existingTeam);
        assertNotNull(savedId);
        assertEquals(maxLegacyId, savedId);

        Long maxUpdatedCasiId = jdoTestEntityDao.getMaxId(de.smava.casi.model.Team.class);
        assertEquals(maxCasiId, maxUpdatedCasiId);
    }

    private Team createTeam() {
        Team team = new Team();
        team.setName("Test team " + NOW);
        team.setExpirationDate(addMonth(NOW, 2));
        return team;
    }

}
