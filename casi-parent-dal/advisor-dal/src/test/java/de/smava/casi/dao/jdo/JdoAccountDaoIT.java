package de.smava.casi.dao.jdo;

import de.smava.webapp.account.dao.AccountDao;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.account.domain.AccountRole;
import de.smava.webapp.account.domain.AccountRoleState;
import de.smava.webapp.account.domain.Person;
import de.smava.webapp.account.domain.abstracts.AbstractAccount;
import de.smava.webapp.commons.security.Role;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static de.smava.webapp.account.domain.AccountRoleState.STATE_ACCEPTED;
import static org.junit.Assert.assertNotNull;

@Ignore
@ContextConfiguration("classpath*:advisor-dao-test-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoAccountDaoIT {

    @Autowired
    private AccountDao accountDao;

    @Test
    public void testSaveAccount() {
        Account account = createAccount();
        Long savedId = accountDao.save(account);
        assertNotNull(savedId);
    }

    private Account createAccount() {
        Date now = new Date();
        String randomPart = UUID.randomUUID().toString().replace("-", "");

        Account account = new Account();

        Person person = new Person(Person.PRIMARY_PERSON_TYPE);
        person.setCreationDate(now);
        person.setFirstName("Johnny");
        person.setLastName("Smith");
        account.setPerson(person);
        account.setCreationDate(now);
        account.setEmail("johnny.smith." + randomPart + "@smava.de");
        account.setUsername("jsmith" + randomPart);

        AccountRoleState roleState = new AccountRoleState();
        roleState.setName(STATE_ACCEPTED);
        roleState.setValidFrom(new Date());

        AccountRole role = new AccountRole();
        role.setName(Role.ROLE_CREDIT_ADVISOR.name());
        role.setStates(Collections.singleton(roleState));
        account.setAccountRoles(Collections.singleton(role));
        account.setState(AbstractAccount.USER_STATE_ACTIVATED);

        return account;
    }
}
