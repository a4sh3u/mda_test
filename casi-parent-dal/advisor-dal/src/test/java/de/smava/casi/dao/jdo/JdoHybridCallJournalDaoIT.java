package de.smava.casi.dao.jdo;

import de.smava.webapp.applicant.casi.dao.CallJournalDao;
import de.smava.webapp.applicant.casi.domain.CallJournal;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;

import static org.junit.Assert.assertNotNull;

@Ignore
@ContextConfiguration(
        locations = {
                "classpath*:advisor-dao-test-context.xml"
        })
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoHybridCallJournalDaoIT {

    @Autowired
    private CallJournalDao callJournalDao;

    @Test
    public void testSaveCallJournal() {
        CallJournal callJournal = createJournal();
        Long savedId = callJournalDao.save(callJournal);
        assertNotNull(savedId);
    }

    private CallJournal createJournal() {
        CallJournal callJournal = new CallJournal();
        callJournal.setDestinationNumber("01234-7350015");
        callJournal.setAdvisorSmavaRefId(135L);
        callJournal.setLoanApplicationId(123456789L);
        callJournal.setCallDateTime(new Date());
        callJournal.setCallSuccessful(true);
        callJournal.setBorrowerId(85989L);
        return callJournal;
    }
}
