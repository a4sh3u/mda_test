package de.smava.casi.dao.jdo;

import de.smava.casi.data.DataFixtures;
import de.smava.casi.type.GroupState;
import de.smava.webapp.applicant.brokerage.dao.AdvisorGroupDao;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import de.smava.webapp.commons.dao.jdo.JdoTestEntityDao;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@Ignore
@ContextConfiguration("classpath*:advisor-dao-test-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoHybridAdvisorGroupDaoIT {

    private static final String LONG_GROUP_CONDITION = "#event.loanApplication.state.name() == \"APPLICATION_INCOMPLETE\" AND " +
            "#hasPersonalAdvisor == false " +
            "AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId) " +
            "AND NOT #event.saleAffiliateInfo.externalAffiliateId.equals(\"31dd9ba3\") AND " +
            "((#event.loanApplication.requestedAmount >= 5000 AND #event.loanApplication.requestedAmount <= 20000 " +
            "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 4)) OR " +
            "(#event.loanApplication.requestedAmount >= 20001 " +
            "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 8) " +
            "AND #event.loanApplication.requestedAmount <= 120000))";

    @Autowired
    public DataSource dataSource;

    @Autowired
    private JdoTestEntityDao jdoTestEntityDao;

    @Autowired
    private AdvisorGroupDao advisorGroupDao;

    @Test
    public void testSaveGroup() {
        String groupCondition = "#event.loanApplication.requestedAmount >= 0 " +
                "and #event.loanApplication.requestedAmount <= 3000 " +
                "AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)";
        AdvisorGroup group = createAdvisorGroup(groupCondition);

        Long maxCasiGroupId = jdoTestEntityDao.getMaxId(de.smava.casi.model.AdvisorGroup.class);

        Long savedId = advisorGroupDao.save(group);
        assertNotNull(savedId);

        jdoTestEntityDao.forceFlush();

        de.smava.casi.model.AdvisorGroup lastInsertedGroup = jdoTestEntityDao.getLastInsertedObject(de.smava.casi.model.AdvisorGroup.class);
        assertNotNull(lastInsertedGroup);
        assertFalse(lastInsertedGroup.getId().equals(maxCasiGroupId));
        assertEquals(group.getName(), lastInsertedGroup.getName());
    }

    @Test
    public void testSaveGroupWithVeryLongCondition() {
        AdvisorGroup group = createAdvisorGroup(LONG_GROUP_CONDITION);

        Long maxCasiGroupId = jdoTestEntityDao.getMaxId(de.smava.casi.model.AdvisorGroup.class);

        Long savedId = advisorGroupDao.save(group);
        assertNotNull(savedId);

        jdoTestEntityDao.forceFlush();

        de.smava.casi.model.AdvisorGroup lastInsertedGroup = jdoTestEntityDao.getLastInsertedObject(de.smava.casi.model.AdvisorGroup.class);
        assertNotNull(lastInsertedGroup);
        assertFalse(lastInsertedGroup.getId().equals(maxCasiGroupId));
        assertEquals(group.getName(), lastInsertedGroup.getName());
    }

    private AdvisorGroup createAdvisorGroup(String groupCondition) {
        AdvisorGroup group = new AdvisorGroup();
        group.setName("Test group " + new Date());
        group.setPriority(1);
        group.setCondition(groupCondition);
        return group;
    }

    @Test
    public void testUpdateExistingGroup() {
        DataFixtures.executeScript(dataSource, "classpath:data/insert-casi-group.sql");
        AdvisorGroup legacyGroup = jdoTestEntityDao.getLastInsertedObject(AdvisorGroup.class);
        assertNotNull(legacyGroup);

        Long maxLegacyId = jdoTestEntityDao.getMaxId(AdvisorGroup.class);
        assertEquals(maxLegacyId, legacyGroup.getId());

        Long maxNewId = jdoTestEntityDao.getMaxId(de.smava.casi.model.AdvisorGroup.class);
        legacyGroup.setName("Test group " + new Date());
        legacyGroup.setCondition(LONG_GROUP_CONDITION);
        Long savedId = advisorGroupDao.save(legacyGroup);
        assertNotNull(savedId);
        assertEquals(maxLegacyId, savedId);

        Long maxUpdatedId = jdoTestEntityDao.getMaxId(de.smava.casi.model.AdvisorGroup.class);
        assertEquals(maxNewId, maxUpdatedId);
    }

    @Test
    public void testDisableAdvisorGroup() {
        DataFixtures.executeScript(dataSource, "classpath:data/insert-casi-group.sql");
        AdvisorGroup legacyGroup = jdoTestEntityDao.getLastInsertedObject(AdvisorGroup.class);
        assertNotNull(legacyGroup);
        Long targetNewId = jdoTestEntityDao.getMaxId(de.smava.casi.model.AdvisorGroup.class);

        advisorGroupDao.disableAdvisorGroup(legacyGroup.getId());
        de.smava.casi.model.AdvisorGroup updatedCasiGroup = jdoTestEntityDao.getObjectById(de.smava.casi.model.AdvisorGroup.class, targetNewId);
        assertNotNull(updatedCasiGroup);
        assertEquals(GroupState.EXPIRED, updatedCasiGroup.getState());
    }
}
