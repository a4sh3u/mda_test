package de.smava.casi.converter;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.hamcrest.collection.IsMapContaining;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(DataProviderRunner.class)
public class AdvisorGroupPropertiesElConverterTest {

    @DataProvider
    public static Object[][] data() {
        return new Object[][]{
                {ElConverterDataItem.create("#event.loanApplication.state.name() == \"APPLICATION_INCOMPLETE\" " +
                        "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 22) " +
                        "AND #hasPersonalAdvisor == false " +
                        "AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId) " +
                        "AND NOT #event.saleAffiliateInfo.externalAffiliateId.equals(\"31dd9ba3\")")
                        .addProperty("isLoanApplicationIncomplete", true)
                        .addProperty("laAgeInHours", 22)
                        .addProperty("isEmptyPersonalAdvisor", true)
                        .addProperty("isFromOfflineAffiliate", false)
                        .addProperty("hasNotSaleAffiliateIds", Collections.singletonList("31dd9ba3"))
                },
                {ElConverterDataItem.create("#event.loanApplication.requestedAmount >= 0 " +
                        "and #event.loanApplication.requestedAmount <= 3000 " +
                        "AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)")
                        .addProperty("minRequestedAmount", 0L)
                        .addProperty("maxRequestedAmount", 3000L)
                        .addProperty("isFromOfflineAffiliate", false)
                },
                {ElConverterDataItem.create("#offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)")
                        .addProperty("isFromOfflineAffiliate", true)
                },
                {ElConverterDataItem.create("#event.saleAffiliateInfo.externalAffiliateId.equals(\"eedb3c4a\")")
                        .addProperty("hasSaleAffiliateIds", Collections.singletonList("eedb3c4a"))
                },
                {ElConverterDataItem.create("#event.loanApplication.state.name() == \"APPLICATION_INCOMPLETE\" " +
                        "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 22) " +
                        "AND #hasPersonalAdvisor == false " +
                        "AND #event.saleAffiliateInfo.externalAffiliateId.equals(\"31dd9ba3\")")
                        .addProperty("isLoanApplicationIncomplete", true)
                        .addProperty("laAgeInHours", 22)
                        .addProperty("isEmptyPersonalAdvisor", true)
                        .addProperty("hasSaleAffiliateIds", Collections.singletonList("31dd9ba3"))
                },
                {ElConverterDataItem.create("#event.loanApplication.state.name() == \"APPLICATION_INCOMPLETE\" " +
                        "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 8) " +
                        "AND #hasPersonalAdvisor == false " +
                        "AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId) " +
                        "AND NOT #event.saleAffiliateInfo.externalAffiliateId.equals(\"31dd9ba3\") " +
                        "AND (#event.loanApplication.requestedAmount >= 10000 " +
                        "AND #event.loanApplication.requestedAmount <= 120000)")
                        .addProperty("isLoanApplicationIncomplete", true)
                        .addProperty("laAgeInHours", 8)
                        .addProperty("isEmptyPersonalAdvisor", true)
                        .addProperty("isFromOfflineAffiliate", false)
                        .addProperty("hasNotSaleAffiliateIds", Collections.singletonList("31dd9ba3"))
                        .addProperty("minRequestedAmount", 10000L)
                        .addProperty("maxRequestedAmount", 120000L)
                        .skipElComparison()
                },
                {ElConverterDataItem.create("{'eedb3c4a', '31dd9ba3'}.contains(#event.saleAffiliateInfo.externalAffiliateId)")
                        .addProperty("hasSaleAffiliateIds", Arrays.asList("eedb3c4a", "31dd9ba3"))
                },
                {ElConverterDataItem.create("NOT {'eedb3c4a', '31dd9ba3'}.contains(#event.saleAffiliateInfo.externalAffiliateId)")
                        .addProperty("hasNotSaleAffiliateIds", Arrays.asList("eedb3c4a", "31dd9ba3"))
                },
                {ElConverterDataItem.create("#event.loanApplication.state.name() == \"APPLICATION_INCOMPLETE\" " +
                        "AND #hasPersonalAdvisor == false " +
                        "AND NOT #offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId) " +
                        "AND NOT #event.saleAffiliateInfo.externalAffiliateId.equals(\"31dd9ba3\") " +
                        "AND " +
                        "((#event.loanApplication.requestedAmount >= 10000 " +
                            "AND #event.loanApplication.requestedAmount <= 20000 " +
                            "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 1)) " +
                        "OR " +
                        "(#event.loanApplication.requestedAmount >= 20001 " +
                            "AND #root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, 8)))")
                        .addProperty("isLoanApplicationIncomplete", true)
                        .addProperty("isEmptyPersonalAdvisor", true)
                        .addProperty("isFromOfflineAffiliate", false)
                        .addProperty("hasNotSaleAffiliateIds", Collections.singletonList("31dd9ba3"))
                        .addProperty("complex", new LinkedList<HashMap<String, Object>>() {{
                                add(new LinkedHashMap<String, Object>() {{
                                    put("minRequestedAmount", 10000L);
                                    put("maxRequestedAmount", 20000L);
                                    put("laAgeInHours", 1);
                                }});
                                add(new LinkedHashMap<String, Object>() {{
                                    put("minRequestedAmount", 20001L);
                                    put("laAgeInHours", 8);
                                }});
                        }})
                }
        };
    }

    @Test
    @UseDataProvider("data")
    public void testConvertFromElExpression(ElConverterDataItem dataItem) {
        Map<String, Object> actualProperties = AdvisorGroupPropertiesElConverter.fromElExpression(dataItem.getElExpression());
        assertThat(actualProperties, is(notNullValue()));
        assertThat(actualProperties.size(), equalTo(dataItem.getProperties().size()));
        for (Map.Entry<String, Object> property : dataItem.getProperties().entrySet()) {
            assertThat(actualProperties, IsMapContaining.hasEntry(property.getKey(), property.getValue()));
        }
    }

    @Test
    @UseDataProvider("data")
    public void testConvertToElExpression(ElConverterDataItem dataItem) {

        String actualExpression = AdvisorGroupPropertiesElConverter.toElExpression(dataItem.getProperties());
        assertThat(actualExpression, is(notNullValue()));

        if (!dataItem.isSkipElComparison()) {
            assertThat(actualExpression.toUpperCase(), equalTo(dataItem.getElExpression().toUpperCase()));
        }
    }

    private static class ElConverterDataItem {
        String elExpression;
        Map<String, Object> properties;
        boolean skipElComparison;

        ElConverterDataItem(String elExpression, Map<String, Object> properties) {
            this.elExpression = elExpression;
            this.properties = properties;
            this.skipElComparison = false;
        }

        ElConverterDataItem addProperty(String key, Object value) {
            if (properties == null) {
                properties = new LinkedHashMap<String, Object>();
            }
            properties.put(key, value);
            return this;
        }

        /*
         * Fix for redundant usage of brackets. We can't exactly match strings there.
         */
        ElConverterDataItem skipElComparison() {
            this.skipElComparison = true;
            return this;
        }

        String getElExpression() {
            return elExpression;
        }

        Map<String, Object> getProperties() {
            return properties;
        }

        boolean isSkipElComparison() {
            return skipElComparison;
        }

        static ElConverterDataItem create(String elExpression) {
            return new ElConverterDataItem(elExpression, new LinkedHashMap<String, Object>());
        }

    }

}
