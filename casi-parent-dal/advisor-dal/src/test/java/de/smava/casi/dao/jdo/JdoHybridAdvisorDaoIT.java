package de.smava.casi.dao.jdo;

import de.smava.casi.dao.jdo.utils.PublicSequence;
import de.smava.casi.data.DataFixtures;
import de.smava.webapp.account.dao.AdvisorDao;
import de.smava.webapp.account.domain.*;
import de.smava.webapp.account.domain.abstracts.AbstractAccount;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoTestEntityDao;
import de.smava.webapp.commons.security.Role;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static de.smava.webapp.account.domain.AccountRoleState.STATE_ACCEPTED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Ignore
@ContextConfiguration("classpath*:advisor-dao-test-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoHybridAdvisorDaoIT {

    @Autowired
    public DataSource dataSource;

    @Autowired
    private AdvisorDao advisorDao;

    @Autowired
    private JdoTestEntityDao jdoTestEntityDao;

    @Autowired
    private PublicSequence publicSequence;

    @Test
    public void testSaveAdvisor() {
        Advisor advisor = createTestAdvisor();
        AdvisorAccount advisorAccount = advisor.getAccount();
        AdvisorPerson advisorPerson = advisorAccount.getPerson();
        jdoTestEntityDao.save(advisorPerson);
        jdoTestEntityDao.save(advisorAccount);

        Long savedId = advisorDao.save(advisor);
        assertNotNull(savedId);
    }

    @Test
    public void testUpdateAdvisor() {
        DataFixtures.executeScript(dataSource, "classpath:data/insert-casi-advisor.sql");
        Advisor existingAdvisor = jdoTestEntityDao.getLastInsertedObject(Advisor.class);
        assertNotNull(existingAdvisor);

        Long maxLegacyId = jdoTestEntityDao.getMaxId(Advisor.class);
        assertEquals(existingAdvisor.getId(), maxLegacyId);

        Long maxCasiId = jdoTestEntityDao.getMaxId(de.smava.casi.model.Advisor.class);
        existingAdvisor.setLocation(AdvisorLocationType.BERLIN);
        Long updatedId = advisorDao.save(existingAdvisor);
        assertNotNull(updatedId);
        assertEquals(maxLegacyId, updatedId);

        Long maxUpdatedCasiId = jdoTestEntityDao.getMaxId(de.smava.casi.model.Advisor.class);
        assertEquals(maxCasiId, maxUpdatedCasiId);
    }

    private Advisor createTestAdvisor() {
        AdvisorRoleState roleState = new AdvisorRoleState();
        roleState.setId(publicSequence.next());
        roleState.setName(STATE_ACCEPTED);
        roleState.setValidFrom(new Date());

        AdvisorRole role = new AdvisorRole();
        role.setId(publicSequence.next());
        role.setName(Role.ROLE_CREDIT_ADVISOR.name());
        role.setStates(Collections.singleton(roleState));

        AdvisorAccount account = new AdvisorAccount();
        account.setId(publicSequence.next());
        String randomPart = UUID.randomUUID().toString().replace("-", "");
        account.setEmail("johnny.smith." + randomPart + "@smava.de");
        account.setCreationDate(CurrentDate.getDate());
        account.setAccountRoles(Collections.singleton(role));
        account.setState(AbstractAccount.USER_STATE_ACTIVATED);
        account.setUsername("jsmith" + randomPart);

        AdvisorPerson person = new AdvisorPerson();
        person.setId(publicSequence.next());
        person.setFirstName("Johnny");
        person.setLastName("Smith");
        person.setCreationDate(CurrentDate.getDate());
        account.setPerson(person);

        Advisor advisor = new Advisor();
        advisor.setId(account.getId());
        advisor.setAccount(account);

        return advisor;
    }
}
