package de.smava.casi.dao.jdo;

import de.smava.casi.data.DataFixtures;
import de.smava.casi.type.GroupState;
import de.smava.webapp.applicant.brokerage.dao.AdvisorGroupAssignmentDao;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroupAssignment;
import de.smava.webapp.commons.dao.jdo.JdoTestEntityDao;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore
@ContextConfiguration("classpath*:advisor-dao-test-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager", defaultRollback = false)
@RunWith(SpringJUnit4ClassRunner.class)
public class JdoHybridAdvisorGroupAssignmentDaoIT {

    @Autowired
    private AdvisorGroupAssignmentDao advisorGroupAssignmentDao;

    @Autowired
    private JdoTestEntityDao jdoTestEntityDao;

    @Autowired
    public DataSource dataSource;

    @Test
    public void testSaveAdvisorGroupAssignment() {
        AdvisorGroupAssignment legacyGroupAssignment = new AdvisorGroupAssignment();
        legacyGroupAssignment.setAdvisorId(28176L); // "credit_advisor@backoffice.de"
        AdvisorGroup legacyGroup = jdoTestEntityDao.getObjectById(AdvisorGroup.class, 58389L);// "K1"
        legacyGroupAssignment.setAdvisorGroup(legacyGroup);
        legacyGroupAssignment.setCreationDate(new Date());

        Long savedId = advisorGroupAssignmentDao.save(legacyGroupAssignment);
        assertNotNull(savedId);
    }

    @Test
    public void testExpireAdvisorGroupAssignment() {
        DataFixtures.executeScript(dataSource, "classpath:data/insert-casi-advisor-with-group.sql");
        AdvisorGroupAssignment legacyGroupAssignment = jdoTestEntityDao.getLastInsertedObject(AdvisorGroupAssignment.class);
        assertNotNull(legacyGroupAssignment);

        advisorGroupAssignmentDao.expireAdvisorGroupAssignment(legacyGroupAssignment.getAdvisorId(),
                legacyGroupAssignment.getAdvisorGroup().getId());

        de.smava.casi.model.AdvisorGroupAssignment groupAssignment =
                jdoTestEntityDao.getLastInsertedObject(de.smava.casi.model.AdvisorGroupAssignment.class);
        assertNotNull(groupAssignment);
        assertTrue(groupAssignment.getState().equals(GroupState.EXPIRED));
    }
}
