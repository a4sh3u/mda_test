package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAccountDao;
import de.smava.webapp.account.domain.AdvisorAccount;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.springframework.stereotype.Repository;

@Repository
public class JdoCasiAccountDao extends JdoGenericDao<AdvisorAccount> implements CasiAccountDao {

    @Override
    public AdvisorAccount load(Long id) {
        return getPersistenceManager().getObjectById(AdvisorAccount.class, id);
    }

    @Override
    public Long save(AdvisorAccount advisorAccount) {
        return getPersistenceManager().makePersistent(advisorAccount).getId();
    }

    @Override
    public boolean exists(Long id) {
        return load(id) != null;
    }
}
