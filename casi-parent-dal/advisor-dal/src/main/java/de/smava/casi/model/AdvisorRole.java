package de.smava.casi.model;

import de.smava.casi.type.AdvisorRoleStateType;
import de.smava.casi.type.AdvisorRoleType;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public class AdvisorRole implements BaseEntity {
    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    private AdvisorRoleType _name;
    private AdvisorRoleStateType _state;
    private Date _creationDate;
    private Advisor _advisor;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public AdvisorRoleType getName() {
        return _name;
    }

    public void setName(AdvisorRoleType name) {
        this._name = name;
    }

    public AdvisorRoleStateType getState() {
        return _state;
    }

    public void setState(AdvisorRoleStateType state) {
        this._state = state;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this._creationDate = creationDate;
    }

    public Advisor getAdvisor() {
        return _advisor;
    }

    public void setAdvisor(Advisor advisor) {
        this._advisor = advisor;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    public static AdvisorRole crete(AdvisorRoleType roleType){
        AdvisorRole role = new AdvisorRole();
        role.setName(roleType);
        role.setCreationDate(CurrentDate.getDate());
        role.setState(AdvisorRoleStateType.APPLIED);
        return role;
    }
}
