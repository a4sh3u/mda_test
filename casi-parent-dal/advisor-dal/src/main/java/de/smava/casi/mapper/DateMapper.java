package de.smava.casi.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.Date;

@Mapper(componentModel = "spring", imports = Date.class)
public abstract class DateMapper {

    @Named("copyDate")
    public Date copyDate(Date sourceDate) {
        if(sourceDate != null){
            return new Date(sourceDate.getTime());
        }
        return null;
    }

}
