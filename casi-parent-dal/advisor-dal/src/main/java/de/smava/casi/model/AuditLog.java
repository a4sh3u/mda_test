package de.smava.casi.model;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public class AuditLog implements BaseEntity {

    private Long _id;

    private String _action;

    private String _entity;

    private Long _entityId;

    private Integer _entityVersion;

    private String _modifiedBy;

    private String _objectSnapshot;

    private Date _changeTime;

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public String getAction() {
        return _action;
    }

    public void setAction(String action) {
        this._action = action;
    }

    public String getEntity() {
        return _entity;
    }

    public void setEntity(String entity) {
        this._entity = entity;
    }

    public Long getEntityId() {
        return _entityId;
    }

    public void setEntityId(Long entityId) {
        this._entityId = entityId;
    }

    public Integer getEntityVersion() {
        return _entityVersion;
    }

    public void setEntityVersion(Integer entityVersion) {
        this._entityVersion = entityVersion;
    }

    public String getModifiedBy() {
        return _modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this._modifiedBy = modifiedBy;
    }

    public String getObjectSnapshot() {
        return _objectSnapshot;
    }

    public void setObjectSnapshot(String objectSnapshot) {
        this._objectSnapshot = objectSnapshot;
    }

    public Date getChangeTime() {
        return _changeTime;
    }

    public void setChangeTime(Date changeTime) {
        this._changeTime = changeTime;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }
}
