package de.smava.casi.converter;

import org.apache.commons.lang3.tuple.Pair;

public interface AdvisorGroupProperty {

    String getLabel();

    String getExpression();

    Class<?> getValueClass();

    boolean match(String condition);

    String constructCondition(Object value);

    Pair<String, Object> extractLabelAndValue(String condition);

}
