package de.smava.casi.type;

/**
 * The type of the assignment reflects the event where the assignment occured.
 * Created by aherr on 24.03.15.
 */
public enum AdvisorAssignmentType {

    /**
     * reflects {@link de.smava.webapp.brokerage.domain.EventQueueType#BORROWER_CREDIT_CONSULTING_FIRST_CALL}
     */
    FIRST_CALL,

    /**
     * reflects {@link de.smava.webapp.brokerage.domain.EventQueueType#BORROWER_CREDIT_CONSULTING_ADVISORY_NO_OFFER}
     */
    NO_OFFER,

    /**
     * manual changes by advisor
     */
    MANUAL_ADVISOR_CHANGE,

    /**
     * when customer creates a new loan application and the personal
     * advisor is deactivated or doesnt have casi advisor role anymore
     */
    AUTOMATIC_DEASSIGNMENT;
}
