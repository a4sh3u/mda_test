package de.smava.casi.dao;

import de.smava.webapp.account.domain.AdvisorAccount;

public interface CasiAccountDao {

    AdvisorAccount load(Long id);

}
