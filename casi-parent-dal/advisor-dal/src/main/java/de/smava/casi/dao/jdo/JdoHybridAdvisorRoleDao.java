package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAdvisorRoleDao;
import de.smava.casi.model.AdvisorRole;
import de.smava.webapp.account.dao.jdo.JdoAccountRoleDao;
import de.smava.webapp.account.domain.AccountRole;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary
@Repository
public class JdoHybridAdvisorRoleDao extends JdoAccountRoleDao implements CasiAdvisorRoleDao {

    @Override
    public Long save(AccountRole accountRole) {
        Long savedId = super.save(accountRole);
        return savedId;
    }

    @Override
    public Long save(AdvisorRole advisorRole) {
        return saveEntity(advisorRole);
    }
}
