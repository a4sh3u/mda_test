package de.smava.casi.mapper;

import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.account.domain.AdvisorAccount;
import org.mapstruct.*;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = DateMapper.class)
public abstract class AdvisorMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "team", ignore = true),
            @Mapping(target = "smavaId", source = "account.smavaId"),
            @Mapping(target = "email", source = "account.email"),
            @Mapping(target = "username", source = "account.username"),
            @Mapping(target = "firstName", source = "account.person.firstName"),
            @Mapping(target = "lastName", source = "account.person.lastName"),
            @Mapping(target = "phone", source = "account", qualifiedByName = "legacyPhoneToPhone"),
            @Mapping(target = "state", source = "account.state")
    })
    public abstract de.smava.casi.model.Advisor legacyAdvisorToAdvisor(Advisor source);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "team", ignore = true),
            @Mapping(target = "smavaId", source = "account.smavaId"),
            @Mapping(target = "email", source = "account.email"),
            @Mapping(target = "username", source = "account.username"),
            @Mapping(target = "firstName", source = "account.person.firstName"),
            @Mapping(target = "lastName", source = "account.person.lastName"),
            @Mapping(target = "phone", source = "account", qualifiedByName = "legacyPhoneToPhone"),
            @Mapping(target = "state", source = "account.state"),
            @Mapping(target = "blockedDate", source = "blockedDate", qualifiedByName = "copyDate"),
            @Mapping(target = "unlimitedPickingDate", source = "unlimitedPickingDate", qualifiedByName = "copyDate")
    })
    public abstract void updateAdvisorFromLegacyAdvisor(Advisor source, @MappingTarget de.smava.casi.model.Advisor target);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "team", ignore = true)
    })
    public abstract Advisor advisorToLegacyAdvisor(de.smava.casi.model.Advisor source);

    @Named("legacyPhoneToPhone")
    String legacyAccountPhoneToAdvisorPhone(AdvisorAccount account) {
        String legacyPhoneCode = account.getPhoneCode();
        String legacyPhone = account.getPhone();
        String advisorPhone = null;
        if (legacyPhoneCode != null) {
            advisorPhone = legacyPhoneCode;
        }
        if (legacyPhone != null) {
            advisorPhone = advisorPhone == null ? legacyPhone : advisorPhone + legacyPhone;
        }
        return advisorPhone;
    }
}
