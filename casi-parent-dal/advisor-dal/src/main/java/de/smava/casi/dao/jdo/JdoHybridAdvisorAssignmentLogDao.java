package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAdvisorDao;
import de.smava.casi.dao.CustomerAssignmentDao;
import de.smava.casi.mapper.AdvisorAssignmentLogMapper;
import de.smava.casi.model.Advisor;
import de.smava.casi.model.CustomerAssignment;
import de.smava.casi.type.AdvisorAssignmentType;
import de.smava.webapp.brokerage.dao.jdo.JdoAdvisorAssignmentLogDao;
import de.smava.webapp.brokerage.domain.AdvisorAssignmentLog;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import static de.smava.casi.type.AdvisorAssignmentType.*;

@Primary
@Repository(value = "advisorAssignmentLogDao")
public class JdoHybridAdvisorAssignmentLogDao extends JdoAdvisorAssignmentLogDao {

    private static final Logger LOGGER = Logger.getLogger(JdoHybridAdvisorAssignmentLogDao.class);

    @Autowired
    AdvisorAssignmentLogMapper advisorAssignmentLogMapper;

    @Autowired
    CasiAdvisorDao casiAdvisorDao;

    @Autowired
    CustomerAssignmentDao customerAssignmentDao;

    @Override
    public Long save(AdvisorAssignmentLog advisorAssignmentLog) {
        Long savedId = super.save(advisorAssignmentLog);
        Advisor assignedAdvisor = getCasiAdvisor(advisorAssignmentLog);
        de.smava.casi.model.AdvisorAssignmentLog newLog =
                mapLegacyAdvisorAssignmentLogToAdvisorAssignmentLog(advisorAssignmentLog, assignedAdvisor);
        handleCustomerAssignmentEntity(newLog, assignedAdvisor);
        saveInNewThread(newLog);
        return savedId;
    }

    private de.smava.casi.model.AdvisorAssignmentLog mapLegacyAdvisorAssignmentLogToAdvisorAssignmentLog(
            AdvisorAssignmentLog log, Advisor assignedAdvisor) {
        de.smava.casi.model.AdvisorAssignmentLog newLog =
                advisorAssignmentLogMapper.legacyAdvisorAssignmentLogToAdvisorAssignmentLog(log);

        newLog.setCreditAdvisorId(assignedAdvisor != null ? assignedAdvisor.getId() : null);

        de.smava.webapp.account.domain.Advisor legacyCreatedBy = log.getCreatedBy();
        if (legacyCreatedBy != null) {
            Long smavaId = legacyCreatedBy.getAccount().getSmavaId();
            Advisor advisor = casiAdvisorDao.getCasiAdvisorBySmavaId(smavaId);
            newLog.setCreatedById(advisor.getId());
        }
        return newLog;
    }

    private Advisor getCasiAdvisor(AdvisorAssignmentLog log) {
        de.smava.webapp.account.domain.Advisor legacyAdvisor = log.getCreditAdvisor();
        if (legacyAdvisor != null) {
            Long smavaId = legacyAdvisor.getAccount().getSmavaId();
            return casiAdvisorDao.getCasiAdvisorBySmavaId(smavaId);
        }
        return null;
    }

    private void handleCustomerAssignmentEntity(de.smava.casi.model.AdvisorAssignmentLog log, Advisor assignedAdvisor) {
        AdvisorAssignmentType customerType = log.getCustomerType();
        if (assignedAdvisor != null &&
                (NO_OFFER.equals(customerType) || FIRST_CALL.equals(customerType) || MANUAL_ADVISOR_CHANGE.equals(customerType)) ) {
            CustomerAssignment assignment = new CustomerAssignment();
            assignment.setAdvisor(assignedAdvisor);
            assignment.setBorrowerSmavaId(log.getCustomerNumber());
            if (MANUAL_ADVISOR_CHANGE.equals(customerType)) {
                customerAssignmentDao.deleteAssignment(log.getCustomerNumber());
            }
            customerAssignmentDao.save(assignment);
        }
        else if (AUTOMATIC_DEASSIGNMENT.equals(customerType) ||
                (MANUAL_ADVISOR_CHANGE.equals(customerType) && assignedAdvisor == null)) {
            customerAssignmentDao.deleteAssignment(log.getCustomerNumber());
        }
        else {
            LOGGER.warn("Can't recognize assignment event. Log: " + log);
        }
    }
}
