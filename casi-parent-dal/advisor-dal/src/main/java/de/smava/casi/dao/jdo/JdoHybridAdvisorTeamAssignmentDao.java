package de.smava.casi.dao.jdo;

import de.smava.webapp.account.dao.jdo.JdoAdvisorTeamAssignmentDao;
import de.smava.webapp.account.domain.AdvisorTeamAssignment;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary
@Repository(value = "advisorTeamAssignmentDao")
public class JdoHybridAdvisorTeamAssignmentDao extends JdoAdvisorTeamAssignmentDao {

    @Override
    public Long save(AdvisorTeamAssignment advisorTeamAssignment) {
       return getPersistenceManager().makePersistent(advisorTeamAssignment).getId();
    }

}
