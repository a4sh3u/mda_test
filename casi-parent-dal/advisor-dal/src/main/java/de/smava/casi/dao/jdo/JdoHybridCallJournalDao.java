package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAdvisorDao;
import de.smava.casi.mapper.CallJournalMapper;
import de.smava.casi.model.Advisor;
import de.smava.webapp.account.dao.AccountDao;
import de.smava.webapp.applicant.casi.dao.jdo.JdoCallJournalDao;
import de.smava.webapp.applicant.casi.domain.CallJournal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary
@Repository(value = "callJournalDao")
public class JdoHybridCallJournalDao extends JdoCallJournalDao {

    @Autowired
    CallJournalMapper callJournalMapper;

    @Autowired
    CasiAdvisorDao casiAdvisorDao;

    @Autowired
    AccountDao accountDao;

    @Override
    public Long save(CallJournal callJournal) {
        Long savedId = super.save(callJournal);
        de.smava.casi.model.CallJournal newCallJournal = mapLegacyCallJournalToCallJournal(callJournal);
        saveInNewThread(newCallJournal);
        return savedId;
    }

    private de.smava.casi.model.CallJournal mapLegacyCallJournalToCallJournal(CallJournal callJournal) {
        de.smava.casi.model.CallJournal newCallJournal = callJournalMapper.legacyCallJournalToCallJournal(callJournal);

        Long smavaRefId = callJournal.getAdvisorSmavaRefId();
        Advisor advisor = casiAdvisorDao.getCasiAdvisorBySmavaId(smavaRefId);
        newCallJournal.setAdvisorId(advisor.getId());

        long publicBorrowerId = callJournal.getBorrowerId();
        Long borrowerSmavaId = accountDao.getSmavaId(publicBorrowerId);
        newCallJournal.setBorrowerSmavaId(borrowerSmavaId);

        return newCallJournal;
    }
}
