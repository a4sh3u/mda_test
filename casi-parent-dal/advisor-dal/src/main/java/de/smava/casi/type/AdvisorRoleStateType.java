package de.smava.casi.type;

public enum AdvisorRoleStateType {
    ACCEPTED,
    APPLIED,
    DENIED,
    VOID,
}
