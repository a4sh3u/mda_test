package de.smava.casi.type;

public enum AdvisorRoleType {
    ROLE_CASI_GUEST,

    ROLE_CASI_ANALYTICS,

    ROLE_CREDIT_ADVISOR,

    ROLE_TEAM_LEAD,

    ROLE_CREDIT_ADVISORY_ADMIN
}