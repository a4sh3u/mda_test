package de.smava.casi.converter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import java.util.*;

public final class AdvisorGroupPropertiesElConverter {

    public static final String IS_LOAN_APPLICATION_INCOMPLETE_LABEL = "isLoanApplicationIncomplete";

    private static final Logger LOG = Logger.getLogger(AdvisorGroupPropertiesElConverter.class);
    private static final String CONDITION_SEPARATOR = " AND ";
    private static final String COMMA_SEPARATOR = ",";
    private static final String CONDITION_SEPARATOR_REGEX = "\\s((AND)|(and))\\s";
    private static final String COMPLEX_CONDITION_SEPARATOR_REGEX = "\\s((AND)|(and))\\s+\\(";
    private static final String NOT_OPERATOR = "NOT";
    private static final String EQUALS_OPERATOR = "==";
    private static final String NOT_EQUALS_OPERATOR = "!=";
    private static final String OR_OPERATOR = " OR ";
    private static final String PLACE_HOLDER = "???";
    private static final String OPENING_PARENTHESIS = "(";
    private static final String CLOSING_PARENTHESIS = ")";
    private static final String OPENING_BRACE = "{";
    private static final String CLOSING_BRACE = "}";
    private static final String SINGLE_QUOTE = "'";
    private static final String SPACE_CHAR = " ";

    private enum AdvisorGroupPropertyEnum implements AdvisorGroupProperty {
        MIN_REQUESTED_AMOUNT("minRequestedAmount", "#event.loanApplication.requestedAmount >= ", Number.class) {
            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                return getExpression() + value;
            }

            @Override
            protected Object extractValue(String condition) {
                String valueString = condition.replace(getExpression(), "")
                        .replace(OPENING_PARENTHESIS, "")
                        .replace(CLOSING_PARENTHESIS, "")
                        .trim();
                return Long.valueOf(valueString);
            }
        },
        MAX_REQUESTED_AMOUNT("maxRequestedAmount", "#event.loanApplication.requestedAmount <= ", Number.class) {
            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                return getExpression() + value;
            }

            @Override
            protected Object extractValue(String condition) {
                String valueString = condition.replace(getExpression(), "")
                        .replace(OPENING_PARENTHESIS, "")
                        .replace(CLOSING_PARENTHESIS, "")
                        .trim();
                return Long.valueOf(valueString);
            }
        },
        IS_INVITED_BY_OFFLINE_AFFILIATE_AGENT("isFromOfflineAffiliate",
                "#offlineAgentAffiliateIds.contains(#event.saleAffiliateInfo.externalAffiliateId)", Boolean.class) {
            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                boolean isInvitedByAgent = (Boolean) value;
                if (!isInvitedByAgent) {
                    return NOT_OPERATOR + " " + getExpression();
                }
                return getExpression();
            }

            @Override
            protected Object extractValue(String condition) {
                return !condition.contains(NOT_OPERATOR);
            }
        },
        IS_LOAN_APPLICATION_INCOMPLETE(IS_LOAN_APPLICATION_INCOMPLETE_LABEL,
                "#event.loanApplication.state.name() == \"APPLICATION_INCOMPLETE\"", Boolean.class) {
            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                boolean isInCompleteState = (Boolean) value;
                if (!isInCompleteState) {
                    return getExpression().replace(EQUALS_OPERATOR, NOT_EQUALS_OPERATOR);
                }
                return getExpression();
            }

            @Override
            protected Object extractValue(String condition) {
                return condition.contains(EQUALS_OPERATOR);
            }

            @Override
            public boolean match(String condition) {
                String unchangedPart = getExpression().substring(0, 34);
                return condition.contains(unchangedPart);
            }
        },
        IS_EMPTY_PERSONAL_ADVISOR("isEmptyPersonalAdvisor",
                "#hasPersonalAdvisor == false", Boolean.class) {
            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                boolean isEmptyAdvisor = (Boolean) value;
                if (!isEmptyAdvisor) {
                    return getExpression().replace(EQUALS_OPERATOR, NOT_EQUALS_OPERATOR);
                }
                return getExpression();
            }

            @Override
            protected Object extractValue(String condition) {
                return condition.contains(EQUALS_OPERATOR);
            }

            @Override
            public boolean match(String condition) {
                String unchangedPart = getExpression().substring(0, 19);
                return condition.contains(unchangedPart);
            }
        },
        HAS_SALE_AFFILIATE_IDS("hasSaleAffiliateIds",
                OPENING_BRACE + PLACE_HOLDER + CLOSING_BRACE + ".contains(#event.saleAffiliateInfo.externalAffiliateId)",
                "#event.saleAffiliateInfo.externalAffiliateId.equals(\"" + PLACE_HOLDER + "\")", List.class) {
            @Override
            @SuppressWarnings("unchecked")
            public String constructCondition(Object value) {
                validateValueType(value);
                return constructConditionFromCollection((List<String>) value, getAlternativeExpression(), getExpression());
            }

            @Override
            protected Object extractValue(String condition) {
                return extractValuesToList(condition, getAlternativeExpression(), getExpression());
            }

            @Override
            public boolean match(String condition) {
                return matchSingleCase(condition) || matchPluralCase(condition);
            }

            private boolean matchSingleCase(String condition) {
                String unchangedPart = getAlternativeExpression().substring(0, 53);
                String notOperator = condition.startsWith(OPENING_PARENTHESIS) ? OPENING_PARENTHESIS + NOT_OPERATOR
                        : NOT_OPERATOR;
                return condition.contains(unchangedPart) && !condition.startsWith(notOperator);
            }

            private boolean matchPluralCase(String condition) {
                int shift = PLACE_HOLDER.length() + 2;
                String unchangedPart = getExpression().substring(shift);
                return condition.startsWith(OPENING_BRACE) && condition.endsWith(unchangedPart);
            }
        },
        HAS_NOT_SALE_AFFILIATE_IDS("hasNotSaleAffiliateIds",
                "NOT {" + PLACE_HOLDER +"}.contains(#event.saleAffiliateInfo.externalAffiliateId)",
                "NOT #event.saleAffiliateInfo.externalAffiliateId.equals(\"" + PLACE_HOLDER + "\")", List.class) {
            @Override
            @SuppressWarnings("unchecked")
            public String constructCondition(Object value) {
                validateValueType(value);
                return constructConditionFromCollection((List<String>) value, getAlternativeExpression(), getExpression());
            }

            @Override
            protected Object extractValue(String condition) {
                return extractValuesToList(condition, getAlternativeExpression(), getExpression());
            }

            @Override
            public boolean match(String condition) {
                return matchSingleCase(condition) || matchPluralCase(condition);
            }

            private boolean matchSingleCase(String condition) {
                String unchangedPart = getAlternativeExpression().substring(0, 56);
                return condition.contains(unchangedPart);
            }

            private boolean matchPluralCase(String condition) {
                int shift = getExpression().indexOf(CLOSING_BRACE);
                String unchangedPart = getExpression().substring(shift);
                return condition.startsWith(NOT_OPERATOR + SPACE_CHAR + OPENING_BRACE) && condition.endsWith(unchangedPart);
            }
        },
        LA_AGE_IN_HOURS("laAgeInHours",
                "#root.datesDifferenceGreaterThanHours(#event.loanApplication.creationDate, #currentDate, " + PLACE_HOLDER + ")", Number.class) {
            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                Integer ageInHours = (Integer) value;
                return getExpression().replace(PLACE_HOLDER, ageInHours.toString());
            }

            @Override
            protected Object extractValue(String condition) {
                String valueString = condition.substring(89, condition.length() - 1);
                return Integer.valueOf(valueString);
            }

            @Override
            public boolean match(String condition) {
                String unchangedPart = getExpression().substring(0, 89);
                return condition.contains(unchangedPart);
            }
        },
        COMPLEX("complex", "", List.class) {
            @Override
            protected Object extractValue(String condition) {
                List<Map<String, Object>> propertiesCollection = new ArrayList<Map<String, Object>>();
                condition = normalizeCondition(condition);
                String[] parts = condition.split(OR_OPERATOR);
                for (String part : parts) {
                    part = normalizeCondition(part);
                    Map<String, Object> properties = createProperties(part.split(CONDITION_SEPARATOR_REGEX));
                    propertiesCollection.add(properties);
                }
                return propertiesCollection;
            }

            @Override
            public String constructCondition(Object value) {
                validateValueType(value);
                @SuppressWarnings("unchecked")
                List<Map<String, Object>> subConditions = (List<Map<String, Object>>) value;
                StringBuilder result = new StringBuilder("");
                if (!subConditions.isEmpty()) {
                    result.append(OPENING_PARENTHESIS);
                    for (Map<String, Object> subCondition : subConditions) {
                        if (result.length() > 1) {
                            result.append(OR_OPERATOR);
                        }
                        result.append(OPENING_PARENTHESIS);
                        result.append(toElExpression(subCondition));
                        result.append(CLOSING_PARENTHESIS);
                    }
                    result.append(CLOSING_PARENTHESIS);
                }
                return result.toString();
            }

            @Override
            public boolean match(String condition) {
                return false; // It is a special case which handled separately by extractComplexCondition(...)
            }
        }
        ;

        private String label;
        private String expression;
        private String alternativeExpression;
        private Class<?> valueClass;

        AdvisorGroupPropertyEnum(String label, String expression, Class<?> valueClass) {
            this.label = label;
            this.expression = expression;
            this.alternativeExpression = null;
            this.valueClass = valueClass;
        }

        AdvisorGroupPropertyEnum(String label, String expression, String alternativeExpression, Class<?> valueClass) {
            this.label = label;
            this.expression = expression;
            this.alternativeExpression = alternativeExpression;
            this.valueClass = valueClass;
        }

        public String getLabel() {
            return label;
        }

        public String getExpression() {
            return expression;
        }

        public String getAlternativeExpression() {
            return alternativeExpression;
        }

        public Class<?> getValueClass() {
            return valueClass;
        }

        public boolean match(String condition) {
            return condition.contains(expression);
        }

        public static AdvisorGroupPropertyEnum getAdvisorGroupPropertyByLabel(String label) {
            for (AdvisorGroupPropertyEnum property : AdvisorGroupPropertyEnum.values()) {
                if (property.getLabel().equals(label)) {
                    return property;
                }
            }
            return null;
        }

        public Pair<String, Object> extractLabelAndValue(String condition) {
            try {
                Object value = extractValue(condition);
                return Pair.of(getLabel(), value);
            }
            catch (RuntimeException ex) {
                LOG.error("Can't parse expression: " + condition);
                throw ex;
            }
        }

        protected abstract Object extractValue(String condition);

        protected void validateValueType(Object value) {
            Class<?> clazz = getValueClass();
            if (!clazz.isInstance(value)) {
                throw new IllegalArgumentException("Value [" + value + "] is not of proper type. Should be instance of " + clazz.getName());
            }
        }
    }

    private AdvisorGroupPropertiesElConverter() {
    }

    public static String toElExpression(Map<String, Object> properties) {
        if (properties != null && !properties.isEmpty()) {
            List<String> conditions = new ArrayList<String>();
            for (Map.Entry<String, Object> entry : properties.entrySet()) {
                String label = entry.getKey();
                AdvisorGroupPropertyEnum property = AdvisorGroupPropertyEnum.getAdvisorGroupPropertyByLabel(label);
                if (property != null) {
                    conditions.add(property.constructCondition(entry.getValue()));
                }
                else {
                    throw new IllegalArgumentException("Given property not recognized: " + label);
                }
            }
            return StringUtils.join(conditions, CONDITION_SEPARATOR);
        }
        return null;
    }

    public static Map<String, Object> fromElExpression(String expression) {
        Map<String, Object> properties = new LinkedHashMap<String, Object>();
        if (expression != null && !expression.isEmpty()) {
            Pair<String, List<Map<String, Object>>> result = extractComplexCondition(expression);
            String[] conditions = result.getLeft().split(CONDITION_SEPARATOR_REGEX);
            properties = createProperties(conditions);
            if (!result.getRight().isEmpty()) {
                properties.put(AdvisorGroupPropertyEnum.COMPLEX.getLabel(), result.getRight());
            }
        }
        return properties;
    }

    private static Map<String, Object> createProperties(String[] conditions) {
        Map<String, Object> properties = new LinkedHashMap<String, Object>();
        for (String condition : conditions) {
            boolean matched = false;
            for (AdvisorGroupProperty property : AdvisorGroupPropertyEnum.values()) {
                if (property.match(condition)) {
                    matched = true;
                    Pair<String, Object> labelAndValue = property.extractLabelAndValue(condition);
                    properties.put(labelAndValue.getLeft(), labelAndValue.getRight());
                    break;
                }
            }
            if (!matched) {
                throw new IllegalStateException("Not all properties were recognized. Unknown condition string: " + condition);
            }
        }
        return properties;
    }

    private static Pair<String, List<Map<String, Object>>> extractComplexCondition(String expression) {
        String[] subexpressions = expression.split(COMPLEX_CONDITION_SEPARATOR_REGEX);
        if (subexpressions.length == 2 && subexpressions[1].contains(OR_OPERATOR)) {
            @SuppressWarnings("unchecked")
            List<Map<String, Object>> propertiesCollection =
                    (List<Map<String, Object>>) AdvisorGroupPropertyEnum.COMPLEX.extractValue(OPENING_PARENTHESIS + subexpressions[1]);
            return ImmutablePair.of(subexpressions[0], propertiesCollection);
        }
        return ImmutablePair.of(expression, Collections.<Map<String,Object>>emptyList());
    }

    private static String constructConditionFromCollection(Collection<String> affiliateIds, String singleExpression, String pluralExpression) {
        StringBuilder result = new StringBuilder("");
        if (affiliateIds.size() == 1) {
            result.append(singleExpression.replace(PLACE_HOLDER, affiliateIds.iterator().next()));
        }
        else {
            String unchangedPluralPart = pluralExpression.substring(0, pluralExpression.indexOf(OPENING_BRACE) + 1);
            result.append(unchangedPluralPart);
            int minLength = unchangedPluralPart.length();
            for (String affiliateId : affiliateIds) {
                if (result.length() > minLength) {
                    result.append(COMMA_SEPARATOR).append(SPACE_CHAR);
                }
                result.append(SINGLE_QUOTE).append(affiliateId).append(SINGLE_QUOTE);
            }
            unchangedPluralPart = pluralExpression.substring(pluralExpression.indexOf(CLOSING_BRACE));
            result.append(unchangedPluralPart);
        }
        return result.toString();
    }

    private static List<String> extractValuesToList(String condition, String singleExpression, String pluralExpression) {
        condition = normalizeCondition(condition);
        List<String> values = new LinkedList<String>();
        String unchangedSinglePart = singleExpression.substring(0, singleExpression.indexOf(PLACE_HOLDER));

        if (condition.contains(unchangedSinglePart)) {
            values.add(extractValuesArea(condition, singleExpression));
        }
        else {
            condition = extractValuesArea(condition, pluralExpression);
            String[] subConditions = condition.split(COMMA_SEPARATOR);
            for (String subCondition : subConditions) {
                String value = subCondition.replace(SINGLE_QUOTE, "").trim();
                values.add(value);
            }
        }

        return values;
    }

    private static String extractValuesArea(String condition, String expression) {
        int valueBeginIndex = expression.indexOf(PLACE_HOLDER);
        int valueIndexFromEnd = expression.length() - valueBeginIndex - PLACE_HOLDER.length();
        return condition.substring(valueBeginIndex, condition.length() - valueIndexFromEnd);
    }

    private static String normalizeCondition(String condition) {
        if (condition.startsWith(OPENING_PARENTHESIS) && condition.endsWith(CLOSING_PARENTHESIS)) {
            return condition.substring(1, condition.length() - 1);
        }
        return condition;
    }

}
