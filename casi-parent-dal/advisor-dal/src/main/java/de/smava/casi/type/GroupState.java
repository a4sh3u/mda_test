package de.smava.casi.type;

public enum GroupState {
    ACTIVE,
    EXPIRED
}
