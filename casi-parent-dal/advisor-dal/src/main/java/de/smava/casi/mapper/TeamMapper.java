package de.smava.casi.mapper;

import de.smava.webapp.account.domain.Team;
import org.mapstruct.*;

@Mapper(componentModel = "spring", uses = DateMapper.class)
public abstract class TeamMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "legacyId", source = "id"),
            @Mapping(target = "teamLead", ignore = true),
            @Mapping(target = "advisors", ignore = true)
    })
    public abstract de.smava.casi.model.Team legacyTeamToTeam(Team legacyTeam);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "legacyId", source = "id"),
            @Mapping(target = "teamLead", ignore = true),
            @Mapping(target = "advisors", ignore = true),
            @Mapping(target = "expirationDate", source = "expirationDate", qualifiedByName = "copyDate")
    })
    public abstract void updateTeamFromLegacyTeam(Team sourceTeam, @MappingTarget de.smava.casi.model.Team targetTeam);

    public abstract Team teamToLegacyTeam(de.smava.casi.model.Team team);

}
