package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAdvisorDao;
import de.smava.casi.mapper.AdvisorGroupAssignmentMapper;
import de.smava.casi.model.AdvisorGroup;
import de.smava.webapp.account.dao.AccountDao;
import de.smava.webapp.applicant.brokerage.dao.jdo.JdoAdvisorGroupAssignmentDao;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroupAssignment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Primary
@Repository(value = "advisorGroupAssignmentDao")
public class JdoHybridAdvisorGroupAssignmentDao extends JdoAdvisorGroupAssignmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoHybridAdvisorGroupAssignmentDao.class);

    @Autowired
    AdvisorGroupAssignmentMapper advisorGroupAssignmentMapper;

    @Autowired
    CasiAdvisorDao casiAdvisorDao;

    @Autowired
    AccountDao accountDao;

    @Override
    public Long save(AdvisorGroupAssignment advisorGroupAssignment) {
        Long savedId = super.save(advisorGroupAssignment);
        de.smava.casi.model.AdvisorGroupAssignment newAssignment = mapLegacyAssignmentToAssignment(advisorGroupAssignment);
        saveInNewThread(newAssignment);
        return savedId;
    }

    @Override
    public void expireAdvisorGroupAssignment(Long legacyAdvisorId, Long legacyAdvisorGroupId) {
        super.expireAdvisorGroupAssignment(legacyAdvisorId, legacyAdvisorGroupId);
        try {
            Long smavaId = accountDao.getSmavaId(legacyAdvisorId);
            de.smava.casi.model.Advisor advisor = casiAdvisorDao.getCasiAdvisorBySmavaId(smavaId);
            AdvisorGroup advisorGroup = getAdvisorGroupByGroupLegacyId(legacyAdvisorGroupId);

            Query query = getPersistenceManager().newNamedQuery(de.smava.casi.model.AdvisorGroupAssignment.class,
                    "expireAdvisorGroupAssignment");

            Map<Integer, Object> parameters = new HashMap<Integer, Object>();
            parameters.put(1, advisor.getId());
            parameters.put(2, advisorGroup.getId());

            query.executeWithMap(parameters);
        }
        catch (Exception ex) {
            LOGGER.error("Error during CASI AdvisorGroupAssignment expiration", ex);
        }
    }

    private de.smava.casi.model.AdvisorGroupAssignment mapLegacyAssignmentToAssignment(AdvisorGroupAssignment advisorGroupAssignment) {
        try {
            de.smava.casi.model.AdvisorGroupAssignment newAssignment =
                    advisorGroupAssignmentMapper.legacyAdvisorGroupAssignmentToAdvisorGroupAssignment(advisorGroupAssignment);

            Long legacyAdvisorId = advisorGroupAssignment.getAdvisorId();
            Long smavaId = accountDao.getSmavaId(legacyAdvisorId);
            de.smava.casi.model.Advisor advisor = casiAdvisorDao.getCasiAdvisorBySmavaId(smavaId);
            newAssignment.setAdvisorId(advisor.getId());
            AdvisorGroup advisorGroup = getAdvisorGroupByGroupLegacyId(newAssignment.getLegacyGroupId());
            newAssignment.setAdvisorGroupId(advisorGroup.getId());
            return newAssignment;
        }
        catch (Exception ex) {
            LOGGER.error("Error during CASI AdvisorGroupAssignment mapping", ex);
            return null;
        }
    }

    private de.smava.casi.model.AdvisorGroup getAdvisorGroupByGroupLegacyId(Long legacyGroupId) {
        Query query = getPersistenceManager().newQuery(Query.SQL,
                "select * from casiadvisor.advisor_group where old_temp_id = ?");
        query.setClass(de.smava.casi.model.AdvisorGroup.class);
        query.setUnique(true);
        Map<Integer, Object> params = Collections.<Integer, Object>singletonMap(1, legacyGroupId);

        return (de.smava.casi.model.AdvisorGroup) query.executeWithMap(params);
    }
}
