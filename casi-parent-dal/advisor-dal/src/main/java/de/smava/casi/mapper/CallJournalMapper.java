package de.smava.casi.mapper;

import de.smava.webapp.applicant.casi.domain.CallJournal;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface CallJournalMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "advisorId", ignore = true)
    })
    de.smava.casi.model.CallJournal legacyCallJournalToCallJournal(CallJournal source);

    @Mappings({
            @Mapping(target = "id", ignore = true)
    })
    CallJournal callJournalToLegacyCallJournal(de.smava.casi.model.CallJournal source);
}
