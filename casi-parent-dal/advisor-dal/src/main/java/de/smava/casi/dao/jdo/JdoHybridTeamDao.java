package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAdvisorDao;
import de.smava.casi.mapper.TeamMapper;
import de.smava.webapp.account.dao.TeamDao;
import de.smava.webapp.account.dao.jdo.JdoTeamDao;
import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.account.domain.Team;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collections;

@Primary
@Repository(value = "teamDao")
public class JdoHybridTeamDao extends JdoTeamDao implements TeamDao {

    private static final Logger LOGGER = Logger.getLogger(JdoHybridTeamDao.class);

    @Autowired
    TeamMapper teamMapper;

    @Autowired
    CasiAdvisorDao casiAdvisorDao;

    @Override
    public Long save(Team team) {
        boolean isNewTeam = team.isNew();
        Long id = super.save(team);
        de.smava.casi.model.Team newTeam = mapLegacyTeamToTeam(team, isNewTeam);
        saveInNewThread(newTeam);
        return id;
    }

    private de.smava.casi.model.Team mapLegacyTeamToTeam(Team team, boolean isNewTeam) {
        de.smava.casi.model.Team newTeam;
        if (isNewTeam) {
            newTeam = teamMapper.legacyTeamToTeam(team);
            mapTeamLead(team, newTeam);
        }
        else {
            newTeam = getTeamByLegacyId(team.getId());
            if (newTeam != null) {
                teamMapper.updateTeamFromLegacyTeam(team, newTeam);
                mapTeamLead(team, newTeam);
            }
            else  {
                LOGGER.warn("Can't match new CASI team for legacy team ID: " + team.getId());
            }
        }
        return newTeam;
    }

    private void mapTeamLead(Team fromTeam, de.smava.casi.model.Team toTeam) {
        Advisor teamLead = fromTeam.getTeamLead();
        if (teamLead != null) {
            Long smavaRefId = teamLead.getAccount().getSmavaId();
            de.smava.casi.model.Advisor casiTeamLead = casiAdvisorDao.getCasiAdvisorBySmavaId(smavaRefId);
            if (casiTeamLead != null) {
                toTeam.setTeamLead(casiTeamLead);
            }
        }
        else if (toTeam.getTeamLead() != null) {
            toTeam.setTeamLead(null);
        }
    }

    private de.smava.casi.model.Team getTeamByLegacyId(long legacyTeamId) {
        Query query = getPersistenceManager().newQuery(Query.SQL, "SELECT id as id, name, expiration_date," +
                " team_lead_id, version, db_creation_date," +
                " db_modify_date, old_temp_id FROM casiadvisor.team WHERE old_temp_id = :legacyId;");
        query.setClass(de.smava.casi.model.Team.class);
        query.setUnique(true);

        return (de.smava.casi.model.Team) query.executeWithMap(Collections.singletonMap("legacyId", legacyTeamId));
    }

}
