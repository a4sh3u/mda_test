package de.smava.casi.model;

import de.smava.webapp.commons.domain.BaseEntity;

public class CustomerAssignment implements BaseEntity {

    private Long _id;
    private Advisor _advisor;
    private Long _borrowerSmavaId;

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public Advisor getAdvisor() {
        return _advisor;
    }

    public void setAdvisor(Advisor advisor) {
        this._advisor = advisor;
    }

    public Long getBorrowerSmavaId() {
        return _borrowerSmavaId;
    }

    public void setBorrowerSmavaId(Long borrowerSmavaId) {
        this._borrowerSmavaId = borrowerSmavaId;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }
}
