package de.smava.casi.type;

public enum AdvisorState {
    DEACTIVATED,
    ACTIVATED,
    DELETED
}
