package de.smava.casi.dao;

import de.smava.casi.model.Advisor;

public interface CasiAdvisorDao {

    Advisor getCasiAdvisorBySmavaId(Long smavaRefId);

}
