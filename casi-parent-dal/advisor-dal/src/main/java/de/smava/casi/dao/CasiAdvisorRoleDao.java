package de.smava.casi.dao;

import de.smava.casi.model.AdvisorRole;

public interface CasiAdvisorRoleDao {

    Long save(AdvisorRole advisorRole);

}
