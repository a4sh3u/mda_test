package de.smava.casi.model;

import de.smava.casi.type.GroupState;
import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public class AdvisorGroupAssignment implements BaseEntity {
    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    private Long _advisorId;
    private Long _advisorGroupId;
    private Date _creationDate;
    private GroupState _state;
    private transient Long legacyGroupId;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public Long getAdvisorId() {
        return _advisorId;
    }

    public void setAdvisorId(Long advisorId) {
        this._advisorId = advisorId;
    }

    public void setAdvisorGroupId(Long advisorGroupId) {
        _advisorGroupId = advisorGroupId;
    }

    public Long getAdvisorGroupId() {
        return _advisorGroupId;
    }

    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public GroupState getState() {
        return _state;
    }

    public void setState(GroupState state) {
        this._state = state;
    }

    public void setStateAsString(String stateString) {
        if (stateString != null && !stateString.isEmpty()) {
            this._state = GroupState.valueOf(stateString);
        }
    }

    public Long getLegacyGroupId() {
        return legacyGroupId;
    }

    public void setLegacyGroupId(Long legacyGroupId) {
        this.legacyGroupId = legacyGroupId;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }
}
