package de.smava.casi.mapper;

import de.smava.webapp.brokerage.domain.AdvisorAssignmentLog;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AdvisorAssignmentLogMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creditAdvisorId", ignore = true),
            @Mapping(target = "createdById", ignore = true)
    })
    de.smava.casi.model.AdvisorAssignmentLog legacyAdvisorAssignmentLogToAdvisorAssignmentLog(AdvisorAssignmentLog source);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "creditAdvisor", ignore = true),
            @Mapping(target = "createdBy", ignore = true)
    })
    AdvisorAssignmentLog advisorAssignmentLogToLegacyAdvisorAssignmentLog(de.smava.casi.model.AdvisorAssignmentLog source);
}
