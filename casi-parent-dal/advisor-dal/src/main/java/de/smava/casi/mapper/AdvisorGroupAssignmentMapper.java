package de.smava.casi.mapper;

import de.smava.webapp.applicant.brokerage.domain.AdvisorGroupAssignment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AdvisorGroupAssignmentMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "advisorId", ignore = true),
            @Mapping(target = "advisorGroupId", ignore = true),
            @Mapping(target = "legacyGroupId", source = "advisorGroup.id"),
            @Mapping(target = "stateAsString", expression = "java(source.getExpirationDate() == null ? \"ACTIVE\" : \"EXPIRED\")")
    })
    de.smava.casi.model.AdvisorGroupAssignment legacyAdvisorGroupAssignmentToAdvisorGroupAssignment(AdvisorGroupAssignment source);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "advisorId", ignore = true),
            @Mapping(target = "advisorGroup", ignore = true)
    })
    AdvisorGroupAssignment advisorGroupAssignmentToLegacyAdvisorGroupAssignment(de.smava.casi.model.AdvisorGroupAssignment source);
}
