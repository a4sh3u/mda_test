package de.smava.casi.type;

public enum AdvisorLocationType {
    BERLIN("Berlin"),
    HAMBURG("Hamburg"),
    LEIPZIG("Leipzig"),
    HOME_OFFICE("Home Office"),
    UNKNOWN("Unknown");

    private final String value;

    AdvisorLocationType(String value) {
        this.value = value;
    }

    public String value(){
        return value;
    }

}
