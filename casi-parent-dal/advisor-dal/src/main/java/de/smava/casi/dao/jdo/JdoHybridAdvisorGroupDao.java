package de.smava.casi.dao.jdo;

import de.smava.casi.converter.AdvisorGroupPropertiesElConverter;
import de.smava.casi.mapper.AdvisorGroupMapper;
import de.smava.webapp.applicant.brokerage.dao.jdo.JdoAdvisorGroupDao;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Primary
@Repository(value = "advisorGroupDao")
public class JdoHybridAdvisorGroupDao extends JdoAdvisorGroupDao {

    @Autowired
    private AdvisorGroupMapper advisorGroupMapper;

    @Override
    public Long save(AdvisorGroup advisorGroup) {
        boolean isNewGroup = advisorGroup.isNew();
        Long savedId = super.save(advisorGroup);
        de.smava.casi.model.AdvisorGroup newGroup = mapLegacyGroupToGroup(advisorGroup, isNewGroup);
        saveInNewThread(newGroup);
        return savedId;
    }

    @Override
    public void disableAdvisorGroup(Long id) {
        super.disableAdvisorGroup(id);

        Query query = getPersistenceManager().newQuery(Query.SQL, "update casiadvisor.advisor_group ag " +
                "set " +
                "state = 'EXPIRED' " +
                "where " +
                "ag.old_temp_id = ?;");

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, id);

        query.executeWithMap(parameters);
    }

    private de.smava.casi.model.AdvisorGroup mapLegacyGroupToGroup(AdvisorGroup legacyGroup, boolean isNewGroup) {
        de.smava.casi.model.AdvisorGroup casiGroup;
        if (isNewGroup) {
            casiGroup = advisorGroupMapper.legacyGroupToGroup(legacyGroup);
            mapProperties(legacyGroup, casiGroup);
        }
        else {
            casiGroup = getCasiAdvisorGroupByLegacyId(legacyGroup.getId());
            advisorGroupMapper.updateGroupFromLegacyGroup(legacyGroup, casiGroup);
            mapProperties(legacyGroup, casiGroup);
        }
        return casiGroup;
    }

    private void mapProperties(AdvisorGroup fromGroup, de.smava.casi.model.AdvisorGroup toGroup) {
        Map<String, Object> properties = AdvisorGroupPropertiesElConverter.fromElExpression(fromGroup.getCondition());
        toGroup.setProperties(properties);
    }

    private de.smava.casi.model.AdvisorGroup getCasiAdvisorGroupByLegacyId(long legacyGroupId) {
        Query query = getPersistenceManager().newQuery(Query.SQL, "SELECT id, name, description, priority, properties, " +
                "state, version, db_creation_date, db_modify_date, old_temp_id" +
                "  FROM casiadvisor.advisor_group WHERE old_temp_id = :legacyGroupId;");
        query.setClass(de.smava.casi.model.AdvisorGroup.class);
        query.setUnique(true);
        return (de.smava.casi.model.AdvisorGroup) query.executeWithMap(Collections.singletonMap("legacyGroupId", legacyGroupId));
    }

}
