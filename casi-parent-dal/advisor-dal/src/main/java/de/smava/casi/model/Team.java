package de.smava.casi.model;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Team implements BaseEntity {
    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    private String _name;
    private Date _expirationDate;
    private Advisor _teamLead;
    private Set<Advisor> _advisors = new HashSet<Advisor>();
    private Long _legacyId;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public Date getExpirationDate() {
        return _expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this._expirationDate = expirationDate;
    }

    public Advisor getTeamLead() {
        return _teamLead;
    }

    public void setTeamLead(Advisor teamLead) {
        this._teamLead = teamLead;
    }

    public Set<Advisor> getAdvisors() {
        return _advisors;
    }

    public void setAdvisors(Set<Advisor> advisors) {
        this._advisors = advisors;
    }

    public Long getLegacyId() {
        return _legacyId;
    }

    public void setLegacyId(Long legacyId) {
        this._legacyId = legacyId;
    }

    public void addAdvisor(Advisor advisor) {
        if (advisor != null) {
            advisor.setTeam(this);
            _advisors.add(advisor);
        }
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }
}
