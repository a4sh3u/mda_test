package de.smava.casi.model;

import java.io.Serializable;
import java.util.Date;

public class AdvisorTeamAssignment implements Serializable {
    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    private Advisor _advisor;
    private Team _team;
    private Date _creationDate;
    private Date _expirationDate;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public void setAdvisor(Advisor advisor) {
        _advisor = advisor;
    }

    public Advisor getAdvisor() {
        return _advisor;
    }

    public void setTeam(Team team) {
        _team = team;
    }

    public Team getTeam() {
        return _team;
    }

    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }

    public Date getExpirationDate() {
        return _expirationDate;
    }
}
