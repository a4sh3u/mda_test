package de.smava.casi.model;

import de.smava.casi.type.AdvisorLocationType;
import de.smava.casi.type.AdvisorRoleType;
import de.smava.casi.type.AdvisorState;
import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class Advisor implements BaseEntity {
    private static final long serialVersionUID = 236145197223517199L;
    public static final Integer DEFAULT_MAX_CUSTOMERS_PER_DAY = 50;

    public Advisor() {
        setLocation(AdvisorLocationType.UNKNOWN);
        setMaxCustomerPerDay(DEFAULT_MAX_CUSTOMERS_PER_DAY);
    }

    private Long _id;
    private Long _smavaId;
    private String _email;
    private String _username;
    private String _firstName;
    private String _lastName;
    private String _phone;
    private String _sip;
    private AdvisorLocationType _location;
    private Integer _maxCustomerPerDay;
    private Date _creationDate;
    private Date _blockedDate;
    private Date _unlimitedPickingDate;
    private AdvisorState _state;
    private Team _team;
    private Set<AdvisorRole> _roles = new HashSet<AdvisorRole>();


    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public Long getSmavaId() {
        return _smavaId;
    }

    public void setSmavaId(Long smavaId) {
        this._smavaId = smavaId;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String email) {
        this._email = email;
    }

    public String getUsername() {
        return _username;
    }

    public void setUsername(String username) {
        this._username = username;
    }

    public String getFirstName() {
        return _firstName;
    }

    public void setFirstName(String firstName) {
        this._firstName = firstName;
    }

    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        this._lastName = lastName;
    }

    public void setBlockedDate(Date blockedDate) {
        _blockedDate = blockedDate;
    }

    public Date getBlockedDate() {
        return _blockedDate;
    }

    public void setUnlimitedPickingDate(Date unlimitedPickingDate) {
        _unlimitedPickingDate = unlimitedPickingDate;
    }

    public Date getUnlimitedPickingDate() {
        return _unlimitedPickingDate;
    }

    public void setMaxCustomerPerDay(Integer maxCustomerPerDay) {
        _maxCustomerPerDay = maxCustomerPerDay;
    }

    public Integer getMaxCustomerPerDay() {
        return _maxCustomerPerDay;
    }

    public void setLocation(AdvisorLocationType location) {
        _location = location;
    }

    public AdvisorLocationType getLocation() {
        return _location;
    }

    public void setTeam(Team team) {
        _team = team;
    }

    public Team getTeam() {
        return _team;
    }

    public void setSip(String sip) {
        _sip = sip;
    }

    public String getSip() {
        return _sip;
    }

    public String getPhone() {
        return _phone;
    }

    public void setPhone(String phone) {
        this._phone = phone;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this._creationDate = creationDate;
    }

    public AdvisorState getState() {
        return _state;
    }

    public void setState(AdvisorState state) {
        this._state = state;
    }

    public Set<AdvisorRole> getRoles() {
        return _roles;
    }

    public void setRoles(Set<AdvisorRole> roles) {
        this._roles = roles;
    }

    public Set<AdvisorRoleType> getRoleNames() {
        Set<AdvisorRoleType> roleTypes = new HashSet<AdvisorRoleType>(_roles.size());
        for (AdvisorRole role : _roles) {
            roleTypes.add(role.getName());
        }
        return roleTypes;
    }

    public void addRole(AdvisorRole role){
        role.setAdvisor(this);
        _roles.add(role);
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }
}
