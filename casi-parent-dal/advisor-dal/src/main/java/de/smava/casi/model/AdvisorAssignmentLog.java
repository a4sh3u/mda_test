package de.smava.casi.model;

import de.smava.casi.type.AdvisorAssignmentType;
import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public class AdvisorAssignmentLog implements BaseEntity {
    private static final long serialVersionUID = 236145197223517199L;

    private Long _id;
    private Long _creditAdvisorId;
    private Long _customerNumber;
    private Long _loanApplicationId;
    private Date _assignmentDate;
    private AdvisorAssignmentType _customerType;
    private Long _createdById;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public Long getCreditAdvisorId() {
        return _creditAdvisorId;
    }

    public void setCreditAdvisorId(Long creditAdvisorId) {
        this._creditAdvisorId = creditAdvisorId;
    }

    public Long getCustomerNumber() {
        return _customerNumber;
    }

    public void setCustomerNumber(Long customerNumber) {
        this._customerNumber = customerNumber;
    }

    public Long getLoanApplicationId() {
        return _loanApplicationId;
    }

    public void setLoanApplicationId(Long loanApplicationId) {
        this._loanApplicationId = loanApplicationId;
    }

    public Date getAssignmentDate() {
        return _assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate) {
        this._assignmentDate = assignmentDate;
    }

    public AdvisorAssignmentType getCustomerType() {
        return _customerType;
    }

    public void setCustomerType(AdvisorAssignmentType customerType) {
        this._customerType = customerType;
    }

    public Long getCreatedById() {
        return _createdById;
    }

    public void setCreatedById(Long createdById) {
        this._createdById = createdById;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    @Override
    public String toString() {
        return "AdvisorAssignmentLog{" +
                "_id=" + _id +
                ", _creditAdvisorId=" + _creditAdvisorId +
                ", _customerNumber=" + _customerNumber +
                ", _loanApplicationId=" + _loanApplicationId +
                ", _assignmentDate=" + _assignmentDate +
                ", _customerType=" + _customerType +
                ", _createdById=" + _createdById +
                '}';
    }
}
