package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CustomerAssignmentDao;
import de.smava.casi.model.CustomerAssignment;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@Repository(value = "customerAssignmentDao")
public class JdoCustomerAssignmentDao extends JdoGenericDao<CustomerAssignment> implements CustomerAssignmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCustomerAssignmentDao.class);

    @Override
    public CustomerAssignment load(Long id) {
        String className = JdoCustomerAssignmentDao.class.getSimpleName();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("get " + className + " - id: " + id);
        }
        CustomerAssignment result = getPersistenceManager().getObjectById(CustomerAssignment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("get " + className + " - result: " + result);
        }
        return result;
    }

    @Override
    public Long save(CustomerAssignment customerAssignment) {
        return saveEntity(customerAssignment);
    }

    @Override
    public Long deleteAssignment(long borrowerSmavaId) {
        PersistenceManager persistenceManager = getPersistenceManager();
        Query query = persistenceManager.newQuery(de.smava.casi.model.Advisor.class);
        query.setFilter("this._borrowerSmavaId == :borrowerSmavaId");
        query.setUnique(true);
        query.setClass(CustomerAssignment.class);
        CustomerAssignment customerAssignment = (CustomerAssignment) query.execute(borrowerSmavaId);
        Long deletedId = null;
        if (customerAssignment != null) {
            deletedId = customerAssignment.getId();
            persistenceManager.deletePersistent(customerAssignment);
            persistenceManager.evict(customerAssignment);
            LOGGER.info("Customer Assignment with ID " + deletedId + " deleted.");
        }
        else {
            LOGGER.warn("Customer Assignment for borrower smava ID " + borrowerSmavaId + " not found for deletion");
        }
        return deletedId;
    }

    @Override
    public boolean exists(Long id) {
        return getPersistenceManager().getObjectById(CustomerAssignment.class, id) != null;
    }
}
