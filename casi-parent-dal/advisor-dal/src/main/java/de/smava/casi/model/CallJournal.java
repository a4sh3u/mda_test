package de.smava.casi.model;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public class CallJournal implements BaseEntity {

    private Long _id;

    private Long _advisorId;

    private Long _borrowerSmavaId;

    private String _destinationNumber;

    private Long _loanApplicationId;

    private Date _callDateTime;

    private Boolean _callSuccessful;

    private String _failureReason;

    @Override
    public Long getId() {
        return _id;
    }

    @Override
    public void setId(Long id) {
        this._id = id;
    }

    public Long getAdvisorId() {
        return _advisorId;
    }

    public void setAdvisorId(Long advisorId) {
        this._advisorId = advisorId;
    }

    public Long getBorrowerSmavaId() {
        return _borrowerSmavaId;
    }

    public void setBorrowerSmavaId(Long borrowerSmavaId) {
        this._borrowerSmavaId = borrowerSmavaId;
    }

    public String getDestinationNumber() {
        return _destinationNumber;
    }

    public void setDestinationNumber(String destinationNumber) {
        this._destinationNumber = destinationNumber;
    }

    public Long getLoanApplicationId() {
        return _loanApplicationId;
    }

    public void setLoanApplicationId(Long loanApplicationId) {
        this._loanApplicationId = loanApplicationId;
    }

    public Date getCallDateTime() {
        return _callDateTime;
    }

    public void setCallDateTime(Date callDateTime) {
        this._callDateTime = callDateTime;
    }

    public Boolean getCallSuccessful() {
        return _callSuccessful;
    }

    public void setCallSuccessful(Boolean callSuccessful) {
        this._callSuccessful = callSuccessful;
    }

    public String getFailureReason() {
        return _failureReason;
    }

    public void setFailureReason(String failureReason) {
        this._failureReason = failureReason;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }
}
