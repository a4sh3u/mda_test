package de.smava.casi.mapper;

import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface AdvisorGroupMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "legacyId", source = "id"),
            @Mapping(target = "properties", ignore = true),
            @Mapping(target = "stateAsString", expression = "java(source.getExpirationDate() == null ? \"ACTIVE\" : \"EXPIRED\")")
    })
    de.smava.casi.model.AdvisorGroup legacyGroupToGroup(AdvisorGroup source);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "legacyId", source = "id"),
            @Mapping(target = "properties", ignore = true),
            @Mapping(target = "stateAsString", expression = "java(source.getExpirationDate() == null ? \"ACTIVE\" : \"EXPIRED\")")
    })
    void updateGroupFromLegacyGroup(AdvisorGroup source, @MappingTarget de.smava.casi.model.AdvisorGroup target);

    @Mappings({
            @Mapping(target = "condition", ignore = true)
    })
    AdvisorGroup groupToLegacyGroup(de.smava.casi.model.AdvisorGroup source);

}
