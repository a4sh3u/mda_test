package de.smava.casi.dao.jdo;

import de.smava.casi.dao.CasiAdvisorDao;
import de.smava.casi.mapper.AdvisorMapper;
import de.smava.webapp.account.dao.jdo.JdoAdvisorDao;
import de.smava.webapp.account.domain.Advisor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;

@Primary
@Repository
public class JdoHybridAdvisorDao extends JdoAdvisorDao implements CasiAdvisorDao {

    private static final Logger LOGGER = Logger.getLogger(JdoHybridAdvisorDao.class);

    @Autowired
    AdvisorMapper advisorMapper;

    @Override
    public Long save(Advisor advisor) {
        Long persistedId = super.save(advisor);
        saveInNewThread(mapLegacyAdvisorToAdvisor(advisor));
        return persistedId;
    }

    @Override
    public de.smava.casi.model.Advisor getCasiAdvisorBySmavaId(Long smavaRefId) {
        Query query = getPersistenceManager().newQuery(de.smava.casi.model.Advisor.class);
        query.setFilter("this._smavaId == :smavaRefId");
        query.setUnique(true);
        query.setClass(de.smava.casi.model.Advisor.class);
        return (de.smava.casi.model.Advisor) query.execute(smavaRefId);
    }

    private de.smava.casi.model.Advisor mapLegacyAdvisorToAdvisor(Advisor advisor) {
        Long advisorSmavaId = advisor.getAccount().getSmavaId();
        if (advisorSmavaId == null) {
            LOGGER.warn("Can't create or find CASI advisor. SmavaId is NULL");
            return null;
        }
        de.smava.casi.model.Advisor casiAdvisor = getCasiAdvisorBySmavaId(advisorSmavaId);
        if (casiAdvisor == null) {
            casiAdvisor = advisorMapper.legacyAdvisorToAdvisor(advisor);
        }
        else {
            advisorMapper.updateAdvisorFromLegacyAdvisor(advisor, casiAdvisor);
        }
        return casiAdvisor;
    }

}
