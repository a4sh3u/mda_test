package de.smava.casi.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.smava.casi.type.GroupState;
import de.smava.webapp.commons.domain.BaseEntity;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AdvisorGroup implements BaseEntity {
    private static final long serialVersionUID = 236145197223517199L;
    private static final Logger LOGGER = Logger.getLogger(AdvisorGroup.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private Long _id;
    private String _name;
    private Integer _priority;
    private GroupState _state = GroupState.ACTIVE;
    private String _properties;
    private Long _legacyId;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public void setPriority(Integer priority) {
        _priority = priority;
    }

    public Integer getPriority() {
        return _priority;
    }

    public GroupState getState() {
        return _state;
    }

    public void setState(GroupState state) {
        this._state = state;
    }

    public void setStateAsString(String stateString) {
        if (stateString != null && !stateString.isEmpty()) {
            this._state = GroupState.valueOf(stateString);
        }
    }

    public Map<String, Object> getProperties() {
        try {
            GroupProperties groupProperties = MAPPER.readValue(_properties, GroupProperties.class);
            return groupProperties.conditions;
        } catch (IOException e) {
            LOGGER.error("Can't read group properties, Reason: " + e.getMessage());
            return new HashMap<String, Object>();
        }
    }

    public void setProperties(Map<String, Object> properties) {
        if (properties != null) {
            GroupProperties groupProperties = new GroupProperties(properties);
            try {
                _properties = MAPPER.writeValueAsString(groupProperties);
            } catch (JsonProcessingException e) {
                LOGGER.error("Can't set group properties, Reason: " + e.getMessage());
            }
        }
    }

    public Long getLegacyId() {
        return _legacyId;
    }

    public void setLegacyId(Long legacyId) {
        this._legacyId = legacyId;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    @Override
    public String toString() {
        return "AdvisorGroup{" +
                "_id=" + _id +
                ", _name='" + _name + '\'' +
                ", _priority=" + _priority +
                ", _state=" + _state +
                ", _properties='" + _properties + '\'' +
                '}';
    }

    private static class GroupProperties implements Serializable {

        Map<String, Object> conditions;

        GroupProperties() {
        }

        GroupProperties(Map<String, Object> conditions) {
            this.conditions = conditions;
        }

        public Map<String, Object> getConditions() {
            return conditions;
        }

        public void setConditions(Map<String, Object> conditions) {
            this.conditions = conditions;
        }
    }

}
