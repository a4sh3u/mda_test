package de.smava.casi.dao;

import de.smava.casi.model.CustomerAssignment;

public interface CustomerAssignmentDao {

    Long save(CustomerAssignment customerAssignment);

    Long deleteAssignment(long borrowerSmavaId);

}
