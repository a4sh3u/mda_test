//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.professional;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(smava professional)}
import java.io.Serializable;
import java.util.List;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'SmavaProfessionalForm'.
 *
 * @author generator
 */
public class SmavaProfessionalForm implements Serializable {

    public static final String AMOUNT_EXPR = "smavaProfessionalForm.amount";
    public static final String AMOUNT_PATH = "command.smavaProfessionalForm.amount";
    public static final String DURATIONS_EXPR = "smavaProfessionalForm.durations";
    public static final String DURATIONS_PATH = "command.smavaProfessionalForm.durations";
    public static final String CONFIRMED_EXPR = "smavaProfessionalForm.confirmed";
    public static final String CONFIRMED_PATH = "command.smavaProfessionalForm.confirmed";
    public static final String TRANSACTION_PIN_EXPR = "smavaProfessionalForm.transactionPin";
    public static final String TRANSACTION_PIN_PATH = "command.smavaProfessionalForm.transactionPin";
    public static final String TERMS_AND_CONDITIONS_EXPR = "smavaProfessionalForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_PATH = "command.smavaProfessionalForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_APPLIED_EXPR = "smavaProfessionalForm.termsAndConditionsApplied";
    public static final String TERMS_AND_CONDITIONS_APPLIED_PATH = "command.smavaProfessionalForm.termsAndConditionsApplied";
    public static final String INDIVIDUAL_FACTORIZATION_CONTRACT_CONFIRM_EXPR = "smavaProfessionalForm.individualFactorizationContractConfirm";
    public static final String INDIVIDUAL_FACTORIZATION_CONTRACT_CONFIRM_PATH = "command.smavaProfessionalForm.individualFactorizationContractConfirm";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(smava professional)}
    private static final long serialVersionUID = -8738786528999083733L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private List<String> _durations;
    private List<String> _durationsInitVal;
    private boolean _durationsIsSet;
    private Boolean _confirmed;
    private Boolean _confirmedInitVal;
    private boolean _confirmedIsSet;
    private String _transactionPin;
    private String _transactionPinInitVal;
    private boolean _transactionPinIsSet;
    private Boolean _termsAndConditions;
    private Boolean _termsAndConditionsInitVal;
    private boolean _termsAndConditionsIsSet;
    private Boolean _termsAndConditionsApplied;
    private Boolean _termsAndConditionsAppliedInitVal;
    private boolean _termsAndConditionsAppliedIsSet;
    private Boolean _individualFactorizationContractConfirm;
    private Boolean _individualFactorizationContractConfirmInitVal;
    private boolean _individualFactorizationContractConfirmIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setDurations(List<String> durations) {
        if (!_durationsIsSet) {
            _durationsIsSet = true;
            _durationsInitVal = durations;
        }
        _durations = durations;
    }

    public List<String> getDurations() {
        return _durations;
    }

    public List<String> durationsInitVal() {
        return _durationsInitVal;
    }

	public boolean durationsIsDirty() {
        return !valuesAreEqual(_durationsInitVal, _durations);
    }

    public boolean durationsIsSet() {
        return _durationsIsSet;
    }	


    public void setConfirmed(Boolean confirmed) {
        if (!_confirmedIsSet) {
            _confirmedIsSet = true;
            _confirmedInitVal = confirmed;
        }
        _confirmed = confirmed;
    }

    public Boolean getConfirmed() {
        return _confirmed;
    }

    public Boolean confirmedInitVal() {
        return _confirmedInitVal;
    }

	public boolean confirmedIsDirty() {
        return !valuesAreEqual(_confirmedInitVal, _confirmed);
    }

    public boolean confirmedIsSet() {
        return _confirmedIsSet;
    }	


    public void setTransactionPin(String transactionPin) {
        if (!_transactionPinIsSet) {
            _transactionPinIsSet = true;
            _transactionPinInitVal = transactionPin;
        }
        _transactionPin = transactionPin;
    }

    public String getTransactionPin() {
        return _transactionPin;
    }

    public String transactionPinInitVal() {
        return _transactionPinInitVal;
    }

	public boolean transactionPinIsDirty() {
        return !valuesAreEqual(_transactionPinInitVal, _transactionPin);
    }

    public boolean transactionPinIsSet() {
        return _transactionPinIsSet;
    }	


    public void setTermsAndConditions(Boolean termsAndConditions) {
        if (!_termsAndConditionsIsSet) {
            _termsAndConditionsIsSet = true;
            _termsAndConditionsInitVal = termsAndConditions;
        }
        _termsAndConditions = termsAndConditions;
    }

    public Boolean getTermsAndConditions() {
        return _termsAndConditions;
    }

    public Boolean termsAndConditionsInitVal() {
        return _termsAndConditionsInitVal;
    }

	public boolean termsAndConditionsIsDirty() {
        return !valuesAreEqual(_termsAndConditionsInitVal, _termsAndConditions);
    }

    public boolean termsAndConditionsIsSet() {
        return _termsAndConditionsIsSet;
    }	


    public void setTermsAndConditionsApplied(Boolean termsAndConditionsApplied) {
        if (!_termsAndConditionsAppliedIsSet) {
            _termsAndConditionsAppliedIsSet = true;
            _termsAndConditionsAppliedInitVal = termsAndConditionsApplied;
        }
        _termsAndConditionsApplied = termsAndConditionsApplied;
    }

    public Boolean getTermsAndConditionsApplied() {
        return _termsAndConditionsApplied;
    }

    public Boolean termsAndConditionsAppliedInitVal() {
        return _termsAndConditionsAppliedInitVal;
    }

	public boolean termsAndConditionsAppliedIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAppliedInitVal, _termsAndConditionsApplied);
    }

    public boolean termsAndConditionsAppliedIsSet() {
        return _termsAndConditionsAppliedIsSet;
    }	


    public void setIndividualFactorizationContractConfirm(Boolean individualFactorizationContractConfirm) {
        if (!_individualFactorizationContractConfirmIsSet) {
            _individualFactorizationContractConfirmIsSet = true;
            _individualFactorizationContractConfirmInitVal = individualFactorizationContractConfirm;
        }
        _individualFactorizationContractConfirm = individualFactorizationContractConfirm;
    }

    public Boolean getIndividualFactorizationContractConfirm() {
        return _individualFactorizationContractConfirm;
    }

    public Boolean individualFactorizationContractConfirmInitVal() {
        return _individualFactorizationContractConfirmInitVal;
    }

	public boolean individualFactorizationContractConfirmIsDirty() {
        return !valuesAreEqual(_individualFactorizationContractConfirmInitVal, _individualFactorizationContractConfirm);
    }

    public boolean individualFactorizationContractConfirmIsSet() {
        return _individualFactorizationContractConfirmIsSet;
    }	

}
