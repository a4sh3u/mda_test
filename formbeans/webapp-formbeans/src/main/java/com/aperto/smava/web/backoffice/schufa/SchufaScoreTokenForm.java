//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Tools\workspace\smava-webapp-parent\smava-webapp\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Tools\workspace\smava-webapp-parent\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.schufa;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(schufa score token)}
import java.io.Serializable;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'SchufaScoreTokenForm'.
 *
 * @author generator
 */
public class SchufaScoreTokenForm implements Serializable {

    public static final String TOKEN_ID_EXPR = "schufaScoreTokenForm.tokenId";
    public static final String TOKEN_ID_PATH = "command.schufaScoreTokenForm.tokenId";
    public static final String DESCRIPTION_EXPR = "schufaScoreTokenForm.description";
    public static final String DESCRIPTION_PATH = "command.schufaScoreTokenForm.description";
    public static final String TOKEN_CODE_EXPR = "schufaScoreTokenForm.tokenCode";
    public static final String TOKEN_CODE_PATH = "command.schufaScoreTokenForm.tokenCode";
    public static final String CURRENCY_EXPR = "schufaScoreTokenForm.currency";
    public static final String CURRENCY_PATH = "command.schufaScoreTokenForm.currency";
    public static final String AMOUNT_EXPR = "schufaScoreTokenForm.amount";
    public static final String AMOUNT_PATH = "command.schufaScoreTokenForm.amount";
    public static final String DATE_EXPR = "schufaScoreTokenForm.date";
    public static final String DATE_PATH = "command.schufaScoreTokenForm.date";
    public static final String RATE_TYPE_EXPR = "schufaScoreTokenForm.rateType";
    public static final String RATE_TYPE_PATH = "command.schufaScoreTokenForm.rateType";
    public static final String RATE_COUNT_EXPR = "schufaScoreTokenForm.rateCount";
    public static final String RATE_COUNT_PATH = "command.schufaScoreTokenForm.rateCount";
    public static final String ACCOUNT_NUMBER_EXPR = "schufaScoreTokenForm.accountNumber";
    public static final String ACCOUNT_NUMBER_PATH = "command.schufaScoreTokenForm.accountNumber";
    public static final String TOKEN_TEXT_EXPR = "schufaScoreTokenForm.tokenText";
    public static final String TOKEN_TEXT_PATH = "command.schufaScoreTokenForm.tokenText";
    public static final String TEXT_FIELD_EXPR = "schufaScoreTokenForm.textField";
    public static final String TEXT_FIELD_PATH = "command.schufaScoreTokenForm.textField";
    public static final String COMMENT_EXPR = "schufaScoreTokenForm.comment";
    public static final String COMMENT_PATH = "command.schufaScoreTokenForm.comment";
    public static final String FINISHED_EXPR = "schufaScoreTokenForm.finished";
    public static final String FINISHED_PATH = "command.schufaScoreTokenForm.finished";
    public static final String BUSINESS_EXPR = "schufaScoreTokenForm.business";
    public static final String BUSINESS_PATH = "command.schufaScoreTokenForm.business";
    public static final String CUSTOM_RATE_EXPR = "schufaScoreTokenForm.customRate";
    public static final String CUSTOM_RATE_PATH = "command.schufaScoreTokenForm.customRate";
    public static final String CALCULATED_RATE_EXPR = "schufaScoreTokenForm.calculatedRate";
    public static final String CALCULATED_RATE_PATH = "command.schufaScoreTokenForm.calculatedRate";
    public static final String IS_SMAVA_TOKEN_EXPR = "schufaScoreTokenForm.isSmavaToken";
    public static final String IS_SMAVA_TOKEN_PATH = "command.schufaScoreTokenForm.isSmavaToken";
    public static final String IS_REFINANCE_EXPR = "schufaScoreTokenForm.isRefinance";
    public static final String IS_REFINANCE_PATH = "command.schufaScoreTokenForm.isRefinance";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(schufa score token)}
    public void setCommentInitVal(String initVal) {
        _commentInitVal = initVal;
    }
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _tokenId;
    private Long _tokenIdInitVal;
    private boolean _tokenIdIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private String _tokenCode;
    private String _tokenCodeInitVal;
    private boolean _tokenCodeIsSet;
    private String _currency;
    private String _currencyInitVal;
    private boolean _currencyIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _date;
    private String _dateInitVal;
    private boolean _dateIsSet;
    private String _rateType;
    private String _rateTypeInitVal;
    private boolean _rateTypeIsSet;
    private String _rateCount;
    private String _rateCountInitVal;
    private boolean _rateCountIsSet;
    private String _accountNumber;
    private String _accountNumberInitVal;
    private boolean _accountNumberIsSet;
    private String _tokenText;
    private String _tokenTextInitVal;
    private boolean _tokenTextIsSet;
    private String _textField;
    private String _textFieldInitVal;
    private boolean _textFieldIsSet;
    private String _comment;
    private String _commentInitVal;
    private boolean _commentIsSet;
    private boolean _finished;
    private boolean _finishedInitVal;
    private boolean _finishedIsSet;
    private boolean _business;
    private boolean _businessInitVal;
    private boolean _businessIsSet;
    private String _customRate;
    private String _customRateInitVal;
    private boolean _customRateIsSet;
    private String _calculatedRate;
    private String _calculatedRateInitVal;
    private boolean _calculatedRateIsSet;
    private boolean _isSmavaToken;
    private boolean _isSmavaTokenInitVal;
    private boolean _isSmavaTokenIsSet;
    private boolean _isRefinance;
    private boolean _isRefinanceInitVal;
    private boolean _isRefinanceIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setTokenId(Long tokenId) {
        if (!_tokenIdIsSet) {
            _tokenIdIsSet = true;
            _tokenIdInitVal = tokenId;
        }
        _tokenId = tokenId;
    }

    public Long getTokenId() {
        return _tokenId;
    }

    public Long tokenIdInitVal() {
        return _tokenIdInitVal;
    }

	public boolean tokenIdIsDirty() {
        return !valuesAreEqual(_tokenIdInitVal, _tokenId);
    }

    public boolean tokenIdIsSet() {
        return _tokenIdIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setTokenCode(String tokenCode) {
        if (!_tokenCodeIsSet) {
            _tokenCodeIsSet = true;
            _tokenCodeInitVal = tokenCode;
        }
        _tokenCode = tokenCode;
    }

    public String getTokenCode() {
        return _tokenCode;
    }

    public String tokenCodeInitVal() {
        return _tokenCodeInitVal;
    }

	public boolean tokenCodeIsDirty() {
        return !valuesAreEqual(_tokenCodeInitVal, _tokenCode);
    }

    public boolean tokenCodeIsSet() {
        return _tokenCodeIsSet;
    }	


    public void setCurrency(String currency) {
        if (!_currencyIsSet) {
            _currencyIsSet = true;
            _currencyInitVal = currency;
        }
        _currency = currency;
    }

    public String getCurrency() {
        return _currency;
    }

    public String currencyInitVal() {
        return _currencyInitVal;
    }

	public boolean currencyIsDirty() {
        return !valuesAreEqual(_currencyInitVal, _currency);
    }

    public boolean currencyIsSet() {
        return _currencyIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setDate(String date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = date;
        }
        _date = date;
    }

    public String getDate() {
        return _date;
    }

    public String dateInitVal() {
        return _dateInitVal;
    }

	public boolean dateIsDirty() {
        return !valuesAreEqual(_dateInitVal, _date);
    }

    public boolean dateIsSet() {
        return _dateIsSet;
    }	


    public void setRateType(String rateType) {
        if (!_rateTypeIsSet) {
            _rateTypeIsSet = true;
            _rateTypeInitVal = rateType;
        }
        _rateType = rateType;
    }

    public String getRateType() {
        return _rateType;
    }

    public String rateTypeInitVal() {
        return _rateTypeInitVal;
    }

	public boolean rateTypeIsDirty() {
        return !valuesAreEqual(_rateTypeInitVal, _rateType);
    }

    public boolean rateTypeIsSet() {
        return _rateTypeIsSet;
    }	


    public void setRateCount(String rateCount) {
        if (!_rateCountIsSet) {
            _rateCountIsSet = true;
            _rateCountInitVal = rateCount;
        }
        _rateCount = rateCount;
    }

    public String getRateCount() {
        return _rateCount;
    }

    public String rateCountInitVal() {
        return _rateCountInitVal;
    }

	public boolean rateCountIsDirty() {
        return !valuesAreEqual(_rateCountInitVal, _rateCount);
    }

    public boolean rateCountIsSet() {
        return _rateCountIsSet;
    }	


    public void setAccountNumber(String accountNumber) {
        if (!_accountNumberIsSet) {
            _accountNumberIsSet = true;
            _accountNumberInitVal = accountNumber;
        }
        _accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return _accountNumber;
    }

    public String accountNumberInitVal() {
        return _accountNumberInitVal;
    }

	public boolean accountNumberIsDirty() {
        return !valuesAreEqual(_accountNumberInitVal, _accountNumber);
    }

    public boolean accountNumberIsSet() {
        return _accountNumberIsSet;
    }	


    public void setTokenText(String tokenText) {
        if (!_tokenTextIsSet) {
            _tokenTextIsSet = true;
            _tokenTextInitVal = tokenText;
        }
        _tokenText = tokenText;
    }

    public String getTokenText() {
        return _tokenText;
    }

    public String tokenTextInitVal() {
        return _tokenTextInitVal;
    }

	public boolean tokenTextIsDirty() {
        return !valuesAreEqual(_tokenTextInitVal, _tokenText);
    }

    public boolean tokenTextIsSet() {
        return _tokenTextIsSet;
    }	


    public void setTextField(String textField) {
        if (!_textFieldIsSet) {
            _textFieldIsSet = true;
            _textFieldInitVal = textField;
        }
        _textField = textField;
    }

    public String getTextField() {
        return _textField;
    }

    public String textFieldInitVal() {
        return _textFieldInitVal;
    }

	public boolean textFieldIsDirty() {
        return !valuesAreEqual(_textFieldInitVal, _textField);
    }

    public boolean textFieldIsSet() {
        return _textFieldIsSet;
    }	


    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = comment;
        }
        _comment = comment;
    }

    public String getComment() {
        return _comment;
    }

    public String commentInitVal() {
        return _commentInitVal;
    }

	public boolean commentIsDirty() {
        return !valuesAreEqual(_commentInitVal, _comment);
    }

    public boolean commentIsSet() {
        return _commentIsSet;
    }	


    public void setFinished(boolean finished) {
        if (!_finishedIsSet) {
            _finishedIsSet = true;
            _finishedInitVal = finished;
        }
        _finished = finished;
    }

    public boolean getFinished() {
        return _finished;
    }

    public boolean finishedInitVal() {
        return _finishedInitVal;
    }

	public boolean finishedIsDirty() {
        return !valuesAreEqual(_finishedInitVal, _finished);
    }

    public boolean finishedIsSet() {
        return _finishedIsSet;
    }	


    public void setBusiness(boolean business) {
        if (!_businessIsSet) {
            _businessIsSet = true;
            _businessInitVal = business;
        }
        _business = business;
    }

    public boolean getBusiness() {
        return _business;
    }

    public boolean businessInitVal() {
        return _businessInitVal;
    }

	public boolean businessIsDirty() {
        return !valuesAreEqual(_businessInitVal, _business);
    }

    public boolean businessIsSet() {
        return _businessIsSet;
    }	


    public void setCustomRate(String customRate) {
        if (!_customRateIsSet) {
            _customRateIsSet = true;
            _customRateInitVal = customRate;
        }
        _customRate = customRate;
    }

    public String getCustomRate() {
        return _customRate;
    }

    public String customRateInitVal() {
        return _customRateInitVal;
    }

	public boolean customRateIsDirty() {
        return !valuesAreEqual(_customRateInitVal, _customRate);
    }

    public boolean customRateIsSet() {
        return _customRateIsSet;
    }	


    public void setCalculatedRate(String calculatedRate) {
        if (!_calculatedRateIsSet) {
            _calculatedRateIsSet = true;
            _calculatedRateInitVal = calculatedRate;
        }
        _calculatedRate = calculatedRate;
    }

    public String getCalculatedRate() {
        return _calculatedRate;
    }

    public String calculatedRateInitVal() {
        return _calculatedRateInitVal;
    }

	public boolean calculatedRateIsDirty() {
        return !valuesAreEqual(_calculatedRateInitVal, _calculatedRate);
    }

    public boolean calculatedRateIsSet() {
        return _calculatedRateIsSet;
    }	


    public void setIsSmavaToken(boolean isSmavaToken) {
        if (!_isSmavaTokenIsSet) {
            _isSmavaTokenIsSet = true;
            _isSmavaTokenInitVal = isSmavaToken;
        }
        _isSmavaToken = isSmavaToken;
    }

    public boolean getIsSmavaToken() {
        return _isSmavaToken;
    }

    public boolean isSmavaTokenInitVal() {
        return _isSmavaTokenInitVal;
    }

	public boolean isSmavaTokenIsDirty() {
        return !valuesAreEqual(_isSmavaTokenInitVal, _isSmavaToken);
    }

    public boolean isSmavaTokenIsSet() {
        return _isSmavaTokenIsSet;
    }	


    public void setIsRefinance(boolean isRefinance) {
        if (!_isRefinanceIsSet) {
            _isRefinanceIsSet = true;
            _isRefinanceInitVal = isRefinance;
        }
        _isRefinance = isRefinance;
    }

    public boolean getIsRefinance() {
        return _isRefinance;
    }

    public boolean isRefinanceInitVal() {
        return _isRefinanceInitVal;
    }

	public boolean isRefinanceIsDirty() {
        return !valuesAreEqual(_isRefinanceInitVal, _isRefinance);
    }

    public boolean isRefinanceIsSet() {
        return _isRefinanceIsSet;
    }	

}
