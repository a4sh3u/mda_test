//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.system;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo bid search configuration)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoBidSearchConfigurationForm'.
 *
 * @author generator
 */
public class BoBidSearchConfigurationForm implements Serializable {

    public static final String BID_SEARCH_MAX_INVEST_LIMIT_EXPR = "boBidSearchConfigurationForm.bidSearchMaxInvestLimit";
    public static final String BID_SEARCH_MAX_INVEST_LIMIT_PATH = "command.boBidSearchConfigurationForm.bidSearchMaxInvestLimit";
    public static final String BID_SEARCH_TIME_MATCH_1_EXPR = "boBidSearchConfigurationForm.bidSearchTimeMatch1";
    public static final String BID_SEARCH_TIME_MATCH_1_PATH = "command.boBidSearchConfigurationForm.bidSearchTimeMatch1";
    public static final String BID_SEARCH_TIME_MATCH_2_EXPR = "boBidSearchConfigurationForm.bidSearchTimeMatch2";
    public static final String BID_SEARCH_TIME_MATCH_2_PATH = "command.boBidSearchConfigurationForm.bidSearchTimeMatch2";
    public static final String BID_SEARCH_TIME_MATCH_3_EXPR = "boBidSearchConfigurationForm.bidSearchTimeMatch3";
    public static final String BID_SEARCH_TIME_MATCH_3_PATH = "command.boBidSearchConfigurationForm.bidSearchTimeMatch3";
    public static final String BID_SEARCH_BARRIER_1_EXPR = "boBidSearchConfigurationForm.bidSearchBarrier1";
    public static final String BID_SEARCH_BARRIER_1_PATH = "command.boBidSearchConfigurationForm.bidSearchBarrier1";
    public static final String BID_SEARCH_BARRIER_2_EXPR = "boBidSearchConfigurationForm.bidSearchBarrier2";
    public static final String BID_SEARCH_BARRIER_2_PATH = "command.boBidSearchConfigurationForm.bidSearchBarrier2";
    public static final String BID_SEARCH_BARRIER_3_EXPR = "boBidSearchConfigurationForm.bidSearchBarrier3";
    public static final String BID_SEARCH_BARRIER_3_PATH = "command.boBidSearchConfigurationForm.bidSearchBarrier3";
    public static final String ACTIVATE_BID_SEARCH_EXPR = "boBidSearchConfigurationForm.activateBidSearch";
    public static final String ACTIVATE_BID_SEARCH_PATH = "command.boBidSearchConfigurationForm.activateBidSearch";
    public static final String RAPID_MATCHING_BUFFER_PERCENT_EXPR = "boBidSearchConfigurationForm.rapidMatchingBufferPercent";
    public static final String RAPID_MATCHING_BUFFER_PERCENT_PATH = "command.boBidSearchConfigurationForm.rapidMatchingBufferPercent";
    public static final String RAPID_MATCHING_BUFFER_AMOUNT_EXPR = "boBidSearchConfigurationForm.rapidMatchingBufferAmount";
    public static final String RAPID_MATCHING_BUFFER_AMOUNT_PATH = "command.boBidSearchConfigurationForm.rapidMatchingBufferAmount";
    public static final String RAPID_MATCHING_MAX_INTEREST_EXPR = "boBidSearchConfigurationForm.rapidMatchingMaxInterest";
    public static final String RAPID_MATCHING_MAX_INTEREST_PATH = "command.boBidSearchConfigurationForm.rapidMatchingMaxInterest";
    public static final String AVAIL_AMOUNT_CACHE_EXPIRY_EXPR = "boBidSearchConfigurationForm.availAmountCacheExpiry";
    public static final String AVAIL_AMOUNT_CACHE_EXPIRY_PATH = "command.boBidSearchConfigurationForm.availAmountCacheExpiry";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo bid search configuration)}
    /**
     * generated serial.
     */
	private static final long serialVersionUID = -7366807358879111474L;
// !!!!!!!! End of insert code section !!!!!!!!

    private Double _bidSearchMaxInvestLimit;
    private Double _bidSearchMaxInvestLimitInitVal;
    private boolean _bidSearchMaxInvestLimitIsSet;
    private Integer _bidSearchTimeMatch1;
    private Integer _bidSearchTimeMatch1InitVal;
    private boolean _bidSearchTimeMatch1IsSet;
    private Integer _bidSearchTimeMatch2;
    private Integer _bidSearchTimeMatch2InitVal;
    private boolean _bidSearchTimeMatch2IsSet;
    private Integer _bidSearchTimeMatch3;
    private Integer _bidSearchTimeMatch3InitVal;
    private boolean _bidSearchTimeMatch3IsSet;
    private Integer _bidSearchBarrier1;
    private Integer _bidSearchBarrier1InitVal;
    private boolean _bidSearchBarrier1IsSet;
    private Integer _bidSearchBarrier2;
    private Integer _bidSearchBarrier2InitVal;
    private boolean _bidSearchBarrier2IsSet;
    private Integer _bidSearchBarrier3;
    private Integer _bidSearchBarrier3InitVal;
    private boolean _bidSearchBarrier3IsSet;
    private Boolean _activateBidSearch;
    private Boolean _activateBidSearchInitVal;
    private boolean _activateBidSearchIsSet;
    private double _rapidMatchingBufferPercent;
    private double _rapidMatchingBufferPercentInitVal;
    private boolean _rapidMatchingBufferPercentIsSet;
    private double _rapidMatchingBufferAmount;
    private double _rapidMatchingBufferAmountInitVal;
    private boolean _rapidMatchingBufferAmountIsSet;
    private double _rapidMatchingMaxInterest;
    private double _rapidMatchingMaxInterestInitVal;
    private boolean _rapidMatchingMaxInterestIsSet;
    private Integer _availAmountCacheExpiry;
    private Integer _availAmountCacheExpiryInitVal;
    private boolean _availAmountCacheExpiryIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setBidSearchMaxInvestLimit(Double bidSearchMaxInvestLimit) {
        if (!_bidSearchMaxInvestLimitIsSet) {
            _bidSearchMaxInvestLimitIsSet = true;
            _bidSearchMaxInvestLimitInitVal = bidSearchMaxInvestLimit;
        }
        _bidSearchMaxInvestLimit = bidSearchMaxInvestLimit;
    }

    public Double getBidSearchMaxInvestLimit() {
        return _bidSearchMaxInvestLimit;
    }

    public Double bidSearchMaxInvestLimitInitVal() {
        return _bidSearchMaxInvestLimitInitVal;
    }

	public boolean bidSearchMaxInvestLimitIsDirty() {
        return !valuesAreEqual(_bidSearchMaxInvestLimitInitVal, _bidSearchMaxInvestLimit);
    }

    public boolean bidSearchMaxInvestLimitIsSet() {
        return _bidSearchMaxInvestLimitIsSet;
    }	


    public void setBidSearchTimeMatch1(Integer bidSearchTimeMatch1) {
        if (!_bidSearchTimeMatch1IsSet) {
            _bidSearchTimeMatch1IsSet = true;
            _bidSearchTimeMatch1InitVal = bidSearchTimeMatch1;
        }
        _bidSearchTimeMatch1 = bidSearchTimeMatch1;
    }

    public Integer getBidSearchTimeMatch1() {
        return _bidSearchTimeMatch1;
    }

    public Integer bidSearchTimeMatch1InitVal() {
        return _bidSearchTimeMatch1InitVal;
    }

	public boolean bidSearchTimeMatch1IsDirty() {
        return !valuesAreEqual(_bidSearchTimeMatch1InitVal, _bidSearchTimeMatch1);
    }

    public boolean bidSearchTimeMatch1IsSet() {
        return _bidSearchTimeMatch1IsSet;
    }	


    public void setBidSearchTimeMatch2(Integer bidSearchTimeMatch2) {
        if (!_bidSearchTimeMatch2IsSet) {
            _bidSearchTimeMatch2IsSet = true;
            _bidSearchTimeMatch2InitVal = bidSearchTimeMatch2;
        }
        _bidSearchTimeMatch2 = bidSearchTimeMatch2;
    }

    public Integer getBidSearchTimeMatch2() {
        return _bidSearchTimeMatch2;
    }

    public Integer bidSearchTimeMatch2InitVal() {
        return _bidSearchTimeMatch2InitVal;
    }

	public boolean bidSearchTimeMatch2IsDirty() {
        return !valuesAreEqual(_bidSearchTimeMatch2InitVal, _bidSearchTimeMatch2);
    }

    public boolean bidSearchTimeMatch2IsSet() {
        return _bidSearchTimeMatch2IsSet;
    }	


    public void setBidSearchTimeMatch3(Integer bidSearchTimeMatch3) {
        if (!_bidSearchTimeMatch3IsSet) {
            _bidSearchTimeMatch3IsSet = true;
            _bidSearchTimeMatch3InitVal = bidSearchTimeMatch3;
        }
        _bidSearchTimeMatch3 = bidSearchTimeMatch3;
    }

    public Integer getBidSearchTimeMatch3() {
        return _bidSearchTimeMatch3;
    }

    public Integer bidSearchTimeMatch3InitVal() {
        return _bidSearchTimeMatch3InitVal;
    }

	public boolean bidSearchTimeMatch3IsDirty() {
        return !valuesAreEqual(_bidSearchTimeMatch3InitVal, _bidSearchTimeMatch3);
    }

    public boolean bidSearchTimeMatch3IsSet() {
        return _bidSearchTimeMatch3IsSet;
    }	


    public void setBidSearchBarrier1(Integer bidSearchBarrier1) {
        if (!_bidSearchBarrier1IsSet) {
            _bidSearchBarrier1IsSet = true;
            _bidSearchBarrier1InitVal = bidSearchBarrier1;
        }
        _bidSearchBarrier1 = bidSearchBarrier1;
    }

    public Integer getBidSearchBarrier1() {
        return _bidSearchBarrier1;
    }

    public Integer bidSearchBarrier1InitVal() {
        return _bidSearchBarrier1InitVal;
    }

	public boolean bidSearchBarrier1IsDirty() {
        return !valuesAreEqual(_bidSearchBarrier1InitVal, _bidSearchBarrier1);
    }

    public boolean bidSearchBarrier1IsSet() {
        return _bidSearchBarrier1IsSet;
    }	


    public void setBidSearchBarrier2(Integer bidSearchBarrier2) {
        if (!_bidSearchBarrier2IsSet) {
            _bidSearchBarrier2IsSet = true;
            _bidSearchBarrier2InitVal = bidSearchBarrier2;
        }
        _bidSearchBarrier2 = bidSearchBarrier2;
    }

    public Integer getBidSearchBarrier2() {
        return _bidSearchBarrier2;
    }

    public Integer bidSearchBarrier2InitVal() {
        return _bidSearchBarrier2InitVal;
    }

	public boolean bidSearchBarrier2IsDirty() {
        return !valuesAreEqual(_bidSearchBarrier2InitVal, _bidSearchBarrier2);
    }

    public boolean bidSearchBarrier2IsSet() {
        return _bidSearchBarrier2IsSet;
    }	


    public void setBidSearchBarrier3(Integer bidSearchBarrier3) {
        if (!_bidSearchBarrier3IsSet) {
            _bidSearchBarrier3IsSet = true;
            _bidSearchBarrier3InitVal = bidSearchBarrier3;
        }
        _bidSearchBarrier3 = bidSearchBarrier3;
    }

    public Integer getBidSearchBarrier3() {
        return _bidSearchBarrier3;
    }

    public Integer bidSearchBarrier3InitVal() {
        return _bidSearchBarrier3InitVal;
    }

	public boolean bidSearchBarrier3IsDirty() {
        return !valuesAreEqual(_bidSearchBarrier3InitVal, _bidSearchBarrier3);
    }

    public boolean bidSearchBarrier3IsSet() {
        return _bidSearchBarrier3IsSet;
    }	


    public void setActivateBidSearch(Boolean activateBidSearch) {
        if (!_activateBidSearchIsSet) {
            _activateBidSearchIsSet = true;
            _activateBidSearchInitVal = activateBidSearch;
        }
        _activateBidSearch = activateBidSearch;
    }

    public Boolean getActivateBidSearch() {
        return _activateBidSearch;
    }

    public Boolean activateBidSearchInitVal() {
        return _activateBidSearchInitVal;
    }

	public boolean activateBidSearchIsDirty() {
        return !valuesAreEqual(_activateBidSearchInitVal, _activateBidSearch);
    }

    public boolean activateBidSearchIsSet() {
        return _activateBidSearchIsSet;
    }	


    public void setRapidMatchingBufferPercent(double rapidMatchingBufferPercent) {
        if (!_rapidMatchingBufferPercentIsSet) {
            _rapidMatchingBufferPercentIsSet = true;
            _rapidMatchingBufferPercentInitVal = rapidMatchingBufferPercent;
        }
        _rapidMatchingBufferPercent = rapidMatchingBufferPercent;
    }

    public double getRapidMatchingBufferPercent() {
        return _rapidMatchingBufferPercent;
    }

    public double rapidMatchingBufferPercentInitVal() {
        return _rapidMatchingBufferPercentInitVal;
    }

	public boolean rapidMatchingBufferPercentIsDirty() {
        return !valuesAreEqual(_rapidMatchingBufferPercentInitVal, _rapidMatchingBufferPercent);
    }

    public boolean rapidMatchingBufferPercentIsSet() {
        return _rapidMatchingBufferPercentIsSet;
    }	


    public void setRapidMatchingBufferAmount(double rapidMatchingBufferAmount) {
        if (!_rapidMatchingBufferAmountIsSet) {
            _rapidMatchingBufferAmountIsSet = true;
            _rapidMatchingBufferAmountInitVal = rapidMatchingBufferAmount;
        }
        _rapidMatchingBufferAmount = rapidMatchingBufferAmount;
    }

    public double getRapidMatchingBufferAmount() {
        return _rapidMatchingBufferAmount;
    }

    public double rapidMatchingBufferAmountInitVal() {
        return _rapidMatchingBufferAmountInitVal;
    }

	public boolean rapidMatchingBufferAmountIsDirty() {
        return !valuesAreEqual(_rapidMatchingBufferAmountInitVal, _rapidMatchingBufferAmount);
    }

    public boolean rapidMatchingBufferAmountIsSet() {
        return _rapidMatchingBufferAmountIsSet;
    }	


    public void setRapidMatchingMaxInterest(double rapidMatchingMaxInterest) {
        if (!_rapidMatchingMaxInterestIsSet) {
            _rapidMatchingMaxInterestIsSet = true;
            _rapidMatchingMaxInterestInitVal = rapidMatchingMaxInterest;
        }
        _rapidMatchingMaxInterest = rapidMatchingMaxInterest;
    }

    public double getRapidMatchingMaxInterest() {
        return _rapidMatchingMaxInterest;
    }

    public double rapidMatchingMaxInterestInitVal() {
        return _rapidMatchingMaxInterestInitVal;
    }

	public boolean rapidMatchingMaxInterestIsDirty() {
        return !valuesAreEqual(_rapidMatchingMaxInterestInitVal, _rapidMatchingMaxInterest);
    }

    public boolean rapidMatchingMaxInterestIsSet() {
        return _rapidMatchingMaxInterestIsSet;
    }	


    public void setAvailAmountCacheExpiry(Integer availAmountCacheExpiry) {
        if (!_availAmountCacheExpiryIsSet) {
            _availAmountCacheExpiryIsSet = true;
            _availAmountCacheExpiryInitVal = availAmountCacheExpiry;
        }
        _availAmountCacheExpiry = availAmountCacheExpiry;
    }

    public Integer getAvailAmountCacheExpiry() {
        return _availAmountCacheExpiry;
    }

    public Integer availAmountCacheExpiryInitVal() {
        return _availAmountCacheExpiryInitVal;
    }

	public boolean availAmountCacheExpiryIsDirty() {
        return !valuesAreEqual(_availAmountCacheExpiryInitVal, _availAmountCacheExpiry);
    }

    public boolean availAmountCacheExpiryIsSet() {
        return _availAmountCacheExpiryIsSet;
    }	

}
