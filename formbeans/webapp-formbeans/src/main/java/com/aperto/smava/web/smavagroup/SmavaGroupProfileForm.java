//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.smavagroup;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(smava group profile)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'SmavaGroupProfileForm'.
 *
 * @author generator
 */
public class SmavaGroupProfileForm implements Serializable {

    public static final String SMAVA_GROUP_ID_EXPR = "smavaGroupProfileForm.smavaGroupId";
    public static final String SMAVA_GROUP_ID_PATH = "command.smavaGroupProfileForm.smavaGroupId";
    public static final String TEXT_EXPR = "smavaGroupProfileForm.text";
    public static final String TEXT_PATH = "command.smavaGroupProfileForm.text";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(smava group profile)}
// !!!!!!!! End of insert code section !!!!!!!!


    private Long _smavaGroupId;
    private Long _smavaGroupIdInitVal;
    private boolean _smavaGroupIdIsSet;

    private String _text;
    private String _textInitVal;
    private boolean _textIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setSmavaGroupId(Long smavaGroupId) {
        if (!_smavaGroupIdIsSet) {
            _smavaGroupIdIsSet = true;
            _smavaGroupIdInitVal = smavaGroupId;
        }
        _smavaGroupId = smavaGroupId;
    }

    public Long getSmavaGroupId() {
        return _smavaGroupId;
    }

    public Long smavaGroupIdInitVal() {
        return _smavaGroupIdInitVal;
    }

    public boolean smavaGroupIdIsDirty() {
        return !valuesAreEqual(_smavaGroupIdInitVal, _smavaGroupId);
    }

    public boolean smavaGroupIdIsSet() {
        return _smavaGroupIdIsSet;
    }

    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = text;
        }
        _text = text;
    }

    public String getText() {
        return _text;
    }

    public String textInitVal() {
        return _textInitVal;
    }

    public boolean textIsDirty() {
        return !valuesAreEqual(_textInitVal, _text);
    }

    public boolean textIsSet() {
        return _textIsSet;
    }

}
