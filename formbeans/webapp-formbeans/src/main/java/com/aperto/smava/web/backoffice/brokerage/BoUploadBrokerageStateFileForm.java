//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.brokerage;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo upload dtaus file)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoUploadBrokerageCsvStateFileForm'.
 *
 * @author generator
 */
public class BoUploadBrokerageStateFileForm {

    public static final String FILE_EXPR = "boUploadBrokerageStateFileForm.targoFile";
    public static final String FILE_PATH = "command.boUploadBrokerageStateFileForm.targoFile";

    public static final String ACTIVATION_DATE_EXPR = "boUploadBrokerageStateFileForm.activationDate";
    public static final String ACTIVATION_DATE_PATH = "command.boUploadBrokerageStateFileForm.activationDate";

    public static final String TARGO_UPLOAD_DONE_EXPR  = "boUploadBrokerageStateFileForm.targoUploadDone";
    public static final String TARGO_UPLOAD_DONE_PATH = "command.boUploadBrokerageStateFileForm.targoUploadDone";
    
    public static final String ERROR_EXPR = "boUploadBrokerageStateFileForm.errorText";
    public static final String ERROR_PATH = "command.boUploadBrokerageStateFileForm.errorText";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo upload dtaus file)}
// !!!!!!!! End of insert code section !!!!!!!!


    private org.springframework.web.multipart.commons.CommonsMultipartFile _targoFile;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _targoFileInitVal;
    private boolean _targoFileIsSet;
    private String _activationDate;
    private String _errorText;
    private boolean _targoUploadDone;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setTargoFile(org.springframework.web.multipart.commons.CommonsMultipartFile file) {
        if (!_targoFileIsSet) {
            _targoFileIsSet = true;
            _targoFile = file;
        }
        _targoFile = file;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile getTargoFile() {
        return _targoFile;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile fileInitVal() {
        return _targoFileInitVal;
    }

    public String getActivationDate() {
		return _activationDate;
	}

	public void setActivationDate(String activationDate) {
		_activationDate = activationDate;
	}

	public String getErrorText() {
        return _errorText;
    }

    public void setErrorText( String error){
    	_errorText = error;
    }
    
    public boolean isTargoUploadDone() {
        return _targoUploadDone;
    }

    public void setTargoUploadDone( boolean targoUploadDone){
    	_targoUploadDone = targoUploadDone;
    }
    
    
    public boolean fileIsDirty() {
        return !valuesAreEqual(_targoFileInitVal, _targoFile);
    }

    public boolean fileIsSet() {
        return _targoFileIsSet;
    }
    
   

}
