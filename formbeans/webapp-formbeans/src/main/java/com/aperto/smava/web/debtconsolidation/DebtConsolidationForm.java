//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.debtconsolidation;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(debt consolidation)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'DebtConsolidationForm'.
 *
 * @author generator
 */
public class DebtConsolidationForm implements Serializable {

    public static final String ID_EXPR = "debtConsolidationForm.id";
    public static final String ID_PATH = "command.debtConsolidationForm.id";
    public static final String KIND_OF_CREDIT_EXPR = "debtConsolidationForm.kindOfCredit";
    public static final String KIND_OF_CREDIT_PATH = "command.debtConsolidationForm.kindOfCredit";
    public static final String MONTHLY_INSTALLMENT_EXPR = "debtConsolidationForm.monthlyInstallment";
    public static final String MONTHLY_INSTALLMENT_PATH = "command.debtConsolidationForm.monthlyInstallment";
    public static final String MONTHS_REMAINING_EXPR = "debtConsolidationForm.monthsRemaining";
    public static final String MONTHS_REMAINING_PATH = "command.debtConsolidationForm.monthsRemaining";
    public static final String CONSOLIDATION_DEBT_RADIO_EXPR = "debtConsolidationForm.consolidationDebtRadio";
    public static final String CONSOLIDATION_DEBT_RADIO_PATH = "command.debtConsolidationForm.consolidationDebtRadio";
    public static final String INTEREST_EXPR = "debtConsolidationForm.interest";
    public static final String INTEREST_PATH = "command.debtConsolidationForm.interest";
    public static final String ACCOUNT_NUMBER_EXPR = "debtConsolidationForm.accountNumber";
    public static final String ACCOUNT_NUMBER_PATH = "command.debtConsolidationForm.accountNumber";
    public static final String BANK_CODE_EXPR = "debtConsolidationForm.bankCode";
    public static final String BANK_CODE_PATH = "command.debtConsolidationForm.bankCode";
    public static final String IBAN_EXPR = "debtConsolidationForm.iban";
    public static final String IBAN_PATH = "command.debtConsolidationForm.iban";
    public static final String BIC_EXPR = "debtConsolidationForm.bic";
    public static final String BIC_PATH = "command.debtConsolidationForm.bic";
    public static final String BANK_EXPR = "debtConsolidationForm.bank";
    public static final String BANK_PATH = "command.debtConsolidationForm.bank";
    public static final String DEBT_AMOUNT_EXPR = "debtConsolidationForm.debtAmount";
    public static final String DEBT_AMOUNT_PATH = "command.debtConsolidationForm.debtAmount";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(debt consolidation)}

    public static final String KIND_OF_CREDIT_1_EXPR = "debtConsolidationForm1.kindOfCredit";
    public static final String KIND_OF_CREDIT_2_EXPR = "debtConsolidationForm2.kindOfCredit";
    public static final String KIND_OF_CREDIT_3_EXPR = "debtConsolidationForm3.kindOfCredit";
    public static final String KIND_OF_CREDIT_4_EXPR = "debtConsolidationForm4.kindOfCredit";

// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _kindOfCredit;
    private String _kindOfCreditInitVal;
    private boolean _kindOfCreditIsSet;
    private String _monthlyInstallment;
    private String _monthlyInstallmentInitVal;
    private boolean _monthlyInstallmentIsSet;
    private String _monthsRemaining;
    private String _monthsRemainingInitVal;
    private boolean _monthsRemainingIsSet;
    private Boolean _consolidationDebtRadio;
    private Boolean _consolidationDebtRadioInitVal;
    private boolean _consolidationDebtRadioIsSet;
    private String _interest;
    private String _interestInitVal;
    private boolean _interestIsSet;
    private String _accountNumber;
    private String _accountNumberInitVal;
    private boolean _accountNumberIsSet;
    private String _bankCode;
    private String _bankCodeInitVal;
    private boolean _bankCodeIsSet;
    private String _iban;
    private String _ibanInitVal;
    private boolean _ibanIsSet;
    private String _bic;
    private String _bicInitVal;
    private boolean _bicIsSet;
    private String _bank;
    private String _bankInitVal;
    private boolean _bankIsSet;
    private String _debtAmount;
    private String _debtAmountInitVal;
    private boolean _debtAmountIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setKindOfCredit(String kindOfCredit) {
        if (!_kindOfCreditIsSet) {
            _kindOfCreditIsSet = true;
            _kindOfCreditInitVal = kindOfCredit;
        }
        _kindOfCredit = kindOfCredit;
    }

    public String getKindOfCredit() {
        return _kindOfCredit;
    }

    public String kindOfCreditInitVal() {
        return _kindOfCreditInitVal;
    }

	public boolean kindOfCreditIsDirty() {
        return !valuesAreEqual(_kindOfCreditInitVal, _kindOfCredit);
    }

    public boolean kindOfCreditIsSet() {
        return _kindOfCreditIsSet;
    }	


    public void setMonthlyInstallment(String monthlyInstallment) {
        if (!_monthlyInstallmentIsSet) {
            _monthlyInstallmentIsSet = true;
            _monthlyInstallmentInitVal = monthlyInstallment;
        }
        _monthlyInstallment = monthlyInstallment;
    }

    public String getMonthlyInstallment() {
        return _monthlyInstallment;
    }

    public String monthlyInstallmentInitVal() {
        return _monthlyInstallmentInitVal;
    }

	public boolean monthlyInstallmentIsDirty() {
        return !valuesAreEqual(_monthlyInstallmentInitVal, _monthlyInstallment);
    }

    public boolean monthlyInstallmentIsSet() {
        return _monthlyInstallmentIsSet;
    }	


    public void setMonthsRemaining(String monthsRemaining) {
        if (!_monthsRemainingIsSet) {
            _monthsRemainingIsSet = true;
            _monthsRemainingInitVal = monthsRemaining;
        }
        _monthsRemaining = monthsRemaining;
    }

    public String getMonthsRemaining() {
        return _monthsRemaining;
    }

    public String monthsRemainingInitVal() {
        return _monthsRemainingInitVal;
    }

	public boolean monthsRemainingIsDirty() {
        return !valuesAreEqual(_monthsRemainingInitVal, _monthsRemaining);
    }

    public boolean monthsRemainingIsSet() {
        return _monthsRemainingIsSet;
    }	


    public void setConsolidationDebtRadio(Boolean consolidationDebtRadio) {
        if (!_consolidationDebtRadioIsSet) {
            _consolidationDebtRadioIsSet = true;
            _consolidationDebtRadioInitVal = consolidationDebtRadio;
        }
        _consolidationDebtRadio = consolidationDebtRadio;
    }

    public Boolean getConsolidationDebtRadio() {
        return _consolidationDebtRadio;
    }

    public Boolean consolidationDebtRadioInitVal() {
        return _consolidationDebtRadioInitVal;
    }

	public boolean consolidationDebtRadioIsDirty() {
        return !valuesAreEqual(_consolidationDebtRadioInitVal, _consolidationDebtRadio);
    }

    public boolean consolidationDebtRadioIsSet() {
        return _consolidationDebtRadioIsSet;
    }	


    public void setInterest(String interest) {
        if (!_interestIsSet) {
            _interestIsSet = true;
            _interestInitVal = interest;
        }
        _interest = interest;
    }

    public String getInterest() {
        return _interest;
    }

    public String interestInitVal() {
        return _interestInitVal;
    }

	public boolean interestIsDirty() {
        return !valuesAreEqual(_interestInitVal, _interest);
    }

    public boolean interestIsSet() {
        return _interestIsSet;
    }	


    public void setAccountNumber(String accountNumber) {
        if (!_accountNumberIsSet) {
            _accountNumberIsSet = true;
            _accountNumberInitVal = accountNumber;
        }
        _accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return _accountNumber;
    }

    public String accountNumberInitVal() {
        return _accountNumberInitVal;
    }

	public boolean accountNumberIsDirty() {
        return !valuesAreEqual(_accountNumberInitVal, _accountNumber);
    }

    public boolean accountNumberIsSet() {
        return _accountNumberIsSet;
    }	


    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = bankCode;
        }
        _bankCode = bankCode;
    }

    public String getBankCode() {
        return _bankCode;
    }

    public String bankCodeInitVal() {
        return _bankCodeInitVal;
    }

	public boolean bankCodeIsDirty() {
        return !valuesAreEqual(_bankCodeInitVal, _bankCode);
    }

    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }	


    public void setIban(String iban) {
        if (!_ibanIsSet) {
            _ibanIsSet = true;
            _ibanInitVal = iban;
        }
        _iban = iban;
    }

    public String getIban() {
        return _iban;
    }

    public String ibanInitVal() {
        return _ibanInitVal;
    }

	public boolean ibanIsDirty() {
        return !valuesAreEqual(_ibanInitVal, _iban);
    }

    public boolean ibanIsSet() {
        return _ibanIsSet;
    }	


    public void setBic(String bic) {
        if (!_bicIsSet) {
            _bicIsSet = true;
            _bicInitVal = bic;
        }
        _bic = bic;
    }

    public String getBic() {
        return _bic;
    }

    public String bicInitVal() {
        return _bicInitVal;
    }

	public boolean bicIsDirty() {
        return !valuesAreEqual(_bicInitVal, _bic);
    }

    public boolean bicIsSet() {
        return _bicIsSet;
    }	


    public void setBank(String bank) {
        if (!_bankIsSet) {
            _bankIsSet = true;
            _bankInitVal = bank;
        }
        _bank = bank;
    }

    public String getBank() {
        return _bank;
    }

    public String bankInitVal() {
        return _bankInitVal;
    }

	public boolean bankIsDirty() {
        return !valuesAreEqual(_bankInitVal, _bank);
    }

    public boolean bankIsSet() {
        return _bankIsSet;
    }	


    public void setDebtAmount(String debtAmount) {
        if (!_debtAmountIsSet) {
            _debtAmountIsSet = true;
            _debtAmountInitVal = debtAmount;
        }
        _debtAmount = debtAmount;
    }

    public String getDebtAmount() {
        return _debtAmount;
    }

    public String debtAmountInitVal() {
        return _debtAmountInitVal;
    }

	public boolean debtAmountIsDirty() {
        return !valuesAreEqual(_debtAmountInitVal, _debtAmount);
    }

    public boolean debtAmountIsSet() {
        return _debtAmountIsSet;
    }	

}
