//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\temp\mda\model.xml
//
//    Or you can change the template:
//
//    P:\temp\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.contract;



// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo object contract)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoObjectContractForm'.
 *
 * @author generator
 */
public class BoObjectContractForm implements Serializable {

    public static final String OBJECTION_DOCUMENT_ID_EXPR = "boObjectContractForm.objectionDocumentId";
    public static final String OBJECTION_DOCUMENT_ID_PATH = "command.boObjectContractForm.objectionDocumentId";
    public static final String OBJECTION_DATE_EXPR = "boObjectContractForm.objectionDate";
    public static final String OBJECTION_DATE_PATH = "command.boObjectContractForm.objectionDate";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo object contract)}
// !!!!!!!! End of insert code section !!!!!!!!


    private String _objectionDocumentId;
    private String _objectionDocumentIdInitVal;
    private boolean _objectionDocumentIdIsSet;

    private String _objectionDate;
    private String _objectionDateInitVal;
    private boolean _objectionDateIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setObjectionDocumentId(String objectionDocumentId) {
        if (!_objectionDocumentIdIsSet) {
            _objectionDocumentIdIsSet = true;
            _objectionDocumentIdInitVal = objectionDocumentId;
        }
        _objectionDocumentId = objectionDocumentId;
    }

    public String getObjectionDocumentId() {
        return _objectionDocumentId;
    }

    public String objectionDocumentIdInitVal() {
        return _objectionDocumentIdInitVal;
    }

    public boolean objectionDocumentIdIsDirty() {
        return !valuesAreEqual(_objectionDocumentIdInitVal, _objectionDocumentId);
    }

    public boolean objectionDocumentIdIsSet() {
        return _objectionDocumentIdIsSet;
    }

    public void setObjectionDate(String objectionDate) {
        if (!_objectionDateIsSet) {
            _objectionDateIsSet = true;
            _objectionDateInitVal = objectionDate;
        }
        _objectionDate = objectionDate;
    }

    public String getObjectionDate() {
        return _objectionDate;
    }

    public String objectionDateInitVal() {
        return _objectionDateInitVal;
    }

    public boolean objectionDateIsDirty() {
        return !valuesAreEqual(_objectionDateInitVal, _objectionDate);
    }

    public boolean objectionDateIsSet() {
        return _objectionDateIsSet;
    }

}
