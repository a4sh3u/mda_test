//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.partner;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(check terms and conditions)}

import java.io.Serializable;


// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CheckTermsAndConditionsForm'.
 *
 * @author generator
 */
public class CheckTermsAndConditionsForm implements Serializable {

    public static final String ASSET_MANAGER_GUIDELINE_DATE_EXPR = "checkTermsAndConditionsForm.assetManagerGuidelineDate";
    public static final String ASSET_MANAGER_GUIDELINE_DATE_PATH = "command.checkTermsAndConditionsForm.assetManagerGuidelineDate";
    public static final String TERMS_AND_CONDITIONS_EXPR = "checkTermsAndConditionsForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_PATH = "command.checkTermsAndConditionsForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_APPLIED_EXPR = "checkTermsAndConditionsForm.termsAndConditionsApplied";
    public static final String TERMS_AND_CONDITIONS_APPLIED_PATH = "command.checkTermsAndConditionsForm.termsAndConditionsApplied";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(check terms and conditions)}
    /**
     * generated serial.
     */
    private static final long serialVersionUID = -548870246574706498L;
// !!!!!!!! End of insert code section !!!!!!!!

    private Boolean _assetManagerGuidelineDate;
    private Boolean _assetManagerGuidelineDateInitVal;
    private boolean _assetManagerGuidelineDateIsSet;
    private Boolean _termsAndConditions;
    private Boolean _termsAndConditionsInitVal;
    private boolean _termsAndConditionsIsSet;
    private Boolean _termsAndConditionsApplied;
    private Boolean _termsAndConditionsAppliedInitVal;
    private boolean _termsAndConditionsAppliedIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setAssetManagerGuidelineDate(Boolean assetManagerGuidelineDate) {
        if (!_assetManagerGuidelineDateIsSet) {
            _assetManagerGuidelineDateIsSet = true;
            _assetManagerGuidelineDateInitVal = assetManagerGuidelineDate;
        }
        _assetManagerGuidelineDate = assetManagerGuidelineDate;
    }

    public Boolean getAssetManagerGuidelineDate() {
        return _assetManagerGuidelineDate;
    }

    public Boolean assetManagerGuidelineDateInitVal() {
        return _assetManagerGuidelineDateInitVal;
    }

	public boolean assetManagerGuidelineDateIsDirty() {
        return !valuesAreEqual(_assetManagerGuidelineDateInitVal, _assetManagerGuidelineDate);
    }

    public boolean assetManagerGuidelineDateIsSet() {
        return _assetManagerGuidelineDateIsSet;
    }	


    public void setTermsAndConditions(Boolean termsAndConditions) {
        if (!_termsAndConditionsIsSet) {
            _termsAndConditionsIsSet = true;
            _termsAndConditionsInitVal = termsAndConditions;
        }
        _termsAndConditions = termsAndConditions;
    }

    public Boolean getTermsAndConditions() {
        return _termsAndConditions;
    }

    public Boolean termsAndConditionsInitVal() {
        return _termsAndConditionsInitVal;
    }

	public boolean termsAndConditionsIsDirty() {
        return !valuesAreEqual(_termsAndConditionsInitVal, _termsAndConditions);
    }

    public boolean termsAndConditionsIsSet() {
        return _termsAndConditionsIsSet;
    }	


    public void setTermsAndConditionsApplied(Boolean termsAndConditionsApplied) {
        if (!_termsAndConditionsAppliedIsSet) {
            _termsAndConditionsAppliedIsSet = true;
            _termsAndConditionsAppliedInitVal = termsAndConditionsApplied;
        }
        _termsAndConditionsApplied = termsAndConditionsApplied;
    }

    public Boolean getTermsAndConditionsApplied() {
        return _termsAndConditionsApplied;
    }

    public Boolean termsAndConditionsAppliedInitVal() {
        return _termsAndConditionsAppliedInitVal;
    }

	public boolean termsAndConditionsAppliedIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAppliedInitVal, _termsAndConditionsApplied);
    }

    public boolean termsAndConditionsAppliedIsSet() {
        return _termsAndConditionsAppliedIsSet;
    }	

}
