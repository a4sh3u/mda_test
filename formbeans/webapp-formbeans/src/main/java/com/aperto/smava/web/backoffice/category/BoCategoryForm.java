//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse-jee-ganymede\eclipse\workspace\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse-jee-ganymede\eclipse\workspace\trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.category;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo category)}

import de.smava.webapp.account.domain.CategoryImage;
import de.smava.webapp.account.domain.Order;
import de.smava.webapp.commons.web.ImageAwareCommand;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoCategoryForm'.
 *
 * @author generator
 */
public class BoCategoryForm implements Serializable, ImageAwareCommand {

    public static final String ID_EXPR = "boCategoryForm.id";
    public static final String ID_PATH = "command.boCategoryForm.id";
    public static final String NAME_EXPR = "boCategoryForm.name";
    public static final String NAME_PATH = "command.boCategoryForm.name";
    public static final String DESCRIPTION_EXPR = "boCategoryForm.description";
    public static final String DESCRIPTION_PATH = "command.boCategoryForm.description";
    public static final String PARENT_ID_EXPR = "boCategoryForm.parentId";
    public static final String PARENT_ID_PATH = "command.boCategoryForm.parentId";
    public static final String ACTION_EXPR = "boCategoryForm.action";
    public static final String ACTION_PATH = "command.boCategoryForm.action";
    public static final String MOVE_CATEGORY_ID_EXPR = "boCategoryForm.moveCategoryId";
    public static final String MOVE_CATEGORY_ID_PATH = "command.boCategoryForm.moveCategoryId";
    public static final String CAN_DELETE_EXPR = "boCategoryForm.canDelete";
    public static final String CAN_DELETE_PATH = "command.boCategoryForm.canDelete";
    public static final String IMAGE_FILE_EXPR = "boCategoryForm.imageFile";
    public static final String IMAGE_FILE_PATH = "command.boCategoryForm.imageFile";
    public static final String IMAGE_EXPR = "boCategoryForm.image";
    public static final String IMAGE_PATH = "command.boCategoryForm.image";
    public static final String IMAGE_HEIGHT_EXPR = "boCategoryForm.imageHeight";
    public static final String IMAGE_HEIGHT_PATH = "command.boCategoryForm.imageHeight";
    public static final String IMAGE_WIDTH_EXPR = "boCategoryForm.imageWidth";
    public static final String IMAGE_WIDTH_PATH = "command.boCategoryForm.imageWidth";
    public static final String DELETE_IMAGE_ID_EXPR = "boCategoryForm.deleteImageId";
    public static final String DELETE_IMAGE_ID_PATH = "command.boCategoryForm.deleteImageId";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo category)}
	private static final long serialVersionUID = 125593254242874428L;

    private Collection<Order> _orders;

    public Collection<Order> getOrders() {
        return _orders;
    }

    public void setOrders(Collection<Order> orders) {
        _orders = orders;
    }

    private Collection<CategoryImage> _images;

    public Collection<CategoryImage> getImages() {
        return _images;
    }

    public void setImages(Collection<CategoryImage> images) {
    	_images = images;
    }
    
    private byte[] _imageBytes = null;

    public byte[] getImageBytes() {
        if (_imageBytes == null && _imageFile != null && !_imageFile.isEmpty()) {
        	try {
        		_imageBytes = _imageFile.getBytes();
        	} catch (IOException e) {
        		_imageBytes = null;
        	}
        }

        byte[] imageBytes = null;
        if (_imageBytes != null) {
            imageBytes = _imageBytes.clone();
        }
        return imageBytes;
    }

    public void setImageBytes(byte[] bytes) {
        if (bytes == null) {
            _imageBytes = null;
        } else {
            _imageBytes = bytes.clone();
        }
    }
    
	public byte[] getFile() {
		return getImageBytes();
	}

    
    private boolean _isNewUpload;
//    private byte[] _file = null;

//    public byte[] getFile() {
//        byte[] file = null;
//        if (_file != null) {
//            file = _file.clone();
//        }
//        return file;
//    }

//    public void setFile(byte[] file) {
//        _file = file.clone();
//    }

    public String getNoImageExpression() {
        return null;
    }

    public String getImageFileExpression() {
        return "imageFile";
    }

    public void updateNoImage() {
        // Nothing to do
    }

    public void resetForm() {
        // Nothing to do
    }
    
    public String getImageCategory() {
        return IMAGE_CATEGORY_CATEGORY;
    }    
    
	public boolean isNewUpload() {
		return _isNewUpload;
	}

	public void setNewUpload(boolean isNewUpload) {
		_isNewUpload = isNewUpload;
	}

// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private Long _parentId;
    private Long _parentIdInitVal;
    private boolean _parentIdIsSet;
    private String _action;
    private String _actionInitVal;
    private boolean _actionIsSet;
    private Long _moveCategoryId;
    private Long _moveCategoryIdInitVal;
    private boolean _moveCategoryIdIsSet;
    private Boolean _canDelete;
    private Boolean _canDeleteInitVal;
    private boolean _canDeleteIsSet;
    private org.springframework.web.multipart.MultipartFile _imageFile;
    private org.springframework.web.multipart.MultipartFile _imageFileInitVal;
    private boolean _imageFileIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private int _imageHeight;
    private int _imageHeightInitVal;
    private boolean _imageHeightIsSet;
    private int _imageWidth;
    private int _imageWidthInitVal;
    private boolean _imageWidthIsSet;
    private String _deleteImageId;
    private String _deleteImageIdInitVal;
    private boolean _deleteImageIdIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setParentId(Long parentId) {
        if (!_parentIdIsSet) {
            _parentIdIsSet = true;
            _parentIdInitVal = parentId;
        }
        _parentId = parentId;
    }

    public Long getParentId() {
        return _parentId;
    }

    public Long parentIdInitVal() {
        return _parentIdInitVal;
    }

	public boolean parentIdIsDirty() {
        return !valuesAreEqual(_parentIdInitVal, _parentId);
    }

    public boolean parentIdIsSet() {
        return _parentIdIsSet;
    }	


    public void setAction(String action) {
        if (!_actionIsSet) {
            _actionIsSet = true;
            _actionInitVal = action;
        }
        _action = action;
    }

    public String getAction() {
        return _action;
    }

    public String actionInitVal() {
        return _actionInitVal;
    }

	public boolean actionIsDirty() {
        return !valuesAreEqual(_actionInitVal, _action);
    }

    public boolean actionIsSet() {
        return _actionIsSet;
    }	


    public void setMoveCategoryId(Long moveCategoryId) {
        if (!_moveCategoryIdIsSet) {
            _moveCategoryIdIsSet = true;
            _moveCategoryIdInitVal = moveCategoryId;
        }
        _moveCategoryId = moveCategoryId;
    }

    public Long getMoveCategoryId() {
        return _moveCategoryId;
    }

    public Long moveCategoryIdInitVal() {
        return _moveCategoryIdInitVal;
    }

	public boolean moveCategoryIdIsDirty() {
        return !valuesAreEqual(_moveCategoryIdInitVal, _moveCategoryId);
    }

    public boolean moveCategoryIdIsSet() {
        return _moveCategoryIdIsSet;
    }	


    public void setCanDelete(Boolean canDelete) {
        if (!_canDeleteIsSet) {
            _canDeleteIsSet = true;
            _canDeleteInitVal = canDelete;
        }
        _canDelete = canDelete;
    }

    public Boolean getCanDelete() {
        return _canDelete;
    }

    public Boolean canDeleteInitVal() {
        return _canDeleteInitVal;
    }

	public boolean canDeleteIsDirty() {
        return !valuesAreEqual(_canDeleteInitVal, _canDelete);
    }

    public boolean canDeleteIsSet() {
        return _canDeleteIsSet;
    }	


    public void setImageFile(org.springframework.web.multipart.MultipartFile imageFile) {
        if (!_imageFileIsSet) {
            _imageFileIsSet = true;
            _imageFileInitVal = imageFile;
        }
        _imageFile = imageFile;
    }

    public org.springframework.web.multipart.MultipartFile getImageFile() {
        return _imageFile;
    }

    public org.springframework.web.multipart.MultipartFile imageFileInitVal() {
        return _imageFileInitVal;
    }

	public boolean imageFileIsDirty() {
        return !valuesAreEqual(_imageFileInitVal, _imageFile);
    }

    public boolean imageFileIsSet() {
        return _imageFileIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = imageHeight;
        }
        _imageHeight = imageHeight;
    }

    public int getImageHeight() {
        return _imageHeight;
    }

    public int imageHeightInitVal() {
        return _imageHeightInitVal;
    }

	public boolean imageHeightIsDirty() {
        return !valuesAreEqual(_imageHeightInitVal, _imageHeight);
    }

    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }	


    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = imageWidth;
        }
        _imageWidth = imageWidth;
    }

    public int getImageWidth() {
        return _imageWidth;
    }

    public int imageWidthInitVal() {
        return _imageWidthInitVal;
    }

	public boolean imageWidthIsDirty() {
        return !valuesAreEqual(_imageWidthInitVal, _imageWidth);
    }

    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }	


    public void setDeleteImageId(String deleteImageId) {
        if (!_deleteImageIdIsSet) {
            _deleteImageIdIsSet = true;
            _deleteImageIdInitVal = deleteImageId;
        }
        _deleteImageId = deleteImageId;
    }

    public String getDeleteImageId() {
        return _deleteImageId;
    }

    public String deleteImageIdInitVal() {
        return _deleteImageIdInitVal;
    }

	public boolean deleteImageIdIsDirty() {
        return !valuesAreEqual(_deleteImageIdInitVal, _deleteImageId);
    }

    public boolean deleteImageIdIsSet() {
        return _deleteImageIdIsSet;
    }	

}
