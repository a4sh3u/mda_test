//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/formbean.tpl
//
//
//
//
package com.aperto.smava.web.account;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(password)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'PasswordForm'.
 *
 * @author generator
 */
public class PasswordForm implements Serializable {

    public static final String OLD_PASSWORD_EXPR = "passwordForm.oldPassword";
    public static final String OLD_PASSWORD_PATH = "command.passwordForm.oldPassword";
    public static final String OLD_PASSWORD_REPEAT_EXPR = "passwordForm.oldPasswordRepeat";
    public static final String OLD_PASSWORD_REPEAT_PATH = "command.passwordForm.oldPasswordRepeat";
    public static final String NEW_PASSWORD_EXPR = "passwordForm.newPassword";
    public static final String NEW_PASSWORD_PATH = "command.passwordForm.newPassword";
    public static final String NEW_PASSWORD_REPEAT_EXPR = "passwordForm.newPasswordRepeat";
    public static final String NEW_PASSWORD_REPEAT_PATH = "command.passwordForm.newPasswordRepeat";
    public static final String TYPE_EXPR = "passwordForm.type";
    public static final String TYPE_PATH = "command.passwordForm.type";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(password)}
    private static final long serialVersionUID = -7864829685743031781L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _oldPassword;
    private String _oldPasswordInitVal;
    private boolean _oldPasswordIsSet;
    private String _oldPasswordRepeat;
    private String _oldPasswordRepeatInitVal;
    private boolean _oldPasswordRepeatIsSet;
    private String _newPassword;
    private String _newPasswordInitVal;
    private boolean _newPasswordIsSet;
    private String _newPasswordRepeat;
    private String _newPasswordRepeatInitVal;
    private boolean _newPasswordRepeatIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setOldPassword(String oldPassword) {
        if (!_oldPasswordIsSet) {
            _oldPasswordIsSet = true;
            _oldPasswordInitVal = oldPassword;
        }
        _oldPassword = oldPassword;
    }

    public String getOldPassword() {
        return _oldPassword;
    }

    public String oldPasswordInitVal() {
        return _oldPasswordInitVal;
    }

	public boolean oldPasswordIsDirty() {
        return !valuesAreEqual(_oldPasswordInitVal, _oldPassword);
    }

    public boolean oldPasswordIsSet() {
        return _oldPasswordIsSet;
    }	


    public void setOldPasswordRepeat(String oldPasswordRepeat) {
        if (!_oldPasswordRepeatIsSet) {
            _oldPasswordRepeatIsSet = true;
            _oldPasswordRepeatInitVal = oldPasswordRepeat;
        }
        _oldPasswordRepeat = oldPasswordRepeat;
    }

    public String getOldPasswordRepeat() {
        return _oldPasswordRepeat;
    }

    public String oldPasswordRepeatInitVal() {
        return _oldPasswordRepeatInitVal;
    }

	public boolean oldPasswordRepeatIsDirty() {
        return !valuesAreEqual(_oldPasswordRepeatInitVal, _oldPasswordRepeat);
    }

    public boolean oldPasswordRepeatIsSet() {
        return _oldPasswordRepeatIsSet;
    }	


    public void setNewPassword(String newPassword) {
        if (!_newPasswordIsSet) {
            _newPasswordIsSet = true;
            _newPasswordInitVal = newPassword;
        }
        _newPassword = newPassword;
    }

    public String getNewPassword() {
        return _newPassword;
    }

    public String newPasswordInitVal() {
        return _newPasswordInitVal;
    }

	public boolean newPasswordIsDirty() {
        return !valuesAreEqual(_newPasswordInitVal, _newPassword);
    }

    public boolean newPasswordIsSet() {
        return _newPasswordIsSet;
    }	


    public void setNewPasswordRepeat(String newPasswordRepeat) {
        if (!_newPasswordRepeatIsSet) {
            _newPasswordRepeatIsSet = true;
            _newPasswordRepeatInitVal = newPasswordRepeat;
        }
        _newPasswordRepeat = newPasswordRepeat;
    }

    public String getNewPasswordRepeat() {
        return _newPasswordRepeat;
    }

    public String newPasswordRepeatInitVal() {
        return _newPasswordRepeatInitVal;
    }

	public boolean newPasswordRepeatIsDirty() {
        return !valuesAreEqual(_newPasswordRepeatInitVal, _newPasswordRepeat);
    }

    public boolean newPasswordRepeatIsSet() {
        return _newPasswordRepeatIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	

}
