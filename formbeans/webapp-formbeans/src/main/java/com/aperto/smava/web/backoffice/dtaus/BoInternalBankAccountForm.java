//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.dtaus;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo internal bank account)}

import java.io.Serializable;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoInternalBankAccountForm'.
 *
 * @author generator
 */
public class BoInternalBankAccountForm implements Serializable {

    public static final String BANK_ACCOUNT_TYPE_EXPR = "boInternalBankAccountForm.bankAccountType";
    public static final String BANK_ACCOUNT_TYPE_PATH = "command.boInternalBankAccountForm.bankAccountType";
    public static final String ACCOUNT_ID_EXPR = "boInternalBankAccountForm.accountId";
    public static final String ACCOUNT_ID_PATH = "command.boInternalBankAccountForm.accountId";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo internal bank account)}

    private static final long serialVersionUID = 72340172838076673L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _bankAccountType;
    private String _bankAccountTypeInitVal;
    private boolean _bankAccountTypeIsSet;
    private Long _accountId;
    private Long _accountIdInitVal;
    private boolean _accountIdIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setBankAccountType(String bankAccountType) {
        if (!_bankAccountTypeIsSet) {
            _bankAccountTypeIsSet = true;
            _bankAccountTypeInitVal = bankAccountType;
        }
        _bankAccountType = bankAccountType;
    }

    public String getBankAccountType() {
        return _bankAccountType;
    }

    public String bankAccountTypeInitVal() {
        return _bankAccountTypeInitVal;
    }

	public boolean bankAccountTypeIsDirty() {
        return !valuesAreEqual(_bankAccountTypeInitVal, _bankAccountType);
    }

    public boolean bankAccountTypeIsSet() {
        return _bankAccountTypeIsSet;
    }	


    public void setAccountId(Long accountId) {
        if (!_accountIdIsSet) {
            _accountIdIsSet = true;
            _accountIdInitVal = accountId;
        }
        _accountId = accountId;
    }

    public Long getAccountId() {
        return _accountId;
    }

    public Long accountIdInitVal() {
        return _accountIdInitVal;
    }

	public boolean accountIdIsDirty() {
        return !valuesAreEqual(_accountIdInitVal, _accountId);
    }

    public boolean accountIdIsSet() {
        return _accountIdIsSet;
    }	

}
