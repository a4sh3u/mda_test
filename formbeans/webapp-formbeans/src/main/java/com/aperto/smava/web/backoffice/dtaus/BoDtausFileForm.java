//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.dtaus;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo dtaus file)}

import de.smava.webapp.account.domain.DtausFileOutTransaction;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoDtausFileForm'.
 *
 * @author generator
 */
public class BoDtausFileForm implements Serializable {

    public static final String ID_EXPR = "boDtausFileForm.id";
    public static final String ID_PATH = "command.boDtausFileForm.id";
    public static final String NAME_EXPR = "boDtausFileForm.name";
    public static final String NAME_PATH = "command.boDtausFileForm.name";
    public static final String CREATION_DATE_EXPR = "boDtausFileForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boDtausFileForm.creationDate";
    public static final String DUE_DATE_EXPR = "boDtausFileForm.dueDate";
    public static final String DUE_DATE_PATH = "command.boDtausFileForm.dueDate";
    public static final String RELEASE_DATE_EXPR = "boDtausFileForm.releaseDate";
    public static final String RELEASE_DATE_PATH = "command.boDtausFileForm.releaseDate";
    public static final String STATE_EXPR = "boDtausFileForm.state";
    public static final String STATE_PATH = "command.boDtausFileForm.state";
    public static final String DIRECTION_EXPR = "boDtausFileForm.direction";
    public static final String DIRECTION_PATH = "command.boDtausFileForm.direction";
    public static final String DESCRIPTION_EXPR = "boDtausFileForm.description";
    public static final String DESCRIPTION_PATH = "command.boDtausFileForm.description";
    public static final String NOTE_EXPR = "boDtausFileForm.note";
    public static final String NOTE_PATH = "command.boDtausFileForm.note";
    public static final String SUCCESSFUL_TRANSACTIONS_EXPR = "boDtausFileForm.successfulTransactions";
    public static final String SUCCESSFUL_TRANSACTIONS_PATH = "command.boDtausFileForm.successfulTransactions";
    public static final String DTAUS_FILE_OUT_TRANSACTIONS_EXPR = "boDtausFileForm.dtausFileOutTransactions";
    public static final String DTAUS_FILE_OUT_TRANSACTIONS_PATH = "command.boDtausFileForm.dtausFileOutTransactions";
    public static final String REMOVE_TRANSACTION_EXPR = "boDtausFileForm.removeTransaction";
    public static final String REMOVE_TRANSACTION_PATH = "command.boDtausFileForm.removeTransaction";
    public static final String DEBIT_NOTE_DATE_EXPR = "boDtausFileForm.debitNoteDate";
    public static final String DEBIT_NOTE_DATE_PATH = "command.boDtausFileForm.debitNoteDate";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo dtaus file)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private Date _creationDate;
    private Date _creationDateInitVal;
    private boolean _creationDateIsSet;
    private Date _dueDate;
    private Date _dueDateInitVal;
    private boolean _dueDateIsSet;
    private Date _releaseDate;
    private Date _releaseDateInitVal;
    private boolean _releaseDateIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _direction;
    private String _directionInitVal;
    private boolean _directionIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private String _note;
    private String _noteInitVal;
    private boolean _noteIsSet;
    private String _successfulTransactions;
    private String _successfulTransactionsInitVal;
    private boolean _successfulTransactionsIsSet;
    private List<DtausFileOutTransaction> _dtausFileOutTransactions;
    private List<DtausFileOutTransaction> _dtausFileOutTransactionsInitVal;
    private boolean _dtausFileOutTransactionsIsSet;
    private String _removeTransaction;
    private String _removeTransactionInitVal;
    private boolean _removeTransactionIsSet;
    private String _debitNoteDate;
    private String _debitNoteDateInitVal;
    private boolean _debitNoteDateIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;

            if (creationDate == null) {
                _creationDateInitVal = null;
            } else {
                _creationDateInitVal = (Date) creationDate.clone();
            }
        }

        if (creationDate == null) {
            _creationDate = null;
        } else {
            _creationDate = (Date) creationDate.clone();
        }
    }

    public Date getCreationDate() {
        Date creationDate = null;
        if (_creationDate != null) {
            creationDate = (Date) _creationDate.clone();
        }
        return creationDate;
    }

    public Date creationDateInitVal() {
        Date creationDateInitVal = null;
        if (_creationDateInitVal != null) {
            creationDateInitVal = (Date) _creationDateInitVal.clone();
        }
        return creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setDueDate(Date dueDate) {
        if (!_dueDateIsSet) {
            _dueDateIsSet = true;

            if (dueDate == null) {
                _dueDateInitVal = null;
            } else {
                _dueDateInitVal = (Date) dueDate.clone();
            }
        }

        if (dueDate == null) {
            _dueDate = null;
        } else {
            _dueDate = (Date) dueDate.clone();
        }
    }

    public Date getDueDate() {
        Date dueDate = null;
        if (_dueDate != null) {
            dueDate = (Date) _dueDate.clone();
        }
        return dueDate;
    }

    public Date dueDateInitVal() {
        Date dueDateInitVal = null;
        if (_dueDateInitVal != null) {
            dueDateInitVal = (Date) _dueDateInitVal.clone();
        }
        return dueDateInitVal;
    }

	public boolean dueDateIsDirty() {
        return !valuesAreEqual(_dueDateInitVal, _dueDate);
    }

    public boolean dueDateIsSet() {
        return _dueDateIsSet;
    }	


    public void setReleaseDate(Date releaseDate) {
        if (!_releaseDateIsSet) {
            _releaseDateIsSet = true;

            if (releaseDate == null) {
                _releaseDateInitVal = null;
            } else {
                _releaseDateInitVal = (Date) releaseDate.clone();
            }
        }

        if (releaseDate == null) {
            _releaseDate = null;
        } else {
            _releaseDate = (Date) releaseDate.clone();
        }
    }

    public Date getReleaseDate() {
        Date releaseDate = null;
        if (_releaseDate != null) {
            releaseDate = (Date) _releaseDate.clone();
        }
        return releaseDate;
    }

    public Date releaseDateInitVal() {
        Date releaseDateInitVal = null;
        if (_releaseDateInitVal != null) {
            releaseDateInitVal = (Date) _releaseDateInitVal.clone();
        }
        return releaseDateInitVal;
    }

	public boolean releaseDateIsDirty() {
        return !valuesAreEqual(_releaseDateInitVal, _releaseDate);
    }

    public boolean releaseDateIsSet() {
        return _releaseDateIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setDirection(String direction) {
        if (!_directionIsSet) {
            _directionIsSet = true;
            _directionInitVal = direction;
        }
        _direction = direction;
    }

    public String getDirection() {
        return _direction;
    }

    public String directionInitVal() {
        return _directionInitVal;
    }

	public boolean directionIsDirty() {
        return !valuesAreEqual(_directionInitVal, _direction);
    }

    public boolean directionIsSet() {
        return _directionIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setNote(String note) {
        if (!_noteIsSet) {
            _noteIsSet = true;
            _noteInitVal = note;
        }
        _note = note;
    }

    public String getNote() {
        return _note;
    }

    public String noteInitVal() {
        return _noteInitVal;
    }

	public boolean noteIsDirty() {
        return !valuesAreEqual(_noteInitVal, _note);
    }

    public boolean noteIsSet() {
        return _noteIsSet;
    }	


    public void setSuccessfulTransactions(String successfulTransactions) {
        if (!_successfulTransactionsIsSet) {
            _successfulTransactionsIsSet = true;
            _successfulTransactionsInitVal = successfulTransactions;
        }
        _successfulTransactions = successfulTransactions;
    }

    public String getSuccessfulTransactions() {
        return _successfulTransactions;
    }

    public String successfulTransactionsInitVal() {
        return _successfulTransactionsInitVal;
    }

	public boolean successfulTransactionsIsDirty() {
        return !valuesAreEqual(_successfulTransactionsInitVal, _successfulTransactions);
    }

    public boolean successfulTransactionsIsSet() {
        return _successfulTransactionsIsSet;
    }	


    public void setDtausFileOutTransactions(List<DtausFileOutTransaction> dtausFileOutTransactions) {
        if (!_dtausFileOutTransactionsIsSet) {
            _dtausFileOutTransactionsIsSet = true;
            _dtausFileOutTransactionsInitVal = dtausFileOutTransactions;
        }
        _dtausFileOutTransactions = dtausFileOutTransactions;
    }

    public List<DtausFileOutTransaction> getDtausFileOutTransactions() {
        return _dtausFileOutTransactions;
    }

    public List<DtausFileOutTransaction> dtausFileOutTransactionsInitVal() {
        return _dtausFileOutTransactionsInitVal;
    }

	public boolean dtausFileOutTransactionsIsDirty() {
        return !valuesAreEqual(_dtausFileOutTransactionsInitVal, _dtausFileOutTransactions);
    }

    public boolean dtausFileOutTransactionsIsSet() {
        return _dtausFileOutTransactionsIsSet;
    }	


    public void setRemoveTransaction(String removeTransaction) {
        if (!_removeTransactionIsSet) {
            _removeTransactionIsSet = true;
            _removeTransactionInitVal = removeTransaction;
        }
        _removeTransaction = removeTransaction;
    }

    public String getRemoveTransaction() {
        return _removeTransaction;
    }

    public String removeTransactionInitVal() {
        return _removeTransactionInitVal;
    }

	public boolean removeTransactionIsDirty() {
        return !valuesAreEqual(_removeTransactionInitVal, _removeTransaction);
    }

    public boolean removeTransactionIsSet() {
        return _removeTransactionIsSet;
    }	


    public void setDebitNoteDate(String debitNoteDate) {
        if (!_debitNoteDateIsSet) {
            _debitNoteDateIsSet = true;
            _debitNoteDateInitVal = debitNoteDate;
        }
        _debitNoteDate = debitNoteDate;
    }

    public String getDebitNoteDate() {
        return _debitNoteDate;
    }

    public String debitNoteDateInitVal() {
        return _debitNoteDateInitVal;
    }

	public boolean debitNoteDateIsDirty() {
        return !valuesAreEqual(_debitNoteDateInitVal, _debitNoteDate);
    }

    public boolean debitNoteDateIsSet() {
        return _debitNoteDateIsSet;
    }	

}
