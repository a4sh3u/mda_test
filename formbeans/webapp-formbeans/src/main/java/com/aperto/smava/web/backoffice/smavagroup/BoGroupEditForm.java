//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.smavagroup;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo group edit)}
import java.util.Date;
import java.util.Map;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoGroupEditForm'.
 *
 * @author generator
 */
public class BoGroupEditForm {

    public static final String NAME_EXPR = "boGroupEditForm.name";
    public static final String NAME_PATH = "command.boGroupEditForm.name";
    public static final String TAGLINE_EXPR = "boGroupEditForm.tagline";
    public static final String TAGLINE_PATH = "command.boGroupEditForm.tagline";
    public static final String IMAGE_EXPR = "boGroupEditForm.image";
    public static final String IMAGE_PATH = "command.boGroupEditForm.image";
    public static final String DESCRIPTION_EXPR = "boGroupEditForm.description";
    public static final String DESCRIPTION_PATH = "command.boGroupEditForm.description";
    public static final String RATES_BONUSA_EXPR = "boGroupEditForm.ratesBonusa";
    public static final String RATES_BONUSA_PATH = "command.boGroupEditForm.ratesBonusa";
    public static final String RATES_BONUSB_EXPR = "boGroupEditForm.ratesBonusb";
    public static final String RATES_BONUSB_PATH = "command.boGroupEditForm.ratesBonusb";
    public static final String RATES_BONUSC_EXPR = "boGroupEditForm.ratesBonusc";
    public static final String RATES_BONUSC_PATH = "command.boGroupEditForm.ratesBonusc";
    public static final String CREATION_DATE_EXPR = "boGroupEditForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boGroupEditForm.creationDate";
    public static final String STATE_EXPR = "boGroupEditForm.state";
    public static final String STATE_PATH = "command.boGroupEditForm.state";
    public static final String IMAGE_WIDTH_EXPR = "boGroupEditForm.imageWidth";
    public static final String IMAGE_WIDTH_PATH = "command.boGroupEditForm.imageWidth";
    public static final String IMAGE_HEIGHT_EXPR = "boGroupEditForm.imageHeight";
    public static final String IMAGE_HEIGHT_PATH = "command.boGroupEditForm.imageHeight";
    public static final String TYPE_EXPR = "boGroupEditForm.type";
    public static final String TYPE_PATH = "command.boGroupEditForm.type";
    public static final String OWNER_EXPR = "boGroupEditForm.owner";
    public static final String OWNER_PATH = "command.boGroupEditForm.owner";
    public static final String REAPPLICATION_REQUIRED_EXPR = "boGroupEditForm.reapplicationRequired";
    public static final String REAPPLICATION_REQUIRED_PATH = "command.boGroupEditForm.reapplicationRequired";
    public static final String LENDER_DESCRIPTION_EXPR = "boGroupEditForm.lenderDescription";
    public static final String LENDER_DESCRIPTION_PATH = "command.boGroupEditForm.lenderDescription";
    public static final String BORROWER_DESCRIPTION_EXPR = "boGroupEditForm.borrowerDescription";
    public static final String BORROWER_DESCRIPTION_PATH = "command.boGroupEditForm.borrowerDescription";
    public static final String MAIN_CATEGORY_EXPR = "boGroupEditForm.mainCategory";
    public static final String MAIN_CATEGORY_PATH = "command.boGroupEditForm.mainCategory";
    public static final String SECOND_CATEGORY_EXPR = "boGroupEditForm.secondCategory";
    public static final String SECOND_CATEGORY_PATH = "command.boGroupEditForm.secondCategory";
    public static final String RATES_BONUS_EXPR = "boGroupEditForm.ratesBonus";
    public static final String RATES_BONUS_PATH = "command.boGroupEditForm.ratesBonus";
    public static final String INTERESTING_EXPR = "boGroupEditForm.interesting";
    public static final String INTERESTING_PATH = "command.boGroupEditForm.interesting";
    public static final String BEST_MARKET_EXPR = "boGroupEditForm.bestMarket";
    public static final String BEST_MARKET_PATH = "command.boGroupEditForm.bestMarket";
    public static final String WORST_MARKET_EXPR = "boGroupEditForm.worstMarket";
    public static final String WORST_MARKET_PATH = "command.boGroupEditForm.worstMarket";
    public static final String FORUM_LINK_EXPR = "boGroupEditForm.forumLink";
    public static final String FORUM_LINK_PATH = "command.boGroupEditForm.forumLink";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo group edit)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _tagline;
    private String _taglineInitVal;
    private boolean _taglineIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private Float _ratesBonusa;
    private Float _ratesBonusaInitVal;
    private boolean _ratesBonusaIsSet;
    private Float _ratesBonusb;
    private Float _ratesBonusbInitVal;
    private boolean _ratesBonusbIsSet;
    private Float _ratesBonusc;
    private Float _ratesBonuscInitVal;
    private boolean _ratesBonuscIsSet;
    private Date _creationDate;
    private Date _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private int _imageWidth;
    private int _imageWidthInitVal;
    private boolean _imageWidthIsSet;
    private int _imageHeight;
    private int _imageHeightInitVal;
    private boolean _imageHeightIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _owner;
    private String _ownerInitVal;
    private boolean _ownerIsSet;
    private boolean _reapplicationRequired;
    private boolean _reapplicationRequiredInitVal;
    private boolean _reapplicationRequiredIsSet;
    private String _lenderDescription;
    private String _lenderDescriptionInitVal;
    private boolean _lenderDescriptionIsSet;
    private String _borrowerDescription;
    private String _borrowerDescriptionInitVal;
    private boolean _borrowerDescriptionIsSet;
    private Long _mainCategory;
    private Long _mainCategoryInitVal;
    private boolean _mainCategoryIsSet;
    private Long _secondCategory;
    private Long _secondCategoryInitVal;
    private boolean _secondCategoryIsSet;
    private Map<String, Float> _ratesBonus;
    private Map<String, Float> _ratesBonusInitVal;
    private boolean _ratesBonusIsSet;
    private String _interesting;
    private String _interestingInitVal;
    private boolean _interestingIsSet;
    private String _bestMarket;
    private String _bestMarketInitVal;
    private boolean _bestMarketIsSet;
    private String _worstMarket;
    private String _worstMarketInitVal;
    private boolean _worstMarketIsSet;
    private String _forumLink;
    private String _forumLinkInitVal;
    private boolean _forumLinkIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setTagline(String tagline) {
        if (!_taglineIsSet) {
            _taglineIsSet = true;
            _taglineInitVal = tagline;
        }
        _tagline = tagline;
    }

    public String getTagline() {
        return _tagline;
    }

    public String taglineInitVal() {
        return _taglineInitVal;
    }

	public boolean taglineIsDirty() {
        return !valuesAreEqual(_taglineInitVal, _tagline);
    }

    public boolean taglineIsSet() {
        return _taglineIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setRatesBonusa(Float ratesBonusa) {
        if (!_ratesBonusaIsSet) {
            _ratesBonusaIsSet = true;
            _ratesBonusaInitVal = ratesBonusa;
        }
        _ratesBonusa = ratesBonusa;
    }

    public Float getRatesBonusa() {
        return _ratesBonusa;
    }

    public Float ratesBonusaInitVal() {
        return _ratesBonusaInitVal;
    }

	public boolean ratesBonusaIsDirty() {
        return !valuesAreEqual(_ratesBonusaInitVal, _ratesBonusa);
    }

    public boolean ratesBonusaIsSet() {
        return _ratesBonusaIsSet;
    }	


    public void setRatesBonusb(Float ratesBonusb) {
        if (!_ratesBonusbIsSet) {
            _ratesBonusbIsSet = true;
            _ratesBonusbInitVal = ratesBonusb;
        }
        _ratesBonusb = ratesBonusb;
    }

    public Float getRatesBonusb() {
        return _ratesBonusb;
    }

    public Float ratesBonusbInitVal() {
        return _ratesBonusbInitVal;
    }

	public boolean ratesBonusbIsDirty() {
        return !valuesAreEqual(_ratesBonusbInitVal, _ratesBonusb);
    }

    public boolean ratesBonusbIsSet() {
        return _ratesBonusbIsSet;
    }	


    public void setRatesBonusc(Float ratesBonusc) {
        if (!_ratesBonuscIsSet) {
            _ratesBonuscIsSet = true;
            _ratesBonuscInitVal = ratesBonusc;
        }
        _ratesBonusc = ratesBonusc;
    }

    public Float getRatesBonusc() {
        return _ratesBonusc;
    }

    public Float ratesBonuscInitVal() {
        return _ratesBonuscInitVal;
    }

	public boolean ratesBonuscIsDirty() {
        return !valuesAreEqual(_ratesBonuscInitVal, _ratesBonusc);
    }

    public boolean ratesBonuscIsSet() {
        return _ratesBonuscIsSet;
    }	


    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;

            if (creationDate == null) {
                _creationDateInitVal = null;
            } else {
                _creationDateInitVal = (Date) creationDate.clone();
            }
        }

        if (creationDate == null) {
            _creationDate = null;
        } else {
            _creationDate = (Date) creationDate.clone();
        }
    }

    public Date getCreationDate() {
        Date creationDate = null;
        if (_creationDate != null) {
            creationDate = (Date) _creationDate.clone();
        }
        return creationDate;
    }

    public Date creationDateInitVal() {
        Date creationDateInitVal = null;
        if (_creationDateInitVal != null) {
            creationDateInitVal = (Date) _creationDateInitVal.clone();
        }
        return creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = imageWidth;
        }
        _imageWidth = imageWidth;
    }

    public int getImageWidth() {
        return _imageWidth;
    }

    public int imageWidthInitVal() {
        return _imageWidthInitVal;
    }

	public boolean imageWidthIsDirty() {
        return !valuesAreEqual(_imageWidthInitVal, _imageWidth);
    }

    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }	


    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = imageHeight;
        }
        _imageHeight = imageHeight;
    }

    public int getImageHeight() {
        return _imageHeight;
    }

    public int imageHeightInitVal() {
        return _imageHeightInitVal;
    }

	public boolean imageHeightIsDirty() {
        return !valuesAreEqual(_imageHeightInitVal, _imageHeight);
    }

    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setOwner(String owner) {
        if (!_ownerIsSet) {
            _ownerIsSet = true;
            _ownerInitVal = owner;
        }
        _owner = owner;
    }

    public String getOwner() {
        return _owner;
    }

    public String ownerInitVal() {
        return _ownerInitVal;
    }

	public boolean ownerIsDirty() {
        return !valuesAreEqual(_ownerInitVal, _owner);
    }

    public boolean ownerIsSet() {
        return _ownerIsSet;
    }	


    public void setReapplicationRequired(boolean reapplicationRequired) {
        if (!_reapplicationRequiredIsSet) {
            _reapplicationRequiredIsSet = true;
            _reapplicationRequiredInitVal = reapplicationRequired;
        }
        _reapplicationRequired = reapplicationRequired;
    }

    public boolean getReapplicationRequired() {
        return _reapplicationRequired;
    }

    public boolean reapplicationRequiredInitVal() {
        return _reapplicationRequiredInitVal;
    }

	public boolean reapplicationRequiredIsDirty() {
        return !valuesAreEqual(_reapplicationRequiredInitVal, _reapplicationRequired);
    }

    public boolean reapplicationRequiredIsSet() {
        return _reapplicationRequiredIsSet;
    }	


    public void setLenderDescription(String lenderDescription) {
        if (!_lenderDescriptionIsSet) {
            _lenderDescriptionIsSet = true;
            _lenderDescriptionInitVal = lenderDescription;
        }
        _lenderDescription = lenderDescription;
    }

    public String getLenderDescription() {
        return _lenderDescription;
    }

    public String lenderDescriptionInitVal() {
        return _lenderDescriptionInitVal;
    }

	public boolean lenderDescriptionIsDirty() {
        return !valuesAreEqual(_lenderDescriptionInitVal, _lenderDescription);
    }

    public boolean lenderDescriptionIsSet() {
        return _lenderDescriptionIsSet;
    }	


    public void setBorrowerDescription(String borrowerDescription) {
        if (!_borrowerDescriptionIsSet) {
            _borrowerDescriptionIsSet = true;
            _borrowerDescriptionInitVal = borrowerDescription;
        }
        _borrowerDescription = borrowerDescription;
    }

    public String getBorrowerDescription() {
        return _borrowerDescription;
    }

    public String borrowerDescriptionInitVal() {
        return _borrowerDescriptionInitVal;
    }

	public boolean borrowerDescriptionIsDirty() {
        return !valuesAreEqual(_borrowerDescriptionInitVal, _borrowerDescription);
    }

    public boolean borrowerDescriptionIsSet() {
        return _borrowerDescriptionIsSet;
    }	


    public void setMainCategory(Long mainCategory) {
        if (!_mainCategoryIsSet) {
            _mainCategoryIsSet = true;
            _mainCategoryInitVal = mainCategory;
        }
        _mainCategory = mainCategory;
    }

    public Long getMainCategory() {
        return _mainCategory;
    }

    public Long mainCategoryInitVal() {
        return _mainCategoryInitVal;
    }

	public boolean mainCategoryIsDirty() {
        return !valuesAreEqual(_mainCategoryInitVal, _mainCategory);
    }

    public boolean mainCategoryIsSet() {
        return _mainCategoryIsSet;
    }	


    public void setSecondCategory(Long secondCategory) {
        if (!_secondCategoryIsSet) {
            _secondCategoryIsSet = true;
            _secondCategoryInitVal = secondCategory;
        }
        _secondCategory = secondCategory;
    }

    public Long getSecondCategory() {
        return _secondCategory;
    }

    public Long secondCategoryInitVal() {
        return _secondCategoryInitVal;
    }

	public boolean secondCategoryIsDirty() {
        return !valuesAreEqual(_secondCategoryInitVal, _secondCategory);
    }

    public boolean secondCategoryIsSet() {
        return _secondCategoryIsSet;
    }	


    public void setRatesBonus(Map<String, Float> ratesBonus) {
        if (!_ratesBonusIsSet) {
            _ratesBonusIsSet = true;
            _ratesBonusInitVal = ratesBonus;
        }
        _ratesBonus = ratesBonus;
    }

    public Map<String, Float> getRatesBonus() {
        return _ratesBonus;
    }

    public Map<String, Float> ratesBonusInitVal() {
        return _ratesBonusInitVal;
    }

	public boolean ratesBonusIsDirty() {
        return !valuesAreEqual(_ratesBonusInitVal, _ratesBonus);
    }

    public boolean ratesBonusIsSet() {
        return _ratesBonusIsSet;
    }	


    public void setInteresting(String interesting) {
        if (!_interestingIsSet) {
            _interestingIsSet = true;
            _interestingInitVal = interesting;
        }
        _interesting = interesting;
    }

    public String getInteresting() {
        return _interesting;
    }

    public String interestingInitVal() {
        return _interestingInitVal;
    }

	public boolean interestingIsDirty() {
        return !valuesAreEqual(_interestingInitVal, _interesting);
    }

    public boolean interestingIsSet() {
        return _interestingIsSet;
    }	


    public void setBestMarket(String bestMarket) {
        if (!_bestMarketIsSet) {
            _bestMarketIsSet = true;
            _bestMarketInitVal = bestMarket;
        }
        _bestMarket = bestMarket;
    }

    public String getBestMarket() {
        return _bestMarket;
    }

    public String bestMarketInitVal() {
        return _bestMarketInitVal;
    }

	public boolean bestMarketIsDirty() {
        return !valuesAreEqual(_bestMarketInitVal, _bestMarket);
    }

    public boolean bestMarketIsSet() {
        return _bestMarketIsSet;
    }	


    public void setWorstMarket(String worstMarket) {
        if (!_worstMarketIsSet) {
            _worstMarketIsSet = true;
            _worstMarketInitVal = worstMarket;
        }
        _worstMarket = worstMarket;
    }

    public String getWorstMarket() {
        return _worstMarket;
    }

    public String worstMarketInitVal() {
        return _worstMarketInitVal;
    }

	public boolean worstMarketIsDirty() {
        return !valuesAreEqual(_worstMarketInitVal, _worstMarket);
    }

    public boolean worstMarketIsSet() {
        return _worstMarketIsSet;
    }	


    public void setForumLink(String forumLink) {
        if (!_forumLinkIsSet) {
            _forumLinkIsSet = true;
            _forumLinkInitVal = forumLink;
        }
        _forumLink = forumLink;
    }

    public String getForumLink() {
        return _forumLink;
    }

    public String forumLinkInitVal() {
        return _forumLinkInitVal;
    }

	public boolean forumLinkIsDirty() {
        return !valuesAreEqual(_forumLinkInitVal, _forumLink);
    }

    public boolean forumLinkIsSet() {
        return _forumLinkIsSet;
    }	

}
