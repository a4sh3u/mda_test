//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.registration;


    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(registration calculation)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'RegistrationCalculationForm'.
 *
 * @author generator
 */
public class RegistrationCalculationForm implements Serializable {

    public static final String CATEGORY_EXPR = "registrationCalculationForm.category";
    public static final String CATEGORY_PATH = "command.registrationCalculationForm.category";
    public static final String AMOUNT_EXPR = "registrationCalculationForm.amount";
    public static final String AMOUNT_PATH = "command.registrationCalculationForm.amount";
    public static final String TERM_EXPR = "registrationCalculationForm.term";
    public static final String TERM_PATH = "command.registrationCalculationForm.term";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(registration calculation)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _category;
    private Long _categoryInitVal;
    private boolean _categoryIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _term;
    private String _termInitVal;
    private boolean _termIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setCategory(Long category) {
        if (!_categoryIsSet) {
            _categoryIsSet = true;
            _categoryInitVal = category;
        }
        _category = category;
    }

    public Long getCategory() {
        return _category;
    }

    public Long categoryInitVal() {
        return _categoryInitVal;
    }

	public boolean categoryIsDirty() {
        return !valuesAreEqual(_categoryInitVal, _category);
    }

    public boolean categoryIsSet() {
        return _categoryIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setTerm(String term) {
        if (!_termIsSet) {
            _termIsSet = true;
            _termInitVal = term;
        }
        _term = term;
    }

    public String getTerm() {
        return _term;
    }

    public String termInitVal() {
        return _termInitVal;
    }

	public boolean termIsDirty() {
        return !valuesAreEqual(_termInitVal, _term);
    }

    public boolean termIsSet() {
        return _termIsSet;
    }	

}
