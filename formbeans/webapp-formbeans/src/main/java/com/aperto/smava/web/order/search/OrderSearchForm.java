//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.order.search;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(order search)}

import java.io.Serializable;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'OrderSearchForm'.
 *
 * @author generator
 */
public class OrderSearchForm implements Serializable {

    public static final String BID_SEARCH_ID_EXPR = "orderSearchForm.bidSearchId";
    public static final String BID_SEARCH_ID_PATH = "command.orderSearchForm.bidSearchId";
    public static final String NAME_EXPR = "orderSearchForm.name";
    public static final String NAME_PATH = "command.orderSearchForm.name";
    public static final String RATING_MIN_EXPR = "orderSearchForm.ratingMin";
    public static final String RATING_MIN_PATH = "command.orderSearchForm.ratingMin";
    public static final String RATING_MAX_EXPR = "orderSearchForm.ratingMax";
    public static final String RATING_MAX_PATH = "command.orderSearchForm.ratingMax";
    public static final String DURATIONS_EXPR = "orderSearchForm.durations";
    public static final String DURATIONS_PATH = "command.orderSearchForm.durations";
    public static final String INTEREST_MIN_EXPR = "orderSearchForm.interestMin";
    public static final String INTEREST_MIN_PATH = "command.orderSearchForm.interestMin";
    public static final String INTEREST_MAX_EXPR = "orderSearchForm.interestMax";
    public static final String INTEREST_MAX_PATH = "command.orderSearchForm.interestMax";
    public static final String ROI_MIN_EXPR = "orderSearchForm.roiMin";
    public static final String ROI_MIN_PATH = "command.orderSearchForm.roiMin";
    public static final String ROI_MAX_EXPR = "orderSearchForm.roiMax";
    public static final String ROI_MAX_PATH = "command.orderSearchForm.roiMax";
    public static final String CREDIT_RATE_INDICATOR_EXPR = "orderSearchForm.creditRateIndicator";
    public static final String CREDIT_RATE_INDICATOR_PATH = "command.orderSearchForm.creditRateIndicator";
    public static final String AMOUNT_MIN_EXPR = "orderSearchForm.amountMin";
    public static final String AMOUNT_MIN_PATH = "command.orderSearchForm.amountMin";
    public static final String AMOUNT_MAX_EXPR = "orderSearchForm.amountMax";
    public static final String AMOUNT_MAX_PATH = "command.orderSearchForm.amountMax";
    public static final String MATCHED_RATE_EXPR = "orderSearchForm.matchedRate";
    public static final String MATCHED_RATE_PATH = "command.orderSearchForm.matchedRate";
    public static final String SEARCH_PHRASE_EXPR = "orderSearchForm.searchPhrase";
    public static final String SEARCH_PHRASE_PATH = "command.orderSearchForm.searchPhrase";
    public static final String INCLUDE_FINISHED_ORDERS_EXPR = "orderSearchForm.includeFinishedOrders";
    public static final String INCLUDE_FINISHED_ORDERS_PATH = "command.orderSearchForm.includeFinishedOrders";
    public static final String EMPLOYMENT_EXPR = "orderSearchForm.employment";
    public static final String EMPLOYMENT_PATH = "command.orderSearchForm.employment";
    public static final String IS_MAIL_NOTIFICATION_WANTED_EXPR = "orderSearchForm.isMailNotificationWanted";
    public static final String IS_MAIL_NOTIFICATION_WANTED_PATH = "command.orderSearchForm.isMailNotificationWanted";
    public static final String EMPLOYMENTS_EXPR = "orderSearchForm.employments";
    public static final String EMPLOYMENTS_PATH = "command.orderSearchForm.employments";
    public static final String AGE_RANGE_MIN_EXPR = "orderSearchForm.ageRangeMin";
    public static final String AGE_RANGE_MIN_PATH = "command.orderSearchForm.ageRangeMin";
    public static final String AGE_RANGE_MAX_EXPR = "orderSearchForm.ageRangeMax";
    public static final String AGE_RANGE_MAX_PATH = "command.orderSearchForm.ageRangeMax";
    public static final String EXCEPT_LATE_PAYER_EXPR = "orderSearchForm.exceptLatePayer";
    public static final String EXCEPT_LATE_PAYER_PATH = "command.orderSearchForm.exceptLatePayer";
    public static final String INVESTMENT_AMOUNT_EXPR = "orderSearchForm.investmentAmount";
    public static final String INVESTMENT_AMOUNT_PATH = "command.orderSearchForm.investmentAmount";
    public static final String INVESTMENT_LIMIT_PER_BORROWER_EXPR = "orderSearchForm.investmentLimitPerBorrower";
    public static final String INVESTMENT_LIMIT_PER_BORROWER_PATH = "command.orderSearchForm.investmentLimitPerBorrower";
    public static final String MIN_INVESTMENT_LIMIT_PER_BORROWER_EXPR = "orderSearchForm.minInvestmentLimitPerBorrower";
    public static final String MIN_INVESTMENT_LIMIT_PER_BORROWER_PATH = "command.orderSearchForm.minInvestmentLimitPerBorrower";
    public static final String CREATION_DATE_EXPR = "orderSearchForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.orderSearchForm.creationDate";
    public static final String VALID_UNTIL_EXPR = "orderSearchForm.validUntil";
    public static final String VALID_UNTIL_PATH = "command.orderSearchForm.validUntil";
    public static final String PARTNER_VALID_UNTIL_EXPR = "orderSearchForm.partnerValidUntil";
    public static final String PARTNER_VALID_UNTIL_PATH = "command.orderSearchForm.partnerValidUntil";
    public static final String STATE_EXPR = "orderSearchForm.state";
    public static final String STATE_PATH = "command.orderSearchForm.state";
    public static final String TYPE_EXPR = "orderSearchForm.type";
    public static final String TYPE_PATH = "command.orderSearchForm.type";
    public static final String OFFER_CONFIRMED_EXPR = "orderSearchForm.offerConfirmed";
    public static final String OFFER_CONFIRMED_PATH = "command.orderSearchForm.offerConfirmed";
    public static final String TRANSACTION_PIN_EXPR = "orderSearchForm.transactionPin";
    public static final String TRANSACTION_PIN_PATH = "command.orderSearchForm.transactionPin";
    public static final String TERMS_AND_CONDITIONS_EXPR = "orderSearchForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_PATH = "command.orderSearchForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_APPLIED_EXPR = "orderSearchForm.termsAndConditionsApplied";
    public static final String TERMS_AND_CONDITIONS_APPLIED_PATH = "command.orderSearchForm.termsAndConditionsApplied";
    public static final String AGE_GROUPS_EXPR = "orderSearchForm.ageGroups";
    public static final String AGE_GROUPS_PATH = "command.orderSearchForm.ageGroups";
    public static final String GENDERS_EXPR = "orderSearchForm.genders";
    public static final String GENDERS_PATH = "command.orderSearchForm.genders";
    public static final String INDIVIDUAL_FACTORIZATION_CONTRACT_CONFIRM_EXPR = "orderSearchForm.individualFactorizationContractConfirm";
    public static final String INDIVIDUAL_FACTORIZATION_CONTRACT_CONFIRM_PATH = "command.orderSearchForm.individualFactorizationContractConfirm";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(order search)}
    /**
	 * generated serial.
	 */
	private static final long serialVersionUID = 8666620201457871575L;
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _bidSearchId;
    private Long _bidSearchIdInitVal;
    private boolean _bidSearchIdIsSet;
    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _ratingMin;
    private String _ratingMinInitVal;
    private boolean _ratingMinIsSet;
    private String _ratingMax;
    private String _ratingMaxInitVal;
    private boolean _ratingMaxIsSet;
    private List<String> _durations;
    private List<String> _durationsInitVal;
    private boolean _durationsIsSet;
    private String _interestMin;
    private String _interestMinInitVal;
    private boolean _interestMinIsSet;
    private String _interestMax;
    private String _interestMaxInitVal;
    private boolean _interestMaxIsSet;
    private String _roiMin;
    private String _roiMinInitVal;
    private boolean _roiMinIsSet;
    private String _roiMax;
    private String _roiMaxInitVal;
    private boolean _roiMaxIsSet;
    private String _creditRateIndicator;
    private String _creditRateIndicatorInitVal;
    private boolean _creditRateIndicatorIsSet;
    private String _amountMin;
    private String _amountMinInitVal;
    private boolean _amountMinIsSet;
    private String _amountMax;
    private String _amountMaxInitVal;
    private boolean _amountMaxIsSet;
    private String _matchedRate;
    private String _matchedRateInitVal;
    private boolean _matchedRateIsSet;
    private String _searchPhrase;
    private String _searchPhraseInitVal;
    private boolean _searchPhraseIsSet;
    private Boolean _includeFinishedOrders;
    private Boolean _includeFinishedOrdersInitVal;
    private boolean _includeFinishedOrdersIsSet;
    private String _employment;
    private String _employmentInitVal;
    private boolean _employmentIsSet;
    private boolean _isMailNotificationWanted;
    private boolean _isMailNotificationWantedInitVal;
    private boolean _isMailNotificationWantedIsSet;
    private List<String> _employments;
    private List<String> _employmentsInitVal;
    private boolean _employmentsIsSet;
    private String _ageRangeMin;
    private String _ageRangeMinInitVal;
    private boolean _ageRangeMinIsSet;
    private String _ageRangeMax;
    private String _ageRangeMaxInitVal;
    private boolean _ageRangeMaxIsSet;
    private Boolean _exceptLatePayer;
    private Boolean _exceptLatePayerInitVal;
    private boolean _exceptLatePayerIsSet;
    private String _investmentAmount;
    private String _investmentAmountInitVal;
    private boolean _investmentAmountIsSet;
    private String _investmentLimitPerBorrower;
    private String _investmentLimitPerBorrowerInitVal;
    private boolean _investmentLimitPerBorrowerIsSet;
    private String _minInvestmentLimitPerBorrower;
    private String _minInvestmentLimitPerBorrowerInitVal;
    private boolean _minInvestmentLimitPerBorrowerIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _validUntil;
    private String _validUntilInitVal;
    private boolean _validUntilIsSet;
    private String _partnerValidUntil;
    private String _partnerValidUntilInitVal;
    private boolean _partnerValidUntilIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private Boolean _offerConfirmed;
    private Boolean _offerConfirmedInitVal;
    private boolean _offerConfirmedIsSet;
    private String _transactionPin;
    private String _transactionPinInitVal;
    private boolean _transactionPinIsSet;
    private Boolean _termsAndConditions;
    private Boolean _termsAndConditionsInitVal;
    private boolean _termsAndConditionsIsSet;
    private Boolean _termsAndConditionsApplied;
    private Boolean _termsAndConditionsAppliedInitVal;
    private boolean _termsAndConditionsAppliedIsSet;
    private List<String> _ageGroups;
    private List<String> _ageGroupsInitVal;
    private boolean _ageGroupsIsSet;
    private List<String> _genders;
    private List<String> _gendersInitVal;
    private boolean _gendersIsSet;
    private Boolean _individualFactorizationContractConfirm;
    private Boolean _individualFactorizationContractConfirmInitVal;
    private boolean _individualFactorizationContractConfirmIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setBidSearchId(Long bidSearchId) {
        if (!_bidSearchIdIsSet) {
            _bidSearchIdIsSet = true;
            _bidSearchIdInitVal = bidSearchId;
        }
        _bidSearchId = bidSearchId;
    }

    public Long getBidSearchId() {
        return _bidSearchId;
    }

    public Long bidSearchIdInitVal() {
        return _bidSearchIdInitVal;
    }

	public boolean bidSearchIdIsDirty() {
        return !valuesAreEqual(_bidSearchIdInitVal, _bidSearchId);
    }

    public boolean bidSearchIdIsSet() {
        return _bidSearchIdIsSet;
    }	


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setRatingMin(String ratingMin) {
        if (!_ratingMinIsSet) {
            _ratingMinIsSet = true;
            _ratingMinInitVal = ratingMin;
        }
        _ratingMin = ratingMin;
    }

    public String getRatingMin() {
        return _ratingMin;
    }

    public String ratingMinInitVal() {
        return _ratingMinInitVal;
    }

	public boolean ratingMinIsDirty() {
        return !valuesAreEqual(_ratingMinInitVal, _ratingMin);
    }

    public boolean ratingMinIsSet() {
        return _ratingMinIsSet;
    }	


    public void setRatingMax(String ratingMax) {
        if (!_ratingMaxIsSet) {
            _ratingMaxIsSet = true;
            _ratingMaxInitVal = ratingMax;
        }
        _ratingMax = ratingMax;
    }

    public String getRatingMax() {
        return _ratingMax;
    }

    public String ratingMaxInitVal() {
        return _ratingMaxInitVal;
    }

	public boolean ratingMaxIsDirty() {
        return !valuesAreEqual(_ratingMaxInitVal, _ratingMax);
    }

    public boolean ratingMaxIsSet() {
        return _ratingMaxIsSet;
    }	


    public void setDurations(List<String> durations) {
        if (!_durationsIsSet) {
            _durationsIsSet = true;
            _durationsInitVal = durations;
        }
        _durations = durations;
    }

    public List<String> getDurations() {
        return _durations;
    }

    public List<String> durationsInitVal() {
        return _durationsInitVal;
    }

	public boolean durationsIsDirty() {
        return !valuesAreEqual(_durationsInitVal, _durations);
    }

    public boolean durationsIsSet() {
        return _durationsIsSet;
    }	


    public void setInterestMin(String interestMin) {
        if (!_interestMinIsSet) {
            _interestMinIsSet = true;
            _interestMinInitVal = interestMin;
        }
        _interestMin = interestMin;
    }

    public String getInterestMin() {
        return _interestMin;
    }

    public String interestMinInitVal() {
        return _interestMinInitVal;
    }

	public boolean interestMinIsDirty() {
        return !valuesAreEqual(_interestMinInitVal, _interestMin);
    }

    public boolean interestMinIsSet() {
        return _interestMinIsSet;
    }	


    public void setInterestMax(String interestMax) {
        if (!_interestMaxIsSet) {
            _interestMaxIsSet = true;
            _interestMaxInitVal = interestMax;
        }
        _interestMax = interestMax;
    }

    public String getInterestMax() {
        return _interestMax;
    }

    public String interestMaxInitVal() {
        return _interestMaxInitVal;
    }

	public boolean interestMaxIsDirty() {
        return !valuesAreEqual(_interestMaxInitVal, _interestMax);
    }

    public boolean interestMaxIsSet() {
        return _interestMaxIsSet;
    }	


    public void setRoiMin(String roiMin) {
        if (!_roiMinIsSet) {
            _roiMinIsSet = true;
            _roiMinInitVal = roiMin;
        }
        _roiMin = roiMin;
    }

    public String getRoiMin() {
        return _roiMin;
    }

    public String roiMinInitVal() {
        return _roiMinInitVal;
    }

	public boolean roiMinIsDirty() {
        return !valuesAreEqual(_roiMinInitVal, _roiMin);
    }

    public boolean roiMinIsSet() {
        return _roiMinIsSet;
    }	


    public void setRoiMax(String roiMax) {
        if (!_roiMaxIsSet) {
            _roiMaxIsSet = true;
            _roiMaxInitVal = roiMax;
        }
        _roiMax = roiMax;
    }

    public String getRoiMax() {
        return _roiMax;
    }

    public String roiMaxInitVal() {
        return _roiMaxInitVal;
    }

	public boolean roiMaxIsDirty() {
        return !valuesAreEqual(_roiMaxInitVal, _roiMax);
    }

    public boolean roiMaxIsSet() {
        return _roiMaxIsSet;
    }	


    public void setCreditRateIndicator(String creditRateIndicator) {
        if (!_creditRateIndicatorIsSet) {
            _creditRateIndicatorIsSet = true;
            _creditRateIndicatorInitVal = creditRateIndicator;
        }
        _creditRateIndicator = creditRateIndicator;
    }

    public String getCreditRateIndicator() {
        return _creditRateIndicator;
    }

    public String creditRateIndicatorInitVal() {
        return _creditRateIndicatorInitVal;
    }

	public boolean creditRateIndicatorIsDirty() {
        return !valuesAreEqual(_creditRateIndicatorInitVal, _creditRateIndicator);
    }

    public boolean creditRateIndicatorIsSet() {
        return _creditRateIndicatorIsSet;
    }	


    public void setAmountMin(String amountMin) {
        if (!_amountMinIsSet) {
            _amountMinIsSet = true;
            _amountMinInitVal = amountMin;
        }
        _amountMin = amountMin;
    }

    public String getAmountMin() {
        return _amountMin;
    }

    public String amountMinInitVal() {
        return _amountMinInitVal;
    }

	public boolean amountMinIsDirty() {
        return !valuesAreEqual(_amountMinInitVal, _amountMin);
    }

    public boolean amountMinIsSet() {
        return _amountMinIsSet;
    }	


    public void setAmountMax(String amountMax) {
        if (!_amountMaxIsSet) {
            _amountMaxIsSet = true;
            _amountMaxInitVal = amountMax;
        }
        _amountMax = amountMax;
    }

    public String getAmountMax() {
        return _amountMax;
    }

    public String amountMaxInitVal() {
        return _amountMaxInitVal;
    }

	public boolean amountMaxIsDirty() {
        return !valuesAreEqual(_amountMaxInitVal, _amountMax);
    }

    public boolean amountMaxIsSet() {
        return _amountMaxIsSet;
    }	


    public void setMatchedRate(String matchedRate) {
        if (!_matchedRateIsSet) {
            _matchedRateIsSet = true;
            _matchedRateInitVal = matchedRate;
        }
        _matchedRate = matchedRate;
    }

    public String getMatchedRate() {
        return _matchedRate;
    }

    public String matchedRateInitVal() {
        return _matchedRateInitVal;
    }

	public boolean matchedRateIsDirty() {
        return !valuesAreEqual(_matchedRateInitVal, _matchedRate);
    }

    public boolean matchedRateIsSet() {
        return _matchedRateIsSet;
    }	


    public void setSearchPhrase(String searchPhrase) {
        if (!_searchPhraseIsSet) {
            _searchPhraseIsSet = true;
            _searchPhraseInitVal = searchPhrase;
        }
        _searchPhrase = searchPhrase;
    }

    public String getSearchPhrase() {
        return _searchPhrase;
    }

    public String searchPhraseInitVal() {
        return _searchPhraseInitVal;
    }

	public boolean searchPhraseIsDirty() {
        return !valuesAreEqual(_searchPhraseInitVal, _searchPhrase);
    }

    public boolean searchPhraseIsSet() {
        return _searchPhraseIsSet;
    }	


    public void setIncludeFinishedOrders(Boolean includeFinishedOrders) {
        if (!_includeFinishedOrdersIsSet) {
            _includeFinishedOrdersIsSet = true;
            _includeFinishedOrdersInitVal = includeFinishedOrders;
        }
        _includeFinishedOrders = includeFinishedOrders;
    }

    public Boolean getIncludeFinishedOrders() {
        return _includeFinishedOrders;
    }

    public Boolean includeFinishedOrdersInitVal() {
        return _includeFinishedOrdersInitVal;
    }

	public boolean includeFinishedOrdersIsDirty() {
        return !valuesAreEqual(_includeFinishedOrdersInitVal, _includeFinishedOrders);
    }

    public boolean includeFinishedOrdersIsSet() {
        return _includeFinishedOrdersIsSet;
    }	


    public void setEmployment(String employment) {
        if (!_employmentIsSet) {
            _employmentIsSet = true;
            _employmentInitVal = employment;
        }
        _employment = employment;
    }

    public String getEmployment() {
        return _employment;
    }

    public String employmentInitVal() {
        return _employmentInitVal;
    }

	public boolean employmentIsDirty() {
        return !valuesAreEqual(_employmentInitVal, _employment);
    }

    public boolean employmentIsSet() {
        return _employmentIsSet;
    }	


    public void setIsMailNotificationWanted(boolean isMailNotificationWanted) {
        if (!_isMailNotificationWantedIsSet) {
            _isMailNotificationWantedIsSet = true;
            _isMailNotificationWantedInitVal = isMailNotificationWanted;
        }
        _isMailNotificationWanted = isMailNotificationWanted;
    }

    public boolean getIsMailNotificationWanted() {
        return _isMailNotificationWanted;
    }

    public boolean isMailNotificationWantedInitVal() {
        return _isMailNotificationWantedInitVal;
    }

	public boolean isMailNotificationWantedIsDirty() {
        return !valuesAreEqual(_isMailNotificationWantedInitVal, _isMailNotificationWanted);
    }

    public boolean isMailNotificationWantedIsSet() {
        return _isMailNotificationWantedIsSet;
    }	


    public void setEmployments(List<String> employments) {
        if (!_employmentsIsSet) {
            _employmentsIsSet = true;
            _employmentsInitVal = employments;
        }
        _employments = employments;
    }

    public List<String> getEmployments() {
        return _employments;
    }

    public List<String> employmentsInitVal() {
        return _employmentsInitVal;
    }

	public boolean employmentsIsDirty() {
        return !valuesAreEqual(_employmentsInitVal, _employments);
    }

    public boolean employmentsIsSet() {
        return _employmentsIsSet;
    }	


    public void setAgeRangeMin(String ageRangeMin) {
        if (!_ageRangeMinIsSet) {
            _ageRangeMinIsSet = true;
            _ageRangeMinInitVal = ageRangeMin;
        }
        _ageRangeMin = ageRangeMin;
    }

    public String getAgeRangeMin() {
        return _ageRangeMin;
    }

    public String ageRangeMinInitVal() {
        return _ageRangeMinInitVal;
    }

	public boolean ageRangeMinIsDirty() {
        return !valuesAreEqual(_ageRangeMinInitVal, _ageRangeMin);
    }

    public boolean ageRangeMinIsSet() {
        return _ageRangeMinIsSet;
    }	


    public void setAgeRangeMax(String ageRangeMax) {
        if (!_ageRangeMaxIsSet) {
            _ageRangeMaxIsSet = true;
            _ageRangeMaxInitVal = ageRangeMax;
        }
        _ageRangeMax = ageRangeMax;
    }

    public String getAgeRangeMax() {
        return _ageRangeMax;
    }

    public String ageRangeMaxInitVal() {
        return _ageRangeMaxInitVal;
    }

	public boolean ageRangeMaxIsDirty() {
        return !valuesAreEqual(_ageRangeMaxInitVal, _ageRangeMax);
    }

    public boolean ageRangeMaxIsSet() {
        return _ageRangeMaxIsSet;
    }	


    public void setExceptLatePayer(Boolean exceptLatePayer) {
        if (!_exceptLatePayerIsSet) {
            _exceptLatePayerIsSet = true;
            _exceptLatePayerInitVal = exceptLatePayer;
        }
        _exceptLatePayer = exceptLatePayer;
    }

    public Boolean getExceptLatePayer() {
        return _exceptLatePayer;
    }

    public Boolean exceptLatePayerInitVal() {
        return _exceptLatePayerInitVal;
    }

	public boolean exceptLatePayerIsDirty() {
        return !valuesAreEqual(_exceptLatePayerInitVal, _exceptLatePayer);
    }

    public boolean exceptLatePayerIsSet() {
        return _exceptLatePayerIsSet;
    }	


    public void setInvestmentAmount(String investmentAmount) {
        if (!_investmentAmountIsSet) {
            _investmentAmountIsSet = true;
            _investmentAmountInitVal = investmentAmount;
        }
        _investmentAmount = investmentAmount;
    }

    public String getInvestmentAmount() {
        return _investmentAmount;
    }

    public String investmentAmountInitVal() {
        return _investmentAmountInitVal;
    }

	public boolean investmentAmountIsDirty() {
        return !valuesAreEqual(_investmentAmountInitVal, _investmentAmount);
    }

    public boolean investmentAmountIsSet() {
        return _investmentAmountIsSet;
    }	


    public void setInvestmentLimitPerBorrower(String investmentLimitPerBorrower) {
        if (!_investmentLimitPerBorrowerIsSet) {
            _investmentLimitPerBorrowerIsSet = true;
            _investmentLimitPerBorrowerInitVal = investmentLimitPerBorrower;
        }
        _investmentLimitPerBorrower = investmentLimitPerBorrower;
    }

    public String getInvestmentLimitPerBorrower() {
        return _investmentLimitPerBorrower;
    }

    public String investmentLimitPerBorrowerInitVal() {
        return _investmentLimitPerBorrowerInitVal;
    }

	public boolean investmentLimitPerBorrowerIsDirty() {
        return !valuesAreEqual(_investmentLimitPerBorrowerInitVal, _investmentLimitPerBorrower);
    }

    public boolean investmentLimitPerBorrowerIsSet() {
        return _investmentLimitPerBorrowerIsSet;
    }	


    public void setMinInvestmentLimitPerBorrower(String minInvestmentLimitPerBorrower) {
        if (!_minInvestmentLimitPerBorrowerIsSet) {
            _minInvestmentLimitPerBorrowerIsSet = true;
            _minInvestmentLimitPerBorrowerInitVal = minInvestmentLimitPerBorrower;
        }
        _minInvestmentLimitPerBorrower = minInvestmentLimitPerBorrower;
    }

    public String getMinInvestmentLimitPerBorrower() {
        return _minInvestmentLimitPerBorrower;
    }

    public String minInvestmentLimitPerBorrowerInitVal() {
        return _minInvestmentLimitPerBorrowerInitVal;
    }

	public boolean minInvestmentLimitPerBorrowerIsDirty() {
        return !valuesAreEqual(_minInvestmentLimitPerBorrowerInitVal, _minInvestmentLimitPerBorrower);
    }

    public boolean minInvestmentLimitPerBorrowerIsSet() {
        return _minInvestmentLimitPerBorrowerIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setValidUntil(String validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = validUntil;
        }
        _validUntil = validUntil;
    }

    public String getValidUntil() {
        return _validUntil;
    }

    public String validUntilInitVal() {
        return _validUntilInitVal;
    }

	public boolean validUntilIsDirty() {
        return !valuesAreEqual(_validUntilInitVal, _validUntil);
    }

    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }	


    public void setPartnerValidUntil(String partnerValidUntil) {
        if (!_partnerValidUntilIsSet) {
            _partnerValidUntilIsSet = true;
            _partnerValidUntilInitVal = partnerValidUntil;
        }
        _partnerValidUntil = partnerValidUntil;
    }

    public String getPartnerValidUntil() {
        return _partnerValidUntil;
    }

    public String partnerValidUntilInitVal() {
        return _partnerValidUntilInitVal;
    }

	public boolean partnerValidUntilIsDirty() {
        return !valuesAreEqual(_partnerValidUntilInitVal, _partnerValidUntil);
    }

    public boolean partnerValidUntilIsSet() {
        return _partnerValidUntilIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setOfferConfirmed(Boolean offerConfirmed) {
        if (!_offerConfirmedIsSet) {
            _offerConfirmedIsSet = true;
            _offerConfirmedInitVal = offerConfirmed;
        }
        _offerConfirmed = offerConfirmed;
    }

    public Boolean getOfferConfirmed() {
        return _offerConfirmed;
    }

    public Boolean offerConfirmedInitVal() {
        return _offerConfirmedInitVal;
    }

	public boolean offerConfirmedIsDirty() {
        return !valuesAreEqual(_offerConfirmedInitVal, _offerConfirmed);
    }

    public boolean offerConfirmedIsSet() {
        return _offerConfirmedIsSet;
    }	


    public void setTransactionPin(String transactionPin) {
        if (!_transactionPinIsSet) {
            _transactionPinIsSet = true;
            _transactionPinInitVal = transactionPin;
        }
        _transactionPin = transactionPin;
    }

    public String getTransactionPin() {
        return _transactionPin;
    }

    public String transactionPinInitVal() {
        return _transactionPinInitVal;
    }

	public boolean transactionPinIsDirty() {
        return !valuesAreEqual(_transactionPinInitVal, _transactionPin);
    }

    public boolean transactionPinIsSet() {
        return _transactionPinIsSet;
    }	


    public void setTermsAndConditions(Boolean termsAndConditions) {
        if (!_termsAndConditionsIsSet) {
            _termsAndConditionsIsSet = true;
            _termsAndConditionsInitVal = termsAndConditions;
        }
        _termsAndConditions = termsAndConditions;
    }

    public Boolean getTermsAndConditions() {
        return _termsAndConditions;
    }

    public Boolean termsAndConditionsInitVal() {
        return _termsAndConditionsInitVal;
    }

	public boolean termsAndConditionsIsDirty() {
        return !valuesAreEqual(_termsAndConditionsInitVal, _termsAndConditions);
    }

    public boolean termsAndConditionsIsSet() {
        return _termsAndConditionsIsSet;
    }	


    public void setTermsAndConditionsApplied(Boolean termsAndConditionsApplied) {
        if (!_termsAndConditionsAppliedIsSet) {
            _termsAndConditionsAppliedIsSet = true;
            _termsAndConditionsAppliedInitVal = termsAndConditionsApplied;
        }
        _termsAndConditionsApplied = termsAndConditionsApplied;
    }

    public Boolean getTermsAndConditionsApplied() {
        return _termsAndConditionsApplied;
    }

    public Boolean termsAndConditionsAppliedInitVal() {
        return _termsAndConditionsAppliedInitVal;
    }

	public boolean termsAndConditionsAppliedIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAppliedInitVal, _termsAndConditionsApplied);
    }

    public boolean termsAndConditionsAppliedIsSet() {
        return _termsAndConditionsAppliedIsSet;
    }	


    public void setAgeGroups(List<String> ageGroups) {
        if (!_ageGroupsIsSet) {
            _ageGroupsIsSet = true;
            _ageGroupsInitVal = ageGroups;
        }
        _ageGroups = ageGroups;
    }

    public List<String> getAgeGroups() {
        return _ageGroups;
    }

    public List<String> ageGroupsInitVal() {
        return _ageGroupsInitVal;
    }

	public boolean ageGroupsIsDirty() {
        return !valuesAreEqual(_ageGroupsInitVal, _ageGroups);
    }

    public boolean ageGroupsIsSet() {
        return _ageGroupsIsSet;
    }	


    public void setGenders(List<String> genders) {
        if (!_gendersIsSet) {
            _gendersIsSet = true;
            _gendersInitVal = genders;
        }
        _genders = genders;
    }

    public List<String> getGenders() {
        return _genders;
    }

    public List<String> gendersInitVal() {
        return _gendersInitVal;
    }

	public boolean gendersIsDirty() {
        return !valuesAreEqual(_gendersInitVal, _genders);
    }

    public boolean gendersIsSet() {
        return _gendersIsSet;
    }	


    public void setIndividualFactorizationContractConfirm(Boolean individualFactorizationContractConfirm) {
        if (!_individualFactorizationContractConfirmIsSet) {
            _individualFactorizationContractConfirmIsSet = true;
            _individualFactorizationContractConfirmInitVal = individualFactorizationContractConfirm;
        }
        _individualFactorizationContractConfirm = individualFactorizationContractConfirm;
    }

    public Boolean getIndividualFactorizationContractConfirm() {
        return _individualFactorizationContractConfirm;
    }

    public Boolean individualFactorizationContractConfirmInitVal() {
        return _individualFactorizationContractConfirmInitVal;
    }

	public boolean individualFactorizationContractConfirmIsDirty() {
        return !valuesAreEqual(_individualFactorizationContractConfirmInitVal, _individualFactorizationContractConfirm);
    }

    public boolean individualFactorizationContractConfirmIsSet() {
        return _individualFactorizationContractConfirmIsSet;
    }	

}
