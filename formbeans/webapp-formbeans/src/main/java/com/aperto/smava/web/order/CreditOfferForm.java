//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.order;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(credit offer)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CreditOfferForm'.
 *
 * @author generator
 */
public class CreditOfferForm implements Serializable {

	public static final String BROKERAGE_AGREEMENT_CHECK_EXPR = "creditOfferForm.brokerageAgreementCheck";
    public static final String BROKERAGE_AGREEMENT_CHECK_PATH = "command.creditOfferForm.brokerageAgreementCheck";
    public static final String SCHUFA_AGREEMENT_CHECK_EXPR = "creditOfferForm.schufaAgreementCheck";
    public static final String SCHUFA_AGREEMENT_CHECK_PATH = "command.creditOfferForm.schufaAgreementCheck";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(credit offer)}

    /**
     * 
     */
    private static final long serialVersionUID = -4852633095717680184L;

    // !!!!!!!! End of insert code section !!!!!!!!

    private boolean _brokerageAgreementCheck;
    private boolean _brokerageAgreementCheckInitVal;
    private boolean _brokerageAgreementCheckIsSet;
    private boolean _schufaAgreementCheck;
    private boolean _schufaAgreementCheckInitVal;
    private boolean _schufaAgreementCheckIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setBrokerageAgreementCheck(boolean brokerageAgreementCheck) {
        if (!_brokerageAgreementCheckIsSet) {
            _brokerageAgreementCheckIsSet = true;
            _brokerageAgreementCheckInitVal = brokerageAgreementCheck;
        }
        _brokerageAgreementCheck = brokerageAgreementCheck;
    }

    public boolean getBrokerageAgreementCheck() {
        return _brokerageAgreementCheck;
    }

    public boolean brokerageAgreementCheckInitVal() {
        return _brokerageAgreementCheckInitVal;
    }

	public boolean brokerageAgreementCheckIsDirty() {
        return !valuesAreEqual(_brokerageAgreementCheckInitVal, _brokerageAgreementCheck);
    }

    public boolean brokerageAgreementCheckIsSet() {
        return _brokerageAgreementCheckIsSet;
    }	


    public void setSchufaAgreementCheck(boolean schufaAgreementCheck) {
        if (!_schufaAgreementCheckIsSet) {
            _schufaAgreementCheckIsSet = true;
            _schufaAgreementCheckInitVal = schufaAgreementCheck;
        }
        _schufaAgreementCheck = schufaAgreementCheck;
    }

    public boolean getSchufaAgreementCheck() {
        return _schufaAgreementCheck;
    }

    public boolean schufaAgreementCheckInitVal() {
        return _schufaAgreementCheckInitVal;
    }

	public boolean schufaAgreementCheckIsDirty() {
        return !valuesAreEqual(_schufaAgreementCheckInitVal, _schufaAgreementCheck);
    }

    public boolean schufaAgreementCheckIsSet() {
        return _schufaAgreementCheckIsSet;
    }	

}
