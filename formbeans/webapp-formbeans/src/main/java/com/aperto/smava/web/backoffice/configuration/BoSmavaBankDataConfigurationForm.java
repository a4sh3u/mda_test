//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk-createOrderFlow\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk-createOrderFlow\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.configuration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo smava bank data configuration)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoSmavaBankDataConfigurationForm'.
 *
 * @author generator
 */
public class BoSmavaBankDataConfigurationForm implements Serializable {

    public static final String SMAVA_ACCOUNT_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.smavaAccountNumber";
    public static final String SMAVA_ACCOUNT_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.smavaAccountNumber";
    public static final String SMAVA_ACCOUNT_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.smavaAccountBankName";
    public static final String SMAVA_ACCOUNT_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.smavaAccountBankName";
    public static final String SMAVA_ACCOUNT_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.smavaAccountBankCode";
    public static final String SMAVA_ACCOUNT_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.smavaAccountBankCode";
    public static final String OUT_PAYMENT_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.outPaymentNumber";
    public static final String OUT_PAYMENT_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.outPaymentNumber";
    public static final String OUT_PAYMENT_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.outPaymentBankName";
    public static final String OUT_PAYMENT_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.outPaymentBankName";
    public static final String OUT_PAYMENT_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.outPaymentBankCode";
    public static final String OUT_PAYMENT_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.outPaymentBankCode";
    public static final String CALL_MONEY_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.callMoneyNumber";
    public static final String CALL_MONEY_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.callMoneyNumber";
    public static final String CALL_MONEY_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.callMoneyBankName";
    public static final String CALL_MONEY_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.callMoneyBankName";
    public static final String CALL_MONEY_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.callMoneyBankCode";
    public static final String CALL_MONEY_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.callMoneyBankCode";
    public static final String SMAVA_INTERIM_ACCOUNT_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.smavaInterimAccountNumber";
    public static final String SMAVA_INTERIM_ACCOUNT_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.smavaInterimAccountNumber";
    public static final String SMAVA_INTERIM_ACCOUNT_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.smavaInterimAccountBankName";
    public static final String SMAVA_INTERIM_ACCOUNT_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.smavaInterimAccountBankName";
    public static final String SMAVA_INTERIM_ACCOUNT_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.smavaInterimAccountBankCode";
    public static final String SMAVA_INTERIM_ACCOUNT_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.smavaInterimAccountBankCode";
    public static final String RDI_PREMIUM_ACCOUNT_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.rdiPremiumAccountNumber";
    public static final String RDI_PREMIUM_ACCOUNT_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.rdiPremiumAccountNumber";
    public static final String RDI_PREMIUM_ACCOUNT_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.rdiPremiumAccountBankName";
    public static final String RDI_PREMIUM_ACCOUNT_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.rdiPremiumAccountBankName";
    public static final String RDI_PREMIUM_ACCOUNT_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.rdiPremiumAccountBankCode";
    public static final String RDI_PREMIUM_ACCOUNT_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.rdiPremiumAccountBankCode";
    public static final String RDI_AMORTIZATION_ACCOUNT_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.rdiAmortizationAccountNumber";
    public static final String RDI_AMORTIZATION_ACCOUNT_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.rdiAmortizationAccountNumber";
    public static final String RDI_AMORTIZATION_ACCOUNT_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.rdiAmortizationAccountBankName";
    public static final String RDI_AMORTIZATION_ACCOUNT_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.rdiAmortizationAccountBankName";
    public static final String RDI_AMORTIZATION_ACCOUNT_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.rdiAmortizationAccountBankCode";
    public static final String RDI_AMORTIZATION_ACCOUNT_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.rdiAmortizationAccountBankCode";
    public static final String ENCASHMENT_BANK_DEPOSITOR_EXPR = "boSmavaBankDataConfigurationForm.encashmentBankDepositor";
    public static final String ENCASHMENT_BANK_DEPOSITOR_PATH = "command.boSmavaBankDataConfigurationForm.encashmentBankDepositor";
    public static final String ENCASHMENT_BANK_ACCOUNT_NUMBER_EXPR = "boSmavaBankDataConfigurationForm.encashmentBankAccountNumber";
    public static final String ENCASHMENT_BANK_ACCOUNT_NUMBER_PATH = "command.boSmavaBankDataConfigurationForm.encashmentBankAccountNumber";
    public static final String ENCASHMENT_BANK_CODE_EXPR = "boSmavaBankDataConfigurationForm.encashmentBankCode";
    public static final String ENCASHMENT_BANK_CODE_PATH = "command.boSmavaBankDataConfigurationForm.encashmentBankCode";
    public static final String ENCASHMENT_BANK_NAME_EXPR = "boSmavaBankDataConfigurationForm.encashmentBankName";
    public static final String ENCASHMENT_BANK_NAME_PATH = "command.boSmavaBankDataConfigurationForm.encashmentBankName";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo smava bank data configuration)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _smavaAccountNumber;
    private String _smavaAccountNumberInitVal;
    private boolean _smavaAccountNumberIsSet;
    private String _smavaAccountBankName;
    private String _smavaAccountBankNameInitVal;
    private boolean _smavaAccountBankNameIsSet;
    private String _smavaAccountBankCode;
    private String _smavaAccountBankCodeInitVal;
    private boolean _smavaAccountBankCodeIsSet;
    private String _outPaymentNumber;
    private String _outPaymentNumberInitVal;
    private boolean _outPaymentNumberIsSet;
    private String _outPaymentBankName;
    private String _outPaymentBankNameInitVal;
    private boolean _outPaymentBankNameIsSet;
    private String _outPaymentBankCode;
    private String _outPaymentBankCodeInitVal;
    private boolean _outPaymentBankCodeIsSet;
    private String _callMoneyNumber;
    private String _callMoneyNumberInitVal;
    private boolean _callMoneyNumberIsSet;
    private String _callMoneyBankName;
    private String _callMoneyBankNameInitVal;
    private boolean _callMoneyBankNameIsSet;
    private String _callMoneyBankCode;
    private String _callMoneyBankCodeInitVal;
    private boolean _callMoneyBankCodeIsSet;
    private String _smavaInterimAccountNumber;
    private String _smavaInterimAccountNumberInitVal;
    private boolean _smavaInterimAccountNumberIsSet;
    private String _smavaInterimAccountBankName;
    private String _smavaInterimAccountBankNameInitVal;
    private boolean _smavaInterimAccountBankNameIsSet;
    private String _smavaInterimAccountBankCode;
    private String _smavaInterimAccountBankCodeInitVal;
    private boolean _smavaInterimAccountBankCodeIsSet;
    private String _rdiPremiumAccountNumber;
    private String _rdiPremiumAccountNumberInitVal;
    private boolean _rdiPremiumAccountNumberIsSet;
    private String _rdiPremiumAccountBankName;
    private String _rdiPremiumAccountBankNameInitVal;
    private boolean _rdiPremiumAccountBankNameIsSet;
    private String _rdiPremiumAccountBankCode;
    private String _rdiPremiumAccountBankCodeInitVal;
    private boolean _rdiPremiumAccountBankCodeIsSet;
    private String _rdiAmortizationAccountNumber;
    private String _rdiAmortizationAccountNumberInitVal;
    private boolean _rdiAmortizationAccountNumberIsSet;
    private String _rdiAmortizationAccountBankName;
    private String _rdiAmortizationAccountBankNameInitVal;
    private boolean _rdiAmortizationAccountBankNameIsSet;
    private String _rdiAmortizationAccountBankCode;
    private String _rdiAmortizationAccountBankCodeInitVal;
    private boolean _rdiAmortizationAccountBankCodeIsSet;
    private String _encashmentBankDepositor;
    private String _encashmentBankDepositorInitVal;
    private boolean _encashmentBankDepositorIsSet;
    private String _encashmentBankAccountNumber;
    private String _encashmentBankAccountNumberInitVal;
    private boolean _encashmentBankAccountNumberIsSet;
    private String _encashmentBankCode;
    private String _encashmentBankCodeInitVal;
    private boolean _encashmentBankCodeIsSet;
    private String _encashmentBankName;
    private String _encashmentBankNameInitVal;
    private boolean _encashmentBankNameIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setSmavaAccountNumber(String smavaAccountNumber) {
        if (!_smavaAccountNumberIsSet) {
            _smavaAccountNumberIsSet = true;
            _smavaAccountNumberInitVal = smavaAccountNumber;
        }
        _smavaAccountNumber = smavaAccountNumber;
    }

    public String getSmavaAccountNumber() {
        return _smavaAccountNumber;
    }

    public String smavaAccountNumberInitVal() {
        return _smavaAccountNumberInitVal;
    }

	public boolean smavaAccountNumberIsDirty() {
        return !valuesAreEqual(_smavaAccountNumberInitVal, _smavaAccountNumber);
    }

    public boolean smavaAccountNumberIsSet() {
        return _smavaAccountNumberIsSet;
    }	


    public void setSmavaAccountBankName(String smavaAccountBankName) {
        if (!_smavaAccountBankNameIsSet) {
            _smavaAccountBankNameIsSet = true;
            _smavaAccountBankNameInitVal = smavaAccountBankName;
        }
        _smavaAccountBankName = smavaAccountBankName;
    }

    public String getSmavaAccountBankName() {
        return _smavaAccountBankName;
    }

    public String smavaAccountBankNameInitVal() {
        return _smavaAccountBankNameInitVal;
    }

	public boolean smavaAccountBankNameIsDirty() {
        return !valuesAreEqual(_smavaAccountBankNameInitVal, _smavaAccountBankName);
    }

    public boolean smavaAccountBankNameIsSet() {
        return _smavaAccountBankNameIsSet;
    }	


    public void setSmavaAccountBankCode(String smavaAccountBankCode) {
        if (!_smavaAccountBankCodeIsSet) {
            _smavaAccountBankCodeIsSet = true;
            _smavaAccountBankCodeInitVal = smavaAccountBankCode;
        }
        _smavaAccountBankCode = smavaAccountBankCode;
    }

    public String getSmavaAccountBankCode() {
        return _smavaAccountBankCode;
    }

    public String smavaAccountBankCodeInitVal() {
        return _smavaAccountBankCodeInitVal;
    }

	public boolean smavaAccountBankCodeIsDirty() {
        return !valuesAreEqual(_smavaAccountBankCodeInitVal, _smavaAccountBankCode);
    }

    public boolean smavaAccountBankCodeIsSet() {
        return _smavaAccountBankCodeIsSet;
    }	


    public void setOutPaymentNumber(String outPaymentNumber) {
        if (!_outPaymentNumberIsSet) {
            _outPaymentNumberIsSet = true;
            _outPaymentNumberInitVal = outPaymentNumber;
        }
        _outPaymentNumber = outPaymentNumber;
    }

    public String getOutPaymentNumber() {
        return _outPaymentNumber;
    }

    public String outPaymentNumberInitVal() {
        return _outPaymentNumberInitVal;
    }

	public boolean outPaymentNumberIsDirty() {
        return !valuesAreEqual(_outPaymentNumberInitVal, _outPaymentNumber);
    }

    public boolean outPaymentNumberIsSet() {
        return _outPaymentNumberIsSet;
    }	


    public void setOutPaymentBankName(String outPaymentBankName) {
        if (!_outPaymentBankNameIsSet) {
            _outPaymentBankNameIsSet = true;
            _outPaymentBankNameInitVal = outPaymentBankName;
        }
        _outPaymentBankName = outPaymentBankName;
    }

    public String getOutPaymentBankName() {
        return _outPaymentBankName;
    }

    public String outPaymentBankNameInitVal() {
        return _outPaymentBankNameInitVal;
    }

	public boolean outPaymentBankNameIsDirty() {
        return !valuesAreEqual(_outPaymentBankNameInitVal, _outPaymentBankName);
    }

    public boolean outPaymentBankNameIsSet() {
        return _outPaymentBankNameIsSet;
    }	


    public void setOutPaymentBankCode(String outPaymentBankCode) {
        if (!_outPaymentBankCodeIsSet) {
            _outPaymentBankCodeIsSet = true;
            _outPaymentBankCodeInitVal = outPaymentBankCode;
        }
        _outPaymentBankCode = outPaymentBankCode;
    }

    public String getOutPaymentBankCode() {
        return _outPaymentBankCode;
    }

    public String outPaymentBankCodeInitVal() {
        return _outPaymentBankCodeInitVal;
    }

	public boolean outPaymentBankCodeIsDirty() {
        return !valuesAreEqual(_outPaymentBankCodeInitVal, _outPaymentBankCode);
    }

    public boolean outPaymentBankCodeIsSet() {
        return _outPaymentBankCodeIsSet;
    }	


    public void setCallMoneyNumber(String callMoneyNumber) {
        if (!_callMoneyNumberIsSet) {
            _callMoneyNumberIsSet = true;
            _callMoneyNumberInitVal = callMoneyNumber;
        }
        _callMoneyNumber = callMoneyNumber;
    }

    public String getCallMoneyNumber() {
        return _callMoneyNumber;
    }

    public String callMoneyNumberInitVal() {
        return _callMoneyNumberInitVal;
    }

	public boolean callMoneyNumberIsDirty() {
        return !valuesAreEqual(_callMoneyNumberInitVal, _callMoneyNumber);
    }

    public boolean callMoneyNumberIsSet() {
        return _callMoneyNumberIsSet;
    }	


    public void setCallMoneyBankName(String callMoneyBankName) {
        if (!_callMoneyBankNameIsSet) {
            _callMoneyBankNameIsSet = true;
            _callMoneyBankNameInitVal = callMoneyBankName;
        }
        _callMoneyBankName = callMoneyBankName;
    }

    public String getCallMoneyBankName() {
        return _callMoneyBankName;
    }

    public String callMoneyBankNameInitVal() {
        return _callMoneyBankNameInitVal;
    }

	public boolean callMoneyBankNameIsDirty() {
        return !valuesAreEqual(_callMoneyBankNameInitVal, _callMoneyBankName);
    }

    public boolean callMoneyBankNameIsSet() {
        return _callMoneyBankNameIsSet;
    }	


    public void setCallMoneyBankCode(String callMoneyBankCode) {
        if (!_callMoneyBankCodeIsSet) {
            _callMoneyBankCodeIsSet = true;
            _callMoneyBankCodeInitVal = callMoneyBankCode;
        }
        _callMoneyBankCode = callMoneyBankCode;
    }

    public String getCallMoneyBankCode() {
        return _callMoneyBankCode;
    }

    public String callMoneyBankCodeInitVal() {
        return _callMoneyBankCodeInitVal;
    }

	public boolean callMoneyBankCodeIsDirty() {
        return !valuesAreEqual(_callMoneyBankCodeInitVal, _callMoneyBankCode);
    }

    public boolean callMoneyBankCodeIsSet() {
        return _callMoneyBankCodeIsSet;
    }	


    public void setSmavaInterimAccountNumber(String smavaInterimAccountNumber) {
        if (!_smavaInterimAccountNumberIsSet) {
            _smavaInterimAccountNumberIsSet = true;
            _smavaInterimAccountNumberInitVal = smavaInterimAccountNumber;
        }
        _smavaInterimAccountNumber = smavaInterimAccountNumber;
    }

    public String getSmavaInterimAccountNumber() {
        return _smavaInterimAccountNumber;
    }

    public String smavaInterimAccountNumberInitVal() {
        return _smavaInterimAccountNumberInitVal;
    }

	public boolean smavaInterimAccountNumberIsDirty() {
        return !valuesAreEqual(_smavaInterimAccountNumberInitVal, _smavaInterimAccountNumber);
    }

    public boolean smavaInterimAccountNumberIsSet() {
        return _smavaInterimAccountNumberIsSet;
    }	


    public void setSmavaInterimAccountBankName(String smavaInterimAccountBankName) {
        if (!_smavaInterimAccountBankNameIsSet) {
            _smavaInterimAccountBankNameIsSet = true;
            _smavaInterimAccountBankNameInitVal = smavaInterimAccountBankName;
        }
        _smavaInterimAccountBankName = smavaInterimAccountBankName;
    }

    public String getSmavaInterimAccountBankName() {
        return _smavaInterimAccountBankName;
    }

    public String smavaInterimAccountBankNameInitVal() {
        return _smavaInterimAccountBankNameInitVal;
    }

	public boolean smavaInterimAccountBankNameIsDirty() {
        return !valuesAreEqual(_smavaInterimAccountBankNameInitVal, _smavaInterimAccountBankName);
    }

    public boolean smavaInterimAccountBankNameIsSet() {
        return _smavaInterimAccountBankNameIsSet;
    }	


    public void setSmavaInterimAccountBankCode(String smavaInterimAccountBankCode) {
        if (!_smavaInterimAccountBankCodeIsSet) {
            _smavaInterimAccountBankCodeIsSet = true;
            _smavaInterimAccountBankCodeInitVal = smavaInterimAccountBankCode;
        }
        _smavaInterimAccountBankCode = smavaInterimAccountBankCode;
    }

    public String getSmavaInterimAccountBankCode() {
        return _smavaInterimAccountBankCode;
    }

    public String smavaInterimAccountBankCodeInitVal() {
        return _smavaInterimAccountBankCodeInitVal;
    }

	public boolean smavaInterimAccountBankCodeIsDirty() {
        return !valuesAreEqual(_smavaInterimAccountBankCodeInitVal, _smavaInterimAccountBankCode);
    }

    public boolean smavaInterimAccountBankCodeIsSet() {
        return _smavaInterimAccountBankCodeIsSet;
    }	


    public void setRdiPremiumAccountNumber(String rdiPremiumAccountNumber) {
        if (!_rdiPremiumAccountNumberIsSet) {
            _rdiPremiumAccountNumberIsSet = true;
            _rdiPremiumAccountNumberInitVal = rdiPremiumAccountNumber;
        }
        _rdiPremiumAccountNumber = rdiPremiumAccountNumber;
    }

    public String getRdiPremiumAccountNumber() {
        return _rdiPremiumAccountNumber;
    }

    public String rdiPremiumAccountNumberInitVal() {
        return _rdiPremiumAccountNumberInitVal;
    }

	public boolean rdiPremiumAccountNumberIsDirty() {
        return !valuesAreEqual(_rdiPremiumAccountNumberInitVal, _rdiPremiumAccountNumber);
    }

    public boolean rdiPremiumAccountNumberIsSet() {
        return _rdiPremiumAccountNumberIsSet;
    }	


    public void setRdiPremiumAccountBankName(String rdiPremiumAccountBankName) {
        if (!_rdiPremiumAccountBankNameIsSet) {
            _rdiPremiumAccountBankNameIsSet = true;
            _rdiPremiumAccountBankNameInitVal = rdiPremiumAccountBankName;
        }
        _rdiPremiumAccountBankName = rdiPremiumAccountBankName;
    }

    public String getRdiPremiumAccountBankName() {
        return _rdiPremiumAccountBankName;
    }

    public String rdiPremiumAccountBankNameInitVal() {
        return _rdiPremiumAccountBankNameInitVal;
    }

	public boolean rdiPremiumAccountBankNameIsDirty() {
        return !valuesAreEqual(_rdiPremiumAccountBankNameInitVal, _rdiPremiumAccountBankName);
    }

    public boolean rdiPremiumAccountBankNameIsSet() {
        return _rdiPremiumAccountBankNameIsSet;
    }	


    public void setRdiPremiumAccountBankCode(String rdiPremiumAccountBankCode) {
        if (!_rdiPremiumAccountBankCodeIsSet) {
            _rdiPremiumAccountBankCodeIsSet = true;
            _rdiPremiumAccountBankCodeInitVal = rdiPremiumAccountBankCode;
        }
        _rdiPremiumAccountBankCode = rdiPremiumAccountBankCode;
    }

    public String getRdiPremiumAccountBankCode() {
        return _rdiPremiumAccountBankCode;
    }

    public String rdiPremiumAccountBankCodeInitVal() {
        return _rdiPremiumAccountBankCodeInitVal;
    }

	public boolean rdiPremiumAccountBankCodeIsDirty() {
        return !valuesAreEqual(_rdiPremiumAccountBankCodeInitVal, _rdiPremiumAccountBankCode);
    }

    public boolean rdiPremiumAccountBankCodeIsSet() {
        return _rdiPremiumAccountBankCodeIsSet;
    }	


    public void setRdiAmortizationAccountNumber(String rdiAmortizationAccountNumber) {
        if (!_rdiAmortizationAccountNumberIsSet) {
            _rdiAmortizationAccountNumberIsSet = true;
            _rdiAmortizationAccountNumberInitVal = rdiAmortizationAccountNumber;
        }
        _rdiAmortizationAccountNumber = rdiAmortizationAccountNumber;
    }

    public String getRdiAmortizationAccountNumber() {
        return _rdiAmortizationAccountNumber;
    }

    public String rdiAmortizationAccountNumberInitVal() {
        return _rdiAmortizationAccountNumberInitVal;
    }

	public boolean rdiAmortizationAccountNumberIsDirty() {
        return !valuesAreEqual(_rdiAmortizationAccountNumberInitVal, _rdiAmortizationAccountNumber);
    }

    public boolean rdiAmortizationAccountNumberIsSet() {
        return _rdiAmortizationAccountNumberIsSet;
    }	


    public void setRdiAmortizationAccountBankName(String rdiAmortizationAccountBankName) {
        if (!_rdiAmortizationAccountBankNameIsSet) {
            _rdiAmortizationAccountBankNameIsSet = true;
            _rdiAmortizationAccountBankNameInitVal = rdiAmortizationAccountBankName;
        }
        _rdiAmortizationAccountBankName = rdiAmortizationAccountBankName;
    }

    public String getRdiAmortizationAccountBankName() {
        return _rdiAmortizationAccountBankName;
    }

    public String rdiAmortizationAccountBankNameInitVal() {
        return _rdiAmortizationAccountBankNameInitVal;
    }

	public boolean rdiAmortizationAccountBankNameIsDirty() {
        return !valuesAreEqual(_rdiAmortizationAccountBankNameInitVal, _rdiAmortizationAccountBankName);
    }

    public boolean rdiAmortizationAccountBankNameIsSet() {
        return _rdiAmortizationAccountBankNameIsSet;
    }	


    public void setRdiAmortizationAccountBankCode(String rdiAmortizationAccountBankCode) {
        if (!_rdiAmortizationAccountBankCodeIsSet) {
            _rdiAmortizationAccountBankCodeIsSet = true;
            _rdiAmortizationAccountBankCodeInitVal = rdiAmortizationAccountBankCode;
        }
        _rdiAmortizationAccountBankCode = rdiAmortizationAccountBankCode;
    }

    public String getRdiAmortizationAccountBankCode() {
        return _rdiAmortizationAccountBankCode;
    }

    public String rdiAmortizationAccountBankCodeInitVal() {
        return _rdiAmortizationAccountBankCodeInitVal;
    }

	public boolean rdiAmortizationAccountBankCodeIsDirty() {
        return !valuesAreEqual(_rdiAmortizationAccountBankCodeInitVal, _rdiAmortizationAccountBankCode);
    }

    public boolean rdiAmortizationAccountBankCodeIsSet() {
        return _rdiAmortizationAccountBankCodeIsSet;
    }	


    public void setEncashmentBankDepositor(String encashmentBankDepositor) {
        if (!_encashmentBankDepositorIsSet) {
            _encashmentBankDepositorIsSet = true;
            _encashmentBankDepositorInitVal = encashmentBankDepositor;
        }
        _encashmentBankDepositor = encashmentBankDepositor;
    }

    public String getEncashmentBankDepositor() {
        return _encashmentBankDepositor;
    }

    public String encashmentBankDepositorInitVal() {
        return _encashmentBankDepositorInitVal;
    }

	public boolean encashmentBankDepositorIsDirty() {
        return !valuesAreEqual(_encashmentBankDepositorInitVal, _encashmentBankDepositor);
    }

    public boolean encashmentBankDepositorIsSet() {
        return _encashmentBankDepositorIsSet;
    }	


    public void setEncashmentBankAccountNumber(String encashmentBankAccountNumber) {
        if (!_encashmentBankAccountNumberIsSet) {
            _encashmentBankAccountNumberIsSet = true;
            _encashmentBankAccountNumberInitVal = encashmentBankAccountNumber;
        }
        _encashmentBankAccountNumber = encashmentBankAccountNumber;
    }

    public String getEncashmentBankAccountNumber() {
        return _encashmentBankAccountNumber;
    }

    public String encashmentBankAccountNumberInitVal() {
        return _encashmentBankAccountNumberInitVal;
    }

	public boolean encashmentBankAccountNumberIsDirty() {
        return !valuesAreEqual(_encashmentBankAccountNumberInitVal, _encashmentBankAccountNumber);
    }

    public boolean encashmentBankAccountNumberIsSet() {
        return _encashmentBankAccountNumberIsSet;
    }	


    public void setEncashmentBankCode(String encashmentBankCode) {
        if (!_encashmentBankCodeIsSet) {
            _encashmentBankCodeIsSet = true;
            _encashmentBankCodeInitVal = encashmentBankCode;
        }
        _encashmentBankCode = encashmentBankCode;
    }

    public String getEncashmentBankCode() {
        return _encashmentBankCode;
    }

    public String encashmentBankCodeInitVal() {
        return _encashmentBankCodeInitVal;
    }

	public boolean encashmentBankCodeIsDirty() {
        return !valuesAreEqual(_encashmentBankCodeInitVal, _encashmentBankCode);
    }

    public boolean encashmentBankCodeIsSet() {
        return _encashmentBankCodeIsSet;
    }	


    public void setEncashmentBankName(String encashmentBankName) {
        if (!_encashmentBankNameIsSet) {
            _encashmentBankNameIsSet = true;
            _encashmentBankNameInitVal = encashmentBankName;
        }
        _encashmentBankName = encashmentBankName;
    }

    public String getEncashmentBankName() {
        return _encashmentBankName;
    }

    public String encashmentBankNameInitVal() {
        return _encashmentBankNameInitVal;
    }

	public boolean encashmentBankNameIsDirty() {
        return !valuesAreEqual(_encashmentBankNameInitVal, _encashmentBankName);
    }

    public boolean encashmentBankNameIsSet() {
        return _encashmentBankNameIsSet;
    }	

}
