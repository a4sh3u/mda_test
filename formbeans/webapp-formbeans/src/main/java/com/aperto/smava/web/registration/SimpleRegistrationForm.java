//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.registration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(simple registration)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'SimpleRegistrationForm'.
 *
 * @author generator
 */
public class SimpleRegistrationForm implements Serializable {

    public static final String EMAIL_EXPR = "simpleRegistrationForm.email";
    public static final String EMAIL_PATH = "command.simpleRegistrationForm.email";
    public static final String FIRST_NAME_EXPR = "simpleRegistrationForm.firstName";
    public static final String FIRST_NAME_PATH = "command.simpleRegistrationForm.firstName";
    public static final String LAST_NAME_EXPR = "simpleRegistrationForm.lastName";
    public static final String LAST_NAME_PATH = "command.simpleRegistrationForm.lastName";
    public static final String SALUTATION_EXPR = "simpleRegistrationForm.salutation";
    public static final String SALUTATION_PATH = "command.simpleRegistrationForm.salutation";
    public static final String PRIVACY_EXPR = "simpleRegistrationForm.privacy";
    public static final String PRIVACY_PATH = "command.simpleRegistrationForm.privacy";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(simple registration)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private String _firstName;
    private String _firstNameInitVal;
    private boolean _firstNameIsSet;
    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;
    private String _salutation;
    private String _salutationInitVal;
    private boolean _salutationIsSet;
    private Boolean _privacy;
    private Boolean _privacyInitVal;
    private boolean _privacyIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = firstName;
        }
        _firstName = firstName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String firstNameInitVal() {
        return _firstNameInitVal;
    }

	public boolean firstNameIsDirty() {
        return !valuesAreEqual(_firstNameInitVal, _firstName);
    }

    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }	


    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

	public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }	


    public void setSalutation(String salutation) {
        if (!_salutationIsSet) {
            _salutationIsSet = true;
            _salutationInitVal = salutation;
        }
        _salutation = salutation;
    }

    public String getSalutation() {
        return _salutation;
    }

    public String salutationInitVal() {
        return _salutationInitVal;
    }

	public boolean salutationIsDirty() {
        return !valuesAreEqual(_salutationInitVal, _salutation);
    }

    public boolean salutationIsSet() {
        return _salutationIsSet;
    }	


    public void setPrivacy(Boolean privacy) {
        if (!_privacyIsSet) {
            _privacyIsSet = true;
            _privacyInitVal = privacy;
        }
        _privacy = privacy;
    }

    public Boolean getPrivacy() {
        return _privacy;
    }

    public Boolean privacyInitVal() {
        return _privacyInitVal;
    }

	public boolean privacyIsDirty() {
        return !valuesAreEqual(_privacyInitVal, _privacy);
    }

    public boolean privacyIsSet() {
        return _privacyIsSet;
    }	

}
