//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk-createOrderFlow\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk-createOrderFlow\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.rdi;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo rdi contract)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoRdiContractForm'.
 *
 * @author generator
 */
public class BoRdiContractForm {

    public static final String ID_EXPR = "boRdiContractForm.id";
    public static final String ID_PATH = "command.boRdiContractForm.id";
    public static final String TYPE_EXPR = "boRdiContractForm.type";
    public static final String TYPE_PATH = "command.boRdiContractForm.type";
    public static final String STATE_EXPR = "boRdiContractForm.state";
    public static final String STATE_PATH = "command.boRdiContractForm.state";
    public static final String CREATION_DATE_EXPR = "boRdiContractForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boRdiContractForm.creationDate";
    public static final String EXPIRATION_DATE_EXPR = "boRdiContractForm.expirationDate";
    public static final String EXPIRATION_DATE_PATH = "command.boRdiContractForm.expirationDate";
    public static final String FROM_DATE_EXPR = "boRdiContractForm.fromDate";
    public static final String FROM_DATE_PATH = "command.boRdiContractForm.fromDate";
    public static final String TO_DATE_EXPR = "boRdiContractForm.toDate";
    public static final String TO_DATE_PATH = "command.boRdiContractForm.toDate";
    public static final String TERMS_AND_CONDITIONS_ACCEPTANCE_DATE_EXPR = "boRdiContractForm.termsAndConditionsAcceptanceDate";
    public static final String TERMS_AND_CONDITIONS_ACCEPTANCE_DATE_PATH = "command.boRdiContractForm.termsAndConditionsAcceptanceDate";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo rdi contract)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _expirationDate;
    private String _expirationDateInitVal;
    private boolean _expirationDateIsSet;
    private String _fromDate;
    private String _fromDateInitVal;
    private boolean _fromDateIsSet;
    private String _toDate;
    private String _toDateInitVal;
    private boolean _toDateIsSet;
    private String _termsAndConditionsAcceptanceDate;
    private String _termsAndConditionsAcceptanceDateInitVal;
    private boolean _termsAndConditionsAcceptanceDateIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setExpirationDate(String expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = expirationDate;
        }
        _expirationDate = expirationDate;
    }

    public String getExpirationDate() {
        return _expirationDate;
    }

    public String expirationDateInitVal() {
        return _expirationDateInitVal;
    }

	public boolean expirationDateIsDirty() {
        return !valuesAreEqual(_expirationDateInitVal, _expirationDate);
    }

    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }	


    public void setFromDate(String fromDate) {
        if (!_fromDateIsSet) {
            _fromDateIsSet = true;
            _fromDateInitVal = fromDate;
        }
        _fromDate = fromDate;
    }

    public String getFromDate() {
        return _fromDate;
    }

    public String fromDateInitVal() {
        return _fromDateInitVal;
    }

	public boolean fromDateIsDirty() {
        return !valuesAreEqual(_fromDateInitVal, _fromDate);
    }

    public boolean fromDateIsSet() {
        return _fromDateIsSet;
    }	


    public void setToDate(String toDate) {
        if (!_toDateIsSet) {
            _toDateIsSet = true;
            _toDateInitVal = toDate;
        }
        _toDate = toDate;
    }

    public String getToDate() {
        return _toDate;
    }

    public String toDateInitVal() {
        return _toDateInitVal;
    }

	public boolean toDateIsDirty() {
        return !valuesAreEqual(_toDateInitVal, _toDate);
    }

    public boolean toDateIsSet() {
        return _toDateIsSet;
    }	


    public void setTermsAndConditionsAcceptanceDate(String termsAndConditionsAcceptanceDate) {
        if (!_termsAndConditionsAcceptanceDateIsSet) {
            _termsAndConditionsAcceptanceDateIsSet = true;
            _termsAndConditionsAcceptanceDateInitVal = termsAndConditionsAcceptanceDate;
        }
        _termsAndConditionsAcceptanceDate = termsAndConditionsAcceptanceDate;
    }

    public String getTermsAndConditionsAcceptanceDate() {
        return _termsAndConditionsAcceptanceDate;
    }

    public String termsAndConditionsAcceptanceDateInitVal() {
        return _termsAndConditionsAcceptanceDateInitVal;
    }

	public boolean termsAndConditionsAcceptanceDateIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAcceptanceDateInitVal, _termsAndConditionsAcceptanceDate);
    }

    public boolean termsAndConditionsAcceptanceDateIsSet() {
        return _termsAndConditionsAcceptanceDateIsSet;
    }	

}
