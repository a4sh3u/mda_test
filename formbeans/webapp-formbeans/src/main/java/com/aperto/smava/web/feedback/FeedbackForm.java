//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\wks\maven2\smava-webapp-parent\smava-webapp\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\wks\maven2\smava-webapp-parent\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.feedback;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(feedback)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'FeedbackForm'.
 *
 * @author generator
 */
public class FeedbackForm {

    public static final String ORDER_ID_EXPR = "feedbackForm.orderId";
    public static final String ORDER_ID_PATH = "command.feedbackForm.orderId";
    public static final String TEXT1_EXPR = "feedbackForm.text1";
    public static final String TEXT1_PATH = "command.feedbackForm.text1";
    public static final String TEXT2_EXPR = "feedbackForm.text2";
    public static final String TEXT2_PATH = "command.feedbackForm.text2";
    public static final String TEXT3_EXPR = "feedbackForm.text3";
    public static final String TEXT3_PATH = "command.feedbackForm.text3";
    public static final String ALLOWE_RELEASE_EXPR = "feedbackForm.alloweRelease";
    public static final String ALLOWE_RELEASE_PATH = "command.feedbackForm.alloweRelease";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(feedback)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _orderId;
    private Long _orderIdInitVal;
    private boolean _orderIdIsSet;
    private String _text1;
    private String _text1InitVal;
    private boolean _text1IsSet;
    private String _text2;
    private String _text2InitVal;
    private boolean _text2IsSet;
    private String _text3;
    private String _text3InitVal;
    private boolean _text3IsSet;
    private boolean _alloweRelease;
    private boolean _alloweReleaseInitVal;
    private boolean _alloweReleaseIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setOrderId(Long orderId) {
        if (!_orderIdIsSet) {
            _orderIdIsSet = true;
            _orderIdInitVal = orderId;
        }
        _orderId = orderId;
    }

    public Long getOrderId() {
        return _orderId;
    }

    public Long orderIdInitVal() {
        return _orderIdInitVal;
    }

	public boolean orderIdIsDirty() {
        return !valuesAreEqual(_orderIdInitVal, _orderId);
    }

    public boolean orderIdIsSet() {
        return _orderIdIsSet;
    }	


    public void setText1(String text1) {
        if (!_text1IsSet) {
            _text1IsSet = true;
            _text1InitVal = text1;
        }
        _text1 = text1;
    }

    public String getText1() {
        return _text1;
    }

    public String text1InitVal() {
        return _text1InitVal;
    }

	public boolean text1IsDirty() {
        return !valuesAreEqual(_text1InitVal, _text1);
    }

    public boolean text1IsSet() {
        return _text1IsSet;
    }	


    public void setText2(String text2) {
        if (!_text2IsSet) {
            _text2IsSet = true;
            _text2InitVal = text2;
        }
        _text2 = text2;
    }

    public String getText2() {
        return _text2;
    }

    public String text2InitVal() {
        return _text2InitVal;
    }

	public boolean text2IsDirty() {
        return !valuesAreEqual(_text2InitVal, _text2);
    }

    public boolean text2IsSet() {
        return _text2IsSet;
    }	


    public void setText3(String text3) {
        if (!_text3IsSet) {
            _text3IsSet = true;
            _text3InitVal = text3;
        }
        _text3 = text3;
    }

    public String getText3() {
        return _text3;
    }

    public String text3InitVal() {
        return _text3InitVal;
    }

	public boolean text3IsDirty() {
        return !valuesAreEqual(_text3InitVal, _text3);
    }

    public boolean text3IsSet() {
        return _text3IsSet;
    }	


    public void setAlloweRelease(boolean alloweRelease) {
        if (!_alloweReleaseIsSet) {
            _alloweReleaseIsSet = true;
            _alloweReleaseInitVal = alloweRelease;
        }
        _alloweRelease = alloweRelease;
    }

    public boolean getAlloweRelease() {
        return _alloweRelease;
    }

    public boolean alloweReleaseInitVal() {
        return _alloweReleaseInitVal;
    }

	public boolean alloweReleaseIsDirty() {
        return !valuesAreEqual(_alloweReleaseInitVal, _alloweRelease);
    }

    public boolean alloweReleaseIsSet() {
        return _alloweReleaseIsSet;
    }	

}
