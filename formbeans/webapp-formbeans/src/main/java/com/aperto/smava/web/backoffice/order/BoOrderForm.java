//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.order;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo order)}
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoOrderForm'.
 *
 * @author generator
 */
public class BoOrderForm {

    public static final String ID_EXPR = "boOrderForm.id";
    public static final String ID_PATH = "command.boOrderForm.id";
    public static final String CREATION_DATE_EXPR = "boOrderForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boOrderForm.creationDate";
    public static final String ACTIVATION_DATE_EXPR = "boOrderForm.activationDate";
    public static final String ACTIVATION_DATE_PATH = "command.boOrderForm.activationDate";
    public static final String AMOUNT_EXPR = "boOrderForm.amount";
    public static final String AMOUNT_PATH = "command.boOrderForm.amount";
    public static final String CREDIT_TERM_EXPR = "boOrderForm.creditTerm";
    public static final String CREDIT_TERM_PATH = "command.boOrderForm.creditTerm";
    public static final String INTEREST_EXPR = "boOrderForm.interest";
    public static final String INTEREST_PATH = "command.boOrderForm.interest";
    public static final String CATEGORY_EXPR = "boOrderForm.category";
    public static final String CATEGORY_PATH = "command.boOrderForm.category";
    public static final String TITLE_EXPR = "boOrderForm.title";
    public static final String TITLE_PATH = "command.boOrderForm.title";
    public static final String DESCRIPTION_EXPR = "boOrderForm.description";
    public static final String DESCRIPTION_PATH = "command.boOrderForm.description";
    public static final String IMAGE_EXPR = "boOrderForm.image";
    public static final String IMAGE_PATH = "command.boOrderForm.image";
    public static final String VALID_DAYS_EXPR = "boOrderForm.validDays";
    public static final String VALID_DAYS_PATH = "command.boOrderForm.validDays";
    public static final String GROUP_ID_EXPR = "boOrderForm.groupId";
    public static final String GROUP_ID_PATH = "command.boOrderForm.groupId";
    public static final String STATE_EXPR = "boOrderForm.state";
    public static final String STATE_PATH = "command.boOrderForm.state";
    public static final String UPDATE_DATE_EXPR = "boOrderForm.updateDate";
    public static final String UPDATE_DATE_PATH = "command.boOrderForm.updateDate";
    public static final String CONTRACT_ID_EXPR = "boOrderForm.contractId";
    public static final String CONTRACT_ID_PATH = "command.boOrderForm.contractId";
    public static final String LENDER_ID_EXPR = "boOrderForm.lenderId";
    public static final String LENDER_ID_PATH = "command.boOrderForm.lenderId";
    public static final String LENDER_EXPR = "boOrderForm.lender";
    public static final String LENDER_PATH = "command.boOrderForm.lender";
    public static final String BORROWER_ID_EXPR = "boOrderForm.borrowerId";
    public static final String BORROWER_ID_PATH = "command.boOrderForm.borrowerId";
    public static final String BORROWER_EXPR = "boOrderForm.borrower";
    public static final String BORROWER_PATH = "command.boOrderForm.borrower";
    public static final String INTERESTING_EXPR = "boOrderForm.interesting";
    public static final String INTERESTING_PATH = "command.boOrderForm.interesting";
    public static final String PROVISION_RATE_EXPR = "boOrderForm.provisionRate";
    public static final String PROVISION_RATE_PATH = "command.boOrderForm.provisionRate";
    public static final String INTERNAL_DEBT_CONSOLIDATION_EXPR = "boOrderForm.internalDebtConsolidation";
    public static final String INTERNAL_DEBT_CONSOLIDATION_PATH = "command.boOrderForm.internalDebtConsolidation";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo order)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _activationDate;
    private String _activationDateInitVal;
    private boolean _activationDateIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _creditTerm;
    private String _creditTermInitVal;
    private boolean _creditTermIsSet;
    private String _interest;
    private String _interestInitVal;
    private boolean _interestIsSet;
    private Long _category;
    private Long _categoryInitVal;
    private boolean _categoryIsSet;
    private String _title;
    private String _titleInitVal;
    private boolean _titleIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private Integer _validDays;
    private Integer _validDaysInitVal;
    private boolean _validDaysIsSet;
    private Long _groupId;
    private Long _groupIdInitVal;
    private boolean _groupIdIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private Date _updateDate;
    private Date _updateDateInitVal;
    private boolean _updateDateIsSet;
    private Long _contractId;
    private Long _contractIdInitVal;
    private boolean _contractIdIsSet;
    private Long _lenderId;
    private Long _lenderIdInitVal;
    private boolean _lenderIdIsSet;
    private String _lender;
    private String _lenderInitVal;
    private boolean _lenderIsSet;
    private Long _borrowerId;
    private Long _borrowerIdInitVal;
    private boolean _borrowerIdIsSet;
    private String _borrower;
    private String _borrowerInitVal;
    private boolean _borrowerIsSet;
    private String _interesting;
    private String _interestingInitVal;
    private boolean _interestingIsSet;
    private double _provisionRate;
    private double _provisionRateInitVal;
    private boolean _provisionRateIsSet;
    private boolean _internalDebtConsolidation;
    private boolean _internalDebtConsolidationInitVal;
    private boolean _internalDebtConsolidationIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setActivationDate(String activationDate) {
        if (!_activationDateIsSet) {
            _activationDateIsSet = true;
            _activationDateInitVal = activationDate;
        }
        _activationDate = activationDate;
    }

    public String getActivationDate() {
        return _activationDate;
    }

    public String activationDateInitVal() {
        return _activationDateInitVal;
    }

	public boolean activationDateIsDirty() {
        return !valuesAreEqual(_activationDateInitVal, _activationDate);
    }

    public boolean activationDateIsSet() {
        return _activationDateIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setCreditTerm(String creditTerm) {
        if (!_creditTermIsSet) {
            _creditTermIsSet = true;
            _creditTermInitVal = creditTerm;
        }
        _creditTerm = creditTerm;
    }

    public String getCreditTerm() {
        return _creditTerm;
    }

    public String creditTermInitVal() {
        return _creditTermInitVal;
    }

	public boolean creditTermIsDirty() {
        return !valuesAreEqual(_creditTermInitVal, _creditTerm);
    }

    public boolean creditTermIsSet() {
        return _creditTermIsSet;
    }	


    public void setInterest(String interest) {
        if (!_interestIsSet) {
            _interestIsSet = true;
            _interestInitVal = interest;
        }
        _interest = interest;
    }

    public String getInterest() {
        return _interest;
    }

    public String interestInitVal() {
        return _interestInitVal;
    }

	public boolean interestIsDirty() {
        return !valuesAreEqual(_interestInitVal, _interest);
    }

    public boolean interestIsSet() {
        return _interestIsSet;
    }	


    public void setCategory(Long category) {
        if (!_categoryIsSet) {
            _categoryIsSet = true;
            _categoryInitVal = category;
        }
        _category = category;
    }

    public Long getCategory() {
        return _category;
    }

    public Long categoryInitVal() {
        return _categoryInitVal;
    }

	public boolean categoryIsDirty() {
        return !valuesAreEqual(_categoryInitVal, _category);
    }

    public boolean categoryIsSet() {
        return _categoryIsSet;
    }	


    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = title;
        }
        _title = title;
    }

    public String getTitle() {
        return _title;
    }

    public String titleInitVal() {
        return _titleInitVal;
    }

	public boolean titleIsDirty() {
        return !valuesAreEqual(_titleInitVal, _title);
    }

    public boolean titleIsSet() {
        return _titleIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setValidDays(Integer validDays) {
        if (!_validDaysIsSet) {
            _validDaysIsSet = true;
            _validDaysInitVal = validDays;
        }
        _validDays = validDays;
    }

    public Integer getValidDays() {
        return _validDays;
    }

    public Integer validDaysInitVal() {
        return _validDaysInitVal;
    }

	public boolean validDaysIsDirty() {
        return !valuesAreEqual(_validDaysInitVal, _validDays);
    }

    public boolean validDaysIsSet() {
        return _validDaysIsSet;
    }	


    public void setGroupId(Long groupId) {
        if (!_groupIdIsSet) {
            _groupIdIsSet = true;
            _groupIdInitVal = groupId;
        }
        _groupId = groupId;
    }

    public Long getGroupId() {
        return _groupId;
    }

    public Long groupIdInitVal() {
        return _groupIdInitVal;
    }

	public boolean groupIdIsDirty() {
        return !valuesAreEqual(_groupIdInitVal, _groupId);
    }

    public boolean groupIdIsSet() {
        return _groupIdIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setUpdateDate(Date updateDate) {
        if (!_updateDateIsSet) {
            _updateDateIsSet = true;

            if (updateDate == null) {
                _updateDateInitVal = null;
            } else {
                _updateDateInitVal = (Date) updateDate.clone();
            }
        }

        if (updateDate == null) {
            _updateDate = null;
        } else {
            _updateDate = (Date) updateDate.clone();
        }
    }

    public Date getUpdateDate() {
        Date updateDate = null;
        if (_updateDate != null) {
            updateDate = (Date) _updateDate.clone();
        }
        return updateDate;
    }

    public Date updateDateInitVal() {
        Date updateDateInitVal = null;
        if (_updateDateInitVal != null) {
            updateDateInitVal = (Date) _updateDateInitVal.clone();
        }
        return updateDateInitVal;
    }

	public boolean updateDateIsDirty() {
        return !valuesAreEqual(_updateDateInitVal, _updateDate);
    }

    public boolean updateDateIsSet() {
        return _updateDateIsSet;
    }	


    public void setContractId(Long contractId) {
        if (!_contractIdIsSet) {
            _contractIdIsSet = true;
            _contractIdInitVal = contractId;
        }
        _contractId = contractId;
    }

    public Long getContractId() {
        return _contractId;
    }

    public Long contractIdInitVal() {
        return _contractIdInitVal;
    }

	public boolean contractIdIsDirty() {
        return !valuesAreEqual(_contractIdInitVal, _contractId);
    }

    public boolean contractIdIsSet() {
        return _contractIdIsSet;
    }	


    public void setLenderId(Long lenderId) {
        if (!_lenderIdIsSet) {
            _lenderIdIsSet = true;
            _lenderIdInitVal = lenderId;
        }
        _lenderId = lenderId;
    }

    public Long getLenderId() {
        return _lenderId;
    }

    public Long lenderIdInitVal() {
        return _lenderIdInitVal;
    }

	public boolean lenderIdIsDirty() {
        return !valuesAreEqual(_lenderIdInitVal, _lenderId);
    }

    public boolean lenderIdIsSet() {
        return _lenderIdIsSet;
    }	


    public void setLender(String lender) {
        if (!_lenderIsSet) {
            _lenderIsSet = true;
            _lenderInitVal = lender;
        }
        _lender = lender;
    }

    public String getLender() {
        return _lender;
    }

    public String lenderInitVal() {
        return _lenderInitVal;
    }

	public boolean lenderIsDirty() {
        return !valuesAreEqual(_lenderInitVal, _lender);
    }

    public boolean lenderIsSet() {
        return _lenderIsSet;
    }	


    public void setBorrowerId(Long borrowerId) {
        if (!_borrowerIdIsSet) {
            _borrowerIdIsSet = true;
            _borrowerIdInitVal = borrowerId;
        }
        _borrowerId = borrowerId;
    }

    public Long getBorrowerId() {
        return _borrowerId;
    }

    public Long borrowerIdInitVal() {
        return _borrowerIdInitVal;
    }

	public boolean borrowerIdIsDirty() {
        return !valuesAreEqual(_borrowerIdInitVal, _borrowerId);
    }

    public boolean borrowerIdIsSet() {
        return _borrowerIdIsSet;
    }	


    public void setBorrower(String borrower) {
        if (!_borrowerIsSet) {
            _borrowerIsSet = true;
            _borrowerInitVal = borrower;
        }
        _borrower = borrower;
    }

    public String getBorrower() {
        return _borrower;
    }

    public String borrowerInitVal() {
        return _borrowerInitVal;
    }

	public boolean borrowerIsDirty() {
        return !valuesAreEqual(_borrowerInitVal, _borrower);
    }

    public boolean borrowerIsSet() {
        return _borrowerIsSet;
    }	


    public void setInteresting(String interesting) {
        if (!_interestingIsSet) {
            _interestingIsSet = true;
            _interestingInitVal = interesting;
        }
        _interesting = interesting;
    }

    public String getInteresting() {
        return _interesting;
    }

    public String interestingInitVal() {
        return _interestingInitVal;
    }

	public boolean interestingIsDirty() {
        return !valuesAreEqual(_interestingInitVal, _interesting);
    }

    public boolean interestingIsSet() {
        return _interestingIsSet;
    }	


    public void setProvisionRate(double provisionRate) {
        if (!_provisionRateIsSet) {
            _provisionRateIsSet = true;
            _provisionRateInitVal = provisionRate;
        }
        _provisionRate = provisionRate;
    }

    public double getProvisionRate() {
        return _provisionRate;
    }

    public double provisionRateInitVal() {
        return _provisionRateInitVal;
    }

	public boolean provisionRateIsDirty() {
        return !valuesAreEqual(_provisionRateInitVal, _provisionRate);
    }

    public boolean provisionRateIsSet() {
        return _provisionRateIsSet;
    }	


    public void setInternalDebtConsolidation(boolean internalDebtConsolidation) {
        if (!_internalDebtConsolidationIsSet) {
            _internalDebtConsolidationIsSet = true;
            _internalDebtConsolidationInitVal = internalDebtConsolidation;
        }
        _internalDebtConsolidation = internalDebtConsolidation;
    }

    public boolean getInternalDebtConsolidation() {
        return _internalDebtConsolidation;
    }

    public boolean internalDebtConsolidationInitVal() {
        return _internalDebtConsolidationInitVal;
    }

	public boolean internalDebtConsolidationIsDirty() {
        return !valuesAreEqual(_internalDebtConsolidationInitVal, _internalDebtConsolidation);
    }

    public boolean internalDebtConsolidationIsSet() {
        return _internalDebtConsolidationIsSet;
    }	

}
