//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.offer;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo offer)}
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoOfferForm'.
 *
 * @author generator
 */
public class BoOfferForm {

    public static final String ID_EXPR = "boOfferForm.id";
    public static final String ID_PATH = "command.boOfferForm.id";
    public static final String AMOUNT_EXPR = "boOfferForm.amount";
    public static final String AMOUNT_PATH = "command.boOfferForm.amount";
    public static final String CREATION_DATE_EXPR = "boOfferForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boOfferForm.creationDate";
    public static final String TITLE_EXPR = "boOfferForm.title";
    public static final String TITLE_PATH = "command.boOfferForm.title";
    public static final String DESCRIPTION_EXPR = "boOfferForm.description";
    public static final String DESCRIPTION_PATH = "command.boOfferForm.description";
    public static final String GROUP_ID_EXPR = "boOfferForm.groupId";
    public static final String GROUP_ID_PATH = "command.boOfferForm.groupId";
    public static final String STATE_EXPR = "boOfferForm.state";
    public static final String STATE_PATH = "command.boOfferForm.state";
    public static final String UPDATE_DATE_EXPR = "boOfferForm.updateDate";
    public static final String UPDATE_DATE_PATH = "command.boOfferForm.updateDate";
    public static final String TERM_EXPR = "boOfferForm.term";
    public static final String TERM_PATH = "command.boOfferForm.term";
    public static final String RISK_EXPR = "boOfferForm.risk";
    public static final String RISK_PATH = "command.boOfferForm.risk";
    public static final String RATE_EXPR = "boOfferForm.rate";
    public static final String RATE_PATH = "command.boOfferForm.rate";
    public static final String TERM_STRING_EXPR = "boOfferForm.termString";
    public static final String TERM_STRING_PATH = "command.boOfferForm.termString";
    public static final String RISK_STRING_EXPR = "boOfferForm.riskString";
    public static final String RISK_STRING_PATH = "command.boOfferForm.riskString";
    public static final String CONTRACT_ID_EXPR = "boOfferForm.contractId";
    public static final String CONTRACT_ID_PATH = "command.boOfferForm.contractId";
    public static final String LENDER_ID_EXPR = "boOfferForm.lenderId";
    public static final String LENDER_ID_PATH = "command.boOfferForm.lenderId";
    public static final String LENDER_EXPR = "boOfferForm.lender";
    public static final String LENDER_PATH = "command.boOfferForm.lender";
    public static final String BORROWER_ID_EXPR = "boOfferForm.borrowerId";
    public static final String BORROWER_ID_PATH = "command.boOfferForm.borrowerId";
    public static final String BORROWER_EXPR = "boOfferForm.borrower";
    public static final String BORROWER_PATH = "command.boOfferForm.borrower";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo offer)}
// !!!!!!!! End of insert code section !!!!!!!!


    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;

    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;

    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;

    private String _title;
    private String _titleInitVal;
    private boolean _titleIsSet;

    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;

    private Long _groupId;
    private Long _groupIdInitVal;
    private boolean _groupIdIsSet;

    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;

    private String _updateDate;
    private String _updateDateInitVal;
    private boolean _updateDateIsSet;

    private List<String> _term;
    private List<String> _termInitVal;
    private boolean _termIsSet;

    private List<String> _risk;
    private List<String> _riskInitVal;
    private boolean _riskIsSet;

    private String _rate;
    private String _rateInitVal;
    private boolean _rateIsSet;

    private String _termString;
    private String _termStringInitVal;
    private boolean _termStringIsSet;

    private String _riskString;
    private String _riskStringInitVal;
    private boolean _riskStringIsSet;

    private Long _contractId;
    private Long _contractIdInitVal;
    private boolean _contractIdIsSet;

    private Long _lenderId;
    private Long _lenderIdInitVal;
    private boolean _lenderIdIsSet;

    private String _lender;
    private String _lenderInitVal;
    private boolean _lenderIsSet;

    private Long _borrowerId;
    private Long _borrowerIdInitVal;
    private boolean _borrowerIdIsSet;

    private String _borrower;
    private String _borrowerInitVal;
    private boolean _borrowerIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

    public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }

    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

    public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }

    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

    public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = title;
        }
        _title = title;
    }

    public String getTitle() {
        return _title;
    }

    public String titleInitVal() {
        return _titleInitVal;
    }

    public boolean titleIsDirty() {
        return !valuesAreEqual(_titleInitVal, _title);
    }

    public boolean titleIsSet() {
        return _titleIsSet;
    }

    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

    public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }

    public void setGroupId(Long groupId) {
        if (!_groupIdIsSet) {
            _groupIdIsSet = true;
            _groupIdInitVal = groupId;
        }
        _groupId = groupId;
    }

    public Long getGroupId() {
        return _groupId;
    }

    public Long groupIdInitVal() {
        return _groupIdInitVal;
    }

    public boolean groupIdIsDirty() {
        return !valuesAreEqual(_groupIdInitVal, _groupId);
    }

    public boolean groupIdIsSet() {
        return _groupIdIsSet;
    }

    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

    public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }

    public void setUpdateDate(String updateDate) {
        if (!_updateDateIsSet) {
            _updateDateIsSet = true;
            _updateDateInitVal = updateDate;
        }
        _updateDate = updateDate;
    }

    public String getUpdateDate() {
        return _updateDate;
    }

    public String updateDateInitVal() {
        return _updateDateInitVal;
    }

    public boolean updateDateIsDirty() {
        return !valuesAreEqual(_updateDateInitVal, _updateDate);
    }

    public boolean updateDateIsSet() {
        return _updateDateIsSet;
    }

    public void setTerm(List<String> term) {
        if (!_termIsSet) {
            _termIsSet = true;
            _termInitVal = term;
        }
        _term = term;
    }

    public List<String> getTerm() {
        return _term;
    }

    public List<String> termInitVal() {
        return _termInitVal;
    }

    public boolean termIsDirty() {
        return !valuesAreEqual(_termInitVal, _term);
    }

    public boolean termIsSet() {
        return _termIsSet;
    }

    public void setRisk(List<String> risk) {
        if (!_riskIsSet) {
            _riskIsSet = true;
            _riskInitVal = risk;
        }
        _risk = risk;
    }

    public List<String> getRisk() {
        return _risk;
    }

    public List<String> riskInitVal() {
        return _riskInitVal;
    }

    public boolean riskIsDirty() {
        return !valuesAreEqual(_riskInitVal, _risk);
    }

    public boolean riskIsSet() {
        return _riskIsSet;
    }

    public void setRate(String rate) {
        if (!_rateIsSet) {
            _rateIsSet = true;
            _rateInitVal = rate;
        }
        _rate = rate;
    }

    public String getRate() {
        return _rate;
    }

    public String rateInitVal() {
        return _rateInitVal;
    }

    public boolean rateIsDirty() {
        return !valuesAreEqual(_rateInitVal, _rate);
    }

    public boolean rateIsSet() {
        return _rateIsSet;
    }

    public void setTermString(String termString) {
        if (!_termStringIsSet) {
            _termStringIsSet = true;
            _termStringInitVal = termString;
        }
        _termString = termString;
    }

    public String getTermString() {
        return _termString;
    }

    public String termStringInitVal() {
        return _termStringInitVal;
    }

    public boolean termStringIsDirty() {
        return !valuesAreEqual(_termStringInitVal, _termString);
    }

    public boolean termStringIsSet() {
        return _termStringIsSet;
    }

    public void setRiskString(String riskString) {
        if (!_riskStringIsSet) {
            _riskStringIsSet = true;
            _riskStringInitVal = riskString;
        }
        _riskString = riskString;
    }

    public String getRiskString() {
        return _riskString;
    }

    public String riskStringInitVal() {
        return _riskStringInitVal;
    }

    public boolean riskStringIsDirty() {
        return !valuesAreEqual(_riskStringInitVal, _riskString);
    }

    public boolean riskStringIsSet() {
        return _riskStringIsSet;
    }

    public void setContractId(Long contractId) {
        if (!_contractIdIsSet) {
            _contractIdIsSet = true;
            _contractIdInitVal = contractId;
        }
        _contractId = contractId;
    }

    public Long getContractId() {
        return _contractId;
    }

    public Long contractIdInitVal() {
        return _contractIdInitVal;
    }

    public boolean contractIdIsDirty() {
        return !valuesAreEqual(_contractIdInitVal, _contractId);
    }

    public boolean contractIdIsSet() {
        return _contractIdIsSet;
    }

    public void setLenderId(Long lenderId) {
        if (!_lenderIdIsSet) {
            _lenderIdIsSet = true;
            _lenderIdInitVal = lenderId;
        }
        _lenderId = lenderId;
    }

    public Long getLenderId() {
        return _lenderId;
    }

    public Long lenderIdInitVal() {
        return _lenderIdInitVal;
    }

    public boolean lenderIdIsDirty() {
        return !valuesAreEqual(_lenderIdInitVal, _lenderId);
    }

    public boolean lenderIdIsSet() {
        return _lenderIdIsSet;
    }

    public void setLender(String lender) {
        if (!_lenderIsSet) {
            _lenderIsSet = true;
            _lenderInitVal = lender;
        }
        _lender = lender;
    }

    public String getLender() {
        return _lender;
    }

    public String lenderInitVal() {
        return _lenderInitVal;
    }

    public boolean lenderIsDirty() {
        return !valuesAreEqual(_lenderInitVal, _lender);
    }

    public boolean lenderIsSet() {
        return _lenderIsSet;
    }

    public void setBorrowerId(Long borrowerId) {
        if (!_borrowerIdIsSet) {
            _borrowerIdIsSet = true;
            _borrowerIdInitVal = borrowerId;
        }
        _borrowerId = borrowerId;
    }

    public Long getBorrowerId() {
        return _borrowerId;
    }

    public Long borrowerIdInitVal() {
        return _borrowerIdInitVal;
    }

    public boolean borrowerIdIsDirty() {
        return !valuesAreEqual(_borrowerIdInitVal, _borrowerId);
    }

    public boolean borrowerIdIsSet() {
        return _borrowerIdIsSet;
    }

    public void setBorrower(String borrower) {
        if (!_borrowerIsSet) {
            _borrowerIsSet = true;
            _borrowerInitVal = borrower;
        }
        _borrower = borrower;
    }

    public String getBorrower() {
        return _borrower;
    }

    public String borrowerInitVal() {
        return _borrowerInitVal;
    }

    public boolean borrowerIsDirty() {
        return !valuesAreEqual(_borrowerInitVal, _borrower);
    }

    public boolean borrowerIsSet() {
        return _borrowerIsSet;
    }

}
