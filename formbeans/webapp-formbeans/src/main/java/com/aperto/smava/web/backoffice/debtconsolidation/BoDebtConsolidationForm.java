//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.debtconsolidation;
            
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo debt consolidation)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoDebtConsolidationForm'.
 *
 * @author generator
 */
public class BoDebtConsolidationForm extends com.aperto.smava.web.debtconsolidation.DebtConsolidationForm {

    public static final String STATE_EXPR = "debtConsolidationForm.state";
    public static final String STATE_PATH = "command.debtConsolidationForm.state";
    public static final String LOAN_ID_EXPR = "debtConsolidationForm.loanId";
    public static final String LOAN_ID_PATH = "command.debtConsolidationForm.loanId";
    public static final String DUE_DATE_EXPR = "debtConsolidationForm.dueDate";
    public static final String DUE_DATE_PATH = "command.debtConsolidationForm.dueDate";
    public static final String START_DATE_EXPR = "debtConsolidationForm.startDate";
    public static final String START_DATE_PATH = "command.debtConsolidationForm.startDate";
    public static final String GENERATE_DOCUMENTS_EXPR = "debtConsolidationForm.generateDocuments";
    public static final String GENERATE_DOCUMENTS_PATH = "command.debtConsolidationForm.generateDocuments";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo debt consolidation)}
    private Long _orderId;
    private String _orderState;
    private boolean _isSaveAllowed; 
    
    public Long getOrderId(){
    	return _orderId;
    }
    
    public void setOrderId(Long orderId){
    	_orderId = orderId;
    }
    
    public String getOrderState(){
    	return _orderState;
    }
    
    public void setOrderState(String s){
    	_orderState = s;
    }
    
    public boolean isSaveAllowed() {
		return _isSaveAllowed;
	}

	public void setSaveAllowed(boolean isSaveAllowed) {
		_isSaveAllowed = isSaveAllowed;
	}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _loanId;
    private String _loanIdInitVal;
    private boolean _loanIdIsSet;
    private String _dueDate;
    private String _dueDateInitVal;
    private boolean _dueDateIsSet;
    private String _startDate;
    private String _startDateInitVal;
    private boolean _startDateIsSet;
    private Boolean _generateDocuments;
    private Boolean _generateDocumentsInitVal;
    private boolean _generateDocumentsIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setLoanId(String loanId) {
        if (!_loanIdIsSet) {
            _loanIdIsSet = true;
            _loanIdInitVal = loanId;
        }
        _loanId = loanId;
    }

    public String getLoanId() {
        return _loanId;
    }

    public String loanIdInitVal() {
        return _loanIdInitVal;
    }

	public boolean loanIdIsDirty() {
        return !valuesAreEqual(_loanIdInitVal, _loanId);
    }

    public boolean loanIdIsSet() {
        return _loanIdIsSet;
    }	


    public void setDueDate(String dueDate) {
        if (!_dueDateIsSet) {
            _dueDateIsSet = true;
            _dueDateInitVal = dueDate;
        }
        _dueDate = dueDate;
    }

    public String getDueDate() {
        return _dueDate;
    }

    public String dueDateInitVal() {
        return _dueDateInitVal;
    }

	public boolean dueDateIsDirty() {
        return !valuesAreEqual(_dueDateInitVal, _dueDate);
    }

    public boolean dueDateIsSet() {
        return _dueDateIsSet;
    }	


    public void setStartDate(String startDate) {
        if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = startDate;
        }
        _startDate = startDate;
    }

    public String getStartDate() {
        return _startDate;
    }

    public String startDateInitVal() {
        return _startDateInitVal;
    }

	public boolean startDateIsDirty() {
        return !valuesAreEqual(_startDateInitVal, _startDate);
    }

    public boolean startDateIsSet() {
        return _startDateIsSet;
    }	


    public void setGenerateDocuments(Boolean generateDocuments) {
        if (!_generateDocumentsIsSet) {
            _generateDocumentsIsSet = true;
            _generateDocumentsInitVal = generateDocuments;
        }
        _generateDocuments = generateDocuments;
    }

    public Boolean getGenerateDocuments() {
        return _generateDocuments;
    }

    public Boolean generateDocumentsInitVal() {
        return _generateDocumentsInitVal;
    }

	public boolean generateDocumentsIsDirty() {
        return !valuesAreEqual(_generateDocumentsInitVal, _generateDocuments);
    }

    public boolean generateDocumentsIsSet() {
        return _generateDocumentsIsSet;
    }	

}
