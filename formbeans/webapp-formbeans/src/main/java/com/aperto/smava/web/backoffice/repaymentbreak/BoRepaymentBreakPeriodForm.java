//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.repaymentbreak;

    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo repayment break period)}

import java.io.Serializable;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoRepaymentBreakPeriodForm'.
 *
 * @author generator
 */
public class BoRepaymentBreakPeriodForm implements Serializable {

	public static final String CURRENT_RATE_EXPR = "boRepaymentBreakPeriodForm.currentRate";
    public static final String CURRENT_RATE_PATH = "command.boRepaymentBreakPeriodForm.currentRate";
    public static final String MIN_RATE_EXPR = "boRepaymentBreakPeriodForm.minRate";
    public static final String MIN_RATE_PATH = "command.boRepaymentBreakPeriodForm.minRate";
    public static final String MONTH1_EXPR = "boRepaymentBreakPeriodForm.month1";
    public static final String MONTH1_PATH = "command.boRepaymentBreakPeriodForm.month1";
    public static final String RATE1_EXPR = "boRepaymentBreakPeriodForm.rate1";
    public static final String RATE1_PATH = "command.boRepaymentBreakPeriodForm.rate1";
    public static final String MONTH2_EXPR = "boRepaymentBreakPeriodForm.month2";
    public static final String MONTH2_PATH = "command.boRepaymentBreakPeriodForm.month2";
    public static final String RATE2_EXPR = "boRepaymentBreakPeriodForm.rate2";
    public static final String RATE2_PATH = "command.boRepaymentBreakPeriodForm.rate2";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo repayment break period)}

    /**
     * generated serial.
     */
    private static final long serialVersionUID = 6874327983439924568L;

// !!!!!!!! End of insert code section !!!!!!!!

    private Double _currentRate;
    private Double _currentRateInitVal;
    private boolean _currentRateIsSet;
    private Double _minRate;
    private Double _minRateInitVal;
    private boolean _minRateIsSet;
    private Date _month1;
    private Date _month1InitVal;
    private boolean _month1IsSet;
    private String _rate1;
    private String _rate1InitVal;
    private boolean _rate1IsSet;
    private Date _month2;
    private Date _month2InitVal;
    private boolean _month2IsSet;
    private String _rate2;
    private String _rate2InitVal;
    private boolean _rate2IsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setCurrentRate(Double currentRate) {
        if (!_currentRateIsSet) {
            _currentRateIsSet = true;
            _currentRateInitVal = currentRate;
        }
        _currentRate = currentRate;
    }

    public Double getCurrentRate() {
        return _currentRate;
    }

    public Double currentRateInitVal() {
        return _currentRateInitVal;
    }

	public boolean currentRateIsDirty() {
        return !valuesAreEqual(_currentRateInitVal, _currentRate);
    }

    public boolean currentRateIsSet() {
        return _currentRateIsSet;
    }	


    public void setMinRate(Double minRate) {
        if (!_minRateIsSet) {
            _minRateIsSet = true;
            _minRateInitVal = minRate;
        }
        _minRate = minRate;
    }

    public Double getMinRate() {
        return _minRate;
    }

    public Double minRateInitVal() {
        return _minRateInitVal;
    }

	public boolean minRateIsDirty() {
        return !valuesAreEqual(_minRateInitVal, _minRate);
    }

    public boolean minRateIsSet() {
        return _minRateIsSet;
    }	


    public void setMonth1(Date month1) {
        if (!_month1IsSet) {
            _month1IsSet = true;

            if (month1 == null) {
                _month1InitVal = null;
            } else {
                _month1InitVal = (Date) month1.clone();
            }
        }

        if (month1 == null) {
            _month1 = null;
        } else {
            _month1 = (Date) month1.clone();
        }
    }

    public Date getMonth1() {
        Date month1 = null;
        if (_month1 != null) {
            month1 = (Date) _month1.clone();
        }
        return month1;
    }

    public Date month1InitVal() {
        Date month1InitVal = null;
        if (_month1InitVal != null) {
            month1InitVal = (Date) _month1InitVal.clone();
        }
        return month1InitVal;
    }

	public boolean month1IsDirty() {
        return !valuesAreEqual(_month1InitVal, _month1);
    }

    public boolean month1IsSet() {
        return _month1IsSet;
    }	


    public void setRate1(String rate1) {
        if (!_rate1IsSet) {
            _rate1IsSet = true;
            _rate1InitVal = rate1;
        }
        _rate1 = rate1;
    }

    public String getRate1() {
        return _rate1;
    }

    public String rate1InitVal() {
        return _rate1InitVal;
    }

	public boolean rate1IsDirty() {
        return !valuesAreEqual(_rate1InitVal, _rate1);
    }

    public boolean rate1IsSet() {
        return _rate1IsSet;
    }	


    public void setMonth2(Date month2) {
        if (!_month2IsSet) {
            _month2IsSet = true;

            if (month2 == null) {
                _month2InitVal = null;
            } else {
                _month2InitVal = (Date) month2.clone();
            }
        }

        if (month2 == null) {
            _month2 = null;
        } else {
            _month2 = (Date) month2.clone();
        }
    }

    public Date getMonth2() {
        Date month2 = null;
        if (_month2 != null) {
            month2 = (Date) _month2.clone();
        }
        return month2;
    }

    public Date month2InitVal() {
        Date month2InitVal = null;
        if (_month2InitVal != null) {
            month2InitVal = (Date) _month2InitVal.clone();
        }
        return month2InitVal;
    }

	public boolean month2IsDirty() {
        return !valuesAreEqual(_month2InitVal, _month2);
    }

    public boolean month2IsSet() {
        return _month2IsSet;
    }	


    public void setRate2(String rate2) {
        if (!_rate2IsSet) {
            _rate2IsSet = true;
            _rate2InitVal = rate2;
        }
        _rate2 = rate2;
    }

    public String getRate2() {
        return _rate2;
    }

    public String rate2InitVal() {
        return _rate2InitVal;
    }

	public boolean rate2IsDirty() {
        return !valuesAreEqual(_rate2InitVal, _rate2);
    }

    public boolean rate2IsSet() {
        return _rate2IsSet;
    }	

}
