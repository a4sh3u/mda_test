//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.order;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(order calculation)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'OrderCalculationForm'.
 *
 * @author generator
 */
public class OrderCalculationForm implements Serializable {

    public static final String AMOUNT_EXPR = "orderCalculationForm.amount";
    public static final String AMOUNT_PATH = "command.orderCalculationForm.amount";
    public static final String INTEREST_EXPR = "orderCalculationForm.interest";
    public static final String INTEREST_PATH = "command.orderCalculationForm.interest";
    public static final String DURATION_EXPR = "orderCalculationForm.duration";
    public static final String DURATION_PATH = "command.orderCalculationForm.duration";
    public static final String RDI_CONTRACT_TYPE_EXPR = "orderCalculationForm.rdiContractType";
    public static final String RDI_CONTRACT_TYPE_PATH = "command.orderCalculationForm.rdiContractType";
    public static final String ALTERNATE_AMOUNT_EXPR = "orderCalculationForm.alternateAmount";
    public static final String ALTERNATE_AMOUNT_PATH = "command.orderCalculationForm.alternateAmount";
    public static final String LAST_REQUESTED_AMOUNT_EXPR = "orderCalculationForm.lastRequestedAmount";
    public static final String LAST_REQUESTED_AMOUNT_PATH = "command.orderCalculationForm.lastRequestedAmount";
    public static final String POSSIBLE_SMAVA_DURATION_IN_BROKERAGE_EXPR = "orderCalculationForm.possibleSmavaDurationInBrokerage";
    public static final String POSSIBLE_SMAVA_DURATION_IN_BROKERAGE_PATH = "command.orderCalculationForm.possibleSmavaDurationInBrokerage";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(order calculation)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _interest;
    private String _interestInitVal;
    private boolean _interestIsSet;
    private String _duration;
    private String _durationInitVal;
    private boolean _durationIsSet;
    private String _rdiContractType;
    private String _rdiContractTypeInitVal;
    private boolean _rdiContractTypeIsSet;
    private String _alternateAmount;
    private String _alternateAmountInitVal;
    private boolean _alternateAmountIsSet;
    private String _lastRequestedAmount;
    private String _lastRequestedAmountInitVal;
    private boolean _lastRequestedAmountIsSet;
    private String _possibleSmavaDurationInBrokerage;
    private String _possibleSmavaDurationInBrokerageInitVal;
    private boolean _possibleSmavaDurationInBrokerageIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setInterest(String interest) {
        if (!_interestIsSet) {
            _interestIsSet = true;
            _interestInitVal = interest;
        }
        _interest = interest;
    }

    public String getInterest() {
        return _interest;
    }

    public String interestInitVal() {
        return _interestInitVal;
    }

	public boolean interestIsDirty() {
        return !valuesAreEqual(_interestInitVal, _interest);
    }

    public boolean interestIsSet() {
        return _interestIsSet;
    }	


    public void setDuration(String duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = duration;
        }
        _duration = duration;
    }

    public String getDuration() {
        return _duration;
    }

    public String durationInitVal() {
        return _durationInitVal;
    }

	public boolean durationIsDirty() {
        return !valuesAreEqual(_durationInitVal, _duration);
    }

    public boolean durationIsSet() {
        return _durationIsSet;
    }	


    public void setRdiContractType(String rdiContractType) {
        if (!_rdiContractTypeIsSet) {
            _rdiContractTypeIsSet = true;
            _rdiContractTypeInitVal = rdiContractType;
        }
        _rdiContractType = rdiContractType;
    }

    public String getRdiContractType() {
        return _rdiContractType;
    }

    public String rdiContractTypeInitVal() {
        return _rdiContractTypeInitVal;
    }

	public boolean rdiContractTypeIsDirty() {
        return !valuesAreEqual(_rdiContractTypeInitVal, _rdiContractType);
    }

    public boolean rdiContractTypeIsSet() {
        return _rdiContractTypeIsSet;
    }	


    public void setAlternateAmount(String alternateAmount) {
        if (!_alternateAmountIsSet) {
            _alternateAmountIsSet = true;
            _alternateAmountInitVal = alternateAmount;
        }
        _alternateAmount = alternateAmount;
    }

    public String getAlternateAmount() {
        return _alternateAmount;
    }

    public String alternateAmountInitVal() {
        return _alternateAmountInitVal;
    }

	public boolean alternateAmountIsDirty() {
        return !valuesAreEqual(_alternateAmountInitVal, _alternateAmount);
    }

    public boolean alternateAmountIsSet() {
        return _alternateAmountIsSet;
    }	


    public void setLastRequestedAmount(String lastRequestedAmount) {
        if (!_lastRequestedAmountIsSet) {
            _lastRequestedAmountIsSet = true;
            _lastRequestedAmountInitVal = lastRequestedAmount;
        }
        _lastRequestedAmount = lastRequestedAmount;
    }

    public String getLastRequestedAmount() {
        return _lastRequestedAmount;
    }

    public String lastRequestedAmountInitVal() {
        return _lastRequestedAmountInitVal;
    }

	public boolean lastRequestedAmountIsDirty() {
        return !valuesAreEqual(_lastRequestedAmountInitVal, _lastRequestedAmount);
    }

    public boolean lastRequestedAmountIsSet() {
        return _lastRequestedAmountIsSet;
    }	


    public void setPossibleSmavaDurationInBrokerage(String possibleSmavaDurationInBrokerage) {
        if (!_possibleSmavaDurationInBrokerageIsSet) {
            _possibleSmavaDurationInBrokerageIsSet = true;
            _possibleSmavaDurationInBrokerageInitVal = possibleSmavaDurationInBrokerage;
        }
        _possibleSmavaDurationInBrokerage = possibleSmavaDurationInBrokerage;
    }

    public String getPossibleSmavaDurationInBrokerage() {
        return _possibleSmavaDurationInBrokerage;
    }

    public String possibleSmavaDurationInBrokerageInitVal() {
        return _possibleSmavaDurationInBrokerageInitVal;
    }

	public boolean possibleSmavaDurationInBrokerageIsDirty() {
        return !valuesAreEqual(_possibleSmavaDurationInBrokerageInitVal, _possibleSmavaDurationInBrokerage);
    }

    public boolean possibleSmavaDurationInBrokerageIsSet() {
        return _possibleSmavaDurationInBrokerageIsSet;
    }	

}
