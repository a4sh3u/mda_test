//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.message;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo send message)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoSendMessageForm'.
 *
 * @author generator
 */
public class BoSendMessageForm implements Serializable {

    public static final String SENDER_EMAIL_ADDRESS_EXPR = "boSendMessageForm.senderEmailAddress";
    public static final String SENDER_EMAIL_ADDRESS_PATH = "command.boSendMessageForm.senderEmailAddress";
    public static final String RECEIVER_EMAIL_ADDRESS_EXPR = "boSendMessageForm.receiverEmailAddress";
    public static final String RECEIVER_EMAIL_ADDRESS_PATH = "command.boSendMessageForm.receiverEmailAddress";
    public static final String SUBJECT_EXPR = "boSendMessageForm.subject";
    public static final String SUBJECT_PATH = "command.boSendMessageForm.subject";
    public static final String TEXT_EXPR = "boSendMessageForm.text";
    public static final String TEXT_PATH = "command.boSendMessageForm.text";
    public static final String DOCUMENT_TYPE_EXPR = "boSendMessageForm.documentType";
    public static final String DOCUMENT_TYPE_PATH = "command.boSendMessageForm.documentType";
    public static final String OWNER_ID_EXPR = "boSendMessageForm.ownerId";
    public static final String OWNER_ID_PATH = "command.boSendMessageForm.ownerId";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo send message)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _senderEmailAddress;
    private String _senderEmailAddressInitVal;
    private boolean _senderEmailAddressIsSet;
    private String _receiverEmailAddress;
    private String _receiverEmailAddressInitVal;
    private boolean _receiverEmailAddressIsSet;
    private String _subject;
    private String _subjectInitVal;
    private boolean _subjectIsSet;
    private String _text;
    private String _textInitVal;
    private boolean _textIsSet;
    private String _documentType;
    private String _documentTypeInitVal;
    private boolean _documentTypeIsSet;
    private String _ownerId;
    private String _ownerIdInitVal;
    private boolean _ownerIdIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setSenderEmailAddress(String senderEmailAddress) {
        if (!_senderEmailAddressIsSet) {
            _senderEmailAddressIsSet = true;
            _senderEmailAddressInitVal = senderEmailAddress;
        }
        _senderEmailAddress = senderEmailAddress;
    }

    public String getSenderEmailAddress() {
        return _senderEmailAddress;
    }

    public String senderEmailAddressInitVal() {
        return _senderEmailAddressInitVal;
    }

	public boolean senderEmailAddressIsDirty() {
        return !valuesAreEqual(_senderEmailAddressInitVal, _senderEmailAddress);
    }

    public boolean senderEmailAddressIsSet() {
        return _senderEmailAddressIsSet;
    }	


    public void setReceiverEmailAddress(String receiverEmailAddress) {
        if (!_receiverEmailAddressIsSet) {
            _receiverEmailAddressIsSet = true;
            _receiverEmailAddressInitVal = receiverEmailAddress;
        }
        _receiverEmailAddress = receiverEmailAddress;
    }

    public String getReceiverEmailAddress() {
        return _receiverEmailAddress;
    }

    public String receiverEmailAddressInitVal() {
        return _receiverEmailAddressInitVal;
    }

	public boolean receiverEmailAddressIsDirty() {
        return !valuesAreEqual(_receiverEmailAddressInitVal, _receiverEmailAddress);
    }

    public boolean receiverEmailAddressIsSet() {
        return _receiverEmailAddressIsSet;
    }	


    public void setSubject(String subject) {
        if (!_subjectIsSet) {
            _subjectIsSet = true;
            _subjectInitVal = subject;
        }
        _subject = subject;
    }

    public String getSubject() {
        return _subject;
    }

    public String subjectInitVal() {
        return _subjectInitVal;
    }

	public boolean subjectIsDirty() {
        return !valuesAreEqual(_subjectInitVal, _subject);
    }

    public boolean subjectIsSet() {
        return _subjectIsSet;
    }	


    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = text;
        }
        _text = text;
    }

    public String getText() {
        return _text;
    }

    public String textInitVal() {
        return _textInitVal;
    }

	public boolean textIsDirty() {
        return !valuesAreEqual(_textInitVal, _text);
    }

    public boolean textIsSet() {
        return _textIsSet;
    }	


    public void setDocumentType(String documentType) {
        if (!_documentTypeIsSet) {
            _documentTypeIsSet = true;
            _documentTypeInitVal = documentType;
        }
        _documentType = documentType;
    }

    public String getDocumentType() {
        return _documentType;
    }

    public String documentTypeInitVal() {
        return _documentTypeInitVal;
    }

	public boolean documentTypeIsDirty() {
        return !valuesAreEqual(_documentTypeInitVal, _documentType);
    }

    public boolean documentTypeIsSet() {
        return _documentTypeIsSet;
    }	


    public void setOwnerId(String ownerId) {
        if (!_ownerIdIsSet) {
            _ownerIdIsSet = true;
            _ownerIdInitVal = ownerId;
        }
        _ownerId = ownerId;
    }

    public String getOwnerId() {
        return _ownerId;
    }

    public String ownerIdInitVal() {
        return _ownerIdInitVal;
    }

	public boolean ownerIdIsDirty() {
        return !valuesAreEqual(_ownerIdInitVal, _ownerId);
    }

    public boolean ownerIdIsSet() {
        return _ownerIdIsSet;
    }	

}
