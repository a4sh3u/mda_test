//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.document;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo document)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoDocumentForm'.
 *
 * @author generator
 */
public class BoDocumentForm implements Serializable {

    public static final String ID_EXPR = "boDocumentForm.id";
    public static final String ID_PATH = "command.boDocumentForm.id";
    public static final String SUBJECT_EXPR = "boDocumentForm.subject";
    public static final String SUBJECT_PATH = "command.boDocumentForm.subject";
    public static final String TEXT_EXPR = "boDocumentForm.text";
    public static final String TEXT_PATH = "command.boDocumentForm.text";
    public static final String NOTES_EXPR = "boDocumentForm.notes";
    public static final String NOTES_PATH = "command.boDocumentForm.notes";
    public static final String CREATION_DATE_EXPR = "boDocumentForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boDocumentForm.creationDate";
    public static final String DATE_EXPR = "boDocumentForm.date";
    public static final String DATE_PATH = "command.boDocumentForm.date";
    public static final String DIRECTION_EXPR = "boDocumentForm.direction";
    public static final String DIRECTION_PATH = "command.boDocumentForm.direction";
    public static final String STATE_EXPR = "boDocumentForm.state";
    public static final String STATE_PATH = "command.boDocumentForm.state";
    public static final String APPROVAL_STATE_EXPR = "boDocumentForm.approvalState";
    public static final String APPROVAL_STATE_PATH = "command.boDocumentForm.approvalState";
    public static final String ADD_CONTRACT_ID_EXPR = "boDocumentForm.addContractId";
    public static final String ADD_CONTRACT_ID_PATH = "command.boDocumentForm.addContractId";
    public static final String DELETE_CONTRACT_ID_EXPR = "boDocumentForm.deleteContractId";
    public static final String DELETE_CONTRACT_ID_PATH = "command.boDocumentForm.deleteContractId";
    public static final String ADD_CONTRACTS_OF_ORDER_ID_EXPR = "boDocumentForm.addContractsOfOrderId";
    public static final String ADD_CONTRACTS_OF_ORDER_ID_PATH = "command.boDocumentForm.addContractsOfOrderId";
    public static final String ACCOUNT_ID_EXPR = "boDocumentForm.accountId";
    public static final String ACCOUNT_ID_PATH = "command.boDocumentForm.accountId";
    public static final String ACCOUNT_NAME_EXPR = "boDocumentForm.accountName";
    public static final String ACCOUNT_NAME_PATH = "command.boDocumentForm.accountName";
    public static final String TYPE_EXPR = "boDocumentForm.type";
    public static final String TYPE_PATH = "command.boDocumentForm.type";
    public static final String TYPE_OF_DISPATCH_EXPR = "boDocumentForm.typeOfDispatch";
    public static final String TYPE_OF_DISPATCH_PATH = "command.boDocumentForm.typeOfDispatch";
    public static final String ADDRESS_FROM_ID_EXPR = "boDocumentForm.addressFromId";
    public static final String ADDRESS_FROM_ID_PATH = "command.boDocumentForm.addressFromId";
    public static final String ADDRESS_TO_ID_EXPR = "boDocumentForm.addressToId";
    public static final String ADDRESS_TO_ID_PATH = "command.boDocumentForm.addressToId";
    public static final String EMAIL_EXPR = "boDocumentForm.email";
    public static final String EMAIL_PATH = "command.boDocumentForm.email";
    public static final String NAME_EXPR = "boDocumentForm.name";
    public static final String NAME_PATH = "command.boDocumentForm.name";
    public static final String STREET_EXPR = "boDocumentForm.street";
    public static final String STREET_PATH = "command.boDocumentForm.street";
    public static final String STREET_NUMBER_EXPR = "boDocumentForm.streetNumber";
    public static final String STREET_NUMBER_PATH = "command.boDocumentForm.streetNumber";
    public static final String ZIP_CODE_EXPR = "boDocumentForm.zipCode";
    public static final String ZIP_CODE_PATH = "command.boDocumentForm.zipCode";
    public static final String CITY_EXPR = "boDocumentForm.city";
    public static final String CITY_PATH = "command.boDocumentForm.city";
    public static final String COUNTRY_EXPR = "boDocumentForm.country";
    public static final String COUNTRY_PATH = "command.boDocumentForm.country";
    public static final String PHONE_EXPR = "boDocumentForm.phone";
    public static final String PHONE_PATH = "command.boDocumentForm.phone";
    public static final String FAX_EXPR = "boDocumentForm.fax";
    public static final String FAX_PATH = "command.boDocumentForm.fax";
    public static final String EMAIL_FROM_EXPR = "boDocumentForm.emailFrom";
    public static final String EMAIL_FROM_PATH = "command.boDocumentForm.emailFrom";
    public static final String NAME_FROM_EXPR = "boDocumentForm.nameFrom";
    public static final String NAME_FROM_PATH = "command.boDocumentForm.nameFrom";
    public static final String STREET_FROM_EXPR = "boDocumentForm.streetFrom";
    public static final String STREET_FROM_PATH = "command.boDocumentForm.streetFrom";
    public static final String STREET_NUMBER_FROM_EXPR = "boDocumentForm.streetNumberFrom";
    public static final String STREET_NUMBER_FROM_PATH = "command.boDocumentForm.streetNumberFrom";
    public static final String ZIP_CODE_FROM_EXPR = "boDocumentForm.zipCodeFrom";
    public static final String ZIP_CODE_FROM_PATH = "command.boDocumentForm.zipCodeFrom";
    public static final String CITY_FROM_EXPR = "boDocumentForm.cityFrom";
    public static final String CITY_FROM_PATH = "command.boDocumentForm.cityFrom";
    public static final String COUNTRY_FROM_EXPR = "boDocumentForm.countryFrom";
    public static final String COUNTRY_FROM_PATH = "command.boDocumentForm.countryFrom";
    public static final String PHONE_FROM_EXPR = "boDocumentForm.phoneFrom";
    public static final String PHONE_FROM_PATH = "command.boDocumentForm.phoneFrom";
    public static final String FAX_FROM_EXPR = "boDocumentForm.faxFrom";
    public static final String FAX_FROM_PATH = "command.boDocumentForm.faxFrom";
    public static final String ATTACHMENT_EXPR = "boDocumentForm.attachment";
    public static final String ATTACHMENT_PATH = "command.boDocumentForm.attachment";
    public static final String DELETE_ATTACHMENT_ID_EXPR = "boDocumentForm.deleteAttachmentId";
    public static final String DELETE_ATTACHMENT_ID_PATH = "command.boDocumentForm.deleteAttachmentId";
    public static final String CONSOLIDATED_DEBT_ID_EXPR = "boDocumentForm.consolidatedDebtId";
    public static final String CONSOLIDATED_DEBT_ID_PATH = "command.boDocumentForm.consolidatedDebtId";
    public static final String TYPE_OF_PROVIDER_EXPR = "boDocumentForm.typeOfProvider";
    public static final String TYPE_OF_PROVIDER_PATH = "command.boDocumentForm.typeOfProvider";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo document)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _subject;
    private String _subjectInitVal;
    private boolean _subjectIsSet;
    private String _text;
    private String _textInitVal;
    private boolean _textIsSet;
    private String _notes;
    private String _notesInitVal;
    private boolean _notesIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _date;
    private String _dateInitVal;
    private boolean _dateIsSet;
    private String _direction;
    private String _directionInitVal;
    private boolean _directionIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _approvalState;
    private String _approvalStateInitVal;
    private boolean _approvalStateIsSet;
    private String _addContractId;
    private String _addContractIdInitVal;
    private boolean _addContractIdIsSet;
    private String _deleteContractId;
    private String _deleteContractIdInitVal;
    private boolean _deleteContractIdIsSet;
    private String _addContractsOfOrderId;
    private String _addContractsOfOrderIdInitVal;
    private boolean _addContractsOfOrderIdIsSet;
    private String _accountId;
    private String _accountIdInitVal;
    private boolean _accountIdIsSet;
    private String _accountName;
    private String _accountNameInitVal;
    private boolean _accountNameIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _typeOfDispatch;
    private String _typeOfDispatchInitVal;
    private boolean _typeOfDispatchIsSet;
    private String _addressFromId;
    private String _addressFromIdInitVal;
    private boolean _addressFromIdIsSet;
    private String _addressToId;
    private String _addressToIdInitVal;
    private boolean _addressToIdIsSet;
    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _street;
    private String _streetInitVal;
    private boolean _streetIsSet;
    private String _streetNumber;
    private String _streetNumberInitVal;
    private boolean _streetNumberIsSet;
    private String _zipCode;
    private String _zipCodeInitVal;
    private boolean _zipCodeIsSet;
    private String _city;
    private String _cityInitVal;
    private boolean _cityIsSet;
    private String _country;
    private String _countryInitVal;
    private boolean _countryIsSet;
    private String _phone;
    private String _phoneInitVal;
    private boolean _phoneIsSet;
    private String _fax;
    private String _faxInitVal;
    private boolean _faxIsSet;
    private String _emailFrom;
    private String _emailFromInitVal;
    private boolean _emailFromIsSet;
    private String _nameFrom;
    private String _nameFromInitVal;
    private boolean _nameFromIsSet;
    private String _streetFrom;
    private String _streetFromInitVal;
    private boolean _streetFromIsSet;
    private String _streetNumberFrom;
    private String _streetNumberFromInitVal;
    private boolean _streetNumberFromIsSet;
    private String _zipCodeFrom;
    private String _zipCodeFromInitVal;
    private boolean _zipCodeFromIsSet;
    private String _cityFrom;
    private String _cityFromInitVal;
    private boolean _cityFromIsSet;
    private String _countryFrom;
    private String _countryFromInitVal;
    private boolean _countryFromIsSet;
    private String _phoneFrom;
    private String _phoneFromInitVal;
    private boolean _phoneFromIsSet;
    private String _faxFrom;
    private String _faxFromInitVal;
    private boolean _faxFromIsSet;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _attachment;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _attachmentInitVal;
    private boolean _attachmentIsSet;
    private String _deleteAttachmentId;
    private String _deleteAttachmentIdInitVal;
    private boolean _deleteAttachmentIdIsSet;
    private String _consolidatedDebtId;
    private String _consolidatedDebtIdInitVal;
    private boolean _consolidatedDebtIdIsSet;
    private String _typeOfProvider;
    private String _typeOfProviderInitVal;
    private boolean _typeOfProviderIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setSubject(String subject) {
        if (!_subjectIsSet) {
            _subjectIsSet = true;
            _subjectInitVal = subject;
        }
        _subject = subject;
    }

    public String getSubject() {
        return _subject;
    }

    public String subjectInitVal() {
        return _subjectInitVal;
    }

	public boolean subjectIsDirty() {
        return !valuesAreEqual(_subjectInitVal, _subject);
    }

    public boolean subjectIsSet() {
        return _subjectIsSet;
    }	


    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = text;
        }
        _text = text;
    }

    public String getText() {
        return _text;
    }

    public String textInitVal() {
        return _textInitVal;
    }

	public boolean textIsDirty() {
        return !valuesAreEqual(_textInitVal, _text);
    }

    public boolean textIsSet() {
        return _textIsSet;
    }	


    public void setNotes(String notes) {
        if (!_notesIsSet) {
            _notesIsSet = true;
            _notesInitVal = notes;
        }
        _notes = notes;
    }

    public String getNotes() {
        return _notes;
    }

    public String notesInitVal() {
        return _notesInitVal;
    }

	public boolean notesIsDirty() {
        return !valuesAreEqual(_notesInitVal, _notes);
    }

    public boolean notesIsSet() {
        return _notesIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setDate(String date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = date;
        }
        _date = date;
    }

    public String getDate() {
        return _date;
    }

    public String dateInitVal() {
        return _dateInitVal;
    }

	public boolean dateIsDirty() {
        return !valuesAreEqual(_dateInitVal, _date);
    }

    public boolean dateIsSet() {
        return _dateIsSet;
    }	


    public void setDirection(String direction) {
        if (!_directionIsSet) {
            _directionIsSet = true;
            _directionInitVal = direction;
        }
        _direction = direction;
    }

    public String getDirection() {
        return _direction;
    }

    public String directionInitVal() {
        return _directionInitVal;
    }

	public boolean directionIsDirty() {
        return !valuesAreEqual(_directionInitVal, _direction);
    }

    public boolean directionIsSet() {
        return _directionIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setApprovalState(String approvalState) {
        if (!_approvalStateIsSet) {
            _approvalStateIsSet = true;
            _approvalStateInitVal = approvalState;
        }
        _approvalState = approvalState;
    }

    public String getApprovalState() {
        return _approvalState;
    }

    public String approvalStateInitVal() {
        return _approvalStateInitVal;
    }

	public boolean approvalStateIsDirty() {
        return !valuesAreEqual(_approvalStateInitVal, _approvalState);
    }

    public boolean approvalStateIsSet() {
        return _approvalStateIsSet;
    }	


    public void setAddContractId(String addContractId) {
        if (!_addContractIdIsSet) {
            _addContractIdIsSet = true;
            _addContractIdInitVal = addContractId;
        }
        _addContractId = addContractId;
    }

    public String getAddContractId() {
        return _addContractId;
    }

    public String addContractIdInitVal() {
        return _addContractIdInitVal;
    }

	public boolean addContractIdIsDirty() {
        return !valuesAreEqual(_addContractIdInitVal, _addContractId);
    }

    public boolean addContractIdIsSet() {
        return _addContractIdIsSet;
    }	


    public void setDeleteContractId(String deleteContractId) {
        if (!_deleteContractIdIsSet) {
            _deleteContractIdIsSet = true;
            _deleteContractIdInitVal = deleteContractId;
        }
        _deleteContractId = deleteContractId;
    }

    public String getDeleteContractId() {
        return _deleteContractId;
    }

    public String deleteContractIdInitVal() {
        return _deleteContractIdInitVal;
    }

	public boolean deleteContractIdIsDirty() {
        return !valuesAreEqual(_deleteContractIdInitVal, _deleteContractId);
    }

    public boolean deleteContractIdIsSet() {
        return _deleteContractIdIsSet;
    }	


    public void setAddContractsOfOrderId(String addContractsOfOrderId) {
        if (!_addContractsOfOrderIdIsSet) {
            _addContractsOfOrderIdIsSet = true;
            _addContractsOfOrderIdInitVal = addContractsOfOrderId;
        }
        _addContractsOfOrderId = addContractsOfOrderId;
    }

    public String getAddContractsOfOrderId() {
        return _addContractsOfOrderId;
    }

    public String addContractsOfOrderIdInitVal() {
        return _addContractsOfOrderIdInitVal;
    }

	public boolean addContractsOfOrderIdIsDirty() {
        return !valuesAreEqual(_addContractsOfOrderIdInitVal, _addContractsOfOrderId);
    }

    public boolean addContractsOfOrderIdIsSet() {
        return _addContractsOfOrderIdIsSet;
    }	


    public void setAccountId(String accountId) {
        if (!_accountIdIsSet) {
            _accountIdIsSet = true;
            _accountIdInitVal = accountId;
        }
        _accountId = accountId;
    }

    public String getAccountId() {
        return _accountId;
    }

    public String accountIdInitVal() {
        return _accountIdInitVal;
    }

	public boolean accountIdIsDirty() {
        return !valuesAreEqual(_accountIdInitVal, _accountId);
    }

    public boolean accountIdIsSet() {
        return _accountIdIsSet;
    }	


    public void setAccountName(String accountName) {
        if (!_accountNameIsSet) {
            _accountNameIsSet = true;
            _accountNameInitVal = accountName;
        }
        _accountName = accountName;
    }

    public String getAccountName() {
        return _accountName;
    }

    public String accountNameInitVal() {
        return _accountNameInitVal;
    }

	public boolean accountNameIsDirty() {
        return !valuesAreEqual(_accountNameInitVal, _accountName);
    }

    public boolean accountNameIsSet() {
        return _accountNameIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setTypeOfDispatch(String typeOfDispatch) {
        if (!_typeOfDispatchIsSet) {
            _typeOfDispatchIsSet = true;
            _typeOfDispatchInitVal = typeOfDispatch;
        }
        _typeOfDispatch = typeOfDispatch;
    }

    public String getTypeOfDispatch() {
        return _typeOfDispatch;
    }

    public String typeOfDispatchInitVal() {
        return _typeOfDispatchInitVal;
    }

	public boolean typeOfDispatchIsDirty() {
        return !valuesAreEqual(_typeOfDispatchInitVal, _typeOfDispatch);
    }

    public boolean typeOfDispatchIsSet() {
        return _typeOfDispatchIsSet;
    }	


    public void setAddressFromId(String addressFromId) {
        if (!_addressFromIdIsSet) {
            _addressFromIdIsSet = true;
            _addressFromIdInitVal = addressFromId;
        }
        _addressFromId = addressFromId;
    }

    public String getAddressFromId() {
        return _addressFromId;
    }

    public String addressFromIdInitVal() {
        return _addressFromIdInitVal;
    }

	public boolean addressFromIdIsDirty() {
        return !valuesAreEqual(_addressFromIdInitVal, _addressFromId);
    }

    public boolean addressFromIdIsSet() {
        return _addressFromIdIsSet;
    }	


    public void setAddressToId(String addressToId) {
        if (!_addressToIdIsSet) {
            _addressToIdIsSet = true;
            _addressToIdInitVal = addressToId;
        }
        _addressToId = addressToId;
    }

    public String getAddressToId() {
        return _addressToId;
    }

    public String addressToIdInitVal() {
        return _addressToIdInitVal;
    }

	public boolean addressToIdIsDirty() {
        return !valuesAreEqual(_addressToIdInitVal, _addressToId);
    }

    public boolean addressToIdIsSet() {
        return _addressToIdIsSet;
    }	


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setStreet(String street) {
        if (!_streetIsSet) {
            _streetIsSet = true;
            _streetInitVal = street;
        }
        _street = street;
    }

    public String getStreet() {
        return _street;
    }

    public String streetInitVal() {
        return _streetInitVal;
    }

	public boolean streetIsDirty() {
        return !valuesAreEqual(_streetInitVal, _street);
    }

    public boolean streetIsSet() {
        return _streetIsSet;
    }	


    public void setStreetNumber(String streetNumber) {
        if (!_streetNumberIsSet) {
            _streetNumberIsSet = true;
            _streetNumberInitVal = streetNumber;
        }
        _streetNumber = streetNumber;
    }

    public String getStreetNumber() {
        return _streetNumber;
    }

    public String streetNumberInitVal() {
        return _streetNumberInitVal;
    }

	public boolean streetNumberIsDirty() {
        return !valuesAreEqual(_streetNumberInitVal, _streetNumber);
    }

    public boolean streetNumberIsSet() {
        return _streetNumberIsSet;
    }	


    public void setZipCode(String zipCode) {
        if (!_zipCodeIsSet) {
            _zipCodeIsSet = true;
            _zipCodeInitVal = zipCode;
        }
        _zipCode = zipCode;
    }

    public String getZipCode() {
        return _zipCode;
    }

    public String zipCodeInitVal() {
        return _zipCodeInitVal;
    }

	public boolean zipCodeIsDirty() {
        return !valuesAreEqual(_zipCodeInitVal, _zipCode);
    }

    public boolean zipCodeIsSet() {
        return _zipCodeIsSet;
    }	


    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = city;
        }
        _city = city;
    }

    public String getCity() {
        return _city;
    }

    public String cityInitVal() {
        return _cityInitVal;
    }

	public boolean cityIsDirty() {
        return !valuesAreEqual(_cityInitVal, _city);
    }

    public boolean cityIsSet() {
        return _cityIsSet;
    }	


    public void setCountry(String country) {
        if (!_countryIsSet) {
            _countryIsSet = true;
            _countryInitVal = country;
        }
        _country = country;
    }

    public String getCountry() {
        return _country;
    }

    public String countryInitVal() {
        return _countryInitVal;
    }

	public boolean countryIsDirty() {
        return !valuesAreEqual(_countryInitVal, _country);
    }

    public boolean countryIsSet() {
        return _countryIsSet;
    }	


    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = phone;
        }
        _phone = phone;
    }

    public String getPhone() {
        return _phone;
    }

    public String phoneInitVal() {
        return _phoneInitVal;
    }

	public boolean phoneIsDirty() {
        return !valuesAreEqual(_phoneInitVal, _phone);
    }

    public boolean phoneIsSet() {
        return _phoneIsSet;
    }	


    public void setFax(String fax) {
        if (!_faxIsSet) {
            _faxIsSet = true;
            _faxInitVal = fax;
        }
        _fax = fax;
    }

    public String getFax() {
        return _fax;
    }

    public String faxInitVal() {
        return _faxInitVal;
    }

	public boolean faxIsDirty() {
        return !valuesAreEqual(_faxInitVal, _fax);
    }

    public boolean faxIsSet() {
        return _faxIsSet;
    }	


    public void setEmailFrom(String emailFrom) {
        if (!_emailFromIsSet) {
            _emailFromIsSet = true;
            _emailFromInitVal = emailFrom;
        }
        _emailFrom = emailFrom;
    }

    public String getEmailFrom() {
        return _emailFrom;
    }

    public String emailFromInitVal() {
        return _emailFromInitVal;
    }

	public boolean emailFromIsDirty() {
        return !valuesAreEqual(_emailFromInitVal, _emailFrom);
    }

    public boolean emailFromIsSet() {
        return _emailFromIsSet;
    }	


    public void setNameFrom(String nameFrom) {
        if (!_nameFromIsSet) {
            _nameFromIsSet = true;
            _nameFromInitVal = nameFrom;
        }
        _nameFrom = nameFrom;
    }

    public String getNameFrom() {
        return _nameFrom;
    }

    public String nameFromInitVal() {
        return _nameFromInitVal;
    }

	public boolean nameFromIsDirty() {
        return !valuesAreEqual(_nameFromInitVal, _nameFrom);
    }

    public boolean nameFromIsSet() {
        return _nameFromIsSet;
    }	


    public void setStreetFrom(String streetFrom) {
        if (!_streetFromIsSet) {
            _streetFromIsSet = true;
            _streetFromInitVal = streetFrom;
        }
        _streetFrom = streetFrom;
    }

    public String getStreetFrom() {
        return _streetFrom;
    }

    public String streetFromInitVal() {
        return _streetFromInitVal;
    }

	public boolean streetFromIsDirty() {
        return !valuesAreEqual(_streetFromInitVal, _streetFrom);
    }

    public boolean streetFromIsSet() {
        return _streetFromIsSet;
    }	


    public void setStreetNumberFrom(String streetNumberFrom) {
        if (!_streetNumberFromIsSet) {
            _streetNumberFromIsSet = true;
            _streetNumberFromInitVal = streetNumberFrom;
        }
        _streetNumberFrom = streetNumberFrom;
    }

    public String getStreetNumberFrom() {
        return _streetNumberFrom;
    }

    public String streetNumberFromInitVal() {
        return _streetNumberFromInitVal;
    }

	public boolean streetNumberFromIsDirty() {
        return !valuesAreEqual(_streetNumberFromInitVal, _streetNumberFrom);
    }

    public boolean streetNumberFromIsSet() {
        return _streetNumberFromIsSet;
    }	


    public void setZipCodeFrom(String zipCodeFrom) {
        if (!_zipCodeFromIsSet) {
            _zipCodeFromIsSet = true;
            _zipCodeFromInitVal = zipCodeFrom;
        }
        _zipCodeFrom = zipCodeFrom;
    }

    public String getZipCodeFrom() {
        return _zipCodeFrom;
    }

    public String zipCodeFromInitVal() {
        return _zipCodeFromInitVal;
    }

	public boolean zipCodeFromIsDirty() {
        return !valuesAreEqual(_zipCodeFromInitVal, _zipCodeFrom);
    }

    public boolean zipCodeFromIsSet() {
        return _zipCodeFromIsSet;
    }	


    public void setCityFrom(String cityFrom) {
        if (!_cityFromIsSet) {
            _cityFromIsSet = true;
            _cityFromInitVal = cityFrom;
        }
        _cityFrom = cityFrom;
    }

    public String getCityFrom() {
        return _cityFrom;
    }

    public String cityFromInitVal() {
        return _cityFromInitVal;
    }

	public boolean cityFromIsDirty() {
        return !valuesAreEqual(_cityFromInitVal, _cityFrom);
    }

    public boolean cityFromIsSet() {
        return _cityFromIsSet;
    }	


    public void setCountryFrom(String countryFrom) {
        if (!_countryFromIsSet) {
            _countryFromIsSet = true;
            _countryFromInitVal = countryFrom;
        }
        _countryFrom = countryFrom;
    }

    public String getCountryFrom() {
        return _countryFrom;
    }

    public String countryFromInitVal() {
        return _countryFromInitVal;
    }

	public boolean countryFromIsDirty() {
        return !valuesAreEqual(_countryFromInitVal, _countryFrom);
    }

    public boolean countryFromIsSet() {
        return _countryFromIsSet;
    }	


    public void setPhoneFrom(String phoneFrom) {
        if (!_phoneFromIsSet) {
            _phoneFromIsSet = true;
            _phoneFromInitVal = phoneFrom;
        }
        _phoneFrom = phoneFrom;
    }

    public String getPhoneFrom() {
        return _phoneFrom;
    }

    public String phoneFromInitVal() {
        return _phoneFromInitVal;
    }

	public boolean phoneFromIsDirty() {
        return !valuesAreEqual(_phoneFromInitVal, _phoneFrom);
    }

    public boolean phoneFromIsSet() {
        return _phoneFromIsSet;
    }	


    public void setFaxFrom(String faxFrom) {
        if (!_faxFromIsSet) {
            _faxFromIsSet = true;
            _faxFromInitVal = faxFrom;
        }
        _faxFrom = faxFrom;
    }

    public String getFaxFrom() {
        return _faxFrom;
    }

    public String faxFromInitVal() {
        return _faxFromInitVal;
    }

	public boolean faxFromIsDirty() {
        return !valuesAreEqual(_faxFromInitVal, _faxFrom);
    }

    public boolean faxFromIsSet() {
        return _faxFromIsSet;
    }	


    public void setAttachment(org.springframework.web.multipart.commons.CommonsMultipartFile attachment) {
        if (!_attachmentIsSet) {
            _attachmentIsSet = true;
            _attachmentInitVal = attachment;
        }
        _attachment = attachment;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile getAttachment() {
        return _attachment;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile attachmentInitVal() {
        return _attachmentInitVal;
    }

	public boolean attachmentIsDirty() {
        return !valuesAreEqual(_attachmentInitVal, _attachment);
    }

    public boolean attachmentIsSet() {
        return _attachmentIsSet;
    }	


    public void setDeleteAttachmentId(String deleteAttachmentId) {
        if (!_deleteAttachmentIdIsSet) {
            _deleteAttachmentIdIsSet = true;
            _deleteAttachmentIdInitVal = deleteAttachmentId;
        }
        _deleteAttachmentId = deleteAttachmentId;
    }

    public String getDeleteAttachmentId() {
        return _deleteAttachmentId;
    }

    public String deleteAttachmentIdInitVal() {
        return _deleteAttachmentIdInitVal;
    }

	public boolean deleteAttachmentIdIsDirty() {
        return !valuesAreEqual(_deleteAttachmentIdInitVal, _deleteAttachmentId);
    }

    public boolean deleteAttachmentIdIsSet() {
        return _deleteAttachmentIdIsSet;
    }	


    public void setConsolidatedDebtId(String consolidatedDebtId) {
        if (!_consolidatedDebtIdIsSet) {
            _consolidatedDebtIdIsSet = true;
            _consolidatedDebtIdInitVal = consolidatedDebtId;
        }
        _consolidatedDebtId = consolidatedDebtId;
    }

    public String getConsolidatedDebtId() {
        return _consolidatedDebtId;
    }

    public String consolidatedDebtIdInitVal() {
        return _consolidatedDebtIdInitVal;
    }

	public boolean consolidatedDebtIdIsDirty() {
        return !valuesAreEqual(_consolidatedDebtIdInitVal, _consolidatedDebtId);
    }

    public boolean consolidatedDebtIdIsSet() {
        return _consolidatedDebtIdIsSet;
    }	


    public void setTypeOfProvider(String typeOfProvider) {
        if (!_typeOfProviderIsSet) {
            _typeOfProviderIsSet = true;
            _typeOfProviderInitVal = typeOfProvider;
        }
        _typeOfProvider = typeOfProvider;
    }

    public String getTypeOfProvider() {
        return _typeOfProvider;
    }

    public String typeOfProviderInitVal() {
        return _typeOfProviderInitVal;
    }

	public boolean typeOfProviderIsDirty() {
        return !valuesAreEqual(_typeOfProviderInitVal, _typeOfProvider);
    }

    public boolean typeOfProviderIsSet() {
        return _typeOfProviderIsSet;
    }	

}
