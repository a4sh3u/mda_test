//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.smavagroup;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(create smava group)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CreateSmavaGroupForm'.
 *
 * @author generator
 */
public class CreateSmavaGroupForm implements Serializable {

    public static final String NAME_EXPR = "createSmavaGroupForm.name";
    public static final String NAME_PATH = "command.createSmavaGroupForm.name";
    public static final String TAGLINE_EXPR = "createSmavaGroupForm.tagline";
    public static final String TAGLINE_PATH = "command.createSmavaGroupForm.tagline";
    public static final String IMAGE_EXPR = "createSmavaGroupForm.image";
    public static final String IMAGE_PATH = "command.createSmavaGroupForm.image";
    public static final String IMAGEFILE_EXPR = "createSmavaGroupForm.imagefile";
    public static final String IMAGEFILE_PATH = "command.createSmavaGroupForm.imagefile";
    public static final String DESCRIPTION_EXPR = "createSmavaGroupForm.description";
    public static final String DESCRIPTION_PATH = "command.createSmavaGroupForm.description";
    public static final String TYPE_EXPR = "createSmavaGroupForm.type";
    public static final String TYPE_PATH = "command.createSmavaGroupForm.type";
    public static final String BEST_MARKET_EXPR = "createSmavaGroupForm.bestMarket";
    public static final String BEST_MARKET_PATH = "command.createSmavaGroupForm.bestMarket";
    public static final String WORST_MARKET_EXPR = "createSmavaGroupForm.worstMarket";
    public static final String WORST_MARKET_PATH = "command.createSmavaGroupForm.worstMarket";
    public static final String LENDER_DESCRIPTION_EXPR = "createSmavaGroupForm.lenderDescription";
    public static final String LENDER_DESCRIPTION_PATH = "command.createSmavaGroupForm.lenderDescription";
    public static final String BORROWER_DESCRIPTION_EXPR = "createSmavaGroupForm.borrowerDescription";
    public static final String BORROWER_DESCRIPTION_PATH = "command.createSmavaGroupForm.borrowerDescription";
    public static final String MAIN_CATEGORY_EXPR = "createSmavaGroupForm.mainCategory";
    public static final String MAIN_CATEGORY_PATH = "command.createSmavaGroupForm.mainCategory";
    public static final String SECOND_CATEGORY_EXPR = "createSmavaGroupForm.secondCategory";
    public static final String SECOND_CATEGORY_PATH = "command.createSmavaGroupForm.secondCategory";
    public static final String REAPPLICATION_REQUIRED_EXPR = "createSmavaGroupForm.reapplicationRequired";
    public static final String REAPPLICATION_REQUIRED_PATH = "command.createSmavaGroupForm.reapplicationRequired";
    public static final String RATE_BONUS_EXPR = "createSmavaGroupForm.rateBonus";
    public static final String RATE_BONUS_PATH = "command.createSmavaGroupForm.rateBonus";
    public static final String DELETE_IMAGE_EXPR = "createSmavaGroupForm.deleteImage";
    public static final String DELETE_IMAGE_PATH = "command.createSmavaGroupForm.deleteImage";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(create smava group)}
    private byte[] _imageBytes = null;

    public byte[] getImageBytes() {
        if (_imageBytes == null && _imagefile != null && !_imagefile.isEmpty()) {
            _imageBytes = _imagefile.getBytes();
        }

        byte[] imageBytes = null;
        if (_imageBytes != null) {
            imageBytes = _imageBytes.clone();
        }
        return imageBytes;
    }

    public void setImageBytes(byte[] bytes) {
        if (bytes == null) {
            _imageBytes = null;
        } else {
            _imageBytes = bytes.clone();
        }
    }
// !!!!!!!! End of insert code section !!!!!!!!

    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _tagline;
    private String _taglineInitVal;
    private boolean _taglineIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _imagefile;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _imagefileInitVal;
    private boolean _imagefileIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _bestMarket;
    private String _bestMarketInitVal;
    private boolean _bestMarketIsSet;
    private String _worstMarket;
    private String _worstMarketInitVal;
    private boolean _worstMarketIsSet;
    private String _lenderDescription;
    private String _lenderDescriptionInitVal;
    private boolean _lenderDescriptionIsSet;
    private String _borrowerDescription;
    private String _borrowerDescriptionInitVal;
    private boolean _borrowerDescriptionIsSet;
    private Long _mainCategory;
    private Long _mainCategoryInitVal;
    private boolean _mainCategoryIsSet;
    private Long _secondCategory;
    private Long _secondCategoryInitVal;
    private boolean _secondCategoryIsSet;
    private Boolean _reapplicationRequired;
    private Boolean _reapplicationRequiredInitVal;
    private boolean _reapplicationRequiredIsSet;
    private String _rateBonus;
    private String _rateBonusInitVal;
    private boolean _rateBonusIsSet;
    private boolean _deleteImage;
    private boolean _deleteImageInitVal;
    private boolean _deleteImageIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setTagline(String tagline) {
        if (!_taglineIsSet) {
            _taglineIsSet = true;
            _taglineInitVal = tagline;
        }
        _tagline = tagline;
    }

    public String getTagline() {
        return _tagline;
    }

    public String taglineInitVal() {
        return _taglineInitVal;
    }

	public boolean taglineIsDirty() {
        return !valuesAreEqual(_taglineInitVal, _tagline);
    }

    public boolean taglineIsSet() {
        return _taglineIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setImagefile(org.springframework.web.multipart.commons.CommonsMultipartFile imagefile) {
        if (!_imagefileIsSet) {
            _imagefileIsSet = true;
            _imagefileInitVal = imagefile;
        }
        _imagefile = imagefile;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile getImagefile() {
        return _imagefile;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile imagefileInitVal() {
        return _imagefileInitVal;
    }

	public boolean imagefileIsDirty() {
        return !valuesAreEqual(_imagefileInitVal, _imagefile);
    }

    public boolean imagefileIsSet() {
        return _imagefileIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setBestMarket(String bestMarket) {
        if (!_bestMarketIsSet) {
            _bestMarketIsSet = true;
            _bestMarketInitVal = bestMarket;
        }
        _bestMarket = bestMarket;
    }

    public String getBestMarket() {
        return _bestMarket;
    }

    public String bestMarketInitVal() {
        return _bestMarketInitVal;
    }

	public boolean bestMarketIsDirty() {
        return !valuesAreEqual(_bestMarketInitVal, _bestMarket);
    }

    public boolean bestMarketIsSet() {
        return _bestMarketIsSet;
    }	


    public void setWorstMarket(String worstMarket) {
        if (!_worstMarketIsSet) {
            _worstMarketIsSet = true;
            _worstMarketInitVal = worstMarket;
        }
        _worstMarket = worstMarket;
    }

    public String getWorstMarket() {
        return _worstMarket;
    }

    public String worstMarketInitVal() {
        return _worstMarketInitVal;
    }

	public boolean worstMarketIsDirty() {
        return !valuesAreEqual(_worstMarketInitVal, _worstMarket);
    }

    public boolean worstMarketIsSet() {
        return _worstMarketIsSet;
    }	


    public void setLenderDescription(String lenderDescription) {
        if (!_lenderDescriptionIsSet) {
            _lenderDescriptionIsSet = true;
            _lenderDescriptionInitVal = lenderDescription;
        }
        _lenderDescription = lenderDescription;
    }

    public String getLenderDescription() {
        return _lenderDescription;
    }

    public String lenderDescriptionInitVal() {
        return _lenderDescriptionInitVal;
    }

	public boolean lenderDescriptionIsDirty() {
        return !valuesAreEqual(_lenderDescriptionInitVal, _lenderDescription);
    }

    public boolean lenderDescriptionIsSet() {
        return _lenderDescriptionIsSet;
    }	


    public void setBorrowerDescription(String borrowerDescription) {
        if (!_borrowerDescriptionIsSet) {
            _borrowerDescriptionIsSet = true;
            _borrowerDescriptionInitVal = borrowerDescription;
        }
        _borrowerDescription = borrowerDescription;
    }

    public String getBorrowerDescription() {
        return _borrowerDescription;
    }

    public String borrowerDescriptionInitVal() {
        return _borrowerDescriptionInitVal;
    }

	public boolean borrowerDescriptionIsDirty() {
        return !valuesAreEqual(_borrowerDescriptionInitVal, _borrowerDescription);
    }

    public boolean borrowerDescriptionIsSet() {
        return _borrowerDescriptionIsSet;
    }	


    public void setMainCategory(Long mainCategory) {
        if (!_mainCategoryIsSet) {
            _mainCategoryIsSet = true;
            _mainCategoryInitVal = mainCategory;
        }
        _mainCategory = mainCategory;
    }

    public Long getMainCategory() {
        return _mainCategory;
    }

    public Long mainCategoryInitVal() {
        return _mainCategoryInitVal;
    }

	public boolean mainCategoryIsDirty() {
        return !valuesAreEqual(_mainCategoryInitVal, _mainCategory);
    }

    public boolean mainCategoryIsSet() {
        return _mainCategoryIsSet;
    }	


    public void setSecondCategory(Long secondCategory) {
        if (!_secondCategoryIsSet) {
            _secondCategoryIsSet = true;
            _secondCategoryInitVal = secondCategory;
        }
        _secondCategory = secondCategory;
    }

    public Long getSecondCategory() {
        return _secondCategory;
    }

    public Long secondCategoryInitVal() {
        return _secondCategoryInitVal;
    }

	public boolean secondCategoryIsDirty() {
        return !valuesAreEqual(_secondCategoryInitVal, _secondCategory);
    }

    public boolean secondCategoryIsSet() {
        return _secondCategoryIsSet;
    }	


    public void setReapplicationRequired(Boolean reapplicationRequired) {
        if (!_reapplicationRequiredIsSet) {
            _reapplicationRequiredIsSet = true;
            _reapplicationRequiredInitVal = reapplicationRequired;
        }
        _reapplicationRequired = reapplicationRequired;
    }

    public Boolean getReapplicationRequired() {
        return _reapplicationRequired;
    }

    public Boolean reapplicationRequiredInitVal() {
        return _reapplicationRequiredInitVal;
    }

	public boolean reapplicationRequiredIsDirty() {
        return !valuesAreEqual(_reapplicationRequiredInitVal, _reapplicationRequired);
    }

    public boolean reapplicationRequiredIsSet() {
        return _reapplicationRequiredIsSet;
    }	


    public void setRateBonus(String rateBonus) {
        if (!_rateBonusIsSet) {
            _rateBonusIsSet = true;
            _rateBonusInitVal = rateBonus;
        }
        _rateBonus = rateBonus;
    }

    public String getRateBonus() {
        return _rateBonus;
    }

    public String rateBonusInitVal() {
        return _rateBonusInitVal;
    }

	public boolean rateBonusIsDirty() {
        return !valuesAreEqual(_rateBonusInitVal, _rateBonus);
    }

    public boolean rateBonusIsSet() {
        return _rateBonusIsSet;
    }	


    public void setDeleteImage(boolean deleteImage) {
        if (!_deleteImageIsSet) {
            _deleteImageIsSet = true;
            _deleteImageInitVal = deleteImage;
        }
        _deleteImage = deleteImage;
    }

    public boolean getDeleteImage() {
        return _deleteImage;
    }

    public boolean deleteImageInitVal() {
        return _deleteImageInitVal;
    }

	public boolean deleteImageIsDirty() {
        return !valuesAreEqual(_deleteImageInitVal, _deleteImage);
    }

    public boolean deleteImageIsSet() {
        return _deleteImageIsSet;
    }	

}
