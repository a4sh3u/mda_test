//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.offer;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(create offer)}

import java.io.Serializable;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CreateOfferForm'.
 *
 * @author generator
 */
public class CreateOfferForm implements Serializable {

    public static final String AMOUNT_EXPR = "createOfferForm.amount";
    public static final String AMOUNT_PATH = "command.createOfferForm.amount";
    public static final String TERM_EXPR = "createOfferForm.term";
    public static final String TERM_PATH = "command.createOfferForm.term";
    public static final String RISC_EXPR = "createOfferForm.risc";
    public static final String RISC_PATH = "command.createOfferForm.risc";
    public static final String RATE_EXPR = "createOfferForm.rate";
    public static final String RATE_PATH = "command.createOfferForm.rate";
    public static final String DIR_DEBIT_MANDATE_EXPR = "createOfferForm.dirDebitMandate";
    public static final String DIR_DEBIT_MANDATE_PATH = "command.createOfferForm.dirDebitMandate";
    public static final String TERM_AND_COND_EXPR = "createOfferForm.termAndCond";
    public static final String TERM_AND_COND_PATH = "command.createOfferForm.termAndCond";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(create offer)}
    private static final long serialVersionUID = 3613372184067877526L;
// !!!!!!!! End of insert code section !!!!!!!!


    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;

    private List<String> _term;
    private List<String> _termInitVal;
    private boolean _termIsSet;

    private List<String> _risc;
    private List<String> _riscInitVal;
    private boolean _riscIsSet;

    private String _rate;
    private String _rateInitVal;
    private boolean _rateIsSet;

    private Boolean _dirDebitMandate;
    private Boolean _dirDebitMandateInitVal;
    private boolean _dirDebitMandateIsSet;

    private Boolean _termAndCond;
    private Boolean _termAndCondInitVal;
    private boolean _termAndCondIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

    public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }

    public void setTerm(List<String> term) {
        if (!_termIsSet) {
            _termIsSet = true;
            _termInitVal = term;
        }
        _term = term;
    }

    public List<String> getTerm() {
        return _term;
    }

    public List<String> termInitVal() {
        return _termInitVal;
    }

    public boolean termIsDirty() {
        return !valuesAreEqual(_termInitVal, _term);
    }

    public boolean termIsSet() {
        return _termIsSet;
    }

    public void setRisc(List<String> risc) {
        if (!_riscIsSet) {
            _riscIsSet = true;
            _riscInitVal = risc;
        }
        _risc = risc;
    }

    public List<String> getRisc() {
        return _risc;
    }

    public List<String> riscInitVal() {
        return _riscInitVal;
    }

    public boolean riscIsDirty() {
        return !valuesAreEqual(_riscInitVal, _risc);
    }

    public boolean riscIsSet() {
        return _riscIsSet;
    }

    public void setRate(String rate) {
        if (!_rateIsSet) {
            _rateIsSet = true;
            _rateInitVal = rate;
        }
        _rate = rate;
    }

    public String getRate() {
        return _rate;
    }

    public String rateInitVal() {
        return _rateInitVal;
    }

    public boolean rateIsDirty() {
        return !valuesAreEqual(_rateInitVal, _rate);
    }

    public boolean rateIsSet() {
        return _rateIsSet;
    }

    public void setDirDebitMandate(Boolean dirDebitMandate) {
        if (!_dirDebitMandateIsSet) {
            _dirDebitMandateIsSet = true;
            _dirDebitMandateInitVal = dirDebitMandate;
        }
        _dirDebitMandate = dirDebitMandate;
    }

    public Boolean getDirDebitMandate() {
        return _dirDebitMandate;
    }

    public Boolean dirDebitMandateInitVal() {
        return _dirDebitMandateInitVal;
    }

    public boolean dirDebitMandateIsDirty() {
        return !valuesAreEqual(_dirDebitMandateInitVal, _dirDebitMandate);
    }

    public boolean dirDebitMandateIsSet() {
        return _dirDebitMandateIsSet;
    }

    public void setTermAndCond(Boolean termAndCond) {
        if (!_termAndCondIsSet) {
            _termAndCondIsSet = true;
            _termAndCondInitVal = termAndCond;
        }
        _termAndCond = termAndCond;
    }

    public Boolean getTermAndCond() {
        return _termAndCond;
    }

    public Boolean termAndCondInitVal() {
        return _termAndCondInitVal;
    }

    public boolean termAndCondIsDirty() {
        return !valuesAreEqual(_termAndCondInitVal, _termAndCond);
    }

    public boolean termAndCondIsSet() {
        return _termAndCondIsSet;
    }

}
