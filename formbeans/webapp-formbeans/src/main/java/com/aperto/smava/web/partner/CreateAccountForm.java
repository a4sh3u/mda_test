//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/rfiedler/svn/head11/mda/model.xml
//
//    Or you can change the template:
//
//    /home/rfiedler/svn/head11/mda/templates/formbean.tpl
//
//
//
//
package com.aperto.smava.web.partner;

    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(create account)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CreateAccountForm'.
 *
 * @author generator
 */
public class CreateAccountForm implements Serializable {

    public static final String AGIO_EXPR = "createAccountForm.agio";
    public static final String AGIO_PATH = "command.createAccountForm.agio";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(create account)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _agio;
    private String _agioInitVal;
    private boolean _agioIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setAgio(String agio) {
        if (!_agioIsSet) {
            _agioIsSet = true;
            _agioInitVal = agio;
        }
        _agio = agio;
    }

    public String getAgio() {
        return _agio;
    }

    public String agioInitVal() {
        return _agioInitVal;
    }

	public boolean agioIsDirty() {
        return !valuesAreEqual(_agioInitVal, _agio);
    }

    public boolean agioIsSet() {
        return _agioIsSet;
    }	

}
