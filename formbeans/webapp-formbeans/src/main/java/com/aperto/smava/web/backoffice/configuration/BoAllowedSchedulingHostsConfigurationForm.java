//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.configuration;


    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo allowed scheduling hosts configuration)}
import java.io.Serializable;
import java.util.Collection;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoAllowedSchedulingHostsConfigurationForm'.
 *
 * @author generator
 */
public class BoAllowedSchedulingHostsConfigurationForm implements Serializable {
    
	public static final String ALLOWED_HOSTS_EXPR = "boAllowedSchedulingHostsConfigurationForm.allowedHosts";
    public static final String ALLOWED_HOSTS_PATH = "command.boAllowedSchedulingHostsConfigurationForm.allowedHosts";
    public static final String NEW_HOST_EXPR = "boAllowedSchedulingHostsConfigurationForm.newHost";
    public static final String NEW_HOST_PATH = "command.boAllowedSchedulingHostsConfigurationForm.newHost";
    public static final String ADD_LOCALHOST_EXPR = "boAllowedSchedulingHostsConfigurationForm.addLocalhost";
    public static final String ADD_LOCALHOST_PATH = "command.boAllowedSchedulingHostsConfigurationForm.addLocalhost";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo allowed scheduling hosts configuration)}

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;    
    
// !!!!!!!! End of insert code section !!!!!!!!


    private Collection<String> _allowedHosts;
    private Collection<String> _allowedHostsInitVal;
    private boolean _allowedHostsIsSet;

    private String _newHost;
    private String _newHostInitVal;
    private boolean _newHostIsSet;

    private boolean _addLocalhost;
    private boolean _addLocalhostInitVal;
    private boolean _addLocalhostIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setAllowedHosts(Collection<String> allowedHosts) {
        if (!_allowedHostsIsSet) {
            _allowedHostsIsSet = true;
            _allowedHostsInitVal = allowedHosts;
        }
        _allowedHosts = allowedHosts;
    }

    public Collection<String> getAllowedHosts() {
        return _allowedHosts;
    }

    public Collection<String> allowedHostsInitVal() {
        return _allowedHostsInitVal;
    }

    public boolean allowedHostsIsDirty() {
        return !valuesAreEqual(_allowedHostsInitVal, _allowedHosts);
    }

    public boolean allowedHostsIsSet() {
        return _allowedHostsIsSet;
    }

    public void setNewHost(String newHost) {
        if (!_newHostIsSet) {
            _newHostIsSet = true;
            _newHostInitVal = newHost;
        }
        _newHost = newHost;
    }

    public String getNewHost() {
        return _newHost;
    }

    public String newHostInitVal() {
        return _newHostInitVal;
    }

    public boolean newHostIsDirty() {
        return !valuesAreEqual(_newHostInitVal, _newHost);
    }

    public boolean newHostIsSet() {
        return _newHostIsSet;
    }

    public void setAddLocalhost(boolean addLocalhost) {
        if (!_addLocalhostIsSet) {
            _addLocalhostIsSet = true;
            _addLocalhostInitVal = addLocalhost;
        }
        _addLocalhost = addLocalhost;
    }

    public boolean getAddLocalhost() {
        return _addLocalhost;
    }

    public boolean addLocalhostInitVal() {
        return _addLocalhostInitVal;
    }

    public boolean addLocalhostIsDirty() {
        return !valuesAreEqual(_addLocalhostInitVal, _addLocalhost);
    }

    public boolean addLocalhostIsSet() {
        return _addLocalhostIsSet;
    }

}
