//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.dtaus;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo upload dtaus file)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoUploadDtausFileForm'.
 *
 * @author generator
 */
public class BoUploadDtausFileForm {

    public static final String FILE_EXPR = "boUploadDtausFileForm.file";
    public static final String FILE_PATH = "command.boUploadDtausFileForm.file";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo upload dtaus file)}
// !!!!!!!! End of insert code section !!!!!!!!


    private org.springframework.web.multipart.commons.CommonsMultipartFile _file;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _fileInitVal;
    private boolean _fileIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setFile(org.springframework.web.multipart.commons.CommonsMultipartFile file) {
        if (!_fileIsSet) {
            _fileIsSet = true;
            _fileInitVal = file;
        }
        _file = file;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile getFile() {
        return _file;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile fileInitVal() {
        return _fileInitVal;
    }

    public boolean fileIsDirty() {
        return !valuesAreEqual(_fileInitVal, _file);
    }

    public boolean fileIsSet() {
        return _fileIsSet;
    }

}
