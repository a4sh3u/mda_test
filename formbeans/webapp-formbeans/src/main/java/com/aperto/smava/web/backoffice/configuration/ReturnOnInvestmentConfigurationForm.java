//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\trunk_new\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.configuration;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(return on investment configuration)}
import java.io.Serializable;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'ReturnOnInvestmentConfigurationForm'.
 *
 * @author generator
 */
public class ReturnOnInvestmentConfigurationForm implements Serializable {

    public static final String MIN_ROI_EXPR = "returnOnInvestmentConfigurationForm.minRoi";
    public static final String MIN_ROI_PATH = "command.returnOnInvestmentConfigurationForm.minRoi";
    public static final String MAX_ROI_EXPR = "returnOnInvestmentConfigurationForm.maxRoi";
    public static final String MAX_ROI_PATH = "command.returnOnInvestmentConfigurationForm.maxRoi";
    public static final String BORDER_1_EXPR = "returnOnInvestmentConfigurationForm.border1";
    public static final String BORDER_1_PATH = "command.returnOnInvestmentConfigurationForm.border1";
    public static final String BORDER_2_EXPR = "returnOnInvestmentConfigurationForm.border2";
    public static final String BORDER_2_PATH = "command.returnOnInvestmentConfigurationForm.border2";
    public static final String BORDER_3_EXPR = "returnOnInvestmentConfigurationForm.border3";
    public static final String BORDER_3_PATH = "command.returnOnInvestmentConfigurationForm.border3";
    public static final String BORDER_4_EXPR = "returnOnInvestmentConfigurationForm.border4";
    public static final String BORDER_4_PATH = "command.returnOnInvestmentConfigurationForm.border4";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(return on investment configuration)}
    private static final long serialVersionUID = 2193759963515592430L;
// !!!!!!!! End of insert code section !!!!!!!!

    private Double _minRoi;
    private Double _minRoiInitVal;
    private boolean _minRoiIsSet;
    private Double _maxRoi;
    private Double _maxRoiInitVal;
    private boolean _maxRoiIsSet;
    private Double _border1;
    private Double _border1InitVal;
    private boolean _border1IsSet;
    private Double _border2;
    private Double _border2InitVal;
    private boolean _border2IsSet;
    private Double _border3;
    private Double _border3InitVal;
    private boolean _border3IsSet;
    private Double _border4;
    private Double _border4InitVal;
    private boolean _border4IsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setMinRoi(Double minRoi) {
        if (!_minRoiIsSet) {
            _minRoiIsSet = true;
            _minRoiInitVal = minRoi;
        }
        _minRoi = minRoi;
    }

    public Double getMinRoi() {
        return _minRoi;
    }

    public Double minRoiInitVal() {
        return _minRoiInitVal;
    }

	public boolean minRoiIsDirty() {
        return !valuesAreEqual(_minRoiInitVal, _minRoi);
    }

    public boolean minRoiIsSet() {
        return _minRoiIsSet;
    }	


    public void setMaxRoi(Double maxRoi) {
        if (!_maxRoiIsSet) {
            _maxRoiIsSet = true;
            _maxRoiInitVal = maxRoi;
        }
        _maxRoi = maxRoi;
    }

    public Double getMaxRoi() {
        return _maxRoi;
    }

    public Double maxRoiInitVal() {
        return _maxRoiInitVal;
    }

	public boolean maxRoiIsDirty() {
        return !valuesAreEqual(_maxRoiInitVal, _maxRoi);
    }

    public boolean maxRoiIsSet() {
        return _maxRoiIsSet;
    }	


    public void setBorder1(Double border1) {
        if (!_border1IsSet) {
            _border1IsSet = true;
            _border1InitVal = border1;
        }
        _border1 = border1;
    }

    public Double getBorder1() {
        return _border1;
    }

    public Double border1InitVal() {
        return _border1InitVal;
    }

	public boolean border1IsDirty() {
        return !valuesAreEqual(_border1InitVal, _border1);
    }

    public boolean border1IsSet() {
        return _border1IsSet;
    }	


    public void setBorder2(Double border2) {
        if (!_border2IsSet) {
            _border2IsSet = true;
            _border2InitVal = border2;
        }
        _border2 = border2;
    }

    public Double getBorder2() {
        return _border2;
    }

    public Double border2InitVal() {
        return _border2InitVal;
    }

	public boolean border2IsDirty() {
        return !valuesAreEqual(_border2InitVal, _border2);
    }

    public boolean border2IsSet() {
        return _border2IsSet;
    }	


    public void setBorder3(Double border3) {
        if (!_border3IsSet) {
            _border3IsSet = true;
            _border3InitVal = border3;
        }
        _border3 = border3;
    }

    public Double getBorder3() {
        return _border3;
    }

    public Double border3InitVal() {
        return _border3InitVal;
    }

	public boolean border3IsDirty() {
        return !valuesAreEqual(_border3InitVal, _border3);
    }

    public boolean border3IsSet() {
        return _border3IsSet;
    }	


    public void setBorder4(Double border4) {
        if (!_border4IsSet) {
            _border4IsSet = true;
            _border4InitVal = border4;
        }
        _border4 = border4;
    }

    public Double getBorder4() {
        return _border4;
    }

    public Double border4InitVal() {
        return _border4InitVal;
    }

	public boolean border4IsDirty() {
        return !valuesAreEqual(_border4InitVal, _border4);
    }

    public boolean border4IsSet() {
        return _border4IsSet;
    }	

}
