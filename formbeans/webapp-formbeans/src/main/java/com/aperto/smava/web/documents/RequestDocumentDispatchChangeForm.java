//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package com.aperto.smava.web.documents;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(request document dispatch change)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'RequestDocumentDispatchChangeForm'.
 *
 * @author generator
 */
public class RequestDocumentDispatchChangeForm implements Serializable {

    public static final String DOCUMENT_ID_EXPR = "requestDocumentDispatchChangeForm.documentId";
    public static final String DOCUMENT_ID_PATH = "command.requestDocumentDispatchChangeForm.documentId";
    public static final String DOCUMENT_CONTAINER_ID_EXPR = "requestDocumentDispatchChangeForm.documentContainerId";
    public static final String DOCUMENT_CONTAINER_ID_PATH = "command.requestDocumentDispatchChangeForm.documentContainerId";
    public static final String DOCUMENT_NAME_EXPR = "requestDocumentDispatchChangeForm.documentName";
    public static final String DOCUMENT_NAME_PATH = "command.requestDocumentDispatchChangeForm.documentName";
    public static final String DOCUMENT_CONTAINER_NAME_EXPR = "requestDocumentDispatchChangeForm.documentContainerName";
    public static final String DOCUMENT_CONTAINER_NAME_PATH = "command.requestDocumentDispatchChangeForm.documentContainerName";
    public static final String MAIL_DISPATCH_EXPR = "requestDocumentDispatchChangeForm.mailDispatch";
    public static final String MAIL_DISPATCH_PATH = "command.requestDocumentDispatchChangeForm.mailDispatch";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(request document dispatch change)}
// !!!!!!!! End of insert code section !!!!!!!!


    private Long _documentId;
    private Long _documentIdInitVal;
    private boolean _documentIdIsSet;

    private Long _documentContainerId;
    private Long _documentContainerIdInitVal;
    private boolean _documentContainerIdIsSet;

    private String _documentName;
    private String _documentNameInitVal;
    private boolean _documentNameIsSet;

    private String _documentContainerName;
    private String _documentContainerNameInitVal;
    private boolean _documentContainerNameIsSet;

    private boolean _mailDispatch;
    private boolean _mailDispatchInitVal;
    private boolean _mailDispatchIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setDocumentId(Long documentId) {
        if (!_documentIdIsSet) {
            _documentIdIsSet = true;
            _documentIdInitVal = documentId;
        }
        _documentId = documentId;
    }

    public Long getDocumentId() {
        return _documentId;
    }

    public Long documentIdInitVal() {
        return _documentIdInitVal;
    }

    public boolean documentIdIsDirty() {
        return !valuesAreEqual(_documentIdInitVal, _documentId);
    }

    public boolean documentIdIsSet() {
        return _documentIdIsSet;
    }

    public void setDocumentContainerId(Long documentContainerId) {
        if (!_documentContainerIdIsSet) {
            _documentContainerIdIsSet = true;
            _documentContainerIdInitVal = documentContainerId;
        }
        _documentContainerId = documentContainerId;
    }

    public Long getDocumentContainerId() {
        return _documentContainerId;
    }

    public Long documentContainerIdInitVal() {
        return _documentContainerIdInitVal;
    }

    public boolean documentContainerIdIsDirty() {
        return !valuesAreEqual(_documentContainerIdInitVal, _documentContainerId);
    }

    public boolean documentContainerIdIsSet() {
        return _documentContainerIdIsSet;
    }

    public void setDocumentName(String documentName) {
        if (!_documentNameIsSet) {
            _documentNameIsSet = true;
            _documentNameInitVal = documentName;
        }
        _documentName = documentName;
    }

    public String getDocumentName() {
        return _documentName;
    }

    public String documentNameInitVal() {
        return _documentNameInitVal;
    }

    public boolean documentNameIsDirty() {
        return !valuesAreEqual(_documentNameInitVal, _documentName);
    }

    public boolean documentNameIsSet() {
        return _documentNameIsSet;
    }

    public void setDocumentContainerName(String documentContainerName) {
        if (!_documentContainerNameIsSet) {
            _documentContainerNameIsSet = true;
            _documentContainerNameInitVal = documentContainerName;
        }
        _documentContainerName = documentContainerName;
    }

    public String getDocumentContainerName() {
        return _documentContainerName;
    }

    public String documentContainerNameInitVal() {
        return _documentContainerNameInitVal;
    }

    public boolean documentContainerNameIsDirty() {
        return !valuesAreEqual(_documentContainerNameInitVal, _documentContainerName);
    }

    public boolean documentContainerNameIsSet() {
        return _documentContainerNameIsSet;
    }

    public void setMailDispatch(boolean mailDispatch) {
        if (!_mailDispatchIsSet) {
            _mailDispatchIsSet = true;
            _mailDispatchInitVal = mailDispatch;
        }
        _mailDispatch = mailDispatch;
    }

    public boolean getMailDispatch() {
        return _mailDispatch;
    }

    public boolean mailDispatchInitVal() {
        return _mailDispatchInitVal;
    }

    public boolean mailDispatchIsDirty() {
        return !valuesAreEqual(_mailDispatchInitVal, _mailDispatch);
    }

    public boolean mailDispatchIsSet() {
        return _mailDispatchIsSet;
    }

}
