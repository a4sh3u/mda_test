//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.backoffice.bid;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo contract)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoContractForm'.
 *
 * @author generator
 */
public class BoContractForm {

    public static final String CONTRACT_NUMBER_EXPR = "boContractForm.contractNumber";
    public static final String CONTRACT_NUMBER_PATH = "command.boContractForm.contractNumber";
    public static final String CREATION_DATE_EXPR = "boContractForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boContractForm.creationDate";
    public static final String SEAL_DATE_EXPR = "boContractForm.sealDate";
    public static final String SEAL_DATE_PATH = "command.boContractForm.sealDate";
    public static final String START_DATE_EXPR = "boContractForm.startDate";
    public static final String START_DATE_PATH = "command.boContractForm.startDate";
    public static final String END_DATE_EXPR = "boContractForm.endDate";
    public static final String END_DATE_PATH = "command.boContractForm.endDate";
    public static final String AMOUNT_EXPR = "boContractForm.amount";
    public static final String AMOUNT_PATH = "command.boContractForm.amount";
    public static final String RATE_EXPR = "boContractForm.rate";
    public static final String RATE_PATH = "command.boContractForm.rate";
    public static final String MARKET_NAME_EXPR = "boContractForm.marketName";
    public static final String MARKET_NAME_PATH = "command.boContractForm.marketName";
    public static final String INTENDED_USE_EXPR = "boContractForm.intendedUse";
    public static final String INTENDED_USE_PATH = "command.boContractForm.intendedUse";
    public static final String EFFECTIVE_YIELD_BORROWER_EXPR = "boContractForm.effectiveYieldBorrower";
    public static final String EFFECTIVE_YIELD_BORROWER_PATH = "command.boContractForm.effectiveYieldBorrower";
    public static final String EFFECTIVE_YIELD_LENDER_EXPR = "boContractForm.effectiveYieldLender";
    public static final String EFFECTIVE_YIELD_LENDER_PATH = "command.boContractForm.effectiveYieldLender";
    public static final String TOTAL_CONTRACT_REPAYMENT_EXPR = "boContractForm.totalContractRepayment";
    public static final String TOTAL_CONTRACT_REPAYMENT_PATH = "command.boContractForm.totalContractRepayment";
    public static final String STATE_EXPR = "boContractForm.state";
    public static final String STATE_PATH = "command.boContractForm.state";
    public static final String LENDER_CHARGE_AMOUNT_EXPR = "boContractForm.lenderChargeAmount";
    public static final String LENDER_CHARGE_AMOUNT_PATH = "command.boContractForm.lenderChargeAmount";
    public static final String BORROWER_CHARGE_PERCENTAGE_EXPR = "boContractForm.borrowerChargePercentage";
    public static final String BORROWER_CHARGE_PERCENTAGE_PATH = "command.boContractForm.borrowerChargePercentage";
    public static final String BORROWER_CHARGE_AMOUNT_EXPR = "boContractForm.borrowerChargeAmount";
    public static final String BORROWER_CHARGE_AMOUNT_PATH = "command.boContractForm.borrowerChargeAmount";
    public static final String ENCASHMENT_RATE_EXPR = "boContractForm.encashmentRate";
    public static final String ENCASHMENT_RATE_PATH = "command.boContractForm.encashmentRate";
    public static final String UPDATED_ROI_EXPR = "boContractForm.updatedRoi";
    public static final String UPDATED_ROI_PATH = "command.boContractForm.updatedRoi";
    public static final String CAPITAL_VALUE_EXPR = "boContractForm.capitalValue";
    public static final String CAPITAL_VALUE_PATH = "command.boContractForm.capitalValue";
    public static final String REMAINING_MONTHS_EXPR = "boContractForm.remainingMonths";
    public static final String REMAINING_MONTHS_PATH = "command.boContractForm.remainingMonths";
    public static final String PAID_AMORTISATIONS_EXPR = "boContractForm.paidAmortisations";
    public static final String PAID_AMORTISATIONS_PATH = "command.boContractForm.paidAmortisations";
    public static final String EXPECTED_AMORTISATIONS_EXPR = "boContractForm.expectedAmortisations";
    public static final String EXPECTED_AMORTISATIONS_PATH = "command.boContractForm.expectedAmortisations";
    public static final String PAID_INTEREST_EXPR = "boContractForm.paidInterest";
    public static final String PAID_INTEREST_PATH = "command.boContractForm.paidInterest";
    public static final String EXPECTED_INTEREST_EXPR = "boContractForm.expectedInterest";
    public static final String EXPECTED_INTEREST_PATH = "command.boContractForm.expectedInterest";
    public static final String PAID_INSURANCE_EXPR = "boContractForm.paidInsurance";
    public static final String PAID_INSURANCE_PATH = "command.boContractForm.paidInsurance";
    public static final String EXPECTED_INSURANCE_EXPR = "boContractForm.expectedInsurance";
    public static final String EXPECTED_INSURANCE_PATH = "command.boContractForm.expectedInsurance";
    public static final String EXPECTED_ROI_EXPR = "boContractForm.expectedRoi";
    public static final String EXPECTED_ROI_PATH = "command.boContractForm.expectedRoi";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo contract)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _contractNumber;
    private String _contractNumberInitVal;
    private boolean _contractNumberIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _sealDate;
    private String _sealDateInitVal;
    private boolean _sealDateIsSet;
    private String _startDate;
    private String _startDateInitVal;
    private boolean _startDateIsSet;
    private String _endDate;
    private String _endDateInitVal;
    private boolean _endDateIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _rate;
    private String _rateInitVal;
    private boolean _rateIsSet;
    private String _marketName;
    private String _marketNameInitVal;
    private boolean _marketNameIsSet;
    private String _intendedUse;
    private String _intendedUseInitVal;
    private boolean _intendedUseIsSet;
    private String _effectiveYieldBorrower;
    private String _effectiveYieldBorrowerInitVal;
    private boolean _effectiveYieldBorrowerIsSet;
    private String _effectiveYieldLender;
    private String _effectiveYieldLenderInitVal;
    private boolean _effectiveYieldLenderIsSet;
    private String _totalContractRepayment;
    private String _totalContractRepaymentInitVal;
    private boolean _totalContractRepaymentIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _lenderChargeAmount;
    private String _lenderChargeAmountInitVal;
    private boolean _lenderChargeAmountIsSet;
    private String _borrowerChargePercentage;
    private String _borrowerChargePercentageInitVal;
    private boolean _borrowerChargePercentageIsSet;
    private String _borrowerChargeAmount;
    private String _borrowerChargeAmountInitVal;
    private boolean _borrowerChargeAmountIsSet;
    private Double _encashmentRate;
    private Double _encashmentRateInitVal;
    private boolean _encashmentRateIsSet;
    private Double _updatedRoi;
    private Double _updatedRoiInitVal;
    private boolean _updatedRoiIsSet;
    private Double _capitalValue;
    private Double _capitalValueInitVal;
    private boolean _capitalValueIsSet;
    private int _remainingMonths;
    private int _remainingMonthsInitVal;
    private boolean _remainingMonthsIsSet;
    private Double _paidAmortisations;
    private Double _paidAmortisationsInitVal;
    private boolean _paidAmortisationsIsSet;
    private Double _expectedAmortisations;
    private Double _expectedAmortisationsInitVal;
    private boolean _expectedAmortisationsIsSet;
    private Double _paidInterest;
    private Double _paidInterestInitVal;
    private boolean _paidInterestIsSet;
    private Double _expectedInterest;
    private Double _expectedInterestInitVal;
    private boolean _expectedInterestIsSet;
    private Double _paidInsurance;
    private Double _paidInsuranceInitVal;
    private boolean _paidInsuranceIsSet;
    private Double _expectedInsurance;
    private Double _expectedInsuranceInitVal;
    private boolean _expectedInsuranceIsSet;
    private Double _expectedRoi;
    private Double _expectedRoiInitVal;
    private boolean _expectedRoiIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setContractNumber(String contractNumber) {
        if (!_contractNumberIsSet) {
            _contractNumberIsSet = true;
            _contractNumberInitVal = contractNumber;
        }
        _contractNumber = contractNumber;
    }

    public String getContractNumber() {
        return _contractNumber;
    }

    public String contractNumberInitVal() {
        return _contractNumberInitVal;
    }

	public boolean contractNumberIsDirty() {
        return !valuesAreEqual(_contractNumberInitVal, _contractNumber);
    }

    public boolean contractNumberIsSet() {
        return _contractNumberIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setSealDate(String sealDate) {
        if (!_sealDateIsSet) {
            _sealDateIsSet = true;
            _sealDateInitVal = sealDate;
        }
        _sealDate = sealDate;
    }

    public String getSealDate() {
        return _sealDate;
    }

    public String sealDateInitVal() {
        return _sealDateInitVal;
    }

	public boolean sealDateIsDirty() {
        return !valuesAreEqual(_sealDateInitVal, _sealDate);
    }

    public boolean sealDateIsSet() {
        return _sealDateIsSet;
    }	


    public void setStartDate(String startDate) {
        if (!_startDateIsSet) {
            _startDateIsSet = true;
            _startDateInitVal = startDate;
        }
        _startDate = startDate;
    }

    public String getStartDate() {
        return _startDate;
    }

    public String startDateInitVal() {
        return _startDateInitVal;
    }

	public boolean startDateIsDirty() {
        return !valuesAreEqual(_startDateInitVal, _startDate);
    }

    public boolean startDateIsSet() {
        return _startDateIsSet;
    }	


    public void setEndDate(String endDate) {
        if (!_endDateIsSet) {
            _endDateIsSet = true;
            _endDateInitVal = endDate;
        }
        _endDate = endDate;
    }

    public String getEndDate() {
        return _endDate;
    }

    public String endDateInitVal() {
        return _endDateInitVal;
    }

	public boolean endDateIsDirty() {
        return !valuesAreEqual(_endDateInitVal, _endDate);
    }

    public boolean endDateIsSet() {
        return _endDateIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setRate(String rate) {
        if (!_rateIsSet) {
            _rateIsSet = true;
            _rateInitVal = rate;
        }
        _rate = rate;
    }

    public String getRate() {
        return _rate;
    }

    public String rateInitVal() {
        return _rateInitVal;
    }

	public boolean rateIsDirty() {
        return !valuesAreEqual(_rateInitVal, _rate);
    }

    public boolean rateIsSet() {
        return _rateIsSet;
    }	


    public void setMarketName(String marketName) {
        if (!_marketNameIsSet) {
            _marketNameIsSet = true;
            _marketNameInitVal = marketName;
        }
        _marketName = marketName;
    }

    public String getMarketName() {
        return _marketName;
    }

    public String marketNameInitVal() {
        return _marketNameInitVal;
    }

	public boolean marketNameIsDirty() {
        return !valuesAreEqual(_marketNameInitVal, _marketName);
    }

    public boolean marketNameIsSet() {
        return _marketNameIsSet;
    }	


    public void setIntendedUse(String intendedUse) {
        if (!_intendedUseIsSet) {
            _intendedUseIsSet = true;
            _intendedUseInitVal = intendedUse;
        }
        _intendedUse = intendedUse;
    }

    public String getIntendedUse() {
        return _intendedUse;
    }

    public String intendedUseInitVal() {
        return _intendedUseInitVal;
    }

	public boolean intendedUseIsDirty() {
        return !valuesAreEqual(_intendedUseInitVal, _intendedUse);
    }

    public boolean intendedUseIsSet() {
        return _intendedUseIsSet;
    }	


    public void setEffectiveYieldBorrower(String effectiveYieldBorrower) {
        if (!_effectiveYieldBorrowerIsSet) {
            _effectiveYieldBorrowerIsSet = true;
            _effectiveYieldBorrowerInitVal = effectiveYieldBorrower;
        }
        _effectiveYieldBorrower = effectiveYieldBorrower;
    }

    public String getEffectiveYieldBorrower() {
        return _effectiveYieldBorrower;
    }

    public String effectiveYieldBorrowerInitVal() {
        return _effectiveYieldBorrowerInitVal;
    }

	public boolean effectiveYieldBorrowerIsDirty() {
        return !valuesAreEqual(_effectiveYieldBorrowerInitVal, _effectiveYieldBorrower);
    }

    public boolean effectiveYieldBorrowerIsSet() {
        return _effectiveYieldBorrowerIsSet;
    }	


    public void setEffectiveYieldLender(String effectiveYieldLender) {
        if (!_effectiveYieldLenderIsSet) {
            _effectiveYieldLenderIsSet = true;
            _effectiveYieldLenderInitVal = effectiveYieldLender;
        }
        _effectiveYieldLender = effectiveYieldLender;
    }

    public String getEffectiveYieldLender() {
        return _effectiveYieldLender;
    }

    public String effectiveYieldLenderInitVal() {
        return _effectiveYieldLenderInitVal;
    }

	public boolean effectiveYieldLenderIsDirty() {
        return !valuesAreEqual(_effectiveYieldLenderInitVal, _effectiveYieldLender);
    }

    public boolean effectiveYieldLenderIsSet() {
        return _effectiveYieldLenderIsSet;
    }	


    public void setTotalContractRepayment(String totalContractRepayment) {
        if (!_totalContractRepaymentIsSet) {
            _totalContractRepaymentIsSet = true;
            _totalContractRepaymentInitVal = totalContractRepayment;
        }
        _totalContractRepayment = totalContractRepayment;
    }

    public String getTotalContractRepayment() {
        return _totalContractRepayment;
    }

    public String totalContractRepaymentInitVal() {
        return _totalContractRepaymentInitVal;
    }

	public boolean totalContractRepaymentIsDirty() {
        return !valuesAreEqual(_totalContractRepaymentInitVal, _totalContractRepayment);
    }

    public boolean totalContractRepaymentIsSet() {
        return _totalContractRepaymentIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setLenderChargeAmount(String lenderChargeAmount) {
        if (!_lenderChargeAmountIsSet) {
            _lenderChargeAmountIsSet = true;
            _lenderChargeAmountInitVal = lenderChargeAmount;
        }
        _lenderChargeAmount = lenderChargeAmount;
    }

    public String getLenderChargeAmount() {
        return _lenderChargeAmount;
    }

    public String lenderChargeAmountInitVal() {
        return _lenderChargeAmountInitVal;
    }

	public boolean lenderChargeAmountIsDirty() {
        return !valuesAreEqual(_lenderChargeAmountInitVal, _lenderChargeAmount);
    }

    public boolean lenderChargeAmountIsSet() {
        return _lenderChargeAmountIsSet;
    }	


    public void setBorrowerChargePercentage(String borrowerChargePercentage) {
        if (!_borrowerChargePercentageIsSet) {
            _borrowerChargePercentageIsSet = true;
            _borrowerChargePercentageInitVal = borrowerChargePercentage;
        }
        _borrowerChargePercentage = borrowerChargePercentage;
    }

    public String getBorrowerChargePercentage() {
        return _borrowerChargePercentage;
    }

    public String borrowerChargePercentageInitVal() {
        return _borrowerChargePercentageInitVal;
    }

	public boolean borrowerChargePercentageIsDirty() {
        return !valuesAreEqual(_borrowerChargePercentageInitVal, _borrowerChargePercentage);
    }

    public boolean borrowerChargePercentageIsSet() {
        return _borrowerChargePercentageIsSet;
    }	


    public void setBorrowerChargeAmount(String borrowerChargeAmount) {
        if (!_borrowerChargeAmountIsSet) {
            _borrowerChargeAmountIsSet = true;
            _borrowerChargeAmountInitVal = borrowerChargeAmount;
        }
        _borrowerChargeAmount = borrowerChargeAmount;
    }

    public String getBorrowerChargeAmount() {
        return _borrowerChargeAmount;
    }

    public String borrowerChargeAmountInitVal() {
        return _borrowerChargeAmountInitVal;
    }

	public boolean borrowerChargeAmountIsDirty() {
        return !valuesAreEqual(_borrowerChargeAmountInitVal, _borrowerChargeAmount);
    }

    public boolean borrowerChargeAmountIsSet() {
        return _borrowerChargeAmountIsSet;
    }	


    public void setEncashmentRate(Double encashmentRate) {
        if (!_encashmentRateIsSet) {
            _encashmentRateIsSet = true;
            _encashmentRateInitVal = encashmentRate;
        }
        _encashmentRate = encashmentRate;
    }

    public Double getEncashmentRate() {
        return _encashmentRate;
    }

    public Double encashmentRateInitVal() {
        return _encashmentRateInitVal;
    }

	public boolean encashmentRateIsDirty() {
        return !valuesAreEqual(_encashmentRateInitVal, _encashmentRate);
    }

    public boolean encashmentRateIsSet() {
        return _encashmentRateIsSet;
    }	


    public void setUpdatedRoi(Double updatedRoi) {
        if (!_updatedRoiIsSet) {
            _updatedRoiIsSet = true;
            _updatedRoiInitVal = updatedRoi;
        }
        _updatedRoi = updatedRoi;
    }

    public Double getUpdatedRoi() {
        return _updatedRoi;
    }

    public Double updatedRoiInitVal() {
        return _updatedRoiInitVal;
    }

	public boolean updatedRoiIsDirty() {
        return !valuesAreEqual(_updatedRoiInitVal, _updatedRoi);
    }

    public boolean updatedRoiIsSet() {
        return _updatedRoiIsSet;
    }	


    public void setCapitalValue(Double capitalValue) {
        if (!_capitalValueIsSet) {
            _capitalValueIsSet = true;
            _capitalValueInitVal = capitalValue;
        }
        _capitalValue = capitalValue;
    }

    public Double getCapitalValue() {
        return _capitalValue;
    }

    public Double capitalValueInitVal() {
        return _capitalValueInitVal;
    }

	public boolean capitalValueIsDirty() {
        return !valuesAreEqual(_capitalValueInitVal, _capitalValue);
    }

    public boolean capitalValueIsSet() {
        return _capitalValueIsSet;
    }	


    public void setRemainingMonths(int remainingMonths) {
        if (!_remainingMonthsIsSet) {
            _remainingMonthsIsSet = true;
            _remainingMonthsInitVal = remainingMonths;
        }
        _remainingMonths = remainingMonths;
    }

    public int getRemainingMonths() {
        return _remainingMonths;
    }

    public int remainingMonthsInitVal() {
        return _remainingMonthsInitVal;
    }

	public boolean remainingMonthsIsDirty() {
        return !valuesAreEqual(_remainingMonthsInitVal, _remainingMonths);
    }

    public boolean remainingMonthsIsSet() {
        return _remainingMonthsIsSet;
    }	


    public void setPaidAmortisations(Double paidAmortisations) {
        if (!_paidAmortisationsIsSet) {
            _paidAmortisationsIsSet = true;
            _paidAmortisationsInitVal = paidAmortisations;
        }
        _paidAmortisations = paidAmortisations;
    }

    public Double getPaidAmortisations() {
        return _paidAmortisations;
    }

    public Double paidAmortisationsInitVal() {
        return _paidAmortisationsInitVal;
    }

	public boolean paidAmortisationsIsDirty() {
        return !valuesAreEqual(_paidAmortisationsInitVal, _paidAmortisations);
    }

    public boolean paidAmortisationsIsSet() {
        return _paidAmortisationsIsSet;
    }	


    public void setExpectedAmortisations(Double expectedAmortisations) {
        if (!_expectedAmortisationsIsSet) {
            _expectedAmortisationsIsSet = true;
            _expectedAmortisationsInitVal = expectedAmortisations;
        }
        _expectedAmortisations = expectedAmortisations;
    }

    public Double getExpectedAmortisations() {
        return _expectedAmortisations;
    }

    public Double expectedAmortisationsInitVal() {
        return _expectedAmortisationsInitVal;
    }

	public boolean expectedAmortisationsIsDirty() {
        return !valuesAreEqual(_expectedAmortisationsInitVal, _expectedAmortisations);
    }

    public boolean expectedAmortisationsIsSet() {
        return _expectedAmortisationsIsSet;
    }	


    public void setPaidInterest(Double paidInterest) {
        if (!_paidInterestIsSet) {
            _paidInterestIsSet = true;
            _paidInterestInitVal = paidInterest;
        }
        _paidInterest = paidInterest;
    }

    public Double getPaidInterest() {
        return _paidInterest;
    }

    public Double paidInterestInitVal() {
        return _paidInterestInitVal;
    }

	public boolean paidInterestIsDirty() {
        return !valuesAreEqual(_paidInterestInitVal, _paidInterest);
    }

    public boolean paidInterestIsSet() {
        return _paidInterestIsSet;
    }	


    public void setExpectedInterest(Double expectedInterest) {
        if (!_expectedInterestIsSet) {
            _expectedInterestIsSet = true;
            _expectedInterestInitVal = expectedInterest;
        }
        _expectedInterest = expectedInterest;
    }

    public Double getExpectedInterest() {
        return _expectedInterest;
    }

    public Double expectedInterestInitVal() {
        return _expectedInterestInitVal;
    }

	public boolean expectedInterestIsDirty() {
        return !valuesAreEqual(_expectedInterestInitVal, _expectedInterest);
    }

    public boolean expectedInterestIsSet() {
        return _expectedInterestIsSet;
    }	


    public void setPaidInsurance(Double paidInsurance) {
        if (!_paidInsuranceIsSet) {
            _paidInsuranceIsSet = true;
            _paidInsuranceInitVal = paidInsurance;
        }
        _paidInsurance = paidInsurance;
    }

    public Double getPaidInsurance() {
        return _paidInsurance;
    }

    public Double paidInsuranceInitVal() {
        return _paidInsuranceInitVal;
    }

	public boolean paidInsuranceIsDirty() {
        return !valuesAreEqual(_paidInsuranceInitVal, _paidInsurance);
    }

    public boolean paidInsuranceIsSet() {
        return _paidInsuranceIsSet;
    }	


    public void setExpectedInsurance(Double expectedInsurance) {
        if (!_expectedInsuranceIsSet) {
            _expectedInsuranceIsSet = true;
            _expectedInsuranceInitVal = expectedInsurance;
        }
        _expectedInsurance = expectedInsurance;
    }

    public Double getExpectedInsurance() {
        return _expectedInsurance;
    }

    public Double expectedInsuranceInitVal() {
        return _expectedInsuranceInitVal;
    }

	public boolean expectedInsuranceIsDirty() {
        return !valuesAreEqual(_expectedInsuranceInitVal, _expectedInsurance);
    }

    public boolean expectedInsuranceIsSet() {
        return _expectedInsuranceIsSet;
    }	


    public void setExpectedRoi(Double expectedRoi) {
        if (!_expectedRoiIsSet) {
            _expectedRoiIsSet = true;
            _expectedRoiInitVal = expectedRoi;
        }
        _expectedRoi = expectedRoi;
    }

    public Double getExpectedRoi() {
        return _expectedRoi;
    }

    public Double expectedRoiInitVal() {
        return _expectedRoiInitVal;
    }

	public boolean expectedRoiIsDirty() {
        return !valuesAreEqual(_expectedRoiInitVal, _expectedRoi);
    }

    public boolean expectedRoiIsSet() {
        return _expectedRoiIsSet;
    }	

}
