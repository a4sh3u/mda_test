//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\wks\maven2\smava-webapp-parent\account\src\main\config\mda\model.xml
//
//    Or you can change the template:
//
//    C:\wks\maven2\smava-webapp-parent\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.order.web;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(order)}

import de.smava.webapp.commons.web.ImageAwareCommand;

import java.io.IOException;
import java.io.Serializable;
    

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'OrderForm'.
 *
 * @author generator
 */
public class OrderForm implements Serializable, ImageAwareCommand {

    public static final String CREDIT_AMOUNT_EXPR = "orderForm.creditAmount";
    public static final String CREDIT_AMOUNT_PATH = "command.orderForm.creditAmount";
    public static final String CREDIT_TERM_EXPR = "orderForm.creditTerm";
    public static final String CREDIT_TERM_PATH = "command.orderForm.creditTerm";
    public static final String CREDIT_INTEREST_EXPR = "orderForm.creditInterest";
    public static final String CREDIT_INTEREST_PATH = "command.orderForm.creditInterest";
    public static final String RDI_CONTRACT_TYPE_EXPR = "orderForm.rdiContractType";
    public static final String RDI_CONTRACT_TYPE_PATH = "command.orderForm.rdiContractType";
    public static final String MAIN_CATEGORY_EXPR = "orderForm.mainCategory";
    public static final String MAIN_CATEGORY_PATH = "command.orderForm.mainCategory";
    public static final String TITLE_EXPR = "orderForm.title";
    public static final String TITLE_PATH = "command.orderForm.title";
    public static final String DESCRIPTION_EXPR = "orderForm.description";
    public static final String DESCRIPTION_PATH = "command.orderForm.description";
    public static final String IMAGE_EXPR = "orderForm.image";
    public static final String IMAGE_PATH = "command.orderForm.image";
    public static final String IMAGEFILE_EXPR = "orderForm.imagefile";
    public static final String IMAGEFILE_PATH = "command.orderForm.imagefile";
    public static final String NO_IMAGE_EXPR = "orderForm.noImage";
    public static final String NO_IMAGE_PATH = "command.orderForm.noImage";
    public static final String IS_NEW_UPLOAD_EXPR = "orderForm.isNewUpload";
    public static final String IS_NEW_UPLOAD_PATH = "command.orderForm.isNewUpload";
    public static final String IMAGE_HEIGHT_EXPR = "orderForm.imageHeight";
    public static final String IMAGE_HEIGHT_PATH = "command.orderForm.imageHeight";
    public static final String IMAGE_WIDTH_EXPR = "orderForm.imageWidth";
    public static final String IMAGE_WIDTH_PATH = "command.orderForm.imageWidth";
    public static final String PREVIOUS_CREDIT_INTEREST_EXPR = "orderForm.previousCreditInterest";
    public static final String PREVIOUS_CREDIT_INTEREST_PATH = "command.orderForm.previousCreditInterest";
    public static final String LAST_UPDATE_DATE_EXPR = "orderForm.lastUpdateDate";
    public static final String LAST_UPDATE_DATE_PATH = "command.orderForm.lastUpdateDate";
    public static final String RDI_TERMS_AND_CONDITIONS_EXPR = "orderForm.rdiTermsAndConditions";
    public static final String RDI_TERMS_AND_CONDITIONS_PATH = "command.orderForm.rdiTermsAndConditions";
    public static final String TERMS_AND_CONDITIONS_EXPR = "orderForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_PATH = "command.orderForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_APPLIED_EXPR = "orderForm.termsAndConditionsApplied";
    public static final String TERMS_AND_CONDITIONS_APPLIED_PATH = "command.orderForm.termsAndConditionsApplied";
    public static final String SHOW_PAYMENT_PROFILE_EXPR = "orderForm.showPaymentProfile";
    public static final String SHOW_PAYMENT_PROFILE_PATH = "command.orderForm.showPaymentProfile";
    public static final String POLISH_MONEY_TRANSFER_EXPR = "orderForm.polishMoneyTransfer";
    public static final String POLISH_MONEY_TRANSFER_PATH = "command.orderForm.polishMoneyTransfer";
    public static final String POLISH_TAX_LAW_EXPR = "orderForm.polishTaxLaw";
    public static final String POLISH_TAX_LAW_PATH = "command.orderForm.polishTaxLaw";
    public static final String SCHUFA_EXPR = "orderForm.schufa";
    public static final String SCHUFA_PATH = "command.orderForm.schufa";
    public static final String NEGATIVE_SCHUFA_EXPR = "orderForm.negativeSchufa";
    public static final String NEGATIVE_SCHUFA_PATH = "command.orderForm.negativeSchufa";
    public static final String USE_PRE_MATCH_EXPR = "orderForm.usePreMatch";
    public static final String USE_PRE_MATCH_PATH = "command.orderForm.usePreMatch";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(order)}
    
    public static final String MONTLY_RATE_EXPR = "orderForm.monthlyRate";
    public static final String MONTLY_RATE_PATH = "command.orderForm.monthlyRate";
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private byte[] _imageBytes = null;

    public byte[] getImageBytes() {
        if (_imageBytes == null && _imagefile != null && !_imagefile.isEmpty()) {
        	try {
        		_imageBytes = _imagefile.getBytes();
        	} catch (IOException e) {
        		_imageBytes = null;
        	}
        }

        byte[] imageBytes = null;
        if (_imageBytes != null) {
            imageBytes = _imageBytes.clone();
        }
        return imageBytes;
    }

    public void setImageBytes(byte[] bytes) {
        if (bytes == null) {
            _imageBytes = null;
        } else {
            _imageBytes = bytes.clone();
        }
    }
    
	public byte[] getFile() {
		return getImageBytes();
	}

	public String getImageCategory() {
		return IMAGE_CATEGORY_ORDER;
	}

	public String getImageFileExpression() {
		return IMAGE_EXPR;
	}

	public String getNoImageExpression() {
		return CreateOrderForm.NOIMAGE_EXPR;
	}

	public void resetForm() {
		setImageBytes(null);
        setImagefile(null);		
	}

	public void setNewUpload(boolean isNewUpload) {
		setIsNewUpload(isNewUpload);		
	}
	
	public boolean isNewUpload() {
		return Boolean.TRUE.equals(getIsNewUpload());
	}

	public void updateNoImage() {
		setImage(null);
        setNoImage(false);
		
	}    
	
	public boolean isDeleteImage() {
		return Boolean.TRUE.equals(getNoImage());
	}

	public void setDeleteImage(boolean deleteImage) {
		setNoImage(deleteImage);
	}
    
    
    private String _monthlyRate;
    private String _monthlyRateInitVal;
    private boolean _monthlyRateIsSet;
	    
	 public void setMonthlyRate(String monthlyRate) {
	        if (!_monthlyRateIsSet) {
	        	_monthlyRateIsSet = true;
	            _monthlyRateInitVal = monthlyRate;
	        }
	        _monthlyRate = monthlyRate;
	    }

	    public String getMonthlyRate() {
	        return _monthlyRate;
	    }

	    public String monthlyRateInitVal() {
	        return _monthlyRateInitVal;
	    }

		public boolean monthlyRateIsDirty() {
	        return !valuesAreEqual(_monthlyRateInitVal, _monthlyRate);
	    }

	    public boolean monthlyRateIsSet() {
	        return _monthlyRateIsSet;
	    }	

// !!!!!!!! End of insert code section !!!!!!!!

    private String _creditAmount;
    private String _creditAmountInitVal;
    private boolean _creditAmountIsSet;
    private String _creditTerm;
    private String _creditTermInitVal;
    private boolean _creditTermIsSet;
    private String _creditInterest;
    private String _creditInterestInitVal;
    private boolean _creditInterestIsSet;
    private String _rdiContractType;
    private String _rdiContractTypeInitVal;
    private boolean _rdiContractTypeIsSet;
    private Long _mainCategory;
    private Long _mainCategoryInitVal;
    private boolean _mainCategoryIsSet;
    private String _title;
    private String _titleInitVal;
    private boolean _titleIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private transient org.springframework.web.multipart.MultipartFile _imagefile;
    private transient org.springframework.web.multipart.MultipartFile _imagefileInitVal;
    private transient boolean _imagefileIsSet;
    private Boolean _noImage;
    private Boolean _noImageInitVal;
    private boolean _noImageIsSet;
    private Boolean _isNewUpload;
    private Boolean _isNewUploadInitVal;
    private boolean _isNewUploadIsSet;
    private int _imageHeight;
    private int _imageHeightInitVal;
    private boolean _imageHeightIsSet;
    private int _imageWidth;
    private int _imageWidthInitVal;
    private boolean _imageWidthIsSet;
    private String _previousCreditInterest;
    private String _previousCreditInterestInitVal;
    private boolean _previousCreditInterestIsSet;
    private Long _lastUpdateDate;
    private Long _lastUpdateDateInitVal;
    private boolean _lastUpdateDateIsSet;
    private Boolean _rdiTermsAndConditions;
    private Boolean _rdiTermsAndConditionsInitVal;
    private boolean _rdiTermsAndConditionsIsSet;
    private Boolean _termsAndConditions;
    private Boolean _termsAndConditionsInitVal;
    private boolean _termsAndConditionsIsSet;
    private Boolean _termsAndConditionsApplied;
    private Boolean _termsAndConditionsAppliedInitVal;
    private boolean _termsAndConditionsAppliedIsSet;
    private Boolean _showPaymentProfile;
    private Boolean _showPaymentProfileInitVal;
    private boolean _showPaymentProfileIsSet;
    private Boolean _polishMoneyTransfer;
    private Boolean _polishMoneyTransferInitVal;
    private boolean _polishMoneyTransferIsSet;
    private Boolean _polishTaxLaw;
    private Boolean _polishTaxLawInitVal;
    private boolean _polishTaxLawIsSet;
    private Boolean _schufa;
    private Boolean _schufaInitVal;
    private boolean _schufaIsSet;
    private String _negativeSchufa;
    private String _negativeSchufaInitVal;
    private boolean _negativeSchufaIsSet;
    private Boolean _usePreMatch;
    private Boolean _usePreMatchInitVal;
    private boolean _usePreMatchIsSet;

 

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setCreditAmount(String creditAmount) {
        if (!_creditAmountIsSet) {
            _creditAmountIsSet = true;
            _creditAmountInitVal = creditAmount;
        }
        _creditAmount = creditAmount;
    }

    public String getCreditAmount() {
        return _creditAmount;
    }

    public String creditAmountInitVal() {
        return _creditAmountInitVal;
    }

	public boolean creditAmountIsDirty() {
        return !valuesAreEqual(_creditAmountInitVal, _creditAmount);
    }

    public boolean creditAmountIsSet() {
        return _creditAmountIsSet;
    }	


    public void setCreditTerm(String creditTerm) {
        if (!_creditTermIsSet) {
            _creditTermIsSet = true;
            _creditTermInitVal = creditTerm;
        }
        _creditTerm = creditTerm;
    }

    public String getCreditTerm() {
        return _creditTerm;
    }

    public String creditTermInitVal() {
        return _creditTermInitVal;
    }

	public boolean creditTermIsDirty() {
        return !valuesAreEqual(_creditTermInitVal, _creditTerm);
    }

    public boolean creditTermIsSet() {
        return _creditTermIsSet;
    }	


    public void setCreditInterest(String creditInterest) {
        if (!_creditInterestIsSet) {
            _creditInterestIsSet = true;
            _creditInterestInitVal = creditInterest;
        }
        _creditInterest = creditInterest;
    }

    public String getCreditInterest() {
        return _creditInterest;
    }

    public String creditInterestInitVal() {
        return _creditInterestInitVal;
    }

	public boolean creditInterestIsDirty() {
        return !valuesAreEqual(_creditInterestInitVal, _creditInterest);
    }

    public boolean creditInterestIsSet() {
        return _creditInterestIsSet;
    }	


    public void setRdiContractType(String rdiContractType) {
        if (!_rdiContractTypeIsSet) {
            _rdiContractTypeIsSet = true;
            _rdiContractTypeInitVal = rdiContractType;
        }
        _rdiContractType = rdiContractType;
    }

    public String getRdiContractType() {
        return _rdiContractType;
    }

    public String rdiContractTypeInitVal() {
        return _rdiContractTypeInitVal;
    }

	public boolean rdiContractTypeIsDirty() {
        return !valuesAreEqual(_rdiContractTypeInitVal, _rdiContractType);
    }

    public boolean rdiContractTypeIsSet() {
        return _rdiContractTypeIsSet;
    }	


    public void setMainCategory(Long mainCategory) {
        if (!_mainCategoryIsSet) {
            _mainCategoryIsSet = true;
            _mainCategoryInitVal = mainCategory;
        }
        _mainCategory = mainCategory;
    }

    public Long getMainCategory() {
        return _mainCategory;
    }

    public Long mainCategoryInitVal() {
        return _mainCategoryInitVal;
    }

	public boolean mainCategoryIsDirty() {
        return !valuesAreEqual(_mainCategoryInitVal, _mainCategory);
    }

    public boolean mainCategoryIsSet() {
        return _mainCategoryIsSet;
    }	

    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = title;
        }
        _title = title;
    }

    public String getTitle() {
        return _title;
    }

    public String titleInitVal() {
        return _titleInitVal;
    }

	public boolean titleIsDirty() {
        return !valuesAreEqual(_titleInitVal, _title);
    }

    public boolean titleIsSet() {
        return _titleIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setImagefile(org.springframework.web.multipart.MultipartFile imagefile) {
        if (!_imagefileIsSet) {
            _imagefileIsSet = true;
            _imagefileInitVal = imagefile;
        }
        _imagefile = imagefile;
    }

    public org.springframework.web.multipart.MultipartFile getImagefile() {
        return _imagefile;
    }

    public org.springframework.web.multipart.MultipartFile imagefileInitVal() {
        return _imagefileInitVal;
    }

	public boolean imagefileIsDirty() {
        return !valuesAreEqual(_imagefileInitVal, _imagefile);
    }

    public boolean imagefileIsSet() {
        return _imagefileIsSet;
    }	


    public void setNoImage(Boolean noImage) {
        if (!_noImageIsSet) {
            _noImageIsSet = true;
            _noImageInitVal = noImage;
        }
        _noImage = noImage;
    }

    public Boolean getNoImage() {
        return _noImage;
    }

    public Boolean noImageInitVal() {
        return _noImageInitVal;
    }

	public boolean noImageIsDirty() {
        return !valuesAreEqual(_noImageInitVal, _noImage);
    }

    public boolean noImageIsSet() {
        return _noImageIsSet;
    }	


    public void setIsNewUpload(Boolean isNewUpload) {
        if (!_isNewUploadIsSet) {
            _isNewUploadIsSet = true;
            _isNewUploadInitVal = isNewUpload;
        }
        _isNewUpload = isNewUpload;
    }

    public Boolean getIsNewUpload() {
        return _isNewUpload;
    }

    public Boolean isNewUploadInitVal() {
        return _isNewUploadInitVal;
    }

	public boolean isNewUploadIsDirty() {
        return !valuesAreEqual(_isNewUploadInitVal, _isNewUpload);
    }

    public boolean isNewUploadIsSet() {
        return _isNewUploadIsSet;
    }	


    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = imageHeight;
        }
        _imageHeight = imageHeight;
    }

    public int getImageHeight() {
        return _imageHeight;
    }

    public int imageHeightInitVal() {
        return _imageHeightInitVal;
    }

	public boolean imageHeightIsDirty() {
        return !valuesAreEqual(_imageHeightInitVal, _imageHeight);
    }

    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }	


    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = imageWidth;
        }
        _imageWidth = imageWidth;
    }

    public int getImageWidth() {
        return _imageWidth;
    }

    public int imageWidthInitVal() {
        return _imageWidthInitVal;
    }

	public boolean imageWidthIsDirty() {
        return !valuesAreEqual(_imageWidthInitVal, _imageWidth);
    }

    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }	


    public void setPreviousCreditInterest(String previousCreditInterest) {
        if (!_previousCreditInterestIsSet) {
            _previousCreditInterestIsSet = true;
            _previousCreditInterestInitVal = previousCreditInterest;
        }
        _previousCreditInterest = previousCreditInterest;
    }

    public String getPreviousCreditInterest() {
        return _previousCreditInterest;
    }

    public String previousCreditInterestInitVal() {
        return _previousCreditInterestInitVal;
    }

	public boolean previousCreditInterestIsDirty() {
        return !valuesAreEqual(_previousCreditInterestInitVal, _previousCreditInterest);
    }

    public boolean previousCreditInterestIsSet() {
        return _previousCreditInterestIsSet;
    }	


    public void setLastUpdateDate(Long lastUpdateDate) {
        if (!_lastUpdateDateIsSet) {
            _lastUpdateDateIsSet = true;
            _lastUpdateDateInitVal = lastUpdateDate;
        }
        _lastUpdateDate = lastUpdateDate;
    }

    public Long getLastUpdateDate() {
        return _lastUpdateDate;
    }

    public Long lastUpdateDateInitVal() {
        return _lastUpdateDateInitVal;
    }

	public boolean lastUpdateDateIsDirty() {
        return !valuesAreEqual(_lastUpdateDateInitVal, _lastUpdateDate);
    }

    public boolean lastUpdateDateIsSet() {
        return _lastUpdateDateIsSet;
    }	


    public void setRdiTermsAndConditions(Boolean rdiTermsAndConditions) {
        if (!_rdiTermsAndConditionsIsSet) {
            _rdiTermsAndConditionsIsSet = true;
            _rdiTermsAndConditionsInitVal = rdiTermsAndConditions;
        }
        _rdiTermsAndConditions = rdiTermsAndConditions;
    }

    public Boolean getRdiTermsAndConditions() {
        return _rdiTermsAndConditions;
    }

    public Boolean rdiTermsAndConditionsInitVal() {
        return _rdiTermsAndConditionsInitVal;
    }

	public boolean rdiTermsAndConditionsIsDirty() {
        return !valuesAreEqual(_rdiTermsAndConditionsInitVal, _rdiTermsAndConditions);
    }

    public boolean rdiTermsAndConditionsIsSet() {
        return _rdiTermsAndConditionsIsSet;
    }	


    public void setTermsAndConditions(Boolean termsAndConditions) {
        if (!_termsAndConditionsIsSet) {
            _termsAndConditionsIsSet = true;
            _termsAndConditionsInitVal = termsAndConditions;
        }
        _termsAndConditions = termsAndConditions;
    }

    public Boolean getTermsAndConditions() {
        return _termsAndConditions;
    }

    public Boolean termsAndConditionsInitVal() {
        return _termsAndConditionsInitVal;
    }

	public boolean termsAndConditionsIsDirty() {
        return !valuesAreEqual(_termsAndConditionsInitVal, _termsAndConditions);
    }

    public boolean termsAndConditionsIsSet() {
        return _termsAndConditionsIsSet;
    }	


    public void setTermsAndConditionsApplied(Boolean termsAndConditionsApplied) {
        if (!_termsAndConditionsAppliedIsSet) {
            _termsAndConditionsAppliedIsSet = true;
            _termsAndConditionsAppliedInitVal = termsAndConditionsApplied;
        }
        _termsAndConditionsApplied = termsAndConditionsApplied;
    }

    public Boolean getTermsAndConditionsApplied() {
        return _termsAndConditionsApplied;
    }

    public Boolean termsAndConditionsAppliedInitVal() {
        return _termsAndConditionsAppliedInitVal;
    }

	public boolean termsAndConditionsAppliedIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAppliedInitVal, _termsAndConditionsApplied);
    }

    public boolean termsAndConditionsAppliedIsSet() {
        return _termsAndConditionsAppliedIsSet;
    }	


    public void setShowPaymentProfile(Boolean showPaymentProfile) {
        if (!_showPaymentProfileIsSet) {
            _showPaymentProfileIsSet = true;
            _showPaymentProfileInitVal = showPaymentProfile;
        }
        _showPaymentProfile = showPaymentProfile;
    }

    public Boolean getShowPaymentProfile() {
        return _showPaymentProfile;
    }

    public Boolean showPaymentProfileInitVal() {
        return _showPaymentProfileInitVal;
    }

	public boolean showPaymentProfileIsDirty() {
        return !valuesAreEqual(_showPaymentProfileInitVal, _showPaymentProfile);
    }

    public boolean showPaymentProfileIsSet() {
        return _showPaymentProfileIsSet;
    }	


    public void setPolishMoneyTransfer(Boolean polishMoneyTransfer) {
        if (!_polishMoneyTransferIsSet) {
            _polishMoneyTransferIsSet = true;
            _polishMoneyTransferInitVal = polishMoneyTransfer;
        }
        _polishMoneyTransfer = polishMoneyTransfer;
    }

    public Boolean getPolishMoneyTransfer() {
        return _polishMoneyTransfer;
    }

    public Boolean polishMoneyTransferInitVal() {
        return _polishMoneyTransferInitVal;
    }

	public boolean polishMoneyTransferIsDirty() {
        return !valuesAreEqual(_polishMoneyTransferInitVal, _polishMoneyTransfer);
    }

    public boolean polishMoneyTransferIsSet() {
        return _polishMoneyTransferIsSet;
    }	


    public void setPolishTaxLaw(Boolean polishTaxLaw) {
        if (!_polishTaxLawIsSet) {
            _polishTaxLawIsSet = true;
            _polishTaxLawInitVal = polishTaxLaw;
        }
        _polishTaxLaw = polishTaxLaw;
    }

    public Boolean getPolishTaxLaw() {
        return _polishTaxLaw;
    }

    public Boolean polishTaxLawInitVal() {
        return _polishTaxLawInitVal;
    }

	public boolean polishTaxLawIsDirty() {
        return !valuesAreEqual(_polishTaxLawInitVal, _polishTaxLaw);
    }

    public boolean polishTaxLawIsSet() {
        return _polishTaxLawIsSet;
    }	


    public void setSchufa(Boolean schufa) {
        if (!_schufaIsSet) {
            _schufaIsSet = true;
            _schufaInitVal = schufa;
        }
        _schufa = schufa;
    }

    public Boolean getSchufa() {
        return _schufa;
    }

    public Boolean schufaInitVal() {
        return _schufaInitVal;
    }

	public boolean schufaIsDirty() {
        return !valuesAreEqual(_schufaInitVal, _schufa);
    }

    public boolean schufaIsSet() {
        return _schufaIsSet;
    }	


    public void setNegativeSchufa(String negativeSchufa) {
        if (!_negativeSchufaIsSet) {
            _negativeSchufaIsSet = true;
            _negativeSchufaInitVal = negativeSchufa;
        }
        _negativeSchufa = negativeSchufa;
    }

    public String getNegativeSchufa() {
        return _negativeSchufa;
    }

    public String negativeSchufaInitVal() {
        return _negativeSchufaInitVal;
    }

	public boolean negativeSchufaIsDirty() {
        return !valuesAreEqual(_negativeSchufaInitVal, _negativeSchufa);
    }

    public boolean negativeSchufaIsSet() {
        return _negativeSchufaIsSet;
    }	


    public void setUsePreMatch(Boolean usePreMatch) {
        if (!_usePreMatchIsSet) {
            _usePreMatchIsSet = true;
            _usePreMatchInitVal = usePreMatch;
        }
        _usePreMatch = usePreMatch;
    }

    public Boolean getUsePreMatch() {
        return _usePreMatch;
    }

    public Boolean usePreMatchInitVal() {
        return _usePreMatchInitVal;
    }

	public boolean usePreMatchIsDirty() {
        return !valuesAreEqual(_usePreMatchInitVal, _usePreMatch);
    }

    public boolean usePreMatchIsSet() {
        return _usePreMatchIsSet;
    }	

}
