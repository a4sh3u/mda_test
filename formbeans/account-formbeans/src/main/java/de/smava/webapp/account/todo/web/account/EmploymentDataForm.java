//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.account;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(employment data)}

import de.smava.webapp.account.domain.Sector;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'EmploymentDataForm'.
 *
 * @author generator
 */
public class EmploymentDataForm implements Serializable {

    public static final String OCCUPATION_EXPR = "employmentDataForm.occupation";
    public static final String OCCUPATION_PATH = "command.employmentDataForm.occupation";
    public static final String EMPLOYMENT_EXPR = "employmentDataForm.employment";
    public static final String EMPLOYMENT_PATH = "command.employmentDataForm.employment";
    public static final String START_MONTH_OF_OCCUPATION_EXPR = "employmentDataForm.startMonthOfOccupation";
    public static final String START_MONTH_OF_OCCUPATION_PATH = "command.employmentDataForm.startMonthOfOccupation";
    public static final String START_YEAR_OF_OCCUPATION_EXPR = "employmentDataForm.startYearOfOccupation";
    public static final String START_YEAR_OF_OCCUPATION_PATH = "command.employmentDataForm.startYearOfOccupation";
    public static final String SECTOR_NAME_EXPR = "employmentDataForm.sectorName";
    public static final String SECTOR_NAME_PATH = "command.employmentDataForm.sectorName";
    public static final String SECTOR_FILTERED_EXPR = "employmentDataForm.sectorFiltered";
    public static final String SECTOR_FILTERED_PATH = "command.employmentDataForm.sectorFiltered";
    public static final String SECTOR_COMPOSITE_EXPR = "employmentDataForm.sectorComposite";
    public static final String SECTOR_COMPOSITE_PATH = "command.employmentDataForm.sectorComposite";
    public static final String ROOT_SECTORS_EXPR = "employmentDataForm.rootSectors";
    public static final String ROOT_SECTORS_PATH = "command.employmentDataForm.rootSectors";
    public static final String SECTOR_DESCRIPTION_EXPR = "employmentDataForm.sectorDescription";
    public static final String SECTOR_DESCRIPTION_PATH = "command.employmentDataForm.sectorDescription";
    public static final String OTHER_EARN_DESCRIPTION_EXPR = "employmentDataForm.otherEarnDescription";
    public static final String OTHER_EARN_DESCRIPTION_PATH = "command.employmentDataForm.otherEarnDescription";
    public static final String TEMP_EMPLOYMENT_EXPR = "employmentDataForm.tempEmployment";
    public static final String TEMP_EMPLOYMENT_PATH = "command.employmentDataForm.tempEmployment";
    public static final String COUNTRY_EXPR = "employmentDataForm.country";
    public static final String COUNTRY_PATH = "command.employmentDataForm.country";
    public static final String EMPLOYER_NAME_EXPR = "employmentDataForm.employerName";
    public static final String EMPLOYER_NAME_PATH = "command.employmentDataForm.employerName";
    public static final String CO_BORROWER_EMPLOYER_NAME_EXPR = "employmentDataForm.coBorrowerEmployerName";
    public static final String CO_BORROWER_EMPLOYER_NAME_PATH = "command.employmentDataForm.coBorrowerEmployerName";
    public static final String PHONE_EMPLOYER_EXPR = "employmentDataForm.phoneEmployer";
    public static final String PHONE_EMPLOYER_PATH = "command.employmentDataForm.phoneEmployer";
    public static final String PHONE_CODE_EMPLOYER_EXPR = "employmentDataForm.phoneCodeEmployer";
    public static final String PHONE_CODE_EMPLOYER_PATH = "command.employmentDataForm.phoneCodeEmployer";
    public static final String PHONE_EMPLOYER_CO_BORROWER_EXPR = "employmentDataForm.phoneEmployerCoBorrower";
    public static final String PHONE_EMPLOYER_CO_BORROWER_PATH = "command.employmentDataForm.phoneEmployerCoBorrower";
    public static final String PHONE_CODE_EMPLOYER_CO_BORROWER_EXPR = "employmentDataForm.phoneCodeEmployerCoBorrower";
    public static final String PHONE_CODE_EMPLOYER_CO_BORROWER_PATH = "command.employmentDataForm.phoneCodeEmployerCoBorrower";
    public static final String END_OF_TEMP_EMPLOYMENT_EXPR = "employmentDataForm.endOfTempEmployment";
    public static final String END_OF_TEMP_EMPLOYMENT_PATH = "command.employmentDataForm.endOfTempEmployment";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(employment data)}
	private static final long serialVersionUID = 5113258514098734840L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _occupation;
    private String _occupationInitVal;
    private boolean _occupationIsSet;
    private String _employment;
    private String _employmentInitVal;
    private boolean _employmentIsSet;
    private String _startMonthOfOccupation;
    private String _startMonthOfOccupationInitVal;
    private boolean _startMonthOfOccupationIsSet;
    private String _startYearOfOccupation;
    private String _startYearOfOccupationInitVal;
    private boolean _startYearOfOccupationIsSet;
    private String _sectorName;
    private String _sectorNameInitVal;
    private boolean _sectorNameIsSet;
    private String _sectorFiltered;
    private String _sectorFilteredInitVal;
    private boolean _sectorFilteredIsSet;
    private String _sectorComposite;
    private String _sectorCompositeInitVal;
    private boolean _sectorCompositeIsSet;
    private Collection<Sector> _rootSectors;
    private Collection<Sector> _rootSectorsInitVal;
    private boolean _rootSectorsIsSet;
    private String _sectorDescription;
    private String _sectorDescriptionInitVal;
    private boolean _sectorDescriptionIsSet;
    private String _otherEarnDescription;
    private String _otherEarnDescriptionInitVal;
    private boolean _otherEarnDescriptionIsSet;
    private Boolean _tempEmployment;
    private Boolean _tempEmploymentInitVal;
    private boolean _tempEmploymentIsSet;
    private String _country;
    private String _countryInitVal;
    private boolean _countryIsSet;
    private String _employerName;
    private String _employerNameInitVal;
    private boolean _employerNameIsSet;
    private String _coBorrowerEmployerName;
    private String _coBorrowerEmployerNameInitVal;
    private boolean _coBorrowerEmployerNameIsSet;
    private String _phoneEmployer;
    private String _phoneEmployerInitVal;
    private boolean _phoneEmployerIsSet;
    private String _phoneCodeEmployer;
    private String _phoneCodeEmployerInitVal;
    private boolean _phoneCodeEmployerIsSet;
    private String _phoneEmployerCoBorrower;
    private String _phoneEmployerCoBorrowerInitVal;
    private boolean _phoneEmployerCoBorrowerIsSet;
    private String _phoneCodeEmployerCoBorrower;
    private String _phoneCodeEmployerCoBorrowerInitVal;
    private boolean _phoneCodeEmployerCoBorrowerIsSet;
    private Date _endOfTempEmployment;
    private Date _endOfTempEmploymentInitVal;
    private boolean _endOfTempEmploymentIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setOccupation(String occupation) {
        if (!_occupationIsSet) {
            _occupationIsSet = true;
            _occupationInitVal = occupation;
        }
        _occupation = occupation;
    }

    public String getOccupation() {
        return _occupation;
    }

    public String occupationInitVal() {
        return _occupationInitVal;
    }

	public boolean occupationIsDirty() {
        return !valuesAreEqual(_occupationInitVal, _occupation);
    }

    public boolean occupationIsSet() {
        return _occupationIsSet;
    }	


    public void setEmployment(String employment) {
        if (!_employmentIsSet) {
            _employmentIsSet = true;
            _employmentInitVal = employment;
        }
        _employment = employment;
    }

    public String getEmployment() {
        return _employment;
    }

    public String employmentInitVal() {
        return _employmentInitVal;
    }

	public boolean employmentIsDirty() {
        return !valuesAreEqual(_employmentInitVal, _employment);
    }

    public boolean employmentIsSet() {
        return _employmentIsSet;
    }	


    public void setStartMonthOfOccupation(String startMonthOfOccupation) {
        if (!_startMonthOfOccupationIsSet) {
            _startMonthOfOccupationIsSet = true;
            _startMonthOfOccupationInitVal = startMonthOfOccupation;
        }
        _startMonthOfOccupation = startMonthOfOccupation;
    }

    public String getStartMonthOfOccupation() {
        return _startMonthOfOccupation;
    }

    public String startMonthOfOccupationInitVal() {
        return _startMonthOfOccupationInitVal;
    }

	public boolean startMonthOfOccupationIsDirty() {
        return !valuesAreEqual(_startMonthOfOccupationInitVal, _startMonthOfOccupation);
    }

    public boolean startMonthOfOccupationIsSet() {
        return _startMonthOfOccupationIsSet;
    }	


    public void setStartYearOfOccupation(String startYearOfOccupation) {
        if (!_startYearOfOccupationIsSet) {
            _startYearOfOccupationIsSet = true;
            _startYearOfOccupationInitVal = startYearOfOccupation;
        }
        _startYearOfOccupation = startYearOfOccupation;
    }

    public String getStartYearOfOccupation() {
        return _startYearOfOccupation;
    }

    public String startYearOfOccupationInitVal() {
        return _startYearOfOccupationInitVal;
    }

	public boolean startYearOfOccupationIsDirty() {
        return !valuesAreEqual(_startYearOfOccupationInitVal, _startYearOfOccupation);
    }

    public boolean startYearOfOccupationIsSet() {
        return _startYearOfOccupationIsSet;
    }	


    public void setSectorName(String sectorName) {
        if (!_sectorNameIsSet) {
            _sectorNameIsSet = true;
            _sectorNameInitVal = sectorName;
        }
        _sectorName = sectorName;
    }

    public String getSectorName() {
        return _sectorName;
    }

    public String sectorNameInitVal() {
        return _sectorNameInitVal;
    }

	public boolean sectorNameIsDirty() {
        return !valuesAreEqual(_sectorNameInitVal, _sectorName);
    }

    public boolean sectorNameIsSet() {
        return _sectorNameIsSet;
    }	


    public void setSectorFiltered(String sectorFiltered) {
        if (!_sectorFilteredIsSet) {
            _sectorFilteredIsSet = true;
            _sectorFilteredInitVal = sectorFiltered;
        }
        _sectorFiltered = sectorFiltered;
    }

    public String getSectorFiltered() {
        return _sectorFiltered;
    }

    public String sectorFilteredInitVal() {
        return _sectorFilteredInitVal;
    }

	public boolean sectorFilteredIsDirty() {
        return !valuesAreEqual(_sectorFilteredInitVal, _sectorFiltered);
    }

    public boolean sectorFilteredIsSet() {
        return _sectorFilteredIsSet;
    }	


    public void setSectorComposite(String sectorComposite) {
        if (!_sectorCompositeIsSet) {
            _sectorCompositeIsSet = true;
            _sectorCompositeInitVal = sectorComposite;
        }
        _sectorComposite = sectorComposite;
    }

    public String getSectorComposite() {
        return _sectorComposite;
    }

    public String sectorCompositeInitVal() {
        return _sectorCompositeInitVal;
    }

	public boolean sectorCompositeIsDirty() {
        return !valuesAreEqual(_sectorCompositeInitVal, _sectorComposite);
    }

    public boolean sectorCompositeIsSet() {
        return _sectorCompositeIsSet;
    }	


    public void setRootSectors(Collection<Sector> rootSectors) {
        if (!_rootSectorsIsSet) {
            _rootSectorsIsSet = true;
            _rootSectorsInitVal = rootSectors;
        }
        _rootSectors = rootSectors;
    }

    public Collection<Sector> getRootSectors() {
        return _rootSectors;
    }

    public Collection<Sector> rootSectorsInitVal() {
        return _rootSectorsInitVal;
    }

	public boolean rootSectorsIsDirty() {
        return !valuesAreEqual(_rootSectorsInitVal, _rootSectors);
    }

    public boolean rootSectorsIsSet() {
        return _rootSectorsIsSet;
    }	


    public void setSectorDescription(String sectorDescription) {
        if (!_sectorDescriptionIsSet) {
            _sectorDescriptionIsSet = true;
            _sectorDescriptionInitVal = sectorDescription;
        }
        _sectorDescription = sectorDescription;
    }

    public String getSectorDescription() {
        return _sectorDescription;
    }

    public String sectorDescriptionInitVal() {
        return _sectorDescriptionInitVal;
    }

	public boolean sectorDescriptionIsDirty() {
        return !valuesAreEqual(_sectorDescriptionInitVal, _sectorDescription);
    }

    public boolean sectorDescriptionIsSet() {
        return _sectorDescriptionIsSet;
    }	


    public void setOtherEarnDescription(String otherEarnDescription) {
        if (!_otherEarnDescriptionIsSet) {
            _otherEarnDescriptionIsSet = true;
            _otherEarnDescriptionInitVal = otherEarnDescription;
        }
        _otherEarnDescription = otherEarnDescription;
    }

    public String getOtherEarnDescription() {
        return _otherEarnDescription;
    }

    public String otherEarnDescriptionInitVal() {
        return _otherEarnDescriptionInitVal;
    }

	public boolean otherEarnDescriptionIsDirty() {
        return !valuesAreEqual(_otherEarnDescriptionInitVal, _otherEarnDescription);
    }

    public boolean otherEarnDescriptionIsSet() {
        return _otherEarnDescriptionIsSet;
    }	


    public void setTempEmployment(Boolean tempEmployment) {
        if (!_tempEmploymentIsSet) {
            _tempEmploymentIsSet = true;
            _tempEmploymentInitVal = tempEmployment;
        }
        _tempEmployment = tempEmployment;
    }

    public Boolean getTempEmployment() {
        return _tempEmployment;
    }

    public Boolean tempEmploymentInitVal() {
        return _tempEmploymentInitVal;
    }

	public boolean tempEmploymentIsDirty() {
        return !valuesAreEqual(_tempEmploymentInitVal, _tempEmployment);
    }

    public boolean tempEmploymentIsSet() {
        return _tempEmploymentIsSet;
    }	


    public void setCountry(String country) {
        if (!_countryIsSet) {
            _countryIsSet = true;
            _countryInitVal = country;
        }
        _country = country;
    }

    public String getCountry() {
        return _country;
    }

    public String countryInitVal() {
        return _countryInitVal;
    }

	public boolean countryIsDirty() {
        return !valuesAreEqual(_countryInitVal, _country);
    }

    public boolean countryIsSet() {
        return _countryIsSet;
    }	


    public void setEmployerName(String employerName) {
        if (!_employerNameIsSet) {
            _employerNameIsSet = true;
            _employerNameInitVal = employerName;
        }
        _employerName = employerName;
    }

    public String getEmployerName() {
        return _employerName;
    }

    public String employerNameInitVal() {
        return _employerNameInitVal;
    }

	public boolean employerNameIsDirty() {
        return !valuesAreEqual(_employerNameInitVal, _employerName);
    }

    public boolean employerNameIsSet() {
        return _employerNameIsSet;
    }	


    public void setCoBorrowerEmployerName(String coBorrowerEmployerName) {
        if (!_coBorrowerEmployerNameIsSet) {
            _coBorrowerEmployerNameIsSet = true;
            _coBorrowerEmployerNameInitVal = coBorrowerEmployerName;
        }
        _coBorrowerEmployerName = coBorrowerEmployerName;
    }

    public String getCoBorrowerEmployerName() {
        return _coBorrowerEmployerName;
    }

    public String coBorrowerEmployerNameInitVal() {
        return _coBorrowerEmployerNameInitVal;
    }

	public boolean coBorrowerEmployerNameIsDirty() {
        return !valuesAreEqual(_coBorrowerEmployerNameInitVal, _coBorrowerEmployerName);
    }

    public boolean coBorrowerEmployerNameIsSet() {
        return _coBorrowerEmployerNameIsSet;
    }	


    public void setPhoneEmployer(String phoneEmployer) {
        if (!_phoneEmployerIsSet) {
            _phoneEmployerIsSet = true;
            _phoneEmployerInitVal = phoneEmployer;
        }
        _phoneEmployer = phoneEmployer;
    }

    public String getPhoneEmployer() {
        return _phoneEmployer;
    }

    public String phoneEmployerInitVal() {
        return _phoneEmployerInitVal;
    }

	public boolean phoneEmployerIsDirty() {
        return !valuesAreEqual(_phoneEmployerInitVal, _phoneEmployer);
    }

    public boolean phoneEmployerIsSet() {
        return _phoneEmployerIsSet;
    }	


    public void setPhoneCodeEmployer(String phoneCodeEmployer) {
        if (!_phoneCodeEmployerIsSet) {
            _phoneCodeEmployerIsSet = true;
            _phoneCodeEmployerInitVal = phoneCodeEmployer;
        }
        _phoneCodeEmployer = phoneCodeEmployer;
    }

    public String getPhoneCodeEmployer() {
        return _phoneCodeEmployer;
    }

    public String phoneCodeEmployerInitVal() {
        return _phoneCodeEmployerInitVal;
    }

	public boolean phoneCodeEmployerIsDirty() {
        return !valuesAreEqual(_phoneCodeEmployerInitVal, _phoneCodeEmployer);
    }

    public boolean phoneCodeEmployerIsSet() {
        return _phoneCodeEmployerIsSet;
    }	


    public void setPhoneEmployerCoBorrower(String phoneEmployerCoBorrower) {
        if (!_phoneEmployerCoBorrowerIsSet) {
            _phoneEmployerCoBorrowerIsSet = true;
            _phoneEmployerCoBorrowerInitVal = phoneEmployerCoBorrower;
        }
        _phoneEmployerCoBorrower = phoneEmployerCoBorrower;
    }

    public String getPhoneEmployerCoBorrower() {
        return _phoneEmployerCoBorrower;
    }

    public String phoneEmployerCoBorrowerInitVal() {
        return _phoneEmployerCoBorrowerInitVal;
    }

	public boolean phoneEmployerCoBorrowerIsDirty() {
        return !valuesAreEqual(_phoneEmployerCoBorrowerInitVal, _phoneEmployerCoBorrower);
    }

    public boolean phoneEmployerCoBorrowerIsSet() {
        return _phoneEmployerCoBorrowerIsSet;
    }	


    public void setPhoneCodeEmployerCoBorrower(String phoneCodeEmployerCoBorrower) {
        if (!_phoneCodeEmployerCoBorrowerIsSet) {
            _phoneCodeEmployerCoBorrowerIsSet = true;
            _phoneCodeEmployerCoBorrowerInitVal = phoneCodeEmployerCoBorrower;
        }
        _phoneCodeEmployerCoBorrower = phoneCodeEmployerCoBorrower;
    }

    public String getPhoneCodeEmployerCoBorrower() {
        return _phoneCodeEmployerCoBorrower;
    }

    public String phoneCodeEmployerCoBorrowerInitVal() {
        return _phoneCodeEmployerCoBorrowerInitVal;
    }

	public boolean phoneCodeEmployerCoBorrowerIsDirty() {
        return !valuesAreEqual(_phoneCodeEmployerCoBorrowerInitVal, _phoneCodeEmployerCoBorrower);
    }

    public boolean phoneCodeEmployerCoBorrowerIsSet() {
        return _phoneCodeEmployerCoBorrowerIsSet;
    }	


    public void setEndOfTempEmployment(Date endOfTempEmployment) {
        if (!_endOfTempEmploymentIsSet) {
            _endOfTempEmploymentIsSet = true;

            if (endOfTempEmployment == null) {
                _endOfTempEmploymentInitVal = null;
            } else {
                _endOfTempEmploymentInitVal = (Date) endOfTempEmployment.clone();
            }
        }

        if (endOfTempEmployment == null) {
            _endOfTempEmployment = null;
        } else {
            _endOfTempEmployment = (Date) endOfTempEmployment.clone();
        }
    }

    public Date getEndOfTempEmployment() {
        Date endOfTempEmployment = null;
        if (_endOfTempEmployment != null) {
            endOfTempEmployment = (Date) _endOfTempEmployment.clone();
        }
        return endOfTempEmployment;
    }

    public Date endOfTempEmploymentInitVal() {
        Date endOfTempEmploymentInitVal = null;
        if (_endOfTempEmploymentInitVal != null) {
            endOfTempEmploymentInitVal = (Date) _endOfTempEmploymentInitVal.clone();
        }
        return endOfTempEmploymentInitVal;
    }

	public boolean endOfTempEmploymentIsDirty() {
        return !valuesAreEqual(_endOfTempEmploymentInitVal, _endOfTempEmployment);
    }

    public boolean endOfTempEmploymentIsSet() {
        return _endOfTempEmploymentIsSet;
    }	

}
