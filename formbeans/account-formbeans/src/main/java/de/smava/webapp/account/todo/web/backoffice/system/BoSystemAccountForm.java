//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.system;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo system account)}
import java.io.Serializable;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoSystemAccountForm'.
 *
 * @author generator
 */
public class BoSystemAccountForm implements Serializable {

    public static final String ACCOUNT_ID_EXPR = "boSystemAccountForm.accountId";
    public static final String ACCOUNT_ID_PATH = "command.boSystemAccountForm.accountId";
    public static final String USERNAME_EXPR = "boSystemAccountForm.username";
    public static final String USERNAME_PATH = "command.boSystemAccountForm.username";
    public static final String PASSWORD_EXPR = "boSystemAccountForm.password";
    public static final String PASSWORD_PATH = "command.boSystemAccountForm.password";
    public static final String CREATION_DATE_EXPR = "boSystemAccountForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boSystemAccountForm.creationDate";
    public static final String CHECK_EXPR = "boSystemAccountForm.check";
    public static final String CHECK_PATH = "command.boSystemAccountForm.check";
    public static final String FIRST_NAME_EXPR = "boSystemAccountForm.firstName";
    public static final String FIRST_NAME_PATH = "command.boSystemAccountForm.firstName";
    public static final String LAST_NAME_EXPR = "boSystemAccountForm.lastName";
    public static final String LAST_NAME_PATH = "command.boSystemAccountForm.lastName";
    public static final String STATE_EXPR = "boSystemAccountForm.state";
    public static final String STATE_PATH = "command.boSystemAccountForm.state";
    public static final String EMAIL_EXPR = "boSystemAccountForm.email";
    public static final String EMAIL_PATH = "command.boSystemAccountForm.email";
    public static final String ROLES_EXPR = "boSystemAccountForm.roles";
    public static final String ROLES_PATH = "command.boSystemAccountForm.roles";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo system account)}
// !!!!!!!! End of insert code section !!!!!!!!


    private Long _accountId;
    private Long _accountIdInitVal;
    private boolean _accountIdIsSet;

    private String _username;
    private String _usernameInitVal;
    private boolean _usernameIsSet;

    private String _password;
    private String _passwordInitVal;
    private boolean _passwordIsSet;

    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;

    private String _check;
    private String _checkInitVal;
    private boolean _checkIsSet;

    private String _firstName;
    private String _firstNameInitVal;
    private boolean _firstNameIsSet;

    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;

    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;

    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;

    private Set<String> _roles;
    private Set<String> _rolesInitVal;
    private boolean _rolesIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setAccountId(Long accountId) {
        if (!_accountIdIsSet) {
            _accountIdIsSet = true;
            _accountIdInitVal = accountId;
        }
        _accountId = accountId;
    }

    public Long getAccountId() {
        return _accountId;
    }

    public Long accountIdInitVal() {
        return _accountIdInitVal;
    }

    public boolean accountIdIsDirty() {
        return !valuesAreEqual(_accountIdInitVal, _accountId);
    }

    public boolean accountIdIsSet() {
        return _accountIdIsSet;
    }

    public void setUsername(String username) {
        if (!_usernameIsSet) {
            _usernameIsSet = true;
            _usernameInitVal = username;
        }
        _username = username;
    }

    public String getUsername() {
        return _username;
    }

    public String usernameInitVal() {
        return _usernameInitVal;
    }

    public boolean usernameIsDirty() {
        return !valuesAreEqual(_usernameInitVal, _username);
    }

    public boolean usernameIsSet() {
        return _usernameIsSet;
    }

    public void setPassword(String password) {
        if (!_passwordIsSet) {
            _passwordIsSet = true;
            _passwordInitVal = password;
        }
        _password = password;
    }

    public String getPassword() {
        return _password;
    }

    public String passwordInitVal() {
        return _passwordInitVal;
    }

    public boolean passwordIsDirty() {
        return !valuesAreEqual(_passwordInitVal, _password);
    }

    public boolean passwordIsSet() {
        return _passwordIsSet;
    }

    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

    public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

    public void setCheck(String check) {
        if (!_checkIsSet) {
            _checkIsSet = true;
            _checkInitVal = check;
        }
        _check = check;
    }

    public String getCheck() {
        return _check;
    }

    public String checkInitVal() {
        return _checkInitVal;
    }

    public boolean checkIsDirty() {
        return !valuesAreEqual(_checkInitVal, _check);
    }

    public boolean checkIsSet() {
        return _checkIsSet;
    }

    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = firstName;
        }
        _firstName = firstName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String firstNameInitVal() {
        return _firstNameInitVal;
    }

    public boolean firstNameIsDirty() {
        return !valuesAreEqual(_firstNameInitVal, _firstName);
    }

    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }

    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

    public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }

    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

    public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }

    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

    public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }

    public void setRoles(Set<String> roles) {
        if (!_rolesIsSet) {
            _rolesIsSet = true;
            _rolesInitVal = roles;
        }
        _roles = roles;
    }

    public Set<String> getRoles() {
        return _roles;
    }

    public Set<String> rolesInitVal() {
        return _rolesInitVal;
    }

    public boolean rolesIsDirty() {
        return !valuesAreEqual(_rolesInitVal, _roles);
    }

    public boolean rolesIsSet() {
        return _rolesIsSet;
    }

}
