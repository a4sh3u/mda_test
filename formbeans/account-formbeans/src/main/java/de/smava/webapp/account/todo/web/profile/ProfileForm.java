//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.profile;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(profile)}

import de.smava.webapp.commons.web.ImageAwareCommand;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'ProfileForm'.
 *
 * @author generator
 */
public class ProfileForm implements ImageAwareCommand {

    public static final String DESCRIPTION_EXPR = "profileForm.description";
    public static final String DESCRIPTION_PATH = "command.profileForm.description";
    public static final String IMAGE_EXPR = "profileForm.image";
    public static final String IMAGE_PATH = "command.profileForm.image";
    public static final String HOBBIES_EXPR = "profileForm.hobbies";
    public static final String HOBBIES_PATH = "command.profileForm.hobbies";
    public static final String ACTION_EXPR = "profileForm.action";
    public static final String ACTION_PATH = "command.profileForm.action";
    public static final String CHANGE_DATE_EXPR = "profileForm.changeDate";
    public static final String CHANGE_DATE_PATH = "command.profileForm.changeDate";
    public static final String STATE_EXPR = "profileForm.state";
    public static final String STATE_PATH = "command.profileForm.state";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(profile)}
    private byte[] _file = null;
    private int _imageWidth;
    private int _imageHeight;
    private boolean _isNewUpload;
    
    public byte[] getFile() {
        byte[] file = null;
        if (_file != null) {
            file = _file.clone();
        }
        return file;
    }

    public void setFile(byte[] file) {
        _file = file.clone();
    }

    public int getImageWidth() {
        return _imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        _imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return _imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        _imageHeight = imageHeight;
    }

    public String getNoImageExpression() {
        return null;
    }

    public String getImageFileExpression() {
        return "image";
    }

    public void updateNoImage() {
        // Nothing to do
    }

    public void resetForm() {
        // Nothing to do
    }

    public String getImageCategory() {
        return IMAGE_CATEGORY_PROFILE;
    }
    
	public boolean isNewUpload() {
		return _isNewUpload;
	}

	public void setNewUpload(boolean isNewUpload) {
		_isNewUpload = isNewUpload;
	}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private String _hobbies;
    private String _hobbiesInitVal;
    private boolean _hobbiesIsSet;
    private String _action;
    private String _actionInitVal;
    private boolean _actionIsSet;
    private Date _changeDate;
    private Date _changeDateInitVal;
    private boolean _changeDateIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setHobbies(String hobbies) {
        if (!_hobbiesIsSet) {
            _hobbiesIsSet = true;
            _hobbiesInitVal = hobbies;
        }
        _hobbies = hobbies;
    }

    public String getHobbies() {
        return _hobbies;
    }

    public String hobbiesInitVal() {
        return _hobbiesInitVal;
    }

	public boolean hobbiesIsDirty() {
        return !valuesAreEqual(_hobbiesInitVal, _hobbies);
    }

    public boolean hobbiesIsSet() {
        return _hobbiesIsSet;
    }	


    public void setAction(String action) {
        if (!_actionIsSet) {
            _actionIsSet = true;
            _actionInitVal = action;
        }
        _action = action;
    }

    public String getAction() {
        return _action;
    }

    public String actionInitVal() {
        return _actionInitVal;
    }

	public boolean actionIsDirty() {
        return !valuesAreEqual(_actionInitVal, _action);
    }

    public boolean actionIsSet() {
        return _actionIsSet;
    }	


    public void setChangeDate(Date changeDate) {
        if (!_changeDateIsSet) {
            _changeDateIsSet = true;

            if (changeDate == null) {
                _changeDateInitVal = null;
            } else {
                _changeDateInitVal = (Date) changeDate.clone();
            }
        }

        if (changeDate == null) {
            _changeDate = null;
        } else {
            _changeDate = (Date) changeDate.clone();
        }
    }

    public Date getChangeDate() {
        Date changeDate = null;
        if (_changeDate != null) {
            changeDate = (Date) _changeDate.clone();
        }
        return changeDate;
    }

    public Date changeDateInitVal() {
        Date changeDateInitVal = null;
        if (_changeDateInitVal != null) {
            changeDateInitVal = (Date) _changeDateInitVal.clone();
        }
        return changeDateInitVal;
    }

	public boolean changeDateIsDirty() {
        return !valuesAreEqual(_changeDateInitVal, _changeDate);
    }

    public boolean changeDateIsSet() {
        return _changeDateIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	

}
