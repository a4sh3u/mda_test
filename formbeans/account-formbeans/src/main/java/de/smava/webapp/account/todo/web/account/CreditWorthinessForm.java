//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.account;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(credit worthiness)}

import de.smava.webapp.account.domain.MiscEarnType;
import de.smava.webapp.account.domain.Sector;
import de.smava.webapp.account.todo.web.backoffice.account.BoCreditWorthinessForm;
import de.smava.webapp.commons.util.FormatUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Collection;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CreditWorthinessForm'.
 *
 * @author generator
 */
public class CreditWorthinessForm implements Serializable {

    public static final String OCCUPATION_EXPR = "creditWorthinessForm.occupation";
    public static final String OCCUPATION_PATH = "command.creditWorthinessForm.occupation";
    public static final String EMPLOYMENT_EXPR = "creditWorthinessForm.employment";
    public static final String EMPLOYMENT_PATH = "command.creditWorthinessForm.employment";
    public static final String START_MONTH_OF_OCCUPATION_EXPR = "creditWorthinessForm.startMonthOfOccupation";
    public static final String START_MONTH_OF_OCCUPATION_PATH = "command.creditWorthinessForm.startMonthOfOccupation";
    public static final String START_YEAR_OF_OCCUPATION_EXPR = "creditWorthinessForm.startYearOfOccupation";
    public static final String START_YEAR_OF_OCCUPATION_PATH = "command.creditWorthinessForm.startYearOfOccupation";
    public static final String SECTOR_NAME_EXPR = "creditWorthinessForm.sectorName";
    public static final String SECTOR_NAME_PATH = "command.creditWorthinessForm.sectorName";
    public static final String SECTOR_FILTERED_EXPR = "creditWorthinessForm.sectorFiltered";
    public static final String SECTOR_FILTERED_PATH = "command.creditWorthinessForm.sectorFiltered";
    public static final String SECTOR_COMPOSITE_EXPR = "creditWorthinessForm.sectorComposite";
    public static final String SECTOR_COMPOSITE_PATH = "command.creditWorthinessForm.sectorComposite";
    public static final String ROOT_SECTORS_EXPR = "creditWorthinessForm.rootSectors";
    public static final String ROOT_SECTORS_PATH = "command.creditWorthinessForm.rootSectors";
    public static final String SECTOR_DESCRIPTION_EXPR = "creditWorthinessForm.sectorDescription";
    public static final String SECTOR_DESCRIPTION_PATH = "command.creditWorthinessForm.sectorDescription";
    public static final String TEMP_EMPLOYMENT_EXPR = "creditWorthinessForm.tempEmployment";
    public static final String TEMP_EMPLOYMENT_PATH = "command.creditWorthinessForm.tempEmployment";
    public static final String SHARED_LOAN_EXPR = "creditWorthinessForm.sharedLoan";
    public static final String SHARED_LOAN_PATH = "command.creditWorthinessForm.sharedLoan";
    public static final String OTHER_INCOME_EXPR = "creditWorthinessForm.otherIncome";
    public static final String OTHER_INCOME_PATH = "command.creditWorthinessForm.otherIncome";
    public static final String OTHER_EARN_DESCRIPTION_EXPR = "creditWorthinessForm.otherEarnDescription";
    public static final String OTHER_EARN_DESCRIPTION_PATH = "command.creditWorthinessForm.otherEarnDescription";
	public static final String INCOME_EXPR = "creditWorthinessForm.income";
    public static final String INCOME_PATH = "command.creditWorthinessForm.income";
	public static final String RECEIVED_PALIMONY_EXPR = "creditWorthinessForm.receivedPalimony";
    public static final String RECEIVED_PALIMONY_PATH = "command.creditWorthinessForm.receivedPalimony";
	public static final String MISC_EARN_TYPE_EXPR = "creditWorthinessForm.miscEarnType";
    public static final String MISC_EARN_TYPE_PATH = "command.creditWorthinessForm.miscEarnType";
	public static final String MISC_EARN_AMOUNT_1_EXPR = "creditWorthinessForm.miscEarnAmount1";
    public static final String MISC_EARN_AMOUNT_1_PATH = "command.creditWorthinessForm.miscEarnAmount1";
	public static final String RECEIVED_RENT_EXPR = "creditWorthinessForm.receivedRent";
    public static final String RECEIVED_RENT_PATH = "command.creditWorthinessForm.receivedRent";
    public static final String MODE_OF_ACCOMODATION_EXPR = "creditWorthinessForm.modeOfAccomodation";
    public static final String MODE_OF_ACCOMODATION_PATH = "command.creditWorthinessForm.modeOfAccomodation";
    public static final String PAID_ALIMONY_EXPR = "creditWorthinessForm.paidAlimony";
    public static final String PAID_ALIMONY_PATH = "command.creditWorthinessForm.paidAlimony";
    public static final String PAID_RENT_EXPR = "creditWorthinessForm.paidRent";
    public static final String PAID_RENT_PATH = "command.creditWorthinessForm.paidRent";
    public static final String EXPENSES_CAR_EXPR = "creditWorthinessForm.expensesCar";
    public static final String EXPENSES_CAR_PATH = "command.creditWorthinessForm.expensesCar";
    public static final String NUM_OF_CARS_EXPR = "creditWorthinessForm.numOfCars";
    public static final String NUM_OF_CARS_PATH = "command.creditWorthinessForm.numOfCars";
    public static final String PAID_ALIMONY_RADIO_EXPR = "creditWorthinessForm.paidAlimonyRadio";
    public static final String PAID_ALIMONY_RADIO_PATH = "command.creditWorthinessForm.paidAlimonyRadio";
    public static final String LEASING_EXPR = "creditWorthinessForm.leasing";
    public static final String LEASING_PATH = "command.creditWorthinessForm.leasing";
    public static final String LEASING_RADIO_EXPR = "creditWorthinessForm.leasingRadio";
    public static final String LEASING_RADIO_PATH = "command.creditWorthinessForm.leasingRadio";
    public static final String CREDIT_EXPENSES_EXPR = "creditWorthinessForm.creditExpenses";
    public static final String CREDIT_EXPENSES_PATH = "command.creditWorthinessForm.creditExpenses";
    public static final String TOTAL_PRIVATE_CREDIT_AMOUNT_EXPR = "creditWorthinessForm.totalPrivateCreditAmount";
    public static final String TOTAL_PRIVATE_CREDIT_AMOUNT_PATH = "command.creditWorthinessForm.totalPrivateCreditAmount";
    public static final String CREDIT_EXPENSES_RADIO_EXPR = "creditWorthinessForm.creditExpensesRadio";
    public static final String CREDIT_EXPENSES_RADIO_PATH = "command.creditWorthinessForm.creditExpensesRadio";
    public static final String SAVINGS_EXPR = "creditWorthinessForm.savings";
    public static final String SAVINGS_PATH = "command.creditWorthinessForm.savings";
    public static final String SAVINGS_RADIO_EXPR = "creditWorthinessForm.savingsRadio";
    public static final String SAVINGS_RADIO_PATH = "command.creditWorthinessForm.savingsRadio";
    public static final String NUMBER_OF_CHILDREN_EXPR = "creditWorthinessForm.numberOfChildren";
    public static final String NUMBER_OF_CHILDREN_PATH = "command.creditWorthinessForm.numberOfChildren";
    public static final String NUMBER_OF_OTHER_IN_HOUSEHOLD_EXPR = "creditWorthinessForm.numberOfOtherInHousehold";
    public static final String NUMBER_OF_OTHER_IN_HOUSEHOLD_PATH = "command.creditWorthinessForm.numberOfOtherInHousehold";
    public static final String NUMBER_OF_OTHER_IN_HOUSEHOLD_CHECK_EXPR = "creditWorthinessForm.numberOfOtherInHouseholdCheck";
    public static final String NUMBER_OF_OTHER_IN_HOUSEHOLD_CHECK_PATH = "command.creditWorthinessForm.numberOfOtherInHouseholdCheck";
    public static final String INSURANCE_EXPR = "creditWorthinessForm.insurance";
    public static final String INSURANCE_PATH = "command.creditWorthinessForm.insurance";
    public static final String PRIVATE_HEALTH_INSURANCE_EXPR = "creditWorthinessForm.privateHealthInsurance";
    public static final String PRIVATE_HEALTH_INSURANCE_PATH = "command.creditWorthinessForm.privateHealthInsurance";
    public static final String PRIVATE_HEALTH_INSURANCE_RADIO_EXPR = "creditWorthinessForm.privateHealthInsuranceRadio";
    public static final String PRIVATE_HEALTH_INSURANCE_RADIO_PATH = "command.creditWorthinessForm.privateHealthInsuranceRadio";
    public static final String MORTGAGE_EXPR = "creditWorthinessForm.mortgage";
    public static final String MORTGAGE_PATH = "command.creditWorthinessForm.mortgage";
    public static final String MORTGAGE_RADIO_EXPR = "creditWorthinessForm.mortgageRadio";
    public static final String MORTGAGE_RADIO_PATH = "command.creditWorthinessForm.mortgageRadio";
    public static final String CREDITCARDS_EXPR = "creditWorthinessForm.creditcards";
    public static final String CREDITCARDS_PATH = "command.creditWorthinessForm.creditcards";
    public static final String PROPERTY_OWNED_EXPR = "creditWorthinessForm.propertyOwned";
    public static final String PROPERTY_OWNED_PATH = "command.creditWorthinessForm.propertyOwned";
    public static final String PROPERTY_OWNED_SQUARE_METERS_EXPR = "creditWorthinessForm.propertyOwnedSquareMeters";
    public static final String PROPERTY_OWNED_SQUARE_METERS_PATH = "command.creditWorthinessForm.propertyOwnedSquareMeters";
    public static final String FREELANCER_INCOME_CONFIRMATION_LIMIT_DATE_EXPR = "creditWorthinessForm.freelancerIncomeConfirmationLimitDate";
    public static final String FREELANCER_INCOME_CONFIRMATION_LIMIT_DATE_PATH = "command.creditWorthinessForm.freelancerIncomeConfirmationLimitDate";
    public static final String PAID_TRADE_TAX_EXPR = "creditWorthinessForm.paidTradeTax";
    public static final String PAID_TRADE_TAX_PATH = "command.creditWorthinessForm.paidTradeTax";
    public static final String PAID_CHURCH_TAX_EXPR = "creditWorthinessForm.paidChurchTax";
    public static final String PAID_CHURCH_TAX_PATH = "command.creditWorthinessForm.paidChurchTax";
    public static final String BUSINESS_LOAN_EXPR = "creditWorthinessForm.businessLoan";
    public static final String BUSINESS_LOAN_PATH = "command.creditWorthinessForm.businessLoan";
    public static final String BUSINESS_LEASING_EXPR = "creditWorthinessForm.businessLeasing";
    public static final String BUSINESS_LEASING_PATH = "command.creditWorthinessForm.businessLeasing";
    public static final String CONTRACT_OF_EMPLOYMENT_EXPR = "creditWorthinessForm.contractOfEmployment";
    public static final String CONTRACT_OF_EMPLOYMENT_PATH = "command.creditWorthinessForm.contractOfEmployment";
    public static final String CIVIL_CONTRACT_EXPR = "creditWorthinessForm.civilContract";
    public static final String CIVIL_CONTRACT_PATH = "command.creditWorthinessForm.civilContract";
    public static final String BUSINESS_ACTIVITIES_EXPR = "creditWorthinessForm.businessActivities";
    public static final String BUSINESS_ACTIVITIES_PATH = "command.creditWorthinessForm.businessActivities";
    public static final String BLACKLIST_CHECK_EXPR = "creditWorthinessForm.blacklistCheck";
    public static final String BLACKLIST_CHECK_PATH = "command.creditWorthinessForm.blacklistCheck";
    public static final String SHARED_BANKACCOUNT_EXPR = "creditWorthinessForm.sharedBankaccount";
    public static final String SHARED_BANKACCOUNT_PATH = "command.creditWorthinessForm.sharedBankaccount";
    public static final String INCOME_BY_BANKVIEW_EXPR = "creditWorthinessForm.incomeByBankview";
    public static final String INCOME_BY_BANKVIEW_PATH = "command.creditWorthinessForm.incomeByBankview";
    public static final String RECEIVED_PALIMONY_BY_BANKVIEW_EXPR = "creditWorthinessForm.receivedPalimonyByBankview";
    public static final String RECEIVED_PALIMONY_BY_BANKVIEW_PATH = "command.creditWorthinessForm.receivedPalimonyByBankview";
    public static final String RECEIVED_RENT_BY_BANKVIEW_EXPR = "creditWorthinessForm.receivedRentByBankview";
    public static final String RECEIVED_RENT_BY_BANKVIEW_PATH = "command.creditWorthinessForm.receivedRentByBankview";
    public static final String MISC_EARNS_BY_BANKVIEW_EXPR = "creditWorthinessForm.miscEarnsByBankview";
    public static final String MISC_EARNS_BY_BANKVIEW_PATH = "command.creditWorthinessForm.miscEarnsByBankview";
    public static final String PAID_RENT_BY_BANKVIEW_EXPR = "creditWorthinessForm.paidRentByBankview";
    public static final String PAID_RENT_BY_BANKVIEW_PATH = "command.creditWorthinessForm.paidRentByBankview";
    public static final String PRIVATE_HEALTH_INSURANCE_BY_BANKVIEW_EXPR = "creditWorthinessForm.privateHealthInsuranceByBankview";
    public static final String PRIVATE_HEALTH_INSURANCE_BY_BANKVIEW_PATH = "command.creditWorthinessForm.privateHealthInsuranceByBankview";
    public static final String SAVINGS_BY_BANKVIEW_EXPR = "creditWorthinessForm.savingsByBankview";
    public static final String SAVINGS_BY_BANKVIEW_PATH = "command.creditWorthinessForm.savingsByBankview";
    public static final String PAID_ALIMONY_BY_BANKVIEW_EXPR = "creditWorthinessForm.paidAlimonyByBankview";
    public static final String PAID_ALIMONY_BY_BANKVIEW_PATH = "command.creditWorthinessForm.paidAlimonyByBankview";
    public static final String PRIVATE_LEASING_RATES_BY_BANKVIEW_EXPR = "creditWorthinessForm.privateLeasingRatesByBankview";
    public static final String PRIVATE_LEASING_RATES_BY_BANKVIEW_PATH = "command.creditWorthinessForm.privateLeasingRatesByBankview";
    public static final String REAL_ESTATE_CREDIT_RATES_BY_BANKVIEW_EXPR = "creditWorthinessForm.realEstateCreditRatesByBankview";
    public static final String REAL_ESTATE_CREDIT_RATES_BY_BANKVIEW_PATH = "command.creditWorthinessForm.realEstateCreditRatesByBankview";
    public static final String PRIVATE_CREDIT_RATES_BY_BANKVIEW_EXPR = "creditWorthinessForm.privateCreditRatesByBankview";
    public static final String PRIVATE_CREDIT_RATES_BY_BANKVIEW_PATH = "command.creditWorthinessForm.privateCreditRatesByBankview";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(credit worthiness)}
    private static final long serialVersionUID = 1L;
	
    public static final String MISC_EARN_1_EXPR = "creditWorthinessForm.miscEarn1";
    public static final String MISC_EARN_1_PATH = "command.creditWorthinessForm.miscEarn1";
    private String _miscEarn1;
    private String _miscEarn1InitVal;
    private boolean _miscEarn1IsSet;
    
    public CreditWorthinessForm() {
    	/*
    	_selfEmployedIncomeFirstYear = new FreelancerIncome();
    	_selfEmployedIncomeSecondYear = new FreelancerIncome();
    	_selfEmployedIncomeThirdYear = new FreelancerIncome();
    	*/
    	_mortgage = "0";
    	_creditExpenses = "0";
//    	_otherIncome = Boolean.TRUE;
    }
   
    //removed from generator, because additional handling required
    //if in old regflow miscEarnType dropdown not existent but miscEarn1 set, also set miscEarnType to "MISC" if miscEarn1 is set
    public void setMiscEarn1(String miscEarn1) {
        if (!_miscEarn1IsSet) {
            _miscEarn1IsSet = true;
            _miscEarn1InitVal = miscEarn1;
        }
        _miscEarn1 = miscEarn1;
        //Only do this if regflow, leave CRM with maximum freedom to set values
        if(StringUtils.isNotEmpty(miscEarn1) && !(this instanceof BoCreditWorthinessForm)) {
        	setMiscEarnType(MiscEarnType.MISC);
        }
        
    }

    public String getMiscEarn1() {
        return _miscEarn1;
    }

    public String miscEarn1InitVal() {
        return _miscEarn1InitVal;
    }
    
    public boolean miscEarn1IsDirty() {
        return !valuesAreEqual(_miscEarn1InitVal, _miscEarn1);
    }

    public boolean miscEarn1IsSet() {
        return _miscEarn1IsSet;
    }	
    
    //only for internal use so NO direct user input
    private double _deposit = 0.0;
    
    public final double getDeposit() {
		return _deposit;
	}

	public final void setDeposit(double consolidationDebt) {
		_deposit = consolidationDebt;
	}
	
	public String getCreditExpensesMinusDeposit() {
		final String creditExpenses = getCreditExpenses();
		try {
			double creditMinusDeposit = FormatUtils.parseAmount(creditExpenses) - _deposit;
			if (creditMinusDeposit < 0.0) {
				creditMinusDeposit = 0.0;
			}
			return FormatUtils.formatDoubleAmount(creditMinusDeposit);
		} catch (ParseException e) {
			return creditExpenses;
		}
	}
	
	public boolean allOtherIncomeEmpty() {
		return StringUtils.isBlank(_receivedPalimony)
				&& StringUtils.isBlank(_receivedRent)
				&& StringUtils.isBlank(_miscEarn1)
				&& StringUtils.isBlank(_miscEarnAmount1);
	}
    
// !!!!!!!! End of insert code section !!!!!!!!

    private String _occupation;
    private String _occupationInitVal;
    private boolean _occupationIsSet;
    private String _employment;
    private String _employmentInitVal;
    private boolean _employmentIsSet;
    private String _startMonthOfOccupation;
    private String _startMonthOfOccupationInitVal;
    private boolean _startMonthOfOccupationIsSet;
    private String _startYearOfOccupation;
    private String _startYearOfOccupationInitVal;
    private boolean _startYearOfOccupationIsSet;
    private String _sectorName;
    private String _sectorNameInitVal;
    private boolean _sectorNameIsSet;
    private String _sectorFiltered;
    private String _sectorFilteredInitVal;
    private boolean _sectorFilteredIsSet;
    private String _sectorComposite;
    private String _sectorCompositeInitVal;
    private boolean _sectorCompositeIsSet;
    private Collection<Sector> _rootSectors;
    private Collection<Sector> _rootSectorsInitVal;
    private boolean _rootSectorsIsSet;
    private String _sectorDescription;
    private String _sectorDescriptionInitVal;
    private boolean _sectorDescriptionIsSet;
    private Boolean _tempEmployment;
    private Boolean _tempEmploymentInitVal;
    private boolean _tempEmploymentIsSet;
    private boolean _sharedLoan;
    private boolean _sharedLoanInitVal;
    private boolean _sharedLoanIsSet;
    private Boolean _otherIncome;
    private Boolean _otherIncomeInitVal;
    private boolean _otherIncomeIsSet;
    private String _otherEarnDescription;
    private String _otherEarnDescriptionInitVal;
    private boolean _otherEarnDescriptionIsSet;
   	private String _income;
    private String _incomeInitVal;
    private boolean _incomeIsSet;
   	private String _receivedPalimony;
    private String _receivedPalimonyInitVal;
    private boolean _receivedPalimonyIsSet;
   	private de.smava.webapp.account.domain.MiscEarnType _miscEarnType;
    private de.smava.webapp.account.domain.MiscEarnType _miscEarnTypeInitVal;
    private boolean _miscEarnTypeIsSet;
   	private String _miscEarnAmount1;
    private String _miscEarnAmount1InitVal;
    private boolean _miscEarnAmount1IsSet;
   	private String _receivedRent;
    private String _receivedRentInitVal;
    private boolean _receivedRentIsSet;
    private String _modeOfAccomodation;
    private String _modeOfAccomodationInitVal;
    private boolean _modeOfAccomodationIsSet;
    private String _paidAlimony;
    private String _paidAlimonyInitVal;
    private boolean _paidAlimonyIsSet;
    private String _paidRent;
    private String _paidRentInitVal;
    private boolean _paidRentIsSet;
    private String _expensesCar;
    private String _expensesCarInitVal;
    private boolean _expensesCarIsSet;
    private String _numOfCars;
    private String _numOfCarsInitVal;
    private boolean _numOfCarsIsSet;
    private Boolean _paidAlimonyRadio;
    private Boolean _paidAlimonyRadioInitVal;
    private boolean _paidAlimonyRadioIsSet;
    private String _leasing;
    private String _leasingInitVal;
    private boolean _leasingIsSet;
    private Boolean _leasingRadio;
    private Boolean _leasingRadioInitVal;
    private boolean _leasingRadioIsSet;
    private String _creditExpenses;
    private String _creditExpensesInitVal;
    private boolean _creditExpensesIsSet;
    private String _totalPrivateCreditAmount;
    private String _totalPrivateCreditAmountInitVal;
    private boolean _totalPrivateCreditAmountIsSet;
    private Boolean _creditExpensesRadio;
    private Boolean _creditExpensesRadioInitVal;
    private boolean _creditExpensesRadioIsSet;
    private String _savings;
    private String _savingsInitVal;
    private boolean _savingsIsSet;
    private Boolean _savingsRadio;
    private Boolean _savingsRadioInitVal;
    private boolean _savingsRadioIsSet;
    private String _numberOfChildren;
    private String _numberOfChildrenInitVal;
    private boolean _numberOfChildrenIsSet;
    private String _numberOfOtherInHousehold;
    private String _numberOfOtherInHouseholdInitVal;
    private boolean _numberOfOtherInHouseholdIsSet;
    private String _numberOfOtherInHouseholdCheck;
    private String _numberOfOtherInHouseholdCheckInitVal;
    private boolean _numberOfOtherInHouseholdCheckIsSet;
    private String _insurance;
    private String _insuranceInitVal;
    private boolean _insuranceIsSet;
    private String _privateHealthInsurance;
    private String _privateHealthInsuranceInitVal;
    private boolean _privateHealthInsuranceIsSet;
    private Boolean _privateHealthInsuranceRadio;
    private Boolean _privateHealthInsuranceRadioInitVal;
    private boolean _privateHealthInsuranceRadioIsSet;
    private String _mortgage;
    private String _mortgageInitVal;
    private boolean _mortgageIsSet;
    private Boolean _mortgageRadio;
    private Boolean _mortgageRadioInitVal;
    private boolean _mortgageRadioIsSet;
    private de.smava.webapp.account.domain.CreditCardType _creditcards;
    private de.smava.webapp.account.domain.CreditCardType _creditcardsInitVal;
    private boolean _creditcardsIsSet;
    private de.smava.webapp.account.domain.PropertyOwnedType _propertyOwned;
    private de.smava.webapp.account.domain.PropertyOwnedType _propertyOwnedInitVal;
    private boolean _propertyOwnedIsSet;
    private String _propertyOwnedSquareMeters;
    private String _propertyOwnedSquareMetersInitVal;
    private boolean _propertyOwnedSquareMetersIsSet;
    private String _freelancerIncomeConfirmationLimitDate;
    private String _freelancerIncomeConfirmationLimitDateInitVal;
    private boolean _freelancerIncomeConfirmationLimitDateIsSet;
    private boolean _paidTradeTax;
    private boolean _paidTradeTaxInitVal;
    private boolean _paidTradeTaxIsSet;
    private boolean _paidChurchTax;
    private boolean _paidChurchTaxInitVal;
    private boolean _paidChurchTaxIsSet;
    private String _businessLoan;
    private String _businessLoanInitVal;
    private boolean _businessLoanIsSet;
    private String _businessLeasing;
    private String _businessLeasingInitVal;
    private boolean _businessLeasingIsSet;
    private String _contractOfEmployment;
    private String _contractOfEmploymentInitVal;
    private boolean _contractOfEmploymentIsSet;
    private String _civilContract;
    private String _civilContractInitVal;
    private boolean _civilContractIsSet;
    private String _businessActivities;
    private String _businessActivitiesInitVal;
    private boolean _businessActivitiesIsSet;
    private Boolean _blacklistCheck;
    private Boolean _blacklistCheckInitVal;
    private boolean _blacklistCheckIsSet;
    private Boolean _sharedBankaccount;
    private Boolean _sharedBankaccountInitVal;
    private boolean _sharedBankaccountIsSet;
    private Boolean _incomeByBankview;
    private Boolean _incomeByBankviewInitVal;
    private boolean _incomeByBankviewIsSet;
    private Boolean _receivedPalimonyByBankview;
    private Boolean _receivedPalimonyByBankviewInitVal;
    private boolean _receivedPalimonyByBankviewIsSet;
    private Boolean _receivedRentByBankview;
    private Boolean _receivedRentByBankviewInitVal;
    private boolean _receivedRentByBankviewIsSet;
    private Boolean _miscEarnsByBankview;
    private Boolean _miscEarnsByBankviewInitVal;
    private boolean _miscEarnsByBankviewIsSet;
    private Boolean _paidRentByBankview;
    private Boolean _paidRentByBankviewInitVal;
    private boolean _paidRentByBankviewIsSet;
    private Boolean _privateHealthInsuranceByBankview;
    private Boolean _privateHealthInsuranceByBankviewInitVal;
    private boolean _privateHealthInsuranceByBankviewIsSet;
    private Boolean _savingsByBankview;
    private Boolean _savingsByBankviewInitVal;
    private boolean _savingsByBankviewIsSet;
    private Boolean _paidAlimonyByBankview;
    private Boolean _paidAlimonyByBankviewInitVal;
    private boolean _paidAlimonyByBankviewIsSet;
    private Boolean _privateLeasingRatesByBankview;
    private Boolean _privateLeasingRatesByBankviewInitVal;
    private boolean _privateLeasingRatesByBankviewIsSet;
    private Boolean _realEstateCreditRatesByBankview;
    private Boolean _realEstateCreditRatesByBankviewInitVal;
    private boolean _realEstateCreditRatesByBankviewIsSet;
    private Boolean _privateCreditRatesByBankview;
    private Boolean _privateCreditRatesByBankviewInitVal;
    private boolean _privateCreditRatesByBankviewIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setOccupation(String occupation) {
        if (!_occupationIsSet) {
            _occupationIsSet = true;
            _occupationInitVal = occupation;
        }
        _occupation = occupation;
    }

    public String getOccupation() {
        return _occupation;
    }

    public String occupationInitVal() {
        return _occupationInitVal;
    }

	public boolean occupationIsDirty() {
        return !valuesAreEqual(_occupationInitVal, _occupation);
    }

    public boolean occupationIsSet() {
        return _occupationIsSet;
    }


    public void setEmployment(String employment) {
        if (!_employmentIsSet) {
            _employmentIsSet = true;
            _employmentInitVal = employment;
        }
        _employment = employment;
    }

    public String getEmployment() {
        return _employment;
    }

    public String employmentInitVal() {
        return _employmentInitVal;
    }

	public boolean employmentIsDirty() {
        return !valuesAreEqual(_employmentInitVal, _employment);
    }

    public boolean employmentIsSet() {
        return _employmentIsSet;
    }


    public void setStartMonthOfOccupation(String startMonthOfOccupation) {
        if (!_startMonthOfOccupationIsSet) {
            _startMonthOfOccupationIsSet = true;
            _startMonthOfOccupationInitVal = startMonthOfOccupation;
        }
        _startMonthOfOccupation = startMonthOfOccupation;
    }

    public String getStartMonthOfOccupation() {
        return _startMonthOfOccupation;
    }

    public String startMonthOfOccupationInitVal() {
        return _startMonthOfOccupationInitVal;
    }

	public boolean startMonthOfOccupationIsDirty() {
        return !valuesAreEqual(_startMonthOfOccupationInitVal, _startMonthOfOccupation);
    }

    public boolean startMonthOfOccupationIsSet() {
        return _startMonthOfOccupationIsSet;
    }


    public void setStartYearOfOccupation(String startYearOfOccupation) {
        if (!_startYearOfOccupationIsSet) {
            _startYearOfOccupationIsSet = true;
            _startYearOfOccupationInitVal = startYearOfOccupation;
        }
        _startYearOfOccupation = startYearOfOccupation;
    }

    public String getStartYearOfOccupation() {
        return _startYearOfOccupation;
    }

    public String startYearOfOccupationInitVal() {
        return _startYearOfOccupationInitVal;
    }

	public boolean startYearOfOccupationIsDirty() {
        return !valuesAreEqual(_startYearOfOccupationInitVal, _startYearOfOccupation);
    }

    public boolean startYearOfOccupationIsSet() {
        return _startYearOfOccupationIsSet;
    }


    public void setSectorName(String sectorName) {
        if (!_sectorNameIsSet) {
            _sectorNameIsSet = true;
            _sectorNameInitVal = sectorName;
        }
        _sectorName = sectorName;
    }

    public String getSectorName() {
        return _sectorName;
    }

    public String sectorNameInitVal() {
        return _sectorNameInitVal;
    }

	public boolean sectorNameIsDirty() {
        return !valuesAreEqual(_sectorNameInitVal, _sectorName);
    }

    public boolean sectorNameIsSet() {
        return _sectorNameIsSet;
    }


    public void setSectorFiltered(String sectorFiltered) {
        if (!_sectorFilteredIsSet) {
            _sectorFilteredIsSet = true;
            _sectorFilteredInitVal = sectorFiltered;
        }
        _sectorFiltered = sectorFiltered;
    }

    public String getSectorFiltered() {
        return _sectorFiltered;
    }

    public String sectorFilteredInitVal() {
        return _sectorFilteredInitVal;
    }

	public boolean sectorFilteredIsDirty() {
        return !valuesAreEqual(_sectorFilteredInitVal, _sectorFiltered);
    }

    public boolean sectorFilteredIsSet() {
        return _sectorFilteredIsSet;
    }


    public void setSectorComposite(String sectorComposite) {
        if (!_sectorCompositeIsSet) {
            _sectorCompositeIsSet = true;
            _sectorCompositeInitVal = sectorComposite;
        }
        _sectorComposite = sectorComposite;
    }

    public String getSectorComposite() {
        return _sectorComposite;
    }

    public String sectorCompositeInitVal() {
        return _sectorCompositeInitVal;
    }

	public boolean sectorCompositeIsDirty() {
        return !valuesAreEqual(_sectorCompositeInitVal, _sectorComposite);
    }

    public boolean sectorCompositeIsSet() {
        return _sectorCompositeIsSet;
    }


    public void setRootSectors(Collection<Sector> rootSectors) {
        if (!_rootSectorsIsSet) {
            _rootSectorsIsSet = true;
            _rootSectorsInitVal = rootSectors;
        }
        _rootSectors = rootSectors;
    }

    public Collection<Sector> getRootSectors() {
        return _rootSectors;
    }

    public Collection<Sector> rootSectorsInitVal() {
        return _rootSectorsInitVal;
    }

	public boolean rootSectorsIsDirty() {
        return !valuesAreEqual(_rootSectorsInitVal, _rootSectors);
    }

    public boolean rootSectorsIsSet() {
        return _rootSectorsIsSet;
    }


    public void setSectorDescription(String sectorDescription) {
        if (!_sectorDescriptionIsSet) {
            _sectorDescriptionIsSet = true;
            _sectorDescriptionInitVal = sectorDescription;
        }
        _sectorDescription = sectorDescription;
    }

    public String getSectorDescription() {
        return _sectorDescription;
    }

    public String sectorDescriptionInitVal() {
        return _sectorDescriptionInitVal;
    }

	public boolean sectorDescriptionIsDirty() {
        return !valuesAreEqual(_sectorDescriptionInitVal, _sectorDescription);
    }

    public boolean sectorDescriptionIsSet() {
        return _sectorDescriptionIsSet;
    }


    public void setTempEmployment(Boolean tempEmployment) {
        if (!_tempEmploymentIsSet) {
            _tempEmploymentIsSet = true;
            _tempEmploymentInitVal = tempEmployment;
        }
        _tempEmployment = tempEmployment;
    }

    public Boolean getTempEmployment() {
        return _tempEmployment;
    }

    public Boolean tempEmploymentInitVal() {
        return _tempEmploymentInitVal;
    }

	public boolean tempEmploymentIsDirty() {
        return !valuesAreEqual(_tempEmploymentInitVal, _tempEmployment);
    }

    public boolean tempEmploymentIsSet() {
        return _tempEmploymentIsSet;
    }


    public void setSharedLoan(boolean sharedLoan) {
        if (!_sharedLoanIsSet) {
            _sharedLoanIsSet = true;
            _sharedLoanInitVal = sharedLoan;
        }
        _sharedLoan = sharedLoan;
    }

    public boolean getSharedLoan() {
        return _sharedLoan;
    }

    public boolean sharedLoanInitVal() {
        return _sharedLoanInitVal;
    }

	public boolean sharedLoanIsDirty() {
        return !valuesAreEqual(_sharedLoanInitVal, _sharedLoan);
    }

    public boolean sharedLoanIsSet() {
        return _sharedLoanIsSet;
    }


    public void setOtherIncome(Boolean otherIncome) {
        if (!_otherIncomeIsSet) {
            _otherIncomeIsSet = true;
            _otherIncomeInitVal = otherIncome;
        }
        _otherIncome = otherIncome;
    }

    public Boolean getOtherIncome() {
        return _otherIncome;
    }

    public Boolean otherIncomeInitVal() {
        return _otherIncomeInitVal;
    }

	public boolean otherIncomeIsDirty() {
        return !valuesAreEqual(_otherIncomeInitVal, _otherIncome);
    }

    public boolean otherIncomeIsSet() {
        return _otherIncomeIsSet;
    }


    public void setOtherEarnDescription(String otherEarnDescription) {
        if (!_otherEarnDescriptionIsSet) {
            _otherEarnDescriptionIsSet = true;
            _otherEarnDescriptionInitVal = otherEarnDescription;
        }
        _otherEarnDescription = otherEarnDescription;
    }

    public String getOtherEarnDescription() {
        return _otherEarnDescription;
    }

    public String otherEarnDescriptionInitVal() {
        return _otherEarnDescriptionInitVal;
    }

	public boolean otherEarnDescriptionIsDirty() {
        return !valuesAreEqual(_otherEarnDescriptionInitVal, _otherEarnDescription);
    }

    public boolean otherEarnDescriptionIsSet() {
        return _otherEarnDescriptionIsSet;
    }


    public void setIncome(String income) {
        if (!_incomeIsSet) {
            _incomeIsSet = true;
            _incomeInitVal = income;
        }
        _income = income;
    }

    public String getIncome() {
        return _income;
    }

    public String incomeInitVal() {
        return _incomeInitVal;
    }
    public void setReceivedPalimony(String receivedPalimony) {
        if (!_receivedPalimonyIsSet) {
            _receivedPalimonyIsSet = true;
            _receivedPalimonyInitVal = receivedPalimony;
        }
        _receivedPalimony = receivedPalimony;
    }

    public String getReceivedPalimony() {
        return _receivedPalimony;
    }

    public String receivedPalimonyInitVal() {
        return _receivedPalimonyInitVal;
    }
    public void setMiscEarnType(de.smava.webapp.account.domain.MiscEarnType miscEarnType) {
        if (!_miscEarnTypeIsSet) {
            _miscEarnTypeIsSet = true;
            _miscEarnTypeInitVal = miscEarnType;
        }
        _miscEarnType = miscEarnType;
    }

    public de.smava.webapp.account.domain.MiscEarnType getMiscEarnType() {
        return _miscEarnType;
    }

    public de.smava.webapp.account.domain.MiscEarnType miscEarnTypeInitVal() {
        return _miscEarnTypeInitVal;
    }
    public void setMiscEarnAmount1(String miscEarnAmount1) {
        if (!_miscEarnAmount1IsSet) {
            _miscEarnAmount1IsSet = true;
            _miscEarnAmount1InitVal = miscEarnAmount1;
        }
        _miscEarnAmount1 = miscEarnAmount1;
    }

    public String getMiscEarnAmount1() {
        return _miscEarnAmount1;
    }

    public String miscEarnAmount1InitVal() {
        return _miscEarnAmount1InitVal;
    }
    public void setReceivedRent(String receivedRent) {
        if (!_receivedRentIsSet) {
            _receivedRentIsSet = true;
            _receivedRentInitVal = receivedRent;
        }
        _receivedRent = receivedRent;
    }

    public String getReceivedRent() {
        return _receivedRent;
    }

    public String receivedRentInitVal() {
        return _receivedRentInitVal;
    }

	public boolean incomeDataSetIsDirty() {
		boolean result = false;

		result = result || incomeIsDirty();		
		result = result || receivedPalimonyIsDirty();		
		result = result || miscEarnTypeIsDirty();		
		result = result || miscEarnAmount1IsDirty();		
		result = result || receivedRentIsDirty();		
	
		return result;
	}
	
	public boolean incomeIsDirty() {
        return !valuesAreEqual(_incomeInitVal, _income);
    }

    public boolean incomeIsSet() {
        return _incomeIsSet;
    }	
	public boolean receivedPalimonyIsDirty() {
        return !valuesAreEqual(_receivedPalimonyInitVal, _receivedPalimony);
    }

    public boolean receivedPalimonyIsSet() {
        return _receivedPalimonyIsSet;
    }	
	public boolean miscEarnTypeIsDirty() {
        return !valuesAreEqual(_miscEarnTypeInitVal, _miscEarnType);
    }

    public boolean miscEarnTypeIsSet() {
        return _miscEarnTypeIsSet;
    }	
	public boolean miscEarnAmount1IsDirty() {
        return !valuesAreEqual(_miscEarnAmount1InitVal, _miscEarnAmount1);
    }

    public boolean miscEarnAmount1IsSet() {
        return _miscEarnAmount1IsSet;
    }	
	public boolean receivedRentIsDirty() {
        return !valuesAreEqual(_receivedRentInitVal, _receivedRent);
    }

    public boolean receivedRentIsSet() {
        return _receivedRentIsSet;
    }	


    public void setModeOfAccomodation(String modeOfAccomodation) {
        if (!_modeOfAccomodationIsSet) {
            _modeOfAccomodationIsSet = true;
            _modeOfAccomodationInitVal = modeOfAccomodation;
        }
        _modeOfAccomodation = modeOfAccomodation;
    }

    public String getModeOfAccomodation() {
        return _modeOfAccomodation;
    }

    public String modeOfAccomodationInitVal() {
        return _modeOfAccomodationInitVal;
    }

	public boolean modeOfAccomodationIsDirty() {
        return !valuesAreEqual(_modeOfAccomodationInitVal, _modeOfAccomodation);
    }

    public boolean modeOfAccomodationIsSet() {
        return _modeOfAccomodationIsSet;
    }	


    public void setPaidAlimony(String paidAlimony) {
        if (!_paidAlimonyIsSet) {
            _paidAlimonyIsSet = true;
            _paidAlimonyInitVal = paidAlimony;
        }
        _paidAlimony = paidAlimony;
    }

    public String getPaidAlimony() {
        return _paidAlimony;
    }

    public String paidAlimonyInitVal() {
        return _paidAlimonyInitVal;
    }

	public boolean paidAlimonyIsDirty() {
        return !valuesAreEqual(_paidAlimonyInitVal, _paidAlimony);
    }

    public boolean paidAlimonyIsSet() {
        return _paidAlimonyIsSet;
    }	


    public void setPaidRent(String paidRent) {
        if (!_paidRentIsSet) {
            _paidRentIsSet = true;
            _paidRentInitVal = paidRent;
        }
        _paidRent = paidRent;
    }

    public String getPaidRent() {
        return _paidRent;
    }

    public String paidRentInitVal() {
        return _paidRentInitVal;
    }

	public boolean paidRentIsDirty() {
        return !valuesAreEqual(_paidRentInitVal, _paidRent);
    }

    public boolean paidRentIsSet() {
        return _paidRentIsSet;
    }	


    public void setExpensesCar(String expensesCar) {
        if (!_expensesCarIsSet) {
            _expensesCarIsSet = true;
            _expensesCarInitVal = expensesCar;
        }
        _expensesCar = expensesCar;
    }

    public String getExpensesCar() {
        return _expensesCar;
    }

    public String expensesCarInitVal() {
        return _expensesCarInitVal;
    }

	public boolean expensesCarIsDirty() {
        return !valuesAreEqual(_expensesCarInitVal, _expensesCar);
    }

    public boolean expensesCarIsSet() {
        return _expensesCarIsSet;
    }	


    public void setNumOfCars(String numOfCars) {
        if (!_numOfCarsIsSet) {
            _numOfCarsIsSet = true;
            _numOfCarsInitVal = numOfCars;
        }
        _numOfCars = numOfCars;
    }

    public String getNumOfCars() {
        return _numOfCars;
    }

    public String numOfCarsInitVal() {
        return _numOfCarsInitVal;
    }

	public boolean numOfCarsIsDirty() {
        return !valuesAreEqual(_numOfCarsInitVal, _numOfCars);
    }

    public boolean numOfCarsIsSet() {
        return _numOfCarsIsSet;
    }	


    public void setPaidAlimonyRadio(Boolean paidAlimonyRadio) {
        if (!_paidAlimonyRadioIsSet) {
            _paidAlimonyRadioIsSet = true;
            _paidAlimonyRadioInitVal = paidAlimonyRadio;
        }
        _paidAlimonyRadio = paidAlimonyRadio;
    }

    public Boolean getPaidAlimonyRadio() {
        return _paidAlimonyRadio;
    }

    public Boolean paidAlimonyRadioInitVal() {
        return _paidAlimonyRadioInitVal;
    }

	public boolean paidAlimonyRadioIsDirty() {
        return !valuesAreEqual(_paidAlimonyRadioInitVal, _paidAlimonyRadio);
    }

    public boolean paidAlimonyRadioIsSet() {
        return _paidAlimonyRadioIsSet;
    }	


    public void setLeasing(String leasing) {
        if (!_leasingIsSet) {
            _leasingIsSet = true;
            _leasingInitVal = leasing;
        }
        _leasing = leasing;
    }

    public String getLeasing() {
        return _leasing;
    }

    public String leasingInitVal() {
        return _leasingInitVal;
    }

	public boolean leasingIsDirty() {
        return !valuesAreEqual(_leasingInitVal, _leasing);
    }

    public boolean leasingIsSet() {
        return _leasingIsSet;
    }	


    public void setLeasingRadio(Boolean leasingRadio) {
        if (!_leasingRadioIsSet) {
            _leasingRadioIsSet = true;
            _leasingRadioInitVal = leasingRadio;
        }
        _leasingRadio = leasingRadio;
    }

    public Boolean getLeasingRadio() {
        return _leasingRadio;
    }

    public Boolean leasingRadioInitVal() {
        return _leasingRadioInitVal;
    }

	public boolean leasingRadioIsDirty() {
        return !valuesAreEqual(_leasingRadioInitVal, _leasingRadio);
    }

    public boolean leasingRadioIsSet() {
        return _leasingRadioIsSet;
    }	


    public void setCreditExpenses(String creditExpenses) {
        if (!_creditExpensesIsSet) {
            _creditExpensesIsSet = true;
            _creditExpensesInitVal = creditExpenses;
        }
        _creditExpenses = creditExpenses;
    }

    public String getCreditExpenses() {
        return _creditExpenses;
    }

    public String creditExpensesInitVal() {
        return _creditExpensesInitVal;
    }

	public boolean creditExpensesIsDirty() {
        return !valuesAreEqual(_creditExpensesInitVal, _creditExpenses);
    }

    public boolean creditExpensesIsSet() {
        return _creditExpensesIsSet;
    }	


    public void setTotalPrivateCreditAmount(String totalPrivateCreditAmount) {
        if (!_totalPrivateCreditAmountIsSet) {
            _totalPrivateCreditAmountIsSet = true;
            _totalPrivateCreditAmountInitVal = totalPrivateCreditAmount;
        }
        _totalPrivateCreditAmount = totalPrivateCreditAmount;
    }

    public String getTotalPrivateCreditAmount() {
        return _totalPrivateCreditAmount;
    }

    public String totalPrivateCreditAmountInitVal() {
        return _totalPrivateCreditAmountInitVal;
    }

	public boolean totalPrivateCreditAmountIsDirty() {
        return !valuesAreEqual(_totalPrivateCreditAmountInitVal, _totalPrivateCreditAmount);
    }

    public boolean totalPrivateCreditAmountIsSet() {
        return _totalPrivateCreditAmountIsSet;
    }	


    public void setCreditExpensesRadio(Boolean creditExpensesRadio) {
        if (!_creditExpensesRadioIsSet) {
            _creditExpensesRadioIsSet = true;
            _creditExpensesRadioInitVal = creditExpensesRadio;
        }
        _creditExpensesRadio = creditExpensesRadio;
    }

    public Boolean getCreditExpensesRadio() {
        return _creditExpensesRadio;
    }

    public Boolean creditExpensesRadioInitVal() {
        return _creditExpensesRadioInitVal;
    }

	public boolean creditExpensesRadioIsDirty() {
        return !valuesAreEqual(_creditExpensesRadioInitVal, _creditExpensesRadio);
    }

    public boolean creditExpensesRadioIsSet() {
        return _creditExpensesRadioIsSet;
    }	


    public void setSavings(String savings) {
        if (!_savingsIsSet) {
            _savingsIsSet = true;
            _savingsInitVal = savings;
        }
        _savings = savings;
    }

    public String getSavings() {
        return _savings;
    }

    public String savingsInitVal() {
        return _savingsInitVal;
    }

	public boolean savingsIsDirty() {
        return !valuesAreEqual(_savingsInitVal, _savings);
    }

    public boolean savingsIsSet() {
        return _savingsIsSet;
    }	


    public void setSavingsRadio(Boolean savingsRadio) {
        if (!_savingsRadioIsSet) {
            _savingsRadioIsSet = true;
            _savingsRadioInitVal = savingsRadio;
        }
        _savingsRadio = savingsRadio;
    }

    public Boolean getSavingsRadio() {
        return _savingsRadio;
    }

    public Boolean savingsRadioInitVal() {
        return _savingsRadioInitVal;
    }

	public boolean savingsRadioIsDirty() {
        return !valuesAreEqual(_savingsRadioInitVal, _savingsRadio);
    }

    public boolean savingsRadioIsSet() {
        return _savingsRadioIsSet;
    }	


    public void setNumberOfChildren(String numberOfChildren) {
        if (!_numberOfChildrenIsSet) {
            _numberOfChildrenIsSet = true;
            _numberOfChildrenInitVal = numberOfChildren;
        }
        _numberOfChildren = numberOfChildren;
    }

    public String getNumberOfChildren() {
        return _numberOfChildren;
    }

    public String numberOfChildrenInitVal() {
        return _numberOfChildrenInitVal;
    }

	public boolean numberOfChildrenIsDirty() {
        return !valuesAreEqual(_numberOfChildrenInitVal, _numberOfChildren);
    }

    public boolean numberOfChildrenIsSet() {
        return _numberOfChildrenIsSet;
    }	


    public void setNumberOfOtherInHousehold(String numberOfOtherInHousehold) {
        if (!_numberOfOtherInHouseholdIsSet) {
            _numberOfOtherInHouseholdIsSet = true;
            _numberOfOtherInHouseholdInitVal = numberOfOtherInHousehold;
        }
        _numberOfOtherInHousehold = numberOfOtherInHousehold;
    }

    public String getNumberOfOtherInHousehold() {
        return _numberOfOtherInHousehold;
    }

    public String numberOfOtherInHouseholdInitVal() {
        return _numberOfOtherInHouseholdInitVal;
    }

	public boolean numberOfOtherInHouseholdIsDirty() {
        return !valuesAreEqual(_numberOfOtherInHouseholdInitVal, _numberOfOtherInHousehold);
    }

    public boolean numberOfOtherInHouseholdIsSet() {
        return _numberOfOtherInHouseholdIsSet;
    }	


    public void setNumberOfOtherInHouseholdCheck(String numberOfOtherInHouseholdCheck) {
        if (!_numberOfOtherInHouseholdCheckIsSet) {
            _numberOfOtherInHouseholdCheckIsSet = true;
            _numberOfOtherInHouseholdCheckInitVal = numberOfOtherInHouseholdCheck;
        }
        _numberOfOtherInHouseholdCheck = numberOfOtherInHouseholdCheck;
    }

    public String getNumberOfOtherInHouseholdCheck() {
        return _numberOfOtherInHouseholdCheck;
    }

    public String numberOfOtherInHouseholdCheckInitVal() {
        return _numberOfOtherInHouseholdCheckInitVal;
    }

	public boolean numberOfOtherInHouseholdCheckIsDirty() {
        return !valuesAreEqual(_numberOfOtherInHouseholdCheckInitVal, _numberOfOtherInHouseholdCheck);
    }

    public boolean numberOfOtherInHouseholdCheckIsSet() {
        return _numberOfOtherInHouseholdCheckIsSet;
    }	


    public void setInsurance(String insurance) {
        if (!_insuranceIsSet) {
            _insuranceIsSet = true;
            _insuranceInitVal = insurance;
        }
        _insurance = insurance;
    }

    public String getInsurance() {
        return _insurance;
    }

    public String insuranceInitVal() {
        return _insuranceInitVal;
    }

	public boolean insuranceIsDirty() {
        return !valuesAreEqual(_insuranceInitVal, _insurance);
    }

    public boolean insuranceIsSet() {
        return _insuranceIsSet;
    }	


    public void setPrivateHealthInsurance(String privateHealthInsurance) {
        if (!_privateHealthInsuranceIsSet) {
            _privateHealthInsuranceIsSet = true;
            _privateHealthInsuranceInitVal = privateHealthInsurance;
        }
        _privateHealthInsurance = privateHealthInsurance;
    }

    public String getPrivateHealthInsurance() {
        return _privateHealthInsurance;
    }

    public String privateHealthInsuranceInitVal() {
        return _privateHealthInsuranceInitVal;
    }

	public boolean privateHealthInsuranceIsDirty() {
        return !valuesAreEqual(_privateHealthInsuranceInitVal, _privateHealthInsurance);
    }

    public boolean privateHealthInsuranceIsSet() {
        return _privateHealthInsuranceIsSet;
    }	


    public void setPrivateHealthInsuranceRadio(Boolean privateHealthInsuranceRadio) {
        if (!_privateHealthInsuranceRadioIsSet) {
            _privateHealthInsuranceRadioIsSet = true;
            _privateHealthInsuranceRadioInitVal = privateHealthInsuranceRadio;
        }
        _privateHealthInsuranceRadio = privateHealthInsuranceRadio;
    }

    public Boolean getPrivateHealthInsuranceRadio() {
        return _privateHealthInsuranceRadio;
    }

    public Boolean privateHealthInsuranceRadioInitVal() {
        return _privateHealthInsuranceRadioInitVal;
    }

	public boolean privateHealthInsuranceRadioIsDirty() {
        return !valuesAreEqual(_privateHealthInsuranceRadioInitVal, _privateHealthInsuranceRadio);
    }

    public boolean privateHealthInsuranceRadioIsSet() {
        return _privateHealthInsuranceRadioIsSet;
    }	


    public void setMortgage(String mortgage) {
        if (!_mortgageIsSet) {
            _mortgageIsSet = true;
            _mortgageInitVal = mortgage;
        }
        _mortgage = mortgage;
    }

    public String getMortgage() {
        return _mortgage;
    }

    public String mortgageInitVal() {
        return _mortgageInitVal;
    }

	public boolean mortgageIsDirty() {
        return !valuesAreEqual(_mortgageInitVal, _mortgage);
    }

    public boolean mortgageIsSet() {
        return _mortgageIsSet;
    }	


    public void setMortgageRadio(Boolean mortgageRadio) {
        if (!_mortgageRadioIsSet) {
            _mortgageRadioIsSet = true;
            _mortgageRadioInitVal = mortgageRadio;
        }
        _mortgageRadio = mortgageRadio;
    }

    public Boolean getMortgageRadio() {
        return _mortgageRadio;
    }

    public Boolean mortgageRadioInitVal() {
        return _mortgageRadioInitVal;
    }

	public boolean mortgageRadioIsDirty() {
        return !valuesAreEqual(_mortgageRadioInitVal, _mortgageRadio);
    }

    public boolean mortgageRadioIsSet() {
        return _mortgageRadioIsSet;
    }	


    public void setCreditcards(de.smava.webapp.account.domain.CreditCardType creditcards) {
        if (!_creditcardsIsSet) {
            _creditcardsIsSet = true;
            _creditcardsInitVal = creditcards;
        }
        _creditcards = creditcards;
    }

    public de.smava.webapp.account.domain.CreditCardType getCreditcards() {
        return _creditcards;
    }

    public de.smava.webapp.account.domain.CreditCardType creditcardsInitVal() {
        return _creditcardsInitVal;
    }

	public boolean creditcardsIsDirty() {
        return !valuesAreEqual(_creditcardsInitVal, _creditcards);
    }

    public boolean creditcardsIsSet() {
        return _creditcardsIsSet;
    }	


    public void setPropertyOwned(de.smava.webapp.account.domain.PropertyOwnedType propertyOwned) {
        if (!_propertyOwnedIsSet) {
            _propertyOwnedIsSet = true;
            _propertyOwnedInitVal = propertyOwned;
        }
        _propertyOwned = propertyOwned;
    }

    public de.smava.webapp.account.domain.PropertyOwnedType getPropertyOwned() {
        return _propertyOwned;
    }

    public de.smava.webapp.account.domain.PropertyOwnedType propertyOwnedInitVal() {
        return _propertyOwnedInitVal;
    }

	public boolean propertyOwnedIsDirty() {
        return !valuesAreEqual(_propertyOwnedInitVal, _propertyOwned);
    }

    public boolean propertyOwnedIsSet() {
        return _propertyOwnedIsSet;
    }	


    public void setPropertyOwnedSquareMeters(String propertyOwnedSquareMeters) {
        if (!_propertyOwnedSquareMetersIsSet) {
            _propertyOwnedSquareMetersIsSet = true;
            _propertyOwnedSquareMetersInitVal = propertyOwnedSquareMeters;
        }
        _propertyOwnedSquareMeters = propertyOwnedSquareMeters;
    }

    public String getPropertyOwnedSquareMeters() {
        return _propertyOwnedSquareMeters;
    }

    public String propertyOwnedSquareMetersInitVal() {
        return _propertyOwnedSquareMetersInitVal;
    }

	public boolean propertyOwnedSquareMetersIsDirty() {
        return !valuesAreEqual(_propertyOwnedSquareMetersInitVal, _propertyOwnedSquareMeters);
    }

    public boolean propertyOwnedSquareMetersIsSet() {
        return _propertyOwnedSquareMetersIsSet;
    }	


    public void setFreelancerIncomeConfirmationLimitDate(String freelancerIncomeConfirmationLimitDate) {
        if (!_freelancerIncomeConfirmationLimitDateIsSet) {
            _freelancerIncomeConfirmationLimitDateIsSet = true;
            _freelancerIncomeConfirmationLimitDateInitVal = freelancerIncomeConfirmationLimitDate;
        }
        _freelancerIncomeConfirmationLimitDate = freelancerIncomeConfirmationLimitDate;
    }

    public String getFreelancerIncomeConfirmationLimitDate() {
        return _freelancerIncomeConfirmationLimitDate;
    }

    public String freelancerIncomeConfirmationLimitDateInitVal() {
        return _freelancerIncomeConfirmationLimitDateInitVal;
    }

	public boolean freelancerIncomeConfirmationLimitDateIsDirty() {
        return !valuesAreEqual(_freelancerIncomeConfirmationLimitDateInitVal, _freelancerIncomeConfirmationLimitDate);
    }

    public boolean freelancerIncomeConfirmationLimitDateIsSet() {
        return _freelancerIncomeConfirmationLimitDateIsSet;
    }	


    public void setPaidTradeTax(boolean paidTradeTax) {
        if (!_paidTradeTaxIsSet) {
            _paidTradeTaxIsSet = true;
            _paidTradeTaxInitVal = paidTradeTax;
        }
        _paidTradeTax = paidTradeTax;
    }

    public boolean getPaidTradeTax() {
        return _paidTradeTax;
    }

    public boolean paidTradeTaxInitVal() {
        return _paidTradeTaxInitVal;
    }

	public boolean paidTradeTaxIsDirty() {
        return !valuesAreEqual(_paidTradeTaxInitVal, _paidTradeTax);
    }

    public boolean paidTradeTaxIsSet() {
        return _paidTradeTaxIsSet;
    }	


    public void setPaidChurchTax(boolean paidChurchTax) {
        if (!_paidChurchTaxIsSet) {
            _paidChurchTaxIsSet = true;
            _paidChurchTaxInitVal = paidChurchTax;
        }
        _paidChurchTax = paidChurchTax;
    }

    public boolean getPaidChurchTax() {
        return _paidChurchTax;
    }

    public boolean paidChurchTaxInitVal() {
        return _paidChurchTaxInitVal;
    }

	public boolean paidChurchTaxIsDirty() {
        return !valuesAreEqual(_paidChurchTaxInitVal, _paidChurchTax);
    }

    public boolean paidChurchTaxIsSet() {
        return _paidChurchTaxIsSet;
    }	


    public void setBusinessLoan(String businessLoan) {
        if (!_businessLoanIsSet) {
            _businessLoanIsSet = true;
            _businessLoanInitVal = businessLoan;
        }
        _businessLoan = businessLoan;
    }

    public String getBusinessLoan() {
        return _businessLoan;
    }

    public String businessLoanInitVal() {
        return _businessLoanInitVal;
    }

	public boolean businessLoanIsDirty() {
        return !valuesAreEqual(_businessLoanInitVal, _businessLoan);
    }

    public boolean businessLoanIsSet() {
        return _businessLoanIsSet;
    }	


    public void setBusinessLeasing(String businessLeasing) {
        if (!_businessLeasingIsSet) {
            _businessLeasingIsSet = true;
            _businessLeasingInitVal = businessLeasing;
        }
        _businessLeasing = businessLeasing;
    }

    public String getBusinessLeasing() {
        return _businessLeasing;
    }

    public String businessLeasingInitVal() {
        return _businessLeasingInitVal;
    }

	public boolean businessLeasingIsDirty() {
        return !valuesAreEqual(_businessLeasingInitVal, _businessLeasing);
    }

    public boolean businessLeasingIsSet() {
        return _businessLeasingIsSet;
    }	


    public void setContractOfEmployment(String contractOfEmployment) {
        if (!_contractOfEmploymentIsSet) {
            _contractOfEmploymentIsSet = true;
            _contractOfEmploymentInitVal = contractOfEmployment;
        }
        _contractOfEmployment = contractOfEmployment;
    }

    public String getContractOfEmployment() {
        return _contractOfEmployment;
    }

    public String contractOfEmploymentInitVal() {
        return _contractOfEmploymentInitVal;
    }

	public boolean contractOfEmploymentIsDirty() {
        return !valuesAreEqual(_contractOfEmploymentInitVal, _contractOfEmployment);
    }

    public boolean contractOfEmploymentIsSet() {
        return _contractOfEmploymentIsSet;
    }	


    public void setCivilContract(String civilContract) {
        if (!_civilContractIsSet) {
            _civilContractIsSet = true;
            _civilContractInitVal = civilContract;
        }
        _civilContract = civilContract;
    }

    public String getCivilContract() {
        return _civilContract;
    }

    public String civilContractInitVal() {
        return _civilContractInitVal;
    }

	public boolean civilContractIsDirty() {
        return !valuesAreEqual(_civilContractInitVal, _civilContract);
    }

    public boolean civilContractIsSet() {
        return _civilContractIsSet;
    }	


    public void setBusinessActivities(String businessActivities) {
        if (!_businessActivitiesIsSet) {
            _businessActivitiesIsSet = true;
            _businessActivitiesInitVal = businessActivities;
        }
        _businessActivities = businessActivities;
    }

    public String getBusinessActivities() {
        return _businessActivities;
    }

    public String businessActivitiesInitVal() {
        return _businessActivitiesInitVal;
    }

	public boolean businessActivitiesIsDirty() {
        return !valuesAreEqual(_businessActivitiesInitVal, _businessActivities);
    }

    public boolean businessActivitiesIsSet() {
        return _businessActivitiesIsSet;
    }	


    public void setBlacklistCheck(Boolean blacklistCheck) {
        if (!_blacklistCheckIsSet) {
            _blacklistCheckIsSet = true;
            _blacklistCheckInitVal = blacklistCheck;
        }
        _blacklistCheck = blacklistCheck;
    }

    public Boolean getBlacklistCheck() {
        return _blacklistCheck;
    }

    public Boolean blacklistCheckInitVal() {
        return _blacklistCheckInitVal;
    }

	public boolean blacklistCheckIsDirty() {
        return !valuesAreEqual(_blacklistCheckInitVal, _blacklistCheck);
    }

    public boolean blacklistCheckIsSet() {
        return _blacklistCheckIsSet;
    }	


    public void setSharedBankaccount(Boolean sharedBankaccount) {
        if (!_sharedBankaccountIsSet) {
            _sharedBankaccountIsSet = true;
            _sharedBankaccountInitVal = sharedBankaccount;
        }
        _sharedBankaccount = sharedBankaccount;
    }

    public Boolean getSharedBankaccount() {
        return _sharedBankaccount;
    }

    public Boolean sharedBankaccountInitVal() {
        return _sharedBankaccountInitVal;
    }

	public boolean sharedBankaccountIsDirty() {
        return !valuesAreEqual(_sharedBankaccountInitVal, _sharedBankaccount);
    }

    public boolean sharedBankaccountIsSet() {
        return _sharedBankaccountIsSet;
    }	


    public void setIncomeByBankview(Boolean incomeByBankview) {
        if (!_incomeByBankviewIsSet) {
            _incomeByBankviewIsSet = true;
            _incomeByBankviewInitVal = incomeByBankview;
        }
        _incomeByBankview = incomeByBankview;
    }

    public Boolean getIncomeByBankview() {
        return _incomeByBankview;
    }

    public Boolean incomeByBankviewInitVal() {
        return _incomeByBankviewInitVal;
    }

	public boolean incomeByBankviewIsDirty() {
        return !valuesAreEqual(_incomeByBankviewInitVal, _incomeByBankview);
    }

    public boolean incomeByBankviewIsSet() {
        return _incomeByBankviewIsSet;
    }	


    public void setReceivedPalimonyByBankview(Boolean receivedPalimonyByBankview) {
        if (!_receivedPalimonyByBankviewIsSet) {
            _receivedPalimonyByBankviewIsSet = true;
            _receivedPalimonyByBankviewInitVal = receivedPalimonyByBankview;
        }
        _receivedPalimonyByBankview = receivedPalimonyByBankview;
    }

    public Boolean getReceivedPalimonyByBankview() {
        return _receivedPalimonyByBankview;
    }

    public Boolean receivedPalimonyByBankviewInitVal() {
        return _receivedPalimonyByBankviewInitVal;
    }

	public boolean receivedPalimonyByBankviewIsDirty() {
        return !valuesAreEqual(_receivedPalimonyByBankviewInitVal, _receivedPalimonyByBankview);
    }

    public boolean receivedPalimonyByBankviewIsSet() {
        return _receivedPalimonyByBankviewIsSet;
    }	


    public void setReceivedRentByBankview(Boolean receivedRentByBankview) {
        if (!_receivedRentByBankviewIsSet) {
            _receivedRentByBankviewIsSet = true;
            _receivedRentByBankviewInitVal = receivedRentByBankview;
        }
        _receivedRentByBankview = receivedRentByBankview;
    }

    public Boolean getReceivedRentByBankview() {
        return _receivedRentByBankview;
    }

    public Boolean receivedRentByBankviewInitVal() {
        return _receivedRentByBankviewInitVal;
    }

	public boolean receivedRentByBankviewIsDirty() {
        return !valuesAreEqual(_receivedRentByBankviewInitVal, _receivedRentByBankview);
    }

    public boolean receivedRentByBankviewIsSet() {
        return _receivedRentByBankviewIsSet;
    }	


    public void setMiscEarnsByBankview(Boolean miscEarnsByBankview) {
        if (!_miscEarnsByBankviewIsSet) {
            _miscEarnsByBankviewIsSet = true;
            _miscEarnsByBankviewInitVal = miscEarnsByBankview;
        }
        _miscEarnsByBankview = miscEarnsByBankview;
    }

    public Boolean getMiscEarnsByBankview() {
        return _miscEarnsByBankview;
    }

    public Boolean miscEarnsByBankviewInitVal() {
        return _miscEarnsByBankviewInitVal;
    }

	public boolean miscEarnsByBankviewIsDirty() {
        return !valuesAreEqual(_miscEarnsByBankviewInitVal, _miscEarnsByBankview);
    }

    public boolean miscEarnsByBankviewIsSet() {
        return _miscEarnsByBankviewIsSet;
    }	


    public void setPaidRentByBankview(Boolean paidRentByBankview) {
        if (!_paidRentByBankviewIsSet) {
            _paidRentByBankviewIsSet = true;
            _paidRentByBankviewInitVal = paidRentByBankview;
        }
        _paidRentByBankview = paidRentByBankview;
    }

    public Boolean getPaidRentByBankview() {
        return _paidRentByBankview;
    }

    public Boolean paidRentByBankviewInitVal() {
        return _paidRentByBankviewInitVal;
    }

	public boolean paidRentByBankviewIsDirty() {
        return !valuesAreEqual(_paidRentByBankviewInitVal, _paidRentByBankview);
    }

    public boolean paidRentByBankviewIsSet() {
        return _paidRentByBankviewIsSet;
    }	


    public void setPrivateHealthInsuranceByBankview(Boolean privateHealthInsuranceByBankview) {
        if (!_privateHealthInsuranceByBankviewIsSet) {
            _privateHealthInsuranceByBankviewIsSet = true;
            _privateHealthInsuranceByBankviewInitVal = privateHealthInsuranceByBankview;
        }
        _privateHealthInsuranceByBankview = privateHealthInsuranceByBankview;
    }

    public Boolean getPrivateHealthInsuranceByBankview() {
        return _privateHealthInsuranceByBankview;
    }

    public Boolean privateHealthInsuranceByBankviewInitVal() {
        return _privateHealthInsuranceByBankviewInitVal;
    }

	public boolean privateHealthInsuranceByBankviewIsDirty() {
        return !valuesAreEqual(_privateHealthInsuranceByBankviewInitVal, _privateHealthInsuranceByBankview);
    }

    public boolean privateHealthInsuranceByBankviewIsSet() {
        return _privateHealthInsuranceByBankviewIsSet;
    }	


    public void setSavingsByBankview(Boolean savingsByBankview) {
        if (!_savingsByBankviewIsSet) {
            _savingsByBankviewIsSet = true;
            _savingsByBankviewInitVal = savingsByBankview;
        }
        _savingsByBankview = savingsByBankview;
    }

    public Boolean getSavingsByBankview() {
        return _savingsByBankview;
    }

    public Boolean savingsByBankviewInitVal() {
        return _savingsByBankviewInitVal;
    }

	public boolean savingsByBankviewIsDirty() {
        return !valuesAreEqual(_savingsByBankviewInitVal, _savingsByBankview);
    }

    public boolean savingsByBankviewIsSet() {
        return _savingsByBankviewIsSet;
    }	


    public void setPaidAlimonyByBankview(Boolean paidAlimonyByBankview) {
        if (!_paidAlimonyByBankviewIsSet) {
            _paidAlimonyByBankviewIsSet = true;
            _paidAlimonyByBankviewInitVal = paidAlimonyByBankview;
        }
        _paidAlimonyByBankview = paidAlimonyByBankview;
    }

    public Boolean getPaidAlimonyByBankview() {
        return _paidAlimonyByBankview;
    }

    public Boolean paidAlimonyByBankviewInitVal() {
        return _paidAlimonyByBankviewInitVal;
    }

	public boolean paidAlimonyByBankviewIsDirty() {
        return !valuesAreEqual(_paidAlimonyByBankviewInitVal, _paidAlimonyByBankview);
    }

    public boolean paidAlimonyByBankviewIsSet() {
        return _paidAlimonyByBankviewIsSet;
    }	


    public void setPrivateLeasingRatesByBankview(Boolean privateLeasingRatesByBankview) {
        if (!_privateLeasingRatesByBankviewIsSet) {
            _privateLeasingRatesByBankviewIsSet = true;
            _privateLeasingRatesByBankviewInitVal = privateLeasingRatesByBankview;
        }
        _privateLeasingRatesByBankview = privateLeasingRatesByBankview;
    }

    public Boolean getPrivateLeasingRatesByBankview() {
        return _privateLeasingRatesByBankview;
    }

    public Boolean privateLeasingRatesByBankviewInitVal() {
        return _privateLeasingRatesByBankviewInitVal;
    }

	public boolean privateLeasingRatesByBankviewIsDirty() {
        return !valuesAreEqual(_privateLeasingRatesByBankviewInitVal, _privateLeasingRatesByBankview);
    }

    public boolean privateLeasingRatesByBankviewIsSet() {
        return _privateLeasingRatesByBankviewIsSet;
    }	


    public void setRealEstateCreditRatesByBankview(Boolean realEstateCreditRatesByBankview) {
        if (!_realEstateCreditRatesByBankviewIsSet) {
            _realEstateCreditRatesByBankviewIsSet = true;
            _realEstateCreditRatesByBankviewInitVal = realEstateCreditRatesByBankview;
        }
        _realEstateCreditRatesByBankview = realEstateCreditRatesByBankview;
    }

    public Boolean getRealEstateCreditRatesByBankview() {
        return _realEstateCreditRatesByBankview;
    }

    public Boolean realEstateCreditRatesByBankviewInitVal() {
        return _realEstateCreditRatesByBankviewInitVal;
    }

	public boolean realEstateCreditRatesByBankviewIsDirty() {
        return !valuesAreEqual(_realEstateCreditRatesByBankviewInitVal, _realEstateCreditRatesByBankview);
    }

    public boolean realEstateCreditRatesByBankviewIsSet() {
        return _realEstateCreditRatesByBankviewIsSet;
    }	


    public void setPrivateCreditRatesByBankview(Boolean privateCreditRatesByBankview) {
        if (!_privateCreditRatesByBankviewIsSet) {
            _privateCreditRatesByBankviewIsSet = true;
            _privateCreditRatesByBankviewInitVal = privateCreditRatesByBankview;
        }
        _privateCreditRatesByBankview = privateCreditRatesByBankview;
    }

    public Boolean getPrivateCreditRatesByBankview() {
        return _privateCreditRatesByBankview;
    }

    public Boolean privateCreditRatesByBankviewInitVal() {
        return _privateCreditRatesByBankviewInitVal;
    }

	public boolean privateCreditRatesByBankviewIsDirty() {
        return !valuesAreEqual(_privateCreditRatesByBankviewInitVal, _privateCreditRatesByBankview);
    }

    public boolean privateCreditRatesByBankviewIsSet() {
        return _privateCreditRatesByBankviewIsSet;
    }	

}
