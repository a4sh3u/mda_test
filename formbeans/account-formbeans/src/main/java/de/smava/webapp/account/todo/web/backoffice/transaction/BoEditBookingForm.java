//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse_2\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.transaction;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo edit booking)}

import de.smava.webapp.account.domain.BookingGroup;

import java.io.Serializable;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoEditBookingForm'.
 *
 * @author generator
 */
public class BoEditBookingForm implements Serializable {

    public static final String CONTRACT_EXPR = "boEditBookingForm.contract";
    public static final String CONTRACT_PATH = "command.boEditBookingForm.contract";
    public static final String TYPE_EXPR = "boEditBookingForm.type";
    public static final String TYPE_PATH = "command.boEditBookingForm.type";
    public static final String AMOUNT_EXPR = "boEditBookingForm.amount";
    public static final String AMOUNT_PATH = "command.boEditBookingForm.amount";
    public static final String BOOKING_GROUPS_EXPR = "boEditBookingForm.bookingGroups";
    public static final String BOOKING_GROUPS_PATH = "command.boEditBookingForm.bookingGroups";
    public static final String DELETE_BOOKING_ASSIGNMENT_ID_EXPR = "boEditBookingForm.deleteBookingAssignmentId";
    public static final String DELETE_BOOKING_ASSIGNMENT_ID_PATH = "command.boEditBookingForm.deleteBookingAssignmentId";
    public static final String ADD_BOOKING_GROUP_ID_EXPR = "boEditBookingForm.addBookingGroupId";
    public static final String ADD_BOOKING_GROUP_ID_PATH = "command.boEditBookingForm.addBookingGroupId";
    public static final String BOOKING_ID_EXPR = "boEditBookingForm.bookingId";
    public static final String BOOKING_ID_PATH = "command.boEditBookingForm.bookingId";
    public static final String BOOKING_GROUP_TARGET_EXPR = "boEditBookingForm.bookingGroupTarget";
    public static final String BOOKING_GROUP_TARGET_PATH = "command.boEditBookingForm.bookingGroupTarget";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo edit booking)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _contract;
    private String _contractInitVal;
    private boolean _contractIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private List<BookingGroup> _bookingGroups;
    private List<BookingGroup> _bookingGroupsInitVal;
    private boolean _bookingGroupsIsSet;
    private String _deleteBookingAssignmentId;
    private String _deleteBookingAssignmentIdInitVal;
    private boolean _deleteBookingAssignmentIdIsSet;
    private String _addBookingGroupId;
    private String _addBookingGroupIdInitVal;
    private boolean _addBookingGroupIdIsSet;
    private String _bookingId;
    private String _bookingIdInitVal;
    private boolean _bookingIdIsSet;
    private boolean _bookingGroupTarget;
    private boolean _bookingGroupTargetInitVal;
    private boolean _bookingGroupTargetIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setContract(String contract) {
        if (!_contractIsSet) {
            _contractIsSet = true;
            _contractInitVal = contract;
        }
        _contract = contract;
    }

    public String getContract() {
        return _contract;
    }

    public String contractInitVal() {
        return _contractInitVal;
    }

	public boolean contractIsDirty() {
        return !valuesAreEqual(_contractInitVal, _contract);
    }

    public boolean contractIsSet() {
        return _contractIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setBookingGroups(List<BookingGroup> bookingGroups) {
        if (!_bookingGroupsIsSet) {
            _bookingGroupsIsSet = true;
            _bookingGroupsInitVal = bookingGroups;
        }
        _bookingGroups = bookingGroups;
    }

    public List<BookingGroup> getBookingGroups() {
        return _bookingGroups;
    }

    public List<BookingGroup> bookingGroupsInitVal() {
        return _bookingGroupsInitVal;
    }

	public boolean bookingGroupsIsDirty() {
        return !valuesAreEqual(_bookingGroupsInitVal, _bookingGroups);
    }

    public boolean bookingGroupsIsSet() {
        return _bookingGroupsIsSet;
    }	


    public void setDeleteBookingAssignmentId(String deleteBookingAssignmentId) {
        if (!_deleteBookingAssignmentIdIsSet) {
            _deleteBookingAssignmentIdIsSet = true;
            _deleteBookingAssignmentIdInitVal = deleteBookingAssignmentId;
        }
        _deleteBookingAssignmentId = deleteBookingAssignmentId;
    }

    public String getDeleteBookingAssignmentId() {
        return _deleteBookingAssignmentId;
    }

    public String deleteBookingAssignmentIdInitVal() {
        return _deleteBookingAssignmentIdInitVal;
    }

	public boolean deleteBookingAssignmentIdIsDirty() {
        return !valuesAreEqual(_deleteBookingAssignmentIdInitVal, _deleteBookingAssignmentId);
    }

    public boolean deleteBookingAssignmentIdIsSet() {
        return _deleteBookingAssignmentIdIsSet;
    }	


    public void setAddBookingGroupId(String addBookingGroupId) {
        if (!_addBookingGroupIdIsSet) {
            _addBookingGroupIdIsSet = true;
            _addBookingGroupIdInitVal = addBookingGroupId;
        }
        _addBookingGroupId = addBookingGroupId;
    }

    public String getAddBookingGroupId() {
        return _addBookingGroupId;
    }

    public String addBookingGroupIdInitVal() {
        return _addBookingGroupIdInitVal;
    }

	public boolean addBookingGroupIdIsDirty() {
        return !valuesAreEqual(_addBookingGroupIdInitVal, _addBookingGroupId);
    }

    public boolean addBookingGroupIdIsSet() {
        return _addBookingGroupIdIsSet;
    }	


    public void setBookingId(String bookingId) {
        if (!_bookingIdIsSet) {
            _bookingIdIsSet = true;
            _bookingIdInitVal = bookingId;
        }
        _bookingId = bookingId;
    }

    public String getBookingId() {
        return _bookingId;
    }

    public String bookingIdInitVal() {
        return _bookingIdInitVal;
    }

	public boolean bookingIdIsDirty() {
        return !valuesAreEqual(_bookingIdInitVal, _bookingId);
    }

    public boolean bookingIdIsSet() {
        return _bookingIdIsSet;
    }	


    public void setBookingGroupTarget(boolean bookingGroupTarget) {
        if (!_bookingGroupTargetIsSet) {
            _bookingGroupTargetIsSet = true;
            _bookingGroupTargetInitVal = bookingGroupTarget;
        }
        _bookingGroupTarget = bookingGroupTarget;
    }

    public boolean getBookingGroupTarget() {
        return _bookingGroupTarget;
    }

    public boolean bookingGroupTargetInitVal() {
        return _bookingGroupTargetInitVal;
    }

	public boolean bookingGroupTargetIsDirty() {
        return !valuesAreEqual(_bookingGroupTargetInitVal, _bookingGroupTarget);
    }

    public boolean bookingGroupTargetIsSet() {
        return _bookingGroupTargetIsSet;
    }	

}
