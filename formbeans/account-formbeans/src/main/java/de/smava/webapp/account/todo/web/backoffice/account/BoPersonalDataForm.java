//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.account;
            
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo personal data)}

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoPersonalDataForm'.
 *
 * @author generator
 */
public class BoPersonalDataForm extends de.smava.webapp.account.todo.web.registration.PersonalDataForm implements Serializable {

    public static final String LOCK_DATE_EXPR = "personalDataForm.lockDate";
    public static final String LOCK_DATE_PATH = "command.personalDataForm.lockDate";
    public static final String POSTIDENT_DATE_EXPR = "personalDataForm.postidentDate";
    public static final String POSTIDENT_DATE_PATH = "command.personalDataForm.postidentDate";
    public static final String STATE_EXPR = "personalDataForm.state";
    public static final String STATE_PATH = "command.personalDataForm.state";
    public static final String PREFERRED_ROLE_EXPR = "personalDataForm.preferredRole";
    public static final String PREFERRED_ROLE_PATH = "command.personalDataForm.preferredRole";
    public static final String ROLES_EXPR = "personalDataForm.roles";
    public static final String ROLES_PATH = "command.personalDataForm.roles";
    public static final String PAYOUT_MODE_EXPR = "personalDataForm.payoutMode";
    public static final String PAYOUT_MODE_PATH = "command.personalDataForm.payoutMode";
    public static final String TERM_AND_COND_EXPR = "personalDataForm.termAndCond";
    public static final String TERM_AND_COND_PATH = "command.personalDataForm.termAndCond";
    public static final String ASSET_MANAGER_GUIDELINE_EXPR = "personalDataForm.assetManagerGuideline";
    public static final String ASSET_MANAGER_GUIDELINE_PATH = "command.personalDataForm.assetManagerGuideline";
    public static final String NEWSLETTER_SUBSCRIPTION_EXPR = "personalDataForm.newsletterSubscription";
    public static final String NEWSLETTER_SUBSCRIPTION_PATH = "command.personalDataForm.newsletterSubscription";
    public static final String RECEIVE_LOAN_OFFERS_EXPR = "personalDataForm.receiveLoanOffers";
    public static final String RECEIVE_LOAN_OFFERS_PATH = "command.personalDataForm.receiveLoanOffers";
    public static final String RECEIVE_CONSOLIDATION_OFFERS_EXPR = "personalDataForm.receiveConsolidationOffers";
    public static final String RECEIVE_CONSOLIDATION_OFFERS_PATH = "command.personalDataForm.receiveConsolidationOffers";
    public static final String SEND_LENDER_STATUS_EMAIL_EXPR = "personalDataForm.sendLenderStatusEmail";
    public static final String SEND_LENDER_STATUS_EMAIL_PATH = "command.personalDataForm.sendLenderStatusEmail";
    public static final String EXTERNAL_REFERENCE_ID_EXPR = "personalDataForm.externalReferenceId";
    public static final String EXTERNAL_REFERENCE_ID_PATH = "command.personalDataForm.externalReferenceId";
    public static final String MARKETING_PLACEMENT_ID_EXPR = "personalDataForm.marketingPlacementId";
    public static final String MARKETING_PLACEMENT_ID_PATH = "command.personalDataForm.marketingPlacementId";
    public static final String PERSONAL_ID_DATE_EXPR = "personalDataForm.personalIdDate";
    public static final String PERSONAL_ID_DATE_PATH = "command.personalDataForm.personalIdDate";
    public static final String PAYMENT_PROFILE_DATE_EXPR = "personalDataForm.paymentProfileDate";
    public static final String PAYMENT_PROFILE_DATE_PATH = "command.personalDataForm.paymentProfileDate";
    public static final String IS_MANAGED_EXPR = "personalDataForm.isManaged";
    public static final String IS_MANAGED_PATH = "command.personalDataForm.isManaged";
    public static final String USED_AGIO_EXPR = "personalDataForm.usedAgio";
    public static final String USED_AGIO_PATH = "command.personalDataForm.usedAgio";
    public static final String FIDOR_TERM_AND_COND_EXPR = "personalDataForm.fidorTermAndCond";
    public static final String FIDOR_TERM_AND_COND_PATH = "command.personalDataForm.fidorTermAndCond";
    public static final String DATA_SHARING_PARTNER_EXPR = "personalDataForm.dataSharingPartner";
    public static final String DATA_SHARING_PARTNER_PATH = "command.personalDataForm.dataSharingPartner";
    public static final String VIDEOIDENT_DATE_EXPR = "personalDataForm.videoidentDate";
    public static final String VIDEOIDENT_DATE_PATH = "command.personalDataForm.videoidentDate";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo personal data)}
    private static final long serialVersionUID = -4554275644016617713L;
    
    public boolean areAllFieldsEmpty() {
    	boolean result = super.areAllFieldsEmpty();
    	if (StringUtils.isNotBlank(_lockDate)) {
    		result = false;
		}
    	if (StringUtils.isNotBlank(_state)) {
    		result = false;
		}
    	if (StringUtils.isNotBlank(_preferredRole)) {
    		result = false;
		}
    	if (StringUtils.isNotBlank(_externalReferenceId)) {
    		result = false;
		}
    	if (StringUtils.isNotBlank(_marketingPlacementId)) {
    		result = false;
		}
    	if (StringUtils.isNotBlank(_usedAgio)) {
    		result = false;
		}

    	return result;
    }
    
// !!!!!!!! End of insert code section !!!!!!!!

    private String _lockDate;
    private String _lockDateInitVal;
    private boolean _lockDateIsSet;
    private Date _postidentDate;
    private Date _postidentDateInitVal;
    private boolean _postidentDateIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _preferredRole;
    private String _preferredRoleInitVal;
    private boolean _preferredRoleIsSet;
    private Set<String> _roles;
    private Set<String> _rolesInitVal;
    private boolean _rolesIsSet;
    private String _payoutMode;
    private String _payoutModeInitVal;
    private boolean _payoutModeIsSet;
    private Date _termAndCond;
    private Date _termAndCondInitVal;
    private boolean _termAndCondIsSet;
    private Date _assetManagerGuideline;
    private Date _assetManagerGuidelineInitVal;
    private boolean _assetManagerGuidelineIsSet;
    private Boolean _newsletterSubscription;
    private Boolean _newsletterSubscriptionInitVal;
    private boolean _newsletterSubscriptionIsSet;
    private Boolean _receiveLoanOffers;
    private Boolean _receiveLoanOffersInitVal;
    private boolean _receiveLoanOffersIsSet;
    private Boolean _receiveConsolidationOffers;
    private Boolean _receiveConsolidationOffersInitVal;
    private boolean _receiveConsolidationOffersIsSet;
    private boolean _sendLenderStatusEmail;
    private boolean _sendLenderStatusEmailInitVal;
    private boolean _sendLenderStatusEmailIsSet;
    private String _externalReferenceId;
    private String _externalReferenceIdInitVal;
    private boolean _externalReferenceIdIsSet;
    private String _marketingPlacementId;
    private String _marketingPlacementIdInitVal;
    private boolean _marketingPlacementIdIsSet;
    private Date _personalIdDate;
    private Date _personalIdDateInitVal;
    private boolean _personalIdDateIsSet;
    private Date _paymentProfileDate;
    private Date _paymentProfileDateInitVal;
    private boolean _paymentProfileDateIsSet;
    private Boolean _isManaged;
    private Boolean _isManagedInitVal;
    private boolean _isManagedIsSet;
    private String _usedAgio;
    private String _usedAgioInitVal;
    private boolean _usedAgioIsSet;
    private Date _fidorTermAndCond;
    private Date _fidorTermAndCondInitVal;
    private boolean _fidorTermAndCondIsSet;
    private String _dataSharingPartner;
    private String _dataSharingPartnerInitVal;
    private boolean _dataSharingPartnerIsSet;
    private Date _videoidentDate;
    private Date _videoidentDateInitVal;
    private boolean _videoidentDateIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setLockDate(String lockDate) {
        if (!_lockDateIsSet) {
            _lockDateIsSet = true;
            _lockDateInitVal = lockDate;
        }
        _lockDate = lockDate;
    }

    public String getLockDate() {
        return _lockDate;
    }

    public String lockDateInitVal() {
        return _lockDateInitVal;
    }

	public boolean lockDateIsDirty() {
        return !valuesAreEqual(_lockDateInitVal, _lockDate);
    }

    public boolean lockDateIsSet() {
        return _lockDateIsSet;
    }	


    public void setPostidentDate(Date postidentDate) {
        if (!_postidentDateIsSet) {
            _postidentDateIsSet = true;

            if (postidentDate == null) {
                _postidentDateInitVal = null;
            } else {
                _postidentDateInitVal = (Date) postidentDate.clone();
            }
        }

        if (postidentDate == null) {
            _postidentDate = null;
        } else {
            _postidentDate = (Date) postidentDate.clone();
        }
    }

    public Date getPostidentDate() {
        Date postidentDate = null;
        if (_postidentDate != null) {
            postidentDate = (Date) _postidentDate.clone();
        }
        return postidentDate;
    }

    public Date postidentDateInitVal() {
        Date postidentDateInitVal = null;
        if (_postidentDateInitVal != null) {
            postidentDateInitVal = (Date) _postidentDateInitVal.clone();
        }
        return postidentDateInitVal;
    }

	public boolean postidentDateIsDirty() {
        return !valuesAreEqual(_postidentDateInitVal, _postidentDate);
    }

    public boolean postidentDateIsSet() {
        return _postidentDateIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setPreferredRole(String preferredRole) {
        if (!_preferredRoleIsSet) {
            _preferredRoleIsSet = true;
            _preferredRoleInitVal = preferredRole;
        }
        _preferredRole = preferredRole;
    }

    public String getPreferredRole() {
        return _preferredRole;
    }

    public String preferredRoleInitVal() {
        return _preferredRoleInitVal;
    }

	public boolean preferredRoleIsDirty() {
        return !valuesAreEqual(_preferredRoleInitVal, _preferredRole);
    }

    public boolean preferredRoleIsSet() {
        return _preferredRoleIsSet;
    }	


    public void setRoles(Set<String> roles) {
        if (!_rolesIsSet) {
            _rolesIsSet = true;
            _rolesInitVal = roles;
        }
        _roles = roles;
    }

    public Set<String> getRoles() {
        return _roles;
    }

    public Set<String> rolesInitVal() {
        return _rolesInitVal;
    }

	public boolean rolesIsDirty() {
        return !valuesAreEqual(_rolesInitVal, _roles);
    }

    public boolean rolesIsSet() {
        return _rolesIsSet;
    }	


    public void setPayoutMode(String payoutMode) {
        if (!_payoutModeIsSet) {
            _payoutModeIsSet = true;
            _payoutModeInitVal = payoutMode;
        }
        _payoutMode = payoutMode;
    }

    public String getPayoutMode() {
        return _payoutMode;
    }

    public String payoutModeInitVal() {
        return _payoutModeInitVal;
    }

	public boolean payoutModeIsDirty() {
        return !valuesAreEqual(_payoutModeInitVal, _payoutMode);
    }

    public boolean payoutModeIsSet() {
        return _payoutModeIsSet;
    }	


    public void setTermAndCond(Date termAndCond) {
        if (!_termAndCondIsSet) {
            _termAndCondIsSet = true;

            if (termAndCond == null) {
                _termAndCondInitVal = null;
            } else {
                _termAndCondInitVal = (Date) termAndCond.clone();
            }
        }

        if (termAndCond == null) {
            _termAndCond = null;
        } else {
            _termAndCond = (Date) termAndCond.clone();
        }
    }

    public Date getTermAndCond() {
        Date termAndCond = null;
        if (_termAndCond != null) {
            termAndCond = (Date) _termAndCond.clone();
        }
        return termAndCond;
    }

    public Date termAndCondInitVal() {
        Date termAndCondInitVal = null;
        if (_termAndCondInitVal != null) {
            termAndCondInitVal = (Date) _termAndCondInitVal.clone();
        }
        return termAndCondInitVal;
    }

	public boolean termAndCondIsDirty() {
        return !valuesAreEqual(_termAndCondInitVal, _termAndCond);
    }

    public boolean termAndCondIsSet() {
        return _termAndCondIsSet;
    }	


    public void setAssetManagerGuideline(Date assetManagerGuideline) {
        if (!_assetManagerGuidelineIsSet) {
            _assetManagerGuidelineIsSet = true;

            if (assetManagerGuideline == null) {
                _assetManagerGuidelineInitVal = null;
            } else {
                _assetManagerGuidelineInitVal = (Date) assetManagerGuideline.clone();
            }
        }

        if (assetManagerGuideline == null) {
            _assetManagerGuideline = null;
        } else {
            _assetManagerGuideline = (Date) assetManagerGuideline.clone();
        }
    }

    public Date getAssetManagerGuideline() {
        Date assetManagerGuideline = null;
        if (_assetManagerGuideline != null) {
            assetManagerGuideline = (Date) _assetManagerGuideline.clone();
        }
        return assetManagerGuideline;
    }

    public Date assetManagerGuidelineInitVal() {
        Date assetManagerGuidelineInitVal = null;
        if (_assetManagerGuidelineInitVal != null) {
            assetManagerGuidelineInitVal = (Date) _assetManagerGuidelineInitVal.clone();
        }
        return assetManagerGuidelineInitVal;
    }

	public boolean assetManagerGuidelineIsDirty() {
        return !valuesAreEqual(_assetManagerGuidelineInitVal, _assetManagerGuideline);
    }

    public boolean assetManagerGuidelineIsSet() {
        return _assetManagerGuidelineIsSet;
    }	


    public void setNewsletterSubscription(Boolean newsletterSubscription) {
        if (!_newsletterSubscriptionIsSet) {
            _newsletterSubscriptionIsSet = true;
            _newsletterSubscriptionInitVal = newsletterSubscription;
        }
        _newsletterSubscription = newsletterSubscription;
    }

    public Boolean getNewsletterSubscription() {
        return _newsletterSubscription;
    }

    public Boolean newsletterSubscriptionInitVal() {
        return _newsletterSubscriptionInitVal;
    }

	public boolean newsletterSubscriptionIsDirty() {
        return !valuesAreEqual(_newsletterSubscriptionInitVal, _newsletterSubscription);
    }

    public boolean newsletterSubscriptionIsSet() {
        return _newsletterSubscriptionIsSet;
    }	


    public void setReceiveLoanOffers(Boolean receiveLoanOffers) {
        if (!_receiveLoanOffersIsSet) {
            _receiveLoanOffersIsSet = true;
            _receiveLoanOffersInitVal = receiveLoanOffers;
        }
        _receiveLoanOffers = receiveLoanOffers;
    }

    public Boolean getReceiveLoanOffers() {
        return _receiveLoanOffers;
    }

    public Boolean receiveLoanOffersInitVal() {
        return _receiveLoanOffersInitVal;
    }

	public boolean receiveLoanOffersIsDirty() {
        return !valuesAreEqual(_receiveLoanOffersInitVal, _receiveLoanOffers);
    }

    public boolean receiveLoanOffersIsSet() {
        return _receiveLoanOffersIsSet;
    }	


    public void setReceiveConsolidationOffers(Boolean receiveConsolidationOffers) {
        if (!_receiveConsolidationOffersIsSet) {
            _receiveConsolidationOffersIsSet = true;
            _receiveConsolidationOffersInitVal = receiveConsolidationOffers;
        }
        _receiveConsolidationOffers = receiveConsolidationOffers;
    }

    public Boolean getReceiveConsolidationOffers() {
        return _receiveConsolidationOffers;
    }

    public Boolean receiveConsolidationOffersInitVal() {
        return _receiveConsolidationOffersInitVal;
    }

	public boolean receiveConsolidationOffersIsDirty() {
        return !valuesAreEqual(_receiveConsolidationOffersInitVal, _receiveConsolidationOffers);
    }

    public boolean receiveConsolidationOffersIsSet() {
        return _receiveConsolidationOffersIsSet;
    }	


    public void setSendLenderStatusEmail(boolean sendLenderStatusEmail) {
        if (!_sendLenderStatusEmailIsSet) {
            _sendLenderStatusEmailIsSet = true;
            _sendLenderStatusEmailInitVal = sendLenderStatusEmail;
        }
        _sendLenderStatusEmail = sendLenderStatusEmail;
    }

    public boolean getSendLenderStatusEmail() {
        return _sendLenderStatusEmail;
    }

    public boolean sendLenderStatusEmailInitVal() {
        return _sendLenderStatusEmailInitVal;
    }

	public boolean sendLenderStatusEmailIsDirty() {
        return !valuesAreEqual(_sendLenderStatusEmailInitVal, _sendLenderStatusEmail);
    }

    public boolean sendLenderStatusEmailIsSet() {
        return _sendLenderStatusEmailIsSet;
    }	


    public void setExternalReferenceId(String externalReferenceId) {
        if (!_externalReferenceIdIsSet) {
            _externalReferenceIdIsSet = true;
            _externalReferenceIdInitVal = externalReferenceId;
        }
        _externalReferenceId = externalReferenceId;
    }

    public String getExternalReferenceId() {
        return _externalReferenceId;
    }

    public String externalReferenceIdInitVal() {
        return _externalReferenceIdInitVal;
    }

	public boolean externalReferenceIdIsDirty() {
        return !valuesAreEqual(_externalReferenceIdInitVal, _externalReferenceId);
    }

    public boolean externalReferenceIdIsSet() {
        return _externalReferenceIdIsSet;
    }	


    public void setMarketingPlacementId(String marketingPlacementId) {
        if (!_marketingPlacementIdIsSet) {
            _marketingPlacementIdIsSet = true;
            _marketingPlacementIdInitVal = marketingPlacementId;
        }
        _marketingPlacementId = marketingPlacementId;
    }

    public String getMarketingPlacementId() {
        return _marketingPlacementId;
    }

    public String marketingPlacementIdInitVal() {
        return _marketingPlacementIdInitVal;
    }

	public boolean marketingPlacementIdIsDirty() {
        return !valuesAreEqual(_marketingPlacementIdInitVal, _marketingPlacementId);
    }

    public boolean marketingPlacementIdIsSet() {
        return _marketingPlacementIdIsSet;
    }	


    public void setPersonalIdDate(Date personalIdDate) {
        if (!_personalIdDateIsSet) {
            _personalIdDateIsSet = true;

            if (personalIdDate == null) {
                _personalIdDateInitVal = null;
            } else {
                _personalIdDateInitVal = (Date) personalIdDate.clone();
            }
        }

        if (personalIdDate == null) {
            _personalIdDate = null;
        } else {
            _personalIdDate = (Date) personalIdDate.clone();
        }
    }

    public Date getPersonalIdDate() {
        Date personalIdDate = null;
        if (_personalIdDate != null) {
            personalIdDate = (Date) _personalIdDate.clone();
        }
        return personalIdDate;
    }

    public Date personalIdDateInitVal() {
        Date personalIdDateInitVal = null;
        if (_personalIdDateInitVal != null) {
            personalIdDateInitVal = (Date) _personalIdDateInitVal.clone();
        }
        return personalIdDateInitVal;
    }

	public boolean personalIdDateIsDirty() {
        return !valuesAreEqual(_personalIdDateInitVal, _personalIdDate);
    }

    public boolean personalIdDateIsSet() {
        return _personalIdDateIsSet;
    }	


    public void setPaymentProfileDate(Date paymentProfileDate) {
        if (!_paymentProfileDateIsSet) {
            _paymentProfileDateIsSet = true;

            if (paymentProfileDate == null) {
                _paymentProfileDateInitVal = null;
            } else {
                _paymentProfileDateInitVal = (Date) paymentProfileDate.clone();
            }
        }

        if (paymentProfileDate == null) {
            _paymentProfileDate = null;
        } else {
            _paymentProfileDate = (Date) paymentProfileDate.clone();
        }
    }

    public Date getPaymentProfileDate() {
        Date paymentProfileDate = null;
        if (_paymentProfileDate != null) {
            paymentProfileDate = (Date) _paymentProfileDate.clone();
        }
        return paymentProfileDate;
    }

    public Date paymentProfileDateInitVal() {
        Date paymentProfileDateInitVal = null;
        if (_paymentProfileDateInitVal != null) {
            paymentProfileDateInitVal = (Date) _paymentProfileDateInitVal.clone();
        }
        return paymentProfileDateInitVal;
    }

	public boolean paymentProfileDateIsDirty() {
        return !valuesAreEqual(_paymentProfileDateInitVal, _paymentProfileDate);
    }

    public boolean paymentProfileDateIsSet() {
        return _paymentProfileDateIsSet;
    }	


    public void setIsManaged(Boolean isManaged) {
        if (!_isManagedIsSet) {
            _isManagedIsSet = true;
            _isManagedInitVal = isManaged;
        }
        _isManaged = isManaged;
    }

    public Boolean getIsManaged() {
        return _isManaged;
    }

    public Boolean isManagedInitVal() {
        return _isManagedInitVal;
    }

	public boolean isManagedIsDirty() {
        return !valuesAreEqual(_isManagedInitVal, _isManaged);
    }

    public boolean isManagedIsSet() {
        return _isManagedIsSet;
    }	


    public void setUsedAgio(String usedAgio) {
        if (!_usedAgioIsSet) {
            _usedAgioIsSet = true;
            _usedAgioInitVal = usedAgio;
        }
        _usedAgio = usedAgio;
    }

    public String getUsedAgio() {
        return _usedAgio;
    }

    public String usedAgioInitVal() {
        return _usedAgioInitVal;
    }

	public boolean usedAgioIsDirty() {
        return !valuesAreEqual(_usedAgioInitVal, _usedAgio);
    }

    public boolean usedAgioIsSet() {
        return _usedAgioIsSet;
    }	


    public void setFidorTermAndCond(Date fidorTermAndCond) {
        if (!_fidorTermAndCondIsSet) {
            _fidorTermAndCondIsSet = true;

            if (fidorTermAndCond == null) {
                _fidorTermAndCondInitVal = null;
            } else {
                _fidorTermAndCondInitVal = (Date) fidorTermAndCond.clone();
            }
        }

        if (fidorTermAndCond == null) {
            _fidorTermAndCond = null;
        } else {
            _fidorTermAndCond = (Date) fidorTermAndCond.clone();
        }
    }

    public Date getFidorTermAndCond() {
        Date fidorTermAndCond = null;
        if (_fidorTermAndCond != null) {
            fidorTermAndCond = (Date) _fidorTermAndCond.clone();
        }
        return fidorTermAndCond;
    }

    public Date fidorTermAndCondInitVal() {
        Date fidorTermAndCondInitVal = null;
        if (_fidorTermAndCondInitVal != null) {
            fidorTermAndCondInitVal = (Date) _fidorTermAndCondInitVal.clone();
        }
        return fidorTermAndCondInitVal;
    }

	public boolean fidorTermAndCondIsDirty() {
        return !valuesAreEqual(_fidorTermAndCondInitVal, _fidorTermAndCond);
    }

    public boolean fidorTermAndCondIsSet() {
        return _fidorTermAndCondIsSet;
    }	


    public void setDataSharingPartner(String dataSharingPartner) {
        if (!_dataSharingPartnerIsSet) {
            _dataSharingPartnerIsSet = true;
            _dataSharingPartnerInitVal = dataSharingPartner;
        }
        _dataSharingPartner = dataSharingPartner;
    }

    public String getDataSharingPartner() {
        return _dataSharingPartner;
    }

    public String dataSharingPartnerInitVal() {
        return _dataSharingPartnerInitVal;
    }

	public boolean dataSharingPartnerIsDirty() {
        return !valuesAreEqual(_dataSharingPartnerInitVal, _dataSharingPartner);
    }

    public boolean dataSharingPartnerIsSet() {
        return _dataSharingPartnerIsSet;
    }	


    public void setVideoidentDate(Date videoidentDate) {
        if (!_videoidentDateIsSet) {
            _videoidentDateIsSet = true;

            if (videoidentDate == null) {
                _videoidentDateInitVal = null;
            } else {
                _videoidentDateInitVal = (Date) videoidentDate.clone();
            }
        }

        if (videoidentDate == null) {
            _videoidentDate = null;
        } else {
            _videoidentDate = (Date) videoidentDate.clone();
        }
    }

    public Date getVideoidentDate() {
        Date videoidentDate = null;
        if (_videoidentDate != null) {
            videoidentDate = (Date) _videoidentDate.clone();
        }
        return videoidentDate;
    }

    public Date videoidentDateInitVal() {
        Date videoidentDateInitVal = null;
        if (_videoidentDateInitVal != null) {
            videoidentDateInitVal = (Date) _videoidentDateInitVal.clone();
        }
        return videoidentDateInitVal;
    }

	public boolean videoidentDateIsDirty() {
        return !valuesAreEqual(_videoidentDateInitVal, _videoidentDate);
    }

    public boolean videoidentDateIsSet() {
        return _videoidentDateIsSet;
    }	

}
