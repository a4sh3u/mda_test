//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.registration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(address data)}

import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.apache.commons.lang.StringUtils;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.io.Serializable;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'AddressDataForm'.
 *
 * @author generator
 */
public class AddressDataForm implements Serializable {

    public static final String CREATE_NEW_ADDRESS_EXPR = "addressDataForm.createNewAddress";
    public static final String CREATE_NEW_ADDRESS_PATH = "command.addressDataForm.createNewAddress";
    public static final String HANDLE_MAIL_ADDRESS_EXPR = "addressDataForm.handleMailAddress";
    public static final String HANDLE_MAIL_ADDRESS_PATH = "command.addressDataForm.handleMailAddress";
    public static final String HANDLE_MAIN_ADDRESS_EXPR = "addressDataForm.handleMainAddress";
    public static final String HANDLE_MAIN_ADDRESS_PATH = "command.addressDataForm.handleMainAddress";
    public static final String HANDLE_PREV_ADDRESS_EXPR = "addressDataForm.handlePrevAddress";
    public static final String HANDLE_PREV_ADDRESS_PATH = "command.addressDataForm.handlePrevAddress";
    public static final String HANDLE_EMPLOYER_ADDRESS_EXPR = "addressDataForm.handleEmployerAddress";
    public static final String HANDLE_EMPLOYER_ADDRESS_PATH = "command.addressDataForm.handleEmployerAddress";
    public static final String HANDLE_CO_EMPLOYER_ADDRESS_EXPR = "addressDataForm.handleCoEmployerAddress";
    public static final String HANDLE_CO_EMPLOYER_ADDRESS_PATH = "command.addressDataForm.handleCoEmployerAddress";
    public static final String HANDLE_REGISTRATION_EXPR = "addressDataForm.handleRegistration";
    public static final String HANDLE_REGISTRATION_PATH = "command.addressDataForm.handleRegistration";
    public static final String MAIL_ADDRESS_ENABLED_EXPR = "addressDataForm.mailAddressEnabled";
    public static final String MAIL_ADDRESS_ENABLED_PATH = "command.addressDataForm.mailAddressEnabled";
	public static final String MAIN_ADDRESS_STREET_EXPR = "addressDataForm.mainAddressStreet";
    public static final String MAIN_ADDRESS_STREET_PATH = "command.addressDataForm.mainAddressStreet";
	public static final String MAIN_ADDRESS_STREET_NUMBER_EXPR = "addressDataForm.mainAddressStreetNumber";
    public static final String MAIN_ADDRESS_STREET_NUMBER_PATH = "command.addressDataForm.mainAddressStreetNumber";
	public static final String MAIN_ADDRESS_ZIP_EXPR = "addressDataForm.mainAddressZip";
    public static final String MAIN_ADDRESS_ZIP_PATH = "command.addressDataForm.mainAddressZip";
	public static final String MAIN_ADDRESS_CITY_EXPR = "addressDataForm.mainAddressCity";
    public static final String MAIN_ADDRESS_CITY_PATH = "command.addressDataForm.mainAddressCity";
	public static final String MAIN_ADDRESS_COUNTRY_EXPR = "addressDataForm.mainAddressCountry";
    public static final String MAIN_ADDRESS_COUNTRY_PATH = "command.addressDataForm.mainAddressCountry";
	public static final String MAIN_ADDRESS_MOVE_DATE_MONTH_EXPR = "addressDataForm.mainAddressMoveDateMonth";
    public static final String MAIN_ADDRESS_MOVE_DATE_MONTH_PATH = "command.addressDataForm.mainAddressMoveDateMonth";
	public static final String MAIN_ADDRESS_MOVE_DATE_YEAR_EXPR = "addressDataForm.mainAddressMoveDateYear";
    public static final String MAIN_ADDRESS_MOVE_DATE_YEAR_PATH = "command.addressDataForm.mainAddressMoveDateYear";
	public static final String PREV_ADDRESS_STREET_EXPR = "addressDataForm.prevAddressStreet";
    public static final String PREV_ADDRESS_STREET_PATH = "command.addressDataForm.prevAddressStreet";
	public static final String PREV_ADDRESS_STREET_NUMBER_EXPR = "addressDataForm.prevAddressStreetNumber";
    public static final String PREV_ADDRESS_STREET_NUMBER_PATH = "command.addressDataForm.prevAddressStreetNumber";
	public static final String PREV_ADDRESS_ZIP_EXPR = "addressDataForm.prevAddressZip";
    public static final String PREV_ADDRESS_ZIP_PATH = "command.addressDataForm.prevAddressZip";
	public static final String PREV_ADDRESS_CITY_EXPR = "addressDataForm.prevAddressCity";
    public static final String PREV_ADDRESS_CITY_PATH = "command.addressDataForm.prevAddressCity";
	public static final String PREV_ADDRESS_COUNTRY_EXPR = "addressDataForm.prevAddressCountry";
    public static final String PREV_ADDRESS_COUNTRY_PATH = "command.addressDataForm.prevAddressCountry";
	public static final String MAIL_ADDRESS_STREET_EXPR = "addressDataForm.mailAddressStreet";
    public static final String MAIL_ADDRESS_STREET_PATH = "command.addressDataForm.mailAddressStreet";
	public static final String MAIL_ADDRESS_STREET_NUMBER_EXPR = "addressDataForm.mailAddressStreetNumber";
    public static final String MAIL_ADDRESS_STREET_NUMBER_PATH = "command.addressDataForm.mailAddressStreetNumber";
	public static final String MAIL_ADDRESS_ZIP_EXPR = "addressDataForm.mailAddressZip";
    public static final String MAIL_ADDRESS_ZIP_PATH = "command.addressDataForm.mailAddressZip";
	public static final String MAIL_ADDRESS_CITY_EXPR = "addressDataForm.mailAddressCity";
    public static final String MAIL_ADDRESS_CITY_PATH = "command.addressDataForm.mailAddressCity";
	public static final String MAIL_ADDRESS_COUNTRY_EXPR = "addressDataForm.mailAddressCountry";
    public static final String MAIL_ADDRESS_COUNTRY_PATH = "command.addressDataForm.mailAddressCountry";
	public static final String EMPLOYER_ADDRESS_STREET_EXPR = "addressDataForm.employerAddressStreet";
    public static final String EMPLOYER_ADDRESS_STREET_PATH = "command.addressDataForm.employerAddressStreet";
	public static final String EMPLOYER_ADDRESS_STREET_NUMBER_EXPR = "addressDataForm.employerAddressStreetNumber";
    public static final String EMPLOYER_ADDRESS_STREET_NUMBER_PATH = "command.addressDataForm.employerAddressStreetNumber";
	public static final String EMPLOYER_ADDRESS_ZIP_EXPR = "addressDataForm.employerAddressZip";
    public static final String EMPLOYER_ADDRESS_ZIP_PATH = "command.addressDataForm.employerAddressZip";
	public static final String EMPLOYER_ADDRESS_CITY_EXPR = "addressDataForm.employerAddressCity";
    public static final String EMPLOYER_ADDRESS_CITY_PATH = "command.addressDataForm.employerAddressCity";
	public static final String EMPLOYER_ADDRESS_COUNTRY_EXPR = "addressDataForm.employerAddressCountry";
    public static final String EMPLOYER_ADDRESS_COUNTRY_PATH = "command.addressDataForm.employerAddressCountry";
	public static final String CO_EMPLOYER_ADDRESS_STREET_EXPR = "addressDataForm.coEmployerAddressStreet";
    public static final String CO_EMPLOYER_ADDRESS_STREET_PATH = "command.addressDataForm.coEmployerAddressStreet";
	public static final String CO_EMPLOYER_ADDRESS_STREET_NUMBER_EXPR = "addressDataForm.coEmployerAddressStreetNumber";
    public static final String CO_EMPLOYER_ADDRESS_STREET_NUMBER_PATH = "command.addressDataForm.coEmployerAddressStreetNumber";
	public static final String CO_EMPLOYER_ADDRESS_ZIP_EXPR = "addressDataForm.coEmployerAddressZip";
    public static final String CO_EMPLOYER_ADDRESS_ZIP_PATH = "command.addressDataForm.coEmployerAddressZip";
	public static final String CO_EMPLOYER_ADDRESS_CITY_EXPR = "addressDataForm.coEmployerAddressCity";
    public static final String CO_EMPLOYER_ADDRESS_CITY_PATH = "command.addressDataForm.coEmployerAddressCity";
	public static final String CO_EMPLOYER_ADDRESS_COUNTRY_EXPR = "addressDataForm.coEmployerAddressCountry";
    public static final String CO_EMPLOYER_ADDRESS_COUNTRY_PATH = "command.addressDataForm.coEmployerAddressCountry";
    public static final String DEFAULT_COUNTRY_EXPR = "addressDataForm.defaultCountry";
    public static final String DEFAULT_COUNTRY_PATH = "command.addressDataForm.defaultCountry";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(address data)}

    /**
	 * generated serial.
	 */
	private static final long serialVersionUID = -3543624424747601187L;

    /**
     * AddressDataForm default constructor by default.
     * Defaults are required for existing code fragments.
     */
    public AddressDataForm() {
    	
    	// create new address by default
        _createNewAddress = true;
        _createNewAddressInitVal = true;
        
        // handle main address by default
        _handleMainAddress = true;
        _handleMainAddressInitVal = true;
        
        // ignore mail address by default
        _handleMailAddress = false;
        _handleMailAddressInitVal = false;
        
    	// ignore registration by default
        _handleRegistration = false;
        _handleRegistrationInitVal = false;
        
        // no extra mail address by default
        _mailAddressEnabled = false;
        _mailAddressEnabledInitVal = false;
        
        _handlePrevAddress = false;
        _handlePrevAddressInitVal = false;
        
    }
    
    public boolean hasDirtyAddress() {
    	return isMainAddressDirty()
    		|| isMailAddressDirty() 
    		|| isPrevAddressDirty();
    }

    public boolean isMainAddressDirty() {
    	return mainAddressCityIsDirty()
    		|| mainAddressCountryIsDirty()
    		|| mainAddressStreetIsDirty()
    		|| mainAddressStreetNumberIsDirty()
    		|| mainAddressZipIsDirty()
    		|| mainAddressMoveDateMonthIsDirty()
    		|| mainAddressMoveDateYearIsDirty();
    }

    public boolean isPrevAddressDirty() {
    	return prevAddressCityIsDirty()
    		|| prevAddressCountryIsDirty()
    		|| prevAddressStreetIsDirty()
    		|| prevAddressStreetNumberIsDirty()
    		|| prevAddressZipIsDirty();
    }
    
    public boolean isMailAddressDirty() {
    	return mailAddressCityIsDirty()
    		|| mailAddressCountryIsDirty()
    		|| mailAddressStreetIsDirty()
    		|| mailAddressStreetNumberIsDirty()
    		|| mailAddressZipIsDirty();
    }
    
    public boolean isTermOfResidence() {
    	boolean result = false;
    	if (_mainAddressMoveDateMonth != null && StringUtils.isNotBlank(_mainAddressMoveDateMonth) 
    			&& _mainAddressMoveDateYear != null && StringUtils.isNotBlank(_mainAddressMoveDateYear)) {
    		Date moveDate = DateUtils.createDate(Integer.valueOf(_mainAddressMoveDateYear), Integer.valueOf(_mainAddressMoveDateMonth), 1);
    		if (moveDate.before(CurrentDate.getDate())) {
	        	Interval interval = new Interval(moveDate.getTime(), CurrentDate.getTime());
	        	Period period = interval.toPeriod(PeriodType.years());
	        	int years = period.getYears();
	        	if (years < 2) {
	        		result = true;
	        	}
    		}
    	}
    	return result;
    }
    
// !!!!!!!! End of insert code section !!!!!!!!

    private Boolean _createNewAddress;
    private Boolean _createNewAddressInitVal;
    private boolean _createNewAddressIsSet;
    private Boolean _handleMailAddress;
    private Boolean _handleMailAddressInitVal;
    private boolean _handleMailAddressIsSet;
    private Boolean _handleMainAddress;
    private Boolean _handleMainAddressInitVal;
    private boolean _handleMainAddressIsSet;
    private Boolean _handlePrevAddress;
    private Boolean _handlePrevAddressInitVal;
    private boolean _handlePrevAddressIsSet;
    private Boolean _handleEmployerAddress;
    private Boolean _handleEmployerAddressInitVal;
    private boolean _handleEmployerAddressIsSet;
    private Boolean _handleCoEmployerAddress;
    private Boolean _handleCoEmployerAddressInitVal;
    private boolean _handleCoEmployerAddressIsSet;
    private Boolean _handleRegistration;
    private Boolean _handleRegistrationInitVal;
    private boolean _handleRegistrationIsSet;
    private Boolean _mailAddressEnabled;
    private Boolean _mailAddressEnabledInitVal;
    private boolean _mailAddressEnabledIsSet;
   	private String _mainAddressStreet;
    private String _mainAddressStreetInitVal;
    private boolean _mainAddressStreetIsSet;
   	private String _mainAddressStreetNumber;
    private String _mainAddressStreetNumberInitVal;
    private boolean _mainAddressStreetNumberIsSet;
   	private String _mainAddressZip;
    private String _mainAddressZipInitVal;
    private boolean _mainAddressZipIsSet;
   	private String _mainAddressCity;
    private String _mainAddressCityInitVal;
    private boolean _mainAddressCityIsSet;
   	private String _mainAddressCountry;
    private String _mainAddressCountryInitVal;
    private boolean _mainAddressCountryIsSet;
   	private String _mainAddressMoveDateMonth;
    private String _mainAddressMoveDateMonthInitVal;
    private boolean _mainAddressMoveDateMonthIsSet;
   	private String _mainAddressMoveDateYear;
    private String _mainAddressMoveDateYearInitVal;
    private boolean _mainAddressMoveDateYearIsSet;
   	private String _prevAddressStreet;
    private String _prevAddressStreetInitVal;
    private boolean _prevAddressStreetIsSet;
   	private String _prevAddressStreetNumber;
    private String _prevAddressStreetNumberInitVal;
    private boolean _prevAddressStreetNumberIsSet;
   	private String _prevAddressZip;
    private String _prevAddressZipInitVal;
    private boolean _prevAddressZipIsSet;
   	private String _prevAddressCity;
    private String _prevAddressCityInitVal;
    private boolean _prevAddressCityIsSet;
   	private String _prevAddressCountry;
    private String _prevAddressCountryInitVal;
    private boolean _prevAddressCountryIsSet;
   	private String _mailAddressStreet;
    private String _mailAddressStreetInitVal;
    private boolean _mailAddressStreetIsSet;
   	private String _mailAddressStreetNumber;
    private String _mailAddressStreetNumberInitVal;
    private boolean _mailAddressStreetNumberIsSet;
   	private String _mailAddressZip;
    private String _mailAddressZipInitVal;
    private boolean _mailAddressZipIsSet;
   	private String _mailAddressCity;
    private String _mailAddressCityInitVal;
    private boolean _mailAddressCityIsSet;
   	private String _mailAddressCountry;
    private String _mailAddressCountryInitVal;
    private boolean _mailAddressCountryIsSet;
   	private String _employerAddressStreet;
    private String _employerAddressStreetInitVal;
    private boolean _employerAddressStreetIsSet;
   	private String _employerAddressStreetNumber;
    private String _employerAddressStreetNumberInitVal;
    private boolean _employerAddressStreetNumberIsSet;
   	private String _employerAddressZip;
    private String _employerAddressZipInitVal;
    private boolean _employerAddressZipIsSet;
   	private String _employerAddressCity;
    private String _employerAddressCityInitVal;
    private boolean _employerAddressCityIsSet;
   	private String _employerAddressCountry;
    private String _employerAddressCountryInitVal;
    private boolean _employerAddressCountryIsSet;
   	private String _coEmployerAddressStreet;
    private String _coEmployerAddressStreetInitVal;
    private boolean _coEmployerAddressStreetIsSet;
   	private String _coEmployerAddressStreetNumber;
    private String _coEmployerAddressStreetNumberInitVal;
    private boolean _coEmployerAddressStreetNumberIsSet;
   	private String _coEmployerAddressZip;
    private String _coEmployerAddressZipInitVal;
    private boolean _coEmployerAddressZipIsSet;
   	private String _coEmployerAddressCity;
    private String _coEmployerAddressCityInitVal;
    private boolean _coEmployerAddressCityIsSet;
   	private String _coEmployerAddressCountry;
    private String _coEmployerAddressCountryInitVal;
    private boolean _coEmployerAddressCountryIsSet;
    private String _defaultCountry;
    private String _defaultCountryInitVal;
    private boolean _defaultCountryIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setCreateNewAddress(Boolean createNewAddress) {
        if (!_createNewAddressIsSet) {
            _createNewAddressIsSet = true;
            _createNewAddressInitVal = createNewAddress;
        }
        _createNewAddress = createNewAddress;
    }

    public Boolean getCreateNewAddress() {
        return _createNewAddress;
    }

    public Boolean createNewAddressInitVal() {
        return _createNewAddressInitVal;
    }

	public boolean createNewAddressIsDirty() {
        return !valuesAreEqual(_createNewAddressInitVal, _createNewAddress);
    }

    public boolean createNewAddressIsSet() {
        return _createNewAddressIsSet;
    }	


    public void setHandleMailAddress(Boolean handleMailAddress) {
        if (!_handleMailAddressIsSet) {
            _handleMailAddressIsSet = true;
            _handleMailAddressInitVal = handleMailAddress;
        }
        _handleMailAddress = handleMailAddress;
    }

    public Boolean getHandleMailAddress() {
        return _handleMailAddress;
    }

    public Boolean handleMailAddressInitVal() {
        return _handleMailAddressInitVal;
    }

	public boolean handleMailAddressIsDirty() {
        return !valuesAreEqual(_handleMailAddressInitVal, _handleMailAddress);
    }

    public boolean handleMailAddressIsSet() {
        return _handleMailAddressIsSet;
    }	


    public void setHandleMainAddress(Boolean handleMainAddress) {
        if (!_handleMainAddressIsSet) {
            _handleMainAddressIsSet = true;
            _handleMainAddressInitVal = handleMainAddress;
        }
        _handleMainAddress = handleMainAddress;
    }

    public Boolean getHandleMainAddress() {
        return _handleMainAddress;
    }

    public Boolean handleMainAddressInitVal() {
        return _handleMainAddressInitVal;
    }

	public boolean handleMainAddressIsDirty() {
        return !valuesAreEqual(_handleMainAddressInitVal, _handleMainAddress);
    }

    public boolean handleMainAddressIsSet() {
        return _handleMainAddressIsSet;
    }	


    public void setHandlePrevAddress(Boolean handlePrevAddress) {
        if (!_handlePrevAddressIsSet) {
            _handlePrevAddressIsSet = true;
            _handlePrevAddressInitVal = handlePrevAddress;
        }
        _handlePrevAddress = handlePrevAddress;
    }

    public Boolean getHandlePrevAddress() {
        return _handlePrevAddress;
    }

    public Boolean handlePrevAddressInitVal() {
        return _handlePrevAddressInitVal;
    }

	public boolean handlePrevAddressIsDirty() {
        return !valuesAreEqual(_handlePrevAddressInitVal, _handlePrevAddress);
    }

    public boolean handlePrevAddressIsSet() {
        return _handlePrevAddressIsSet;
    }	


    public void setHandleEmployerAddress(Boolean handleEmployerAddress) {
        if (!_handleEmployerAddressIsSet) {
            _handleEmployerAddressIsSet = true;
            _handleEmployerAddressInitVal = handleEmployerAddress;
        }
        _handleEmployerAddress = handleEmployerAddress;
    }

    public Boolean getHandleEmployerAddress() {
        return _handleEmployerAddress;
    }

    public Boolean handleEmployerAddressInitVal() {
        return _handleEmployerAddressInitVal;
    }

	public boolean handleEmployerAddressIsDirty() {
        return !valuesAreEqual(_handleEmployerAddressInitVal, _handleEmployerAddress);
    }

    public boolean handleEmployerAddressIsSet() {
        return _handleEmployerAddressIsSet;
    }	


    public void setHandleCoEmployerAddress(Boolean handleCoEmployerAddress) {
        if (!_handleCoEmployerAddressIsSet) {
            _handleCoEmployerAddressIsSet = true;
            _handleCoEmployerAddressInitVal = handleCoEmployerAddress;
        }
        _handleCoEmployerAddress = handleCoEmployerAddress;
    }

    public Boolean getHandleCoEmployerAddress() {
        return _handleCoEmployerAddress;
    }

    public Boolean handleCoEmployerAddressInitVal() {
        return _handleCoEmployerAddressInitVal;
    }

	public boolean handleCoEmployerAddressIsDirty() {
        return !valuesAreEqual(_handleCoEmployerAddressInitVal, _handleCoEmployerAddress);
    }

    public boolean handleCoEmployerAddressIsSet() {
        return _handleCoEmployerAddressIsSet;
    }	


    public void setHandleRegistration(Boolean handleRegistration) {
        if (!_handleRegistrationIsSet) {
            _handleRegistrationIsSet = true;
            _handleRegistrationInitVal = handleRegistration;
        }
        _handleRegistration = handleRegistration;
    }

    public Boolean getHandleRegistration() {
        return _handleRegistration;
    }

    public Boolean handleRegistrationInitVal() {
        return _handleRegistrationInitVal;
    }

	public boolean handleRegistrationIsDirty() {
        return !valuesAreEqual(_handleRegistrationInitVal, _handleRegistration);
    }

    public boolean handleRegistrationIsSet() {
        return _handleRegistrationIsSet;
    }	


    public void setMailAddressEnabled(Boolean mailAddressEnabled) {
        if (!_mailAddressEnabledIsSet) {
            _mailAddressEnabledIsSet = true;
            _mailAddressEnabledInitVal = mailAddressEnabled;
        }
        _mailAddressEnabled = mailAddressEnabled;
    }

    public Boolean getMailAddressEnabled() {
        return _mailAddressEnabled;
    }

    public Boolean mailAddressEnabledInitVal() {
        return _mailAddressEnabledInitVal;
    }

	public boolean mailAddressEnabledIsDirty() {
        return !valuesAreEqual(_mailAddressEnabledInitVal, _mailAddressEnabled);
    }

    public boolean mailAddressEnabledIsSet() {
        return _mailAddressEnabledIsSet;
    }	

	
    public void setMainAddressStreet(String mainAddressStreet) {
        if (!_mainAddressStreetIsSet) {
            _mainAddressStreetIsSet = true;
            _mainAddressStreetInitVal = mainAddressStreet;
        }
        _mainAddressStreet = mainAddressStreet;
    }

    public String getMainAddressStreet() {
        return _mainAddressStreet;
    }

    public String mainAddressStreetInitVal() {
        return _mainAddressStreetInitVal;
    }
    public void setMainAddressStreetNumber(String mainAddressStreetNumber) {
        if (!_mainAddressStreetNumberIsSet) {
            _mainAddressStreetNumberIsSet = true;
            _mainAddressStreetNumberInitVal = mainAddressStreetNumber;
        }
        _mainAddressStreetNumber = mainAddressStreetNumber;
    }

    public String getMainAddressStreetNumber() {
        return _mainAddressStreetNumber;
    }

    public String mainAddressStreetNumberInitVal() {
        return _mainAddressStreetNumberInitVal;
    }
    public void setMainAddressZip(String mainAddressZip) {
        if (!_mainAddressZipIsSet) {
            _mainAddressZipIsSet = true;
            _mainAddressZipInitVal = mainAddressZip;
        }
        _mainAddressZip = mainAddressZip;
    }

    public String getMainAddressZip() {
        return _mainAddressZip;
    }

    public String mainAddressZipInitVal() {
        return _mainAddressZipInitVal;
    }
    public void setMainAddressCity(String mainAddressCity) {
        if (!_mainAddressCityIsSet) {
            _mainAddressCityIsSet = true;
            _mainAddressCityInitVal = mainAddressCity;
        }
        _mainAddressCity = mainAddressCity;
    }

    public String getMainAddressCity() {
        return _mainAddressCity;
    }

    public String mainAddressCityInitVal() {
        return _mainAddressCityInitVal;
    }
    public void setMainAddressCountry(String mainAddressCountry) {
        if (!_mainAddressCountryIsSet) {
            _mainAddressCountryIsSet = true;
            _mainAddressCountryInitVal = mainAddressCountry;
        }
        _mainAddressCountry = mainAddressCountry;
    }

    public String getMainAddressCountry() {
        return _mainAddressCountry;
    }

    public String mainAddressCountryInitVal() {
        return _mainAddressCountryInitVal;
    }
    public void setMainAddressMoveDateMonth(String mainAddressMoveDateMonth) {
        if (!_mainAddressMoveDateMonthIsSet) {
            _mainAddressMoveDateMonthIsSet = true;
            _mainAddressMoveDateMonthInitVal = mainAddressMoveDateMonth;
        }
        _mainAddressMoveDateMonth = mainAddressMoveDateMonth;
    }

    public String getMainAddressMoveDateMonth() {
        return _mainAddressMoveDateMonth;
    }

    public String mainAddressMoveDateMonthInitVal() {
        return _mainAddressMoveDateMonthInitVal;
    }
    public void setMainAddressMoveDateYear(String mainAddressMoveDateYear) {
        if (!_mainAddressMoveDateYearIsSet) {
            _mainAddressMoveDateYearIsSet = true;
            _mainAddressMoveDateYearInitVal = mainAddressMoveDateYear;
        }
        _mainAddressMoveDateYear = mainAddressMoveDateYear;
    }

    public String getMainAddressMoveDateYear() {
        return _mainAddressMoveDateYear;
    }

    public String mainAddressMoveDateYearInitVal() {
        return _mainAddressMoveDateYearInitVal;
    }

	public boolean mainAddressSetIsDirty() {
		boolean result = false;

		result = result || mainAddressStreetIsDirty();		
		result = result || mainAddressStreetNumberIsDirty();		
		result = result || mainAddressZipIsDirty();		
		result = result || mainAddressCityIsDirty();		
		result = result || mainAddressCountryIsDirty();		
		result = result || mainAddressMoveDateMonthIsDirty();		
		result = result || mainAddressMoveDateYearIsDirty();		
	
		return result;
	}
	
	public boolean mainAddressStreetIsDirty() {
        return !valuesAreEqual(_mainAddressStreetInitVal, _mainAddressStreet);
    }

    public boolean mainAddressStreetIsSet() {
        return _mainAddressStreetIsSet;
    }	
	public boolean mainAddressStreetNumberIsDirty() {
        return !valuesAreEqual(_mainAddressStreetNumberInitVal, _mainAddressStreetNumber);
    }

    public boolean mainAddressStreetNumberIsSet() {
        return _mainAddressStreetNumberIsSet;
    }	
	public boolean mainAddressZipIsDirty() {
        return !valuesAreEqual(_mainAddressZipInitVal, _mainAddressZip);
    }

    public boolean mainAddressZipIsSet() {
        return _mainAddressZipIsSet;
    }	
	public boolean mainAddressCityIsDirty() {
        return !valuesAreEqual(_mainAddressCityInitVal, _mainAddressCity);
    }

    public boolean mainAddressCityIsSet() {
        return _mainAddressCityIsSet;
    }	
	public boolean mainAddressCountryIsDirty() {
        return !valuesAreEqual(_mainAddressCountryInitVal, _mainAddressCountry);
    }

    public boolean mainAddressCountryIsSet() {
        return _mainAddressCountryIsSet;
    }	
	public boolean mainAddressMoveDateMonthIsDirty() {
        return !valuesAreEqual(_mainAddressMoveDateMonthInitVal, _mainAddressMoveDateMonth);
    }

    public boolean mainAddressMoveDateMonthIsSet() {
        return _mainAddressMoveDateMonthIsSet;
    }	
	public boolean mainAddressMoveDateYearIsDirty() {
        return !valuesAreEqual(_mainAddressMoveDateYearInitVal, _mainAddressMoveDateYear);
    }

    public boolean mainAddressMoveDateYearIsSet() {
        return _mainAddressMoveDateYearIsSet;
    }	

	
    public void setPrevAddressStreet(String prevAddressStreet) {
        if (!_prevAddressStreetIsSet) {
            _prevAddressStreetIsSet = true;
            _prevAddressStreetInitVal = prevAddressStreet;
        }
        _prevAddressStreet = prevAddressStreet;
    }

    public String getPrevAddressStreet() {
        return _prevAddressStreet;
    }

    public String prevAddressStreetInitVal() {
        return _prevAddressStreetInitVal;
    }
    public void setPrevAddressStreetNumber(String prevAddressStreetNumber) {
        if (!_prevAddressStreetNumberIsSet) {
            _prevAddressStreetNumberIsSet = true;
            _prevAddressStreetNumberInitVal = prevAddressStreetNumber;
        }
        _prevAddressStreetNumber = prevAddressStreetNumber;
    }

    public String getPrevAddressStreetNumber() {
        return _prevAddressStreetNumber;
    }

    public String prevAddressStreetNumberInitVal() {
        return _prevAddressStreetNumberInitVal;
    }
    public void setPrevAddressZip(String prevAddressZip) {
        if (!_prevAddressZipIsSet) {
            _prevAddressZipIsSet = true;
            _prevAddressZipInitVal = prevAddressZip;
        }
        _prevAddressZip = prevAddressZip;
    }

    public String getPrevAddressZip() {
        return _prevAddressZip;
    }

    public String prevAddressZipInitVal() {
        return _prevAddressZipInitVal;
    }
    public void setPrevAddressCity(String prevAddressCity) {
        if (!_prevAddressCityIsSet) {
            _prevAddressCityIsSet = true;
            _prevAddressCityInitVal = prevAddressCity;
        }
        _prevAddressCity = prevAddressCity;
    }

    public String getPrevAddressCity() {
        return _prevAddressCity;
    }

    public String prevAddressCityInitVal() {
        return _prevAddressCityInitVal;
    }
    public void setPrevAddressCountry(String prevAddressCountry) {
        if (!_prevAddressCountryIsSet) {
            _prevAddressCountryIsSet = true;
            _prevAddressCountryInitVal = prevAddressCountry;
        }
        _prevAddressCountry = prevAddressCountry;
    }

    public String getPrevAddressCountry() {
        return _prevAddressCountry;
    }

    public String prevAddressCountryInitVal() {
        return _prevAddressCountryInitVal;
    }

	public boolean prevAddressSetIsDirty() {
		boolean result = false;

		result = result || prevAddressStreetIsDirty();		
		result = result || prevAddressStreetNumberIsDirty();		
		result = result || prevAddressZipIsDirty();		
		result = result || prevAddressCityIsDirty();		
		result = result || prevAddressCountryIsDirty();		
	
		return result;
	}
	
	public boolean prevAddressStreetIsDirty() {
        return !valuesAreEqual(_prevAddressStreetInitVal, _prevAddressStreet);
    }

    public boolean prevAddressStreetIsSet() {
        return _prevAddressStreetIsSet;
    }	
	public boolean prevAddressStreetNumberIsDirty() {
        return !valuesAreEqual(_prevAddressStreetNumberInitVal, _prevAddressStreetNumber);
    }

    public boolean prevAddressStreetNumberIsSet() {
        return _prevAddressStreetNumberIsSet;
    }	
	public boolean prevAddressZipIsDirty() {
        return !valuesAreEqual(_prevAddressZipInitVal, _prevAddressZip);
    }

    public boolean prevAddressZipIsSet() {
        return _prevAddressZipIsSet;
    }	
	public boolean prevAddressCityIsDirty() {
        return !valuesAreEqual(_prevAddressCityInitVal, _prevAddressCity);
    }

    public boolean prevAddressCityIsSet() {
        return _prevAddressCityIsSet;
    }	
	public boolean prevAddressCountryIsDirty() {
        return !valuesAreEqual(_prevAddressCountryInitVal, _prevAddressCountry);
    }

    public boolean prevAddressCountryIsSet() {
        return _prevAddressCountryIsSet;
    }	

	
    public void setMailAddressStreet(String mailAddressStreet) {
        if (!_mailAddressStreetIsSet) {
            _mailAddressStreetIsSet = true;
            _mailAddressStreetInitVal = mailAddressStreet;
        }
        _mailAddressStreet = mailAddressStreet;
    }

    public String getMailAddressStreet() {
        return _mailAddressStreet;
    }

    public String mailAddressStreetInitVal() {
        return _mailAddressStreetInitVal;
    }
    public void setMailAddressStreetNumber(String mailAddressStreetNumber) {
        if (!_mailAddressStreetNumberIsSet) {
            _mailAddressStreetNumberIsSet = true;
            _mailAddressStreetNumberInitVal = mailAddressStreetNumber;
        }
        _mailAddressStreetNumber = mailAddressStreetNumber;
    }

    public String getMailAddressStreetNumber() {
        return _mailAddressStreetNumber;
    }

    public String mailAddressStreetNumberInitVal() {
        return _mailAddressStreetNumberInitVal;
    }
    public void setMailAddressZip(String mailAddressZip) {
        if (!_mailAddressZipIsSet) {
            _mailAddressZipIsSet = true;
            _mailAddressZipInitVal = mailAddressZip;
        }
        _mailAddressZip = mailAddressZip;
    }

    public String getMailAddressZip() {
        return _mailAddressZip;
    }

    public String mailAddressZipInitVal() {
        return _mailAddressZipInitVal;
    }
    public void setMailAddressCity(String mailAddressCity) {
        if (!_mailAddressCityIsSet) {
            _mailAddressCityIsSet = true;
            _mailAddressCityInitVal = mailAddressCity;
        }
        _mailAddressCity = mailAddressCity;
    }

    public String getMailAddressCity() {
        return _mailAddressCity;
    }

    public String mailAddressCityInitVal() {
        return _mailAddressCityInitVal;
    }
    public void setMailAddressCountry(String mailAddressCountry) {
        if (!_mailAddressCountryIsSet) {
            _mailAddressCountryIsSet = true;
            _mailAddressCountryInitVal = mailAddressCountry;
        }
        _mailAddressCountry = mailAddressCountry;
    }

    public String getMailAddressCountry() {
        return _mailAddressCountry;
    }

    public String mailAddressCountryInitVal() {
        return _mailAddressCountryInitVal;
    }

	public boolean mailAddressSetIsDirty() {
		boolean result = false;

		result = result || mailAddressStreetIsDirty();		
		result = result || mailAddressStreetNumberIsDirty();		
		result = result || mailAddressZipIsDirty();		
		result = result || mailAddressCityIsDirty();		
		result = result || mailAddressCountryIsDirty();		
	
		return result;
	}
	
	public boolean mailAddressStreetIsDirty() {
        return !valuesAreEqual(_mailAddressStreetInitVal, _mailAddressStreet);
    }

    public boolean mailAddressStreetIsSet() {
        return _mailAddressStreetIsSet;
    }	
	public boolean mailAddressStreetNumberIsDirty() {
        return !valuesAreEqual(_mailAddressStreetNumberInitVal, _mailAddressStreetNumber);
    }

    public boolean mailAddressStreetNumberIsSet() {
        return _mailAddressStreetNumberIsSet;
    }	
	public boolean mailAddressZipIsDirty() {
        return !valuesAreEqual(_mailAddressZipInitVal, _mailAddressZip);
    }

    public boolean mailAddressZipIsSet() {
        return _mailAddressZipIsSet;
    }	
	public boolean mailAddressCityIsDirty() {
        return !valuesAreEqual(_mailAddressCityInitVal, _mailAddressCity);
    }

    public boolean mailAddressCityIsSet() {
        return _mailAddressCityIsSet;
    }	
	public boolean mailAddressCountryIsDirty() {
        return !valuesAreEqual(_mailAddressCountryInitVal, _mailAddressCountry);
    }

    public boolean mailAddressCountryIsSet() {
        return _mailAddressCountryIsSet;
    }	

	
    public void setEmployerAddressStreet(String employerAddressStreet) {
        if (!_employerAddressStreetIsSet) {
            _employerAddressStreetIsSet = true;
            _employerAddressStreetInitVal = employerAddressStreet;
        }
        _employerAddressStreet = employerAddressStreet;
    }

    public String getEmployerAddressStreet() {
        return _employerAddressStreet;
    }

    public String employerAddressStreetInitVal() {
        return _employerAddressStreetInitVal;
    }
    public void setEmployerAddressStreetNumber(String employerAddressStreetNumber) {
        if (!_employerAddressStreetNumberIsSet) {
            _employerAddressStreetNumberIsSet = true;
            _employerAddressStreetNumberInitVal = employerAddressStreetNumber;
        }
        _employerAddressStreetNumber = employerAddressStreetNumber;
    }

    public String getEmployerAddressStreetNumber() {
        return _employerAddressStreetNumber;
    }

    public String employerAddressStreetNumberInitVal() {
        return _employerAddressStreetNumberInitVal;
    }
    public void setEmployerAddressZip(String employerAddressZip) {
        if (!_employerAddressZipIsSet) {
            _employerAddressZipIsSet = true;
            _employerAddressZipInitVal = employerAddressZip;
        }
        _employerAddressZip = employerAddressZip;
    }

    public String getEmployerAddressZip() {
        return _employerAddressZip;
    }

    public String employerAddressZipInitVal() {
        return _employerAddressZipInitVal;
    }
    public void setEmployerAddressCity(String employerAddressCity) {
        if (!_employerAddressCityIsSet) {
            _employerAddressCityIsSet = true;
            _employerAddressCityInitVal = employerAddressCity;
        }
        _employerAddressCity = employerAddressCity;
    }

    public String getEmployerAddressCity() {
        return _employerAddressCity;
    }

    public String employerAddressCityInitVal() {
        return _employerAddressCityInitVal;
    }
    public void setEmployerAddressCountry(String employerAddressCountry) {
        if (!_employerAddressCountryIsSet) {
            _employerAddressCountryIsSet = true;
            _employerAddressCountryInitVal = employerAddressCountry;
        }
        _employerAddressCountry = employerAddressCountry;
    }

    public String getEmployerAddressCountry() {
        return _employerAddressCountry;
    }

    public String employerAddressCountryInitVal() {
        return _employerAddressCountryInitVal;
    }

	public boolean employerAddressSetIsDirty() {
		boolean result = false;

		result = result || employerAddressStreetIsDirty();		
		result = result || employerAddressStreetNumberIsDirty();		
		result = result || employerAddressZipIsDirty();		
		result = result || employerAddressCityIsDirty();		
		result = result || employerAddressCountryIsDirty();		
	
		return result;
	}
	
	public boolean employerAddressStreetIsDirty() {
        return !valuesAreEqual(_employerAddressStreetInitVal, _employerAddressStreet);
    }

    public boolean employerAddressStreetIsSet() {
        return _employerAddressStreetIsSet;
    }	
	public boolean employerAddressStreetNumberIsDirty() {
        return !valuesAreEqual(_employerAddressStreetNumberInitVal, _employerAddressStreetNumber);
    }

    public boolean employerAddressStreetNumberIsSet() {
        return _employerAddressStreetNumberIsSet;
    }	
	public boolean employerAddressZipIsDirty() {
        return !valuesAreEqual(_employerAddressZipInitVal, _employerAddressZip);
    }

    public boolean employerAddressZipIsSet() {
        return _employerAddressZipIsSet;
    }	
	public boolean employerAddressCityIsDirty() {
        return !valuesAreEqual(_employerAddressCityInitVal, _employerAddressCity);
    }

    public boolean employerAddressCityIsSet() {
        return _employerAddressCityIsSet;
    }	
	public boolean employerAddressCountryIsDirty() {
        return !valuesAreEqual(_employerAddressCountryInitVal, _employerAddressCountry);
    }

    public boolean employerAddressCountryIsSet() {
        return _employerAddressCountryIsSet;
    }	

	
    public void setCoEmployerAddressStreet(String coEmployerAddressStreet) {
        if (!_coEmployerAddressStreetIsSet) {
            _coEmployerAddressStreetIsSet = true;
            _coEmployerAddressStreetInitVal = coEmployerAddressStreet;
        }
        _coEmployerAddressStreet = coEmployerAddressStreet;
    }

    public String getCoEmployerAddressStreet() {
        return _coEmployerAddressStreet;
    }

    public String coEmployerAddressStreetInitVal() {
        return _coEmployerAddressStreetInitVal;
    }
    public void setCoEmployerAddressStreetNumber(String coEmployerAddressStreetNumber) {
        if (!_coEmployerAddressStreetNumberIsSet) {
            _coEmployerAddressStreetNumberIsSet = true;
            _coEmployerAddressStreetNumberInitVal = coEmployerAddressStreetNumber;
        }
        _coEmployerAddressStreetNumber = coEmployerAddressStreetNumber;
    }

    public String getCoEmployerAddressStreetNumber() {
        return _coEmployerAddressStreetNumber;
    }

    public String coEmployerAddressStreetNumberInitVal() {
        return _coEmployerAddressStreetNumberInitVal;
    }
    public void setCoEmployerAddressZip(String coEmployerAddressZip) {
        if (!_coEmployerAddressZipIsSet) {
            _coEmployerAddressZipIsSet = true;
            _coEmployerAddressZipInitVal = coEmployerAddressZip;
        }
        _coEmployerAddressZip = coEmployerAddressZip;
    }

    public String getCoEmployerAddressZip() {
        return _coEmployerAddressZip;
    }

    public String coEmployerAddressZipInitVal() {
        return _coEmployerAddressZipInitVal;
    }
    public void setCoEmployerAddressCity(String coEmployerAddressCity) {
        if (!_coEmployerAddressCityIsSet) {
            _coEmployerAddressCityIsSet = true;
            _coEmployerAddressCityInitVal = coEmployerAddressCity;
        }
        _coEmployerAddressCity = coEmployerAddressCity;
    }

    public String getCoEmployerAddressCity() {
        return _coEmployerAddressCity;
    }

    public String coEmployerAddressCityInitVal() {
        return _coEmployerAddressCityInitVal;
    }
    public void setCoEmployerAddressCountry(String coEmployerAddressCountry) {
        if (!_coEmployerAddressCountryIsSet) {
            _coEmployerAddressCountryIsSet = true;
            _coEmployerAddressCountryInitVal = coEmployerAddressCountry;
        }
        _coEmployerAddressCountry = coEmployerAddressCountry;
    }

    public String getCoEmployerAddressCountry() {
        return _coEmployerAddressCountry;
    }

    public String coEmployerAddressCountryInitVal() {
        return _coEmployerAddressCountryInitVal;
    }

	public boolean coEmployerAddressSetIsDirty() {
		boolean result = false;

		result = result || coEmployerAddressStreetIsDirty();		
		result = result || coEmployerAddressStreetNumberIsDirty();		
		result = result || coEmployerAddressZipIsDirty();		
		result = result || coEmployerAddressCityIsDirty();		
		result = result || coEmployerAddressCountryIsDirty();		
	
		return result;
	}
	
	public boolean coEmployerAddressStreetIsDirty() {
        return !valuesAreEqual(_coEmployerAddressStreetInitVal, _coEmployerAddressStreet);
    }

    public boolean coEmployerAddressStreetIsSet() {
        return _coEmployerAddressStreetIsSet;
    }	
	public boolean coEmployerAddressStreetNumberIsDirty() {
        return !valuesAreEqual(_coEmployerAddressStreetNumberInitVal, _coEmployerAddressStreetNumber);
    }

    public boolean coEmployerAddressStreetNumberIsSet() {
        return _coEmployerAddressStreetNumberIsSet;
    }	
	public boolean coEmployerAddressZipIsDirty() {
        return !valuesAreEqual(_coEmployerAddressZipInitVal, _coEmployerAddressZip);
    }

    public boolean coEmployerAddressZipIsSet() {
        return _coEmployerAddressZipIsSet;
    }	
	public boolean coEmployerAddressCityIsDirty() {
        return !valuesAreEqual(_coEmployerAddressCityInitVal, _coEmployerAddressCity);
    }

    public boolean coEmployerAddressCityIsSet() {
        return _coEmployerAddressCityIsSet;
    }	
	public boolean coEmployerAddressCountryIsDirty() {
        return !valuesAreEqual(_coEmployerAddressCountryInitVal, _coEmployerAddressCountry);
    }

    public boolean coEmployerAddressCountryIsSet() {
        return _coEmployerAddressCountryIsSet;
    }	


    public void setDefaultCountry(String defaultCountry) {
        if (!_defaultCountryIsSet) {
            _defaultCountryIsSet = true;
            _defaultCountryInitVal = defaultCountry;
        }
        _defaultCountry = defaultCountry;
    }

    public String getDefaultCountry() {
        return _defaultCountry;
    }

    public String defaultCountryInitVal() {
        return _defaultCountryInitVal;
    }

	public boolean defaultCountryIsDirty() {
        return !valuesAreEqual(_defaultCountryInitVal, _defaultCountry);
    }

    public boolean defaultCountryIsSet() {
        return _defaultCountryIsSet;
    }	

}
