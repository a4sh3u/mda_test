//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.registration;
            
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(show bank data)}

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'ShowBankDataForm'.
 *
 * @author generator
 */
public class ShowBankDataForm extends de.smava.webapp.account.todo.web.registration.BankDataForm implements Serializable {

    public static final String INTERNAL_BANK_ACCOUNT_TYPE_BIW_EXPR = "bankDataForm.internalBankAccountTypeBiw";
    public static final String INTERNAL_BANK_ACCOUNT_TYPE_BIW_PATH = "command.bankDataForm.internalBankAccountTypeBiw";
    public static final String INTERNAL_BANK_BIW_EXPR = "bankDataForm.internalBankBiw";
    public static final String INTERNAL_BANK_BIW_PATH = "command.bankDataForm.internalBankBiw";
    public static final String INTERNAL_ACCOUNT_NUMBER_BIW_EXPR = "bankDataForm.internalAccountNumberBiw";
    public static final String INTERNAL_ACCOUNT_NUMBER_BIW_PATH = "command.bankDataForm.internalAccountNumberBiw";
    public static final String INTERNAL_BANK_NUMBER_BIW_EXPR = "bankDataForm.internalBankNumberBiw";
    public static final String INTERNAL_BANK_NUMBER_BIW_PATH = "command.bankDataForm.internalBankNumberBiw";
    public static final String INTERNAL_BANK_CUSTOMER_NUMBER_BIW_EXPR = "bankDataForm.internalBankCustomerNumberBiw";
    public static final String INTERNAL_BANK_CUSTOMER_NUMBER_BIW_PATH = "command.bankDataForm.internalBankCustomerNumberBiw";
    public static final String INTERNAL_BANK_BIC_BIW_EXPR = "bankDataForm.internalBankBicBiw";
    public static final String INTERNAL_BANK_BIC_BIW_PATH = "command.bankDataForm.internalBankBicBiw";
    public static final String INTERNAL_BANK_IBAN_BIW_EXPR = "bankDataForm.internalBankIbanBiw";
    public static final String INTERNAL_BANK_IBAN_BIW_PATH = "command.bankDataForm.internalBankIbanBiw";
    public static final String DEPOSIT_OWNER_FIDOR_EXPR = "bankDataForm.depositOwnerFidor";
    public static final String DEPOSIT_OWNER_FIDOR_PATH = "command.bankDataForm.depositOwnerFidor";
    public static final String DEPOSIT_ACCOUNT_NUMBER_FIDOR_EXPR = "bankDataForm.depositAccountNumberFidor";
    public static final String DEPOSIT_ACCOUNT_NUMBER_FIDOR_PATH = "command.bankDataForm.depositAccountNumberFidor";
    public static final String DEPOSIT_BANK_CODE_FIDOR_EXPR = "bankDataForm.depositBankCodeFidor";
    public static final String DEPOSIT_BANK_CODE_FIDOR_PATH = "command.bankDataForm.depositBankCodeFidor";
    public static final String DEPOSIT_BANK_FIDOR_EXPR = "bankDataForm.depositBankFidor";
    public static final String DEPOSIT_BANK_FIDOR_PATH = "command.bankDataForm.depositBankFidor";
    public static final String DEPOSIT_INTERNAL_NUMBER_FIDOR_EXPR = "bankDataForm.depositInternalNumberFidor";
    public static final String DEPOSIT_INTERNAL_NUMBER_FIDOR_PATH = "command.bankDataForm.depositInternalNumberFidor";
    public static final String DEPOSIT_IBAN_FIDOR_EXPR = "bankDataForm.depositIbanFidor";
    public static final String DEPOSIT_IBAN_FIDOR_PATH = "command.bankDataForm.depositIbanFidor";
    public static final String DEPOSIT_BANK_BIC_FIDOR_EXPR = "bankDataForm.depositBankBicFidor";
    public static final String DEPOSIT_BANK_BIC_FIDOR_PATH = "command.bankDataForm.depositBankBicFidor";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(show bank data)}

	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 1384764225927639053L;

// !!!!!!!! End of insert code section !!!!!!!!

    private String _internalBankAccountTypeBiw;
    private String _internalBankAccountTypeBiwInitVal;
    private boolean _internalBankAccountTypeBiwIsSet;
    private String _internalBankBiw;
    private String _internalBankBiwInitVal;
    private boolean _internalBankBiwIsSet;
    private String _internalAccountNumberBiw;
    private String _internalAccountNumberBiwInitVal;
    private boolean _internalAccountNumberBiwIsSet;
    private String _internalBankNumberBiw;
    private String _internalBankNumberBiwInitVal;
    private boolean _internalBankNumberBiwIsSet;
    private String _internalBankCustomerNumberBiw;
    private String _internalBankCustomerNumberBiwInitVal;
    private boolean _internalBankCustomerNumberBiwIsSet;
    private String _internalBankBicBiw;
    private String _internalBankBicBiwInitVal;
    private boolean _internalBankBicBiwIsSet;
    private String _internalBankIbanBiw;
    private String _internalBankIbanBiwInitVal;
    private boolean _internalBankIbanBiwIsSet;
    private String _depositOwnerFidor;
    private String _depositOwnerFidorInitVal;
    private boolean _depositOwnerFidorIsSet;
    private String _depositAccountNumberFidor;
    private String _depositAccountNumberFidorInitVal;
    private boolean _depositAccountNumberFidorIsSet;
    private String _depositBankCodeFidor;
    private String _depositBankCodeFidorInitVal;
    private boolean _depositBankCodeFidorIsSet;
    private String _depositBankFidor;
    private String _depositBankFidorInitVal;
    private boolean _depositBankFidorIsSet;
    private String _depositInternalNumberFidor;
    private String _depositInternalNumberFidorInitVal;
    private boolean _depositInternalNumberFidorIsSet;
    private String _depositIbanFidor;
    private String _depositIbanFidorInitVal;
    private boolean _depositIbanFidorIsSet;
    private String _depositBankBicFidor;
    private String _depositBankBicFidorInitVal;
    private boolean _depositBankBicFidorIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setInternalBankAccountTypeBiw(String internalBankAccountTypeBiw) {
        if (!_internalBankAccountTypeBiwIsSet) {
            _internalBankAccountTypeBiwIsSet = true;
            _internalBankAccountTypeBiwInitVal = internalBankAccountTypeBiw;
        }
        _internalBankAccountTypeBiw = internalBankAccountTypeBiw;
    }

    public String getInternalBankAccountTypeBiw() {
        return _internalBankAccountTypeBiw;
    }

    public String internalBankAccountTypeBiwInitVal() {
        return _internalBankAccountTypeBiwInitVal;
    }

	public boolean internalBankAccountTypeBiwIsDirty() {
        return !valuesAreEqual(_internalBankAccountTypeBiwInitVal, _internalBankAccountTypeBiw);
    }

    public boolean internalBankAccountTypeBiwIsSet() {
        return _internalBankAccountTypeBiwIsSet;
    }	


    public void setInternalBankBiw(String internalBankBiw) {
        if (!_internalBankBiwIsSet) {
            _internalBankBiwIsSet = true;
            _internalBankBiwInitVal = internalBankBiw;
        }
        _internalBankBiw = internalBankBiw;
    }

    public String getInternalBankBiw() {
        return _internalBankBiw;
    }

    public String internalBankBiwInitVal() {
        return _internalBankBiwInitVal;
    }

	public boolean internalBankBiwIsDirty() {
        return !valuesAreEqual(_internalBankBiwInitVal, _internalBankBiw);
    }

    public boolean internalBankBiwIsSet() {
        return _internalBankBiwIsSet;
    }	


    public void setInternalAccountNumberBiw(String internalAccountNumberBiw) {
        if (!_internalAccountNumberBiwIsSet) {
            _internalAccountNumberBiwIsSet = true;
            _internalAccountNumberBiwInitVal = internalAccountNumberBiw;
        }
        _internalAccountNumberBiw = internalAccountNumberBiw;
    }

    public String getInternalAccountNumberBiw() {
        return _internalAccountNumberBiw;
    }

    public String internalAccountNumberBiwInitVal() {
        return _internalAccountNumberBiwInitVal;
    }

	public boolean internalAccountNumberBiwIsDirty() {
        return !valuesAreEqual(_internalAccountNumberBiwInitVal, _internalAccountNumberBiw);
    }

    public boolean internalAccountNumberBiwIsSet() {
        return _internalAccountNumberBiwIsSet;
    }	


    public void setInternalBankNumberBiw(String internalBankNumberBiw) {
        if (!_internalBankNumberBiwIsSet) {
            _internalBankNumberBiwIsSet = true;
            _internalBankNumberBiwInitVal = internalBankNumberBiw;
        }
        _internalBankNumberBiw = internalBankNumberBiw;
    }

    public String getInternalBankNumberBiw() {
        return _internalBankNumberBiw;
    }

    public String internalBankNumberBiwInitVal() {
        return _internalBankNumberBiwInitVal;
    }

	public boolean internalBankNumberBiwIsDirty() {
        return !valuesAreEqual(_internalBankNumberBiwInitVal, _internalBankNumberBiw);
    }

    public boolean internalBankNumberBiwIsSet() {
        return _internalBankNumberBiwIsSet;
    }	


    public void setInternalBankCustomerNumberBiw(String internalBankCustomerNumberBiw) {
        if (!_internalBankCustomerNumberBiwIsSet) {
            _internalBankCustomerNumberBiwIsSet = true;
            _internalBankCustomerNumberBiwInitVal = internalBankCustomerNumberBiw;
        }
        _internalBankCustomerNumberBiw = internalBankCustomerNumberBiw;
    }

    public String getInternalBankCustomerNumberBiw() {
        return _internalBankCustomerNumberBiw;
    }

    public String internalBankCustomerNumberBiwInitVal() {
        return _internalBankCustomerNumberBiwInitVal;
    }

	public boolean internalBankCustomerNumberBiwIsDirty() {
        return !valuesAreEqual(_internalBankCustomerNumberBiwInitVal, _internalBankCustomerNumberBiw);
    }

    public boolean internalBankCustomerNumberBiwIsSet() {
        return _internalBankCustomerNumberBiwIsSet;
    }	


    public void setInternalBankBicBiw(String internalBankBicBiw) {
        if (!_internalBankBicBiwIsSet) {
            _internalBankBicBiwIsSet = true;
            _internalBankBicBiwInitVal = internalBankBicBiw;
        }
        _internalBankBicBiw = internalBankBicBiw;
    }

    public String getInternalBankBicBiw() {
        return _internalBankBicBiw;
    }

    public String internalBankBicBiwInitVal() {
        return _internalBankBicBiwInitVal;
    }

	public boolean internalBankBicBiwIsDirty() {
        return !valuesAreEqual(_internalBankBicBiwInitVal, _internalBankBicBiw);
    }

    public boolean internalBankBicBiwIsSet() {
        return _internalBankBicBiwIsSet;
    }	


    public void setInternalBankIbanBiw(String internalBankIbanBiw) {
        if (!_internalBankIbanBiwIsSet) {
            _internalBankIbanBiwIsSet = true;
            _internalBankIbanBiwInitVal = internalBankIbanBiw;
        }
        _internalBankIbanBiw = internalBankIbanBiw;
    }

    public String getInternalBankIbanBiw() {
        return _internalBankIbanBiw;
    }

    public String internalBankIbanBiwInitVal() {
        return _internalBankIbanBiwInitVal;
    }

	public boolean internalBankIbanBiwIsDirty() {
        return !valuesAreEqual(_internalBankIbanBiwInitVal, _internalBankIbanBiw);
    }

    public boolean internalBankIbanBiwIsSet() {
        return _internalBankIbanBiwIsSet;
    }	


    public void setDepositOwnerFidor(String depositOwnerFidor) {
        if (!_depositOwnerFidorIsSet) {
            _depositOwnerFidorIsSet = true;
            _depositOwnerFidorInitVal = depositOwnerFidor;
        }
        _depositOwnerFidor = depositOwnerFidor;
    }

    public String getDepositOwnerFidor() {
        return _depositOwnerFidor;
    }

    public String depositOwnerFidorInitVal() {
        return _depositOwnerFidorInitVal;
    }

	public boolean depositOwnerFidorIsDirty() {
        return !valuesAreEqual(_depositOwnerFidorInitVal, _depositOwnerFidor);
    }

    public boolean depositOwnerFidorIsSet() {
        return _depositOwnerFidorIsSet;
    }	


    public void setDepositAccountNumberFidor(String depositAccountNumberFidor) {
        if (!_depositAccountNumberFidorIsSet) {
            _depositAccountNumberFidorIsSet = true;
            _depositAccountNumberFidorInitVal = depositAccountNumberFidor;
        }
        _depositAccountNumberFidor = depositAccountNumberFidor;
    }

    public String getDepositAccountNumberFidor() {
        return _depositAccountNumberFidor;
    }

    public String depositAccountNumberFidorInitVal() {
        return _depositAccountNumberFidorInitVal;
    }

	public boolean depositAccountNumberFidorIsDirty() {
        return !valuesAreEqual(_depositAccountNumberFidorInitVal, _depositAccountNumberFidor);
    }

    public boolean depositAccountNumberFidorIsSet() {
        return _depositAccountNumberFidorIsSet;
    }	


    public void setDepositBankCodeFidor(String depositBankCodeFidor) {
        if (!_depositBankCodeFidorIsSet) {
            _depositBankCodeFidorIsSet = true;
            _depositBankCodeFidorInitVal = depositBankCodeFidor;
        }
        _depositBankCodeFidor = depositBankCodeFidor;
    }

    public String getDepositBankCodeFidor() {
        return _depositBankCodeFidor;
    }

    public String depositBankCodeFidorInitVal() {
        return _depositBankCodeFidorInitVal;
    }

	public boolean depositBankCodeFidorIsDirty() {
        return !valuesAreEqual(_depositBankCodeFidorInitVal, _depositBankCodeFidor);
    }

    public boolean depositBankCodeFidorIsSet() {
        return _depositBankCodeFidorIsSet;
    }	


    public void setDepositBankFidor(String depositBankFidor) {
        if (!_depositBankFidorIsSet) {
            _depositBankFidorIsSet = true;
            _depositBankFidorInitVal = depositBankFidor;
        }
        _depositBankFidor = depositBankFidor;
    }

    public String getDepositBankFidor() {
        return _depositBankFidor;
    }

    public String depositBankFidorInitVal() {
        return _depositBankFidorInitVal;
    }

	public boolean depositBankFidorIsDirty() {
        return !valuesAreEqual(_depositBankFidorInitVal, _depositBankFidor);
    }

    public boolean depositBankFidorIsSet() {
        return _depositBankFidorIsSet;
    }	


    public void setDepositInternalNumberFidor(String depositInternalNumberFidor) {
        if (!_depositInternalNumberFidorIsSet) {
            _depositInternalNumberFidorIsSet = true;
            _depositInternalNumberFidorInitVal = depositInternalNumberFidor;
        }
        _depositInternalNumberFidor = depositInternalNumberFidor;
    }

    public String getDepositInternalNumberFidor() {
        return _depositInternalNumberFidor;
    }

    public String depositInternalNumberFidorInitVal() {
        return _depositInternalNumberFidorInitVal;
    }

	public boolean depositInternalNumberFidorIsDirty() {
        return !valuesAreEqual(_depositInternalNumberFidorInitVal, _depositInternalNumberFidor);
    }

    public boolean depositInternalNumberFidorIsSet() {
        return _depositInternalNumberFidorIsSet;
    }	


    public void setDepositIbanFidor(String depositIbanFidor) {
        if (!_depositIbanFidorIsSet) {
            _depositIbanFidorIsSet = true;
            _depositIbanFidorInitVal = depositIbanFidor;
        }
        _depositIbanFidor = depositIbanFidor;
    }

    public String getDepositIbanFidor() {
        return _depositIbanFidor;
    }

    public String depositIbanFidorInitVal() {
        return _depositIbanFidorInitVal;
    }

	public boolean depositIbanFidorIsDirty() {
        return !valuesAreEqual(_depositIbanFidorInitVal, _depositIbanFidor);
    }

    public boolean depositIbanFidorIsSet() {
        return _depositIbanFidorIsSet;
    }	


    public void setDepositBankBicFidor(String depositBankBicFidor) {
        if (!_depositBankBicFidorIsSet) {
            _depositBankBicFidorIsSet = true;
            _depositBankBicFidorInitVal = depositBankBicFidor;
        }
        _depositBankBicFidor = depositBankBicFidor;
    }

    public String getDepositBankBicFidor() {
        return _depositBankBicFidor;
    }

    public String depositBankBicFidorInitVal() {
        return _depositBankBicFidorInitVal;
    }

	public boolean depositBankBicFidorIsDirty() {
        return !valuesAreEqual(_depositBankBicFidorInitVal, _depositBankBicFidor);
    }

    public boolean depositBankBicFidorIsSet() {
        return _depositBankBicFidorIsSet;
    }	

}
