//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse-ganymede\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse-ganymede\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.web;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(user data)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'UserDataForm'.
 *
 * @author generator
 */
public class UserDataForm implements Serializable {

	public static final String USERNAME_EXPR = "userDataForm.username";
    public static final String USERNAME_PATH = "command.userDataForm.username";
    public static final String EMAIL_EXPR = "userDataForm.email";
    public static final String EMAIL_PATH = "command.userDataForm.email";
    public static final String EMAIL2_EXPR = "userDataForm.email2";
    public static final String EMAIL2_PATH = "command.userDataForm.email2";
    public static final String PASSWORD_EXPR = "userDataForm.password";
    public static final String PASSWORD_PATH = "command.userDataForm.password";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(user data)}
    /**
     * generated serial.
     */
    private static final long serialVersionUID = 2867500788997853017L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _username;
    private String _usernameInitVal;
    private boolean _usernameIsSet;
    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private String _email2;
    private String _email2InitVal;
    private boolean _email2IsSet;
    private String _password;
    private String _passwordInitVal;
    private boolean _passwordIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setUsername(String username) {
        if (!_usernameIsSet) {
            _usernameIsSet = true;
            _usernameInitVal = username;
        }
        _username = username;
    }

    public String getUsername() {
        return _username;
    }

    public String usernameInitVal() {
        return _usernameInitVal;
    }

	public boolean usernameIsDirty() {
        return !valuesAreEqual(_usernameInitVal, _username);
    }

    public boolean usernameIsSet() {
        return _usernameIsSet;
    }	


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setEmail2(String email2) {
        if (!_email2IsSet) {
            _email2IsSet = true;
            _email2InitVal = email2;
        }
        _email2 = email2;
    }

    public String getEmail2() {
        return _email2;
    }

    public String email2InitVal() {
        return _email2InitVal;
    }

	public boolean email2IsDirty() {
        return !valuesAreEqual(_email2InitVal, _email2);
    }

    public boolean email2IsSet() {
        return _email2IsSet;
    }	


    public void setPassword(String password) {
        if (!_passwordIsSet) {
            _passwordIsSet = true;
            _passwordInitVal = password;
        }
        _password = password;
    }

    public String getPassword() {
        return _password;
    }

    public String passwordInitVal() {
        return _passwordInitVal;
    }

	public boolean passwordIsDirty() {
        return !valuesAreEqual(_passwordInitVal, _password);
    }

    public boolean passwordIsSet() {
        return _passwordIsSet;
    }	

}
