//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.casi.search;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(casi search)}
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'CasiSearchForm'.
 *
 * @author generator
 */
public class CasiSearchForm implements Serializable {

    public static final String FIRST_NAME_EXPR = "casiSearchForm.firstName";
    public static final String FIRST_NAME_PATH = "command.casiSearchForm.firstName";
    public static final String LAST_NAME_EXPR = "casiSearchForm.lastName";
    public static final String LAST_NAME_PATH = "command.casiSearchForm.lastName";
    public static final String EMAIL_EXPR = "casiSearchForm.email";
    public static final String EMAIL_PATH = "command.casiSearchForm.email";
    public static final String ACCOUNT_ID_EXPR = "casiSearchForm.accountId";
    public static final String ACCOUNT_ID_PATH = "command.casiSearchForm.accountId";
    public static final String PHONE_EXPR = "casiSearchForm.phone";
    public static final String PHONE_PATH = "command.casiSearchForm.phone";
    public static final String BIRTH_DATE_EXPR = "casiSearchForm.birthDate";
    public static final String BIRTH_DATE_PATH = "command.casiSearchForm.birthDate";
    public static final String ACCOUNT_DATE_EXPR = "casiSearchForm.accountDate";
    public static final String ACCOUNT_DATE_PATH = "command.casiSearchForm.accountDate";
    public static final String CREDIT_ADVISORS_EXPR = "casiSearchForm.creditAdvisors";
    public static final String CREDIT_ADVISORS_PATH = "command.casiSearchForm.creditAdvisors";
    public static final String CREDIT_ADVISOR_EXPR = "casiSearchForm.creditAdvisor";
    public static final String CREDIT_ADVISOR_PATH = "command.casiSearchForm.creditAdvisor";
    public static final String DOCUMENT_TYPES_FOR_CALLS_EXPR = "casiSearchForm.documentTypesForCalls";
    public static final String DOCUMENT_TYPES_FOR_CALLS_PATH = "command.casiSearchForm.documentTypesForCalls";
    public static final String CHOSEN_DOCUMENT_TYPES_EXPR = "casiSearchForm.chosenDocumentTypes";
    public static final String CHOSEN_DOCUMENT_TYPES_PATH = "command.casiSearchForm.chosenDocumentTypes";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(casi search)}
    private static final long serialVersionUID = 8056033894602706055L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _firstName;
    private String _firstNameInitVal;
    private boolean _firstNameIsSet;
    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;
    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private String _accountId;
    private String _accountIdInitVal;
    private boolean _accountIdIsSet;
    private String _phone;
    private String _phoneInitVal;
    private boolean _phoneIsSet;
    private Date _birthDate;
    private Date _birthDateInitVal;
    private boolean _birthDateIsSet;
    private String _accountDate;
    private String _accountDateInitVal;
    private boolean _accountDateIsSet;
    private Map<Long, String> _creditAdvisors;
    private Map<Long, String> _creditAdvisorsInitVal;
    private boolean _creditAdvisorsIsSet;
    private Long _creditAdvisor;
    private Long _creditAdvisorInitVal;
    private boolean _creditAdvisorIsSet;
    private Map<String, String> _documentTypesForCalls;
    private Map<String, String> _documentTypesForCallsInitVal;
    private boolean _documentTypesForCallsIsSet;
    private List<String> _chosenDocumentTypes;
    private List<String> _chosenDocumentTypesInitVal;
    private boolean _chosenDocumentTypesIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = firstName;
        }
        _firstName = firstName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String firstNameInitVal() {
        return _firstNameInitVal;
    }

	public boolean firstNameIsDirty() {
        return !valuesAreEqual(_firstNameInitVal, _firstName);
    }

    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }	


    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

	public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }	


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setAccountId(String accountId) {
        if (!_accountIdIsSet) {
            _accountIdIsSet = true;
            _accountIdInitVal = accountId;
        }
        _accountId = accountId;
    }

    public String getAccountId() {
        return _accountId;
    }

    public String accountIdInitVal() {
        return _accountIdInitVal;
    }

	public boolean accountIdIsDirty() {
        return !valuesAreEqual(_accountIdInitVal, _accountId);
    }

    public boolean accountIdIsSet() {
        return _accountIdIsSet;
    }	


    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = phone;
        }
        _phone = phone;
    }

    public String getPhone() {
        return _phone;
    }

    public String phoneInitVal() {
        return _phoneInitVal;
    }

	public boolean phoneIsDirty() {
        return !valuesAreEqual(_phoneInitVal, _phone);
    }

    public boolean phoneIsSet() {
        return _phoneIsSet;
    }	


    public void setBirthDate(Date birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;

            if (birthDate == null) {
                _birthDateInitVal = null;
            } else {
                _birthDateInitVal = (Date) birthDate.clone();
            }
        }

        if (birthDate == null) {
            _birthDate = null;
        } else {
            _birthDate = (Date) birthDate.clone();
        }
    }

    public Date getBirthDate() {
        Date birthDate = null;
        if (_birthDate != null) {
            birthDate = (Date) _birthDate.clone();
        }
        return birthDate;
    }

    public Date birthDateInitVal() {
        Date birthDateInitVal = null;
        if (_birthDateInitVal != null) {
            birthDateInitVal = (Date) _birthDateInitVal.clone();
        }
        return birthDateInitVal;
    }

	public boolean birthDateIsDirty() {
        return !valuesAreEqual(_birthDateInitVal, _birthDate);
    }

    public boolean birthDateIsSet() {
        return _birthDateIsSet;
    }	


    public void setAccountDate(String accountDate) {
        if (!_accountDateIsSet) {
            _accountDateIsSet = true;
            _accountDateInitVal = accountDate;
        }
        _accountDate = accountDate;
    }

    public String getAccountDate() {
        return _accountDate;
    }

    public String accountDateInitVal() {
        return _accountDateInitVal;
    }

	public boolean accountDateIsDirty() {
        return !valuesAreEqual(_accountDateInitVal, _accountDate);
    }

    public boolean accountDateIsSet() {
        return _accountDateIsSet;
    }	


    public void setCreditAdvisors(Map<Long, String> creditAdvisors) {
        if (!_creditAdvisorsIsSet) {
            _creditAdvisorsIsSet = true;
            _creditAdvisorsInitVal = creditAdvisors;
        }
        _creditAdvisors = creditAdvisors;
    }

    public Map<Long, String> getCreditAdvisors() {
        return _creditAdvisors;
    }

    public Map<Long, String> creditAdvisorsInitVal() {
        return _creditAdvisorsInitVal;
    }

	public boolean creditAdvisorsIsDirty() {
        return !valuesAreEqual(_creditAdvisorsInitVal, _creditAdvisors);
    }

    public boolean creditAdvisorsIsSet() {
        return _creditAdvisorsIsSet;
    }	


    public void setCreditAdvisor(Long creditAdvisor) {
        if (!_creditAdvisorIsSet) {
            _creditAdvisorIsSet = true;
            _creditAdvisorInitVal = creditAdvisor;
        }
        _creditAdvisor = creditAdvisor;
    }

    public Long getCreditAdvisor() {
        return _creditAdvisor;
    }

    public Long creditAdvisorInitVal() {
        return _creditAdvisorInitVal;
    }

	public boolean creditAdvisorIsDirty() {
        return !valuesAreEqual(_creditAdvisorInitVal, _creditAdvisor);
    }

    public boolean creditAdvisorIsSet() {
        return _creditAdvisorIsSet;
    }	


    public void setDocumentTypesForCalls(Map<String, String> documentTypesForCalls) {
        if (!_documentTypesForCallsIsSet) {
            _documentTypesForCallsIsSet = true;
            _documentTypesForCallsInitVal = documentTypesForCalls;
        }
        _documentTypesForCalls = documentTypesForCalls;
    }

    public Map<String, String> getDocumentTypesForCalls() {
        return _documentTypesForCalls;
    }

    public Map<String, String> documentTypesForCallsInitVal() {
        return _documentTypesForCallsInitVal;
    }

	public boolean documentTypesForCallsIsDirty() {
        return !valuesAreEqual(_documentTypesForCallsInitVal, _documentTypesForCalls);
    }

    public boolean documentTypesForCallsIsSet() {
        return _documentTypesForCallsIsSet;
    }	


    public void setChosenDocumentTypes(List<String> chosenDocumentTypes) {
        if (!_chosenDocumentTypesIsSet) {
            _chosenDocumentTypesIsSet = true;
            _chosenDocumentTypesInitVal = chosenDocumentTypes;
        }
        _chosenDocumentTypes = chosenDocumentTypes;
    }

    public List<String> getChosenDocumentTypes() {
        return _chosenDocumentTypes;
    }

    public List<String> chosenDocumentTypesInitVal() {
        return _chosenDocumentTypesInitVal;
    }

	public boolean chosenDocumentTypesIsDirty() {
        return !valuesAreEqual(_chosenDocumentTypesInitVal, _chosenDocumentTypes);
    }

    public boolean chosenDocumentTypesIsSet() {
        return _chosenDocumentTypesIsSet;
    }	

}
