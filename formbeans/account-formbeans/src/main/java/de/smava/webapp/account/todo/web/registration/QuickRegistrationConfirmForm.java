package de.smava.webapp.account.todo.web.registration;

/**
 * This form is only created because the aperto generator uses this name in the quick_registration_confirm.jsp.
 *
 * @author ingmar.decker
 */
public class QuickRegistrationConfirmForm extends QuickRegistrationForm {
}