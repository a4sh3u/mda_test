/**
 * 
 */
package de.smava.webapp.account.todo.web.registration;

import java.io.Serializable;

/**
 * @author bvoss
 *
 */
public interface ChecksNeededSupportForm extends Serializable {
	
	void setTermAndCondCheckNeeded(Boolean needed);
	
	void setSchufaNeeded(Boolean needed);
	
	void setPrivacyCheckNeeded(boolean needed);
	
	void setFidorTermAndCondCheckNeeded(Boolean needed);
	
	void setFidorServiceProviderRestrictionNeeded(Boolean fidorServiceProviderRestrictionNeeded);
}
