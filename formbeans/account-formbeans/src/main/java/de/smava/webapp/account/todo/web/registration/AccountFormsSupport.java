/**
 * 
 */
package de.smava.webapp.account.todo.web.registration;

import de.smava.webapp.account.todo.web.account.CreditWorthinessForm;
import de.smava.webapp.account.todo.web.account.EmploymentDataForm;

/**
 * @author dimitar
 *
 */
public interface AccountFormsSupport {

	AddressDataForm getAddressDataForm();
	PersonalDataForm getPersonalDataForm();
//	Collection<FreelancerIncomeForm> getFreelancerIncomeForms();
	CreditWorthinessForm getCreditWorthinessForm();
	CreditWorthinessForm getCreditWorthinessForm2();
	EmploymentDataForm getEmploymentDataForm();
	EmploymentDataForm getEmploymentDataForm2();
}
