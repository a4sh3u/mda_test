//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.account;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo assets transaction)}

import de.smava.webapp.account.domain.Offer;
import de.smava.webapp.account.domain.Transaction;

import java.io.Serializable;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoAssetsTransactionForm'.
 *
 * @author generator
 */
public class BoAssetsTransactionForm implements Serializable {

    public static final String OFFERS_EXPR = "boAssetsTransactionForm.offers";
    public static final String OFFERS_PATH = "command.boAssetsTransactionForm.offers";
    public static final String PAYOUT_TRANSACTIONS_EXPR = "boAssetsTransactionForm.payoutTransactions";
    public static final String PAYOUT_TRANSACTIONS_PATH = "command.boAssetsTransactionForm.payoutTransactions";
    public static final String DEPOSIT_TRANSACTIONS_EXPR = "boAssetsTransactionForm.depositTransactions";
    public static final String DEPOSIT_TRANSACTIONS_PATH = "command.boAssetsTransactionForm.depositTransactions";
    public static final String INVESTMENT_TRANSACTIONS_EXPR = "boAssetsTransactionForm.investmentTransactions";
    public static final String INVESTMENT_TRANSACTIONS_PATH = "command.boAssetsTransactionForm.investmentTransactions";
    public static final String PAYOUT_SUM_EXPR = "boAssetsTransactionForm.payoutSum";
    public static final String PAYOUT_SUM_PATH = "command.boAssetsTransactionForm.payoutSum";
    public static final String DEPOSIT_SUM_EXPR = "boAssetsTransactionForm.depositSum";
    public static final String DEPOSIT_SUM_PATH = "command.boAssetsTransactionForm.depositSum";
    public static final String INVESTMENT_SUM_EXPR = "boAssetsTransactionForm.investmentSum";
    public static final String INVESTMENT_SUM_PATH = "command.boAssetsTransactionForm.investmentSum";
    public static final String ASSETS_SUM_EXPR = "boAssetsTransactionForm.assetsSum";
    public static final String ASSETS_SUM_PATH = "command.boAssetsTransactionForm.assetsSum";
    public static final String ASSETS_SUM_FIDOR_EXPR = "boAssetsTransactionForm.assetsSumFidor";
    public static final String ASSETS_SUM_FIDOR_PATH = "command.boAssetsTransactionForm.assetsSumFidor";
    public static final String CHARGES_EXPR = "boAssetsTransactionForm.charges";
    public static final String CHARGES_PATH = "command.boAssetsTransactionForm.charges";
    public static final String SCHEDULED_CHARGES_EXPR = "boAssetsTransactionForm.scheduledCharges";
    public static final String SCHEDULED_CHARGES_PATH = "command.boAssetsTransactionForm.scheduledCharges";
    public static final String BOUND_SUM_EXPR = "boAssetsTransactionForm.boundSum";
    public static final String BOUND_SUM_PATH = "command.boAssetsTransactionForm.boundSum";
    public static final String BOUND_SUM_FIDOR_EXPR = "boAssetsTransactionForm.boundSumFidor";
    public static final String BOUND_SUM_FIDOR_PATH = "command.boAssetsTransactionForm.boundSumFidor";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo assets transaction)}
    
    public static final String BOUND_SUM_TOTAL_EXPR = "boAssetsTransactionForm.boundSumTotal";
    public static final String BOUND_SUM_TOTAL_PATH = "command.boAssetsTransactionForm.boundSumTotal";

    private Double _boundSumTotal;
    private Double _boundSumTotalInitVal;
    private boolean _boundSumTotalIsSet;
    
    public void setBoundSumTotal(Double boundSumTotal) {
        if (!_boundSumTotalIsSet) {
            _boundSumTotalIsSet = true;
            _boundSumTotalInitVal = boundSumTotal;
        }
        _boundSumTotal = boundSumTotal;
    }

    public Double getBoundSumTotal() {
        return _boundSumTotal;
    }

    public Double boundSumTotalInitVal() {
        return _boundSumTotalInitVal;
    }

	public boolean boundSumTotalIsDirty() {
        return !valuesAreEqual(_boundSumTotalInitVal, _boundSumTotal);
    }

    public boolean boundSumTotalIsSet() {
        return _boundSumTotalIsSet;
    }	
    /**
     * generated serial.
     */
    private static final long serialVersionUID = 1L;
// !!!!!!!! End of insert code section !!!!!!!!

    private List<Offer> _offers;
    private List<Offer> _offersInitVal;
    private boolean _offersIsSet;
    private List<Transaction> _payoutTransactions;
    private List<Transaction> _payoutTransactionsInitVal;
    private boolean _payoutTransactionsIsSet;
    private List<Transaction> _depositTransactions;
    private List<Transaction> _depositTransactionsInitVal;
    private boolean _depositTransactionsIsSet;
    private List<Transaction> _investmentTransactions;
    private List<Transaction> _investmentTransactionsInitVal;
    private boolean _investmentTransactionsIsSet;
    private Double _payoutSum;
    private Double _payoutSumInitVal;
    private boolean _payoutSumIsSet;
    private Double _depositSum;
    private Double _depositSumInitVal;
    private boolean _depositSumIsSet;
    private Double _investmentSum;
    private Double _investmentSumInitVal;
    private boolean _investmentSumIsSet;
    private Double _assetsSum;
    private Double _assetsSumInitVal;
    private boolean _assetsSumIsSet;
    private Double _assetsSumFidor;
    private Double _assetsSumFidorInitVal;
    private boolean _assetsSumFidorIsSet;
    private Double _charges;
    private Double _chargesInitVal;
    private boolean _chargesIsSet;
    private Double _scheduledCharges;
    private Double _scheduledChargesInitVal;
    private boolean _scheduledChargesIsSet;
    private Double _boundSum;
    private Double _boundSumInitVal;
    private boolean _boundSumIsSet;
    private Double _boundSumFidor;
    private Double _boundSumFidorInitVal;
    private boolean _boundSumFidorIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setOffers(List<Offer> offers) {
        if (!_offersIsSet) {
            _offersIsSet = true;
            _offersInitVal = offers;
        }
        _offers = offers;
    }

    public List<Offer> getOffers() {
        return _offers;
    }

    public List<Offer> offersInitVal() {
        return _offersInitVal;
    }

	public boolean offersIsDirty() {
        return !valuesAreEqual(_offersInitVal, _offers);
    }

    public boolean offersIsSet() {
        return _offersIsSet;
    }	


    public void setPayoutTransactions(List<Transaction> payoutTransactions) {
        if (!_payoutTransactionsIsSet) {
            _payoutTransactionsIsSet = true;
            _payoutTransactionsInitVal = payoutTransactions;
        }
        _payoutTransactions = payoutTransactions;
    }

    public List<Transaction> getPayoutTransactions() {
        return _payoutTransactions;
    }

    public List<Transaction> payoutTransactionsInitVal() {
        return _payoutTransactionsInitVal;
    }

	public boolean payoutTransactionsIsDirty() {
        return !valuesAreEqual(_payoutTransactionsInitVal, _payoutTransactions);
    }

    public boolean payoutTransactionsIsSet() {
        return _payoutTransactionsIsSet;
    }	


    public void setDepositTransactions(List<Transaction> depositTransactions) {
        if (!_depositTransactionsIsSet) {
            _depositTransactionsIsSet = true;
            _depositTransactionsInitVal = depositTransactions;
        }
        _depositTransactions = depositTransactions;
    }

    public List<Transaction> getDepositTransactions() {
        return _depositTransactions;
    }

    public List<Transaction> depositTransactionsInitVal() {
        return _depositTransactionsInitVal;
    }

	public boolean depositTransactionsIsDirty() {
        return !valuesAreEqual(_depositTransactionsInitVal, _depositTransactions);
    }

    public boolean depositTransactionsIsSet() {
        return _depositTransactionsIsSet;
    }	


    public void setInvestmentTransactions(List<Transaction> investmentTransactions) {
        if (!_investmentTransactionsIsSet) {
            _investmentTransactionsIsSet = true;
            _investmentTransactionsInitVal = investmentTransactions;
        }
        _investmentTransactions = investmentTransactions;
    }

    public List<Transaction> getInvestmentTransactions() {
        return _investmentTransactions;
    }

    public List<Transaction> investmentTransactionsInitVal() {
        return _investmentTransactionsInitVal;
    }

	public boolean investmentTransactionsIsDirty() {
        return !valuesAreEqual(_investmentTransactionsInitVal, _investmentTransactions);
    }

    public boolean investmentTransactionsIsSet() {
        return _investmentTransactionsIsSet;
    }	


    public void setPayoutSum(Double payoutSum) {
        if (!_payoutSumIsSet) {
            _payoutSumIsSet = true;
            _payoutSumInitVal = payoutSum;
        }
        _payoutSum = payoutSum;
    }

    public Double getPayoutSum() {
        return _payoutSum;
    }

    public Double payoutSumInitVal() {
        return _payoutSumInitVal;
    }

	public boolean payoutSumIsDirty() {
        return !valuesAreEqual(_payoutSumInitVal, _payoutSum);
    }

    public boolean payoutSumIsSet() {
        return _payoutSumIsSet;
    }	


    public void setDepositSum(Double depositSum) {
        if (!_depositSumIsSet) {
            _depositSumIsSet = true;
            _depositSumInitVal = depositSum;
        }
        _depositSum = depositSum;
    }

    public Double getDepositSum() {
        return _depositSum;
    }

    public Double depositSumInitVal() {
        return _depositSumInitVal;
    }

	public boolean depositSumIsDirty() {
        return !valuesAreEqual(_depositSumInitVal, _depositSum);
    }

    public boolean depositSumIsSet() {
        return _depositSumIsSet;
    }	


    public void setInvestmentSum(Double investmentSum) {
        if (!_investmentSumIsSet) {
            _investmentSumIsSet = true;
            _investmentSumInitVal = investmentSum;
        }
        _investmentSum = investmentSum;
    }

    public Double getInvestmentSum() {
        return _investmentSum;
    }

    public Double investmentSumInitVal() {
        return _investmentSumInitVal;
    }

	public boolean investmentSumIsDirty() {
        return !valuesAreEqual(_investmentSumInitVal, _investmentSum);
    }

    public boolean investmentSumIsSet() {
        return _investmentSumIsSet;
    }	


    public void setAssetsSum(Double assetsSum) {
        if (!_assetsSumIsSet) {
            _assetsSumIsSet = true;
            _assetsSumInitVal = assetsSum;
        }
        _assetsSum = assetsSum;
    }

    public Double getAssetsSum() {
        return _assetsSum;
    }

    public Double assetsSumInitVal() {
        return _assetsSumInitVal;
    }

	public boolean assetsSumIsDirty() {
        return !valuesAreEqual(_assetsSumInitVal, _assetsSum);
    }

    public boolean assetsSumIsSet() {
        return _assetsSumIsSet;
    }	


    public void setAssetsSumFidor(Double assetsSumFidor) {
        if (!_assetsSumFidorIsSet) {
            _assetsSumFidorIsSet = true;
            _assetsSumFidorInitVal = assetsSumFidor;
        }
        _assetsSumFidor = assetsSumFidor;
    }

    public Double getAssetsSumFidor() {
        return _assetsSumFidor;
    }

    public Double assetsSumFidorInitVal() {
        return _assetsSumFidorInitVal;
    }

	public boolean assetsSumFidorIsDirty() {
        return !valuesAreEqual(_assetsSumFidorInitVal, _assetsSumFidor);
    }

    public boolean assetsSumFidorIsSet() {
        return _assetsSumFidorIsSet;
    }	


    public void setCharges(Double charges) {
        if (!_chargesIsSet) {
            _chargesIsSet = true;
            _chargesInitVal = charges;
        }
        _charges = charges;
    }

    public Double getCharges() {
        return _charges;
    }

    public Double chargesInitVal() {
        return _chargesInitVal;
    }

	public boolean chargesIsDirty() {
        return !valuesAreEqual(_chargesInitVal, _charges);
    }

    public boolean chargesIsSet() {
        return _chargesIsSet;
    }	


    public void setScheduledCharges(Double scheduledCharges) {
        if (!_scheduledChargesIsSet) {
            _scheduledChargesIsSet = true;
            _scheduledChargesInitVal = scheduledCharges;
        }
        _scheduledCharges = scheduledCharges;
    }

    public Double getScheduledCharges() {
        return _scheduledCharges;
    }

    public Double scheduledChargesInitVal() {
        return _scheduledChargesInitVal;
    }

	public boolean scheduledChargesIsDirty() {
        return !valuesAreEqual(_scheduledChargesInitVal, _scheduledCharges);
    }

    public boolean scheduledChargesIsSet() {
        return _scheduledChargesIsSet;
    }	


    public void setBoundSum(Double boundSum) {
        if (!_boundSumIsSet) {
            _boundSumIsSet = true;
            _boundSumInitVal = boundSum;
        }
        _boundSum = boundSum;
    }

    public Double getBoundSum() {
        return _boundSum;
    }

    public Double boundSumInitVal() {
        return _boundSumInitVal;
    }

	public boolean boundSumIsDirty() {
        return !valuesAreEqual(_boundSumInitVal, _boundSum);
    }

    public boolean boundSumIsSet() {
        return _boundSumIsSet;
    }	


    public void setBoundSumFidor(Double boundSumFidor) {
        if (!_boundSumFidorIsSet) {
            _boundSumFidorIsSet = true;
            _boundSumFidorInitVal = boundSumFidor;
        }
        _boundSumFidor = boundSumFidor;
    }

    public Double getBoundSumFidor() {
        return _boundSumFidor;
    }

    public Double boundSumFidorInitVal() {
        return _boundSumFidorInitVal;
    }

	public boolean boundSumFidorIsDirty() {
        return !valuesAreEqual(_boundSumFidorInitVal, _boundSumFidor);
    }

    public boolean boundSumFidorIsSet() {
        return _boundSumFidorIsSet;
    }	
    
    

}
