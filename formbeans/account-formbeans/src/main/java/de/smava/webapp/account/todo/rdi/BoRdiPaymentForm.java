//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    D:\projects\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    D:\projects\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.rdi;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo rdi payment)}
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoRdiPaymentForm'.
 *
 * @author generator
 */
public class BoRdiPaymentForm {

    public static final String ID_EXPR = "boRdiPaymentForm.id";
    public static final String ID_PATH = "command.boRdiPaymentForm.id";
    public static final String STATE_EXPR = "boRdiPaymentForm.state";
    public static final String STATE_PATH = "command.boRdiPaymentForm.state";
    public static final String TYPE_EXPR = "boRdiPaymentForm.type";
    public static final String TYPE_PATH = "command.boRdiPaymentForm.type";
    public static final String CREATION_DATE_EXPR = "boRdiPaymentForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boRdiPaymentForm.creationDate";
    public static final String EXPIRATION_DATE_EXPR = "boRdiPaymentForm.expirationDate";
    public static final String EXPIRATION_DATE_PATH = "command.boRdiPaymentForm.expirationDate";
    public static final String FROM_DATE_EXPR = "boRdiPaymentForm.fromDate";
    public static final String FROM_DATE_PATH = "command.boRdiPaymentForm.fromDate";
    public static final String TO_DATE_EXPR = "boRdiPaymentForm.toDate";
    public static final String TO_DATE_PATH = "command.boRdiPaymentForm.toDate";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo rdi payment)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Long _id;
    private Long _idInitVal;
    private boolean _idIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _expirationDate;
    private String _expirationDateInitVal;
    private boolean _expirationDateIsSet;
    private String _fromDate;
    private String _fromDateInitVal;
    private boolean _fromDateIsSet;
    private String _toDate;
    private String _toDateInitVal;
    private boolean _toDateIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setId(Long id) {
        if (!_idIsSet) {
            _idIsSet = true;
            _idInitVal = id;
        }
        _id = id;
    }

    public Long getId() {
        return _id;
    }

    public Long idInitVal() {
        return _idInitVal;
    }

	public boolean idIsDirty() {
        return !valuesAreEqual(_idInitVal, _id);
    }

    public boolean idIsSet() {
        return _idIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setExpirationDate(String expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = expirationDate;
        }
        _expirationDate = expirationDate;
    }

    public String getExpirationDate() {
        return _expirationDate;
    }

    public String expirationDateInitVal() {
        return _expirationDateInitVal;
    }

	public boolean expirationDateIsDirty() {
        return !valuesAreEqual(_expirationDateInitVal, _expirationDate);
    }

    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }	


    public void setFromDate(String fromDate) {
        if (!_fromDateIsSet) {
            _fromDateIsSet = true;
            _fromDateInitVal = fromDate;
        }
        _fromDate = fromDate;
    }

    public String getFromDate() {
        return _fromDate;
    }

    public String fromDateInitVal() {
        return _fromDateInitVal;
    }

	public boolean fromDateIsDirty() {
        return !valuesAreEqual(_fromDateInitVal, _fromDate);
    }

    public boolean fromDateIsSet() {
        return _fromDateIsSet;
    }	


    public void setToDate(String toDate) {
        if (!_toDateIsSet) {
            _toDateIsSet = true;
            _toDateInitVal = toDate;
        }
        _toDate = toDate;
    }

    public String getToDate() {
        return _toDate;
    }

    public String toDateInitVal() {
        return _toDateInitVal;
    }

	public boolean toDateIsDirty() {
        return !valuesAreEqual(_toDateInitVal, _toDate);
    }

    public boolean toDateIsSet() {
        return _toDateIsSet;
    }	

}
