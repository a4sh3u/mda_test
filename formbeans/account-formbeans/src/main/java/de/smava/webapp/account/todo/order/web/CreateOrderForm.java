package de.smava.webapp.account.todo.order.web;
    
import java.io.Serializable;

/**
 * Form class 'CreateOrderForm'.
 *
 * @author generator not any longer!!! ist in der model.xml schon 'ewig' auskommentiert
 */
public class CreateOrderForm implements Serializable {

    public static final String CREDIT_AMOUNT_EXPR = "createOrderForm.creditAmount";
    public static final String CREDIT_AMOUNT_PATH = "command.createOrderForm.creditAmount";
    public static final String CREDIT_TERM_EXPR = "createOrderForm.creditTerm";
    public static final String CREDIT_TERM_PATH = "command.createOrderForm.creditTerm";
    public static final String INTEREST_RATES_EXPR = "createOrderForm.interestRates";
    public static final String INTEREST_RATES_PATH = "command.createOrderForm.interestRates";
    public static final String LEAD_TIME_INTEREST_EXPR = "createOrderForm.leadTimeInterest";
    public static final String LEAD_TIME_INTEREST_PATH = "command.createOrderForm.leadTimeInterest";
    public static final String FIRST_INTEREST_RATES_EXPR = "createOrderForm.firstInterestRates";
    public static final String FIRST_INTEREST_RATES_PATH = "command.createOrderForm.firstInterestRates";
    public static final String SMAVA_GROUP_BONUS_EXPR = "createOrderForm.smavaGroupBonus";
    public static final String SMAVA_GROUP_BONUS_PATH = "command.createOrderForm.smavaGroupBonus";
    public static final String CATEGORY_EXPR = "createOrderForm.category";
    public static final String CATEGORY_PATH = "command.createOrderForm.category";
    public static final String TITLE_EXPR = "createOrderForm.title";
    public static final String TITLE_PATH = "command.createOrderForm.title";
    public static final String DESCRIPTION_EXPR = "createOrderForm.description";
    public static final String DESCRIPTION_PATH = "command.createOrderForm.description";
    public static final String IMAGE_EXPR = "createOrderForm.image";
    public static final String IMAGE_PATH = "command.createOrderForm.image";
    public static final String IMAGEFILE_EXPR = "createOrderForm.imagefile";
    public static final String IMAGEFILE_PATH = "command.createOrderForm.imagefile";
    public static final String NOIMAGE_EXPR = "createOrderForm.noimage";
    public static final String NOIMAGE_PATH = "command.createOrderForm.noimage";
    public static final String LAST_UPDATE_EXPR = "createOrderForm.lastUpdate";
    public static final String LAST_UPDATE_PATH = "command.createOrderForm.lastUpdate";
    public static final String VALID_DAYS_EXPR = "createOrderForm.validDays";
    public static final String VALID_DAYS_PATH = "command.createOrderForm.validDays";
    public static final String DIR_DEBIT_MANDATE_EXPR = "createOrderForm.dirDebitMandate";
    public static final String DIR_DEBIT_MANDATE_PATH = "command.createOrderForm.dirDebitMandate";
    public static final String BANK_EXPR = "createOrderForm.bank";
    public static final String BANK_PATH = "command.createOrderForm.bank";
    public static final String BANK_CODE_EXPR = "createOrderForm.bankCode";
    public static final String BANK_CODE_PATH = "command.createOrderForm.bankCode";
    public static final String BANK_ACCOUNT_EXPR = "createOrderForm.bankAccount";
    public static final String BANK_ACCOUNT_PATH = "command.createOrderForm.bankAccount";
    public static final String TERMS_AND_CONDITIONS_EXPR = "createOrderForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_PATH = "command.createOrderForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_APPLIED_EXPR = "createOrderForm.termsAndConditionsApplied";
    public static final String TERMS_AND_CONDITIONS_APPLIED_PATH = "command.createOrderForm.termsAndConditionsApplied";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(create order)}
    private byte[] _imageBytes = null;

    public byte[] getImageBytes() {
        if (_imageBytes == null && _imagefile != null && !_imagefile.isEmpty()) {
            _imageBytes = _imagefile.getBytes();
        }

        byte[] imageBytes = null;
        if (_imageBytes != null) {
            imageBytes = _imageBytes.clone();
        }
        return imageBytes;
    }

    public void setImageBytes(byte[] bytes) {
        if (bytes == null) {
            _imageBytes = null;
        } else {
            _imageBytes = bytes.clone();
        }
    }
// !!!!!!!! End of insert code section !!!!!!!!


    private String _creditAmount;
    private String _creditAmountInitVal;
    private boolean _creditAmountIsSet;

    private String _creditTerm;
    private String _creditTermInitVal;
    private boolean _creditTermIsSet;

    private String _interestRates;
    private String _interestRatesInitVal;
    private boolean _interestRatesIsSet;

    private String _leadTimeInterest;
    private String _leadTimeInterestInitVal;
    private boolean _leadTimeInterestIsSet;

    private String _firstInterestRates;
    private String _firstInterestRatesInitVal;
    private boolean _firstInterestRatesIsSet;

    private String _smavaGroupBonus;
    private String _smavaGroupBonusInitVal;
    private boolean _smavaGroupBonusIsSet;

    private Long _category;
    private Long _categoryInitVal;
    private boolean _categoryIsSet;

    private String _title;
    private String _titleInitVal;
    private boolean _titleIsSet;

    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;

    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;

    private org.springframework.web.multipart.commons.CommonsMultipartFile _imagefile;
    private org.springframework.web.multipart.commons.CommonsMultipartFile _imagefileInitVal;
    private boolean _imagefileIsSet;

    private Boolean _noimage;
    private Boolean _noimageInitVal;
    private boolean _noimageIsSet;

    private Long _lastUpdate;
    private Long _lastUpdateInitVal;
    private boolean _lastUpdateIsSet;

    private Integer _validDays;
    private Integer _validDaysInitVal;
    private boolean _validDaysIsSet;

    private Boolean _dirDebitMandate;
    private Boolean _dirDebitMandateInitVal;
    private boolean _dirDebitMandateIsSet;

    private String _bank;
    private String _bankInitVal;
    private boolean _bankIsSet;

    private String _bankCode;
    private String _bankCodeInitVal;
    private boolean _bankCodeIsSet;

    private String _bankAccount;
    private String _bankAccountInitVal;
    private boolean _bankAccountIsSet;

    private Boolean _termsAndConditions;
    private Boolean _termsAndConditionsInitVal;
    private boolean _termsAndConditionsIsSet;

    private Boolean _termsAndConditionsApplied;
    private Boolean _termsAndConditionsAppliedInitVal;
    private boolean _termsAndConditionsAppliedIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setCreditAmount(String creditAmount) {
        if (!_creditAmountIsSet) {
            _creditAmountIsSet = true;
            _creditAmountInitVal = creditAmount;
        }
        _creditAmount = creditAmount;
    }

    public String getCreditAmount() {
        return _creditAmount;
    }

    public String creditAmountInitVal() {
        return _creditAmountInitVal;
    }

    public boolean creditAmountIsDirty() {
        return !valuesAreEqual(_creditAmountInitVal, _creditAmount);
    }

    public boolean creditAmountIsSet() {
        return _creditAmountIsSet;
    }

    public void setCreditTerm(String creditTerm) {
        if (!_creditTermIsSet) {
            _creditTermIsSet = true;
            _creditTermInitVal = creditTerm;
        }
        _creditTerm = creditTerm;
    }

    public String getCreditTerm() {
        return _creditTerm;
    }

    public String creditTermInitVal() {
        return _creditTermInitVal;
    }

    public boolean creditTermIsDirty() {
        return !valuesAreEqual(_creditTermInitVal, _creditTerm);
    }

    public boolean creditTermIsSet() {
        return _creditTermIsSet;
    }

    public void setInterestRates(String interestRates) {
        if (!_interestRatesIsSet) {
            _interestRatesIsSet = true;
            _interestRatesInitVal = interestRates;
        }
        _interestRates = interestRates;
    }

    public String getInterestRates() {
        return _interestRates;
    }

    public String interestRatesInitVal() {
        return _interestRatesInitVal;
    }

    public boolean interestRatesIsDirty() {
        return !valuesAreEqual(_interestRatesInitVal, _interestRates);
    }

    public boolean interestRatesIsSet() {
        return _interestRatesIsSet;
    }

    public void setLeadTimeInterest(String leadTimeInterest) {
        if (!_leadTimeInterestIsSet) {
            _leadTimeInterestIsSet = true;
            _leadTimeInterestInitVal = leadTimeInterest;
        }
        _leadTimeInterest = leadTimeInterest;
    }

    public String getLeadTimeInterest() {
        return _leadTimeInterest;
    }

    public String leadTimeInterestInitVal() {
        return _leadTimeInterestInitVal;
    }

    public boolean leadTimeInterestIsDirty() {
        return !valuesAreEqual(_leadTimeInterestInitVal, _leadTimeInterest);
    }

    public boolean leadTimeInterestIsSet() {
        return _leadTimeInterestIsSet;
    }

    public void setFirstInterestRates(String firstInterestRates) {
        if (!_firstInterestRatesIsSet) {
            _firstInterestRatesIsSet = true;
            _firstInterestRatesInitVal = firstInterestRates;
        }
        _firstInterestRates = firstInterestRates;
    }

    public String getFirstInterestRates() {
        return _firstInterestRates;
    }

    public String firstInterestRatesInitVal() {
        return _firstInterestRatesInitVal;
    }

    public boolean firstInterestRatesIsDirty() {
        return !valuesAreEqual(_firstInterestRatesInitVal, _firstInterestRates);
    }

    public boolean firstInterestRatesIsSet() {
        return _firstInterestRatesIsSet;
    }

    public void setSmavaGroupBonus(String smavaGroupBonus) {
        if (!_smavaGroupBonusIsSet) {
            _smavaGroupBonusIsSet = true;
            _smavaGroupBonusInitVal = smavaGroupBonus;
        }
        _smavaGroupBonus = smavaGroupBonus;
    }

    public String getSmavaGroupBonus() {
        return _smavaGroupBonus;
    }

    public String smavaGroupBonusInitVal() {
        return _smavaGroupBonusInitVal;
    }

    public boolean smavaGroupBonusIsDirty() {
        return !valuesAreEqual(_smavaGroupBonusInitVal, _smavaGroupBonus);
    }

    public boolean smavaGroupBonusIsSet() {
        return _smavaGroupBonusIsSet;
    }

    public void setCategory(Long category) {
        if (!_categoryIsSet) {
            _categoryIsSet = true;
            _categoryInitVal = category;
        }
        _category = category;
    }

    public Long getCategory() {
        return _category;
    }

    public Long categoryInitVal() {
        return _categoryInitVal;
    }

    public boolean categoryIsDirty() {
        return !valuesAreEqual(_categoryInitVal, _category);
    }

    public boolean categoryIsSet() {
        return _categoryIsSet;
    }

    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = title;
        }
        _title = title;
    }

    public String getTitle() {
        return _title;
    }

    public String titleInitVal() {
        return _titleInitVal;
    }

    public boolean titleIsDirty() {
        return !valuesAreEqual(_titleInitVal, _title);
    }

    public boolean titleIsSet() {
        return _titleIsSet;
    }

    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

    public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }

    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

    public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }

    public void setImagefile(org.springframework.web.multipart.commons.CommonsMultipartFile imagefile) {
        if (!_imagefileIsSet) {
            _imagefileIsSet = true;
            _imagefileInitVal = imagefile;
        }
        _imagefile = imagefile;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile getImagefile() {
        return _imagefile;
    }

    public org.springframework.web.multipart.commons.CommonsMultipartFile imagefileInitVal() {
        return _imagefileInitVal;
    }

    public boolean imagefileIsDirty() {
        return !valuesAreEqual(_imagefileInitVal, _imagefile);
    }

    public boolean imagefileIsSet() {
        return _imagefileIsSet;
    }

    public void setNoimage(Boolean noimage) {
        if (!_noimageIsSet) {
            _noimageIsSet = true;
            _noimageInitVal = noimage;
        }
        _noimage = noimage;
    }

    public Boolean getNoimage() {
        return _noimage;
    }

    public Boolean noimageInitVal() {
        return _noimageInitVal;
    }

    public boolean noimageIsDirty() {
        return !valuesAreEqual(_noimageInitVal, _noimage);
    }

    public boolean noimageIsSet() {
        return _noimageIsSet;
    }

    public void setLastUpdate(Long lastUpdate) {
        if (!_lastUpdateIsSet) {
            _lastUpdateIsSet = true;
            _lastUpdateInitVal = lastUpdate;
        }
        _lastUpdate = lastUpdate;
    }

    public Long getLastUpdate() {
        return _lastUpdate;
    }

    public Long lastUpdateInitVal() {
        return _lastUpdateInitVal;
    }

    public boolean lastUpdateIsDirty() {
        return !valuesAreEqual(_lastUpdateInitVal, _lastUpdate);
    }

    public boolean lastUpdateIsSet() {
        return _lastUpdateIsSet;
    }

    public void setValidDays(Integer validDays) {
        if (!_validDaysIsSet) {
            _validDaysIsSet = true;
            _validDaysInitVal = validDays;
        }
        _validDays = validDays;
    }

    public Integer getValidDays() {
        return _validDays;
    }

    public Integer validDaysInitVal() {
        return _validDaysInitVal;
    }

    public boolean validDaysIsDirty() {
        return !valuesAreEqual(_validDaysInitVal, _validDays);
    }

    public boolean validDaysIsSet() {
        return _validDaysIsSet;
    }

    public void setDirDebitMandate(Boolean dirDebitMandate) {
        if (!_dirDebitMandateIsSet) {
            _dirDebitMandateIsSet = true;
            _dirDebitMandateInitVal = dirDebitMandate;
        }
        _dirDebitMandate = dirDebitMandate;
    }

    public Boolean getDirDebitMandate() {
        return _dirDebitMandate;
    }

    public Boolean dirDebitMandateInitVal() {
        return _dirDebitMandateInitVal;
    }

    public boolean dirDebitMandateIsDirty() {
        return !valuesAreEqual(_dirDebitMandateInitVal, _dirDebitMandate);
    }

    public boolean dirDebitMandateIsSet() {
        return _dirDebitMandateIsSet;
    }

    public void setBank(String bank) {
        if (!_bankIsSet) {
            _bankIsSet = true;
            _bankInitVal = bank;
        }
        _bank = bank;
    }

    public String getBank() {
        return _bank;
    }

    public String bankInitVal() {
        return _bankInitVal;
    }

    public boolean bankIsDirty() {
        return !valuesAreEqual(_bankInitVal, _bank);
    }

    public boolean bankIsSet() {
        return _bankIsSet;
    }

    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = bankCode;
        }
        _bankCode = bankCode;
    }

    public String getBankCode() {
        return _bankCode;
    }

    public String bankCodeInitVal() {
        return _bankCodeInitVal;
    }

    public boolean bankCodeIsDirty() {
        return !valuesAreEqual(_bankCodeInitVal, _bankCode);
    }

    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }

    public void setBankAccount(String bankAccount) {
        if (!_bankAccountIsSet) {
            _bankAccountIsSet = true;
            _bankAccountInitVal = bankAccount;
        }
        _bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return _bankAccount;
    }

    public String bankAccountInitVal() {
        return _bankAccountInitVal;
    }

    public boolean bankAccountIsDirty() {
        return !valuesAreEqual(_bankAccountInitVal, _bankAccount);
    }

    public boolean bankAccountIsSet() {
        return _bankAccountIsSet;
    }

    public void setTermsAndConditions(Boolean termsAndConditions) {
        if (!_termsAndConditionsIsSet) {
            _termsAndConditionsIsSet = true;
            _termsAndConditionsInitVal = termsAndConditions;
        }
        _termsAndConditions = termsAndConditions;
    }

    public Boolean getTermsAndConditions() {
        return _termsAndConditions;
    }

    public Boolean termsAndConditionsInitVal() {
        return _termsAndConditionsInitVal;
    }

    public boolean termsAndConditionsIsDirty() {
        return !valuesAreEqual(_termsAndConditionsInitVal, _termsAndConditions);
    }

    public boolean termsAndConditionsIsSet() {
        return _termsAndConditionsIsSet;
    }

    public void setTermsAndConditionsApplied(Boolean termsAndConditionsApplied) {
        if (!_termsAndConditionsAppliedIsSet) {
            _termsAndConditionsAppliedIsSet = true;
            _termsAndConditionsAppliedInitVal = termsAndConditionsApplied;
        }
        _termsAndConditionsApplied = termsAndConditionsApplied;
    }

    public Boolean getTermsAndConditionsApplied() {
        return _termsAndConditionsApplied;
    }

    public Boolean termsAndConditionsAppliedInitVal() {
        return _termsAndConditionsAppliedInitVal;
    }

    public boolean termsAndConditionsAppliedIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAppliedInitVal, _termsAndConditionsApplied);
    }

    public boolean termsAndConditionsAppliedIsSet() {
        return _termsAndConditionsAppliedIsSet;
    }

}
