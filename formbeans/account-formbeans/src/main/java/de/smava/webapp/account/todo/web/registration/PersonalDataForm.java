//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.registration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(personal data)}

import org.apache.commons.lang.StringUtils;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'PersonalDataForm'.
 *
 * @author generator
 */
public class PersonalDataForm implements de.smava.webapp.account.todo.web.registration.ChecksNeededSupportForm {

    public static final String CUSTOMER_NUMBER_EXPR = "personalDataForm.customerNumber";
    public static final String CUSTOMER_NUMBER_PATH = "command.personalDataForm.customerNumber";
    public static final String IS_LENDER_EXPR = "personalDataForm.isLender";
    public static final String IS_LENDER_PATH = "command.personalDataForm.isLender";
    public static final String IS_BORROWER_EXPR = "personalDataForm.isBorrower";
    public static final String IS_BORROWER_PATH = "command.personalDataForm.isBorrower";
    public static final String IS_EDIT_EXPR = "personalDataForm.isEdit";
    public static final String IS_EDIT_PATH = "command.personalDataForm.isEdit";
    public static final String IS_SHOW_EXPR = "personalDataForm.isShow";
    public static final String IS_SHOW_PATH = "command.personalDataForm.isShow";
    public static final String SALUTATION_EXPR = "personalDataForm.salutation";
    public static final String SALUTATION_PATH = "command.personalDataForm.salutation";
    public static final String LAST_NAME_EXPR = "personalDataForm.lastName";
    public static final String LAST_NAME_PATH = "command.personalDataForm.lastName";
    public static final String BIRTH_NAME_EXPR = "personalDataForm.birthName";
    public static final String BIRTH_NAME_PATH = "command.personalDataForm.birthName";
    public static final String FIRST_NAME_EXPR = "personalDataForm.firstName";
    public static final String FIRST_NAME_PATH = "command.personalDataForm.firstName";
    public static final String TITLE_EXPR = "personalDataForm.title";
    public static final String TITLE_PATH = "command.personalDataForm.title";
    public static final String CITIZENSHIP_EXPR = "personalDataForm.citizenship";
    public static final String CITIZENSHIP_PATH = "command.personalDataForm.citizenship";
    public static final String COUNTRY_OF_BIRTH_EXPR = "personalDataForm.countryOfBirth";
    public static final String COUNTRY_OF_BIRTH_PATH = "command.personalDataForm.countryOfBirth";
    public static final String PHONE_EXPR = "personalDataForm.phone";
    public static final String PHONE_PATH = "command.personalDataForm.phone";
    public static final String PHONE_CODE_EXPR = "personalDataForm.phoneCode";
    public static final String PHONE_CODE_PATH = "command.personalDataForm.phoneCode";
    public static final String PHONE2_EXPR = "personalDataForm.phone2";
    public static final String PHONE2_PATH = "command.personalDataForm.phone2";
    public static final String PHONE2_CODE_EXPR = "personalDataForm.phone2Code";
    public static final String PHONE2_CODE_PATH = "command.personalDataForm.phone2Code";
    public static final String BIRTH_DATE_DAY_EXPR = "personalDataForm.birthDateDay";
    public static final String BIRTH_DATE_DAY_PATH = "command.personalDataForm.birthDateDay";
    public static final String BIRTH_DATE_MONTH_EXPR = "personalDataForm.birthDateMonth";
    public static final String BIRTH_DATE_MONTH_PATH = "command.personalDataForm.birthDateMonth";
    public static final String BIRTH_DATE_YEAR_EXPR = "personalDataForm.birthDateYear";
    public static final String BIRTH_DATE_YEAR_PATH = "command.personalDataForm.birthDateYear";
    public static final String PLACE_OF_BIRTH_EXPR = "personalDataForm.placeOfBirth";
    public static final String PLACE_OF_BIRTH_PATH = "command.personalDataForm.placeOfBirth";
    public static final String FAMILY_STATUS_EXPR = "personalDataForm.familyStatus";
    public static final String FAMILY_STATUS_PATH = "command.personalDataForm.familyStatus";
    public static final String PESEL_EXPR = "personalDataForm.pesel";
    public static final String PESEL_PATH = "command.personalDataForm.pesel";
    public static final String IDENTITY_CARD_NUMBER_EXPR = "personalDataForm.identityCardNumber";
    public static final String IDENTITY_CARD_NUMBER_PATH = "command.personalDataForm.identityCardNumber";
    public static final String NIP_EXPR = "personalDataForm.nip";
    public static final String NIP_PATH = "command.personalDataForm.nip";
    public static final String USERNAME_EXPR = "personalDataForm.username";
    public static final String USERNAME_PATH = "command.personalDataForm.username";
    public static final String EMAIL_EXPR = "personalDataForm.email";
    public static final String EMAIL_PATH = "command.personalDataForm.email";
    public static final String EMAIL2_EXPR = "personalDataForm.email2";
    public static final String EMAIL2_PATH = "command.personalDataForm.email2";
    public static final String SCHUFA_EXPR = "personalDataForm.schufa";
    public static final String SCHUFA_PATH = "command.personalDataForm.schufa";
    public static final String SCHUFA_NEEDED_EXPR = "personalDataForm.schufaNeeded";
    public static final String SCHUFA_NEEDED_PATH = "command.personalDataForm.schufaNeeded";
    public static final String NEGATIVE_SCHUFA_EXPR = "personalDataForm.negativeSchufa";
    public static final String NEGATIVE_SCHUFA_PATH = "command.personalDataForm.negativeSchufa";
    public static final String PRIVACY_EXPR = "personalDataForm.privacy";
    public static final String PRIVACY_PATH = "command.personalDataForm.privacy";
    public static final String PRIVACY_DATE_EXPR = "personalDataForm.privacyDate";
    public static final String PRIVACY_DATE_PATH = "command.personalDataForm.privacyDate";
    public static final String PRIVACY_CHECK_NEEDED_EXPR = "personalDataForm.privacyCheckNeeded";
    public static final String PRIVACY_CHECK_NEEDED_PATH = "command.personalDataForm.privacyCheckNeeded";
    public static final String BIRTH_DATE_EXPR = "personalDataForm.birthDate";
    public static final String BIRTH_DATE_PATH = "command.personalDataForm.birthDate";
    public static final String FOR_OWN_ACCOUNT_EXPR = "personalDataForm.forOwnAccount";
    public static final String FOR_OWN_ACCOUNT_PATH = "command.personalDataForm.forOwnAccount";
    public static final String TERM_AND_COND_CHECK_EXPR = "personalDataForm.termAndCondCheck";
    public static final String TERM_AND_COND_CHECK_PATH = "command.personalDataForm.termAndCondCheck";
    public static final String TERM_AND_COND_CHECK_NEEDED_EXPR = "personalDataForm.termAndCondCheckNeeded";
    public static final String TERM_AND_COND_CHECK_NEEDED_PATH = "command.personalDataForm.termAndCondCheckNeeded";
    public static final String TERM_AND_COND_APPLIED_EXPR = "personalDataForm.termAndCondApplied";
    public static final String TERM_AND_COND_APPLIED_PATH = "command.personalDataForm.termAndCondApplied";
    public static final String INVITATION_CODE_RADIO_EXPR = "personalDataForm.invitationCodeRadio";
    public static final String INVITATION_CODE_RADIO_PATH = "command.personalDataForm.invitationCodeRadio";
    public static final String INVITATION_CODE_EXPR = "personalDataForm.invitationCode";
    public static final String INVITATION_CODE_PATH = "command.personalDataForm.invitationCode";
    public static final String EDUCATION_EARNED_EXPR = "personalDataForm.educationEarned";
    public static final String EDUCATION_EARNED_PATH = "command.personalDataForm.educationEarned";
    public static final String FIDOR_TERM_AND_COND_CHECK_EXPR = "personalDataForm.fidorTermAndCondCheck";
    public static final String FIDOR_TERM_AND_COND_CHECK_PATH = "command.personalDataForm.fidorTermAndCondCheck";
    public static final String FIDOR_TERM_AND_COND_CHECK_NEEDED_EXPR = "personalDataForm.fidorTermAndCondCheckNeeded";
    public static final String FIDOR_TERM_AND_COND_CHECK_NEEDED_PATH = "command.personalDataForm.fidorTermAndCondCheckNeeded";
    public static final String FIDOR_TERM_AND_COND_CHECK_IGNORE_EXPR = "personalDataForm.fidorTermAndCondCheckIgnore";
    public static final String FIDOR_TERM_AND_COND_CHECK_IGNORE_PATH = "command.personalDataForm.fidorTermAndCondCheckIgnore";
    public static final String FIDOR_SERVICE_PROVIDER_RESTRICTION_EXPR = "personalDataForm.fidorServiceProviderRestriction";
    public static final String FIDOR_SERVICE_PROVIDER_RESTRICTION_PATH = "command.personalDataForm.fidorServiceProviderRestriction";
    public static final String FIDOR_SERVICE_PROVIDER_RESTRICTION_NEEDED_EXPR = "personalDataForm.fidorServiceProviderRestrictionNeeded";
    public static final String FIDOR_SERVICE_PROVIDER_RESTRICTION_NEEDED_PATH = "command.personalDataForm.fidorServiceProviderRestrictionNeeded";
    public static final String FAKE_UNIQUE_PERSON_EXPR = "personalDataForm.fakeUniquePerson";
    public static final String FAKE_UNIQUE_PERSON_PATH = "command.personalDataForm.fakeUniquePerson";
    public static final String LAST_REQUESTED_AMOUNT_EXPR = "personalDataForm.lastRequestedAmount";
    public static final String LAST_REQUESTED_AMOUNT_PATH = "command.personalDataForm.lastRequestedAmount";
    public static final String RELIGION_EXPR = "personalDataForm.religion";
    public static final String RELIGION_PATH = "command.personalDataForm.religion";
    public static final String ARE_TAXES_PAID_IN_USA_EXPR = "personalDataForm.areTaxesPaidInUsa";
    public static final String ARE_TAXES_PAID_IN_USA_PATH = "command.personalDataForm.areTaxesPaidInUsa";
    public static final String IS_FOREIGN_ACCOUNT_TAX_COMPLIANCE_ACT_EXPR = "personalDataForm.isForeignAccountTaxComplianceAct";
    public static final String IS_FOREIGN_ACCOUNT_TAX_COMPLIANCE_ACT_PATH = "command.personalDataForm.isForeignAccountTaxComplianceAct";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(personal data)}
    /**
	 * generated serial.
	 */
	private static final long serialVersionUID = 6898272425497614284L;
	
	public boolean areAllFieldsEmpty() {
		if (StringUtils.isNotBlank(_birthDate)) {
				return false;
		}
		if (StringUtils.isNotBlank(_birthDateDay)) {
				return false;
		} 
		if (StringUtils.isNotBlank(_birthDateMonth)) {
				return false;
		}
		if (StringUtils.isNotBlank(_birthDateYear)) {
				return false;
		}
		if (StringUtils.isNotBlank(_firstName)) {
				return false;
		} 
		if (StringUtils.isNotBlank(_lastName)) {
				return false;
		}
		if (StringUtils.isNotBlank(_salutation)) {
				return false;
		}
		if (StringUtils.isNotBlank(_citizenship)) {
				return false;
		}
		if (StringUtils.isNotBlank(_countryOfBirth)) {
				return false;
		}
		if (StringUtils.isNotBlank(_phone)) {
				return false;
		}
		if (StringUtils.isNotBlank(_phoneCode)) {
				return false;
		}
		if (StringUtils.isNotBlank(_phone2)) {
				return false;
		}
		if (StringUtils.isNotBlank(_phone2Code)) {
				return false;
		}
		if (StringUtils.isNotBlank(_placeOfBirth)) {
				return false;
		}
		if (StringUtils.isNotBlank(_familyStatus)) {
				return false;
		}
		if (StringUtils.isNotBlank(_pesel)) {
				return false;
		}
		if (StringUtils.isNotBlank(_identityCardNumber)) {
				return false;
		}
		if (StringUtils.isNotBlank(_nip)) {
				return false;
		}
		if (StringUtils.isNotBlank(_username)) {
				return false;
		}
		if (StringUtils.isNotBlank(_email)) {
				return false;
		}
		if (StringUtils.isNotBlank(_email2)) {
				return false;
		}
		if (StringUtils.isNotBlank(_birthDate)) {
				return false;
		}
		if (StringUtils.isNotBlank(_invitationCode)) {
				return false;
		}
		if (StringUtils.isNotBlank(_educationEarned)) {
				return false;
		}
		if (StringUtils.isNotBlank(_fakeUniquePerson)) {
				return false;
		}

		return true;
	}
	
		public boolean equalPersons(PersonalDataForm other) {
		if (_birthDate == null) {
			if (other._birthDate != null) {
                return false;
            }
		} else if (!_birthDate.equals(other._birthDate)) {
            return false;
        }
		if (_birthDateDay == null) {
			if (other._birthDateDay != null) {
                return false;
            }
		} else if (!_birthDateDay.equals(other._birthDateDay)) {
            return false;
        }
		if (_birthDateMonth == null) {
			if (other._birthDateMonth != null) {
                return false;
            }
		} else if (!_birthDateMonth.equals(other._birthDateMonth)) {
            return false;
        }
		if (_birthDateYear == null) {
			if (other._birthDateYear != null) {
                return false;
            }
		} else if (!_birthDateYear.equals(other._birthDateYear)) {
            return false;
        }
		if (_firstName == null) {
			if (other._firstName != null) {
                return false;
            }
		} else if (!_firstName.equals(other._firstName)) {
            return false;
        }
		if (_lastName == null) {
			if (other._lastName != null) {
                return false;
            }
		} else if (!_lastName.equals(other._lastName)) {
            return false;
        }
		return true;
	}
	
// !!!!!!!! End of insert code section !!!!!!!!

    private String _customerNumber;
    private String _customerNumberInitVal;
    private boolean _customerNumberIsSet;
    private Boolean _isLender;
    private Boolean _isLenderInitVal;
    private boolean _isLenderIsSet;
    private Boolean _isBorrower;
    private Boolean _isBorrowerInitVal;
    private boolean _isBorrowerIsSet;
    private Boolean _isEdit;
    private Boolean _isEditInitVal;
    private boolean _isEditIsSet;
    private Boolean _isShow;
    private Boolean _isShowInitVal;
    private boolean _isShowIsSet;
    private String _salutation;
    private String _salutationInitVal;
    private boolean _salutationIsSet;
    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;
    private String _birthName;
    private String _birthNameInitVal;
    private boolean _birthNameIsSet;
    private String _firstName;
    private String _firstNameInitVal;
    private boolean _firstNameIsSet;
    private String _title;
    private String _titleInitVal;
    private boolean _titleIsSet;
    private String _citizenship;
    private String _citizenshipInitVal;
    private boolean _citizenshipIsSet;
    private String _countryOfBirth;
    private String _countryOfBirthInitVal;
    private boolean _countryOfBirthIsSet;
    private String _phone;
    private String _phoneInitVal;
    private boolean _phoneIsSet;
    private String _phoneCode;
    private String _phoneCodeInitVal;
    private boolean _phoneCodeIsSet;
    private String _phone2;
    private String _phone2InitVal;
    private boolean _phone2IsSet;
    private String _phone2Code;
    private String _phone2CodeInitVal;
    private boolean _phone2CodeIsSet;
    private String _birthDateDay;
    private String _birthDateDayInitVal;
    private boolean _birthDateDayIsSet;
    private String _birthDateMonth;
    private String _birthDateMonthInitVal;
    private boolean _birthDateMonthIsSet;
    private String _birthDateYear;
    private String _birthDateYearInitVal;
    private boolean _birthDateYearIsSet;
    private String _placeOfBirth;
    private String _placeOfBirthInitVal;
    private boolean _placeOfBirthIsSet;
    private String _familyStatus;
    private String _familyStatusInitVal;
    private boolean _familyStatusIsSet;
    private String _pesel;
    private String _peselInitVal;
    private boolean _peselIsSet;
    private String _identityCardNumber;
    private String _identityCardNumberInitVal;
    private boolean _identityCardNumberIsSet;
    private String _nip;
    private String _nipInitVal;
    private boolean _nipIsSet;
    private String _username;
    private String _usernameInitVal;
    private boolean _usernameIsSet;
    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private String _email2;
    private String _email2InitVal;
    private boolean _email2IsSet;
    private Boolean _schufa;
    private Boolean _schufaInitVal;
    private boolean _schufaIsSet;
    private Boolean _schufaNeeded;
    private Boolean _schufaNeededInitVal;
    private boolean _schufaNeededIsSet;
    private String _negativeSchufa;
    private String _negativeSchufaInitVal;
    private boolean _negativeSchufaIsSet;
    private Boolean _privacy;
    private Boolean _privacyInitVal;
    private boolean _privacyIsSet;
    private Date _privacyDate;
    private Date _privacyDateInitVal;
    private boolean _privacyDateIsSet;
    private boolean _privacyCheckNeeded;
    private boolean _privacyCheckNeededInitVal;
    private boolean _privacyCheckNeededIsSet;
    private String _birthDate;
    private String _birthDateInitVal;
    private boolean _birthDateIsSet;
    private Boolean _forOwnAccount;
    private Boolean _forOwnAccountInitVal;
    private boolean _forOwnAccountIsSet;
    private Boolean _termAndCondCheck;
    private Boolean _termAndCondCheckInitVal;
    private boolean _termAndCondCheckIsSet;
    private Boolean _termAndCondCheckNeeded;
    private Boolean _termAndCondCheckNeededInitVal;
    private boolean _termAndCondCheckNeededIsSet;
    private Boolean _termAndCondApplied;
    private Boolean _termAndCondAppliedInitVal;
    private boolean _termAndCondAppliedIsSet;
    private Boolean _invitationCodeRadio;
    private Boolean _invitationCodeRadioInitVal;
    private boolean _invitationCodeRadioIsSet;
    private String _invitationCode;
    private String _invitationCodeInitVal;
    private boolean _invitationCodeIsSet;
    private String _educationEarned;
    private String _educationEarnedInitVal;
    private boolean _educationEarnedIsSet;
    private Boolean _fidorTermAndCondCheck;
    private Boolean _fidorTermAndCondCheckInitVal;
    private boolean _fidorTermAndCondCheckIsSet;
    private Boolean _fidorTermAndCondCheckNeeded;
    private Boolean _fidorTermAndCondCheckNeededInitVal;
    private boolean _fidorTermAndCondCheckNeededIsSet;
    private Boolean _fidorTermAndCondCheckIgnore;
    private Boolean _fidorTermAndCondCheckIgnoreInitVal;
    private boolean _fidorTermAndCondCheckIgnoreIsSet;
    private Boolean _fidorServiceProviderRestriction;
    private Boolean _fidorServiceProviderRestrictionInitVal;
    private boolean _fidorServiceProviderRestrictionIsSet;
    private Boolean _fidorServiceProviderRestrictionNeeded;
    private Boolean _fidorServiceProviderRestrictionNeededInitVal;
    private boolean _fidorServiceProviderRestrictionNeededIsSet;
    private String _fakeUniquePerson;
    private String _fakeUniquePersonInitVal;
    private boolean _fakeUniquePersonIsSet;
    private String _lastRequestedAmount;
    private String _lastRequestedAmountInitVal;
    private boolean _lastRequestedAmountIsSet;
    private String _religion;
    private String _religionInitVal;
    private boolean _religionIsSet;
    private Boolean _areTaxesPaidInUsa;
    private Boolean _areTaxesPaidInUsaInitVal;
    private boolean _areTaxesPaidInUsaIsSet;
    private Boolean _isForeignAccountTaxComplianceAct;
    private Boolean _isForeignAccountTaxComplianceActInitVal;
    private boolean _isForeignAccountTaxComplianceActIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setCustomerNumber(String customerNumber) {
        if (!_customerNumberIsSet) {
            _customerNumberIsSet = true;
            _customerNumberInitVal = customerNumber;
        }
        _customerNumber = customerNumber;
    }

    public String getCustomerNumber() {
        return _customerNumber;
    }

    public String customerNumberInitVal() {
        return _customerNumberInitVal;
    }

	public boolean customerNumberIsDirty() {
        return !valuesAreEqual(_customerNumberInitVal, _customerNumber);
    }

    public boolean customerNumberIsSet() {
        return _customerNumberIsSet;
    }	


    public void setIsLender(Boolean isLender) {
        if (!_isLenderIsSet) {
            _isLenderIsSet = true;
            _isLenderInitVal = isLender;
        }
        _isLender = isLender;
    }

    public Boolean getIsLender() {
        return _isLender;
    }

    public Boolean isLenderInitVal() {
        return _isLenderInitVal;
    }

	public boolean isLenderIsDirty() {
        return !valuesAreEqual(_isLenderInitVal, _isLender);
    }

    public boolean isLenderIsSet() {
        return _isLenderIsSet;
    }	


    public void setIsBorrower(Boolean isBorrower) {
        if (!_isBorrowerIsSet) {
            _isBorrowerIsSet = true;
            _isBorrowerInitVal = isBorrower;
        }
        _isBorrower = isBorrower;
    }

    public Boolean getIsBorrower() {
        return _isBorrower;
    }

    public Boolean isBorrowerInitVal() {
        return _isBorrowerInitVal;
    }

	public boolean isBorrowerIsDirty() {
        return !valuesAreEqual(_isBorrowerInitVal, _isBorrower);
    }

    public boolean isBorrowerIsSet() {
        return _isBorrowerIsSet;
    }	


    public void setIsEdit(Boolean isEdit) {
        if (!_isEditIsSet) {
            _isEditIsSet = true;
            _isEditInitVal = isEdit;
        }
        _isEdit = isEdit;
    }

    public Boolean getIsEdit() {
        return _isEdit;
    }

    public Boolean isEditInitVal() {
        return _isEditInitVal;
    }

	public boolean isEditIsDirty() {
        return !valuesAreEqual(_isEditInitVal, _isEdit);
    }

    public boolean isEditIsSet() {
        return _isEditIsSet;
    }	


    public void setIsShow(Boolean isShow) {
        if (!_isShowIsSet) {
            _isShowIsSet = true;
            _isShowInitVal = isShow;
        }
        _isShow = isShow;
    }

    public Boolean getIsShow() {
        return _isShow;
    }

    public Boolean isShowInitVal() {
        return _isShowInitVal;
    }

	public boolean isShowIsDirty() {
        return !valuesAreEqual(_isShowInitVal, _isShow);
    }

    public boolean isShowIsSet() {
        return _isShowIsSet;
    }	


    public void setSalutation(String salutation) {
        if (!_salutationIsSet) {
            _salutationIsSet = true;
            _salutationInitVal = salutation;
        }
        _salutation = salutation;
    }

    public String getSalutation() {
        return _salutation;
    }

    public String salutationInitVal() {
        return _salutationInitVal;
    }

	public boolean salutationIsDirty() {
        return !valuesAreEqual(_salutationInitVal, _salutation);
    }

    public boolean salutationIsSet() {
        return _salutationIsSet;
    }	


    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

	public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }	


    public void setBirthName(String birthName) {
        if (!_birthNameIsSet) {
            _birthNameIsSet = true;
            _birthNameInitVal = birthName;
        }
        _birthName = birthName;
    }

    public String getBirthName() {
        return _birthName;
    }

    public String birthNameInitVal() {
        return _birthNameInitVal;
    }

	public boolean birthNameIsDirty() {
        return !valuesAreEqual(_birthNameInitVal, _birthName);
    }

    public boolean birthNameIsSet() {
        return _birthNameIsSet;
    }	


    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = firstName;
        }
        _firstName = firstName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String firstNameInitVal() {
        return _firstNameInitVal;
    }

	public boolean firstNameIsDirty() {
        return !valuesAreEqual(_firstNameInitVal, _firstName);
    }

    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }	


    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = title;
        }
        _title = title;
    }

    public String getTitle() {
        return _title;
    }

    public String titleInitVal() {
        return _titleInitVal;
    }

	public boolean titleIsDirty() {
        return !valuesAreEqual(_titleInitVal, _title);
    }

    public boolean titleIsSet() {
        return _titleIsSet;
    }	


    public void setCitizenship(String citizenship) {
        if (!_citizenshipIsSet) {
            _citizenshipIsSet = true;
            _citizenshipInitVal = citizenship;
        }
        _citizenship = citizenship;
    }

    public String getCitizenship() {
        return _citizenship;
    }

    public String citizenshipInitVal() {
        return _citizenshipInitVal;
    }

	public boolean citizenshipIsDirty() {
        return !valuesAreEqual(_citizenshipInitVal, _citizenship);
    }

    public boolean citizenshipIsSet() {
        return _citizenshipIsSet;
    }	


    public void setCountryOfBirth(String countryOfBirth) {
        if (!_countryOfBirthIsSet) {
            _countryOfBirthIsSet = true;
            _countryOfBirthInitVal = countryOfBirth;
        }
        _countryOfBirth = countryOfBirth;
    }

    public String getCountryOfBirth() {
        return _countryOfBirth;
    }

    public String countryOfBirthInitVal() {
        return _countryOfBirthInitVal;
    }

	public boolean countryOfBirthIsDirty() {
        return !valuesAreEqual(_countryOfBirthInitVal, _countryOfBirth);
    }

    public boolean countryOfBirthIsSet() {
        return _countryOfBirthIsSet;
    }	


    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = phone;
        }
        _phone = phone;
    }

    public String getPhone() {
        return _phone;
    }

    public String phoneInitVal() {
        return _phoneInitVal;
    }

	public boolean phoneIsDirty() {
        return !valuesAreEqual(_phoneInitVal, _phone);
    }

    public boolean phoneIsSet() {
        return _phoneIsSet;
    }	


    public void setPhoneCode(String phoneCode) {
        if (!_phoneCodeIsSet) {
            _phoneCodeIsSet = true;
            _phoneCodeInitVal = phoneCode;
        }
        _phoneCode = phoneCode;
    }

    public String getPhoneCode() {
        return _phoneCode;
    }

    public String phoneCodeInitVal() {
        return _phoneCodeInitVal;
    }

	public boolean phoneCodeIsDirty() {
        return !valuesAreEqual(_phoneCodeInitVal, _phoneCode);
    }

    public boolean phoneCodeIsSet() {
        return _phoneCodeIsSet;
    }	


    public void setPhone2(String phone2) {
        if (!_phone2IsSet) {
            _phone2IsSet = true;
            _phone2InitVal = phone2;
        }
        _phone2 = phone2;
    }

    public String getPhone2() {
        return _phone2;
    }

    public String phone2InitVal() {
        return _phone2InitVal;
    }

	public boolean phone2IsDirty() {
        return !valuesAreEqual(_phone2InitVal, _phone2);
    }

    public boolean phone2IsSet() {
        return _phone2IsSet;
    }	


    public void setPhone2Code(String phone2Code) {
        if (!_phone2CodeIsSet) {
            _phone2CodeIsSet = true;
            _phone2CodeInitVal = phone2Code;
        }
        _phone2Code = phone2Code;
    }

    public String getPhone2Code() {
        return _phone2Code;
    }

    public String phone2CodeInitVal() {
        return _phone2CodeInitVal;
    }

	public boolean phone2CodeIsDirty() {
        return !valuesAreEqual(_phone2CodeInitVal, _phone2Code);
    }

    public boolean phone2CodeIsSet() {
        return _phone2CodeIsSet;
    }	


    public void setBirthDateDay(String birthDateDay) {
        if (!_birthDateDayIsSet) {
            _birthDateDayIsSet = true;
            _birthDateDayInitVal = birthDateDay;
        }
        _birthDateDay = birthDateDay;
    }

    public String getBirthDateDay() {
        return _birthDateDay;
    }

    public String birthDateDayInitVal() {
        return _birthDateDayInitVal;
    }

	public boolean birthDateDayIsDirty() {
        return !valuesAreEqual(_birthDateDayInitVal, _birthDateDay);
    }

    public boolean birthDateDayIsSet() {
        return _birthDateDayIsSet;
    }	


    public void setBirthDateMonth(String birthDateMonth) {
        if (!_birthDateMonthIsSet) {
            _birthDateMonthIsSet = true;
            _birthDateMonthInitVal = birthDateMonth;
        }
        _birthDateMonth = birthDateMonth;
    }

    public String getBirthDateMonth() {
        return _birthDateMonth;
    }

    public String birthDateMonthInitVal() {
        return _birthDateMonthInitVal;
    }

	public boolean birthDateMonthIsDirty() {
        return !valuesAreEqual(_birthDateMonthInitVal, _birthDateMonth);
    }

    public boolean birthDateMonthIsSet() {
        return _birthDateMonthIsSet;
    }	


    public void setBirthDateYear(String birthDateYear) {
        if (!_birthDateYearIsSet) {
            _birthDateYearIsSet = true;
            _birthDateYearInitVal = birthDateYear;
        }
        _birthDateYear = birthDateYear;
    }

    public String getBirthDateYear() {
        return _birthDateYear;
    }

    public String birthDateYearInitVal() {
        return _birthDateYearInitVal;
    }

	public boolean birthDateYearIsDirty() {
        return !valuesAreEqual(_birthDateYearInitVal, _birthDateYear);
    }

    public boolean birthDateYearIsSet() {
        return _birthDateYearIsSet;
    }	


    public void setPlaceOfBirth(String placeOfBirth) {
        if (!_placeOfBirthIsSet) {
            _placeOfBirthIsSet = true;
            _placeOfBirthInitVal = placeOfBirth;
        }
        _placeOfBirth = placeOfBirth;
    }

    public String getPlaceOfBirth() {
        return _placeOfBirth;
    }

    public String placeOfBirthInitVal() {
        return _placeOfBirthInitVal;
    }

	public boolean placeOfBirthIsDirty() {
        return !valuesAreEqual(_placeOfBirthInitVal, _placeOfBirth);
    }

    public boolean placeOfBirthIsSet() {
        return _placeOfBirthIsSet;
    }	


    public void setFamilyStatus(String familyStatus) {
        if (!_familyStatusIsSet) {
            _familyStatusIsSet = true;
            _familyStatusInitVal = familyStatus;
        }
        _familyStatus = familyStatus;
    }

    public String getFamilyStatus() {
        return _familyStatus;
    }

    public String familyStatusInitVal() {
        return _familyStatusInitVal;
    }

	public boolean familyStatusIsDirty() {
        return !valuesAreEqual(_familyStatusInitVal, _familyStatus);
    }

    public boolean familyStatusIsSet() {
        return _familyStatusIsSet;
    }	


    public void setPesel(String pesel) {
        if (!_peselIsSet) {
            _peselIsSet = true;
            _peselInitVal = pesel;
        }
        _pesel = pesel;
    }

    public String getPesel() {
        return _pesel;
    }

    public String peselInitVal() {
        return _peselInitVal;
    }

	public boolean peselIsDirty() {
        return !valuesAreEqual(_peselInitVal, _pesel);
    }

    public boolean peselIsSet() {
        return _peselIsSet;
    }	


    public void setIdentityCardNumber(String identityCardNumber) {
        if (!_identityCardNumberIsSet) {
            _identityCardNumberIsSet = true;
            _identityCardNumberInitVal = identityCardNumber;
        }
        _identityCardNumber = identityCardNumber;
    }

    public String getIdentityCardNumber() {
        return _identityCardNumber;
    }

    public String identityCardNumberInitVal() {
        return _identityCardNumberInitVal;
    }

	public boolean identityCardNumberIsDirty() {
        return !valuesAreEqual(_identityCardNumberInitVal, _identityCardNumber);
    }

    public boolean identityCardNumberIsSet() {
        return _identityCardNumberIsSet;
    }	


    public void setNip(String nip) {
        if (!_nipIsSet) {
            _nipIsSet = true;
            _nipInitVal = nip;
        }
        _nip = nip;
    }

    public String getNip() {
        return _nip;
    }

    public String nipInitVal() {
        return _nipInitVal;
    }

	public boolean nipIsDirty() {
        return !valuesAreEqual(_nipInitVal, _nip);
    }

    public boolean nipIsSet() {
        return _nipIsSet;
    }	


    public void setUsername(String username) {
        if (!_usernameIsSet) {
            _usernameIsSet = true;
            _usernameInitVal = username;
        }
        _username = username;
    }

    public String getUsername() {
        return _username;
    }

    public String usernameInitVal() {
        return _usernameInitVal;
    }

	public boolean usernameIsDirty() {
        return !valuesAreEqual(_usernameInitVal, _username);
    }

    public boolean usernameIsSet() {
        return _usernameIsSet;
    }	


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setEmail2(String email2) {
        if (!_email2IsSet) {
            _email2IsSet = true;
            _email2InitVal = email2;
        }
        _email2 = email2;
    }

    public String getEmail2() {
        return _email2;
    }

    public String email2InitVal() {
        return _email2InitVal;
    }

	public boolean email2IsDirty() {
        return !valuesAreEqual(_email2InitVal, _email2);
    }

    public boolean email2IsSet() {
        return _email2IsSet;
    }	


    public void setSchufa(Boolean schufa) {
        if (!_schufaIsSet) {
            _schufaIsSet = true;
            _schufaInitVal = schufa;
        }
        _schufa = schufa;
    }

    public Boolean getSchufa() {
        return _schufa;
    }

    public Boolean schufaInitVal() {
        return _schufaInitVal;
    }

	public boolean schufaIsDirty() {
        return !valuesAreEqual(_schufaInitVal, _schufa);
    }

    public boolean schufaIsSet() {
        return _schufaIsSet;
    }	


    public void setSchufaNeeded(Boolean schufaNeeded) {
        if (!_schufaNeededIsSet) {
            _schufaNeededIsSet = true;
            _schufaNeededInitVal = schufaNeeded;
        }
        _schufaNeeded = schufaNeeded;
    }

    public Boolean getSchufaNeeded() {
        return _schufaNeeded;
    }

    public Boolean schufaNeededInitVal() {
        return _schufaNeededInitVal;
    }

	public boolean schufaNeededIsDirty() {
        return !valuesAreEqual(_schufaNeededInitVal, _schufaNeeded);
    }

    public boolean schufaNeededIsSet() {
        return _schufaNeededIsSet;
    }	


    public void setNegativeSchufa(String negativeSchufa) {
        if (!_negativeSchufaIsSet) {
            _negativeSchufaIsSet = true;
            _negativeSchufaInitVal = negativeSchufa;
        }
        _negativeSchufa = negativeSchufa;
    }

    public String getNegativeSchufa() {
        return _negativeSchufa;
    }

    public String negativeSchufaInitVal() {
        return _negativeSchufaInitVal;
    }

	public boolean negativeSchufaIsDirty() {
        return !valuesAreEqual(_negativeSchufaInitVal, _negativeSchufa);
    }

    public boolean negativeSchufaIsSet() {
        return _negativeSchufaIsSet;
    }	


    public void setPrivacy(Boolean privacy) {
        if (!_privacyIsSet) {
            _privacyIsSet = true;
            _privacyInitVal = privacy;
        }
        _privacy = privacy;
    }

    public Boolean getPrivacy() {
        return _privacy;
    }

    public Boolean privacyInitVal() {
        return _privacyInitVal;
    }

	public boolean privacyIsDirty() {
        return !valuesAreEqual(_privacyInitVal, _privacy);
    }

    public boolean privacyIsSet() {
        return _privacyIsSet;
    }	


    public void setPrivacyDate(Date privacyDate) {
        if (!_privacyDateIsSet) {
            _privacyDateIsSet = true;

            if (privacyDate == null) {
                _privacyDateInitVal = null;
            } else {
                _privacyDateInitVal = (Date) privacyDate.clone();
            }
        }

        if (privacyDate == null) {
            _privacyDate = null;
        } else {
            _privacyDate = (Date) privacyDate.clone();
        }
    }

    public Date getPrivacyDate() {
        Date privacyDate = null;
        if (_privacyDate != null) {
            privacyDate = (Date) _privacyDate.clone();
        }
        return privacyDate;
    }

    public Date privacyDateInitVal() {
        Date privacyDateInitVal = null;
        if (_privacyDateInitVal != null) {
            privacyDateInitVal = (Date) _privacyDateInitVal.clone();
        }
        return privacyDateInitVal;
    }

	public boolean privacyDateIsDirty() {
        return !valuesAreEqual(_privacyDateInitVal, _privacyDate);
    }

    public boolean privacyDateIsSet() {
        return _privacyDateIsSet;
    }	


    public void setPrivacyCheckNeeded(boolean privacyCheckNeeded) {
        if (!_privacyCheckNeededIsSet) {
            _privacyCheckNeededIsSet = true;
            _privacyCheckNeededInitVal = privacyCheckNeeded;
        }
        _privacyCheckNeeded = privacyCheckNeeded;
    }

    public boolean getPrivacyCheckNeeded() {
        return _privacyCheckNeeded;
    }

    public boolean privacyCheckNeededInitVal() {
        return _privacyCheckNeededInitVal;
    }

	public boolean privacyCheckNeededIsDirty() {
        return !valuesAreEqual(_privacyCheckNeededInitVal, _privacyCheckNeeded);
    }

    public boolean privacyCheckNeededIsSet() {
        return _privacyCheckNeededIsSet;
    }	


    public void setBirthDate(String birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;
            _birthDateInitVal = birthDate;
        }
        _birthDate = birthDate;
    }

    public String getBirthDate() {
        return _birthDate;
    }

    public String birthDateInitVal() {
        return _birthDateInitVal;
    }

	public boolean birthDateIsDirty() {
        return !valuesAreEqual(_birthDateInitVal, _birthDate);
    }

    public boolean birthDateIsSet() {
        return _birthDateIsSet;
    }	


    public void setForOwnAccount(Boolean forOwnAccount) {
        if (!_forOwnAccountIsSet) {
            _forOwnAccountIsSet = true;
            _forOwnAccountInitVal = forOwnAccount;
        }
        _forOwnAccount = forOwnAccount;
    }

    public Boolean getForOwnAccount() {
        return _forOwnAccount;
    }

    public Boolean forOwnAccountInitVal() {
        return _forOwnAccountInitVal;
    }

	public boolean forOwnAccountIsDirty() {
        return !valuesAreEqual(_forOwnAccountInitVal, _forOwnAccount);
    }

    public boolean forOwnAccountIsSet() {
        return _forOwnAccountIsSet;
    }	


    public void setTermAndCondCheck(Boolean termAndCondCheck) {
        if (!_termAndCondCheckIsSet) {
            _termAndCondCheckIsSet = true;
            _termAndCondCheckInitVal = termAndCondCheck;
        }
        _termAndCondCheck = termAndCondCheck;
    }

    public Boolean getTermAndCondCheck() {
        return _termAndCondCheck;
    }

    public Boolean termAndCondCheckInitVal() {
        return _termAndCondCheckInitVal;
    }

	public boolean termAndCondCheckIsDirty() {
        return !valuesAreEqual(_termAndCondCheckInitVal, _termAndCondCheck);
    }

    public boolean termAndCondCheckIsSet() {
        return _termAndCondCheckIsSet;
    }	


    public void setTermAndCondCheckNeeded(Boolean termAndCondCheckNeeded) {
        if (!_termAndCondCheckNeededIsSet) {
            _termAndCondCheckNeededIsSet = true;
            _termAndCondCheckNeededInitVal = termAndCondCheckNeeded;
        }
        _termAndCondCheckNeeded = termAndCondCheckNeeded;
    }

    public Boolean getTermAndCondCheckNeeded() {
        return _termAndCondCheckNeeded;
    }

    public Boolean termAndCondCheckNeededInitVal() {
        return _termAndCondCheckNeededInitVal;
    }

	public boolean termAndCondCheckNeededIsDirty() {
        return !valuesAreEqual(_termAndCondCheckNeededInitVal, _termAndCondCheckNeeded);
    }

    public boolean termAndCondCheckNeededIsSet() {
        return _termAndCondCheckNeededIsSet;
    }	


    public void setTermAndCondApplied(Boolean termAndCondApplied) {
        if (!_termAndCondAppliedIsSet) {
            _termAndCondAppliedIsSet = true;
            _termAndCondAppliedInitVal = termAndCondApplied;
        }
        _termAndCondApplied = termAndCondApplied;
    }

    public Boolean getTermAndCondApplied() {
        return _termAndCondApplied;
    }

    public Boolean termAndCondAppliedInitVal() {
        return _termAndCondAppliedInitVal;
    }

	public boolean termAndCondAppliedIsDirty() {
        return !valuesAreEqual(_termAndCondAppliedInitVal, _termAndCondApplied);
    }

    public boolean termAndCondAppliedIsSet() {
        return _termAndCondAppliedIsSet;
    }	


    public void setInvitationCodeRadio(Boolean invitationCodeRadio) {
        if (!_invitationCodeRadioIsSet) {
            _invitationCodeRadioIsSet = true;
            _invitationCodeRadioInitVal = invitationCodeRadio;
        }
        _invitationCodeRadio = invitationCodeRadio;
    }

    public Boolean getInvitationCodeRadio() {
        return _invitationCodeRadio;
    }

    public Boolean invitationCodeRadioInitVal() {
        return _invitationCodeRadioInitVal;
    }

	public boolean invitationCodeRadioIsDirty() {
        return !valuesAreEqual(_invitationCodeRadioInitVal, _invitationCodeRadio);
    }

    public boolean invitationCodeRadioIsSet() {
        return _invitationCodeRadioIsSet;
    }	


    public void setInvitationCode(String invitationCode) {
        if (!_invitationCodeIsSet) {
            _invitationCodeIsSet = true;
            _invitationCodeInitVal = invitationCode;
        }
        _invitationCode = invitationCode;
    }

    public String getInvitationCode() {
        return _invitationCode;
    }

    public String invitationCodeInitVal() {
        return _invitationCodeInitVal;
    }

	public boolean invitationCodeIsDirty() {
        return !valuesAreEqual(_invitationCodeInitVal, _invitationCode);
    }

    public boolean invitationCodeIsSet() {
        return _invitationCodeIsSet;
    }	


    public void setEducationEarned(String educationEarned) {
        if (!_educationEarnedIsSet) {
            _educationEarnedIsSet = true;
            _educationEarnedInitVal = educationEarned;
        }
        _educationEarned = educationEarned;
    }

    public String getEducationEarned() {
        return _educationEarned;
    }

    public String educationEarnedInitVal() {
        return _educationEarnedInitVal;
    }

	public boolean educationEarnedIsDirty() {
        return !valuesAreEqual(_educationEarnedInitVal, _educationEarned);
    }

    public boolean educationEarnedIsSet() {
        return _educationEarnedIsSet;
    }	


    public void setFidorTermAndCondCheck(Boolean fidorTermAndCondCheck) {
        if (!_fidorTermAndCondCheckIsSet) {
            _fidorTermAndCondCheckIsSet = true;
            _fidorTermAndCondCheckInitVal = fidorTermAndCondCheck;
        }
        _fidorTermAndCondCheck = fidorTermAndCondCheck;
    }

    public Boolean getFidorTermAndCondCheck() {
        return _fidorTermAndCondCheck;
    }

    public Boolean fidorTermAndCondCheckInitVal() {
        return _fidorTermAndCondCheckInitVal;
    }

	public boolean fidorTermAndCondCheckIsDirty() {
        return !valuesAreEqual(_fidorTermAndCondCheckInitVal, _fidorTermAndCondCheck);
    }

    public boolean fidorTermAndCondCheckIsSet() {
        return _fidorTermAndCondCheckIsSet;
    }	


    public void setFidorTermAndCondCheckNeeded(Boolean fidorTermAndCondCheckNeeded) {
        if (!_fidorTermAndCondCheckNeededIsSet) {
            _fidorTermAndCondCheckNeededIsSet = true;
            _fidorTermAndCondCheckNeededInitVal = fidorTermAndCondCheckNeeded;
        }
        _fidorTermAndCondCheckNeeded = fidorTermAndCondCheckNeeded;
    }

    public Boolean getFidorTermAndCondCheckNeeded() {
        return _fidorTermAndCondCheckNeeded;
    }

    public Boolean fidorTermAndCondCheckNeededInitVal() {
        return _fidorTermAndCondCheckNeededInitVal;
    }

	public boolean fidorTermAndCondCheckNeededIsDirty() {
        return !valuesAreEqual(_fidorTermAndCondCheckNeededInitVal, _fidorTermAndCondCheckNeeded);
    }

    public boolean fidorTermAndCondCheckNeededIsSet() {
        return _fidorTermAndCondCheckNeededIsSet;
    }	


    public void setFidorTermAndCondCheckIgnore(Boolean fidorTermAndCondCheckIgnore) {
        if (!_fidorTermAndCondCheckIgnoreIsSet) {
            _fidorTermAndCondCheckIgnoreIsSet = true;
            _fidorTermAndCondCheckIgnoreInitVal = fidorTermAndCondCheckIgnore;
        }
        _fidorTermAndCondCheckIgnore = fidorTermAndCondCheckIgnore;
    }

    public Boolean getFidorTermAndCondCheckIgnore() {
        return _fidorTermAndCondCheckIgnore;
    }

    public Boolean fidorTermAndCondCheckIgnoreInitVal() {
        return _fidorTermAndCondCheckIgnoreInitVal;
    }

	public boolean fidorTermAndCondCheckIgnoreIsDirty() {
        return !valuesAreEqual(_fidorTermAndCondCheckIgnoreInitVal, _fidorTermAndCondCheckIgnore);
    }

    public boolean fidorTermAndCondCheckIgnoreIsSet() {
        return _fidorTermAndCondCheckIgnoreIsSet;
    }	


    public void setFidorServiceProviderRestriction(Boolean fidorServiceProviderRestriction) {
        if (!_fidorServiceProviderRestrictionIsSet) {
            _fidorServiceProviderRestrictionIsSet = true;
            _fidorServiceProviderRestrictionInitVal = fidorServiceProviderRestriction;
        }
        _fidorServiceProviderRestriction = fidorServiceProviderRestriction;
    }

    public Boolean getFidorServiceProviderRestriction() {
        return _fidorServiceProviderRestriction;
    }

    public Boolean fidorServiceProviderRestrictionInitVal() {
        return _fidorServiceProviderRestrictionInitVal;
    }

	public boolean fidorServiceProviderRestrictionIsDirty() {
        return !valuesAreEqual(_fidorServiceProviderRestrictionInitVal, _fidorServiceProviderRestriction);
    }

    public boolean fidorServiceProviderRestrictionIsSet() {
        return _fidorServiceProviderRestrictionIsSet;
    }	


    public void setFidorServiceProviderRestrictionNeeded(Boolean fidorServiceProviderRestrictionNeeded) {
        if (!_fidorServiceProviderRestrictionNeededIsSet) {
            _fidorServiceProviderRestrictionNeededIsSet = true;
            _fidorServiceProviderRestrictionNeededInitVal = fidorServiceProviderRestrictionNeeded;
        }
        _fidorServiceProviderRestrictionNeeded = fidorServiceProviderRestrictionNeeded;
    }

    public Boolean getFidorServiceProviderRestrictionNeeded() {
        return _fidorServiceProviderRestrictionNeeded;
    }

    public Boolean fidorServiceProviderRestrictionNeededInitVal() {
        return _fidorServiceProviderRestrictionNeededInitVal;
    }

	public boolean fidorServiceProviderRestrictionNeededIsDirty() {
        return !valuesAreEqual(_fidorServiceProviderRestrictionNeededInitVal, _fidorServiceProviderRestrictionNeeded);
    }

    public boolean fidorServiceProviderRestrictionNeededIsSet() {
        return _fidorServiceProviderRestrictionNeededIsSet;
    }	


    public void setFakeUniquePerson(String fakeUniquePerson) {
        if (!_fakeUniquePersonIsSet) {
            _fakeUniquePersonIsSet = true;
            _fakeUniquePersonInitVal = fakeUniquePerson;
        }
        _fakeUniquePerson = fakeUniquePerson;
    }

    public String getFakeUniquePerson() {
        return _fakeUniquePerson;
    }

    public String fakeUniquePersonInitVal() {
        return _fakeUniquePersonInitVal;
    }

	public boolean fakeUniquePersonIsDirty() {
        return !valuesAreEqual(_fakeUniquePersonInitVal, _fakeUniquePerson);
    }

    public boolean fakeUniquePersonIsSet() {
        return _fakeUniquePersonIsSet;
    }	


    public void setLastRequestedAmount(String lastRequestedAmount) {
        if (!_lastRequestedAmountIsSet) {
            _lastRequestedAmountIsSet = true;
            _lastRequestedAmountInitVal = lastRequestedAmount;
        }
        _lastRequestedAmount = lastRequestedAmount;
    }

    public String getLastRequestedAmount() {
        return _lastRequestedAmount;
    }

    public String lastRequestedAmountInitVal() {
        return _lastRequestedAmountInitVal;
    }

	public boolean lastRequestedAmountIsDirty() {
        return !valuesAreEqual(_lastRequestedAmountInitVal, _lastRequestedAmount);
    }

    public boolean lastRequestedAmountIsSet() {
        return _lastRequestedAmountIsSet;
    }	


    public void setReligion(String religion) {
        if (!_religionIsSet) {
            _religionIsSet = true;
            _religionInitVal = religion;
        }
        _religion = religion;
    }

    public String getReligion() {
        return _religion;
    }

    public String religionInitVal() {
        return _religionInitVal;
    }

	public boolean religionIsDirty() {
        return !valuesAreEqual(_religionInitVal, _religion);
    }

    public boolean religionIsSet() {
        return _religionIsSet;
    }	


    public void setAreTaxesPaidInUsa(Boolean areTaxesPaidInUsa) {
        if (!_areTaxesPaidInUsaIsSet) {
            _areTaxesPaidInUsaIsSet = true;
            _areTaxesPaidInUsaInitVal = areTaxesPaidInUsa;
        }
        _areTaxesPaidInUsa = areTaxesPaidInUsa;
    }

    public Boolean getAreTaxesPaidInUsa() {
        return _areTaxesPaidInUsa;
    }

    public Boolean areTaxesPaidInUsaInitVal() {
        return _areTaxesPaidInUsaInitVal;
    }

	public boolean areTaxesPaidInUsaIsDirty() {
        return !valuesAreEqual(_areTaxesPaidInUsaInitVal, _areTaxesPaidInUsa);
    }

    public boolean areTaxesPaidInUsaIsSet() {
        return _areTaxesPaidInUsaIsSet;
    }	


    public void setIsForeignAccountTaxComplianceAct(Boolean isForeignAccountTaxComplianceAct) {
        if (!_isForeignAccountTaxComplianceActIsSet) {
            _isForeignAccountTaxComplianceActIsSet = true;
            _isForeignAccountTaxComplianceActInitVal = isForeignAccountTaxComplianceAct;
        }
        _isForeignAccountTaxComplianceAct = isForeignAccountTaxComplianceAct;
    }

    public Boolean getIsForeignAccountTaxComplianceAct() {
        return _isForeignAccountTaxComplianceAct;
    }

    public Boolean isForeignAccountTaxComplianceActInitVal() {
        return _isForeignAccountTaxComplianceActInitVal;
    }

	public boolean isForeignAccountTaxComplianceActIsDirty() {
        return !valuesAreEqual(_isForeignAccountTaxComplianceActInitVal, _isForeignAccountTaxComplianceAct);
    }

    public boolean isForeignAccountTaxComplianceActIsSet() {
        return _isForeignAccountTaxComplianceActIsSet;
    }	

}
