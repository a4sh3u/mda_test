//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.registration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(quick registration)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'QuickRegistrationForm'.
 *
 * @author generator
 */
public class QuickRegistrationForm implements Serializable {

    public static final String SALUTATION_EXPR = "quickRegistrationForm.salutation";
    public static final String SALUTATION_PATH = "command.quickRegistrationForm.salutation";
    public static final String USERNAME_EXPR = "quickRegistrationForm.username";
    public static final String USERNAME_PATH = "command.quickRegistrationForm.username";
    public static final String EMAIL_EXPR = "quickRegistrationForm.email";
    public static final String EMAIL_PATH = "command.quickRegistrationForm.email";
    public static final String EMAIL_RECAP_EXPR = "quickRegistrationForm.emailRecap";
    public static final String EMAIL_RECAP_PATH = "command.quickRegistrationForm.emailRecap";
    public static final String TERM_AND_COND_EXPR = "quickRegistrationForm.termAndCond";
    public static final String TERM_AND_COND_PATH = "command.quickRegistrationForm.termAndCond";
    public static final String PRIVACY_EXPR = "quickRegistrationForm.privacy";
    public static final String PRIVACY_PATH = "command.quickRegistrationForm.privacy";
    public static final String FIRST_NAME_EXPR = "quickRegistrationForm.firstName";
    public static final String FIRST_NAME_PATH = "command.quickRegistrationForm.firstName";
    public static final String LAST_NAME_EXPR = "quickRegistrationForm.lastName";
    public static final String LAST_NAME_PATH = "command.quickRegistrationForm.lastName";
    public static final String OLD_PASSWORD_EXPR = "quickRegistrationForm.oldPassword";
    public static final String OLD_PASSWORD_PATH = "command.quickRegistrationForm.oldPassword";
    public static final String OLD_PASSWORD_REPEAT_EXPR = "quickRegistrationForm.oldPasswordRepeat";
    public static final String OLD_PASSWORD_REPEAT_PATH = "command.quickRegistrationForm.oldPasswordRepeat";
    public static final String NEW_PASSWORD_EXPR = "quickRegistrationForm.newPassword";
    public static final String NEW_PASSWORD_PATH = "command.quickRegistrationForm.newPassword";
    public static final String NEW_PASSWORD_REPEAT_EXPR = "quickRegistrationForm.newPasswordRepeat";
    public static final String NEW_PASSWORD_REPEAT_PATH = "command.quickRegistrationForm.newPasswordRepeat";
    public static final String INVITATION_CODE_EXPR = "quickRegistrationForm.invitationCode";
    public static final String INVITATION_CODE_PATH = "command.quickRegistrationForm.invitationCode";
    public static final String PREFERRED_ROLE_EXPR = "quickRegistrationForm.preferredRole";
    public static final String PREFERRED_ROLE_PATH = "command.quickRegistrationForm.preferredRole";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(quick registration)}
    public boolean isDirty() {
        return usernameIsDirty() || emailIsDirty() || firstNameIsDirty() || lastNameIsDirty() || newPasswordIsDirty();
    }
    // to hide internal security role names from users that look into html source codes:
    public static final String PREFERRED_ROLE_LENDER = "lender";
    public static final String PREFERRED_ROLE_BORROWER = "borrower";
// !!!!!!!! End of insert code section !!!!!!!!


    private String _salutation;
    private String _salutationInitVal;
    private boolean _salutationIsSet;

    private String _username;
    private String _usernameInitVal;
    private boolean _usernameIsSet;

    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;

    private String _emailRecap;
    private String _emailRecapInitVal;
    private boolean _emailRecapIsSet;

    private Boolean _termAndCond;
    private Boolean _termAndCondInitVal;
    private boolean _termAndCondIsSet;

    private Boolean _privacy;
    private Boolean _privacyInitVal;
    private boolean _privacyIsSet;

    private String _firstName;
    private String _firstNameInitVal;
    private boolean _firstNameIsSet;

    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;

    private String _oldPassword;
    private String _oldPasswordInitVal;
    private boolean _oldPasswordIsSet;

    private String _oldPasswordRepeat;
    private String _oldPasswordRepeatInitVal;
    private boolean _oldPasswordRepeatIsSet;

    private String _newPassword;
    private String _newPasswordInitVal;
    private boolean _newPasswordIsSet;

    private String _newPasswordRepeat;
    private String _newPasswordRepeatInitVal;
    private boolean _newPasswordRepeatIsSet;

    private String _invitationCode;
    private String _invitationCodeInitVal;
    private boolean _invitationCodeIsSet;

    private String _preferredRole;
    private String _preferredRoleInitVal;
    private boolean _preferredRoleIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setSalutation(String salutation) {
        if (!_salutationIsSet) {
            _salutationIsSet = true;
            _salutationInitVal = salutation;
        }
        _salutation = salutation;
    }

    public String getSalutation() {
        return _salutation;
    }

    public String salutationInitVal() {
        return _salutationInitVal;
    }

    public boolean salutationIsDirty() {
        return !valuesAreEqual(_salutationInitVal, _salutation);
    }

    public boolean salutationIsSet() {
        return _salutationIsSet;
    }

    public void setUsername(String username) {
        if (!_usernameIsSet) {
            _usernameIsSet = true;
            _usernameInitVal = username;
        }
        _username = username;
    }

    public String getUsername() {
        return _username;
    }

    public String usernameInitVal() {
        return _usernameInitVal;
    }

    public boolean usernameIsDirty() {
        return !valuesAreEqual(_usernameInitVal, _username);
    }

    public boolean usernameIsSet() {
        return _usernameIsSet;
    }

    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

    public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }

    public void setEmailRecap(String emailRecap) {
        if (!_emailRecapIsSet) {
            _emailRecapIsSet = true;
            _emailRecapInitVal = emailRecap;
        }
        _emailRecap = emailRecap;
    }

    public String getEmailRecap() {
        return _emailRecap;
    }

    public String emailRecapInitVal() {
        return _emailRecapInitVal;
    }

    public boolean emailRecapIsDirty() {
        return !valuesAreEqual(_emailRecapInitVal, _emailRecap);
    }

    public boolean emailRecapIsSet() {
        return _emailRecapIsSet;
    }

    public void setTermAndCond(Boolean termAndCond) {
        if (!_termAndCondIsSet) {
            _termAndCondIsSet = true;
            _termAndCondInitVal = termAndCond;
        }
        _termAndCond = termAndCond;
    }

    public Boolean getTermAndCond() {
        return _termAndCond;
    }

    public Boolean termAndCondInitVal() {
        return _termAndCondInitVal;
    }

    public boolean termAndCondIsDirty() {
        return !valuesAreEqual(_termAndCondInitVal, _termAndCond);
    }

    public boolean termAndCondIsSet() {
        return _termAndCondIsSet;
    }

    public void setPrivacy(Boolean privacy) {
        if (!_privacyIsSet) {
            _privacyIsSet = true;
            _privacyInitVal = privacy;
        }
        _privacy = privacy;
    }

    public Boolean getPrivacy() {
        return _privacy;
    }

    public Boolean privacyInitVal() {
        return _privacyInitVal;
    }

    public boolean privacyIsDirty() {
        return !valuesAreEqual(_privacyInitVal, _privacy);
    }

    public boolean privacyIsSet() {
        return _privacyIsSet;
    }

    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = firstName;
        }
        _firstName = firstName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String firstNameInitVal() {
        return _firstNameInitVal;
    }

    public boolean firstNameIsDirty() {
        return !valuesAreEqual(_firstNameInitVal, _firstName);
    }

    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }

    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

    public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }

    public void setOldPassword(String oldPassword) {
        if (!_oldPasswordIsSet) {
            _oldPasswordIsSet = true;
            _oldPasswordInitVal = oldPassword;
        }
        _oldPassword = oldPassword;
    }

    public String getOldPassword() {
        return _oldPassword;
    }

    public String oldPasswordInitVal() {
        return _oldPasswordInitVal;
    }

    public boolean oldPasswordIsDirty() {
        return !valuesAreEqual(_oldPasswordInitVal, _oldPassword);
    }

    public boolean oldPasswordIsSet() {
        return _oldPasswordIsSet;
    }

    public void setOldPasswordRepeat(String oldPasswordRepeat) {
        if (!_oldPasswordRepeatIsSet) {
            _oldPasswordRepeatIsSet = true;
            _oldPasswordRepeatInitVal = oldPasswordRepeat;
        }
        _oldPasswordRepeat = oldPasswordRepeat;
    }

    public String getOldPasswordRepeat() {
        return _oldPasswordRepeat;
    }

    public String oldPasswordRepeatInitVal() {
        return _oldPasswordRepeatInitVal;
    }

    public boolean oldPasswordRepeatIsDirty() {
        return !valuesAreEqual(_oldPasswordRepeatInitVal, _oldPasswordRepeat);
    }

    public boolean oldPasswordRepeatIsSet() {
        return _oldPasswordRepeatIsSet;
    }

    public void setNewPassword(String newPassword) {
        if (!_newPasswordIsSet) {
            _newPasswordIsSet = true;
            _newPasswordInitVal = newPassword;
        }
        _newPassword = newPassword;
    }

    public String getNewPassword() {
        return _newPassword;
    }

    public String newPasswordInitVal() {
        return _newPasswordInitVal;
    }

    public boolean newPasswordIsDirty() {
        return !valuesAreEqual(_newPasswordInitVal, _newPassword);
    }

    public boolean newPasswordIsSet() {
        return _newPasswordIsSet;
    }

    public void setNewPasswordRepeat(String newPasswordRepeat) {
        if (!_newPasswordRepeatIsSet) {
            _newPasswordRepeatIsSet = true;
            _newPasswordRepeatInitVal = newPasswordRepeat;
        }
        _newPasswordRepeat = newPasswordRepeat;
    }

    public String getNewPasswordRepeat() {
        return _newPasswordRepeat;
    }

    public String newPasswordRepeatInitVal() {
        return _newPasswordRepeatInitVal;
    }

    public boolean newPasswordRepeatIsDirty() {
        return !valuesAreEqual(_newPasswordRepeatInitVal, _newPasswordRepeat);
    }

    public boolean newPasswordRepeatIsSet() {
        return _newPasswordRepeatIsSet;
    }

    public void setInvitationCode(String invitationCode) {
        if (!_invitationCodeIsSet) {
            _invitationCodeIsSet = true;
            _invitationCodeInitVal = invitationCode;
        }
        _invitationCode = invitationCode;
    }

    public String getInvitationCode() {
        return _invitationCode;
    }

    public String invitationCodeInitVal() {
        return _invitationCodeInitVal;
    }

    public boolean invitationCodeIsDirty() {
        return !valuesAreEqual(_invitationCodeInitVal, _invitationCode);
    }

    public boolean invitationCodeIsSet() {
        return _invitationCodeIsSet;
    }

    public void setPreferredRole(String preferredRole) {
        if (!_preferredRoleIsSet) {
            _preferredRoleIsSet = true;
            _preferredRoleInitVal = preferredRole;
        }
        _preferredRole = preferredRole;
    }

    public String getPreferredRole() {
        return _preferredRole;
    }

    public String preferredRoleInitVal() {
        return _preferredRoleInitVal;
    }

    public boolean preferredRoleIsDirty() {
        return !valuesAreEqual(_preferredRoleInitVal, _preferredRole);
    }

    public boolean preferredRoleIsSet() {
        return _preferredRoleIsSet;
    }

}
