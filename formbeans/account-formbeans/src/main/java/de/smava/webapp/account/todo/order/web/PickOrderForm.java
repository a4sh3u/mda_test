//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.order.web;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(pick order)}
import java.io.Serializable;


// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'PickOrderForm'.
 *
 * @author generator
 */
public class PickOrderForm implements Serializable {

    public static final String ORDER_ID_EXPR = "pickOrderForm.orderId";
    public static final String ORDER_ID_PATH = "command.pickOrderForm.orderId";
    public static final String LAST_UPDATE_EXPR = "pickOrderForm.lastUpdate";
    public static final String LAST_UPDATE_PATH = "command.pickOrderForm.lastUpdate";
    public static final String INTEREST_EXPR = "pickOrderForm.interest";
    public static final String INTEREST_PATH = "command.pickOrderForm.interest";
    public static final String BORROWER_EXPR = "pickOrderForm.borrower";
    public static final String BORROWER_PATH = "command.pickOrderForm.borrower";
    public static final String TITLE_EXPR = "pickOrderForm.title";
    public static final String TITLE_PATH = "command.pickOrderForm.title";
    public static final String ORIGINAL_AMOUNT_EXPR = "pickOrderForm.originalAmount";
    public static final String ORIGINAL_AMOUNT_PATH = "command.pickOrderForm.originalAmount";
    public static final String MARKET_EXPR = "pickOrderForm.market";
    public static final String MARKET_PATH = "command.pickOrderForm.market";
    public static final String EXPECTED_YIELD_EXPR = "pickOrderForm.expectedYield";
    public static final String EXPECTED_YIELD_PATH = "command.pickOrderForm.expectedYield";
    public static final String EXPECTED_RISK_EXPR = "pickOrderForm.expectedRisk";
    public static final String EXPECTED_RISK_PATH = "command.pickOrderForm.expectedRisk";
    public static final String OPEN_AMOUNT_EXPR = "pickOrderForm.openAmount";
    public static final String OPEN_AMOUNT_PATH = "command.pickOrderForm.openAmount";
    public static final String DURATION_EXPR = "pickOrderForm.duration";
    public static final String DURATION_PATH = "command.pickOrderForm.duration";
    public static final String AMOUNT_EXPR = "pickOrderForm.amount";
    public static final String AMOUNT_PATH = "command.pickOrderForm.amount";
    public static final String TRANSACTION_PIN_EXPR = "pickOrderForm.transactionPin";
    public static final String TRANSACTION_PIN_PATH = "command.pickOrderForm.transactionPin";
    public static final String TERMS_AND_CONDITIONS_EXPR = "pickOrderForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_PATH = "command.pickOrderForm.termsAndConditions";
    public static final String TERMS_AND_CONDITIONS_APPLIED_EXPR = "pickOrderForm.termsAndConditionsApplied";
    public static final String TERMS_AND_CONDITIONS_APPLIED_PATH = "command.pickOrderForm.termsAndConditionsApplied";
    public static final String OFFER_CONFIRMED_EXPR = "pickOrderForm.offerConfirmed";
    public static final String OFFER_CONFIRMED_PATH = "command.pickOrderForm.offerConfirmed";
    public static final String DEBIT_EXPR = "pickOrderForm.debit";
    public static final String DEBIT_PATH = "command.pickOrderForm.debit";
    public static final String AMOUNT_PLUS_CHARGE_EXPR = "pickOrderForm.amountPlusCharge";
    public static final String AMOUNT_PLUS_CHARGE_PATH = "command.pickOrderForm.amountPlusCharge";
    public static final String LENDER_CHARGE_EXPR = "pickOrderForm.lenderCharge";
    public static final String LENDER_CHARGE_PATH = "command.pickOrderForm.lenderCharge";
    public static final String LENDER_CHARGE_PERCENTAL_EXPR = "pickOrderForm.lenderChargePercental";
    public static final String LENDER_CHARGE_PERCENTAL_PATH = "command.pickOrderForm.lenderChargePercental";
    public static final String INDIVIDUAL_FACTORIZATION_CONTRACT_EXPR = "pickOrderForm.individualFactorizationContract";
    public static final String INDIVIDUAL_FACTORIZATION_CONTRACT_PATH = "command.pickOrderForm.individualFactorizationContract";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(pick order)}
    private static final long serialVersionUID = -3881573087956408473L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _orderId;
    private String _orderIdInitVal;
    private boolean _orderIdIsSet;
    private Long _lastUpdate;
    private Long _lastUpdateInitVal;
    private boolean _lastUpdateIsSet;
    private String _interest;
    private String _interestInitVal;
    private boolean _interestIsSet;
    private String _borrower;
    private String _borrowerInitVal;
    private boolean _borrowerIsSet;
    private String _title;
    private String _titleInitVal;
    private boolean _titleIsSet;
    private String _originalAmount;
    private String _originalAmountInitVal;
    private boolean _originalAmountIsSet;
    private String _market;
    private String _marketInitVal;
    private boolean _marketIsSet;
    private String _expectedYield;
    private String _expectedYieldInitVal;
    private boolean _expectedYieldIsSet;
    private String _expectedRisk;
    private String _expectedRiskInitVal;
    private boolean _expectedRiskIsSet;
    private String _openAmount;
    private String _openAmountInitVal;
    private boolean _openAmountIsSet;
    private String _duration;
    private String _durationInitVal;
    private boolean _durationIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _transactionPin;
    private String _transactionPinInitVal;
    private boolean _transactionPinIsSet;
    private Boolean _termsAndConditions;
    private Boolean _termsAndConditionsInitVal;
    private boolean _termsAndConditionsIsSet;
    private Boolean _termsAndConditionsApplied;
    private Boolean _termsAndConditionsAppliedInitVal;
    private boolean _termsAndConditionsAppliedIsSet;
    private Boolean _offerConfirmed;
    private Boolean _offerConfirmedInitVal;
    private boolean _offerConfirmedIsSet;
    private Boolean _debit;
    private Boolean _debitInitVal;
    private boolean _debitIsSet;
    private String _amountPlusCharge;
    private String _amountPlusChargeInitVal;
    private boolean _amountPlusChargeIsSet;
    private String _lenderCharge;
    private String _lenderChargeInitVal;
    private boolean _lenderChargeIsSet;
    private String _lenderChargePercental;
    private String _lenderChargePercentalInitVal;
    private boolean _lenderChargePercentalIsSet;
    private Boolean _individualFactorizationContract;
    private Boolean _individualFactorizationContractInitVal;
    private boolean _individualFactorizationContractIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setOrderId(String orderId) {
        if (!_orderIdIsSet) {
            _orderIdIsSet = true;
            _orderIdInitVal = orderId;
        }
        _orderId = orderId;
    }

    public String getOrderId() {
        return _orderId;
    }

    public String orderIdInitVal() {
        return _orderIdInitVal;
    }

	public boolean orderIdIsDirty() {
        return !valuesAreEqual(_orderIdInitVal, _orderId);
    }

    public boolean orderIdIsSet() {
        return _orderIdIsSet;
    }	


    public void setLastUpdate(Long lastUpdate) {
        if (!_lastUpdateIsSet) {
            _lastUpdateIsSet = true;
            _lastUpdateInitVal = lastUpdate;
        }
        _lastUpdate = lastUpdate;
    }

    public Long getLastUpdate() {
        return _lastUpdate;
    }

    public Long lastUpdateInitVal() {
        return _lastUpdateInitVal;
    }

	public boolean lastUpdateIsDirty() {
        return !valuesAreEqual(_lastUpdateInitVal, _lastUpdate);
    }

    public boolean lastUpdateIsSet() {
        return _lastUpdateIsSet;
    }	


    public void setInterest(String interest) {
        if (!_interestIsSet) {
            _interestIsSet = true;
            _interestInitVal = interest;
        }
        _interest = interest;
    }

    public String getInterest() {
        return _interest;
    }

    public String interestInitVal() {
        return _interestInitVal;
    }

	public boolean interestIsDirty() {
        return !valuesAreEqual(_interestInitVal, _interest);
    }

    public boolean interestIsSet() {
        return _interestIsSet;
    }	


    public void setBorrower(String borrower) {
        if (!_borrowerIsSet) {
            _borrowerIsSet = true;
            _borrowerInitVal = borrower;
        }
        _borrower = borrower;
    }

    public String getBorrower() {
        return _borrower;
    }

    public String borrowerInitVal() {
        return _borrowerInitVal;
    }

	public boolean borrowerIsDirty() {
        return !valuesAreEqual(_borrowerInitVal, _borrower);
    }

    public boolean borrowerIsSet() {
        return _borrowerIsSet;
    }	


    public void setTitle(String title) {
        if (!_titleIsSet) {
            _titleIsSet = true;
            _titleInitVal = title;
        }
        _title = title;
    }

    public String getTitle() {
        return _title;
    }

    public String titleInitVal() {
        return _titleInitVal;
    }

	public boolean titleIsDirty() {
        return !valuesAreEqual(_titleInitVal, _title);
    }

    public boolean titleIsSet() {
        return _titleIsSet;
    }	


    public void setOriginalAmount(String originalAmount) {
        if (!_originalAmountIsSet) {
            _originalAmountIsSet = true;
            _originalAmountInitVal = originalAmount;
        }
        _originalAmount = originalAmount;
    }

    public String getOriginalAmount() {
        return _originalAmount;
    }

    public String originalAmountInitVal() {
        return _originalAmountInitVal;
    }

	public boolean originalAmountIsDirty() {
        return !valuesAreEqual(_originalAmountInitVal, _originalAmount);
    }

    public boolean originalAmountIsSet() {
        return _originalAmountIsSet;
    }	


    public void setMarket(String market) {
        if (!_marketIsSet) {
            _marketIsSet = true;
            _marketInitVal = market;
        }
        _market = market;
    }

    public String getMarket() {
        return _market;
    }

    public String marketInitVal() {
        return _marketInitVal;
    }

	public boolean marketIsDirty() {
        return !valuesAreEqual(_marketInitVal, _market);
    }

    public boolean marketIsSet() {
        return _marketIsSet;
    }	


    public void setExpectedYield(String expectedYield) {
        if (!_expectedYieldIsSet) {
            _expectedYieldIsSet = true;
            _expectedYieldInitVal = expectedYield;
        }
        _expectedYield = expectedYield;
    }

    public String getExpectedYield() {
        return _expectedYield;
    }

    public String expectedYieldInitVal() {
        return _expectedYieldInitVal;
    }

	public boolean expectedYieldIsDirty() {
        return !valuesAreEqual(_expectedYieldInitVal, _expectedYield);
    }

    public boolean expectedYieldIsSet() {
        return _expectedYieldIsSet;
    }	


    public void setExpectedRisk(String expectedRisk) {
        if (!_expectedRiskIsSet) {
            _expectedRiskIsSet = true;
            _expectedRiskInitVal = expectedRisk;
        }
        _expectedRisk = expectedRisk;
    }

    public String getExpectedRisk() {
        return _expectedRisk;
    }

    public String expectedRiskInitVal() {
        return _expectedRiskInitVal;
    }

	public boolean expectedRiskIsDirty() {
        return !valuesAreEqual(_expectedRiskInitVal, _expectedRisk);
    }

    public boolean expectedRiskIsSet() {
        return _expectedRiskIsSet;
    }	


    public void setOpenAmount(String openAmount) {
        if (!_openAmountIsSet) {
            _openAmountIsSet = true;
            _openAmountInitVal = openAmount;
        }
        _openAmount = openAmount;
    }

    public String getOpenAmount() {
        return _openAmount;
    }

    public String openAmountInitVal() {
        return _openAmountInitVal;
    }

	public boolean openAmountIsDirty() {
        return !valuesAreEqual(_openAmountInitVal, _openAmount);
    }

    public boolean openAmountIsSet() {
        return _openAmountIsSet;
    }	


    public void setDuration(String duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = duration;
        }
        _duration = duration;
    }

    public String getDuration() {
        return _duration;
    }

    public String durationInitVal() {
        return _durationInitVal;
    }

	public boolean durationIsDirty() {
        return !valuesAreEqual(_durationInitVal, _duration);
    }

    public boolean durationIsSet() {
        return _durationIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setTransactionPin(String transactionPin) {
        if (!_transactionPinIsSet) {
            _transactionPinIsSet = true;
            _transactionPinInitVal = transactionPin;
        }
        _transactionPin = transactionPin;
    }

    public String getTransactionPin() {
        return _transactionPin;
    }

    public String transactionPinInitVal() {
        return _transactionPinInitVal;
    }

	public boolean transactionPinIsDirty() {
        return !valuesAreEqual(_transactionPinInitVal, _transactionPin);
    }

    public boolean transactionPinIsSet() {
        return _transactionPinIsSet;
    }	


    public void setTermsAndConditions(Boolean termsAndConditions) {
        if (!_termsAndConditionsIsSet) {
            _termsAndConditionsIsSet = true;
            _termsAndConditionsInitVal = termsAndConditions;
        }
        _termsAndConditions = termsAndConditions;
    }

    public Boolean getTermsAndConditions() {
        return _termsAndConditions;
    }

    public Boolean termsAndConditionsInitVal() {
        return _termsAndConditionsInitVal;
    }

	public boolean termsAndConditionsIsDirty() {
        return !valuesAreEqual(_termsAndConditionsInitVal, _termsAndConditions);
    }

    public boolean termsAndConditionsIsSet() {
        return _termsAndConditionsIsSet;
    }	


    public void setTermsAndConditionsApplied(Boolean termsAndConditionsApplied) {
        if (!_termsAndConditionsAppliedIsSet) {
            _termsAndConditionsAppliedIsSet = true;
            _termsAndConditionsAppliedInitVal = termsAndConditionsApplied;
        }
        _termsAndConditionsApplied = termsAndConditionsApplied;
    }

    public Boolean getTermsAndConditionsApplied() {
        return _termsAndConditionsApplied;
    }

    public Boolean termsAndConditionsAppliedInitVal() {
        return _termsAndConditionsAppliedInitVal;
    }

	public boolean termsAndConditionsAppliedIsDirty() {
        return !valuesAreEqual(_termsAndConditionsAppliedInitVal, _termsAndConditionsApplied);
    }

    public boolean termsAndConditionsAppliedIsSet() {
        return _termsAndConditionsAppliedIsSet;
    }	


    public void setOfferConfirmed(Boolean offerConfirmed) {
        if (!_offerConfirmedIsSet) {
            _offerConfirmedIsSet = true;
            _offerConfirmedInitVal = offerConfirmed;
        }
        _offerConfirmed = offerConfirmed;
    }

    public Boolean getOfferConfirmed() {
        return _offerConfirmed;
    }

    public Boolean offerConfirmedInitVal() {
        return _offerConfirmedInitVal;
    }

	public boolean offerConfirmedIsDirty() {
        return !valuesAreEqual(_offerConfirmedInitVal, _offerConfirmed);
    }

    public boolean offerConfirmedIsSet() {
        return _offerConfirmedIsSet;
    }	


    public void setDebit(Boolean debit) {
        if (!_debitIsSet) {
            _debitIsSet = true;
            _debitInitVal = debit;
        }
        _debit = debit;
    }

    public Boolean getDebit() {
        return _debit;
    }

    public Boolean debitInitVal() {
        return _debitInitVal;
    }

	public boolean debitIsDirty() {
        return !valuesAreEqual(_debitInitVal, _debit);
    }

    public boolean debitIsSet() {
        return _debitIsSet;
    }	


    public void setAmountPlusCharge(String amountPlusCharge) {
        if (!_amountPlusChargeIsSet) {
            _amountPlusChargeIsSet = true;
            _amountPlusChargeInitVal = amountPlusCharge;
        }
        _amountPlusCharge = amountPlusCharge;
    }

    public String getAmountPlusCharge() {
        return _amountPlusCharge;
    }

    public String amountPlusChargeInitVal() {
        return _amountPlusChargeInitVal;
    }

	public boolean amountPlusChargeIsDirty() {
        return !valuesAreEqual(_amountPlusChargeInitVal, _amountPlusCharge);
    }

    public boolean amountPlusChargeIsSet() {
        return _amountPlusChargeIsSet;
    }	


    public void setLenderCharge(String lenderCharge) {
        if (!_lenderChargeIsSet) {
            _lenderChargeIsSet = true;
            _lenderChargeInitVal = lenderCharge;
        }
        _lenderCharge = lenderCharge;
    }

    public String getLenderCharge() {
        return _lenderCharge;
    }

    public String lenderChargeInitVal() {
        return _lenderChargeInitVal;
    }

	public boolean lenderChargeIsDirty() {
        return !valuesAreEqual(_lenderChargeInitVal, _lenderCharge);
    }

    public boolean lenderChargeIsSet() {
        return _lenderChargeIsSet;
    }	


    public void setLenderChargePercental(String lenderChargePercental) {
        if (!_lenderChargePercentalIsSet) {
            _lenderChargePercentalIsSet = true;
            _lenderChargePercentalInitVal = lenderChargePercental;
        }
        _lenderChargePercental = lenderChargePercental;
    }

    public String getLenderChargePercental() {
        return _lenderChargePercental;
    }

    public String lenderChargePercentalInitVal() {
        return _lenderChargePercentalInitVal;
    }

	public boolean lenderChargePercentalIsDirty() {
        return !valuesAreEqual(_lenderChargePercentalInitVal, _lenderChargePercental);
    }

    public boolean lenderChargePercentalIsSet() {
        return _lenderChargePercentalIsSet;
    }	


    public void setIndividualFactorizationContract(Boolean individualFactorizationContract) {
        if (!_individualFactorizationContractIsSet) {
            _individualFactorizationContractIsSet = true;
            _individualFactorizationContractInitVal = individualFactorizationContract;
        }
        _individualFactorizationContract = individualFactorizationContract;
    }

    public Boolean getIndividualFactorizationContract() {
        return _individualFactorizationContract;
    }

    public Boolean individualFactorizationContractInitVal() {
        return _individualFactorizationContractInitVal;
    }

	public boolean individualFactorizationContractIsDirty() {
        return !valuesAreEqual(_individualFactorizationContractInitVal, _individualFactorizationContract);
    }

    public boolean individualFactorizationContractIsSet() {
        return _individualFactorizationContractIsSet;
    }	

}
