//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.transaction;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo edit transaction)}

import de.smava.webapp.account.domain.BankAccount;
import de.smava.webapp.account.domain.DtausFile;

import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoEditTransactionForm'.
 *
 * @author generator
 */
public class BoEditTransactionForm implements Serializable {

    public static final String TRANSACTION_NUMBER_EXPR = "boEditTransactionForm.transactionNumber";
    public static final String TRANSACTION_NUMBER_PATH = "command.boEditTransactionForm.transactionNumber";
    public static final String PAID_DATE_EXPR = "boEditTransactionForm.paidDate";
    public static final String PAID_DATE_PATH = "command.boEditTransactionForm.paidDate";
    public static final String DUE_DATE_EXPR = "boEditTransactionForm.dueDate";
    public static final String DUE_DATE_PATH = "command.boEditTransactionForm.dueDate";
    public static final String CREATION_DATE_EXPR = "boEditTransactionForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boEditTransactionForm.creationDate";
    public static final String MODIFIED_DATE_EXPR = "boEditTransactionForm.modifiedDate";
    public static final String MODIFIED_DATE_PATH = "command.boEditTransactionForm.modifiedDate";
    public static final String STATE_EXPR = "boEditTransactionForm.state";
    public static final String STATE_PATH = "command.boEditTransactionForm.state";
    public static final String CREDIT_BANK_ACCOUNT_EXPR = "boEditTransactionForm.creditBankAccount";
    public static final String CREDIT_BANK_ACCOUNT_PATH = "command.boEditTransactionForm.creditBankAccount";
    public static final String DEBIT_BANK_ACCOUNT_EXPR = "boEditTransactionForm.debitBankAccount";
    public static final String DEBIT_BANK_ACCOUNT_PATH = "command.boEditTransactionForm.debitBankAccount";
    public static final String CREATE_NEW_DEBIT_ACCOUNT_EXPR = "boEditTransactionForm.createNewDebitAccount";
    public static final String CREATE_NEW_DEBIT_ACCOUNT_PATH = "command.boEditTransactionForm.createNewDebitAccount";
    public static final String TYPE_EXPR = "boEditTransactionForm.type";
    public static final String TYPE_PATH = "command.boEditTransactionForm.type";
    public static final String AMOUNT_EXPR = "boEditTransactionForm.amount";
    public static final String AMOUNT_PATH = "command.boEditTransactionForm.amount";
    public static final String DESCRIPTION_EXPR = "boEditTransactionForm.description";
    public static final String DESCRIPTION_PATH = "command.boEditTransactionForm.description";
    public static final String DTAUS_FILE_EXPR = "boEditTransactionForm.dtausFile";
    public static final String DTAUS_FILE_PATH = "command.boEditTransactionForm.dtausFile";
    public static final String CREDIT_BANK_ACCOUNT_OBJECT_EXPR = "boEditTransactionForm.creditBankAccountObject";
    public static final String CREDIT_BANK_ACCOUNT_OBJECT_PATH = "command.boEditTransactionForm.creditBankAccountObject";
    public static final String DEBIT_BANK_ACCOUNT_OBJECT_EXPR = "boEditTransactionForm.debitBankAccountObject";
    public static final String DEBIT_BANK_ACCOUNT_OBJECT_PATH = "command.boEditTransactionForm.debitBankAccountObject";
    public static final String DTAUS_DEBIT_ACCOUNT_NAME_EXPR = "boEditTransactionForm.dtausDebitAccountName";
    public static final String DTAUS_DEBIT_ACCOUNT_NAME_PATH = "command.boEditTransactionForm.dtausDebitAccountName";
    public static final String DTAUS_DEBIT_ACCOUNT_NUMBER_EXPR = "boEditTransactionForm.dtausDebitAccountNumber";
    public static final String DTAUS_DEBIT_ACCOUNT_NUMBER_PATH = "command.boEditTransactionForm.dtausDebitAccountNumber";
    public static final String DTAUS_DEBIT_BANK_NAME_EXPR = "boEditTransactionForm.dtausDebitBankName";
    public static final String DTAUS_DEBIT_BANK_NAME_PATH = "command.boEditTransactionForm.dtausDebitBankName";
    public static final String DTAUS_DEBIT_BANK_CODE_EXPR = "boEditTransactionForm.dtausDebitBankCode";
    public static final String DTAUS_DEBIT_BANK_CODE_PATH = "command.boEditTransactionForm.dtausDebitBankCode";
    public static final String DTAUS_DEBIT_BANK_IBAN_EXPR = "boEditTransactionForm.dtausDebitBankIban";
    public static final String DTAUS_DEBIT_BANK_IBAN_PATH = "command.boEditTransactionForm.dtausDebitBankIban";
    public static final String DTAUS_CREDIT_ACCOUNT_NAME_EXPR = "boEditTransactionForm.dtausCreditAccountName";
    public static final String DTAUS_CREDIT_ACCOUNT_NAME_PATH = "command.boEditTransactionForm.dtausCreditAccountName";
    public static final String DTAUS_CREDIT_ACCOUNT_NUMBER_EXPR = "boEditTransactionForm.dtausCreditAccountNumber";
    public static final String DTAUS_CREDIT_ACCOUNT_NUMBER_PATH = "command.boEditTransactionForm.dtausCreditAccountNumber";
    public static final String DTAUS_CREDIT_BANK_NAME_EXPR = "boEditTransactionForm.dtausCreditBankName";
    public static final String DTAUS_CREDIT_BANK_NAME_PATH = "command.boEditTransactionForm.dtausCreditBankName";
    public static final String DTAUS_CREDIT_BANK_CODE_EXPR = "boEditTransactionForm.dtausCreditBankCode";
    public static final String DTAUS_CREDIT_BANK_CODE_PATH = "command.boEditTransactionForm.dtausCreditBankCode";
    public static final String DTAUS_CREDIT_BANK_IBAN_EXPR = "boEditTransactionForm.dtausCreditBankIban";
    public static final String DTAUS_CREDIT_BANK_IBAN_PATH = "command.boEditTransactionForm.dtausCreditBankIban";
    public static final String REMINDER_STATE_LEVEL_EXPR = "boEditTransactionForm.reminderStateLevel";
    public static final String REMINDER_STATE_LEVEL_PATH = "command.boEditTransactionForm.reminderStateLevel";
    public static final String TARGET_TRANSACTION_ID_EXPR = "boEditTransactionForm.targetTransactionId";
    public static final String TARGET_TRANSACTION_ID_PATH = "command.boEditTransactionForm.targetTransactionId";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo edit transaction)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _transactionNumber;
    private String _transactionNumberInitVal;
    private boolean _transactionNumberIsSet;
    private String _paidDate;
    private String _paidDateInitVal;
    private boolean _paidDateIsSet;
    private String _dueDate;
    private String _dueDateInitVal;
    private boolean _dueDateIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _modifiedDate;
    private String _modifiedDateInitVal;
    private boolean _modifiedDateIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _creditBankAccount;
    private String _creditBankAccountInitVal;
    private boolean _creditBankAccountIsSet;
    private String _debitBankAccount;
    private String _debitBankAccountInitVal;
    private boolean _debitBankAccountIsSet;
    private String _createNewDebitAccount;
    private String _createNewDebitAccountInitVal;
    private boolean _createNewDebitAccountIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private String _amount;
    private String _amountInitVal;
    private boolean _amountIsSet;
    private String _description;
    private String _descriptionInitVal;
    private boolean _descriptionIsSet;
    private DtausFile _dtausFile;
    private DtausFile _dtausFileInitVal;
    private boolean _dtausFileIsSet;
    private BankAccount _creditBankAccountObject;
    private BankAccount _creditBankAccountObjectInitVal;
    private boolean _creditBankAccountObjectIsSet;
    private BankAccount _debitBankAccountObject;
    private BankAccount _debitBankAccountObjectInitVal;
    private boolean _debitBankAccountObjectIsSet;
    private String _dtausDebitAccountName;
    private String _dtausDebitAccountNameInitVal;
    private boolean _dtausDebitAccountNameIsSet;
    private String _dtausDebitAccountNumber;
    private String _dtausDebitAccountNumberInitVal;
    private boolean _dtausDebitAccountNumberIsSet;
    private String _dtausDebitBankName;
    private String _dtausDebitBankNameInitVal;
    private boolean _dtausDebitBankNameIsSet;
    private String _dtausDebitBankCode;
    private String _dtausDebitBankCodeInitVal;
    private boolean _dtausDebitBankCodeIsSet;
    private String _dtausDebitBankIban;
    private String _dtausDebitBankIbanInitVal;
    private boolean _dtausDebitBankIbanIsSet;
    private String _dtausCreditAccountName;
    private String _dtausCreditAccountNameInitVal;
    private boolean _dtausCreditAccountNameIsSet;
    private String _dtausCreditAccountNumber;
    private String _dtausCreditAccountNumberInitVal;
    private boolean _dtausCreditAccountNumberIsSet;
    private String _dtausCreditBankName;
    private String _dtausCreditBankNameInitVal;
    private boolean _dtausCreditBankNameIsSet;
    private String _dtausCreditBankCode;
    private String _dtausCreditBankCodeInitVal;
    private boolean _dtausCreditBankCodeIsSet;
    private String _dtausCreditBankIban;
    private String _dtausCreditBankIbanInitVal;
    private boolean _dtausCreditBankIbanIsSet;
    private Integer _reminderStateLevel;
    private Integer _reminderStateLevelInitVal;
    private boolean _reminderStateLevelIsSet;
    private Long _targetTransactionId;
    private Long _targetTransactionIdInitVal;
    private boolean _targetTransactionIdIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setTransactionNumber(String transactionNumber) {
        if (!_transactionNumberIsSet) {
            _transactionNumberIsSet = true;
            _transactionNumberInitVal = transactionNumber;
        }
        _transactionNumber = transactionNumber;
    }

    public String getTransactionNumber() {
        return _transactionNumber;
    }

    public String transactionNumberInitVal() {
        return _transactionNumberInitVal;
    }

	public boolean transactionNumberIsDirty() {
        return !valuesAreEqual(_transactionNumberInitVal, _transactionNumber);
    }

    public boolean transactionNumberIsSet() {
        return _transactionNumberIsSet;
    }	


    public void setPaidDate(String paidDate) {
        if (!_paidDateIsSet) {
            _paidDateIsSet = true;
            _paidDateInitVal = paidDate;
        }
        _paidDate = paidDate;
    }

    public String getPaidDate() {
        return _paidDate;
    }

    public String paidDateInitVal() {
        return _paidDateInitVal;
    }

	public boolean paidDateIsDirty() {
        return !valuesAreEqual(_paidDateInitVal, _paidDate);
    }

    public boolean paidDateIsSet() {
        return _paidDateIsSet;
    }	


    public void setDueDate(String dueDate) {
        if (!_dueDateIsSet) {
            _dueDateIsSet = true;
            _dueDateInitVal = dueDate;
        }
        _dueDate = dueDate;
    }

    public String getDueDate() {
        return _dueDate;
    }

    public String dueDateInitVal() {
        return _dueDateInitVal;
    }

	public boolean dueDateIsDirty() {
        return !valuesAreEqual(_dueDateInitVal, _dueDate);
    }

    public boolean dueDateIsSet() {
        return _dueDateIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setModifiedDate(String modifiedDate) {
        if (!_modifiedDateIsSet) {
            _modifiedDateIsSet = true;
            _modifiedDateInitVal = modifiedDate;
        }
        _modifiedDate = modifiedDate;
    }

    public String getModifiedDate() {
        return _modifiedDate;
    }

    public String modifiedDateInitVal() {
        return _modifiedDateInitVal;
    }

	public boolean modifiedDateIsDirty() {
        return !valuesAreEqual(_modifiedDateInitVal, _modifiedDate);
    }

    public boolean modifiedDateIsSet() {
        return _modifiedDateIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setCreditBankAccount(String creditBankAccount) {
        if (!_creditBankAccountIsSet) {
            _creditBankAccountIsSet = true;
            _creditBankAccountInitVal = creditBankAccount;
        }
        _creditBankAccount = creditBankAccount;
    }

    public String getCreditBankAccount() {
        return _creditBankAccount;
    }

    public String creditBankAccountInitVal() {
        return _creditBankAccountInitVal;
    }

	public boolean creditBankAccountIsDirty() {
        return !valuesAreEqual(_creditBankAccountInitVal, _creditBankAccount);
    }

    public boolean creditBankAccountIsSet() {
        return _creditBankAccountIsSet;
    }	


    public void setDebitBankAccount(String debitBankAccount) {
        if (!_debitBankAccountIsSet) {
            _debitBankAccountIsSet = true;
            _debitBankAccountInitVal = debitBankAccount;
        }
        _debitBankAccount = debitBankAccount;
    }

    public String getDebitBankAccount() {
        return _debitBankAccount;
    }

    public String debitBankAccountInitVal() {
        return _debitBankAccountInitVal;
    }

	public boolean debitBankAccountIsDirty() {
        return !valuesAreEqual(_debitBankAccountInitVal, _debitBankAccount);
    }

    public boolean debitBankAccountIsSet() {
        return _debitBankAccountIsSet;
    }	


    public void setCreateNewDebitAccount(String createNewDebitAccount) {
        if (!_createNewDebitAccountIsSet) {
            _createNewDebitAccountIsSet = true;
            _createNewDebitAccountInitVal = createNewDebitAccount;
        }
        _createNewDebitAccount = createNewDebitAccount;
    }

    public String getCreateNewDebitAccount() {
        return _createNewDebitAccount;
    }

    public String createNewDebitAccountInitVal() {
        return _createNewDebitAccountInitVal;
    }

	public boolean createNewDebitAccountIsDirty() {
        return !valuesAreEqual(_createNewDebitAccountInitVal, _createNewDebitAccount);
    }

    public boolean createNewDebitAccountIsSet() {
        return _createNewDebitAccountIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public String getAmount() {
        return _amount;
    }

    public String amountInitVal() {
        return _amountInitVal;
    }

	public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }	


    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = description;
        }
        _description = description;
    }

    public String getDescription() {
        return _description;
    }

    public String descriptionInitVal() {
        return _descriptionInitVal;
    }

	public boolean descriptionIsDirty() {
        return !valuesAreEqual(_descriptionInitVal, _description);
    }

    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }	


    public void setDtausFile(DtausFile dtausFile) {
        if (!_dtausFileIsSet) {
            _dtausFileIsSet = true;
            _dtausFileInitVal = dtausFile;
        }
        _dtausFile = dtausFile;
    }

    public DtausFile getDtausFile() {
        return _dtausFile;
    }

    public DtausFile dtausFileInitVal() {
        return _dtausFileInitVal;
    }

	public boolean dtausFileIsDirty() {
        return !valuesAreEqual(_dtausFileInitVal, _dtausFile);
    }

    public boolean dtausFileIsSet() {
        return _dtausFileIsSet;
    }	


    public void setCreditBankAccountObject(BankAccount creditBankAccountObject) {
        if (!_creditBankAccountObjectIsSet) {
            _creditBankAccountObjectIsSet = true;
            _creditBankAccountObjectInitVal = creditBankAccountObject;
        }
        _creditBankAccountObject = creditBankAccountObject;
    }

    public BankAccount getCreditBankAccountObject() {
        return _creditBankAccountObject;
    }

    public BankAccount creditBankAccountObjectInitVal() {
        return _creditBankAccountObjectInitVal;
    }

	public boolean creditBankAccountObjectIsDirty() {
        return !valuesAreEqual(_creditBankAccountObjectInitVal, _creditBankAccountObject);
    }

    public boolean creditBankAccountObjectIsSet() {
        return _creditBankAccountObjectIsSet;
    }	


    public void setDebitBankAccountObject(BankAccount debitBankAccountObject) {
        if (!_debitBankAccountObjectIsSet) {
            _debitBankAccountObjectIsSet = true;
            _debitBankAccountObjectInitVal = debitBankAccountObject;
        }
        _debitBankAccountObject = debitBankAccountObject;
    }

    public BankAccount getDebitBankAccountObject() {
        return _debitBankAccountObject;
    }

    public BankAccount debitBankAccountObjectInitVal() {
        return _debitBankAccountObjectInitVal;
    }

	public boolean debitBankAccountObjectIsDirty() {
        return !valuesAreEqual(_debitBankAccountObjectInitVal, _debitBankAccountObject);
    }

    public boolean debitBankAccountObjectIsSet() {
        return _debitBankAccountObjectIsSet;
    }	


    public void setDtausDebitAccountName(String dtausDebitAccountName) {
        if (!_dtausDebitAccountNameIsSet) {
            _dtausDebitAccountNameIsSet = true;
            _dtausDebitAccountNameInitVal = dtausDebitAccountName;
        }
        _dtausDebitAccountName = dtausDebitAccountName;
    }

    public String getDtausDebitAccountName() {
        return _dtausDebitAccountName;
    }

    public String dtausDebitAccountNameInitVal() {
        return _dtausDebitAccountNameInitVal;
    }

	public boolean dtausDebitAccountNameIsDirty() {
        return !valuesAreEqual(_dtausDebitAccountNameInitVal, _dtausDebitAccountName);
    }

    public boolean dtausDebitAccountNameIsSet() {
        return _dtausDebitAccountNameIsSet;
    }	


    public void setDtausDebitAccountNumber(String dtausDebitAccountNumber) {
        if (!_dtausDebitAccountNumberIsSet) {
            _dtausDebitAccountNumberIsSet = true;
            _dtausDebitAccountNumberInitVal = dtausDebitAccountNumber;
        }
        _dtausDebitAccountNumber = dtausDebitAccountNumber;
    }

    public String getDtausDebitAccountNumber() {
        return _dtausDebitAccountNumber;
    }

    public String dtausDebitAccountNumberInitVal() {
        return _dtausDebitAccountNumberInitVal;
    }

	public boolean dtausDebitAccountNumberIsDirty() {
        return !valuesAreEqual(_dtausDebitAccountNumberInitVal, _dtausDebitAccountNumber);
    }

    public boolean dtausDebitAccountNumberIsSet() {
        return _dtausDebitAccountNumberIsSet;
    }	


    public void setDtausDebitBankName(String dtausDebitBankName) {
        if (!_dtausDebitBankNameIsSet) {
            _dtausDebitBankNameIsSet = true;
            _dtausDebitBankNameInitVal = dtausDebitBankName;
        }
        _dtausDebitBankName = dtausDebitBankName;
    }

    public String getDtausDebitBankName() {
        return _dtausDebitBankName;
    }

    public String dtausDebitBankNameInitVal() {
        return _dtausDebitBankNameInitVal;
    }

	public boolean dtausDebitBankNameIsDirty() {
        return !valuesAreEqual(_dtausDebitBankNameInitVal, _dtausDebitBankName);
    }

    public boolean dtausDebitBankNameIsSet() {
        return _dtausDebitBankNameIsSet;
    }	


    public void setDtausDebitBankCode(String dtausDebitBankCode) {
        if (!_dtausDebitBankCodeIsSet) {
            _dtausDebitBankCodeIsSet = true;
            _dtausDebitBankCodeInitVal = dtausDebitBankCode;
        }
        _dtausDebitBankCode = dtausDebitBankCode;
    }

    public String getDtausDebitBankCode() {
        return _dtausDebitBankCode;
    }

    public String dtausDebitBankCodeInitVal() {
        return _dtausDebitBankCodeInitVal;
    }

	public boolean dtausDebitBankCodeIsDirty() {
        return !valuesAreEqual(_dtausDebitBankCodeInitVal, _dtausDebitBankCode);
    }

    public boolean dtausDebitBankCodeIsSet() {
        return _dtausDebitBankCodeIsSet;
    }	


    public void setDtausDebitBankIban(String dtausDebitBankIban) {
        if (!_dtausDebitBankIbanIsSet) {
            _dtausDebitBankIbanIsSet = true;
            _dtausDebitBankIbanInitVal = dtausDebitBankIban;
        }
        _dtausDebitBankIban = dtausDebitBankIban;
    }

    public String getDtausDebitBankIban() {
        return _dtausDebitBankIban;
    }

    public String dtausDebitBankIbanInitVal() {
        return _dtausDebitBankIbanInitVal;
    }

	public boolean dtausDebitBankIbanIsDirty() {
        return !valuesAreEqual(_dtausDebitBankIbanInitVal, _dtausDebitBankIban);
    }

    public boolean dtausDebitBankIbanIsSet() {
        return _dtausDebitBankIbanIsSet;
    }	


    public void setDtausCreditAccountName(String dtausCreditAccountName) {
        if (!_dtausCreditAccountNameIsSet) {
            _dtausCreditAccountNameIsSet = true;
            _dtausCreditAccountNameInitVal = dtausCreditAccountName;
        }
        _dtausCreditAccountName = dtausCreditAccountName;
    }

    public String getDtausCreditAccountName() {
        return _dtausCreditAccountName;
    }

    public String dtausCreditAccountNameInitVal() {
        return _dtausCreditAccountNameInitVal;
    }

	public boolean dtausCreditAccountNameIsDirty() {
        return !valuesAreEqual(_dtausCreditAccountNameInitVal, _dtausCreditAccountName);
    }

    public boolean dtausCreditAccountNameIsSet() {
        return _dtausCreditAccountNameIsSet;
    }	


    public void setDtausCreditAccountNumber(String dtausCreditAccountNumber) {
        if (!_dtausCreditAccountNumberIsSet) {
            _dtausCreditAccountNumberIsSet = true;
            _dtausCreditAccountNumberInitVal = dtausCreditAccountNumber;
        }
        _dtausCreditAccountNumber = dtausCreditAccountNumber;
    }

    public String getDtausCreditAccountNumber() {
        return _dtausCreditAccountNumber;
    }

    public String dtausCreditAccountNumberInitVal() {
        return _dtausCreditAccountNumberInitVal;
    }

	public boolean dtausCreditAccountNumberIsDirty() {
        return !valuesAreEqual(_dtausCreditAccountNumberInitVal, _dtausCreditAccountNumber);
    }

    public boolean dtausCreditAccountNumberIsSet() {
        return _dtausCreditAccountNumberIsSet;
    }	


    public void setDtausCreditBankName(String dtausCreditBankName) {
        if (!_dtausCreditBankNameIsSet) {
            _dtausCreditBankNameIsSet = true;
            _dtausCreditBankNameInitVal = dtausCreditBankName;
        }
        _dtausCreditBankName = dtausCreditBankName;
    }

    public String getDtausCreditBankName() {
        return _dtausCreditBankName;
    }

    public String dtausCreditBankNameInitVal() {
        return _dtausCreditBankNameInitVal;
    }

	public boolean dtausCreditBankNameIsDirty() {
        return !valuesAreEqual(_dtausCreditBankNameInitVal, _dtausCreditBankName);
    }

    public boolean dtausCreditBankNameIsSet() {
        return _dtausCreditBankNameIsSet;
    }	


    public void setDtausCreditBankCode(String dtausCreditBankCode) {
        if (!_dtausCreditBankCodeIsSet) {
            _dtausCreditBankCodeIsSet = true;
            _dtausCreditBankCodeInitVal = dtausCreditBankCode;
        }
        _dtausCreditBankCode = dtausCreditBankCode;
    }

    public String getDtausCreditBankCode() {
        return _dtausCreditBankCode;
    }

    public String dtausCreditBankCodeInitVal() {
        return _dtausCreditBankCodeInitVal;
    }

	public boolean dtausCreditBankCodeIsDirty() {
        return !valuesAreEqual(_dtausCreditBankCodeInitVal, _dtausCreditBankCode);
    }

    public boolean dtausCreditBankCodeIsSet() {
        return _dtausCreditBankCodeIsSet;
    }	


    public void setDtausCreditBankIban(String dtausCreditBankIban) {
        if (!_dtausCreditBankIbanIsSet) {
            _dtausCreditBankIbanIsSet = true;
            _dtausCreditBankIbanInitVal = dtausCreditBankIban;
        }
        _dtausCreditBankIban = dtausCreditBankIban;
    }

    public String getDtausCreditBankIban() {
        return _dtausCreditBankIban;
    }

    public String dtausCreditBankIbanInitVal() {
        return _dtausCreditBankIbanInitVal;
    }

	public boolean dtausCreditBankIbanIsDirty() {
        return !valuesAreEqual(_dtausCreditBankIbanInitVal, _dtausCreditBankIban);
    }

    public boolean dtausCreditBankIbanIsSet() {
        return _dtausCreditBankIbanIsSet;
    }	


    public void setReminderStateLevel(Integer reminderStateLevel) {
        if (!_reminderStateLevelIsSet) {
            _reminderStateLevelIsSet = true;
            _reminderStateLevelInitVal = reminderStateLevel;
        }
        _reminderStateLevel = reminderStateLevel;
    }

    public Integer getReminderStateLevel() {
        return _reminderStateLevel;
    }

    public Integer reminderStateLevelInitVal() {
        return _reminderStateLevelInitVal;
    }

	public boolean reminderStateLevelIsDirty() {
        return !valuesAreEqual(_reminderStateLevelInitVal, _reminderStateLevel);
    }

    public boolean reminderStateLevelIsSet() {
        return _reminderStateLevelIsSet;
    }	


    public void setTargetTransactionId(Long targetTransactionId) {
        if (!_targetTransactionIdIsSet) {
            _targetTransactionIdIsSet = true;
            _targetTransactionIdInitVal = targetTransactionId;
        }
        _targetTransactionId = targetTransactionId;
    }

    public Long getTargetTransactionId() {
        return _targetTransactionId;
    }

    public Long targetTransactionIdInitVal() {
        return _targetTransactionIdInitVal;
    }

	public boolean targetTransactionIdIsDirty() {
        return !valuesAreEqual(_targetTransactionIdInitVal, _targetTransactionId);
    }

    public boolean targetTransactionIdIsSet() {
        return _targetTransactionIdIsSet;
    }	

}
