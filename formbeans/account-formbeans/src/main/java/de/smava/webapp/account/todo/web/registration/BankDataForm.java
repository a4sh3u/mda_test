//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.registration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bank data)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BankDataForm'.
 *
 * @author generator
 */
public class BankDataForm implements Serializable {

    public static final String BANK_ACCOUNT_TYPE_EXPR = "bankDataForm.bankAccountType";
    public static final String BANK_ACCOUNT_TYPE_PATH = "command.bankDataForm.bankAccountType";
	public static final String BANK_EXPR = "bankDataForm.bank";
    public static final String BANK_PATH = "command.bankDataForm.bank";
	public static final String BANK_CODE_EXPR = "bankDataForm.bankCode";
    public static final String BANK_CODE_PATH = "command.bankDataForm.bankCode";
	public static final String BANK_ACCOUNT_EXPR = "bankDataForm.bankAccount";
    public static final String BANK_ACCOUNT_PATH = "command.bankDataForm.bankAccount";
	public static final String BANK_IBAN_EXPR = "bankDataForm.bankIban";
    public static final String BANK_IBAN_PATH = "command.bankDataForm.bankIban";
	public static final String BANK_ACCOUNT_VALID_EXPR = "bankDataForm.bankAccountValid";
    public static final String BANK_ACCOUNT_VALID_PATH = "command.bankDataForm.bankAccountValid";
	public static final String DIR_DEBIT_MANDATE_EXPR = "bankDataForm.dirDebitMandate";
    public static final String DIR_DEBIT_MANDATE_PATH = "command.bankDataForm.dirDebitMandate";
	public static final String ACTIVITY_FEE_EXPR = "bankDataForm.activityFee";
    public static final String ACTIVITY_FEE_PATH = "command.bankDataForm.activityFee";
    public static final String BANK_CUSTOMER_NUMBER_EXPR = "bankDataForm.bankCustomerNumber";
    public static final String BANK_CUSTOMER_NUMBER_PATH = "command.bankDataForm.bankCustomerNumber";
    public static final String BANK_BIC_EXPR = "bankDataForm.bankBic";
    public static final String BANK_BIC_PATH = "command.bankDataForm.bankBic";
    public static final String NAME_EXPR = "bankDataForm.name";
    public static final String NAME_PATH = "command.bankDataForm.name";
    public static final String SEPA_MANDATE_REFERENCE_EXPR = "bankDataForm.sepaMandateReference";
    public static final String SEPA_MANDATE_REFERENCE_PATH = "command.bankDataForm.sepaMandateReference";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bank data)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _bankAccountType;
    private String _bankAccountTypeInitVal;
    private boolean _bankAccountTypeIsSet;
   	private String _bank;
    private String _bankInitVal;
    private boolean _bankIsSet;
   	private String _bankCode;
    private String _bankCodeInitVal;
    private boolean _bankCodeIsSet;
   	private String _bankAccount;
    private String _bankAccountInitVal;
    private boolean _bankAccountIsSet;
   	private String _bankIban;
    private String _bankIbanInitVal;
    private boolean _bankIbanIsSet;
   	private Boolean _bankAccountValid;
    private Boolean _bankAccountValidInitVal;
    private boolean _bankAccountValidIsSet;
   	private Boolean _dirDebitMandate;
    private Boolean _dirDebitMandateInitVal;
    private boolean _dirDebitMandateIsSet;
   	private Boolean _activityFee;
    private Boolean _activityFeeInitVal;
    private boolean _activityFeeIsSet;
    private String _bankCustomerNumber;
    private String _bankCustomerNumberInitVal;
    private boolean _bankCustomerNumberIsSet;
    private String _bankBic;
    private String _bankBicInitVal;
    private boolean _bankBicIsSet;
    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _sepaMandateReference;
    private String _sepaMandateReferenceInitVal;
    private boolean _sepaMandateReferenceIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setBankAccountType(String bankAccountType) {
        if (!_bankAccountTypeIsSet) {
            _bankAccountTypeIsSet = true;
            _bankAccountTypeInitVal = bankAccountType;
        }
        _bankAccountType = bankAccountType;
    }

    public String getBankAccountType() {
        return _bankAccountType;
    }

    public String bankAccountTypeInitVal() {
        return _bankAccountTypeInitVal;
    }

	public boolean bankAccountTypeIsDirty() {
        return !valuesAreEqual(_bankAccountTypeInitVal, _bankAccountType);
    }

    public boolean bankAccountTypeIsSet() {
        return _bankAccountTypeIsSet;
    }	

	
    public void setBank(String bank) {
        if (!_bankIsSet) {
            _bankIsSet = true;
            _bankInitVal = bank;
        }
        _bank = bank;
    }

    public String getBank() {
        return _bank;
    }

    public String bankInitVal() {
        return _bankInitVal;
    }
    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = bankCode;
        }
        _bankCode = bankCode;
    }

    public String getBankCode() {
        return _bankCode;
    }

    public String bankCodeInitVal() {
        return _bankCodeInitVal;
    }
    public void setBankAccount(String bankAccount) {
        if (!_bankAccountIsSet) {
            _bankAccountIsSet = true;
            _bankAccountInitVal = bankAccount;
        }
        _bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return _bankAccount;
    }

    public String bankAccountInitVal() {
        return _bankAccountInitVal;
    }
    public void setBankIban(String bankIban) {
        if (!_bankIbanIsSet) {
            _bankIbanIsSet = true;
            _bankIbanInitVal = bankIban;
        }
        _bankIban = bankIban;
    }

    public String getBankIban() {
        return _bankIban;
    }

    public String bankIbanInitVal() {
        return _bankIbanInitVal;
    }

	public boolean bankDataSetIsDirty() {
		boolean result = false;

		result = result || bankIsDirty();		
		result = result || bankCodeIsDirty();		
		result = result || bankAccountIsDirty();		
		result = result || bankIbanIsDirty();		
	
		return result;
	}
	
	public boolean bankIsDirty() {
        return !valuesAreEqual(_bankInitVal, _bank);
    }

    public boolean bankIsSet() {
        return _bankIsSet;
    }	
	public boolean bankCodeIsDirty() {
        return !valuesAreEqual(_bankCodeInitVal, _bankCode);
    }

    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }	
	public boolean bankAccountIsDirty() {
        return !valuesAreEqual(_bankAccountInitVal, _bankAccount);
    }

    public boolean bankAccountIsSet() {
        return _bankAccountIsSet;
    }	
	public boolean bankIbanIsDirty() {
        return !valuesAreEqual(_bankIbanInitVal, _bankIban);
    }

    public boolean bankIbanIsSet() {
        return _bankIbanIsSet;
    }	

	
    public void setBankAccountValid(Boolean bankAccountValid) {
        if (!_bankAccountValidIsSet) {
            _bankAccountValidIsSet = true;
            _bankAccountValidInitVal = bankAccountValid;
        }
        _bankAccountValid = bankAccountValid;
    }

    public Boolean getBankAccountValid() {
        return _bankAccountValid;
    }

    public Boolean bankAccountValidInitVal() {
        return _bankAccountValidInitVal;
    }
    public void setDirDebitMandate(Boolean dirDebitMandate) {
        if (!_dirDebitMandateIsSet) {
            _dirDebitMandateIsSet = true;
            _dirDebitMandateInitVal = dirDebitMandate;
        }
        _dirDebitMandate = dirDebitMandate;
    }

    public Boolean getDirDebitMandate() {
        return _dirDebitMandate;
    }

    public Boolean dirDebitMandateInitVal() {
        return _dirDebitMandateInitVal;
    }
    public void setActivityFee(Boolean activityFee) {
        if (!_activityFeeIsSet) {
            _activityFeeIsSet = true;
            _activityFeeInitVal = activityFee;
        }
        _activityFee = activityFee;
    }

    public Boolean getActivityFee() {
        return _activityFee;
    }

    public Boolean activityFeeInitVal() {
        return _activityFeeInitVal;
    }

	public boolean bankFlagsSetIsDirty() {
		boolean result = false;

		result = result || bankAccountValidIsDirty();		
		result = result || dirDebitMandateIsDirty();		
		result = result || activityFeeIsDirty();		
	
		return result;
	}
	
	public boolean bankAccountValidIsDirty() {
        return !valuesAreEqual(_bankAccountValidInitVal, _bankAccountValid);
    }

    public boolean bankAccountValidIsSet() {
        return _bankAccountValidIsSet;
    }	
	public boolean dirDebitMandateIsDirty() {
        return !valuesAreEqual(_dirDebitMandateInitVal, _dirDebitMandate);
    }

    public boolean dirDebitMandateIsSet() {
        return _dirDebitMandateIsSet;
    }	
	public boolean activityFeeIsDirty() {
        return !valuesAreEqual(_activityFeeInitVal, _activityFee);
    }

    public boolean activityFeeIsSet() {
        return _activityFeeIsSet;
    }	


    public void setBankCustomerNumber(String bankCustomerNumber) {
        if (!_bankCustomerNumberIsSet) {
            _bankCustomerNumberIsSet = true;
            _bankCustomerNumberInitVal = bankCustomerNumber;
        }
        _bankCustomerNumber = bankCustomerNumber;
    }

    public String getBankCustomerNumber() {
        return _bankCustomerNumber;
    }

    public String bankCustomerNumberInitVal() {
        return _bankCustomerNumberInitVal;
    }

	public boolean bankCustomerNumberIsDirty() {
        return !valuesAreEqual(_bankCustomerNumberInitVal, _bankCustomerNumber);
    }

    public boolean bankCustomerNumberIsSet() {
        return _bankCustomerNumberIsSet;
    }	


    public void setBankBic(String bankBic) {
        if (!_bankBicIsSet) {
            _bankBicIsSet = true;
            _bankBicInitVal = bankBic;
        }
        _bankBic = bankBic;
    }

    public String getBankBic() {
        return _bankBic;
    }

    public String bankBicInitVal() {
        return _bankBicInitVal;
    }

	public boolean bankBicIsDirty() {
        return !valuesAreEqual(_bankBicInitVal, _bankBic);
    }

    public boolean bankBicIsSet() {
        return _bankBicIsSet;
    }	


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setSepaMandateReference(String sepaMandateReference) {
        if (!_sepaMandateReferenceIsSet) {
            _sepaMandateReferenceIsSet = true;
            _sepaMandateReferenceInitVal = sepaMandateReference;
        }
        _sepaMandateReference = sepaMandateReference;
    }

    public String getSepaMandateReference() {
        return _sepaMandateReference;
    }

    public String sepaMandateReferenceInitVal() {
        return _sepaMandateReferenceInitVal;
    }

	public boolean sepaMandateReferenceIsDirty() {
        return !valuesAreEqual(_sepaMandateReferenceInitVal, _sepaMandateReference);
    }

    public boolean sepaMandateReferenceIsSet() {
        return _sepaMandateReferenceIsSet;
    }	

}
