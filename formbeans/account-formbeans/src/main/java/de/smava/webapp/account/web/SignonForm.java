//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.web;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(signon)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'SignonForm'.
 *
 * @author generator
 */
public class SignonForm implements Serializable {

    public static final String EMAIL_EXPR = "signonForm.email";
    public static final String EMAIL_PATH = "command.signonForm.email";
    public static final String PASSWORD_EXPR = "signonForm.password";
    public static final String PASSWORD_PATH = "command.signonForm.password";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(signon)}
    private static final long serialVersionUID = 2364549782712215407L;
// !!!!!!!! End of insert code section !!!!!!!!


    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;

    private String _password;
    private String _passwordInitVal;
    private boolean _passwordIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

    public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }

    public void setPassword(String password) {
        if (!_passwordIsSet) {
            _passwordIsSet = true;
            _passwordInitVal = password;
        }
        _password = password;
    }

    public String getPassword() {
        return _password;
    }

    public String passwordInitVal() {
        return _passwordInitVal;
    }

    public boolean passwordIsDirty() {
        return !valuesAreEqual(_passwordInitVal, _password);
    }

    public boolean passwordIsSet() {
        return _passwordIsSet;
    }

}
