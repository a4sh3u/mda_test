//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.account;
            
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo credit worthiness)}
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoCreditWorthinessForm'.
 *
 * @author generator
 */
public class BoCreditWorthinessForm extends de.smava.webapp.account.todo.web.account.CreditWorthinessForm implements Serializable {

    public static final String BORROWERS_MARKET_EXPR = "creditWorthinessForm.borrowersMarket";
    public static final String BORROWERS_MARKET_PATH = "command.creditWorthinessForm.borrowersMarket";
    public static final String GENERAL_RATES_TOO_HIGH_EXPR = "creditWorthinessForm.generalRatesTooHigh";
    public static final String GENERAL_RATES_TOO_HIGH_PATH = "command.creditWorthinessForm.generalRatesTooHigh";
    public static final String EXTENDED_RATES_TOO_HIGH_EXPR = "creditWorthinessForm.extendedRatesTooHigh";
    public static final String EXTENDED_RATES_TOO_HIGH_PATH = "command.creditWorthinessForm.extendedRatesTooHigh";
    public static final String GENERAL_MONTHLY_RATE_EXPR = "creditWorthinessForm.generalMonthlyRate";
    public static final String GENERAL_MONTHLY_RATE_PATH = "command.creditWorthinessForm.generalMonthlyRate";
    public static final String NEW_GENERAL_MONTHLY_RATE_EXPR = "creditWorthinessForm.newGeneralMonthlyRate";
    public static final String NEW_GENERAL_MONTHLY_RATE_PATH = "command.creditWorthinessForm.newGeneralMonthlyRate";
    public static final String EXTENDED_MONTHLY_RATE_EXPR = "creditWorthinessForm.extendedMonthlyRate";
    public static final String EXTENDED_MONTHLY_RATE_PATH = "command.creditWorthinessForm.extendedMonthlyRate";
    public static final String NEW_EXTENDED_MONTHLY_RATE_EXPR = "creditWorthinessForm.newExtendedMonthlyRate";
    public static final String NEW_EXTENDED_MONTHLY_RATE_PATH = "command.creditWorthinessForm.newExtendedMonthlyRate";
    public static final String RATING_HISTORY_EXPR = "creditWorthinessForm.ratingHistory";
    public static final String RATING_HISTORY_PATH = "command.creditWorthinessForm.ratingHistory";
    public static final String GENERAL_CREDIT_LINES_MAP_EXPR = "creditWorthinessForm.generalCreditLinesMap";
    public static final String GENERAL_CREDIT_LINES_MAP_PATH = "command.creditWorthinessForm.generalCreditLinesMap";
    public static final String EXTENDED_CREDIT_LINES_MAP_EXPR = "creditWorthinessForm.extendedCreditLinesMap";
    public static final String EXTENDED_CREDIT_LINES_MAP_PATH = "command.creditWorthinessForm.extendedCreditLinesMap";
    public static final String SMAVA_CREDIT_RATE_POSSIBILITY_EXPR = "creditWorthinessForm.smavaCreditRatePossibility";
    public static final String SMAVA_CREDIT_RATE_POSSIBILITY_PATH = "command.creditWorthinessForm.smavaCreditRatePossibility";
    public static final String BROKERAGE_CREDIT_RATE_POSSIBILITY_EXPR = "creditWorthinessForm.brokerageCreditRatePossibility";
    public static final String BROKERAGE_CREDIT_RATE_POSSIBILITY_PATH = "command.creditWorthinessForm.brokerageCreditRatePossibility";
    public static final String SCHUFA_EXPR = "creditWorthinessForm.schufa";
    public static final String SCHUFA_PATH = "command.creditWorthinessForm.schufa";
    public static final String SCHUFA_LOAN_EXPR = "creditWorthinessForm.schufaLoan";
    public static final String SCHUFA_LOAN_PATH = "command.creditWorthinessForm.schufaLoan";
    public static final String SCHUFA_LEASING_EXPR = "creditWorthinessForm.schufaLeasing";
    public static final String SCHUFA_LEASING_PATH = "command.creditWorthinessForm.schufaLeasing";
    public static final String SCHUFA_LOAN_RELATED_SAVINGS_EXPR = "creditWorthinessForm.schufaLoanRelatedSavings";
    public static final String SCHUFA_LOAN_RELATED_SAVINGS_PATH = "command.creditWorthinessForm.schufaLoanRelatedSavings";
    public static final String SCHUFA_BUSINESS_LOAN_EXPR = "creditWorthinessForm.schufaBusinessLoan";
    public static final String SCHUFA_BUSINESS_LOAN_PATH = "command.creditWorthinessForm.schufaBusinessLoan";
    public static final String SCHUFA_BUSINESS_LEASING_EXPR = "creditWorthinessForm.schufaBusinessLeasing";
    public static final String SCHUFA_BUSINESS_LEASING_PATH = "command.creditWorthinessForm.schufaBusinessLeasing";
    public static final String REMOVE_ORDER_FROM_MARKETPLACE_WARNING_EXPR = "creditWorthinessForm.removeOrderFromMarketplaceWarning";
    public static final String REMOVE_ORDER_FROM_MARKETPLACE_WARNING_PATH = "command.creditWorthinessForm.removeOrderFromMarketplaceWarning";
    public static final String CREDIT_RATE_INDICATOR_EXPR = "creditWorthinessForm.creditRateIndicator";
    public static final String CREDIT_RATE_INDICATOR_PATH = "command.creditWorthinessForm.creditRateIndicator";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo credit worthiness)}
    private static final long serialVersionUID = 7734365594385947337L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _borrowersMarket;
    private String _borrowersMarketInitVal;
    private boolean _borrowersMarketIsSet;
    private boolean _generalRatesTooHigh;
    private boolean _generalRatesTooHighInitVal;
    private boolean _generalRatesTooHighIsSet;
    private boolean _extendedRatesTooHigh;
    private boolean _extendedRatesTooHighInitVal;
    private boolean _extendedRatesTooHighIsSet;
    private String _generalMonthlyRate;
    private String _generalMonthlyRateInitVal;
    private boolean _generalMonthlyRateIsSet;
    private String _newGeneralMonthlyRate;
    private String _newGeneralMonthlyRateInitVal;
    private boolean _newGeneralMonthlyRateIsSet;
    private String _extendedMonthlyRate;
    private String _extendedMonthlyRateInitVal;
    private boolean _extendedMonthlyRateIsSet;
    private String _newExtendedMonthlyRate;
    private String _newExtendedMonthlyRateInitVal;
    private boolean _newExtendedMonthlyRateIsSet;
    private String _ratingHistory;
    private String _ratingHistoryInitVal;
    private boolean _ratingHistoryIsSet;
    private Map<String, String> _generalCreditLinesMap;
    private Map<String, String> _generalCreditLinesMapInitVal;
    private boolean _generalCreditLinesMapIsSet;
    private Map<String, String> _extendedCreditLinesMap;
    private Map<String, String> _extendedCreditLinesMapInitVal;
    private boolean _extendedCreditLinesMapIsSet;
    private String _smavaCreditRatePossibility;
    private String _smavaCreditRatePossibilityInitVal;
    private boolean _smavaCreditRatePossibilityIsSet;
    private String _brokerageCreditRatePossibility;
    private String _brokerageCreditRatePossibilityInitVal;
    private boolean _brokerageCreditRatePossibilityIsSet;
    private Date _schufa;
    private Date _schufaInitVal;
    private boolean _schufaIsSet;
    private String _schufaLoan;
    private String _schufaLoanInitVal;
    private boolean _schufaLoanIsSet;
    private String _schufaLeasing;
    private String _schufaLeasingInitVal;
    private boolean _schufaLeasingIsSet;
    private String _schufaLoanRelatedSavings;
    private String _schufaLoanRelatedSavingsInitVal;
    private boolean _schufaLoanRelatedSavingsIsSet;
    private String _schufaBusinessLoan;
    private String _schufaBusinessLoanInitVal;
    private boolean _schufaBusinessLoanIsSet;
    private String _schufaBusinessLeasing;
    private String _schufaBusinessLeasingInitVal;
    private boolean _schufaBusinessLeasingIsSet;
    private boolean _removeOrderFromMarketplaceWarning;
    private boolean _removeOrderFromMarketplaceWarningInitVal;
    private boolean _removeOrderFromMarketplaceWarningIsSet;
    private int _creditRateIndicator;
    private int _creditRateIndicatorInitVal;
    private boolean _creditRateIndicatorIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setBorrowersMarket(String borrowersMarket) {
        if (!_borrowersMarketIsSet) {
            _borrowersMarketIsSet = true;
            _borrowersMarketInitVal = borrowersMarket;
        }
        _borrowersMarket = borrowersMarket;
    }

    public String getBorrowersMarket() {
        return _borrowersMarket;
    }

    public String borrowersMarketInitVal() {
        return _borrowersMarketInitVal;
    }

	public boolean borrowersMarketIsDirty() {
        return !valuesAreEqual(_borrowersMarketInitVal, _borrowersMarket);
    }

    public boolean borrowersMarketIsSet() {
        return _borrowersMarketIsSet;
    }	


    public void setGeneralRatesTooHigh(boolean generalRatesTooHigh) {
        if (!_generalRatesTooHighIsSet) {
            _generalRatesTooHighIsSet = true;
            _generalRatesTooHighInitVal = generalRatesTooHigh;
        }
        _generalRatesTooHigh = generalRatesTooHigh;
    }

    public boolean getGeneralRatesTooHigh() {
        return _generalRatesTooHigh;
    }

    public boolean generalRatesTooHighInitVal() {
        return _generalRatesTooHighInitVal;
    }

	public boolean generalRatesTooHighIsDirty() {
        return !valuesAreEqual(_generalRatesTooHighInitVal, _generalRatesTooHigh);
    }

    public boolean generalRatesTooHighIsSet() {
        return _generalRatesTooHighIsSet;
    }	


    public void setExtendedRatesTooHigh(boolean extendedRatesTooHigh) {
        if (!_extendedRatesTooHighIsSet) {
            _extendedRatesTooHighIsSet = true;
            _extendedRatesTooHighInitVal = extendedRatesTooHigh;
        }
        _extendedRatesTooHigh = extendedRatesTooHigh;
    }

    public boolean getExtendedRatesTooHigh() {
        return _extendedRatesTooHigh;
    }

    public boolean extendedRatesTooHighInitVal() {
        return _extendedRatesTooHighInitVal;
    }

	public boolean extendedRatesTooHighIsDirty() {
        return !valuesAreEqual(_extendedRatesTooHighInitVal, _extendedRatesTooHigh);
    }

    public boolean extendedRatesTooHighIsSet() {
        return _extendedRatesTooHighIsSet;
    }	


    public void setGeneralMonthlyRate(String generalMonthlyRate) {
        if (!_generalMonthlyRateIsSet) {
            _generalMonthlyRateIsSet = true;
            _generalMonthlyRateInitVal = generalMonthlyRate;
        }
        _generalMonthlyRate = generalMonthlyRate;
    }

    public String getGeneralMonthlyRate() {
        return _generalMonthlyRate;
    }

    public String generalMonthlyRateInitVal() {
        return _generalMonthlyRateInitVal;
    }

	public boolean generalMonthlyRateIsDirty() {
        return !valuesAreEqual(_generalMonthlyRateInitVal, _generalMonthlyRate);
    }

    public boolean generalMonthlyRateIsSet() {
        return _generalMonthlyRateIsSet;
    }	


    public void setNewGeneralMonthlyRate(String newGeneralMonthlyRate) {
        if (!_newGeneralMonthlyRateIsSet) {
            _newGeneralMonthlyRateIsSet = true;
            _newGeneralMonthlyRateInitVal = newGeneralMonthlyRate;
        }
        _newGeneralMonthlyRate = newGeneralMonthlyRate;
    }

    public String getNewGeneralMonthlyRate() {
        return _newGeneralMonthlyRate;
    }

    public String newGeneralMonthlyRateInitVal() {
        return _newGeneralMonthlyRateInitVal;
    }

	public boolean newGeneralMonthlyRateIsDirty() {
        return !valuesAreEqual(_newGeneralMonthlyRateInitVal, _newGeneralMonthlyRate);
    }

    public boolean newGeneralMonthlyRateIsSet() {
        return _newGeneralMonthlyRateIsSet;
    }	


    public void setExtendedMonthlyRate(String extendedMonthlyRate) {
        if (!_extendedMonthlyRateIsSet) {
            _extendedMonthlyRateIsSet = true;
            _extendedMonthlyRateInitVal = extendedMonthlyRate;
        }
        _extendedMonthlyRate = extendedMonthlyRate;
    }

    public String getExtendedMonthlyRate() {
        return _extendedMonthlyRate;
    }

    public String extendedMonthlyRateInitVal() {
        return _extendedMonthlyRateInitVal;
    }

	public boolean extendedMonthlyRateIsDirty() {
        return !valuesAreEqual(_extendedMonthlyRateInitVal, _extendedMonthlyRate);
    }

    public boolean extendedMonthlyRateIsSet() {
        return _extendedMonthlyRateIsSet;
    }	


    public void setNewExtendedMonthlyRate(String newExtendedMonthlyRate) {
        if (!_newExtendedMonthlyRateIsSet) {
            _newExtendedMonthlyRateIsSet = true;
            _newExtendedMonthlyRateInitVal = newExtendedMonthlyRate;
        }
        _newExtendedMonthlyRate = newExtendedMonthlyRate;
    }

    public String getNewExtendedMonthlyRate() {
        return _newExtendedMonthlyRate;
    }

    public String newExtendedMonthlyRateInitVal() {
        return _newExtendedMonthlyRateInitVal;
    }

	public boolean newExtendedMonthlyRateIsDirty() {
        return !valuesAreEqual(_newExtendedMonthlyRateInitVal, _newExtendedMonthlyRate);
    }

    public boolean newExtendedMonthlyRateIsSet() {
        return _newExtendedMonthlyRateIsSet;
    }	


    public void setRatingHistory(String ratingHistory) {
        if (!_ratingHistoryIsSet) {
            _ratingHistoryIsSet = true;
            _ratingHistoryInitVal = ratingHistory;
        }
        _ratingHistory = ratingHistory;
    }

    public String getRatingHistory() {
        return _ratingHistory;
    }

    public String ratingHistoryInitVal() {
        return _ratingHistoryInitVal;
    }

	public boolean ratingHistoryIsDirty() {
        return !valuesAreEqual(_ratingHistoryInitVal, _ratingHistory);
    }

    public boolean ratingHistoryIsSet() {
        return _ratingHistoryIsSet;
    }	


    public void setGeneralCreditLinesMap(Map<String, String> generalCreditLinesMap) {
        if (!_generalCreditLinesMapIsSet) {
            _generalCreditLinesMapIsSet = true;
            _generalCreditLinesMapInitVal = generalCreditLinesMap;
        }
        _generalCreditLinesMap = generalCreditLinesMap;
    }

    public Map<String, String> getGeneralCreditLinesMap() {
        return _generalCreditLinesMap;
    }

    public Map<String, String> generalCreditLinesMapInitVal() {
        return _generalCreditLinesMapInitVal;
    }

	public boolean generalCreditLinesMapIsDirty() {
        return !valuesAreEqual(_generalCreditLinesMapInitVal, _generalCreditLinesMap);
    }

    public boolean generalCreditLinesMapIsSet() {
        return _generalCreditLinesMapIsSet;
    }	


    public void setExtendedCreditLinesMap(Map<String, String> extendedCreditLinesMap) {
        if (!_extendedCreditLinesMapIsSet) {
            _extendedCreditLinesMapIsSet = true;
            _extendedCreditLinesMapInitVal = extendedCreditLinesMap;
        }
        _extendedCreditLinesMap = extendedCreditLinesMap;
    }

    public Map<String, String> getExtendedCreditLinesMap() {
        return _extendedCreditLinesMap;
    }

    public Map<String, String> extendedCreditLinesMapInitVal() {
        return _extendedCreditLinesMapInitVal;
    }

	public boolean extendedCreditLinesMapIsDirty() {
        return !valuesAreEqual(_extendedCreditLinesMapInitVal, _extendedCreditLinesMap);
    }

    public boolean extendedCreditLinesMapIsSet() {
        return _extendedCreditLinesMapIsSet;
    }	


    public void setSmavaCreditRatePossibility(String smavaCreditRatePossibility) {
        if (!_smavaCreditRatePossibilityIsSet) {
            _smavaCreditRatePossibilityIsSet = true;
            _smavaCreditRatePossibilityInitVal = smavaCreditRatePossibility;
        }
        _smavaCreditRatePossibility = smavaCreditRatePossibility;
    }

    public String getSmavaCreditRatePossibility() {
        return _smavaCreditRatePossibility;
    }

    public String smavaCreditRatePossibilityInitVal() {
        return _smavaCreditRatePossibilityInitVal;
    }

	public boolean smavaCreditRatePossibilityIsDirty() {
        return !valuesAreEqual(_smavaCreditRatePossibilityInitVal, _smavaCreditRatePossibility);
    }

    public boolean smavaCreditRatePossibilityIsSet() {
        return _smavaCreditRatePossibilityIsSet;
    }	


    public void setBrokerageCreditRatePossibility(String brokerageCreditRatePossibility) {
        if (!_brokerageCreditRatePossibilityIsSet) {
            _brokerageCreditRatePossibilityIsSet = true;
            _brokerageCreditRatePossibilityInitVal = brokerageCreditRatePossibility;
        }
        _brokerageCreditRatePossibility = brokerageCreditRatePossibility;
    }

    public String getBrokerageCreditRatePossibility() {
        return _brokerageCreditRatePossibility;
    }

    public String brokerageCreditRatePossibilityInitVal() {
        return _brokerageCreditRatePossibilityInitVal;
    }

	public boolean brokerageCreditRatePossibilityIsDirty() {
        return !valuesAreEqual(_brokerageCreditRatePossibilityInitVal, _brokerageCreditRatePossibility);
    }

    public boolean brokerageCreditRatePossibilityIsSet() {
        return _brokerageCreditRatePossibilityIsSet;
    }	


    public void setSchufa(Date schufa) {
        if (!_schufaIsSet) {
            _schufaIsSet = true;

            if (schufa == null) {
                _schufaInitVal = null;
            } else {
                _schufaInitVal = (Date) schufa.clone();
            }
        }

        if (schufa == null) {
            _schufa = null;
        } else {
            _schufa = (Date) schufa.clone();
        }
    }

    public Date getSchufa() {
        Date schufa = null;
        if (_schufa != null) {
            schufa = (Date) _schufa.clone();
        }
        return schufa;
    }

    public Date schufaInitVal() {
        Date schufaInitVal = null;
        if (_schufaInitVal != null) {
            schufaInitVal = (Date) _schufaInitVal.clone();
        }
        return schufaInitVal;
    }

	public boolean schufaIsDirty() {
        return !valuesAreEqual(_schufaInitVal, _schufa);
    }

    public boolean schufaIsSet() {
        return _schufaIsSet;
    }	


    public void setSchufaLoan(String schufaLoan) {
        if (!_schufaLoanIsSet) {
            _schufaLoanIsSet = true;
            _schufaLoanInitVal = schufaLoan;
        }
        _schufaLoan = schufaLoan;
    }

    public String getSchufaLoan() {
        return _schufaLoan;
    }

    public String schufaLoanInitVal() {
        return _schufaLoanInitVal;
    }

	public boolean schufaLoanIsDirty() {
        return !valuesAreEqual(_schufaLoanInitVal, _schufaLoan);
    }

    public boolean schufaLoanIsSet() {
        return _schufaLoanIsSet;
    }	


    public void setSchufaLeasing(String schufaLeasing) {
        if (!_schufaLeasingIsSet) {
            _schufaLeasingIsSet = true;
            _schufaLeasingInitVal = schufaLeasing;
        }
        _schufaLeasing = schufaLeasing;
    }

    public String getSchufaLeasing() {
        return _schufaLeasing;
    }

    public String schufaLeasingInitVal() {
        return _schufaLeasingInitVal;
    }

	public boolean schufaLeasingIsDirty() {
        return !valuesAreEqual(_schufaLeasingInitVal, _schufaLeasing);
    }

    public boolean schufaLeasingIsSet() {
        return _schufaLeasingIsSet;
    }	


    public void setSchufaLoanRelatedSavings(String schufaLoanRelatedSavings) {
        if (!_schufaLoanRelatedSavingsIsSet) {
            _schufaLoanRelatedSavingsIsSet = true;
            _schufaLoanRelatedSavingsInitVal = schufaLoanRelatedSavings;
        }
        _schufaLoanRelatedSavings = schufaLoanRelatedSavings;
    }

    public String getSchufaLoanRelatedSavings() {
        return _schufaLoanRelatedSavings;
    }

    public String schufaLoanRelatedSavingsInitVal() {
        return _schufaLoanRelatedSavingsInitVal;
    }

	public boolean schufaLoanRelatedSavingsIsDirty() {
        return !valuesAreEqual(_schufaLoanRelatedSavingsInitVal, _schufaLoanRelatedSavings);
    }

    public boolean schufaLoanRelatedSavingsIsSet() {
        return _schufaLoanRelatedSavingsIsSet;
    }	


    public void setSchufaBusinessLoan(String schufaBusinessLoan) {
        if (!_schufaBusinessLoanIsSet) {
            _schufaBusinessLoanIsSet = true;
            _schufaBusinessLoanInitVal = schufaBusinessLoan;
        }
        _schufaBusinessLoan = schufaBusinessLoan;
    }

    public String getSchufaBusinessLoan() {
        return _schufaBusinessLoan;
    }

    public String schufaBusinessLoanInitVal() {
        return _schufaBusinessLoanInitVal;
    }

	public boolean schufaBusinessLoanIsDirty() {
        return !valuesAreEqual(_schufaBusinessLoanInitVal, _schufaBusinessLoan);
    }

    public boolean schufaBusinessLoanIsSet() {
        return _schufaBusinessLoanIsSet;
    }	


    public void setSchufaBusinessLeasing(String schufaBusinessLeasing) {
        if (!_schufaBusinessLeasingIsSet) {
            _schufaBusinessLeasingIsSet = true;
            _schufaBusinessLeasingInitVal = schufaBusinessLeasing;
        }
        _schufaBusinessLeasing = schufaBusinessLeasing;
    }

    public String getSchufaBusinessLeasing() {
        return _schufaBusinessLeasing;
    }

    public String schufaBusinessLeasingInitVal() {
        return _schufaBusinessLeasingInitVal;
    }

	public boolean schufaBusinessLeasingIsDirty() {
        return !valuesAreEqual(_schufaBusinessLeasingInitVal, _schufaBusinessLeasing);
    }

    public boolean schufaBusinessLeasingIsSet() {
        return _schufaBusinessLeasingIsSet;
    }	


    public void setRemoveOrderFromMarketplaceWarning(boolean removeOrderFromMarketplaceWarning) {
        if (!_removeOrderFromMarketplaceWarningIsSet) {
            _removeOrderFromMarketplaceWarningIsSet = true;
            _removeOrderFromMarketplaceWarningInitVal = removeOrderFromMarketplaceWarning;
        }
        _removeOrderFromMarketplaceWarning = removeOrderFromMarketplaceWarning;
    }

    public boolean getRemoveOrderFromMarketplaceWarning() {
        return _removeOrderFromMarketplaceWarning;
    }

    public boolean removeOrderFromMarketplaceWarningInitVal() {
        return _removeOrderFromMarketplaceWarningInitVal;
    }

	public boolean removeOrderFromMarketplaceWarningIsDirty() {
        return !valuesAreEqual(_removeOrderFromMarketplaceWarningInitVal, _removeOrderFromMarketplaceWarning);
    }

    public boolean removeOrderFromMarketplaceWarningIsSet() {
        return _removeOrderFromMarketplaceWarningIsSet;
    }	


    public void setCreditRateIndicator(int creditRateIndicator) {
        if (!_creditRateIndicatorIsSet) {
            _creditRateIndicatorIsSet = true;
            _creditRateIndicatorInitVal = creditRateIndicator;
        }
        _creditRateIndicator = creditRateIndicator;
    }

    public int getCreditRateIndicator() {
        return _creditRateIndicator;
    }

    public int creditRateIndicatorInitVal() {
        return _creditRateIndicatorInitVal;
    }

	public boolean creditRateIndicatorIsDirty() {
        return !valuesAreEqual(_creditRateIndicatorInitVal, _creditRateIndicator);
    }

    public boolean creditRateIndicatorIsSet() {
        return _creditRateIndicatorIsSet;
    }	

}
