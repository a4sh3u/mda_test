//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.search;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo search)}

import java.io.Serializable;
import java.util.Date;
import java.util.List;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoSearchForm'.
 *
 * @author generator
 */
public class BoSearchForm implements Serializable {

    public static final String SEARCH_ACCOUNT_ID_EXPR = "boSearchForm.searchAccountId";
    public static final String SEARCH_ACCOUNT_ID_PATH = "command.boSearchForm.searchAccountId";
    public static final String ACCOUNT_ID_EXPR = "boSearchForm.accountId";
    public static final String ACCOUNT_ID_PATH = "command.boSearchForm.accountId";
    public static final String SEARCH_EMAIL_EXPR = "boSearchForm.searchEmail";
    public static final String SEARCH_EMAIL_PATH = "command.boSearchForm.searchEmail";
    public static final String EMAIL_EXPR = "boSearchForm.email";
    public static final String EMAIL_PATH = "command.boSearchForm.email";
    public static final String SEARCH_USERNAME_EXPR = "boSearchForm.searchUsername";
    public static final String SEARCH_USERNAME_PATH = "command.boSearchForm.searchUsername";
    public static final String USERNAME_EXPR = "boSearchForm.username";
    public static final String USERNAME_PATH = "command.boSearchForm.username";
    public static final String SEARCH_PERSON_NAMES_EXPR = "boSearchForm.searchPersonNames";
    public static final String SEARCH_PERSON_NAMES_PATH = "command.boSearchForm.searchPersonNames";
    public static final String FIRST_NAME_EXPR = "boSearchForm.firstName";
    public static final String FIRST_NAME_PATH = "command.boSearchForm.firstName";
    public static final String LAST_NAME_EXPR = "boSearchForm.lastName";
    public static final String LAST_NAME_PATH = "command.boSearchForm.lastName";
    public static final String SEARCH_ROLES_EXPR = "boSearchForm.searchRoles";
    public static final String SEARCH_ROLES_PATH = "command.boSearchForm.searchRoles";
    public static final String ROLES_EXPR = "boSearchForm.roles";
    public static final String ROLES_PATH = "command.boSearchForm.roles";
    public static final String SEARCH_REMINDED_USERS_EXPR = "boSearchForm.searchRemindedUsers";
    public static final String SEARCH_REMINDED_USERS_PATH = "command.boSearchForm.searchRemindedUsers";
    public static final String SEARCH_REMINDED_BORROWER_EXPR = "boSearchForm.searchRemindedBorrower";
    public static final String SEARCH_REMINDED_BORROWER_PATH = "command.boSearchForm.searchRemindedBorrower";
    public static final String SEARCH_BANK_ACCOUNT_NO_EXPR = "boSearchForm.searchBankAccountNo";
    public static final String SEARCH_BANK_ACCOUNT_NO_PATH = "command.boSearchForm.searchBankAccountNo";
    public static final String SEARCH_IBAN_EXPR = "boSearchForm.searchIban";
    public static final String SEARCH_IBAN_PATH = "command.boSearchForm.searchIban";
    public static final String BANK_ACCOUNT_NO_EXPR = "boSearchForm.bankAccountNo";
    public static final String BANK_ACCOUNT_NO_PATH = "command.boSearchForm.bankAccountNo";
    public static final String PHONE_EXPR = "boSearchForm.phone";
    public static final String PHONE_PATH = "command.boSearchForm.phone";
    public static final String BIRTH_DATE_EXPR = "boSearchForm.birthDate";
    public static final String BIRTH_DATE_PATH = "command.boSearchForm.birthDate";
    public static final String IBAN_EXPR = "boSearchForm.iban";
    public static final String IBAN_PATH = "command.boSearchForm.iban";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo search)}
// !!!!!!!! End of insert code section !!!!!!!!

    private boolean _searchAccountId;
    private boolean _searchAccountIdInitVal;
    private boolean _searchAccountIdIsSet;
    private String _accountId;
    private String _accountIdInitVal;
    private boolean _accountIdIsSet;
    private boolean _searchEmail;
    private boolean _searchEmailInitVal;
    private boolean _searchEmailIsSet;
    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private boolean _searchUsername;
    private boolean _searchUsernameInitVal;
    private boolean _searchUsernameIsSet;
    private String _username;
    private String _usernameInitVal;
    private boolean _usernameIsSet;
    private boolean _searchPersonNames;
    private boolean _searchPersonNamesInitVal;
    private boolean _searchPersonNamesIsSet;
    private String _firstName;
    private String _firstNameInitVal;
    private boolean _firstNameIsSet;
    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;
    private boolean _searchRoles;
    private boolean _searchRolesInitVal;
    private boolean _searchRolesIsSet;
    private List<String> _roles;
    private List<String> _rolesInitVal;
    private boolean _rolesIsSet;
    private boolean _searchRemindedUsers;
    private boolean _searchRemindedUsersInitVal;
    private boolean _searchRemindedUsersIsSet;
    private boolean _searchRemindedBorrower;
    private boolean _searchRemindedBorrowerInitVal;
    private boolean _searchRemindedBorrowerIsSet;
    private boolean _searchBankAccountNo;
    private boolean _searchBankAccountNoInitVal;
    private boolean _searchBankAccountNoIsSet;
    private boolean _searchIban;
    private boolean _searchIbanInitVal;
    private boolean _searchIbanIsSet;
    private String _bankAccountNo;
    private String _bankAccountNoInitVal;
    private boolean _bankAccountNoIsSet;
    private String _phone;
    private String _phoneInitVal;
    private boolean _phoneIsSet;
    private Date _birthDate;
    private Date _birthDateInitVal;
    private boolean _birthDateIsSet;
    private String _iban;
    private String _ibanInitVal;
    private boolean _ibanIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setSearchAccountId(boolean searchAccountId) {
        if (!_searchAccountIdIsSet) {
            _searchAccountIdIsSet = true;
            _searchAccountIdInitVal = searchAccountId;
        }
        _searchAccountId = searchAccountId;
    }

    public boolean getSearchAccountId() {
        return _searchAccountId;
    }

    public boolean searchAccountIdInitVal() {
        return _searchAccountIdInitVal;
    }

	public boolean searchAccountIdIsDirty() {
        return !valuesAreEqual(_searchAccountIdInitVal, _searchAccountId);
    }

    public boolean searchAccountIdIsSet() {
        return _searchAccountIdIsSet;
    }	


    public void setAccountId(String accountId) {
        if (!_accountIdIsSet) {
            _accountIdIsSet = true;
            _accountIdInitVal = accountId;
        }
        _accountId = accountId;
    }

    public String getAccountId() {
        return _accountId;
    }

    public String accountIdInitVal() {
        return _accountIdInitVal;
    }

	public boolean accountIdIsDirty() {
        return !valuesAreEqual(_accountIdInitVal, _accountId);
    }

    public boolean accountIdIsSet() {
        return _accountIdIsSet;
    }	


    public void setSearchEmail(boolean searchEmail) {
        if (!_searchEmailIsSet) {
            _searchEmailIsSet = true;
            _searchEmailInitVal = searchEmail;
        }
        _searchEmail = searchEmail;
    }

    public boolean getSearchEmail() {
        return _searchEmail;
    }

    public boolean searchEmailInitVal() {
        return _searchEmailInitVal;
    }

	public boolean searchEmailIsDirty() {
        return !valuesAreEqual(_searchEmailInitVal, _searchEmail);
    }

    public boolean searchEmailIsSet() {
        return _searchEmailIsSet;
    }	


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setSearchUsername(boolean searchUsername) {
        if (!_searchUsernameIsSet) {
            _searchUsernameIsSet = true;
            _searchUsernameInitVal = searchUsername;
        }
        _searchUsername = searchUsername;
    }

    public boolean getSearchUsername() {
        return _searchUsername;
    }

    public boolean searchUsernameInitVal() {
        return _searchUsernameInitVal;
    }

	public boolean searchUsernameIsDirty() {
        return !valuesAreEqual(_searchUsernameInitVal, _searchUsername);
    }

    public boolean searchUsernameIsSet() {
        return _searchUsernameIsSet;
    }	


    public void setUsername(String username) {
        if (!_usernameIsSet) {
            _usernameIsSet = true;
            _usernameInitVal = username;
        }
        _username = username;
    }

    public String getUsername() {
        return _username;
    }

    public String usernameInitVal() {
        return _usernameInitVal;
    }

	public boolean usernameIsDirty() {
        return !valuesAreEqual(_usernameInitVal, _username);
    }

    public boolean usernameIsSet() {
        return _usernameIsSet;
    }	


    public void setSearchPersonNames(boolean searchPersonNames) {
        if (!_searchPersonNamesIsSet) {
            _searchPersonNamesIsSet = true;
            _searchPersonNamesInitVal = searchPersonNames;
        }
        _searchPersonNames = searchPersonNames;
    }

    public boolean getSearchPersonNames() {
        return _searchPersonNames;
    }

    public boolean searchPersonNamesInitVal() {
        return _searchPersonNamesInitVal;
    }

	public boolean searchPersonNamesIsDirty() {
        return !valuesAreEqual(_searchPersonNamesInitVal, _searchPersonNames);
    }

    public boolean searchPersonNamesIsSet() {
        return _searchPersonNamesIsSet;
    }	


    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = firstName;
        }
        _firstName = firstName;
    }

    public String getFirstName() {
        return _firstName;
    }

    public String firstNameInitVal() {
        return _firstNameInitVal;
    }

	public boolean firstNameIsDirty() {
        return !valuesAreEqual(_firstNameInitVal, _firstName);
    }

    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }	


    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

	public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }	


    public void setSearchRoles(boolean searchRoles) {
        if (!_searchRolesIsSet) {
            _searchRolesIsSet = true;
            _searchRolesInitVal = searchRoles;
        }
        _searchRoles = searchRoles;
    }

    public boolean getSearchRoles() {
        return _searchRoles;
    }

    public boolean searchRolesInitVal() {
        return _searchRolesInitVal;
    }

	public boolean searchRolesIsDirty() {
        return !valuesAreEqual(_searchRolesInitVal, _searchRoles);
    }

    public boolean searchRolesIsSet() {
        return _searchRolesIsSet;
    }	


    public void setRoles(List<String> roles) {
        if (!_rolesIsSet) {
            _rolesIsSet = true;
            _rolesInitVal = roles;
        }
        _roles = roles;
    }

    public List<String> getRoles() {
        return _roles;
    }

    public List<String> rolesInitVal() {
        return _rolesInitVal;
    }

	public boolean rolesIsDirty() {
        return !valuesAreEqual(_rolesInitVal, _roles);
    }

    public boolean rolesIsSet() {
        return _rolesIsSet;
    }	


    public void setSearchRemindedUsers(boolean searchRemindedUsers) {
        if (!_searchRemindedUsersIsSet) {
            _searchRemindedUsersIsSet = true;
            _searchRemindedUsersInitVal = searchRemindedUsers;
        }
        _searchRemindedUsers = searchRemindedUsers;
    }

    public boolean getSearchRemindedUsers() {
        return _searchRemindedUsers;
    }

    public boolean searchRemindedUsersInitVal() {
        return _searchRemindedUsersInitVal;
    }

	public boolean searchRemindedUsersIsDirty() {
        return !valuesAreEqual(_searchRemindedUsersInitVal, _searchRemindedUsers);
    }

    public boolean searchRemindedUsersIsSet() {
        return _searchRemindedUsersIsSet;
    }	


    public void setSearchRemindedBorrower(boolean searchRemindedBorrower) {
        if (!_searchRemindedBorrowerIsSet) {
            _searchRemindedBorrowerIsSet = true;
            _searchRemindedBorrowerInitVal = searchRemindedBorrower;
        }
        _searchRemindedBorrower = searchRemindedBorrower;
    }

    public boolean getSearchRemindedBorrower() {
        return _searchRemindedBorrower;
    }

    public boolean searchRemindedBorrowerInitVal() {
        return _searchRemindedBorrowerInitVal;
    }

	public boolean searchRemindedBorrowerIsDirty() {
        return !valuesAreEqual(_searchRemindedBorrowerInitVal, _searchRemindedBorrower);
    }

    public boolean searchRemindedBorrowerIsSet() {
        return _searchRemindedBorrowerIsSet;
    }	


    public void setSearchBankAccountNo(boolean searchBankAccountNo) {
        if (!_searchBankAccountNoIsSet) {
            _searchBankAccountNoIsSet = true;
            _searchBankAccountNoInitVal = searchBankAccountNo;
        }
        _searchBankAccountNo = searchBankAccountNo;
    }

    public boolean getSearchBankAccountNo() {
        return _searchBankAccountNo;
    }

    public boolean searchBankAccountNoInitVal() {
        return _searchBankAccountNoInitVal;
    }

	public boolean searchBankAccountNoIsDirty() {
        return !valuesAreEqual(_searchBankAccountNoInitVal, _searchBankAccountNo);
    }

    public boolean searchBankAccountNoIsSet() {
        return _searchBankAccountNoIsSet;
    }	


    public void setSearchIban(boolean searchIban) {
        if (!_searchIbanIsSet) {
            _searchIbanIsSet = true;
            _searchIbanInitVal = searchIban;
        }
        _searchIban = searchIban;
    }

    public boolean getSearchIban() {
        return _searchIban;
    }

    public boolean searchIbanInitVal() {
        return _searchIbanInitVal;
    }

	public boolean searchIbanIsDirty() {
        return !valuesAreEqual(_searchIbanInitVal, _searchIban);
    }

    public boolean searchIbanIsSet() {
        return _searchIbanIsSet;
    }	


    public void setBankAccountNo(String bankAccountNo) {
        if (!_bankAccountNoIsSet) {
            _bankAccountNoIsSet = true;
            _bankAccountNoInitVal = bankAccountNo;
        }
        _bankAccountNo = bankAccountNo;
    }

    public String getBankAccountNo() {
        return _bankAccountNo;
    }

    public String bankAccountNoInitVal() {
        return _bankAccountNoInitVal;
    }

	public boolean bankAccountNoIsDirty() {
        return !valuesAreEqual(_bankAccountNoInitVal, _bankAccountNo);
    }

    public boolean bankAccountNoIsSet() {
        return _bankAccountNoIsSet;
    }	


    public void setPhone(String phone) {
        if (!_phoneIsSet) {
            _phoneIsSet = true;
            _phoneInitVal = phone;
        }
        _phone = phone;
    }

    public String getPhone() {
        return _phone;
    }

    public String phoneInitVal() {
        return _phoneInitVal;
    }

	public boolean phoneIsDirty() {
        return !valuesAreEqual(_phoneInitVal, _phone);
    }

    public boolean phoneIsSet() {
        return _phoneIsSet;
    }	


    public void setBirthDate(Date birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;

            if (birthDate == null) {
                _birthDateInitVal = null;
            } else {
                _birthDateInitVal = (Date) birthDate.clone();
            }
        }

        if (birthDate == null) {
            _birthDate = null;
        } else {
            _birthDate = (Date) birthDate.clone();
        }
    }

    public Date getBirthDate() {
        Date birthDate = null;
        if (_birthDate != null) {
            birthDate = (Date) _birthDate.clone();
        }
        return birthDate;
    }

    public Date birthDateInitVal() {
        Date birthDateInitVal = null;
        if (_birthDateInitVal != null) {
            birthDateInitVal = (Date) _birthDateInitVal.clone();
        }
        return birthDateInitVal;
    }

	public boolean birthDateIsDirty() {
        return !valuesAreEqual(_birthDateInitVal, _birthDate);
    }

    public boolean birthDateIsSet() {
        return _birthDateIsSet;
    }	


    public void setIban(String iban) {
        if (!_ibanIsSet) {
            _ibanIsSet = true;
            _ibanInitVal = iban;
        }
        _iban = iban;
    }

    public String getIban() {
        return _iban;
    }

    public String ibanInitVal() {
        return _ibanInitVal;
    }

	public boolean ibanIsDirty() {
        return !valuesAreEqual(_ibanInitVal, _iban);
    }

    public boolean ibanIsSet() {
        return _ibanIsSet;
    }	

}
