//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.configuration;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo configuration)}
import java.io.Serializable;
import java.util.List;
import java.util.Set;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoConfigurationForm'.
 *
 * @author generator
 */
public class BoConfigurationForm implements Serializable {

    public static final String MARKET_STOPPED_EXPR = "boConfigurationForm.marketStopped";
    public static final String MARKET_STOPPED_PATH = "command.boConfigurationForm.marketStopped";
    public static final String SMAVA_GEBUEHR_EXPR = "boConfigurationForm.smavaGebuehr";
    public static final String SMAVA_GEBUEHR_PATH = "command.boConfigurationForm.smavaGebuehr";
    public static final String DAYS_TO_GO_EXPR = "boConfigurationForm.daysToGo";
    public static final String DAYS_TO_GO_PATH = "command.boConfigurationForm.daysToGo";
    public static final String SHOW_FINANCED_ORDERS_PERIOD_EXPR = "boConfigurationForm.showFinancedOrdersPeriod";
    public static final String SHOW_FINANCED_ORDERS_PERIOD_PATH = "command.boConfigurationForm.showFinancedOrdersPeriod";
    public static final String MAX_ORDER_DURATION_EXPR = "boConfigurationForm.maxOrderDuration";
    public static final String MAX_ORDER_DURATION_PATH = "command.boConfigurationForm.maxOrderDuration";
    public static final String PARTIAL_MATCH_ACCEPTANCE_DURATION_EXPR = "boConfigurationForm.partialMatchAcceptanceDuration";
    public static final String PARTIAL_MATCH_ACCEPTANCE_DURATION_PATH = "command.boConfigurationForm.partialMatchAcceptanceDuration";
    public static final String MAX_OFFERS_PER_LENDER_EXPR = "boConfigurationForm.maxOffersPerLender";
    public static final String MAX_OFFERS_PER_LENDER_PATH = "command.boConfigurationForm.maxOffersPerLender";
    public static final String MIN_AMOUNT_ORDER_EXPR = "boConfigurationForm.minAmountOrder";
    public static final String MIN_AMOUNT_ORDER_PATH = "command.boConfigurationForm.minAmountOrder";
    public static final String MIN_AMOUNT_OFFER_EXPR = "boConfigurationForm.minAmountOffer";
    public static final String MIN_AMOUNT_OFFER_PATH = "command.boConfigurationForm.minAmountOffer";
    public static final String MIN_AMOUNT_PICKING_EXPR = "boConfigurationForm.minAmountPicking";
    public static final String MIN_AMOUNT_PICKING_PATH = "command.boConfigurationForm.minAmountPicking";
    public static final String AMOUNT_STEP_EXPR = "boConfigurationForm.amountStep";
    public static final String AMOUNT_STEP_PATH = "command.boConfigurationForm.amountStep";
    public static final String SCHUFA_AUTOMATIC_SCORE_REQUEST_EXPR = "boConfigurationForm.schufaAutomaticScoreRequest";
    public static final String SCHUFA_AUTOMATIC_SCORE_REQUEST_PATH = "command.boConfigurationForm.schufaAutomaticScoreRequest";
    public static final String SCHUFA_BO_MANUAL_SCORE_REQUEST_EXPR = "boConfigurationForm.schufaBoManualScoreRequest";
    public static final String SCHUFA_BO_MANUAL_SCORE_REQUEST_PATH = "command.boConfigurationForm.schufaBoManualScoreRequest";
    public static final String SCHUFA_CONTRACT_INFORMATION_EXPR = "boConfigurationForm.schufaContractInformation";
    public static final String SCHUFA_CONTRACT_INFORMATION_PATH = "command.boConfigurationForm.schufaContractInformation";
    public static final String SCHUFA_ENCASHMENT_INFORMATION_EXPR = "boConfigurationForm.schufaEncashmentInformation";
    public static final String SCHUFA_ENCASHMENT_INFORMATION_PATH = "command.boConfigurationForm.schufaEncashmentInformation";
    public static final String CHECK_NEGATIVE_SCHUFA_EXPR = "boConfigurationForm.checkNegativeSchufa";
    public static final String CHECK_NEGATIVE_SCHUFA_PATH = "command.boConfigurationForm.checkNegativeSchufa";
    public static final String SCHUFA_CONNECTION_TIMEOUT_EXPR = "boConfigurationForm.schufaConnectionTimeout";
    public static final String SCHUFA_CONNECTION_TIMEOUT_PATH = "command.boConfigurationForm.schufaConnectionTimeout";
    public static final String SCHUFA_READ_TIMEOUT_EXPR = "boConfigurationForm.schufaReadTimeout";
    public static final String SCHUFA_READ_TIMEOUT_PATH = "command.boConfigurationForm.schufaReadTimeout";
    public static final String MAX_SCHUFA_RATING_EXPR = "boConfigurationForm.maxSchufaRating";
    public static final String MAX_SCHUFA_RATING_PATH = "command.boConfigurationForm.maxSchufaRating";
    public static final String MAX_SCHUFA_RATING_FOR_SECOND_PERSON_EXPR = "boConfigurationForm.maxSchufaRatingForSecondPerson";
    public static final String MAX_SCHUFA_RATING_FOR_SECOND_PERSON_PATH = "command.boConfigurationForm.maxSchufaRatingForSecondPerson";
    public static final String DEFAULT_RATING_LETTER_EXPR = "boConfigurationForm.defaultRatingLetter";
    public static final String DEFAULT_RATING_LETTER_PATH = "command.boConfigurationForm.defaultRatingLetter";
    public static final String MAX_OFFER_AMOUNT_EXPR = "boConfigurationForm.maxOfferAmount";
    public static final String MAX_OFFER_AMOUNT_PATH = "command.boConfigurationForm.maxOfferAmount";
    public static final String MAX_ORDER_AMOUNT_EXPR = "boConfigurationForm.maxOrderAmount";
    public static final String MAX_ORDER_AMOUNT_PATH = "command.boConfigurationForm.maxOrderAmount";
    public static final String MAX_BROKERAGE_AMOUNT_EXPR = "boConfigurationForm.maxBrokerageAmount";
    public static final String MAX_BROKERAGE_AMOUNT_PATH = "command.boConfigurationForm.maxBrokerageAmount";
    public static final String RECOMMENDATION_BONUS_BORROWER_EXPR = "boConfigurationForm.recommendationBonusBorrower";
    public static final String RECOMMENDATION_BONUS_BORROWER_PATH = "command.boConfigurationForm.recommendationBonusBorrower";
    public static final String RECOMMENDATION_BONUS_LENDER_EXPR = "boConfigurationForm.recommendationBonusLender";
    public static final String RECOMMENDATION_BONUS_LENDER_PATH = "command.boConfigurationForm.recommendationBonusLender";
    public static final String WELCOME_BONUS_BORROWER_EXPR = "boConfigurationForm.welcomeBonusBorrower";
    public static final String WELCOME_BONUS_BORROWER_PATH = "command.boConfigurationForm.welcomeBonusBorrower";
    public static final String WELCOME_BONUS_LENDER_EXPR = "boConfigurationForm.welcomeBonusLender";
    public static final String WELCOME_BONUS_LENDER_PATH = "command.boConfigurationForm.welcomeBonusLender";
    public static final String ACTIVE_DURATIONS_EXPR = "boConfigurationForm.activeDurations";
    public static final String ACTIVE_DURATIONS_PATH = "command.boConfigurationForm.activeDurations";
    public static final String PDF_ENCODING_EXPR = "boConfigurationForm.pdfEncoding";
    public static final String PDF_ENCODING_PATH = "command.boConfigurationForm.pdfEncoding";
    public static final String MARKETING_COOKIE_DURATION_EXPR = "boConfigurationForm.marketingCookieDuration";
    public static final String MARKETING_COOKIE_DURATION_PATH = "command.boConfigurationForm.marketingCookieDuration";
    public static final String LENDER_REPAYMENT_DELAY_EXPR = "boConfigurationForm.lenderRepaymentDelay";
    public static final String LENDER_REPAYMENT_DELAY_PATH = "command.boConfigurationForm.lenderRepaymentDelay";
    public static final String BROKERAGE_REGISTRATION_TIMEOUT_EXPR = "boConfigurationForm.brokerageRegistrationTimeout";
    public static final String BROKERAGE_REGISTRATION_TIMEOUT_PATH = "command.boConfigurationForm.brokerageRegistrationTimeout";
    public static final String BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFERS_EXPR = "boConfigurationForm.borrowerCreditConsultingDocumentMinOffers";
    public static final String BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFERS_PATH = "command.boConfigurationForm.borrowerCreditConsultingDocumentMinOffers";
    public static final String BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFER_AMOUNT_EXPR = "boConfigurationForm.borrowerCreditConsultingDocumentMinOfferAmount";
    public static final String BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFER_AMOUNT_PATH = "command.boConfigurationForm.borrowerCreditConsultingDocumentMinOfferAmount";
    public static final String BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT_DELAY_EXPR = "boConfigurationForm.borrowerCreditConsultingRegistrationDropoutDelay";
    public static final String BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT_DELAY_PATH = "command.boConfigurationForm.borrowerCreditConsultingRegistrationDropoutDelay";
    public static final String OBJECTION_TERM_EXPR = "boConfigurationForm.objectionTerm";
    public static final String OBJECTION_TERM_PATH = "command.boConfigurationForm.objectionTerm";
    public static final String TRANSFER_TERM_EXPR = "boConfigurationForm.transferTerm";
    public static final String TRANSFER_TERM_PATH = "command.boConfigurationForm.transferTerm";
    public static final String LEAD_TIME_EXPR = "boConfigurationForm.leadTime";
    public static final String LEAD_TIME_PATH = "command.boConfigurationForm.leadTime";
    public static final String VISIBLE_DOCUMENT_TYPES_EXPR = "boConfigurationForm.visibleDocumentTypes";
    public static final String VISIBLE_DOCUMENT_TYPES_PATH = "command.boConfigurationForm.visibleDocumentTypes";
    public static final String DOCUMENT_DOWNLOAD_TIMEOUT_PERIOD_EXPR = "boConfigurationForm.documentDownloadTimeoutPeriod";
    public static final String DOCUMENT_DOWNLOAD_TIMEOUT_PERIOD_PATH = "command.boConfigurationForm.documentDownloadTimeoutPeriod";
    public static final String BANK_DATA_FTP_EXPR = "boConfigurationForm.bankDataFtp";
    public static final String BANK_DATA_FTP_PATH = "command.boConfigurationForm.bankDataFtp";
    public static final String DEFAULT_MARKETING_PLACEMENT_ID_EXPR = "boConfigurationForm.defaultMarketingPlacementId";
    public static final String DEFAULT_MARKETING_PLACEMENT_ID_PATH = "command.boConfigurationForm.defaultMarketingPlacementId";
    public static final String XML_INTERFACE_CAMPAIGN_ID_EXPR = "boConfigurationForm.xmlInterfaceCampaignId";
    public static final String XML_INTERFACE_CAMPAIGN_ID_PATH = "command.boConfigurationForm.xmlInterfaceCampaignId";
    public static final String PLACEMENT_LEAD_VALIDITY_EXPR = "boConfigurationForm.placementLeadValidity";
    public static final String PLACEMENT_LEAD_VALIDITY_PATH = "command.boConfigurationForm.placementLeadValidity";
    public static final String PLACEMENT_SALES_VALIDITY_EXPR = "boConfigurationForm.placementSalesValidity";
    public static final String PLACEMENT_SALES_VALIDITY_PATH = "command.boConfigurationForm.placementSalesValidity";
    public static final String FIRST_DOCUMENTS_REMINDER_TERM_EXPR = "boConfigurationForm.firstDocumentsReminderTerm";
    public static final String FIRST_DOCUMENTS_REMINDER_TERM_PATH = "command.boConfigurationForm.firstDocumentsReminderTerm";
    public static final String SECOND_DOCUMENTS_REMINDER_TERM_EXPR = "boConfigurationForm.secondDocumentsReminderTerm";
    public static final String SECOND_DOCUMENTS_REMINDER_TERM_PATH = "command.boConfigurationForm.secondDocumentsReminderTerm";
    public static final String THIRD_DOCUMENTS_REMINDER_TERM_EXPR = "boConfigurationForm.thirdDocumentsReminderTerm";
    public static final String THIRD_DOCUMENTS_REMINDER_TERM_PATH = "command.boConfigurationForm.thirdDocumentsReminderTerm";
    public static final String FOURTH_DOCUMENTS_REMINDER_TERM_EXPR = "boConfigurationForm.fourthDocumentsReminderTerm";
    public static final String FOURTH_DOCUMENTS_REMINDER_TERM_PATH = "command.boConfigurationForm.fourthDocumentsReminderTerm";
    public static final String FIRST_REMINDER_FINE_EXPR = "boConfigurationForm.firstReminderFine";
    public static final String FIRST_REMINDER_FINE_PATH = "command.boConfigurationForm.firstReminderFine";
    public static final String SECOND_REMINDER_FINE_EXPR = "boConfigurationForm.secondReminderFine";
    public static final String SECOND_REMINDER_FINE_PATH = "command.boConfigurationForm.secondReminderFine";
    public static final String OVERDUE_BASE_RATE_EXPR = "boConfigurationForm.overdueBaseRate";
    public static final String OVERDUE_BASE_RATE_PATH = "command.boConfigurationForm.overdueBaseRate";
    public static final String SEND_LENDER_REMINDER_NOTIFICATION_EXPR = "boConfigurationForm.sendLenderReminderNotification";
    public static final String SEND_LENDER_REMINDER_NOTIFICATION_PATH = "command.boConfigurationForm.sendLenderReminderNotification";
    public static final String SEND_GROUPOWNER_REMINDER_NOTIFICATION_EXPR = "boConfigurationForm.sendGroupownerReminderNotification";
    public static final String SEND_GROUPOWNER_REMINDER_NOTIFICATION_PATH = "command.boConfigurationForm.sendGroupownerReminderNotification";
    public static final String SEND_INVITER_REMINDER_NOTIFICATION_EXPR = "boConfigurationForm.sendInviterReminderNotification";
    public static final String SEND_INVITER_REMINDER_NOTIFICATION_PATH = "command.boConfigurationForm.sendInviterReminderNotification";
    public static final String REMINDER_TERM_EXPR = "boConfigurationForm.reminderTerm";
    public static final String REMINDER_TERM_PATH = "command.boConfigurationForm.reminderTerm";
    public static final String REMINDER_BUFFER_EXPR = "boConfigurationForm.reminderBuffer";
    public static final String REMINDER_BUFFER_PATH = "command.boConfigurationForm.reminderBuffer";
    public static final String MINIMUM_INTEREST_EXPR = "boConfigurationForm.minimumInterest";
    public static final String MINIMUM_INTEREST_PATH = "command.boConfigurationForm.minimumInterest";
    public static final String MAXIMUM_INTEREST_EXPR = "boConfigurationForm.maximumInterest";
    public static final String MAXIMUM_INTEREST_PATH = "command.boConfigurationForm.maximumInterest";
    public static final String MARKET_TICKER_TERM_EXPR = "boConfigurationForm.marketTickerTerm";
    public static final String MARKET_TICKER_TERM_PATH = "command.boConfigurationForm.marketTickerTerm";
    public static final String SMAVA_SELECT_AVERAGE_INTEREST_TERM_EXPR = "boConfigurationForm.smavaSelectAverageInterestTerm";
    public static final String SMAVA_SELECT_AVERAGE_INTEREST_TERM_PATH = "command.boConfigurationForm.smavaSelectAverageInterestTerm";
    public static final String MARKET_TICKER_MINIMUM_CONTRACTS_COUNT_EXPR = "boConfigurationForm.marketTickerMinimumContractsCount";
    public static final String MARKET_TICKER_MINIMUM_CONTRACTS_COUNT_PATH = "command.boConfigurationForm.marketTickerMinimumContractsCount";
    public static final String MARKETING_URL_FILTER_PLACEMENT_ID_EXPR = "boConfigurationForm.marketingUrlFilterPlacementId";
    public static final String MARKETING_URL_FILTER_PLACEMENT_ID_PATH = "command.boConfigurationForm.marketingUrlFilterPlacementId";
    public static final String SMAVA_LOGO_REQUIRED_IN_BACKOFFICE_EXPR = "boConfigurationForm.smavaLogoRequiredInBackoffice";
    public static final String SMAVA_LOGO_REQUIRED_IN_BACKOFFICE_PATH = "command.boConfigurationForm.smavaLogoRequiredInBackoffice";
    public static final String SMAVA_LOGO_LOCATION_EXPR = "boConfigurationForm.smavaLogoLocation";
    public static final String SMAVA_LOGO_LOCATION_PATH = "command.boConfigurationForm.smavaLogoLocation";
    public static final String LAST_AGB_UPDATE_DATE_EXPR = "boConfigurationForm.lastAgbUpdateDate";
    public static final String LAST_AGB_UPDATE_DATE_PATH = "command.boConfigurationForm.lastAgbUpdateDate";
    public static final String LAST_PRIVACY_UPDATE_DATE_EXPR = "boConfigurationForm.lastPrivacyUpdateDate";
    public static final String LAST_PRIVACY_UPDATE_DATE_PATH = "command.boConfigurationForm.lastPrivacyUpdateDate";
    public static final String LAST_SCHUFA_UPDATE_DATE_EXPR = "boConfigurationForm.lastSchufaUpdateDate";
    public static final String LAST_SCHUFA_UPDATE_DATE_PATH = "command.boConfigurationForm.lastSchufaUpdateDate";
    public static final String CALL_SERVICE_PRIMARY_PHONE_NUMBER_EXPR = "boConfigurationForm.callServicePrimaryPhoneNumber";
    public static final String CALL_SERVICE_PRIMARY_PHONE_NUMBER_PATH = "command.boConfigurationForm.callServicePrimaryPhoneNumber";
    public static final String CALL_SERVICE_PRIMARY_PHONE_PRESELECTION_EXPR = "boConfigurationForm.callServicePrimaryPhonePreselection";
    public static final String CALL_SERVICE_PRIMARY_PHONE_PRESELECTION_PATH = "command.boConfigurationForm.callServicePrimaryPhonePreselection";
    public static final String CALL_SERVICE_SECONDARY_PHONE_NUMBER_EXPR = "boConfigurationForm.callServiceSecondaryPhoneNumber";
    public static final String CALL_SERVICE_SECONDARY_PHONE_NUMBER_PATH = "command.boConfigurationForm.callServiceSecondaryPhoneNumber";
    public static final String CALL_SERVICE_SECONDARY_PHONE_PRESELECTION_EXPR = "boConfigurationForm.callServiceSecondaryPhonePreselection";
    public static final String CALL_SERVICE_SECONDARY_PHONE_PRESELECTION_PATH = "command.boConfigurationForm.callServiceSecondaryPhonePreselection";
    public static final String CALL_SERVICE_ENABLED_EXPR = "boConfigurationForm.callServiceEnabled";
    public static final String CALL_SERVICE_ENABLED_PATH = "command.boConfigurationForm.callServiceEnabled";
    public static final String RDI_ACTIVATED_EXPR = "boConfigurationForm.rdiActivated";
    public static final String RDI_ACTIVATED_PATH = "command.boConfigurationForm.rdiActivated";
    public static final String RDI_OBJECTION_TERM_EXPR = "boConfigurationForm.rdiObjectionTerm";
    public static final String RDI_OBJECTION_TERM_PATH = "command.boConfigurationForm.rdiObjectionTerm";
    public static final String MAXIMUM_GROUP_MEMBERSHIPS_EXPR = "boConfigurationForm.maximumGroupMemberships";
    public static final String MAXIMUM_GROUP_MEMBERSHIPS_PATH = "command.boConfigurationForm.maximumGroupMemberships";
    public static final String MARKED_SMAVA_GROUPS_BLOCK_VISIBLE_EXPR = "boConfigurationForm.markedSmavaGroupsBlockVisible";
    public static final String MARKED_SMAVA_GROUPS_BLOCK_VISIBLE_PATH = "command.boConfigurationForm.markedSmavaGroupsBlockVisible";
    public static final String SMAVA_GROUP_MARKING_ALLOWED_EXPR = "boConfigurationForm.smavaGroupMarkingAllowed";
    public static final String SMAVA_GROUP_MARKING_ALLOWED_PATH = "command.boConfigurationForm.smavaGroupMarkingAllowed";
    public static final String SUPPRESS_NOTIFICATIONS_EXPR = "boConfigurationForm.suppressNotifications";
    public static final String SUPPRESS_NOTIFICATIONS_PATH = "command.boConfigurationForm.suppressNotifications";
    public static final String MARKETING_DEFAULT_INTEREST_EXPR = "boConfigurationForm.marketingDefaultInterest";
    public static final String MARKETING_DEFAULT_INTEREST_PATH = "command.boConfigurationForm.marketingDefaultInterest";
    public static final String MARKETING_DEFAULT_INTEREST_FOR_INTEREST_SLIDER_EXPR = "boConfigurationForm.marketingDefaultInterestForInterestSlider";
    public static final String MARKETING_DEFAULT_INTEREST_FOR_INTEREST_SLIDER_PATH = "command.boConfigurationForm.marketingDefaultInterestForInterestSlider";
    public static final String PAYMENT_PROFILE_DATE_EXPR = "boConfigurationForm.paymentProfileDate";
    public static final String PAYMENT_PROFILE_DATE_PATH = "command.boConfigurationForm.paymentProfileDate";
    public static final String ACTIVITY_FEE_START_DATE_EXPR = "boConfigurationForm.activityFeeStartDate";
    public static final String ACTIVITY_FEE_START_DATE_PATH = "command.boConfigurationForm.activityFeeStartDate";
    public static final String POSTIDENT_FEE_START_DATE_EXPR = "boConfigurationForm.postidentFeeStartDate";
    public static final String POSTIDENT_FEE_START_DATE_PATH = "command.boConfigurationForm.postidentFeeStartDate";
    public static final String FAKE_DATE_EXPR = "boConfigurationForm.fakeDate";
    public static final String FAKE_DATE_PATH = "command.boConfigurationForm.fakeDate";
    public static final String USE_FAKE_DATE_EXPR = "boConfigurationForm.useFakeDate";
    public static final String USE_FAKE_DATE_PATH = "command.boConfigurationForm.useFakeDate";
    public static final String PARTIAL_MATCH_ACCEPTANCE_LIMIT_EXPR = "boConfigurationForm.partialMatchAcceptanceLimit";
    public static final String PARTIAL_MATCH_ACCEPTANCE_LIMIT_PATH = "command.boConfigurationForm.partialMatchAcceptanceLimit";
    public static final String BORROWER_DELAYED_PAYMENT_LIMIT_DAYS_EXPR = "boConfigurationForm.borrowerDelayedPaymentLimitDays";
    public static final String BORROWER_DELAYED_PAYMENT_LIMIT_DAYS_PATH = "command.boConfigurationForm.borrowerDelayedPaymentLimitDays";
    public static final String SCHUFA_TOKEN_RATE_COUNT_PERIOD_EXPR = "boConfigurationForm.schufaTokenRateCountPeriod";
    public static final String SCHUFA_TOKEN_RATE_COUNT_PERIOD_PATH = "command.boConfigurationForm.schufaTokenRateCountPeriod";
    public static final String SCHUFA_TOKEN_DEFAULT_HYPO_INTEREST_EXPR = "boConfigurationForm.schufaTokenDefaultHypoInterest";
    public static final String SCHUFA_TOKEN_DEFAULT_HYPO_INTEREST_PATH = "command.boConfigurationForm.schufaTokenDefaultHypoInterest";
    public static final String SCHUFA_TOKEN_DEFAULT_CREDIT_INTEREST_EXPR = "boConfigurationForm.schufaTokenDefaultCreditInterest";
    public static final String SCHUFA_TOKEN_DEFAULT_CREDIT_INTEREST_PATH = "command.boConfigurationForm.schufaTokenDefaultCreditInterest";
    public static final String FIRST_REPAYMENT_REMINDER_TERM_EXPR = "boConfigurationForm.firstRepaymentReminderTerm";
    public static final String FIRST_REPAYMENT_REMINDER_TERM_PATH = "command.boConfigurationForm.firstRepaymentReminderTerm";
    public static final String DELAYED_REPAYMENT_REMINDER_TERM_EXPR = "boConfigurationForm.delayedRepaymentReminderTerm";
    public static final String DELAYED_REPAYMENT_REMINDER_TERM_PATH = "command.boConfigurationForm.delayedRepaymentReminderTerm";
    public static final String MAXIMUM_ACTIVE_DAYS_EXPR = "boConfigurationForm.maximumActiveDays";
    public static final String MAXIMUM_ACTIVE_DAYS_PATH = "command.boConfigurationForm.maximumActiveDays";
    public static final String MINIMUM_FINANCED_RATE_EXPR = "boConfigurationForm.minimumFinancedRate";
    public static final String MINIMUM_FINANCED_RATE_PATH = "command.boConfigurationForm.minimumFinancedRate";
    public static final String SCORE_OBSOLETE_EXPR = "boConfigurationForm.scoreObsolete";
    public static final String SCORE_OBSOLETE_PATH = "command.boConfigurationForm.scoreObsolete";
    public static final String APPROVAL_OBSOLETE_EXPR = "boConfigurationForm.approvalObsolete";
    public static final String APPROVAL_OBSOLETE_PATH = "command.boConfigurationForm.approvalObsolete";
    public static final String USE_DTA_EXPR = "boConfigurationForm.useDta";
    public static final String USE_DTA_PATH = "command.boConfigurationForm.useDta";
    public static final String USE_DTA_FORMAT_EXPR = "boConfigurationForm.useDtaFormat";
    public static final String USE_DTA_FORMAT_PATH = "command.boConfigurationForm.useDtaFormat";
    public static final String PIN_GENERATION_TYPE_EXPR = "boConfigurationForm.pinGenerationType";
    public static final String PIN_GENERATION_TYPE_PATH = "command.boConfigurationForm.pinGenerationType";
    public static final String USE_SCHUFA_EXPR = "boConfigurationForm.useSchufa";
    public static final String USE_SCHUFA_PATH = "command.boConfigurationForm.useSchufa";
    public static final String NEED_BANK_DATA_EXPR = "boConfigurationForm.needBankData";
    public static final String NEED_BANK_DATA_PATH = "command.boConfigurationForm.needBankData";
    public static final String AUTOMATIC_INTERNAL_BANK_ACCOUNT_GENERATION_EXPR = "boConfigurationForm.automaticInternalBankAccountGeneration";
    public static final String AUTOMATIC_INTERNAL_BANK_ACCOUNT_GENERATION_PATH = "command.boConfigurationForm.automaticInternalBankAccountGeneration";
    public static final String ZUS_EXPR = "boConfigurationForm.zus";
    public static final String ZUS_PATH = "command.boConfigurationForm.zus";
    public static final String POOL_COMPENSATION_EXPR = "boConfigurationForm.poolCompensation";
    public static final String POOL_COMPENSATION_PATH = "command.boConfigurationForm.poolCompensation";
    public static final String EXTERNAL_LENDER_PAYOUT_EXPR = "boConfigurationForm.externalLenderPayout";
    public static final String EXTERNAL_LENDER_PAYOUT_PATH = "command.boConfigurationForm.externalLenderPayout";
    public static final String LENDER_TRANSACTION_FEE_DEPOSIT_EXPR = "boConfigurationForm.lenderTransactionFeeDeposit";
    public static final String LENDER_TRANSACTION_FEE_DEPOSIT_PATH = "command.boConfigurationForm.lenderTransactionFeeDeposit";
    public static final String LENDER_TRANSACTION_FEE_PAYOUT_EXPR = "boConfigurationForm.lenderTransactionFeePayout";
    public static final String LENDER_TRANSACTION_FEE_PAYOUT_PATH = "command.boConfigurationForm.lenderTransactionFeePayout";
    public static final String BORROWER_TRANSACTION_FEE_DEPOSIT_EXPR = "boConfigurationForm.borrowerTransactionFeeDeposit";
    public static final String BORROWER_TRANSACTION_FEE_DEPOSIT_PATH = "command.boConfigurationForm.borrowerTransactionFeeDeposit";
    public static final String BORROWER_TRANSACTION_FEE_PAYOUT_EXPR = "boConfigurationForm.borrowerTransactionFeePayout";
    public static final String BORROWER_TRANSACTION_FEE_PAYOUT_PATH = "command.boConfigurationForm.borrowerTransactionFeePayout";
    public static final String TRANSACTION_MAPPING_TYPE_EXPR = "boConfigurationForm.transactionMappingType";
    public static final String TRANSACTION_MAPPING_TYPE_PATH = "command.boConfigurationForm.transactionMappingType";
    public static final String BLACKLIST_FEE_EXPR = "boConfigurationForm.blacklistFee";
    public static final String BLACKLIST_FEE_PATH = "command.boConfigurationForm.blacklistFee";
    public static final String BLACKLIST_CHECK_VALIDITY_EXPR = "boConfigurationForm.blacklistCheckValidity";
    public static final String BLACKLIST_CHECK_VALIDITY_PATH = "command.boConfigurationForm.blacklistCheckValidity";
    public static final String EXCLUDE_INTERNAL_TRANSACTIONS_EXPR = "boConfigurationForm.excludeInternalTransactions";
    public static final String EXCLUDE_INTERNAL_TRANSACTIONS_PATH = "command.boConfigurationForm.excludeInternalTransactions";
    public static final String INCLUDE_SCHUFA_CREDIT_DEFAULT_RATES_EXPR = "boConfigurationForm.includeSchufaCreditDefaultRates";
    public static final String INCLUDE_SCHUFA_CREDIT_DEFAULT_RATES_PATH = "command.boConfigurationForm.includeSchufaCreditDefaultRates";
    public static final String USE_ROI_SIMULATION_EXPR = "boConfigurationForm.useRoiSimulation";
    public static final String USE_ROI_SIMULATION_PATH = "command.boConfigurationForm.useRoiSimulation";
    public static final String EXTERNAL_BORROWER_REPAYMENT_TRANSACTION_TYPE_EXPR = "boConfigurationForm.externalBorrowerRepaymentTransactionType";
    public static final String EXTERNAL_BORROWER_REPAYMENT_TRANSACTION_TYPE_PATH = "command.boConfigurationForm.externalBorrowerRepaymentTransactionType";
    public static final String MINIMUM_WORK_EXPERIENCE_EXPR = "boConfigurationForm.minimumWorkExperience";
    public static final String MINIMUM_WORK_EXPERIENCE_PATH = "command.boConfigurationForm.minimumWorkExperience";
    public static final String DEFAULT_SEO_PLACEMENT_ID_EXPR = "boConfigurationForm.defaultSeoPlacementId";
    public static final String DEFAULT_SEO_PLACEMENT_ID_PATH = "command.boConfigurationForm.defaultSeoPlacementId";
    public static final String DEFAULT_INVITATION_PLACEMENT_ID_EXPR = "boConfigurationForm.defaultInvitationPlacementId";
    public static final String DEFAULT_INVITATION_PLACEMENT_ID_PATH = "command.boConfigurationForm.defaultInvitationPlacementId";
    public static final String SEO_KEYWORDS_TO_EXCLUDE_EXPR = "boConfigurationForm.seoKeywordsToExclude";
    public static final String SEO_KEYWORDS_TO_EXCLUDE_PATH = "command.boConfigurationForm.seoKeywordsToExclude";
    public static final String BASE_AMOUNT_FOR_ROI_CALCULATION_EXPR = "boConfigurationForm.baseAmountForRoiCalculation";
    public static final String BASE_AMOUNT_FOR_ROI_CALCULATION_PATH = "command.boConfigurationForm.baseAmountForRoiCalculation";
    public static final String SHOW_EXTENDED_BID_ASSISTANT_VIEW_EXPR = "boConfigurationForm.showExtendedBidAssistantView";
    public static final String SHOW_EXTENDED_BID_ASSISTANT_VIEW_PATH = "command.boConfigurationForm.showExtendedBidAssistantView";
    public static final String VALIDITY_OF_THE_CREDIT_OFFER_TERM_EXPR = "boConfigurationForm.validityOfTheCreditOfferTerm";
    public static final String VALIDITY_OF_THE_CREDIT_OFFER_TERM_PATH = "command.boConfigurationForm.validityOfTheCreditOfferTerm";
    public static final String DEFAULT_CREDIT_AMOUNT_EXPR = "boConfigurationForm.defaultCreditAmount";
    public static final String DEFAULT_CREDIT_AMOUNT_PATH = "command.boConfigurationForm.defaultCreditAmount";
    public static final String DEFAULT_CREDIT_DURATION_EXPR = "boConfigurationForm.defaultCreditDuration";
    public static final String DEFAULT_CREDIT_DURATION_PATH = "command.boConfigurationForm.defaultCreditDuration";
    public static final String DEFAULT_RDI_CONTRACT_TYPE_EXPR = "boConfigurationForm.defaultRdiContractType";
    public static final String DEFAULT_RDI_CONTRACT_TYPE_PATH = "command.boConfigurationForm.defaultRdiContractType";
    public static final String ACTIVATE_RAPID_MATCHING_EXPR = "boConfigurationForm.activateRapidMatching";
    public static final String ACTIVATE_RAPID_MATCHING_PATH = "command.boConfigurationForm.activateRapidMatching";
    public static final String ACTIVATE_RAPID_MATCHING_FOR_SOAP_EXPR = "boConfigurationForm.activateRapidMatchingForSoap";
    public static final String ACTIVATE_RAPID_MATCHING_FOR_SOAP_PATH = "command.boConfigurationForm.activateRapidMatchingForSoap";
    public static final String MINIMUM_ROI_PORTFOLIO_EXPR = "boConfigurationForm.minimumRoiPortfolio";
    public static final String MINIMUM_ROI_PORTFOLIO_PATH = "command.boConfigurationForm.minimumRoiPortfolio";
    public static final String REPAYMENT_BREAK_ENABLED_EXPR = "boConfigurationForm.repaymentBreakEnabled";
    public static final String REPAYMENT_BREAK_ENABLED_PATH = "command.boConfigurationForm.repaymentBreakEnabled";
    public static final String REPAYMENT_BREAK_FEE_EXPR = "boConfigurationForm.repaymentBreakFee";
    public static final String REPAYMENT_BREAK_FEE_PATH = "command.boConfigurationForm.repaymentBreakFee";
    public static final String ALLOW_SHARED_LOAN_EXPR = "boConfigurationForm.allowSharedLoan";
    public static final String ALLOW_SHARED_LOAN_PATH = "command.boConfigurationForm.allowSharedLoan";
    public static final String ALLOW_DEBT_CONSOLIDATION_EXPR = "boConfigurationForm.allowDebtConsolidation";
    public static final String ALLOW_DEBT_CONSOLIDATION_PATH = "command.boConfigurationForm.allowDebtConsolidation";
    public static final String CREDIT_PROJECT_COUNT_EXPR = "boConfigurationForm.creditProjectCount";
    public static final String CREDIT_PROJECT_COUNT_PATH = "command.boConfigurationForm.creditProjectCount";
    public static final String REFUND_PERIOD_EXPR = "boConfigurationForm.refundPeriod";
    public static final String REFUND_PERIOD_PATH = "command.boConfigurationForm.refundPeriod";
    public static final String PROVISION_REFUND_RATE_EXPR = "boConfigurationForm.provisionRefundRate";
    public static final String PROVISION_REFUND_RATE_PATH = "command.boConfigurationForm.provisionRefundRate";
    public static final String ALLOW_BORROWER_PROMOTION_EXPR = "boConfigurationForm.allowBorrowerPromotion";
    public static final String ALLOW_BORROWER_PROMOTION_PATH = "command.boConfigurationForm.allowBorrowerPromotion";
    public static final String ACCOUNT_MANAGEMENT_LENDER_CHARGE_EXPR = "boConfigurationForm.accountManagementLenderCharge";
    public static final String ACCOUNT_MANAGEMENT_LENDER_CHARGE_PATH = "command.boConfigurationForm.accountManagementLenderCharge";
    public static final String ACCOUNT_MANAGEMENT_BORROWER_CHARGE_EXPR = "boConfigurationForm.accountManagementBorrowerCharge";
    public static final String ACCOUNT_MANAGEMENT_BORROWER_CHARGE_PATH = "command.boConfigurationForm.accountManagementBorrowerCharge";
    public static final String BORROWER_POSTIDENT_FEE_EXPR = "boConfigurationForm.borrowerPostidentFee";
    public static final String BORROWER_POSTIDENT_FEE_PATH = "command.boConfigurationForm.borrowerPostidentFee";
    public static final String RETURN_DEBIT_CHARGE_EXPR = "boConfigurationForm.returnDebitCharge";
    public static final String RETURN_DEBIT_CHARGE_PATH = "command.boConfigurationForm.returnDebitCharge";
    public static final String ALLOW_BANK_VIEW_EXPR = "boConfigurationForm.allowBankView";
    public static final String ALLOW_BANK_VIEW_PATH = "command.boConfigurationForm.allowBankView";
    public static final String BANKVIEW_VALIDITY_PERIOD_EXPR = "boConfigurationForm.bankviewValidityPeriod";
    public static final String BANKVIEW_VALIDITY_PERIOD_PATH = "command.boConfigurationForm.bankviewValidityPeriod";
    public static final String BROKERAGE_DIFFERENCE_TO_CHOSEN_AMOUNT_EXPR = "boConfigurationForm.brokerageDifferenceToChosenAmount";
    public static final String BROKERAGE_DIFFERENCE_TO_CHOSEN_AMOUNT_PATH = "command.boConfigurationForm.brokerageDifferenceToChosenAmount";
    public static final String BROKERAGE_SEND_NO_DOCUMENTS_DAYS_EXPR = "boConfigurationForm.brokerageSendNoDocumentsDays";
    public static final String BROKERAGE_SEND_NO_DOCUMENTS_DAYS_PATH = "command.boConfigurationForm.brokerageSendNoDocumentsDays";
    public static final String BROKERAGE_MAX_PERIOD_FOR_ALTERNATIVE_OFFERS_EXPR = "boConfigurationForm.brokerageMaxPeriodForAlternativeOffers";
    public static final String BROKERAGE_MAX_PERIOD_FOR_ALTERNATIVE_OFFERS_PATH = "command.boConfigurationForm.brokerageMaxPeriodForAlternativeOffers";
    public static final String FIDOR_ACTIVATED_EXPR = "boConfigurationForm.fidorActivated";
    public static final String FIDOR_ACTIVATED_PATH = "command.boConfigurationForm.fidorActivated";
    public static final String FIDOR_DUMMY_EXPR = "boConfigurationForm.fidorDummy";
    public static final String FIDOR_DUMMY_PATH = "command.boConfigurationForm.fidorDummy";
    public static final String CACHE_EXPIRATION_TIME_EXPR = "boConfigurationForm.cacheExpirationTime";
    public static final String CACHE_EXPIRATION_TIME_PATH = "command.boConfigurationForm.cacheExpirationTime";
    public static final String PA_PRO_ACTIVATED_EXPR = "boConfigurationForm.paProActivated";
    public static final String PA_PRO_ACTIVATED_PATH = "command.boConfigurationForm.paProActivated";
    public static final String DEFAULT_REGISTRATION_ROUTE_EXPR = "boConfigurationForm.defaultRegistrationRoute";
    public static final String DEFAULT_REGISTRATION_ROUTE_PATH = "command.boConfigurationForm.defaultRegistrationRoute";
    public static final String REGISTRATION_OFFER_LIST_POLLING_TIMEOUT_EXPR = "boConfigurationForm.registrationOfferListPollingTimeout";
    public static final String REGISTRATION_OFFER_LIST_POLLING_TIMEOUT_PATH = "command.boConfigurationForm.registrationOfferListPollingTimeout";
    public static final String REGISTRATION_BANK_ACCOUNT_VALIDATION_TIMEOUT_EXPR = "boConfigurationForm.registrationBankAccountValidationTimeout";
    public static final String REGISTRATION_BANK_ACCOUNT_VALIDATION_TIMEOUT_PATH = "command.boConfigurationForm.registrationBankAccountValidationTimeout";
    public static final String SHOW_HOTLINE_IMAGE_EXPR = "boConfigurationForm.showHotlineImage";
    public static final String SHOW_HOTLINE_IMAGE_PATH = "command.boConfigurationForm.showHotlineImage";
    public static final String BANKDATA_VALIDATION_ACTIVATED_EXPR = "boConfigurationForm.bankdataValidationActivated";
    public static final String BANKDATA_VALIDATION_ACTIVATED_PATH = "command.boConfigurationForm.bankdataValidationActivated";
    public static final String TRACK_VALIDATION_ERRORS_EXPR = "boConfigurationForm.trackValidationErrors";
    public static final String TRACK_VALIDATION_ERRORS_PATH = "command.boConfigurationForm.trackValidationErrors";
    public static final String FIDOR_MAINTENANCE_WINDOW_EXPR = "boConfigurationForm.fidorMaintenanceWindow";
    public static final String FIDOR_MAINTENANCE_WINDOW_PATH = "command.boConfigurationForm.fidorMaintenanceWindow";
    public static final String GOOGLE_TAG_MANAGER_EXPR = "boConfigurationForm.googleTagManager";
    public static final String GOOGLE_TAG_MANAGER_PATH = "command.boConfigurationForm.googleTagManager";
    public static final String ARVATO_ENABLED_EXPR = "boConfigurationForm.arvatoEnabled";
    public static final String ARVATO_ENABLED_PATH = "command.boConfigurationForm.arvatoEnabled";
    public static final String ARVATO_THRESHOLD_EXPR = "boConfigurationForm.arvatoThreshold";
    public static final String ARVATO_THRESHOLD_PATH = "command.boConfigurationForm.arvatoThreshold";
    public static final String BROKERAGE_EMAIL_TIMEOUT_EXPR = "boConfigurationForm.brokerageEmailTimeout";
    public static final String BROKERAGE_EMAIL_TIMEOUT_PATH = "command.boConfigurationForm.brokerageEmailTimeout";
    public static final String BROKERAGE_EMAIL_2_3_INTEREST_GENERAL_EXPR = "boConfigurationForm.brokerageEmail23InterestGeneral";
    public static final String BROKERAGE_EMAIL_2_3_INTEREST_GENERAL_PATH = "command.boConfigurationForm.brokerageEmail23InterestGeneral";
    public static final String BROKERAGE_EMAIL_2_3_INTEREST_FREELANCER_EXPR = "boConfigurationForm.brokerageEmail23InterestFreelancer";
    public static final String BROKERAGE_EMAIL_2_3_INTEREST_FREELANCER_PATH = "command.boConfigurationForm.brokerageEmail23InterestFreelancer";
    public static final String CREDIT_GUARANTEE_AND_TOP_ACCEPTANCE_CHANCE_TIMEOUT_EXPR = "boConfigurationForm.creditGuaranteeAndTopAcceptanceChanceTimeout";
    public static final String CREDIT_GUARANTEE_AND_TOP_ACCEPTANCE_CHANCE_TIMEOUT_PATH = "command.boConfigurationForm.creditGuaranteeAndTopAcceptanceChanceTimeout";
    public static final String ACTIVATE_CREDIT_GUARANTEE_EXPR = "boConfigurationForm.activateCreditGuarantee";
    public static final String ACTIVATE_CREDIT_GUARANTEE_PATH = "command.boConfigurationForm.activateCreditGuarantee";
    public static final String CREDIT_GUARANTEE_MIN_AMOUNT_EXPR = "boConfigurationForm.creditGuaranteeMinAmount";
    public static final String CREDIT_GUARANTEE_MIN_AMOUNT_PATH = "command.boConfigurationForm.creditGuaranteeMinAmount";
    public static final String CREDIT_GUARANTEE_MAX_AMOUNT_EXPR = "boConfigurationForm.creditGuaranteeMaxAmount";
    public static final String CREDIT_GUARANTEE_MAX_AMOUNT_PATH = "command.boConfigurationForm.creditGuaranteeMaxAmount";
    public static final String CREDIT_GUARANTEE_MIN_DURATION_EXPR = "boConfigurationForm.creditGuaranteeMinDuration";
    public static final String CREDIT_GUARANTEE_MIN_DURATION_PATH = "command.boConfigurationForm.creditGuaranteeMinDuration";
    public static final String CREDIT_GUARANTEE_MAX_DURATION_EXPR = "boConfigurationForm.creditGuaranteeMaxDuration";
    public static final String CREDIT_GUARANTEE_MAX_DURATION_PATH = "command.boConfigurationForm.creditGuaranteeMaxDuration";
    public static final String CREDIT_GUARANTEE_MIN_CHECKMARKS_EXPR = "boConfigurationForm.creditGuaranteeMinCheckmarks";
    public static final String CREDIT_GUARANTEE_MIN_CHECKMARKS_PATH = "command.boConfigurationForm.creditGuaranteeMinCheckmarks";
    public static final String CREDIT_GUARANTEE_EMAIL_TIMEOUT_EXPR = "boConfigurationForm.creditGuaranteeEmailTimeout";
    public static final String CREDIT_GUARANTEE_EMAIL_TIMEOUT_PATH = "command.boConfigurationForm.creditGuaranteeEmailTimeout";
    public static final String CREDIT_GUARANTEE_EXPRESSION_EXPR = "boConfigurationForm.creditGuaranteeExpression";
    public static final String CREDIT_GUARANTEE_EXPRESSION_PATH = "command.boConfigurationForm.creditGuaranteeExpression";
    public static final String ACTIVATE_TOP_ACCEPTANCE_CHANCE_EXPR = "boConfigurationForm.activateTopAcceptanceChance";
    public static final String ACTIVATE_TOP_ACCEPTANCE_CHANCE_PATH = "command.boConfigurationForm.activateTopAcceptanceChance";
    public static final String TOP_ACCEPTANCE_CHANCE_MIN_AMOUNT_EXPR = "boConfigurationForm.topAcceptanceChanceMinAmount";
    public static final String TOP_ACCEPTANCE_CHANCE_MIN_AMOUNT_PATH = "command.boConfigurationForm.topAcceptanceChanceMinAmount";
    public static final String TOP_ACCEPTANCE_CHANCE_MAX_AMOUNT_EXPR = "boConfigurationForm.topAcceptanceChanceMaxAmount";
    public static final String TOP_ACCEPTANCE_CHANCE_MAX_AMOUNT_PATH = "command.boConfigurationForm.topAcceptanceChanceMaxAmount";
    public static final String TOP_ACCEPTANCE_CHANCE_MIN_DURATION_EXPR = "boConfigurationForm.topAcceptanceChanceMinDuration";
    public static final String TOP_ACCEPTANCE_CHANCE_MIN_DURATION_PATH = "command.boConfigurationForm.topAcceptanceChanceMinDuration";
    public static final String TOP_ACCEPTANCE_CHANCE_MAX_DURATION_EXPR = "boConfigurationForm.topAcceptanceChanceMaxDuration";
    public static final String TOP_ACCEPTANCE_CHANCE_MAX_DURATION_PATH = "command.boConfigurationForm.topAcceptanceChanceMaxDuration";
    public static final String TOP_ACCEPTANCE_CHANCE_MIN_CHECKMARKS_EXPR = "boConfigurationForm.topAcceptanceChanceMinCheckmarks";
    public static final String TOP_ACCEPTANCE_CHANCE_MIN_CHECKMARKS_PATH = "command.boConfigurationForm.topAcceptanceChanceMinCheckmarks";
    public static final String VIDEO_IDENT_ENABLED_EXPR = "boConfigurationForm.videoIdentEnabled";
    public static final String VIDEO_IDENT_ENABLED_PATH = "command.boConfigurationForm.videoIdentEnabled";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo configuration)}
	private static final long serialVersionUID = 7224765917990173219L;
	
	 	public static final String BROKERAGE_BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFERS_EXPR = "boConfigurationForm.borrowerCreditConsultingDocumentMinOffers";
	    public static final String BROKERAGE_BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFERS_PATH = "command.boConfigurationForm.borrowerCreditConsultingDocumentMinOffers";
	    public static final String BROKERAGE_BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFER_AMOUNT_EXPR = "boConfigurationForm.borrowerCreditConsultingDocumentMinOfferAmount";
	    public static final String BROKERAGE_BORROWER_CREDIT_CONSULTING_DOCUMENT_MIN_OFFER_AMOUNT_PATH = "command.boConfigurationForm.borrowerCreditConsultingDocumentMinOfferAmount";

	    public static final String MAX_AGE_OF_FIRST_PERSON_EXPR = "boConfigurationForm.maxAgeOfFirstPerson";
	    public static final String MAX_AGE_OF_FIRST_PERSON_PATH = "command.boConfigurationForm.maxAgeOfFirstPerson";


	    private Integer _maxAgeFirstPerson;
	    private Integer _maxAgeFirstPersonInitVal;
	    private boolean _maxAgeFirstPersonIsSet;


	    public void setMaxAgeOfFirstPerson(Integer mafp) {
	        if (!_maxAgeFirstPersonIsSet) {
	        	_maxAgeFirstPersonIsSet = true;
	        	_maxAgeFirstPersonInitVal = mafp;
	        }
	        _maxAgeFirstPerson = mafp;
	    }

	    public Integer getMaxAgeOfFirstPerson() {
	        return _maxAgeFirstPerson;
	    }

		public boolean maxAgeOfFirstPersonIsDirty() {
	        return !valuesAreEqual(_maxAgeFirstPersonInitVal, _maxAgeFirstPerson);
	    }
		
// !!!!!!!! End of insert code section !!!!!!!!

    private Boolean _marketStopped;
    private Boolean _marketStoppedInitVal;
    private boolean _marketStoppedIsSet;
    private Float _smavaGebuehr;
    private Float _smavaGebuehrInitVal;
    private boolean _smavaGebuehrIsSet;
    private Integer _daysToGo;
    private Integer _daysToGoInitVal;
    private boolean _daysToGoIsSet;
    private Integer _showFinancedOrdersPeriod;
    private Integer _showFinancedOrdersPeriodInitVal;
    private boolean _showFinancedOrdersPeriodIsSet;
    private Integer _maxOrderDuration;
    private Integer _maxOrderDurationInitVal;
    private boolean _maxOrderDurationIsSet;
    private Integer _partialMatchAcceptanceDuration;
    private Integer _partialMatchAcceptanceDurationInitVal;
    private boolean _partialMatchAcceptanceDurationIsSet;
    private Integer _maxOffersPerLender;
    private Integer _maxOffersPerLenderInitVal;
    private boolean _maxOffersPerLenderIsSet;
    private Double _minAmountOrder;
    private Double _minAmountOrderInitVal;
    private boolean _minAmountOrderIsSet;
    private Double _minAmountOffer;
    private Double _minAmountOfferInitVal;
    private boolean _minAmountOfferIsSet;
    private Double _minAmountPicking;
    private Double _minAmountPickingInitVal;
    private boolean _minAmountPickingIsSet;
    private Double _amountStep;
    private Double _amountStepInitVal;
    private boolean _amountStepIsSet;
    private Boolean _schufaAutomaticScoreRequest;
    private Boolean _schufaAutomaticScoreRequestInitVal;
    private boolean _schufaAutomaticScoreRequestIsSet;
    private Boolean _schufaBoManualScoreRequest;
    private Boolean _schufaBoManualScoreRequestInitVal;
    private boolean _schufaBoManualScoreRequestIsSet;
    private Boolean _schufaContractInformation;
    private Boolean _schufaContractInformationInitVal;
    private boolean _schufaContractInformationIsSet;
    private Boolean _schufaEncashmentInformation;
    private Boolean _schufaEncashmentInformationInitVal;
    private boolean _schufaEncashmentInformationIsSet;
    private Boolean _checkNegativeSchufa;
    private Boolean _checkNegativeSchufaInitVal;
    private boolean _checkNegativeSchufaIsSet;
    private Integer _schufaConnectionTimeout;
    private Integer _schufaConnectionTimeoutInitVal;
    private boolean _schufaConnectionTimeoutIsSet;
    private Integer _schufaReadTimeout;
    private Integer _schufaReadTimeoutInitVal;
    private boolean _schufaReadTimeoutIsSet;
    private String _maxSchufaRating;
    private String _maxSchufaRatingInitVal;
    private boolean _maxSchufaRatingIsSet;
    private String _maxSchufaRatingForSecondPerson;
    private String _maxSchufaRatingForSecondPersonInitVal;
    private boolean _maxSchufaRatingForSecondPersonIsSet;
    private String _defaultRatingLetter;
    private String _defaultRatingLetterInitVal;
    private boolean _defaultRatingLetterIsSet;
    private Double _maxOfferAmount;
    private Double _maxOfferAmountInitVal;
    private boolean _maxOfferAmountIsSet;
    private Double _maxOrderAmount;
    private Double _maxOrderAmountInitVal;
    private boolean _maxOrderAmountIsSet;
    private Double _maxBrokerageAmount;
    private Double _maxBrokerageAmountInitVal;
    private boolean _maxBrokerageAmountIsSet;
    private Double _recommendationBonusBorrower;
    private Double _recommendationBonusBorrowerInitVal;
    private boolean _recommendationBonusBorrowerIsSet;
    private Double _recommendationBonusLender;
    private Double _recommendationBonusLenderInitVal;
    private boolean _recommendationBonusLenderIsSet;
    private Double _welcomeBonusBorrower;
    private Double _welcomeBonusBorrowerInitVal;
    private boolean _welcomeBonusBorrowerIsSet;
    private Double _welcomeBonusLender;
    private Double _welcomeBonusLenderInitVal;
    private boolean _welcomeBonusLenderIsSet;
    private List<String> _activeDurations;
    private List<String> _activeDurationsInitVal;
    private boolean _activeDurationsIsSet;
    private String _pdfEncoding;
    private String _pdfEncodingInitVal;
    private boolean _pdfEncodingIsSet;
    private Integer _marketingCookieDuration;
    private Integer _marketingCookieDurationInitVal;
    private boolean _marketingCookieDurationIsSet;
    private Integer _lenderRepaymentDelay;
    private Integer _lenderRepaymentDelayInitVal;
    private boolean _lenderRepaymentDelayIsSet;
    private Long _brokerageRegistrationTimeout;
    private Long _brokerageRegistrationTimeoutInitVal;
    private boolean _brokerageRegistrationTimeoutIsSet;
    private Long _borrowerCreditConsultingDocumentMinOffers;
    private Long _borrowerCreditConsultingDocumentMinOffersInitVal;
    private boolean _borrowerCreditConsultingDocumentMinOffersIsSet;
    private Long _borrowerCreditConsultingDocumentMinOfferAmount;
    private Long _borrowerCreditConsultingDocumentMinOfferAmountInitVal;
    private boolean _borrowerCreditConsultingDocumentMinOfferAmountIsSet;
    private Integer _borrowerCreditConsultingRegistrationDropoutDelay;
    private Integer _borrowerCreditConsultingRegistrationDropoutDelayInitVal;
    private boolean _borrowerCreditConsultingRegistrationDropoutDelayIsSet;
    private Integer _objectionTerm;
    private Integer _objectionTermInitVal;
    private boolean _objectionTermIsSet;
    private Integer _transferTerm;
    private Integer _transferTermInitVal;
    private boolean _transferTermIsSet;
    private Integer _leadTime;
    private Integer _leadTimeInitVal;
    private boolean _leadTimeIsSet;
    private Set<String> _visibleDocumentTypes;
    private Set<String> _visibleDocumentTypesInitVal;
    private boolean _visibleDocumentTypesIsSet;
    private Integer _documentDownloadTimeoutPeriod;
    private Integer _documentDownloadTimeoutPeriodInitVal;
    private boolean _documentDownloadTimeoutPeriodIsSet;
    private Boolean _bankDataFtp;
    private Boolean _bankDataFtpInitVal;
    private boolean _bankDataFtpIsSet;
    private Long _defaultMarketingPlacementId;
    private Long _defaultMarketingPlacementIdInitVal;
    private boolean _defaultMarketingPlacementIdIsSet;
    private Long _xmlInterfaceCampaignId;
    private Long _xmlInterfaceCampaignIdInitVal;
    private boolean _xmlInterfaceCampaignIdIsSet;
    private Integer _placementLeadValidity;
    private Integer _placementLeadValidityInitVal;
    private boolean _placementLeadValidityIsSet;
    private Integer _placementSalesValidity;
    private Integer _placementSalesValidityInitVal;
    private boolean _placementSalesValidityIsSet;
    private Integer _firstDocumentsReminderTerm;
    private Integer _firstDocumentsReminderTermInitVal;
    private boolean _firstDocumentsReminderTermIsSet;
    private Integer _secondDocumentsReminderTerm;
    private Integer _secondDocumentsReminderTermInitVal;
    private boolean _secondDocumentsReminderTermIsSet;
    private Integer _thirdDocumentsReminderTerm;
    private Integer _thirdDocumentsReminderTermInitVal;
    private boolean _thirdDocumentsReminderTermIsSet;
    private Integer _fourthDocumentsReminderTerm;
    private Integer _fourthDocumentsReminderTermInitVal;
    private boolean _fourthDocumentsReminderTermIsSet;
    private Double _firstReminderFine;
    private Double _firstReminderFineInitVal;
    private boolean _firstReminderFineIsSet;
    private Double _secondReminderFine;
    private Double _secondReminderFineInitVal;
    private boolean _secondReminderFineIsSet;
    private Double _overdueBaseRate;
    private Double _overdueBaseRateInitVal;
    private boolean _overdueBaseRateIsSet;
    private Boolean _sendLenderReminderNotification;
    private Boolean _sendLenderReminderNotificationInitVal;
    private boolean _sendLenderReminderNotificationIsSet;
    private Boolean _sendGroupownerReminderNotification;
    private Boolean _sendGroupownerReminderNotificationInitVal;
    private boolean _sendGroupownerReminderNotificationIsSet;
    private Boolean _sendInviterReminderNotification;
    private Boolean _sendInviterReminderNotificationInitVal;
    private boolean _sendInviterReminderNotificationIsSet;
    private Integer _reminderTerm;
    private Integer _reminderTermInitVal;
    private boolean _reminderTermIsSet;
    private Integer _reminderBuffer;
    private Integer _reminderBufferInitVal;
    private boolean _reminderBufferIsSet;
    private Double _minimumInterest;
    private Double _minimumInterestInitVal;
    private boolean _minimumInterestIsSet;
    private Double _maximumInterest;
    private Double _maximumInterestInitVal;
    private boolean _maximumInterestIsSet;
    private Integer _marketTickerTerm;
    private Integer _marketTickerTermInitVal;
    private boolean _marketTickerTermIsSet;
    private Integer _smavaSelectAverageInterestTerm;
    private Integer _smavaSelectAverageInterestTermInitVal;
    private boolean _smavaSelectAverageInterestTermIsSet;
    private Integer _marketTickerMinimumContractsCount;
    private Integer _marketTickerMinimumContractsCountInitVal;
    private boolean _marketTickerMinimumContractsCountIsSet;
    private Long _marketingUrlFilterPlacementId;
    private Long _marketingUrlFilterPlacementIdInitVal;
    private boolean _marketingUrlFilterPlacementIdIsSet;
    private Boolean _smavaLogoRequiredInBackoffice;
    private Boolean _smavaLogoRequiredInBackofficeInitVal;
    private boolean _smavaLogoRequiredInBackofficeIsSet;
    private String _smavaLogoLocation;
    private String _smavaLogoLocationInitVal;
    private boolean _smavaLogoLocationIsSet;
    private String _lastAgbUpdateDate;
    private String _lastAgbUpdateDateInitVal;
    private boolean _lastAgbUpdateDateIsSet;
    private String _lastPrivacyUpdateDate;
    private String _lastPrivacyUpdateDateInitVal;
    private boolean _lastPrivacyUpdateDateIsSet;
    private String _lastSchufaUpdateDate;
    private String _lastSchufaUpdateDateInitVal;
    private boolean _lastSchufaUpdateDateIsSet;
    private String _callServicePrimaryPhoneNumber;
    private String _callServicePrimaryPhoneNumberInitVal;
    private boolean _callServicePrimaryPhoneNumberIsSet;
    private String _callServicePrimaryPhonePreselection;
    private String _callServicePrimaryPhonePreselectionInitVal;
    private boolean _callServicePrimaryPhonePreselectionIsSet;
    private String _callServiceSecondaryPhoneNumber;
    private String _callServiceSecondaryPhoneNumberInitVal;
    private boolean _callServiceSecondaryPhoneNumberIsSet;
    private String _callServiceSecondaryPhonePreselection;
    private String _callServiceSecondaryPhonePreselectionInitVal;
    private boolean _callServiceSecondaryPhonePreselectionIsSet;
    private boolean _callServiceEnabled;
    private boolean _callServiceEnabledInitVal;
    private boolean _callServiceEnabledIsSet;
    private boolean _rdiActivated;
    private boolean _rdiActivatedInitVal;
    private boolean _rdiActivatedIsSet;
    private Integer _rdiObjectionTerm;
    private Integer _rdiObjectionTermInitVal;
    private boolean _rdiObjectionTermIsSet;
    private Integer _maximumGroupMemberships;
    private Integer _maximumGroupMembershipsInitVal;
    private boolean _maximumGroupMembershipsIsSet;
    private boolean _markedSmavaGroupsBlockVisible;
    private boolean _markedSmavaGroupsBlockVisibleInitVal;
    private boolean _markedSmavaGroupsBlockVisibleIsSet;
    private boolean _smavaGroupMarkingAllowed;
    private boolean _smavaGroupMarkingAllowedInitVal;
    private boolean _smavaGroupMarkingAllowedIsSet;
    private boolean _suppressNotifications;
    private boolean _suppressNotificationsInitVal;
    private boolean _suppressNotificationsIsSet;
    private Double _marketingDefaultInterest;
    private Double _marketingDefaultInterestInitVal;
    private boolean _marketingDefaultInterestIsSet;
    private Boolean _marketingDefaultInterestForInterestSlider;
    private Boolean _marketingDefaultInterestForInterestSliderInitVal;
    private boolean _marketingDefaultInterestForInterestSliderIsSet;
    private String _paymentProfileDate;
    private String _paymentProfileDateInitVal;
    private boolean _paymentProfileDateIsSet;
    private String _activityFeeStartDate;
    private String _activityFeeStartDateInitVal;
    private boolean _activityFeeStartDateIsSet;
    private String _postidentFeeStartDate;
    private String _postidentFeeStartDateInitVal;
    private boolean _postidentFeeStartDateIsSet;
    private String _fakeDate;
    private String _fakeDateInitVal;
    private boolean _fakeDateIsSet;
    private boolean _useFakeDate;
    private boolean _useFakeDateInitVal;
    private boolean _useFakeDateIsSet;
    private String _partialMatchAcceptanceLimit;
    private String _partialMatchAcceptanceLimitInitVal;
    private boolean _partialMatchAcceptanceLimitIsSet;
    private Integer _borrowerDelayedPaymentLimitDays;
    private Integer _borrowerDelayedPaymentLimitDaysInitVal;
    private boolean _borrowerDelayedPaymentLimitDaysIsSet;
    private Integer _schufaTokenRateCountPeriod;
    private Integer _schufaTokenRateCountPeriodInitVal;
    private boolean _schufaTokenRateCountPeriodIsSet;
    private String _schufaTokenDefaultHypoInterest;
    private String _schufaTokenDefaultHypoInterestInitVal;
    private boolean _schufaTokenDefaultHypoInterestIsSet;
    private String _schufaTokenDefaultCreditInterest;
    private String _schufaTokenDefaultCreditInterestInitVal;
    private boolean _schufaTokenDefaultCreditInterestIsSet;
    private Integer _firstRepaymentReminderTerm;
    private Integer _firstRepaymentReminderTermInitVal;
    private boolean _firstRepaymentReminderTermIsSet;
    private Integer _delayedRepaymentReminderTerm;
    private Integer _delayedRepaymentReminderTermInitVal;
    private boolean _delayedRepaymentReminderTermIsSet;
    private Integer _maximumActiveDays;
    private Integer _maximumActiveDaysInitVal;
    private boolean _maximumActiveDaysIsSet;
    private Float _minimumFinancedRate;
    private Float _minimumFinancedRateInitVal;
    private boolean _minimumFinancedRateIsSet;
    private Integer _scoreObsolete;
    private Integer _scoreObsoleteInitVal;
    private boolean _scoreObsoleteIsSet;
    private Integer _approvalObsolete;
    private Integer _approvalObsoleteInitVal;
    private boolean _approvalObsoleteIsSet;
    private Boolean _useDta;
    private Boolean _useDtaInitVal;
    private boolean _useDtaIsSet;
    private Boolean _useDtaFormat;
    private Boolean _useDtaFormatInitVal;
    private boolean _useDtaFormatIsSet;
    private Boolean _pinGenerationType;
    private Boolean _pinGenerationTypeInitVal;
    private boolean _pinGenerationTypeIsSet;
    private Boolean _useSchufa;
    private Boolean _useSchufaInitVal;
    private boolean _useSchufaIsSet;
    private Boolean _needBankData;
    private Boolean _needBankDataInitVal;
    private boolean _needBankDataIsSet;
    private Boolean _automaticInternalBankAccountGeneration;
    private Boolean _automaticInternalBankAccountGenerationInitVal;
    private boolean _automaticInternalBankAccountGenerationIsSet;
    private double _zus;
    private double _zusInitVal;
    private boolean _zusIsSet;
    private Boolean _poolCompensation;
    private Boolean _poolCompensationInitVal;
    private boolean _poolCompensationIsSet;
    private Boolean _externalLenderPayout;
    private Boolean _externalLenderPayoutInitVal;
    private boolean _externalLenderPayoutIsSet;
    private double _lenderTransactionFeeDeposit;
    private double _lenderTransactionFeeDepositInitVal;
    private boolean _lenderTransactionFeeDepositIsSet;
    private double _lenderTransactionFeePayout;
    private double _lenderTransactionFeePayoutInitVal;
    private boolean _lenderTransactionFeePayoutIsSet;
    private double _borrowerTransactionFeeDeposit;
    private double _borrowerTransactionFeeDepositInitVal;
    private boolean _borrowerTransactionFeeDepositIsSet;
    private double _borrowerTransactionFeePayout;
    private double _borrowerTransactionFeePayoutInitVal;
    private boolean _borrowerTransactionFeePayoutIsSet;
    private String _transactionMappingType;
    private String _transactionMappingTypeInitVal;
    private boolean _transactionMappingTypeIsSet;
    private double _blacklistFee;
    private double _blacklistFeeInitVal;
    private boolean _blacklistFeeIsSet;
    private Integer _blacklistCheckValidity;
    private Integer _blacklistCheckValidityInitVal;
    private boolean _blacklistCheckValidityIsSet;
    private Boolean _excludeInternalTransactions;
    private Boolean _excludeInternalTransactionsInitVal;
    private boolean _excludeInternalTransactionsIsSet;
    private Boolean _includeSchufaCreditDefaultRates;
    private Boolean _includeSchufaCreditDefaultRatesInitVal;
    private boolean _includeSchufaCreditDefaultRatesIsSet;
    private Boolean _useRoiSimulation;
    private Boolean _useRoiSimulationInitVal;
    private boolean _useRoiSimulationIsSet;
    private String _externalBorrowerRepaymentTransactionType;
    private String _externalBorrowerRepaymentTransactionTypeInitVal;
    private boolean _externalBorrowerRepaymentTransactionTypeIsSet;
    private Integer _minimumWorkExperience;
    private Integer _minimumWorkExperienceInitVal;
    private boolean _minimumWorkExperienceIsSet;
    private Long _defaultSeoPlacementId;
    private Long _defaultSeoPlacementIdInitVal;
    private boolean _defaultSeoPlacementIdIsSet;
    private Long _defaultInvitationPlacementId;
    private Long _defaultInvitationPlacementIdInitVal;
    private boolean _defaultInvitationPlacementIdIsSet;
    private String _seoKeywordsToExclude;
    private String _seoKeywordsToExcludeInitVal;
    private boolean _seoKeywordsToExcludeIsSet;
    private Double _baseAmountForRoiCalculation;
    private Double _baseAmountForRoiCalculationInitVal;
    private boolean _baseAmountForRoiCalculationIsSet;
    private boolean _showExtendedBidAssistantView;
    private boolean _showExtendedBidAssistantViewInitVal;
    private boolean _showExtendedBidAssistantViewIsSet;
    private Integer _validityOfTheCreditOfferTerm;
    private Integer _validityOfTheCreditOfferTermInitVal;
    private boolean _validityOfTheCreditOfferTermIsSet;
    private double _defaultCreditAmount;
    private double _defaultCreditAmountInitVal;
    private boolean _defaultCreditAmountIsSet;
    private Integer _defaultCreditDuration;
    private Integer _defaultCreditDurationInitVal;
    private boolean _defaultCreditDurationIsSet;
    private String _defaultRdiContractType;
    private String _defaultRdiContractTypeInitVal;
    private boolean _defaultRdiContractTypeIsSet;
    private Boolean _activateRapidMatching;
    private Boolean _activateRapidMatchingInitVal;
    private boolean _activateRapidMatchingIsSet;
    private boolean _activateRapidMatchingForSoap;
    private boolean _activateRapidMatchingForSoapInitVal;
    private boolean _activateRapidMatchingForSoapIsSet;
    private String _minimumRoiPortfolio;
    private String _minimumRoiPortfolioInitVal;
    private boolean _minimumRoiPortfolioIsSet;
    private Boolean _repaymentBreakEnabled;
    private Boolean _repaymentBreakEnabledInitVal;
    private boolean _repaymentBreakEnabledIsSet;
    private Double _repaymentBreakFee;
    private Double _repaymentBreakFeeInitVal;
    private boolean _repaymentBreakFeeIsSet;
    private Boolean _allowSharedLoan;
    private Boolean _allowSharedLoanInitVal;
    private boolean _allowSharedLoanIsSet;
    private Boolean _allowDebtConsolidation;
    private Boolean _allowDebtConsolidationInitVal;
    private boolean _allowDebtConsolidationIsSet;
    private Integer _creditProjectCount;
    private Integer _creditProjectCountInitVal;
    private boolean _creditProjectCountIsSet;
    private Integer _refundPeriod;
    private Integer _refundPeriodInitVal;
    private boolean _refundPeriodIsSet;
    private Double _provisionRefundRate;
    private Double _provisionRefundRateInitVal;
    private boolean _provisionRefundRateIsSet;
    private Boolean _allowBorrowerPromotion;
    private Boolean _allowBorrowerPromotionInitVal;
    private boolean _allowBorrowerPromotionIsSet;
    private Double _accountManagementLenderCharge;
    private Double _accountManagementLenderChargeInitVal;
    private boolean _accountManagementLenderChargeIsSet;
    private Double _accountManagementBorrowerCharge;
    private Double _accountManagementBorrowerChargeInitVal;
    private boolean _accountManagementBorrowerChargeIsSet;
    private double _borrowerPostidentFee;
    private double _borrowerPostidentFeeInitVal;
    private boolean _borrowerPostidentFeeIsSet;
    private double _returnDebitCharge;
    private double _returnDebitChargeInitVal;
    private boolean _returnDebitChargeIsSet;
    private Boolean _allowBankView;
    private Boolean _allowBankViewInitVal;
    private boolean _allowBankViewIsSet;
    private Integer _bankviewValidityPeriod;
    private Integer _bankviewValidityPeriodInitVal;
    private boolean _bankviewValidityPeriodIsSet;
    private double _brokerageDifferenceToChosenAmount;
    private double _brokerageDifferenceToChosenAmountInitVal;
    private boolean _brokerageDifferenceToChosenAmountIsSet;
    private Integer _brokerageSendNoDocumentsDays;
    private Integer _brokerageSendNoDocumentsDaysInitVal;
    private boolean _brokerageSendNoDocumentsDaysIsSet;
    private Integer _brokerageMaxPeriodForAlternativeOffers;
    private Integer _brokerageMaxPeriodForAlternativeOffersInitVal;
    private boolean _brokerageMaxPeriodForAlternativeOffersIsSet;
    private Boolean _fidorActivated;
    private Boolean _fidorActivatedInitVal;
    private boolean _fidorActivatedIsSet;
    private Boolean _fidorDummy;
    private Boolean _fidorDummyInitVal;
    private boolean _fidorDummyIsSet;
    private Integer _cacheExpirationTime;
    private Integer _cacheExpirationTimeInitVal;
    private boolean _cacheExpirationTimeIsSet;
    private Boolean _paProActivated;
    private Boolean _paProActivatedInitVal;
    private boolean _paProActivatedIsSet;
    private String _defaultRegistrationRoute;
    private String _defaultRegistrationRouteInitVal;
    private boolean _defaultRegistrationRouteIsSet;
    private Integer _registrationOfferListPollingTimeout;
    private Integer _registrationOfferListPollingTimeoutInitVal;
    private boolean _registrationOfferListPollingTimeoutIsSet;
    private Integer _registrationBankAccountValidationTimeout;
    private Integer _registrationBankAccountValidationTimeoutInitVal;
    private boolean _registrationBankAccountValidationTimeoutIsSet;
    private Boolean _showHotlineImage;
    private Boolean _showHotlineImageInitVal;
    private boolean _showHotlineImageIsSet;
    private Boolean _bankdataValidationActivated;
    private Boolean _bankdataValidationActivatedInitVal;
    private boolean _bankdataValidationActivatedIsSet;
    private Boolean _trackValidationErrors;
    private Boolean _trackValidationErrorsInitVal;
    private boolean _trackValidationErrorsIsSet;
    private Boolean _fidorMaintenanceWindow;
    private Boolean _fidorMaintenanceWindowInitVal;
    private boolean _fidorMaintenanceWindowIsSet;
    private Boolean _googleTagManager;
    private Boolean _googleTagManagerInitVal;
    private boolean _googleTagManagerIsSet;
    private Boolean _arvatoEnabled;
    private Boolean _arvatoEnabledInitVal;
    private boolean _arvatoEnabledIsSet;
    private Integer _arvatoThreshold;
    private Integer _arvatoThresholdInitVal;
    private boolean _arvatoThresholdIsSet;
    private Integer _brokerageEmailTimeout;
    private Integer _brokerageEmailTimeoutInitVal;
    private boolean _brokerageEmailTimeoutIsSet;
    private Double _brokerageEmail23InterestGeneral;
    private Double _brokerageEmail23InterestGeneralInitVal;
    private boolean _brokerageEmail23InterestGeneralIsSet;
    private Double _brokerageEmail23InterestFreelancer;
    private Double _brokerageEmail23InterestFreelancerInitVal;
    private boolean _brokerageEmail23InterestFreelancerIsSet;
    private Integer _creditGuaranteeAndTopAcceptanceChanceTimeout;
    private Integer _creditGuaranteeAndTopAcceptanceChanceTimeoutInitVal;
    private boolean _creditGuaranteeAndTopAcceptanceChanceTimeoutIsSet;
    private Boolean _activateCreditGuarantee;
    private Boolean _activateCreditGuaranteeInitVal;
    private boolean _activateCreditGuaranteeIsSet;
    private Long _creditGuaranteeMinAmount;
    private Long _creditGuaranteeMinAmountInitVal;
    private boolean _creditGuaranteeMinAmountIsSet;
    private Long _creditGuaranteeMaxAmount;
    private Long _creditGuaranteeMaxAmountInitVal;
    private boolean _creditGuaranteeMaxAmountIsSet;
    private Integer _creditGuaranteeMinDuration;
    private Integer _creditGuaranteeMinDurationInitVal;
    private boolean _creditGuaranteeMinDurationIsSet;
    private Integer _creditGuaranteeMaxDuration;
    private Integer _creditGuaranteeMaxDurationInitVal;
    private boolean _creditGuaranteeMaxDurationIsSet;
    private Integer _creditGuaranteeMinCheckmarks;
    private Integer _creditGuaranteeMinCheckmarksInitVal;
    private boolean _creditGuaranteeMinCheckmarksIsSet;
    private Integer _creditGuaranteeEmailTimeout;
    private Integer _creditGuaranteeEmailTimeoutInitVal;
    private boolean _creditGuaranteeEmailTimeoutIsSet;
    private String _creditGuaranteeExpression;
    private String _creditGuaranteeExpressionInitVal;
    private boolean _creditGuaranteeExpressionIsSet;
    private Boolean _activateTopAcceptanceChance;
    private Boolean _activateTopAcceptanceChanceInitVal;
    private boolean _activateTopAcceptanceChanceIsSet;
    private Long _topAcceptanceChanceMinAmount;
    private Long _topAcceptanceChanceMinAmountInitVal;
    private boolean _topAcceptanceChanceMinAmountIsSet;
    private Long _topAcceptanceChanceMaxAmount;
    private Long _topAcceptanceChanceMaxAmountInitVal;
    private boolean _topAcceptanceChanceMaxAmountIsSet;
    private Integer _topAcceptanceChanceMinDuration;
    private Integer _topAcceptanceChanceMinDurationInitVal;
    private boolean _topAcceptanceChanceMinDurationIsSet;
    private Integer _topAcceptanceChanceMaxDuration;
    private Integer _topAcceptanceChanceMaxDurationInitVal;
    private boolean _topAcceptanceChanceMaxDurationIsSet;
    private Integer _topAcceptanceChanceMinCheckmarks;
    private Integer _topAcceptanceChanceMinCheckmarksInitVal;
    private boolean _topAcceptanceChanceMinCheckmarksIsSet;
    private Boolean _videoIdentEnabled;
    private Boolean _videoIdentEnabledInitVal;
    private boolean _videoIdentEnabledIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setMarketStopped(Boolean marketStopped) {
        if (!_marketStoppedIsSet) {
            _marketStoppedIsSet = true;
            _marketStoppedInitVal = marketStopped;
        }
        _marketStopped = marketStopped;
    }

    public Boolean getMarketStopped() {
        return _marketStopped;
    }

    public Boolean marketStoppedInitVal() {
        return _marketStoppedInitVal;
    }

	public boolean marketStoppedIsDirty() {
        return !valuesAreEqual(_marketStoppedInitVal, _marketStopped);
    }

    public boolean marketStoppedIsSet() {
        return _marketStoppedIsSet;
    }	


    public void setSmavaGebuehr(Float smavaGebuehr) {
        if (!_smavaGebuehrIsSet) {
            _smavaGebuehrIsSet = true;
            _smavaGebuehrInitVal = smavaGebuehr;
        }
        _smavaGebuehr = smavaGebuehr;
    }

    public Float getSmavaGebuehr() {
        return _smavaGebuehr;
    }

    public Float smavaGebuehrInitVal() {
        return _smavaGebuehrInitVal;
    }

	public boolean smavaGebuehrIsDirty() {
        return !valuesAreEqual(_smavaGebuehrInitVal, _smavaGebuehr);
    }

    public boolean smavaGebuehrIsSet() {
        return _smavaGebuehrIsSet;
    }	


    public void setDaysToGo(Integer daysToGo) {
        if (!_daysToGoIsSet) {
            _daysToGoIsSet = true;
            _daysToGoInitVal = daysToGo;
        }
        _daysToGo = daysToGo;
    }

    public Integer getDaysToGo() {
        return _daysToGo;
    }

    public Integer daysToGoInitVal() {
        return _daysToGoInitVal;
    }

	public boolean daysToGoIsDirty() {
        return !valuesAreEqual(_daysToGoInitVal, _daysToGo);
    }

    public boolean daysToGoIsSet() {
        return _daysToGoIsSet;
    }	


    public void setShowFinancedOrdersPeriod(Integer showFinancedOrdersPeriod) {
        if (!_showFinancedOrdersPeriodIsSet) {
            _showFinancedOrdersPeriodIsSet = true;
            _showFinancedOrdersPeriodInitVal = showFinancedOrdersPeriod;
        }
        _showFinancedOrdersPeriod = showFinancedOrdersPeriod;
    }

    public Integer getShowFinancedOrdersPeriod() {
        return _showFinancedOrdersPeriod;
    }

    public Integer showFinancedOrdersPeriodInitVal() {
        return _showFinancedOrdersPeriodInitVal;
    }

	public boolean showFinancedOrdersPeriodIsDirty() {
        return !valuesAreEqual(_showFinancedOrdersPeriodInitVal, _showFinancedOrdersPeriod);
    }

    public boolean showFinancedOrdersPeriodIsSet() {
        return _showFinancedOrdersPeriodIsSet;
    }	


    public void setMaxOrderDuration(Integer maxOrderDuration) {
        if (!_maxOrderDurationIsSet) {
            _maxOrderDurationIsSet = true;
            _maxOrderDurationInitVal = maxOrderDuration;
        }
        _maxOrderDuration = maxOrderDuration;
    }

    public Integer getMaxOrderDuration() {
        return _maxOrderDuration;
    }

    public Integer maxOrderDurationInitVal() {
        return _maxOrderDurationInitVal;
    }

	public boolean maxOrderDurationIsDirty() {
        return !valuesAreEqual(_maxOrderDurationInitVal, _maxOrderDuration);
    }

    public boolean maxOrderDurationIsSet() {
        return _maxOrderDurationIsSet;
    }	


    public void setPartialMatchAcceptanceDuration(Integer partialMatchAcceptanceDuration) {
        if (!_partialMatchAcceptanceDurationIsSet) {
            _partialMatchAcceptanceDurationIsSet = true;
            _partialMatchAcceptanceDurationInitVal = partialMatchAcceptanceDuration;
        }
        _partialMatchAcceptanceDuration = partialMatchAcceptanceDuration;
    }

    public Integer getPartialMatchAcceptanceDuration() {
        return _partialMatchAcceptanceDuration;
    }

    public Integer partialMatchAcceptanceDurationInitVal() {
        return _partialMatchAcceptanceDurationInitVal;
    }

	public boolean partialMatchAcceptanceDurationIsDirty() {
        return !valuesAreEqual(_partialMatchAcceptanceDurationInitVal, _partialMatchAcceptanceDuration);
    }

    public boolean partialMatchAcceptanceDurationIsSet() {
        return _partialMatchAcceptanceDurationIsSet;
    }	


    public void setMaxOffersPerLender(Integer maxOffersPerLender) {
        if (!_maxOffersPerLenderIsSet) {
            _maxOffersPerLenderIsSet = true;
            _maxOffersPerLenderInitVal = maxOffersPerLender;
        }
        _maxOffersPerLender = maxOffersPerLender;
    }

    public Integer getMaxOffersPerLender() {
        return _maxOffersPerLender;
    }

    public Integer maxOffersPerLenderInitVal() {
        return _maxOffersPerLenderInitVal;
    }

	public boolean maxOffersPerLenderIsDirty() {
        return !valuesAreEqual(_maxOffersPerLenderInitVal, _maxOffersPerLender);
    }

    public boolean maxOffersPerLenderIsSet() {
        return _maxOffersPerLenderIsSet;
    }	


    public void setMinAmountOrder(Double minAmountOrder) {
        if (!_minAmountOrderIsSet) {
            _minAmountOrderIsSet = true;
            _minAmountOrderInitVal = minAmountOrder;
        }
        _minAmountOrder = minAmountOrder;
    }

    public Double getMinAmountOrder() {
        return _minAmountOrder;
    }

    public Double minAmountOrderInitVal() {
        return _minAmountOrderInitVal;
    }

	public boolean minAmountOrderIsDirty() {
        return !valuesAreEqual(_minAmountOrderInitVal, _minAmountOrder);
    }

    public boolean minAmountOrderIsSet() {
        return _minAmountOrderIsSet;
    }	


    public void setMinAmountOffer(Double minAmountOffer) {
        if (!_minAmountOfferIsSet) {
            _minAmountOfferIsSet = true;
            _minAmountOfferInitVal = minAmountOffer;
        }
        _minAmountOffer = minAmountOffer;
    }

    public Double getMinAmountOffer() {
        return _minAmountOffer;
    }

    public Double minAmountOfferInitVal() {
        return _minAmountOfferInitVal;
    }

	public boolean minAmountOfferIsDirty() {
        return !valuesAreEqual(_minAmountOfferInitVal, _minAmountOffer);
    }

    public boolean minAmountOfferIsSet() {
        return _minAmountOfferIsSet;
    }	


    public void setMinAmountPicking(Double minAmountPicking) {
        if (!_minAmountPickingIsSet) {
            _minAmountPickingIsSet = true;
            _minAmountPickingInitVal = minAmountPicking;
        }
        _minAmountPicking = minAmountPicking;
    }

    public Double getMinAmountPicking() {
        return _minAmountPicking;
    }

    public Double minAmountPickingInitVal() {
        return _minAmountPickingInitVal;
    }

	public boolean minAmountPickingIsDirty() {
        return !valuesAreEqual(_minAmountPickingInitVal, _minAmountPicking);
    }

    public boolean minAmountPickingIsSet() {
        return _minAmountPickingIsSet;
    }	


    public void setAmountStep(Double amountStep) {
        if (!_amountStepIsSet) {
            _amountStepIsSet = true;
            _amountStepInitVal = amountStep;
        }
        _amountStep = amountStep;
    }

    public Double getAmountStep() {
        return _amountStep;
    }

    public Double amountStepInitVal() {
        return _amountStepInitVal;
    }

	public boolean amountStepIsDirty() {
        return !valuesAreEqual(_amountStepInitVal, _amountStep);
    }

    public boolean amountStepIsSet() {
        return _amountStepIsSet;
    }	


    public void setSchufaAutomaticScoreRequest(Boolean schufaAutomaticScoreRequest) {
        if (!_schufaAutomaticScoreRequestIsSet) {
            _schufaAutomaticScoreRequestIsSet = true;
            _schufaAutomaticScoreRequestInitVal = schufaAutomaticScoreRequest;
        }
        _schufaAutomaticScoreRequest = schufaAutomaticScoreRequest;
    }

    public Boolean getSchufaAutomaticScoreRequest() {
        return _schufaAutomaticScoreRequest;
    }

    public Boolean schufaAutomaticScoreRequestInitVal() {
        return _schufaAutomaticScoreRequestInitVal;
    }

	public boolean schufaAutomaticScoreRequestIsDirty() {
        return !valuesAreEqual(_schufaAutomaticScoreRequestInitVal, _schufaAutomaticScoreRequest);
    }

    public boolean schufaAutomaticScoreRequestIsSet() {
        return _schufaAutomaticScoreRequestIsSet;
    }	


    public void setSchufaBoManualScoreRequest(Boolean schufaBoManualScoreRequest) {
        if (!_schufaBoManualScoreRequestIsSet) {
            _schufaBoManualScoreRequestIsSet = true;
            _schufaBoManualScoreRequestInitVal = schufaBoManualScoreRequest;
        }
        _schufaBoManualScoreRequest = schufaBoManualScoreRequest;
    }

    public Boolean getSchufaBoManualScoreRequest() {
        return _schufaBoManualScoreRequest;
    }

    public Boolean schufaBoManualScoreRequestInitVal() {
        return _schufaBoManualScoreRequestInitVal;
    }

	public boolean schufaBoManualScoreRequestIsDirty() {
        return !valuesAreEqual(_schufaBoManualScoreRequestInitVal, _schufaBoManualScoreRequest);
    }

    public boolean schufaBoManualScoreRequestIsSet() {
        return _schufaBoManualScoreRequestIsSet;
    }	


    public void setSchufaContractInformation(Boolean schufaContractInformation) {
        if (!_schufaContractInformationIsSet) {
            _schufaContractInformationIsSet = true;
            _schufaContractInformationInitVal = schufaContractInformation;
        }
        _schufaContractInformation = schufaContractInformation;
    }

    public Boolean getSchufaContractInformation() {
        return _schufaContractInformation;
    }

    public Boolean schufaContractInformationInitVal() {
        return _schufaContractInformationInitVal;
    }

	public boolean schufaContractInformationIsDirty() {
        return !valuesAreEqual(_schufaContractInformationInitVal, _schufaContractInformation);
    }

    public boolean schufaContractInformationIsSet() {
        return _schufaContractInformationIsSet;
    }	


    public void setSchufaEncashmentInformation(Boolean schufaEncashmentInformation) {
        if (!_schufaEncashmentInformationIsSet) {
            _schufaEncashmentInformationIsSet = true;
            _schufaEncashmentInformationInitVal = schufaEncashmentInformation;
        }
        _schufaEncashmentInformation = schufaEncashmentInformation;
    }

    public Boolean getSchufaEncashmentInformation() {
        return _schufaEncashmentInformation;
    }

    public Boolean schufaEncashmentInformationInitVal() {
        return _schufaEncashmentInformationInitVal;
    }

	public boolean schufaEncashmentInformationIsDirty() {
        return !valuesAreEqual(_schufaEncashmentInformationInitVal, _schufaEncashmentInformation);
    }

    public boolean schufaEncashmentInformationIsSet() {
        return _schufaEncashmentInformationIsSet;
    }	


    public void setCheckNegativeSchufa(Boolean checkNegativeSchufa) {
        if (!_checkNegativeSchufaIsSet) {
            _checkNegativeSchufaIsSet = true;
            _checkNegativeSchufaInitVal = checkNegativeSchufa;
        }
        _checkNegativeSchufa = checkNegativeSchufa;
    }

    public Boolean getCheckNegativeSchufa() {
        return _checkNegativeSchufa;
    }

    public Boolean checkNegativeSchufaInitVal() {
        return _checkNegativeSchufaInitVal;
    }

	public boolean checkNegativeSchufaIsDirty() {
        return !valuesAreEqual(_checkNegativeSchufaInitVal, _checkNegativeSchufa);
    }

    public boolean checkNegativeSchufaIsSet() {
        return _checkNegativeSchufaIsSet;
    }	


    public void setSchufaConnectionTimeout(Integer schufaConnectionTimeout) {
        if (!_schufaConnectionTimeoutIsSet) {
            _schufaConnectionTimeoutIsSet = true;
            _schufaConnectionTimeoutInitVal = schufaConnectionTimeout;
        }
        _schufaConnectionTimeout = schufaConnectionTimeout;
    }

    public Integer getSchufaConnectionTimeout() {
        return _schufaConnectionTimeout;
    }

    public Integer schufaConnectionTimeoutInitVal() {
        return _schufaConnectionTimeoutInitVal;
    }

	public boolean schufaConnectionTimeoutIsDirty() {
        return !valuesAreEqual(_schufaConnectionTimeoutInitVal, _schufaConnectionTimeout);
    }

    public boolean schufaConnectionTimeoutIsSet() {
        return _schufaConnectionTimeoutIsSet;
    }	


    public void setSchufaReadTimeout(Integer schufaReadTimeout) {
        if (!_schufaReadTimeoutIsSet) {
            _schufaReadTimeoutIsSet = true;
            _schufaReadTimeoutInitVal = schufaReadTimeout;
        }
        _schufaReadTimeout = schufaReadTimeout;
    }

    public Integer getSchufaReadTimeout() {
        return _schufaReadTimeout;
    }

    public Integer schufaReadTimeoutInitVal() {
        return _schufaReadTimeoutInitVal;
    }

	public boolean schufaReadTimeoutIsDirty() {
        return !valuesAreEqual(_schufaReadTimeoutInitVal, _schufaReadTimeout);
    }

    public boolean schufaReadTimeoutIsSet() {
        return _schufaReadTimeoutIsSet;
    }	


    public void setMaxSchufaRating(String maxSchufaRating) {
        if (!_maxSchufaRatingIsSet) {
            _maxSchufaRatingIsSet = true;
            _maxSchufaRatingInitVal = maxSchufaRating;
        }
        _maxSchufaRating = maxSchufaRating;
    }

    public String getMaxSchufaRating() {
        return _maxSchufaRating;
    }

    public String maxSchufaRatingInitVal() {
        return _maxSchufaRatingInitVal;
    }

	public boolean maxSchufaRatingIsDirty() {
        return !valuesAreEqual(_maxSchufaRatingInitVal, _maxSchufaRating);
    }

    public boolean maxSchufaRatingIsSet() {
        return _maxSchufaRatingIsSet;
    }	


    public void setMaxSchufaRatingForSecondPerson(String maxSchufaRatingForSecondPerson) {
        if (!_maxSchufaRatingForSecondPersonIsSet) {
            _maxSchufaRatingForSecondPersonIsSet = true;
            _maxSchufaRatingForSecondPersonInitVal = maxSchufaRatingForSecondPerson;
        }
        _maxSchufaRatingForSecondPerson = maxSchufaRatingForSecondPerson;
    }

    public String getMaxSchufaRatingForSecondPerson() {
        return _maxSchufaRatingForSecondPerson;
    }

    public String maxSchufaRatingForSecondPersonInitVal() {
        return _maxSchufaRatingForSecondPersonInitVal;
    }

	public boolean maxSchufaRatingForSecondPersonIsDirty() {
        return !valuesAreEqual(_maxSchufaRatingForSecondPersonInitVal, _maxSchufaRatingForSecondPerson);
    }

    public boolean maxSchufaRatingForSecondPersonIsSet() {
        return _maxSchufaRatingForSecondPersonIsSet;
    }	


    public void setDefaultRatingLetter(String defaultRatingLetter) {
        if (!_defaultRatingLetterIsSet) {
            _defaultRatingLetterIsSet = true;
            _defaultRatingLetterInitVal = defaultRatingLetter;
        }
        _defaultRatingLetter = defaultRatingLetter;
    }

    public String getDefaultRatingLetter() {
        return _defaultRatingLetter;
    }

    public String defaultRatingLetterInitVal() {
        return _defaultRatingLetterInitVal;
    }

	public boolean defaultRatingLetterIsDirty() {
        return !valuesAreEqual(_defaultRatingLetterInitVal, _defaultRatingLetter);
    }

    public boolean defaultRatingLetterIsSet() {
        return _defaultRatingLetterIsSet;
    }	


    public void setMaxOfferAmount(Double maxOfferAmount) {
        if (!_maxOfferAmountIsSet) {
            _maxOfferAmountIsSet = true;
            _maxOfferAmountInitVal = maxOfferAmount;
        }
        _maxOfferAmount = maxOfferAmount;
    }

    public Double getMaxOfferAmount() {
        return _maxOfferAmount;
    }

    public Double maxOfferAmountInitVal() {
        return _maxOfferAmountInitVal;
    }

	public boolean maxOfferAmountIsDirty() {
        return !valuesAreEqual(_maxOfferAmountInitVal, _maxOfferAmount);
    }

    public boolean maxOfferAmountIsSet() {
        return _maxOfferAmountIsSet;
    }	


    public void setMaxOrderAmount(Double maxOrderAmount) {
        if (!_maxOrderAmountIsSet) {
            _maxOrderAmountIsSet = true;
            _maxOrderAmountInitVal = maxOrderAmount;
        }
        _maxOrderAmount = maxOrderAmount;
    }

    public Double getMaxOrderAmount() {
        return _maxOrderAmount;
    }

    public Double maxOrderAmountInitVal() {
        return _maxOrderAmountInitVal;
    }

	public boolean maxOrderAmountIsDirty() {
        return !valuesAreEqual(_maxOrderAmountInitVal, _maxOrderAmount);
    }

    public boolean maxOrderAmountIsSet() {
        return _maxOrderAmountIsSet;
    }	


    public void setMaxBrokerageAmount(Double maxBrokerageAmount) {
        if (!_maxBrokerageAmountIsSet) {
            _maxBrokerageAmountIsSet = true;
            _maxBrokerageAmountInitVal = maxBrokerageAmount;
        }
        _maxBrokerageAmount = maxBrokerageAmount;
    }

    public Double getMaxBrokerageAmount() {
        return _maxBrokerageAmount;
    }

    public Double maxBrokerageAmountInitVal() {
        return _maxBrokerageAmountInitVal;
    }

	public boolean maxBrokerageAmountIsDirty() {
        return !valuesAreEqual(_maxBrokerageAmountInitVal, _maxBrokerageAmount);
    }

    public boolean maxBrokerageAmountIsSet() {
        return _maxBrokerageAmountIsSet;
    }	


    public void setRecommendationBonusBorrower(Double recommendationBonusBorrower) {
        if (!_recommendationBonusBorrowerIsSet) {
            _recommendationBonusBorrowerIsSet = true;
            _recommendationBonusBorrowerInitVal = recommendationBonusBorrower;
        }
        _recommendationBonusBorrower = recommendationBonusBorrower;
    }

    public Double getRecommendationBonusBorrower() {
        return _recommendationBonusBorrower;
    }

    public Double recommendationBonusBorrowerInitVal() {
        return _recommendationBonusBorrowerInitVal;
    }

	public boolean recommendationBonusBorrowerIsDirty() {
        return !valuesAreEqual(_recommendationBonusBorrowerInitVal, _recommendationBonusBorrower);
    }

    public boolean recommendationBonusBorrowerIsSet() {
        return _recommendationBonusBorrowerIsSet;
    }	


    public void setRecommendationBonusLender(Double recommendationBonusLender) {
        if (!_recommendationBonusLenderIsSet) {
            _recommendationBonusLenderIsSet = true;
            _recommendationBonusLenderInitVal = recommendationBonusLender;
        }
        _recommendationBonusLender = recommendationBonusLender;
    }

    public Double getRecommendationBonusLender() {
        return _recommendationBonusLender;
    }

    public Double recommendationBonusLenderInitVal() {
        return _recommendationBonusLenderInitVal;
    }

	public boolean recommendationBonusLenderIsDirty() {
        return !valuesAreEqual(_recommendationBonusLenderInitVal, _recommendationBonusLender);
    }

    public boolean recommendationBonusLenderIsSet() {
        return _recommendationBonusLenderIsSet;
    }	


    public void setWelcomeBonusBorrower(Double welcomeBonusBorrower) {
        if (!_welcomeBonusBorrowerIsSet) {
            _welcomeBonusBorrowerIsSet = true;
            _welcomeBonusBorrowerInitVal = welcomeBonusBorrower;
        }
        _welcomeBonusBorrower = welcomeBonusBorrower;
    }

    public Double getWelcomeBonusBorrower() {
        return _welcomeBonusBorrower;
    }

    public Double welcomeBonusBorrowerInitVal() {
        return _welcomeBonusBorrowerInitVal;
    }

	public boolean welcomeBonusBorrowerIsDirty() {
        return !valuesAreEqual(_welcomeBonusBorrowerInitVal, _welcomeBonusBorrower);
    }

    public boolean welcomeBonusBorrowerIsSet() {
        return _welcomeBonusBorrowerIsSet;
    }	


    public void setWelcomeBonusLender(Double welcomeBonusLender) {
        if (!_welcomeBonusLenderIsSet) {
            _welcomeBonusLenderIsSet = true;
            _welcomeBonusLenderInitVal = welcomeBonusLender;
        }
        _welcomeBonusLender = welcomeBonusLender;
    }

    public Double getWelcomeBonusLender() {
        return _welcomeBonusLender;
    }

    public Double welcomeBonusLenderInitVal() {
        return _welcomeBonusLenderInitVal;
    }

	public boolean welcomeBonusLenderIsDirty() {
        return !valuesAreEqual(_welcomeBonusLenderInitVal, _welcomeBonusLender);
    }

    public boolean welcomeBonusLenderIsSet() {
        return _welcomeBonusLenderIsSet;
    }	


    public void setActiveDurations(List<String> activeDurations) {
        if (!_activeDurationsIsSet) {
            _activeDurationsIsSet = true;
            _activeDurationsInitVal = activeDurations;
        }
        _activeDurations = activeDurations;
    }

    public List<String> getActiveDurations() {
        return _activeDurations;
    }

    public List<String> activeDurationsInitVal() {
        return _activeDurationsInitVal;
    }

	public boolean activeDurationsIsDirty() {
        return !valuesAreEqual(_activeDurationsInitVal, _activeDurations);
    }

    public boolean activeDurationsIsSet() {
        return _activeDurationsIsSet;
    }	


    public void setPdfEncoding(String pdfEncoding) {
        if (!_pdfEncodingIsSet) {
            _pdfEncodingIsSet = true;
            _pdfEncodingInitVal = pdfEncoding;
        }
        _pdfEncoding = pdfEncoding;
    }

    public String getPdfEncoding() {
        return _pdfEncoding;
    }

    public String pdfEncodingInitVal() {
        return _pdfEncodingInitVal;
    }

	public boolean pdfEncodingIsDirty() {
        return !valuesAreEqual(_pdfEncodingInitVal, _pdfEncoding);
    }

    public boolean pdfEncodingIsSet() {
        return _pdfEncodingIsSet;
    }	


    public void setMarketingCookieDuration(Integer marketingCookieDuration) {
        if (!_marketingCookieDurationIsSet) {
            _marketingCookieDurationIsSet = true;
            _marketingCookieDurationInitVal = marketingCookieDuration;
        }
        _marketingCookieDuration = marketingCookieDuration;
    }

    public Integer getMarketingCookieDuration() {
        return _marketingCookieDuration;
    }

    public Integer marketingCookieDurationInitVal() {
        return _marketingCookieDurationInitVal;
    }

	public boolean marketingCookieDurationIsDirty() {
        return !valuesAreEqual(_marketingCookieDurationInitVal, _marketingCookieDuration);
    }

    public boolean marketingCookieDurationIsSet() {
        return _marketingCookieDurationIsSet;
    }	


    public void setLenderRepaymentDelay(Integer lenderRepaymentDelay) {
        if (!_lenderRepaymentDelayIsSet) {
            _lenderRepaymentDelayIsSet = true;
            _lenderRepaymentDelayInitVal = lenderRepaymentDelay;
        }
        _lenderRepaymentDelay = lenderRepaymentDelay;
    }

    public Integer getLenderRepaymentDelay() {
        return _lenderRepaymentDelay;
    }

    public Integer lenderRepaymentDelayInitVal() {
        return _lenderRepaymentDelayInitVal;
    }

	public boolean lenderRepaymentDelayIsDirty() {
        return !valuesAreEqual(_lenderRepaymentDelayInitVal, _lenderRepaymentDelay);
    }

    public boolean lenderRepaymentDelayIsSet() {
        return _lenderRepaymentDelayIsSet;
    }	


    public void setBrokerageRegistrationTimeout(Long brokerageRegistrationTimeout) {
        if (!_brokerageRegistrationTimeoutIsSet) {
            _brokerageRegistrationTimeoutIsSet = true;
            _brokerageRegistrationTimeoutInitVal = brokerageRegistrationTimeout;
        }
        _brokerageRegistrationTimeout = brokerageRegistrationTimeout;
    }

    public Long getBrokerageRegistrationTimeout() {
        return _brokerageRegistrationTimeout;
    }

    public Long brokerageRegistrationTimeoutInitVal() {
        return _brokerageRegistrationTimeoutInitVal;
    }

	public boolean brokerageRegistrationTimeoutIsDirty() {
        return !valuesAreEqual(_brokerageRegistrationTimeoutInitVal, _brokerageRegistrationTimeout);
    }

    public boolean brokerageRegistrationTimeoutIsSet() {
        return _brokerageRegistrationTimeoutIsSet;
    }	


    public void setBorrowerCreditConsultingDocumentMinOffers(Long borrowerCreditConsultingDocumentMinOffers) {
        if (!_borrowerCreditConsultingDocumentMinOffersIsSet) {
            _borrowerCreditConsultingDocumentMinOffersIsSet = true;
            _borrowerCreditConsultingDocumentMinOffersInitVal = borrowerCreditConsultingDocumentMinOffers;
        }
        _borrowerCreditConsultingDocumentMinOffers = borrowerCreditConsultingDocumentMinOffers;
    }

    public Long getBorrowerCreditConsultingDocumentMinOffers() {
        return _borrowerCreditConsultingDocumentMinOffers;
    }

    public Long borrowerCreditConsultingDocumentMinOffersInitVal() {
        return _borrowerCreditConsultingDocumentMinOffersInitVal;
    }

	public boolean borrowerCreditConsultingDocumentMinOffersIsDirty() {
        return !valuesAreEqual(_borrowerCreditConsultingDocumentMinOffersInitVal, _borrowerCreditConsultingDocumentMinOffers);
    }

    public boolean borrowerCreditConsultingDocumentMinOffersIsSet() {
        return _borrowerCreditConsultingDocumentMinOffersIsSet;
    }	


    public void setBorrowerCreditConsultingDocumentMinOfferAmount(Long borrowerCreditConsultingDocumentMinOfferAmount) {
        if (!_borrowerCreditConsultingDocumentMinOfferAmountIsSet) {
            _borrowerCreditConsultingDocumentMinOfferAmountIsSet = true;
            _borrowerCreditConsultingDocumentMinOfferAmountInitVal = borrowerCreditConsultingDocumentMinOfferAmount;
        }
        _borrowerCreditConsultingDocumentMinOfferAmount = borrowerCreditConsultingDocumentMinOfferAmount;
    }

    public Long getBorrowerCreditConsultingDocumentMinOfferAmount() {
        return _borrowerCreditConsultingDocumentMinOfferAmount;
    }

    public Long borrowerCreditConsultingDocumentMinOfferAmountInitVal() {
        return _borrowerCreditConsultingDocumentMinOfferAmountInitVal;
    }

	public boolean borrowerCreditConsultingDocumentMinOfferAmountIsDirty() {
        return !valuesAreEqual(_borrowerCreditConsultingDocumentMinOfferAmountInitVal, _borrowerCreditConsultingDocumentMinOfferAmount);
    }

    public boolean borrowerCreditConsultingDocumentMinOfferAmountIsSet() {
        return _borrowerCreditConsultingDocumentMinOfferAmountIsSet;
    }	


    public void setBorrowerCreditConsultingRegistrationDropoutDelay(Integer borrowerCreditConsultingRegistrationDropoutDelay) {
        if (!_borrowerCreditConsultingRegistrationDropoutDelayIsSet) {
            _borrowerCreditConsultingRegistrationDropoutDelayIsSet = true;
            _borrowerCreditConsultingRegistrationDropoutDelayInitVal = borrowerCreditConsultingRegistrationDropoutDelay;
        }
        _borrowerCreditConsultingRegistrationDropoutDelay = borrowerCreditConsultingRegistrationDropoutDelay;
    }

    public Integer getBorrowerCreditConsultingRegistrationDropoutDelay() {
        return _borrowerCreditConsultingRegistrationDropoutDelay;
    }

    public Integer borrowerCreditConsultingRegistrationDropoutDelayInitVal() {
        return _borrowerCreditConsultingRegistrationDropoutDelayInitVal;
    }

	public boolean borrowerCreditConsultingRegistrationDropoutDelayIsDirty() {
        return !valuesAreEqual(_borrowerCreditConsultingRegistrationDropoutDelayInitVal, _borrowerCreditConsultingRegistrationDropoutDelay);
    }

    public boolean borrowerCreditConsultingRegistrationDropoutDelayIsSet() {
        return _borrowerCreditConsultingRegistrationDropoutDelayIsSet;
    }	


    public void setObjectionTerm(Integer objectionTerm) {
        if (!_objectionTermIsSet) {
            _objectionTermIsSet = true;
            _objectionTermInitVal = objectionTerm;
        }
        _objectionTerm = objectionTerm;
    }

    public Integer getObjectionTerm() {
        return _objectionTerm;
    }

    public Integer objectionTermInitVal() {
        return _objectionTermInitVal;
    }

	public boolean objectionTermIsDirty() {
        return !valuesAreEqual(_objectionTermInitVal, _objectionTerm);
    }

    public boolean objectionTermIsSet() {
        return _objectionTermIsSet;
    }	


    public void setTransferTerm(Integer transferTerm) {
        if (!_transferTermIsSet) {
            _transferTermIsSet = true;
            _transferTermInitVal = transferTerm;
        }
        _transferTerm = transferTerm;
    }

    public Integer getTransferTerm() {
        return _transferTerm;
    }

    public Integer transferTermInitVal() {
        return _transferTermInitVal;
    }

	public boolean transferTermIsDirty() {
        return !valuesAreEqual(_transferTermInitVal, _transferTerm);
    }

    public boolean transferTermIsSet() {
        return _transferTermIsSet;
    }	


    public void setLeadTime(Integer leadTime) {
        if (!_leadTimeIsSet) {
            _leadTimeIsSet = true;
            _leadTimeInitVal = leadTime;
        }
        _leadTime = leadTime;
    }

    public Integer getLeadTime() {
        return _leadTime;
    }

    public Integer leadTimeInitVal() {
        return _leadTimeInitVal;
    }

	public boolean leadTimeIsDirty() {
        return !valuesAreEqual(_leadTimeInitVal, _leadTime);
    }

    public boolean leadTimeIsSet() {
        return _leadTimeIsSet;
    }	


    public void setVisibleDocumentTypes(Set<String> visibleDocumentTypes) {
        if (!_visibleDocumentTypesIsSet) {
            _visibleDocumentTypesIsSet = true;
            _visibleDocumentTypesInitVal = visibleDocumentTypes;
        }
        _visibleDocumentTypes = visibleDocumentTypes;
    }

    public Set<String> getVisibleDocumentTypes() {
        return _visibleDocumentTypes;
    }

    public Set<String> visibleDocumentTypesInitVal() {
        return _visibleDocumentTypesInitVal;
    }

	public boolean visibleDocumentTypesIsDirty() {
        return !valuesAreEqual(_visibleDocumentTypesInitVal, _visibleDocumentTypes);
    }

    public boolean visibleDocumentTypesIsSet() {
        return _visibleDocumentTypesIsSet;
    }	


    public void setDocumentDownloadTimeoutPeriod(Integer documentDownloadTimeoutPeriod) {
        if (!_documentDownloadTimeoutPeriodIsSet) {
            _documentDownloadTimeoutPeriodIsSet = true;
            _documentDownloadTimeoutPeriodInitVal = documentDownloadTimeoutPeriod;
        }
        _documentDownloadTimeoutPeriod = documentDownloadTimeoutPeriod;
    }

    public Integer getDocumentDownloadTimeoutPeriod() {
        return _documentDownloadTimeoutPeriod;
    }

    public Integer documentDownloadTimeoutPeriodInitVal() {
        return _documentDownloadTimeoutPeriodInitVal;
    }

	public boolean documentDownloadTimeoutPeriodIsDirty() {
        return !valuesAreEqual(_documentDownloadTimeoutPeriodInitVal, _documentDownloadTimeoutPeriod);
    }

    public boolean documentDownloadTimeoutPeriodIsSet() {
        return _documentDownloadTimeoutPeriodIsSet;
    }	


    public void setBankDataFtp(Boolean bankDataFtp) {
        if (!_bankDataFtpIsSet) {
            _bankDataFtpIsSet = true;
            _bankDataFtpInitVal = bankDataFtp;
        }
        _bankDataFtp = bankDataFtp;
    }

    public Boolean getBankDataFtp() {
        return _bankDataFtp;
    }

    public Boolean bankDataFtpInitVal() {
        return _bankDataFtpInitVal;
    }

	public boolean bankDataFtpIsDirty() {
        return !valuesAreEqual(_bankDataFtpInitVal, _bankDataFtp);
    }

    public boolean bankDataFtpIsSet() {
        return _bankDataFtpIsSet;
    }	


    public void setDefaultMarketingPlacementId(Long defaultMarketingPlacementId) {
        if (!_defaultMarketingPlacementIdIsSet) {
            _defaultMarketingPlacementIdIsSet = true;
            _defaultMarketingPlacementIdInitVal = defaultMarketingPlacementId;
        }
        _defaultMarketingPlacementId = defaultMarketingPlacementId;
    }

    public Long getDefaultMarketingPlacementId() {
        return _defaultMarketingPlacementId;
    }

    public Long defaultMarketingPlacementIdInitVal() {
        return _defaultMarketingPlacementIdInitVal;
    }

	public boolean defaultMarketingPlacementIdIsDirty() {
        return !valuesAreEqual(_defaultMarketingPlacementIdInitVal, _defaultMarketingPlacementId);
    }

    public boolean defaultMarketingPlacementIdIsSet() {
        return _defaultMarketingPlacementIdIsSet;
    }	


    public void setXmlInterfaceCampaignId(Long xmlInterfaceCampaignId) {
        if (!_xmlInterfaceCampaignIdIsSet) {
            _xmlInterfaceCampaignIdIsSet = true;
            _xmlInterfaceCampaignIdInitVal = xmlInterfaceCampaignId;
        }
        _xmlInterfaceCampaignId = xmlInterfaceCampaignId;
    }

    public Long getXmlInterfaceCampaignId() {
        return _xmlInterfaceCampaignId;
    }

    public Long xmlInterfaceCampaignIdInitVal() {
        return _xmlInterfaceCampaignIdInitVal;
    }

	public boolean xmlInterfaceCampaignIdIsDirty() {
        return !valuesAreEqual(_xmlInterfaceCampaignIdInitVal, _xmlInterfaceCampaignId);
    }

    public boolean xmlInterfaceCampaignIdIsSet() {
        return _xmlInterfaceCampaignIdIsSet;
    }	


    public void setPlacementLeadValidity(Integer placementLeadValidity) {
        if (!_placementLeadValidityIsSet) {
            _placementLeadValidityIsSet = true;
            _placementLeadValidityInitVal = placementLeadValidity;
        }
        _placementLeadValidity = placementLeadValidity;
    }

    public Integer getPlacementLeadValidity() {
        return _placementLeadValidity;
    }

    public Integer placementLeadValidityInitVal() {
        return _placementLeadValidityInitVal;
    }

	public boolean placementLeadValidityIsDirty() {
        return !valuesAreEqual(_placementLeadValidityInitVal, _placementLeadValidity);
    }

    public boolean placementLeadValidityIsSet() {
        return _placementLeadValidityIsSet;
    }	


    public void setPlacementSalesValidity(Integer placementSalesValidity) {
        if (!_placementSalesValidityIsSet) {
            _placementSalesValidityIsSet = true;
            _placementSalesValidityInitVal = placementSalesValidity;
        }
        _placementSalesValidity = placementSalesValidity;
    }

    public Integer getPlacementSalesValidity() {
        return _placementSalesValidity;
    }

    public Integer placementSalesValidityInitVal() {
        return _placementSalesValidityInitVal;
    }

	public boolean placementSalesValidityIsDirty() {
        return !valuesAreEqual(_placementSalesValidityInitVal, _placementSalesValidity);
    }

    public boolean placementSalesValidityIsSet() {
        return _placementSalesValidityIsSet;
    }	


    public void setFirstDocumentsReminderTerm(Integer firstDocumentsReminderTerm) {
        if (!_firstDocumentsReminderTermIsSet) {
            _firstDocumentsReminderTermIsSet = true;
            _firstDocumentsReminderTermInitVal = firstDocumentsReminderTerm;
        }
        _firstDocumentsReminderTerm = firstDocumentsReminderTerm;
    }

    public Integer getFirstDocumentsReminderTerm() {
        return _firstDocumentsReminderTerm;
    }

    public Integer firstDocumentsReminderTermInitVal() {
        return _firstDocumentsReminderTermInitVal;
    }

	public boolean firstDocumentsReminderTermIsDirty() {
        return !valuesAreEqual(_firstDocumentsReminderTermInitVal, _firstDocumentsReminderTerm);
    }

    public boolean firstDocumentsReminderTermIsSet() {
        return _firstDocumentsReminderTermIsSet;
    }	


    public void setSecondDocumentsReminderTerm(Integer secondDocumentsReminderTerm) {
        if (!_secondDocumentsReminderTermIsSet) {
            _secondDocumentsReminderTermIsSet = true;
            _secondDocumentsReminderTermInitVal = secondDocumentsReminderTerm;
        }
        _secondDocumentsReminderTerm = secondDocumentsReminderTerm;
    }

    public Integer getSecondDocumentsReminderTerm() {
        return _secondDocumentsReminderTerm;
    }

    public Integer secondDocumentsReminderTermInitVal() {
        return _secondDocumentsReminderTermInitVal;
    }

	public boolean secondDocumentsReminderTermIsDirty() {
        return !valuesAreEqual(_secondDocumentsReminderTermInitVal, _secondDocumentsReminderTerm);
    }

    public boolean secondDocumentsReminderTermIsSet() {
        return _secondDocumentsReminderTermIsSet;
    }	


    public void setThirdDocumentsReminderTerm(Integer thirdDocumentsReminderTerm) {
        if (!_thirdDocumentsReminderTermIsSet) {
            _thirdDocumentsReminderTermIsSet = true;
            _thirdDocumentsReminderTermInitVal = thirdDocumentsReminderTerm;
        }
        _thirdDocumentsReminderTerm = thirdDocumentsReminderTerm;
    }

    public Integer getThirdDocumentsReminderTerm() {
        return _thirdDocumentsReminderTerm;
    }

    public Integer thirdDocumentsReminderTermInitVal() {
        return _thirdDocumentsReminderTermInitVal;
    }

	public boolean thirdDocumentsReminderTermIsDirty() {
        return !valuesAreEqual(_thirdDocumentsReminderTermInitVal, _thirdDocumentsReminderTerm);
    }

    public boolean thirdDocumentsReminderTermIsSet() {
        return _thirdDocumentsReminderTermIsSet;
    }	


    public void setFourthDocumentsReminderTerm(Integer fourthDocumentsReminderTerm) {
        if (!_fourthDocumentsReminderTermIsSet) {
            _fourthDocumentsReminderTermIsSet = true;
            _fourthDocumentsReminderTermInitVal = fourthDocumentsReminderTerm;
        }
        _fourthDocumentsReminderTerm = fourthDocumentsReminderTerm;
    }

    public Integer getFourthDocumentsReminderTerm() {
        return _fourthDocumentsReminderTerm;
    }

    public Integer fourthDocumentsReminderTermInitVal() {
        return _fourthDocumentsReminderTermInitVal;
    }

	public boolean fourthDocumentsReminderTermIsDirty() {
        return !valuesAreEqual(_fourthDocumentsReminderTermInitVal, _fourthDocumentsReminderTerm);
    }

    public boolean fourthDocumentsReminderTermIsSet() {
        return _fourthDocumentsReminderTermIsSet;
    }	


    public void setFirstReminderFine(Double firstReminderFine) {
        if (!_firstReminderFineIsSet) {
            _firstReminderFineIsSet = true;
            _firstReminderFineInitVal = firstReminderFine;
        }
        _firstReminderFine = firstReminderFine;
    }

    public Double getFirstReminderFine() {
        return _firstReminderFine;
    }

    public Double firstReminderFineInitVal() {
        return _firstReminderFineInitVal;
    }

	public boolean firstReminderFineIsDirty() {
        return !valuesAreEqual(_firstReminderFineInitVal, _firstReminderFine);
    }

    public boolean firstReminderFineIsSet() {
        return _firstReminderFineIsSet;
    }	


    public void setSecondReminderFine(Double secondReminderFine) {
        if (!_secondReminderFineIsSet) {
            _secondReminderFineIsSet = true;
            _secondReminderFineInitVal = secondReminderFine;
        }
        _secondReminderFine = secondReminderFine;
    }

    public Double getSecondReminderFine() {
        return _secondReminderFine;
    }

    public Double secondReminderFineInitVal() {
        return _secondReminderFineInitVal;
    }

	public boolean secondReminderFineIsDirty() {
        return !valuesAreEqual(_secondReminderFineInitVal, _secondReminderFine);
    }

    public boolean secondReminderFineIsSet() {
        return _secondReminderFineIsSet;
    }	


    public void setOverdueBaseRate(Double overdueBaseRate) {
        if (!_overdueBaseRateIsSet) {
            _overdueBaseRateIsSet = true;
            _overdueBaseRateInitVal = overdueBaseRate;
        }
        _overdueBaseRate = overdueBaseRate;
    }

    public Double getOverdueBaseRate() {
        return _overdueBaseRate;
    }

    public Double overdueBaseRateInitVal() {
        return _overdueBaseRateInitVal;
    }

	public boolean overdueBaseRateIsDirty() {
        return !valuesAreEqual(_overdueBaseRateInitVal, _overdueBaseRate);
    }

    public boolean overdueBaseRateIsSet() {
        return _overdueBaseRateIsSet;
    }	


    public void setSendLenderReminderNotification(Boolean sendLenderReminderNotification) {
        if (!_sendLenderReminderNotificationIsSet) {
            _sendLenderReminderNotificationIsSet = true;
            _sendLenderReminderNotificationInitVal = sendLenderReminderNotification;
        }
        _sendLenderReminderNotification = sendLenderReminderNotification;
    }

    public Boolean getSendLenderReminderNotification() {
        return _sendLenderReminderNotification;
    }

    public Boolean sendLenderReminderNotificationInitVal() {
        return _sendLenderReminderNotificationInitVal;
    }

	public boolean sendLenderReminderNotificationIsDirty() {
        return !valuesAreEqual(_sendLenderReminderNotificationInitVal, _sendLenderReminderNotification);
    }

    public boolean sendLenderReminderNotificationIsSet() {
        return _sendLenderReminderNotificationIsSet;
    }	


    public void setSendGroupownerReminderNotification(Boolean sendGroupownerReminderNotification) {
        if (!_sendGroupownerReminderNotificationIsSet) {
            _sendGroupownerReminderNotificationIsSet = true;
            _sendGroupownerReminderNotificationInitVal = sendGroupownerReminderNotification;
        }
        _sendGroupownerReminderNotification = sendGroupownerReminderNotification;
    }

    public Boolean getSendGroupownerReminderNotification() {
        return _sendGroupownerReminderNotification;
    }

    public Boolean sendGroupownerReminderNotificationInitVal() {
        return _sendGroupownerReminderNotificationInitVal;
    }

	public boolean sendGroupownerReminderNotificationIsDirty() {
        return !valuesAreEqual(_sendGroupownerReminderNotificationInitVal, _sendGroupownerReminderNotification);
    }

    public boolean sendGroupownerReminderNotificationIsSet() {
        return _sendGroupownerReminderNotificationIsSet;
    }	


    public void setSendInviterReminderNotification(Boolean sendInviterReminderNotification) {
        if (!_sendInviterReminderNotificationIsSet) {
            _sendInviterReminderNotificationIsSet = true;
            _sendInviterReminderNotificationInitVal = sendInviterReminderNotification;
        }
        _sendInviterReminderNotification = sendInviterReminderNotification;
    }

    public Boolean getSendInviterReminderNotification() {
        return _sendInviterReminderNotification;
    }

    public Boolean sendInviterReminderNotificationInitVal() {
        return _sendInviterReminderNotificationInitVal;
    }

	public boolean sendInviterReminderNotificationIsDirty() {
        return !valuesAreEqual(_sendInviterReminderNotificationInitVal, _sendInviterReminderNotification);
    }

    public boolean sendInviterReminderNotificationIsSet() {
        return _sendInviterReminderNotificationIsSet;
    }	


    public void setReminderTerm(Integer reminderTerm) {
        if (!_reminderTermIsSet) {
            _reminderTermIsSet = true;
            _reminderTermInitVal = reminderTerm;
        }
        _reminderTerm = reminderTerm;
    }

    public Integer getReminderTerm() {
        return _reminderTerm;
    }

    public Integer reminderTermInitVal() {
        return _reminderTermInitVal;
    }

	public boolean reminderTermIsDirty() {
        return !valuesAreEqual(_reminderTermInitVal, _reminderTerm);
    }

    public boolean reminderTermIsSet() {
        return _reminderTermIsSet;
    }	


    public void setReminderBuffer(Integer reminderBuffer) {
        if (!_reminderBufferIsSet) {
            _reminderBufferIsSet = true;
            _reminderBufferInitVal = reminderBuffer;
        }
        _reminderBuffer = reminderBuffer;
    }

    public Integer getReminderBuffer() {
        return _reminderBuffer;
    }

    public Integer reminderBufferInitVal() {
        return _reminderBufferInitVal;
    }

	public boolean reminderBufferIsDirty() {
        return !valuesAreEqual(_reminderBufferInitVal, _reminderBuffer);
    }

    public boolean reminderBufferIsSet() {
        return _reminderBufferIsSet;
    }	


    public void setMinimumInterest(Double minimumInterest) {
        if (!_minimumInterestIsSet) {
            _minimumInterestIsSet = true;
            _minimumInterestInitVal = minimumInterest;
        }
        _minimumInterest = minimumInterest;
    }

    public Double getMinimumInterest() {
        return _minimumInterest;
    }

    public Double minimumInterestInitVal() {
        return _minimumInterestInitVal;
    }

	public boolean minimumInterestIsDirty() {
        return !valuesAreEqual(_minimumInterestInitVal, _minimumInterest);
    }

    public boolean minimumInterestIsSet() {
        return _minimumInterestIsSet;
    }	


    public void setMaximumInterest(Double maximumInterest) {
        if (!_maximumInterestIsSet) {
            _maximumInterestIsSet = true;
            _maximumInterestInitVal = maximumInterest;
        }
        _maximumInterest = maximumInterest;
    }

    public Double getMaximumInterest() {
        return _maximumInterest;
    }

    public Double maximumInterestInitVal() {
        return _maximumInterestInitVal;
    }

	public boolean maximumInterestIsDirty() {
        return !valuesAreEqual(_maximumInterestInitVal, _maximumInterest);
    }

    public boolean maximumInterestIsSet() {
        return _maximumInterestIsSet;
    }	


    public void setMarketTickerTerm(Integer marketTickerTerm) {
        if (!_marketTickerTermIsSet) {
            _marketTickerTermIsSet = true;
            _marketTickerTermInitVal = marketTickerTerm;
        }
        _marketTickerTerm = marketTickerTerm;
    }

    public Integer getMarketTickerTerm() {
        return _marketTickerTerm;
    }

    public Integer marketTickerTermInitVal() {
        return _marketTickerTermInitVal;
    }

	public boolean marketTickerTermIsDirty() {
        return !valuesAreEqual(_marketTickerTermInitVal, _marketTickerTerm);
    }

    public boolean marketTickerTermIsSet() {
        return _marketTickerTermIsSet;
    }	


    public void setSmavaSelectAverageInterestTerm(Integer smavaSelectAverageInterestTerm) {
        if (!_smavaSelectAverageInterestTermIsSet) {
            _smavaSelectAverageInterestTermIsSet = true;
            _smavaSelectAverageInterestTermInitVal = smavaSelectAverageInterestTerm;
        }
        _smavaSelectAverageInterestTerm = smavaSelectAverageInterestTerm;
    }

    public Integer getSmavaSelectAverageInterestTerm() {
        return _smavaSelectAverageInterestTerm;
    }

    public Integer smavaSelectAverageInterestTermInitVal() {
        return _smavaSelectAverageInterestTermInitVal;
    }

	public boolean smavaSelectAverageInterestTermIsDirty() {
        return !valuesAreEqual(_smavaSelectAverageInterestTermInitVal, _smavaSelectAverageInterestTerm);
    }

    public boolean smavaSelectAverageInterestTermIsSet() {
        return _smavaSelectAverageInterestTermIsSet;
    }	


    public void setMarketTickerMinimumContractsCount(Integer marketTickerMinimumContractsCount) {
        if (!_marketTickerMinimumContractsCountIsSet) {
            _marketTickerMinimumContractsCountIsSet = true;
            _marketTickerMinimumContractsCountInitVal = marketTickerMinimumContractsCount;
        }
        _marketTickerMinimumContractsCount = marketTickerMinimumContractsCount;
    }

    public Integer getMarketTickerMinimumContractsCount() {
        return _marketTickerMinimumContractsCount;
    }

    public Integer marketTickerMinimumContractsCountInitVal() {
        return _marketTickerMinimumContractsCountInitVal;
    }

	public boolean marketTickerMinimumContractsCountIsDirty() {
        return !valuesAreEqual(_marketTickerMinimumContractsCountInitVal, _marketTickerMinimumContractsCount);
    }

    public boolean marketTickerMinimumContractsCountIsSet() {
        return _marketTickerMinimumContractsCountIsSet;
    }	


    public void setMarketingUrlFilterPlacementId(Long marketingUrlFilterPlacementId) {
        if (!_marketingUrlFilterPlacementIdIsSet) {
            _marketingUrlFilterPlacementIdIsSet = true;
            _marketingUrlFilterPlacementIdInitVal = marketingUrlFilterPlacementId;
        }
        _marketingUrlFilterPlacementId = marketingUrlFilterPlacementId;
    }

    public Long getMarketingUrlFilterPlacementId() {
        return _marketingUrlFilterPlacementId;
    }

    public Long marketingUrlFilterPlacementIdInitVal() {
        return _marketingUrlFilterPlacementIdInitVal;
    }

	public boolean marketingUrlFilterPlacementIdIsDirty() {
        return !valuesAreEqual(_marketingUrlFilterPlacementIdInitVal, _marketingUrlFilterPlacementId);
    }

    public boolean marketingUrlFilterPlacementIdIsSet() {
        return _marketingUrlFilterPlacementIdIsSet;
    }	


    public void setSmavaLogoRequiredInBackoffice(Boolean smavaLogoRequiredInBackoffice) {
        if (!_smavaLogoRequiredInBackofficeIsSet) {
            _smavaLogoRequiredInBackofficeIsSet = true;
            _smavaLogoRequiredInBackofficeInitVal = smavaLogoRequiredInBackoffice;
        }
        _smavaLogoRequiredInBackoffice = smavaLogoRequiredInBackoffice;
    }

    public Boolean getSmavaLogoRequiredInBackoffice() {
        return _smavaLogoRequiredInBackoffice;
    }

    public Boolean smavaLogoRequiredInBackofficeInitVal() {
        return _smavaLogoRequiredInBackofficeInitVal;
    }

	public boolean smavaLogoRequiredInBackofficeIsDirty() {
        return !valuesAreEqual(_smavaLogoRequiredInBackofficeInitVal, _smavaLogoRequiredInBackoffice);
    }

    public boolean smavaLogoRequiredInBackofficeIsSet() {
        return _smavaLogoRequiredInBackofficeIsSet;
    }	


    public void setSmavaLogoLocation(String smavaLogoLocation) {
        if (!_smavaLogoLocationIsSet) {
            _smavaLogoLocationIsSet = true;
            _smavaLogoLocationInitVal = smavaLogoLocation;
        }
        _smavaLogoLocation = smavaLogoLocation;
    }

    public String getSmavaLogoLocation() {
        return _smavaLogoLocation;
    }

    public String smavaLogoLocationInitVal() {
        return _smavaLogoLocationInitVal;
    }

	public boolean smavaLogoLocationIsDirty() {
        return !valuesAreEqual(_smavaLogoLocationInitVal, _smavaLogoLocation);
    }

    public boolean smavaLogoLocationIsSet() {
        return _smavaLogoLocationIsSet;
    }	


    public void setLastAgbUpdateDate(String lastAgbUpdateDate) {
        if (!_lastAgbUpdateDateIsSet) {
            _lastAgbUpdateDateIsSet = true;
            _lastAgbUpdateDateInitVal = lastAgbUpdateDate;
        }
        _lastAgbUpdateDate = lastAgbUpdateDate;
    }

    public String getLastAgbUpdateDate() {
        return _lastAgbUpdateDate;
    }

    public String lastAgbUpdateDateInitVal() {
        return _lastAgbUpdateDateInitVal;
    }

	public boolean lastAgbUpdateDateIsDirty() {
        return !valuesAreEqual(_lastAgbUpdateDateInitVal, _lastAgbUpdateDate);
    }

    public boolean lastAgbUpdateDateIsSet() {
        return _lastAgbUpdateDateIsSet;
    }	


    public void setLastPrivacyUpdateDate(String lastPrivacyUpdateDate) {
        if (!_lastPrivacyUpdateDateIsSet) {
            _lastPrivacyUpdateDateIsSet = true;
            _lastPrivacyUpdateDateInitVal = lastPrivacyUpdateDate;
        }
        _lastPrivacyUpdateDate = lastPrivacyUpdateDate;
    }

    public String getLastPrivacyUpdateDate() {
        return _lastPrivacyUpdateDate;
    }

    public String lastPrivacyUpdateDateInitVal() {
        return _lastPrivacyUpdateDateInitVal;
    }

	public boolean lastPrivacyUpdateDateIsDirty() {
        return !valuesAreEqual(_lastPrivacyUpdateDateInitVal, _lastPrivacyUpdateDate);
    }

    public boolean lastPrivacyUpdateDateIsSet() {
        return _lastPrivacyUpdateDateIsSet;
    }	


    public void setLastSchufaUpdateDate(String lastSchufaUpdateDate) {
        if (!_lastSchufaUpdateDateIsSet) {
            _lastSchufaUpdateDateIsSet = true;
            _lastSchufaUpdateDateInitVal = lastSchufaUpdateDate;
        }
        _lastSchufaUpdateDate = lastSchufaUpdateDate;
    }

    public String getLastSchufaUpdateDate() {
        return _lastSchufaUpdateDate;
    }

    public String lastSchufaUpdateDateInitVal() {
        return _lastSchufaUpdateDateInitVal;
    }

	public boolean lastSchufaUpdateDateIsDirty() {
        return !valuesAreEqual(_lastSchufaUpdateDateInitVal, _lastSchufaUpdateDate);
    }

    public boolean lastSchufaUpdateDateIsSet() {
        return _lastSchufaUpdateDateIsSet;
    }	


    public void setCallServicePrimaryPhoneNumber(String callServicePrimaryPhoneNumber) {
        if (!_callServicePrimaryPhoneNumberIsSet) {
            _callServicePrimaryPhoneNumberIsSet = true;
            _callServicePrimaryPhoneNumberInitVal = callServicePrimaryPhoneNumber;
        }
        _callServicePrimaryPhoneNumber = callServicePrimaryPhoneNumber;
    }

    public String getCallServicePrimaryPhoneNumber() {
        return _callServicePrimaryPhoneNumber;
    }

    public String callServicePrimaryPhoneNumberInitVal() {
        return _callServicePrimaryPhoneNumberInitVal;
    }

	public boolean callServicePrimaryPhoneNumberIsDirty() {
        return !valuesAreEqual(_callServicePrimaryPhoneNumberInitVal, _callServicePrimaryPhoneNumber);
    }

    public boolean callServicePrimaryPhoneNumberIsSet() {
        return _callServicePrimaryPhoneNumberIsSet;
    }	


    public void setCallServicePrimaryPhonePreselection(String callServicePrimaryPhonePreselection) {
        if (!_callServicePrimaryPhonePreselectionIsSet) {
            _callServicePrimaryPhonePreselectionIsSet = true;
            _callServicePrimaryPhonePreselectionInitVal = callServicePrimaryPhonePreselection;
        }
        _callServicePrimaryPhonePreselection = callServicePrimaryPhonePreselection;
    }

    public String getCallServicePrimaryPhonePreselection() {
        return _callServicePrimaryPhonePreselection;
    }

    public String callServicePrimaryPhonePreselectionInitVal() {
        return _callServicePrimaryPhonePreselectionInitVal;
    }

	public boolean callServicePrimaryPhonePreselectionIsDirty() {
        return !valuesAreEqual(_callServicePrimaryPhonePreselectionInitVal, _callServicePrimaryPhonePreselection);
    }

    public boolean callServicePrimaryPhonePreselectionIsSet() {
        return _callServicePrimaryPhonePreselectionIsSet;
    }	


    public void setCallServiceSecondaryPhoneNumber(String callServiceSecondaryPhoneNumber) {
        if (!_callServiceSecondaryPhoneNumberIsSet) {
            _callServiceSecondaryPhoneNumberIsSet = true;
            _callServiceSecondaryPhoneNumberInitVal = callServiceSecondaryPhoneNumber;
        }
        _callServiceSecondaryPhoneNumber = callServiceSecondaryPhoneNumber;
    }

    public String getCallServiceSecondaryPhoneNumber() {
        return _callServiceSecondaryPhoneNumber;
    }

    public String callServiceSecondaryPhoneNumberInitVal() {
        return _callServiceSecondaryPhoneNumberInitVal;
    }

	public boolean callServiceSecondaryPhoneNumberIsDirty() {
        return !valuesAreEqual(_callServiceSecondaryPhoneNumberInitVal, _callServiceSecondaryPhoneNumber);
    }

    public boolean callServiceSecondaryPhoneNumberIsSet() {
        return _callServiceSecondaryPhoneNumberIsSet;
    }	


    public void setCallServiceSecondaryPhonePreselection(String callServiceSecondaryPhonePreselection) {
        if (!_callServiceSecondaryPhonePreselectionIsSet) {
            _callServiceSecondaryPhonePreselectionIsSet = true;
            _callServiceSecondaryPhonePreselectionInitVal = callServiceSecondaryPhonePreselection;
        }
        _callServiceSecondaryPhonePreselection = callServiceSecondaryPhonePreselection;
    }

    public String getCallServiceSecondaryPhonePreselection() {
        return _callServiceSecondaryPhonePreselection;
    }

    public String callServiceSecondaryPhonePreselectionInitVal() {
        return _callServiceSecondaryPhonePreselectionInitVal;
    }

	public boolean callServiceSecondaryPhonePreselectionIsDirty() {
        return !valuesAreEqual(_callServiceSecondaryPhonePreselectionInitVal, _callServiceSecondaryPhonePreselection);
    }

    public boolean callServiceSecondaryPhonePreselectionIsSet() {
        return _callServiceSecondaryPhonePreselectionIsSet;
    }	


    public void setCallServiceEnabled(boolean callServiceEnabled) {
        if (!_callServiceEnabledIsSet) {
            _callServiceEnabledIsSet = true;
            _callServiceEnabledInitVal = callServiceEnabled;
        }
        _callServiceEnabled = callServiceEnabled;
    }

    public boolean getCallServiceEnabled() {
        return _callServiceEnabled;
    }

    public boolean callServiceEnabledInitVal() {
        return _callServiceEnabledInitVal;
    }

	public boolean callServiceEnabledIsDirty() {
        return !valuesAreEqual(_callServiceEnabledInitVal, _callServiceEnabled);
    }

    public boolean callServiceEnabledIsSet() {
        return _callServiceEnabledIsSet;
    }	


    public void setRdiActivated(boolean rdiActivated) {
        if (!_rdiActivatedIsSet) {
            _rdiActivatedIsSet = true;
            _rdiActivatedInitVal = rdiActivated;
        }
        _rdiActivated = rdiActivated;
    }

    public boolean getRdiActivated() {
        return _rdiActivated;
    }

    public boolean rdiActivatedInitVal() {
        return _rdiActivatedInitVal;
    }

	public boolean rdiActivatedIsDirty() {
        return !valuesAreEqual(_rdiActivatedInitVal, _rdiActivated);
    }

    public boolean rdiActivatedIsSet() {
        return _rdiActivatedIsSet;
    }	


    public void setRdiObjectionTerm(Integer rdiObjectionTerm) {
        if (!_rdiObjectionTermIsSet) {
            _rdiObjectionTermIsSet = true;
            _rdiObjectionTermInitVal = rdiObjectionTerm;
        }
        _rdiObjectionTerm = rdiObjectionTerm;
    }

    public Integer getRdiObjectionTerm() {
        return _rdiObjectionTerm;
    }

    public Integer rdiObjectionTermInitVal() {
        return _rdiObjectionTermInitVal;
    }

	public boolean rdiObjectionTermIsDirty() {
        return !valuesAreEqual(_rdiObjectionTermInitVal, _rdiObjectionTerm);
    }

    public boolean rdiObjectionTermIsSet() {
        return _rdiObjectionTermIsSet;
    }	


    public void setMaximumGroupMemberships(Integer maximumGroupMemberships) {
        if (!_maximumGroupMembershipsIsSet) {
            _maximumGroupMembershipsIsSet = true;
            _maximumGroupMembershipsInitVal = maximumGroupMemberships;
        }
        _maximumGroupMemberships = maximumGroupMemberships;
    }

    public Integer getMaximumGroupMemberships() {
        return _maximumGroupMemberships;
    }

    public Integer maximumGroupMembershipsInitVal() {
        return _maximumGroupMembershipsInitVal;
    }

	public boolean maximumGroupMembershipsIsDirty() {
        return !valuesAreEqual(_maximumGroupMembershipsInitVal, _maximumGroupMemberships);
    }

    public boolean maximumGroupMembershipsIsSet() {
        return _maximumGroupMembershipsIsSet;
    }	


    public void setMarkedSmavaGroupsBlockVisible(boolean markedSmavaGroupsBlockVisible) {
        if (!_markedSmavaGroupsBlockVisibleIsSet) {
            _markedSmavaGroupsBlockVisibleIsSet = true;
            _markedSmavaGroupsBlockVisibleInitVal = markedSmavaGroupsBlockVisible;
        }
        _markedSmavaGroupsBlockVisible = markedSmavaGroupsBlockVisible;
    }

    public boolean getMarkedSmavaGroupsBlockVisible() {
        return _markedSmavaGroupsBlockVisible;
    }

    public boolean markedSmavaGroupsBlockVisibleInitVal() {
        return _markedSmavaGroupsBlockVisibleInitVal;
    }

	public boolean markedSmavaGroupsBlockVisibleIsDirty() {
        return !valuesAreEqual(_markedSmavaGroupsBlockVisibleInitVal, _markedSmavaGroupsBlockVisible);
    }

    public boolean markedSmavaGroupsBlockVisibleIsSet() {
        return _markedSmavaGroupsBlockVisibleIsSet;
    }	


    public void setSmavaGroupMarkingAllowed(boolean smavaGroupMarkingAllowed) {
        if (!_smavaGroupMarkingAllowedIsSet) {
            _smavaGroupMarkingAllowedIsSet = true;
            _smavaGroupMarkingAllowedInitVal = smavaGroupMarkingAllowed;
        }
        _smavaGroupMarkingAllowed = smavaGroupMarkingAllowed;
    }

    public boolean getSmavaGroupMarkingAllowed() {
        return _smavaGroupMarkingAllowed;
    }

    public boolean smavaGroupMarkingAllowedInitVal() {
        return _smavaGroupMarkingAllowedInitVal;
    }

	public boolean smavaGroupMarkingAllowedIsDirty() {
        return !valuesAreEqual(_smavaGroupMarkingAllowedInitVal, _smavaGroupMarkingAllowed);
    }

    public boolean smavaGroupMarkingAllowedIsSet() {
        return _smavaGroupMarkingAllowedIsSet;
    }	


    public void setSuppressNotifications(boolean suppressNotifications) {
        if (!_suppressNotificationsIsSet) {
            _suppressNotificationsIsSet = true;
            _suppressNotificationsInitVal = suppressNotifications;
        }
        _suppressNotifications = suppressNotifications;
    }

    public boolean getSuppressNotifications() {
        return _suppressNotifications;
    }

    public boolean suppressNotificationsInitVal() {
        return _suppressNotificationsInitVal;
    }

	public boolean suppressNotificationsIsDirty() {
        return !valuesAreEqual(_suppressNotificationsInitVal, _suppressNotifications);
    }

    public boolean suppressNotificationsIsSet() {
        return _suppressNotificationsIsSet;
    }	


    public void setMarketingDefaultInterest(Double marketingDefaultInterest) {
        if (!_marketingDefaultInterestIsSet) {
            _marketingDefaultInterestIsSet = true;
            _marketingDefaultInterestInitVal = marketingDefaultInterest;
        }
        _marketingDefaultInterest = marketingDefaultInterest;
    }

    public Double getMarketingDefaultInterest() {
        return _marketingDefaultInterest;
    }

    public Double marketingDefaultInterestInitVal() {
        return _marketingDefaultInterestInitVal;
    }

	public boolean marketingDefaultInterestIsDirty() {
        return !valuesAreEqual(_marketingDefaultInterestInitVal, _marketingDefaultInterest);
    }

    public boolean marketingDefaultInterestIsSet() {
        return _marketingDefaultInterestIsSet;
    }	


    public void setMarketingDefaultInterestForInterestSlider(Boolean marketingDefaultInterestForInterestSlider) {
        if (!_marketingDefaultInterestForInterestSliderIsSet) {
            _marketingDefaultInterestForInterestSliderIsSet = true;
            _marketingDefaultInterestForInterestSliderInitVal = marketingDefaultInterestForInterestSlider;
        }
        _marketingDefaultInterestForInterestSlider = marketingDefaultInterestForInterestSlider;
    }

    public Boolean getMarketingDefaultInterestForInterestSlider() {
        return _marketingDefaultInterestForInterestSlider;
    }

    public Boolean marketingDefaultInterestForInterestSliderInitVal() {
        return _marketingDefaultInterestForInterestSliderInitVal;
    }

	public boolean marketingDefaultInterestForInterestSliderIsDirty() {
        return !valuesAreEqual(_marketingDefaultInterestForInterestSliderInitVal, _marketingDefaultInterestForInterestSlider);
    }

    public boolean marketingDefaultInterestForInterestSliderIsSet() {
        return _marketingDefaultInterestForInterestSliderIsSet;
    }	


    public void setPaymentProfileDate(String paymentProfileDate) {
        if (!_paymentProfileDateIsSet) {
            _paymentProfileDateIsSet = true;
            _paymentProfileDateInitVal = paymentProfileDate;
        }
        _paymentProfileDate = paymentProfileDate;
    }

    public String getPaymentProfileDate() {
        return _paymentProfileDate;
    }

    public String paymentProfileDateInitVal() {
        return _paymentProfileDateInitVal;
    }

	public boolean paymentProfileDateIsDirty() {
        return !valuesAreEqual(_paymentProfileDateInitVal, _paymentProfileDate);
    }

    public boolean paymentProfileDateIsSet() {
        return _paymentProfileDateIsSet;
    }	


    public void setActivityFeeStartDate(String activityFeeStartDate) {
        if (!_activityFeeStartDateIsSet) {
            _activityFeeStartDateIsSet = true;
            _activityFeeStartDateInitVal = activityFeeStartDate;
        }
        _activityFeeStartDate = activityFeeStartDate;
    }

    public String getActivityFeeStartDate() {
        return _activityFeeStartDate;
    }

    public String activityFeeStartDateInitVal() {
        return _activityFeeStartDateInitVal;
    }

	public boolean activityFeeStartDateIsDirty() {
        return !valuesAreEqual(_activityFeeStartDateInitVal, _activityFeeStartDate);
    }

    public boolean activityFeeStartDateIsSet() {
        return _activityFeeStartDateIsSet;
    }	


    public void setPostidentFeeStartDate(String postidentFeeStartDate) {
        if (!_postidentFeeStartDateIsSet) {
            _postidentFeeStartDateIsSet = true;
            _postidentFeeStartDateInitVal = postidentFeeStartDate;
        }
        _postidentFeeStartDate = postidentFeeStartDate;
    }

    public String getPostidentFeeStartDate() {
        return _postidentFeeStartDate;
    }

    public String postidentFeeStartDateInitVal() {
        return _postidentFeeStartDateInitVal;
    }

	public boolean postidentFeeStartDateIsDirty() {
        return !valuesAreEqual(_postidentFeeStartDateInitVal, _postidentFeeStartDate);
    }

    public boolean postidentFeeStartDateIsSet() {
        return _postidentFeeStartDateIsSet;
    }	


    public void setFakeDate(String fakeDate) {
        if (!_fakeDateIsSet) {
            _fakeDateIsSet = true;
            _fakeDateInitVal = fakeDate;
        }
        _fakeDate = fakeDate;
    }

    public String getFakeDate() {
        return _fakeDate;
    }

    public String fakeDateInitVal() {
        return _fakeDateInitVal;
    }

	public boolean fakeDateIsDirty() {
        return !valuesAreEqual(_fakeDateInitVal, _fakeDate);
    }

    public boolean fakeDateIsSet() {
        return _fakeDateIsSet;
    }	


    public void setUseFakeDate(boolean useFakeDate) {
        if (!_useFakeDateIsSet) {
            _useFakeDateIsSet = true;
            _useFakeDateInitVal = useFakeDate;
        }
        _useFakeDate = useFakeDate;
    }

    public boolean getUseFakeDate() {
        return _useFakeDate;
    }

    public boolean useFakeDateInitVal() {
        return _useFakeDateInitVal;
    }

	public boolean useFakeDateIsDirty() {
        return !valuesAreEqual(_useFakeDateInitVal, _useFakeDate);
    }

    public boolean useFakeDateIsSet() {
        return _useFakeDateIsSet;
    }	


    public void setPartialMatchAcceptanceLimit(String partialMatchAcceptanceLimit) {
        if (!_partialMatchAcceptanceLimitIsSet) {
            _partialMatchAcceptanceLimitIsSet = true;
            _partialMatchAcceptanceLimitInitVal = partialMatchAcceptanceLimit;
        }
        _partialMatchAcceptanceLimit = partialMatchAcceptanceLimit;
    }

    public String getPartialMatchAcceptanceLimit() {
        return _partialMatchAcceptanceLimit;
    }

    public String partialMatchAcceptanceLimitInitVal() {
        return _partialMatchAcceptanceLimitInitVal;
    }

	public boolean partialMatchAcceptanceLimitIsDirty() {
        return !valuesAreEqual(_partialMatchAcceptanceLimitInitVal, _partialMatchAcceptanceLimit);
    }

    public boolean partialMatchAcceptanceLimitIsSet() {
        return _partialMatchAcceptanceLimitIsSet;
    }	


    public void setBorrowerDelayedPaymentLimitDays(Integer borrowerDelayedPaymentLimitDays) {
        if (!_borrowerDelayedPaymentLimitDaysIsSet) {
            _borrowerDelayedPaymentLimitDaysIsSet = true;
            _borrowerDelayedPaymentLimitDaysInitVal = borrowerDelayedPaymentLimitDays;
        }
        _borrowerDelayedPaymentLimitDays = borrowerDelayedPaymentLimitDays;
    }

    public Integer getBorrowerDelayedPaymentLimitDays() {
        return _borrowerDelayedPaymentLimitDays;
    }

    public Integer borrowerDelayedPaymentLimitDaysInitVal() {
        return _borrowerDelayedPaymentLimitDaysInitVal;
    }

	public boolean borrowerDelayedPaymentLimitDaysIsDirty() {
        return !valuesAreEqual(_borrowerDelayedPaymentLimitDaysInitVal, _borrowerDelayedPaymentLimitDays);
    }

    public boolean borrowerDelayedPaymentLimitDaysIsSet() {
        return _borrowerDelayedPaymentLimitDaysIsSet;
    }	


    public void setSchufaTokenRateCountPeriod(Integer schufaTokenRateCountPeriod) {
        if (!_schufaTokenRateCountPeriodIsSet) {
            _schufaTokenRateCountPeriodIsSet = true;
            _schufaTokenRateCountPeriodInitVal = schufaTokenRateCountPeriod;
        }
        _schufaTokenRateCountPeriod = schufaTokenRateCountPeriod;
    }

    public Integer getSchufaTokenRateCountPeriod() {
        return _schufaTokenRateCountPeriod;
    }

    public Integer schufaTokenRateCountPeriodInitVal() {
        return _schufaTokenRateCountPeriodInitVal;
    }

	public boolean schufaTokenRateCountPeriodIsDirty() {
        return !valuesAreEqual(_schufaTokenRateCountPeriodInitVal, _schufaTokenRateCountPeriod);
    }

    public boolean schufaTokenRateCountPeriodIsSet() {
        return _schufaTokenRateCountPeriodIsSet;
    }	


    public void setSchufaTokenDefaultHypoInterest(String schufaTokenDefaultHypoInterest) {
        if (!_schufaTokenDefaultHypoInterestIsSet) {
            _schufaTokenDefaultHypoInterestIsSet = true;
            _schufaTokenDefaultHypoInterestInitVal = schufaTokenDefaultHypoInterest;
        }
        _schufaTokenDefaultHypoInterest = schufaTokenDefaultHypoInterest;
    }

    public String getSchufaTokenDefaultHypoInterest() {
        return _schufaTokenDefaultHypoInterest;
    }

    public String schufaTokenDefaultHypoInterestInitVal() {
        return _schufaTokenDefaultHypoInterestInitVal;
    }

	public boolean schufaTokenDefaultHypoInterestIsDirty() {
        return !valuesAreEqual(_schufaTokenDefaultHypoInterestInitVal, _schufaTokenDefaultHypoInterest);
    }

    public boolean schufaTokenDefaultHypoInterestIsSet() {
        return _schufaTokenDefaultHypoInterestIsSet;
    }	


    public void setSchufaTokenDefaultCreditInterest(String schufaTokenDefaultCreditInterest) {
        if (!_schufaTokenDefaultCreditInterestIsSet) {
            _schufaTokenDefaultCreditInterestIsSet = true;
            _schufaTokenDefaultCreditInterestInitVal = schufaTokenDefaultCreditInterest;
        }
        _schufaTokenDefaultCreditInterest = schufaTokenDefaultCreditInterest;
    }

    public String getSchufaTokenDefaultCreditInterest() {
        return _schufaTokenDefaultCreditInterest;
    }

    public String schufaTokenDefaultCreditInterestInitVal() {
        return _schufaTokenDefaultCreditInterestInitVal;
    }

	public boolean schufaTokenDefaultCreditInterestIsDirty() {
        return !valuesAreEqual(_schufaTokenDefaultCreditInterestInitVal, _schufaTokenDefaultCreditInterest);
    }

    public boolean schufaTokenDefaultCreditInterestIsSet() {
        return _schufaTokenDefaultCreditInterestIsSet;
    }	


    public void setFirstRepaymentReminderTerm(Integer firstRepaymentReminderTerm) {
        if (!_firstRepaymentReminderTermIsSet) {
            _firstRepaymentReminderTermIsSet = true;
            _firstRepaymentReminderTermInitVal = firstRepaymentReminderTerm;
        }
        _firstRepaymentReminderTerm = firstRepaymentReminderTerm;
    }

    public Integer getFirstRepaymentReminderTerm() {
        return _firstRepaymentReminderTerm;
    }

    public Integer firstRepaymentReminderTermInitVal() {
        return _firstRepaymentReminderTermInitVal;
    }

	public boolean firstRepaymentReminderTermIsDirty() {
        return !valuesAreEqual(_firstRepaymentReminderTermInitVal, _firstRepaymentReminderTerm);
    }

    public boolean firstRepaymentReminderTermIsSet() {
        return _firstRepaymentReminderTermIsSet;
    }	


    public void setDelayedRepaymentReminderTerm(Integer delayedRepaymentReminderTerm) {
        if (!_delayedRepaymentReminderTermIsSet) {
            _delayedRepaymentReminderTermIsSet = true;
            _delayedRepaymentReminderTermInitVal = delayedRepaymentReminderTerm;
        }
        _delayedRepaymentReminderTerm = delayedRepaymentReminderTerm;
    }

    public Integer getDelayedRepaymentReminderTerm() {
        return _delayedRepaymentReminderTerm;
    }

    public Integer delayedRepaymentReminderTermInitVal() {
        return _delayedRepaymentReminderTermInitVal;
    }

	public boolean delayedRepaymentReminderTermIsDirty() {
        return !valuesAreEqual(_delayedRepaymentReminderTermInitVal, _delayedRepaymentReminderTerm);
    }

    public boolean delayedRepaymentReminderTermIsSet() {
        return _delayedRepaymentReminderTermIsSet;
    }	


    public void setMaximumActiveDays(Integer maximumActiveDays) {
        if (!_maximumActiveDaysIsSet) {
            _maximumActiveDaysIsSet = true;
            _maximumActiveDaysInitVal = maximumActiveDays;
        }
        _maximumActiveDays = maximumActiveDays;
    }

    public Integer getMaximumActiveDays() {
        return _maximumActiveDays;
    }

    public Integer maximumActiveDaysInitVal() {
        return _maximumActiveDaysInitVal;
    }

	public boolean maximumActiveDaysIsDirty() {
        return !valuesAreEqual(_maximumActiveDaysInitVal, _maximumActiveDays);
    }

    public boolean maximumActiveDaysIsSet() {
        return _maximumActiveDaysIsSet;
    }	


    public void setMinimumFinancedRate(Float minimumFinancedRate) {
        if (!_minimumFinancedRateIsSet) {
            _minimumFinancedRateIsSet = true;
            _minimumFinancedRateInitVal = minimumFinancedRate;
        }
        _minimumFinancedRate = minimumFinancedRate;
    }

    public Float getMinimumFinancedRate() {
        return _minimumFinancedRate;
    }

    public Float minimumFinancedRateInitVal() {
        return _minimumFinancedRateInitVal;
    }

	public boolean minimumFinancedRateIsDirty() {
        return !valuesAreEqual(_minimumFinancedRateInitVal, _minimumFinancedRate);
    }

    public boolean minimumFinancedRateIsSet() {
        return _minimumFinancedRateIsSet;
    }	


    public void setScoreObsolete(Integer scoreObsolete) {
        if (!_scoreObsoleteIsSet) {
            _scoreObsoleteIsSet = true;
            _scoreObsoleteInitVal = scoreObsolete;
        }
        _scoreObsolete = scoreObsolete;
    }

    public Integer getScoreObsolete() {
        return _scoreObsolete;
    }

    public Integer scoreObsoleteInitVal() {
        return _scoreObsoleteInitVal;
    }

	public boolean scoreObsoleteIsDirty() {
        return !valuesAreEqual(_scoreObsoleteInitVal, _scoreObsolete);
    }

    public boolean scoreObsoleteIsSet() {
        return _scoreObsoleteIsSet;
    }	


    public void setApprovalObsolete(Integer approvalObsolete) {
        if (!_approvalObsoleteIsSet) {
            _approvalObsoleteIsSet = true;
            _approvalObsoleteInitVal = approvalObsolete;
        }
        _approvalObsolete = approvalObsolete;
    }

    public Integer getApprovalObsolete() {
        return _approvalObsolete;
    }

    public Integer approvalObsoleteInitVal() {
        return _approvalObsoleteInitVal;
    }

	public boolean approvalObsoleteIsDirty() {
        return !valuesAreEqual(_approvalObsoleteInitVal, _approvalObsolete);
    }

    public boolean approvalObsoleteIsSet() {
        return _approvalObsoleteIsSet;
    }	


    public void setUseDta(Boolean useDta) {
        if (!_useDtaIsSet) {
            _useDtaIsSet = true;
            _useDtaInitVal = useDta;
        }
        _useDta = useDta;
    }

    public Boolean getUseDta() {
        return _useDta;
    }

    public Boolean useDtaInitVal() {
        return _useDtaInitVal;
    }

	public boolean useDtaIsDirty() {
        return !valuesAreEqual(_useDtaInitVal, _useDta);
    }

    public boolean useDtaIsSet() {
        return _useDtaIsSet;
    }	


    public void setUseDtaFormat(Boolean useDtaFormat) {
        if (!_useDtaFormatIsSet) {
            _useDtaFormatIsSet = true;
            _useDtaFormatInitVal = useDtaFormat;
        }
        _useDtaFormat = useDtaFormat;
    }

    public Boolean getUseDtaFormat() {
        return _useDtaFormat;
    }

    public Boolean useDtaFormatInitVal() {
        return _useDtaFormatInitVal;
    }

	public boolean useDtaFormatIsDirty() {
        return !valuesAreEqual(_useDtaFormatInitVal, _useDtaFormat);
    }

    public boolean useDtaFormatIsSet() {
        return _useDtaFormatIsSet;
    }	


    public void setPinGenerationType(Boolean pinGenerationType) {
        if (!_pinGenerationTypeIsSet) {
            _pinGenerationTypeIsSet = true;
            _pinGenerationTypeInitVal = pinGenerationType;
        }
        _pinGenerationType = pinGenerationType;
    }

    public Boolean getPinGenerationType() {
        return _pinGenerationType;
    }

    public Boolean pinGenerationTypeInitVal() {
        return _pinGenerationTypeInitVal;
    }

	public boolean pinGenerationTypeIsDirty() {
        return !valuesAreEqual(_pinGenerationTypeInitVal, _pinGenerationType);
    }

    public boolean pinGenerationTypeIsSet() {
        return _pinGenerationTypeIsSet;
    }	


    public void setUseSchufa(Boolean useSchufa) {
        if (!_useSchufaIsSet) {
            _useSchufaIsSet = true;
            _useSchufaInitVal = useSchufa;
        }
        _useSchufa = useSchufa;
    }

    public Boolean getUseSchufa() {
        return _useSchufa;
    }

    public Boolean useSchufaInitVal() {
        return _useSchufaInitVal;
    }

	public boolean useSchufaIsDirty() {
        return !valuesAreEqual(_useSchufaInitVal, _useSchufa);
    }

    public boolean useSchufaIsSet() {
        return _useSchufaIsSet;
    }	


    public void setNeedBankData(Boolean needBankData) {
        if (!_needBankDataIsSet) {
            _needBankDataIsSet = true;
            _needBankDataInitVal = needBankData;
        }
        _needBankData = needBankData;
    }

    public Boolean getNeedBankData() {
        return _needBankData;
    }

    public Boolean needBankDataInitVal() {
        return _needBankDataInitVal;
    }

	public boolean needBankDataIsDirty() {
        return !valuesAreEqual(_needBankDataInitVal, _needBankData);
    }

    public boolean needBankDataIsSet() {
        return _needBankDataIsSet;
    }	


    public void setAutomaticInternalBankAccountGeneration(Boolean automaticInternalBankAccountGeneration) {
        if (!_automaticInternalBankAccountGenerationIsSet) {
            _automaticInternalBankAccountGenerationIsSet = true;
            _automaticInternalBankAccountGenerationInitVal = automaticInternalBankAccountGeneration;
        }
        _automaticInternalBankAccountGeneration = automaticInternalBankAccountGeneration;
    }

    public Boolean getAutomaticInternalBankAccountGeneration() {
        return _automaticInternalBankAccountGeneration;
    }

    public Boolean automaticInternalBankAccountGenerationInitVal() {
        return _automaticInternalBankAccountGenerationInitVal;
    }

	public boolean automaticInternalBankAccountGenerationIsDirty() {
        return !valuesAreEqual(_automaticInternalBankAccountGenerationInitVal, _automaticInternalBankAccountGeneration);
    }

    public boolean automaticInternalBankAccountGenerationIsSet() {
        return _automaticInternalBankAccountGenerationIsSet;
    }	


    public void setZus(double zus) {
        if (!_zusIsSet) {
            _zusIsSet = true;
            _zusInitVal = zus;
        }
        _zus = zus;
    }

    public double getZus() {
        return _zus;
    }

    public double zusInitVal() {
        return _zusInitVal;
    }

	public boolean zusIsDirty() {
        return !valuesAreEqual(_zusInitVal, _zus);
    }

    public boolean zusIsSet() {
        return _zusIsSet;
    }	


    public void setPoolCompensation(Boolean poolCompensation) {
        if (!_poolCompensationIsSet) {
            _poolCompensationIsSet = true;
            _poolCompensationInitVal = poolCompensation;
        }
        _poolCompensation = poolCompensation;
    }

    public Boolean getPoolCompensation() {
        return _poolCompensation;
    }

    public Boolean poolCompensationInitVal() {
        return _poolCompensationInitVal;
    }

	public boolean poolCompensationIsDirty() {
        return !valuesAreEqual(_poolCompensationInitVal, _poolCompensation);
    }

    public boolean poolCompensationIsSet() {
        return _poolCompensationIsSet;
    }	


    public void setExternalLenderPayout(Boolean externalLenderPayout) {
        if (!_externalLenderPayoutIsSet) {
            _externalLenderPayoutIsSet = true;
            _externalLenderPayoutInitVal = externalLenderPayout;
        }
        _externalLenderPayout = externalLenderPayout;
    }

    public Boolean getExternalLenderPayout() {
        return _externalLenderPayout;
    }

    public Boolean externalLenderPayoutInitVal() {
        return _externalLenderPayoutInitVal;
    }

	public boolean externalLenderPayoutIsDirty() {
        return !valuesAreEqual(_externalLenderPayoutInitVal, _externalLenderPayout);
    }

    public boolean externalLenderPayoutIsSet() {
        return _externalLenderPayoutIsSet;
    }	


    public void setLenderTransactionFeeDeposit(double lenderTransactionFeeDeposit) {
        if (!_lenderTransactionFeeDepositIsSet) {
            _lenderTransactionFeeDepositIsSet = true;
            _lenderTransactionFeeDepositInitVal = lenderTransactionFeeDeposit;
        }
        _lenderTransactionFeeDeposit = lenderTransactionFeeDeposit;
    }

    public double getLenderTransactionFeeDeposit() {
        return _lenderTransactionFeeDeposit;
    }

    public double lenderTransactionFeeDepositInitVal() {
        return _lenderTransactionFeeDepositInitVal;
    }

	public boolean lenderTransactionFeeDepositIsDirty() {
        return !valuesAreEqual(_lenderTransactionFeeDepositInitVal, _lenderTransactionFeeDeposit);
    }

    public boolean lenderTransactionFeeDepositIsSet() {
        return _lenderTransactionFeeDepositIsSet;
    }	


    public void setLenderTransactionFeePayout(double lenderTransactionFeePayout) {
        if (!_lenderTransactionFeePayoutIsSet) {
            _lenderTransactionFeePayoutIsSet = true;
            _lenderTransactionFeePayoutInitVal = lenderTransactionFeePayout;
        }
        _lenderTransactionFeePayout = lenderTransactionFeePayout;
    }

    public double getLenderTransactionFeePayout() {
        return _lenderTransactionFeePayout;
    }

    public double lenderTransactionFeePayoutInitVal() {
        return _lenderTransactionFeePayoutInitVal;
    }

	public boolean lenderTransactionFeePayoutIsDirty() {
        return !valuesAreEqual(_lenderTransactionFeePayoutInitVal, _lenderTransactionFeePayout);
    }

    public boolean lenderTransactionFeePayoutIsSet() {
        return _lenderTransactionFeePayoutIsSet;
    }	


    public void setBorrowerTransactionFeeDeposit(double borrowerTransactionFeeDeposit) {
        if (!_borrowerTransactionFeeDepositIsSet) {
            _borrowerTransactionFeeDepositIsSet = true;
            _borrowerTransactionFeeDepositInitVal = borrowerTransactionFeeDeposit;
        }
        _borrowerTransactionFeeDeposit = borrowerTransactionFeeDeposit;
    }

    public double getBorrowerTransactionFeeDeposit() {
        return _borrowerTransactionFeeDeposit;
    }

    public double borrowerTransactionFeeDepositInitVal() {
        return _borrowerTransactionFeeDepositInitVal;
    }

	public boolean borrowerTransactionFeeDepositIsDirty() {
        return !valuesAreEqual(_borrowerTransactionFeeDepositInitVal, _borrowerTransactionFeeDeposit);
    }

    public boolean borrowerTransactionFeeDepositIsSet() {
        return _borrowerTransactionFeeDepositIsSet;
    }	


    public void setBorrowerTransactionFeePayout(double borrowerTransactionFeePayout) {
        if (!_borrowerTransactionFeePayoutIsSet) {
            _borrowerTransactionFeePayoutIsSet = true;
            _borrowerTransactionFeePayoutInitVal = borrowerTransactionFeePayout;
        }
        _borrowerTransactionFeePayout = borrowerTransactionFeePayout;
    }

    public double getBorrowerTransactionFeePayout() {
        return _borrowerTransactionFeePayout;
    }

    public double borrowerTransactionFeePayoutInitVal() {
        return _borrowerTransactionFeePayoutInitVal;
    }

	public boolean borrowerTransactionFeePayoutIsDirty() {
        return !valuesAreEqual(_borrowerTransactionFeePayoutInitVal, _borrowerTransactionFeePayout);
    }

    public boolean borrowerTransactionFeePayoutIsSet() {
        return _borrowerTransactionFeePayoutIsSet;
    }	


    public void setTransactionMappingType(String transactionMappingType) {
        if (!_transactionMappingTypeIsSet) {
            _transactionMappingTypeIsSet = true;
            _transactionMappingTypeInitVal = transactionMappingType;
        }
        _transactionMappingType = transactionMappingType;
    }

    public String getTransactionMappingType() {
        return _transactionMappingType;
    }

    public String transactionMappingTypeInitVal() {
        return _transactionMappingTypeInitVal;
    }

	public boolean transactionMappingTypeIsDirty() {
        return !valuesAreEqual(_transactionMappingTypeInitVal, _transactionMappingType);
    }

    public boolean transactionMappingTypeIsSet() {
        return _transactionMappingTypeIsSet;
    }	


    public void setBlacklistFee(double blacklistFee) {
        if (!_blacklistFeeIsSet) {
            _blacklistFeeIsSet = true;
            _blacklistFeeInitVal = blacklistFee;
        }
        _blacklistFee = blacklistFee;
    }

    public double getBlacklistFee() {
        return _blacklistFee;
    }

    public double blacklistFeeInitVal() {
        return _blacklistFeeInitVal;
    }

	public boolean blacklistFeeIsDirty() {
        return !valuesAreEqual(_blacklistFeeInitVal, _blacklistFee);
    }

    public boolean blacklistFeeIsSet() {
        return _blacklistFeeIsSet;
    }	


    public void setBlacklistCheckValidity(Integer blacklistCheckValidity) {
        if (!_blacklistCheckValidityIsSet) {
            _blacklistCheckValidityIsSet = true;
            _blacklistCheckValidityInitVal = blacklistCheckValidity;
        }
        _blacklistCheckValidity = blacklistCheckValidity;
    }

    public Integer getBlacklistCheckValidity() {
        return _blacklistCheckValidity;
    }

    public Integer blacklistCheckValidityInitVal() {
        return _blacklistCheckValidityInitVal;
    }

	public boolean blacklistCheckValidityIsDirty() {
        return !valuesAreEqual(_blacklistCheckValidityInitVal, _blacklistCheckValidity);
    }

    public boolean blacklistCheckValidityIsSet() {
        return _blacklistCheckValidityIsSet;
    }	


    public void setExcludeInternalTransactions(Boolean excludeInternalTransactions) {
        if (!_excludeInternalTransactionsIsSet) {
            _excludeInternalTransactionsIsSet = true;
            _excludeInternalTransactionsInitVal = excludeInternalTransactions;
        }
        _excludeInternalTransactions = excludeInternalTransactions;
    }

    public Boolean getExcludeInternalTransactions() {
        return _excludeInternalTransactions;
    }

    public Boolean excludeInternalTransactionsInitVal() {
        return _excludeInternalTransactionsInitVal;
    }

	public boolean excludeInternalTransactionsIsDirty() {
        return !valuesAreEqual(_excludeInternalTransactionsInitVal, _excludeInternalTransactions);
    }

    public boolean excludeInternalTransactionsIsSet() {
        return _excludeInternalTransactionsIsSet;
    }	


    public void setIncludeSchufaCreditDefaultRates(Boolean includeSchufaCreditDefaultRates) {
        if (!_includeSchufaCreditDefaultRatesIsSet) {
            _includeSchufaCreditDefaultRatesIsSet = true;
            _includeSchufaCreditDefaultRatesInitVal = includeSchufaCreditDefaultRates;
        }
        _includeSchufaCreditDefaultRates = includeSchufaCreditDefaultRates;
    }

    public Boolean getIncludeSchufaCreditDefaultRates() {
        return _includeSchufaCreditDefaultRates;
    }

    public Boolean includeSchufaCreditDefaultRatesInitVal() {
        return _includeSchufaCreditDefaultRatesInitVal;
    }

	public boolean includeSchufaCreditDefaultRatesIsDirty() {
        return !valuesAreEqual(_includeSchufaCreditDefaultRatesInitVal, _includeSchufaCreditDefaultRates);
    }

    public boolean includeSchufaCreditDefaultRatesIsSet() {
        return _includeSchufaCreditDefaultRatesIsSet;
    }	


    public void setUseRoiSimulation(Boolean useRoiSimulation) {
        if (!_useRoiSimulationIsSet) {
            _useRoiSimulationIsSet = true;
            _useRoiSimulationInitVal = useRoiSimulation;
        }
        _useRoiSimulation = useRoiSimulation;
    }

    public Boolean getUseRoiSimulation() {
        return _useRoiSimulation;
    }

    public Boolean useRoiSimulationInitVal() {
        return _useRoiSimulationInitVal;
    }

	public boolean useRoiSimulationIsDirty() {
        return !valuesAreEqual(_useRoiSimulationInitVal, _useRoiSimulation);
    }

    public boolean useRoiSimulationIsSet() {
        return _useRoiSimulationIsSet;
    }	


    public void setExternalBorrowerRepaymentTransactionType(String externalBorrowerRepaymentTransactionType) {
        if (!_externalBorrowerRepaymentTransactionTypeIsSet) {
            _externalBorrowerRepaymentTransactionTypeIsSet = true;
            _externalBorrowerRepaymentTransactionTypeInitVal = externalBorrowerRepaymentTransactionType;
        }
        _externalBorrowerRepaymentTransactionType = externalBorrowerRepaymentTransactionType;
    }

    public String getExternalBorrowerRepaymentTransactionType() {
        return _externalBorrowerRepaymentTransactionType;
    }

    public String externalBorrowerRepaymentTransactionTypeInitVal() {
        return _externalBorrowerRepaymentTransactionTypeInitVal;
    }

	public boolean externalBorrowerRepaymentTransactionTypeIsDirty() {
        return !valuesAreEqual(_externalBorrowerRepaymentTransactionTypeInitVal, _externalBorrowerRepaymentTransactionType);
    }

    public boolean externalBorrowerRepaymentTransactionTypeIsSet() {
        return _externalBorrowerRepaymentTransactionTypeIsSet;
    }	


    public void setMinimumWorkExperience(Integer minimumWorkExperience) {
        if (!_minimumWorkExperienceIsSet) {
            _minimumWorkExperienceIsSet = true;
            _minimumWorkExperienceInitVal = minimumWorkExperience;
        }
        _minimumWorkExperience = minimumWorkExperience;
    }

    public Integer getMinimumWorkExperience() {
        return _minimumWorkExperience;
    }

    public Integer minimumWorkExperienceInitVal() {
        return _minimumWorkExperienceInitVal;
    }

	public boolean minimumWorkExperienceIsDirty() {
        return !valuesAreEqual(_minimumWorkExperienceInitVal, _minimumWorkExperience);
    }

    public boolean minimumWorkExperienceIsSet() {
        return _minimumWorkExperienceIsSet;
    }	


    public void setDefaultSeoPlacementId(Long defaultSeoPlacementId) {
        if (!_defaultSeoPlacementIdIsSet) {
            _defaultSeoPlacementIdIsSet = true;
            _defaultSeoPlacementIdInitVal = defaultSeoPlacementId;
        }
        _defaultSeoPlacementId = defaultSeoPlacementId;
    }

    public Long getDefaultSeoPlacementId() {
        return _defaultSeoPlacementId;
    }

    public Long defaultSeoPlacementIdInitVal() {
        return _defaultSeoPlacementIdInitVal;
    }

	public boolean defaultSeoPlacementIdIsDirty() {
        return !valuesAreEqual(_defaultSeoPlacementIdInitVal, _defaultSeoPlacementId);
    }

    public boolean defaultSeoPlacementIdIsSet() {
        return _defaultSeoPlacementIdIsSet;
    }	


    public void setDefaultInvitationPlacementId(Long defaultInvitationPlacementId) {
        if (!_defaultInvitationPlacementIdIsSet) {
            _defaultInvitationPlacementIdIsSet = true;
            _defaultInvitationPlacementIdInitVal = defaultInvitationPlacementId;
        }
        _defaultInvitationPlacementId = defaultInvitationPlacementId;
    }

    public Long getDefaultInvitationPlacementId() {
        return _defaultInvitationPlacementId;
    }

    public Long defaultInvitationPlacementIdInitVal() {
        return _defaultInvitationPlacementIdInitVal;
    }

	public boolean defaultInvitationPlacementIdIsDirty() {
        return !valuesAreEqual(_defaultInvitationPlacementIdInitVal, _defaultInvitationPlacementId);
    }

    public boolean defaultInvitationPlacementIdIsSet() {
        return _defaultInvitationPlacementIdIsSet;
    }	


    public void setSeoKeywordsToExclude(String seoKeywordsToExclude) {
        if (!_seoKeywordsToExcludeIsSet) {
            _seoKeywordsToExcludeIsSet = true;
            _seoKeywordsToExcludeInitVal = seoKeywordsToExclude;
        }
        _seoKeywordsToExclude = seoKeywordsToExclude;
    }

    public String getSeoKeywordsToExclude() {
        return _seoKeywordsToExclude;
    }

    public String seoKeywordsToExcludeInitVal() {
        return _seoKeywordsToExcludeInitVal;
    }

	public boolean seoKeywordsToExcludeIsDirty() {
        return !valuesAreEqual(_seoKeywordsToExcludeInitVal, _seoKeywordsToExclude);
    }

    public boolean seoKeywordsToExcludeIsSet() {
        return _seoKeywordsToExcludeIsSet;
    }	


    public void setBaseAmountForRoiCalculation(Double baseAmountForRoiCalculation) {
        if (!_baseAmountForRoiCalculationIsSet) {
            _baseAmountForRoiCalculationIsSet = true;
            _baseAmountForRoiCalculationInitVal = baseAmountForRoiCalculation;
        }
        _baseAmountForRoiCalculation = baseAmountForRoiCalculation;
    }

    public Double getBaseAmountForRoiCalculation() {
        return _baseAmountForRoiCalculation;
    }

    public Double baseAmountForRoiCalculationInitVal() {
        return _baseAmountForRoiCalculationInitVal;
    }

	public boolean baseAmountForRoiCalculationIsDirty() {
        return !valuesAreEqual(_baseAmountForRoiCalculationInitVal, _baseAmountForRoiCalculation);
    }

    public boolean baseAmountForRoiCalculationIsSet() {
        return _baseAmountForRoiCalculationIsSet;
    }	


    public void setShowExtendedBidAssistantView(boolean showExtendedBidAssistantView) {
        if (!_showExtendedBidAssistantViewIsSet) {
            _showExtendedBidAssistantViewIsSet = true;
            _showExtendedBidAssistantViewInitVal = showExtendedBidAssistantView;
        }
        _showExtendedBidAssistantView = showExtendedBidAssistantView;
    }

    public boolean getShowExtendedBidAssistantView() {
        return _showExtendedBidAssistantView;
    }

    public boolean showExtendedBidAssistantViewInitVal() {
        return _showExtendedBidAssistantViewInitVal;
    }

	public boolean showExtendedBidAssistantViewIsDirty() {
        return !valuesAreEqual(_showExtendedBidAssistantViewInitVal, _showExtendedBidAssistantView);
    }

    public boolean showExtendedBidAssistantViewIsSet() {
        return _showExtendedBidAssistantViewIsSet;
    }	


    public void setValidityOfTheCreditOfferTerm(Integer validityOfTheCreditOfferTerm) {
        if (!_validityOfTheCreditOfferTermIsSet) {
            _validityOfTheCreditOfferTermIsSet = true;
            _validityOfTheCreditOfferTermInitVal = validityOfTheCreditOfferTerm;
        }
        _validityOfTheCreditOfferTerm = validityOfTheCreditOfferTerm;
    }

    public Integer getValidityOfTheCreditOfferTerm() {
        return _validityOfTheCreditOfferTerm;
    }

    public Integer validityOfTheCreditOfferTermInitVal() {
        return _validityOfTheCreditOfferTermInitVal;
    }

	public boolean validityOfTheCreditOfferTermIsDirty() {
        return !valuesAreEqual(_validityOfTheCreditOfferTermInitVal, _validityOfTheCreditOfferTerm);
    }

    public boolean validityOfTheCreditOfferTermIsSet() {
        return _validityOfTheCreditOfferTermIsSet;
    }	


    public void setDefaultCreditAmount(double defaultCreditAmount) {
        if (!_defaultCreditAmountIsSet) {
            _defaultCreditAmountIsSet = true;
            _defaultCreditAmountInitVal = defaultCreditAmount;
        }
        _defaultCreditAmount = defaultCreditAmount;
    }

    public double getDefaultCreditAmount() {
        return _defaultCreditAmount;
    }

    public double defaultCreditAmountInitVal() {
        return _defaultCreditAmountInitVal;
    }

	public boolean defaultCreditAmountIsDirty() {
        return !valuesAreEqual(_defaultCreditAmountInitVal, _defaultCreditAmount);
    }

    public boolean defaultCreditAmountIsSet() {
        return _defaultCreditAmountIsSet;
    }	


    public void setDefaultCreditDuration(Integer defaultCreditDuration) {
        if (!_defaultCreditDurationIsSet) {
            _defaultCreditDurationIsSet = true;
            _defaultCreditDurationInitVal = defaultCreditDuration;
        }
        _defaultCreditDuration = defaultCreditDuration;
    }

    public Integer getDefaultCreditDuration() {
        return _defaultCreditDuration;
    }

    public Integer defaultCreditDurationInitVal() {
        return _defaultCreditDurationInitVal;
    }

	public boolean defaultCreditDurationIsDirty() {
        return !valuesAreEqual(_defaultCreditDurationInitVal, _defaultCreditDuration);
    }

    public boolean defaultCreditDurationIsSet() {
        return _defaultCreditDurationIsSet;
    }	


    public void setDefaultRdiContractType(String defaultRdiContractType) {
        if (!_defaultRdiContractTypeIsSet) {
            _defaultRdiContractTypeIsSet = true;
            _defaultRdiContractTypeInitVal = defaultRdiContractType;
        }
        _defaultRdiContractType = defaultRdiContractType;
    }

    public String getDefaultRdiContractType() {
        return _defaultRdiContractType;
    }

    public String defaultRdiContractTypeInitVal() {
        return _defaultRdiContractTypeInitVal;
    }

	public boolean defaultRdiContractTypeIsDirty() {
        return !valuesAreEqual(_defaultRdiContractTypeInitVal, _defaultRdiContractType);
    }

    public boolean defaultRdiContractTypeIsSet() {
        return _defaultRdiContractTypeIsSet;
    }	


    public void setActivateRapidMatching(Boolean activateRapidMatching) {
        if (!_activateRapidMatchingIsSet) {
            _activateRapidMatchingIsSet = true;
            _activateRapidMatchingInitVal = activateRapidMatching;
        }
        _activateRapidMatching = activateRapidMatching;
    }

    public Boolean getActivateRapidMatching() {
        return _activateRapidMatching;
    }

    public Boolean activateRapidMatchingInitVal() {
        return _activateRapidMatchingInitVal;
    }

	public boolean activateRapidMatchingIsDirty() {
        return !valuesAreEqual(_activateRapidMatchingInitVal, _activateRapidMatching);
    }

    public boolean activateRapidMatchingIsSet() {
        return _activateRapidMatchingIsSet;
    }	


    public void setActivateRapidMatchingForSoap(boolean activateRapidMatchingForSoap) {
        if (!_activateRapidMatchingForSoapIsSet) {
            _activateRapidMatchingForSoapIsSet = true;
            _activateRapidMatchingForSoapInitVal = activateRapidMatchingForSoap;
        }
        _activateRapidMatchingForSoap = activateRapidMatchingForSoap;
    }

    public boolean getActivateRapidMatchingForSoap() {
        return _activateRapidMatchingForSoap;
    }

    public boolean activateRapidMatchingForSoapInitVal() {
        return _activateRapidMatchingForSoapInitVal;
    }

	public boolean activateRapidMatchingForSoapIsDirty() {
        return !valuesAreEqual(_activateRapidMatchingForSoapInitVal, _activateRapidMatchingForSoap);
    }

    public boolean activateRapidMatchingForSoapIsSet() {
        return _activateRapidMatchingForSoapIsSet;
    }	


    public void setMinimumRoiPortfolio(String minimumRoiPortfolio) {
        if (!_minimumRoiPortfolioIsSet) {
            _minimumRoiPortfolioIsSet = true;
            _minimumRoiPortfolioInitVal = minimumRoiPortfolio;
        }
        _minimumRoiPortfolio = minimumRoiPortfolio;
    }

    public String getMinimumRoiPortfolio() {
        return _minimumRoiPortfolio;
    }

    public String minimumRoiPortfolioInitVal() {
        return _minimumRoiPortfolioInitVal;
    }

	public boolean minimumRoiPortfolioIsDirty() {
        return !valuesAreEqual(_minimumRoiPortfolioInitVal, _minimumRoiPortfolio);
    }

    public boolean minimumRoiPortfolioIsSet() {
        return _minimumRoiPortfolioIsSet;
    }	


    public void setRepaymentBreakEnabled(Boolean repaymentBreakEnabled) {
        if (!_repaymentBreakEnabledIsSet) {
            _repaymentBreakEnabledIsSet = true;
            _repaymentBreakEnabledInitVal = repaymentBreakEnabled;
        }
        _repaymentBreakEnabled = repaymentBreakEnabled;
    }

    public Boolean getRepaymentBreakEnabled() {
        return _repaymentBreakEnabled;
    }

    public Boolean repaymentBreakEnabledInitVal() {
        return _repaymentBreakEnabledInitVal;
    }

	public boolean repaymentBreakEnabledIsDirty() {
        return !valuesAreEqual(_repaymentBreakEnabledInitVal, _repaymentBreakEnabled);
    }

    public boolean repaymentBreakEnabledIsSet() {
        return _repaymentBreakEnabledIsSet;
    }	


    public void setRepaymentBreakFee(Double repaymentBreakFee) {
        if (!_repaymentBreakFeeIsSet) {
            _repaymentBreakFeeIsSet = true;
            _repaymentBreakFeeInitVal = repaymentBreakFee;
        }
        _repaymentBreakFee = repaymentBreakFee;
    }

    public Double getRepaymentBreakFee() {
        return _repaymentBreakFee;
    }

    public Double repaymentBreakFeeInitVal() {
        return _repaymentBreakFeeInitVal;
    }

	public boolean repaymentBreakFeeIsDirty() {
        return !valuesAreEqual(_repaymentBreakFeeInitVal, _repaymentBreakFee);
    }

    public boolean repaymentBreakFeeIsSet() {
        return _repaymentBreakFeeIsSet;
    }	


    public void setAllowSharedLoan(Boolean allowSharedLoan) {
        if (!_allowSharedLoanIsSet) {
            _allowSharedLoanIsSet = true;
            _allowSharedLoanInitVal = allowSharedLoan;
        }
        _allowSharedLoan = allowSharedLoan;
    }

    public Boolean getAllowSharedLoan() {
        return _allowSharedLoan;
    }

    public Boolean allowSharedLoanInitVal() {
        return _allowSharedLoanInitVal;
    }

	public boolean allowSharedLoanIsDirty() {
        return !valuesAreEqual(_allowSharedLoanInitVal, _allowSharedLoan);
    }

    public boolean allowSharedLoanIsSet() {
        return _allowSharedLoanIsSet;
    }	


    public void setAllowDebtConsolidation(Boolean allowDebtConsolidation) {
        if (!_allowDebtConsolidationIsSet) {
            _allowDebtConsolidationIsSet = true;
            _allowDebtConsolidationInitVal = allowDebtConsolidation;
        }
        _allowDebtConsolidation = allowDebtConsolidation;
    }

    public Boolean getAllowDebtConsolidation() {
        return _allowDebtConsolidation;
    }

    public Boolean allowDebtConsolidationInitVal() {
        return _allowDebtConsolidationInitVal;
    }

	public boolean allowDebtConsolidationIsDirty() {
        return !valuesAreEqual(_allowDebtConsolidationInitVal, _allowDebtConsolidation);
    }

    public boolean allowDebtConsolidationIsSet() {
        return _allowDebtConsolidationIsSet;
    }	


    public void setCreditProjectCount(Integer creditProjectCount) {
        if (!_creditProjectCountIsSet) {
            _creditProjectCountIsSet = true;
            _creditProjectCountInitVal = creditProjectCount;
        }
        _creditProjectCount = creditProjectCount;
    }

    public Integer getCreditProjectCount() {
        return _creditProjectCount;
    }

    public Integer creditProjectCountInitVal() {
        return _creditProjectCountInitVal;
    }

	public boolean creditProjectCountIsDirty() {
        return !valuesAreEqual(_creditProjectCountInitVal, _creditProjectCount);
    }

    public boolean creditProjectCountIsSet() {
        return _creditProjectCountIsSet;
    }	


    public void setRefundPeriod(Integer refundPeriod) {
        if (!_refundPeriodIsSet) {
            _refundPeriodIsSet = true;
            _refundPeriodInitVal = refundPeriod;
        }
        _refundPeriod = refundPeriod;
    }

    public Integer getRefundPeriod() {
        return _refundPeriod;
    }

    public Integer refundPeriodInitVal() {
        return _refundPeriodInitVal;
    }

	public boolean refundPeriodIsDirty() {
        return !valuesAreEqual(_refundPeriodInitVal, _refundPeriod);
    }

    public boolean refundPeriodIsSet() {
        return _refundPeriodIsSet;
    }	


    public void setProvisionRefundRate(Double provisionRefundRate) {
        if (!_provisionRefundRateIsSet) {
            _provisionRefundRateIsSet = true;
            _provisionRefundRateInitVal = provisionRefundRate;
        }
        _provisionRefundRate = provisionRefundRate;
    }

    public Double getProvisionRefundRate() {
        return _provisionRefundRate;
    }

    public Double provisionRefundRateInitVal() {
        return _provisionRefundRateInitVal;
    }

	public boolean provisionRefundRateIsDirty() {
        return !valuesAreEqual(_provisionRefundRateInitVal, _provisionRefundRate);
    }

    public boolean provisionRefundRateIsSet() {
        return _provisionRefundRateIsSet;
    }	


    public void setAllowBorrowerPromotion(Boolean allowBorrowerPromotion) {
        if (!_allowBorrowerPromotionIsSet) {
            _allowBorrowerPromotionIsSet = true;
            _allowBorrowerPromotionInitVal = allowBorrowerPromotion;
        }
        _allowBorrowerPromotion = allowBorrowerPromotion;
    }

    public Boolean getAllowBorrowerPromotion() {
        return _allowBorrowerPromotion;
    }

    public Boolean allowBorrowerPromotionInitVal() {
        return _allowBorrowerPromotionInitVal;
    }

	public boolean allowBorrowerPromotionIsDirty() {
        return !valuesAreEqual(_allowBorrowerPromotionInitVal, _allowBorrowerPromotion);
    }

    public boolean allowBorrowerPromotionIsSet() {
        return _allowBorrowerPromotionIsSet;
    }	


    public void setAccountManagementLenderCharge(Double accountManagementLenderCharge) {
        if (!_accountManagementLenderChargeIsSet) {
            _accountManagementLenderChargeIsSet = true;
            _accountManagementLenderChargeInitVal = accountManagementLenderCharge;
        }
        _accountManagementLenderCharge = accountManagementLenderCharge;
    }

    public Double getAccountManagementLenderCharge() {
        return _accountManagementLenderCharge;
    }

    public Double accountManagementLenderChargeInitVal() {
        return _accountManagementLenderChargeInitVal;
    }

	public boolean accountManagementLenderChargeIsDirty() {
        return !valuesAreEqual(_accountManagementLenderChargeInitVal, _accountManagementLenderCharge);
    }

    public boolean accountManagementLenderChargeIsSet() {
        return _accountManagementLenderChargeIsSet;
    }	


    public void setAccountManagementBorrowerCharge(Double accountManagementBorrowerCharge) {
        if (!_accountManagementBorrowerChargeIsSet) {
            _accountManagementBorrowerChargeIsSet = true;
            _accountManagementBorrowerChargeInitVal = accountManagementBorrowerCharge;
        }
        _accountManagementBorrowerCharge = accountManagementBorrowerCharge;
    }

    public Double getAccountManagementBorrowerCharge() {
        return _accountManagementBorrowerCharge;
    }

    public Double accountManagementBorrowerChargeInitVal() {
        return _accountManagementBorrowerChargeInitVal;
    }

	public boolean accountManagementBorrowerChargeIsDirty() {
        return !valuesAreEqual(_accountManagementBorrowerChargeInitVal, _accountManagementBorrowerCharge);
    }

    public boolean accountManagementBorrowerChargeIsSet() {
        return _accountManagementBorrowerChargeIsSet;
    }	


    public void setBorrowerPostidentFee(double borrowerPostidentFee) {
        if (!_borrowerPostidentFeeIsSet) {
            _borrowerPostidentFeeIsSet = true;
            _borrowerPostidentFeeInitVal = borrowerPostidentFee;
        }
        _borrowerPostidentFee = borrowerPostidentFee;
    }

    public double getBorrowerPostidentFee() {
        return _borrowerPostidentFee;
    }

    public double borrowerPostidentFeeInitVal() {
        return _borrowerPostidentFeeInitVal;
    }

	public boolean borrowerPostidentFeeIsDirty() {
        return !valuesAreEqual(_borrowerPostidentFeeInitVal, _borrowerPostidentFee);
    }

    public boolean borrowerPostidentFeeIsSet() {
        return _borrowerPostidentFeeIsSet;
    }	


    public void setReturnDebitCharge(double returnDebitCharge) {
        if (!_returnDebitChargeIsSet) {
            _returnDebitChargeIsSet = true;
            _returnDebitChargeInitVal = returnDebitCharge;
        }
        _returnDebitCharge = returnDebitCharge;
    }

    public double getReturnDebitCharge() {
        return _returnDebitCharge;
    }

    public double returnDebitChargeInitVal() {
        return _returnDebitChargeInitVal;
    }

	public boolean returnDebitChargeIsDirty() {
        return !valuesAreEqual(_returnDebitChargeInitVal, _returnDebitCharge);
    }

    public boolean returnDebitChargeIsSet() {
        return _returnDebitChargeIsSet;
    }	


    public void setAllowBankView(Boolean allowBankView) {
        if (!_allowBankViewIsSet) {
            _allowBankViewIsSet = true;
            _allowBankViewInitVal = allowBankView;
        }
        _allowBankView = allowBankView;
    }

    public Boolean getAllowBankView() {
        return _allowBankView;
    }

    public Boolean allowBankViewInitVal() {
        return _allowBankViewInitVal;
    }

	public boolean allowBankViewIsDirty() {
        return !valuesAreEqual(_allowBankViewInitVal, _allowBankView);
    }

    public boolean allowBankViewIsSet() {
        return _allowBankViewIsSet;
    }	


    public void setBankviewValidityPeriod(Integer bankviewValidityPeriod) {
        if (!_bankviewValidityPeriodIsSet) {
            _bankviewValidityPeriodIsSet = true;
            _bankviewValidityPeriodInitVal = bankviewValidityPeriod;
        }
        _bankviewValidityPeriod = bankviewValidityPeriod;
    }

    public Integer getBankviewValidityPeriod() {
        return _bankviewValidityPeriod;
    }

    public Integer bankviewValidityPeriodInitVal() {
        return _bankviewValidityPeriodInitVal;
    }

	public boolean bankviewValidityPeriodIsDirty() {
        return !valuesAreEqual(_bankviewValidityPeriodInitVal, _bankviewValidityPeriod);
    }

    public boolean bankviewValidityPeriodIsSet() {
        return _bankviewValidityPeriodIsSet;
    }	


    public void setBrokerageDifferenceToChosenAmount(double brokerageDifferenceToChosenAmount) {
        if (!_brokerageDifferenceToChosenAmountIsSet) {
            _brokerageDifferenceToChosenAmountIsSet = true;
            _brokerageDifferenceToChosenAmountInitVal = brokerageDifferenceToChosenAmount;
        }
        _brokerageDifferenceToChosenAmount = brokerageDifferenceToChosenAmount;
    }

    public double getBrokerageDifferenceToChosenAmount() {
        return _brokerageDifferenceToChosenAmount;
    }

    public double brokerageDifferenceToChosenAmountInitVal() {
        return _brokerageDifferenceToChosenAmountInitVal;
    }

	public boolean brokerageDifferenceToChosenAmountIsDirty() {
        return !valuesAreEqual(_brokerageDifferenceToChosenAmountInitVal, _brokerageDifferenceToChosenAmount);
    }

    public boolean brokerageDifferenceToChosenAmountIsSet() {
        return _brokerageDifferenceToChosenAmountIsSet;
    }	


    public void setBrokerageSendNoDocumentsDays(Integer brokerageSendNoDocumentsDays) {
        if (!_brokerageSendNoDocumentsDaysIsSet) {
            _brokerageSendNoDocumentsDaysIsSet = true;
            _brokerageSendNoDocumentsDaysInitVal = brokerageSendNoDocumentsDays;
        }
        _brokerageSendNoDocumentsDays = brokerageSendNoDocumentsDays;
    }

    public Integer getBrokerageSendNoDocumentsDays() {
        return _brokerageSendNoDocumentsDays;
    }

    public Integer brokerageSendNoDocumentsDaysInitVal() {
        return _brokerageSendNoDocumentsDaysInitVal;
    }

	public boolean brokerageSendNoDocumentsDaysIsDirty() {
        return !valuesAreEqual(_brokerageSendNoDocumentsDaysInitVal, _brokerageSendNoDocumentsDays);
    }

    public boolean brokerageSendNoDocumentsDaysIsSet() {
        return _brokerageSendNoDocumentsDaysIsSet;
    }	


    public void setBrokerageMaxPeriodForAlternativeOffers(Integer brokerageMaxPeriodForAlternativeOffers) {
        if (!_brokerageMaxPeriodForAlternativeOffersIsSet) {
            _brokerageMaxPeriodForAlternativeOffersIsSet = true;
            _brokerageMaxPeriodForAlternativeOffersInitVal = brokerageMaxPeriodForAlternativeOffers;
        }
        _brokerageMaxPeriodForAlternativeOffers = brokerageMaxPeriodForAlternativeOffers;
    }

    public Integer getBrokerageMaxPeriodForAlternativeOffers() {
        return _brokerageMaxPeriodForAlternativeOffers;
    }

    public Integer brokerageMaxPeriodForAlternativeOffersInitVal() {
        return _brokerageMaxPeriodForAlternativeOffersInitVal;
    }

	public boolean brokerageMaxPeriodForAlternativeOffersIsDirty() {
        return !valuesAreEqual(_brokerageMaxPeriodForAlternativeOffersInitVal, _brokerageMaxPeriodForAlternativeOffers);
    }

    public boolean brokerageMaxPeriodForAlternativeOffersIsSet() {
        return _brokerageMaxPeriodForAlternativeOffersIsSet;
    }	


    public void setFidorActivated(Boolean fidorActivated) {
        if (!_fidorActivatedIsSet) {
            _fidorActivatedIsSet = true;
            _fidorActivatedInitVal = fidorActivated;
        }
        _fidorActivated = fidorActivated;
    }

    public Boolean getFidorActivated() {
        return _fidorActivated;
    }

    public Boolean fidorActivatedInitVal() {
        return _fidorActivatedInitVal;
    }

	public boolean fidorActivatedIsDirty() {
        return !valuesAreEqual(_fidorActivatedInitVal, _fidorActivated);
    }

    public boolean fidorActivatedIsSet() {
        return _fidorActivatedIsSet;
    }	


    public void setFidorDummy(Boolean fidorDummy) {
        if (!_fidorDummyIsSet) {
            _fidorDummyIsSet = true;
            _fidorDummyInitVal = fidorDummy;
        }
        _fidorDummy = fidorDummy;
    }

    public Boolean getFidorDummy() {
        return _fidorDummy;
    }

    public Boolean fidorDummyInitVal() {
        return _fidorDummyInitVal;
    }

	public boolean fidorDummyIsDirty() {
        return !valuesAreEqual(_fidorDummyInitVal, _fidorDummy);
    }

    public boolean fidorDummyIsSet() {
        return _fidorDummyIsSet;
    }	


    public void setCacheExpirationTime(Integer cacheExpirationTime) {
        if (!_cacheExpirationTimeIsSet) {
            _cacheExpirationTimeIsSet = true;
            _cacheExpirationTimeInitVal = cacheExpirationTime;
        }
        _cacheExpirationTime = cacheExpirationTime;
    }

    public Integer getCacheExpirationTime() {
        return _cacheExpirationTime;
    }

    public Integer cacheExpirationTimeInitVal() {
        return _cacheExpirationTimeInitVal;
    }

	public boolean cacheExpirationTimeIsDirty() {
        return !valuesAreEqual(_cacheExpirationTimeInitVal, _cacheExpirationTime);
    }

    public boolean cacheExpirationTimeIsSet() {
        return _cacheExpirationTimeIsSet;
    }	


    public void setPaProActivated(Boolean paProActivated) {
        if (!_paProActivatedIsSet) {
            _paProActivatedIsSet = true;
            _paProActivatedInitVal = paProActivated;
        }
        _paProActivated = paProActivated;
    }

    public Boolean getPaProActivated() {
        return _paProActivated;
    }

    public Boolean paProActivatedInitVal() {
        return _paProActivatedInitVal;
    }

	public boolean paProActivatedIsDirty() {
        return !valuesAreEqual(_paProActivatedInitVal, _paProActivated);
    }

    public boolean paProActivatedIsSet() {
        return _paProActivatedIsSet;
    }	


    public void setDefaultRegistrationRoute(String defaultRegistrationRoute) {
        if (!_defaultRegistrationRouteIsSet) {
            _defaultRegistrationRouteIsSet = true;
            _defaultRegistrationRouteInitVal = defaultRegistrationRoute;
        }
        _defaultRegistrationRoute = defaultRegistrationRoute;
    }

    public String getDefaultRegistrationRoute() {
        return _defaultRegistrationRoute;
    }

    public String defaultRegistrationRouteInitVal() {
        return _defaultRegistrationRouteInitVal;
    }

	public boolean defaultRegistrationRouteIsDirty() {
        return !valuesAreEqual(_defaultRegistrationRouteInitVal, _defaultRegistrationRoute);
    }

    public boolean defaultRegistrationRouteIsSet() {
        return _defaultRegistrationRouteIsSet;
    }	


    public void setRegistrationOfferListPollingTimeout(Integer registrationOfferListPollingTimeout) {
        if (!_registrationOfferListPollingTimeoutIsSet) {
            _registrationOfferListPollingTimeoutIsSet = true;
            _registrationOfferListPollingTimeoutInitVal = registrationOfferListPollingTimeout;
        }
        _registrationOfferListPollingTimeout = registrationOfferListPollingTimeout;
    }

    public Integer getRegistrationOfferListPollingTimeout() {
        return _registrationOfferListPollingTimeout;
    }

    public Integer registrationOfferListPollingTimeoutInitVal() {
        return _registrationOfferListPollingTimeoutInitVal;
    }

	public boolean registrationOfferListPollingTimeoutIsDirty() {
        return !valuesAreEqual(_registrationOfferListPollingTimeoutInitVal, _registrationOfferListPollingTimeout);
    }

    public boolean registrationOfferListPollingTimeoutIsSet() {
        return _registrationOfferListPollingTimeoutIsSet;
    }	


    public void setRegistrationBankAccountValidationTimeout(Integer registrationBankAccountValidationTimeout) {
        if (!_registrationBankAccountValidationTimeoutIsSet) {
            _registrationBankAccountValidationTimeoutIsSet = true;
            _registrationBankAccountValidationTimeoutInitVal = registrationBankAccountValidationTimeout;
        }
        _registrationBankAccountValidationTimeout = registrationBankAccountValidationTimeout;
    }

    public Integer getRegistrationBankAccountValidationTimeout() {
        return _registrationBankAccountValidationTimeout;
    }

    public Integer registrationBankAccountValidationTimeoutInitVal() {
        return _registrationBankAccountValidationTimeoutInitVal;
    }

	public boolean registrationBankAccountValidationTimeoutIsDirty() {
        return !valuesAreEqual(_registrationBankAccountValidationTimeoutInitVal, _registrationBankAccountValidationTimeout);
    }

    public boolean registrationBankAccountValidationTimeoutIsSet() {
        return _registrationBankAccountValidationTimeoutIsSet;
    }	


    public void setShowHotlineImage(Boolean showHotlineImage) {
        if (!_showHotlineImageIsSet) {
            _showHotlineImageIsSet = true;
            _showHotlineImageInitVal = showHotlineImage;
        }
        _showHotlineImage = showHotlineImage;
    }

    public Boolean getShowHotlineImage() {
        return _showHotlineImage;
    }

    public Boolean showHotlineImageInitVal() {
        return _showHotlineImageInitVal;
    }

	public boolean showHotlineImageIsDirty() {
        return !valuesAreEqual(_showHotlineImageInitVal, _showHotlineImage);
    }

    public boolean showHotlineImageIsSet() {
        return _showHotlineImageIsSet;
    }	


    public void setBankdataValidationActivated(Boolean bankdataValidationActivated) {
        if (!_bankdataValidationActivatedIsSet) {
            _bankdataValidationActivatedIsSet = true;
            _bankdataValidationActivatedInitVal = bankdataValidationActivated;
        }
        _bankdataValidationActivated = bankdataValidationActivated;
    }

    public Boolean getBankdataValidationActivated() {
        return _bankdataValidationActivated;
    }

    public Boolean bankdataValidationActivatedInitVal() {
        return _bankdataValidationActivatedInitVal;
    }

	public boolean bankdataValidationActivatedIsDirty() {
        return !valuesAreEqual(_bankdataValidationActivatedInitVal, _bankdataValidationActivated);
    }

    public boolean bankdataValidationActivatedIsSet() {
        return _bankdataValidationActivatedIsSet;
    }	


    public void setTrackValidationErrors(Boolean trackValidationErrors) {
        if (!_trackValidationErrorsIsSet) {
            _trackValidationErrorsIsSet = true;
            _trackValidationErrorsInitVal = trackValidationErrors;
        }
        _trackValidationErrors = trackValidationErrors;
    }

    public Boolean getTrackValidationErrors() {
        return _trackValidationErrors;
    }

    public Boolean trackValidationErrorsInitVal() {
        return _trackValidationErrorsInitVal;
    }

	public boolean trackValidationErrorsIsDirty() {
        return !valuesAreEqual(_trackValidationErrorsInitVal, _trackValidationErrors);
    }

    public boolean trackValidationErrorsIsSet() {
        return _trackValidationErrorsIsSet;
    }	


    public void setFidorMaintenanceWindow(Boolean fidorMaintenanceWindow) {
        if (!_fidorMaintenanceWindowIsSet) {
            _fidorMaintenanceWindowIsSet = true;
            _fidorMaintenanceWindowInitVal = fidorMaintenanceWindow;
        }
        _fidorMaintenanceWindow = fidorMaintenanceWindow;
    }

    public Boolean getFidorMaintenanceWindow() {
        return _fidorMaintenanceWindow;
    }

    public Boolean fidorMaintenanceWindowInitVal() {
        return _fidorMaintenanceWindowInitVal;
    }

	public boolean fidorMaintenanceWindowIsDirty() {
        return !valuesAreEqual(_fidorMaintenanceWindowInitVal, _fidorMaintenanceWindow);
    }

    public boolean fidorMaintenanceWindowIsSet() {
        return _fidorMaintenanceWindowIsSet;
    }	


    public void setGoogleTagManager(Boolean googleTagManager) {
        if (!_googleTagManagerIsSet) {
            _googleTagManagerIsSet = true;
            _googleTagManagerInitVal = googleTagManager;
        }
        _googleTagManager = googleTagManager;
    }

    public Boolean getGoogleTagManager() {
        return _googleTagManager;
    }

    public Boolean googleTagManagerInitVal() {
        return _googleTagManagerInitVal;
    }

	public boolean googleTagManagerIsDirty() {
        return !valuesAreEqual(_googleTagManagerInitVal, _googleTagManager);
    }

    public boolean googleTagManagerIsSet() {
        return _googleTagManagerIsSet;
    }	


    public void setArvatoEnabled(Boolean arvatoEnabled) {
        if (!_arvatoEnabledIsSet) {
            _arvatoEnabledIsSet = true;
            _arvatoEnabledInitVal = arvatoEnabled;
        }
        _arvatoEnabled = arvatoEnabled;
    }

    public Boolean getArvatoEnabled() {
        return _arvatoEnabled;
    }

    public Boolean arvatoEnabledInitVal() {
        return _arvatoEnabledInitVal;
    }

	public boolean arvatoEnabledIsDirty() {
        return !valuesAreEqual(_arvatoEnabledInitVal, _arvatoEnabled);
    }

    public boolean arvatoEnabledIsSet() {
        return _arvatoEnabledIsSet;
    }	


    public void setArvatoThreshold(Integer arvatoThreshold) {
        if (!_arvatoThresholdIsSet) {
            _arvatoThresholdIsSet = true;
            _arvatoThresholdInitVal = arvatoThreshold;
        }
        _arvatoThreshold = arvatoThreshold;
    }

    public Integer getArvatoThreshold() {
        return _arvatoThreshold;
    }

    public Integer arvatoThresholdInitVal() {
        return _arvatoThresholdInitVal;
    }

	public boolean arvatoThresholdIsDirty() {
        return !valuesAreEqual(_arvatoThresholdInitVal, _arvatoThreshold);
    }

    public boolean arvatoThresholdIsSet() {
        return _arvatoThresholdIsSet;
    }	


    public void setBrokerageEmailTimeout(Integer brokerageEmailTimeout) {
        if (!_brokerageEmailTimeoutIsSet) {
            _brokerageEmailTimeoutIsSet = true;
            _brokerageEmailTimeoutInitVal = brokerageEmailTimeout;
        }
        _brokerageEmailTimeout = brokerageEmailTimeout;
    }

    public Integer getBrokerageEmailTimeout() {
        return _brokerageEmailTimeout;
    }

    public Integer brokerageEmailTimeoutInitVal() {
        return _brokerageEmailTimeoutInitVal;
    }

	public boolean brokerageEmailTimeoutIsDirty() {
        return !valuesAreEqual(_brokerageEmailTimeoutInitVal, _brokerageEmailTimeout);
    }

    public boolean brokerageEmailTimeoutIsSet() {
        return _brokerageEmailTimeoutIsSet;
    }	


    public void setBrokerageEmail23InterestGeneral(Double brokerageEmail23InterestGeneral) {
        if (!_brokerageEmail23InterestGeneralIsSet) {
            _brokerageEmail23InterestGeneralIsSet = true;
            _brokerageEmail23InterestGeneralInitVal = brokerageEmail23InterestGeneral;
        }
        _brokerageEmail23InterestGeneral = brokerageEmail23InterestGeneral;
    }

    public Double getBrokerageEmail23InterestGeneral() {
        return _brokerageEmail23InterestGeneral;
    }

    public Double brokerageEmail23InterestGeneralInitVal() {
        return _brokerageEmail23InterestGeneralInitVal;
    }

	public boolean brokerageEmail23InterestGeneralIsDirty() {
        return !valuesAreEqual(_brokerageEmail23InterestGeneralInitVal, _brokerageEmail23InterestGeneral);
    }

    public boolean brokerageEmail23InterestGeneralIsSet() {
        return _brokerageEmail23InterestGeneralIsSet;
    }	


    public void setBrokerageEmail23InterestFreelancer(Double brokerageEmail23InterestFreelancer) {
        if (!_brokerageEmail23InterestFreelancerIsSet) {
            _brokerageEmail23InterestFreelancerIsSet = true;
            _brokerageEmail23InterestFreelancerInitVal = brokerageEmail23InterestFreelancer;
        }
        _brokerageEmail23InterestFreelancer = brokerageEmail23InterestFreelancer;
    }

    public Double getBrokerageEmail23InterestFreelancer() {
        return _brokerageEmail23InterestFreelancer;
    }

    public Double brokerageEmail23InterestFreelancerInitVal() {
        return _brokerageEmail23InterestFreelancerInitVal;
    }

	public boolean brokerageEmail23InterestFreelancerIsDirty() {
        return !valuesAreEqual(_brokerageEmail23InterestFreelancerInitVal, _brokerageEmail23InterestFreelancer);
    }

    public boolean brokerageEmail23InterestFreelancerIsSet() {
        return _brokerageEmail23InterestFreelancerIsSet;
    }	


    public void setCreditGuaranteeAndTopAcceptanceChanceTimeout(Integer creditGuaranteeAndTopAcceptanceChanceTimeout) {
        if (!_creditGuaranteeAndTopAcceptanceChanceTimeoutIsSet) {
            _creditGuaranteeAndTopAcceptanceChanceTimeoutIsSet = true;
            _creditGuaranteeAndTopAcceptanceChanceTimeoutInitVal = creditGuaranteeAndTopAcceptanceChanceTimeout;
        }
        _creditGuaranteeAndTopAcceptanceChanceTimeout = creditGuaranteeAndTopAcceptanceChanceTimeout;
    }

    public Integer getCreditGuaranteeAndTopAcceptanceChanceTimeout() {
        return _creditGuaranteeAndTopAcceptanceChanceTimeout;
    }

    public Integer creditGuaranteeAndTopAcceptanceChanceTimeoutInitVal() {
        return _creditGuaranteeAndTopAcceptanceChanceTimeoutInitVal;
    }

	public boolean creditGuaranteeAndTopAcceptanceChanceTimeoutIsDirty() {
        return !valuesAreEqual(_creditGuaranteeAndTopAcceptanceChanceTimeoutInitVal, _creditGuaranteeAndTopAcceptanceChanceTimeout);
    }

    public boolean creditGuaranteeAndTopAcceptanceChanceTimeoutIsSet() {
        return _creditGuaranteeAndTopAcceptanceChanceTimeoutIsSet;
    }	


    public void setActivateCreditGuarantee(Boolean activateCreditGuarantee) {
        if (!_activateCreditGuaranteeIsSet) {
            _activateCreditGuaranteeIsSet = true;
            _activateCreditGuaranteeInitVal = activateCreditGuarantee;
        }
        _activateCreditGuarantee = activateCreditGuarantee;
    }

    public Boolean getActivateCreditGuarantee() {
        return _activateCreditGuarantee;
    }

    public Boolean activateCreditGuaranteeInitVal() {
        return _activateCreditGuaranteeInitVal;
    }

	public boolean activateCreditGuaranteeIsDirty() {
        return !valuesAreEqual(_activateCreditGuaranteeInitVal, _activateCreditGuarantee);
    }

    public boolean activateCreditGuaranteeIsSet() {
        return _activateCreditGuaranteeIsSet;
    }	


    public void setCreditGuaranteeMinAmount(Long creditGuaranteeMinAmount) {
        if (!_creditGuaranteeMinAmountIsSet) {
            _creditGuaranteeMinAmountIsSet = true;
            _creditGuaranteeMinAmountInitVal = creditGuaranteeMinAmount;
        }
        _creditGuaranteeMinAmount = creditGuaranteeMinAmount;
    }

    public Long getCreditGuaranteeMinAmount() {
        return _creditGuaranteeMinAmount;
    }

    public Long creditGuaranteeMinAmountInitVal() {
        return _creditGuaranteeMinAmountInitVal;
    }

	public boolean creditGuaranteeMinAmountIsDirty() {
        return !valuesAreEqual(_creditGuaranteeMinAmountInitVal, _creditGuaranteeMinAmount);
    }

    public boolean creditGuaranteeMinAmountIsSet() {
        return _creditGuaranteeMinAmountIsSet;
    }	


    public void setCreditGuaranteeMaxAmount(Long creditGuaranteeMaxAmount) {
        if (!_creditGuaranteeMaxAmountIsSet) {
            _creditGuaranteeMaxAmountIsSet = true;
            _creditGuaranteeMaxAmountInitVal = creditGuaranteeMaxAmount;
        }
        _creditGuaranteeMaxAmount = creditGuaranteeMaxAmount;
    }

    public Long getCreditGuaranteeMaxAmount() {
        return _creditGuaranteeMaxAmount;
    }

    public Long creditGuaranteeMaxAmountInitVal() {
        return _creditGuaranteeMaxAmountInitVal;
    }

	public boolean creditGuaranteeMaxAmountIsDirty() {
        return !valuesAreEqual(_creditGuaranteeMaxAmountInitVal, _creditGuaranteeMaxAmount);
    }

    public boolean creditGuaranteeMaxAmountIsSet() {
        return _creditGuaranteeMaxAmountIsSet;
    }	


    public void setCreditGuaranteeMinDuration(Integer creditGuaranteeMinDuration) {
        if (!_creditGuaranteeMinDurationIsSet) {
            _creditGuaranteeMinDurationIsSet = true;
            _creditGuaranteeMinDurationInitVal = creditGuaranteeMinDuration;
        }
        _creditGuaranteeMinDuration = creditGuaranteeMinDuration;
    }

    public Integer getCreditGuaranteeMinDuration() {
        return _creditGuaranteeMinDuration;
    }

    public Integer creditGuaranteeMinDurationInitVal() {
        return _creditGuaranteeMinDurationInitVal;
    }

	public boolean creditGuaranteeMinDurationIsDirty() {
        return !valuesAreEqual(_creditGuaranteeMinDurationInitVal, _creditGuaranteeMinDuration);
    }

    public boolean creditGuaranteeMinDurationIsSet() {
        return _creditGuaranteeMinDurationIsSet;
    }	


    public void setCreditGuaranteeMaxDuration(Integer creditGuaranteeMaxDuration) {
        if (!_creditGuaranteeMaxDurationIsSet) {
            _creditGuaranteeMaxDurationIsSet = true;
            _creditGuaranteeMaxDurationInitVal = creditGuaranteeMaxDuration;
        }
        _creditGuaranteeMaxDuration = creditGuaranteeMaxDuration;
    }

    public Integer getCreditGuaranteeMaxDuration() {
        return _creditGuaranteeMaxDuration;
    }

    public Integer creditGuaranteeMaxDurationInitVal() {
        return _creditGuaranteeMaxDurationInitVal;
    }

	public boolean creditGuaranteeMaxDurationIsDirty() {
        return !valuesAreEqual(_creditGuaranteeMaxDurationInitVal, _creditGuaranteeMaxDuration);
    }

    public boolean creditGuaranteeMaxDurationIsSet() {
        return _creditGuaranteeMaxDurationIsSet;
    }	


    public void setCreditGuaranteeMinCheckmarks(Integer creditGuaranteeMinCheckmarks) {
        if (!_creditGuaranteeMinCheckmarksIsSet) {
            _creditGuaranteeMinCheckmarksIsSet = true;
            _creditGuaranteeMinCheckmarksInitVal = creditGuaranteeMinCheckmarks;
        }
        _creditGuaranteeMinCheckmarks = creditGuaranteeMinCheckmarks;
    }

    public Integer getCreditGuaranteeMinCheckmarks() {
        return _creditGuaranteeMinCheckmarks;
    }

    public Integer creditGuaranteeMinCheckmarksInitVal() {
        return _creditGuaranteeMinCheckmarksInitVal;
    }

	public boolean creditGuaranteeMinCheckmarksIsDirty() {
        return !valuesAreEqual(_creditGuaranteeMinCheckmarksInitVal, _creditGuaranteeMinCheckmarks);
    }

    public boolean creditGuaranteeMinCheckmarksIsSet() {
        return _creditGuaranteeMinCheckmarksIsSet;
    }	


    public void setCreditGuaranteeEmailTimeout(Integer creditGuaranteeEmailTimeout) {
        if (!_creditGuaranteeEmailTimeoutIsSet) {
            _creditGuaranteeEmailTimeoutIsSet = true;
            _creditGuaranteeEmailTimeoutInitVal = creditGuaranteeEmailTimeout;
        }
        _creditGuaranteeEmailTimeout = creditGuaranteeEmailTimeout;
    }

    public Integer getCreditGuaranteeEmailTimeout() {
        return _creditGuaranteeEmailTimeout;
    }

    public Integer creditGuaranteeEmailTimeoutInitVal() {
        return _creditGuaranteeEmailTimeoutInitVal;
    }

	public boolean creditGuaranteeEmailTimeoutIsDirty() {
        return !valuesAreEqual(_creditGuaranteeEmailTimeoutInitVal, _creditGuaranteeEmailTimeout);
    }

    public boolean creditGuaranteeEmailTimeoutIsSet() {
        return _creditGuaranteeEmailTimeoutIsSet;
    }	


    public void setCreditGuaranteeExpression(String creditGuaranteeExpression) {
        if (!_creditGuaranteeExpressionIsSet) {
            _creditGuaranteeExpressionIsSet = true;
            _creditGuaranteeExpressionInitVal = creditGuaranteeExpression;
        }
        _creditGuaranteeExpression = creditGuaranteeExpression;
    }

    public String getCreditGuaranteeExpression() {
        return _creditGuaranteeExpression;
    }

    public String creditGuaranteeExpressionInitVal() {
        return _creditGuaranteeExpressionInitVal;
    }

	public boolean creditGuaranteeExpressionIsDirty() {
        return !valuesAreEqual(_creditGuaranteeExpressionInitVal, _creditGuaranteeExpression);
    }

    public boolean creditGuaranteeExpressionIsSet() {
        return _creditGuaranteeExpressionIsSet;
    }	


    public void setActivateTopAcceptanceChance(Boolean activateTopAcceptanceChance) {
        if (!_activateTopAcceptanceChanceIsSet) {
            _activateTopAcceptanceChanceIsSet = true;
            _activateTopAcceptanceChanceInitVal = activateTopAcceptanceChance;
        }
        _activateTopAcceptanceChance = activateTopAcceptanceChance;
    }

    public Boolean getActivateTopAcceptanceChance() {
        return _activateTopAcceptanceChance;
    }

    public Boolean activateTopAcceptanceChanceInitVal() {
        return _activateTopAcceptanceChanceInitVal;
    }

	public boolean activateTopAcceptanceChanceIsDirty() {
        return !valuesAreEqual(_activateTopAcceptanceChanceInitVal, _activateTopAcceptanceChance);
    }

    public boolean activateTopAcceptanceChanceIsSet() {
        return _activateTopAcceptanceChanceIsSet;
    }	


    public void setTopAcceptanceChanceMinAmount(Long topAcceptanceChanceMinAmount) {
        if (!_topAcceptanceChanceMinAmountIsSet) {
            _topAcceptanceChanceMinAmountIsSet = true;
            _topAcceptanceChanceMinAmountInitVal = topAcceptanceChanceMinAmount;
        }
        _topAcceptanceChanceMinAmount = topAcceptanceChanceMinAmount;
    }

    public Long getTopAcceptanceChanceMinAmount() {
        return _topAcceptanceChanceMinAmount;
    }

    public Long topAcceptanceChanceMinAmountInitVal() {
        return _topAcceptanceChanceMinAmountInitVal;
    }

	public boolean topAcceptanceChanceMinAmountIsDirty() {
        return !valuesAreEqual(_topAcceptanceChanceMinAmountInitVal, _topAcceptanceChanceMinAmount);
    }

    public boolean topAcceptanceChanceMinAmountIsSet() {
        return _topAcceptanceChanceMinAmountIsSet;
    }	


    public void setTopAcceptanceChanceMaxAmount(Long topAcceptanceChanceMaxAmount) {
        if (!_topAcceptanceChanceMaxAmountIsSet) {
            _topAcceptanceChanceMaxAmountIsSet = true;
            _topAcceptanceChanceMaxAmountInitVal = topAcceptanceChanceMaxAmount;
        }
        _topAcceptanceChanceMaxAmount = topAcceptanceChanceMaxAmount;
    }

    public Long getTopAcceptanceChanceMaxAmount() {
        return _topAcceptanceChanceMaxAmount;
    }

    public Long topAcceptanceChanceMaxAmountInitVal() {
        return _topAcceptanceChanceMaxAmountInitVal;
    }

	public boolean topAcceptanceChanceMaxAmountIsDirty() {
        return !valuesAreEqual(_topAcceptanceChanceMaxAmountInitVal, _topAcceptanceChanceMaxAmount);
    }

    public boolean topAcceptanceChanceMaxAmountIsSet() {
        return _topAcceptanceChanceMaxAmountIsSet;
    }	


    public void setTopAcceptanceChanceMinDuration(Integer topAcceptanceChanceMinDuration) {
        if (!_topAcceptanceChanceMinDurationIsSet) {
            _topAcceptanceChanceMinDurationIsSet = true;
            _topAcceptanceChanceMinDurationInitVal = topAcceptanceChanceMinDuration;
        }
        _topAcceptanceChanceMinDuration = topAcceptanceChanceMinDuration;
    }

    public Integer getTopAcceptanceChanceMinDuration() {
        return _topAcceptanceChanceMinDuration;
    }

    public Integer topAcceptanceChanceMinDurationInitVal() {
        return _topAcceptanceChanceMinDurationInitVal;
    }

	public boolean topAcceptanceChanceMinDurationIsDirty() {
        return !valuesAreEqual(_topAcceptanceChanceMinDurationInitVal, _topAcceptanceChanceMinDuration);
    }

    public boolean topAcceptanceChanceMinDurationIsSet() {
        return _topAcceptanceChanceMinDurationIsSet;
    }	


    public void setTopAcceptanceChanceMaxDuration(Integer topAcceptanceChanceMaxDuration) {
        if (!_topAcceptanceChanceMaxDurationIsSet) {
            _topAcceptanceChanceMaxDurationIsSet = true;
            _topAcceptanceChanceMaxDurationInitVal = topAcceptanceChanceMaxDuration;
        }
        _topAcceptanceChanceMaxDuration = topAcceptanceChanceMaxDuration;
    }

    public Integer getTopAcceptanceChanceMaxDuration() {
        return _topAcceptanceChanceMaxDuration;
    }

    public Integer topAcceptanceChanceMaxDurationInitVal() {
        return _topAcceptanceChanceMaxDurationInitVal;
    }

	public boolean topAcceptanceChanceMaxDurationIsDirty() {
        return !valuesAreEqual(_topAcceptanceChanceMaxDurationInitVal, _topAcceptanceChanceMaxDuration);
    }

    public boolean topAcceptanceChanceMaxDurationIsSet() {
        return _topAcceptanceChanceMaxDurationIsSet;
    }	


    public void setTopAcceptanceChanceMinCheckmarks(Integer topAcceptanceChanceMinCheckmarks) {
        if (!_topAcceptanceChanceMinCheckmarksIsSet) {
            _topAcceptanceChanceMinCheckmarksIsSet = true;
            _topAcceptanceChanceMinCheckmarksInitVal = topAcceptanceChanceMinCheckmarks;
        }
        _topAcceptanceChanceMinCheckmarks = topAcceptanceChanceMinCheckmarks;
    }

    public Integer getTopAcceptanceChanceMinCheckmarks() {
        return _topAcceptanceChanceMinCheckmarks;
    }

    public Integer topAcceptanceChanceMinCheckmarksInitVal() {
        return _topAcceptanceChanceMinCheckmarksInitVal;
    }

	public boolean topAcceptanceChanceMinCheckmarksIsDirty() {
        return !valuesAreEqual(_topAcceptanceChanceMinCheckmarksInitVal, _topAcceptanceChanceMinCheckmarks);
    }

    public boolean topAcceptanceChanceMinCheckmarksIsSet() {
        return _topAcceptanceChanceMinCheckmarksIsSet;
    }	


    public void setVideoIdentEnabled(Boolean videoIdentEnabled) {
        if (!_videoIdentEnabledIsSet) {
            _videoIdentEnabledIsSet = true;
            _videoIdentEnabledInitVal = videoIdentEnabled;
        }
        _videoIdentEnabled = videoIdentEnabled;
    }

    public Boolean getVideoIdentEnabled() {
        return _videoIdentEnabled;
    }

    public Boolean videoIdentEnabledInitVal() {
        return _videoIdentEnabledInitVal;
    }

	public boolean videoIdentEnabledIsDirty() {
        return !valuesAreEqual(_videoIdentEnabledInitVal, _videoIdentEnabled);
    }

    public boolean videoIdentEnabledIsSet() {
        return _videoIdentEnabledIsSet;
    }	

}
