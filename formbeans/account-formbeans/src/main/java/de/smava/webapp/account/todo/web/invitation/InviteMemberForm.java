//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.invitation;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(invite member)}
import de.smava.webapp.account.domain.Account;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'InviteMemberForm'.
 *
 * @author generator
 */
public class InviteMemberForm {

    public static final String SENDER_EXPR = "inviteMemberForm.sender";
    public static final String SENDER_PATH = "command.inviteMemberForm.sender";
    public static final String SENDER_NAME_EXPR = "inviteMemberForm.senderName";
    public static final String SENDER_NAME_PATH = "command.inviteMemberForm.senderName";
    public static final String SENDER_EMAIL_EXPR = "inviteMemberForm.senderEmail";
    public static final String SENDER_EMAIL_PATH = "command.inviteMemberForm.senderEmail";
    public static final String EMAIL1_EXPR = "inviteMemberForm.email1";
    public static final String EMAIL1_PATH = "command.inviteMemberForm.email1";
    public static final String EMAIL2_EXPR = "inviteMemberForm.email2";
    public static final String EMAIL2_PATH = "command.inviteMemberForm.email2";
    public static final String EMAIL3_EXPR = "inviteMemberForm.email3";
    public static final String EMAIL3_PATH = "command.inviteMemberForm.email3";
    public static final String EMAIL4_EXPR = "inviteMemberForm.email4";
    public static final String EMAIL4_PATH = "command.inviteMemberForm.email4";
    public static final String TEXT_EXPR = "inviteMemberForm.text";
    public static final String TEXT_PATH = "command.inviteMemberForm.text";
    public static final String ORDER_ID_EXPR = "inviteMemberForm.orderId";
    public static final String ORDER_ID_PATH = "command.inviteMemberForm.orderId";
    public static final String GROUP_ID_EXPR = "inviteMemberForm.groupId";
    public static final String GROUP_ID_PATH = "command.inviteMemberForm.groupId";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(invite member)}
// !!!!!!!! End of insert code section !!!!!!!!


    private Account _sender;
    private Account _senderInitVal;
    private boolean _senderIsSet;

    private String _senderName;
    private String _senderNameInitVal;
    private boolean _senderNameIsSet;

    private String _senderEmail;
    private String _senderEmailInitVal;
    private boolean _senderEmailIsSet;

    private String _email1;
    private String _email1InitVal;
    private boolean _email1IsSet;

    private String _email2;
    private String _email2InitVal;
    private boolean _email2IsSet;

    private String _email3;
    private String _email3InitVal;
    private boolean _email3IsSet;

    private String _email4;
    private String _email4InitVal;
    private boolean _email4IsSet;

    private String _text;
    private String _textInitVal;
    private boolean _textIsSet;

    private String _orderId;
    private String _orderIdInitVal;
    private boolean _orderIdIsSet;

    private String _groupId;
    private String _groupIdInitVal;
    private boolean _groupIdIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setSender(Account sender) {
        if (!_senderIsSet) {
            _senderIsSet = true;
            _senderInitVal = sender;
        }
        _sender = sender;
    }

    public Account getSender() {
        return _sender;
    }

    public Account senderInitVal() {
        return _senderInitVal;
    }

    public boolean senderIsDirty() {
        return !valuesAreEqual(_senderInitVal, _sender);
    }

    public boolean senderIsSet() {
        return _senderIsSet;
    }

    public void setSenderName(String senderName) {
        if (!_senderNameIsSet) {
            _senderNameIsSet = true;
            _senderNameInitVal = senderName;
        }
        _senderName = senderName;
    }

    public String getSenderName() {
        return _senderName;
    }

    public String senderNameInitVal() {
        return _senderNameInitVal;
    }

    public boolean senderNameIsDirty() {
        return !valuesAreEqual(_senderNameInitVal, _senderName);
    }

    public boolean senderNameIsSet() {
        return _senderNameIsSet;
    }

    public void setSenderEmail(String senderEmail) {
        if (!_senderEmailIsSet) {
            _senderEmailIsSet = true;
            _senderEmailInitVal = senderEmail;
        }
        _senderEmail = senderEmail;
    }

    public String getSenderEmail() {
        return _senderEmail;
    }

    public String senderEmailInitVal() {
        return _senderEmailInitVal;
    }

    public boolean senderEmailIsDirty() {
        return !valuesAreEqual(_senderEmailInitVal, _senderEmail);
    }

    public boolean senderEmailIsSet() {
        return _senderEmailIsSet;
    }

    public void setEmail1(String email1) {
        if (!_email1IsSet) {
            _email1IsSet = true;
            _email1InitVal = email1;
        }
        _email1 = email1;
    }

    public String getEmail1() {
        return _email1;
    }

    public String email1InitVal() {
        return _email1InitVal;
    }

    public boolean email1IsDirty() {
        return !valuesAreEqual(_email1InitVal, _email1);
    }

    public boolean email1IsSet() {
        return _email1IsSet;
    }

    public void setEmail2(String email2) {
        if (!_email2IsSet) {
            _email2IsSet = true;
            _email2InitVal = email2;
        }
        _email2 = email2;
    }

    public String getEmail2() {
        return _email2;
    }

    public String email2InitVal() {
        return _email2InitVal;
    }

    public boolean email2IsDirty() {
        return !valuesAreEqual(_email2InitVal, _email2);
    }

    public boolean email2IsSet() {
        return _email2IsSet;
    }

    public void setEmail3(String email3) {
        if (!_email3IsSet) {
            _email3IsSet = true;
            _email3InitVal = email3;
        }
        _email3 = email3;
    }

    public String getEmail3() {
        return _email3;
    }

    public String email3InitVal() {
        return _email3InitVal;
    }

    public boolean email3IsDirty() {
        return !valuesAreEqual(_email3InitVal, _email3);
    }

    public boolean email3IsSet() {
        return _email3IsSet;
    }

    public void setEmail4(String email4) {
        if (!_email4IsSet) {
            _email4IsSet = true;
            _email4InitVal = email4;
        }
        _email4 = email4;
    }

    public String getEmail4() {
        return _email4;
    }

    public String email4InitVal() {
        return _email4InitVal;
    }

    public boolean email4IsDirty() {
        return !valuesAreEqual(_email4InitVal, _email4);
    }

    public boolean email4IsSet() {
        return _email4IsSet;
    }

    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = text;
        }
        _text = text;
    }

    public String getText() {
        return _text;
    }

    public String textInitVal() {
        return _textInitVal;
    }

    public boolean textIsDirty() {
        return !valuesAreEqual(_textInitVal, _text);
    }

    public boolean textIsSet() {
        return _textIsSet;
    }

    public void setOrderId(String orderId) {
        if (!_orderIdIsSet) {
            _orderIdIsSet = true;
            _orderIdInitVal = orderId;
        }
        _orderId = orderId;
    }

    public String getOrderId() {
        return _orderId;
    }

    public String orderIdInitVal() {
        return _orderIdInitVal;
    }

    public boolean orderIdIsDirty() {
        return !valuesAreEqual(_orderIdInitVal, _orderId);
    }

    public boolean orderIdIsSet() {
        return _orderIdIsSet;
    }

    public void setGroupId(String groupId) {
        if (!_groupIdIsSet) {
            _groupIdIsSet = true;
            _groupIdInitVal = groupId;
        }
        _groupId = groupId;
    }

    public String getGroupId() {
        return _groupId;
    }

    public String groupIdInitVal() {
        return _groupIdInitVal;
    }

    public boolean groupIdIsDirty() {
        return !valuesAreEqual(_groupIdInitVal, _groupId);
    }

    public boolean groupIdIsSet() {
        return _groupIdIsSet;
    }

}
