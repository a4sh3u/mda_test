//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/formbean.tpl
//
//
//
//
package de.smava.webapp.account.todo.web.backoffice.configuration;

    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo smava select configuration)}

import java.io.Serializable;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoSmavaSelectConfigurationForm'.
 *
 * @author generator
 */
public class BoSmavaSelectConfigurationForm implements Serializable {

	public static final String PARTNER_DURATIONS_EXPR = "boSmavaSelectConfigurationForm.partnerDurations";
    public static final String PARTNER_DURATIONS_PATH = "command.boSmavaSelectConfigurationForm.partnerDurations";
    public static final String PARTNER_BID_ASSISTANT_VALID_DATE_EXPR = "boSmavaSelectConfigurationForm.partnerBidAssistantValidDate";
    public static final String PARTNER_BID_ASSISTANT_VALID_DATE_PATH = "command.boSmavaSelectConfigurationForm.partnerBidAssistantValidDate";
    public static final String PARTNER_MAX_AGIO_EXPR = "boSmavaSelectConfigurationForm.partnerMaxAgio";
    public static final String PARTNER_MAX_AGIO_PATH = "command.boSmavaSelectConfigurationForm.partnerMaxAgio";
    public static final String PUBLICATION_NAME_EXPR = "boSmavaSelectConfigurationForm.publicationName";
    public static final String PUBLICATION_NAME_PATH = "command.boSmavaSelectConfigurationForm.publicationName";
    public static final String PUBLICATION_TYPE_EXPR = "boSmavaSelectConfigurationForm.publicationType";
    public static final String PUBLICATION_TYPE_PATH = "command.boSmavaSelectConfigurationForm.publicationType";
    public static final String PUBLICATION_RANGE_EXPR = "boSmavaSelectConfigurationForm.publicationRange";
    public static final String PUBLICATION_RANGE_PATH = "command.boSmavaSelectConfigurationForm.publicationRange";
    public static final String PUBLICATION_BULK_EXPR = "boSmavaSelectConfigurationForm.publicationBulk";
    public static final String PUBLICATION_BULK_PATH = "command.boSmavaSelectConfigurationForm.publicationBulk";
    public static final String PUBLICATION_POSITION_EXPR = "boSmavaSelectConfigurationForm.publicationPosition";
    public static final String PUBLICATION_POSITION_PATH = "command.boSmavaSelectConfigurationForm.publicationPosition";
    public static final String PUBLICATION_AD_FORMAT_EXPR = "boSmavaSelectConfigurationForm.publicationAdFormat";
    public static final String PUBLICATION_AD_FORMAT_PATH = "command.boSmavaSelectConfigurationForm.publicationAdFormat";
    public static final String PUBLICATION_CAMPAIGN_EXPR = "boSmavaSelectConfigurationForm.publicationCampaign";
    public static final String PUBLICATION_CAMPAIGN_PATH = "command.boSmavaSelectConfigurationForm.publicationCampaign";
    public static final String BASE_PROVISION_DEAL_1_EXPR = "boSmavaSelectConfigurationForm.baseProvisionDeal1";
    public static final String BASE_PROVISION_DEAL_1_PATH = "command.boSmavaSelectConfigurationForm.baseProvisionDeal1";
    public static final String BASE_PROVISION_DEAL_2_EXPR = "boSmavaSelectConfigurationForm.baseProvisionDeal2";
    public static final String BASE_PROVISION_DEAL_2_PATH = "command.boSmavaSelectConfigurationForm.baseProvisionDeal2";
    public static final String BASE_PROVISION_DEAL_3_EXPR = "boSmavaSelectConfigurationForm.baseProvisionDeal3";
    public static final String BASE_PROVISION_DEAL_3_PATH = "command.boSmavaSelectConfigurationForm.baseProvisionDeal3";
    public static final String BASE_PROVISION_DEAL_4_EXPR = "boSmavaSelectConfigurationForm.baseProvisionDeal4";
    public static final String BASE_PROVISION_DEAL_4_PATH = "command.boSmavaSelectConfigurationForm.baseProvisionDeal4";
    public static final String PLACEMENT_URL_EXPR = "boSmavaSelectConfigurationForm.placementUrl";
    public static final String PLACEMENT_URL_PATH = "command.boSmavaSelectConfigurationForm.placementUrl";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo smava select configuration)}

    private static final long serialVersionUID = 714306639076188179L;

// !!!!!!!! End of insert code section !!!!!!!!

    private List<String> _partnerDurations;
    private List<String> _partnerDurationsInitVal;
    private boolean _partnerDurationsIsSet;
    private Integer _partnerBidAssistantValidDate;
    private Integer _partnerBidAssistantValidDateInitVal;
    private boolean _partnerBidAssistantValidDateIsSet;
    private Double _partnerMaxAgio;
    private Double _partnerMaxAgioInitVal;
    private boolean _partnerMaxAgioIsSet;
    private String _publicationName;
    private String _publicationNameInitVal;
    private boolean _publicationNameIsSet;
    private String _publicationType;
    private String _publicationTypeInitVal;
    private boolean _publicationTypeIsSet;
    private String _publicationRange;
    private String _publicationRangeInitVal;
    private boolean _publicationRangeIsSet;
    private Integer _publicationBulk;
    private Integer _publicationBulkInitVal;
    private boolean _publicationBulkIsSet;
    private String _publicationPosition;
    private String _publicationPositionInitVal;
    private boolean _publicationPositionIsSet;
    private Integer _publicationAdFormat;
    private Integer _publicationAdFormatInitVal;
    private boolean _publicationAdFormatIsSet;
    private Integer _publicationCampaign;
    private Integer _publicationCampaignInitVal;
    private boolean _publicationCampaignIsSet;
    private Double _baseProvisionDeal1;
    private Double _baseProvisionDeal1InitVal;
    private boolean _baseProvisionDeal1IsSet;
    private Double _baseProvisionDeal2;
    private Double _baseProvisionDeal2InitVal;
    private boolean _baseProvisionDeal2IsSet;
    private Double _baseProvisionDeal3;
    private Double _baseProvisionDeal3InitVal;
    private boolean _baseProvisionDeal3IsSet;
    private Double _baseProvisionDeal4;
    private Double _baseProvisionDeal4InitVal;
    private boolean _baseProvisionDeal4IsSet;
    private String _placementUrl;
    private String _placementUrlInitVal;
    private boolean _placementUrlIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }

    public void setPartnerDurations(List<String> partnerDurations) {
        if (!_partnerDurationsIsSet) {
            _partnerDurationsIsSet = true;
            _partnerDurationsInitVal = partnerDurations;
        }
        _partnerDurations = partnerDurations;
    }

    public List<String> getPartnerDurations() {
        return _partnerDurations;
    }

    public List<String> partnerDurationsInitVal() {
        return _partnerDurationsInitVal;
    }

	public boolean partnerDurationsIsDirty() {
        return !valuesAreEqual(_partnerDurationsInitVal, _partnerDurations);
    }

    public boolean partnerDurationsIsSet() {
        return _partnerDurationsIsSet;
    }	

    public void setPartnerBidAssistantValidDate(Integer partnerBidAssistantValidDate) {
        if (!_partnerBidAssistantValidDateIsSet) {
            _partnerBidAssistantValidDateIsSet = true;
            _partnerBidAssistantValidDateInitVal = partnerBidAssistantValidDate;
        }
        _partnerBidAssistantValidDate = partnerBidAssistantValidDate;
    }

    public Integer getPartnerBidAssistantValidDate() {
        return _partnerBidAssistantValidDate;
    }

    public Integer partnerBidAssistantValidDateInitVal() {
        return _partnerBidAssistantValidDateInitVal;
    }

	public boolean partnerBidAssistantValidDateIsDirty() {
        return !valuesAreEqual(_partnerBidAssistantValidDateInitVal, _partnerBidAssistantValidDate);
    }

    public boolean partnerBidAssistantValidDateIsSet() {
        return _partnerBidAssistantValidDateIsSet;
    }	

    public void setPartnerMaxAgio(Double partnerMaxAgio) {
        if (!_partnerMaxAgioIsSet) {
            _partnerMaxAgioIsSet = true;
            _partnerMaxAgioInitVal = partnerMaxAgio;
        }
        _partnerMaxAgio = partnerMaxAgio;
    }

    public Double getPartnerMaxAgio() {
        return _partnerMaxAgio;
    }

    public Double partnerMaxAgioInitVal() {
        return _partnerMaxAgioInitVal;
    }

	public boolean partnerMaxAgioIsDirty() {
        return !valuesAreEqual(_partnerMaxAgioInitVal, _partnerMaxAgio);
    }

    public boolean partnerMaxAgioIsSet() {
        return _partnerMaxAgioIsSet;
    }	

    public void setPublicationName(String publicationName) {
        if (!_publicationNameIsSet) {
            _publicationNameIsSet = true;
            _publicationNameInitVal = publicationName;
        }
        _publicationName = publicationName;
    }

    public String getPublicationName() {
        return _publicationName;
    }

    public String publicationNameInitVal() {
        return _publicationNameInitVal;
    }

	public boolean publicationNameIsDirty() {
        return !valuesAreEqual(_publicationNameInitVal, _publicationName);
    }

    public boolean publicationNameIsSet() {
        return _publicationNameIsSet;
    }	

    public void setPublicationType(String publicationType) {
        if (!_publicationTypeIsSet) {
            _publicationTypeIsSet = true;
            _publicationTypeInitVal = publicationType;
        }
        _publicationType = publicationType;
    }

    public String getPublicationType() {
        return _publicationType;
    }

    public String publicationTypeInitVal() {
        return _publicationTypeInitVal;
    }

	public boolean publicationTypeIsDirty() {
        return !valuesAreEqual(_publicationTypeInitVal, _publicationType);
    }

    public boolean publicationTypeIsSet() {
        return _publicationTypeIsSet;
    }	

    public void setPublicationRange(String publicationRange) {
        if (!_publicationRangeIsSet) {
            _publicationRangeIsSet = true;
            _publicationRangeInitVal = publicationRange;
        }
        _publicationRange = publicationRange;
    }

    public String getPublicationRange() {
        return _publicationRange;
    }

    public String publicationRangeInitVal() {
        return _publicationRangeInitVal;
    }

	public boolean publicationRangeIsDirty() {
        return !valuesAreEqual(_publicationRangeInitVal, _publicationRange);
    }

    public boolean publicationRangeIsSet() {
        return _publicationRangeIsSet;
    }	

    public void setPublicationBulk(Integer publicationBulk) {
        if (!_publicationBulkIsSet) {
            _publicationBulkIsSet = true;
            _publicationBulkInitVal = publicationBulk;
        }
        _publicationBulk = publicationBulk;
    }

    public Integer getPublicationBulk() {
        return _publicationBulk;
    }

    public Integer publicationBulkInitVal() {
        return _publicationBulkInitVal;
    }

	public boolean publicationBulkIsDirty() {
        return !valuesAreEqual(_publicationBulkInitVal, _publicationBulk);
    }

    public boolean publicationBulkIsSet() {
        return _publicationBulkIsSet;
    }	

    public void setPublicationPosition(String publicationPosition) {
        if (!_publicationPositionIsSet) {
            _publicationPositionIsSet = true;
            _publicationPositionInitVal = publicationPosition;
        }
        _publicationPosition = publicationPosition;
    }

    public String getPublicationPosition() {
        return _publicationPosition;
    }

    public String publicationPositionInitVal() {
        return _publicationPositionInitVal;
    }

	public boolean publicationPositionIsDirty() {
        return !valuesAreEqual(_publicationPositionInitVal, _publicationPosition);
    }

    public boolean publicationPositionIsSet() {
        return _publicationPositionIsSet;
    }	

    public void setPublicationAdFormat(Integer publicationAdFormat) {
        if (!_publicationAdFormatIsSet) {
            _publicationAdFormatIsSet = true;
            _publicationAdFormatInitVal = publicationAdFormat;
        }
        _publicationAdFormat = publicationAdFormat;
    }

    public Integer getPublicationAdFormat() {
        return _publicationAdFormat;
    }

    public Integer publicationAdFormatInitVal() {
        return _publicationAdFormatInitVal;
    }

	public boolean publicationAdFormatIsDirty() {
        return !valuesAreEqual(_publicationAdFormatInitVal, _publicationAdFormat);
    }

    public boolean publicationAdFormatIsSet() {
        return _publicationAdFormatIsSet;
    }	

    public void setPublicationCampaign(Integer publicationCampaign) {
        if (!_publicationCampaignIsSet) {
            _publicationCampaignIsSet = true;
            _publicationCampaignInitVal = publicationCampaign;
        }
        _publicationCampaign = publicationCampaign;
    }

    public Integer getPublicationCampaign() {
        return _publicationCampaign;
    }

    public Integer publicationCampaignInitVal() {
        return _publicationCampaignInitVal;
    }

	public boolean publicationCampaignIsDirty() {
        return !valuesAreEqual(_publicationCampaignInitVal, _publicationCampaign);
    }

    public boolean publicationCampaignIsSet() {
        return _publicationCampaignIsSet;
    }	

    public void setBaseProvisionDeal1(Double baseProvisionDeal1) {
        if (!_baseProvisionDeal1IsSet) {
            _baseProvisionDeal1IsSet = true;
            _baseProvisionDeal1InitVal = baseProvisionDeal1;
        }
        _baseProvisionDeal1 = baseProvisionDeal1;
    }

    public Double getBaseProvisionDeal1() {
        return _baseProvisionDeal1;
    }

    public Double baseProvisionDeal1InitVal() {
        return _baseProvisionDeal1InitVal;
    }

	public boolean baseProvisionDeal1IsDirty() {
        return !valuesAreEqual(_baseProvisionDeal1InitVal, _baseProvisionDeal1);
    }

    public boolean baseProvisionDeal1IsSet() {
        return _baseProvisionDeal1IsSet;
    }	

    public void setBaseProvisionDeal2(Double baseProvisionDeal2) {
        if (!_baseProvisionDeal2IsSet) {
            _baseProvisionDeal2IsSet = true;
            _baseProvisionDeal2InitVal = baseProvisionDeal2;
        }
        _baseProvisionDeal2 = baseProvisionDeal2;
    }

    public Double getBaseProvisionDeal2() {
        return _baseProvisionDeal2;
    }

    public Double baseProvisionDeal2InitVal() {
        return _baseProvisionDeal2InitVal;
    }

	public boolean baseProvisionDeal2IsDirty() {
        return !valuesAreEqual(_baseProvisionDeal2InitVal, _baseProvisionDeal2);
    }

    public boolean baseProvisionDeal2IsSet() {
        return _baseProvisionDeal2IsSet;
    }	

    public void setBaseProvisionDeal3(Double baseProvisionDeal3) {
        if (!_baseProvisionDeal3IsSet) {
            _baseProvisionDeal3IsSet = true;
            _baseProvisionDeal3InitVal = baseProvisionDeal3;
        }
        _baseProvisionDeal3 = baseProvisionDeal3;
    }

    public Double getBaseProvisionDeal3() {
        return _baseProvisionDeal3;
    }

    public Double baseProvisionDeal3InitVal() {
        return _baseProvisionDeal3InitVal;
    }

	public boolean baseProvisionDeal3IsDirty() {
        return !valuesAreEqual(_baseProvisionDeal3InitVal, _baseProvisionDeal3);
    }

    public boolean baseProvisionDeal3IsSet() {
        return _baseProvisionDeal3IsSet;
    }	

    public void setBaseProvisionDeal4(Double baseProvisionDeal4) {
        if (!_baseProvisionDeal4IsSet) {
            _baseProvisionDeal4IsSet = true;
            _baseProvisionDeal4InitVal = baseProvisionDeal4;
        }
        _baseProvisionDeal4 = baseProvisionDeal4;
    }

    public Double getBaseProvisionDeal4() {
        return _baseProvisionDeal4;
    }

    public Double baseProvisionDeal4InitVal() {
        return _baseProvisionDeal4InitVal;
    }

	public boolean baseProvisionDeal4IsDirty() {
        return !valuesAreEqual(_baseProvisionDeal4InitVal, _baseProvisionDeal4);
    }

    public boolean baseProvisionDeal4IsSet() {
        return _baseProvisionDeal4IsSet;
    }	

    public void setPlacementUrl(String placementUrl) {
        if (!_placementUrlIsSet) {
            _placementUrlIsSet = true;
            _placementUrlInitVal = placementUrl;
        }
        _placementUrl = placementUrl;
    }

    public String getPlacementUrl() {
        return _placementUrl;
    }

    public String placementUrlInitVal() {
        return _placementUrlInitVal;
    }

	public boolean placementUrlIsDirty() {
        return !valuesAreEqual(_placementUrlInitVal, _placementUrl);
    }

    public boolean placementUrlIsSet() {
        return _placementUrlIsSet;
    }	
}
