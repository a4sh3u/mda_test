package de.smava.webapp.account.todo.financial;

public interface FreelancerIncomeFormIf {

	public boolean getEnabled();
	public boolean getExtended();
}
