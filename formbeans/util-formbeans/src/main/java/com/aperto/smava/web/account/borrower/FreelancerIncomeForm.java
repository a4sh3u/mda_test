//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package com.aperto.smava.web.account.borrower;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(freelancer income)}

import de.smava.webapp.account.domain.FreelancerIncomeCalculation;
import de.smava.webapp.commons.util.FormatUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.text.ParseException;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'FreelancerIncomeForm'.
 *
 * @author generator
 */
public class FreelancerIncomeForm implements Serializable, de.smava.webapp.account.domain.FreelancerIncomeCalculation, de.smava.webapp.account.todo.financial.FreelancerIncomeFormIf {

    public static final String ENABLED_EXPR = "freelancerIncomeForm.enabled";
    public static final String ENABLED_PATH = "command.freelancerIncomeForm.enabled";
    public static final String YEAR_EXPR = "freelancerIncomeForm.year";
    public static final String YEAR_PATH = "command.freelancerIncomeForm.year";
    public static final String DURATION_EXPR = "freelancerIncomeForm.duration";
    public static final String DURATION_PATH = "command.freelancerIncomeForm.duration";
    public static final String VOLUME_EXPR = "freelancerIncomeForm.volume";
    public static final String VOLUME_PATH = "command.freelancerIncomeForm.volume";
    public static final String EXPENSES_EXPR = "freelancerIncomeForm.expenses";
    public static final String EXPENSES_PATH = "command.freelancerIncomeForm.expenses";
    public static final String TRADE_TAX_EXPR = "freelancerIncomeForm.tradeTax";
    public static final String TRADE_TAX_PATH = "command.freelancerIncomeForm.tradeTax";
    public static final String INCOME_TAX_EXPR = "freelancerIncomeForm.incomeTax";
    public static final String INCOME_TAX_PATH = "command.freelancerIncomeForm.incomeTax";
    public static final String SOLIDARITY_TAX_EXPR = "freelancerIncomeForm.solidarityTax";
    public static final String SOLIDARITY_TAX_PATH = "command.freelancerIncomeForm.solidarityTax";
    public static final String CHURCH_TAX_EXPR = "freelancerIncomeForm.churchTax";
    public static final String CHURCH_TAX_PATH = "command.freelancerIncomeForm.churchTax";
    public static final String PERIOD_EARNINGS_BEFORE_TAXES_EXPR = "freelancerIncomeForm.periodEarningsBeforeTaxes";
    public static final String PERIOD_EARNINGS_BEFORE_TAXES_PATH = "command.freelancerIncomeForm.periodEarningsBeforeTaxes";
    public static final String ANNUAL_EARNINGS_BEFORE_TAXES_EXPR = "freelancerIncomeForm.annualEarningsBeforeTaxes";
    public static final String ANNUAL_EARNINGS_BEFORE_TAXES_PATH = "command.freelancerIncomeForm.annualEarningsBeforeTaxes";
    public static final String ANNUAL_PROFIT_AFTER_TAX_EXPR = "freelancerIncomeForm.annualProfitAfterTax";
    public static final String ANNUAL_PROFIT_AFTER_TAX_PATH = "command.freelancerIncomeForm.annualProfitAfterTax";
    public static final String MONTHLY_NET_INCOME_EXPR = "freelancerIncomeForm.monthlyNetIncome";
    public static final String MONTHLY_NET_INCOME_PATH = "command.freelancerIncomeForm.monthlyNetIncome";
    public static final String SPECIAL_RESERVE_EXPR = "freelancerIncomeForm.specialReserve";
    public static final String SPECIAL_RESERVE_PATH = "command.freelancerIncomeForm.specialReserve";
    public static final String SPECIAL_RESERVE_DISSOLUTION_EXPR = "freelancerIncomeForm.specialReserveDissolution";
    public static final String SPECIAL_RESERVE_DISSOLUTION_PATH = "command.freelancerIncomeForm.specialReserveDissolution";
    public static final String NON_CASH_EXPENSES_EXPR = "freelancerIncomeForm.nonCashExpenses";
    public static final String NON_CASH_EXPENSES_PATH = "command.freelancerIncomeForm.nonCashExpenses";
    public static final String OWN_CONSUMPTION_EXPR = "freelancerIncomeForm.ownConsumption";
    public static final String OWN_CONSUMPTION_PATH = "command.freelancerIncomeForm.ownConsumption";
    public static final String NON_CASH_REVENUES_EXPR = "freelancerIncomeForm.nonCashRevenues";
    public static final String NON_CASH_REVENUES_PATH = "command.freelancerIncomeForm.nonCashRevenues";
    public static final String ADJUSTMENTS_EXPR = "freelancerIncomeForm.adjustments";
    public static final String ADJUSTMENTS_PATH = "command.freelancerIncomeForm.adjustments";
    public static final String PROFIT_EXPR = "freelancerIncomeForm.profit";
    public static final String PROFIT_PATH = "command.freelancerIncomeForm.profit";
    public static final String EXTENDED_EXPR = "freelancerIncomeForm.extended";
    public static final String EXTENDED_PATH = "command.freelancerIncomeForm.extended";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(freelancer income)}

    private static final Logger LOGGER = Logger.getLogger(FreelancerIncomeForm.class);
    

	public Double getExpensesValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getExpenses())) {
				result = FormatUtils.parseAmount(getExpenses());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}

	public Double getVolumeValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getVolume())) {
				result = FormatUtils.parseAmount(getVolume());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}

	public void setExpensesValue(Double expenses) {
		if (expenses != null) {
			setExpenses(FormatUtils.formatAmount(expenses));
		} else {
			setExpenses(null);
		}
		
	}

	public void setVolumeValue(Double volume) {
		if (volume == null) {
			setVolume(null);
		} else {
			setVolume(FormatUtils.formatAmount(volume));
		}		
	}
	
	public Double getIncomeTaxValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getIncomeTax())) {
				result = FormatUtils.parseAmount(getIncomeTax());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}

	public Double getOwnConsumptionValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getOwnConsumption())) {
				result = FormatUtils.parseAmount(getOwnConsumption());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}

	public Double getSpecialReserveDissolutionValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getSpecialReserveDissolution())) {
				result = FormatUtils.parseAmount(getSpecialReserveDissolution());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}

	public Double getSpecialReserveValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getSpecialReserve())) {
				result = FormatUtils.parseAmount(getSpecialReserve());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}

	public void setIncomeTaxValue(Double incomeTaxValue) {
		if (incomeTaxValue == null) {
			setIncomeTax(null);
		} else {
			setIncomeTax(FormatUtils.formatAmount(incomeTaxValue));
		}		
	}

	public void setOwnConsumptionValue(Double ownConsumptionValue) {
		if (ownConsumptionValue == null) {
			setOwnConsumption(null);
		} else {
			setOwnConsumption(FormatUtils.formatAmount(ownConsumptionValue));
		}		
	}

	public void setSpecialReserveDissolutionValue(Double specialReserveDissolutionValue) {
		if (specialReserveDissolutionValue == null) {
			setSpecialReserveDissolution(null);
		} else {
			setSpecialReserveDissolution(FormatUtils.formatAmount(specialReserveDissolutionValue));
		}		
	}

	public void setSpecialReserveValue(Double specialReserveValue) {
		if (specialReserveValue == null) {
			setSpecialReserve(null);
		} else {
			setSpecialReserve(FormatUtils.formatAmount(specialReserveValue));
		}		
	}
	
	public void setChurchTaxValue(Double churchTaxValue) {
		if (churchTaxValue == null) {
			setChurchTax(null);
		} else {
			setChurchTax(FormatUtils.formatAmount(churchTaxValue));
		}
	}
	
	public Double getChurchTaxValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getChurchTax())) {
				result = FormatUtils.parseAmount(getChurchTax());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}
	
	public void setSolidarityTaxValue(Double solidarityTaxValue) {
		if (solidarityTaxValue == null) {
			setSolidarityTax(null);
		} else {
			setSolidarityTax(FormatUtils.formatAmount(solidarityTaxValue));
		}
	}
	
	public Double getSolidarityTaxValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getSolidarityTax())) {
				result = FormatUtils.parseAmount(getSolidarityTax());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}
	
	public void setTradeTaxValue(Double tradeTaxValue) {
		if (tradeTaxValue == null) {
			setTradeTax(null);
		} else {
			setTradeTax(FormatUtils.formatAmount(tradeTaxValue));
		}
	}
	
	public Double getTradeTaxValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getTradeTax())) {
				result = FormatUtils.parseAmount(getTradeTax());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}
	
	public void setNonCashRevenuesValue(Double nonCashRevenuesValue) {
		if (nonCashRevenuesValue == null) {
			setNonCashRevenues(null);
		} else {
			setNonCashRevenues(FormatUtils.formatAmount(nonCashRevenuesValue));
		}
	}
	
	public Double getNonCashRevenuesValue() {
		Double result = null;
		try {
			if (StringUtils.isNotEmpty(getNonCashRevenues())) {
				result = FormatUtils.parseAmount(getNonCashRevenues());
			}
		} catch (ParseException e) {
			LOGGER.debug(e);
		}
		return result;
	}
	

// !!!!!!!! End of insert code section !!!!!!!!

    private boolean _enabled;
    private boolean _enabledInitVal;
    private boolean _enabledIsSet;
    private Integer _year;
    private Integer _yearInitVal;
    private boolean _yearIsSet;
    private Integer _duration;
    private Integer _durationInitVal;
    private boolean _durationIsSet;
    private String _volume;
    private String _volumeInitVal;
    private boolean _volumeIsSet;
    private String _expenses;
    private String _expensesInitVal;
    private boolean _expensesIsSet;
    private String _tradeTax;
    private String _tradeTaxInitVal;
    private boolean _tradeTaxIsSet;
    private String _incomeTax;
    private String _incomeTaxInitVal;
    private boolean _incomeTaxIsSet;
    private String _solidarityTax;
    private String _solidarityTaxInitVal;
    private boolean _solidarityTaxIsSet;
    private String _churchTax;
    private String _churchTaxInitVal;
    private boolean _churchTaxIsSet;
    private Double _periodEarningsBeforeTaxes;
    private Double _periodEarningsBeforeTaxesInitVal;
    private boolean _periodEarningsBeforeTaxesIsSet;
    private Double _annualEarningsBeforeTaxes;
    private Double _annualEarningsBeforeTaxesInitVal;
    private boolean _annualEarningsBeforeTaxesIsSet;
    private Double _annualProfitAfterTax;
    private Double _annualProfitAfterTaxInitVal;
    private boolean _annualProfitAfterTaxIsSet;
    private Double _monthlyNetIncome;
    private Double _monthlyNetIncomeInitVal;
    private boolean _monthlyNetIncomeIsSet;
    private String _specialReserve;
    private String _specialReserveInitVal;
    private boolean _specialReserveIsSet;
    private String _specialReserveDissolution;
    private String _specialReserveDissolutionInitVal;
    private boolean _specialReserveDissolutionIsSet;
    private Double _nonCashExpenses;
    private Double _nonCashExpensesInitVal;
    private boolean _nonCashExpensesIsSet;
    private String _ownConsumption;
    private String _ownConsumptionInitVal;
    private boolean _ownConsumptionIsSet;
    private String _nonCashRevenues;
    private String _nonCashRevenuesInitVal;
    private boolean _nonCashRevenuesIsSet;
    private Double _adjustments;
    private Double _adjustmentsInitVal;
    private boolean _adjustmentsIsSet;
    private Double _profit;
    private Double _profitInitVal;
    private boolean _profitIsSet;
    private boolean _extended;
    private boolean _extendedInitVal;
    private boolean _extendedIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setEnabled(boolean enabled) {
        if (!_enabledIsSet) {
            _enabledIsSet = true;
            _enabledInitVal = enabled;
        }
        _enabled = enabled;
    }

    public boolean getEnabled() {
        return _enabled;
    }

    public boolean enabledInitVal() {
        return _enabledInitVal;
    }

	public boolean enabledIsDirty() {
        return !valuesAreEqual(_enabledInitVal, _enabled);
    }

    public boolean enabledIsSet() {
        return _enabledIsSet;
    }	


    public void setYear(Integer year) {
        if (!_yearIsSet) {
            _yearIsSet = true;
            _yearInitVal = year;
        }
        _year = year;
    }

    public Integer getYear() {
        return _year;
    }

    public Integer yearInitVal() {
        return _yearInitVal;
    }

	public boolean yearIsDirty() {
        return !valuesAreEqual(_yearInitVal, _year);
    }

    public boolean yearIsSet() {
        return _yearIsSet;
    }	


    public void setDuration(Integer duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = duration;
        }
        _duration = duration;
    }

    public Integer getDuration() {
        return _duration;
    }

    public Integer durationInitVal() {
        return _durationInitVal;
    }

	public boolean durationIsDirty() {
        return !valuesAreEqual(_durationInitVal, _duration);
    }

    public boolean durationIsSet() {
        return _durationIsSet;
    }	


    public void setVolume(String volume) {
        if (!_volumeIsSet) {
            _volumeIsSet = true;
            _volumeInitVal = volume;
        }
        _volume = volume;
    }

    public String getVolume() {
        return _volume;
    }

    public String volumeInitVal() {
        return _volumeInitVal;
    }

	public boolean volumeIsDirty() {
        return !valuesAreEqual(_volumeInitVal, _volume);
    }

    public boolean volumeIsSet() {
        return _volumeIsSet;
    }	


    public void setExpenses(String expenses) {
        if (!_expensesIsSet) {
            _expensesIsSet = true;
            _expensesInitVal = expenses;
        }
        _expenses = expenses;
    }

    public String getExpenses() {
        return _expenses;
    }

    public String expensesInitVal() {
        return _expensesInitVal;
    }

	public boolean expensesIsDirty() {
        return !valuesAreEqual(_expensesInitVal, _expenses);
    }

    public boolean expensesIsSet() {
        return _expensesIsSet;
    }	


    public void setTradeTax(String tradeTax) {
        if (!_tradeTaxIsSet) {
            _tradeTaxIsSet = true;
            _tradeTaxInitVal = tradeTax;
        }
        _tradeTax = tradeTax;
    }

    public String getTradeTax() {
        return _tradeTax;
    }

    public String tradeTaxInitVal() {
        return _tradeTaxInitVal;
    }

	public boolean tradeTaxIsDirty() {
        return !valuesAreEqual(_tradeTaxInitVal, _tradeTax);
    }

    public boolean tradeTaxIsSet() {
        return _tradeTaxIsSet;
    }	


    public void setIncomeTax(String incomeTax) {
        if (!_incomeTaxIsSet) {
            _incomeTaxIsSet = true;
            _incomeTaxInitVal = incomeTax;
        }
        _incomeTax = incomeTax;
    }

    public String getIncomeTax() {
        return _incomeTax;
    }

    public String incomeTaxInitVal() {
        return _incomeTaxInitVal;
    }

	public boolean incomeTaxIsDirty() {
        return !valuesAreEqual(_incomeTaxInitVal, _incomeTax);
    }

    public boolean incomeTaxIsSet() {
        return _incomeTaxIsSet;
    }	


    public void setSolidarityTax(String solidarityTax) {
        if (!_solidarityTaxIsSet) {
            _solidarityTaxIsSet = true;
            _solidarityTaxInitVal = solidarityTax;
        }
        _solidarityTax = solidarityTax;
    }

    public String getSolidarityTax() {
        return _solidarityTax;
    }

    public String solidarityTaxInitVal() {
        return _solidarityTaxInitVal;
    }

	public boolean solidarityTaxIsDirty() {
        return !valuesAreEqual(_solidarityTaxInitVal, _solidarityTax);
    }

    public boolean solidarityTaxIsSet() {
        return _solidarityTaxIsSet;
    }	


    public void setChurchTax(String churchTax) {
        if (!_churchTaxIsSet) {
            _churchTaxIsSet = true;
            _churchTaxInitVal = churchTax;
        }
        _churchTax = churchTax;
    }

    public String getChurchTax() {
        return _churchTax;
    }

    public String churchTaxInitVal() {
        return _churchTaxInitVal;
    }

	public boolean churchTaxIsDirty() {
        return !valuesAreEqual(_churchTaxInitVal, _churchTax);
    }

    public boolean churchTaxIsSet() {
        return _churchTaxIsSet;
    }	


    public void setPeriodEarningsBeforeTaxes(Double periodEarningsBeforeTaxes) {
        if (!_periodEarningsBeforeTaxesIsSet) {
            _periodEarningsBeforeTaxesIsSet = true;
            _periodEarningsBeforeTaxesInitVal = periodEarningsBeforeTaxes;
        }
        _periodEarningsBeforeTaxes = periodEarningsBeforeTaxes;
    }

    public Double getPeriodEarningsBeforeTaxes() {
        return _periodEarningsBeforeTaxes;
    }

    public Double periodEarningsBeforeTaxesInitVal() {
        return _periodEarningsBeforeTaxesInitVal;
    }

	public boolean periodEarningsBeforeTaxesIsDirty() {
        return !valuesAreEqual(_periodEarningsBeforeTaxesInitVal, _periodEarningsBeforeTaxes);
    }

    public boolean periodEarningsBeforeTaxesIsSet() {
        return _periodEarningsBeforeTaxesIsSet;
    }	


    public void setAnnualEarningsBeforeTaxes(Double annualEarningsBeforeTaxes) {
        if (!_annualEarningsBeforeTaxesIsSet) {
            _annualEarningsBeforeTaxesIsSet = true;
            _annualEarningsBeforeTaxesInitVal = annualEarningsBeforeTaxes;
        }
        _annualEarningsBeforeTaxes = annualEarningsBeforeTaxes;
    }

    public Double getAnnualEarningsBeforeTaxes() {
        return _annualEarningsBeforeTaxes;
    }

    public Double annualEarningsBeforeTaxesInitVal() {
        return _annualEarningsBeforeTaxesInitVal;
    }

	public boolean annualEarningsBeforeTaxesIsDirty() {
        return !valuesAreEqual(_annualEarningsBeforeTaxesInitVal, _annualEarningsBeforeTaxes);
    }

    public boolean annualEarningsBeforeTaxesIsSet() {
        return _annualEarningsBeforeTaxesIsSet;
    }	


    public void setAnnualProfitAfterTax(Double annualProfitAfterTax) {
        if (!_annualProfitAfterTaxIsSet) {
            _annualProfitAfterTaxIsSet = true;
            _annualProfitAfterTaxInitVal = annualProfitAfterTax;
        }
        _annualProfitAfterTax = annualProfitAfterTax;
    }

    public Double getAnnualProfitAfterTax() {
        return _annualProfitAfterTax;
    }

    public Double annualProfitAfterTaxInitVal() {
        return _annualProfitAfterTaxInitVal;
    }

	public boolean annualProfitAfterTaxIsDirty() {
        return !valuesAreEqual(_annualProfitAfterTaxInitVal, _annualProfitAfterTax);
    }

    public boolean annualProfitAfterTaxIsSet() {
        return _annualProfitAfterTaxIsSet;
    }	


    public void setMonthlyNetIncome(Double monthlyNetIncome) {
        if (!_monthlyNetIncomeIsSet) {
            _monthlyNetIncomeIsSet = true;
            _monthlyNetIncomeInitVal = monthlyNetIncome;
        }
        _monthlyNetIncome = monthlyNetIncome;
    }

    public Double getMonthlyNetIncome() {
        return _monthlyNetIncome;
    }

    public Double monthlyNetIncomeInitVal() {
        return _monthlyNetIncomeInitVal;
    }

	public boolean monthlyNetIncomeIsDirty() {
        return !valuesAreEqual(_monthlyNetIncomeInitVal, _monthlyNetIncome);
    }

    public boolean monthlyNetIncomeIsSet() {
        return _monthlyNetIncomeIsSet;
    }	


    public void setSpecialReserve(String specialReserve) {
        if (!_specialReserveIsSet) {
            _specialReserveIsSet = true;
            _specialReserveInitVal = specialReserve;
        }
        _specialReserve = specialReserve;
    }

    public String getSpecialReserve() {
        return _specialReserve;
    }

    public String specialReserveInitVal() {
        return _specialReserveInitVal;
    }

	public boolean specialReserveIsDirty() {
        return !valuesAreEqual(_specialReserveInitVal, _specialReserve);
    }

    public boolean specialReserveIsSet() {
        return _specialReserveIsSet;
    }	


    public void setSpecialReserveDissolution(String specialReserveDissolution) {
        if (!_specialReserveDissolutionIsSet) {
            _specialReserveDissolutionIsSet = true;
            _specialReserveDissolutionInitVal = specialReserveDissolution;
        }
        _specialReserveDissolution = specialReserveDissolution;
    }

    public String getSpecialReserveDissolution() {
        return _specialReserveDissolution;
    }

    public String specialReserveDissolutionInitVal() {
        return _specialReserveDissolutionInitVal;
    }

	public boolean specialReserveDissolutionIsDirty() {
        return !valuesAreEqual(_specialReserveDissolutionInitVal, _specialReserveDissolution);
    }

    public boolean specialReserveDissolutionIsSet() {
        return _specialReserveDissolutionIsSet;
    }	


    public void setNonCashExpenses(Double nonCashExpenses) {
        if (!_nonCashExpensesIsSet) {
            _nonCashExpensesIsSet = true;
            _nonCashExpensesInitVal = nonCashExpenses;
        }
        _nonCashExpenses = nonCashExpenses;
    }

    public Double getNonCashExpenses() {
        return _nonCashExpenses;
    }

    public Double nonCashExpensesInitVal() {
        return _nonCashExpensesInitVal;
    }

	public boolean nonCashExpensesIsDirty() {
        return !valuesAreEqual(_nonCashExpensesInitVal, _nonCashExpenses);
    }

    public boolean nonCashExpensesIsSet() {
        return _nonCashExpensesIsSet;
    }	


    public void setOwnConsumption(String ownConsumption) {
        if (!_ownConsumptionIsSet) {
            _ownConsumptionIsSet = true;
            _ownConsumptionInitVal = ownConsumption;
        }
        _ownConsumption = ownConsumption;
    }

    public String getOwnConsumption() {
        return _ownConsumption;
    }

    public String ownConsumptionInitVal() {
        return _ownConsumptionInitVal;
    }

	public boolean ownConsumptionIsDirty() {
        return !valuesAreEqual(_ownConsumptionInitVal, _ownConsumption);
    }

    public boolean ownConsumptionIsSet() {
        return _ownConsumptionIsSet;
    }	


    public void setNonCashRevenues(String nonCashRevenues) {
        if (!_nonCashRevenuesIsSet) {
            _nonCashRevenuesIsSet = true;
            _nonCashRevenuesInitVal = nonCashRevenues;
        }
        _nonCashRevenues = nonCashRevenues;
    }

    public String getNonCashRevenues() {
        return _nonCashRevenues;
    }

    public String nonCashRevenuesInitVal() {
        return _nonCashRevenuesInitVal;
    }

	public boolean nonCashRevenuesIsDirty() {
        return !valuesAreEqual(_nonCashRevenuesInitVal, _nonCashRevenues);
    }

    public boolean nonCashRevenuesIsSet() {
        return _nonCashRevenuesIsSet;
    }	


    public void setAdjustments(Double adjustments) {
        if (!_adjustmentsIsSet) {
            _adjustmentsIsSet = true;
            _adjustmentsInitVal = adjustments;
        }
        _adjustments = adjustments;
    }

    public Double getAdjustments() {
        return _adjustments;
    }

    public Double adjustmentsInitVal() {
        return _adjustmentsInitVal;
    }

	public boolean adjustmentsIsDirty() {
        return !valuesAreEqual(_adjustmentsInitVal, _adjustments);
    }

    public boolean adjustmentsIsSet() {
        return _adjustmentsIsSet;
    }	


    public void setProfit(Double profit) {
        if (!_profitIsSet) {
            _profitIsSet = true;
            _profitInitVal = profit;
        }
        _profit = profit;
    }

    public Double getProfit() {
        return _profit;
    }

    public Double profitInitVal() {
        return _profitInitVal;
    }

	public boolean profitIsDirty() {
        return !valuesAreEqual(_profitInitVal, _profit);
    }

    public boolean profitIsSet() {
        return _profitIsSet;
    }	


    public void setExtended(boolean extended) {
        if (!_extendedIsSet) {
            _extendedIsSet = true;
            _extendedInitVal = extended;
        }
        _extended = extended;
    }

    public boolean getExtended() {
        return _extended;
    }

    public boolean extendedInitVal() {
        return _extendedInitVal;
    }

	public boolean extendedIsDirty() {
        return !valuesAreEqual(_extendedInitVal, _extended);
    }

    public boolean extendedIsSet() {
        return _extendedIsSet;
    }	

}
