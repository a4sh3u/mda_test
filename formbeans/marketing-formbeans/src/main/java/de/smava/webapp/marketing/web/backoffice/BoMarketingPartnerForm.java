//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing partner)}
import de.smava.webapp.commons.web.ImageAwareCommand;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoMarketingPartnerForm'.
 *
 * @author generator
 */
public class BoMarketingPartnerForm implements ImageAwareCommand {

    public static final String NAME_EXPR = "boMarketingPartnerForm.name";
    public static final String NAME_PATH = "command.boMarketingPartnerForm.name";
    public static final String CREATION_DATE_EXPR = "boMarketingPartnerForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boMarketingPartnerForm.creationDate";
    public static final String STATE_EXPR = "boMarketingPartnerForm.state";
    public static final String STATE_PATH = "command.boMarketingPartnerForm.state";
    public static final String STREET_EXPR = "boMarketingPartnerForm.street";
    public static final String STREET_PATH = "command.boMarketingPartnerForm.street";
    public static final String STREET_NUMBER_EXPR = "boMarketingPartnerForm.streetNumber";
    public static final String STREET_NUMBER_PATH = "command.boMarketingPartnerForm.streetNumber";
    public static final String ZIP_CODE_EXPR = "boMarketingPartnerForm.zipCode";
    public static final String ZIP_CODE_PATH = "command.boMarketingPartnerForm.zipCode";
    public static final String CITY_EXPR = "boMarketingPartnerForm.city";
    public static final String CITY_PATH = "command.boMarketingPartnerForm.city";
    public static final String BANK_ACCOUNT_EXPR = "boMarketingPartnerForm.bankAccount";
    public static final String BANK_ACCOUNT_PATH = "command.boMarketingPartnerForm.bankAccount";
    public static final String BANK_CODE_EXPR = "boMarketingPartnerForm.bankCode";
    public static final String BANK_CODE_PATH = "command.boMarketingPartnerForm.bankCode";
    public static final String BANK_EXPR = "boMarketingPartnerForm.bank";
    public static final String BANK_PATH = "command.boMarketingPartnerForm.bank";
    public static final String IMAGE_EXPR = "boMarketingPartnerForm.image";
    public static final String IMAGE_PATH = "command.boMarketingPartnerForm.image";
    public static final String IMAGE_HEIGHT_EXPR = "boMarketingPartnerForm.imageHeight";
    public static final String IMAGE_HEIGHT_PATH = "command.boMarketingPartnerForm.imageHeight";
    public static final String IMAGE_WIDTH_EXPR = "boMarketingPartnerForm.imageWidth";
    public static final String IMAGE_WIDTH_PATH = "command.boMarketingPartnerForm.imageWidth";
    public static final String MARKETING_CONTACT_EXPR = "boMarketingPartnerForm.marketingContact";
    public static final String MARKETING_CONTACT_PATH = "command.boMarketingPartnerForm.marketingContact";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing partner)}
    private byte[] _file = null;
    private boolean _isNewUpload;
    
    public byte[] getFile() {
        byte[] file = null;
        if (_file != null) {
            file = _file.clone();
        }
        return file;
    }

    public void setFile(byte[] file) {
        _file = file.clone();
    }

    public String getNoImageExpression() {
        return null;
    }

    public String getImageFileExpression() {
        return "image";
    }

    public void updateNoImage() {
        // Nothing to do
    }

    public void resetForm() {
        // Nothing to do
    }

    public String getImageCategory() {
        return IMAGE_CATEGORY_MARKETING;
    }
    
	public boolean isNewUpload() {
		return _isNewUpload;
	}

	public void setNewUpload(boolean isNewUpload) {
		_isNewUpload = isNewUpload;
	}
    
// !!!!!!!! End of insert code section !!!!!!!!

    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _creationDate;
    private String _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _state;
    private String _stateInitVal;
    private boolean _stateIsSet;
    private String _street;
    private String _streetInitVal;
    private boolean _streetIsSet;
    private String _streetNumber;
    private String _streetNumberInitVal;
    private boolean _streetNumberIsSet;
    private String _zipCode;
    private String _zipCodeInitVal;
    private boolean _zipCodeIsSet;
    private String _city;
    private String _cityInitVal;
    private boolean _cityIsSet;
    private String _bankAccount;
    private String _bankAccountInitVal;
    private boolean _bankAccountIsSet;
    private String _bankCode;
    private String _bankCodeInitVal;
    private boolean _bankCodeIsSet;
    private String _bank;
    private String _bankInitVal;
    private boolean _bankIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private int _imageHeight;
    private int _imageHeightInitVal;
    private boolean _imageHeightIsSet;
    private int _imageWidth;
    private int _imageWidthInitVal;
    private boolean _imageWidthIsSet;
    private String _marketingContact;
    private String _marketingContactInitVal;
    private boolean _marketingContactIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setCreationDate(String creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = creationDate;
        }
        _creationDate = creationDate;
    }

    public String getCreationDate() {
        return _creationDate;
    }

    public String creationDateInitVal() {
        return _creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = state;
        }
        _state = state;
    }

    public String getState() {
        return _state;
    }

    public String stateInitVal() {
        return _stateInitVal;
    }

	public boolean stateIsDirty() {
        return !valuesAreEqual(_stateInitVal, _state);
    }

    public boolean stateIsSet() {
        return _stateIsSet;
    }	


    public void setStreet(String street) {
        if (!_streetIsSet) {
            _streetIsSet = true;
            _streetInitVal = street;
        }
        _street = street;
    }

    public String getStreet() {
        return _street;
    }

    public String streetInitVal() {
        return _streetInitVal;
    }

	public boolean streetIsDirty() {
        return !valuesAreEqual(_streetInitVal, _street);
    }

    public boolean streetIsSet() {
        return _streetIsSet;
    }	


    public void setStreetNumber(String streetNumber) {
        if (!_streetNumberIsSet) {
            _streetNumberIsSet = true;
            _streetNumberInitVal = streetNumber;
        }
        _streetNumber = streetNumber;
    }

    public String getStreetNumber() {
        return _streetNumber;
    }

    public String streetNumberInitVal() {
        return _streetNumberInitVal;
    }

	public boolean streetNumberIsDirty() {
        return !valuesAreEqual(_streetNumberInitVal, _streetNumber);
    }

    public boolean streetNumberIsSet() {
        return _streetNumberIsSet;
    }	


    public void setZipCode(String zipCode) {
        if (!_zipCodeIsSet) {
            _zipCodeIsSet = true;
            _zipCodeInitVal = zipCode;
        }
        _zipCode = zipCode;
    }

    public String getZipCode() {
        return _zipCode;
    }

    public String zipCodeInitVal() {
        return _zipCodeInitVal;
    }

	public boolean zipCodeIsDirty() {
        return !valuesAreEqual(_zipCodeInitVal, _zipCode);
    }

    public boolean zipCodeIsSet() {
        return _zipCodeIsSet;
    }	


    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = city;
        }
        _city = city;
    }

    public String getCity() {
        return _city;
    }

    public String cityInitVal() {
        return _cityInitVal;
    }

	public boolean cityIsDirty() {
        return !valuesAreEqual(_cityInitVal, _city);
    }

    public boolean cityIsSet() {
        return _cityIsSet;
    }	


    public void setBankAccount(String bankAccount) {
        if (!_bankAccountIsSet) {
            _bankAccountIsSet = true;
            _bankAccountInitVal = bankAccount;
        }
        _bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return _bankAccount;
    }

    public String bankAccountInitVal() {
        return _bankAccountInitVal;
    }

	public boolean bankAccountIsDirty() {
        return !valuesAreEqual(_bankAccountInitVal, _bankAccount);
    }

    public boolean bankAccountIsSet() {
        return _bankAccountIsSet;
    }	


    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = bankCode;
        }
        _bankCode = bankCode;
    }

    public String getBankCode() {
        return _bankCode;
    }

    public String bankCodeInitVal() {
        return _bankCodeInitVal;
    }

	public boolean bankCodeIsDirty() {
        return !valuesAreEqual(_bankCodeInitVal, _bankCode);
    }

    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }	


    public void setBank(String bank) {
        if (!_bankIsSet) {
            _bankIsSet = true;
            _bankInitVal = bank;
        }
        _bank = bank;
    }

    public String getBank() {
        return _bank;
    }

    public String bankInitVal() {
        return _bankInitVal;
    }

	public boolean bankIsDirty() {
        return !valuesAreEqual(_bankInitVal, _bank);
    }

    public boolean bankIsSet() {
        return _bankIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = imageHeight;
        }
        _imageHeight = imageHeight;
    }

    public int getImageHeight() {
        return _imageHeight;
    }

    public int imageHeightInitVal() {
        return _imageHeightInitVal;
    }

	public boolean imageHeightIsDirty() {
        return !valuesAreEqual(_imageHeightInitVal, _imageHeight);
    }

    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }	


    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = imageWidth;
        }
        _imageWidth = imageWidth;
    }

    public int getImageWidth() {
        return _imageWidth;
    }

    public int imageWidthInitVal() {
        return _imageWidthInitVal;
    }

	public boolean imageWidthIsDirty() {
        return !valuesAreEqual(_imageWidthInitVal, _imageWidth);
    }

    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }	


    public void setMarketingContact(String marketingContact) {
        if (!_marketingContactIsSet) {
            _marketingContactIsSet = true;
            _marketingContactInitVal = marketingContact;
        }
        _marketingContact = marketingContact;
    }

    public String getMarketingContact() {
        return _marketingContact;
    }

    public String marketingContactInitVal() {
        return _marketingContactInitVal;
    }

	public boolean marketingContactIsDirty() {
        return !valuesAreEqual(_marketingContactInitVal, _marketingContact);
    }

    public boolean marketingContactIsSet() {
        return _marketingContactIsSet;
    }	

}
