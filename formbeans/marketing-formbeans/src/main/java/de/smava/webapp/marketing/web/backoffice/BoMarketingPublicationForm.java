//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;



// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing publication)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoMarketingPublicationForm'.
 *
 * @author generator
 */
public class BoMarketingPublicationForm implements Serializable {

    public static final String NAME_EXPR = "boMarketingPublicationForm.name";
    public static final String NAME_PATH = "command.boMarketingPublicationForm.name";
    public static final String TYPE_EXPR = "boMarketingPublicationForm.type";
    public static final String TYPE_PATH = "command.boMarketingPublicationForm.type";
    public static final String RANGE_EXPR = "boMarketingPublicationForm.range";
    public static final String RANGE_PATH = "command.boMarketingPublicationForm.range";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing publication)}
// !!!!!!!! End of insert code section !!!!!!!!


    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;

    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;

    private String _range;
    private String _rangeInitVal;
    private boolean _rangeIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

    public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }

    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

    public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }

    public void setRange(String range) {
        if (!_rangeIsSet) {
            _rangeIsSet = true;
            _rangeInitVal = range;
        }
        _range = range;
    }

    public String getRange() {
        return _range;
    }

    public String rangeInitVal() {
        return _rangeInitVal;
    }

    public boolean rangeIsDirty() {
        return !valuesAreEqual(_rangeInitVal, _range);
    }

    public boolean rangeIsSet() {
        return _rangeIsSet;
    }

}
