//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\model.xml
//
//    Or you can change the template:
//
//    C:\Programme\eclipse\workspace\smava-trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing ad format)}

import de.smava.webapp.commons.web.ImageAwareCommand;
import de.smava.webapp.marketing.domain.MarketingAdvertisement;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoMarketingAdFormatForm'.
 *
 * @author generator
 */
public class BoMarketingAdFormatForm implements ImageAwareCommand {

    public static final String TEXT_EXPR = "boMarketingAdFormatForm.text";
    public static final String TEXT_PATH = "command.boMarketingAdFormatForm.text";
    public static final String IMAGE_EXPR = "boMarketingAdFormatForm.image";
    public static final String IMAGE_PATH = "command.boMarketingAdFormatForm.image";
    public static final String IMAGE_HEIGHT_EXPR = "boMarketingAdFormatForm.imageHeight";
    public static final String IMAGE_HEIGHT_PATH = "command.boMarketingAdFormatForm.imageHeight";
    public static final String IMAGE_WIDTH_EXPR = "boMarketingAdFormatForm.imageWidth";
    public static final String IMAGE_WIDTH_PATH = "command.boMarketingAdFormatForm.imageWidth";
    public static final String TYPE_EXPR = "boMarketingAdFormatForm.type";
    public static final String TYPE_PATH = "command.boMarketingAdFormatForm.type";
    public static final String MARKETING_ADVERTISEMENT_EXPR = "boMarketingAdFormatForm.marketingAdvertisement";
    public static final String MARKETING_ADVERTISEMENT_PATH = "command.boMarketingAdFormatForm.marketingAdvertisement";
    public static final String ACTION_EXPR = "boMarketingAdFormatForm.action";
    public static final String ACTION_PATH = "command.boMarketingAdFormatForm.action";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing ad format)}

    
    private boolean _isNewUpload;
    private byte[] _file = null;

    public byte[] getFile() {
        byte[] file = null;
        if (_file != null) {
            file = _file.clone();
        }
        return file;
    }

    public void setFile(byte[] file) {
        _file = file.clone();
    }

    public String getNoImageExpression() {
        return null;
    }

    public String getImageFileExpression() {
        return "image";
    }

    public void updateNoImage() {
        // Nothing to do
    }

    public void resetForm() {
        // Nothing to do
    }
    
    public String getImageCategory() {
        return IMAGE_CATEGORY_MARKETING;
    }    
    
	public boolean isNewUpload() {
		return _isNewUpload;
	}

	public void setNewUpload(boolean isNewUpload) {
		_isNewUpload = isNewUpload;
	}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _text;
    private String _textInitVal;
    private boolean _textIsSet;
    private String _image;
    private String _imageInitVal;
    private boolean _imageIsSet;
    private int _imageHeight;
    private int _imageHeightInitVal;
    private boolean _imageHeightIsSet;
    private int _imageWidth;
    private int _imageWidthInitVal;
    private boolean _imageWidthIsSet;
    private String _type;
    private String _typeInitVal;
    private boolean _typeIsSet;
    private MarketingAdvertisement _marketingAdvertisement;
    private MarketingAdvertisement _marketingAdvertisementInitVal;
    private boolean _marketingAdvertisementIsSet;
    private String _action;
    private String _actionInitVal;
    private boolean _actionIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setText(String text) {
        if (!_textIsSet) {
            _textIsSet = true;
            _textInitVal = text;
        }
        _text = text;
    }

    public String getText() {
        return _text;
    }

    public String textInitVal() {
        return _textInitVal;
    }

	public boolean textIsDirty() {
        return !valuesAreEqual(_textInitVal, _text);
    }

    public boolean textIsSet() {
        return _textIsSet;
    }	


    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = image;
        }
        _image = image;
    }

    public String getImage() {
        return _image;
    }

    public String imageInitVal() {
        return _imageInitVal;
    }

	public boolean imageIsDirty() {
        return !valuesAreEqual(_imageInitVal, _image);
    }

    public boolean imageIsSet() {
        return _imageIsSet;
    }	


    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = imageHeight;
        }
        _imageHeight = imageHeight;
    }

    public int getImageHeight() {
        return _imageHeight;
    }

    public int imageHeightInitVal() {
        return _imageHeightInitVal;
    }

	public boolean imageHeightIsDirty() {
        return !valuesAreEqual(_imageHeightInitVal, _imageHeight);
    }

    public boolean imageHeightIsSet() {
        return _imageHeightIsSet;
    }	


    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = imageWidth;
        }
        _imageWidth = imageWidth;
    }

    public int getImageWidth() {
        return _imageWidth;
    }

    public int imageWidthInitVal() {
        return _imageWidthInitVal;
    }

	public boolean imageWidthIsDirty() {
        return !valuesAreEqual(_imageWidthInitVal, _imageWidth);
    }

    public boolean imageWidthIsSet() {
        return _imageWidthIsSet;
    }	


    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = type;
        }
        _type = type;
    }

    public String getType() {
        return _type;
    }

    public String typeInitVal() {
        return _typeInitVal;
    }

	public boolean typeIsDirty() {
        return !valuesAreEqual(_typeInitVal, _type);
    }

    public boolean typeIsSet() {
        return _typeIsSet;
    }	


    public void setMarketingAdvertisement(MarketingAdvertisement marketingAdvertisement) {
        if (!_marketingAdvertisementIsSet) {
            _marketingAdvertisementIsSet = true;
            _marketingAdvertisementInitVal = marketingAdvertisement;
        }
        _marketingAdvertisement = marketingAdvertisement;
    }

    public MarketingAdvertisement getMarketingAdvertisement() {
        return _marketingAdvertisement;
    }

    public MarketingAdvertisement marketingAdvertisementInitVal() {
        return _marketingAdvertisementInitVal;
    }

	public boolean marketingAdvertisementIsDirty() {
        return !valuesAreEqual(_marketingAdvertisementInitVal, _marketingAdvertisement);
    }

    public boolean marketingAdvertisementIsSet() {
        return _marketingAdvertisementIsSet;
    }	


    public void setAction(String action) {
        if (!_actionIsSet) {
            _actionIsSet = true;
            _actionInitVal = action;
        }
        _action = action;
    }

    public String getAction() {
        return _action;
    }

    public String actionInitVal() {
        return _actionInitVal;
    }

	public boolean actionIsDirty() {
        return !valuesAreEqual(_actionInitVal, _action);
    }

    public boolean actionIsSet() {
        return _actionIsSet;
    }	

}
