//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\ART\Baustelle\development\mda\model.xml
//
//    Or you can change the template:
//
//    P:\ART\Baustelle\development\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing campaign)}

import de.smava.webapp.marketing.domain.MarketingPlacement;

import java.util.Date;
import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoMarketingCampaignForm'.
 *
 * @author generator
 */
public class BoMarketingCampaignForm {

    public static final String NAME_EXPR = "boMarketingCampaignForm.name";
    public static final String NAME_PATH = "command.boMarketingCampaignForm.name";
    public static final String CREATION_DATE_EXPR = "boMarketingCampaignForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boMarketingCampaignForm.creationDate";
    public static final String COMMENT_EXPR = "boMarketingCampaignForm.comment";
    public static final String COMMENT_PATH = "command.boMarketingCampaignForm.comment";
    public static final String PLACEMENT_EXPR = "boMarketingCampaignForm.placement";
    public static final String PLACEMENT_PATH = "command.boMarketingCampaignForm.placement";
    public static final String MARKETING_PLACEMENTS_EXPR = "boMarketingCampaignForm.marketingPlacements";
    public static final String MARKETING_PLACEMENTS_PATH = "command.boMarketingCampaignForm.marketingPlacements";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing campaign)}
// !!!!!!!! End of insert code section !!!!!!!!


    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;

    private Date _creationDate;
    private Date _creationDateInitVal;
    private boolean _creationDateIsSet;

    private String _comment;
    private String _commentInitVal;
    private boolean _commentIsSet;

    private String _placement;
    private String _placementInitVal;
    private boolean _placementIsSet;

    private Set<MarketingPlacement> _marketingPlacements;
    private Set<MarketingPlacement> _marketingPlacementsInitVal;
    private boolean _marketingPlacementsIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

    public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }

    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;

            if (creationDate == null) {
                _creationDateInitVal = null;
            } else {
                _creationDateInitVal = (Date) creationDate.clone();
            }
        }

        if (creationDate == null) {
            _creationDate = null;
        } else {
            _creationDate = (Date) creationDate.clone();
        }
    }

    public Date getCreationDate() {
        Date creationDate = null;
        if (_creationDate != null) {
            creationDate = (Date) _creationDate.clone();
        }
        return creationDate;
    }

    public Date creationDateInitVal() {
        Date creationDateInitVal = null;
        if (_creationDateInitVal != null) {
            creationDateInitVal = (Date) _creationDateInitVal.clone();
        }
        return creationDateInitVal;
    }

    public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = comment;
        }
        _comment = comment;
    }

    public String getComment() {
        return _comment;
    }

    public String commentInitVal() {
        return _commentInitVal;
    }

    public boolean commentIsDirty() {
        return !valuesAreEqual(_commentInitVal, _comment);
    }

    public boolean commentIsSet() {
        return _commentIsSet;
    }

    public void setPlacement(String placement) {
        if (!_placementIsSet) {
            _placementIsSet = true;
            _placementInitVal = placement;
        }
        _placement = placement;
    }

    public String getPlacement() {
        return _placement;
    }

    public String placementInitVal() {
        return _placementInitVal;
    }

    public boolean placementIsDirty() {
        return !valuesAreEqual(_placementInitVal, _placement);
    }

    public boolean placementIsSet() {
        return _placementIsSet;
    }

    public void setMarketingPlacements(Set<MarketingPlacement> marketingPlacements) {
        if (!_marketingPlacementsIsSet) {
            _marketingPlacementsIsSet = true;
            _marketingPlacementsInitVal = marketingPlacements;
        }
        _marketingPlacements = marketingPlacements;
    }

    public Set<MarketingPlacement> getMarketingPlacements() {
        return _marketingPlacements;
    }

    public Set<MarketingPlacement> marketingPlacementsInitVal() {
        return _marketingPlacementsInitVal;
    }

    public boolean marketingPlacementsIsDirty() {
        return !valuesAreEqual(_marketingPlacementsInitVal, _marketingPlacements);
    }

    public boolean marketingPlacementsIsSet() {
        return _marketingPlacementsIsSet;
    }

}
