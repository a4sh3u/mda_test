//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing payment)}
import java.io.Serializable;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoMarketingPaymentForm'.
 *
 * @author generator
 */
public class BoMarketingPaymentForm implements Serializable {

    public static final String CREATION_DATE_EXPR = "boMarketingPaymentForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boMarketingPaymentForm.creationDate";
    public static final String AMOUNT_EXPR = "boMarketingPaymentForm.amount";
    public static final String AMOUNT_PATH = "command.boMarketingPaymentForm.amount";
    public static final String FROM_EXPR = "boMarketingPaymentForm.from";
    public static final String FROM_PATH = "command.boMarketingPaymentForm.from";
    public static final String UNTIL_EXPR = "boMarketingPaymentForm.until";
    public static final String UNTIL_PATH = "command.boMarketingPaymentForm.until";
    public static final String MARKETING_PLACEMENT_EXPR = "boMarketingPaymentForm.marketingPlacement";
    public static final String MARKETING_PLACEMENT_PATH = "command.boMarketingPaymentForm.marketingPlacement";
    public static final String MARKETING_PARTNER_EXPR = "boMarketingPaymentForm.marketingPartner";
    public static final String MARKETING_PARTNER_PATH = "command.boMarketingPaymentForm.marketingPartner";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing payment)}
// !!!!!!!! End of insert code section !!!!!!!!


    private Date _creationDate;
    private Date _creationDateInitVal;
    private boolean _creationDateIsSet;

    private Float _amount;
    private Float _amountInitVal;
    private boolean _amountIsSet;

    private String _from;
    private String _fromInitVal;
    private boolean _fromIsSet;

    private String _until;
    private String _untilInitVal;
    private boolean _untilIsSet;

    private String _marketingPlacement;
    private String _marketingPlacementInitVal;
    private boolean _marketingPlacementIsSet;

    private String _marketingPartner;
    private String _marketingPartnerInitVal;
    private boolean _marketingPartnerIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;

            if (creationDate == null) {
                _creationDateInitVal = null;
            } else {
                _creationDateInitVal = (Date) creationDate.clone();
            }
        }

        if (creationDate == null) {
            _creationDate = null;
        } else {
            _creationDate = (Date) creationDate.clone();
        }
    }

    public Date getCreationDate() {
        Date creationDate = null;
        if (_creationDate != null) {
            creationDate = (Date) _creationDate.clone();
        }
        return creationDate;
    }

    public Date creationDateInitVal() {
        Date creationDateInitVal = null;
        if (_creationDateInitVal != null) {
            creationDateInitVal = (Date) _creationDateInitVal.clone();
        }
        return creationDateInitVal;
    }

    public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

    public void setAmount(Float amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = amount;
        }
        _amount = amount;
    }

    public Float getAmount() {
        return _amount;
    }

    public Float amountInitVal() {
        return _amountInitVal;
    }

    public boolean amountIsDirty() {
        return !valuesAreEqual(_amountInitVal, _amount);
    }

    public boolean amountIsSet() {
        return _amountIsSet;
    }

    public void setFrom(String from) {
        if (!_fromIsSet) {
            _fromIsSet = true;
            _fromInitVal = from;
        }
        _from = from;
    }

    public String getFrom() {
        return _from;
    }

    public String fromInitVal() {
        return _fromInitVal;
    }

    public boolean fromIsDirty() {
        return !valuesAreEqual(_fromInitVal, _from);
    }

    public boolean fromIsSet() {
        return _fromIsSet;
    }

    public void setUntil(String until) {
        if (!_untilIsSet) {
            _untilIsSet = true;
            _untilInitVal = until;
        }
        _until = until;
    }

    public String getUntil() {
        return _until;
    }

    public String untilInitVal() {
        return _untilInitVal;
    }

    public boolean untilIsDirty() {
        return !valuesAreEqual(_untilInitVal, _until);
    }

    public boolean untilIsSet() {
        return _untilIsSet;
    }

    public void setMarketingPlacement(String marketingPlacement) {
        if (!_marketingPlacementIsSet) {
            _marketingPlacementIsSet = true;
            _marketingPlacementInitVal = marketingPlacement;
        }
        _marketingPlacement = marketingPlacement;
    }

    public String getMarketingPlacement() {
        return _marketingPlacement;
    }

    public String marketingPlacementInitVal() {
        return _marketingPlacementInitVal;
    }

    public boolean marketingPlacementIsDirty() {
        return !valuesAreEqual(_marketingPlacementInitVal, _marketingPlacement);
    }

    public boolean marketingPlacementIsSet() {
        return _marketingPlacementIsSet;
    }

    public void setMarketingPartner(String marketingPartner) {
        if (!_marketingPartnerIsSet) {
            _marketingPartnerIsSet = true;
            _marketingPartnerInitVal = marketingPartner;
        }
        _marketingPartner = marketingPartner;
    }

    public String getMarketingPartner() {
        return _marketingPartner;
    }

    public String marketingPartnerInitVal() {
        return _marketingPartnerInitVal;
    }

    public boolean marketingPartnerIsDirty() {
        return !valuesAreEqual(_marketingPartnerInitVal, _marketingPartner);
    }

    public boolean marketingPartnerIsSet() {
        return _marketingPartnerIsSet;
    }

}
