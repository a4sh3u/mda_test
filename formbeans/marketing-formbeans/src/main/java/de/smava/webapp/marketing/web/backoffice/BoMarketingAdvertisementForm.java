//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    P:\dev\trunk\mda\model.xml
//
//    Or you can change the template:
//
//    P:\dev\trunk\mda\templates\formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing advertisement)}
import java.io.Serializable;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * Form class 'BoMarketingAdvertisementForm'.
 *
 * @author generator
 */
public class BoMarketingAdvertisementForm implements Serializable {

    public static final String NAME_EXPR = "boMarketingAdvertisementForm.name";
    public static final String NAME_PATH = "command.boMarketingAdvertisementForm.name";
    public static final String CREATION_DATE_EXPR = "boMarketingAdvertisementForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boMarketingAdvertisementForm.creationDate";
    public static final String COMMENT_EXPR = "boMarketingAdvertisementForm.comment";
    public static final String COMMENT_PATH = "command.boMarketingAdvertisementForm.comment";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing advertisement)}
// !!!!!!!! End of insert code section !!!!!!!!


    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;

    private Date _creationDate;
    private Date _creationDateInitVal;
    private boolean _creationDateIsSet;

    private String _comment;
    private String _commentInitVal;
    private boolean _commentIsSet;

    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

    public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }

    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;

            if (creationDate == null) {
                _creationDateInitVal = null;
            } else {
                _creationDateInitVal = (Date) creationDate.clone();
            }
        }

        if (creationDate == null) {
            _creationDate = null;
        } else {
            _creationDate = (Date) creationDate.clone();
        }
    }

    public Date getCreationDate() {
        Date creationDate = null;
        if (_creationDate != null) {
            creationDate = (Date) _creationDate.clone();
        }
        return creationDate;
    }

    public Date creationDateInitVal() {
        Date creationDateInitVal = null;
        if (_creationDateInitVal != null) {
            creationDateInitVal = (Date) _creationDateInitVal.clone();
        }
        return creationDateInitVal;
    }

    public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = comment;
        }
        _comment = comment;
    }

    public String getComment() {
        return _comment;
    }

    public String commentInitVal() {
        return _commentInitVal;
    }

    public boolean commentIsDirty() {
        return !valuesAreEqual(_commentInitVal, _comment);
    }

    public boolean commentIsSet() {
        return _commentIsSet;
    }

}
