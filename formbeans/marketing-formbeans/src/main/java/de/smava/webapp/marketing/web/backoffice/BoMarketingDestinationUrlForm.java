//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing destination url)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoMarketingDestinationUrlForm'.
 *
 * @author generator
 */
public class BoMarketingDestinationUrlForm implements Serializable {

    public static final String URL_EXPR = "boMarketingDestinationUrlForm.url";
    public static final String URL_PATH = "command.boMarketingDestinationUrlForm.url";
    public static final String URL_ID_EXPR = "boMarketingDestinationUrlForm.urlId";
    public static final String URL_ID_PATH = "command.boMarketingDestinationUrlForm.urlId";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing destination url)}
    /**
     * generated serial.
     */
    private static final long serialVersionUID = -7401872225095060887L;
// !!!!!!!! End of insert code section !!!!!!!!

    private String _url;
    private String _urlInitVal;
    private boolean _urlIsSet;
    private Long _urlId;
    private Long _urlIdInitVal;
    private boolean _urlIdIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setUrl(String url) {
        if (!_urlIsSet) {
            _urlIsSet = true;
            _urlInitVal = url;
        }
        _url = url;
    }

    public String getUrl() {
        return _url;
    }

    public String urlInitVal() {
        return _urlInitVal;
    }

	public boolean urlIsDirty() {
        return !valuesAreEqual(_urlInitVal, _url);
    }

    public boolean urlIsSet() {
        return _urlIsSet;
    }	


    public void setUrlId(Long urlId) {
        if (!_urlIdIsSet) {
            _urlIdIsSet = true;
            _urlIdInitVal = urlId;
        }
        _urlId = urlId;
    }

    public Long getUrlId() {
        return _urlId;
    }

    public Long urlIdInitVal() {
        return _urlIdInitVal;
    }

	public boolean urlIdIsDirty() {
        return !valuesAreEqual(_urlIdInitVal, _urlId);
    }

    public boolean urlIdIsSet() {
        return _urlIdIsSet;
    }	

}
