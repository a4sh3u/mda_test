//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing placement)}
import java.io.Serializable;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoMarketingPlacementForm'.
 *
 * @author generator
 */
public class BoMarketingPlacementForm implements Serializable {

    public static final String CREATION_DATE_EXPR = "boMarketingPlacementForm.creationDate";
    public static final String CREATION_DATE_PATH = "command.boMarketingPlacementForm.creationDate";
    public static final String PUBLICATION_EXPR = "boMarketingPlacementForm.publication";
    public static final String PUBLICATION_PATH = "command.boMarketingPlacementForm.publication";
    public static final String QUANTITY_EXPR = "boMarketingPlacementForm.quantity";
    public static final String QUANTITY_PATH = "command.boMarketingPlacementForm.quantity";
    public static final String POSITION_EXPR = "boMarketingPlacementForm.position";
    public static final String POSITION_PATH = "command.boMarketingPlacementForm.position";
    public static final String ADVERTISEMENT_EXPR = "boMarketingPlacementForm.advertisement";
    public static final String ADVERTISEMENT_PATH = "command.boMarketingPlacementForm.advertisement";
    public static final String CAMPAIGN_EXPR = "boMarketingPlacementForm.campaign";
    public static final String CAMPAIGN_PATH = "command.boMarketingPlacementForm.campaign";
    public static final String AD_FORMAT_EXPR = "boMarketingPlacementForm.adFormat";
    public static final String AD_FORMAT_PATH = "command.boMarketingPlacementForm.adFormat";
    public static final String DEAL_AMOUNT_EXPR = "boMarketingPlacementForm.dealAmount";
    public static final String DEAL_AMOUNT_PATH = "command.boMarketingPlacementForm.dealAmount";
    public static final String DEAL_TYPE_EXPR = "boMarketingPlacementForm.dealType";
    public static final String DEAL_TYPE_PATH = "command.boMarketingPlacementForm.dealType";
    public static final String ALTERNATE_CODE_EXPR = "boMarketingPlacementForm.alternateCode";
    public static final String ALTERNATE_CODE_PATH = "command.boMarketingPlacementForm.alternateCode";
    public static final String MARKETING_CONTACT_EXPR = "boMarketingPlacementForm.marketingContact";
    public static final String MARKETING_CONTACT_PATH = "command.boMarketingPlacementForm.marketingContact";
    public static final String NO_BROKERAGE_EXPR = "boMarketingPlacementForm.noBrokerage";
    public static final String NO_BROKERAGE_PATH = "command.boMarketingPlacementForm.noBrokerage";
    public static final String USER_DATA_SHARING_EXPR = "boMarketingPlacementForm.userDataSharing";
    public static final String USER_DATA_SHARING_PATH = "command.boMarketingPlacementForm.userDataSharing";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing placement)}
// !!!!!!!! End of insert code section !!!!!!!!

    private Date _creationDate;
    private Date _creationDateInitVal;
    private boolean _creationDateIsSet;
    private String _publication;
    private String _publicationInitVal;
    private boolean _publicationIsSet;
    private int _quantity;
    private int _quantityInitVal;
    private boolean _quantityIsSet;
    private String _position;
    private String _positionInitVal;
    private boolean _positionIsSet;
    private String _advertisement;
    private String _advertisementInitVal;
    private boolean _advertisementIsSet;
    private String _campaign;
    private String _campaignInitVal;
    private boolean _campaignIsSet;
    private String _adFormat;
    private String _adFormatInitVal;
    private boolean _adFormatIsSet;
    private String _dealAmount;
    private String _dealAmountInitVal;
    private boolean _dealAmountIsSet;
    private String _dealType;
    private String _dealTypeInitVal;
    private boolean _dealTypeIsSet;
    private String _alternateCode;
    private String _alternateCodeInitVal;
    private boolean _alternateCodeIsSet;
    private String _marketingContact;
    private String _marketingContactInitVal;
    private boolean _marketingContactIsSet;
    private Boolean _noBrokerage;
    private Boolean _noBrokerageInitVal;
    private boolean _noBrokerageIsSet;
    private Boolean _userDataSharing;
    private Boolean _userDataSharingInitVal;
    private boolean _userDataSharingIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;

            if (creationDate == null) {
                _creationDateInitVal = null;
            } else {
                _creationDateInitVal = (Date) creationDate.clone();
            }
        }

        if (creationDate == null) {
            _creationDate = null;
        } else {
            _creationDate = (Date) creationDate.clone();
        }
    }

    public Date getCreationDate() {
        Date creationDate = null;
        if (_creationDate != null) {
            creationDate = (Date) _creationDate.clone();
        }
        return creationDate;
    }

    public Date creationDateInitVal() {
        Date creationDateInitVal = null;
        if (_creationDateInitVal != null) {
            creationDateInitVal = (Date) _creationDateInitVal.clone();
        }
        return creationDateInitVal;
    }

	public boolean creationDateIsDirty() {
        return !valuesAreEqual(_creationDateInitVal, _creationDate);
    }

    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }	


    public void setPublication(String publication) {
        if (!_publicationIsSet) {
            _publicationIsSet = true;
            _publicationInitVal = publication;
        }
        _publication = publication;
    }

    public String getPublication() {
        return _publication;
    }

    public String publicationInitVal() {
        return _publicationInitVal;
    }

	public boolean publicationIsDirty() {
        return !valuesAreEqual(_publicationInitVal, _publication);
    }

    public boolean publicationIsSet() {
        return _publicationIsSet;
    }	


    public void setQuantity(int quantity) {
        if (!_quantityIsSet) {
            _quantityIsSet = true;
            _quantityInitVal = quantity;
        }
        _quantity = quantity;
    }

    public int getQuantity() {
        return _quantity;
    }

    public int quantityInitVal() {
        return _quantityInitVal;
    }

	public boolean quantityIsDirty() {
        return !valuesAreEqual(_quantityInitVal, _quantity);
    }

    public boolean quantityIsSet() {
        return _quantityIsSet;
    }	


    public void setPosition(String position) {
        if (!_positionIsSet) {
            _positionIsSet = true;
            _positionInitVal = position;
        }
        _position = position;
    }

    public String getPosition() {
        return _position;
    }

    public String positionInitVal() {
        return _positionInitVal;
    }

	public boolean positionIsDirty() {
        return !valuesAreEqual(_positionInitVal, _position);
    }

    public boolean positionIsSet() {
        return _positionIsSet;
    }	


    public void setAdvertisement(String advertisement) {
        if (!_advertisementIsSet) {
            _advertisementIsSet = true;
            _advertisementInitVal = advertisement;
        }
        _advertisement = advertisement;
    }

    public String getAdvertisement() {
        return _advertisement;
    }

    public String advertisementInitVal() {
        return _advertisementInitVal;
    }

	public boolean advertisementIsDirty() {
        return !valuesAreEqual(_advertisementInitVal, _advertisement);
    }

    public boolean advertisementIsSet() {
        return _advertisementIsSet;
    }	


    public void setCampaign(String campaign) {
        if (!_campaignIsSet) {
            _campaignIsSet = true;
            _campaignInitVal = campaign;
        }
        _campaign = campaign;
    }

    public String getCampaign() {
        return _campaign;
    }

    public String campaignInitVal() {
        return _campaignInitVal;
    }

	public boolean campaignIsDirty() {
        return !valuesAreEqual(_campaignInitVal, _campaign);
    }

    public boolean campaignIsSet() {
        return _campaignIsSet;
    }	


    public void setAdFormat(String adFormat) {
        if (!_adFormatIsSet) {
            _adFormatIsSet = true;
            _adFormatInitVal = adFormat;
        }
        _adFormat = adFormat;
    }

    public String getAdFormat() {
        return _adFormat;
    }

    public String adFormatInitVal() {
        return _adFormatInitVal;
    }

	public boolean adFormatIsDirty() {
        return !valuesAreEqual(_adFormatInitVal, _adFormat);
    }

    public boolean adFormatIsSet() {
        return _adFormatIsSet;
    }	


    public void setDealAmount(String dealAmount) {
        if (!_dealAmountIsSet) {
            _dealAmountIsSet = true;
            _dealAmountInitVal = dealAmount;
        }
        _dealAmount = dealAmount;
    }

    public String getDealAmount() {
        return _dealAmount;
    }

    public String dealAmountInitVal() {
        return _dealAmountInitVal;
    }

	public boolean dealAmountIsDirty() {
        return !valuesAreEqual(_dealAmountInitVal, _dealAmount);
    }

    public boolean dealAmountIsSet() {
        return _dealAmountIsSet;
    }	


    public void setDealType(String dealType) {
        if (!_dealTypeIsSet) {
            _dealTypeIsSet = true;
            _dealTypeInitVal = dealType;
        }
        _dealType = dealType;
    }

    public String getDealType() {
        return _dealType;
    }

    public String dealTypeInitVal() {
        return _dealTypeInitVal;
    }

	public boolean dealTypeIsDirty() {
        return !valuesAreEqual(_dealTypeInitVal, _dealType);
    }

    public boolean dealTypeIsSet() {
        return _dealTypeIsSet;
    }	


    public void setAlternateCode(String alternateCode) {
        if (!_alternateCodeIsSet) {
            _alternateCodeIsSet = true;
            _alternateCodeInitVal = alternateCode;
        }
        _alternateCode = alternateCode;
    }

    public String getAlternateCode() {
        return _alternateCode;
    }

    public String alternateCodeInitVal() {
        return _alternateCodeInitVal;
    }

	public boolean alternateCodeIsDirty() {
        return !valuesAreEqual(_alternateCodeInitVal, _alternateCode);
    }

    public boolean alternateCodeIsSet() {
        return _alternateCodeIsSet;
    }	


    public void setMarketingContact(String marketingContact) {
        if (!_marketingContactIsSet) {
            _marketingContactIsSet = true;
            _marketingContactInitVal = marketingContact;
        }
        _marketingContact = marketingContact;
    }

    public String getMarketingContact() {
        return _marketingContact;
    }

    public String marketingContactInitVal() {
        return _marketingContactInitVal;
    }

	public boolean marketingContactIsDirty() {
        return !valuesAreEqual(_marketingContactInitVal, _marketingContact);
    }

    public boolean marketingContactIsSet() {
        return _marketingContactIsSet;
    }	


    public void setNoBrokerage(Boolean noBrokerage) {
        if (!_noBrokerageIsSet) {
            _noBrokerageIsSet = true;
            _noBrokerageInitVal = noBrokerage;
        }
        _noBrokerage = noBrokerage;
    }

    public Boolean getNoBrokerage() {
        return _noBrokerage;
    }

    public Boolean noBrokerageInitVal() {
        return _noBrokerageInitVal;
    }

	public boolean noBrokerageIsDirty() {
        return !valuesAreEqual(_noBrokerageInitVal, _noBrokerage);
    }

    public boolean noBrokerageIsSet() {
        return _noBrokerageIsSet;
    }	


    public void setUserDataSharing(Boolean userDataSharing) {
        if (!_userDataSharingIsSet) {
            _userDataSharingIsSet = true;
            _userDataSharingInitVal = userDataSharing;
        }
        _userDataSharing = userDataSharing;
    }

    public Boolean getUserDataSharing() {
        return _userDataSharing;
    }

    public Boolean userDataSharingInitVal() {
        return _userDataSharingInitVal;
    }

	public boolean userDataSharingIsDirty() {
        return !valuesAreEqual(_userDataSharingInitVal, _userDataSharing);
    }

    public boolean userDataSharingIsSet() {
        return _userDataSharingIsSet;
    }	

}
