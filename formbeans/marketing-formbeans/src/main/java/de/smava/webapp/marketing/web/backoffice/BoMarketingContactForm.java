//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    /home/ihle/workspace/trunk/mda/model.xml
//
//    Or you can change the template:
//
//    /home/ihle/workspace/trunk/mda/templates/formbean.tpl
//
//
//
//
package de.smava.webapp.marketing.web.backoffice;
    
// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|formbean(bo marketing contact)}
import java.io.Serializable;

// !!!!!!!! End of insert code section !!!!!!!!

/**
 * Form class 'BoMarketingContactForm'.
 *
 * @author generator
 */
public class BoMarketingContactForm implements Serializable {

    public static final String SALUTATION_EXPR = "boMarketingContactForm.salutation";
    public static final String SALUTATION_PATH = "command.boMarketingContactForm.salutation";
    public static final String NAME_EXPR = "boMarketingContactForm.name";
    public static final String NAME_PATH = "command.boMarketingContactForm.name";
    public static final String LAST_NAME_EXPR = "boMarketingContactForm.lastName";
    public static final String LAST_NAME_PATH = "command.boMarketingContactForm.lastName";
    public static final String EMAIL_EXPR = "boMarketingContactForm.email";
    public static final String EMAIL_PATH = "command.boMarketingContactForm.email";
    public static final String PREFIX_PHONE1_EXPR = "boMarketingContactForm.prefixPhone1";
    public static final String PREFIX_PHONE1_PATH = "command.boMarketingContactForm.prefixPhone1";
    public static final String PHONE1_EXPR = "boMarketingContactForm.phone1";
    public static final String PHONE1_PATH = "command.boMarketingContactForm.phone1";
    public static final String PREFIX_PHONE2_EXPR = "boMarketingContactForm.prefixPhone2";
    public static final String PREFIX_PHONE2_PATH = "command.boMarketingContactForm.prefixPhone2";
    public static final String PHONE2_EXPR = "boMarketingContactForm.phone2";
    public static final String PHONE2_PATH = "command.boMarketingContactForm.phone2";
    public static final String COMMENT_EXPR = "boMarketingContactForm.comment";
    public static final String COMMENT_PATH = "command.boMarketingContactForm.comment";
    public static final String POSITION_EXPR = "boMarketingContactForm.position";
    public static final String POSITION_PATH = "command.boMarketingContactForm.position";
    public static final String ROLE_EXPR = "boMarketingContactForm.role";
    public static final String ROLE_PATH = "command.boMarketingContactForm.role";
    public static final String MARKETING_PLACEMENT_EXPR = "boMarketingContactForm.marketingPlacement";
    public static final String MARKETING_PLACEMENT_PATH = "command.boMarketingContactForm.marketingPlacement";
    public static final String STREET_EXPR = "boMarketingContactForm.street";
    public static final String STREET_PATH = "command.boMarketingContactForm.street";
    public static final String STREET_NUMBER_EXPR = "boMarketingContactForm.streetNumber";
    public static final String STREET_NUMBER_PATH = "command.boMarketingContactForm.streetNumber";
    public static final String ZIP_CODE_EXPR = "boMarketingContactForm.zipCode";
    public static final String ZIP_CODE_PATH = "command.boMarketingContactForm.zipCode";
    public static final String CITY_EXPR = "boMarketingContactForm.city";
    public static final String CITY_PATH = "command.boMarketingContactForm.city";
    public static final String BANK_ACCOUNT_EXPR = "boMarketingContactForm.bankAccount";
    public static final String BANK_ACCOUNT_PATH = "command.boMarketingContactForm.bankAccount";
    public static final String BANK_CODE_EXPR = "boMarketingContactForm.bankCode";
    public static final String BANK_CODE_PATH = "command.boMarketingContactForm.bankCode";
    public static final String BANK_EXPR = "boMarketingContactForm.bank";
    public static final String BANK_PATH = "command.boMarketingContactForm.bank";
    public static final String IS_AGENT_ADMIN_EXPR = "boMarketingContactForm.isAgentAdmin";
    public static final String IS_AGENT_ADMIN_PATH = "command.boMarketingContactForm.isAgentAdmin";
    public static final String AUTO_GENERATE_PLACEMENTS_EXPR = "boMarketingContactForm.autoGeneratePlacements";
    public static final String AUTO_GENERATE_PLACEMENTS_PATH = "command.boMarketingContactForm.autoGeneratePlacements";

// !!!!!!!! You can insert code here: !!!!!!!!																														{|additional fields and methods|formbean(bo marketing contact)}
// !!!!!!!! End of insert code section !!!!!!!!

    private String _salutation;
    private String _salutationInitVal;
    private boolean _salutationIsSet;
    private String _name;
    private String _nameInitVal;
    private boolean _nameIsSet;
    private String _lastName;
    private String _lastNameInitVal;
    private boolean _lastNameIsSet;
    private String _email;
    private String _emailInitVal;
    private boolean _emailIsSet;
    private String _prefixPhone1;
    private String _prefixPhone1InitVal;
    private boolean _prefixPhone1IsSet;
    private String _phone1;
    private String _phone1InitVal;
    private boolean _phone1IsSet;
    private String _prefixPhone2;
    private String _prefixPhone2InitVal;
    private boolean _prefixPhone2IsSet;
    private String _phone2;
    private String _phone2InitVal;
    private boolean _phone2IsSet;
    private String _comment;
    private String _commentInitVal;
    private boolean _commentIsSet;
    private String _position;
    private String _positionInitVal;
    private boolean _positionIsSet;
    private String _role;
    private String _roleInitVal;
    private boolean _roleIsSet;
    private String _marketingPlacement;
    private String _marketingPlacementInitVal;
    private boolean _marketingPlacementIsSet;
    private String _street;
    private String _streetInitVal;
    private boolean _streetIsSet;
    private String _streetNumber;
    private String _streetNumberInitVal;
    private boolean _streetNumberIsSet;
    private String _zipCode;
    private String _zipCodeInitVal;
    private boolean _zipCodeIsSet;
    private String _city;
    private String _cityInitVal;
    private boolean _cityIsSet;
    private String _bankAccount;
    private String _bankAccountInitVal;
    private boolean _bankAccountIsSet;
    private String _bankCode;
    private String _bankCodeInitVal;
    private boolean _bankCodeIsSet;
    private String _bank;
    private String _bankInitVal;
    private boolean _bankIsSet;
    private Boolean _isAgentAdmin;
    private Boolean _isAgentAdminInitVal;
    private boolean _isAgentAdminIsSet;
    private Boolean _autoGeneratePlacements;
    private Boolean _autoGeneratePlacementsInitVal;
    private boolean _autoGeneratePlacementsIsSet;


    private static boolean valuesAreEqual(Object val1, Object val2) {
        boolean result;
        if (val1 == val2) {
            result = true;
        } else if (val1 == null || val2 == null) {
            result = false;
        } else {
            result = val1.equals(val2);
        }
        return result;
    }



    public void setSalutation(String salutation) {
        if (!_salutationIsSet) {
            _salutationIsSet = true;
            _salutationInitVal = salutation;
        }
        _salutation = salutation;
    }

    public String getSalutation() {
        return _salutation;
    }

    public String salutationInitVal() {
        return _salutationInitVal;
    }

	public boolean salutationIsDirty() {
        return !valuesAreEqual(_salutationInitVal, _salutation);
    }

    public boolean salutationIsSet() {
        return _salutationIsSet;
    }	


    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = name;
        }
        _name = name;
    }

    public String getName() {
        return _name;
    }

    public String nameInitVal() {
        return _nameInitVal;
    }

	public boolean nameIsDirty() {
        return !valuesAreEqual(_nameInitVal, _name);
    }

    public boolean nameIsSet() {
        return _nameIsSet;
    }	


    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = lastName;
        }
        _lastName = lastName;
    }

    public String getLastName() {
        return _lastName;
    }

    public String lastNameInitVal() {
        return _lastNameInitVal;
    }

	public boolean lastNameIsDirty() {
        return !valuesAreEqual(_lastNameInitVal, _lastName);
    }

    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }	


    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = email;
        }
        _email = email;
    }

    public String getEmail() {
        return _email;
    }

    public String emailInitVal() {
        return _emailInitVal;
    }

	public boolean emailIsDirty() {
        return !valuesAreEqual(_emailInitVal, _email);
    }

    public boolean emailIsSet() {
        return _emailIsSet;
    }	


    public void setPrefixPhone1(String prefixPhone1) {
        if (!_prefixPhone1IsSet) {
            _prefixPhone1IsSet = true;
            _prefixPhone1InitVal = prefixPhone1;
        }
        _prefixPhone1 = prefixPhone1;
    }

    public String getPrefixPhone1() {
        return _prefixPhone1;
    }

    public String prefixPhone1InitVal() {
        return _prefixPhone1InitVal;
    }

	public boolean prefixPhone1IsDirty() {
        return !valuesAreEqual(_prefixPhone1InitVal, _prefixPhone1);
    }

    public boolean prefixPhone1IsSet() {
        return _prefixPhone1IsSet;
    }	


    public void setPhone1(String phone1) {
        if (!_phone1IsSet) {
            _phone1IsSet = true;
            _phone1InitVal = phone1;
        }
        _phone1 = phone1;
    }

    public String getPhone1() {
        return _phone1;
    }

    public String phone1InitVal() {
        return _phone1InitVal;
    }

	public boolean phone1IsDirty() {
        return !valuesAreEqual(_phone1InitVal, _phone1);
    }

    public boolean phone1IsSet() {
        return _phone1IsSet;
    }	


    public void setPrefixPhone2(String prefixPhone2) {
        if (!_prefixPhone2IsSet) {
            _prefixPhone2IsSet = true;
            _prefixPhone2InitVal = prefixPhone2;
        }
        _prefixPhone2 = prefixPhone2;
    }

    public String getPrefixPhone2() {
        return _prefixPhone2;
    }

    public String prefixPhone2InitVal() {
        return _prefixPhone2InitVal;
    }

	public boolean prefixPhone2IsDirty() {
        return !valuesAreEqual(_prefixPhone2InitVal, _prefixPhone2);
    }

    public boolean prefixPhone2IsSet() {
        return _prefixPhone2IsSet;
    }	


    public void setPhone2(String phone2) {
        if (!_phone2IsSet) {
            _phone2IsSet = true;
            _phone2InitVal = phone2;
        }
        _phone2 = phone2;
    }

    public String getPhone2() {
        return _phone2;
    }

    public String phone2InitVal() {
        return _phone2InitVal;
    }

	public boolean phone2IsDirty() {
        return !valuesAreEqual(_phone2InitVal, _phone2);
    }

    public boolean phone2IsSet() {
        return _phone2IsSet;
    }	


    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = comment;
        }
        _comment = comment;
    }

    public String getComment() {
        return _comment;
    }

    public String commentInitVal() {
        return _commentInitVal;
    }

	public boolean commentIsDirty() {
        return !valuesAreEqual(_commentInitVal, _comment);
    }

    public boolean commentIsSet() {
        return _commentIsSet;
    }	


    public void setPosition(String position) {
        if (!_positionIsSet) {
            _positionIsSet = true;
            _positionInitVal = position;
        }
        _position = position;
    }

    public String getPosition() {
        return _position;
    }

    public String positionInitVal() {
        return _positionInitVal;
    }

	public boolean positionIsDirty() {
        return !valuesAreEqual(_positionInitVal, _position);
    }

    public boolean positionIsSet() {
        return _positionIsSet;
    }	


    public void setRole(String role) {
        if (!_roleIsSet) {
            _roleIsSet = true;
            _roleInitVal = role;
        }
        _role = role;
    }

    public String getRole() {
        return _role;
    }

    public String roleInitVal() {
        return _roleInitVal;
    }

	public boolean roleIsDirty() {
        return !valuesAreEqual(_roleInitVal, _role);
    }

    public boolean roleIsSet() {
        return _roleIsSet;
    }	


    public void setMarketingPlacement(String marketingPlacement) {
        if (!_marketingPlacementIsSet) {
            _marketingPlacementIsSet = true;
            _marketingPlacementInitVal = marketingPlacement;
        }
        _marketingPlacement = marketingPlacement;
    }

    public String getMarketingPlacement() {
        return _marketingPlacement;
    }

    public String marketingPlacementInitVal() {
        return _marketingPlacementInitVal;
    }

	public boolean marketingPlacementIsDirty() {
        return !valuesAreEqual(_marketingPlacementInitVal, _marketingPlacement);
    }

    public boolean marketingPlacementIsSet() {
        return _marketingPlacementIsSet;
    }	


    public void setStreet(String street) {
        if (!_streetIsSet) {
            _streetIsSet = true;
            _streetInitVal = street;
        }
        _street = street;
    }

    public String getStreet() {
        return _street;
    }

    public String streetInitVal() {
        return _streetInitVal;
    }

	public boolean streetIsDirty() {
        return !valuesAreEqual(_streetInitVal, _street);
    }

    public boolean streetIsSet() {
        return _streetIsSet;
    }	


    public void setStreetNumber(String streetNumber) {
        if (!_streetNumberIsSet) {
            _streetNumberIsSet = true;
            _streetNumberInitVal = streetNumber;
        }
        _streetNumber = streetNumber;
    }

    public String getStreetNumber() {
        return _streetNumber;
    }

    public String streetNumberInitVal() {
        return _streetNumberInitVal;
    }

	public boolean streetNumberIsDirty() {
        return !valuesAreEqual(_streetNumberInitVal, _streetNumber);
    }

    public boolean streetNumberIsSet() {
        return _streetNumberIsSet;
    }	


    public void setZipCode(String zipCode) {
        if (!_zipCodeIsSet) {
            _zipCodeIsSet = true;
            _zipCodeInitVal = zipCode;
        }
        _zipCode = zipCode;
    }

    public String getZipCode() {
        return _zipCode;
    }

    public String zipCodeInitVal() {
        return _zipCodeInitVal;
    }

	public boolean zipCodeIsDirty() {
        return !valuesAreEqual(_zipCodeInitVal, _zipCode);
    }

    public boolean zipCodeIsSet() {
        return _zipCodeIsSet;
    }	


    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = city;
        }
        _city = city;
    }

    public String getCity() {
        return _city;
    }

    public String cityInitVal() {
        return _cityInitVal;
    }

	public boolean cityIsDirty() {
        return !valuesAreEqual(_cityInitVal, _city);
    }

    public boolean cityIsSet() {
        return _cityIsSet;
    }	


    public void setBankAccount(String bankAccount) {
        if (!_bankAccountIsSet) {
            _bankAccountIsSet = true;
            _bankAccountInitVal = bankAccount;
        }
        _bankAccount = bankAccount;
    }

    public String getBankAccount() {
        return _bankAccount;
    }

    public String bankAccountInitVal() {
        return _bankAccountInitVal;
    }

	public boolean bankAccountIsDirty() {
        return !valuesAreEqual(_bankAccountInitVal, _bankAccount);
    }

    public boolean bankAccountIsSet() {
        return _bankAccountIsSet;
    }	


    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = bankCode;
        }
        _bankCode = bankCode;
    }

    public String getBankCode() {
        return _bankCode;
    }

    public String bankCodeInitVal() {
        return _bankCodeInitVal;
    }

	public boolean bankCodeIsDirty() {
        return !valuesAreEqual(_bankCodeInitVal, _bankCode);
    }

    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }	


    public void setBank(String bank) {
        if (!_bankIsSet) {
            _bankIsSet = true;
            _bankInitVal = bank;
        }
        _bank = bank;
    }

    public String getBank() {
        return _bank;
    }

    public String bankInitVal() {
        return _bankInitVal;
    }

	public boolean bankIsDirty() {
        return !valuesAreEqual(_bankInitVal, _bank);
    }

    public boolean bankIsSet() {
        return _bankIsSet;
    }	


    public void setIsAgentAdmin(Boolean isAgentAdmin) {
        if (!_isAgentAdminIsSet) {
            _isAgentAdminIsSet = true;
            _isAgentAdminInitVal = isAgentAdmin;
        }
        _isAgentAdmin = isAgentAdmin;
    }

    public Boolean getIsAgentAdmin() {
        return _isAgentAdmin;
    }

    public Boolean isAgentAdminInitVal() {
        return _isAgentAdminInitVal;
    }

	public boolean isAgentAdminIsDirty() {
        return !valuesAreEqual(_isAgentAdminInitVal, _isAgentAdmin);
    }

    public boolean isAgentAdminIsSet() {
        return _isAgentAdminIsSet;
    }	


    public void setAutoGeneratePlacements(Boolean autoGeneratePlacements) {
        if (!_autoGeneratePlacementsIsSet) {
            _autoGeneratePlacementsIsSet = true;
            _autoGeneratePlacementsInitVal = autoGeneratePlacements;
        }
        _autoGeneratePlacements = autoGeneratePlacements;
    }

    public Boolean getAutoGeneratePlacements() {
        return _autoGeneratePlacements;
    }

    public Boolean autoGeneratePlacementsInitVal() {
        return _autoGeneratePlacementsInitVal;
    }

	public boolean autoGeneratePlacementsIsDirty() {
        return !valuesAreEqual(_autoGeneratePlacementsInitVal, _autoGeneratePlacements);
    }

    public boolean autoGeneratePlacementsIsSet() {
        return _autoGeneratePlacementsIsSet;
    }	

}
