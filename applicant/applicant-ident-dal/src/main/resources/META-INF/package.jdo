<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE jdo PUBLIC
        "-//Sun Microsystems, Inc.//DTD Java Data Objects Metadata 2.0//EN"
        "http://java.sun.com/dtd/jdo_2_0.dtd">
<jdo schema="brokerage">
    <package name="de.smava.webapp.applicant.ident.domain" schema="brokerage">
        <class name="IdentificationBrokerageBankResult" identity-type="application" table="identification_brokerage_bank_result" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_account"><column name="account_id" allows-null="false" /></field>
            <field name="_brokerageBank"><column name="brokerage_bank_id" allows-null="false" /></field>

            <field name="_identificationBrokerageBankResultApplicants" mapped-by="_identificationBrokerageBankResult">
                <collection element-type="IdentificationBrokerageBankResultApplicant" />
            </field>

            <query name="fetchIdentificationBrokerageBankResult" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResult
                    where
                         bank == this._brokerageBank && this._account._id == accountId
                     parameters
                         java.lang.Long accountId,de.smava.webapp.applicant.brokerage.domain.BrokerageBank bank
                ]]>
            </query>
        </class>

        <class name="IdentificationBrokerageBankResultApplicant" identity-type="application" table="identification_brokerage_bank_result_applicant" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_identificationBrokerageBankResult"><column name="identification_brokerage_bank_result_id" /></field>
            <field name="_type"><column name="type" /></field>
            <field name="_state"><column name="state" /></field>
            <field name="_responseData"><column name="response_data" jdbc-type="LONGVARCHAR" /></field>
            <field name="_creationDate"><column name="creation_date" allows-null="false" /></field>

            <field name="_applicant"><column name="applicant_id" allows-null="false" /></field>
        </class>

        <class name="VideoIdentification" identity-type="application" table="video_identification" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_brokerageApplication"><column name="brokerage_application_id" allows-null="false" /></field>
            <field name="_applicant"><column name="applicant_id" allows-null="false" /></field>
            <field name="_status"><column name="status" /></field>
            <field name="_creationDate"><column name="creation_date" allows-null="false" /></field>
            <field name="_updateDate"><column name="update_date" /></field>
            <query name="findByBrokerageApplicationAndApplicant" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.applicant.ident.domain.VideoIdentification
                    where
                        this._brokerageApplication._id == brokerageApplicationId &&
                        this._applicant._id == applicantId
                    parameters
                        java.lang.Long brokerageApplicationId, java.lang.Long applicantId
                ]]>
            </query>


            <query name="findByBrokerageApplicationId" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.applicant.ident.domain.VideoIdentification
                    where
                        this._brokerageApplication._id == brokerageApplicationId
                    parameters
                        java.lang.Long brokerageApplicationId
                ]]>
            </query>

            <query name="getVIByStatusAndUpdateDateOlderThanAndBankIn" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.applicant.ident.domain.VideoIdentification
                    where
                        this._status == status &&
                        (this._updateDate == null || this._updateDate < updateDate) &&
                        bankNames.contains(this._brokerageApplication._brokerageBank._name)
                    parameters
                        de.smava.webapp.applicant.ident.type.VideoIdentificationStatus status, java.util.Date updateDate, java.util.Collection bankNames
                ]]>
            </query>
        </class>


        <class name="Esign" identity-type="application" table="esign" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_brokerageApplication"><column name="brokerage_application_id" allows-null="false" /></field>
            <field name="_applicant"><column name="applicant_id" allows-null="false" /></field>
            <field name="_status"><column name="status" /></field>
            <field name="_creationDate"><column name="creation_date" allows-null="false" /></field>
            <field name="_updateDate"><column name="update_date" /></field>
            <field name="_esignUrl"><column name="esign_url" /></field>
            <field name="_contractDocAvailable"><column name="contract_doc_available" /></field>
            <query name="findByBrokerageApplicationIdAndApplicantId" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.applicant.ident.domain.Esign
                    where
                        this._brokerageApplication._id == brokerageApplicationId &&
                        this._applicant._id == applicantId
                    parameters
                        java.lang.Long brokerageApplicationId, java.lang.Long applicantId
                ]]>
            </query>

            <query name="getEsignByStatusAndUpdateDateOlderThanAndBankIn" language="javax.jdo.query.JDOQL">
                <![CDATA[
                    select
                    from
                        de.smava.webapp.applicant.ident.domain.Esign
                    where
                        this._status == status &&
                        (this._updateDate == null || this._updateDate < updateDate) &&
                        bankNames.contains(this._brokerageApplication._brokerageBank._name)
                    parameters
                        de.smava.webapp.applicant.ident.type.EsignStatus status, java.util.Date updateDate, java.util.Collection bankNames
                ]]>
            </query>
        </class>


        <class name="HttpLog" identity-type="application" table="http_log" detachable="false">
            <inheritance strategy="new-table" />
            <field name="_type"><column name="type" /></field>
            <field name="_url"><column name="url" /></field>
            <field name="_method"><column name="method" /></field>
            <field name="_body"><column name="body" jdbc-type="LONGVARCHAR" /></field>
            <field name="_params"><column name="params" jdbc-type="LONGVARCHAR" /></field>
            <field name="_headers"><column name="headers" jdbc-type="LONGVARCHAR" /></field>
            <field name="_response"><column name="response" jdbc-type="LONGVARCHAR" /></field>
            <field name="_creationDate"><column name="creation_date" /></field>

        </class>
    </package>
</jdo>