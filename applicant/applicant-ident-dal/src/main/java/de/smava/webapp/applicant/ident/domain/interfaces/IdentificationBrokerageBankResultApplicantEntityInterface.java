package de.smava.webapp.applicant.ident.domain.interfaces;



import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResult;
import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResultApplicant;

import java.util.Date;


/**
 * The domain object that represents 'IdentificationBrokerageBankResultApplicants'.
 *
 * @author generator
 */
public interface IdentificationBrokerageBankResultApplicantEntityInterface {

    /**
     * Setter for the property 'identificationBrokerageBankResult'.
     *
     * 
     *
     */
    void setIdentificationBrokerageBankResult(IdentificationBrokerageBankResult identificationBrokerageBankResult);

    /**
     * Returns the property 'identificationBrokerageBankResult'.
     *
     * 
     *
     */
    IdentificationBrokerageBankResult getIdentificationBrokerageBankResult();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType getType();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState getState();
    /**
     * Setter for the property 'responseData'.
     *
     * 
     *
     */
    void setResponseData(String responseData);

    /**
     * Returns the property 'responseData'.
     *
     * 
     *
     */
    String getResponseData();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant);

    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getApplicant();
    /**
     * Helper method to get reference of this object as model type.
     */
    IdentificationBrokerageBankResultApplicant asIdentificationBrokerageBankResultApplicant();
}
