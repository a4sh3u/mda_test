package de.smava.webapp.applicant.ident.domain.history;



import de.smava.webapp.applicant.ident.domain.abstracts.AbstractIdentificationBrokerageBankResultApplicant;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'IdentificationBrokerageBankResultApplicants'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IdentificationBrokerageBankResultApplicantHistory extends AbstractIdentificationBrokerageBankResultApplicant {

    protected transient de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _responseDataInitVal;
    protected transient boolean _responseDataIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;



    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType typeInitVal() {
        de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState stateInitVal() {
        de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'responseData'.
     */
    public String responseDataInitVal() {
        String result;
        if (_responseDataIsSet) {
            result = _responseDataInitVal;
        } else {
            result = getResponseData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'responseData'.
     */
    public boolean responseDataIsDirty() {
        return !valuesAreEqual(responseDataInitVal(), getResponseData());
    }

    /**
     * Returns true if the setter method was called for the property 'responseData'.
     */
    public boolean responseDataIsSet() {
        return _responseDataIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creationDate'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creationDate'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creationDate'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
}
