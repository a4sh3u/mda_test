//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result applicant)}

import de.smava.webapp.applicant.ident.domain.history.IdentificationBrokerageBankResultApplicantHistory;

import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationBrokerageBankResultApplicants'.
 *
 * 
 *
 * @author generator
 */
public class IdentificationBrokerageBankResultApplicant extends IdentificationBrokerageBankResultApplicantHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(identification brokerage bank result applicant)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected IdentificationBrokerageBankResult _identificationBrokerageBankResult;
        protected de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType _type;
        protected de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState _state;
        protected String _responseData;
        protected Date _creationDate;
        protected de.smava.webapp.applicant.domain.Applicant _applicant;
        
                                    
    /**
     * Setter for the property 'identificationBrokerageBankResult'.
     *
     * 
     *
     */
    public void setIdentificationBrokerageBankResult(IdentificationBrokerageBankResult identificationBrokerageBankResult) {
        _identificationBrokerageBankResult = identificationBrokerageBankResult;
    }
            
    /**
     * Returns the property 'identificationBrokerageBankResult'.
     *
     * 
     *
     */
    public IdentificationBrokerageBankResult getIdentificationBrokerageBankResult() {
        return _identificationBrokerageBankResult;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultType getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.ident.type.IdentificationBrokerageBankResultState getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'responseData'.
     *
     * 
     *
     */
    public void setResponseData(String responseData) {
        if (!_responseDataIsSet) {
            _responseDataIsSet = true;
            _responseDataInitVal = getResponseData();
        }
        registerChange("responseData", _responseDataInitVal, responseData);
        _responseData = responseData;
    }
                        
    /**
     * Returns the property 'responseData'.
     *
     * 
     *
     */
    public String getResponseData() {
        return _responseData;
    }
                                    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creationDate", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    public void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant) {
        _applicant = applicant;
    }
            
    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Applicant getApplicant() {
        return _applicant;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_identificationBrokerageBankResult instanceof de.smava.webapp.commons.domain.Entity && !_identificationBrokerageBankResult.getChangeSet().isEmpty()) {
             for (String element : _identificationBrokerageBankResult.getChangeSet()) {
                 result.add("identificationBrokerageBankResult : " + element);
             }
         }

         if (_applicant instanceof de.smava.webapp.commons.domain.Entity && !_applicant.getChangeSet().isEmpty()) {
             for (String element : _applicant.getChangeSet()) {
                 result.add("applicant : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdentificationBrokerageBankResultApplicant.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _responseData=").append(_responseData);
            builder.append("\n}");
        } else {
            builder.append(IdentificationBrokerageBankResultApplicant.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IdentificationBrokerageBankResultApplicant asIdentificationBrokerageBankResultApplicant() {
        return this;
    }
}
