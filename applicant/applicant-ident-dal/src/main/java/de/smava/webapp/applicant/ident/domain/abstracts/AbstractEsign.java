//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(esign)}
import de.smava.webapp.applicant.ident.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;


import de.smava.webapp.applicant.ident.domain.interfaces.EsignEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Esigns'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractEsign
    extends BrokerageEntity    implements EsignEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractEsign.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Esign)) {
            return;
        }
        
        this.setBrokerageApplication(((Esign) oldEntity).getBrokerageApplication());    
        this.setApplicant(((Esign) oldEntity).getApplicant());    
        this.setStatus(((Esign) oldEntity).getStatus());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setUpdateDate(((Esign) oldEntity).getUpdateDate());    
        this.setEsignUrl(((Esign) oldEntity).getEsignUrl());    
        this.setContractDocAvailable(((Esign) oldEntity).getContractDocAvailable());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Esign)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((Esign) otherEntity).getBrokerageApplication());    
        equals = equals && valuesAreEqual(this.getApplicant(), ((Esign) otherEntity).getApplicant());    
        equals = equals && valuesAreEqual(this.getStatus(), ((Esign) otherEntity).getStatus());    
            
        equals = equals && valuesAreEqual(this.getUpdateDate(), ((Esign) otherEntity).getUpdateDate());    
        equals = equals && valuesAreEqual(this.getEsignUrl(), ((Esign) otherEntity).getEsignUrl());    
        equals = equals && valuesAreEqual(this.getContractDocAvailable(), ((Esign) otherEntity).getContractDocAvailable());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(esign)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

