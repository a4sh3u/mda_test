package de.smava.webapp.applicant.ident.domain.interfaces;



import de.smava.webapp.applicant.ident.domain.VideoIdentification;

import java.util.Date;


/**
 * The domain object that represents 'VideoIdentifications'.
 *
 * @author generator
 */
public interface VideoIdentificationEntityInterface {

    /**
     * Setter for the property 'brokerageApplication'.
     *
     * 
     *
     */
    void setBrokerageApplication(de.smava.webapp.applicant.brokerage.domain.BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerageApplication'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant);

    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getApplicant();
    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    void setStatus(de.smava.webapp.applicant.ident.type.VideoIdentificationStatus status);

    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.ident.type.VideoIdentificationStatus getStatus();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'updateDate'.
     *
     * 
     *
     */
    void setUpdateDate(Date updateDate);

    /**
     * Returns the property 'updateDate'.
     *
     * 
     *
     */
    Date getUpdateDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    VideoIdentification asVideoIdentification();
}
