//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainHistoryClass.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.history;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(esign)}

import de.smava.webapp.applicant.ident.domain.abstracts.AbstractEsign;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that has all history aggregation related fields for 'Esigns'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EsignHistory extends AbstractEsign {

    protected transient de.smava.webapp.applicant.ident.type.EsignStatus _statusInitVal;
    protected transient boolean _statusIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _updateDateInitVal;
    protected transient boolean _updateDateIsSet;
    protected transient String _esignUrlInitVal;
    protected transient boolean _esignUrlIsSet;
    protected transient Boolean _contractDocAvailableInitVal;
    protected transient boolean _contractDocAvailableIsSet;


			
    /**
     * Returns the initial value of the property 'status'.
     */
    public de.smava.webapp.applicant.ident.type.EsignStatus statusInitVal() {
        de.smava.webapp.applicant.ident.type.EsignStatus result;
        if (_statusIsSet) {
            result = _statusInitVal;
        } else {
            result = getStatus();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'status'.
     */
    public boolean statusIsDirty() {
        return !valuesAreEqual(statusInitVal(), getStatus());
    }

    /**
     * Returns true if the setter method was called for the property 'status'.
     */
    public boolean statusIsSet() {
        return _statusIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creationDate'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creationDate'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creationDate'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'updateDate'.
     */
    public Date updateDateInitVal() {
        Date result;
        if (_updateDateIsSet) {
            result = _updateDateInitVal;
        } else {
            result = getUpdateDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'updateDate'.
     */
    public boolean updateDateIsDirty() {
        return !valuesAreEqual(updateDateInitVal(), getUpdateDate());
    }

    /**
     * Returns true if the setter method was called for the property 'updateDate'.
     */
    public boolean updateDateIsSet() {
        return _updateDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'esignUrl'.
     */
    public String esignUrlInitVal() {
        String result;
        if (_esignUrlIsSet) {
            result = _esignUrlInitVal;
        } else {
            result = getEsignUrl();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'esignUrl'.
     */
    public boolean esignUrlIsDirty() {
        return !valuesAreEqual(esignUrlInitVal(), getEsignUrl());
    }

    /**
     * Returns true if the setter method was called for the property 'esignUrl'.
     */
    public boolean esignUrlIsSet() {
        return _esignUrlIsSet;
    }
	
    /**
     * Returns the initial value of the property 'contractDocAvailable'.
     */
    public Boolean contractDocAvailableInitVal() {
        Boolean result;
        if (_contractDocAvailableIsSet) {
            result = _contractDocAvailableInitVal;
        } else {
            result = getContractDocAvailable();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'contractDocAvailable'.
     */
    public boolean contractDocAvailableIsDirty() {
        return !valuesAreEqual(contractDocAvailableInitVal(), getContractDocAvailable());
    }

    /**
     * Returns true if the setter method was called for the property 'contractDocAvailable'.
     */
    public boolean contractDocAvailableIsSet() {
        return _contractDocAvailableIsSet;
    }

}
