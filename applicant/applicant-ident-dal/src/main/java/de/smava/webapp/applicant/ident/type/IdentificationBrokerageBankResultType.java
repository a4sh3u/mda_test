package de.smava.webapp.applicant.ident.type;

/**
 * @author dsuszczynski
 * @since 26.02.15
 */
public enum IdentificationBrokerageBankResultType {
    VIDEO,
    POSTIDENT,
    UNKNOWN,
    PERSONAL,
    OTHER
}
