//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result)}
import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.applicant.ident.dao.IdentificationBrokerageBankResultDao;
import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResult;

import org.springframework.stereotype.Repository;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import javax.jdo.Query;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'IdentificationBrokerageBankResults'.
 *
 * @author generator
 */
@Repository(value = "applicantIdentificationBrokerageBankResultDao")
public class JdoIdentificationBrokerageBankResultDao extends JdoBaseDao implements IdentificationBrokerageBankResultDao {

    private static final Logger LOGGER = Logger.getLogger(JdoIdentificationBrokerageBankResultDao.class);

    private static final String CLASS_NAME = "IdentificationBrokerageBankResult";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(identification brokerage bank result)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the identification brokerage bank result identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public IdentificationBrokerageBankResult load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        IdentificationBrokerageBankResult result = getEntity(IdentificationBrokerageBankResult.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public IdentificationBrokerageBankResult getIdentificationBrokerageBankResult(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	IdentificationBrokerageBankResult entity = findUniqueEntity(IdentificationBrokerageBankResult.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the identification brokerage bank result.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(IdentificationBrokerageBankResult identificationBrokerageBankResult) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveIdentificationBrokerageBankResult: " + identificationBrokerageBankResult);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(identification brokerage bank result)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(identificationBrokerageBankResult);
    }

    /**
     * @deprecated Use {@link #save(IdentificationBrokerageBankResult) instead}
     */
    public Long saveIdentificationBrokerageBankResult(IdentificationBrokerageBankResult identificationBrokerageBankResult) {
        return save(identificationBrokerageBankResult);
    }

    /**
     * Deletes an identification brokerage bank result, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification brokerage bank result
     */
    public void deleteIdentificationBrokerageBankResult(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteIdentificationBrokerageBankResult: " + id);
        }
        deleteEntity(IdentificationBrokerageBankResult.class, id);
    }

    /**
     * Retrieves all 'IdentificationBrokerageBankResult' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<IdentificationBrokerageBankResult> result = getEntities(IdentificationBrokerageBankResult.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'IdentificationBrokerageBankResult' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationBrokerageBankResult> result = getEntities(IdentificationBrokerageBankResult.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankResult' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResult> result = getEntities(IdentificationBrokerageBankResult.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResult' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<IdentificationBrokerageBankResult> getIdentificationBrokerageBankResultList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResult> result = getEntities(IdentificationBrokerageBankResult.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationBrokerageBankResult' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResult> findIdentificationBrokerageBankResultList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<IdentificationBrokerageBankResult> result = findEntities(IdentificationBrokerageBankResult.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationBrokerageBankResult' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResult> findIdentificationBrokerageBankResultList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<IdentificationBrokerageBankResult> result = findEntities(IdentificationBrokerageBankResult.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'IdentificationBrokerageBankResult' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResult> findIdentificationBrokerageBankResultList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationBrokerageBankResult> result = findEntities(IdentificationBrokerageBankResult.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankResult' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResult> findIdentificationBrokerageBankResultList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResult> result = findEntities(IdentificationBrokerageBankResult.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResult' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResult> findIdentificationBrokerageBankResultList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResult> result = findEntities(IdentificationBrokerageBankResult.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResult' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResult> findIdentificationBrokerageBankResultList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResult> result = findEntities(IdentificationBrokerageBankResult.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankResult' instances.
     */
    public long getIdentificationBrokerageBankResultCount() {
        long result = getEntityCount(IdentificationBrokerageBankResult.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankResult' instances which match the given whereClause.
     */
    public long getIdentificationBrokerageBankResultCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(IdentificationBrokerageBankResult.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankResult' instances which match the given whereClause together with params specified in object array.
     */
    public long getIdentificationBrokerageBankResultCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(IdentificationBrokerageBankResult.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification brokerage bank result)}
    //
    // insert custom methods here

    @Override
    public IdentificationBrokerageBankResult getIdentificationBrokerageBankResult( Account account, BrokerageBank bank){
        Query query = getPersistenceManager().newNamedQuery(IdentificationBrokerageBankResult.class, "fetchIdentificationBrokerageBankResult");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("bank", bank);
        params.put("accountId", account.getId());

        @SuppressWarnings("unchecked")
        Collection<IdentificationBrokerageBankResult> identificationBrokerageBankResult = (Collection<IdentificationBrokerageBankResult>) query.executeWithMap(params);

        if ( identificationBrokerageBankResult.size() > 0 ){
            return identificationBrokerageBankResult.iterator().next();
        }
        return null;
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
