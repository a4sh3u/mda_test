//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(http log)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.ident.domain.HttpLog;
import de.smava.webapp.applicant.ident.domain.interfaces.HttpLogEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'HttpLogs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractHttpLog
    extends BrokerageEntity    implements HttpLogEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractHttpLog.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof HttpLog)) {
            return;
        }
        
        this.setType(((HttpLog) oldEntity).getType());    
        this.setUrl(((HttpLog) oldEntity).getUrl());    
        this.setMethod(((HttpLog) oldEntity).getMethod());    
        this.setBody(((HttpLog) oldEntity).getBody());    
        this.setParams(((HttpLog) oldEntity).getParams());    
        this.setHeaders(((HttpLog) oldEntity).getHeaders());    
        this.setResponse(((HttpLog) oldEntity).getResponse());    
        this.setCreationDate(CurrentDate.getDate());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof HttpLog)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getType(), ((HttpLog) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getUrl(), ((HttpLog) otherEntity).getUrl());    
        equals = equals && valuesAreEqual(this.getMethod(), ((HttpLog) otherEntity).getMethod());    
        equals = equals && valuesAreEqual(this.getBody(), ((HttpLog) otherEntity).getBody());    
        equals = equals && valuesAreEqual(this.getParams(), ((HttpLog) otherEntity).getParams());    
        equals = equals && valuesAreEqual(this.getHeaders(), ((HttpLog) otherEntity).getHeaders());    
        equals = equals && valuesAreEqual(this.getResponse(), ((HttpLog) otherEntity).getResponse());    
            
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(http log)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

