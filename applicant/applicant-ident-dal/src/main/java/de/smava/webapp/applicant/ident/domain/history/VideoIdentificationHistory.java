package de.smava.webapp.applicant.ident.domain.history;



import de.smava.webapp.applicant.ident.domain.abstracts.AbstractVideoIdentification;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'VideoIdentifications'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class VideoIdentificationHistory extends AbstractVideoIdentification {

    protected transient de.smava.webapp.applicant.ident.type.VideoIdentificationStatus _statusInitVal;
    protected transient boolean _statusIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _updateDateInitVal;
    protected transient boolean _updateDateIsSet;



    /**
     * Returns the initial value of the property 'status'.
     */
    public de.smava.webapp.applicant.ident.type.VideoIdentificationStatus statusInitVal() {
        de.smava.webapp.applicant.ident.type.VideoIdentificationStatus result;
        if (_statusIsSet) {
            result = _statusInitVal;
        } else {
            result = getStatus();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'status'.
     */
    public boolean statusIsDirty() {
        return !valuesAreEqual(statusInitVal(), getStatus());
    }

    /**
     * Returns true if the setter method was called for the property 'status'.
     */
    public boolean statusIsSet() {
        return _statusIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creationDate'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creationDate'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creationDate'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'updateDate'.
     */
    public Date updateDateInitVal() {
        Date result;
        if (_updateDateIsSet) {
            result = _updateDateInitVal;
        } else {
            result = getUpdateDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'updateDate'.
     */
    public boolean updateDateIsDirty() {
        return !valuesAreEqual(updateDateInitVal(), getUpdateDate());
    }

    /**
     * Returns true if the setter method was called for the property 'updateDate'.
     */
    public boolean updateDateIsSet() {
        return _updateDateIsSet;
    }

}
