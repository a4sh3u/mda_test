package de.smava.webapp.applicant.ident.domain.interfaces;



import de.smava.webapp.applicant.ident.domain.HttpLog;

import java.util.Date;


/**
 * The domain object that represents 'HttpLogs'.
 *
 * @author generator
 */
public interface HttpLogEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'url'.
     *
     * 
     *
     */
    void setUrl(String url);

    /**
     * Returns the property 'url'.
     *
     * 
     *
     */
    String getUrl();
    /**
     * Setter for the property 'method'.
     *
     * 
     *
     */
    void setMethod(String method);

    /**
     * Returns the property 'method'.
     *
     * 
     *
     */
    String getMethod();
    /**
     * Setter for the property 'body'.
     *
     * 
     *
     */
    void setBody(String body);

    /**
     * Returns the property 'body'.
     *
     * 
     *
     */
    String getBody();
    /**
     * Setter for the property 'params'.
     *
     * 
     *
     */
    void setParams(String params);

    /**
     * Returns the property 'params'.
     *
     * 
     *
     */
    String getParams();
    /**
     * Setter for the property 'headers'.
     *
     * 
     *
     */
    void setHeaders(String headers);

    /**
     * Returns the property 'headers'.
     *
     * 
     *
     */
    String getHeaders();
    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    void setResponse(String response);

    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    String getResponse();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    HttpLog asHttpLog();
}
