package de.smava.webapp.applicant.ident.domain.history;



import de.smava.webapp.applicant.ident.domain.abstracts.AbstractIdentificationBrokerageBankResult;




/**
 * The domain object that has all history aggregation related fields for 'IdentificationBrokerageBankResults'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IdentificationBrokerageBankResultHistory extends AbstractIdentificationBrokerageBankResult {



			
}
