//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result applicant)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResultApplicant;
import de.smava.webapp.applicant.ident.domain.interfaces.IdentificationBrokerageBankResultApplicantEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationBrokerageBankResultApplicants'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractIdentificationBrokerageBankResultApplicant
    extends BrokerageEntity    implements IdentificationBrokerageBankResultApplicantEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractIdentificationBrokerageBankResultApplicant.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof IdentificationBrokerageBankResultApplicant)) {
            return;
        }
        
        this.setIdentificationBrokerageBankResult(((IdentificationBrokerageBankResultApplicant) oldEntity).getIdentificationBrokerageBankResult());    
        this.setType(((IdentificationBrokerageBankResultApplicant) oldEntity).getType());    
        this.setState(((IdentificationBrokerageBankResultApplicant) oldEntity).getState());    
        this.setResponseData(((IdentificationBrokerageBankResultApplicant) oldEntity).getResponseData());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setApplicant(((IdentificationBrokerageBankResultApplicant) oldEntity).getApplicant());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof IdentificationBrokerageBankResultApplicant)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getIdentificationBrokerageBankResult(), ((IdentificationBrokerageBankResultApplicant) otherEntity).getIdentificationBrokerageBankResult());    
        equals = equals && valuesAreEqual(this.getType(), ((IdentificationBrokerageBankResultApplicant) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getState(), ((IdentificationBrokerageBankResultApplicant) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getResponseData(), ((IdentificationBrokerageBankResultApplicant) otherEntity).getResponseData());    
            
        equals = equals && valuesAreEqual(this.getApplicant(), ((IdentificationBrokerageBankResultApplicant) otherEntity).getApplicant());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(identification brokerage bank result applicant)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

