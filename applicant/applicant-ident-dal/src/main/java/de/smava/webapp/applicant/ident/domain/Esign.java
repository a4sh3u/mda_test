//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(esign)}
import de.smava.webapp.applicant.ident.domain.history.EsignHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Esigns'.
 *
 * 
 *
 * @author generator
 */
public class Esign extends EsignHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(esign)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.brokerage.domain.BrokerageApplication _brokerageApplication;
        protected de.smava.webapp.applicant.domain.Applicant _applicant;
        protected de.smava.webapp.applicant.ident.type.EsignStatus _status;
        protected Date _creationDate;
        protected Date _updateDate;
        protected String _esignUrl;
        protected Boolean _contractDocAvailable;
        
                                    
    /**
     * Setter for the property 'brokerageApplication'.
     *
     * 
     *
     */
    public void setBrokerageApplication(de.smava.webapp.applicant.brokerage.domain.BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    /**
     * Returns the property 'brokerageApplication'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }
                                            
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    public void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant) {
        _applicant = applicant;
    }
            
    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Applicant getApplicant() {
        return _applicant;
    }
                                    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    public void setStatus(de.smava.webapp.applicant.ident.type.EsignStatus status) {
        if (!_statusIsSet) {
            _statusIsSet = true;
            _statusInitVal = getStatus();
        }
        registerChange("status", _statusInitVal, status);
        _status = status;
    }
                        
    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.ident.type.EsignStatus getStatus() {
        return _status;
    }
                                    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creationDate", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'updateDate'.
     *
     * 
     *
     */
    public void setUpdateDate(Date updateDate) {
        if (!_updateDateIsSet) {
            _updateDateIsSet = true;
            _updateDateInitVal = getUpdateDate();
        }
        registerChange("updateDate", _updateDateInitVal, updateDate);
        _updateDate = updateDate;
    }
                        
    /**
     * Returns the property 'updateDate'.
     *
     * 
     *
     */
    public Date getUpdateDate() {
        return _updateDate;
    }
                                    /**
     * Setter for the property 'esignUrl'.
     *
     * 
     *
     */
    public void setEsignUrl(String esignUrl) {
        if (!_esignUrlIsSet) {
            _esignUrlIsSet = true;
            _esignUrlInitVal = getEsignUrl();
        }
        registerChange("esignUrl", _esignUrlInitVal, esignUrl);
        _esignUrl = esignUrl;
    }
                        
    /**
     * Returns the property 'esignUrl'.
     *
     * 
     *
     */
    public String getEsignUrl() {
        return _esignUrl;
    }
                                    /**
     * Setter for the property 'contractDocAvailable'.
     *
     * 
     *
     */
    public void setContractDocAvailable(Boolean contractDocAvailable) {
        if (!_contractDocAvailableIsSet) {
            _contractDocAvailableIsSet = true;
            _contractDocAvailableInitVal = getContractDocAvailable();
        }
        registerChange("contractDocAvailable", _contractDocAvailableInitVal, contractDocAvailable);
        _contractDocAvailable = contractDocAvailable;
    }
                        
    /**
     * Returns the property 'contractDocAvailable'.
     *
     * 
     *
     */
    public Boolean getContractDocAvailable() {
        return _contractDocAvailable;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageApplication instanceof de.smava.webapp.commons.domain.Entity && !_brokerageApplication.getChangeSet().isEmpty()) {
             for (String element : _brokerageApplication.getChangeSet()) {
                 result.add("brokerageApplication : " + element);
             }
         }

         if (_applicant instanceof de.smava.webapp.commons.domain.Entity && !_applicant.getChangeSet().isEmpty()) {
             for (String element : _applicant.getChangeSet()) {
                 result.add("applicant : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Esign.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _status=").append(_status);
            builder.append("\n    _esignUrl=").append(_esignUrl);
            builder.append("\n}");
        } else {
            builder.append(Esign.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Esign asEsign() {
        return this;
    }
}
