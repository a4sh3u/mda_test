//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResult;
import de.smava.webapp.applicant.ident.domain.interfaces.IdentificationBrokerageBankResultEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationBrokerageBankResults'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractIdentificationBrokerageBankResult
    extends BrokerageEntity    implements IdentificationBrokerageBankResultEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractIdentificationBrokerageBankResult.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof IdentificationBrokerageBankResult)) {
            return;
        }
        
        this.setAccount(((IdentificationBrokerageBankResult) oldEntity).getAccount());    
        this.setBrokerageBank(((IdentificationBrokerageBankResult) oldEntity).getBrokerageBank());    
        this.setIdentificationBrokerageBankResultApplicants(((IdentificationBrokerageBankResult) oldEntity).getIdentificationBrokerageBankResultApplicants());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof IdentificationBrokerageBankResult)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getAccount(), ((IdentificationBrokerageBankResult) otherEntity).getAccount());    
        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((IdentificationBrokerageBankResult) otherEntity).getBrokerageBank());    
        equals = equals && valuesAreEqual(this.getIdentificationBrokerageBankResultApplicants(), ((IdentificationBrokerageBankResult) otherEntity).getIdentificationBrokerageBankResultApplicants());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(identification brokerage bank result)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

