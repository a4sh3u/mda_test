//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(http log)}
import de.smava.webapp.applicant.ident.domain.history.HttpLogHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'HttpLogs'.
 *
 * 
 *
 * @author generator
 */
public class HttpLog extends HttpLogHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(http log)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _type;
        protected String _url;
        protected String _method;
        protected String _body;
        protected String _params;
        protected String _headers;
        protected String _response;
        protected Date _creationDate;
        
                            /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'url'.
     *
     * 
     *
     */
    public void setUrl(String url) {
        if (!_urlIsSet) {
            _urlIsSet = true;
            _urlInitVal = getUrl();
        }
        registerChange("url", _urlInitVal, url);
        _url = url;
    }
                        
    /**
     * Returns the property 'url'.
     *
     * 
     *
     */
    public String getUrl() {
        return _url;
    }
                                    /**
     * Setter for the property 'method'.
     *
     * 
     *
     */
    public void setMethod(String method) {
        if (!_methodIsSet) {
            _methodIsSet = true;
            _methodInitVal = getMethod();
        }
        registerChange("method", _methodInitVal, method);
        _method = method;
    }
                        
    /**
     * Returns the property 'method'.
     *
     * 
     *
     */
    public String getMethod() {
        return _method;
    }
                                    /**
     * Setter for the property 'body'.
     *
     * 
     *
     */
    public void setBody(String body) {
        if (!_bodyIsSet) {
            _bodyIsSet = true;
            _bodyInitVal = getBody();
        }
        registerChange("body", _bodyInitVal, body);
        _body = body;
    }
                        
    /**
     * Returns the property 'body'.
     *
     * 
     *
     */
    public String getBody() {
        return _body;
    }
                                    /**
     * Setter for the property 'params'.
     *
     * 
     *
     */
    public void setParams(String params) {
        if (!_paramsIsSet) {
            _paramsIsSet = true;
            _paramsInitVal = getParams();
        }
        registerChange("params", _paramsInitVal, params);
        _params = params;
    }
                        
    /**
     * Returns the property 'params'.
     *
     * 
     *
     */
    public String getParams() {
        return _params;
    }
                                    /**
     * Setter for the property 'headers'.
     *
     * 
     *
     */
    public void setHeaders(String headers) {
        if (!_headersIsSet) {
            _headersIsSet = true;
            _headersInitVal = getHeaders();
        }
        registerChange("headers", _headersInitVal, headers);
        _headers = headers;
    }
                        
    /**
     * Returns the property 'headers'.
     *
     * 
     *
     */
    public String getHeaders() {
        return _headers;
    }
                                    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    public void setResponse(String response) {
        if (!_responseIsSet) {
            _responseIsSet = true;
            _responseInitVal = getResponse();
        }
        registerChange("response", _responseInitVal, response);
        _response = response;
    }
                        
    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    public String getResponse() {
        return _response;
    }
                                    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creationDate", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(HttpLog.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _url=").append(_url);
            builder.append("\n    _method=").append(_method);
            builder.append("\n    _body=").append(_body);
            builder.append("\n    _params=").append(_params);
            builder.append("\n    _headers=").append(_headers);
            builder.append("\n    _response=").append(_response);
            builder.append("\n}");
        } else {
            builder.append(HttpLog.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public HttpLog asHttpLog() {
        return this;
    }
}
