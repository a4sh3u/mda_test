//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result)}
import de.smava.webapp.applicant.ident.domain.history.IdentificationBrokerageBankResultHistory;

import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'IdentificationBrokerageBankResults'.
 *
 * 
 *
 * @author generator
 */
public class IdentificationBrokerageBankResult extends IdentificationBrokerageBankResultHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(identification brokerage bank result)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBank _brokerageBank;
        protected Set<IdentificationBrokerageBankResultApplicant> _identificationBrokerageBankResultApplicants;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'brokerageBank'.
     *
     * 
     *
     */
    public void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerageBank'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'identificationBrokerageBankResultApplicants'.
     *
     * 
     *
     */
    public void setIdentificationBrokerageBankResultApplicants(Set<IdentificationBrokerageBankResultApplicant> identificationBrokerageBankResultApplicants) {
        _identificationBrokerageBankResultApplicants = identificationBrokerageBankResultApplicants;
    }
            
    /**
     * Returns the property 'identificationBrokerageBankResultApplicants'.
     *
     * 
     *
     */
    public Set<IdentificationBrokerageBankResultApplicant> getIdentificationBrokerageBankResultApplicants() {
        return _identificationBrokerageBankResultApplicants;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerageBank : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(IdentificationBrokerageBankResult.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(IdentificationBrokerageBankResult.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public IdentificationBrokerageBankResult asIdentificationBrokerageBankResult() {
        return this;
    }
}
