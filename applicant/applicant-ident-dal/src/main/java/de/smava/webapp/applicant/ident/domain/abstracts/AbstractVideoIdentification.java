//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(video identification)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.ident.domain.VideoIdentification;
import de.smava.webapp.applicant.ident.domain.interfaces.VideoIdentificationEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'VideoIdentifications'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractVideoIdentification
    extends BrokerageEntity    implements VideoIdentificationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractVideoIdentification.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof VideoIdentification)) {
            return;
        }
        
        this.setBrokerageApplication(((VideoIdentification) oldEntity).getBrokerageApplication());    
        this.setApplicant(((VideoIdentification) oldEntity).getApplicant());    
        this.setStatus(((VideoIdentification) oldEntity).getStatus());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setUpdateDate(((VideoIdentification) oldEntity).getUpdateDate());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof VideoIdentification)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((VideoIdentification) otherEntity).getBrokerageApplication());    
        equals = equals && valuesAreEqual(this.getApplicant(), ((VideoIdentification) otherEntity).getApplicant());    
        equals = equals && valuesAreEqual(this.getStatus(), ((VideoIdentification) otherEntity).getStatus());    
            
        equals = equals && valuesAreEqual(this.getUpdateDate(), ((VideoIdentification) otherEntity).getUpdateDate());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(video identification)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

