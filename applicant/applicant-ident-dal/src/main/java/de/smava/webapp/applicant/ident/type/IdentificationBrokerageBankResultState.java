package de.smava.webapp.applicant.ident.type;

/**
 * @author dsuszczynski
 * @since 26.02.15
 */
public enum IdentificationBrokerageBankResultState {
    IDENTIFIED,
    IDENTIFIED_DATA_CHANGE,
    NOT_IDENTIFIED,
    UNKNOWN
}
