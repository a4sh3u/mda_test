//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(identification brokerage bank result applicant)}
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.applicant.ident.dao.IdentificationBrokerageBankResultApplicantDao;
import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResultApplicant;

import javax.transaction.Synchronization;

import org.springframework.stereotype.Repository;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'IdentificationBrokerageBankResultApplicants'.
 *
 * @author generator
 */
@Repository(value = "applicantIdentificationBrokerageBankResultApplicantDao")
public class JdoIdentificationBrokerageBankResultApplicantDao extends JdoBaseDao implements IdentificationBrokerageBankResultApplicantDao {

    private static final Logger LOGGER = Logger.getLogger(JdoIdentificationBrokerageBankResultApplicantDao.class);

    private static final String CLASS_NAME = "IdentificationBrokerageBankResultApplicant";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(identification brokerage bank result applicant)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the identification brokerage bank result applicant identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public IdentificationBrokerageBankResultApplicant load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        IdentificationBrokerageBankResultApplicant result = getEntity(IdentificationBrokerageBankResultApplicant.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public IdentificationBrokerageBankResultApplicant getIdentificationBrokerageBankResultApplicant(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	IdentificationBrokerageBankResultApplicant entity = findUniqueEntity(IdentificationBrokerageBankResultApplicant.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the identification brokerage bank result applicant.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(IdentificationBrokerageBankResultApplicant identificationBrokerageBankResultApplicant) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveIdentificationBrokerageBankResultApplicant: " + identificationBrokerageBankResultApplicant);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(identification brokerage bank result applicant)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(identificationBrokerageBankResultApplicant);
    }

    /**
     * @deprecated Use {@link #save(IdentificationBrokerageBankResultApplicant) instead}
     */
    public Long saveIdentificationBrokerageBankResultApplicant(IdentificationBrokerageBankResultApplicant identificationBrokerageBankResultApplicant) {
        return save(identificationBrokerageBankResultApplicant);
    }

    /**
     * Deletes an identification brokerage bank result applicant, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the identification brokerage bank result applicant
     */
    public void deleteIdentificationBrokerageBankResultApplicant(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteIdentificationBrokerageBankResultApplicant: " + id);
        }
        deleteEntity(IdentificationBrokerageBankResultApplicant.class, id);
    }

    /**
     * Retrieves all 'IdentificationBrokerageBankResultApplicant' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<IdentificationBrokerageBankResultApplicant> getIdentificationBrokerageBankResultApplicantList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = getEntities(IdentificationBrokerageBankResultApplicant.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'IdentificationBrokerageBankResultApplicant' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<IdentificationBrokerageBankResultApplicant> getIdentificationBrokerageBankResultApplicantList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = getEntities(IdentificationBrokerageBankResultApplicant.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankResultApplicant' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<IdentificationBrokerageBankResultApplicant> getIdentificationBrokerageBankResultApplicantList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = getEntities(IdentificationBrokerageBankResultApplicant.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResultApplicant' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<IdentificationBrokerageBankResultApplicant> getIdentificationBrokerageBankResultApplicantList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = getEntities(IdentificationBrokerageBankResultApplicant.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationBrokerageBankResultApplicant' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResultApplicant> findIdentificationBrokerageBankResultApplicantList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = findEntities(IdentificationBrokerageBankResultApplicant.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'IdentificationBrokerageBankResultApplicant' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResultApplicant> findIdentificationBrokerageBankResultApplicantList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = findEntities(IdentificationBrokerageBankResultApplicant.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'IdentificationBrokerageBankResultApplicant' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResultApplicant> findIdentificationBrokerageBankResultApplicantList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = findEntities(IdentificationBrokerageBankResultApplicant.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'IdentificationBrokerageBankResultApplicant' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResultApplicant> findIdentificationBrokerageBankResultApplicantList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = findEntities(IdentificationBrokerageBankResultApplicant.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResultApplicant' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResultApplicant> findIdentificationBrokerageBankResultApplicantList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = findEntities(IdentificationBrokerageBankResultApplicant.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'IdentificationBrokerageBankResultApplicant' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<IdentificationBrokerageBankResultApplicant> findIdentificationBrokerageBankResultApplicantList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<IdentificationBrokerageBankResultApplicant> result = findEntities(IdentificationBrokerageBankResultApplicant.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankResultApplicant' instances.
     */
    public long getIdentificationBrokerageBankResultApplicantCount() {
        long result = getEntityCount(IdentificationBrokerageBankResultApplicant.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankResultApplicant' instances which match the given whereClause.
     */
    public long getIdentificationBrokerageBankResultApplicantCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(IdentificationBrokerageBankResultApplicant.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'IdentificationBrokerageBankResultApplicant' instances which match the given whereClause together with params specified in object array.
     */
    public long getIdentificationBrokerageBankResultApplicantCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(IdentificationBrokerageBankResultApplicant.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(identification brokerage bank result applicant)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
