package de.smava.webapp.applicant.ident.domain.history;



import de.smava.webapp.applicant.ident.domain.abstracts.AbstractHttpLog;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'HttpLogs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class HttpLogHistory extends AbstractHttpLog {

    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _urlInitVal;
    protected transient boolean _urlIsSet;
    protected transient String _methodInitVal;
    protected transient boolean _methodIsSet;
    protected transient String _bodyInitVal;
    protected transient boolean _bodyIsSet;
    protected transient String _paramsInitVal;
    protected transient boolean _paramsIsSet;
    protected transient String _headersInitVal;
    protected transient boolean _headersIsSet;
    protected transient String _responseInitVal;
    protected transient boolean _responseIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'url'.
     */
    public String urlInitVal() {
        String result;
        if (_urlIsSet) {
            result = _urlInitVal;
        } else {
            result = getUrl();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'url'.
     */
    public boolean urlIsDirty() {
        return !valuesAreEqual(urlInitVal(), getUrl());
    }

    /**
     * Returns true if the setter method was called for the property 'url'.
     */
    public boolean urlIsSet() {
        return _urlIsSet;
    }
	
    /**
     * Returns the initial value of the property 'method'.
     */
    public String methodInitVal() {
        String result;
        if (_methodIsSet) {
            result = _methodInitVal;
        } else {
            result = getMethod();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'method'.
     */
    public boolean methodIsDirty() {
        return !valuesAreEqual(methodInitVal(), getMethod());
    }

    /**
     * Returns true if the setter method was called for the property 'method'.
     */
    public boolean methodIsSet() {
        return _methodIsSet;
    }
	
    /**
     * Returns the initial value of the property 'body'.
     */
    public String bodyInitVal() {
        String result;
        if (_bodyIsSet) {
            result = _bodyInitVal;
        } else {
            result = getBody();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'body'.
     */
    public boolean bodyIsDirty() {
        return !valuesAreEqual(bodyInitVal(), getBody());
    }

    /**
     * Returns true if the setter method was called for the property 'body'.
     */
    public boolean bodyIsSet() {
        return _bodyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'params'.
     */
    public String paramsInitVal() {
        String result;
        if (_paramsIsSet) {
            result = _paramsInitVal;
        } else {
            result = getParams();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'params'.
     */
    public boolean paramsIsDirty() {
        return !valuesAreEqual(paramsInitVal(), getParams());
    }

    /**
     * Returns true if the setter method was called for the property 'params'.
     */
    public boolean paramsIsSet() {
        return _paramsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'headers'.
     */
    public String headersInitVal() {
        String result;
        if (_headersIsSet) {
            result = _headersInitVal;
        } else {
            result = getHeaders();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'headers'.
     */
    public boolean headersIsDirty() {
        return !valuesAreEqual(headersInitVal(), getHeaders());
    }

    /**
     * Returns true if the setter method was called for the property 'headers'.
     */
    public boolean headersIsSet() {
        return _headersIsSet;
    }
	
    /**
     * Returns the initial value of the property 'response'.
     */
    public String responseInitVal() {
        String result;
        if (_responseIsSet) {
            result = _responseInitVal;
        } else {
            result = getResponse();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response'.
     */
    public boolean responseIsDirty() {
        return !valuesAreEqual(responseInitVal(), getResponse());
    }

    /**
     * Returns true if the setter method was called for the property 'response'.
     */
    public boolean responseIsSet() {
        return _responseIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creationDate'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creationDate'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creationDate'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

}
