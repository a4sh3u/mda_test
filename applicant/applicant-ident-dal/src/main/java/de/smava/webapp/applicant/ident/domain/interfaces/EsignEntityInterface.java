//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainEntityInterface.tpl
//
//
//
//
package de.smava.webapp.applicant.ident.domain.interfaces;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(esign)}

import de.smava.webapp.applicant.ident.domain.Esign;

import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * The domain object that represents 'Esigns'.
 *
 * @author generator
 */
public interface EsignEntityInterface {

    /**
     * Setter for the property 'brokerageApplication'.
     *
     * 
     *
     */
    void setBrokerageApplication(de.smava.webapp.applicant.brokerage.domain.BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerageApplication'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant);

    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getApplicant();
    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    void setStatus(de.smava.webapp.applicant.ident.type.EsignStatus status);

    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.ident.type.EsignStatus getStatus();
    /**
     * Setter for the property 'creationDate'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creationDate'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'updateDate'.
     *
     * 
     *
     */
    void setUpdateDate(Date updateDate);

    /**
     * Returns the property 'updateDate'.
     *
     * 
     *
     */
    Date getUpdateDate();
    /**
     * Setter for the property 'esignUrl'.
     *
     * 
     *
     */
    void setEsignUrl(String esignUrl);

    /**
     * Returns the property 'esignUrl'.
     *
     * 
     *
     */
    String getEsignUrl();
    /**
     * Setter for the property 'contractDocAvailable'.
     *
     * 
     *
     */
    void setContractDocAvailable(Boolean contractDocAvailable);

    /**
     * Returns the property 'contractDocAvailable'.
     *
     * 
     *
     */
    Boolean getContractDocAvailable();
    /**
     * Helper method to get reference of this object as model type.
     */
    Esign asEsign();
}
