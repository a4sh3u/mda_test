package de.smava.webapp.applicant.ident.type;

/**
 * Created by aibrahim on 27.06.16.
 */
public enum EsignStatus {

    EXPECTED,
    IN_PROGRESS,
    SENT,
    RECEIVED,
    FAILED;

    public boolean isFinished(){
        return this == SENT || this == RECEIVED;
    }

    public boolean isTransitionIsPossible(EsignStatus to) {
        switch (this) {
            case EXPECTED:
                return true;
            case IN_PROGRESS:
                return !to.equals(EXPECTED);
            case SENT:
                return to.equals(SENT) || to.equals(RECEIVED) || to.equals(FAILED);
            case RECEIVED:
                return to.equals(RECEIVED) || to.equals(FAILED);
            case FAILED:
                return to.equals(FAILED);
            default:
                return true;
        }
    }
}
