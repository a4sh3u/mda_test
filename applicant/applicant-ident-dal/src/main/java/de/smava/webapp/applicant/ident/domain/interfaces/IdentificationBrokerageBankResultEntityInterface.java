package de.smava.webapp.applicant.ident.domain.interfaces;



import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResult;
import de.smava.webapp.applicant.ident.domain.IdentificationBrokerageBankResultApplicant;

import java.util.Set;


/**
 * The domain object that represents 'IdentificationBrokerageBankResults'.
 *
 * @author generator
 */
public interface IdentificationBrokerageBankResultEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'brokerageBank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerageBank'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'identificationBrokerageBankResultApplicants'.
     *
     * 
     *
     */
    void setIdentificationBrokerageBankResultApplicants(Set<IdentificationBrokerageBankResultApplicant> identificationBrokerageBankResultApplicants);

    /**
     * Returns the property 'identificationBrokerageBankResultApplicants'.
     *
     * 
     *
     */
    Set<IdentificationBrokerageBankResultApplicant> getIdentificationBrokerageBankResultApplicants();
    /**
     * Helper method to get reference of this object as model type.
     */
    IdentificationBrokerageBankResult asIdentificationBrokerageBankResult();
}
