package de.smava.webapp.applicant.marketing.builder;


import de.smava.webapp.applicant.account.builder.AccountEntityBuilder;
import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.builder.EntityBuilder;
import de.smava.webapp.applicant.marketing.affiliate.domain.AccountAffiliateInformation;

/**
 * @author Adam
 * @since 2016-01-20.
 */
public class AccountAffiliateInformationEntityBuilder extends EntityBuilder<AccountAffiliateInformation> {

    protected AccountAffiliateInformation createEntity() {
        return new AccountAffiliateInformation();
    }

    public static AccountAffiliateInformationEntityBuilder aBuilder() {
        return new AccountAffiliateInformationEntityBuilder();
    }

    public AccountAffiliateInformationEntityBuilder withData() {
        entity = AccountAffiliateInformationEntityBuilder
                .aBuilder()
                .withData(AccountEntityBuilder.aBuilder().withData().build())
                .build();

        return this;
    }

    public AccountAffiliateInformationEntityBuilder withData(Account account) {
        entity.setId(getNextId());
        entity.setAccount(account);
        entity.setFirstAffiliate(AffiliateInformationEntityBuilder.aBuilder().withData(account).build());
        entity.setSaleAffiliate(AffiliateInformationEntityBuilder.aBuilder().withData(account).build());

        return this;
    }
}
