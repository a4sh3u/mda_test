package de.smava.webapp.applicant.marketing.builder;

import de.smava.webapp.applicant.account.builder.AccountEntityBuilder;
import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.builder.EntityBuilder;
import de.smava.webapp.applicant.marketing.affiliate.domain.AffiliateInformation;
import de.smava.webapp.applicant.marketing.domain.MarketingPlacement;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.joda.time.DateTime;

/**
 * @author Adam
 * @since 2016-01-21.
 */
public class AffiliateInformationEntityBuilder extends EntityBuilder<AffiliateInformation> {

    protected AffiliateInformation createEntity() {
        return new AffiliateInformation();
    }

    public static AffiliateInformationEntityBuilder aBuilder() {
        return new AffiliateInformationEntityBuilder();
    }

    public AffiliateInformationEntityBuilder withData() {
        entity = AffiliateInformationEntityBuilder
                .aBuilder()
                .withData(AccountEntityBuilder.aBuilder().withData().build(),
                        MarketingPlacementEntityBuilder.aBuilder().withData().build())
                .build();

        return this;
    }

    public AffiliateInformationEntityBuilder withData(Account account) {
        entity = AffiliateInformationEntityBuilder
                .aBuilder()
                .withData(account, MarketingPlacementEntityBuilder.aBuilder().withData().build())
                .build();

        return this;
    }

    public AffiliateInformationEntityBuilder withData(MarketingPlacement marketingPlacement) {
        entity = AffiliateInformationEntityBuilder
                .aBuilder()
                .withData(AccountEntityBuilder.aBuilder().withData().build(), marketingPlacement)
                .build();

        return this;
    }

    public AffiliateInformationEntityBuilder withData(Account account, MarketingPlacement marketingPlacement) {
        // TODO: uncomment fields when MDA object is changed
        entity.setId(getNextId());
        entity.setAccount(account);
        entity.setMarketingPlacement(marketingPlacement);
        // entity.setExternalAffiliateSubId("ExternalAffiliateSubId");
        entity.setExternalAffiliateId("ExternalAffiliateId");
        DateTime now = new DateTime(CurrentDate.getDate());
        entity.setCreationDate(now.toDate());
        entity.setExpirationDate(now.plusYears(1).toDate());
        // entity.setExternalAffiliateChannel("ExternalAffiliateChannel");

        return this;
    }
}
