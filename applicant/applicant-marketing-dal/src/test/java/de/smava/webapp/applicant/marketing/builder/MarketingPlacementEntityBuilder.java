package de.smava.webapp.applicant.marketing.builder;

import de.smava.webapp.applicant.builder.EntityBuilder;
import de.smava.webapp.applicant.marketing.domain.MarketingPlacement;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.joda.time.DateTime;

import java.util.Collections;

/**
 * @author Adam
 * @since 2016-01-21.
 */
public class MarketingPlacementEntityBuilder extends EntityBuilder<MarketingPlacement> {

    protected MarketingPlacement createEntity() {
        return new MarketingPlacement();
    }

    public static MarketingPlacementEntityBuilder aBuilder() {
        return new MarketingPlacementEntityBuilder();
    }

    public MarketingPlacementEntityBuilder withData() {
        entity.setId(getNextId());
        entity.setUserDataSharing(false);
        entity.setQuantity(1);
        entity.setCreationDate(CurrentDate.getDate());
        entity.setPublication(null);
        entity.setPosition("Position");
        entity.setAlternateCode("AlternateCode");
        entity.setCampaign(null);
        entity.setComment("Comment");
        entity.setMarketingContactPlacements(Collections.EMPTY_LIST);
        entity.setMarketingDestinationUrlPlacements(Collections.EMPTY_SET);
        entity.setNoBrokerage(true);

        return this;
    }
}
