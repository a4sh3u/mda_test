//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.marketing.domain.MarketingContact;
import de.smava.webapp.applicant.marketing.domain.interfaces.MarketingContactEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingContacts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingContact
    extends BrokerageEntity    implements MarketingContactEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingContact.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingContact)) {
            return;
        }
        
        this.setMarketingPartner(((MarketingContact) oldEntity).getMarketingPartner());    
        this.setMarketingContactPlacements(((MarketingContact) oldEntity).getMarketingContactPlacements());    
        this.setAccount(((MarketingContact) oldEntity).getAccount());    
        this.setIsAgentAdmin(((MarketingContact) oldEntity).getIsAgentAdmin());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingContact)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getMarketingPartner(), ((MarketingContact) otherEntity).getMarketingPartner());    
        equals = equals && valuesAreEqual(this.getMarketingContactPlacements(), ((MarketingContact) otherEntity).getMarketingContactPlacements());    
        equals = equals && valuesAreEqual(this.getAccount(), ((MarketingContact) otherEntity).getAccount());    
        equals = equals && valuesAreEqual(this.getIsAgentAdmin(), ((MarketingContact) otherEntity).getIsAgentAdmin());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing contact)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

