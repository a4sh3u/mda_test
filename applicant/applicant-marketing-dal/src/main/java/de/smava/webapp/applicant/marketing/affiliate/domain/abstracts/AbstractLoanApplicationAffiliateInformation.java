//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.affiliate.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan application affiliate information)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.marketing.affiliate.domain.LoanApplicationAffiliateInformation;
import de.smava.webapp.applicant.marketing.affiliate.domain.interfaces.LoanApplicationAffiliateInformationEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanApplicationAffiliateInformations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractLoanApplicationAffiliateInformation
    extends BrokerageEntity    implements LoanApplicationAffiliateInformationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractLoanApplicationAffiliateInformation.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof LoanApplicationAffiliateInformation)) {
            return;
        }
        
        this.setAccount(((LoanApplicationAffiliateInformation) oldEntity).getAccount());    
        this.setLoanApplication(((LoanApplicationAffiliateInformation) oldEntity).getLoanApplication());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof LoanApplicationAffiliateInformation)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getAccount(), ((LoanApplicationAffiliateInformation) otherEntity).getAccount());    
        equals = equals && valuesAreEqual(this.getLoanApplication(), ((LoanApplicationAffiliateInformation) otherEntity).getLoanApplication());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(loan application affiliate information)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

