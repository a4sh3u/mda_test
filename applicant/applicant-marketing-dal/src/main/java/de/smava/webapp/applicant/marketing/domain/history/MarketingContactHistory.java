package de.smava.webapp.applicant.marketing.domain.history;



import de.smava.webapp.applicant.marketing.domain.abstracts.AbstractMarketingContact;




/**
 * The domain object that has all history aggregation related fields for 'MarketingContacts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingContactHistory extends AbstractMarketingContact {

    protected transient Boolean _isAgentAdminInitVal;
    protected transient boolean _isAgentAdminIsSet;



    /**
     * Returns the initial value of the property 'is agent admin'.
     */
    public Boolean isAgentAdminInitVal() {
        Boolean result;
        if (_isAgentAdminIsSet) {
            result = _isAgentAdminInitVal;
        } else {
            result = getIsAgentAdmin();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'is agent admin'.
     */
    public boolean isAgentAdminIsDirty() {
        return !valuesAreEqual(isAgentAdminInitVal(), getIsAgentAdmin());
    }

    /**
     * Returns true if the setter method was called for the property 'is agent admin'.
     */
    public boolean isAgentAdminIsSet() {
        return _isAgentAdminIsSet;
    }

}
