//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement)}
import de.smava.webapp.applicant.marketing.domain.history.MarketingPlacementHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPlacements'.
 *
 * 
 *
 * @author generator
 */
public class MarketingPlacement extends MarketingPlacementHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing placement)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected int _quantity;
        protected String _position;
        protected String _comment;
        protected MarketingPublication _publication;
        protected MarketingCampaign _campaign;
        protected Set<MarketingDestinationUrlPlacement> _marketingDestinationUrlPlacements;
        protected Collection<MarketingContactPlacement> _marketingContactPlacements;
        protected String _alternateCode;
        protected Boolean _noBrokerage;
        protected Boolean _userDataSharing;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'quantity'.
     *
     * 
     *
     */
    public void setQuantity(int quantity) {
        if (!_quantityIsSet) {
            _quantityIsSet = true;
            _quantityInitVal = getQuantity();
        }
        registerChange("quantity", _quantityInitVal, quantity);
        _quantity = quantity;
    }
                        
    /**
     * Returns the property 'quantity'.
     *
     * 
     *
     */
    public int getQuantity() {
        return _quantity;
    }
                                    /**
     * Setter for the property 'position'.
     *
     * 
     *
     */
    public void setPosition(String position) {
        if (!_positionIsSet) {
            _positionIsSet = true;
            _positionInitVal = getPosition();
        }
        registerChange("position", _positionInitVal, position);
        _position = position;
    }
                        
    /**
     * Returns the property 'position'.
     *
     * 
     *
     */
    public String getPosition() {
        return _position;
    }
                                    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = getComment();
        }
        registerChange("comment", _commentInitVal, comment);
        _comment = comment;
    }
                        
    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    public String getComment() {
        return _comment;
    }
                                            
    /**
     * Setter for the property 'publication'.
     *
     * 
     *
     */
    public void setPublication(MarketingPublication publication) {
        _publication = publication;
    }
            
    /**
     * Returns the property 'publication'.
     *
     * 
     *
     */
    public MarketingPublication getPublication() {
        return _publication;
    }
                                            
    /**
     * Setter for the property 'campaign'.
     *
     * 
     *
     */
    public void setCampaign(MarketingCampaign campaign) {
        _campaign = campaign;
    }
            
    /**
     * Returns the property 'campaign'.
     *
     * 
     *
     */
    public MarketingCampaign getCampaign() {
        return _campaign;
    }
                                            
    /**
     * Setter for the property 'marketing destination url placements'.
     *
     * 
     *
     */
    public void setMarketingDestinationUrlPlacements(Set<MarketingDestinationUrlPlacement> marketingDestinationUrlPlacements) {
        _marketingDestinationUrlPlacements = marketingDestinationUrlPlacements;
    }
            
    /**
     * Returns the property 'marketing destination url placements'.
     *
     * 
     *
     */
    public Set<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacements() {
        return _marketingDestinationUrlPlacements;
    }
                                            
    /**
     * Setter for the property 'marketing contact placements'.
     *
     * 
     *
     */
    public void setMarketingContactPlacements(Collection<MarketingContactPlacement> marketingContactPlacements) {
        _marketingContactPlacements = marketingContactPlacements;
    }
            
    /**
     * Returns the property 'marketing contact placements'.
     *
     * 
     *
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacements() {
        return _marketingContactPlacements;
    }
                                    /**
     * Setter for the property 'alternate code'.
     *
     * 
     *
     */
    public void setAlternateCode(String alternateCode) {
        if (!_alternateCodeIsSet) {
            _alternateCodeIsSet = true;
            _alternateCodeInitVal = getAlternateCode();
        }
        registerChange("alternate code", _alternateCodeInitVal, alternateCode);
        _alternateCode = alternateCode;
    }
                        
    /**
     * Returns the property 'alternate code'.
     *
     * 
     *
     */
    public String getAlternateCode() {
        return _alternateCode;
    }
                                    /**
     * Setter for the property 'no brokerage'.
     *
     * 
     *
     */
    public void setNoBrokerage(Boolean noBrokerage) {
        if (!_noBrokerageIsSet) {
            _noBrokerageIsSet = true;
            _noBrokerageInitVal = getNoBrokerage();
        }
        registerChange("no brokerage", _noBrokerageInitVal, noBrokerage);
        _noBrokerage = noBrokerage;
    }
                        
    /**
     * Returns the property 'no brokerage'.
     *
     * 
     *
     */
    public Boolean getNoBrokerage() {
        return _noBrokerage;
    }
                                    /**
     * Setter for the property 'user data sharing'.
     *
     * 
     *
     */
    public void setUserDataSharing(Boolean userDataSharing) {
        if (!_userDataSharingIsSet) {
            _userDataSharingIsSet = true;
            _userDataSharingInitVal = getUserDataSharing();
        }
        registerChange("user data sharing", _userDataSharingInitVal, userDataSharing);
        _userDataSharing = userDataSharing;
    }
                        
    /**
     * Returns the property 'user data sharing'.
     *
     * 
     *
     */
    public Boolean getUserDataSharing() {
        return _userDataSharing;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_publication instanceof de.smava.webapp.commons.domain.Entity && !_publication.getChangeSet().isEmpty()) {
             for (String element : _publication.getChangeSet()) {
                 result.add("publication : " + element);
             }
         }

         if (_campaign instanceof de.smava.webapp.commons.domain.Entity && !_campaign.getChangeSet().isEmpty()) {
             for (String element : _campaign.getChangeSet()) {
                 result.add("campaign : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingPlacement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _quantity=").append(_quantity);
            builder.append("\n    _position=").append(_position);
            builder.append("\n    _comment=").append(_comment);
            builder.append("\n    _alternateCode=").append(_alternateCode);
            builder.append("\n}");
        } else {
            builder.append(MarketingPlacement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingPlacement asMarketingPlacement() {
        return this;
    }
}
