//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(pap affiliate partner)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.marketing.domain.PapAffiliatePartner;
import de.smava.webapp.applicant.marketing.domain.interfaces.PapAffiliatePartnerEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PapAffiliatePartners'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractPapAffiliatePartner
    extends BrokerageEntity    implements PapAffiliatePartnerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractPapAffiliatePartner.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof PapAffiliatePartner)) {
            return;
        }
        
        this.setAffiliateId(((PapAffiliatePartner) oldEntity).getAffiliateId());    
        this.setPartnerName(((PapAffiliatePartner) oldEntity).getPartnerName());    
        this.setType(((PapAffiliatePartner) oldEntity).getType());    
        this.setNoOfferMails(((PapAffiliatePartner) oldEntity).getNoOfferMails());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof PapAffiliatePartner)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getAffiliateId(), ((PapAffiliatePartner) otherEntity).getAffiliateId());    
        equals = equals && valuesAreEqual(this.getPartnerName(), ((PapAffiliatePartner) otherEntity).getPartnerName());    
        equals = equals && valuesAreEqual(this.getType(), ((PapAffiliatePartner) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getNoOfferMails(), ((PapAffiliatePartner) otherEntity).getNoOfferMails());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(pap affiliate partner)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

