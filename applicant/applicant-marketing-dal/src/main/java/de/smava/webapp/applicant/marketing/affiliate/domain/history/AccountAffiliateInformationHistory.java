package de.smava.webapp.applicant.marketing.affiliate.domain.history;



import de.smava.webapp.applicant.marketing.affiliate.domain.abstracts.AbstractAccountAffiliateInformation;




/**
 * The domain object that has all history aggregation related fields for 'AccountAffiliateInformations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AccountAffiliateInformationHistory extends AbstractAccountAffiliateInformation {



			
}
