//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing publication)}
import de.smava.webapp.applicant.marketing.domain.history.MarketingPublicationHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPublications'.
 *
 * 
 *
 * @author generator
 */
public class MarketingPublication extends MarketingPublicationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing publication)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected String _name;
        protected String _type;
        protected String _range;
        protected MarketingPartner _marketingPartner;
        protected Set<MarketingPlacement> _placements;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'range'.
     *
     * 
     *
     */
    public void setRange(String range) {
        if (!_rangeIsSet) {
            _rangeIsSet = true;
            _rangeInitVal = getRange();
        }
        registerChange("range", _rangeInitVal, range);
        _range = range;
    }
                        
    /**
     * Returns the property 'range'.
     *
     * 
     *
     */
    public String getRange() {
        return _range;
    }
                                            
    /**
     * Setter for the property 'marketing partner'.
     *
     * 
     *
     */
    public void setMarketingPartner(MarketingPartner marketingPartner) {
        _marketingPartner = marketingPartner;
    }
            
    /**
     * Returns the property 'marketing partner'.
     *
     * 
     *
     */
    public MarketingPartner getMarketingPartner() {
        return _marketingPartner;
    }
                                            
    /**
     * Setter for the property 'placements'.
     *
     * 
     *
     */
    public void setPlacements(Set<MarketingPlacement> placements) {
        _placements = placements;
    }
            
    /**
     * Returns the property 'placements'.
     *
     * 
     *
     */
    public Set<MarketingPlacement> getPlacements() {
        return _placements;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_marketingPartner instanceof de.smava.webapp.commons.domain.Entity && !_marketingPartner.getChangeSet().isEmpty()) {
             for (String element : _marketingPartner.getChangeSet()) {
                 result.add("marketing partner : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingPublication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _range=").append(_range);
            builder.append("\n}");
        } else {
            builder.append(MarketingPublication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingPublication asMarketingPublication() {
        return this;
    }
}
