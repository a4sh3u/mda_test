package de.smava.webapp.applicant.marketing.affiliate.domain.interfaces;



import de.smava.webapp.applicant.marketing.affiliate.domain.LoanApplicationAffiliateInformation;


/**
 * The domain object that represents 'LoanApplicationAffiliateInformations'.
 *
 * @author generator
 */
public interface LoanApplicationAffiliateInformationEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication();
    /**
     * Helper method to get reference of this object as model type.
     */
    LoanApplicationAffiliateInformation asLoanApplicationAffiliateInformation();
}
