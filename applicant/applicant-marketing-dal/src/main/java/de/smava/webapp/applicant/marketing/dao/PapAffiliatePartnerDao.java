//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(pap affiliate partner)}

import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.applicant.marketing.domain.PapAffiliatePartner;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'PapAffiliatePartners'.
 *
 * @author generator
 */
public interface PapAffiliatePartnerDao extends BrokerageSchemaDao<PapAffiliatePartner> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the pap affiliate partner identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    PapAffiliatePartner getPapAffiliatePartner(Long id);

    /**
     * Saves the pap affiliate partner.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long savePapAffiliatePartner(PapAffiliatePartner papAffiliatePartner);

    /**
     * Deletes an pap affiliate partner, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the pap affiliate partner
     */
    void deletePapAffiliatePartner(Long id);

    /**
     * Retrieves all 'PapAffiliatePartner' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<PapAffiliatePartner> getPapAffiliatePartnerList();

    /**
     * Retrieves a page of 'PapAffiliatePartner' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<PapAffiliatePartner> getPapAffiliatePartnerList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'PapAffiliatePartner' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<PapAffiliatePartner> getPapAffiliatePartnerList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'PapAffiliatePartner' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<PapAffiliatePartner> getPapAffiliatePartnerList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'PapAffiliatePartner' instances.
     */
    long getPapAffiliatePartnerCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(pap affiliate partner)}
    //
    // insert custom methods here

    PapAffiliatePartner getByAffiliateId(String id);

    Collection<String> getAllOfflineAgentAffiliateIds();
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
