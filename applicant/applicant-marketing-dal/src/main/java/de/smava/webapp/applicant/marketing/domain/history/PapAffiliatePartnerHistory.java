package de.smava.webapp.applicant.marketing.domain.history;



import de.smava.webapp.applicant.marketing.domain.abstracts.AbstractPapAffiliatePartner;




/**
 * The domain object that has all history aggregation related fields for 'PapAffiliatePartners'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class PapAffiliatePartnerHistory extends AbstractPapAffiliatePartner {

    protected transient String _affiliateIdInitVal;
    protected transient boolean _affiliateIdIsSet;
    protected transient String _partnerNameInitVal;
    protected transient boolean _partnerNameIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient Boolean _noOfferMailsInitVal;
    protected transient boolean _noOfferMailsIsSet;


	
    /**
     * Returns the initial value of the property 'affiliate id'.
     */
    public String affiliateIdInitVal() {
        String result;
        if (_affiliateIdIsSet) {
            result = _affiliateIdInitVal;
        } else {
            result = getAffiliateId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'affiliate id'.
     */
    public boolean affiliateIdIsDirty() {
        return !valuesAreEqual(affiliateIdInitVal(), getAffiliateId());
    }

    /**
     * Returns true if the setter method was called for the property 'affiliate id'.
     */
    public boolean affiliateIdIsSet() {
        return _affiliateIdIsSet;
    }
	
    /**
     * Returns the initial value of the property 'partner name'.
     */
    public String partnerNameInitVal() {
        String result;
        if (_partnerNameIsSet) {
            result = _partnerNameInitVal;
        } else {
            result = getPartnerName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'partner name'.
     */
    public boolean partnerNameIsDirty() {
        return !valuesAreEqual(partnerNameInitVal(), getPartnerName());
    }

    /**
     * Returns true if the setter method was called for the property 'partner name'.
     */
    public boolean partnerNameIsSet() {
        return _partnerNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'no offer mails'.
     */
    public Boolean noOfferMailsInitVal() {
        Boolean result;
        if (_noOfferMailsIsSet) {
            result = _noOfferMailsInitVal;
        } else {
            result = getNoOfferMails();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'no offer mails'.
     */
    public boolean noOfferMailsIsDirty() {
        return !valuesAreEqual(noOfferMailsInitVal(), getNoOfferMails());
    }

    /**
     * Returns true if the setter method was called for the property 'no offer mails'.
     */
    public boolean noOfferMailsIsSet() {
        return _noOfferMailsIsSet;
    }

}
