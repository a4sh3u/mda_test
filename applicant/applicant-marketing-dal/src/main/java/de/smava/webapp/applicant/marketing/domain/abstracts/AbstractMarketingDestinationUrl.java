//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.marketing.domain.MarketingDestinationUrl;
import de.smava.webapp.applicant.marketing.domain.interfaces.MarketingDestinationUrlEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingDestinationUrls'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingDestinationUrl
    extends BrokerageEntity    implements MarketingDestinationUrlEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingDestinationUrl.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingDestinationUrl)) {
            return;
        }
        
        this.setUrl(((MarketingDestinationUrl) oldEntity).getUrl());    
        this.setMarketingDestinationUrlPlacements(((MarketingDestinationUrl) oldEntity).getMarketingDestinationUrlPlacements());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingDestinationUrl)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getUrl(), ((MarketingDestinationUrl) otherEntity).getUrl());    
        equals = equals && valuesAreEqual(this.getMarketingDestinationUrlPlacements(), ((MarketingDestinationUrl) otherEntity).getMarketingDestinationUrlPlacements());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing destination url)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

