package de.smava.webapp.applicant.marketing.domain.interfaces;



import de.smava.webapp.applicant.marketing.domain.MarketingPartner;
import de.smava.webapp.applicant.marketing.domain.MarketingPlacement;
import de.smava.webapp.applicant.marketing.domain.MarketingPublication;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'MarketingPublications'.
 *
 * @author generator
 */
public interface MarketingPublicationEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'range'.
     *
     * 
     *
     */
    void setRange(String range);

    /**
     * Returns the property 'range'.
     *
     * 
     *
     */
    String getRange();
    /**
     * Setter for the property 'marketing partner'.
     *
     * 
     *
     */
    void setMarketingPartner(MarketingPartner marketingPartner);

    /**
     * Returns the property 'marketing partner'.
     *
     * 
     *
     */
    MarketingPartner getMarketingPartner();
    /**
     * Setter for the property 'placements'.
     *
     * 
     *
     */
    void setPlacements(Set<MarketingPlacement> placements);

    /**
     * Returns the property 'placements'.
     *
     * 
     *
     */
    Set<MarketingPlacement> getPlacements();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingPublication asMarketingPublication();
}
