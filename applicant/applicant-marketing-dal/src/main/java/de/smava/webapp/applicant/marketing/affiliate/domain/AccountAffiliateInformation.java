//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.affiliate.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account affiliate information)}
import de.smava.webapp.applicant.marketing.affiliate.domain.history.AccountAffiliateInformationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AccountAffiliateInformations'.
 *
 * 
 *
 * @author generator
 */
public class AccountAffiliateInformation extends AccountAffiliateInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(account affiliate information)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected AffiliateInformation _firstAffiliate;
        protected AffiliateInformation _saleAffiliate;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'first affiliate'.
     *
     * 
     *
     */
    public void setFirstAffiliate(AffiliateInformation firstAffiliate) {
        _firstAffiliate = firstAffiliate;
    }
            
    /**
     * Returns the property 'first affiliate'.
     *
     * 
     *
     */
    public AffiliateInformation getFirstAffiliate() {
        return _firstAffiliate;
    }
                                            
    /**
     * Setter for the property 'sale affiliate'.
     *
     * 
     *
     */
    public void setSaleAffiliate(AffiliateInformation saleAffiliate) {
        _saleAffiliate = saleAffiliate;
    }
            
    /**
     * Returns the property 'sale affiliate'.
     *
     * 
     *
     */
    public AffiliateInformation getSaleAffiliate() {
        return _saleAffiliate;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_firstAffiliate instanceof de.smava.webapp.commons.domain.Entity && !_firstAffiliate.getChangeSet().isEmpty()) {
             for (String element : _firstAffiliate.getChangeSet()) {
                 result.add("first affiliate : " + element);
             }
         }

         if (_saleAffiliate instanceof de.smava.webapp.commons.domain.Entity && !_saleAffiliate.getChangeSet().isEmpty()) {
             for (String element : _saleAffiliate.getChangeSet()) {
                 result.add("sale affiliate : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AccountAffiliateInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AccountAffiliateInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AccountAffiliateInformation asAccountAffiliateInformation() {
        return this;
    }
}
