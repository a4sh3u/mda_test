//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing destination url)}
import de.smava.webapp.applicant.marketing.domain.history.MarketingDestinationUrlHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingDestinationUrls'.
 *
 * 
 *
 * @author generator
 */
public class MarketingDestinationUrl extends MarketingDestinationUrlHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing destination url)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _url;
        protected Collection<MarketingDestinationUrlPlacement> _marketingDestinationUrlPlacements;
        
                            /**
     * Setter for the property 'url'.
     *
     * 
     *
     */
    public void setUrl(String url) {
        if (!_urlIsSet) {
            _urlIsSet = true;
            _urlInitVal = getUrl();
        }
        registerChange("url", _urlInitVal, url);
        _url = url;
    }
                        
    /**
     * Returns the property 'url'.
     *
     * 
     *
     */
    public String getUrl() {
        return _url;
    }
                                            
    /**
     * Setter for the property 'marketing destination url placements'.
     *
     * 
     *
     */
    public void setMarketingDestinationUrlPlacements(Collection<MarketingDestinationUrlPlacement> marketingDestinationUrlPlacements) {
        _marketingDestinationUrlPlacements = marketingDestinationUrlPlacements;
    }
            
    /**
     * Returns the property 'marketing destination url placements'.
     *
     * 
     *
     */
    public Collection<MarketingDestinationUrlPlacement> getMarketingDestinationUrlPlacements() {
        return _marketingDestinationUrlPlacements;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingDestinationUrl.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _url=").append(_url);
            builder.append("\n}");
        } else {
            builder.append(MarketingDestinationUrl.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingDestinationUrl asMarketingDestinationUrl() {
        return this;
    }
}
