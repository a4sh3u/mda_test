//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing publication)}
import de.smava.webapp.applicant.marketing.domain.MarketingPublication;
import de.smava.webapp.applicant.marketing.domain.interfaces.MarketingPublicationEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPublications'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingPublication
    extends BrokerageEntity    implements MarketingPublicationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingPublication.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingPublication)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setName(((MarketingPublication) oldEntity).getName());    
        this.setType(((MarketingPublication) oldEntity).getType());    
        this.setRange(((MarketingPublication) oldEntity).getRange());    
        this.setMarketingPartner(((MarketingPublication) oldEntity).getMarketingPartner());    
        this.setPlacements(((MarketingPublication) oldEntity).getPlacements());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingPublication)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getName(), ((MarketingPublication) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getType(), ((MarketingPublication) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getRange(), ((MarketingPublication) otherEntity).getRange());    
        equals = equals && valuesAreEqual(this.getMarketingPartner(), ((MarketingPublication) otherEntity).getMarketingPartner());    
        equals = equals && valuesAreEqual(this.getPlacements(), ((MarketingPublication) otherEntity).getPlacements());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing publication)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

