//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact placement)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.marketing.domain.MarketingContactPlacement;
import de.smava.webapp.applicant.marketing.domain.interfaces.MarketingContactPlacementEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingContactPlacements'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingContactPlacement
    extends BrokerageEntity    implements MarketingContactPlacementEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingContactPlacement.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingContactPlacement)) {
            return;
        }
        
        this.setMarketingPlacement(((MarketingContactPlacement) oldEntity).getMarketingPlacement());    
        this.setMarketingContact(((MarketingContactPlacement) oldEntity).getMarketingContact());    
        this.setType(((MarketingContactPlacement) oldEntity).getType());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingContactPlacement)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getMarketingPlacement(), ((MarketingContactPlacement) otherEntity).getMarketingPlacement());    
        equals = equals && valuesAreEqual(this.getMarketingContact(), ((MarketingContactPlacement) otherEntity).getMarketingContact());    
        equals = equals && valuesAreEqual(this.getType(), ((MarketingContactPlacement) otherEntity).getType());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing contact placement)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

