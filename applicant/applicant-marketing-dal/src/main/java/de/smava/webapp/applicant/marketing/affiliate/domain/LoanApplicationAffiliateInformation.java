//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.affiliate.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan application affiliate information)}
import de.smava.webapp.applicant.marketing.affiliate.domain.history.LoanApplicationAffiliateInformationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanApplicationAffiliateInformations'.
 *
 * 
 *
 * @author generator
 */
public class LoanApplicationAffiliateInformation extends LoanApplicationAffiliateInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(loan application affiliate information)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected de.smava.webapp.applicant.brokerage.domain.LoanApplication _loanApplication;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_loanApplication instanceof de.smava.webapp.commons.domain.Entity && !_loanApplication.getChangeSet().isEmpty()) {
             for (String element : _loanApplication.getChangeSet()) {
                 result.add("loan application : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LoanApplicationAffiliateInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(LoanApplicationAffiliateInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LoanApplicationAffiliateInformation asLoanApplicationAffiliateInformation() {
        return this;
    }
}
