//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing partner)}
import de.smava.webapp.applicant.marketing.domain.history.MarketingPartnerHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPartners'.
 *
 * 
 *
 * @author generator
 */
public class MarketingPartner extends MarketingPartnerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing partner)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected String _state;
        protected String _name;
        protected String _image;
        protected int _imageWidth;
        protected int _imageHeight;
        protected List<MarketingPublication> _publications;
        protected Collection<MarketingContact> _contacts;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'image'.
     *
     * 
     *
     */
    public void setImage(String image) {
        if (!_imageIsSet) {
            _imageIsSet = true;
            _imageInitVal = getImage();
        }
        registerChange("image", _imageInitVal, image);
        _image = image;
    }
                        
    /**
     * Returns the property 'image'.
     *
     * 
     *
     */
    public String getImage() {
        return _image;
    }
                                    /**
     * Setter for the property 'image width'.
     *
     * 
     *
     */
    public void setImageWidth(int imageWidth) {
        if (!_imageWidthIsSet) {
            _imageWidthIsSet = true;
            _imageWidthInitVal = getImageWidth();
        }
        registerChange("image width", _imageWidthInitVal, imageWidth);
        _imageWidth = imageWidth;
    }
                        
    /**
     * Returns the property 'image width'.
     *
     * 
     *
     */
    public int getImageWidth() {
        return _imageWidth;
    }
                                    /**
     * Setter for the property 'image height'.
     *
     * 
     *
     */
    public void setImageHeight(int imageHeight) {
        if (!_imageHeightIsSet) {
            _imageHeightIsSet = true;
            _imageHeightInitVal = getImageHeight();
        }
        registerChange("image height", _imageHeightInitVal, imageHeight);
        _imageHeight = imageHeight;
    }
                        
    /**
     * Returns the property 'image height'.
     *
     * 
     *
     */
    public int getImageHeight() {
        return _imageHeight;
    }
                                            
    /**
     * Setter for the property 'publications'.
     *
     * 
     *
     */
    public void setPublications(List<MarketingPublication> publications) {
        _publications = publications;
    }
            
    /**
     * Returns the property 'publications'.
     *
     * 
     *
     */
    public List<MarketingPublication> getPublications() {
        return _publications;
    }
                                            
    /**
     * Setter for the property 'contacts'.
     *
     * 
     *
     */
    public void setContacts(Collection<MarketingContact> contacts) {
        _contacts = contacts;
    }
            
    /**
     * Returns the property 'contacts'.
     *
     * 
     *
     */
    public Collection<MarketingContact> getContacts() {
        return _contacts;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingPartner.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _image=").append(_image);
            builder.append("\n    _imageWidth=").append(_imageWidth);
            builder.append("\n    _imageHeight=").append(_imageHeight);
            builder.append("\n}");
        } else {
            builder.append(MarketingPartner.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingPartner asMarketingPartner() {
        return this;
    }
}
