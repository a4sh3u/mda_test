//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing partner)}
import de.smava.webapp.applicant.marketing.domain.MarketingPartner;
import de.smava.webapp.applicant.marketing.domain.interfaces.MarketingPartnerEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

                                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPartners'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingPartner
    extends BrokerageEntity    implements MarketingPartnerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingPartner.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingPartner)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setState(((MarketingPartner) oldEntity).getState());    
        this.setName(((MarketingPartner) oldEntity).getName());    
        this.setImage(((MarketingPartner) oldEntity).getImage());    
        this.setImageWidth(((MarketingPartner) oldEntity).getImageWidth());    
        this.setImageHeight(((MarketingPartner) oldEntity).getImageHeight());    
        this.setPublications(((MarketingPartner) oldEntity).getPublications());    
        this.setContacts(((MarketingPartner) oldEntity).getContacts());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingPartner)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getState(), ((MarketingPartner) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getName(), ((MarketingPartner) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getImage(), ((MarketingPartner) otherEntity).getImage());    
        equals = equals && valuesAreEqual(this.getImageWidth(), ((MarketingPartner) otherEntity).getImageWidth());    
        equals = equals && valuesAreEqual(this.getImageHeight(), ((MarketingPartner) otherEntity).getImageHeight());    
        equals = equals && valuesAreEqual(this.getPublications(), ((MarketingPartner) otherEntity).getPublications());    
        equals = equals && valuesAreEqual(this.getContacts(), ((MarketingPartner) otherEntity).getContacts());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing partner)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

