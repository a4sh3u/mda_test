package de.smava.webapp.applicant.marketing.domain.interfaces;



import de.smava.webapp.applicant.marketing.domain.MarketingContact;
import de.smava.webapp.applicant.marketing.domain.MarketingContactPlacement;
import de.smava.webapp.applicant.marketing.domain.MarketingPartner;

import java.util.Collection;


/**
 * The domain object that represents 'MarketingContacts'.
 *
 * @author generator
 */
public interface MarketingContactEntityInterface {

    /**
     * Setter for the property 'marketing partner'.
     *
     * 
     *
     */
    void setMarketingPartner(MarketingPartner marketingPartner);

    /**
     * Returns the property 'marketing partner'.
     *
     * 
     *
     */
    MarketingPartner getMarketingPartner();
    /**
     * Setter for the property 'marketing contact placements'.
     *
     * 
     *
     */
    void setMarketingContactPlacements(Collection<MarketingContactPlacement> marketingContactPlacements);

    /**
     * Returns the property 'marketing contact placements'.
     *
     * 
     *
     */
    Collection<MarketingContactPlacement> getMarketingContactPlacements();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'is agent admin'.
     *
     * 
     *
     */
    void setIsAgentAdmin(Boolean isAgentAdmin);

    /**
     * Returns the property 'is agent admin'.
     *
     * 
     *
     */
    Boolean getIsAgentAdmin();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingContact asMarketingContact();
}
