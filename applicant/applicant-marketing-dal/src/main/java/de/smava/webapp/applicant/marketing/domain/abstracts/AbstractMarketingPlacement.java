//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement)}
import de.smava.webapp.applicant.marketing.domain.MarketingPlacement;
import de.smava.webapp.applicant.marketing.domain.interfaces.MarketingPlacementEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

                                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPlacements'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingPlacement
    extends BrokerageEntity    implements MarketingPlacementEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingPlacement.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingPlacement)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setQuantity(((MarketingPlacement) oldEntity).getQuantity());    
        this.setPosition(((MarketingPlacement) oldEntity).getPosition());    
        this.setComment(((MarketingPlacement) oldEntity).getComment());    
        this.setPublication(((MarketingPlacement) oldEntity).getPublication());    
        this.setCampaign(((MarketingPlacement) oldEntity).getCampaign());    
        this.setMarketingDestinationUrlPlacements(((MarketingPlacement) oldEntity).getMarketingDestinationUrlPlacements());    
        this.setMarketingContactPlacements(((MarketingPlacement) oldEntity).getMarketingContactPlacements());    
        this.setAlternateCode(((MarketingPlacement) oldEntity).getAlternateCode());    
        this.setNoBrokerage(((MarketingPlacement) oldEntity).getNoBrokerage());    
        this.setUserDataSharing(((MarketingPlacement) oldEntity).getUserDataSharing());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingPlacement)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getQuantity(), ((MarketingPlacement) otherEntity).getQuantity());    
        equals = equals && valuesAreEqual(this.getPosition(), ((MarketingPlacement) otherEntity).getPosition());    
        equals = equals && valuesAreEqual(this.getComment(), ((MarketingPlacement) otherEntity).getComment());    
        equals = equals && valuesAreEqual(this.getPublication(), ((MarketingPlacement) otherEntity).getPublication());    
        equals = equals && valuesAreEqual(this.getCampaign(), ((MarketingPlacement) otherEntity).getCampaign());    
        equals = equals && valuesAreEqual(this.getMarketingDestinationUrlPlacements(), ((MarketingPlacement) otherEntity).getMarketingDestinationUrlPlacements());    
        equals = equals && valuesAreEqual(this.getMarketingContactPlacements(), ((MarketingPlacement) otherEntity).getMarketingContactPlacements());    
        equals = equals && valuesAreEqual(this.getAlternateCode(), ((MarketingPlacement) otherEntity).getAlternateCode());    
        equals = equals && valuesAreEqual(this.getNoBrokerage(), ((MarketingPlacement) otherEntity).getNoBrokerage());    
        equals = equals && valuesAreEqual(this.getUserDataSharing(), ((MarketingPlacement) otherEntity).getUserDataSharing());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing placement)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

