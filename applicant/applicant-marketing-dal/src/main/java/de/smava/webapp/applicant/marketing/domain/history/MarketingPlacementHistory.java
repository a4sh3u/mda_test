package de.smava.webapp.applicant.marketing.domain.history;



import de.smava.webapp.applicant.marketing.domain.abstracts.AbstractMarketingPlacement;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'MarketingPlacements'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class MarketingPlacementHistory extends AbstractMarketingPlacement {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient int _quantityInitVal;
    protected transient boolean _quantityIsSet;
    protected transient String _positionInitVal;
    protected transient boolean _positionIsSet;
    protected transient String _commentInitVal;
    protected transient boolean _commentIsSet;
    protected transient String _alternateCodeInitVal;
    protected transient boolean _alternateCodeIsSet;
    protected transient Boolean _noBrokerageInitVal;
    protected transient boolean _noBrokerageIsSet;
    protected transient Boolean _userDataSharingInitVal;
    protected transient boolean _userDataSharingIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'quantity'.
     */
    public int quantityInitVal() {
        int result;
        if (_quantityIsSet) {
            result = _quantityInitVal;
        } else {
            result = getQuantity();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'quantity'.
     */
    public boolean quantityIsDirty() {
        return !valuesAreEqual(quantityInitVal(), getQuantity());
    }

    /**
     * Returns true if the setter method was called for the property 'quantity'.
     */
    public boolean quantityIsSet() {
        return _quantityIsSet;
    }
	
    /**
     * Returns the initial value of the property 'position'.
     */
    public String positionInitVal() {
        String result;
        if (_positionIsSet) {
            result = _positionInitVal;
        } else {
            result = getPosition();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'position'.
     */
    public boolean positionIsDirty() {
        return !valuesAreEqual(positionInitVal(), getPosition());
    }

    /**
     * Returns true if the setter method was called for the property 'position'.
     */
    public boolean positionIsSet() {
        return _positionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'comment'.
     */
    public String commentInitVal() {
        String result;
        if (_commentIsSet) {
            result = _commentInitVal;
        } else {
            result = getComment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comment'.
     */
    public boolean commentIsDirty() {
        return !valuesAreEqual(commentInitVal(), getComment());
    }

    /**
     * Returns true if the setter method was called for the property 'comment'.
     */
    public boolean commentIsSet() {
        return _commentIsSet;
    }
					
    /**
     * Returns the initial value of the property 'alternate code'.
     */
    public String alternateCodeInitVal() {
        String result;
        if (_alternateCodeIsSet) {
            result = _alternateCodeInitVal;
        } else {
            result = getAlternateCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'alternate code'.
     */
    public boolean alternateCodeIsDirty() {
        return !valuesAreEqual(alternateCodeInitVal(), getAlternateCode());
    }

    /**
     * Returns true if the setter method was called for the property 'alternate code'.
     */
    public boolean alternateCodeIsSet() {
        return _alternateCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'no brokerage.
     */
    public Boolean noBrokerageInitVal() {
        Boolean result;
        if (_noBrokerageIsSet) {
            result = _noBrokerageInitVal;
        } else {
            result = getNoBrokerage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'no brokerage.
     */
    public boolean noBrokerageIsDirty() {
        return !valuesAreEqual(noBrokerageInitVal(), getNoBrokerage());
    }

    /**
     * Returns true if the setter method was called for the property 'no brokerage.
     */
    public boolean noBrokerageIsSet() {
        return _noBrokerageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'user data sharing'.
     */
    public Boolean userDataSharingInitVal() {
        Boolean result;
        if (_userDataSharingIsSet) {
            result = _userDataSharingInitVal;
        } else {
            result = getUserDataSharing();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'user data sharing'.
     */
    public boolean userDataSharingIsDirty() {
        return !valuesAreEqual(userDataSharingInitVal(), getUserDataSharing());
    }

    /**
     * Returns true if the setter method was called for the property 'user data sharing'.
     */
    public boolean userDataSharingIsSet() {
        return _userDataSharingIsSet;
    }

}
