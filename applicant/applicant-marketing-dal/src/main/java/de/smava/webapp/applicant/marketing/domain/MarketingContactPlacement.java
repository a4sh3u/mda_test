//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact placement)}
import de.smava.webapp.applicant.marketing.domain.history.MarketingContactPlacementHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingContactPlacements'.
 *
 * 
 *
 * @author generator
 */
public class MarketingContactPlacement extends MarketingContactPlacementHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing contact placement)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected MarketingPlacement _marketingPlacement;
        protected MarketingContact _marketingContact;
        protected String _type;
        
                                    
    /**
     * Setter for the property 'marketing placement'.
     *
     * 
     *
     */
    public void setMarketingPlacement(MarketingPlacement marketingPlacement) {
        _marketingPlacement = marketingPlacement;
    }
            
    /**
     * Returns the property 'marketing placement'.
     *
     * 
     *
     */
    public MarketingPlacement getMarketingPlacement() {
        return _marketingPlacement;
    }
                                            
    /**
     * Setter for the property 'marketing contact'.
     *
     * 
     *
     */
    public void setMarketingContact(MarketingContact marketingContact) {
        _marketingContact = marketingContact;
    }
            
    /**
     * Returns the property 'marketing contact'.
     *
     * 
     *
     */
    public MarketingContact getMarketingContact() {
        return _marketingContact;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_marketingPlacement instanceof de.smava.webapp.commons.domain.Entity && !_marketingPlacement.getChangeSet().isEmpty()) {
             for (String element : _marketingPlacement.getChangeSet()) {
                 result.add("marketing placement : " + element);
             }
         }

         if (_marketingContact instanceof de.smava.webapp.commons.domain.Entity && !_marketingContact.getChangeSet().isEmpty()) {
             for (String element : _marketingContact.getChangeSet()) {
                 result.add("marketing contact : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingContactPlacement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(MarketingContactPlacement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingContactPlacement asMarketingContactPlacement() {
        return this;
    }
}
