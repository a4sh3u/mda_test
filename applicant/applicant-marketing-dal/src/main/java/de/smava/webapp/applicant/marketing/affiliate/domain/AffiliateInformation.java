//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.affiliate.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(affiliate information)}
import de.smava.webapp.applicant.marketing.affiliate.domain.history.AffiliateInformationHistory;
import de.smava.webapp.commons.Validation;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AffiliateInformations'.
 *
 * 
 *
 * @author generator
 */
public class AffiliateInformation extends AffiliateInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(affiliate information)}

    /**
     * Setter for the property 'external affiliate id'.
     */
    public void setExternalAffiliateId(String externalAffiliateId) {
        if (!_externalAffiliateIdIsSet) {
            _externalAffiliateIdIsSet = true;
            _externalAffiliateIdInitVal = getExternalAffiliateId();
        }

        registerChange("external affiliate id", _externalAffiliateIdInitVal, externalAffiliateId);

        if (externalAffiliateId != null) {
            _externalAffiliateId = encode(externalAffiliateId);
            _externalAffiliateId = Validation.validate(MAX_AFFILIATE_CHARS, _externalAffiliateId);
        } else {
            _externalAffiliateChannel = externalAffiliateId;
        }
    }

    /**
     * Returns the property 'external affiliate id'.
     */
    public String getExternalAffiliateId() {
        if (_externalAffiliateId == null) {
            return null;
        }
        return decode(_externalAffiliateId);
    }

    /**
    * Setter for the property 'external affiliate sub id'.
    */
    public void setExternalAffiliateSubId(String externalAffiliateSubId) {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }

    /**
    * Returns the property 'external affiliate sub id'.
    *
    * 
    *
    */
    public String getExternalAffiliateSubId() {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }
                                    /**
    * Setter for the property 'external affiliate channel'.
    */
    public void setExternalAffiliateChannel(String externalAffiliateChannel) {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }

    /**
    * Returns the property 'external affiliate channel'.
    *
    * 
    *
    */
    public String getExternalAffiliateChannel() {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _expirationDate;
        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected de.smava.webapp.applicant.marketing.domain.MarketingPlacement _marketingPlacement;
        protected String _externalAffiliateId;
        protected String _externalAffiliateSubId;
        protected String _externalAffiliateChannel;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    public void setExpirationDate(Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'marketing placement'.
     *
     * 
     *
     */
    public void setMarketingPlacement(de.smava.webapp.applicant.marketing.domain.MarketingPlacement marketingPlacement) {
        _marketingPlacement = marketingPlacement;
    }
            
    /**
     * Returns the property 'marketing placement'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.marketing.domain.MarketingPlacement getMarketingPlacement() {
        return _marketingPlacement;
    }
                                                
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_marketingPlacement instanceof de.smava.webapp.commons.domain.Entity && !_marketingPlacement.getChangeSet().isEmpty()) {
             for (String element : _marketingPlacement.getChangeSet()) {
                 result.add("marketing placement : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AffiliateInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _externalAffiliateId=").append(_externalAffiliateId);
            builder.append("\n    _externalAffiliateSubId=").append(_externalAffiliateSubId);
            builder.append("\n    _externalAffiliateChannel=").append(_externalAffiliateChannel);
            builder.append("\n}");
        } else {
            builder.append(AffiliateInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AffiliateInformation asAffiliateInformation() {
        return this;
    }
}
