//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(pap affiliate partner)}
import de.smava.webapp.applicant.marketing.domain.history.PapAffiliatePartnerHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PapAffiliatePartners'.
 *
 * 
 *
 * @author generator
 */
public class PapAffiliatePartner extends PapAffiliatePartnerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(pap affiliate partner)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _affiliateId;
        protected String _partnerName;
        protected String _type;
        protected Boolean _noOfferMails;
        
                            /**
     * Setter for the property 'affiliate id'.
     *
     * 
     *
     */
    public void setAffiliateId(String affiliateId) {
        if (!_affiliateIdIsSet) {
            _affiliateIdIsSet = true;
            _affiliateIdInitVal = getAffiliateId();
        }
        registerChange("affiliate id", _affiliateIdInitVal, affiliateId);
        _affiliateId = affiliateId;
    }
                        
    /**
     * Returns the property 'affiliate id'.
     *
     * 
     *
     */
    public String getAffiliateId() {
        return _affiliateId;
    }
                                    /**
     * Setter for the property 'partner name'.
     *
     * 
     *
     */
    public void setPartnerName(String partnerName) {
        if (!_partnerNameIsSet) {
            _partnerNameIsSet = true;
            _partnerNameInitVal = getPartnerName();
        }
        registerChange("partner name", _partnerNameInitVal, partnerName);
        _partnerName = partnerName;
    }
                        
    /**
     * Returns the property 'partner name'.
     *
     * 
     *
     */
    public String getPartnerName() {
        return _partnerName;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'no offer mails'.
     *
     * 
     *
     */
    public void setNoOfferMails(Boolean noOfferMails) {
        if (!_noOfferMailsIsSet) {
            _noOfferMailsIsSet = true;
            _noOfferMailsInitVal = getNoOfferMails();
        }
        registerChange("no offer mails", _noOfferMailsInitVal, noOfferMails);
        _noOfferMails = noOfferMails;
    }
                        
    /**
     * Returns the property 'no offer mails'.
     *
     * 
     *
     */
    public Boolean getNoOfferMails() {
        return _noOfferMails;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(PapAffiliatePartner.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _affiliateId=").append(_affiliateId);
            builder.append("\n    _partnerName=").append(_partnerName);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(PapAffiliatePartner.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public PapAffiliatePartner asPapAffiliatePartner() {
        return this;
    }
}
