package de.smava.webapp.applicant.marketing.domain.interfaces;



import de.smava.webapp.applicant.marketing.domain.PapAffiliatePartner;


/**
 * The domain object that represents 'PapAffiliatePartners'.
 *
 * @author generator
 */
public interface PapAffiliatePartnerEntityInterface {

    /**
     * Setter for the property 'affiliate id'.
     *
     * 
     *
     */
    void setAffiliateId(String affiliateId);

    /**
     * Returns the property 'affiliate id'.
     *
     * 
     *
     */
    String getAffiliateId();
    /**
     * Setter for the property 'partner name'.
     *
     * 
     *
     */
    void setPartnerName(String partnerName);

    /**
     * Returns the property 'partner name'.
     *
     * 
     *
     */
    String getPartnerName();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'no offer mails'.
     *
     * 
     *
     */
    void setNoOfferMails(Boolean noOfferMails);

    /**
     * Returns the property 'no offer mails'.
     *
     * 
     *
     */
    Boolean getNoOfferMails();
    /**
     * Helper method to get reference of this object as model type.
     */
    PapAffiliatePartner asPapAffiliatePartner();
}
