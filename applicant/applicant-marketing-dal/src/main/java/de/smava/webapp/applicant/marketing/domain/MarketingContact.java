//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing contact)}
import de.smava.webapp.applicant.marketing.domain.history.MarketingContactHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingContacts'.
 *
 * 
 *
 * @author generator
 */
public class MarketingContact extends MarketingContactHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(marketing contact)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected MarketingPartner _marketingPartner;
        protected Collection<MarketingContactPlacement> _marketingContactPlacements;
        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected Boolean _isAgentAdmin;
        
                                    
    /**
     * Setter for the property 'marketing partner'.
     *
     * 
     *
     */
    public void setMarketingPartner(MarketingPartner marketingPartner) {
        _marketingPartner = marketingPartner;
    }
            
    /**
     * Returns the property 'marketing partner'.
     *
     * 
     *
     */
    public MarketingPartner getMarketingPartner() {
        return _marketingPartner;
    }
                                            
    /**
     * Setter for the property 'marketing contact placements'.
     *
     * 
     *
     */
    public void setMarketingContactPlacements(Collection<MarketingContactPlacement> marketingContactPlacements) {
        _marketingContactPlacements = marketingContactPlacements;
    }
            
    /**
     * Returns the property 'marketing contact placements'.
     *
     * 
     *
     */
    public Collection<MarketingContactPlacement> getMarketingContactPlacements() {
        return _marketingContactPlacements;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'is agent admin'.
     *
     * 
     *
     */
    public void setIsAgentAdmin(Boolean isAgentAdmin) {
        if (!_isAgentAdminIsSet) {
            _isAgentAdminIsSet = true;
            _isAgentAdminInitVal = getIsAgentAdmin();
        }
        registerChange("is agent admin", _isAgentAdminInitVal, isAgentAdmin);
        _isAgentAdmin = isAgentAdmin;
    }
                        
    /**
     * Returns the property 'is agent admin'.
     *
     * 
     *
     */
    public Boolean getIsAgentAdmin() {
        return _isAgentAdmin;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_marketingPartner instanceof de.smava.webapp.commons.domain.Entity && !_marketingPartner.getChangeSet().isEmpty()) {
             for (String element : _marketingPartner.getChangeSet()) {
                 result.add("marketing partner : " + element);
             }
         }

         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MarketingContact.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(MarketingContact.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MarketingContact asMarketingContact() {
        return this;
    }
}
