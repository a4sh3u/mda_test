package de.smava.webapp.applicant.marketing.affiliate.domain.interfaces;



import de.smava.webapp.applicant.marketing.affiliate.domain.AffiliateInformation;

import java.util.Date;


/**
 * The domain object that represents 'AffiliateInformations'.
 *
 * @author generator
 */
public interface AffiliateInformationEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    Date getExpirationDate();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'marketing placement'.
     *
     * 
     *
     */
    void setMarketingPlacement(de.smava.webapp.applicant.marketing.domain.MarketingPlacement marketingPlacement);

    /**
     * Returns the property 'marketing placement'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.marketing.domain.MarketingPlacement getMarketingPlacement();
    /**
     * Setter for the property 'external affiliate id'.
     *
     * 
     *
     */
    void setExternalAffiliateId(String externalAffiliateId);

    /**
     * Returns the property 'external affiliate id'.
     *
     * 
     *
     */
    String getExternalAffiliateId();
    /**
     * Setter for the property 'external affiliate sub id'.
     *
     * 
     *
     */
    void setExternalAffiliateSubId(String externalAffiliateSubId);

    /**
     * Returns the property 'external affiliate sub id'.
     *
     * 
     *
     */
    String getExternalAffiliateSubId();
    /**
     * Setter for the property 'external affiliate channel'.
     *
     * 
     *
     */
    void setExternalAffiliateChannel(String externalAffiliateChannel);

    /**
     * Returns the property 'external affiliate channel'.
     *
     * 
     *
     */
    String getExternalAffiliateChannel();
    /**
     * Helper method to get reference of this object as model type.
     */
    AffiliateInformation asAffiliateInformation();
}
