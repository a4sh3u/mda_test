//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.marketing.affiliate.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(affiliate information)}
import de.smava.webapp.applicant.marketing.affiliate.domain.AffiliateInformation;
import de.smava.webapp.applicant.marketing.affiliate.domain.interfaces.AffiliateInformationEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AffiliateInformations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAffiliateInformation
    extends BrokerageEntity    implements AffiliateInformationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAffiliateInformation.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof AffiliateInformation)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setExpirationDate(((AffiliateInformation) oldEntity).getExpirationDate());    
        this.setAccount(((AffiliateInformation) oldEntity).getAccount());    
        this.setMarketingPlacement(((AffiliateInformation) oldEntity).getMarketingPlacement());    
        this.setExternalAffiliateId(((AffiliateInformation) oldEntity).getExternalAffiliateId());    
        this.setExternalAffiliateSubId(((AffiliateInformation) oldEntity).getExternalAffiliateSubId());    
        this.setExternalAffiliateChannel(((AffiliateInformation) oldEntity).getExternalAffiliateChannel());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof AffiliateInformation)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getExpirationDate(), ((AffiliateInformation) otherEntity).getExpirationDate());    
        equals = equals && valuesAreEqual(this.getAccount(), ((AffiliateInformation) otherEntity).getAccount());    
        equals = equals && valuesAreEqual(this.getMarketingPlacement(), ((AffiliateInformation) otherEntity).getMarketingPlacement());    
        equals = equals && valuesAreEqual(this.getExternalAffiliateId(), ((AffiliateInformation) otherEntity).getExternalAffiliateId());    
        equals = equals && valuesAreEqual(this.getExternalAffiliateSubId(), ((AffiliateInformation) otherEntity).getExternalAffiliateSubId());    
        equals = equals && valuesAreEqual(this.getExternalAffiliateChannel(), ((AffiliateInformation) otherEntity).getExternalAffiliateChannel());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(affiliate information)}

    public static final int MAX_AFFILIATE_CHARS = 512;

    private static final String UTF8 = "utf-8";

    /**
     * encodes the given value as base64 with UFT-8
     *
     * @param value string to encode
     * @return encoded string
     */
    protected String encode(String value) {
        try {
            return new String(Base64.encodeBase64(value.getBytes(UTF8)));
        } catch (Exception e) {
            LOGGER.error("problem with encoding", e);
        }
        return null;
    }

    /**
     * decodes the given value as base64 with UTF8
     *
     * @param value string to decode
     * @return decoded string
     */
    protected String decode(String value) {
        try {
            return new String(Base64.decodeBase64(value), UTF8);
        } catch (Exception e) {
            LOGGER.error("problem with encoding", e);
        }
        return null;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

