package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import de.smava.webapp.commons.dao.BaseDao;

import java.util.Collection;

/**
 * DAO interface for the domain object 'AdvisorGroups'.
 */
public interface AdvisorGroupDao extends BaseDao<AdvisorGroup> {

    /**
     * Deletes an advisor group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the advisor group
     */
    void deleteAdvisorGroup(Long id);

    /**
     * Retrieves all 'AdvisorGroup' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<AdvisorGroup> getAdvisorGroupList();

    Collection<AdvisorGroup> findActiveAdvisorGroups();

    Collection<AdvisorGroup> findAdvisorGroupsForAdvisorId(Long advisorId);

    AdvisorGroup findByName(String name);

    AdvisorGroup findByConditionLiteral(String condition);

    void disableAdvisorGroup(Long id);
}
