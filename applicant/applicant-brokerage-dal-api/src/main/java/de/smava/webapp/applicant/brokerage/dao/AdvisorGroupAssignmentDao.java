package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroupAssignment;
import de.smava.webapp.applicant.brokerage.dto.AdvisorGroupAssignmentExpanded;
import de.smava.webapp.commons.dao.BaseDao;

import java.util.Collection;

/**
 * DAO interface for the domain object 'AdvisorGroupAssignments'.
 */
public interface AdvisorGroupAssignmentDao extends BaseDao<AdvisorGroupAssignment> {

    void deleteAdvisorGroupAssignment(Long id);

    Collection<AdvisorGroupAssignment> getAdvisorGroupAssignmentList();

    Collection<AdvisorGroupAssignmentExpanded> getActiveAdvisorGroupAssignments();

    void expireAdvisorGroupAssignment(Long advisorId, Long advisorGroupId);

    Collection<AdvisorGroupAssignment> findByAdvisorAndGroup(Long advisorId, AdvisorGroup advisorGroup);

    Collection<AdvisorGroupAssignment> findByGroup(Long advisorGroupId);
}
