package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.interfaces.AdvisorGroupAssignmentEntityInterface;
import org.apache.log4j.Logger;

import java.util.*;

public class AdvisorGroupAssignment implements AdvisorGroupAssignmentEntityInterface<AdvisorGroup> {

    private static final Logger LOGGER = Logger.getLogger(AdvisorGroupAssignment.class);

    protected Long _id;
    protected Long _advisorId;
    protected AdvisorGroup _advisorGroup;
    protected Date _creationDate;
    protected Date _expirationDate;

    public Long getId() {
        return _id;
    }

    public void setId(Long _id) {
        this._id = _id;
    }

    public Long getAdvisorId() {
        return _advisorId;
    }

    public void setAdvisorId(Long advisorId) {
        this._advisorId = advisorId;
    }

    public void setAdvisorGroup(AdvisorGroup advisorGroup) {
        _advisorGroup = advisorGroup;
    }
            
    public AdvisorGroup getAdvisorGroup() {
        return _advisorGroup;
    }

    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    public Date getCreationDate() {
        return _creationDate;
    }
    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }
                        
    public Date getExpirationDate() {
        return _expirationDate;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AdvisorGroupAssignment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AdvisorGroupAssignment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
