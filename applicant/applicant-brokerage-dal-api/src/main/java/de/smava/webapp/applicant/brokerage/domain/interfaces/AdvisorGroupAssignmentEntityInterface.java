package de.smava.webapp.applicant.brokerage.domain.interfaces;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

public interface AdvisorGroupAssignmentEntityInterface<AG extends AdvisorGroupEntityInterface> extends BaseEntity {

    void setAdvisorId(Long advisorId);

    Long getAdvisorId();

    void setAdvisorGroup(AG advisorGroup);

    AG getAdvisorGroup();

    void setCreationDate(Date creationDate);

    Date getCreationDate();

    void setExpirationDate(Date expirationDate);

    Date getExpirationDate();
}
