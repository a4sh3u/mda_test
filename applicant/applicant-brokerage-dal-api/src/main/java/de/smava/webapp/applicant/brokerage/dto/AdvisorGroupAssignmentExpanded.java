package de.smava.webapp.applicant.brokerage.dto;

import java.io.Serializable;

public class AdvisorGroupAssignmentExpanded implements Serializable {

    private Long id;

    private Long advisorId;

    private Long groupId;

    private String name;

    private String condition;

    private Integer priority;

    public AdvisorGroupAssignmentExpanded() {
    }

    public AdvisorGroupAssignmentExpanded(Long id, Long advisorId, Long groupId, String name, String condition, Integer priority) {
        this.id = id;
        this.advisorId = advisorId;
        this.groupId = groupId;
        this.name = name;
        this.condition = condition;
        this.priority = priority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAdvisorId() {
        return advisorId;
    }

    public void setAdvisorId(Long advisorId) {
        this.advisorId = advisorId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
