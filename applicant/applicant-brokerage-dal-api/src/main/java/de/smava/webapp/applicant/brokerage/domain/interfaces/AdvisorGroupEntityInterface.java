package de.smava.webapp.applicant.brokerage.domain.interfaces;

import de.smava.webapp.commons.domain.BaseEntity;

import java.util.Date;

/**
 * The domain object that represents 'AdvisorGroups'.
 */
public interface AdvisorGroupEntityInterface extends BaseEntity {

    /**
     * Setter for the property 'name'.
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     */
    String getName();

    /**
     * Setter for the property 'priority'.
     */
    void setPriority(Integer priority);

    /**
     * Returns the property 'priority'.
     */
    Integer getPriority();

    /**
     * Setter for the property 'condition'.
     */
    void setCondition(String condition);

    /**
     * Returns the property 'condition'.
     */
    String getCondition();

    /**
     * Setter for the property 'expiration date'.
     */
    void setExpirationDate(Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     */
    Date getExpirationDate();

}
