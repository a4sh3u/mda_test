package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.interfaces.AdvisorGroupEntityInterface;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * The domain object that represents 'AdvisorGroups'.
 */
public class AdvisorGroup implements AdvisorGroupEntityInterface {

    private static final Logger LOGGER = Logger.getLogger(AdvisorGroup.class);
    private static final long serialVersionUID = 236145197223517135L;

        protected Long _id;
        protected String _name;
        protected Integer _priority;
        protected Integer _monthlyGoal;
        protected String _condition;
        protected Date _expirationDate;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        this._id = id;
    }

    /**
     * Setter for the property 'name'.
     */
    public void setName(String name) {
        _name = name;
    }

    /**
     * Returns the property 'name'.
     */
    public String getName() {
        return _name;
    }

    /**
     * Setter for the property 'priority'.
     */
    public void setPriority(Integer priority) {
        _priority = priority;
    }

    /**
     * Returns the property 'priority'.
     */
    public Integer getPriority() {
        return _priority;
    }

    public Integer getMonthlyGoal() {
        return _monthlyGoal;
    }

    public void setMonthlyGoal(Integer monthlyGoal) {
        this._monthlyGoal = monthlyGoal;
    }

    /**
     * Setter for the property 'condition'.
     */
    public void setCondition(String condition) {
        _condition = condition;
    }

    /**
     * Returns the property 'condition'.
     */
    public String getCondition() {
        return _condition;
    }

    /**
     * Setter for the property 'expiration date'.
     */
    public void setExpirationDate(Date expirationDate) {
        _expirationDate = expirationDate;
    }

    /**
     * Returns the property 'expiration date'.
     */
    public Date getExpirationDate() {
        return _expirationDate;
    }

    @Override
    public boolean isNew() {
        return _id == null;
    }

    /**
     * A string representation of this object.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AdvisorGroup.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _condition=").append(_condition);
            builder.append("\n}");
        } else {
            builder.append(AdvisorGroup.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
