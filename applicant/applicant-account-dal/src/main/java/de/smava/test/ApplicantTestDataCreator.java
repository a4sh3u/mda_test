package de.smava.test;

import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.type.*;
import org.joda.time.DateTime;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by pharbert on 31.08.15.
 */
public class ApplicantTestDataCreator {

    public static Applicant createApplicant() {
        Applicant applicant = new Applicant();

        applicant.setId(1L);
        applicant.setEmail("dasistein@test.de");
        applicant.setPhones(createPhones(applicant));
        applicant.setBankAccount(createBankAccount());
        applicant.setHomeAddress(createLivingAddress());
        applicant.setPreviousHomeAddress(createPreviousAddress());
        applicant.setPersonalData(createPersonalData());
        applicant.setNationalityInfo(createNationalityInfo());
        applicant.setHousehold(createHousehold());
        applicant.setMainIncome(createEmployee());
        applicant.setAdditionalIncomes(createPartTimeIncome(applicant, createAdditionalIncomes(applicant)));
        applicant.setExpenses(createExpenses(applicant));

        return applicant;
    }

    public static Set<Phone> createPhones(Applicant applicant) {
        Set<Phone> phones = new HashSet<Phone>();

        Phone landline = new Phone();
        landline.setPhoneType(PhoneType.LANDLINE);
        landline.setCode("0999");
        landline.setNumber("99999");
        phones.add(landline);

        Phone mobile = new Phone();
        mobile.setPhoneType(PhoneType.MOBILE);
        mobile.setCode("0111");
        mobile.setNumber("11111");
        phones.add(mobile);

        return phones;
    }

    public static BankAccount createBankAccount() {
        BankAccount bankAccount = new BankAccount();

        bankAccount.setAccountNumber("6011300063");
        bankAccount.setBankCode("10050000");
        bankAccount.setBankName("LB Berlin");
        bankAccount.setIban("DE86100500006011300063");
        bankAccount.setBic("BELADEBEXXX");

        return bankAccount;
    }

    public static LivingAddress createLivingAddress() {
        LivingAddress livingAddress = new LivingAddress();

        Address address = new Address();
        address.setStreet("Milchstraße");
        address.setHouseNumber("42");
        address.setCity("Kleinkleckersdorf");
        address.setZipCode("13337");
        address.setCountry(Country.DE);
        livingAddress.setAddress(address);

        livingAddress.setMoveInDate(new DateTime("01-01-15").toDate());

        return livingAddress;
    }

    public static Address createPreviousAddress() {
        Address previousAddress = new Address();

        previousAddress.setStreet("Holzweg");
        previousAddress.setHouseNumber("33");
        previousAddress.setCity("Karstadt");
        previousAddress.setZipCode("15951");
        previousAddress.setCountry(Country.DE);

        return previousAddress;
    }

    public static PersonalData createPersonalData() {
        PersonalData personalData = new PersonalData();

        personalData.setGender(Gender.MALE);
        personalData.setFirstName("Hans");
        personalData.setLastName("Wurst");
        personalData.setBirthName("Lecker");
        personalData.setMaritalState(MaritalState.UNMARRIED);
        personalData.setBirthDate(new DateTime("75-12-24").toDate());

        return personalData;
    }

    public static NationalityInfo createNationalityInfo() {
        NationalityInfo nationalityInfo = new NationalityInfo();

        nationalityInfo.setCountryOfBirth(Country.DE);
        nationalityInfo.setNationality(Country.DE);
        nationalityInfo.setPlaceOfBirth("Pampa");

        return nationalityInfo;
    }

    private static Household createHousehold() {
        Household household = new Household();

        household.setNumberOfChildren(2);
        household.setNumberOfPersonsInHousehold(4);
        household.setNumberOfCars(1);
        household.setNumberOfMotorbikes(3);
        household.setDeptCreditCards(DebtCreditCardsType.DEBIT_AND_CREDIT_CARD);

        return household;
    }

    public static Employee createEmployee() {
        Employee employee = new Employee();

        employee.setIncomeType(IncomeType.EMPLOYMENT);
        employee.setMonthlyNetIncome(1234.56d);

        employee.setEmploymentType(de.smava.webapp.applicant.type.EmploymentType.SALARIED_STAFF);

        employee.setEmployedSince(new DateTime("01-05-10").toDate());
        employee.setStaffType(SalariedStaffType.FULL_TIME);
        employee.setIndustrySector(IndustrySectorType.EDUCATION);
        employee.setEmployer(createEmployer());

        employee.setEmployeeType(EmployeeType.EMPLOYEE);
        employee.setTemporarilyEmployed(false);

        return employee;
    }

    public static Employer createEmployer() {
        Employer employer = new Employer();

        employer.setName("Hinz und Kunz");
        employer.setAddress(createEmployerAddress());
        employer.setPhone(createEmployerPhone());

        return employer;
    }

    public static Address createEmployerAddress() {
        Address employerAddress = new Address();

        employerAddress.setStreet("Arbeitsweg");
        employerAddress.setHouseNumber("99");
        employerAddress.setCity("Werkstadt");
        employerAddress.setZipCode("69696");
        employerAddress.setCountry(Country.DE);

        return employerAddress;
    }

    public static Phone createEmployerPhone() {
        Phone phone = new Phone();

        phone.setPhoneType(PhoneType.MOBILE);
        phone.setCode("09876");
        phone.setNumber("54321");

        return phone;
    }

    public static Set<Income> createAdditionalIncomes(Applicant applicant) {
        Set<Income> additionalIncomes = new HashSet<Income>();

        Income spouseAlimony = new Income();
        spouseAlimony.setIncomeType(IncomeType.SPOUSE_ALIMONY);
        spouseAlimony.setMonthlyNetIncome(100d);
        additionalIncomes.add(spouseAlimony);

        Income childAlimony = new Income();
        childAlimony.setIncomeType(IncomeType.CHILD_ALIMONY);
        childAlimony.setMonthlyNetIncome(200d);
        additionalIncomes.add(childAlimony);

        Income rentalIncome = new Income();
        rentalIncome.setIncomeType(IncomeType.RENTAL_INCOME);
        rentalIncome.setMonthlyNetIncome(300d);
        additionalIncomes.add(rentalIncome);

        Income capitalIncome = new Income();
        capitalIncome.setIncomeType(IncomeType.CAPITAL_INCOME);
        capitalIncome.setMonthlyNetIncome(400d);
        additionalIncomes.add(capitalIncome);

        PensionSupplement pensionSupplement = new PensionSupplement();
        pensionSupplement.setIncomeType(IncomeType.PENSION_SUPPLEMENT);
        pensionSupplement.setMonthlyNetIncome(500d);
        pensionSupplement.setPensionSupplementType(PensionType.OTHER);
        additionalIncomes.add(pensionSupplement);

        Income lowWageGrant = new Income();
        lowWageGrant.setIncomeType(IncomeType.LOW_WAGE_GRANT);
        lowWageGrant.setMonthlyNetIncome(600d);
        additionalIncomes.add(lowWageGrant);

        return additionalIncomes;
    }

    public static Set<Income> createPartTimeIncome(Applicant applicant, Set<Income> additionalIncomes) {
        Income partTimeIncomeIncome = new Income();
        Employee partTimeIncome = new Employee();
        partTimeIncome.setIncomeType(IncomeType.EMPLOYMENT);
        partTimeIncome.setMonthlyNetIncome(789d);
        partTimeIncome.setStaffType(SalariedStaffType.SIDE_JOB);
        partTimeIncome.setEmployedSince(new DateTime("01-05-10").toDate());
        partTimeIncome.setTemporarilyEmployed(true);
        partTimeIncome.setEndOfTemporaryEmployment(new DateTime("01-08-18").toDate());
        additionalIncomes.add(partTimeIncomeIncome);

        return additionalIncomes;
    }

    private static Set<Expense> createExpenses(Applicant applicant) {
        Set<Expense> expenses= new HashSet<Expense>();

        Estate housingCost = new Estate();
        EstateFinancing estateFinancing = new EstateFinancing();
        estateFinancing.setMonthlyFinancingAmount(150d);
        housingCost.setEstateFinancing(estateFinancing);
        housingCost.setMonthlyAmount(100d);
        housingCost.setExpenseType(ExpenseType.HOUSING_COST);
        housingCost.setPartiallyRented(EstateRentType.FULLY_RENTED);
        expenses.add(housingCost);

        Investment investment = new Investment();
        investment.setMonthlyAmount(20d);
        investment.setExpenseType(ExpenseType.INVESTMENT);
        investment.setInvestmentType(InvestmentType.BUILDING_SOCIETY_LOAN);
        expenses.add(investment);

        Expense childAlimony = new Expense();
        childAlimony.setMonthlyAmount(30d);
        childAlimony.setExpenseType(ExpenseType.CHILD_ALIMONY);
        expenses.add(childAlimony);

        Expense spouseAlimony = new Expense();
        spouseAlimony.setMonthlyAmount(40d);
        spouseAlimony.setExpenseType(ExpenseType.SPOUSE_ALIMONY);
        expenses.add(spouseAlimony);

        Expense liveRiskInsurance = new Expense();
        liveRiskInsurance.setMonthlyAmount(50d);
        liveRiskInsurance.setExpenseType(ExpenseType.LIFE_RISK_INSURANCE);
        expenses.add(liveRiskInsurance);

        Expense privateLeasing = new Expense();
        privateLeasing.setMonthlyAmount(60d);
        privateLeasing.setExpenseType(ExpenseType.PRIVATE_LEASING);
        expenses.add(privateLeasing);

        Expense privateHealthInsurance = new Expense();
        privateHealthInsurance.setMonthlyAmount(70d);
        privateHealthInsurance.setExpenseType(ExpenseType.PRIVATE_HEALTH_INSURANCE);
        expenses.add(privateHealthInsurance);

        return expenses;
    }
}
