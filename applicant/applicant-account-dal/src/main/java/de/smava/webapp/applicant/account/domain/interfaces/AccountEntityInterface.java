package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.*;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The domain object that represents 'Accounts'.
 *
 * @author generator
 */
public interface AccountEntityInterface {

    /**
     * Setter for the property 'customer number'.
     *
     * 
     *
     */
    void setCustomerNumber(Long customerNumber);

    /**
     * Returns the property 'customer number'.
     *
     * 
     *
     */
    Long getCustomerNumber();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'credentials'.
     *
     * 
     *
     */
    void setCredentials(Credentials credentials);

    /**
     * Returns the property 'credentials'.
     *
     * 
     *
     */
    Credentials getCredentials();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'lock date'.
     *
     * 
     *
     */
    void setLockDate(Date lockDate);

    /**
     * Returns the property 'lock date'.
     *
     * 
     *
     */
    Date getLockDate();
    /**
     * Setter for the property 'email'.
     *
     * 
     *
     */
    void setEmail(String email);

    /**
     * Returns the property 'email'.
     *
     * 
     *
     */
    String getEmail();
    /**
     * Setter for the property 'last login'.
     *
     * 
     *
     */
    void setLastLogin(Date lastLogin);

    /**
     * Returns the property 'last login'.
     *
     * 
     *
     */
    Date getLastLogin();
    /**
     * Setter for the property 'email validated date'.
     *
     * 
     *
     */
    void setEmailValidatedDate(Date emailValidatedDate);

    /**
     * Returns the property 'email validated date'.
     *
     * 
     *
     */
    Date getEmailValidatedDate();
    /**
     * Setter for the property 'account owner'.
     *
     * 
     *
     */
    void setAccountOwner(de.smava.webapp.applicant.domain.Applicant accountOwner);

    /**
     * Returns the property 'account owner'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getAccountOwner();
    /**
     * Setter for the property 'applicant history'.
     *
     * 
     *
     */
    void setApplicantHistory(Set<de.smava.webapp.applicant.domain.Applicant> applicantHistory);

    /**
     * Returns the property 'applicant history'.
     *
     * 
     *
     */
    Set<de.smava.webapp.applicant.domain.Applicant> getApplicantHistory();
    /**
     * Setter for the property 'account roles'.
     *
     * 
     *
     */
    void setAccountRoles(Set<AccountRole> accountRoles);

    /**
     * Returns the property 'account roles'.
     *
     * 
     *
     */
    Set<AccountRole> getAccountRoles();
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    void setDocuments(List<Document> documents);

    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    List<Document> getDocuments();
    /**
     * Setter for the property 'customer agreements'.
     *
     * 
     *
     */
    void setCustomerAgreements(Set<CustomerAgreement> customerAgreements);

    /**
     * Returns the property 'customer agreements'.
     *
     * 
     *
     */
    Set<CustomerAgreement> getCustomerAgreements();
    /**
     * Setter for the property 'receive loan offers'.
     *
     * 
     *
     */
    void setReceiveLoanOffers(Date receiveLoanOffers);

    /**
     * Returns the property 'receive loan offers'.
     *
     * 
     *
     */
    Date getReceiveLoanOffers();
    /**
     * Setter for the property 'receive consolidation offers'.
     *
     * 
     *
     */
    void setReceiveConsolidationOffers(Date receiveConsolidationOffers);

    /**
     * Returns the property 'receive consolidation offers'.
     *
     * 
     *
     */
    Date getReceiveConsolidationOffers();
    /**
     * Setter for the property 'external customer accounts infos'.
     *
     *
     *
     */
    void setExternalCustomerAccountsInfos(Set<ExternalCustomerAccountsInfo> externalCustomerAccountsInfos);

    /**
     * Returns the property 'external customer accounts infos'.
     *
     *
     *
     */
    Set<ExternalCustomerAccountsInfo> getExternalCustomerAccountsInfos();    
    /**
     * Helper method to get reference of this object as model type.
     */
    Account asAccount();
}
