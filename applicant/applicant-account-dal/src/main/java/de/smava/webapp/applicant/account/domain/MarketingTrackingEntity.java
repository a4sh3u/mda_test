package de.smava.webapp.applicant.account.domain;

/**
 * Interfcae for all enitites that are supposed to be tracked.
 * <p/>
 * Date: 08.11.2006
 *
 * @author ingmar.decker
 */
public interface MarketingTrackingEntity {

    Long getMarketingPlacementId();

    java.util.Date getCreationDate();

}
