package de.smava.webapp.applicant.account.domain.history;



import de.smava.webapp.applicant.account.domain.abstracts.AbstractCredentials;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Credentialss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CredentialsHistory extends AbstractCredentials {

    protected transient Date _passwordChangedDateInitVal;
    protected transient boolean _passwordChangedDateIsSet;
    protected transient String _identificationNameInitVal;
    protected transient boolean _identificationNameIsSet;
    protected transient String _passwordInitVal;
    protected transient boolean _passwordIsSet;
    protected transient boolean _passwordChangedInitVal;
    protected transient boolean _passwordChangedIsSet;
    protected transient String _saltInitVal;
    protected transient boolean _saltIsSet;


	
    /**
     * Returns the initial value of the property 'identification name'.
     */
    public String identificationNameInitVal() {
        String result;
        if (_identificationNameIsSet) {
            result = _identificationNameInitVal;
        } else {
            result = getIdentificationName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'identification name'.
     */
    public boolean identificationNameIsDirty() {
        return !valuesAreEqual(identificationNameInitVal(), getIdentificationName());
    }

    /**
     * Returns true if the setter method was called for the property 'identification name'.
     */
    public boolean identificationNameIsSet() {
        return _identificationNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'password changed'.
     */
    public boolean passwordChangedInitVal() {
        boolean result;
        if (_passwordChangedIsSet) {
            result = _passwordChangedInitVal;
        } else {
            result = getPasswordChanged();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'password changed'.
     */
    public boolean passwordChangedIsDirty() {
        return !valuesAreEqual(passwordChangedInitVal(), getPasswordChanged());
    }

    /**
     * Returns true if the setter method was called for the property 'password changed'.
     */
    public boolean passwordChangedIsSet() {
        return _passwordChangedIsSet;
    }
		
    /**
     * Returns the initial value of the property 'salt'.
     */
    public String saltInitVal() {
        String result;
        if (_saltIsSet) {
            result = _saltInitVal;
        } else {
            result = getSalt();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'salt'.
     */
    public boolean saltIsDirty() {
        return !valuesAreEqual(saltInitVal(), getSalt());
    }

    /**
     * Returns true if the setter method was called for the property 'salt'.
     */
    public boolean saltIsSet() {
        return _saltIsSet;
    }

}
