package de.smava.webapp.applicant.account.domain.history;


import de.smava.webapp.applicant.account.domain.abstracts.AbstractExternalCustomerAccountsInfo;

import java.util.Date;


/**
 * The domain object that has all history aggregation related fields for 'ExternalCustomerAccountsInfos'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ExternalCustomerAccountsInfoHistory extends AbstractExternalCustomerAccountsInfo {

    protected transient String _externalIdInitVal;
    protected transient boolean _externalIdIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _changeDateInitVal;
    protected transient boolean _changeDateIsSet;


			
    /**
     * Returns the initial value of the property 'external id'.
     */
    public String externalIdInitVal() {
        String result;
        if (_externalIdIsSet) {
            result = _externalIdInitVal;
        } else {
            result = getExternalId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external id'.
     */
    public boolean externalIdIsDirty() {
        return !valuesAreEqual(externalIdInitVal(), getExternalId());
    }

    /**
     * Returns true if the setter method was called for the property 'external id'.
     */
    public boolean externalIdIsSet() {
        return _externalIdIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'change date'.
     */
    public Date changeDateInitVal() {
        Date result;
        if (_changeDateIsSet) {
            result = _changeDateInitVal;
        } else {
            result = getChangeDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'change date'.
     */
    public boolean changeDateIsDirty() {
        return !valuesAreEqual(changeDateInitVal(), getChangeDate());
    }

    /**
     * Returns true if the setter method was called for the property 'change date'.
     */
    public boolean changeDateIsSet() {
        return _changeDateIsSet;
    }

}
