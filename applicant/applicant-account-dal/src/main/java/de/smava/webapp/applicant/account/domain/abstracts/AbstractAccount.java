//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account)}

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.account.domain.CustomerAgreement;
import de.smava.webapp.applicant.account.domain.interfaces.AccountEntityInterface;
import de.smava.webapp.applicant.account.type.CustomerAgreementType;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.constants.StringConstants;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.security.Role;
import org.apache.log4j.Logger;

import java.util.*;

                                                                                                                                                                                                                                                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Accounts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAccount
    extends BrokerageEntity    implements AccountEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAccount.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Account)) {
            return;
        }
        
        this.setCustomerNumber(((Account) oldEntity).getCustomerNumber());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setCredentials(((Account) oldEntity).getCredentials());    
        this.setState(((Account) oldEntity).getState());    
        this.setLockDate(((Account) oldEntity).getLockDate());    
        this.setEmail(((Account) oldEntity).getEmail());    
        this.setLastLogin(((Account) oldEntity).getLastLogin());    
        this.setEmailValidatedDate(((Account) oldEntity).getEmailValidatedDate());    
        this.setAccountOwner(((Account) oldEntity).getAccountOwner());
        this.setApplicantHistory(((Account) oldEntity).getApplicantHistory());    
        this.setAccountRoles(((Account) oldEntity).getAccountRoles());    
        this.setDocuments(((Account) oldEntity).getDocuments());    
        this.setCustomerAgreements(((Account) oldEntity).getCustomerAgreements());    
        this.setReceiveLoanOffers(((Account) oldEntity).getReceiveLoanOffers());    
        this.setReceiveConsolidationOffers(((Account) oldEntity).getReceiveConsolidationOffers());    
        this.setExternalCustomerAccountsInfos(((Account) oldEntity).getExternalCustomerAccountsInfos());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Account)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getCustomerNumber(), ((Account) otherEntity).getCustomerNumber());    
            
        equals = equals && valuesAreEqual(this.getCredentials(), ((Account) otherEntity).getCredentials());    
        equals = equals && valuesAreEqual(this.getState(), ((Account) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getLockDate(), ((Account) otherEntity).getLockDate());    
        equals = equals && valuesAreEqual(this.getEmail(), ((Account) otherEntity).getEmail());    
        equals = equals && valuesAreEqual(this.getLastLogin(), ((Account) otherEntity).getLastLogin());    
        equals = equals && valuesAreEqual(this.getEmailValidatedDate(), ((Account) otherEntity).getEmailValidatedDate());    
        equals = equals && valuesAreEqual(this.getAccountOwner(), ((Account) otherEntity).getAccountOwner());    
        equals = equals && valuesAreEqual(this.getApplicantHistory(), ((Account) otherEntity).getApplicantHistory());
        equals = equals && valuesAreEqual(this.getAccountRoles(), ((Account) otherEntity).getAccountRoles());    
        equals = equals && valuesAreEqual(this.getDocuments(), ((Account) otherEntity).getDocuments());    
        equals = equals && valuesAreEqual(this.getCustomerAgreements(), ((Account) otherEntity).getCustomerAgreements());    
        equals = equals && valuesAreEqual(this.getReceiveLoanOffers(), ((Account) otherEntity).getReceiveLoanOffers());    
        equals = equals && valuesAreEqual(this.getReceiveConsolidationOffers(), ((Account) otherEntity).getReceiveConsolidationOffers());    
        equals = equals && valuesAreEqual(this.getExternalCustomerAccountsInfos(), ((Account) otherEntity).getExternalCustomerAccountsInfos());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(account)}
    public static final String USER_STATE_DEACTIVATED = "DEACTIVATED";
    public static final String USER_STATE_ACTIVATED = "ACTIVATED";
    public static final String USER_STATE_ACCRED_IN_PROGRESS = "ACCRED_IN_PROGRESS";
    public static final String USER_STATE_ACCRED_DENIED = "ACCRED_DENIED";
    public static final String USER_STATE_FAKE = "FAKE";
    public static final String USER_STATE_DELETED = "DELETED";

    public static final String BORROWER_REG_STARTED = "BORROWER_REG_STARTED";

    public static final Set<String> GOING_TO_BE_BORROWER_PREFEREDROLES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(BORROWER_REG_STARTED, StringConstants.SHORTLEAD)));
    public static final Set<String> GOING_TO_BE_BORROWER_OR_SALESLEAD_PREFEREDROLES = Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(BORROWER_REG_STARTED, StringConstants.SHORTLEAD, StringConstants.SALESLEAD)));

    private static final long serialVersionUID = -4603699160347935409L;

    @SuppressWarnings("unchecked")
    public static final Collection<String> USER_STATES = ConstCollector.getAll("USER_STATE");

    protected transient Set<Role> _roles;

    public static final String LAST_LOGIN_CHANGE = "last login";

    public CustomerAgreement getLatestCustomerAgreementForType(CustomerAgreementType type) {
        CustomerAgreement ret = null;

        for (CustomerAgreement agreement : getCustomerAgreements()) {
            if (type.equals(agreement.getType())) {
                if (ret == null || (ret.getAgreedOn().before(agreement.getAgreedOn()))) {
                    ret = agreement;
                }
            }
        }

        return ret;
    }

    private Date getLatestCustomerAgreement(CustomerAgreementType type) {
        CustomerAgreement agreement = getLatestCustomerAgreementForType(type);

        if (agreement != null && agreement.getDisagreedOn() == null) {
            return agreement.getAgreedOn();
        }

        return null;
    }

    /**
     * Returns the timestamp for 'TERMS_AND_CONDITIONS'
     */
    public Date getTermsAndConditionsDate() {
        return getLatestCustomerAgreement(CustomerAgreementType.TERMS_AND_CONDITIONS);
    }

    /**
     * Returns the timestamp for 'FIDOR_AGREEMENT'
     */
    public Date getFidorAgreementDate() {
        return getLatestCustomerAgreement(CustomerAgreementType.FIDOR_AGREEMENT);
    }

    /**
     * Returns the timestamp for 'BROKERAGE_AGREEMENT'
     */
    public Date getBrokerageAgreementDate() {
        return getLatestCustomerAgreement(CustomerAgreementType.BROKERAGE_AGREEMENT);
    }

    /**
     * Returns the timestamp for 'PRIVACY_AGREEMENT'
     */
    public Date getPrivacyAgreementDate() {
        return getLatestCustomerAgreement(CustomerAgreementType.PRIVACY_AGREEMENT);
    }

    /**
     * Returns the timestamp for 'SCHUFA_AGREEMENT'
     */
    public Date getSchufaAgreementDate() {
        return getLatestCustomerAgreement(CustomerAgreementType.SCHUFA_AGREEMENT);

    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

