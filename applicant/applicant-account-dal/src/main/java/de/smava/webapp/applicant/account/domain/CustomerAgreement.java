//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer agreement)}
import de.smava.webapp.applicant.account.domain.history.CustomerAgreementHistory;

import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CustomerAgreements'.
 *
 * 
 *
 * @author generator
 */
public class CustomerAgreement extends CustomerAgreementHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(customer agreement)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _agreedOn;
        protected Date _doubleOptIn;
        protected Date _disagreedOn;
        protected String _version;
        protected String _note;
        protected de.smava.webapp.applicant.account.type.CustomerAgreementType _type;
        protected Account _account;
        
                            /**
     * Setter for the property 'agreed on'.
     *
     * 
     *
     */
    public void setAgreedOn(Date agreedOn) {
        if (!_agreedOnIsSet) {
            _agreedOnIsSet = true;
            _agreedOnInitVal = getAgreedOn();
        }
        registerChange("agreed on", _agreedOnInitVal, agreedOn);
        _agreedOn = agreedOn;
    }
                        
    /**
     * Returns the property 'agreed on'.
     *
     * 
     *
     */
    public Date getAgreedOn() {
        return _agreedOn;
    }
                                    /**
     * Setter for the property 'double opt in'.
     *
     * 
     *
     */
    public void setDoubleOptIn(Date doubleOptIn) {
        if (!_doubleOptInIsSet) {
            _doubleOptInIsSet = true;
            _doubleOptInInitVal = getDoubleOptIn();
        }
        registerChange("double opt in", _doubleOptInInitVal, doubleOptIn);
        _doubleOptIn = doubleOptIn;
    }
                        
    /**
     * Returns the property 'double opt in'.
     *
     * 
     *
     */
    public Date getDoubleOptIn() {
        return _doubleOptIn;
    }
                                    /**
     * Setter for the property 'disagreed on'.
     *
     * 
     *
     */
    public void setDisagreedOn(Date disagreedOn) {
        if (!_disagreedOnIsSet) {
            _disagreedOnIsSet = true;
            _disagreedOnInitVal = getDisagreedOn();
        }
        registerChange("disagreed on", _disagreedOnInitVal, disagreedOn);
        _disagreedOn = disagreedOn;
    }
                        
    /**
     * Returns the property 'disagreed on'.
     *
     * 
     *
     */
    public Date getDisagreedOn() {
        return _disagreedOn;
    }
                                    /**
     * Setter for the property 'version'.
     *
     * 
     *
     */
    public void setVersion(String version) {
        if (!_versionIsSet) {
            _versionIsSet = true;
            _versionInitVal = getVersion();
        }
        registerChange("version", _versionInitVal, version);
        _version = version;
    }
                        
    /**
     * Returns the property 'version'.
     *
     * 
     *
     */
    public String getVersion() {
        return _version;
    }
                                    /**
     * Setter for the property 'note'.
     *
     * 
     *
     */
    public void setNote(String note) {
        if (!_noteIsSet) {
            _noteIsSet = true;
            _noteInitVal = getNote();
        }
        registerChange("note", _noteInitVal, note);
        _note = note;
    }
                        
    /**
     * Returns the property 'note'.
     *
     * 
     *
     */
    public String getNote() {
        return _note;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.account.type.CustomerAgreementType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.type.CustomerAgreementType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public Account getAccount() {
        return _account;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CustomerAgreement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _version=").append(_version);
            builder.append("\n    _note=").append(_note);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(CustomerAgreement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CustomerAgreement asCustomerAgreement() {
        return this;
    }
}
