package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.Document;
import de.smava.webapp.applicant.account.domain.DocumentAddressAssignment;


/**
 * The domain object that represents 'DocumentAddressAssignments'.
 *
 * @author generator
 */
public interface DocumentAddressAssignmentEntityInterface {

    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    void setDocument(Document document);

    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    Document getDocument();
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(de.smava.webapp.applicant.domain.Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Address getAddress();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    DocumentAddressAssignment asDocumentAddressAssignment();
}
