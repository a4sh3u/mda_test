package de.smava.webapp.applicant.account.domain.history;



import de.smava.webapp.applicant.account.domain.abstracts.AbstractAccount;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'Accounts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AccountHistory extends AbstractAccount {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient Date _lockDateInitVal;
    protected transient boolean _lockDateIsSet;
    protected transient String _emailInitVal;
    protected transient boolean _emailIsSet;
    protected transient Date _lastLoginInitVal;
    protected transient boolean _lastLoginIsSet;
    protected transient Date _emailValidatedDateInitVal;
    protected transient boolean _emailValidatedDateIsSet;
    protected transient Date _receiveLoanOffersInitVal;
    protected transient boolean _receiveLoanOffersIsSet;
    protected transient Date _receiveConsolidationOffersInitVal;
    protected transient boolean _receiveConsolidationOffersIsSet;


		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'lock date'.
     */
    public Date lockDateInitVal() {
        Date result;
        if (_lockDateIsSet) {
            result = _lockDateInitVal;
        } else {
            result = getLockDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'lock date'.
     */
    public boolean lockDateIsDirty() {
        return !valuesAreEqual(lockDateInitVal(), getLockDate());
    }

    /**
     * Returns true if the setter method was called for the property 'lock date'.
     */
    public boolean lockDateIsSet() {
        return _lockDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last login'.
     */
    public Date lastLoginInitVal() {
        Date result;
        if (_lastLoginIsSet) {
            result = _lastLoginInitVal;
        } else {
            result = getLastLogin();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last login'.
     */
    public boolean lastLoginIsDirty() {
        return !valuesAreEqual(lastLoginInitVal(), getLastLogin());
    }

    /**
     * Returns true if the setter method was called for the property 'last login'.
     */
    public boolean lastLoginIsSet() {
        return _lastLoginIsSet;
    }
	
    /**
     * Returns the initial value of the property 'email validated date'.
     */
    public Date emailValidatedDateInitVal() {
        Date result;
        if (_emailValidatedDateIsSet) {
            result = _emailValidatedDateInitVal;
        } else {
            result = getEmailValidatedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email validated date'.
     */
    public boolean emailValidatedDateIsDirty() {
        return !valuesAreEqual(emailValidatedDateInitVal(), getEmailValidatedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'email validated date'.
     */
    public boolean emailValidatedDateIsSet() {
        return _emailValidatedDateIsSet;
    }
							
    /**
     * Returns the initial value of the property 'receive loan offers'.
     */
    public Date receiveLoanOffersInitVal() {
        Date result;
        if (_receiveLoanOffersIsSet) {
            result = _receiveLoanOffersInitVal;
        } else {
            result = getReceiveLoanOffers();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'receive loan offers'.
     */
    public boolean receiveLoanOffersIsDirty() {
        return !valuesAreEqual(receiveLoanOffersInitVal(), getReceiveLoanOffers());
    }

    /**
     * Returns true if the setter method was called for the property 'receive loan offers'.
     */
    public boolean receiveLoanOffersIsSet() {
        return _receiveLoanOffersIsSet;
    }
	
    /**
     * Returns the initial value of the property 'receive consolidation offers'.
     */
    public Date receiveConsolidationOffersInitVal() {
        Date result;
        if (_receiveConsolidationOffersIsSet) {
            result = _receiveConsolidationOffersInitVal;
        } else {
            result = getReceiveConsolidationOffers();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'receive consolidation offers'.
     */
    public boolean receiveConsolidationOffersIsDirty() {
        return !valuesAreEqual(receiveConsolidationOffersInitVal(), getReceiveConsolidationOffers());
    }

    /**
     * Returns true if the setter method was called for the property 'receive consolidation offers'.
     */
    public boolean receiveConsolidationOffersIsSet() {
        return _receiveConsolidationOffersIsSet;
    }

}
