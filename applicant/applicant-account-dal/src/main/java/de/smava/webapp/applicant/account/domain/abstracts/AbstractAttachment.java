//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(attachment)}
import de.smava.webapp.applicant.account.domain.Attachment;
import de.smava.webapp.applicant.account.domain.interfaces.AttachmentEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Attachments'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAttachment
    extends BrokerageEntity implements AttachmentEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAttachment.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Attachment)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setAttachmentId(((Attachment) oldEntity).getAttachmentId());    
        this.setName(((Attachment) oldEntity).getName());    
        this.setDescription(((Attachment) oldEntity).getDescription());    
        this.setContentType(((Attachment) oldEntity).getContentType());    
        this.setLocation(((Attachment) oldEntity).getLocation());    
        this.setDocument(((Attachment) oldEntity).getDocument());    
        this.setDocumentAttachmentContainer(((Attachment) oldEntity).getDocumentAttachmentContainer());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Attachment)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getAttachmentId(), ((Attachment) otherEntity).getAttachmentId());    
        equals = equals && valuesAreEqual(this.getName(), ((Attachment) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getDescription(), ((Attachment) otherEntity).getDescription());    
        equals = equals && valuesAreEqual(this.getContentType(), ((Attachment) otherEntity).getContentType());    
        equals = equals && valuesAreEqual(this.getLocation(), ((Attachment) otherEntity).getLocation());    
        equals = equals && valuesAreEqual(this.getDocument(), ((Attachment) otherEntity).getDocument());    
        equals = equals && valuesAreEqual(this.getDocumentAttachmentContainer(), ((Attachment) otherEntity).getDocumentAttachmentContainer());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(attachment)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

