//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document)}

import com.aperto.webkit.utils.ConstCollector;
import de.smava.webapp.applicant.account.domain.*;
import de.smava.webapp.applicant.account.domain.interfaces.DocumentEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.*;

                                                                                                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Documents'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractDocument
    extends BrokerageEntity implements DocumentEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDocument.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Document)) {
            return;
        }
        
        this.setState(((Document) oldEntity).getState());    
        this.setApprovalState(((Document) oldEntity).getApprovalState());    
        this.setSubject(((Document) oldEntity).getSubject());    
        this.setText(((Document) oldEntity).getText());    
        this.setNotes(((Document) oldEntity).getNotes());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setDate(((Document) oldEntity).getDate());    
        this.setDirection(((Document) oldEntity).getDirection());    
        this.setLastModifier(((Document) oldEntity).getLastModifier());    
        this.setOwner(((Document) oldEntity).getOwner());    
        this.setType(((Document) oldEntity).getType());    
        this.setTypeOfDispatch(((Document) oldEntity).getTypeOfDispatch());    
        this.setDocumentAddressAssignments(((Document) oldEntity).getDocumentAddressAssignments());    
        this.setEmail(((Document) oldEntity).getEmail());    
        this.setName(((Document) oldEntity).getName());    
        this.setPhone(((Document) oldEntity).getPhone());    
        this.setFax(((Document) oldEntity).getFax());    
        this.setEmailFrom(((Document) oldEntity).getEmailFrom());    
        this.setNameFrom(((Document) oldEntity).getNameFrom());    
        this.setPhoneFrom(((Document) oldEntity).getPhoneFrom());    
        this.setFaxFrom(((Document) oldEntity).getFaxFrom());    
        this.setDocumentContainers(((Document) oldEntity).getDocumentContainers());    
        this.setAttachments(((Document) oldEntity).getAttachments());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Document)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getState(), ((Document) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getApprovalState(), ((Document) otherEntity).getApprovalState());    
        equals = equals && valuesAreEqual(this.getSubject(), ((Document) otherEntity).getSubject());    
        equals = equals && valuesAreEqual(this.getText(), ((Document) otherEntity).getText());    
        equals = equals && valuesAreEqual(this.getNotes(), ((Document) otherEntity).getNotes());    
            
        equals = equals && valuesAreEqual(this.getDate(), ((Document) otherEntity).getDate());    
        equals = equals && valuesAreEqual(this.getDirection(), ((Document) otherEntity).getDirection());    
        equals = equals && valuesAreEqual(this.getLastModifier(), ((Document) otherEntity).getLastModifier());    
        equals = equals && valuesAreEqual(this.getOwner(), ((Document) otherEntity).getOwner());    
        equals = equals && valuesAreEqual(this.getType(), ((Document) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getTypeOfDispatch(), ((Document) otherEntity).getTypeOfDispatch());    
        equals = equals && valuesAreEqual(this.getDocumentAddressAssignments(), ((Document) otherEntity).getDocumentAddressAssignments());    
        equals = equals && valuesAreEqual(this.getEmail(), ((Document) otherEntity).getEmail());    
        equals = equals && valuesAreEqual(this.getName(), ((Document) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getPhone(), ((Document) otherEntity).getPhone());    
        equals = equals && valuesAreEqual(this.getFax(), ((Document) otherEntity).getFax());    
        equals = equals && valuesAreEqual(this.getEmailFrom(), ((Document) otherEntity).getEmailFrom());    
        equals = equals && valuesAreEqual(this.getNameFrom(), ((Document) otherEntity).getNameFrom());    
        equals = equals && valuesAreEqual(this.getPhoneFrom(), ((Document) otherEntity).getPhoneFrom());    
        equals = equals && valuesAreEqual(this.getFaxFrom(), ((Document) otherEntity).getFaxFrom());    
        equals = equals && valuesAreEqual(this.getDocumentContainers(), ((Document) otherEntity).getDocumentContainers());    
        equals = equals && valuesAreEqual(this.getAttachments(), ((Document) otherEntity).getAttachments());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(document)}

    private static final long serialVersionUID = -8768859904217058551L;

    public static final String STATE_OPEN = "OPEN";
    public static final String STATE_SUCCESSFUL = "SUCCESSFUL";
    public static final String STATE_FAILED = "FAILED";
    public static final String STATE_VOID = "VOID";
    public static final String STATE_DELETED = "DELETED";
    public static final String STATE_DRAFT = "DRAFT";


    public static final Set<String> STATES = ConstCollector.getAll("STATE_");
    public static final Collection<String> RDI_PAYOUT_STATES = Collections.unmodifiableCollection(Arrays.asList(new String[]{STATE_SUCCESSFUL, STATE_VOID}));
    public static final Set<String> CASI_JOURNAL_STATES = Collections.unmodifiableSet(new HashSet<String>(Arrays.asList(STATE_OPEN, STATE_SUCCESSFUL, STATE_FAILED, STATE_DRAFT)));

    public static final String APPROVAL_STATE_UNCHECKED = "UNCHECKED";
    public static final String APPROVAL_STATE_OK = "OK";
    public static final String APPROVAL_STATE_NOT_OK = "NOT_OK";

    public static final Set<String> APPROVAL_STATES = ConstCollector.getAll("APPROVAL_STATE_");

    public static final String DOCUMENT_TYPE_ACCOUNT_DEACTIVATION = "ACCOUNT_DEACTIVATION";
    public static final String DOCUMENT_TYPE_ACCOUNT_DELETE = "ACCOUNT_DELETE";
    public static final String DOCUMENT_TYPE_ACCOUNT_DENY = "ACCOUNT_DENY";
    public static final String DOCUMENT_TYPE_ACCOUNT_LOCK = "ACCOUNT_LOCK";
    public static final String DOCUMENT_TYPE_BALANCE_SHEET = "BALANCE_SHEET";
    public static final String DOCUMENT_TYPE_BORROWER_VALIDATION = "BORROWER_VALIDATION"; // KN Pr�fungsCheckliste
    public static final String DOCUMENT_TYPE_CHECKLIST = "CHECKLIST";
    public static final String DOCUMENT_TYPE_CONTRACT_OBJECTION = "CONTRACT_OBJECTION";
    public static final String DOCUMENT_TYPE_CREDIT_AGREEMENT = "CREDIT_AGREEMENT";
    public static final String DOCUMENT_TYPE_CREDIT_PROCUREMENT = "CREDIT_PROCUREMENT";
    public static final String DOCUMENT_TYPE_DEBIT_AUTHORIZATION = "DEBIT_AUTHORIZATION";
    public static final String DOCUMENT_TYPE_EMAIL = "EMAIL";
    public static final String DOCUMENT_TYPE_FACTORIZATION_CONTRACT = "FACTORIZATION_CONTRACT"; // Forderungskaufvertrag
    public static final String DOCUMENT_TYPE_ID_COPY = "ID_COPY";
    public static final String DOCUMENT_TYPE_INFO = "INFO";
    public static final String DOCUMENT_TYPE_LENDER_ACCOUNT_APPLICATION = "LENDER_ACCOUNT_APPLICATION";
    public static final String DOCUMENT_TYPE_MISC = "MISC";
    public static final String DOCUMENT_TYPE_NOTICE = "NOTICE";
    public static final String DOCUMENT_TYPE_PALIMONY = "PALIMONY";
    public static final String DOCUMENT_TYPE_PENSION_GRANTED = "PENSION_GRANTED";
    public static final String DOCUMENT_TYPE_POSTIDENT = "POSTIDENT";
    public static final String DOCUMENT_TYPE_VIDEOIDENT = "VIDEOIDENT";
    public static final String DOCUMENT_TYPE_VIDEOIDENT_PDF = "VIDEOIDENT_PDF";
    public static final String DOCUMENT_TYPE_RDI_REQUEST = "RDI_REQUEST";
    public static final String DOCUMENT_TYPE_REMINDER_FIRST = "REMINDER_FIRST";
    public static final String DOCUMENT_TYPE_REMINDER_PROCESS = "REMINDER_PROCESS";
    public static final String DOCUMENT_TYPE_REMINDER_SECOND = "REMINDER_SECOND";
    public static final String DOCUMENT_TYPE_REQUEST_FOR_PAYMENT = "REQUEST_FOR_PAYMENT";
    public static final String DOCUMENT_TYPE_SALARY_INFO = "SALARY_INFO";
    public static final String DOCUMENT_TYPE_SCHUFA_RECORD = "SCHUFA_RECORD";
    public static final String DOCUMENT_TYPE_SELF_INFO = "SELF_INFO";
    public static final String DOCUMENT_TYPE_SMAVA_AGENT_AGREEMENT = "SMAVA_AGENT_AGREEMENT"; // Agent-Vermittlungsvertrag
    public static final String DOCUMENT_TYPE_TAX_DECLARATION = "TAX_DECLARATION";
    public static final String DOCUMENT_TYPE_TAX_RETURN = "TAX_RETURN";
    public static final String DOCUMENT_TYPE_TRANSACTION_PIN_MAIL = "TRANSACTION_PIN_MAIL";
    public static final String DOCUMENT_TYPE_VG_CONTRACT = "VG_CONTRACT";
    public static final String DOCUMENT_TYPE_WELCOME_MAIL_LENDER = "WELCOME_MAIL_LENDER";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING = "BORROWER_CREDIT_CONSULTING";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_FIRST_CALL = "BORROWER_CREDIT_CONSULTING_FIRST_CALL";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_SECOND_CALL = "BORROWER_CREDIT_CONSULTING_SECOND_CALL";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_FAILED_PAYOUT = "BORROWER_CREDIT_CONSULTING_FAILED_PAYOUT";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_FIRST_CALL_SHORTLEAD = "BORROWER_CREDIT_CONSULTING_FIRST_CALL_SHORTLEAD";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_SECOND_CALL_SHORTLEAD = "BORROWER_CREDIT_CONSULTING_SECOND_CALL_SHORTLEAD";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_DOCUMENTS_MISSING = "BORROWER_CREDIT_CONSULTING_DOCUMENTS_MISSING";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_DOCUMENTS_NOT_SENT = "BORROWER_CREDIT_CONSULTING_DOCUMENTS_NOT_SENT";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_CONSOLIDATION_CHECK = "BORROWER_CREDIT_CONSULTING_CONSOLIDATION_CHECK";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_MANUAL_ALTERNATIVE_OFFER = "BORROWER_CREDIT_CONSULTING_MANUAL_ALTERNATIVE_OFFER";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT = "BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT";
    public static final String DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT_ADDITIONAL_CALL = "BORROWER_CREDIT_CONSULTING_REGISTRATION_DROPOUT_ADDITIONAL_CALL";
    public static final String DOCUMENT_TYPE_DOCUMENT_REMINDER = "DOCUMENT_REMINDER";
    public static final String DOCUMENT_TYPE_ORDER_ACTIVATION = "ORDER_ACTIVATION";
    public static final String DOCUMENT_TYPE_MISSING_DOCUMENT_REMINDER = "MISSING_DOCUMENT_REMINDER";
    public static final String DOCUMENT_TYPE_REGISTRATION_EXPIRED = "REGISTRATION_EXPIRED";
    public static final String DOCUMENT_TYPE_BANK_STATEMENT = "BANK_STATEMENT";
    public static final String DOCUMENT_TYPE_SALARY_STATEMENT = "SALARY_STATEMENT";
    public static final String DOCUMENT_TYPE_BUSINESS_ASSESSMENT = "BUSINESS_ASSESSMENT";
    public static final String DOCUMENT_TYPE_EARNINGS_STATEMENT = "EARNINGS_STATEMENT";
    public static final String DOCUMENT_TYPE_NET_INCOME_METHOD = "NET_INCOME_METHOD";
    public static final String DOCUMENT_TYPE_ANNUAL_STATEMENT = "ANNUAL_STATEMENT";
    public static final String DOCUMENT_TYPE_PROFIT_STATEMENT = "PROFIT_STATEMENT";
    public static final String DOCUMENT_TYPE_REFERENCE_ACCOUNT_CHANGE = "REFERENCE_ACCOUNT_CHANGE";
    public static final String DOCUMENT_TYPE_ADDRESS_CHANGE = "ADDRESS_CHANGE";
    public static final String DOCUMENT_TYPE_REGISTRATION_CARD = "REGISTRATION_CARD";
    public static final String DOCUMENT_TYPE_OTHER_PROFIT_STATEMENT = "OTHER_PROFIT_STATEMENT";
    public static final String DOCUMENT_TYPE_RENTAL_INCOME_STATEMENT = "RENTAL_INCOME_STATEMENT";
    public static final String DOCUMENT_TYPE_PARTNER_ACCOUNT = "PARTNER_ACCOUNT";
    public static final String DOCUMENT_TYPE_OTHER_INCOME_STATEMENT  = "OTHER_INCOME_STATEMENT";
    public static final String DOCUMENT_TYPE_EMPLOYER_CALL  = "EMPLOYER_CALL";
    public static final String DOCUMENT_TYPE_ASSET_MANAGEMENT_AGREEMENT  = "ASSET_MANAGEMENT_AGREEMENT";
    public static final String DOCUMENT_TYPE_BLACKLIST_CHECK = "BLACKLIST_CHECK";
    public static final String DOCUMENT_TYPE_DATA_PRIVACY_PROTECTION_AGREEMENT = "DATA_PRIVACY_PROTECTION_AGREEMENT";
    public static final String DOCUMENT_TYPE_REPAYMENT_BREAK = "REPAYMENT_BREAK";
    public static final String DOCUMENT_TYPE_REPAYMENT_BREAK_FORM = "REPAYMENT_BREAK_FORM";
    public static final String DOCUMENT_TYPE_DEBT_CONSOLIDATION_REQUEST = "DEBT_CONSOLIDATION_REQUEST";
    public static final String DOCUMENT_TYPE_FRAUD_CASE = "FRAUD_CASE";
    public static final String DOCUMENT_TYPE_CUSTOMER_COMPLAINT = "CUSTOMER_COMPLAINT";
    public static final String DOCUMENT_TYPE_SHORTLEAD_CALL = "SHORTLEAD_CALL";
    public static final String DOCUMENT_TYPE_LEAD_CALL = "LEAD_CALL";
    public static final String DOCUMENT_TYPE_SALES_CONTRACT_RENEWAL = "SALES_CONTRACT_RENEWAL";
    public static final String DOCUMENT_TYPE_SALES_REFERRER = "SALES_REFERRER";
    public static final String DOCUMENT_TYPE_SALES_CONTRACT_INCREASE = "SALES_CONTRACT_INCREASE";
    public static final String DOCUMENT_TYPE_SALES_FAILED_KP = "SALES_FAILED_KP";
    public static final String DOCUMENT_TYPE_SALES = "SALES";
    public static final String DOCUMENT_TYPE_SALES_OFFLINE_LEAD = "SALES_OFFLINE_LEAD";
    public static final String DOCUMENT_TYPE_SALES_ABORT = "SALES_ABORT";
    public static final String DOCUMENT_TYPE_SALES_UPSELLING = "SALES_UPSELLING";
    public static final String DOCUMENT_TYPE_ACTIVITY_FEE_OBJECTION = "ACTIVITY_FEE_OBJECTION";
    public static final String DOCUMENT_TYPE_RDI_INFO = "RDI_INFO";
    public static final String DOCUMENT_TYPE_CREDIT_INFO = "CREDIT_INFO";
    public static final String DOCUMENT_TYPE_RDI_CONDITION = "RDI_CONDITION";
    public static final String DOCUMENT_TYPE_SALES_BROKERAGE = "SALES_BROKERAGE";
    public static final String DOCUMENT_TYPE_SECCI = "SECCI";
    public static final String DOCUMENT_TYPE_INFO_CREDIT_FILE = "INFO_CREDIT_FILE";
    public static final String DOCUMENT_TYPE_BORROWER_CONVERSION_REMINDER = "BORROWER_CONVERSION_REMINDER";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_SUCCESSFUL = "CREDIT_ADVISORY_SUCCESSFUL";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_FAILED = "CREDIT_ADVISORY_FAILED";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_CUSTOMER_NOT_AVAILABLE = "CREDIT_ADVISORY_CUSTOMER_NOT_AVAILABLE";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_DUE_DATE = "CREDIT_ADVISORY_DUE_DATE";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_JOURNAL = "CREDIT_ADVISORY_JOURNAL";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_SMS = "CREDIT_ADVISORY_SMS";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_MISSING_DOCUMENTS_REMINDER = "CREDIT_ADVISORY_MISSING_DOCUMENTS_REMINDER";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_APPLICATION_REMINDER = "CREDIT_ADVISORY_APPLICATION_REMINDER";
    public static final String DOCUMENT_TYPE_CREDIT_ADVISORY_SEND_BY_POST = "CREDIT_ADVISORY_SEND_BY_POST";

    public static final String DOCUMENT_TYPE_BROKERAGE_APPLICATION_DOCUMENT = "BROKERAGE_APPLICATION_DOCUMENT";
    public static final String DOCUMENT_TYPE_BROKERAGE_APPLICATION_DOCUMENT_LETTER = "BROKERAGE_APPLICATION_DOCUMENT_LETTER";

    public static final Set<String> ALL_DOCUMENT_TYPE_NAMES = ConstCollector.getAll("DOCUMENT_TYPE_");

    public static final List<String> CREDIT_ADVISORY_CALL_DOCUMENTS = Collections.unmodifiableList(
            new ArrayList<String>(
                    Arrays.asList(	DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_FIRST_CALL_SHORTLEAD,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_FIRST_CALL,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_DOCUMENTS_MISSING,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_DOCUMENTS_NOT_SENT,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_FAILED_PAYOUT,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_SECOND_CALL_SHORTLEAD,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_SECOND_CALL,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_CONSOLIDATION_CHECK,
                            DOCUMENT_TYPE_BORROWER_CREDIT_CONSULTING_MANUAL_ALTERNATIVE_OFFER
                    )));

    public static final Set<String> DEFAULT_UNRESTRICTED_DOCUMENTS_TYPES = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList( DOCUMENT_TYPE_ADDRESS_CHANGE,
                            DOCUMENT_TYPE_BORROWER_VALIDATION,
                            DOCUMENT_TYPE_REFERENCE_ACCOUNT_CHANGE,
                            DOCUMENT_TYPE_REGISTRATION_CARD,
                            DOCUMENT_TYPE_ID_COPY,
                            DOCUMENT_TYPE_POSTIDENT,
                            DOCUMENT_TYPE_VIDEOIDENT
                    )));

    public static final Set<String> DOCUMENT_REMINDER_INVALIDATION_TYPES = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(DOCUMENT_TYPE_SALARY_INFO, DOCUMENT_TYPE_SELF_INFO, DOCUMENT_TYPE_DATA_PRIVACY_PROTECTION_AGREEMENT,
                            DOCUMENT_TYPE_POSTIDENT, DOCUMENT_TYPE_VIDEOIDENT, DOCUMENT_TYPE_SMAVA_AGENT_AGREEMENT, DOCUMENT_TYPE_CREDIT_AGREEMENT,
                            DOCUMENT_TYPE_CREDIT_PROCUREMENT, DOCUMENT_TYPE_DEBIT_AUTHORIZATION, DOCUMENT_TYPE_RDI_REQUEST
                    )));

    public static final Set<String> BANK_USER_RESTRICTED_DOCUMENTS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(DOCUMENT_TYPE_BUSINESS_ASSESSMENT,
                            DOCUMENT_TYPE_NET_INCOME_METHOD,
                            DOCUMENT_TYPE_SALARY_STATEMENT,
                            DOCUMENT_TYPE_SALARY_INFO,
                            DOCUMENT_TYPE_PROFIT_STATEMENT,
                            DOCUMENT_TYPE_EARNINGS_STATEMENT,
                            DOCUMENT_TYPE_ANNUAL_STATEMENT,
                            DOCUMENT_TYPE_BANK_STATEMENT,
                            DOCUMENT_TYPE_RENTAL_INCOME_STATEMENT,
                            DOCUMENT_TYPE_OTHER_INCOME_STATEMENT,
                            DOCUMENT_TYPE_OTHER_PROFIT_STATEMENT,
                            DOCUMENT_TYPE_TAX_RETURN,
                            DOCUMENT_TYPE_TAX_DECLARATION,
                            DOCUMENT_TYPE_PALIMONY,
                            DOCUMENT_TYPE_BALANCE_SHEET,
                            DOCUMENT_TYPE_CONTRACT_OBJECTION,
                            DOCUMENT_TYPE_DEBT_CONSOLIDATION_REQUEST,
                            DOCUMENT_TYPE_EMPLOYER_CALL,
                            DOCUMENT_TYPE_DATA_PRIVACY_PROTECTION_AGREEMENT,
                            DOCUMENT_TYPE_DEBIT_AUTHORIZATION,
                            DOCUMENT_TYPE_LENDER_ACCOUNT_APPLICATION,
                            DOCUMENT_TYPE_CREDIT_AGREEMENT,
                            DOCUMENT_TYPE_PENSION_GRANTED,
                            DOCUMENT_TYPE_SELF_INFO,
                            DOCUMENT_TYPE_REMINDER_FIRST,
                            DOCUMENT_TYPE_REMINDER_SECOND,
                            DOCUMENT_TYPE_SCHUFA_RECORD,
                            DOCUMENT_TYPE_INFO_CREDIT_FILE,
                            DOCUMENT_TYPE_ADDRESS_CHANGE,
                            DOCUMENT_TYPE_BORROWER_VALIDATION,
                            DOCUMENT_TYPE_REFERENCE_ACCOUNT_CHANGE,
                            DOCUMENT_TYPE_REGISTRATION_CARD,
                            DOCUMENT_TYPE_ID_COPY,
                            DOCUMENT_TYPE_POSTIDENT,
                            DOCUMENT_TYPE_FACTORIZATION_CONTRACT
                    )));

    public static final Set<String> BANK_USER_RESTRICTED_BORROWER_DOCUMENTS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(DOCUMENT_TYPE_BUSINESS_ASSESSMENT,
                            DOCUMENT_TYPE_NET_INCOME_METHOD,
                            DOCUMENT_TYPE_SALARY_STATEMENT,
                            DOCUMENT_TYPE_SALARY_INFO,
                            DOCUMENT_TYPE_PROFIT_STATEMENT,
                            DOCUMENT_TYPE_EARNINGS_STATEMENT,
                            DOCUMENT_TYPE_ANNUAL_STATEMENT,
                            DOCUMENT_TYPE_BANK_STATEMENT,
                            DOCUMENT_TYPE_RENTAL_INCOME_STATEMENT,
                            DOCUMENT_TYPE_OTHER_INCOME_STATEMENT,
                            DOCUMENT_TYPE_OTHER_PROFIT_STATEMENT,
                            DOCUMENT_TYPE_TAX_RETURN,
                            DOCUMENT_TYPE_TAX_DECLARATION,
                            DOCUMENT_TYPE_PALIMONY,
                            DOCUMENT_TYPE_BALANCE_SHEET
                    )));

    public static final Set<String> BANK_USER_RESTRICTED_ORDER_DOCUMENTS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(DOCUMENT_TYPE_CONTRACT_OBJECTION,
                            DOCUMENT_TYPE_DEBT_CONSOLIDATION_REQUEST,
                            DOCUMENT_TYPE_EMPLOYER_CALL,
                            DOCUMENT_TYPE_DATA_PRIVACY_PROTECTION_AGREEMENT,
                            DOCUMENT_TYPE_DEBIT_AUTHORIZATION,
                            DOCUMENT_TYPE_LENDER_ACCOUNT_APPLICATION,
                            DOCUMENT_TYPE_CREDIT_AGREEMENT,
                            DOCUMENT_TYPE_PENSION_GRANTED,
                            DOCUMENT_TYPE_SELF_INFO,
                            DOCUMENT_TYPE_REMINDER_FIRST,
                            DOCUMENT_TYPE_REMINDER_SECOND
                    )));

    public static final Set<String> SALARY_DOCUMENT_TYPE_NAMES = new HashSet<String>();

    static {
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_BALANCE_SHEET);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_BUSINESS_ASSESSMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_NET_INCOME_METHOD);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_SALARY_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_PROFIT_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_EARNINGS_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_ANNUAL_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_BANK_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_RENTAL_INCOME_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_PENSION_GRANTED);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_OTHER_INCOME_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_OTHER_PROFIT_STATEMENT);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_TAX_RETURN);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_TAX_DECLARATION);
        SALARY_DOCUMENT_TYPE_NAMES.add(DOCUMENT_TYPE_PALIMONY);
    }

    public static final String TYPE_OF_DISPATCH_EMAIL = "EMAIL";
    public static final String TYPE_OF_DISPATCH_MAIL = "MAIL";
    public static final String TYPE_OF_DISPATCH_FAX = "FAX";
    public static final String TYPE_OF_DISPATCH_PHONE = "PHONE";
    public static final String TYPE_OF_DISPATCH_INTERNAL = "INTERNAL";
    public static final String TYPE_OF_DISPATCH_DOWNLOAD = "DOWNLOAD";
    public static final String TYPE_OF_DISPATCH_SMS = "SMS";

    public static final String DIRECTION_IN = "IN";
    public static final String DIRECTION_OUT = "OUT";
    public static final String DIRECTION_INTERNAL = "INTERNAL";

    public static final Set<String> TYPE_OF_DISPATCH_NAMES = ConstCollector.getAll("TYPE_OF_DISPATCH_");

    public static final Set<String> TYPE_OF_DISPATCH_NAMES_DEAD_LEADS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(TYPE_OF_DISPATCH_MAIL, TYPE_OF_DISPATCH_FAX, TYPE_OF_DISPATCH_EMAIL
                    )));

    public static final Map<String,ArrayList<String>> BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS = new HashMap<String, ArrayList<String>>();

    static {

        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_BUSINESS_ASSESSMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));

        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_NET_INCOME_METHOD, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_SALARY_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_SALARY_INFO, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_PROFIT_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_EARNINGS_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_ANNUAL_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_BANK_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_RENTAL_INCOME_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_OTHER_INCOME_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_OTHER_PROFIT_STATEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_TAX_RETURN, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_TAX_DECLARATION, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_PALIMONY, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_BALANCE_SHEET, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_CONTRACT_OBJECTION, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_DEBT_CONSOLIDATION_REQUEST, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_EMPLOYER_CALL, new ArrayList<String>());
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_DATA_PRIVACY_PROTECTION_AGREEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_DEBIT_AUTHORIZATION, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_LENDER_ACCOUNT_APPLICATION, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_CREDIT_AGREEMENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_PENSION_GRANTED, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_SELF_INFO, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_REMINDER_FIRST, new ArrayList<String>(Arrays.asList(DIRECTION_OUT, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_REMINDER_SECOND, new ArrayList<String>(Arrays.asList(DIRECTION_OUT, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_SCHUFA_RECORD, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_INFO_CREDIT_FILE, new ArrayList<String>());
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_ADDRESS_CHANGE, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_BORROWER_VALIDATION, new ArrayList<String>());
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_REFERENCE_ACCOUNT_CHANGE, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_REGISTRATION_CARD, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_ID_COPY, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_POSTIDENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_VIDEOIDENT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));
        BANK_USER_RESTRICTED_DOCUMENTS_RESTRICTION_DETAILS.put(DOCUMENT_TYPE_FACTORIZATION_CONTRACT, new ArrayList<String>(Arrays.asList(DIRECTION_IN, STATE_SUCCESSFUL )));

    }

    public static Set<String> getAllDocumentTypes() {
        return ALL_DOCUMENT_TYPE_NAMES;
    }

    public boolean isApprovalNeeded() {
        boolean result = false;

        Collection<String> typesOfDispatch = new ArrayList<String>();
        typesOfDispatch.add(Document.TYPE_OF_DISPATCH_EMAIL);
        typesOfDispatch.add(Document.TYPE_OF_DISPATCH_MAIL);
        typesOfDispatch.add(Document.TYPE_OF_DISPATCH_FAX);

        if (DIRECTION_IN.equals(getDirection())
                && getAttachments() != null
                && !getAttachments().isEmpty()
                && STATE_SUCCESSFUL.equals(getState())
                && typesOfDispatch.contains(getTypeOfDispatch())) {
            result = true;
        }

        return result;
    }


    public void addAttachment(Attachment attachment) {
        if (getAttachments() == null) {
            setAttachments(new HashSet<Attachment>());
        }
        getAttachments().add(attachment);
    }

    public void addDocumentContainer(DocumentContainer documentContainer) {
        if (getDocumentContainers() == null) {
            setDocumentContainers(new LinkedList<DocumentContainer>());
        }

        getDocumentContainers().add(documentContainer);
    }

    public void addDocumentAddressAssignment(DocumentAddressAssignment documentAddressAssignment) {
        if (getDocumentAddressAssignments() == null) {
            setDocumentAddressAssignments(new LinkedList<DocumentAddressAssignment>());
        }

        getDocumentAddressAssignments().add(documentAddressAssignment);
    }

    public boolean isAvailableForMailDispatch() {
        return !Document.TYPE_OF_DISPATCH_MAIL.equals(this.getTypeOfDispatch()) &&
                !Document.DOCUMENT_TYPE_FACTORIZATION_CONTRACT.equals(this.getType());
    }

    public List<? extends Entity> getModifier() {
        final List<Account> list = new LinkedList<Account>();
        list.add(getOwner());
        return list;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

