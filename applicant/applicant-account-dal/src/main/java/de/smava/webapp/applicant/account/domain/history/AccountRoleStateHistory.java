package de.smava.webapp.applicant.account.domain.history;



import de.smava.webapp.applicant.account.domain.abstracts.AbstractAccountRoleState;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'AccountRoleStates'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AccountRoleStateHistory extends AbstractAccountRoleState {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient Date _validFromInitVal;
    protected transient boolean _validFromIsSet;
    protected transient Date _validUntilInitVal;
    protected transient boolean _validUntilIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid from'.
     */
    public Date validFromInitVal() {
        Date result;
        if (_validFromIsSet) {
            result = _validFromInitVal;
        } else {
            result = getValidFrom();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid from'.
     */
    public boolean validFromIsDirty() {
        return !valuesAreEqual(validFromInitVal(), getValidFrom());
    }

    /**
     * Returns true if the setter method was called for the property 'valid from'.
     */
    public boolean validFromIsSet() {
        return _validFromIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid until'.
     */
    public Date validUntilInitVal() {
        Date result;
        if (_validUntilIsSet) {
            result = _validUntilInitVal;
        } else {
            result = getValidUntil();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid until'.
     */
    public boolean validUntilIsDirty() {
        return !valuesAreEqual(validUntilInitVal(), getValidUntil());
    }

    /**
     * Returns true if the setter method was called for the property 'valid until'.
     */
    public boolean validUntilIsSet() {
        return _validUntilIsSet;
    }
	
}
