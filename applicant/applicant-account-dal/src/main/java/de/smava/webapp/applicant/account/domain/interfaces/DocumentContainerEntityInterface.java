package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.Document;
import de.smava.webapp.applicant.account.domain.DocumentContainer;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'DocumentContainers'.
 *
 * @author generator
 */
public interface DocumentContainerEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    void setDocuments(List<Document> documents);

    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    List<Document> getDocuments();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    DocumentContainer asDocumentContainer();
}
