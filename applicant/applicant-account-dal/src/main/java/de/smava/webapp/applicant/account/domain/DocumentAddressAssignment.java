//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document address assignment)}

import de.smava.webapp.applicant.account.domain.history.DocumentAddressAssignmentHistory;
import de.smava.webapp.applicant.domain.Address;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentAddressAssignments'.
 *
 * 
 *
 * @author generator
 */
public class DocumentAddressAssignment extends DocumentAddressAssignmentHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(document address assignment)}
    public DocumentAddressAssignment(Document document, Address address, String type) {
        LOGGER.debug("DocumentAddressAssignment.constructor: create assignment with document " + document.getId() + " and address " + address.getId() + " (type = " + type + ")");
        _document = document;
        _address = address;
        _type = type;
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Document _document;
        protected de.smava.webapp.applicant.domain.Address _address;
        protected String _type;
        
                                    
    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    public void setDocument(Document document) {
        _document = document;
    }
            
    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    public Document getDocument() {
        return _document;
    }
                                            
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    public void setAddress(de.smava.webapp.applicant.domain.Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Address getAddress() {
        return _address;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_document instanceof de.smava.webapp.commons.domain.Entity && !_document.getChangeSet().isEmpty()) {
             for (String element : _document.getChangeSet()) {
                 result.add("document : " + element);
             }
         }

         if (_address instanceof de.smava.webapp.commons.domain.Entity && !_address.getChangeSet().isEmpty()) {
             for (String element : _address.getChangeSet()) {
                 result.add("address : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentAddressAssignment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(DocumentAddressAssignment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DocumentAddressAssignment asDocumentAddressAssignment() {
        return this;
    }
}
