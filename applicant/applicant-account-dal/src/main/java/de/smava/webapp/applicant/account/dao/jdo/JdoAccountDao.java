package de.smava.webapp.applicant.account.dao.jdo;

import de.smava.webapp.applicant.account.dao.AccountDao;
import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * DAO implementation for the domain object 'Accounts'.
 *
 * @author generator
 */
@Repository(value = "applicantAccountDao")
public class JdoAccountDao extends JdoBaseDao implements AccountDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAccountDao.class);

    private static final String CLASS_NAME = "Account";
    private static final String STRING_GET = "get";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    /**
     * Returns an attached copy of the account identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public Account load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        Account result = getEntity(Account.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public Account getAccount(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	Account entity = findUniqueEntity(Account.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the account.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(Account account) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAccount: " + account);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(account)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(account);
    }

    /**
     * @deprecated Use {@link #save(Account) instead}
     */
    public Long saveAccount(Account account) {
        return save(account);
    }

    /**
     * Deletes an account, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account
     */
    public void deleteAccount(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAccount: " + id);
        }
        deleteEntity(Account.class, id);
    }

    /**
     * Retrieves all 'Account' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<Account> getAccountList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<Account> result = getEntities(Account.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'Account' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<Account> getAccountList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<Account> result = getEntities(Account.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'Account' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<Account> getAccountList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<Account> result = getEntities(Account.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'Account' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<Account> getAccountList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<Account> result = getEntities(Account.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'Account' instances.
     */
    public long getAccountCount() {
        long result = getEntityCount(Account.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    /**
     * {@inheritDoc}
     */
    public Account findAccountByEmail(final String email) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByEmail");
        query.setUnique(true);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", email);
        Account result = (Account) query.executeWithMap(params);
        return result;
    }

    public Account findAccountBySmavaId(Long smavaId) {
        Query query = getPersistenceManager().newNamedQuery(Account.class, "findAccountByCustomerNumber");
        query.setUnique(true);
        return (Account) query.execute(smavaId);
    }
}
