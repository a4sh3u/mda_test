//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer agreement)}
import de.smava.webapp.applicant.account.domain.CustomerAgreement;
import de.smava.webapp.applicant.account.domain.interfaces.CustomerAgreementEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CustomerAgreements'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCustomerAgreement
    extends BrokerageEntity implements CustomerAgreementEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCustomerAgreement.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof CustomerAgreement)) {
            return;
        }
        
        this.setAgreedOn(((CustomerAgreement) oldEntity).getAgreedOn());    
        this.setDoubleOptIn(((CustomerAgreement) oldEntity).getDoubleOptIn());    
        this.setDisagreedOn(((CustomerAgreement) oldEntity).getDisagreedOn());    
        this.setVersion(((CustomerAgreement) oldEntity).getVersion());    
        this.setNote(((CustomerAgreement) oldEntity).getNote());    
        this.setType(((CustomerAgreement) oldEntity).getType());    
        this.setAccount(((CustomerAgreement) oldEntity).getAccount());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof CustomerAgreement)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getAgreedOn(), ((CustomerAgreement) otherEntity).getAgreedOn());    
        equals = equals && valuesAreEqual(this.getDoubleOptIn(), ((CustomerAgreement) otherEntity).getDoubleOptIn());    
        equals = equals && valuesAreEqual(this.getDisagreedOn(), ((CustomerAgreement) otherEntity).getDisagreedOn());    
        equals = equals && valuesAreEqual(this.getVersion(), ((CustomerAgreement) otherEntity).getVersion());    
        equals = equals && valuesAreEqual(this.getNote(), ((CustomerAgreement) otherEntity).getNote());    
        equals = equals && valuesAreEqual(this.getType(), ((CustomerAgreement) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getAccount(), ((CustomerAgreement) otherEntity).getAccount());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(customer agreement)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

