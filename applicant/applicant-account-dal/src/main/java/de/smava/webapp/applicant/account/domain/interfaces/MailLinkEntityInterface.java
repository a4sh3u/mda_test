package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.account.domain.MailLink;

import java.util.Date;


/**
 * The domain object that represents 'MailLinks'.
 *
 * @author generator
 */
public interface MailLinkEntityInterface {

    /**
     * Setter for the property 'content id'.
     *
     * 
     *
     */
    void setContentId(int contentId);

    /**
     * Returns the property 'content id'.
     *
     * 
     *
     */
    int getContentId();
    /**
     * Setter for the property 'target url'.
     *
     * 
     *
     */
    void setTargetUrl(String targetUrl);

    /**
     * Returns the property 'target url'.
     *
     * 
     *
     */
    String getTargetUrl();
    /**
     * Setter for the property 'params'.
     *
     * 
     *
     */
    void setParams(String params);

    /**
     * Returns the property 'params'.
     *
     * 
     *
     */
    String getParams();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'click date'.
     *
     * 
     *
     */
    void setClickDate(Date clickDate);

    /**
     * Returns the property 'click date'.
     *
     * 
     *
     */
    Date getClickDate();
    /**
     * Setter for the property 'click count'.
     *
     * 
     *
     */
    void setClickCount(int clickCount);

    /**
     * Returns the property 'click count'.
     *
     * 
     *
     */
    int getClickCount();
    /**
     * Setter for the property 'recipient'.
     *
     * 
     *
     */
    void setRecipient(Account recipient);

    /**
     * Returns the property 'recipient'.
     *
     * 
     *
     */
    Account getRecipient();
    /**
     * Setter for the property 'sender'.
     *
     * 
     *
     */
    void setSender(Account sender);

    /**
     * Returns the property 'sender'.
     *
     * 
     *
     */
    Account getSender();
    /**
     * Setter for the property 'mail type'.
     *
     * 
     *
     */
    void setMailType(String mailType);

    /**
     * Returns the property 'mail type'.
     *
     * 
     *
     */
    String getMailType();
    /**
     * Setter for the property 'link name'.
     *
     * 
     *
     */
    void setLinkName(String linkName);

    /**
     * Returns the property 'link name'.
     *
     * 
     *
     */
    String getLinkName();
    /**
     * Setter for the property 'remote address'.
     *
     * 
     *
     */
    void setRemoteAddress(String remoteAddress);

    /**
     * Returns the property 'remote address'.
     *
     * 
     *
     */
    String getRemoteAddress();
    /**
     * Helper method to get reference of this object as model type.
     */
    MailLink asMailLink();
}
