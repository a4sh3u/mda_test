//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document container)}

import de.smava.webapp.applicant.account.domain.Document;
import de.smava.webapp.applicant.account.domain.DocumentContainer;
import de.smava.webapp.applicant.account.domain.interfaces.DocumentContainerEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.LinkedList;

                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentContainers'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractDocumentContainer
    extends BrokerageEntity implements DocumentContainerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDocumentContainer.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof DocumentContainer)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setName(((DocumentContainer) oldEntity).getName());    
        this.setState(((DocumentContainer) oldEntity).getState());    
        this.setDocuments(((DocumentContainer) oldEntity).getDocuments());    
        this.setType(((DocumentContainer) oldEntity).getType());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof DocumentContainer)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getName(), ((DocumentContainer) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getState(), ((DocumentContainer) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getDocuments(), ((DocumentContainer) otherEntity).getDocuments());    
        equals = equals && valuesAreEqual(this.getType(), ((DocumentContainer) otherEntity).getType());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(document container)}
    public static final String TYPE_REGISTRATION_COMPILATION = "REGISTRATION_COMPILATION";
    public static final String TYPE_LETTER_COMPILATION = "LETTER_COMPILATION";
    public static final String REGISTRATION_COMPILATION_NAME_KEY = "REGISTRATION_COMPILATION";
    public static final String CONTRACT_COMPILATION_NAME_KEY = "CONTRACT_COMPILATION";

    public static final String TYPE_CONTRACT_COMPILATION = "CONTRACT_COMPILATION";

    public static final String TYPE_CONSOLIDATED_DEBT_REQUEST_COMPILATION = "CONSOLIDATED_DEBT_REQUEST_COMPILATION";

    public void addDocument(Document document) {
        if (getDocuments() == null) {
            setDocuments(new LinkedList<Document>());
        }

        getDocuments().add(document);
    }

    public void removeDocument(Document document) {
        if (getDocuments() != null) {
            getDocuments().remove(document);
        }
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

