//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document attachment container)}
import de.smava.webapp.applicant.account.domain.DocumentAttachmentContainer;
import de.smava.webapp.applicant.account.domain.interfaces.DocumentAttachmentContainerEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentAttachmentContainers'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractDocumentAttachmentContainer
    extends BrokerageEntity implements DocumentAttachmentContainerEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDocumentAttachmentContainer.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof DocumentAttachmentContainer)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setProcessDate(((DocumentAttachmentContainer) oldEntity).getProcessDate());    
        this.setDate(((DocumentAttachmentContainer) oldEntity).getDate());    
        this.setName(((DocumentAttachmentContainer) oldEntity).getName());    
        this.setState(((DocumentAttachmentContainer) oldEntity).getState());    
        this.setDirection(((DocumentAttachmentContainer) oldEntity).getDirection());    
        this.setOriginalDocumentCount(((DocumentAttachmentContainer) oldEntity).getOriginalDocumentCount());    
        this.setDocumentAttachments(((DocumentAttachmentContainer) oldEntity).getDocumentAttachments());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof DocumentAttachmentContainer)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getProcessDate(), ((DocumentAttachmentContainer) otherEntity).getProcessDate());    
        equals = equals && valuesAreEqual(this.getDate(), ((DocumentAttachmentContainer) otherEntity).getDate());    
        equals = equals && valuesAreEqual(this.getName(), ((DocumentAttachmentContainer) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getState(), ((DocumentAttachmentContainer) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getDirection(), ((DocumentAttachmentContainer) otherEntity).getDirection());    
        equals = equals && valuesAreEqual(this.getOriginalDocumentCount(), ((DocumentAttachmentContainer) otherEntity).getOriginalDocumentCount());    
        equals = equals && valuesAreEqual(this.getDocumentAttachments(), ((DocumentAttachmentContainer) otherEntity).getDocumentAttachments());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(document attachment container)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

