//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(credentials)}
import de.smava.webapp.applicant.account.domain.history.CredentialsHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Credentialss'.
 *
 * 
 *
 * @author generator
 */
public class Credentials extends CredentialsHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(credentials)}
                        /**
    * Setter for the property 'password changed date'.
    */
    public void setPasswordChangedDate(Date passwordChangedDate) {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }

    /**
    * Returns the property 'password changed date'.
    *
    * 
    *
    */
    public Date getPasswordChangedDate() {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }
                                                        /**
    * Setter for the property 'password'.
    */
    public void setPassword(String password) {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }

    /**
    * Returns the property 'password'.
    *
    * 
    *
    */
    public String getPassword() {
        throw new RuntimeException("Please provide custom implementation as field marked 'custom' in model.xml");
    }
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _passwordChangedDate;
        protected String _identificationName;
        protected String _password;
        protected boolean _passwordChanged;
        protected PasswordAlgorithm _passwordAlgorithm;
        protected String _salt;
        
                                        /**
     * Setter for the property 'identification name'.
     *
     * 
     *
     */
    public void setIdentificationName(String identificationName) {
        if (!_identificationNameIsSet) {
            _identificationNameIsSet = true;
            _identificationNameInitVal = getIdentificationName();
        }
        registerChange("identification name", _identificationNameInitVal, identificationName);
        _identificationName = identificationName;
    }
                        
    /**
     * Returns the property 'identification name'.
     *
     * 
     *
     */
    public String getIdentificationName() {
        return _identificationName;
    }
                                                /**
     * Setter for the property 'password changed'.
     *
     * 
     *
     */
    public void setPasswordChanged(boolean passwordChanged) {
        if (!_passwordChangedIsSet) {
            _passwordChangedIsSet = true;
            _passwordChangedInitVal = getPasswordChanged();
        }
        registerChange("password changed", _passwordChangedInitVal, passwordChanged);
        _passwordChanged = passwordChanged;
    }
                        
    /**
     * Returns the property 'password changed'.
     *
     * 
     *
     */
    public boolean getPasswordChanged() {
        return _passwordChanged;
    }
                                            
    /**
     * Setter for the property 'password algorithm'.
     *
     * 
     *
     */
    public void setPasswordAlgorithm(PasswordAlgorithm passwordAlgorithm) {
        _passwordAlgorithm = passwordAlgorithm;
    }
            
    /**
     * Returns the property 'password algorithm'.
     *
     * 
     *
     */
    public PasswordAlgorithm getPasswordAlgorithm() {
        return _passwordAlgorithm;
    }
                                    /**
     * Setter for the property 'salt'.
     *
     * 
     *
     */
    public void setSalt(String salt) {
        if (!_saltIsSet) {
            _saltIsSet = true;
            _saltInitVal = getSalt();
        }
        registerChange("salt", _saltInitVal, salt);
        _salt = salt;
    }
                        
    /**
     * Returns the property 'salt'.
     *
     * 
     *
     */
    public String getSalt() {
        return _salt;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_passwordAlgorithm instanceof de.smava.webapp.commons.domain.Entity && !_passwordAlgorithm.getChangeSet().isEmpty()) {
             for (String element : _passwordAlgorithm.getChangeSet()) {
                 result.add("password algorithm : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Credentials.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _identificationName=").append(_identificationName);
            builder.append("\n    _password=").append(_password);
            builder.append("\n    _passwordChanged=").append(_passwordChanged);
            builder.append("\n    _salt=").append(_salt);
            builder.append("\n}");
        } else {
            builder.append(Credentials.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Credentials asCredentials() {
        return this;
    }
}
