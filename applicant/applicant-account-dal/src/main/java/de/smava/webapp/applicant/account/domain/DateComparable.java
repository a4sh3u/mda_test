package de.smava.webapp.applicant.account.domain;

/**
 * 
 * @author Dimitar Robev
 *
 */
public interface DateComparable {

	java.util.Date getDateToCompare();
}
