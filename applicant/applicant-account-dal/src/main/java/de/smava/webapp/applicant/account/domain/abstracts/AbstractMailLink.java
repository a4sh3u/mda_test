//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(mail link)}
import de.smava.webapp.applicant.account.domain.MailLink;
import de.smava.webapp.applicant.account.domain.interfaces.MailLinkEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MailLinks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMailLink
    extends BrokerageEntity implements MailLinkEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMailLink.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MailLink)) {
            return;
        }
        
        this.setContentId(((MailLink) oldEntity).getContentId());    
        this.setTargetUrl(((MailLink) oldEntity).getTargetUrl());    
        this.setParams(((MailLink) oldEntity).getParams());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setClickDate(((MailLink) oldEntity).getClickDate());    
        this.setClickCount(((MailLink) oldEntity).getClickCount());    
        this.setRecipient(((MailLink) oldEntity).getRecipient());    
        this.setSender(((MailLink) oldEntity).getSender());    
        this.setMailType(((MailLink) oldEntity).getMailType());    
        this.setLinkName(((MailLink) oldEntity).getLinkName());    
        this.setRemoteAddress(((MailLink) oldEntity).getRemoteAddress());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MailLink)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getContentId(), ((MailLink) otherEntity).getContentId());    
        equals = equals && valuesAreEqual(this.getTargetUrl(), ((MailLink) otherEntity).getTargetUrl());    
        equals = equals && valuesAreEqual(this.getParams(), ((MailLink) otherEntity).getParams());    
            
        equals = equals && valuesAreEqual(this.getClickDate(), ((MailLink) otherEntity).getClickDate());    
        equals = equals && valuesAreEqual(this.getClickCount(), ((MailLink) otherEntity).getClickCount());    
        equals = equals && valuesAreEqual(this.getRecipient(), ((MailLink) otherEntity).getRecipient());    
        equals = equals && valuesAreEqual(this.getSender(), ((MailLink) otherEntity).getSender());    
        equals = equals && valuesAreEqual(this.getMailType(), ((MailLink) otherEntity).getMailType());    
        equals = equals && valuesAreEqual(this.getLinkName(), ((MailLink) otherEntity).getLinkName());    
        equals = equals && valuesAreEqual(this.getRemoteAddress(), ((MailLink) otherEntity).getRemoteAddress());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(mail link)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

