//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.account.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(external customer accounts info)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.account.domain.ExternalCustomerAccountsInfo;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'ExternalCustomerAccountsInfos'.
 *
 * @author generator
 */
public interface ExternalCustomerAccountsInfoDao extends BrokerageSchemaDao<ExternalCustomerAccountsInfo> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the external customer accounts info identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    ExternalCustomerAccountsInfo getExternalCustomerAccountsInfo(Long id);

    /**
     * Saves the external customer accounts info.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveExternalCustomerAccountsInfo(ExternalCustomerAccountsInfo externalCustomerAccountsInfo);

    /**
     * Deletes an external customer accounts info, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the external customer accounts info
     */
    void deleteExternalCustomerAccountsInfo(Long id);

    /**
     * Retrieves all 'ExternalCustomerAccountsInfo' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<ExternalCustomerAccountsInfo> getExternalCustomerAccountsInfoList();

    /**
     * Retrieves a page of 'ExternalCustomerAccountsInfo' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<ExternalCustomerAccountsInfo> getExternalCustomerAccountsInfoList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'ExternalCustomerAccountsInfo' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<ExternalCustomerAccountsInfo> getExternalCustomerAccountsInfoList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'ExternalCustomerAccountsInfo' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<ExternalCustomerAccountsInfo> getExternalCustomerAccountsInfoList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'ExternalCustomerAccountsInfo' instances.
     */
    long getExternalCustomerAccountsInfoCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(external customer accounts info)}
    //
    // insert custom methods here

    /**
     * Extract ExternalCustomerAccountsInfo for given borrower and type
     *
     * @param borrower borrower to be searched for
     * @param externalCustomerAccountsInfoType {@link ExternalCustomerAccountsInfoType}
     * @return ExternalCustomerAccountsInfo object or NULL
     */
    ExternalCustomerAccountsInfo getExternalCustomerAccountsInfoForBorrowerAndType(Account borrower, ExternalCustomerAccountsInfoType externalCustomerAccountsInfoType);

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
