package de.smava.webapp.applicant.account.dao;

import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.dao.BaseDao;

import javax.transaction.Synchronization;
import java.util.Collection;

/**
 * DAO interface for the domain object 'Accounts'.
 *
 * @author generator
 */
public interface AccountDao extends BrokerageSchemaDao<Account> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the account identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Account getAccount(Long id);

    /**
     * Saves the account.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveAccount(Account account);

    /**
     * Deletes an account, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the account
     */
    void deleteAccount(Long id);

    /**
     * Retrieves all 'Account' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Account> getAccountList();

    /**
     * Returns the account for the given email adress.
     */
    Account findAccountByEmail(String email);

    /**
     * Returns account for given smavaId
     *
     * @param smavaId is a general account identifier (is also called customerNumber)
     * @return single account record or null
     */
    Account findAccountBySmavaId(Long smavaId);
}
