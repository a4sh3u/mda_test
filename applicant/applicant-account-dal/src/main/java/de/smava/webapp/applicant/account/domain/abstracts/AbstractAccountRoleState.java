//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account role state)}

import de.smava.webapp.applicant.account.domain.*;
import de.smava.webapp.applicant.account.domain.interfaces.AccountRoleStateEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.*;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AccountRoleStates'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAccountRoleState
    extends BrokerageEntity implements DateComparable,AccountRoleStateEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAccountRoleState.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof AccountRoleState)) {
            return;
        }
        
        this.setName(((AccountRoleState) oldEntity).getName());    
        this.setValidFrom(((AccountRoleState) oldEntity).getValidFrom());    
        this.setValidUntil(((AccountRoleState) oldEntity).getValidUntil());    
        this.setAccountRole(((AccountRoleState) oldEntity).getAccountRole());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof AccountRoleState)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((AccountRoleState) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getValidFrom(), ((AccountRoleState) otherEntity).getValidFrom());    
        equals = equals && valuesAreEqual(this.getValidUntil(), ((AccountRoleState) otherEntity).getValidUntil());    
        equals = equals && valuesAreEqual(this.getAccountRole(), ((AccountRoleState) otherEntity).getAccountRole());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(account role state)}
    private static final long serialVersionUID = 738781691927432906L;

    public static final String STATE_APPLIED = "APPLIED";
    public static final String STATE_ACCEPTED = "ACCEPTED";
    public static final String STATE_DENIED = "DENIED";
    public static final String STATE_VOID = "VOID";
    public static final String STATE_EXPIRED = "EXPIRED";

    public static final Set<String> FINAL_STATES = new HashSet<String>();
    static {
        FINAL_STATES.add(STATE_ACCEPTED);
        FINAL_STATES.add(STATE_DENIED);
    }



    public Date getDateToCompare() {
        return getValidFrom();
    }

    public List<? extends Entity> getModifier() {
        List<Account> result = Collections.emptyList();
        if (getAccountRole() != null) {
            Collections.singletonList(getAccountRole().getAccount());
        }
        return result;

    }

    public static AccountRoleState createAppliedAccountRoleState(AccountRole accountRole, Date creationDate) {
        AccountRoleState result = null;

        if (creationDate != null) {
            result = new AccountRoleState(STATE_APPLIED, creationDate, accountRole);
        } else {
            result = new AccountRoleState(STATE_APPLIED, CurrentDate.getDate(), accountRole);
        }

        return result;
    }

    public static AccountRoleState createAppliedAccountRoleState(AccountRole accountRole) {
        return createAppliedAccountRoleState(accountRole, CurrentDate.getDate());
    }

    public static AccountRoleState createAcceptedAccountRoleState(AccountRole accountRole) {
        return new AccountRoleState(STATE_ACCEPTED, CurrentDate.getDate(), accountRole);
    }


    public static AccountRoleState createVoidAccountRoleState(AccountRole accountRole) {
        return new AccountRoleState(STATE_VOID, CurrentDate.getDate(), accountRole);
    }

    public static AccountRoleState createDeniedAccountRoleState(AccountRole accountRole) {
        return new AccountRoleState(STATE_DENIED, CurrentDate.getDate(), accountRole);
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

