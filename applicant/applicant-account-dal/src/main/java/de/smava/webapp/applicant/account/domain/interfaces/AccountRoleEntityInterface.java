package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.account.domain.AccountRole;
import de.smava.webapp.applicant.account.domain.AccountRoleState;

import java.util.Set;


/**
 * The domain object that represents 'AccountRoles'.
 *
 * @author generator
 */
public interface AccountRoleEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'states'.
     *
     * 
     *
     */
    void setStates(Set<AccountRoleState> states);

    /**
     * Returns the property 'states'.
     *
     * 
     *
     */
    Set<AccountRoleState> getStates();
    /**
     * Helper method to get reference of this object as model type.
     */
    AccountRole asAccountRole();
}
