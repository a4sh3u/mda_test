//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document attachment container)}

import de.smava.webapp.applicant.account.domain.history.DocumentAttachmentContainerHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentAttachmentContainers'.
 *
 * 
 *
 * @author generator
 */
public class DocumentAttachmentContainer extends DocumentAttachmentContainerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(document attachment container)}
	/**
	 * generated serial.
	 */
	private static final long serialVersionUID = 71142750227408301L;


    public DocumentAttachmentContainer() {
        this._documentAttachments = new HashSet<Attachment>();
        this._creationDate = CurrentDate.getDate();
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Date _processDate;
        protected Date _date;
        protected String _name;
        protected String _state;
        protected String _direction;
        protected Integer _originalDocumentCount;
        protected Collection<Attachment> _documentAttachments;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'process date'.
     *
     * 
     *
     */
    public void setProcessDate(Date processDate) {
        if (!_processDateIsSet) {
            _processDateIsSet = true;
            _processDateInitVal = getProcessDate();
        }
        registerChange("process date", _processDateInitVal, processDate);
        _processDate = processDate;
    }
                        
    /**
     * Returns the property 'process date'.
     *
     * 
     *
     */
    public Date getProcessDate() {
        return _processDate;
    }
                                    /**
     * Setter for the property 'date'.
     *
     * 
     *
     */
    public void setDate(Date date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = getDate();
        }
        registerChange("date", _dateInitVal, date);
        _date = date;
    }
                        
    /**
     * Returns the property 'date'.
     *
     * 
     *
     */
    public Date getDate() {
        return _date;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'direction'.
     *
     * 
     *
     */
    public void setDirection(String direction) {
        if (!_directionIsSet) {
            _directionIsSet = true;
            _directionInitVal = getDirection();
        }
        registerChange("direction", _directionInitVal, direction);
        _direction = direction;
    }
                        
    /**
     * Returns the property 'direction'.
     *
     * 
     *
     */
    public String getDirection() {
        return _direction;
    }
                                    /**
     * Setter for the property 'original document count'.
     *
     * 
     *
     */
    public void setOriginalDocumentCount(Integer originalDocumentCount) {
        if (!_originalDocumentCountIsSet) {
            _originalDocumentCountIsSet = true;
            _originalDocumentCountInitVal = getOriginalDocumentCount();
        }
        registerChange("original document count", _originalDocumentCountInitVal, originalDocumentCount);
        _originalDocumentCount = originalDocumentCount;
    }
                        
    /**
     * Returns the property 'original document count'.
     *
     * 
     *
     */
    public Integer getOriginalDocumentCount() {
        return _originalDocumentCount;
    }
                                            
    /**
     * Setter for the property 'document attachments'.
     *
     * 
     *
     */
    public void setDocumentAttachments(Collection<Attachment> documentAttachments) {
        _documentAttachments = documentAttachments;
    }
            
    /**
     * Returns the property 'document attachments'.
     *
     * 
     *
     */
    public Collection<Attachment> getDocumentAttachments() {
        return _documentAttachments;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentAttachmentContainer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _direction=").append(_direction);
            builder.append("\n}");
        } else {
            builder.append(DocumentAttachmentContainer.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DocumentAttachmentContainer asDocumentAttachmentContainer() {
        return this;
    }
}
