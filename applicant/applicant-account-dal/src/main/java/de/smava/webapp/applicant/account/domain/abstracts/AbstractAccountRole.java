//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account role)}

import de.smava.webapp.applicant.account.domain.AccountRole;
import de.smava.webapp.applicant.account.domain.AccountRoleState;
import de.smava.webapp.applicant.account.domain.interfaces.AccountRoleEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.security.Role;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashSet;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AccountRoles'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAccountRole
    extends BrokerageEntity implements AccountRoleEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAccountRole.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof AccountRole)) {
            return;
        }
        
        this.setName(((AccountRole) oldEntity).getName());    
        this.setAccount(((AccountRole) oldEntity).getAccount());    
        this.setStates(((AccountRole) oldEntity).getStates());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof AccountRole)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((AccountRole) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getAccount(), ((AccountRole) otherEntity).getAccount());    
        equals = equals && valuesAreEqual(this.getStates(), ((AccountRole) otherEntity).getStates());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(account role)}
    private static final long serialVersionUID = -8494036444196493848L;

    public AccountRoleState getCurrentAccountRoleState() {
        AccountRoleState result = null;

        if (getStates() != null) {
            for (AccountRoleState state : getStates()) {
                if (state.getValidUntil() == null) {
                    result = state;
                    break;
                } else {
                    if (result == null) {
                        result = state;
                    } else if (result.getValidUntil() != null && state.getValidUntil().after(result.getValidUntil())) {
                        result = state;
                    }
                }
            }

        }

        return result;
    }

    public Role getRole() {
        return Enum.valueOf(Role.class, getName());
    }

    public void setCurrentAccountRoleState(AccountRoleState newState) {
        if (getStates() == null) {
            setStates(new HashSet<AccountRoleState>());
        }
        for (AccountRoleState state : getStates()) {
            if (state.getValidUntil() == null) {
                state.setValidUntil(new Date(newState.getValidFrom().getTime()));
            }
        }

        if (newState.getAccountRole() != this) {
            newState.setAccountRole(this.asAccountRole());
        }
        getStates().add(newState);
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

