package de.smava.webapp.applicant.account.domain;

import de.smava.webapp.applicant.account.domain.history.AccountHistory;
import de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType;
import org.apache.commons.collections.CollectionUtils;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * The domain object that represents 'Accounts'.
 *
 * 
 *
 * @author generator
 */
public class Account extends AccountHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(account)}
    public Account() {
        setState(USER_STATE_DEACTIVATED);
        setAccountRoles(new HashSet<AccountRole>());
        setCustomerAgreements(new HashSet<CustomerAgreement>());
        setExternalCustomerAccountsInfos(new HashSet<ExternalCustomerAccountsInfo>());
    }

    /**
     * Setter for the property 'email'.
     */
    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = getEmail();
        }
        registerChange("email", _emailInitVal, email);
        _email = email;
    }

    /**
     * Returns the property 'email'.
     */
    public String getEmail() {
        return _email;
    }

    /**
     * Returns the external customer account info based on type.
     *
     * @param type {@link ExternalCustomerAccountsInfoType}
     * @return ExternalCustomerAccountsInfo
     */
    public ExternalCustomerAccountsInfo retrieveExternalCustomerAccountsInfoByType(ExternalCustomerAccountsInfoType type) {
        ExternalCustomerAccountsInfo result = null;

        if(type != null && !CollectionUtils.isEmpty(getExternalCustomerAccountsInfos())){

            for(ExternalCustomerAccountsInfo info : getExternalCustomerAccountsInfos()){

                if(type.equals(info.getType())){
                    result = info;
                }
            }
        }

        return result;
    }

        protected Long _customerNumber;
        protected Date _creationDate;
        protected Credentials _credentials;
        protected String _state;
        protected Date _lockDate;
        protected String _email;
        protected Date _lastLogin;
        protected Date _emailValidatedDate;
        protected de.smava.webapp.applicant.domain.Applicant _accountOwner;
        protected Set<de.smava.webapp.applicant.domain.Applicant> _applicantHistory;
        protected Set<AccountRole> _accountRoles;
        protected List<Document> _documents;
        protected Set<CustomerAgreement> _customerAgreements;
        protected Date _receiveLoanOffers;
        protected Date _receiveConsolidationOffers;
        protected Set<ExternalCustomerAccountsInfo> _externalCustomerAccountsInfos;
        
                                    
    /**
     * Setter for the property 'customer number'.
     *
     * 
     *
     */
    public void setCustomerNumber(Long customerNumber) {
        _customerNumber = customerNumber;
    }
            
    /**
     * Returns the property 'customer number'.
     *
     * 
     *
     */
    public Long getCustomerNumber() {
        return _customerNumber;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'credentials'.
     *
     * 
     *
     */
    public void setCredentials(Credentials credentials) {
        _credentials = credentials;
    }
            
    /**
     * Returns the property 'credentials'.
     *
     * 
     *
     */
    public Credentials getCredentials() {
        return _credentials;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public String getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'lock date'.
     *
     * 
     *
     */
    public void setLockDate(Date lockDate) {
        if (!_lockDateIsSet) {
            _lockDateIsSet = true;
            _lockDateInitVal = getLockDate();
        }
        registerChange("lock date", _lockDateInitVal, lockDate);
        _lockDate = lockDate;
    }
                        
    /**
     * Returns the property 'lock date'.
     *
     * 
     *
     */
    public Date getLockDate() {
        return _lockDate;
    }
                                                /**
     * Setter for the property 'last login'.
     *
     * 
     *
     */
    public void setLastLogin(Date lastLogin) {
        if (!_lastLoginIsSet) {
            _lastLoginIsSet = true;
            _lastLoginInitVal = getLastLogin();
        }
        registerChange("last login", _lastLoginInitVal, lastLogin);
        _lastLogin = lastLogin;
    }
                        
    /**
     * Returns the property 'last login'.
     *
     * 
     *
     */
    public Date getLastLogin() {
        return _lastLogin;
    }
                                    /**
     * Setter for the property 'email validated date'.
     *
     * 
     *
     */
    public void setEmailValidatedDate(Date emailValidatedDate) {
        if (!_emailValidatedDateIsSet) {
            _emailValidatedDateIsSet = true;
            _emailValidatedDateInitVal = getEmailValidatedDate();
        }
        registerChange("email validated date", _emailValidatedDateInitVal, emailValidatedDate);
        _emailValidatedDate = emailValidatedDate;
    }
                        
    /**
     * Returns the property 'email validated date'.
     *
     * 
     *
     */
    public Date getEmailValidatedDate() {
        return _emailValidatedDate;
    }
                                            
    /**
     * Setter for the property 'account owner'.
     *
     * 
     *
     */
    public void setAccountOwner(de.smava.webapp.applicant.domain.Applicant accountOwner) {
        _accountOwner = accountOwner;
    }
            
    /**
     * Returns the property 'account owner'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Applicant getAccountOwner() {
        return _accountOwner;
    }
                                            
    /**
     * Setter for the property 'applicant history'.
     *
     * 
     *
     */
    public void setApplicantHistory(Set<de.smava.webapp.applicant.domain.Applicant> applicantHistory) {
        _applicantHistory = applicantHistory;
    }
            
    /**
     * Returns the property 'applicant history'.
     *
     * 
     *
     */
    public Set<de.smava.webapp.applicant.domain.Applicant> getApplicantHistory() {
        return _applicantHistory;
    }
                                            
    /**
     * Setter for the property 'account roles'.
     *
     * 
     *
     */
    public void setAccountRoles(Set<AccountRole> accountRoles) {
        _accountRoles = accountRoles;
    }
            
    /**
     * Returns the property 'account roles'.
     *
     * 
     *
     */
    public Set<AccountRole> getAccountRoles() {
        return _accountRoles;
    }
                                            
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    public void setDocuments(List<Document> documents) {
        _documents = documents;
    }
            
    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    public List<Document> getDocuments() {
        return _documents;
    }
                                            
    /**
     * Setter for the property 'customer agreements'.
     *
     * 
     *
     */
    public void setCustomerAgreements(Set<CustomerAgreement> customerAgreements) {
        _customerAgreements = customerAgreements;
    }
            
    /**
     * Returns the property 'customer agreements'.
     *
     * 
     *
     */
    public Set<CustomerAgreement> getCustomerAgreements() {
        return _customerAgreements;
    }
                                    /**
     * Setter for the property 'receive loan offers'.
     *
     * 
     *
     */
    public void setReceiveLoanOffers(Date receiveLoanOffers) {
        if (!_receiveLoanOffersIsSet) {
            _receiveLoanOffersIsSet = true;
            _receiveLoanOffersInitVal = getReceiveLoanOffers();
        }
        registerChange("receive loan offers", _receiveLoanOffersInitVal, receiveLoanOffers);
        _receiveLoanOffers = receiveLoanOffers;
    }
                        
    /**
     * Returns the property 'receive loan offers'.
     *
     * 
     *
     */
    public Date getReceiveLoanOffers() {
        return _receiveLoanOffers;
    }
                                    /**
     * Setter for the property 'receive consolidation offers'.
     *
     * 
     *
     */
    public void setReceiveConsolidationOffers(Date receiveConsolidationOffers) {
        if (!_receiveConsolidationOffersIsSet) {
            _receiveConsolidationOffersIsSet = true;
            _receiveConsolidationOffersInitVal = getReceiveConsolidationOffers();
        }
        registerChange("receive consolidation offers", _receiveConsolidationOffersInitVal, receiveConsolidationOffers);
        _receiveConsolidationOffers = receiveConsolidationOffers;
    }
                        
    /**
     * Returns the property 'receive consolidation offers'.
     *
     * 
     *
     */
    public Date getReceiveConsolidationOffers() {
        return _receiveConsolidationOffers;
    }
                                            
    /**
     * Setter for the property 'external customer accounts infos'.
     *
     * 
     *
     */
    public void setExternalCustomerAccountsInfos(Set<ExternalCustomerAccountsInfo> externalCustomerAccountsInfos) {
        _externalCustomerAccountsInfos = externalCustomerAccountsInfos;
    }
            
    /**
     * Returns the property 'external customer accounts infos'.
     *
     * 
     *
     */
    public Set<ExternalCustomerAccountsInfo> getExternalCustomerAccountsInfos() {
        return _externalCustomerAccountsInfos;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_credentials instanceof de.smava.webapp.commons.domain.Entity && !_credentials.getChangeSet().isEmpty()) {
             for (String element : _credentials.getChangeSet()) {
                 result.add("credentials : " + element);
             }
         }

         if (_accountOwner instanceof de.smava.webapp.commons.domain.Entity && !_accountOwner.getChangeSet().isEmpty()) {
             for (String element : _accountOwner.getChangeSet()) {
                 result.add("account owner : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Account.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _email=").append(_email);
            builder.append("\n}");
        } else {
            builder.append(Account.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Account asAccount() {
        return this;
    }
}
