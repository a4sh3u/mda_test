package de.smava.webapp.applicant.account.domain.history;



import de.smava.webapp.applicant.account.domain.abstracts.AbstractPasswordAlgorithm;




/**
 * The domain object that has all history aggregation related fields for 'PasswordAlgorithms'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class PasswordAlgorithmHistory extends AbstractPasswordAlgorithm {

    protected transient de.smava.webapp.applicant.account.type.PasswordAlgorithmType _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient Boolean _activeInitVal;
    protected transient boolean _activeIsSet;


	
    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.account.type.PasswordAlgorithmType typeInitVal() {
        de.smava.webapp.applicant.account.type.PasswordAlgorithmType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'active'.
     */
    public Boolean activeInitVal() {
        Boolean result;
        if (_activeIsSet) {
            result = _activeInitVal;
        } else {
            result = getActive();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'active'.
     */
    public boolean activeIsDirty() {
        return !valuesAreEqual(activeInitVal(), getActive());
    }

    /**
     * Returns true if the setter method was called for the property 'active'.
     */
    public boolean activeIsSet() {
        return _activeIsSet;
    }

}
