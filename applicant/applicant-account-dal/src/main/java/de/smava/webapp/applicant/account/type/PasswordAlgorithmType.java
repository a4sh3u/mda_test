package de.smava.webapp.applicant.account.type;

public enum PasswordAlgorithmType {

	MD5,
	SHA256,
	PBKDF2
}
