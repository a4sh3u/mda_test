//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(password algorithm)}
import de.smava.webapp.applicant.account.domain.PasswordAlgorithm;
import de.smava.webapp.applicant.account.domain.interfaces.PasswordAlgorithmEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PasswordAlgorithms'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractPasswordAlgorithm
    extends BrokerageEntity implements PasswordAlgorithmEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractPasswordAlgorithm.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof PasswordAlgorithm)) {
            return;
        }
        
        this.setType(((PasswordAlgorithm) oldEntity).getType());    
        this.setActive(((PasswordAlgorithm) oldEntity).getActive());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof PasswordAlgorithm)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getType(), ((PasswordAlgorithm) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getActive(), ((PasswordAlgorithm) otherEntity).getActive());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(password algorithm)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

