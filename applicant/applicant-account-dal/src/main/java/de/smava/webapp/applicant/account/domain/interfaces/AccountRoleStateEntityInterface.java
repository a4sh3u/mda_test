package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.AccountRole;
import de.smava.webapp.applicant.account.domain.AccountRoleState;

import java.util.Date;


/**
 * The domain object that represents 'AccountRoleStates'.
 *
 * @author generator
 */
public interface AccountRoleStateEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'valid from'.
     *
     * 
     *
     */
    void setValidFrom(Date validFrom);

    /**
     * Returns the property 'valid from'.
     *
     * 
     *
     */
    Date getValidFrom();
    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    void setValidUntil(Date validUntil);

    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    Date getValidUntil();
    /**
     * Setter for the property 'account role'.
     *
     * 
     *
     */
    void setAccountRole(AccountRole accountRole);

    /**
     * Returns the property 'account role'.
     *
     * 
     *
     */
    AccountRole getAccountRole();
    /**
     * Helper method to get reference of this object as model type.
     */
    AccountRoleState asAccountRoleState();
}
