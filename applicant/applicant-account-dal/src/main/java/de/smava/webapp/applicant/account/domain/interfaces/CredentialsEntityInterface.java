package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.Credentials;
import de.smava.webapp.applicant.account.domain.PasswordAlgorithm;

import java.util.Date;


/**
 * The domain object that represents 'Credentialss'.
 *
 * @author generator
 */
public interface CredentialsEntityInterface {

    /**
     * Setter for the property 'password changed date'.
     *
     * 
     *
     */
    void setPasswordChangedDate(Date passwordChangedDate);

    /**
     * Returns the property 'password changed date'.
     *
     * 
     *
     */
    Date getPasswordChangedDate();
    /**
     * Setter for the property 'identification name'.
     *
     * 
     *
     */
    void setIdentificationName(String identificationName);

    /**
     * Returns the property 'identification name'.
     *
     * 
     *
     */
    String getIdentificationName();
    /**
     * Setter for the property 'password'.
     *
     * 
     *
     */
    void setPassword(String password);

    /**
     * Returns the property 'password'.
     *
     * 
     *
     */
    String getPassword();
    /**
     * Setter for the property 'password changed'.
     *
     * 
     *
     */
    void setPasswordChanged(boolean passwordChanged);

    /**
     * Returns the property 'password changed'.
     *
     * 
     *
     */
    boolean getPasswordChanged();
    /**
     * Setter for the property 'password algorithm'.
     *
     * 
     *
     */
    void setPasswordAlgorithm(PasswordAlgorithm passwordAlgorithm);

    /**
     * Returns the property 'password algorithm'.
     *
     * 
     *
     */
    PasswordAlgorithm getPasswordAlgorithm();
    /**
     * Setter for the property 'salt'.
     *
     * 
     *
     */
    void setSalt(String salt);

    /**
     * Returns the property 'salt'.
     *
     * 
     *
     */
    String getSalt();
    /**
     * Helper method to get reference of this object as model type.
     */
    Credentials asCredentials();
}
