package de.smava.webapp.applicant.account.domain.history;



import de.smava.webapp.applicant.account.domain.abstracts.AbstractCustomerAgreement;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CustomerAgreements'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CustomerAgreementHistory extends AbstractCustomerAgreement {

    protected transient Date _agreedOnInitVal;
    protected transient boolean _agreedOnIsSet;
    protected transient Date _doubleOptInInitVal;
    protected transient boolean _doubleOptInIsSet;
    protected transient Date _disagreedOnInitVal;
    protected transient boolean _disagreedOnIsSet;
    protected transient String _versionInitVal;
    protected transient boolean _versionIsSet;
    protected transient String _noteInitVal;
    protected transient boolean _noteIsSet;
    protected transient de.smava.webapp.applicant.account.type.CustomerAgreementType _typeInitVal;
    protected transient boolean _typeIsSet;


	
    /**
     * Returns the initial value of the property 'agreed on'.
     */
    public Date agreedOnInitVal() {
        Date result;
        if (_agreedOnIsSet) {
            result = _agreedOnInitVal;
        } else {
            result = getAgreedOn();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'agreed on'.
     */
    public boolean agreedOnIsDirty() {
        return !valuesAreEqual(agreedOnInitVal(), getAgreedOn());
    }

    /**
     * Returns true if the setter method was called for the property 'agreed on'.
     */
    public boolean agreedOnIsSet() {
        return _agreedOnIsSet;
    }
	
    /**
     * Returns the initial value of the property 'double opt in'.
     */
    public Date doubleOptInInitVal() {
        Date result;
        if (_doubleOptInIsSet) {
            result = _doubleOptInInitVal;
        } else {
            result = getDoubleOptIn();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'double opt in'.
     */
    public boolean doubleOptInIsDirty() {
        return !valuesAreEqual(doubleOptInInitVal(), getDoubleOptIn());
    }

    /**
     * Returns true if the setter method was called for the property 'double opt in'.
     */
    public boolean doubleOptInIsSet() {
        return _doubleOptInIsSet;
    }
	
    /**
     * Returns the initial value of the property 'disagreed on'.
     */
    public Date disagreedOnInitVal() {
        Date result;
        if (_disagreedOnIsSet) {
            result = _disagreedOnInitVal;
        } else {
            result = getDisagreedOn();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'disagreed on'.
     */
    public boolean disagreedOnIsDirty() {
        return !valuesAreEqual(disagreedOnInitVal(), getDisagreedOn());
    }

    /**
     * Returns true if the setter method was called for the property 'disagreed on'.
     */
    public boolean disagreedOnIsSet() {
        return _disagreedOnIsSet;
    }
	
    /**
     * Returns the initial value of the property 'version'.
     */
    public String versionInitVal() {
        String result;
        if (_versionIsSet) {
            result = _versionInitVal;
        } else {
            result = getVersion();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'version'.
     */
    public boolean versionIsDirty() {
        return !valuesAreEqual(versionInitVal(), getVersion());
    }

    /**
     * Returns true if the setter method was called for the property 'version'.
     */
    public boolean versionIsSet() {
        return _versionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'note'.
     */
    public String noteInitVal() {
        String result;
        if (_noteIsSet) {
            result = _noteInitVal;
        } else {
            result = getNote();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'note'.
     */
    public boolean noteIsDirty() {
        return !valuesAreEqual(noteInitVal(), getNote());
    }

    /**
     * Returns true if the setter method was called for the property 'note'.
     */
    public boolean noteIsSet() {
        return _noteIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.account.type.CustomerAgreementType typeInitVal() {
        de.smava.webapp.applicant.account.type.CustomerAgreementType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
}
