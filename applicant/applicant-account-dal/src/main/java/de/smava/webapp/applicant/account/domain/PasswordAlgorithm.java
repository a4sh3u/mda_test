//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(password algorithm)}
import de.smava.webapp.applicant.account.domain.history.PasswordAlgorithmHistory;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PasswordAlgorithms'.
 *
 * 
 *
 * @author generator
 */
public class PasswordAlgorithm extends PasswordAlgorithmHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(password algorithm)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.account.type.PasswordAlgorithmType _type;
        protected Boolean _active;
        
                            /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.account.type.PasswordAlgorithmType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.type.PasswordAlgorithmType getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'active'.
     *
     * 
     *
     */
    public void setActive(Boolean active) {
        if (!_activeIsSet) {
            _activeIsSet = true;
            _activeInitVal = getActive();
        }
        registerChange("active", _activeInitVal, active);
        _active = active;
    }
                        
    /**
     * Returns the property 'active'.
     *
     * 
     *
     */
    public Boolean getActive() {
        return _active;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(PasswordAlgorithm.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(PasswordAlgorithm.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public PasswordAlgorithm asPasswordAlgorithm() {
        return this;
    }
}
