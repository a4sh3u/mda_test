//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(attachment)}

import de.smava.webapp.applicant.account.domain.history.AttachmentHistory;

import java.util.Collection;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Attachments'.
 *
 * 
 *
 * @author generator
 */
public class Attachment extends AttachmentHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(attachment)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected Long _attachmentId;
        protected String _name;
        protected String _description;
        protected String _contentType;
        protected String _location;
        protected Document _document;
        protected Collection<de.smava.webapp.applicant.account.domain.DocumentAttachmentContainer> _documentAttachmentContainer;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'attachment id'.
     *
     * 
     *
     */
    public void setAttachmentId(Long attachmentId) {
        _attachmentId = attachmentId;
    }
            
    /**
     * Returns the property 'attachment id'.
     *
     * 
     *
     */
    public Long getAttachmentId() {
        return _attachmentId;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'content type'.
     *
     * 
     *
     */
    public void setContentType(String contentType) {
        if (!_contentTypeIsSet) {
            _contentTypeIsSet = true;
            _contentTypeInitVal = getContentType();
        }
        registerChange("content type", _contentTypeInitVal, contentType);
        _contentType = contentType;
    }
                        
    /**
     * Returns the property 'content type'.
     *
     * 
     *
     */
    public String getContentType() {
        return _contentType;
    }
                                    /**
     * Setter for the property 'location'.
     *
     * 
     *
     */
    public void setLocation(String location) {
        if (!_locationIsSet) {
            _locationIsSet = true;
            _locationInitVal = getLocation();
        }
        registerChange("location", _locationInitVal, location);
        _location = location;
    }
                        
    /**
     * Returns the property 'location'.
     *
     * 
     *
     */
    public String getLocation() {
        return _location;
    }
                                            
    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    public void setDocument(Document document) {
        _document = document;
    }
            
    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    public Document getDocument() {
        return _document;
    }
                                            
    /**
     * Setter for the property 'document attachment container'.
     *
     * 
     *
     */
    public void setDocumentAttachmentContainer(Collection<de.smava.webapp.applicant.account.domain.DocumentAttachmentContainer> documentAttachmentContainer) {
        _documentAttachmentContainer = documentAttachmentContainer;
    }
            
    /**
     * Returns the property 'document attachment container'.
     *
     * 
     *
     */
    public Collection<de.smava.webapp.applicant.account.domain.DocumentAttachmentContainer> getDocumentAttachmentContainer() {
        return _documentAttachmentContainer;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_document instanceof de.smava.webapp.commons.domain.Entity && !_document.getChangeSet().isEmpty()) {
             for (String element : _document.getChangeSet()) {
                 result.add("document : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Attachment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _contentType=").append(_contentType);
            builder.append("\n    _location=").append(_location);
            builder.append("\n}");
        } else {
            builder.append(Attachment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Attachment asAttachment() {
        return this;
    }
}
