//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account role state)}
import de.smava.webapp.applicant.account.domain.history.AccountRoleStateHistory;
import java.util.Date;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AccountRoleStates'.
 *
 * 
 *
 * @author generator
 */
public class AccountRoleState extends AccountRoleStateHistory  implements DateComparable {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(account role state)}
    public AccountRoleState() {
        super();
    }

    public AccountRoleState(String name, java.util.Date validFrom, AccountRole accountRole) {
        setName(name);
        setValidFrom(validFrom);
        setValidUntil(null);
        setAccountRole(accountRole);
    }


    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected Date _validFrom;
        protected Date _validUntil;
        protected AccountRole _accountRole;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'valid from'.
     *
     * 
     *
     */
    public void setValidFrom(Date validFrom) {
        if (!_validFromIsSet) {
            _validFromIsSet = true;
            _validFromInitVal = getValidFrom();
        }
        registerChange("valid from", _validFromInitVal, validFrom);
        _validFrom = validFrom;
    }
                        
    /**
     * Returns the property 'valid from'.
     *
     * 
     *
     */
    public Date getValidFrom() {
        return _validFrom;
    }
                                    /**
     * Setter for the property 'valid until'.
     *
     * 
     *
     */
    public void setValidUntil(Date validUntil) {
        if (!_validUntilIsSet) {
            _validUntilIsSet = true;
            _validUntilInitVal = getValidUntil();
        }
        registerChange("valid until", _validUntilInitVal, validUntil);
        _validUntil = validUntil;
    }
                        
    /**
     * Returns the property 'valid until'.
     *
     * 
     *
     */
    public Date getValidUntil() {
        return _validUntil;
    }
                                            
    /**
     * Setter for the property 'account role'.
     *
     * 
     *
     */
    public void setAccountRole(AccountRole accountRole) {
        _accountRole = accountRole;
    }
            
    /**
     * Returns the property 'account role'.
     *
     * 
     *
     */
    public AccountRole getAccountRole() {
        return _accountRole;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_accountRole instanceof de.smava.webapp.commons.domain.Entity && !_accountRole.getChangeSet().isEmpty()) {
             for (String element : _accountRole.getChangeSet()) {
                 result.add("account role : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AccountRoleState.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(AccountRoleState.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AccountRoleState asAccountRoleState() {
        return this;
    }
}
