//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(document container)}

import de.smava.webapp.applicant.account.domain.history.DocumentContainerHistory;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.Date;
import java.util.List;

// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DocumentContainers'.
 *
 * 
 *
 * @author generator
 */
public class DocumentContainer extends DocumentContainerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(document container)}
    /**
     * Use {@link de.smava.webapp.account.domain.Document#STATE_OPEN} and {@link de.smava.webapp.account.domain.Document#STATE_SUCCESSFUL} to set state.
     */
    public void setState(String state) {
        setStateOnlyForDocumentsOfDispatchType(state, null);
    }

    @Override
    public String getState() {
        return _state;
    }


    public void setStateOnlyForDocumentsOfDispatchType(final String state, final String dispatchType) {
    	if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);

        _state = state;

        final List<Document> documents = getDocuments();
        if (documents != null) {
        	final Date now = CurrentDate.getDate();
            for (Document document : getDocuments()) {
            	if (dispatchType == null || dispatchType.equals(document.getTypeOfDispatch())) {
	                document.setState(state);
	                if (Document.STATE_SUCCESSFUL.equals(state) && document.getDate() == null) {
	                    document.setDate(now);
	                }
            	}
            }
        }
    }

    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected String _name;
        protected String _state;
        protected List<Document> _documents;
        protected String _type;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                                        
    /**
     * Setter for the property 'documents'.
     *
     * 
     *
     */
    public void setDocuments(List<Document> documents) {
        _documents = documents;
    }
            
    /**
     * Returns the property 'documents'.
     *
     * 
     *
     */
    public List<Document> getDocuments() {
        return _documents;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DocumentContainer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(DocumentContainer.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DocumentContainer asDocumentContainer() {
        return this;
    }
}
