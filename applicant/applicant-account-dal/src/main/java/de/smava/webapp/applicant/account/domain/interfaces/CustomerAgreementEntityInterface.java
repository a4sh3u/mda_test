package de.smava.webapp.applicant.account.domain.interfaces;



import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.account.domain.CustomerAgreement;

import java.util.Date;


/**
 * The domain object that represents 'CustomerAgreements'.
 *
 * @author generator
 */
public interface CustomerAgreementEntityInterface {

    /**
     * Setter for the property 'agreed on'.
     *
     * 
     *
     */
    void setAgreedOn(Date agreedOn);

    /**
     * Returns the property 'agreed on'.
     *
     * 
     *
     */
    Date getAgreedOn();
    /**
     * Setter for the property 'double opt in'.
     *
     * 
     *
     */
    void setDoubleOptIn(Date doubleOptIn);

    /**
     * Returns the property 'double opt in'.
     *
     * 
     *
     */
    Date getDoubleOptIn();
    /**
     * Setter for the property 'disagreed on'.
     *
     * 
     *
     */
    void setDisagreedOn(Date disagreedOn);

    /**
     * Returns the property 'disagreed on'.
     *
     * 
     *
     */
    Date getDisagreedOn();
    /**
     * Setter for the property 'version'.
     *
     * 
     *
     */
    void setVersion(String version);

    /**
     * Returns the property 'version'.
     *
     * 
     *
     */
    String getVersion();
    /**
     * Setter for the property 'note'.
     *
     * 
     *
     */
    void setNote(String note);

    /**
     * Returns the property 'note'.
     *
     * 
     *
     */
    String getNote();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.account.type.CustomerAgreementType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.type.CustomerAgreementType getType();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Helper method to get reference of this object as model type.
     */
    CustomerAgreement asCustomerAgreement();
}
