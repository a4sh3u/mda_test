//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(external customer accounts info)}
import de.smava.webapp.applicant.account.domain.history.ExternalCustomerAccountsInfoHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ExternalCustomerAccountsInfos'.
 *
 * 
 *
 * @author generator
 */
public class ExternalCustomerAccountsInfo extends ExternalCustomerAccountsInfoHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(external customer accounts info)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Account _account;
        protected de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType _type;
        protected String _externalId;
        protected Date _creationDate;
        protected Date _changeDate;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType type) {
        _type = type;
    }
            
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'external id'.
     *
     * 
     *
     */
    public void setExternalId(String externalId) {
        if (!_externalIdIsSet) {
            _externalIdIsSet = true;
            _externalIdInitVal = getExternalId();
        }
        registerChange("external id", _externalIdInitVal, externalId);
        _externalId = externalId;
    }
                        
    /**
     * Returns the property 'external id'.
     *
     * 
     *
     */
    public String getExternalId() {
        return _externalId;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'change date'.
     *
     * 
     *
     */
    public void setChangeDate(Date changeDate) {
        if (!_changeDateIsSet) {
            _changeDateIsSet = true;
            _changeDateInitVal = getChangeDate();
        }
        registerChange("change date", _changeDateInitVal, changeDate);
        _changeDate = changeDate;
    }
                        
    /**
     * Returns the property 'change date'.
     *
     * 
     *
     */
    public Date getChangeDate() {
        return _changeDate;
    }
            
    /**
    * A string representation of this object. Mainly for debugging.
    */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ExternalCustomerAccountsInfo.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _externalId=").append(_externalId);
            builder.append("\n}");
        } else {
            builder.append(ExternalCustomerAccountsInfo.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ExternalCustomerAccountsInfo asExternalCustomerAccountsInfo() {
        return this;
    }
}
