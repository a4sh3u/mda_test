package de.smava.webapp.applicant.account.domain.interfaces;

import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.account.domain.ExternalCustomerAccountsInfo;

import java.util.Date;

/**
 * The domain object that represents 'ExternalCustomerAccountsInfos'.
 *
 * @author generator
 */
public interface ExternalCustomerAccountsInfoEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.type.ExternalCustomerAccountsInfoType getType();
    /**
     * Setter for the property 'external id'.
     *
     * 
     *
     */
    void setExternalId(String externalId);

    /**
     * Returns the property 'external id'.
     *
     * 
     *
     */
    String getExternalId();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'change date'.
     *
     * 
     *
     */
    void setChangeDate(Date changeDate);

    /**
     * Returns the property 'change date'.
     *
     * 
     *
     */
    Date getChangeDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    ExternalCustomerAccountsInfo asExternalCustomerAccountsInfo();
}