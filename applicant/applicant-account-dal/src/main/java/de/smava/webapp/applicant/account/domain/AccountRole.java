//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(account role)}

import de.smava.webapp.applicant.account.domain.history.AccountRoleHistory;

import java.util.Set;

@com.fasterxml.jackson.annotation.JsonIdentityInfo(generator = com.fasterxml.jackson.annotation.ObjectIdGenerators.IntSequenceGenerator.class, property = "@accountRoleId")
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AccountRoles'.
 *
 * 
 *
 * @author generator
 */
public class AccountRole extends AccountRoleHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(account role)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected Account _account;
        protected Set<AccountRoleState> _states;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'states'.
     *
     * 
     *
     */
    public void setStates(Set<AccountRoleState> states) {
        _states = states;
    }
            
    /**
     * Returns the property 'states'.
     *
     * 
     *
     */
    public Set<AccountRoleState> getStates() {
        return _states;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AccountRole.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(AccountRole.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AccountRole asAccountRole() {
        return this;
    }
}
