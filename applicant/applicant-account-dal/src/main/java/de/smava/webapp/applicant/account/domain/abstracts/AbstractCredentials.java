//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(credentials)}

import de.smava.webapp.applicant.account.domain.Credentials;
import de.smava.webapp.applicant.account.domain.interfaces.CredentialsEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Credentialss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCredentials
    extends BrokerageEntity implements CredentialsEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCredentials.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Credentials)) {
            return;
        }
        
        this.setPasswordChangedDate(((Credentials) oldEntity).getPasswordChangedDate());    
        this.setIdentificationName(((Credentials) oldEntity).getIdentificationName());    
        this.setPassword(((Credentials) oldEntity).getPassword());    
        this.setPasswordChanged(((Credentials) oldEntity).getPasswordChanged());    
        this.setPasswordAlgorithm(((Credentials) oldEntity).getPasswordAlgorithm());    
        this.setSalt(((Credentials) oldEntity).getSalt());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Credentials)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getPasswordChangedDate(), ((Credentials) otherEntity).getPasswordChangedDate());    
        equals = equals && valuesAreEqual(this.getIdentificationName(), ((Credentials) otherEntity).getIdentificationName());    
        equals = equals && valuesAreEqual(this.getPassword(), ((Credentials) otherEntity).getPassword());    
        equals = equals && valuesAreEqual(this.getPasswordChanged(), ((Credentials) otherEntity).getPasswordChanged());    
        equals = equals && valuesAreEqual(this.getPasswordAlgorithm(), ((Credentials) otherEntity).getPasswordAlgorithm());    
        equals = equals && valuesAreEqual(this.getSalt(), ((Credentials) otherEntity).getSalt());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(credentials)}
    private static final Md5PasswordEncoder MD5_ENCODER = new Md5PasswordEncoder();


    //    /**
//     * Saltsource provides the password salt.
//     */
    public static boolean checkMd5EncodedTPin(String encodedString, String plainString, SaltSource saltSource) {
        return MD5_ENCODER.isPasswordValid(encodedString, plainString, saltSource.getSalt(null));
    }
    //
    public static String encodeTPin(String password, SaltSource saltSource) {
        return MD5_ENCODER.encodePassword(password, saltSource.getSalt(null));
    }


    // !!!!!!!! End of insert code section !!!!!!!!
}

