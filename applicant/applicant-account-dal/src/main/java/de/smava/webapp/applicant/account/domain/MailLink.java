//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.account.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(mail link)}
import de.smava.webapp.applicant.account.domain.history.MailLinkHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MailLinks'.
 *
 * 
 *
 * @author generator
 */
public class MailLink extends MailLinkHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(mail link)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected int _contentId;
        protected String _targetUrl;
        protected String _params;
        protected Date _creationDate;
        protected Date _clickDate;
        protected int _clickCount;
        protected Account _recipient;
        protected Account _sender;
        protected String _mailType;
        protected String _linkName;
        protected String _remoteAddress;
        
                            /**
     * Setter for the property 'content id'.
     *
     * 
     *
     */
    public void setContentId(int contentId) {
        if (!_contentIdIsSet) {
            _contentIdIsSet = true;
            _contentIdInitVal = getContentId();
        }
        registerChange("content id", _contentIdInitVal, contentId);
        _contentId = contentId;
    }
                        
    /**
     * Returns the property 'content id'.
     *
     * 
     *
     */
    public int getContentId() {
        return _contentId;
    }
                                    /**
     * Setter for the property 'target url'.
     *
     * 
     *
     */
    public void setTargetUrl(String targetUrl) {
        if (!_targetUrlIsSet) {
            _targetUrlIsSet = true;
            _targetUrlInitVal = getTargetUrl();
        }
        registerChange("target url", _targetUrlInitVal, targetUrl);
        _targetUrl = targetUrl;
    }
                        
    /**
     * Returns the property 'target url'.
     *
     * 
     *
     */
    public String getTargetUrl() {
        return _targetUrl;
    }
                                    /**
     * Setter for the property 'params'.
     *
     * 
     *
     */
    public void setParams(String params) {
        if (!_paramsIsSet) {
            _paramsIsSet = true;
            _paramsInitVal = getParams();
        }
        registerChange("params", _paramsInitVal, params);
        _params = params;
    }
                        
    /**
     * Returns the property 'params'.
     *
     * 
     *
     */
    public String getParams() {
        return _params;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'click date'.
     *
     * 
     *
     */
    public void setClickDate(Date clickDate) {
        if (!_clickDateIsSet) {
            _clickDateIsSet = true;
            _clickDateInitVal = getClickDate();
        }
        registerChange("click date", _clickDateInitVal, clickDate);
        _clickDate = clickDate;
    }
                        
    /**
     * Returns the property 'click date'.
     *
     * 
     *
     */
    public Date getClickDate() {
        return _clickDate;
    }
                                    /**
     * Setter for the property 'click count'.
     *
     * 
     *
     */
    public void setClickCount(int clickCount) {
        if (!_clickCountIsSet) {
            _clickCountIsSet = true;
            _clickCountInitVal = getClickCount();
        }
        registerChange("click count", _clickCountInitVal, clickCount);
        _clickCount = clickCount;
    }
                        
    /**
     * Returns the property 'click count'.
     *
     * 
     *
     */
    public int getClickCount() {
        return _clickCount;
    }
                                            
    /**
     * Setter for the property 'recipient'.
     *
     * 
     *
     */
    public void setRecipient(Account recipient) {
        _recipient = recipient;
    }
            
    /**
     * Returns the property 'recipient'.
     *
     * 
     *
     */
    public Account getRecipient() {
        return _recipient;
    }
                                            
    /**
     * Setter for the property 'sender'.
     *
     * 
     *
     */
    public void setSender(Account sender) {
        _sender = sender;
    }
            
    /**
     * Returns the property 'sender'.
     *
     * 
     *
     */
    public Account getSender() {
        return _sender;
    }
                                    /**
     * Setter for the property 'mail type'.
     *
     * 
     *
     */
    public void setMailType(String mailType) {
        if (!_mailTypeIsSet) {
            _mailTypeIsSet = true;
            _mailTypeInitVal = getMailType();
        }
        registerChange("mail type", _mailTypeInitVal, mailType);
        _mailType = mailType;
    }
                        
    /**
     * Returns the property 'mail type'.
     *
     * 
     *
     */
    public String getMailType() {
        return _mailType;
    }
                                    /**
     * Setter for the property 'link name'.
     *
     * 
     *
     */
    public void setLinkName(String linkName) {
        if (!_linkNameIsSet) {
            _linkNameIsSet = true;
            _linkNameInitVal = getLinkName();
        }
        registerChange("link name", _linkNameInitVal, linkName);
        _linkName = linkName;
    }
                        
    /**
     * Returns the property 'link name'.
     *
     * 
     *
     */
    public String getLinkName() {
        return _linkName;
    }
                                    /**
     * Setter for the property 'remote address'.
     *
     * 
     *
     */
    public void setRemoteAddress(String remoteAddress) {
        if (!_remoteAddressIsSet) {
            _remoteAddressIsSet = true;
            _remoteAddressInitVal = getRemoteAddress();
        }
        registerChange("remote address", _remoteAddressInitVal, remoteAddress);
        _remoteAddress = remoteAddress;
    }
                        
    /**
     * Returns the property 'remote address'.
     *
     * 
     *
     */
    public String getRemoteAddress() {
        return _remoteAddress;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_recipient instanceof de.smava.webapp.commons.domain.Entity && !_recipient.getChangeSet().isEmpty()) {
             for (String element : _recipient.getChangeSet()) {
                 result.add("recipient : " + element);
             }
         }

         if (_sender instanceof de.smava.webapp.commons.domain.Entity && !_sender.getChangeSet().isEmpty()) {
             for (String element : _sender.getChangeSet()) {
                 result.add("sender : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MailLink.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _contentId=").append(_contentId);
            builder.append("\n    _targetUrl=").append(_targetUrl);
            builder.append("\n    _params=").append(_params);
            builder.append("\n    _clickCount=").append(_clickCount);
            builder.append("\n    _mailType=").append(_mailType);
            builder.append("\n    _linkName=").append(_linkName);
            builder.append("\n    _remoteAddress=").append(_remoteAddress);
            builder.append("\n}");
        } else {
            builder.append(MailLink.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public MailLink asMailLink() {
        return this;
    }
}
