package de.smava.webapp.applicant.account.builder;

import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.builder.EntityBuilder;


import java.util.Date;

/**
 * Created by atomecki on 2015-10-08.
 */
public class AccountEntityBuilder extends EntityBuilder<Account> {

    protected Account createEntity() {
        return new Account();
    }

    public static AccountEntityBuilder aBuilder() {
        return new AccountEntityBuilder();
    }

    public AccountEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(new Date());
        entity.setEmail("test.account@smava.de");

        return this;
    }
}
