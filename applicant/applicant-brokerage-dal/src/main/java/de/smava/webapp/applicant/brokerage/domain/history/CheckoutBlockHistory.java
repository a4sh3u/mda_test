package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractCheckoutBlock;




/**
 * The domain object that has all history aggregation related fields for 'CheckoutBlocks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CheckoutBlockHistory extends AbstractCheckoutBlock {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient Integer _sortOrderInitVal;
    protected transient boolean _sortOrderIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'sort order'.
     */
    public Integer sortOrderInitVal() {
        Integer result;
        if (_sortOrderIsSet) {
            result = _sortOrderInitVal;
        } else {
            result = getSortOrder();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'sort order'.
     */
    public boolean sortOrderIsDirty() {
        return !valuesAreEqual(sortOrderInitVal(), getSortOrder());
    }

    /**
     * Returns true if the setter method was called for the property 'sort order'.
     */
    public boolean sortOrderIsSet() {
        return _sortOrderIsSet;
    }
	
}
