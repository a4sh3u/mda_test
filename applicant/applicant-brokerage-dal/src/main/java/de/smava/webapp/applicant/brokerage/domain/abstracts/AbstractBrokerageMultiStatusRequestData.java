//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage multi status request data)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageMultiStatusRequestData;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageMultiStatusRequestDataEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMultiStatusRequestDatas'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 * <p>
 * Data related to multi-status request to bank
 *
 * @author generator
 */
public abstract class AbstractBrokerageMultiStatusRequestData
        extends BrokerageEntity implements BrokerageMultiStatusRequestDataEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageMultiStatusRequestData.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageMultiStatusRequestData)) {
            return;
        }

        this.setCreationDate(CurrentDate.getDate());
        this.setBrokerageBank(((BrokerageMultiStatusRequestData) oldEntity).getBrokerageBank());
        this.setState(((BrokerageMultiStatusRequestData) oldEntity).getState());
        this.setRequest(((BrokerageMultiStatusRequestData) oldEntity).getRequest());
        this.setResponse(((BrokerageMultiStatusRequestData) oldEntity).getResponse());
        this.setHttpState(((BrokerageMultiStatusRequestData) oldEntity).getHttpState());

    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageMultiStatusRequestData)) {
            equals = false;
        }


        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((BrokerageMultiStatusRequestData) otherEntity).getBrokerageBank());
        equals = equals && valuesAreEqual(this.getState(), ((BrokerageMultiStatusRequestData) otherEntity).getState());
        equals = equals && valuesAreEqual(this.getRequest(), ((BrokerageMultiStatusRequestData) otherEntity).getRequest());
        equals = equals && valuesAreEqual(this.getResponse(), ((BrokerageMultiStatusRequestData) otherEntity).getResponse());
        equals = equals && valuesAreEqual(this.getHttpState(), ((BrokerageMultiStatusRequestData) otherEntity).getHttpState());

        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage multi status request data)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

