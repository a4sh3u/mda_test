package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.BrokerageFilterReasonMultiValDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageFilterReasonMultiVal;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author Adam Budzinski
 * @since 05.09.2017
 */
@Repository(value = "brokerageFilterReasonMultiValDao")
public class JdoBrokerageFilterReasonMultiValDao extends JdoBaseDao implements BrokerageFilterReasonMultiValDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageFilterReasonMultiValDao.class);

    @Override
    public BrokerageFilterReasonMultiVal load(Long id) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("BrokerageFilterReasonMultiVal load for id " + id);
        }

        BrokerageFilterReasonMultiVal result = getEntity(BrokerageFilterReasonMultiVal.class, id);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("BrokerageFilterReasonMultiVal load result " + result);
        }
        return result;
    }

    @Override
    public Long save(BrokerageFilterReasonMultiVal brokerageFilterReasonMultiVal) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageFilterReasonMultiVal: " + brokerageFilterReasonMultiVal);
        }

        return saveEntity(brokerageFilterReasonMultiVal);
    }

    @Override
    public boolean exists(Long id) {
        boolean result = false;

        if (id != null) {
            BrokerageFilterReasonMultiVal entity = findUniqueEntity(BrokerageFilterReasonMultiVal.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }

        return result;
    }

    @Override
    public Collection<BrokerageFilterReasonMultiVal> getByBrokerageApplicationId(Long brokerageApplicationId) {

        String where = OqlTerm.newTerm().equals("_brokerageApplication._id", brokerageApplicationId).toString();

        Sortable sortable = new Sortable() {
            @Override
            public String getSort() {
                return "_id";
            }
            @Override
            public boolean hasSort() {
                return true;
            }
            @Override
            public boolean sortDescending() {
                return true;
            }
        };

        return findEntities(BrokerageFilterReasonMultiVal.class, where, sortable);

    }

    @Override
    public void deleteMainReasonEntriesForBa(Long brokerageApplicationId) {

        for (BrokerageFilterReasonMultiVal entity : getByBrokerageApplicationId(brokerageApplicationId)) {

            if (entity.getApplicantId() == null) {
                try {
                    deleteEntity(BrokerageFilterReasonMultiVal.class, entity.getId());
                } catch (Exception e) {
                    LOGGER.warn(String.format("Could not remove reason code, entry: %s", entity), e);
                }
            }
        }
    }
}
