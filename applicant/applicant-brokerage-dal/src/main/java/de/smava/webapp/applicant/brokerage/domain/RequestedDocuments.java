//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(requested documents)}
import de.smava.webapp.applicant.brokerage.domain.history.RequestedDocumentsHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RequestedDocumentss'.
 *
 * 
 *
 * @author generator
 */
public class RequestedDocuments extends RequestedDocumentsHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(requested documents)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected Integer _numberOfDocuments;
        protected boolean _required;
        protected de.smava.webapp.applicant.brokerage.domain.DocumentCategory _type;
        protected de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration _documentConfiguration;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'number of documents'.
     *
     * 
     *
     */
    public void setNumberOfDocuments(Integer numberOfDocuments) {
        if (!_numberOfDocumentsIsSet) {
            _numberOfDocumentsIsSet = true;
            _numberOfDocumentsInitVal = getNumberOfDocuments();
        }
        registerChange("number of documents", _numberOfDocumentsInitVal, numberOfDocuments);
        _numberOfDocuments = numberOfDocuments;
    }
                        
    /**
     * Returns the property 'number of documents'.
     *
     * 
     *
     */
    public Integer getNumberOfDocuments() {
        return _numberOfDocuments;
    }
                                    /**
     * Setter for the property 'required'.
     *
     * 
     *
     */
    public void setRequired(boolean required) {
        if (!_requiredIsSet) {
            _requiredIsSet = true;
            _requiredInitVal = getRequired();
        }
        registerChange("required", _requiredInitVal, required);
        _required = required;
    }
                        
    /**
     * Returns the property 'required'.
     *
     * 
     *
     */
    public boolean getRequired() {
        return _required;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.brokerage.domain.DocumentCategory type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentCategory getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'document configuration'.
     *
     * 
     *
     */
    public void setDocumentConfiguration(de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration documentConfiguration) {
        _documentConfiguration = documentConfiguration;
    }
            
    /**
     * Returns the property 'document configuration'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration getDocumentConfiguration() {
        return _documentConfiguration;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_documentConfiguration instanceof de.smava.webapp.commons.domain.Entity && !_documentConfiguration.getChangeSet().isEmpty()) {
             for (String element : _documentConfiguration.getChangeSet()) {
                 result.add("document configuration : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RequestedDocuments.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _required=").append(_required);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(RequestedDocuments.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RequestedDocuments asRequestedDocuments() {
        return this;
    }
}
