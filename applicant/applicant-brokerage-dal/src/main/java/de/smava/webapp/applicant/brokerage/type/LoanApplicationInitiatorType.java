package de.smava.webapp.applicant.brokerage.type;

/**
 * The creator of the loan application.
 *
 * Works in conjunction with the LoanApplication#initiatorTool what is the source
 * (route name fro registration route or casi for credit advisor)
 */
public enum LoanApplicationInitiatorType {
    /**
     * from registration route
     */
	CUSTOMER,

    /**
     * by smava people
     */
    CREDIT_ADVISOR;
}
