package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.AdvisorGroupDao;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'AdvisorGroups'.
 */
@Repository(value = "applicantAdvisorGroupDao")
public class JdoAdvisorGroupDao extends JdoGenericDao<AdvisorGroup> implements AdvisorGroupDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAdvisorGroupDao.class);

    private static final String CLASS_NAME = "AdvisorGroup";
    private static final String STRING_GET = "get";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";

    /**
     * Returns an attached copy of the advisor group identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public AdvisorGroup load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        AdvisorGroup result = getPersistenceManager().getObjectById(AdvisorGroup.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        return getPersistenceManager().getObjectById(AdvisorGroup.class, id) != null;
    }

    /**
     * Saves the advisor group.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(AdvisorGroup advisorGroup) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAdvisorGroup: " + advisorGroup);
        }
        return saveEntity(advisorGroup);
    }

    /**
     * Deletes an advisor group, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the advisor group
     */
    public void deleteAdvisorGroup(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAdvisorGroup: " + id);
        }
        final PersistenceManager pm = getPersistenceManager();
        AdvisorGroup entity = pm.getObjectById(AdvisorGroup.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Deleting AdvisorGroup: " + entity);
        }

        pm.deletePersistent(entity);
        pm.evict(entity);
    }

    /**
     * Retrieves all 'AdvisorGroup' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<AdvisorGroup> getAdvisorGroupList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        @SuppressWarnings("unchecked")
        Collection<AdvisorGroup> result = (Collection<AdvisorGroup>) getPersistenceManager().newQuery(AdvisorGroup.class).execute();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    @Override
    public Collection<AdvisorGroup> findActiveAdvisorGroups() {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroup.class, "findActiveAdvisorGroups");

        @SuppressWarnings("unchecked")
        Collection<AdvisorGroup> advisorGroups = (Collection<AdvisorGroup>) query.execute();
        if (advisorGroups == null) {
            return new ArrayList<AdvisorGroup>();
        }
        return advisorGroups;
    }

    @Override
    public Collection<AdvisorGroup> findAdvisorGroupsForAdvisorId(Long advisorId) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroup.class, "findAdvisorGroupsForAdvisorId");

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, advisorId);

        @SuppressWarnings("unchecked")
        Collection<AdvisorGroup> advisorGroups = (Collection<AdvisorGroup>) query.executeWithMap(parameters);
        if (advisorGroups == null) {
            return new ArrayList<AdvisorGroup>();
        }
        return advisorGroups;
    }

    @Override
    public AdvisorGroup findByName(String name) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroup.class, "findByName");
        query.setUnique(true);

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, name);

        return (AdvisorGroup) query.executeWithMap(parameters);
    }

    @Override
    public AdvisorGroup findByConditionLiteral(String condition) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroup.class, "findByConditionLiteral");
        query.setUnique(true);

        Map<Integer, Object> parameters = Collections.<Integer, Object>singletonMap(1, condition);
        return (AdvisorGroup) query.executeWithMap(parameters);
    }

    @Override
    public void disableAdvisorGroup(Long id) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroup.class, "disableAdvisorGroup");

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, id);

        query.executeWithMap(parameters);
    }

}
