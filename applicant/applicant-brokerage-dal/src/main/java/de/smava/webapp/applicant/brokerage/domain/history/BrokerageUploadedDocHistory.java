package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageUploadedDoc;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageUploadedDocs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageUploadedDocHistory extends AbstractBrokerageUploadedDoc {

    protected transient de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus _statusInitVal;
    protected transient boolean _statusIsSet;
    protected transient Date _sendDateInitVal;
    protected transient boolean _sendDateIsSet;
    protected transient Date _receiveDateInitVal;
    protected transient boolean _receiveDateIsSet;
    protected transient Date _deletionDateInitVal;
    protected transient boolean _deletionDateIsSet;
    protected transient Boolean _approvedInitVal;
    protected transient boolean _approvedIsSet;


			
    /**
     * Returns the initial value of the property 'status'.
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus statusInitVal() {
        de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus result;
        if (_statusIsSet) {
            result = _statusInitVal;
        } else {
            result = getStatus();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'status'.
     */
    public boolean statusIsDirty() {
        return !valuesAreEqual(statusInitVal(), getStatus());
    }

    /**
     * Returns true if the setter method was called for the property 'status'.
     */
    public boolean statusIsSet() {
        return _statusIsSet;
    }
	
    /**
     * Returns the initial value of the property 'send date'.
     */
    public Date sendDateInitVal() {
        Date result;
        if (_sendDateIsSet) {
            result = _sendDateInitVal;
        } else {
            result = getSendDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'send date'.
     */
    public boolean sendDateIsDirty() {
        return !valuesAreEqual(sendDateInitVal(), getSendDate());
    }

    /**
     * Returns true if the setter method was called for the property 'send date'.
     */
    public boolean sendDateIsSet() {
        return _sendDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'receive date'.
     */
    public Date receiveDateInitVal() {
        Date result;
        if (_receiveDateIsSet) {
            result = _receiveDateInitVal;
        } else {
            result = getReceiveDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'receive date'.
     */
    public boolean receiveDateIsDirty() {
        return !valuesAreEqual(receiveDateInitVal(), getReceiveDate());
    }

    /**
     * Returns true if the setter method was called for the property 'receive date'.
     */
    public boolean receiveDateIsSet() {
        return _receiveDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'deletion date'.
     */
    public Date deletionDateInitVal() {
        Date result;
        if (_deletionDateIsSet) {
            result = _deletionDateInitVal;
        } else {
            result = getDeletionDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'deletion date'.
     */
    public boolean deletionDateIsDirty() {
        return !valuesAreEqual(deletionDateInitVal(), getDeletionDate());
    }

    /**
     * Returns true if the setter method was called for the property 'deletion date'.
     */
    public boolean deletionDateIsSet() {
        return _deletionDateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'approved'.
     */
    public Boolean approvedInitVal() {
        Boolean result;
        if (_approvedIsSet) {
            result = _approvedInitVal;
        } else {
            result = getApproved();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'approved'.
     */
    public boolean approvedIsDirty() {
        return !valuesAreEqual(approvedInitVal(), getApproved());
    }

    /**
     * Returns true if the setter method was called for the property 'approved'.
     */
    public boolean approvedIsSet() {
        return _approvedIsSet;
    }

}
