package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationCheckoutStatus;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageApplicationCheckoutStatuss'.
 *
 * @author generator
 */
public interface BrokerageApplicationCheckoutStatusEntityInterface {

    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    void setBrokerageApplicationId(Long brokerageApplicationId);

    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    Long getBrokerageApplicationId();
    /**
     * Setter for the property 'current checkout flow'.
     *
     * 
     *
     */
    void setCurrentCheckoutFlow(de.smava.webapp.applicant.brokerage.domain.CheckoutFlow currentCheckoutFlow);

    /**
     * Returns the property 'current checkout flow'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.CheckoutFlow getCurrentCheckoutFlow();
    /**
     * Setter for the property 'current checkout flow page'.
     *
     * 
     *
     */
    void setCurrentCheckoutFlowPage(de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage currentCheckoutFlowPage);

    /**
     * Returns the property 'current checkout flow page'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage getCurrentCheckoutFlowPage();
    /**
     * Setter for the property 'update timestamp'.
     *
     * 
     *
     */
    void setUpdateTimestamp(Date updateTimestamp);

    /**
     * Returns the property 'update timestamp'.
     *
     * 
     *
     */
    Date getUpdateTimestamp();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationCheckoutStatus asBrokerageApplicationCheckoutStatus();
}
