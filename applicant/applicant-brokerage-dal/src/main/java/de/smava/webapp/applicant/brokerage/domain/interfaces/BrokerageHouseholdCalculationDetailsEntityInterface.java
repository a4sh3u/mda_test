package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation;
import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculationDetails;


/**
 * The domain object that represents 'BrokerageHouseholdCalculationDetailss'.
 *
 * @author generator
 */
public interface BrokerageHouseholdCalculationDetailsEntityInterface {

    /**
     * Setter for the property 'brokerage household calculation'.
     *
     * 
     *
     */
    void setBrokerageHouseholdCalculation(BrokerageHouseholdCalculation brokerageHouseholdCalculation);

    /**
     * Returns the property 'brokerage household calculation'.
     *
     * 
     *
     */
    BrokerageHouseholdCalculation getBrokerageHouseholdCalculation();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType getType();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName getName();
    /**
     * Setter for the property 'first applicant value'.
     *
     * 
     *
     */
    void setFirstApplicantValue(Double firstApplicantValue);

    /**
     * Returns the property 'first applicant value'.
     *
     * 
     *
     */
    Double getFirstApplicantValue();
    /**
     * Setter for the property 'second applicant value'.
     *
     * 
     *
     */
    void setSecondApplicantValue(Double secondApplicantValue);

    /**
     * Returns the property 'second applicant value'.
     *
     * 
     *
     */
    Double getSecondApplicantValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageHouseholdCalculationDetails asBrokerageHouseholdCalculationDetails();
}
