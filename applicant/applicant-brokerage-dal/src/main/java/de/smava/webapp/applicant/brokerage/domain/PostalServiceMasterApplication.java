package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.history.PostalServiceMasterApplicationHistory;

import java.util.Date;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public class PostalServiceMasterApplication extends PostalServiceMasterApplicationHistory {
    protected Date _date;
    protected de.smava.webapp.applicant.brokerage.domain.PostalServiceJob _postalServiceJob;
    protected de.smava.webapp.applicant.brokerage.domain.LoanApplication _loanApplication;
    protected String _brokerageApplications;

    @Override
    public Date getDate() {
        return _date;
    }

    @Override
    public void setDate(Date _date) {
        this._date = _date;
    }

    @Override
    public de.smava.webapp.applicant.brokerage.domain.PostalServiceJob getPostalServiceJob() {
        return _postalServiceJob;
    }

    @Override
    public void setPostalServiceJob(de.smava.webapp.applicant.brokerage.domain.PostalServiceJob _postalServiceJob) {
        this._postalServiceJob = _postalServiceJob;
    }

    @Override
    public void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication _loanApplication) {
        this._loanApplication = _loanApplication;
    }

    @Override
    public de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }


    public String getBrokerageApplications() {
        return _brokerageApplications;
    }

    public void setBrokerageApplications(String _brokerageApplications) {
        this._brokerageApplications = _brokerageApplications;
    }
}
