package de.smava.webapp.applicant.brokerage.type;

public enum SpecialOfferType {
    BEST_INTEREST, TOP_INTEREST, SUPER_INTEREST;
}
