package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration;


/**
 * The domain object that represents 'RequestedDocumentsConfigurations'.
 *
 * @author generator
 */
public interface RequestedDocumentsConfigurationEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'employment type'.
     *
     * 
     *
     */
    void setEmploymentType(String employmentType);

    /**
     * Returns the property 'employment type'.
     *
     * 
     *
     */
    String getEmploymentType();
    /**
     * Setter for the property 'application type'.
     *
     * 
     *
     */
    void setApplicationType(String applicationType);

    /**
     * Returns the property 'application type'.
     *
     * 
     *
     */
    String getApplicationType();
    /**
     * Setter for the property 'min volume'.
     *
     * 
     *
     */
    void setMinVolume(Integer minVolume);

    /**
     * Returns the property 'min volume'.
     *
     * 
     *
     */
    Integer getMinVolume();
    /**
     * Setter for the property 'max Volume'.
     *
     * 
     *
     */
    void setMaxVolume(Integer maxVolume);

    /**
     * Returns the property 'max Volume'.
     *
     * 
     *
     */
    Integer getMaxVolume();
    /**
     * Setter for the property 'brokerage bank configuration'.
     *
     * 
     *
     */
    void setBrokerageBankConfiguration(de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration brokerageBankConfiguration);

    /**
     * Returns the property 'brokerage bank configuration'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration getBrokerageBankConfiguration();
    /**
     * Helper method to get reference of this object as model type.
     */
    RequestedDocumentsConfiguration asRequestedDocumentsConfiguration();
}
