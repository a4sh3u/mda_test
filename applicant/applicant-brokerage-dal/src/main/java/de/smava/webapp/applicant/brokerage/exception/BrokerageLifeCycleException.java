package de.smava.webapp.applicant.brokerage.exception;

import de.smava.webapp.commons.exception.CoreException;

/**
 * Represents violated business constraints while processing a brokerage application.
 * @author aherr
 *
 */
public class BrokerageLifeCycleException extends CoreException {
	
}
