//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(master application)}


import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.brokerage.domain.history.MasterApplicationHistory;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;

import java.util.Date;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MasterApplications'.
 *
 * @author generator
 */
public class MasterApplication extends MasterApplicationHistory {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(master application)}
    // !!!!!!!! End of insert code section !!!!!!!!

    protected Account _account;
    protected LoanApplication _loanApplication;
    protected BrokerageState _state;
    protected Date _lastTouchpoint;
    protected Date _creationDate;
    protected Date _stateChangedDate;
    protected de.smava.webapp.marketing.affiliate.domain.AffiliateInformation _affiliateInformation;
    protected LoanApplication _latestLoanApplication;


    /**
     * Setter for the property 'account'.
     */
    public void setAccount(Account account) {
        _account = account;
    }

    /**
     * Returns the property 'account'.
     */
    public Account getAccount() {
        return _account;
    }

    /**
     * Setter for the property 'loan application'.
     */
    public void setLoanApplication(LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }

    /**
     * Returns the property 'loan application'.
     */
    public LoanApplication getLoanApplication() {
        return _loanApplication;
    }

    /**
     * Setter for the property 'state'.
     */
    public void setState(BrokerageState state) {
        _state = state;
    }

    /**
     * Returns the property 'state'.
     */
    public BrokerageState getState() {
        return _state;
    }

    /**
     * Setter for the property 'last touchpoint'.
     */
    public void setLastTouchpoint(Date lastTouchpoint) {
        if (!_lastTouchpointIsSet) {
            _lastTouchpointIsSet = true;
            _lastTouchpointInitVal = getLastTouchpoint();
        }
        registerChange("last touchpoint", _lastTouchpointInitVal, lastTouchpoint);
        _lastTouchpoint = lastTouchpoint;
    }

    /**
     * Returns the property 'last touchpoint'.
     */
    public Date getLastTouchpoint() {
        return _lastTouchpoint;
    }

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }

    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }

    /**
     * Setter for the property 'state changed date'.
     */
    public void setStateChangedDate(Date stateChangedDate) {
        if (!_stateChangedDateIsSet) {
            _stateChangedDateIsSet = true;
            _stateChangedDateInitVal = getStateChangedDate();
        }
        registerChange("state changed date", _stateChangedDateInitVal, stateChangedDate);
        _stateChangedDate = stateChangedDate;
    }

    /**
     * Returns the property 'state changed date'.
     */
    public Date getStateChangedDate() {
        return _stateChangedDate;
    }

    /**
     * Setter for the property 'affiliate information'.
     */
    public void setAffiliateInformation(de.smava.webapp.marketing.affiliate.domain.AffiliateInformation affiliateInformation) {
        _affiliateInformation = affiliateInformation;
    }

    /**
     * Returns the property 'affiliate information'.
     */
    public de.smava.webapp.marketing.affiliate.domain.AffiliateInformation getAffiliateInformation() {
        return _affiliateInformation;
    }

    /**
     * Setter for the property 'latest loan application'.
     */
    public void setLatestLoanApplication(LoanApplication latestLoanApplication) {
        _latestLoanApplication = latestLoanApplication;
    }

    /**
     * Returns the property 'latest loan application'.
     */
    public LoanApplication getLatestLoanApplication() {
        return _latestLoanApplication;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(MasterApplication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(MasterApplication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
     * Helper method to get reference of this object as model type.
     */
    public MasterApplication asMasterApplication() {
        return this;
    }
}
