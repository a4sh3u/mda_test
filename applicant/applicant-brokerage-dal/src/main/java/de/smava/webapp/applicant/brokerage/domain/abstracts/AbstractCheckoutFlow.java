//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(checkout flow)}

import de.smava.webapp.applicant.brokerage.domain.CheckoutFlow;
import de.smava.webapp.applicant.brokerage.domain.interfaces.CheckoutFlowEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CheckoutFlows'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCheckoutFlow
    extends BrokerageEntity    implements CheckoutFlowEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCheckoutFlow.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof CheckoutFlow)) {
            return;
        }
        
        this.setName(((CheckoutFlow) oldEntity).getName());    
        this.setType(((CheckoutFlow) oldEntity).getType());    
        this.setFirstPageId(((CheckoutFlow) oldEntity).getFirstPageId());    
        this.setCheckoutBlock(((CheckoutFlow) oldEntity).getCheckoutBlock());    
            
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof CheckoutFlow)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((CheckoutFlow) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getType(), ((CheckoutFlow) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getFirstPageId(), ((CheckoutFlow) otherEntity).getFirstPageId());    
        equals = equals && valuesAreEqual(this.getCheckoutBlock(), ((CheckoutFlow) otherEntity).getCheckoutBlock());    
            
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(checkout flow)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

