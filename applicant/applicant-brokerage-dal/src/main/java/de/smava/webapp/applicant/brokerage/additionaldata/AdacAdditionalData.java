package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author KF
 * @since 28.04.2017
 */
public class AdacAdditionalData extends AbstractAdditionalData {
    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_ADAC;
    }
}
