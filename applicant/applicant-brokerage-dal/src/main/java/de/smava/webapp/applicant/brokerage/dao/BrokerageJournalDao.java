package de.smava.webapp.applicant.brokerage.dao;

import java.util.Collection;
import java.util.List;
import de.smava.webapp.account.domain.Account;
import de.smava.webapp.applicant.brokerage.type.BrokerageJournalType;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageJournal;

/**
 * DAO interface for the domain object 'BrokerageJournals'.
 */
public interface BrokerageJournalDao extends BrokerageSchemaDao<BrokerageJournal> {

    Collection<BrokerageJournal> findByParameters(String filter, List params);

    BrokerageJournal findLatestByParameters(String filter, List params);

    Collection<BrokerageJournal> findBrokerageJournalsByAccountSortedByCreation(Account customer);

    BrokerageJournal findLatestForBorrowerByTypeAndExternalRefId(Account borrower, BrokerageJournalType type, String externalRefId);

}
