//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage household calculation details)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculationDetails;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageHouseholdCalculationDetailsEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageHouseholdCalculationDetailss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * Partial results for household calculation
 *
 * @author generator
 */
public abstract class AbstractBrokerageHouseholdCalculationDetails
    extends BrokerageEntity    implements BrokerageHouseholdCalculationDetailsEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageHouseholdCalculationDetails.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageHouseholdCalculationDetails)) {
            return;
        }
        
        this.setBrokerageHouseholdCalculation(((BrokerageHouseholdCalculationDetails) oldEntity).getBrokerageHouseholdCalculation());    
        this.setType(((BrokerageHouseholdCalculationDetails) oldEntity).getType());    
        this.setName(((BrokerageHouseholdCalculationDetails) oldEntity).getName());    
        this.setFirstApplicantValue(((BrokerageHouseholdCalculationDetails) oldEntity).getFirstApplicantValue());    
        this.setSecondApplicantValue(((BrokerageHouseholdCalculationDetails) oldEntity).getSecondApplicantValue());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageHouseholdCalculationDetails)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageHouseholdCalculation(), ((BrokerageHouseholdCalculationDetails) otherEntity).getBrokerageHouseholdCalculation());    
        equals = equals && valuesAreEqual(this.getType(), ((BrokerageHouseholdCalculationDetails) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getName(), ((BrokerageHouseholdCalculationDetails) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getFirstApplicantValue(), ((BrokerageHouseholdCalculationDetails) otherEntity).getFirstApplicantValue());    
        equals = equals && valuesAreEqual(this.getSecondApplicantValue(), ((BrokerageHouseholdCalculationDetails) otherEntity).getSecondApplicantValue());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage household calculation details)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

