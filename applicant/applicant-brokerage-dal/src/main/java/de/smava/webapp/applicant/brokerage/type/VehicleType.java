package de.smava.webapp.applicant.brokerage.type;

public enum VehicleType {
	CAR,
	BIKE,
	CARAVAN,
	MOTOCARAVAN;
}
