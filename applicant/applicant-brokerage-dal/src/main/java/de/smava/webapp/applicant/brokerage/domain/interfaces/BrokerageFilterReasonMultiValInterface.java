package de.smava.webapp.applicant.brokerage.domain.interfaces;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationFilterReason;
import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.brokerage.type.SmarketFilterReasonLevel;

/**
 * @author Adam Budzinski
 * @since 04.09.2017
 */
public interface BrokerageFilterReasonMultiValInterface {

    void setLoanApplication(LoanApplication loanApplication);

    LoanApplication getLoanApplication();

    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    BrokerageApplication getBrokerageApplication();

    void setApplicantId(Long id);

    Long getApplicantId();

    void setFilterLevel(SmarketFilterReasonLevel filterLevel);

    SmarketFilterReasonLevel getFilterLevel();

    void setFilterReason(BrokerageApplicationFilterReason filterReason);

    BrokerageApplicationFilterReason getFilterReason();
}
