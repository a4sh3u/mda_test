package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Adam Tomecki
 * @since 06.10.2016
 */
public class SkgAdditionalData extends AbstractAdditionalData {

    /**
     * {@inheritDoc}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_SKG;
    }
}
