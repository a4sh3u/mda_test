//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application request data)}
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.applicant.brokerage.dao.BrokerageApplicationRequestDataDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationRequestData;

import org.springframework.stereotype.Repository;


import java.util.*;

import org.apache.log4j.Logger;

import javax.jdo.Query;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BrokerageApplicationRequestDatas'.
 *
 * @author generator
 */
@Repository(value = "applicantBrokerageApplicationRequestDataDao")
public class JdoBrokerageApplicationRequestDataDao extends JdoBaseDao implements BrokerageApplicationRequestDataDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageApplicationRequestDataDao.class);

    private static final String CLASS_NAME = "BrokerageApplicationRequestData";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(brokerage application request data)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the brokerage application request data identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BrokerageApplicationRequestData load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BrokerageApplicationRequestData result = getEntity(BrokerageApplicationRequestData.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BrokerageApplicationRequestData getBrokerageApplicationRequestData(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BrokerageApplicationRequestData entity = findUniqueEntity(BrokerageApplicationRequestData.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the brokerage application request data.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BrokerageApplicationRequestData brokerageApplicationRequestData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageApplicationRequestData: " + brokerageApplicationRequestData);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(brokerage application request data)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(brokerageApplicationRequestData);
    }

    /**
     * @deprecated Use {@link #save(BrokerageApplicationRequestData) instead}
     */
    public Long saveBrokerageApplicationRequestData(BrokerageApplicationRequestData brokerageApplicationRequestData) {
        return save(brokerageApplicationRequestData);
    }

    /**
     * Deletes an brokerage application request data, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application request data
     */
    public void deleteBrokerageApplicationRequestData(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBrokerageApplicationRequestData: " + id);
        }
        deleteEntity(BrokerageApplicationRequestData.class, id);
    }

    /**
     * Retrieves all 'BrokerageApplicationRequestData' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BrokerageApplicationRequestData> result = getEntities(BrokerageApplicationRequestData.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BrokerageApplicationRequestData' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageApplicationRequestData> result = getEntities(BrokerageApplicationRequestData.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageApplicationRequestData' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplicationRequestData> result = getEntities(BrokerageApplicationRequestData.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplicationRequestData' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplicationRequestData> result = getEntities(BrokerageApplicationRequestData.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageApplicationRequestData' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationRequestDataList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageApplicationRequestData' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationRequestDataList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BrokerageApplicationRequestData' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationRequestDataList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageApplicationRequestData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationRequestDataList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplicationRequestData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationRequestDataList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplicationRequestData' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationRequestDataList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplicationRequestData' instances.
     */
    public long getBrokerageApplicationRequestDataCount() {
        long result = getEntityCount(BrokerageApplicationRequestData.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplicationRequestData' instances which match the given whereClause.
     */
    public long getBrokerageApplicationRequestDataCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BrokerageApplicationRequestData.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplicationRequestData' instances which match the given whereClause together with params specified in object array.
     */
    public long getBrokerageApplicationRequestDataCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BrokerageApplicationRequestData.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationDataBeforeDate(BrokerageApplication ba, List<BrokerageApplicationRequestType> types, List<BrokerageState> states, Date date) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplicationRequestData.class, "getBrokerageApplicationDataBeforeDateByState");

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("brokerageApplication", ba);
        parameters.put("states", states);
        parameters.put("types", types);
        parameters.put("maxDate", date);

        return (Collection<BrokerageApplicationRequestData>)query.executeWithMap(parameters);
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application request data)}
    //
    // insert custom methods here

    @Override
    public BrokerageApplicationRequestData findTheNewestBrokerageAplicationRequestData(BrokerageApplication ba) {
        BrokerageApplicationRequestData result = null;

        Collection<Object> params = new ArrayList<Object>();
        params.add(ba);

        OqlTerm oqlTerm = OqlTerm.newTerm();
        oqlTerm.equals("_brokerageApplication", ba);
        oqlTerm.setSortBy("_creationDate");
        oqlTerm.setSortDescending(true);

        Collection<BrokerageApplicationRequestData> records = findEntities(BrokerageApplicationRequestData.class, oqlTerm.toString(), params.toArray());

        if (!records.isEmpty()) {
            result = records.iterator().next();
        }

        return result;
    }

    @Override
    public Collection<BrokerageApplicationRequestData> findBrokerageApplicationData(BrokerageApplication ba, BrokerageApplicationRequestType bart) {

        Collection<Object> params = new ArrayList<Object>();
        params.add( ba);
        params.add(bart);


        OqlTerm oqlTerm = OqlTerm.newTerm();

        oqlTerm.and(
                oqlTerm.equals("_brokerageApplication", ba),
                oqlTerm.equals("_type", bart));


        Collection<BrokerageApplicationRequestData> result = findEntities(BrokerageApplicationRequestData.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<BrokerageApplicationRequestData> getBardsForBaId(Long baId) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplicationRequestData.class, "getBardsForBaId");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, baId);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplicationRequestData> bards = (Collection<BrokerageApplicationRequestData>) query.executeWithMap(params);

        return bards;
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
