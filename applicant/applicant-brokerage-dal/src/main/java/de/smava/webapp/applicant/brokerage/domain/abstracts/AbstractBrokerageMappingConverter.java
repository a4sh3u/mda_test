//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage mapping converter)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageMappingConverter;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageMappingConverterEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMappingConverters'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageMappingConverter
    extends BrokerageEntity    implements BrokerageMappingConverterEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageMappingConverter.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageMappingConverter)) {
            return;
        }
        
        this.setValue(((BrokerageMappingConverter) oldEntity).getValue());    
        this.setType(((BrokerageMappingConverter) oldEntity).getType());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageMappingConverter)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getValue(), ((BrokerageMappingConverter) otherEntity).getValue());    
        equals = equals && valuesAreEqual(this.getType(), ((BrokerageMappingConverter) otherEntity).getType());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage mapping converter)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

