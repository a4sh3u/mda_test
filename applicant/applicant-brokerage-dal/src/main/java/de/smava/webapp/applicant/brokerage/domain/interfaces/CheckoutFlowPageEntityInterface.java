package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage;

import java.util.Set;


/**
 * The domain object that represents 'CheckoutFlowPages'.
 *
 * @author generator
 */
public interface CheckoutFlowPageEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'previous page id'.
     *
     * 
     *
     */
    void setPreviousPageId(Long previousPageId);

    /**
     * Returns the property 'previous page id'.
     *
     * 
     *
     */
    Long getPreviousPageId();
    /**
     * Setter for the property 'checkout flow'.
     *
     * 
     *
     */
    void setCheckoutFlow(de.smava.webapp.applicant.brokerage.domain.CheckoutFlow checkoutFlow);

    /**
     * Returns the property 'checkout flow'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.CheckoutFlow getCheckoutFlow();
    /**
     * Setter for the property 'next pages'.
     *
     * 
     *
     */
    void setNextPages(Set<CheckoutFlowPage> nextPages);

    /**
     * Returns the property 'next pages'.
     *
     * 
     *
     */
    Set<CheckoutFlowPage> getNextPages();
    /**
     * Helper method to get reference of this object as model type.
     */
    CheckoutFlowPage asCheckoutFlowPage();
}
