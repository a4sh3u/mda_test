package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BankDocumentView;

import java.util.Date;


/**
 * The domain object that represents 'BankDocumentViews'.
 *
 * @author generator
 */
public interface BankDocumentViewEntityInterface {

    /**
     * Setter for the property 'ext ref number'.
     *
     * 
     *
     */
    void setExtRefNumber(String extRefNumber);

    /**
     * Returns the property 'ext ref number'.
     *
     * 
     *
     */
    String getExtRefNumber();
    /**
     * Setter for the property 'customer number'.
     *
     * 
     *
     */
    void setCustomerNumber(Long customerNumber);

    /**
     * Returns the property 'customer number'.
     *
     * 
     *
     */
    Long getCustomerNumber();
    /**
     * Setter for the property 'account id'.
     *
     * 
     *
     */
    void setAccountId(Long accountId);

    /**
     * Returns the property 'account id'.
     *
     * 
     *
     */
    Long getAccountId();
    /**
     * Setter for the property 'brokerage bank id'.
     *
     * 
     *
     */
    void setBrokerageBankId(Long brokerageBankId);

    /**
     * Returns the property 'brokerage bank id'.
     *
     * 
     *
     */
    Long getBrokerageBankId();
    /**
     * Setter for the property 'bank name'.
     *
     * 
     *
     */
    void setBankName(String bankName);

    /**
     * Returns the property 'bank name'.
     *
     * 
     *
     */
    String getBankName();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'send date'.
     *
     * 
     *
     */
    void setSendDate(Date sendDate);

    /**
     * Returns the property 'send date'.
     *
     * 
     *
     */
    Date getSendDate();
    /**
     * Setter for the property 'receive date'.
     *
     * 
     *
     */
    void setReceiveDate(Date receiveDate);

    /**
     * Returns the property 'receive date'.
     *
     * 
     *
     */
    Date getReceiveDate();
    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    void setFirstName(String firstName);

    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    String getFirstName();
    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    void setLastName(String lastName);

    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    String getLastName();
    /**
     * Setter for the property 'brokerage account id'.
     *
     * 
     *
     */
    void setBrokerageAccountId(Long brokerageAccountId);

    /**
     * Returns the property 'brokerage account id'.
     *
     * 
     *
     */
    Long getBrokerageAccountId();
    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    void setAdditionalData(String additionalData);

    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    String getAdditionalData();
    /**
     * Helper method to get reference of this object as model type.
     */
    BankDocumentView asBankDocumentView();
}
