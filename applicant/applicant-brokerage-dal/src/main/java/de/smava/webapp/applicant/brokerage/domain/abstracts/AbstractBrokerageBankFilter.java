//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank filter)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageBankFilter;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageBankFilterEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBankFilters'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageBankFilter
    extends BrokerageEntity    implements BrokerageBankFilterEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageBankFilter.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageBankFilter)) {
            return;
        }
        
        this.setCondition(((BrokerageBankFilter) oldEntity).getCondition());    
        this.setFilterReason(((BrokerageBankFilter) oldEntity).getFilterReason());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageBankFilter)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getCondition(), ((BrokerageBankFilter) otherEntity).getCondition());    
        equals = equals && valuesAreEqual(this.getFilterReason(), ((BrokerageBankFilter) otherEntity).getFilterReason());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage bank filter)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

