package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageMapping;
import de.smava.webapp.applicant.brokerage.domain.BrokerageMappingConverter;


/**
 * The domain object that represents 'BrokerageMappings'.
 *
 * @author generator
 */
public interface BrokerageMappingEntityInterface {

    /**
     * Setter for the property 'converter'.
     *
     * 
     *
     */
    void setConverter(BrokerageMappingConverter converter);

    /**
     * Returns the property 'converter'.
     *
     * 
     *
     */
    BrokerageMappingConverter getConverter();
    /**
     * Setter for the property 'smava value'.
     *
     * 
     *
     */
    void setSmavaValue(String smavaValue);

    /**
     * Returns the property 'smava value'.
     *
     * 
     *
     */
    String getSmavaValue();
    /**
     * Setter for the property 'bank value'.
     *
     * 
     *
     */
    void setBankValue(String bankValue);

    /**
     * Returns the property 'bank value'.
     *
     * 
     *
     */
    String getBankValue();
    /**
     * Setter for the property 'directionType'.
     *
     * 
     *
     */
    void setDirectionType(de.smava.webapp.applicant.brokerage.type.MappingDirectionType directionType);

    /**
     * Returns the property 'directionType'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.MappingDirectionType getDirectionType();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageMapping asBrokerageMapping();
}
