/**
 * 
 */
package de.smava.webapp.applicant.brokerage.type;

/**
 * @author dkeller
 *
 */
public enum BrokerageApplicationRequestType {

	TYPE_APPLICATION_REQUEST,
	TYPE_PRECHECK,
	TYPE_REVERSAL,
	TYPE_STATE_REQUEST,
	TYPE_DOC_REQUEST,
	TYPE_MANUAL_STATE_UPDATE,
	TYPE_VIDEO_IDENT_REQUEST
}
