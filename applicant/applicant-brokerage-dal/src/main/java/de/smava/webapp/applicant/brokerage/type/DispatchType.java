package de.smava.webapp.applicant.brokerage.type;

/**
 * Created by ovolkovskyi on 24.10.2017.
 */
public enum DispatchType {
    EMAIL,
    PHONE,
    MAIL,
    INTERNAL,
    SMS
}
