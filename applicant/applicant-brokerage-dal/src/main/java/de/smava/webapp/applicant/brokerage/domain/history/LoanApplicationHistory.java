package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractLoanApplication;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'LoanApplications'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class LoanApplicationHistory extends AbstractLoanApplication {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.BrokerageState _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.RdiType _requestedRdiTypeInitVal;
    protected transient boolean _requestedRdiTypeIsSet;
    protected transient double _requestedAmountInitVal;
    protected transient boolean _requestedAmountIsSet;
    protected transient int _requestedDurationInitVal;
    protected transient boolean _requestedDurationIsSet;
    protected transient boolean _sharedLoanInitVal;
    protected transient boolean _sharedLoanIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.Category _categoryInitVal;
    protected transient boolean _categoryIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType _initiatorTypeInitVal;
    protected transient boolean _initiatorTypeIsSet;
    protected transient String _initiatorToolInitVal;
    protected transient boolean _initiatorToolIsSet;
    protected transient boolean _reachedEmailTimeoutInitVal;
    protected transient boolean _reachedEmailTimeoutIsSet;
    protected transient boolean _visibleInitVal;
    protected transient boolean _visibleIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageState stateInitVal() {
        de.smava.webapp.applicant.brokerage.type.BrokerageState result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
			
    /**
     * Returns the initial value of the property 'requested rdi type'.
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType requestedRdiTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.RdiType result;
        if (_requestedRdiTypeIsSet) {
            result = _requestedRdiTypeInitVal;
        } else {
            result = getRequestedRdiType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested rdi type'.
     */
    public boolean requestedRdiTypeIsDirty() {
        return !valuesAreEqual(requestedRdiTypeInitVal(), getRequestedRdiType());
    }

    /**
     * Returns true if the setter method was called for the property 'requested rdi type'.
     */
    public boolean requestedRdiTypeIsSet() {
        return _requestedRdiTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested amount'.
     */
    public double requestedAmountInitVal() {
        double result;
        if (_requestedAmountIsSet) {
            result = _requestedAmountInitVal;
        } else {
            result = getRequestedAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested amount'.
     */
    public boolean requestedAmountIsDirty() {
        return !valuesAreEqual(requestedAmountInitVal(), getRequestedAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'requested amount'.
     */
    public boolean requestedAmountIsSet() {
        return _requestedAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested duration'.
     */
    public int requestedDurationInitVal() {
        int result;
        if (_requestedDurationIsSet) {
            result = _requestedDurationInitVal;
        } else {
            result = getRequestedDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested duration'.
     */
    public boolean requestedDurationIsDirty() {
        return !valuesAreEqual(requestedDurationInitVal(), getRequestedDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'requested duration'.
     */
    public boolean requestedDurationIsSet() {
        return _requestedDurationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'shared loan'.
     */
    public boolean sharedLoanInitVal() {
        boolean result;
        if (_sharedLoanIsSet) {
            result = _sharedLoanInitVal;
        } else {
            result = getSharedLoan();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'shared loan'.
     */
    public boolean sharedLoanIsDirty() {
        return !valuesAreEqual(sharedLoanInitVal(), getSharedLoan());
    }

    /**
     * Returns true if the setter method was called for the property 'shared loan'.
     */
    public boolean sharedLoanIsSet() {
        return _sharedLoanIsSet;
    }
		
    /**
     * Returns the initial value of the property 'category'.
     */
    public de.smava.webapp.applicant.brokerage.type.Category categoryInitVal() {
        de.smava.webapp.applicant.brokerage.type.Category result;
        if (_categoryIsSet) {
            result = _categoryInitVal;
        } else {
            result = getCategory();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'category'.
     */
    public boolean categoryIsDirty() {
        return !valuesAreEqual(categoryInitVal(), getCategory());
    }

    /**
     * Returns true if the setter method was called for the property 'category'.
     */
    public boolean categoryIsSet() {
        return _categoryIsSet;
    }
		
    /**
     * Returns the initial value of the property 'initiator type'.
     */
    public de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType initiatorTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType result;
        if (_initiatorTypeIsSet) {
            result = _initiatorTypeInitVal;
        } else {
            result = getInitiatorType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'initiator type'.
     */
    public boolean initiatorTypeIsDirty() {
        return !valuesAreEqual(initiatorTypeInitVal(), getInitiatorType());
    }

    /**
     * Returns true if the setter method was called for the property 'initiator type'.
     */
    public boolean initiatorTypeIsSet() {
        return _initiatorTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'initiator tool'.
     */
    public String initiatorToolInitVal() {
        String result;
        if (_initiatorToolIsSet) {
            result = _initiatorToolInitVal;
        } else {
            result = getInitiatorTool();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'initiator tool'.
     */
    public boolean initiatorToolIsDirty() {
        return !valuesAreEqual(initiatorToolInitVal(), getInitiatorTool());
    }

    /**
     * Returns true if the setter method was called for the property 'initiator tool'.
     */
    public boolean initiatorToolIsSet() {
        return _initiatorToolIsSet;
    }
	
    /**
     * Returns the initial value of the property 'reached email timeout'.
     */
    public boolean reachedEmailTimeoutInitVal() {
        boolean result;
        if (_reachedEmailTimeoutIsSet) {
            result = _reachedEmailTimeoutInitVal;
        } else {
            result = getReachedEmailTimeout();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'reached email timeout'.
     */
    public boolean reachedEmailTimeoutIsDirty() {
        return !valuesAreEqual(reachedEmailTimeoutInitVal(), getReachedEmailTimeout());
    }

    /**
     * Returns true if the setter method was called for the property 'reached email timeout'.
     */
    public boolean reachedEmailTimeoutIsSet() {
        return _reachedEmailTimeoutIsSet;
    }
	
    /**
     * Returns the initial value of the property 'visible'.
     */
    public boolean visibleInitVal() {
        boolean result;
        if (_visibleIsSet) {
            result = _visibleInitVal;
        } else {
            result = getVisible();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'visible'.
     */
    public boolean visibleIsDirty() {
        return !valuesAreEqual(visibleInitVal(), getVisible());
    }

    /**
     * Returns true if the setter method was called for the property 'visible'.
     */
    public boolean visibleIsSet() {
        return _visibleIsSet;
    }

}
