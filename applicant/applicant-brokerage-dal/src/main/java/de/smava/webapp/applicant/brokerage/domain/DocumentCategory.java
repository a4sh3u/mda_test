package de.smava.webapp.applicant.brokerage.domain;

/**
 * Created by asaadat on 22.03.16.
 */
public enum DocumentCategory {

    SALARY_STATEMENT("Gehaltsnachweis", 1),
    BANK_STATEMENT("Kontoauszug", 2),
    REFINANCE_STATEMENT("Ablösebescheinigung", 3),
    EMPLOYMENT_CONTRACT("Arbeitsvertrag", 4),
    ID_CARD("Ausweis", 5),
    RESIDENCE_PERMIT("Aufenthaltsgenehmigung", 6),
    OTHER("Sonstiges", 7),
    PENSION_AWARD("Rentenbescheid", 8),
    CAR_PURCHASE_CONTRACT("Kaufvertrag KFZ", 9),
    PROPERTY_OWNERSHIP("Nachweis Immobilienbesitz", 10),
    TAX_DOCUMENTS("Steuerbescheid", 11),
    ANNUAL_FINANCIAL_STATEMENT("Jahresabschluss", 12),
    BUSINESS_ASSESSMENT("BWA", 13),
    STATEMENT_ON_EXCESS_OF_RECEIPTS_OVER_EXPENSES("Einnahmen-Überschuss-Rechnung", 14);

    private final String label;
    private final Integer order;

    DocumentCategory(String label, Integer order) {
        this.label = label;
        this.order = order;
    }

    public String label() {
        return label;
    }

    public Integer order() {
        return order;
    }

    public static DocumentCategory convert(String label) {
        for (DocumentCategory category : DocumentCategory.values()) {
            if (category.label().equalsIgnoreCase(label)) {
                return category;
            }
        }

        throw new IllegalArgumentException("cannot match given label to any type, label : " + label);
    }

    public static String convertToFileStorageCategory(DocumentCategory documentCategory) {
        switch (documentCategory) {
            default:
                return "GENERIC";
        }
    }
}
