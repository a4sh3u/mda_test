package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageApplication;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplications'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationHistory extends AbstractBrokerageApplication {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _extRefNumberInitVal;
    protected transient boolean _extRefNumberIsSet;
    protected transient String _prevExtRefNumberInitVal;
    protected transient boolean _prevExtRefNumberIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.BrokerageState _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.RdiType _requestedRdiTypeInitVal;
    protected transient boolean _requestedRdiTypeIsSet;
    protected transient double _requestedAmountInitVal;
    protected transient boolean _requestedAmountIsSet;
    protected transient int _requestedDurationInitVal;
    protected transient boolean _requestedDurationIsSet;
    protected transient Double _monthlyRateInitVal;
    protected transient boolean _monthlyRateIsSet;
    protected transient Double _effectiveInterestInitVal;
    protected transient boolean _effectiveInterestIsSet;
    protected transient Double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient Integer _durationInitVal;
    protected transient boolean _durationIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.RdiType _rdiTypeInitVal;
    protected transient boolean _rdiTypeIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.Category _categoryInitVal;
    protected transient boolean _categoryIsSet;
    protected transient Date _papSaleTrackedDateInitVal;
    protected transient boolean _papSaleTrackedDateIsSet;
    protected transient Date _lastStateRequestInitVal;
    protected transient boolean _lastStateRequestIsSet;
    protected transient Date _lastStateChangeInitVal;
    protected transient boolean _lastStateChangeIsSet;
    protected transient Date _emailCreatedInitVal;
    protected transient boolean _emailCreatedIsSet;
    protected transient String _emailTypeInitVal;
    protected transient boolean _emailTypeIsSet;
    protected transient Date _documentsRequestedInitVal;
    protected transient boolean _documentsRequestedIsSet;
    protected transient Date _documentsSentInitVal;
    protected transient boolean _documentsSentIsSet;
    protected transient String _additionalDataInitVal;
    protected transient boolean _additionalDataIsSet;
    protected transient Date _payoutDateInitVal;
    protected transient boolean _payoutDateIsSet;
    protected transient Boolean _isSecondApplicantAllowedInitVal;
    protected transient boolean _isSecondApplicantAllowedIsSet;


    /**
     * Returns the initial value of the property 'isSecondApplicantAllowed'
     */
    public Boolean isSecondApplicantAllowedInitVal() {
        Boolean result;

        if (_isSecondApplicantAllowedIsSet) {
            result = _isSecondApplicantAllowedInitVal;
        } else {
            result = isSecondApplicantAllowed();
        }
        return result;
    }

    /**
     * Returns true if the setter method was called for the property 'isSecondApplicantAllowed'
     */
    public boolean isSecondApplicantAllowedIsSet() {
        return _isSecondApplicantAllowedIsSet;
    }

    /**
     * Compares the initial value with the current value of the property 'isSecondApplicantAllowed'.
     */
    public boolean isSecondApplicantAllowedIsDirty() {
        return !valuesAreEqual(isSecondApplicantAllowedInitVal(), isSecondApplicantAllowed());
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'ext ref number'.
     */
    public String extRefNumberInitVal() {
        String result;
        if (_extRefNumberIsSet) {
            result = _extRefNumberInitVal;
        } else {
            result = getExtRefNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'ext ref number'.
     */
    public boolean extRefNumberIsDirty() {
        return !valuesAreEqual(extRefNumberInitVal(), getExtRefNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'ext ref number'.
     */
    public boolean extRefNumberIsSet() {
        return _extRefNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'prev ext ref number'.
     */
    public String prevExtRefNumberInitVal() {
        String result;
        if (_prevExtRefNumberIsSet) {
            result = _prevExtRefNumberInitVal;
        } else {
            result = getPrevExtRefNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'prev ext ref number'.
     */
    public boolean prevExtRefNumberIsDirty() {
        return !valuesAreEqual(prevExtRefNumberInitVal(), getPrevExtRefNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'prev ext ref number'.
     */
    public boolean prevExtRefNumberIsSet() {
        return _prevExtRefNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageState stateInitVal() {
        de.smava.webapp.applicant.brokerage.type.BrokerageState result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
					
    /**
     * Returns the initial value of the property 'requested rdi type'.
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType requestedRdiTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.RdiType result;
        if (_requestedRdiTypeIsSet) {
            result = _requestedRdiTypeInitVal;
        } else {
            result = getRequestedRdiType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested rdi type'.
     */
    public boolean requestedRdiTypeIsDirty() {
        return !valuesAreEqual(requestedRdiTypeInitVal(), getRequestedRdiType());
    }

    /**
     * Returns true if the setter method was called for the property 'requested rdi type'.
     */
    public boolean requestedRdiTypeIsSet() {
        return _requestedRdiTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested amount'.
     */
    public double requestedAmountInitVal() {
        double result;
        if (_requestedAmountIsSet) {
            result = _requestedAmountInitVal;
        } else {
            result = getRequestedAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested amount'.
     */
    public boolean requestedAmountIsDirty() {
        return !valuesAreEqual(requestedAmountInitVal(), getRequestedAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'requested amount'.
     */
    public boolean requestedAmountIsSet() {
        return _requestedAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'requested duration'.
     */
    public int requestedDurationInitVal() {
        int result;
        if (_requestedDurationIsSet) {
            result = _requestedDurationInitVal;
        } else {
            result = getRequestedDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'requested duration'.
     */
    public boolean requestedDurationIsDirty() {
        return !valuesAreEqual(requestedDurationInitVal(), getRequestedDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'requested duration'.
     */
    public boolean requestedDurationIsSet() {
        return _requestedDurationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'monthly rate'.
     */
    public Double monthlyRateInitVal() {
        Double result;
        if (_monthlyRateIsSet) {
            result = _monthlyRateInitVal;
        } else {
            result = getMonthlyRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'monthly rate'.
     */
    public boolean monthlyRateIsDirty() {
        return !valuesAreEqual(monthlyRateInitVal(), getMonthlyRate());
    }

    /**
     * Returns true if the setter method was called for the property 'monthly rate'.
     */
    public boolean monthlyRateIsSet() {
        return _monthlyRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'effective interest'.
     */
    public Double effectiveInterestInitVal() {
        Double result;
        if (_effectiveInterestIsSet) {
            result = _effectiveInterestInitVal;
        } else {
            result = getEffectiveInterest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'effective interest'.
     */
    public boolean effectiveInterestIsDirty() {
        return !valuesAreEqual(effectiveInterestInitVal(), getEffectiveInterest());
    }

    /**
     * Returns true if the setter method was called for the property 'effective interest'.
     */
    public boolean effectiveInterestIsSet() {
        return _effectiveInterestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public Double amountInitVal() {
        Double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'duration'.
     */
    public Integer durationInitVal() {
        Integer result;
        if (_durationIsSet) {
            result = _durationInitVal;
        } else {
            result = getDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'duration'.
     */
    public boolean durationIsDirty() {
        return !valuesAreEqual(durationInitVal(), getDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'duration'.
     */
    public boolean durationIsSet() {
        return _durationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rdi type'.
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType rdiTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.RdiType result;
        if (_rdiTypeIsSet) {
            result = _rdiTypeInitVal;
        } else {
            result = getRdiType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rdi type'.
     */
    public boolean rdiTypeIsDirty() {
        return !valuesAreEqual(rdiTypeInitVal(), getRdiType());
    }

    /**
     * Returns true if the setter method was called for the property 'rdi type'.
     */
    public boolean rdiTypeIsSet() {
        return _rdiTypeIsSet;
    }
					
    /**
     * Returns the initial value of the property 'category'.
     */
    public de.smava.webapp.applicant.brokerage.type.Category categoryInitVal() {
        de.smava.webapp.applicant.brokerage.type.Category result;
        if (_categoryIsSet) {
            result = _categoryInitVal;
        } else {
            result = getCategory();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'category'.
     */
    public boolean categoryIsDirty() {
        return !valuesAreEqual(categoryInitVal(), getCategory());
    }

    /**
     * Returns true if the setter method was called for the property 'category'.
     */
    public boolean categoryIsSet() {
        return _categoryIsSet;
    }
	
    /**
     * Returns the initial value of the property 'pap sale tracked date'.
     */
    public Date papSaleTrackedDateInitVal() {
        Date result;
        if (_papSaleTrackedDateIsSet) {
            result = _papSaleTrackedDateInitVal;
        } else {
            result = getPapSaleTrackedDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'pap sale tracked date'.
     */
    public boolean papSaleTrackedDateIsDirty() {
        return !valuesAreEqual(papSaleTrackedDateInitVal(), getPapSaleTrackedDate());
    }

    /**
     * Returns true if the setter method was called for the property 'pap sale tracked date'.
     */
    public boolean papSaleTrackedDateIsSet() {
        return _papSaleTrackedDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last state request'.
     */
    public Date lastStateRequestInitVal() {
        Date result;
        if (_lastStateRequestIsSet) {
            result = _lastStateRequestInitVal;
        } else {
            result = getLastStateRequest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last state request'.
     */
    public boolean lastStateRequestIsDirty() {
        return !valuesAreEqual(lastStateRequestInitVal(), getLastStateRequest());
    }

    /**
     * Returns true if the setter method was called for the property 'last state request'.
     */
    public boolean lastStateRequestIsSet() {
        return _lastStateRequestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last state change'.
     */
    public Date lastStateChangeInitVal() {
        Date result;
        if (_lastStateChangeIsSet) {
            result = _lastStateChangeInitVal;
        } else {
            result = getLastStateChange();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last state change'.
     */
    public boolean lastStateChangeIsDirty() {
        return !valuesAreEqual(lastStateChangeInitVal(), getLastStateChange());
    }

    /**
     * Returns true if the setter method was called for the property 'last state change'.
     */
    public boolean lastStateChangeIsSet() {
        return _lastStateChangeIsSet;
    }
		
    /**
     * Returns the initial value of the property 'email created'.
     */
    public Date emailCreatedInitVal() {
        Date result;
        if (_emailCreatedIsSet) {
            result = _emailCreatedInitVal;
        } else {
            result = getEmailCreated();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email created'.
     */
    public boolean emailCreatedIsDirty() {
        return !valuesAreEqual(emailCreatedInitVal(), getEmailCreated());
    }

    /**
     * Returns true if the setter method was called for the property 'email created'.
     */
    public boolean emailCreatedIsSet() {
        return _emailCreatedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'email type'.
     */
    public String emailTypeInitVal() {
        String result;
        if (_emailTypeIsSet) {
            result = _emailTypeInitVal;
        } else {
            result = getEmailType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email type'.
     */
    public boolean emailTypeIsDirty() {
        return !valuesAreEqual(emailTypeInitVal(), getEmailType());
    }

    /**
     * Returns true if the setter method was called for the property 'email type'.
     */
    public boolean emailTypeIsSet() {
        return _emailTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'documents requested'.
     */
    public Date documentsRequestedInitVal() {
        Date result;
        if (_documentsRequestedIsSet) {
            result = _documentsRequestedInitVal;
        } else {
            result = getDocumentsRequested();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'documents requested'.
     */
    public boolean documentsRequestedIsDirty() {
        return !valuesAreEqual(documentsRequestedInitVal(), getDocumentsRequested());
    }

    /**
     * Returns true if the setter method was called for the property 'documents requested'.
     */
    public boolean documentsRequestedIsSet() {
        return _documentsRequestedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'documents sent'.
     */
    public Date documentsSentInitVal() {
        Date result;
        if (_documentsSentIsSet) {
            result = _documentsSentInitVal;
        } else {
            result = getDocumentsSent();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'documents sent'.
     */
    public boolean documentsSentIsDirty() {
        return !valuesAreEqual(documentsSentInitVal(), getDocumentsSent());
    }

    /**
     * Returns true if the setter method was called for the property 'documents sent'.
     */
    public boolean documentsSentIsSet() {
        return _documentsSentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'additional data'.
     */
    public String additionalDataInitVal() {
        String result;
        if (_additionalDataIsSet) {
            result = _additionalDataInitVal;
        } else {
            result = getAdditionalData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'additional data'.
     */
    public boolean additionalDataIsDirty() {
        return !valuesAreEqual(additionalDataInitVal(), getAdditionalData());
    }

    /**
     * Returns true if the setter method was called for the property 'additional data'.
     */
    public boolean additionalDataIsSet() {
        return _additionalDataIsSet;
    }
		
    /**
     * Returns the initial value of the property 'payout date'.
     */
    public Date payoutDateInitVal() {
        Date result;
        if (_payoutDateIsSet) {
            result = _payoutDateInitVal;
        } else {
            result = getPayoutDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'payout date'.
     */
    public boolean payoutDateIsDirty() {
        return !valuesAreEqual(payoutDateInitVal(), getPayoutDate());
    }

    /**
     * Returns true if the setter method was called for the property 'payout date'.
     */
    public boolean payoutDateIsSet() {
        return _payoutDateIsSet;
    }

}
