//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application request data)}
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationRequestData;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageApplicationRequestDatas'.
 *
 * @author generator
 */
public interface BrokerageApplicationRequestDataDao extends BrokerageSchemaDao<BrokerageApplicationRequestData> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage application request data identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageApplicationRequestData getBrokerageApplicationRequestData(Long id);

    /**
     * Saves the brokerage application request data.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageApplicationRequestData(BrokerageApplicationRequestData brokerageApplicationRequestData);

    /**
     * Deletes an brokerage application request data, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application request data
     */
    void deleteBrokerageApplicationRequestData(Long id);

    /**
     * Retrieves all 'BrokerageApplicationRequestData' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList();

    /**
     * Retrieves a page of 'BrokerageApplicationRequestData' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageApplicationRequestData' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageApplicationRequestData' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageApplicationRequestData> getBrokerageApplicationRequestDataList(Pageable pageable, Sortable sortable);


    /**
     * find BARDs which commning from before some date and for correct state and type
     *
     * @param ba        {@link BrokerageApplication}
     * @param types     {@link BrokerageApplicationRequestType}
     * @param state     {@link BrokerageState}
     * @param date      {@link Date}
     * @return          {@link Collection}
     */
    Collection<BrokerageApplicationRequestData> findBrokerageApplicationDataBeforeDate(BrokerageApplication ba, List<BrokerageApplicationRequestType> types, List<BrokerageState> state, Date date);

    /**
     * find the newest BARD based on date
     *
     * @param ba {@link BrokerageApplication}
     * @return   {@link BrokerageApplicationRequestData}
     */
    BrokerageApplicationRequestData findTheNewestBrokerageAplicationRequestData(BrokerageApplication ba);

    /**
     * find BARDs for BA based on type
     *
     * @param ba        {@link BrokerageApplication}
     * @param bart      {@link BrokerageApplicationRequestType}
     * @return          {@link Collection}
     */
    Collection<BrokerageApplicationRequestData> findBrokerageApplicationData(BrokerageApplication ba, BrokerageApplicationRequestType bart);

        /**
         * Returns the number of 'BrokerageApplicationRequestData' instances.
         */
    long getBrokerageApplicationRequestDataCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application request data)}
    //
    // insert custom methods here
    Collection<BrokerageApplicationRequestData> getBardsForBaId(Long baId);
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
