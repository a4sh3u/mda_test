package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageBankFilter;


/**
 * The domain object that represents 'BrokerageBankFilters'.
 *
 * @author generator
 */
public interface BrokerageBankFilterEntityInterface {

    /**
     * Setter for the property 'condition'.
     *
     * 
     *
     */
    void setCondition(String condition);

    /**
     * Returns the property 'condition'.
     *
     * 
     *
     */
    String getCondition();
    /**
     * Setter for the property 'filter reason'.
     *
     * 
     *
     */
    void setFilterReason(de.smava.webapp.applicant.brokerage.type.FilterReasonCode filterReason);

    /**
     * Returns the property 'filter reason'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.FilterReasonCode getFilterReason();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBankFilter asBrokerageBankFilter();
}
