package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageBankFilter;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBankFilters'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankFilterHistory extends AbstractBrokerageBankFilter {

    protected transient String _conditionInitVal;
    protected transient boolean _conditionIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.FilterReasonCode _filterReasonInitVal;
    protected transient boolean _filterReasonIsSet;


	
    /**
     * Returns the initial value of the property 'condition'.
     */
    public String conditionInitVal() {
        String result;
        if (_conditionIsSet) {
            result = _conditionInitVal;
        } else {
            result = getCondition();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'condition'.
     */
    public boolean conditionIsDirty() {
        return !valuesAreEqual(conditionInitVal(), getCondition());
    }

    /**
     * Returns true if the setter method was called for the property 'condition'.
     */
    public boolean conditionIsSet() {
        return _conditionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'filter reason'.
     */
    public de.smava.webapp.applicant.brokerage.type.FilterReasonCode filterReasonInitVal() {
        de.smava.webapp.applicant.brokerage.type.FilterReasonCode result;
        if (_filterReasonIsSet) {
            result = _filterReasonInitVal;
        } else {
            result = getFilterReason();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'filter reason'.
     */
    public boolean filterReasonIsDirty() {
        return !valuesAreEqual(filterReasonInitVal(), getFilterReason());
    }

    /**
     * Returns true if the setter method was called for the property 'filter reason'.
     */
    public boolean filterReasonIsSet() {
        return _filterReasonIsSet;
    }

}
