package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.VehicleInformation;

import java.util.Date;


/**
 * The domain object that represents 'VehicleInformations'.
 *
 * @author generator
 */
public interface VehicleInformationEntityInterface {

    /**
     * Setter for the property 'model'.
     *
     * 
     *
     */
    void setModel(String model);

    /**
     * Returns the property 'model'.
     *
     * 
     *
     */
    String getModel();
    /**
     * Setter for the property 'brand'.
     *
     * 
     *
     */
    void setBrand(String brand);

    /**
     * Returns the property 'brand'.
     *
     * 
     *
     */
    String getBrand();
    /**
     * Setter for the property 'vehicle price'.
     *
     * 
     *
     */
    void setVehiclePrice(Double vehiclePrice);

    /**
     * Returns the property 'vehicle price'.
     *
     * 
     *
     */
    Double getVehiclePrice();
    /**
     * Setter for the property 'down payment'.
     *
     * 
     *
     */
    void setDownPayment(Double downPayment);

    /**
     * Returns the property 'down payment'.
     *
     * 
     *
     */
    Double getDownPayment();
    /**
     * Setter for the property 'registration year'.
     *
     * 
     *
     */
    void setRegistrationYear(Date registrationYear);

    /**
     * Returns the property 'registration year'.
     *
     * 
     *
     */
    Date getRegistrationYear();
    /**
     * Setter for the property 'power kw'.
     *
     * 
     *
     */
    void setPowerKw(Integer powerKw);

    /**
     * Returns the property 'power kw'.
     *
     * 
     *
     */
    Integer getPowerKw();
    /**
     * Setter for the property 'vehicle km'.
     *
     * 
     *
     */
    void setVehicleKm(Integer vehicleKm);

    /**
     * Returns the property 'vehicle km'.
     *
     * 
     *
     */
    Integer getVehicleKm();
    /**
     * Setter for the property 'vehicle type'.
     *
     * 
     *
     */
    void setVehicleType(de.smava.webapp.applicant.brokerage.type.VehicleType vehicleType);

    /**
     * Returns the property 'vehicle type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.VehicleType getVehicleType();
    /**
     * Helper method to get reference of this object as model type.
     */
    VehicleInformation asVehicleInformation();
}
