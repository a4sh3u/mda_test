package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Mateusz Zyla
 * @since 09.01.2018
 */
public class IngDibaAdditionalData extends AbstractAdditionalData {

    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_INGDIBA;
    }
}
