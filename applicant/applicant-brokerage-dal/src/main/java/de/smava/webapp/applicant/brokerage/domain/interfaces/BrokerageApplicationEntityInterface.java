package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.*;
import de.smava.webapp.applicant.domain.BankAccount;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageApplications'.
 *
 * @author generator
 */
public interface BrokerageApplicationEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'ext ref number'.
     *
     * 
     *
     */
    void setExtRefNumber(String extRefNumber);

    /**
     * Returns the property 'ext ref number'.
     *
     * 
     *
     */
    String getExtRefNumber();
    /**
     * Setter for the property 'prev ext ref number'.
     *
     * 
     *
     */
    void setPrevExtRefNumber(String prevExtRefNumber);

    /**
     * Returns the property 'prev ext ref number'.
     *
     * 
     *
     */
    String getPrevExtRefNumber();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(de.smava.webapp.applicant.brokerage.type.BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.BrokerageState getState();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'first applicant'.
     *
     * 
     *
     */
    void setFirstApplicant(de.smava.webapp.applicant.domain.Applicant firstApplicant);

    /**
     * Returns the property 'first applicant'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getFirstApplicant();
    /**
     * Setter for the property 'second applicant'.
     *
     * 
     *
     */
    void setSecondApplicant(de.smava.webapp.applicant.domain.Applicant secondApplicant);

    /**
     * Returns the property 'second applicant'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getSecondApplicant();
    /**
     * Setter for the property 'reference bank account'.
     *
     * 
     *
     */
    void setReferenceBankAccount(BankAccount referenceBankAccount);

    /**
     * Returns the property 'reference bank account'.
     *
     * 
     *
     */
    BankAccount getReferenceBankAccount();
    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    void setRequestedRdiType(de.smava.webapp.applicant.brokerage.type.RdiType requestedRdiType);

    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.RdiType getRequestedRdiType();
    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    void setRequestedAmount(double requestedAmount);

    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    double getRequestedAmount();
    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    void setRequestedDuration(int requestedDuration);

    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    int getRequestedDuration();
    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    void setMonthlyRate(Double monthlyRate);

    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    Double getMonthlyRate();
    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    void setEffectiveInterest(Double effectiveInterest);

    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    Double getEffectiveInterest();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Double getAmount();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'rdi type'.
     *
     * 
     *
     */
    void setRdiType(de.smava.webapp.applicant.brokerage.type.RdiType rdiType);

    /**
     * Returns the property 'rdi type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.RdiType getRdiType();
    /**
     * Setter for the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    void setBrokerageApplicationFilterReason(BrokerageApplicationFilterReason brokerageApplicationFilterReason);

    /**
     * Returns the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    BrokerageApplicationFilterReason getBrokerageApplicationFilterReason();
    /**
     * Setter for the property 'second applicant filter reason'.
     *
     * 
     *
     */
    void setSecondApplicantFilterReason(BrokerageApplicationFilterReason secondApplicantFilterReason);

    /**
     * Returns the property 'second applicant filter reason'.
     *
     * 
     *
     */
    BrokerageApplicationFilterReason getSecondApplicantFilterReason();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication();
    /**
     * Setter for the property 'brokerage application request datas'.
     *
     * 
     *
     */
    void setBrokerageApplicationRequestDatas(Set<BrokerageApplicationRequestData> brokerageApplicationRequestDatas);

    /**
     * Returns the property 'brokerage application request datas'.
     *
     * 
     *
     */
    Set<BrokerageApplicationRequestData> getBrokerageApplicationRequestDatas();
    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    void setCategory(de.smava.webapp.applicant.brokerage.type.Category category);

    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.Category getCategory();
    /**
     * Setter for the property 'pap sale tracked date'.
     *
     * 
     *
     */
    void setPapSaleTrackedDate(Date papSaleTrackedDate);

    /**
     * Returns the property 'pap sale tracked date'.
     *
     * 
     *
     */
    Date getPapSaleTrackedDate();
    /**
     * Setter for the property 'last state request'.
     *
     * 
     *
     */
    void setLastStateRequest(Date lastStateRequest);

    /**
     * Returns the property 'last state request'.
     *
     * 
     *
     */
    Date getLastStateRequest();
    /**
     * Setter for the property 'last state change'.
     *
     * 
     *
     */
    void setLastStateChange(Date lastStateChange);

    /**
     * Returns the property 'last state change'.
     *
     * 
     *
     */
    Date getLastStateChange();
    /**
     * Setter for the property 'document container'.
     *
     * 
     *
     */
    void setDocumentContainer(de.smava.webapp.applicant.account.domain.DocumentContainer documentContainer);

    /**
     * Returns the property 'document container'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.DocumentContainer getDocumentContainer();
    /**
     * Setter for the property 'email created'.
     *
     * 
     *
     */
    void setEmailCreated(Date emailCreated);

    /**
     * Returns the property 'email created'.
     *
     * 
     *
     */
    Date getEmailCreated();
    /**
     * Setter for the property 'email type'.
     *
     * 
     *
     */
    void setEmailType(String emailType);

    /**
     * Returns the property 'email type'.
     *
     * 
     *
     */
    String getEmailType();
    /**
     * Setter for the property 'documents requested'.
     *
     * 
     *
     */
    void setDocumentsRequested(Date documentsRequested);

    /**
     * Returns the property 'documents requested'.
     *
     * 
     *
     */
    Date getDocumentsRequested();
    /**
     * Setter for the property 'documents sent'.
     *
     * 
     *
     */
    void setDocumentsSent(Date documentsSent);

    /**
     * Returns the property 'documents sent'.
     *
     * 
     *
     */
    Date getDocumentsSent();
    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    void setAdditionalData(String additionalData);

    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    String getAdditionalData();
    /**
     * Setter for the property 'household calculation'.
     *
     * 
     *
     */
    void setHouseholdCalculation(de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation householdCalculation);

    /**
     * Returns the property 'household calculation'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation getHouseholdCalculation();
    /**
     * Setter for the property 'payout date'.
     *
     * 
     *
     */
    void setPayoutDate(Date payoutDate);

    /**
     * Returns the property 'payout date'.
     *
     * 
     *
     */
    Date getPayoutDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplication asBrokerageApplication();

    Boolean isSecondApplicantAllowed();

    void setSecondApplicantAllowed(Boolean secondApplicantAllowed);

    Set<BrokerageFilterReasonMultiVal> getBrokerageFilterReasonMultiValues();

    void setBrokerageFilterReasonMultiValues(Set<BrokerageFilterReasonMultiVal> brokerageFilterReasonMultiValues);

    Set<BrokerageApplicationDocument> getDocuments();

    void setDocuments(Set<BrokerageApplicationDocument> documents);
}
