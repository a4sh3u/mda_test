//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageBankHistory;
import org.apache.commons.lang.StringUtils;

import java.util.HashSet;
import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBanks'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBank extends BrokerageBankHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bank)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _marketingPartnerId;
        protected String _name;
        protected boolean _valid;
        protected boolean _comparisonOnly;
        protected Integer _sendInOffset;
        protected Set<BrokerageBankFilter> _filter;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration _configuration;
        protected Set<BrokerageMapping> _mappings;
        protected Set<BrokerageUploadedDoc> _brokerageUploadedDocs;
        protected Set<BrokerageBankDetail> _brokerageBankDetails;
        protected String _statusUpdate;
        protected de.smava.webapp.brokerage.domain.BucketType _bucketType;
        
                                    
    /**
     * Setter for the property 'marketing partner id'.
     *
     * 
     *
     */
    public void setMarketingPartnerId(Long marketingPartnerId) {
        _marketingPartnerId = marketingPartnerId;
    }
            
    /**
     * Returns the property 'marketing partner id'.
     *
     * 
     *
     */
    public Long getMarketingPartnerId() {
        return _marketingPartnerId;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'valid'.
     *
     * 
     *
     */
    public void setValid(boolean valid) {
        if (!_validIsSet) {
            _validIsSet = true;
            _validInitVal = getValid();
        }
        registerChange("valid", _validInitVal, valid);
        _valid = valid;
    }
                        
    /**
     * Returns the property 'valid'.
     *
     * 
     *
     */
    public boolean getValid() {
        return _valid;
    }
                                    /**
     * Setter for the property 'comparison only'.
     *
     * 
     *
     */
    public void setComparisonOnly(boolean comparisonOnly) {
        if (!_comparisonOnlyIsSet) {
            _comparisonOnlyIsSet = true;
            _comparisonOnlyInitVal = getComparisonOnly();
        }
        registerChange("comparison only", _comparisonOnlyInitVal, comparisonOnly);
        _comparisonOnly = comparisonOnly;
    }
                        
    /**
     * Returns the property 'comparison only'.
     *
     * 
     *
     */
    public boolean getComparisonOnly() {
        return _comparisonOnly;
    }
                                    /**
     * Setter for the property 'send in offset'.
     *
     * 
     *
     */
    public void setSendInOffset(Integer sendInOffset) {
        if (!_sendInOffsetIsSet) {
            _sendInOffsetIsSet = true;
            _sendInOffsetInitVal = getSendInOffset();
        }
        registerChange("send in offset", _sendInOffsetInitVal, sendInOffset);
        _sendInOffset = sendInOffset;
    }
                        
    /**
     * Returns the property 'send in offset'.
     *
     * 
     *
     */
    public Integer getSendInOffset() {
        return _sendInOffset;
    }
                                            
    /**
     * Setter for the property 'filter'.
     *
     * 
     *
     */
    public void setFilter(Set<BrokerageBankFilter> filter) {
        _filter = filter;
    }
            
    /**
     * Returns the property 'filter'.
     *
     * 
     *
     */
    public Set<BrokerageBankFilter> getFilter() {
        return _filter;
    }
                                            
    /**
     * Setter for the property 'configuration'.
     *
     * 
     *
     */
    public void setConfiguration(de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration configuration) {
        _configuration = configuration;
    }
            
    /**
     * Returns the property 'configuration'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration getConfiguration() {
        return _configuration;
    }
                                            
    /**
     * Setter for the property 'mappings'.
     *
     * 
     *
     */
    public void setMappings(Set<BrokerageMapping> mappings) {
        _mappings = mappings;
    }
            
    /**
     * Returns the property 'mappings'.
     *
     * 
     *
     */
    public Set<BrokerageMapping> getMappings() {
        return _mappings;
    }
                                            
    /**
     * Setter for the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    public void setBrokerageUploadedDocs(Set<BrokerageUploadedDoc> brokerageUploadedDocs) {
        _brokerageUploadedDocs = brokerageUploadedDocs;
    }
            
    /**
     * Returns the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    public Set<BrokerageUploadedDoc> getBrokerageUploadedDocs() {
        return _brokerageUploadedDocs;
    }
                                            
    /**
     * Setter for the property 'brokerage bank details'.
     *
     * 
     *
     */
    public void setBrokerageBankDetails(Set<BrokerageBankDetail> brokerageBankDetails) {
        _brokerageBankDetails = brokerageBankDetails;
    }
            
    /**
     * Returns the property 'brokerage bank details'.
     *
     * 
     *
     */
    public Set<BrokerageBankDetail> getBrokerageBankDetails() {
        return _brokerageBankDetails;
    }

    /**
     * Setter for the property 'brokerage bank details'.
     */
    public String getStatusUpdateRaw() {
        return _statusUpdate;
    }

    /**
     * Setter for the property 'brokerage bank details'.
     */
    public Set<de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod> getStatusUpdate() {

        Set<de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod> set
                = new HashSet<de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod>();

        if (! StringUtils.isBlank(_statusUpdate)) {

            String[] array = _statusUpdate.split(",");

            for (String element : array) {
                set.add(BrokerageBankStatusUpdateMethod.valueOf(element.trim().toUpperCase()));
            }
        }

        return set;
    }

    /**
     * Setter for the property 'brokerage bank details'.
     */
    public void setStatusUpdate(Set<de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod> statusUpdate) {
        String stringValues = null;

        if (statusUpdate != null && !statusUpdate.isEmpty()) {
            Set<String> stringList = new HashSet<String>();

            for (de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod method : statusUpdate) {
                stringList.add(method.name());
            }
            stringValues = StringUtils.join(stringList, ',');
        }

        if (!_statusUpdateIsSet) {
            _statusUpdateIsSet = true;
            _statusUpdateInitVal = stringValues;
        }

        registerChange("statusUpdate", _statusUpdateInitVal, stringValues);
        _statusUpdate = stringValues;
    }

                                    /**
     * Setter for the property 'is subprime'.

    /**
     * Setter for the property 'bucket type'.
     *
     *
     *
     */
    public void setBucketType(de.smava.webapp.brokerage.domain.BucketType bucketType) {
        _bucketType = bucketType;
    }

    /**
     * Returns the property 'bucket type'.
     *
     *
     *
     */
    public de.smava.webapp.brokerage.domain.BucketType getBucketType() {
        return _bucketType;
    }

    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_configuration instanceof de.smava.webapp.commons.domain.Entity && !_configuration.getChangeSet().isEmpty()) {
             for (String element : _configuration.getChangeSet()) {
                 result.add("configuration : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBank.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _valid=").append(_valid);
            builder.append("\n    _comparisonOnly=").append(_comparisonOnly);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBank.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBank asBrokerageBank() {
        return this;
    }
}
