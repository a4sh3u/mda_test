package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageHouseholdCalculationDetails;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageHouseholdCalculationDetailss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageHouseholdCalculationDetailsHistory extends AbstractBrokerageHouseholdCalculationDetails {

    protected transient de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient Double _firstApplicantValueInitVal;
    protected transient boolean _firstApplicantValueIsSet;
    protected transient Double _secondApplicantValueInitVal;
    protected transient boolean _secondApplicantValueIsSet;


		
    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType typeInitVal() {
        de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName nameInitVal() {
        de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'first applicant value'.
     */
    public Double firstApplicantValueInitVal() {
        Double result;
        if (_firstApplicantValueIsSet) {
            result = _firstApplicantValueInitVal;
        } else {
            result = getFirstApplicantValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first applicant value'.
     */
    public boolean firstApplicantValueIsDirty() {
        return !valuesAreEqual(firstApplicantValueInitVal(), getFirstApplicantValue());
    }

    /**
     * Returns true if the setter method was called for the property 'first applicant value'.
     */
    public boolean firstApplicantValueIsSet() {
        return _firstApplicantValueIsSet;
    }
	
    /**
     * Returns the initial value of the property 'second applicant value'.
     */
    public Double secondApplicantValueInitVal() {
        Double result;
        if (_secondApplicantValueIsSet) {
            result = _secondApplicantValueInitVal;
        } else {
            result = getSecondApplicantValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'second applicant value'.
     */
    public boolean secondApplicantValueIsDirty() {
        return !valuesAreEqual(secondApplicantValueInitVal(), getSecondApplicantValue());
    }

    /**
     * Returns true if the setter method was called for the property 'second applicant value'.
     */
    public boolean secondApplicantValueIsSet() {
        return _secondApplicantValueIsSet;
    }

}
