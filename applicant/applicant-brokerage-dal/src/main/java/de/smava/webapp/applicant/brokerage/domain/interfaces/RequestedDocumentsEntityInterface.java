package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.RequestedDocuments;


/**
 * The domain object that represents 'RequestedDocumentss'.
 *
 * @author generator
 */
public interface RequestedDocumentsEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'number of documents'.
     *
     * 
     *
     */
    void setNumberOfDocuments(Integer numberOfDocuments);

    /**
     * Returns the property 'number of documents'.
     *
     * 
     *
     */
    Integer getNumberOfDocuments();
    /**
     * Setter for the property 'required'.
     *
     * 
     *
     */
    void setRequired(boolean required);

    /**
     * Returns the property 'required'.
     *
     * 
     *
     */
    boolean getRequired();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.brokerage.domain.DocumentCategory type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.DocumentCategory getType();
    /**
     * Setter for the property 'document configuration'.
     *
     * 
     *
     */
    void setDocumentConfiguration(de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration documentConfiguration);

    /**
     * Returns the property 'document configuration'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration getDocumentConfiguration();
    /**
     * Helper method to get reference of this object as model type.
     */
    RequestedDocuments asRequestedDocuments();
}
