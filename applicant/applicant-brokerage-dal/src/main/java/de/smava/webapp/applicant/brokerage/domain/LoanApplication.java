package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.history.LoanApplicationHistory;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'LoanApplications'.
 *
 * 
 *
 * @author generator
 */
public class LoanApplication extends LoanApplicationHistory  {

    protected Date _creationDate;
    protected de.smava.webapp.applicant.brokerage.type.BrokerageState _state;
    protected de.smava.webapp.applicant.account.domain.Account _account;
    protected de.smava.webapp.applicant.domain.ApplicantsRelationship _applicantRelationship;
    protected de.smava.webapp.applicant.brokerage.type.RdiType _requestedRdiType;
    protected double _requestedAmount;
    protected int _requestedDuration;
    protected boolean _sharedLoan;
    protected Set<BrokerageApplication> _brokerageApplications;
    protected de.smava.webapp.applicant.brokerage.type.Category _category;
    protected de.smava.webapp.applicant.brokerage.domain.VehicleInformation _vehicleInformation;
    protected de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType _initiatorType;
    protected String _initiatorTool;
    protected boolean _reachedEmailTimeout;
    protected boolean _visible;
    protected Long _initiatorSmavaId;

     /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(de.smava.webapp.applicant.brokerage.type.BrokerageState state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageState getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'applicant relationship'.
     *
     * 
     *
     */
    public void setApplicantRelationship(de.smava.webapp.applicant.domain.ApplicantsRelationship applicantRelationship) {
        _applicantRelationship = applicantRelationship;
    }
            
    /**
     * Returns the property 'applicant relationship'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.ApplicantsRelationship getApplicantRelationship() {
        return _applicantRelationship;
    }
                                    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    public void setRequestedRdiType(de.smava.webapp.applicant.brokerage.type.RdiType requestedRdiType) {
        if (!_requestedRdiTypeIsSet) {
            _requestedRdiTypeIsSet = true;
            _requestedRdiTypeInitVal = getRequestedRdiType();
        }
        registerChange("requested rdi type", _requestedRdiTypeInitVal, requestedRdiType);
        _requestedRdiType = requestedRdiType;
    }
                        
    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType getRequestedRdiType() {
        return _requestedRdiType;
    }
                                    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    public void setRequestedAmount(double requestedAmount) {
        if (!_requestedAmountIsSet) {
            _requestedAmountIsSet = true;
            _requestedAmountInitVal = getRequestedAmount();
        }
        registerChange("requested amount", _requestedAmountInitVal, requestedAmount);
        _requestedAmount = requestedAmount;
    }
                        
    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    public double getRequestedAmount() {
        return _requestedAmount;
    }
                                    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    public void setRequestedDuration(int requestedDuration) {
        if (!_requestedDurationIsSet) {
            _requestedDurationIsSet = true;
            _requestedDurationInitVal = getRequestedDuration();
        }
        registerChange("requested duration", _requestedDurationInitVal, requestedDuration);
        _requestedDuration = requestedDuration;
    }
                        
    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    public int getRequestedDuration() {
        return _requestedDuration;
    }
                                    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    public void setSharedLoan(boolean sharedLoan) {
        if (!_sharedLoanIsSet) {
            _sharedLoanIsSet = true;
            _sharedLoanInitVal = getSharedLoan();
        }
        registerChange("shared loan", _sharedLoanInitVal, sharedLoan);
        _sharedLoan = sharedLoan;
    }
                        
    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    public boolean getSharedLoan() {
        return _sharedLoan;
    }
                                            
    /**
     * Setter for the property 'brokerage applications'.
     *
     * 
     *
     */
    public void setBrokerageApplications(Set<BrokerageApplication> brokerageApplications) {
        _brokerageApplications = brokerageApplications;
    }
            
    /**
     * Returns the property 'brokerage applications'.
     *
     * 
     *
     */
    public Set<BrokerageApplication> getBrokerageApplications() {
        return _brokerageApplications;
    }
                                    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    public void setCategory(de.smava.webapp.applicant.brokerage.type.Category category) {
        if (!_categoryIsSet) {
            _categoryIsSet = true;
            _categoryInitVal = getCategory();
        }
        registerChange("category", _categoryInitVal, category);
        _category = category;
    }
                        
    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.Category getCategory() {
        return _category;
    }
                                            
    /**
     * Setter for the property 'vehicle information'.
     *
     * 
     *
     */
    public void setVehicleInformation(de.smava.webapp.applicant.brokerage.domain.VehicleInformation vehicleInformation) {
        _vehicleInformation = vehicleInformation;
    }
            
    /**
     * Returns the property 'vehicle information'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.VehicleInformation getVehicleInformation() {
        return _vehicleInformation;
    }
                                    /**
     * Setter for the property 'initiator type'.
     *
     * 
     *
     */
    public void setInitiatorType(de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType initiatorType) {
        if (!_initiatorTypeIsSet) {
            _initiatorTypeIsSet = true;
            _initiatorTypeInitVal = getInitiatorType();
        }
        registerChange("initiator type", _initiatorTypeInitVal, initiatorType);
        _initiatorType = initiatorType;
    }
                        
    /**
     * Returns the property 'initiator type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType getInitiatorType() {
        return _initiatorType;
    }
                                    /**
     * Setter for the property 'initiator tool'.
     *
     * 
     *
     */
    public void setInitiatorTool(String initiatorTool) {
        if (!_initiatorToolIsSet) {
            _initiatorToolIsSet = true;
            _initiatorToolInitVal = getInitiatorTool();
        }
        registerChange("initiator tool", _initiatorToolInitVal, initiatorTool);
        _initiatorTool = initiatorTool;
    }
                        
    /**
     * Returns the property 'initiator tool'.
     *
     * 
     *
     */
    public String getInitiatorTool() {
        return _initiatorTool;
    }
                                    /**
     * Setter for the property 'reached email timeout'.
     *
     * 
     *
     */
    public void setReachedEmailTimeout(boolean reachedEmailTimeout) {
        if (!_reachedEmailTimeoutIsSet) {
            _reachedEmailTimeoutIsSet = true;
            _reachedEmailTimeoutInitVal = getReachedEmailTimeout();
        }
        registerChange("reached email timeout", _reachedEmailTimeoutInitVal, reachedEmailTimeout);
        _reachedEmailTimeout = reachedEmailTimeout;
    }
                        
    /**
     * Returns the property 'reached email timeout'.
     *
     * 
     *
     */
    public boolean getReachedEmailTimeout() {
        return _reachedEmailTimeout;
    }
                                    /**
     * Setter for the property 'visible'.
     *
     * 
     *
     */
    public void setVisible(boolean visible) {
        if (!_visibleIsSet) {
            _visibleIsSet = true;
            _visibleInitVal = getVisible();
        }
        registerChange("visible", _visibleInitVal, visible);
        _visible = visible;
    }
                        
    /**
     * Returns the property 'visible'.
     *
     * 
     *
     */
    public boolean getVisible() {
        return _visible;
    }

    /**
     * Setter for the property '_initiatorSmavaId'.
     *
     *
     *
     */
    public void setInitiatorSmavaId(Long _initiatorSmavaId) {
        this._initiatorSmavaId = _initiatorSmavaId;
    }

    /**
     * Returns the property '_initiatorSmavaId'.
     *
     *
     *
     */
    public Long getInitiatorSmavaId() {
        return _initiatorSmavaId;
    }

    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_applicantRelationship instanceof de.smava.webapp.commons.domain.Entity && !_applicantRelationship.getChangeSet().isEmpty()) {
             for (String element : _applicantRelationship.getChangeSet()) {
                 result.add("applicant relationship : " + element);
             }
         }

         if (_vehicleInformation instanceof de.smava.webapp.commons.domain.Entity && !_vehicleInformation.getChangeSet().isEmpty()) {
             for (String element : _vehicleInformation.getChangeSet()) {
                 result.add("vehicle information : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LoanApplication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _requestedRdiType=").append(_requestedRdiType);
            builder.append("\n    _requestedAmount=").append(_requestedAmount);
            builder.append("\n    _requestedDuration=").append(_requestedDuration);
            builder.append("\n    _sharedLoan=").append(_sharedLoan);
            builder.append("\n    _category=").append(_category);
            builder.append("\n    _initiatorType=").append(_initiatorType);
            builder.append("\n    _initiatorTool=").append(_initiatorTool);
            builder.append("\n    _reachedEmailTimeout=").append(_reachedEmailTimeout);
            builder.append("\n    _visible=").append(_visible);
            builder.append("\n}");
        } else {
            builder.append(LoanApplication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LoanApplication asLoanApplication() {
        return this;
    }
}
