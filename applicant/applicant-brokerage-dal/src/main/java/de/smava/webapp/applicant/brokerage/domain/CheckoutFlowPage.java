//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(checkout flow page)}
import de.smava.webapp.applicant.brokerage.domain.history.CheckoutFlowPageHistory;

import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CheckoutFlowPages'.
 *
 * 
 *
 * @author generator
 */
public class CheckoutFlowPage extends CheckoutFlowPageHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(checkout flow page)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected String _state;
        protected Long _previousPageId;
        protected de.smava.webapp.applicant.brokerage.domain.CheckoutFlow _checkoutFlow;
        protected Set<CheckoutFlowPage> _nextPages;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public String getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'previous page id'.
     *
     * 
     *
     */
    public void setPreviousPageId(Long previousPageId) {
        _previousPageId = previousPageId;
    }
            
    /**
     * Returns the property 'previous page id'.
     *
     * 
     *
     */
    public Long getPreviousPageId() {
        return _previousPageId;
    }
                                            
    /**
     * Setter for the property 'checkout flow'.
     *
     * 
     *
     */
    public void setCheckoutFlow(de.smava.webapp.applicant.brokerage.domain.CheckoutFlow checkoutFlow) {
        _checkoutFlow = checkoutFlow;
    }
            
    /**
     * Returns the property 'checkout flow'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.CheckoutFlow getCheckoutFlow() {
        return _checkoutFlow;
    }
                                            
    /**
     * Setter for the property 'next pages'.
     *
     * 
     *
     */
    public void setNextPages(Set<CheckoutFlowPage> nextPages) {
        _nextPages = nextPages;
    }
            
    /**
     * Returns the property 'next pages'.
     *
     * 
     *
     */
    public Set<CheckoutFlowPage> getNextPages() {
        return _nextPages;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_checkoutFlow instanceof de.smava.webapp.commons.domain.Entity && !_checkoutFlow.getChangeSet().isEmpty()) {
             for (String element : _checkoutFlow.getChangeSet()) {
                 result.add("checkout flow : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CheckoutFlowPage.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _state=").append(_state);
            builder.append("\n}");
        } else {
            builder.append(CheckoutFlowPage.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CheckoutFlowPage asCheckoutFlowPage() {
        return this;
    }
}
