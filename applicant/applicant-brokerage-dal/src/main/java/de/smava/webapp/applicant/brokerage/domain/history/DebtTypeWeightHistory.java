package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractDebtTypeWeight;




/**
 * The domain object that has all history aggregation related fields for 'DebtTypeWeights'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class DebtTypeWeightHistory extends AbstractDebtTypeWeight {

    protected transient de.smava.webapp.applicant.type.IndebtednessDebtType _debtTypeInitVal;
    protected transient boolean _debtTypeIsSet;
    protected transient Double _weightInitVal;
    protected transient boolean _weightIsSet;


	
    /**
     * Returns the initial value of the property 'debt type'.
     */
    public de.smava.webapp.applicant.type.IndebtednessDebtType debtTypeInitVal() {
        de.smava.webapp.applicant.type.IndebtednessDebtType result;
        if (_debtTypeIsSet) {
            result = _debtTypeInitVal;
        } else {
            result = getDebtType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'debt type'.
     */
    public boolean debtTypeIsDirty() {
        return !valuesAreEqual(debtTypeInitVal(), getDebtType());
    }

    /**
     * Returns true if the setter method was called for the property 'debt type'.
     */
    public boolean debtTypeIsSet() {
        return _debtTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'weight'.
     */
    public Double weightInitVal() {
        Double result;
        if (_weightIsSet) {
            result = _weightInitVal;
        } else {
            result = getWeight();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'weight'.
     */
    public boolean weightIsDirty() {
        return !valuesAreEqual(weightInitVal(), getWeight());
    }

    /**
     * Returns true if the setter method was called for the property 'weight'.
     */
    public boolean weightIsSet() {
        return _weightIsSet;
    }
	
}
