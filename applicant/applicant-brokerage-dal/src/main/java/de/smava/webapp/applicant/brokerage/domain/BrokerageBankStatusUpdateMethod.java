package de.smava.webapp.applicant.brokerage.domain;

/**
 * @author Wojciech Teprek
 * @since 28.08.2017
 */
public enum BrokerageBankStatusUpdateMethod {
    STATE_REQUEST,
    PUSH_SERVICE,
    CSV_UPLOAD
}
