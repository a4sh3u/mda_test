//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageApplicationHistory;
import de.smava.webapp.applicant.domain.BankAccount;

import java.util.Date;
import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplications'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageApplication extends BrokerageApplicationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected BrokerageBank _brokerageBank;
        protected String _extRefNumber;
        protected String _prevExtRefNumber;
        protected de.smava.webapp.applicant.brokerage.type.BrokerageState _state;
        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected de.smava.webapp.applicant.domain.Applicant _firstApplicant;
        protected de.smava.webapp.applicant.domain.Applicant _secondApplicant;
        protected BankAccount _referenceBankAccount;
        protected de.smava.webapp.applicant.brokerage.type.RdiType _requestedRdiType;
        protected double _requestedAmount;
        protected int _requestedDuration;
        protected Double _monthlyRate;
        protected Double _effectiveInterest;
        protected Double _amount;
        protected Integer _duration;
        protected de.smava.webapp.applicant.brokerage.type.RdiType _rdiType;
        protected BrokerageApplicationFilterReason _brokerageApplicationFilterReason;
        protected BrokerageApplicationFilterReason _secondApplicantFilterReason;
        protected de.smava.webapp.applicant.brokerage.domain.LoanApplication _loanApplication;
        protected Set<BrokerageApplicationRequestData> _brokerageApplicationRequestDatas;
        protected de.smava.webapp.applicant.brokerage.type.Category _category;
        protected Date _papSaleTrackedDate;
        protected Date _lastStateRequest;
        protected Date _lastStateChange;
        protected de.smava.webapp.applicant.account.domain.DocumentContainer _documentContainer;
        protected Date _emailCreated;
        protected String _emailType;
        protected Date _documentsRequested;
        protected Date _documentsSent;
        protected String _additionalData;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation _householdCalculation;
        protected Date _payoutDate;
        protected Boolean _isSecondApplicantAllowed;
        protected Set<BrokerageFilterReasonMultiVal> _brokerageFilterReasonMultiValues;
        protected Set<BrokerageApplicationDocument> _documents;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'ext ref number'.
     *
     * 
     *
     */
    public void setExtRefNumber(String extRefNumber) {
        if (!_extRefNumberIsSet) {
            _extRefNumberIsSet = true;
            _extRefNumberInitVal = getExtRefNumber();
        }
        registerChange("ext ref number", _extRefNumberInitVal, extRefNumber);
        _extRefNumber = extRefNumber;
    }
                        
    /**
     * Returns the property 'ext ref number'.
     *
     * 
     *
     */
    public String getExtRefNumber() {
        return _extRefNumber;
    }
                                    /**
     * Setter for the property 'prev ext ref number'.
     *
     * 
     *
     */
    public void setPrevExtRefNumber(String prevExtRefNumber) {
        if (!_prevExtRefNumberIsSet) {
            _prevExtRefNumberIsSet = true;
            _prevExtRefNumberInitVal = getPrevExtRefNumber();
        }
        registerChange("prev ext ref number", _prevExtRefNumberInitVal, prevExtRefNumber);
        _prevExtRefNumber = prevExtRefNumber;
    }
                        
    /**
     * Returns the property 'prev ext ref number'.
     *
     * 
     *
     */
    public String getPrevExtRefNumber() {
        return _prevExtRefNumber;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(de.smava.webapp.applicant.brokerage.type.BrokerageState state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageState getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                            
    /**
     * Setter for the property 'first applicant'.
     *
     * 
     *
     */
    public void setFirstApplicant(de.smava.webapp.applicant.domain.Applicant firstApplicant) {
        _firstApplicant = firstApplicant;
    }
            
    /**
     * Returns the property 'first applicant'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Applicant getFirstApplicant() {
        return _firstApplicant;
    }
                                            
    /**
     * Setter for the property 'second applicant'.
     *
     * 
     *
     */
    public void setSecondApplicant(de.smava.webapp.applicant.domain.Applicant secondApplicant) {
        _secondApplicant = secondApplicant;
    }
            
    /**
     * Returns the property 'second applicant'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Applicant getSecondApplicant() {
        return _secondApplicant;
    }
                                            
    /**
     * Setter for the property 'reference bank account'.
     *
     * 
     *
     */
    public void setReferenceBankAccount(BankAccount referenceBankAccount) {
        _referenceBankAccount = referenceBankAccount;
    }
            
    /**
     * Returns the property 'reference bank account'.
     *
     * 
     *
     */
    public BankAccount getReferenceBankAccount() {
        return _referenceBankAccount;
    }
                                    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    public void setRequestedRdiType(de.smava.webapp.applicant.brokerage.type.RdiType requestedRdiType) {
        if (!_requestedRdiTypeIsSet) {
            _requestedRdiTypeIsSet = true;
            _requestedRdiTypeInitVal = getRequestedRdiType();
        }
        registerChange("requested rdi type", _requestedRdiTypeInitVal, requestedRdiType);
        _requestedRdiType = requestedRdiType;
    }
                        
    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType getRequestedRdiType() {
        return _requestedRdiType;
    }
                                    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    public void setRequestedAmount(double requestedAmount) {
        if (!_requestedAmountIsSet) {
            _requestedAmountIsSet = true;
            _requestedAmountInitVal = getRequestedAmount();
        }
        registerChange("requested amount", _requestedAmountInitVal, requestedAmount);
        _requestedAmount = requestedAmount;
    }
                        
    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    public double getRequestedAmount() {
        return _requestedAmount;
    }
                                    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    public void setRequestedDuration(int requestedDuration) {
        if (!_requestedDurationIsSet) {
            _requestedDurationIsSet = true;
            _requestedDurationInitVal = getRequestedDuration();
        }
        registerChange("requested duration", _requestedDurationInitVal, requestedDuration);
        _requestedDuration = requestedDuration;
    }
                        
    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    public int getRequestedDuration() {
        return _requestedDuration;
    }
                                    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    public void setMonthlyRate(Double monthlyRate) {
        if (!_monthlyRateIsSet) {
            _monthlyRateIsSet = true;
            _monthlyRateInitVal = getMonthlyRate();
        }
        registerChange("monthly rate", _monthlyRateInitVal, monthlyRate);
        _monthlyRate = monthlyRate;
    }
                        
    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    public Double getMonthlyRate() {
        return _monthlyRate;
    }
                                    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    public void setEffectiveInterest(Double effectiveInterest) {
        if (!_effectiveInterestIsSet) {
            _effectiveInterestIsSet = true;
            _effectiveInterestInitVal = getEffectiveInterest();
        }
        registerChange("effective interest", _effectiveInterestInitVal, effectiveInterest);
        _effectiveInterest = effectiveInterest;
    }
                        
    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    public Double getEffectiveInterest() {
        return _effectiveInterest;
    }
                                    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(Double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public Double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    public void setDuration(Integer duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = getDuration();
        }
        registerChange("duration", _durationInitVal, duration);
        _duration = duration;
    }
                        
    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    public Integer getDuration() {
        return _duration;
    }
                                    /**
     * Setter for the property 'rdi type'.
     *
     * 
     *
     */
    public void setRdiType(de.smava.webapp.applicant.brokerage.type.RdiType rdiType) {
        if (!_rdiTypeIsSet) {
            _rdiTypeIsSet = true;
            _rdiTypeInitVal = getRdiType();
        }
        registerChange("rdi type", _rdiTypeInitVal, rdiType);
        _rdiType = rdiType;
    }
                        
    /**
     * Returns the property 'rdi type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType getRdiType() {
        return _rdiType;
    }
                                            
    /**
     * Setter for the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    public void setBrokerageApplicationFilterReason(BrokerageApplicationFilterReason brokerageApplicationFilterReason) {
        _brokerageApplicationFilterReason = brokerageApplicationFilterReason;
    }
            
    /**
     * Returns the property 'brokerage application filter reason'.
     *
     * 
     *
     */
    public BrokerageApplicationFilterReason getBrokerageApplicationFilterReason() {
        return _brokerageApplicationFilterReason;
    }
                                            
    /**
     * Setter for the property 'second applicant filter reason'.
     *
     * 
     *
     */
    public void setSecondApplicantFilterReason(BrokerageApplicationFilterReason secondApplicantFilterReason) {
        _secondApplicantFilterReason = secondApplicantFilterReason;
    }
            
    /**
     * Returns the property 'second applicant filter reason'.
     *
     * 
     *
     */
    public BrokerageApplicationFilterReason getSecondApplicantFilterReason() {
        return _secondApplicantFilterReason;
    }
                                            
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                            
    /**
     * Setter for the property 'brokerage application request datas'.
     *
     * 
     *
     */
    public void setBrokerageApplicationRequestDatas(Set<BrokerageApplicationRequestData> brokerageApplicationRequestDatas) {
        _brokerageApplicationRequestDatas = brokerageApplicationRequestDatas;
    }
            
    /**
     * Returns the property 'brokerage application request datas'.
     *
     * 
     *
     */
    public Set<BrokerageApplicationRequestData> getBrokerageApplicationRequestDatas() {
        return _brokerageApplicationRequestDatas;
    }
                                    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    public void setCategory(de.smava.webapp.applicant.brokerage.type.Category category) {
        if (!_categoryIsSet) {
            _categoryIsSet = true;
            _categoryInitVal = getCategory();
        }
        registerChange("category", _categoryInitVal, category);
        _category = category;
    }
                        
    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.Category getCategory() {
        return _category;
    }
                                    /**
     * Setter for the property 'pap sale tracked date'.
     *
     * 
     *
     */
    public void setPapSaleTrackedDate(Date papSaleTrackedDate) {
        if (!_papSaleTrackedDateIsSet) {
            _papSaleTrackedDateIsSet = true;
            _papSaleTrackedDateInitVal = getPapSaleTrackedDate();
        }
        registerChange("pap sale tracked date", _papSaleTrackedDateInitVal, papSaleTrackedDate);
        _papSaleTrackedDate = papSaleTrackedDate;
    }
                        
    /**
     * Returns the property 'pap sale tracked date'.
     *
     * 
     *
     */
    public Date getPapSaleTrackedDate() {
        return _papSaleTrackedDate;
    }
                                    /**
     * Setter for the property 'last state request'.
     *
     * 
     *
     */
    public void setLastStateRequest(Date lastStateRequest) {
        if (!_lastStateRequestIsSet) {
            _lastStateRequestIsSet = true;
            _lastStateRequestInitVal = getLastStateRequest();
        }
        registerChange("last state request", _lastStateRequestInitVal, lastStateRequest);
        _lastStateRequest = lastStateRequest;
    }
                        
    /**
     * Returns the property 'last state request'.
     *
     * 
     *
     */
    public Date getLastStateRequest() {
        return _lastStateRequest;
    }
                                    /**
     * Setter for the property 'last state change'.
     *
     * 
     *
     */
    public void setLastStateChange(Date lastStateChange) {
        if (!_lastStateChangeIsSet) {
            _lastStateChangeIsSet = true;
            _lastStateChangeInitVal = getLastStateChange();
        }
        registerChange("last state change", _lastStateChangeInitVal, lastStateChange);
        _lastStateChange = lastStateChange;
    }
                        
    /**
     * Returns the property 'last state change'.
     *
     * 
     *
     */
    public Date getLastStateChange() {
        return _lastStateChange;
    }
                                            
    /**
     * Setter for the property 'document container'.
     *
     * 
     *
     */
    public void setDocumentContainer(de.smava.webapp.applicant.account.domain.DocumentContainer documentContainer) {
        _documentContainer = documentContainer;
    }
            
    /**
     * Returns the property 'document container'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.DocumentContainer getDocumentContainer() {
        return _documentContainer;
    }
                                    /**
     * Setter for the property 'email created'.
     *
     * 
     *
     */
    public void setEmailCreated(Date emailCreated) {
        if (!_emailCreatedIsSet) {
            _emailCreatedIsSet = true;
            _emailCreatedInitVal = getEmailCreated();
        }
        registerChange("email created", _emailCreatedInitVal, emailCreated);
        _emailCreated = emailCreated;
    }
                        
    /**
     * Returns the property 'email created'.
     *
     * 
     *
     */
    public Date getEmailCreated() {
        return _emailCreated;
    }
                                    /**
     * Setter for the property 'email type'.
     *
     * 
     *
     */
    public void setEmailType(String emailType) {
        if (!_emailTypeIsSet) {
            _emailTypeIsSet = true;
            _emailTypeInitVal = getEmailType();
        }
        registerChange("email type", _emailTypeInitVal, emailType);
        _emailType = emailType;
    }
                        
    /**
     * Returns the property 'email type'.
     *
     * 
     *
     */
    public String getEmailType() {
        return _emailType;
    }
                                    /**
     * Setter for the property 'documents requested'.
     *
     * 
     *
     */
    public void setDocumentsRequested(Date documentsRequested) {
        if (!_documentsRequestedIsSet) {
            _documentsRequestedIsSet = true;
            _documentsRequestedInitVal = getDocumentsRequested();
        }
        registerChange("documents requested", _documentsRequestedInitVal, documentsRequested);
        _documentsRequested = documentsRequested;
    }
                        
    /**
     * Returns the property 'documents requested'.
     *
     * 
     *
     */
    public Date getDocumentsRequested() {
        return _documentsRequested;
    }
                                    /**
     * Setter for the property 'documents sent'.
     *
     * 
     *
     */
    public void setDocumentsSent(Date documentsSent) {
        if (!_documentsSentIsSet) {
            _documentsSentIsSet = true;
            _documentsSentInitVal = getDocumentsSent();
        }
        registerChange("documents sent", _documentsSentInitVal, documentsSent);
        _documentsSent = documentsSent;
    }
                        
    /**
     * Returns the property 'documents sent'.
     *
     * 
     *
     */
    public Date getDocumentsSent() {
        return _documentsSent;
    }
                                    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    public void setAdditionalData(String additionalData) {
        if (!_additionalDataIsSet) {
            _additionalDataIsSet = true;
            _additionalDataInitVal = getAdditionalData();
        }
        registerChange("additional data", _additionalDataInitVal, additionalData);
        _additionalData = additionalData;
    }
                        
    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    public String getAdditionalData() {
        return _additionalData;
    }
                                            
    /**
     * Setter for the property 'household calculation'.
     *
     * 
     *
     */
    public void setHouseholdCalculation(de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation householdCalculation) {
        _householdCalculation = householdCalculation;
    }
            
    /**
     * Returns the property 'household calculation'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation getHouseholdCalculation() {
        return _householdCalculation;
    }
                                    /**
     * Setter for the property 'payout date'.
     *
     * 
     *
     */
    public void setPayoutDate(Date payoutDate) {
        if (!_payoutDateIsSet) {
            _payoutDateIsSet = true;
            _payoutDateInitVal = getPayoutDate();
        }
        registerChange("payout date", _payoutDateInitVal, payoutDate);
        _payoutDate = payoutDate;
    }
                        
    /**
     * Returns the property 'payout date'.
     *
     * 
     *
     */
    public Date getPayoutDate() {
        return _payoutDate;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerage bank : " + element);
             }
         }

         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_firstApplicant instanceof de.smava.webapp.commons.domain.Entity && !_firstApplicant.getChangeSet().isEmpty()) {
             for (String element : _firstApplicant.getChangeSet()) {
                 result.add("first applicant : " + element);
             }
         }

         if (_secondApplicant instanceof de.smava.webapp.commons.domain.Entity && !_secondApplicant.getChangeSet().isEmpty()) {
             for (String element : _secondApplicant.getChangeSet()) {
                 result.add("second applicant : " + element);
             }
         }

         if (_referenceBankAccount instanceof de.smava.webapp.commons.domain.Entity && !_referenceBankAccount.getChangeSet().isEmpty()) {
             for (String element : _referenceBankAccount.getChangeSet()) {
                 result.add("reference bank account : " + element);
             }
         }

         if (_brokerageApplicationFilterReason instanceof de.smava.webapp.commons.domain.Entity && !_brokerageApplicationFilterReason.getChangeSet().isEmpty()) {
             for (String element : _brokerageApplicationFilterReason.getChangeSet()) {
                 result.add("brokerage application filter reason : " + element);
             }
         }

         if (_secondApplicantFilterReason instanceof de.smava.webapp.commons.domain.Entity && !_secondApplicantFilterReason.getChangeSet().isEmpty()) {
             for (String element : _secondApplicantFilterReason.getChangeSet()) {
                 result.add("second applicant filter reason : " + element);
             }
         }

         if (_loanApplication instanceof de.smava.webapp.commons.domain.Entity && !_loanApplication.getChangeSet().isEmpty()) {
             for (String element : _loanApplication.getChangeSet()) {
                 result.add("loan application : " + element);
             }
         }

         if (_documentContainer instanceof de.smava.webapp.commons.domain.Entity && !_documentContainer.getChangeSet().isEmpty()) {
             for (String element : _documentContainer.getChangeSet()) {
                 result.add("document container : " + element);
             }
         }

         if (_householdCalculation instanceof de.smava.webapp.commons.domain.Entity && !_householdCalculation.getChangeSet().isEmpty()) {
             for (String element : _householdCalculation.getChangeSet()) {
                 result.add("household calculation : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplication.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _extRefNumber=").append(_extRefNumber);
            builder.append("\n    _prevExtRefNumber=").append(_prevExtRefNumber);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _requestedRdiType=").append(_requestedRdiType);
            builder.append("\n    _requestedAmount=").append(_requestedAmount);
            builder.append("\n    _requestedDuration=").append(_requestedDuration);
            builder.append("\n    _rdiType=").append(_rdiType);
            builder.append("\n    _category=").append(_category);
            builder.append("\n    _emailType=").append(_emailType);
            builder.append("\n    _additionalData=").append(_additionalData);
            builder.append("\n    _isSecondApplicantAllowed=").append(_isSecondApplicantAllowed);
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplication.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplication asBrokerageApplication() {
        return this;
    }

    /**
     * Returns the property isSecondApplicantAllowed
     */
    @Override
    public Boolean isSecondApplicantAllowed() {
        return _isSecondApplicantAllowed;
    }

    /**
     * Setter for the property isSecondApplicantAllowed
     */
    @Override
    public void setSecondApplicantAllowed(Boolean secondApplicantAllowed) {

        if (!_isSecondApplicantAllowedIsSet) {
            _isSecondApplicantAllowedIsSet = true;
            _isSecondApplicantAllowedInitVal = isSecondApplicantAllowed();
        }
        registerChange("is second applicant allowed", _isSecondApplicantAllowedInitVal, secondApplicantAllowed);
        _isSecondApplicantAllowed = secondApplicantAllowed;
    }

    @Override
    public Set<BrokerageFilterReasonMultiVal> getBrokerageFilterReasonMultiValues() {
        return _brokerageFilterReasonMultiValues;
    }

    @Override
    public void setBrokerageFilterReasonMultiValues(Set<BrokerageFilterReasonMultiVal> brokerageFilterReasonMultiValues) {
        _brokerageFilterReasonMultiValues = brokerageFilterReasonMultiValues;
    }

    @Override
    public Set<BrokerageApplicationDocument> getDocuments() {
        return _documents;
    }

    @Override
    public void setDocuments(Set<BrokerageApplicationDocument> documents) {
        _documents = documents;
    }
}
