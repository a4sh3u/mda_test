package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.history.BrokerageApplicationDocumentHistory;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentDispatchType;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentState;

import java.util.Date;

/**
 * The domain object that represents 'BrokerageApplicationDocuments'.
 *
 * @author Adam Tomecki
 * @since 30.10.2017
 */
public class BrokerageApplicationDocument extends BrokerageApplicationDocumentHistory {

    private Date _creationDate;
    private BrokerageApplication _brokerageApplication;
    private String _contentType;
    private BrokerageApplicationDocumentDispatchType _dispatchType;
    private Long _fileStorageId;
    private String _name;
    private String _notes;
    private BrokerageApplicationDocumentState _state;
    private String _subject;

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }

    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }

    public String getContentType() {
        return _contentType;
    }

    public void setContentType(String contentType) {
        _contentType = contentType;
    }

    public BrokerageApplicationDocumentDispatchType getDispatchType() {
        return _dispatchType;
    }

    public void setDispatchType(BrokerageApplicationDocumentDispatchType dispatchType) {
        if (!dispatchTypeIsSet()) {
            _dispatchTypeIsSet = true;
            _dispatchTypeInitVal = getDispatchType();
        }
        registerChange("dispatchType", _dispatchTypeInitVal, dispatchType);
        _dispatchType = dispatchType;
    }

    public Long getFileStorageId() {
        return _fileStorageId;
    }

    public void setFileStorageId(Long fileStorageId) {
        _fileStorageId = fileStorageId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getNotes() {
        return _notes;
    }

    public void setNotes(String notes) {
        _notes = notes;
    }

    public BrokerageApplicationDocumentState getState() {
        return _state;
    }

    public void setState(BrokerageApplicationDocumentState state) {
        if (!stateIsSet()) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }

    public String getSubject() {
        return _subject;
    }

    public void setSubject(String subject) {
        _subject = subject;
    }
}
