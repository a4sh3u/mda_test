package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.domain.BrokerageFilterReasonMultiVal;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;

import javax.transaction.Synchronization;
import java.util.Collection;

/**
 * @author Adam Budzinski
 * @since 05.09.2017
 */
public interface BrokerageFilterReasonMultiValDao extends BrokerageSchemaDao<BrokerageFilterReasonMultiVal> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    Collection<BrokerageFilterReasonMultiVal> getByBrokerageApplicationId(Long brokerageApplicationId);

    void deleteMainReasonEntriesForBa(Long brokerageApplicationId);
}
