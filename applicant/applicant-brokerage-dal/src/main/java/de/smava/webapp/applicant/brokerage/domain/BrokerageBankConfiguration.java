//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank configuration)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageBankConfigurationHistory;

import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBankConfigurations'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBankConfiguration extends BrokerageBankConfigurationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bank configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected boolean _documentUpload;
        protected boolean _documentUploadViaSftp;
        protected boolean _documentUploadViaPost;
        protected boolean _documentUploadViaBankApi;
        protected boolean _videoIdent;
        protected boolean _esign;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBank _brokerageBank;
        protected Set<RequestedDocumentsConfiguration> _requestedDocumentsConfigurations;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'document upload'.
     *
     * 
     *
     */
    public void setDocumentUpload(boolean documentUpload) {
        if (!_documentUploadIsSet) {
            _documentUploadIsSet = true;
            _documentUploadInitVal = getDocumentUpload();
        }
        registerChange("document upload", _documentUploadInitVal, documentUpload);
        _documentUpload = documentUpload;
    }
                        
    /**
     * Returns the property 'document upload'.
     *
     * 
     *
     */
    public boolean getDocumentUpload() {
        return _documentUpload;
    }
                                    /**
     * Setter for the property 'document upload via sftp'.
     *
     * 
     *
     */
    public void setDocumentUploadViaSftp(boolean documentUploadViaSftp) {
        if (!_documentUploadViaSftpIsSet) {
            _documentUploadViaSftpIsSet = true;
            _documentUploadViaSftpInitVal = getDocumentUploadViaSftp();
        }
        registerChange("document upload via sftp", _documentUploadViaSftpInitVal, documentUploadViaSftp);
        _documentUploadViaSftp = documentUploadViaSftp;
    }
                        
    /**
     * Returns the property 'document upload via sftp'.
     *
     * 
     *
     */
    public boolean getDocumentUploadViaSftp() {
        return _documentUploadViaSftp;
    }
                                    /**
     * Setter for the property 'document upload via post'.
     *
     * 
     *
     */
    public void setDocumentUploadViaPost(boolean documentUploadViaPost) {
        if (!_documentUploadViaPostIsSet) {
            _documentUploadViaPostIsSet = true;
            _documentUploadViaPostInitVal = getDocumentUploadViaPost();
        }
        registerChange("document upload via post", _documentUploadViaPostInitVal, documentUploadViaPost);
        _documentUploadViaPost = documentUploadViaPost;
    }
                        
    /**
     * Returns the property 'document upload via post'.
     *
     * 
     *
     */
    public boolean getDocumentUploadViaPost() {
        return _documentUploadViaPost;
    }
                                    /**
     * Setter for the property 'document upload via bank api'.
     *
     * 
     *
     */
    public void setDocumentUploadViaBankApi(boolean documentUploadViaBankApi) {
        if (!_documentUploadViaBankApiIsSet) {
            _documentUploadViaBankApiIsSet = true;
            _documentUploadViaBankApiInitVal = getDocumentUploadViaBankApi();
        }
        registerChange("document upload via bank api", _documentUploadViaBankApiInitVal, documentUploadViaBankApi);
        _documentUploadViaBankApi = documentUploadViaBankApi;
    }
                        
    /**
     * Returns the property 'document upload via bank api'.
     *
     * 
     *
     */
    public boolean getDocumentUploadViaBankApi() {
        return _documentUploadViaBankApi;
    }
                                    /**
     * Setter for the property 'video ident'.
     *
     * 
     *
     */
    public void setVideoIdent(boolean videoIdent) {
        if (!_videoIdentIsSet) {
            _videoIdentIsSet = true;
            _videoIdentInitVal = getVideoIdent();
        }
        registerChange("video ident", _videoIdentInitVal, videoIdent);
        _videoIdent = videoIdent;
    }
                        
    /**
     * Returns the property 'video ident'.
     *
     * 
     *
     */
    public boolean getVideoIdent() {
        return _videoIdent;
    }
                                    /**
     * Setter for the property 'esign'.
     *
     * 
     *
     */
    public void setEsign(boolean esign) {
        if (!_esignIsSet) {
            _esignIsSet = true;
            _esignInitVal = getEsign();
        }
        registerChange("esign", _esignInitVal, esign);
        _esign = esign;
    }
                        
    /**
     * Returns the property 'esign'.
     *
     * 
     *
     */
    public boolean getEsign() {
        return _esign;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'requested documents configurations'.
     *
     * 
     *
     */
    public void setRequestedDocumentsConfigurations(Set<RequestedDocumentsConfiguration> requestedDocumentsConfigurations) {
        _requestedDocumentsConfigurations = requestedDocumentsConfigurations;
    }
            
    /**
     * Returns the property 'requested documents configurations'.
     *
     * 
     *
     */
    public Set<RequestedDocumentsConfiguration> getRequestedDocumentsConfigurations() {
        return _requestedDocumentsConfigurations;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerage bank : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBankConfiguration.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _documentUpload=").append(_documentUpload);
            builder.append("\n    _documentUploadViaSftp=").append(_documentUploadViaSftp);
            builder.append("\n    _documentUploadViaPost=").append(_documentUploadViaPost);
            builder.append("\n    _documentUploadViaBankApi=").append(_documentUploadViaBankApi);
            builder.append("\n    _videoIdent=").append(_videoIdent);
            builder.append("\n    _esign=").append(_esign);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBankConfiguration.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBankConfiguration asBrokerageBankConfiguration() {
        return this;
    }
}
