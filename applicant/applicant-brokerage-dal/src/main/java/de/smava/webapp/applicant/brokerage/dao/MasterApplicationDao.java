//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(master application)}

import de.smava.webapp.applicant.brokerage.domain.MasterApplication;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'MasterApplications'.
 *
 * @author generator
 */
public interface MasterApplicationDao extends BaseDao<MasterApplication> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the master application identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     */
    MasterApplication getMasterApplication(Long id);

    /**
     * Saves the master application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveMasterApplication(MasterApplication masterApplication);

    /**
     * Deletes an master application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the master application
     */
    void deleteMasterApplication(Long id);

    /**
     * Retrieves all 'MasterApplication' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<MasterApplication> getMasterApplicationList();

    /**
     * Retrieves a page of 'MasterApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<MasterApplication> getMasterApplicationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'MasterApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<MasterApplication> getMasterApplicationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'MasterApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<MasterApplication> getMasterApplicationList(Pageable pageable, Sortable sortable);


    /**
     * Returns the number of 'MasterApplication' instances.
     */
    long getMasterApplicationCount();


    // !!!!!!!! You can insert code here: !!!!!!!!                                                                                                                  {|customMethods|bean(master application)}

    MasterApplication findCurrentMasterApplicationForAccount(Long accountId, int masterApplicationLifetime);

    MasterApplication findMasterApplicationForLoanApplication(Date creationDate, Long accountId);

    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
