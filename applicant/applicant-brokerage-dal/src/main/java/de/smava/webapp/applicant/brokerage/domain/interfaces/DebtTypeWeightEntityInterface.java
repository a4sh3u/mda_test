package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.DebtTypeWeight;


/**
 * The domain object that represents 'DebtTypeWeights'.
 *
 * @author generator
 */
public interface DebtTypeWeightEntityInterface {

    /**
     * Setter for the property 'debt type'.
     *
     * 
     *
     */
    void setDebtType(de.smava.webapp.applicant.type.IndebtednessDebtType debtType);

    /**
     * Returns the property 'debt type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.IndebtednessDebtType getDebtType();
    /**
     * Setter for the property 'weight'.
     *
     * 
     *
     */
    void setWeight(Double weight);

    /**
     * Returns the property 'weight'.
     *
     * 
     *
     */
    Double getWeight();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Helper method to get reference of this object as model type.
     */
    DebtTypeWeight asDebtTypeWeight();
}
