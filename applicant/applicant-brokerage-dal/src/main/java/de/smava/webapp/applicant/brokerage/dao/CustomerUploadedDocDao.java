package de.smava.webapp.applicant.brokerage.dao;

import java.util.Collection;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.applicant.brokerage.domain.DocumentStatus;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.brokerage.domain.CustomerUploadedDoc;

/**
 * DAO interface for the domain object 'CustomerUploadedDocs'.
 */
public interface CustomerUploadedDocDao extends BrokerageSchemaDao<CustomerUploadedDoc> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the customer uploaded doc identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    CustomerUploadedDoc getCustomerUploadedDoc(Long id);

    /**
     * Saves the customer uploaded doc.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveCustomerUploadedDoc(CustomerUploadedDoc customerUploadedDoc);

    /**
     * Deletes an customer uploaded doc, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the customer uploaded doc
     */
    void deleteCustomerUploadedDoc(Long id);

    /**
     * Retrieves all 'CustomerUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<CustomerUploadedDoc> getCustomerUploadedDocList();

    /**
     * Retrieves a page of 'CustomerUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<CustomerUploadedDoc> getCustomerUploadedDocList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'CustomerUploadedDoc' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<CustomerUploadedDoc> getCustomerUploadedDocList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'CustomerUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<CustomerUploadedDoc> getCustomerUploadedDocList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'CustomerUploadedDoc' instances.
     */
    long getCustomerUploadedDocCount();

    Collection<CustomerUploadedDoc> fetchDocumentsByAccount(Long accountId);

    Collection<CustomerUploadedDoc> fetchDocumentsBySmavaId(Long smavaId);

    Collection<CustomerUploadedDoc> fetchDocuments(List<Long> documentIds);

    Collection<CustomerUploadedDoc> fetchDocuments(Long accountId, List<Long> documentIds);

    CustomerUploadedDoc fetchDocumentByAccount(Long id, Long accountId);

    Collection<CustomerUploadedDoc> fetchDocumentsByAccountAndStatusIn(Long accountId, List<DocumentStatus> status);

}
