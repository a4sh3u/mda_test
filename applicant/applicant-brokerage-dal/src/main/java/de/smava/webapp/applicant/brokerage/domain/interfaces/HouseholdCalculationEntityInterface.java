package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation;


/**
 * The domain object that represents 'HouseholdCalculations'.
 *
 * @author generator
 */
public interface HouseholdCalculationEntityInterface {

    /**
     * Setter for the property 'type'.
     *
     * Distinguishes the type of this household calculation item
     *
     */
    void setType(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType type);

    /**
     * Returns the property 'type'.
     *
     * Distinguishes the type of this household calculation item
     *
     */
    de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType getType();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName getName();
    /**
     * Setter for the property 'mapping expression'.
     *
     * Mapping from applicant data in Spring expression language
     *
     */
    void setMappingExpression(String mappingExpression);

    /**
     * Returns the property 'mapping expression'.
     *
     * Mapping from applicant data in Spring expression language
     *
     */
    String getMappingExpression();
    /**
     * Helper method to get reference of this object as model type.
     */
    HouseholdCalculation asHouseholdCalculation();
}
