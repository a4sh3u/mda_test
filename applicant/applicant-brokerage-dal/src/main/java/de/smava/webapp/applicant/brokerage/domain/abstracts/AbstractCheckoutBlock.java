//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(checkout block)}

import de.smava.webapp.applicant.brokerage.domain.CheckoutBlock;
import de.smava.webapp.applicant.brokerage.domain.interfaces.CheckoutBlockEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CheckoutBlocks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCheckoutBlock
    extends BrokerageEntity    implements CheckoutBlockEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCheckoutBlock.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof CheckoutBlock)) {
            return;
        }
        
        this.setName(((CheckoutBlock) oldEntity).getName());    
        this.setSortOrder(((CheckoutBlock) oldEntity).getSortOrder());    
            
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof CheckoutBlock)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((CheckoutBlock) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getSortOrder(), ((CheckoutBlock) otherEntity).getSortOrder());    
            
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(checkout block)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

