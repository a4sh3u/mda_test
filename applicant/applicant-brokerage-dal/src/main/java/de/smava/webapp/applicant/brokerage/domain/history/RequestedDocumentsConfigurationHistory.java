package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractRequestedDocumentsConfiguration;




/**
 * The domain object that has all history aggregation related fields for 'RequestedDocumentsConfigurations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RequestedDocumentsConfigurationHistory extends AbstractRequestedDocumentsConfiguration {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _employmentTypeInitVal;
    protected transient boolean _employmentTypeIsSet;
    protected transient String _applicationTypeInitVal;
    protected transient boolean _applicationTypeIsSet;
    protected transient Integer _minVolumeInitVal;
    protected transient boolean _minVolumeIsSet;
    protected transient Integer _maxVolumeInitVal;
    protected transient boolean _maxVolumeIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'employment type'.
     */
    public String employmentTypeInitVal() {
        String result;
        if (_employmentTypeIsSet) {
            result = _employmentTypeInitVal;
        } else {
            result = getEmploymentType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employment type'.
     */
    public boolean employmentTypeIsDirty() {
        return !valuesAreEqual(employmentTypeInitVal(), getEmploymentType());
    }

    /**
     * Returns true if the setter method was called for the property 'employment type'.
     */
    public boolean employmentTypeIsSet() {
        return _employmentTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'application type'.
     */
    public String applicationTypeInitVal() {
        String result;
        if (_applicationTypeIsSet) {
            result = _applicationTypeInitVal;
        } else {
            result = getApplicationType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'application type'.
     */
    public boolean applicationTypeIsDirty() {
        return !valuesAreEqual(applicationTypeInitVal(), getApplicationType());
    }

    /**
     * Returns true if the setter method was called for the property 'application type'.
     */
    public boolean applicationTypeIsSet() {
        return _applicationTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'min volume'.
     */
    public Integer minVolumeInitVal() {
        Integer result;
        if (_minVolumeIsSet) {
            result = _minVolumeInitVal;
        } else {
            result = getMinVolume();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'min volume'.
     */
    public boolean minVolumeIsDirty() {
        return !valuesAreEqual(minVolumeInitVal(), getMinVolume());
    }

    /**
     * Returns true if the setter method was called for the property 'min volume'.
     */
    public boolean minVolumeIsSet() {
        return _minVolumeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'max Volume'.
     */
    public Integer maxVolumeInitVal() {
        Integer result;
        if (_maxVolumeIsSet) {
            result = _maxVolumeInitVal;
        } else {
            result = getMaxVolume();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'max Volume'.
     */
    public boolean maxVolumeIsDirty() {
        return !valuesAreEqual(maxVolumeInitVal(), getMaxVolume());
    }

    /**
     * Returns true if the setter method was called for the property 'max Volume'.
     */
    public boolean maxVolumeIsSet() {
        return _maxVolumeIsSet;
    }
	
}
