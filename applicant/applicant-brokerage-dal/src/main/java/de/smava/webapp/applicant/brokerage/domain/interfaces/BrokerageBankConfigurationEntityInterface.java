package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration;
import de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration;

import java.util.Set;


/**
 * The domain object that represents 'BrokerageBankConfigurations'.
 *
 * @author generator
 */
public interface BrokerageBankConfigurationEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'document upload'.
     *
     * 
     *
     */
    void setDocumentUpload(boolean documentUpload);

    /**
     * Returns the property 'document upload'.
     *
     * 
     *
     */
    boolean getDocumentUpload();
    /**
     * Setter for the property 'document upload via sftp'.
     *
     * 
     *
     */
    void setDocumentUploadViaSftp(boolean documentUploadViaSftp);

    /**
     * Returns the property 'document upload via sftp'.
     *
     * 
     *
     */
    boolean getDocumentUploadViaSftp();
    /**
     * Setter for the property 'document upload via post'.
     *
     * 
     *
     */
    void setDocumentUploadViaPost(boolean documentUploadViaPost);

    /**
     * Returns the property 'document upload via post'.
     *
     * 
     *
     */
    boolean getDocumentUploadViaPost();
    /**
     * Setter for the property 'document upload via bank api'.
     *
     * 
     *
     */
    void setDocumentUploadViaBankApi(boolean documentUploadViaBankApi);

    /**
     * Returns the property 'document upload via bank api'.
     *
     * 
     *
     */
    boolean getDocumentUploadViaBankApi();
    /**
     * Setter for the property 'video ident'.
     *
     * 
     *
     */
    void setVideoIdent(boolean videoIdent);

    /**
     * Returns the property 'video ident'.
     *
     * 
     *
     */
    boolean getVideoIdent();
    /**
     * Setter for the property 'esign'.
     *
     *
     *
     */
    void setEsign(boolean esign);

    /**
     * Returns the property 'esign'.
     *
     *
     *
     */
    boolean getEsign();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'requested documents configurations'.
     *
     * 
     *
     */
    void setRequestedDocumentsConfigurations(Set<RequestedDocumentsConfiguration> requestedDocumentsConfigurations);

    /**
     * Returns the property 'requested documents configurations'.
     *
     * 
     *
     */
    Set<RequestedDocumentsConfiguration> getRequestedDocumentsConfigurations();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBankConfiguration asBrokerageBankConfiguration();
}
