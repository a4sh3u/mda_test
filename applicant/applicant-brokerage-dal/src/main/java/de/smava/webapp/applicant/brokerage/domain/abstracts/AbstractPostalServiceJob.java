package de.smava.webapp.applicant.brokerage.domain.abstracts;

import de.smava.webapp.applicant.brokerage.domain.interfaces.PostalServiceJobEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public abstract class AbstractPostalServiceJob
        extends BrokerageEntity implements PostalServiceJobEntityInterface {


    protected static final Logger LOGGER = Logger.getLogger(AbstractPostalServiceJob.class);
}
