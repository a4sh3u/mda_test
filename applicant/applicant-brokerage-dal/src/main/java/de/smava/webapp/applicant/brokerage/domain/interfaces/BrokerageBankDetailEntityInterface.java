package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageBankDetail;


/**
 * The domain object that represents 'BrokerageBankDetails'.
 *
 * @author generator
 */
public interface BrokerageBankDetailEntityInterface {

    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName getName();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBankDetail asBrokerageBankDetail();
}
