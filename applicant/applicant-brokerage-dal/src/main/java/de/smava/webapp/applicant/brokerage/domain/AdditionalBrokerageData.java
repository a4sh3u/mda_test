package de.smava.webapp.applicant.brokerage.domain;

import java.util.HashMap;

public interface AdditionalBrokerageData {

    String determineFilterName();

    HashMap<String, Object> readAdditionalData();

    HashMap<String, Object> readAdditionalErrorData();
}
