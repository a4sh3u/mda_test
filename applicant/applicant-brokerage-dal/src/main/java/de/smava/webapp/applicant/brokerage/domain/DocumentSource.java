package de.smava.webapp.applicant.brokerage.domain;

/**
 * Created by asaadat on 22.03.16.
 */
public enum DocumentSource {

    CUSTOMER("Kunde"),
    ADVISOR("Berater");

    private final String label;

    DocumentSource(String label) {
        this.label = label;
    }

    public String label() {
        return label;
    }

    public static DocumentSource convert(String label) {
        for (DocumentSource source : DocumentSource.values()) {
            if (source.label().equalsIgnoreCase(label)) {
                return source;
            }
        }

        throw new IllegalArgumentException("cannot match given label to any DocumentSource, label : " + label);
    }
}
