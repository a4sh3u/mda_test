//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage mapping)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageMapping;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageMappingEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMappings'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageMapping
    extends BrokerageEntity    implements BrokerageMappingEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageMapping.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageMapping)) {
            return;
        }
        
        this.setConverter(((BrokerageMapping) oldEntity).getConverter());    
        this.setSmavaValue(((BrokerageMapping) oldEntity).getSmavaValue());    
        this.setBankValue(((BrokerageMapping) oldEntity).getBankValue());    
        this.setDirectionType(((BrokerageMapping) oldEntity).getDirectionType());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageMapping)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getConverter(), ((BrokerageMapping) otherEntity).getConverter());    
        equals = equals && valuesAreEqual(this.getSmavaValue(), ((BrokerageMapping) otherEntity).getSmavaValue());    
        equals = equals && valuesAreEqual(this.getBankValue(), ((BrokerageMapping) otherEntity).getBankValue());    
        equals = equals && valuesAreEqual(this.getDirectionType(), ((BrokerageMapping) otherEntity).getDirectionType());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage mapping)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

