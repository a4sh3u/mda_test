//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(save to mail)}

import de.smava.webapp.applicant.brokerage.domain.SaveToMail;
import de.smava.webapp.applicant.brokerage.domain.interfaces.SaveToMailEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SaveToMails'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSaveToMail
    extends BrokerageEntity    implements SaveToMailEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSaveToMail.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SaveToMail)) {
            return;
        }
        
        this.setRegistrationRoute(((SaveToMail) oldEntity).getRegistrationRoute());    
        this.setCurrentPage(((SaveToMail) oldEntity).getCurrentPage());    
        this.setLoanApplication(((SaveToMail) oldEntity).getLoanApplication());    
        this.setMailTriggered(((SaveToMail) oldEntity).getMailTriggered());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SaveToMail)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getRegistrationRoute(), ((SaveToMail) otherEntity).getRegistrationRoute());    
        equals = equals && valuesAreEqual(this.getCurrentPage(), ((SaveToMail) otherEntity).getCurrentPage());    
        equals = equals && valuesAreEqual(this.getLoanApplication(), ((SaveToMail) otherEntity).getLoanApplication());    
        equals = equals && valuesAreEqual(this.getMailTriggered(), ((SaveToMail) otherEntity).getMailTriggered());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(save to mail)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

