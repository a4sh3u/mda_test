//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(loan application)}

import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.brokerage.domain.interfaces.LoanApplicationEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

                                                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LoanApplications'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractLoanApplication
    extends BrokerageEntity    implements LoanApplicationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractLoanApplication.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof LoanApplication)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setState(((LoanApplication) oldEntity).getState());    
        this.setAccount(((LoanApplication) oldEntity).getAccount());    
        this.setApplicantRelationship(((LoanApplication) oldEntity).getApplicantRelationship());    
        this.setRequestedRdiType(((LoanApplication) oldEntity).getRequestedRdiType());    
        this.setRequestedAmount(((LoanApplication) oldEntity).getRequestedAmount());    
        this.setRequestedDuration(((LoanApplication) oldEntity).getRequestedDuration());    
        this.setSharedLoan(((LoanApplication) oldEntity).getSharedLoan());    
        this.setBrokerageApplications(((LoanApplication) oldEntity).getBrokerageApplications());    
        this.setCategory(((LoanApplication) oldEntity).getCategory());    
        this.setVehicleInformation(((LoanApplication) oldEntity).getVehicleInformation());    
        this.setInitiatorType(((LoanApplication) oldEntity).getInitiatorType());    
        this.setInitiatorTool(((LoanApplication) oldEntity).getInitiatorTool());    
        this.setReachedEmailTimeout(((LoanApplication) oldEntity).getReachedEmailTimeout());    
        this.setVisible(((LoanApplication) oldEntity).getVisible());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof LoanApplication)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getState(), ((LoanApplication) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getAccount(), ((LoanApplication) otherEntity).getAccount());    
        equals = equals && valuesAreEqual(this.getApplicantRelationship(), ((LoanApplication) otherEntity).getApplicantRelationship());    
        equals = equals && valuesAreEqual(this.getRequestedRdiType(), ((LoanApplication) otherEntity).getRequestedRdiType());    
        equals = equals && valuesAreEqual(this.getRequestedAmount(), ((LoanApplication) otherEntity).getRequestedAmount());    
        equals = equals && valuesAreEqual(this.getRequestedDuration(), ((LoanApplication) otherEntity).getRequestedDuration());    
        equals = equals && valuesAreEqual(this.getSharedLoan(), ((LoanApplication) otherEntity).getSharedLoan());    
        equals = equals && valuesAreEqual(this.getBrokerageApplications(), ((LoanApplication) otherEntity).getBrokerageApplications());    
        equals = equals && valuesAreEqual(this.getCategory(), ((LoanApplication) otherEntity).getCategory());    
        equals = equals && valuesAreEqual(this.getVehicleInformation(), ((LoanApplication) otherEntity).getVehicleInformation());    
        equals = equals && valuesAreEqual(this.getInitiatorType(), ((LoanApplication) otherEntity).getInitiatorType());    
        equals = equals && valuesAreEqual(this.getInitiatorTool(), ((LoanApplication) otherEntity).getInitiatorTool());    
        equals = equals && valuesAreEqual(this.getReachedEmailTimeout(), ((LoanApplication) otherEntity).getReachedEmailTimeout());    
        equals = equals && valuesAreEqual(this.getVisible(), ((LoanApplication) otherEntity).getVisible());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(loan application)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

