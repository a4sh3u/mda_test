//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage household calculation)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageHouseholdCalculationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageHouseholdCalculations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * Brokerage specific household calculation
 *
 * @author generator
 */
public abstract class AbstractBrokerageHouseholdCalculation
    extends BrokerageEntity    implements BrokerageHouseholdCalculationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageHouseholdCalculation.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageHouseholdCalculation)) {
            return;
        }
        
        this.setBrokerageApplication(((BrokerageHouseholdCalculation) oldEntity).getBrokerageApplication());    
        this.setIncome(((BrokerageHouseholdCalculation) oldEntity).getIncome());    
        this.setIncomeSecondApplicant(((BrokerageHouseholdCalculation) oldEntity).getIncomeSecondApplicant());    
        this.setExpenses(((BrokerageHouseholdCalculation) oldEntity).getExpenses());    
        this.setExpensesSecondApplicant(((BrokerageHouseholdCalculation) oldEntity).getExpensesSecondApplicant());    
        this.setDisposableIncome(((BrokerageHouseholdCalculation) oldEntity).getDisposableIncome());    
        this.setDisposableIncomeSecondApplicant(((BrokerageHouseholdCalculation) oldEntity).getDisposableIncomeSecondApplicant());    
        this.setIndebtedness(((BrokerageHouseholdCalculation) oldEntity).getIndebtedness());    
        this.setIndebtednessSecondApplicant(((BrokerageHouseholdCalculation) oldEntity).getIndebtednessSecondApplicant());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setDetails(((BrokerageHouseholdCalculation) oldEntity).getDetails());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageHouseholdCalculation)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((BrokerageHouseholdCalculation) otherEntity).getBrokerageApplication());    
        equals = equals && valuesAreEqual(this.getIncome(), ((BrokerageHouseholdCalculation) otherEntity).getIncome());    
        equals = equals && valuesAreEqual(this.getIncomeSecondApplicant(), ((BrokerageHouseholdCalculation) otherEntity).getIncomeSecondApplicant());    
        equals = equals && valuesAreEqual(this.getExpenses(), ((BrokerageHouseholdCalculation) otherEntity).getExpenses());    
        equals = equals && valuesAreEqual(this.getExpensesSecondApplicant(), ((BrokerageHouseholdCalculation) otherEntity).getExpensesSecondApplicant());    
        equals = equals && valuesAreEqual(this.getDisposableIncome(), ((BrokerageHouseholdCalculation) otherEntity).getDisposableIncome());    
        equals = equals && valuesAreEqual(this.getDisposableIncomeSecondApplicant(), ((BrokerageHouseholdCalculation) otherEntity).getDisposableIncomeSecondApplicant());    
        equals = equals && valuesAreEqual(this.getIndebtedness(), ((BrokerageHouseholdCalculation) otherEntity).getIndebtedness());    
        equals = equals && valuesAreEqual(this.getIndebtednessSecondApplicant(), ((BrokerageHouseholdCalculation) otherEntity).getIndebtednessSecondApplicant());    
            
        equals = equals && valuesAreEqual(this.getDetails(), ((BrokerageHouseholdCalculation) otherEntity).getDetails());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage household calculation)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

