//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(checkout flow)}
import de.smava.webapp.applicant.brokerage.domain.history.CheckoutFlowHistory;

import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CheckoutFlows'.
 *
 * 
 *
 * @author generator
 */
public class CheckoutFlow extends CheckoutFlowHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(checkout flow)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected String _type;
        protected Long _firstPageId;
        protected de.smava.webapp.applicant.brokerage.domain.CheckoutBlock _checkoutBlock;
        protected Set<CheckoutFlowPage> _checkoutFlowPages;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'first page id'.
     *
     * 
     *
     */
    public void setFirstPageId(Long firstPageId) {
        _firstPageId = firstPageId;
    }
            
    /**
     * Returns the property 'first page id'.
     *
     * 
     *
     */
    public Long getFirstPageId() {
        return _firstPageId;
    }
                                            
    /**
     * Setter for the property 'checkout block'.
     *
     * 
     *
     */
    public void setCheckoutBlock(de.smava.webapp.applicant.brokerage.domain.CheckoutBlock checkoutBlock) {
        _checkoutBlock = checkoutBlock;
    }
            
    /**
     * Returns the property 'checkout block'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.CheckoutBlock getCheckoutBlock() {
        return _checkoutBlock;
    }
                                            
    /**
     * Setter for the property 'checkout flow pages'.
     *
     * 
     *
     */
    public void setCheckoutFlowPages(Set<CheckoutFlowPage> checkoutFlowPages) {
        _checkoutFlowPages = checkoutFlowPages;
    }
            
    /**
     * Returns the property 'checkout flow pages'.
     *
     * 
     *
     */
    public Set<CheckoutFlowPage> getCheckoutFlowPages() {
        return _checkoutFlowPages;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_checkoutBlock instanceof de.smava.webapp.commons.domain.Entity && !_checkoutBlock.getChangeSet().isEmpty()) {
             for (String element : _checkoutBlock.getChangeSet()) {
                 result.add("checkout block : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CheckoutFlow.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(CheckoutFlow.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CheckoutFlow asCheckoutFlow() {
        return this;
    }
}
