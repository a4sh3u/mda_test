//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement bank filter)}

import de.smava.webapp.applicant.brokerage.domain.MarketingPlacementBankFilter;
import de.smava.webapp.applicant.brokerage.domain.interfaces.MarketingPlacementBankFilterEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'MarketingPlacementBankFilters'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractMarketingPlacementBankFilter
    extends BrokerageEntity    implements MarketingPlacementBankFilterEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractMarketingPlacementBankFilter.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof MarketingPlacementBankFilter)) {
            return;
        }
        
        this.setExternalAffiliateId(((MarketingPlacementBankFilter) oldEntity).getExternalAffiliateId());    
        this.setBrokerageBank(((MarketingPlacementBankFilter) oldEntity).getBrokerageBank());    
        this.setFilterType(((MarketingPlacementBankFilter) oldEntity).getFilterType());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof MarketingPlacementBankFilter)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getExternalAffiliateId(), ((MarketingPlacementBankFilter) otherEntity).getExternalAffiliateId());    
        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((MarketingPlacementBankFilter) otherEntity).getBrokerageBank());    
        equals = equals && valuesAreEqual(this.getFilterType(), ((MarketingPlacementBankFilter) otherEntity).getFilterType());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(marketing placement bank filter)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

