package de.smava.webapp.applicant.brokerage.type;

/**
 * Created by aherr on 17.12.14.
 */
public enum FilterType {
    INCLUDE, EXCLUDE;
}
