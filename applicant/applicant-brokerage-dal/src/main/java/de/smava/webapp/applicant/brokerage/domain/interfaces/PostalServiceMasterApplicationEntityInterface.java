package de.smava.webapp.applicant.brokerage.domain.interfaces;

import java.util.Date;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public interface PostalServiceMasterApplicationEntityInterface {

    void setDate(Date date);

    Date getDate();

    void setPostalServiceJob(de.smava.webapp.applicant.brokerage.domain.PostalServiceJob postalServiceJob);

    de.smava.webapp.applicant.brokerage.domain.PostalServiceJob getPostalServiceJob();

    void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication);

    de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication();

    void setBrokerageApplications(String brokerageApplications);

    String getBrokerageApplications();
}
