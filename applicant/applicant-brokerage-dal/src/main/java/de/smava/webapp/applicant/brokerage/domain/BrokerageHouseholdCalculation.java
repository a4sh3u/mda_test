//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage household calculation)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageHouseholdCalculationHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageHouseholdCalculations'.
 *
 * Brokerage specific household calculation
 *
 * @author generator
 */
public class BrokerageHouseholdCalculation extends BrokerageHouseholdCalculationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage household calculation)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _brokerageApplication;
        protected Double _income;
        protected Double _incomeSecondApplicant;
        protected Double _expenses;
        protected Double _expensesSecondApplicant;
        protected Double _disposableIncome;
        protected Double _disposableIncomeSecondApplicant;
        protected Double _indebtedness;
        protected Double _indebtednessSecondApplicant;
        protected Date _creationDate;
        protected Set<BrokerageHouseholdCalculationDetails> _details;
        
                                    
    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    public void setBrokerageApplication(Long brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    public Long getBrokerageApplication() {
        return _brokerageApplication;
    }
                                    /**
     * Setter for the property 'income'.
     *
     * 
     *
     */
    public void setIncome(Double income) {
        if (!_incomeIsSet) {
            _incomeIsSet = true;
            _incomeInitVal = getIncome();
        }
        registerChange("income", _incomeInitVal, income);
        _income = income;
    }
                        
    /**
     * Returns the property 'income'.
     *
     * 
     *
     */
    public Double getIncome() {
        return _income;
    }
                                    /**
     * Setter for the property 'income second applicant'.
     *
     * 
     *
     */
    public void setIncomeSecondApplicant(Double incomeSecondApplicant) {
        if (!_incomeSecondApplicantIsSet) {
            _incomeSecondApplicantIsSet = true;
            _incomeSecondApplicantInitVal = getIncomeSecondApplicant();
        }
        registerChange("income second applicant", _incomeSecondApplicantInitVal, incomeSecondApplicant);
        _incomeSecondApplicant = incomeSecondApplicant;
    }
                        
    /**
     * Returns the property 'income second applicant'.
     *
     * 
     *
     */
    public Double getIncomeSecondApplicant() {
        return _incomeSecondApplicant;
    }
                                    /**
     * Setter for the property 'expenses'.
     *
     * 
     *
     */
    public void setExpenses(Double expenses) {
        if (!_expensesIsSet) {
            _expensesIsSet = true;
            _expensesInitVal = getExpenses();
        }
        registerChange("expenses", _expensesInitVal, expenses);
        _expenses = expenses;
    }
                        
    /**
     * Returns the property 'expenses'.
     *
     * 
     *
     */
    public Double getExpenses() {
        return _expenses;
    }
                                    /**
     * Setter for the property 'expenses second applicant'.
     *
     * 
     *
     */
    public void setExpensesSecondApplicant(Double expensesSecondApplicant) {
        if (!_expensesSecondApplicantIsSet) {
            _expensesSecondApplicantIsSet = true;
            _expensesSecondApplicantInitVal = getExpensesSecondApplicant();
        }
        registerChange("expenses second applicant", _expensesSecondApplicantInitVal, expensesSecondApplicant);
        _expensesSecondApplicant = expensesSecondApplicant;
    }
                        
    /**
     * Returns the property 'expenses second applicant'.
     *
     * 
     *
     */
    public Double getExpensesSecondApplicant() {
        return _expensesSecondApplicant;
    }
                                    /**
     * Setter for the property 'disposable income'.
     *
     * 
     *
     */
    public void setDisposableIncome(Double disposableIncome) {
        if (!_disposableIncomeIsSet) {
            _disposableIncomeIsSet = true;
            _disposableIncomeInitVal = getDisposableIncome();
        }
        registerChange("disposable income", _disposableIncomeInitVal, disposableIncome);
        _disposableIncome = disposableIncome;
    }
                        
    /**
     * Returns the property 'disposable income'.
     *
     * 
     *
     */
    public Double getDisposableIncome() {
        return _disposableIncome;
    }
                                    /**
     * Setter for the property 'disposable income second applicant'.
     *
     * 
     *
     */
    public void setDisposableIncomeSecondApplicant(Double disposableIncomeSecondApplicant) {
        if (!_disposableIncomeSecondApplicantIsSet) {
            _disposableIncomeSecondApplicantIsSet = true;
            _disposableIncomeSecondApplicantInitVal = getDisposableIncomeSecondApplicant();
        }
        registerChange("disposable income second applicant", _disposableIncomeSecondApplicantInitVal, disposableIncomeSecondApplicant);
        _disposableIncomeSecondApplicant = disposableIncomeSecondApplicant;
    }
                        
    /**
     * Returns the property 'disposable income second applicant'.
     *
     * 
     *
     */
    public Double getDisposableIncomeSecondApplicant() {
        return _disposableIncomeSecondApplicant;
    }
                                    /**
     * Setter for the property 'indebtedness'.
     *
     * 
     *
     */
    public void setIndebtedness(Double indebtedness) {
        if (!_indebtednessIsSet) {
            _indebtednessIsSet = true;
            _indebtednessInitVal = getIndebtedness();
        }
        registerChange("indebtedness", _indebtednessInitVal, indebtedness);
        _indebtedness = indebtedness;
    }
                        
    /**
     * Returns the property 'indebtedness'.
     *
     * 
     *
     */
    public Double getIndebtedness() {
        return _indebtedness;
    }
                                    /**
     * Setter for the property 'indebtedness second applicant'.
     *
     * 
     *
     */
    public void setIndebtednessSecondApplicant(Double indebtednessSecondApplicant) {
        if (!_indebtednessSecondApplicantIsSet) {
            _indebtednessSecondApplicantIsSet = true;
            _indebtednessSecondApplicantInitVal = getIndebtednessSecondApplicant();
        }
        registerChange("indebtedness second applicant", _indebtednessSecondApplicantInitVal, indebtednessSecondApplicant);
        _indebtednessSecondApplicant = indebtednessSecondApplicant;
    }
                        
    /**
     * Returns the property 'indebtedness second applicant'.
     *
     * 
     *
     */
    public Double getIndebtednessSecondApplicant() {
        return _indebtednessSecondApplicant;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'details'.
     *
     * 
     *
     */
    public void setDetails(Set<BrokerageHouseholdCalculationDetails> details) {
        _details = details;
    }
            
    /**
     * Returns the property 'details'.
     *
     * 
     *
     */
    public Set<BrokerageHouseholdCalculationDetails> getDetails() {
        return _details;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageHouseholdCalculation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageHouseholdCalculation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageHouseholdCalculation asBrokerageHouseholdCalculation() {
        return this;
    }
}
