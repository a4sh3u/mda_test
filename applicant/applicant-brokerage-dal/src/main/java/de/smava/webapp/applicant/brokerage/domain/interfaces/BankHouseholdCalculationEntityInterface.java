package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BankHouseholdCalculation;


/**
 * The domain object that represents 'BankHouseholdCalculations'.
 *
 * @author generator
 */
public interface BankHouseholdCalculationEntityInterface {

    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'household calculation'.
     *
     * 
     *
     */
    void setHouseholdCalculation(de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation householdCalculation);

    /**
     * Returns the property 'household calculation'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation getHouseholdCalculation();
    /**
     * Setter for the property 'bank expression'.
     *
     * Bank specific calculation of household data in Spring expression language
     *
     */
    void setBankExpression(String bankExpression);

    /**
     * Returns the property 'bank expression'.
     *
     * Bank specific calculation of household data in Spring expression language
     *
     */
    String getBankExpression();
    /**
     * Helper method to get reference of this object as model type.
     */
    BankHouseholdCalculation asBankHouseholdCalculation();
}
