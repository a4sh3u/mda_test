package de.smava.webapp.applicant.brokerage.domain.abstracts;

import de.smava.webapp.applicant.brokerage.domain.BrokerageBankMaintenance;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageBankMaintenanceEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;

/**
 * @author Jakub Janus
 * @since 02.08.2017
 */
public abstract class AbstractBrokerageBankMaintenance extends BrokerageEntity implements BrokerageBankMaintenanceEntityInterface {

    @Override
    public void copyFromOldEntity(Entity oldEntity) {
        if (!(oldEntity instanceof BrokerageBankMaintenance)) {
            return;
        }

        BrokerageBankMaintenance oldBrokerageBankMaintenanceEntity = (BrokerageBankMaintenance) oldEntity;

        setBrokerageBank(oldBrokerageBankMaintenanceEntity.getBrokerageBank());
        setExpressionFrom(oldBrokerageBankMaintenanceEntity.getExpressionFrom());
        setExpressionTo(oldBrokerageBankMaintenanceEntity.getExpressionTo());
        setBankState(oldBrokerageBankMaintenanceEntity.getBankState());
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageBankMaintenance)) {
            equals = false;
        }

        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((BrokerageBankMaintenance) otherEntity).getBrokerageBank());
        equals = equals && valuesAreEqual(this.getExpressionFrom(), ((BrokerageBankMaintenance) otherEntity).getExpressionFrom());
        equals = equals && valuesAreEqual(this.getExpressionTo(), ((BrokerageBankMaintenance) otherEntity).getExpressionTo());

        return equals;
    }
}
