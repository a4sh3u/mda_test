package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.CheckoutBlock;
import de.smava.webapp.applicant.brokerage.domain.CheckoutFlow;

import java.util.Set;


/**
 * The domain object that represents 'CheckoutBlocks'.
 *
 * @author generator
 */
public interface CheckoutBlockEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'sort order'.
     *
     * 
     *
     */
    void setSortOrder(Integer sortOrder);

    /**
     * Returns the property 'sort order'.
     *
     * 
     *
     */
    Integer getSortOrder();
    /**
     * Setter for the property 'checkout flows'.
     *
     * 
     *
     */
    void setCheckoutFlows(Set<CheckoutFlow> checkoutFlows);

    /**
     * Returns the property 'checkout flows'.
     *
     * 
     *
     */
    Set<CheckoutFlow> getCheckoutFlows();
    /**
     * Helper method to get reference of this object as model type.
     */
    CheckoutBlock asCheckoutBlock();
}
