//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application checkout status)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageApplicationCheckoutStatusHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationCheckoutStatuss'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageApplicationCheckoutStatus extends BrokerageApplicationCheckoutStatusHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application checkout status)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Long _brokerageApplicationId;
        protected de.smava.webapp.applicant.brokerage.domain.CheckoutFlow _currentCheckoutFlow;
        protected de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage _currentCheckoutFlowPage;
        protected Date _updateTimestamp;
        
                                    
    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    public void setBrokerageApplicationId(Long brokerageApplicationId) {
        _brokerageApplicationId = brokerageApplicationId;
    }
            
    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    public Long getBrokerageApplicationId() {
        return _brokerageApplicationId;
    }
                                            
    /**
     * Setter for the property 'current checkout flow'.
     *
     * 
     *
     */
    public void setCurrentCheckoutFlow(de.smava.webapp.applicant.brokerage.domain.CheckoutFlow currentCheckoutFlow) {
        _currentCheckoutFlow = currentCheckoutFlow;
    }
            
    /**
     * Returns the property 'current checkout flow'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.CheckoutFlow getCurrentCheckoutFlow() {
        return _currentCheckoutFlow;
    }
                                            
    /**
     * Setter for the property 'current checkout flow page'.
     *
     * 
     *
     */
    public void setCurrentCheckoutFlowPage(de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage currentCheckoutFlowPage) {
        _currentCheckoutFlowPage = currentCheckoutFlowPage;
    }
            
    /**
     * Returns the property 'current checkout flow page'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage getCurrentCheckoutFlowPage() {
        return _currentCheckoutFlowPage;
    }
                                    /**
     * Setter for the property 'update timestamp'.
     *
     * 
     *
     */
    public void setUpdateTimestamp(Date updateTimestamp) {
        if (!_updateTimestampIsSet) {
            _updateTimestampIsSet = true;
            _updateTimestampInitVal = getUpdateTimestamp();
        }
        registerChange("update timestamp", _updateTimestampInitVal, updateTimestamp);
        _updateTimestamp = updateTimestamp;
    }
                        
    /**
     * Returns the property 'update timestamp'.
     *
     * 
     *
     */
    public Date getUpdateTimestamp() {
        return _updateTimestamp;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_currentCheckoutFlow instanceof de.smava.webapp.commons.domain.Entity && !_currentCheckoutFlow.getChangeSet().isEmpty()) {
             for (String element : _currentCheckoutFlow.getChangeSet()) {
                 result.add("current checkout flow : " + element);
             }
         }

         if (_currentCheckoutFlowPage instanceof de.smava.webapp.commons.domain.Entity && !_currentCheckoutFlowPage.getChangeSet().isEmpty()) {
             for (String element : _currentCheckoutFlowPage.getChangeSet()) {
                 result.add("current checkout flow page : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplicationCheckoutStatus.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplicationCheckoutStatus.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplicationCheckoutStatus asBrokerageApplicationCheckoutStatus() {
        return this;
    }
}
