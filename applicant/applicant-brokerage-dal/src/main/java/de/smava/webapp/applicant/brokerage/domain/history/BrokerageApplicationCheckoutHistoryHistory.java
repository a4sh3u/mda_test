package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageApplicationCheckoutHistory;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationCheckoutHistorys'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationCheckoutHistoryHistory extends AbstractBrokerageApplicationCheckoutHistory {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _initiatorInitVal;
    protected transient boolean _initiatorIsSet;
    protected transient String _additionalDataInitVal;
    protected transient boolean _additionalDataIsSet;



    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'initiator'.
     */
    public String initiatorInitVal() {
        String result;
        if (_initiatorIsSet) {
            result = _initiatorInitVal;
        } else {
            result = getInitiator();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'initiator'.
     */
    public boolean initiatorIsDirty() {
        return !valuesAreEqual(initiatorInitVal(), getInitiator());
    }

    /**
     * Returns true if the setter method was called for the property 'initiator'.
     */
    public boolean initiatorIsSet() {
        return _initiatorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'additional data'.
     */
    public String additionalDataInitVal() {
        String result;
        if (_additionalDataIsSet) {
            result = _additionalDataInitVal;
        } else {
            result = getAdditionalData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'additional data'.
     */
    public boolean additionalDataIsDirty() {
        return !valuesAreEqual(additionalDataInitVal(), getAdditionalData());
    }

    /**
     * Returns true if the setter method was called for the property 'additional data'.
     */
    public boolean additionalDataIsSet() {
        return _additionalDataIsSet;
    }

}
