package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author KF
 * @since 12.07.2017
 */
public class BankOfScotlandAdditionalData extends AbstractAdditionalData {
    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_BANKOFSCOTLAND;
    }
}
