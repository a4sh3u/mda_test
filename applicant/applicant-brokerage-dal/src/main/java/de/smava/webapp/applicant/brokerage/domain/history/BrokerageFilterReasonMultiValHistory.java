package de.smava.webapp.applicant.brokerage.domain.history;

import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageFilterReasonMultiVal;
import de.smava.webapp.applicant.brokerage.type.SmarketFilterReasonLevel;

/**
 * @author Adam Budzinski
 * @since 04.09.2017
 */
public abstract class BrokerageFilterReasonMultiValHistory extends AbstractBrokerageFilterReasonMultiVal {

    protected transient Long _applicantIdInitVal;
    protected transient boolean _applicantIdIsSet;
    protected transient SmarketFilterReasonLevel _filterLevelInitVal;
    protected transient boolean _filterLevelIsSet;

    public SmarketFilterReasonLevel filterLevelInitVal() {
        SmarketFilterReasonLevel result;

        if (_filterLevelIsSet) {
            result = _filterLevelInitVal;
        } else {
            result = getFilterLevel();
        }
        return result;
    }

    public boolean filterLevelIsSet() {
        return _filterLevelIsSet;
    }

    public boolean filterLevelIsDirty() {
        return !valuesAreEqual(filterLevelInitVal(), getFilterLevel());
    }

    public boolean applicantIdIsSet() {
        return _applicantIdIsSet;
    }

    public Long applicantIdInitVal() {
        Long result;

        if (_applicantIdIsSet) {
            result = _applicantIdInitVal;
        } else {
            result = getApplicantId();
        }
        return result;
    }

    public boolean applicantIdIsDirty() {
        return !valuesAreEqual(applicantIdInitVal(), getApplicantId());
    }
}