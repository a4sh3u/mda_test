package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.domain.MarketingPlacementBankFilter;


/**
 * The domain object that represents 'MarketingPlacementBankFilters'.
 *
 * @author generator
 */
public interface MarketingPlacementBankFilterEntityInterface {

    /**
     * Setter for the property 'external affiliate id'.
     *
     * 
     *
     */
    void setExternalAffiliateId(String externalAffiliateId);

    /**
     * Returns the property 'external affiliate id'.
     *
     * 
     *
     */
    String getExternalAffiliateId();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'filter type'.
     *
     * 
     *
     */
    void setFilterType(de.smava.webapp.applicant.brokerage.type.FilterType filterType);

    /**
     * Returns the property 'filter type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.FilterType getFilterType();
    /**
     * Helper method to get reference of this object as model type.
     */
    MarketingPlacementBankFilter asMarketingPlacementBankFilter();
}
