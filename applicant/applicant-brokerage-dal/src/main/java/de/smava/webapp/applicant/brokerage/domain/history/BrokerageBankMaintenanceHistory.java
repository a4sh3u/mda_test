package de.smava.webapp.applicant.brokerage.domain.history;

import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageBankMaintenance;

/**
 * @author Jakub Janus
 * @since 02.08.2017
 */
public abstract class BrokerageBankMaintenanceHistory extends AbstractBrokerageBankMaintenance {

    protected transient String  _expressionFromInitVal;
    protected transient boolean _expressionFromIsSet;
    protected transient String  _expressionToInitVal;
    protected transient boolean _expressionToIsSet;
    protected transient Boolean _bankStateInitVal;
    protected transient boolean _bankStateIsSet;

    public String expressionFromInitVal() {
        String result;
        if (_expressionFromIsSet) {
            result = _expressionFromInitVal;
        } else {
            result = getExpressionFrom();
        }
        return result;
    }

    public boolean expressionFromIsDirty() {
        return !valuesAreEqual(expressionFromInitVal(), getExpressionFrom());
    }

    public boolean expressionFromIsSet() {
        return _expressionFromIsSet;
    }

    public String expressionToInitVal() {
        String result;
        if (_expressionToIsSet) {
            result = _expressionToInitVal;
        } else {
            result = getExpressionTo();
        }
        return result;
    }

    public boolean expressionToIsDirty() {
        return !valuesAreEqual(expressionToInitVal(), getExpressionTo());
    }

    public boolean expressionToIsSet() {
        return _expressionToIsSet;
    }

    public Boolean bankStateInitVal() {
        Boolean result;
        if (_bankStateIsSet) {
            result = _bankStateInitVal;
        } else {
            result = getBankState();
        }
        return result;
    }

    public boolean bankStateIsDirty() {
        return !valuesAreEqual(bankStateInitVal(), getBankState());
    }

    public boolean bankStateIsSet() {
        return _bankStateIsSet;
    }
}