//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank document view)}

import de.smava.webapp.applicant.brokerage.domain.BankDocumentView;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BankDocumentViewEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankDocumentViews'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBankDocumentView
    extends BrokerageEntity    implements BankDocumentViewEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBankDocumentView.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BankDocumentView)) {
            return;
        }
        
        this.setExtRefNumber(((BankDocumentView) oldEntity).getExtRefNumber());    
        this.setCustomerNumber(((BankDocumentView) oldEntity).getCustomerNumber());    
        this.setAccountId(((BankDocumentView) oldEntity).getAccountId());    
        this.setBrokerageBankId(((BankDocumentView) oldEntity).getBrokerageBankId());    
        this.setBankName(((BankDocumentView) oldEntity).getBankName());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setSendDate(((BankDocumentView) oldEntity).getSendDate());    
        this.setReceiveDate(((BankDocumentView) oldEntity).getReceiveDate());    
        this.setFirstName(((BankDocumentView) oldEntity).getFirstName());    
        this.setLastName(((BankDocumentView) oldEntity).getLastName());    
        this.setBrokerageAccountId(((BankDocumentView) oldEntity).getBrokerageAccountId());    
        this.setAdditionalData(((BankDocumentView) oldEntity).getAdditionalData());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BankDocumentView)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getExtRefNumber(), ((BankDocumentView) otherEntity).getExtRefNumber());    
        equals = equals && valuesAreEqual(this.getCustomerNumber(), ((BankDocumentView) otherEntity).getCustomerNumber());    
        equals = equals && valuesAreEqual(this.getAccountId(), ((BankDocumentView) otherEntity).getAccountId());    
        equals = equals && valuesAreEqual(this.getBrokerageBankId(), ((BankDocumentView) otherEntity).getBrokerageBankId());    
        equals = equals && valuesAreEqual(this.getBankName(), ((BankDocumentView) otherEntity).getBankName());    
            
        equals = equals && valuesAreEqual(this.getSendDate(), ((BankDocumentView) otherEntity).getSendDate());    
        equals = equals && valuesAreEqual(this.getReceiveDate(), ((BankDocumentView) otherEntity).getReceiveDate());    
        equals = equals && valuesAreEqual(this.getFirstName(), ((BankDocumentView) otherEntity).getFirstName());    
        equals = equals && valuesAreEqual(this.getLastName(), ((BankDocumentView) otherEntity).getLastName());    
        equals = equals && valuesAreEqual(this.getBrokerageAccountId(), ((BankDocumentView) otherEntity).getBrokerageAccountId());    
        equals = equals && valuesAreEqual(this.getAdditionalData(), ((BankDocumentView) otherEntity).getAdditionalData());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(bank document view)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

