package de.smava.webapp.applicant.brokerage.type;

/**
 * @author Adam
 * @since 29.09.2016.
 */
public enum MultiStatusRequestState {
    SENDING,
    PENDING,
    FAILED,
    SUCCESS
}
