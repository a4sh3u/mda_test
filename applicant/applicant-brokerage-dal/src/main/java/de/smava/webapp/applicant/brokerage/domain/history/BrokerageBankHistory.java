package de.smava.webapp.applicant.brokerage.domain.history;


import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageBank;


/**
 * The domain object that has all history aggregation related fields for 'BrokerageBanks'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankHistory extends AbstractBrokerageBank {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient boolean _validInitVal;
    protected transient boolean _validIsSet;
    protected transient boolean _comparisonOnlyInitVal;
    protected transient boolean _comparisonOnlyIsSet;
    protected transient Integer _sendInOffsetInitVal;
    protected transient boolean _sendInOffsetIsSet;
    protected transient String _statusUpdateInitVal;
    protected transient boolean _statusUpdateIsSet;
    protected transient de.smava.webapp.brokerage.domain.BucketType _bucketTypeInitVal;
    protected transient boolean _bucketTypeIsSet;

    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'valid'.
     */
    public boolean validInitVal() {
        boolean result;
        if (_validIsSet) {
            result = _validInitVal;
        } else {
            result = getValid();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'valid'.
     */
    public boolean validIsDirty() {
        return !valuesAreEqual(validInitVal(), getValid());
    }

    /**
     * Returns true if the setter method was called for the property 'valid'.
     */
    public boolean validIsSet() {
        return _validIsSet;
    }
	
    /**
     * Returns the initial value of the property 'comparison only'.
     */
    public boolean comparisonOnlyInitVal() {
        boolean result;
        if (_comparisonOnlyIsSet) {
            result = _comparisonOnlyInitVal;
        } else {
            result = getComparisonOnly();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comparison only'.
     */
    public boolean comparisonOnlyIsDirty() {
        return !valuesAreEqual(comparisonOnlyInitVal(), getComparisonOnly());
    }

    /**
     * Returns true if the setter method was called for the property 'comparison only'.
     */
    public boolean comparisonOnlyIsSet() {
        return _comparisonOnlyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'send in offset'.
     */
    public Integer sendInOffsetInitVal() {
        Integer result;
        if (_sendInOffsetIsSet) {
            result = _sendInOffsetInitVal;
        } else {
            result = getSendInOffset();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'send in offset'.
     */
    public boolean sendInOffsetIsDirty() {
        return !valuesAreEqual(sendInOffsetInitVal(), getSendInOffset());
    }

    /**
     * Returns true if the setter method was called for the property 'send in offset'.
     */
    public boolean sendInOffsetIsSet() {
        return _sendInOffsetIsSet;
    }
						
    /**
     * Returns the initial value of the property 'bucket type'.
     */
    public de.smava.webapp.brokerage.domain.BucketType bucketTypeInitVal() {
        de.smava.webapp.brokerage.domain.BucketType result;
        if (_bucketTypeIsSet) {
            result = _bucketTypeInitVal;
        } else {
            result = getBucketType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bucket type'.
     */
    public boolean bucketTypeIsDirty() {
        return !valuesAreEqual(bucketTypeInitVal(), getBucketType());
    }

    /**
     * Returns true if the setter method was called for the property 'bucket type'.
     */
    public boolean bucketTypeIsSet() {
        return _bucketTypeIsSet;
    }

    /**
     * Returns the initial value of the property 'status update'.
     */
    public String statusUpdateInitVal() {
        String result;
        if (_statusUpdateIsSet) {
            result = _statusUpdateInitVal;
        } else {
            result = getStatusUpdateRaw();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'status update'.
     */
    public boolean statusUpdateIsDirty() {
        return !valuesAreEqual(statusUpdateInitVal(), getStatusUpdateRaw());
    }

    /**
     * Returns true if the setter method was called for the property 'status update'.
     */
    public boolean statusUpdateIsSet() {
        return _statusUpdateIsSet;
    }
}
