package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.domain.BrokerageBankMaintenance;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;

import javax.transaction.Synchronization;
import java.util.Collection;

/**
 * @author Jakub Janus
 * @since 02.08.2017
 */
public interface BrokerageBankMaintenanceDao extends BrokerageSchemaDao<BrokerageBankMaintenance> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Retrieves all 'BrokerageBankMaintenance' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageBankMaintenance> getBrokerageBankMaintenanceList();
}
