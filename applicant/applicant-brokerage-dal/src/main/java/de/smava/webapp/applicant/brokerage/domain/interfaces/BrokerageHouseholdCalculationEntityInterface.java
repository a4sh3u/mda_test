package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation;
import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculationDetails;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'BrokerageHouseholdCalculations'.
 *
 * @author generator
 */
public interface BrokerageHouseholdCalculationEntityInterface {

    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    void setBrokerageApplication(Long brokerageApplication);

    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    Long getBrokerageApplication();
    /**
     * Setter for the property 'income'.
     *
     * 
     *
     */
    void setIncome(Double income);

    /**
     * Returns the property 'income'.
     *
     * 
     *
     */
    Double getIncome();
    /**
     * Setter for the property 'income second applicant'.
     *
     * 
     *
     */
    void setIncomeSecondApplicant(Double incomeSecondApplicant);

    /**
     * Returns the property 'income second applicant'.
     *
     * 
     *
     */
    Double getIncomeSecondApplicant();
    /**
     * Setter for the property 'expenses'.
     *
     * 
     *
     */
    void setExpenses(Double expenses);

    /**
     * Returns the property 'expenses'.
     *
     * 
     *
     */
    Double getExpenses();
    /**
     * Setter for the property 'expenses second applicant'.
     *
     * 
     *
     */
    void setExpensesSecondApplicant(Double expensesSecondApplicant);

    /**
     * Returns the property 'expenses second applicant'.
     *
     * 
     *
     */
    Double getExpensesSecondApplicant();
    /**
     * Setter for the property 'disposable income'.
     *
     * 
     *
     */
    void setDisposableIncome(Double disposableIncome);

    /**
     * Returns the property 'disposable income'.
     *
     * 
     *
     */
    Double getDisposableIncome();
    /**
     * Setter for the property 'disposable income second applicant'.
     *
     * 
     *
     */
    void setDisposableIncomeSecondApplicant(Double disposableIncomeSecondApplicant);

    /**
     * Returns the property 'disposable income second applicant'.
     *
     * 
     *
     */
    Double getDisposableIncomeSecondApplicant();
    /**
     * Setter for the property 'indebtedness'.
     *
     * 
     *
     */
    void setIndebtedness(Double indebtedness);

    /**
     * Returns the property 'indebtedness'.
     *
     * 
     *
     */
    Double getIndebtedness();
    /**
     * Setter for the property 'indebtedness second applicant'.
     *
     * 
     *
     */
    void setIndebtednessSecondApplicant(Double indebtednessSecondApplicant);

    /**
     * Returns the property 'indebtedness second applicant'.
     *
     * 
     *
     */
    Double getIndebtednessSecondApplicant();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'details'.
     *
     * 
     *
     */
    void setDetails(Set<BrokerageHouseholdCalculationDetails> details);

    /**
     * Returns the property 'details'.
     *
     * 
     *
     */
    Set<BrokerageHouseholdCalculationDetails> getDetails();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageHouseholdCalculation asBrokerageHouseholdCalculation();
}
