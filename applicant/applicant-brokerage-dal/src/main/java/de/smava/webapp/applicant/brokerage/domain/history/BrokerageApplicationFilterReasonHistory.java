package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageApplicationFilterReason;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationFilterReasons'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationFilterReasonHistory extends AbstractBrokerageApplicationFilterReason {

    protected transient de.smava.webapp.applicant.brokerage.type.FilterReasonCode _reasonCodeInitVal;
    protected transient boolean _reasonCodeIsSet;
    protected transient String _reasonDescriptionInitVal;
    protected transient boolean _reasonDescriptionIsSet;


    /**
     * Returns the initial value of the property 'reason code'.
     */
    public de.smava.webapp.applicant.brokerage.type.FilterReasonCode reasonCodeInitVal() {
        de.smava.webapp.applicant.brokerage.type.FilterReasonCode result;
        if (_reasonCodeIsSet) {
            result = _reasonCodeInitVal;
        } else {
            result = getReasonCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'reason code'.
     */
    public boolean reasonCodeIsDirty() {
        return !valuesAreEqual(reasonCodeInitVal(), getReasonCode());
    }

    /**
     * Returns true if the setter method was called for the property 'reason code'.
     */
    public boolean reasonCodeIsSet() {
        return _reasonCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'reason description'.
     */
    public String reasonDescriptionInitVal() {
        String result;
        if (_reasonDescriptionIsSet) {
            result = _reasonDescriptionInitVal;
        } else {
            result = getReasonDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'reason description'.
     */
    public boolean reasonDescriptionIsDirty() {
        return !valuesAreEqual(reasonDescriptionInitVal(), getReasonDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'reason description'.
     */
    public boolean reasonDescriptionIsSet() {
        return _reasonDescriptionIsSet;
    }

}
