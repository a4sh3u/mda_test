//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageBankEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBanks'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageBank
    extends BrokerageEntity    implements BrokerageBankEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageBank.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageBank)) {
            return;
        }
        
        this.setMarketingPartnerId(((BrokerageBank) oldEntity).getMarketingPartnerId());    
        this.setName(((BrokerageBank) oldEntity).getName());    
        this.setValid(((BrokerageBank) oldEntity).getValid());    
        this.setComparisonOnly(((BrokerageBank) oldEntity).getComparisonOnly());    
        this.setSendInOffset(((BrokerageBank) oldEntity).getSendInOffset());    
        this.setFilter(((BrokerageBank) oldEntity).getFilter());    
        this.setConfiguration(((BrokerageBank) oldEntity).getConfiguration());    
        this.setMappings(((BrokerageBank) oldEntity).getMappings());    
        this.setBrokerageUploadedDocs(((BrokerageBank) oldEntity).getBrokerageUploadedDocs());    
        this.setBrokerageBankDetails(((BrokerageBank) oldEntity).getBrokerageBankDetails());    
        this.setBucketType(((BrokerageBank) oldEntity).getBucketType());
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageBank)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getMarketingPartnerId(), ((BrokerageBank) otherEntity).getMarketingPartnerId());    
        equals = equals && valuesAreEqual(this.getName(), ((BrokerageBank) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getValid(), ((BrokerageBank) otherEntity).getValid());    
        equals = equals && valuesAreEqual(this.getComparisonOnly(), ((BrokerageBank) otherEntity).getComparisonOnly());    
        equals = equals && valuesAreEqual(this.getSendInOffset(), ((BrokerageBank) otherEntity).getSendInOffset());    
        equals = equals && valuesAreEqual(this.getFilter(), ((BrokerageBank) otherEntity).getFilter());    
        equals = equals && valuesAreEqual(this.getConfiguration(), ((BrokerageBank) otherEntity).getConfiguration());    
        equals = equals && valuesAreEqual(this.getMappings(), ((BrokerageBank) otherEntity).getMappings());    
        equals = equals && valuesAreEqual(this.getBrokerageUploadedDocs(), ((BrokerageBank) otherEntity).getBrokerageUploadedDocs());    
        equals = equals && valuesAreEqual(this.getBrokerageBankDetails(), ((BrokerageBank) otherEntity).getBrokerageBankDetails());    
        equals = equals && valuesAreEqual(this.getBucketType(), ((BrokerageBank) otherEntity).getBucketType());
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage bank)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

