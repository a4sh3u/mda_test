package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationCheckoutHistory;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageApplicationCheckoutHistorys'.
 *
 * @author generator
 */
public interface BrokerageApplicationCheckoutHistoryEntityInterface {

    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    void setBrokerageApplicationId(Long brokerageApplicationId);

    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    Long getBrokerageApplicationId();
    /**
     * Setter for the property 'checkout flow page'.
     *
     * 
     *
     */
    void setCheckoutFlowPage(de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage checkoutFlowPage);

    /**
     * Returns the property 'checkout flow page'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage getCheckoutFlowPage();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'initiator'.
     *
     * 
     *
     */
    void setInitiator(String initiator);

    /**
     * Returns the property 'initiator'.
     *
     * 
     *
     */
    String getInitiator();
    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    void setAdditionalData(String additionalData);

    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    String getAdditionalData();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationCheckoutHistory asBrokerageApplicationCheckoutHistory();
}
