package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractCustomerUploadedDoc;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'CustomerUploadedDocs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CustomerUploadedDocHistory extends AbstractCustomerUploadedDoc {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _deletionDateInitVal;
    protected transient boolean _deletionDateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.domain.DocumentStatus _statusInitVal;
    protected transient boolean _statusIsSet;
    protected transient String _externalStorageIdInitVal;
    protected transient boolean _externalStorageIdIsSet;
    protected transient de.smava.webapp.applicant.brokerage.domain.DocumentCategory _categoryInitVal;
    protected transient boolean _categoryIsSet;
    protected transient String _fileNameInitVal;
    protected transient boolean _fileNameIsSet;
    protected transient de.smava.webapp.applicant.brokerage.domain.DocumentSource _sourceInitVal;
    protected transient boolean _sourceIsSet;
    protected transient String  _bankDocumentIdInitVal;
    protected transient boolean _bankDocumentIdIsSet;

		
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'deletion date'.
     */
    public Date deletionDateInitVal() {
        Date result;
        if (_deletionDateIsSet) {
            result = _deletionDateInitVal;
        } else {
            result = getDeletionDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'deletion date'.
     */
    public boolean deletionDateIsDirty() {
        return !valuesAreEqual(deletionDateInitVal(), getDeletionDate());
    }

    /**
     * Returns true if the setter method was called for the property 'deletion date'.
     */
    public boolean deletionDateIsSet() {
        return _deletionDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'status'.
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentStatus statusInitVal() {
        de.smava.webapp.applicant.brokerage.domain.DocumentStatus result;
        if (_statusIsSet) {
            result = _statusInitVal;
        } else {
            result = getStatus();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'status'.
     */
    public boolean statusIsDirty() {
        return !valuesAreEqual(statusInitVal(), getStatus());
    }

    /**
     * Returns true if the setter method was called for the property 'status'.
     */
    public boolean statusIsSet() {
        return _statusIsSet;
    }
	
    /**
     * Returns the initial value of the property 'external storage id'.
     */
    public String externalStorageIdInitVal() {
        String result;
        if (_externalStorageIdIsSet) {
            result = _externalStorageIdInitVal;
        } else {
            result = getExternalStorageId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'external storage id'.
     */
    public boolean externalStorageIdIsDirty() {
        return !valuesAreEqual(externalStorageIdInitVal(), getExternalStorageId());
    }

    /**
     * Returns true if the setter method was called for the property 'external storage id'.
     */
    public boolean externalStorageIdIsSet() {
        return _externalStorageIdIsSet;
    }
	
    /**
     * Returns the initial value of the property 'category'.
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentCategory categoryInitVal() {
        de.smava.webapp.applicant.brokerage.domain.DocumentCategory result;
        if (_categoryIsSet) {
            result = _categoryInitVal;
        } else {
            result = getCategory();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'category'.
     */
    public boolean categoryIsDirty() {
        return !valuesAreEqual(categoryInitVal(), getCategory());
    }

    /**
     * Returns true if the setter method was called for the property 'category'.
     */
    public boolean categoryIsSet() {
        return _categoryIsSet;
    }
	
    /**
     * Returns the initial value of the property 'file name'.
     */
    public String fileNameInitVal() {
        String result;
        if (_fileNameIsSet) {
            result = _fileNameInitVal;
        } else {
            result = getFileName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'file name'.
     */
    public boolean fileNameIsDirty() {
        return !valuesAreEqual(fileNameInitVal(), getFileName());
    }

    /**
     * Returns true if the setter method was called for the property 'file name'.
     */
    public boolean fileNameIsSet() {
        return _fileNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'source'.
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentSource sourceInitVal() {
        de.smava.webapp.applicant.brokerage.domain.DocumentSource result;
        if (_sourceIsSet) {
            result = _sourceInitVal;
        } else {
            result = getSource();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'source'.
     */
    public boolean sourceIsDirty() {
        return !valuesAreEqual(sourceInitVal(), getSource());
    }

    /**
     * Returns true if the setter method was called for the property 'source'.
     */
    public boolean sourceIsSet() {
        return _sourceIsSet;
    }

    /**
     * Returns the initial value of the property 'bank document id'.
     */
    public String bankDocumentIdInitVal() {
        String result;
        if (_bankDocumentIdIsSet) {
            result = _bankDocumentIdInitVal;
        } else {
            result = getExternalStorageId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank document id'..
     */
    public boolean bankDocumentIdIsDirty() {
        return !valuesAreEqual(bankDocumentIdInitVal(), getBankDocumentId());
    }
			
}
