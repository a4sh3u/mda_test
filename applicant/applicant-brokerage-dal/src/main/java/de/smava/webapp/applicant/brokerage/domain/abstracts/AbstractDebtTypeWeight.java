//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(debt type weight)}

import de.smava.webapp.applicant.brokerage.domain.DebtTypeWeight;
import de.smava.webapp.applicant.brokerage.domain.interfaces.DebtTypeWeightEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DebtTypeWeights'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractDebtTypeWeight
    extends BrokerageEntity    implements DebtTypeWeightEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDebtTypeWeight.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof DebtTypeWeight)) {
            return;
        }
        
        this.setDebtType(((DebtTypeWeight) oldEntity).getDebtType());    
        this.setWeight(((DebtTypeWeight) oldEntity).getWeight());    
        this.setBrokerageBank(((DebtTypeWeight) oldEntity).getBrokerageBank());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof DebtTypeWeight)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getDebtType(), ((DebtTypeWeight) otherEntity).getDebtType());    
        equals = equals && valuesAreEqual(this.getWeight(), ((DebtTypeWeight) otherEntity).getWeight());    
        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((DebtTypeWeight) otherEntity).getBrokerageBank());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(debt type weight)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

