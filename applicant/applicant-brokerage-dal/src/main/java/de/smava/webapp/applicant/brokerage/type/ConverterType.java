/**
 * 
 */
package de.smava.webapp.applicant.brokerage.type;

/**
 * @author bvoss
 *
 */
public enum ConverterType {
	
	MAP, EL, BEAN;

}
