package de.smava.webapp.applicant.brokerage.type;

/**
 * @author pvitic
 * @since 22.01.16.
 */
public enum HouseholdCalculationType {
    EXPENSE,
    INCOME,
    CALCULATION
}
