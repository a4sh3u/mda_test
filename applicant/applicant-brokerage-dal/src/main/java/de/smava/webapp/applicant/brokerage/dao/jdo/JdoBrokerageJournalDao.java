package de.smava.webapp.applicant.brokerage.dao.jdo;


import de.smava.webapp.account.domain.Account;
import de.smava.webapp.applicant.brokerage.dao.BrokerageJournalDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageJournal;
import de.smava.webapp.applicant.brokerage.type.BrokerageJournalType;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'BrokerageJournals'.
 *
 */
@Repository(value = "brokerageJournalDao")
public class JdoBrokerageJournalDao extends JdoBaseDao implements BrokerageJournalDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageJournalDao.class);

    private static final String CLASS_NAME = "BrokerageJournal";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    @Override
    public Collection<BrokerageJournal> findByParameters(String filter, List params ) {
        Query query = getPersistenceManager().newQuery(BrokerageJournal.class);
        query.setFilter(filter);
        query.setOrdering("_creationDate DESC");
        return (List<BrokerageJournal>) query.executeWithArray(params.toArray());
    }

    @Override
    public BrokerageJournal findLatestByParameters(String filter, List params ) {
        Query query = getPersistenceManager().newQuery(BrokerageJournal.class);
        query.setFilter(filter);
        query.setOrdering("_creationDate DESC");
        query.setRange(0L, 1L);
        Collection<BrokerageJournal> entries =
                (Collection<BrokerageJournal>) query.executeWithArray(params.toArray());
        if (entries != null && !entries.isEmpty()) {
            return entries.iterator().next();
        } else {
            return null;
        }
    }

    @Override
    public BrokerageJournal findLatestForBorrowerByTypeAndExternalRefId(Account borrower, BrokerageJournalType type, String externalRefId) {
        Query query = getPersistenceManager().newQuery(BrokerageJournal.class);
        query.setFilter("this._customerNumber == :customerNumber && this._type == :type && this._externalRefId == :externalRefId");
        query.setOrdering("_creationDate DESC");
        query.setRange(0,2);

        Collection<BrokerageJournal> entries =
                (Collection<BrokerageJournal>) query.executeWithArray(borrower.getCustomerNumber(), type, externalRefId);
        if (entries != null && !entries.isEmpty()) {
            return entries.iterator().next();
        } else {
            return null;
        }
    }

    /**
     * Returns an attached copy of the brokerage journal identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BrokerageJournal load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BrokerageJournal result = getEntity(BrokerageJournal.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        return exists(BrokerageJournal.class, id);
    }

    @Override
    public Collection<BrokerageJournal> findBrokerageJournalsByAccountSortedByCreation( Account account){
        OqlTerm oqlTerm = OqlTerm.newTerm();

        oqlTerm.equals("_customerNumber", account.getCustomerNumber());
        oqlTerm.setSortBy("_creationDate");
        oqlTerm.setSortDescending(true);

        Collection<BrokerageJournal> result = findEntities(BrokerageJournal.class, oqlTerm.toString() );
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result != null ? result.size() : 0);
        }
        return result;
    }

    /**
     * Saves the brokerage journal.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BrokerageJournal brokerageJournal) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageJournal: " + brokerageJournal);
        }
        return saveEntity(brokerageJournal);
    }

}
