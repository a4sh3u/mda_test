package de.smava.webapp.applicant.brokerage.additionaldata;

import com.fasterxml.jackson.annotation.JsonProperty;
import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Mikolaj Zymon
 * @since 31.03.2017
 */
public class VonEssenSubprimeAdditionalData extends AbstractAdditionalData {

    @JsonProperty
    private String backendUrl;

    @JsonProperty
    private String backendUrlExpiration;

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_VONESSEN_SUBPRIME;
    }

    public String getBackendUrl() {
        return backendUrl;
    }

    public void setBackendUrl(String backendUrl) {
        this.backendUrl = backendUrl;
    }

    public String getBackendUrlExpiration() {
        return backendUrlExpiration;
    }

    public void setBackendUrlExpiration(String backendUrlExpiration) {
        this.backendUrlExpiration = backendUrlExpiration;
    }
}
