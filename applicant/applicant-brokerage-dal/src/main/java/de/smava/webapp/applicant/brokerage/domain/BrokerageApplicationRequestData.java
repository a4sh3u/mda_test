//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application request data)}

import de.smava.webapp.applicant.brokerage.domain.history.BrokerageApplicationRequestDataHistory;

import java.util.Date;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationRequestDatas'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageApplicationRequestData extends BrokerageApplicationRequestDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage application request data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Date _creationDate;
        protected de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType _type;
        protected BrokerageApplication _brokerageApplication;
        protected BrokerageMultiStatusRequestData _brokerageMultiStatusRequestData;
        protected String _request;
        protected String _response;
        protected de.smava.webapp.applicant.brokerage.type.BrokerageState _state;
        protected Double _monthlyRate;
        protected Double _effectiveInterest;
        protected Double _amount;
        protected Integer _duration;
        protected de.smava.webapp.applicant.brokerage.type.RdiType _rdiType;
        protected Date _responseDate;
        protected String _httpState;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType getType() {
        return _type;
    }
                                            
    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }
                                            
    /**
     * Setter for the property 'brokerage multi status request data'.
     *
     * 
     *
     */
    public void setBrokerageMultiStatusRequestData(BrokerageMultiStatusRequestData brokerageMultiStatusRequestData) {
        _brokerageMultiStatusRequestData = brokerageMultiStatusRequestData;
    }
            
    /**
     * Returns the property 'brokerage multi status request data'.
     *
     * 
     *
     */
    public BrokerageMultiStatusRequestData getBrokerageMultiStatusRequestData() {
        return _brokerageMultiStatusRequestData;
    }
                                    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    public void setRequest(String request) {
        if (!_requestIsSet) {
            _requestIsSet = true;
            _requestInitVal = getRequest();
        }
        registerChange("request", _requestInitVal, request);
        _request = request;
    }
                        
    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    public String getRequest() {
        return _request;
    }
                                    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    public void setResponse(String response) {
        if (!_responseIsSet) {
            _responseIsSet = true;
            _responseInitVal = getResponse();
        }
        registerChange("response", _responseInitVal, response);
        _response = response;
    }
                        
    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    public String getResponse() {
        return _response;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(de.smava.webapp.applicant.brokerage.type.BrokerageState state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageState getState() {
        return _state;
    }
                                    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    public void setMonthlyRate(Double monthlyRate) {
        if (!_monthlyRateIsSet) {
            _monthlyRateIsSet = true;
            _monthlyRateInitVal = getMonthlyRate();
        }
        registerChange("monthly rate", _monthlyRateInitVal, monthlyRate);
        _monthlyRate = monthlyRate;
    }
                        
    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    public Double getMonthlyRate() {
        return _monthlyRate;
    }
                                    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    public void setEffectiveInterest(Double effectiveInterest) {
        if (!_effectiveInterestIsSet) {
            _effectiveInterestIsSet = true;
            _effectiveInterestInitVal = getEffectiveInterest();
        }
        registerChange("effective interest", _effectiveInterestInitVal, effectiveInterest);
        _effectiveInterest = effectiveInterest;
    }
                        
    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    public Double getEffectiveInterest() {
        return _effectiveInterest;
    }
                                    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(Double amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public Double getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    public void setDuration(Integer duration) {
        if (!_durationIsSet) {
            _durationIsSet = true;
            _durationInitVal = getDuration();
        }
        registerChange("duration", _durationInitVal, duration);
        _duration = duration;
    }
                        
    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    public Integer getDuration() {
        return _duration;
    }
                                    /**
     * Setter for the property 'rdi type'.
     *
     * 
     *
     */
    public void setRdiType(de.smava.webapp.applicant.brokerage.type.RdiType rdiType) {
        if (!_rdiTypeIsSet) {
            _rdiTypeIsSet = true;
            _rdiTypeInitVal = getRdiType();
        }
        registerChange("rdi type", _rdiTypeInitVal, rdiType);
        _rdiType = rdiType;
    }
                        
    /**
     * Returns the property 'rdi type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType getRdiType() {
        return _rdiType;
    }
                                    /**
     * Setter for the property 'response date'.
     *
     * 
     *
     */
    public void setResponseDate(Date responseDate) {
        if (!_responseDateIsSet) {
            _responseDateIsSet = true;
            _responseDateInitVal = getResponseDate();
        }
        registerChange("response date", _responseDateInitVal, responseDate);
        _responseDate = responseDate;
    }
                        
    /**
     * Returns the property 'response date'.
     *
     * 
     *
     */
    public Date getResponseDate() {
        return _responseDate;
    }

    /**
     * Setter for the property http state
     */
    public void setHttpState(String httpState) {
        if (!_httpStateIsSet) {
            _httpStateIsSet = true;
            _httpStateInitVal = getHttpState();
        }
        registerChange("http state", _httpStateInitVal, httpState);
        _httpState = httpState;
    }

    /**
     * Retturns the property http state
     */
    public String getHttpState() {
        return _httpState;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageApplication instanceof de.smava.webapp.commons.domain.Entity && !_brokerageApplication.getChangeSet().isEmpty()) {
             for (String element : _brokerageApplication.getChangeSet()) {
                 result.add("brokerage application : " + element);
             }
         }

         if (_brokerageMultiStatusRequestData instanceof de.smava.webapp.commons.domain.Entity && !_brokerageMultiStatusRequestData.getChangeSet().isEmpty()) {
             for (String element : _brokerageMultiStatusRequestData.getChangeSet()) {
                 result.add("brokerage multi status request data : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplicationRequestData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _request=").append(_request);
            builder.append("\n    _response=").append(_response);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _rdiType=").append(_rdiType);
            builder.append("\n    _httpState=").append(_httpState);
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplicationRequestData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplicationRequestData asBrokerageApplicationRequestData() {
        return this;
    }
}
