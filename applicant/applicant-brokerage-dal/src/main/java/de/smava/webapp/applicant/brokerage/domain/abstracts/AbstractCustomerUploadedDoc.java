//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer uploaded doc)}

import de.smava.webapp.applicant.brokerage.domain.CustomerUploadedDoc;
import de.smava.webapp.applicant.brokerage.domain.interfaces.CustomerUploadedDocEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CustomerUploadedDocs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCustomerUploadedDoc
    extends BrokerageEntity    implements CustomerUploadedDocEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCustomerUploadedDoc.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof CustomerUploadedDoc)) {
            return;
        }
        
        this.setAccount(((CustomerUploadedDoc) oldEntity).getAccount());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setDeletionDate(((CustomerUploadedDoc) oldEntity).getDeletionDate());    
        this.setStatus(((CustomerUploadedDoc) oldEntity).getStatus());    
        this.setExternalStorageId(((CustomerUploadedDoc) oldEntity).getExternalStorageId());    
        this.setCategory(((CustomerUploadedDoc) oldEntity).getCategory());    
        this.setFileName(((CustomerUploadedDoc) oldEntity).getFileName());    
        this.setSource(((CustomerUploadedDoc) oldEntity).getSource());    
        this.setAdvisorId(((CustomerUploadedDoc) oldEntity).getAdvisorId());    
        this.setApplicant(((CustomerUploadedDoc) oldEntity).getApplicant());    
        this.setBrokerageUploadedDocs(((CustomerUploadedDoc) oldEntity).getBrokerageUploadedDocs());    
        this.setBankDocumentId(((CustomerUploadedDoc) oldEntity).getBankDocumentId());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof CustomerUploadedDoc)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getAccount(), ((CustomerUploadedDoc) otherEntity).getAccount());    
            
        equals = equals && valuesAreEqual(this.getDeletionDate(), ((CustomerUploadedDoc) otherEntity).getDeletionDate());    
        equals = equals && valuesAreEqual(this.getStatus(), ((CustomerUploadedDoc) otherEntity).getStatus());    
        equals = equals && valuesAreEqual(this.getExternalStorageId(), ((CustomerUploadedDoc) otherEntity).getExternalStorageId());    
        equals = equals && valuesAreEqual(this.getCategory(), ((CustomerUploadedDoc) otherEntity).getCategory());    
        equals = equals && valuesAreEqual(this.getFileName(), ((CustomerUploadedDoc) otherEntity).getFileName());    
        equals = equals && valuesAreEqual(this.getSource(), ((CustomerUploadedDoc) otherEntity).getSource());    
        equals = equals && valuesAreEqual(this.getAdvisorId(), ((CustomerUploadedDoc) otherEntity).getAdvisorId());    
        equals = equals && valuesAreEqual(this.getApplicant(), ((CustomerUploadedDoc) otherEntity).getApplicant());    
        equals = equals && valuesAreEqual(this.getBrokerageUploadedDocs(), ((CustomerUploadedDoc) otherEntity).getBrokerageUploadedDocs());    
        equals = equals && valuesAreEqual(this.getBankDocumentId(), ((CustomerUploadedDoc) otherEntity).getBankDocumentId());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(customer uploaded doc)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

