package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.history.BrokerageBankMaintenanceHistory;

/**
 * @author Jakub Janus
 * @since 02.08.2017
 */
public class BrokerageBankMaintenance extends BrokerageBankMaintenanceHistory {

    private BrokerageBank _brokerageBank;
    private String _expressionFrom;
    private String _expressionTo;
    private Boolean _bankState;

    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }

    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }

    public String getExpressionFrom() {
        return _expressionFrom;
    }

    public void setExpressionFrom(String expressionFrom) {
        if (!expressionFromIsSet()) {
            _expressionFromIsSet = true;
            _expressionFromInitVal = getExpressionFrom();
        }
        registerChange("expressionFrom", _expressionFromInitVal, expressionFrom);
        _expressionFrom = expressionFrom;
    }

    public String getExpressionTo() {
        return _expressionTo;
    }

    public void setExpressionTo(String expressionTo) {
        if (!expressionToIsSet()) {
            _expressionToIsSet = true;
            _expressionToInitVal = getExpressionTo();
        }
        registerChange("expressionTo", _expressionToInitVal, expressionTo);
        _expressionTo = expressionTo;
    }

    public Boolean getBankState() {
        return _bankState;
    }

    public void setBankState(Boolean bankState) {
        if (!bankStateIsSet()) {
            _bankStateIsSet = true;
            _bankStateInitVal = getBankState();
        }
        registerChange("bankState", _bankStateInitVal, bankState);
        _bankState = bankState;
    }
}
