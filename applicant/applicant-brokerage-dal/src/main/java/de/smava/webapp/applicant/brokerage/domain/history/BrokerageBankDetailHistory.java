package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageBankDetail;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBankDetails'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankDetailHistory extends AbstractBrokerageBankDetail {

    protected transient de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _valueInitVal;
    protected transient boolean _valueIsSet;



    /**
     * Returns the initial value of the property 'name'.
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName nameInitVal() {
        de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'value'.
     */
    public String valueInitVal() {
        String result;
        if (_valueIsSet) {
            result = _valueInitVal;
        } else {
            result = getValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'value'.
     */
    public boolean valueIsDirty() {
        return !valuesAreEqual(valueInitVal(), getValue());
    }

    /**
     * Returns true if the setter method was called for the property 'value'.
     */
    public boolean valueIsSet() {
        return _valueIsSet;
    }

}
