package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.type.BrokerageJournalType;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ovolkovskyi on 19.10.2017.
 */
public class JournalQueryParam {

    private static final Logger LOGGER = Logger.getLogger(JournalQueryParam.class);

    private String filter;
    private List<List> paramList;

    private JournalQueryParam(String filter, List<List> params) {
        this.filter = filter;
        this.paramList = params;
    }

    public String getFilter() {
        return filter;
    }

    public List getParameters() {
        return this.paramList;
    }

    public static class JournalQueryParamBuilder {

        List<Long> customerNumbers = new LinkedList<Long>();
        List<Long> loanApplicationIds = new LinkedList<Long>();
        List<Long> brokerageApplicationIds = new LinkedList<Long>();
        List<BrokerageJournalType> types = new LinkedList<BrokerageJournalType>();
        Timestamp startTime;

        List paramList = new LinkedList();

        public JournalQueryParamBuilder() {
        }

        public JournalQueryParamBuilder customerNumber(Long customerNumber) {
            customerNumbers.add(customerNumber);
            return this;
        }

        public JournalQueryParamBuilder customerNumbers(List<Long> customerNumbers) {
            this.customerNumbers.addAll(customerNumbers);
            return this;
        }

        public JournalQueryParamBuilder loanApplicationId(Long loanApplicationId) {
            loanApplicationIds.add(loanApplicationId);
            return this;
        }

        public JournalQueryParamBuilder brokerageApplicationId(Long brokerageApplicationId) {
            this.brokerageApplicationIds.add(brokerageApplicationId);
            return this;
        }

        public JournalQueryParamBuilder addType(BrokerageJournalType type) {
            types.add(type);
            return this;
        }

        public JournalQueryParamBuilder addTypes(List<BrokerageJournalType> types) {
            this.types.addAll(types);
            return this;
        }

        public JournalQueryParamBuilder duration(int days) {
            Calendar calendar = CurrentDate.getCalendar();
            calendar.add(Calendar.DAY_OF_MONTH, -days);
            this.startTime = new Timestamp(calendar.getTimeInMillis());
            return this;
        }

        public JournalQueryParam build(){
            String filter = "";
            if (customerNumbers != null && customerNumbers.size() > 0) {
                filter += " :customerNumbers.contains(this._customerNumber) ";
                paramList.add(customerNumbers);
            }
            if (loanApplicationIds != null && loanApplicationIds.size() > 0) {
                if (filter.length() > 0) {
                    filter += " && ";
                }
                filter += " :loanApplicationId.contains(this._loanApplication._id) ";
                paramList.add(loanApplicationIds);
            }
            if (brokerageApplicationIds != null && brokerageApplicationIds.size() > 0) {
                if (filter.length() > 0) {
                    filter += " && ";
                }
                filter += " :brokerageApplicationId.contains(this._brokerageApplication._id) ";
                paramList.add(brokerageApplicationIds);
            }
            if (types != null && types.size() > 0) {
                if (filter.length() > 0) {
                    filter += " && ";
                }
                filter += " :types.contains(this._type) ";
                paramList.add(types);
            }
            if (startTime != null) {
                if (filter.length() > 0) {
                    filter += " && ";
                }
                filter += " this._creationDate >= :startTime ";
                paramList.add(startTime);
            }

            if (paramList.size() == 1) {
                LOGGER.warn("Only one parameter present for JournalQueryParam: " + paramList.get(0));
            }

            return new JournalQueryParam(filter, paramList);
        }

    }

}
