package de.smava.webapp.applicant.brokerage.domain.interfaces;


import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.brokerage.domain.MasterApplication;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;

import java.util.Date;


/**
 * The domain object that represents 'MasterApplications'.
 *
 * @author generator
 */
public interface MasterApplicationEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     *
     *
     */
    void setAccount(Account account);

    /**
     * Returns the property 'account'.
     *
     *
     *
     */
    Account getAccount();
    /**
     * Setter for the property 'loan application'.
     *
     *
     *
     */
    void setLoanApplication(LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     *
     *
     */
    LoanApplication getLoanApplication();
    /**
     * Setter for the property 'state'.
     *
     *
     *
     */
    void setState(BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     *
     *
     */
    BrokerageState getState();
    /**
     * Setter for the property 'last touchpoint'.
     *
     *
     *
     */
    void setLastTouchpoint(Date lastTouchpoint);

    /**
     * Returns the property 'last touchpoint'.
     *
     *
     *
     */
    Date getLastTouchpoint();
    /**
     * Setter for the property 'creation date'.
     *
     *
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     *
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'state changed date'.
     *
     *
     *
     */
    void setStateChangedDate(Date stateChangedDate);

    /**
     * Returns the property 'state changed date'.
     *
     *
     *
     */
    Date getStateChangedDate();
    /**
     * Setter for the property 'affiliate information'.
     *
     *
     *
     */
    void setAffiliateInformation(de.smava.webapp.marketing.affiliate.domain.AffiliateInformation affiliateInformation);

    /**
     * Returns the property 'affiliate information'.
     *
     *
     *
     */
    de.smava.webapp.marketing.affiliate.domain.AffiliateInformation getAffiliateInformation();
    /**
     * Setter for the property 'latest loan application'.
     *
     *
     *
     */
    void setLatestLoanApplication(LoanApplication latestLoanApplication);

    /**
     * Returns the property 'latest loan application'.
     *
     *
     *
     */
    LoanApplication getLatestLoanApplication();
    /**
     * Helper method to get reference of this object as model type.
     */
    MasterApplication asMasterApplication();
}