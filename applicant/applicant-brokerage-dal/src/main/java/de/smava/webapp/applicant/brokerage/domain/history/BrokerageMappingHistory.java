package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageMapping;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageMappings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageMappingHistory extends AbstractBrokerageMapping {

    protected transient String _smavaValueInitVal;
    protected transient boolean _smavaValueIsSet;
    protected transient String _bankValueInitVal;
    protected transient boolean _bankValueIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.MappingDirectionType _directionTypeInitVal;
    protected transient boolean _directionTypeIsSet;


		
    /**
     * Returns the initial value of the property 'smava value'.
     */
    public String smavaValueInitVal() {
        String result;
        if (_smavaValueIsSet) {
            result = _smavaValueInitVal;
        } else {
            result = getSmavaValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'smava value'.
     */
    public boolean smavaValueIsDirty() {
        return !valuesAreEqual(smavaValueInitVal(), getSmavaValue());
    }

    /**
     * Returns true if the setter method was called for the property 'smava value'.
     */
    public boolean smavaValueIsSet() {
        return _smavaValueIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank value'.
     */
    public String bankValueInitVal() {
        String result;
        if (_bankValueIsSet) {
            result = _bankValueInitVal;
        } else {
            result = getBankValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank value'.
     */
    public boolean bankValueIsDirty() {
        return !valuesAreEqual(bankValueInitVal(), getBankValue());
    }

    /**
     * Returns true if the setter method was called for the property 'bank value'.
     */
    public boolean bankValueIsSet() {
        return _bankValueIsSet;
    }
	
    /**
     * Returns the initial value of the property 'directionType'.
     */
    public de.smava.webapp.applicant.brokerage.type.MappingDirectionType directionTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.MappingDirectionType result;
        if (_directionTypeIsSet) {
            result = _directionTypeInitVal;
        } else {
            result = getDirectionType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'directionType'.
     */
    public boolean directionTypeIsDirty() {
        return !valuesAreEqual(directionTypeInitVal(), getDirectionType());
    }

    /**
     * Returns true if the setter method was called for the property 'directionType'.
     */
    public boolean directionTypeIsSet() {
        return _directionTypeIsSet;
    }

}
