package de.smava.webapp.applicant.brokerage.domain.interfaces;


import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.domain.BrokerageMultiStatusRequestData;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageMultiStatusRequestDatas'.
 *
 * @author generator
 */
public interface BrokerageMultiStatusRequestDataEntityInterface {

    /**
     * Setter for the property 'creation date'.
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     */
    Date getCreationDate();

    /**
     * Setter for the property 'brokerage bank'.
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     */
    BrokerageBank getBrokerageBank();

    /**
     * Setter for the property 'state'.
     */
    void setState(de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState state);

    /**
     * Returns the property 'state'.
     */
    de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState getState();

    /**
     * Setter for the property 'request'.
     */
    void setRequest(String request);

    /**
     * Returns the property 'request'.
     */
    String getRequest();

    /**
     * Setter for the property 'response'.
     */
    void setResponse(String response);

    /**
     * Returns the property 'response'.
     */
    String getResponse();

    /**
     * Setter for http state
     */
    void setHttpState(String httpState);

    /**
     * Getter for http state
     */
    String getHttpState();

    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageMultiStatusRequestData asBrokerageMultiStatusRequestData();
}
