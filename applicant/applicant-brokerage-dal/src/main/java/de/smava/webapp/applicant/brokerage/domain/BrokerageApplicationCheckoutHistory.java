package de.smava.webapp.applicant.brokerage.domain;


import de.smava.webapp.applicant.brokerage.domain.history.BrokerageApplicationCheckoutHistoryHistory;

import java.util.*;





/**
 * The domain object that represents 'BrokerageApplicationCheckoutHistorys'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageApplicationCheckoutHistory extends BrokerageApplicationCheckoutHistoryHistory  {




        protected Long _brokerageApplicationId;
        protected de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage _checkoutFlowPage;
        protected Date _creationDate;
        protected String _initiator;
        protected String _additionalData;
        
                                    
    /**
     * Setter for the property 'brokerage application id'.
     *
     * 
     *
     */
    public void setBrokerageApplicationId(Long brokerageApplicationId) {
        _brokerageApplicationId = brokerageApplicationId;
    }
            
    /**
     * Returns the property 'brokerage application id'.
     *
     * 
     *
     */
    public Long getBrokerageApplicationId() {
        return _brokerageApplicationId;
    }
                                            
    /**
     * Setter for the property 'checkout flow page'.
     *
     * 
     *
     */
    public void setCheckoutFlowPage(de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage checkoutFlowPage) {
        _checkoutFlowPage = checkoutFlowPage;
    }
            
    /**
     * Returns the property 'checkout flow page'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage getCheckoutFlowPage() {
        return _checkoutFlowPage;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'initiator'.
     *
     * 
     *
     */
    public void setInitiator(String initiator) {
        if (!_initiatorIsSet) {
            _initiatorIsSet = true;
            _initiatorInitVal = getInitiator();
        }
        registerChange("initiator", _initiatorInitVal, initiator);
        _initiator = initiator;
    }
                        
    /**
     * Returns the property 'initiator'.
     *
     * 
     *
     */
    public String getInitiator() {
        return _initiator;
    }
                                    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    public void setAdditionalData(String additionalData) {
        if (!_additionalDataIsSet) {
            _additionalDataIsSet = true;
            _additionalDataInitVal = getAdditionalData();
        }
        registerChange("additional data", _additionalDataInitVal, additionalData);
        _additionalData = additionalData;
    }
                        
    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    public String getAdditionalData() {
        return _additionalData;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_checkoutFlowPage instanceof de.smava.webapp.commons.domain.Entity && !_checkoutFlowPage.getChangeSet().isEmpty()) {
             for (String element : _checkoutFlowPage.getChangeSet()) {
                 result.add("checkout flow page : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageApplicationCheckoutHistory.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _initiator=").append(_initiator);
            builder.append("\n    _additionalData=").append(_additionalData);
            builder.append("\n}");
        } else {
            builder.append(BrokerageApplicationCheckoutHistory.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageApplicationCheckoutHistory asBrokerageApplicationCheckoutHistory() {
        return this;
    }
}
