package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationDocument;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentState;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * DAO interface for the domain object 'BrokerageApplicationDocuments'.
 *
 * @author Adam Tomecki
 * @since 30.10.2017
 */
public interface BrokerageApplicationDocumentDao extends BrokerageSchemaDao<BrokerageApplicationDocument> {

    /**
     * Set a synchronization for the current transaction to receive events
     * for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Deletes brokerage application document. The invoker is responsible for keeping the referential integrity intact.
     *
     * @param id the id of the brokerage application document to delete
     */
    void delete(Long id);

    /**
     * Retrieves 'BrokerageApplicationDocument' instance for given File Storage id and returns it
     * attached to the current persistence manager.
     */
    BrokerageApplicationDocument getDocumentByFileStorageId(Long fileStorageId);

    /**
     * Retrieves all 'BrokerageApplicationDocuments' instances for brokerage application and returns them
     * attached to the current persistence manager.
     */
    Collection<BrokerageApplicationDocument> getDocumentListByBrokerageApplication(Long brokerageApplicationId);

    /**
     * Retrieves all 'BrokerageApplicationDocuments' instances for brokerage application and states and returns them
     * attached to the current persistence manager.
     */
    Collection<BrokerageApplicationDocument> getDocumentListByBrokerageApplicationAndStates(
            Long brokerageApplicationId,
            List<BrokerageApplicationDocumentState> states
    );

    /**
     * Retrieves all 'BrokerageApplicationDocuments' instances created below limit date and returns them
     *
     * @param lowerLimitDate lower limit for creation date {@link Date}
     * @return brokerage application documents collection {@link Collection}
     */
    Collection<BrokerageApplicationDocument> getDocumentsToRemove(Date lowerLimitDate);
}
