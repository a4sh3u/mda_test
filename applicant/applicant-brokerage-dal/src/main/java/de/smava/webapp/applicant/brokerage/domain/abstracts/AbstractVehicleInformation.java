//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(vehicle information)}

import de.smava.webapp.applicant.brokerage.domain.VehicleInformation;
import de.smava.webapp.applicant.brokerage.domain.interfaces.VehicleInformationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'VehicleInformations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractVehicleInformation
    extends BrokerageEntity    implements VehicleInformationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractVehicleInformation.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof VehicleInformation)) {
            return;
        }
        
        this.setModel(((VehicleInformation) oldEntity).getModel());    
        this.setBrand(((VehicleInformation) oldEntity).getBrand());    
        this.setVehiclePrice(((VehicleInformation) oldEntity).getVehiclePrice());    
        this.setDownPayment(((VehicleInformation) oldEntity).getDownPayment());    
        this.setRegistrationYear(((VehicleInformation) oldEntity).getRegistrationYear());    
        this.setPowerKw(((VehicleInformation) oldEntity).getPowerKw());    
        this.setVehicleKm(((VehicleInformation) oldEntity).getVehicleKm());    
        this.setVehicleType(((VehicleInformation) oldEntity).getVehicleType());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof VehicleInformation)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getModel(), ((VehicleInformation) otherEntity).getModel());    
        equals = equals && valuesAreEqual(this.getBrand(), ((VehicleInformation) otherEntity).getBrand());    
        equals = equals && valuesAreEqual(this.getVehiclePrice(), ((VehicleInformation) otherEntity).getVehiclePrice());    
        equals = equals && valuesAreEqual(this.getDownPayment(), ((VehicleInformation) otherEntity).getDownPayment());    
        equals = equals && valuesAreEqual(this.getRegistrationYear(), ((VehicleInformation) otherEntity).getRegistrationYear());    
        equals = equals && valuesAreEqual(this.getPowerKw(), ((VehicleInformation) otherEntity).getPowerKw());    
        equals = equals && valuesAreEqual(this.getVehicleKm(), ((VehicleInformation) otherEntity).getVehicleKm());    
        equals = equals && valuesAreEqual(this.getVehicleType(), ((VehicleInformation) otherEntity).getVehicleType());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(vehicle information)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

