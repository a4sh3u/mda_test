//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage uploaded doc)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDoc;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageUploadedDocEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageUploadedDocs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageUploadedDoc
    extends BrokerageEntity    implements BrokerageUploadedDocEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageUploadedDoc.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageUploadedDoc)) {
            return;
        }
        
        this.setDocument(((BrokerageUploadedDoc) oldEntity).getDocument());    
        this.setBrokerageBank(((BrokerageUploadedDoc) oldEntity).getBrokerageBank());    
        this.setStatus(((BrokerageUploadedDoc) oldEntity).getStatus());    
        this.setSendDate(((BrokerageUploadedDoc) oldEntity).getSendDate());    
        this.setReceiveDate(((BrokerageUploadedDoc) oldEntity).getReceiveDate());    
        this.setDeletionDate(((BrokerageUploadedDoc) oldEntity).getDeletionDate());    
        this.setAdvisor(((BrokerageUploadedDoc) oldEntity).getAdvisor());    
        this.setConcatenatedDocumentId(((BrokerageUploadedDoc) oldEntity).getConcatenatedDocumentId());    
        this.setApproved(((BrokerageUploadedDoc) oldEntity).getApproved());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageUploadedDoc)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getDocument(), ((BrokerageUploadedDoc) otherEntity).getDocument());    
        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((BrokerageUploadedDoc) otherEntity).getBrokerageBank());    
        equals = equals && valuesAreEqual(this.getStatus(), ((BrokerageUploadedDoc) otherEntity).getStatus());    
        equals = equals && valuesAreEqual(this.getSendDate(), ((BrokerageUploadedDoc) otherEntity).getSendDate());    
        equals = equals && valuesAreEqual(this.getReceiveDate(), ((BrokerageUploadedDoc) otherEntity).getReceiveDate());    
        equals = equals && valuesAreEqual(this.getDeletionDate(), ((BrokerageUploadedDoc) otherEntity).getDeletionDate());    
        equals = equals && valuesAreEqual(this.getAdvisor(), ((BrokerageUploadedDoc) otherEntity).getAdvisor());    
        equals = equals && valuesAreEqual(this.getConcatenatedDocumentId(), ((BrokerageUploadedDoc) otherEntity).getConcatenatedDocumentId());    
        equals = equals && valuesAreEqual(this.getApproved(), ((BrokerageUploadedDoc) otherEntity).getApproved());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage uploaded doc)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

