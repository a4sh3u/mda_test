//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}

import de.smava.webapp.account.todo.util.DateUtils;
import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.brokerage.dao.BrokerageApplicationDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.sql.Timestamp;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BrokerageApplications'.
 *
 * @author generator
 */
@Repository(value = "applicantBrokerageApplicationDao")
public class JdoBrokerageApplicationDao extends JdoBaseDao implements BrokerageApplicationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageApplicationDao.class);

    private static final String CLASS_NAME = "BrokerageApplication";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(brokerage application)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the brokerage application identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BrokerageApplication load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BrokerageApplication result = getEntity(BrokerageApplication.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BrokerageApplication getBrokerageApplication(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BrokerageApplication entity = findUniqueEntity(BrokerageApplication.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the brokerage application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BrokerageApplication brokerageApplication) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageApplication: " + brokerageApplication);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(brokerage application)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(brokerageApplication);
    }

    /**
     * @deprecated Use {@link #save(BrokerageApplication) instead}
     */
    public Long saveBrokerageApplication(BrokerageApplication brokerageApplication) {
        return save(brokerageApplication);
    }

    /**
     * Deletes an brokerage application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application
     */
    public void deleteBrokerageApplication(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBrokerageApplication: " + id);
        }
        deleteEntity(BrokerageApplication.class, id);
    }

    /**
     * Retrieves all 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = getEntities(BrokerageApplication.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageApplication' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BrokerageApplication' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BrokerageApplication' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BrokerageApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BrokerageApplication> findBrokerageApplicationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationList(Account account, BrokerageBank bank, Collection<BrokerageState> states, Date startSearch, Date endSearch) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(account);
        params.add( bank);
        params.addAll(states);
        params.add( startSearch);
        params.add(endSearch);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", account),
                    oqlTerm.equals("_brokerageBank", bank),
                    getStatesTerm(oqlTerm, states),
                    oqlTerm.greaterThanEquals("_creationDate", startSearch),
                    oqlTerm.lessThan("_creationDate", endSearch));
        } else {
            oqlTerm.and(
                    oqlTerm.equals("_account", account),
                    oqlTerm.equals("_brokerageBank", bank),
                    oqlTerm.greaterThanEquals("_creationDate", startSearch),
                    oqlTerm.lessThan("_creationDate", endSearch));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + account.getCustomerNumber());
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationList(BrokerageBank bank, Collection<BrokerageState> states, Date creationDate, Date endDate) {

        Collection<Object> params = new ArrayList<Object>();
        params.add( bank);
        params.add(creationDate);
        params.add( endDate);
        params.addAll(states);

        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_brokerageBank", bank),
                    oqlTerm.greaterThanEquals("_creationDate", creationDate),
                    oqlTerm.lessThan("_creationDate", endDate),
                    getStatesTerm(oqlTerm, states));
        } else {
            oqlTerm.and(
                    oqlTerm.equals("_brokerageBank", bank),
                    oqlTerm.greaterThanEquals("_creationDate", creationDate),
                    oqlTerm.lessThan("_creationDate", endDate));
        }

        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationList(Account account, BrokerageBank bank, Collection<BrokerageState> states) {

        Collection<Object> params = new ArrayList<Object>();
        params.add(account);
        params.add( bank);
        params.addAll(states);
        OqlTerm oqlTerm = OqlTerm.newTerm();
        if (states.size() >= 1) {
            oqlTerm.and(
                    oqlTerm.equals("_account", account),
                    oqlTerm.equals("_brokerageBank", bank),
                    getStatesTerm(oqlTerm, states));
        } else {
            oqlTerm.and(
                    oqlTerm.equals("_account", account),
                    oqlTerm.equals("_brokerageBank", bank));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - start: borrower=" + account.getCustomerNumber());
        }
        Collection<BrokerageApplication> result = findEntities(BrokerageApplication.class, oqlTerm.toString(),  params.toArray());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("findOrderList() - result: size=" + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplication' instances.
     */
    public long getBrokerageApplicationCount() {
        long result = getEntityCount(BrokerageApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplication' instances which match the given whereClause.
     */
    public long getBrokerageApplicationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BrokerageApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BrokerageApplication' instances which match the given whereClause together with params specified in object array.
     */
    public long getBrokerageApplicationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BrokerageApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application)}
    //
    // insert custom methods here

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplicationByAccountAndBankIn(Long accountId,
                                                                                        List<BrokerageBank> brokerageBanks) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationByAccountAndBankIn");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("accountId", accountId);
        params.put("brokerageBanks", brokerageBanks);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> customerUploadedDocs = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplications(List<Long> brokerageApplicationIds) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplications");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageApplicationIds", brokerageApplicationIds);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> customerUploadedDocs = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplicationByAccountAndBankInAndStateInDescOrder(Long accountId,
                                                                                                           List<BrokerageBank> brokerageBanks,
                                                                                                           List<BrokerageState> brokerageStates) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationByAccountAndBankInAndStateInDescOrder");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("accountId", accountId);
        params.put("brokerageBanks", brokerageBanks);
        params.put("brokerageStates", brokerageStates);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> customerUploadedDocs = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplicationBankIdAndByExtRef(Long brokerageBankId, String extRefNumber) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationBankIdAndByExtRef");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageBankId", brokerageBankId);
        params.put("extRefNumber", extRefNumber);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> brokerageApplications = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return brokerageApplications;
    }

    @Override
    public Collection<BrokerageApplication> fetchBrokerageApplicationsWithFailedDocRequestByBanks(List<BrokerageBank> brokerageBanks, Date date) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "fetchBrokerageApplicationsWithFailedDocRequestByBanks");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageBanks", brokerageBanks);
        params.put("creationDateAfter", date);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> brokerageApplications = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return brokerageApplications;
    }

    /**
     * Gets the states term for finding offers and orders.
     */
    private OqlTerm getStatesTerm(OqlTerm oqlTerm, Collection<BrokerageState> states) {
        OqlTerm result;
        if (states.size() > 1) {
            OqlTerm [] orStates = new OqlTerm[states.size()];
            int i = 0;
            for (BrokerageState state : states) {
                orStates[i] = oqlTerm.equals("_state", state);
                i++;
            }
            result = oqlTerm.or(orStates);
        } else if (states.size() == 1) {
            result = oqlTerm.equals("_state", states.iterator().next());
        } else {
            throw new IllegalArgumentException("States must not be empty");
        }
        return result;
    }

    @Override
    public Collection<Long> getRecentlyPaidoutBrokerageApplications( int minElapsedDays, int maxElapsedDays ){

        Date now = CurrentDate.getDate();

        Date limitDateBottom = DateUtils.addDays(now, -maxElapsedDays);
        now = CurrentDate.getDate();
        Date limitDateTop = DateUtils.addDays(now, -minElapsedDays);

        return getRecentlyPaidoutBrokerageApplications(limitDateBottom, limitDateTop);
    }

    @Override
    public Collection<BrokerageApplication> findBrokerageApplicationList(Collection<BrokerageBank> brokerageBanks, Collection<BrokerageState> states, Date startSearch, Date endSearch) {
        Query query = getPersistenceManager().newNamedQuery(BrokerageApplication.class, "findBrokerageApplicationListByBanksAndStates");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageBanks", brokerageBanks);
        params.put("states", states);
        params.put("startSearch", startSearch);
        params.put("endSearch", endSearch);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> brokerageApplications = (Collection<BrokerageApplication>) query.executeWithMap(params);

        return brokerageApplications;
    }

    private Collection<Long> getRecentlyPaidoutBrokerageApplications( Date limitDateBottom, Date limitDateTop ){
        StringBuffer sql =  new StringBuffer();
        sql.append("select distinct (ba.id) as id " +
                " from brokerage.brokerage_application  ba, brokerage.brokerage_application_request_data bard "+
                " where " +
                " 1 = 1 " +
                " and bard.brokerage_application_id = ba.id "+
                " and ba.state = 'APPLICATION_PAYOUT'" +
                " and bard.state = 'APPLICATION_PAYOUT'" +
                " and bard.creation_date BETWEEN :bottom AND :top ");
        Map<String, Object> parameters = new HashMap<String, Object>(2);
        parameters.put("bottom", new Timestamp(limitDateBottom.getTime()));

        parameters.put("top", new Timestamp(limitDateTop.getTime()));

        Query query = getPersistenceManager().newQuery(Query.SQL, sql.toString());
        query.setClass(BrokerageApplication.class);
        @SuppressWarnings("unchecked")
        Collection<BrokerageApplication> bas = (Collection<BrokerageApplication>) query.executeWithMap(parameters);

        Collection<Long> ids = new ArrayList<Long>();
        for (BrokerageApplication a : bas) {
            ids.add(a.getId());
        }
        return ids;
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
