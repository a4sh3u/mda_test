package de.smava.webapp.applicant.brokerage.dao.additionalData;


import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.HashMap;

@JsonFilter(NorisbankAdditionalData.NORISBANK_FILTER)
public class NorisbankAdditionalData extends DeutscheBankAdditionalData {

    private static final long serialVersionUID = 1943863879098413411L;

    public static final String NORISBANK_FILTER = "norisbankFilter";

    public static final String TOTAL_INSURANCE_PREMIUM = "totalInsurancePremium";
    public static final String INSURED_AMOUNT_RKV = "insuredAmountRKV";
    public static final String INSURED_AMOUNT_AUV = "insuredAmountAUV";
    public static final String INSURED_AMOUNT_ALV = "insuredAmountALV";
    public static final String INSURANCE_DURATION = "insuranceDuration";
    public static final String REQUEST_ID = "requestId";
    public static final String DOCUMENT_REQUEST_ERROR_CODE = "documentRequestErrorCode";

    @JsonProperty(TOTAL_INSURANCE_PREMIUM)
    private BigDecimal totalInsurancePremium;

    @JsonProperty(INSURED_AMOUNT_RKV)
    private BigDecimal insuredAmountRKV;

    @JsonProperty(INSURED_AMOUNT_AUV)
    private BigDecimal insuredAmountAUV;

    @JsonProperty(INSURED_AMOUNT_ALV)
    private BigDecimal insuredAmountALV;

    @JsonProperty(INSURANCE_DURATION)
    private Integer insuranceDuration;

    @JsonProperty(DOCUMENT_REQUEST_ERROR_CODE)
    private String documentRequestErrorCode;

    @JsonProperty(REQUEST_ID)
    private String requestId;

    @Override
    public String determineFilterName() {
        return NORISBANK_FILTER;
    }

    public void setTotalInsurancePremium(BigDecimal totalInsurancePremium) {
        this.totalInsurancePremium = totalInsurancePremium;
    }

    public BigDecimal getTotalInsurancePremium() {
        return totalInsurancePremium;
    }

    public void setInsuredAmountRKV(BigDecimal insuredAmountRKV) {
        this.insuredAmountRKV = insuredAmountRKV;
    }

    public BigDecimal getInsuredAmountRKV() {
        return insuredAmountRKV;
    }

    public void setInsuredAmountAUV(BigDecimal insuredAmountAUV) {
        this.insuredAmountAUV = insuredAmountAUV;
    }

    public BigDecimal getInsuredAmountAUV() {
        return insuredAmountAUV;
    }

    public void setInsuredAmountALV(BigDecimal insuredAmountALV) {
        this.insuredAmountALV = insuredAmountALV;
    }

    public BigDecimal getInsuredAmountALV() {
        return insuredAmountALV;
    }

    public void setInsuranceDuration(Integer insuranceDuration) {
        this.insuranceDuration = insuranceDuration;
    }

    public Integer getInsuranceDuration() {
        return insuranceDuration;
    }

    public String getDocumentRequestErrorCode() {
        return documentRequestErrorCode;
    }

    public void setDocumentRequestErrorCode(String documentRequestErrorCode) {
        this.documentRequestErrorCode = documentRequestErrorCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public HashMap<String, Object> readAdditionalData() {
        return readAdditionalData(NORISBANK_FILTER, RETURN_CODE, UPSELLING_OFFER, REQUEST_STATE_TEXT, REASON_FOR_REPAIRS,
                REASON_FOR_REFUSALS, AMOUNT_LAST_INSTALLMENT, NUMBER_INSTALLMENTS, INTEREST_RATE, AMOUNT_INTERESTS,
                TOTAL_LOAN_AMOUNT, DATE_FIRST_INSTALLMENT, MESSAGES, ORDER_REPRINT_DATE, TOTAL_INSURANCE_PREMIUM, INSURED_AMOUNT_RKV,
                INSURED_AMOUNT_AUV, INSURED_AMOUNT_ALV, INSURANCE_DURATION, DOCUMENT_REQUEST_ERROR_CODE, TRANSACTION_ID, REQUEST_ID);
    }
}
