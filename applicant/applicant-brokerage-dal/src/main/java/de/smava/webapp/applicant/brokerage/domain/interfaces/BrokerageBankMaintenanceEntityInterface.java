package de.smava.webapp.applicant.brokerage.domain.interfaces;

import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;

/**
 * @author Jakub Janus
 * @since 02.08.2017
 */
public interface BrokerageBankMaintenanceEntityInterface {

    void setBrokerageBank(BrokerageBank brokerageBank);

    BrokerageBank getBrokerageBank();

    void setExpressionFrom(String expressionFrom);

    String getExpressionFrom();

    void setExpressionTo(String expressionTo);

    String getExpressionTo();

    void setBankState(Boolean bankState);

    Boolean getBankState();
}
