//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage mapping converter)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageMappingConverterHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMappingConverters'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageMappingConverter extends BrokerageMappingConverterHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage mapping converter)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _value;
        protected de.smava.webapp.applicant.brokerage.type.ConverterType _type;
        
                            /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    public String getValue() {
        return _value;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.brokerage.type.ConverterType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.ConverterType getType() {
        return _type;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageMappingConverter.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _value=").append(_value);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(BrokerageMappingConverter.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageMappingConverter asBrokerageMappingConverter() {
        return this;
    }
}
