package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageHouseholdCalculation;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageHouseholdCalculations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageHouseholdCalculationHistory extends AbstractBrokerageHouseholdCalculation {

    protected transient Double _incomeInitVal;
    protected transient boolean _incomeIsSet;
    protected transient Double _incomeSecondApplicantInitVal;
    protected transient boolean _incomeSecondApplicantIsSet;
    protected transient Double _expensesInitVal;
    protected transient boolean _expensesIsSet;
    protected transient Double _expensesSecondApplicantInitVal;
    protected transient boolean _expensesSecondApplicantIsSet;
    protected transient Double _disposableIncomeInitVal;
    protected transient boolean _disposableIncomeIsSet;
    protected transient Double _disposableIncomeSecondApplicantInitVal;
    protected transient boolean _disposableIncomeSecondApplicantIsSet;
    protected transient Double _indebtednessInitVal;
    protected transient boolean _indebtednessIsSet;
    protected transient Double _indebtednessSecondApplicantInitVal;
    protected transient boolean _indebtednessSecondApplicantIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;


		
    /**
     * Returns the initial value of the property 'income'.
     */
    public Double incomeInitVal() {
        Double result;
        if (_incomeIsSet) {
            result = _incomeInitVal;
        } else {
            result = getIncome();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'income'.
     */
    public boolean incomeIsDirty() {
        return !valuesAreEqual(incomeInitVal(), getIncome());
    }

    /**
     * Returns true if the setter method was called for the property 'income'.
     */
    public boolean incomeIsSet() {
        return _incomeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'income second applicant'.
     */
    public Double incomeSecondApplicantInitVal() {
        Double result;
        if (_incomeSecondApplicantIsSet) {
            result = _incomeSecondApplicantInitVal;
        } else {
            result = getIncomeSecondApplicant();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'income second applicant'.
     */
    public boolean incomeSecondApplicantIsDirty() {
        return !valuesAreEqual(incomeSecondApplicantInitVal(), getIncomeSecondApplicant());
    }

    /**
     * Returns true if the setter method was called for the property 'income second applicant'.
     */
    public boolean incomeSecondApplicantIsSet() {
        return _incomeSecondApplicantIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expenses'.
     */
    public Double expensesInitVal() {
        Double result;
        if (_expensesIsSet) {
            result = _expensesInitVal;
        } else {
            result = getExpenses();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expenses'.
     */
    public boolean expensesIsDirty() {
        return !valuesAreEqual(expensesInitVal(), getExpenses());
    }

    /**
     * Returns true if the setter method was called for the property 'expenses'.
     */
    public boolean expensesIsSet() {
        return _expensesIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expenses second applicant'.
     */
    public Double expensesSecondApplicantInitVal() {
        Double result;
        if (_expensesSecondApplicantIsSet) {
            result = _expensesSecondApplicantInitVal;
        } else {
            result = getExpensesSecondApplicant();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expenses second applicant'.
     */
    public boolean expensesSecondApplicantIsDirty() {
        return !valuesAreEqual(expensesSecondApplicantInitVal(), getExpensesSecondApplicant());
    }

    /**
     * Returns true if the setter method was called for the property 'expenses second applicant'.
     */
    public boolean expensesSecondApplicantIsSet() {
        return _expensesSecondApplicantIsSet;
    }
	
    /**
     * Returns the initial value of the property 'disposable income'.
     */
    public Double disposableIncomeInitVal() {
        Double result;
        if (_disposableIncomeIsSet) {
            result = _disposableIncomeInitVal;
        } else {
            result = getDisposableIncome();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'disposable income'.
     */
    public boolean disposableIncomeIsDirty() {
        return !valuesAreEqual(disposableIncomeInitVal(), getDisposableIncome());
    }

    /**
     * Returns true if the setter method was called for the property 'disposable income'.
     */
    public boolean disposableIncomeIsSet() {
        return _disposableIncomeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'disposable income second applicant'.
     */
    public Double disposableIncomeSecondApplicantInitVal() {
        Double result;
        if (_disposableIncomeSecondApplicantIsSet) {
            result = _disposableIncomeSecondApplicantInitVal;
        } else {
            result = getDisposableIncomeSecondApplicant();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'disposable income second applicant'.
     */
    public boolean disposableIncomeSecondApplicantIsDirty() {
        return !valuesAreEqual(disposableIncomeSecondApplicantInitVal(), getDisposableIncomeSecondApplicant());
    }

    /**
     * Returns true if the setter method was called for the property 'disposable income second applicant'.
     */
    public boolean disposableIncomeSecondApplicantIsSet() {
        return _disposableIncomeSecondApplicantIsSet;
    }
	
    /**
     * Returns the initial value of the property 'indebtedness'.
     */
    public Double indebtednessInitVal() {
        Double result;
        if (_indebtednessIsSet) {
            result = _indebtednessInitVal;
        } else {
            result = getIndebtedness();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'indebtedness'.
     */
    public boolean indebtednessIsDirty() {
        return !valuesAreEqual(indebtednessInitVal(), getIndebtedness());
    }

    /**
     * Returns true if the setter method was called for the property 'indebtedness'.
     */
    public boolean indebtednessIsSet() {
        return _indebtednessIsSet;
    }
	
    /**
     * Returns the initial value of the property 'indebtedness second applicant'.
     */
    public Double indebtednessSecondApplicantInitVal() {
        Double result;
        if (_indebtednessSecondApplicantIsSet) {
            result = _indebtednessSecondApplicantInitVal;
        } else {
            result = getIndebtednessSecondApplicant();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'indebtedness second applicant'.
     */
    public boolean indebtednessSecondApplicantIsDirty() {
        return !valuesAreEqual(indebtednessSecondApplicantInitVal(), getIndebtednessSecondApplicant());
    }

    /**
     * Returns true if the setter method was called for the property 'indebtedness second applicant'.
     */
    public boolean indebtednessSecondApplicantIsSet() {
        return _indebtednessSecondApplicantIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
}
