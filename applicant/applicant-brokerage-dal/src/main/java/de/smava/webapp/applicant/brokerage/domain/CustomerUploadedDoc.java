//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer uploaded doc)}
import de.smava.webapp.applicant.brokerage.domain.history.CustomerUploadedDocHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CustomerUploadedDocs'.
 *
 * 
 *
 * @author generator
 */
public class CustomerUploadedDoc extends CustomerUploadedDocHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(customer uploaded doc)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.account.domain.Account _account;
        protected Date _creationDate;
        protected Date _deletionDate;
        protected de.smava.webapp.applicant.brokerage.domain.DocumentStatus _status;
        protected String _externalStorageId;
        protected de.smava.webapp.applicant.brokerage.domain.DocumentCategory _category;
        protected String _fileName;
        protected de.smava.webapp.applicant.brokerage.domain.DocumentSource _source;
        protected Long _advisorId;
        protected de.smava.webapp.applicant.domain.Applicant _applicant;
        protected Set<BrokerageUploadedDoc> _brokerageUploadedDocs;
        protected String _bankDocumentId;
        
                                    
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    public void setAccount(de.smava.webapp.applicant.account.domain.Account account) {
        _account = account;
    }
            
    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.account.domain.Account getAccount() {
        return _account;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'deletion date'.
     *
     * 
     *
     */
    public void setDeletionDate(Date deletionDate) {
        if (!_deletionDateIsSet) {
            _deletionDateIsSet = true;
            _deletionDateInitVal = getDeletionDate();
        }
        registerChange("deletion date", _deletionDateInitVal, deletionDate);
        _deletionDate = deletionDate;
    }
                        
    /**
     * Returns the property 'deletion date'.
     *
     * 
     *
     */
    public Date getDeletionDate() {
        return _deletionDate;
    }
                                    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    public void setStatus(de.smava.webapp.applicant.brokerage.domain.DocumentStatus status) {
        if (!_statusIsSet) {
            _statusIsSet = true;
            _statusInitVal = getStatus();
        }
        registerChange("status", _statusInitVal, status);
        _status = status;
    }
                        
    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentStatus getStatus() {
        return _status;
    }
                                    /**
     * Setter for the property 'external storage id'.
     *
     * 
     *
     */
    public void setExternalStorageId(String externalStorageId) {
        if (!_externalStorageIdIsSet) {
            _externalStorageIdIsSet = true;
            _externalStorageIdInitVal = getExternalStorageId();
        }
        registerChange("external storage id", _externalStorageIdInitVal, externalStorageId);
        _externalStorageId = externalStorageId;
    }
                        
    /**
     * Returns the property 'external storage id'.
     *
     * 
     *
     */
    public String getExternalStorageId() {
        return _externalStorageId;
    }
                                    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    public void setCategory(de.smava.webapp.applicant.brokerage.domain.DocumentCategory category) {
        if (!_categoryIsSet) {
            _categoryIsSet = true;
            _categoryInitVal = getCategory();
        }
        registerChange("category", _categoryInitVal, category);
        _category = category;
    }
                        
    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentCategory getCategory() {
        return _category;
    }
                                    /**
     * Setter for the property 'file name'.
     *
     * 
     *
     */
    public void setFileName(String fileName) {
        if (!_fileNameIsSet) {
            _fileNameIsSet = true;
            _fileNameInitVal = getFileName();
        }
        registerChange("file name", _fileNameInitVal, fileName);
        _fileName = fileName;
    }
                        
    /**
     * Returns the property 'file name'.
     *
     * 
     *
     */
    public String getFileName() {
        return _fileName;
    }
                                    /**
     * Setter for the property 'source'.
     *
     * 
     *
     */
    public void setSource(de.smava.webapp.applicant.brokerage.domain.DocumentSource source) {
        if (!_sourceIsSet) {
            _sourceIsSet = true;
            _sourceInitVal = getSource();
        }
        registerChange("source", _sourceInitVal, source);
        _source = source;
    }
                        
    /**
     * Returns the property 'source'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentSource getSource() {
        return _source;
    }
                                            
    /**
     * Setter for the property 'advisor id'.
     *
     * 
     *
     */
    public void setAdvisorId(Long advisorId) {
        _advisorId = advisorId;
    }
            
    /**
     * Returns the property 'advisor id'.
     *
     * 
     *
     */
    public Long getAdvisorId() {
        return _advisorId;
    }
                                            
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    public void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant) {
        _applicant = applicant;
    }
            
    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.Applicant getApplicant() {
        return _applicant;
    }
                                            
    /**
     * Setter for the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    public void setBrokerageUploadedDocs(Set<BrokerageUploadedDoc> brokerageUploadedDocs) {
        _brokerageUploadedDocs = brokerageUploadedDocs;
    }
            
    /**
     * Returns the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    public Set<BrokerageUploadedDoc> getBrokerageUploadedDocs() {
        return _brokerageUploadedDocs;
    }
                                    /**
     * Setter for the property 'bank document id'.
     *
     * 
     *
     */
    public void setBankDocumentId(String bankDocumentId) {
        if (!_bankDocumentIdIsSet) {
            _bankDocumentIdIsSet = true;
            _bankDocumentIdInitVal = getBankDocumentId();
        }
        registerChange("bank document id", _bankDocumentIdInitVal, bankDocumentId);
        _bankDocumentId = bankDocumentId;
    }
                        
    /**
     * Returns the property 'bank document id'.
     *
     * 
     *
     */
    public String getBankDocumentId() {
        return _bankDocumentId;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_account instanceof de.smava.webapp.commons.domain.Entity && !_account.getChangeSet().isEmpty()) {
             for (String element : _account.getChangeSet()) {
                 result.add("account : " + element);
             }
         }

         if (_applicant instanceof de.smava.webapp.commons.domain.Entity && !_applicant.getChangeSet().isEmpty()) {
             for (String element : _applicant.getChangeSet()) {
                 result.add("applicant : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CustomerUploadedDoc.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _status=").append(_status);
            builder.append("\n    _externalStorageId=").append(_externalStorageId);
            builder.append("\n    _category=").append(_category);
            builder.append("\n    _fileName=").append(_fileName);
            builder.append("\n    _source=").append(_source);
            builder.append("\n    _bankDocumentId=").append(_bankDocumentId);
            builder.append("\n}");
        } else {
            builder.append(CustomerUploadedDoc.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CustomerUploadedDoc asCustomerUploadedDoc() {
        return this;
    }
}
