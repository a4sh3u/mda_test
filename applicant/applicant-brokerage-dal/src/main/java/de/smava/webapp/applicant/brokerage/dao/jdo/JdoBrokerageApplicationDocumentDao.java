package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.BrokerageApplicationDocumentDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationDocument;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentState;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'BrokerageApplicationDocuments'.
 *
 * @author Adam Tomecki
 * @since 30.10.2017
 */
@Repository(value = "brokerageApplicationDocumentDao")
public class JdoBrokerageApplicationDocumentDao extends JdoBaseDao implements BrokerageApplicationDocumentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageApplicationDocumentDao.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerageApplicationDocument load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("BrokerageApplicationDocument load for id " + id);
        }

        BrokerageApplicationDocument result = getEntity(BrokerageApplicationDocument.class, id);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("BrokerageApplicationDocument load result " + result);
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long save(BrokerageApplicationDocument brokerageApplicationDocument) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("save BrokerageApplicationDocument: " + brokerageApplicationDocument);
        }

        return saveEntity(brokerageApplicationDocument);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists(Long id) {
        boolean result = false;

        if (id != null) {
            BrokerageApplicationDocument entity = findUniqueEntity(BrokerageApplicationDocument.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("delete BrokerageApplicationDocument: " + id);
        }

        deleteEntity(BrokerageApplicationDocument.class, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrokerageApplicationDocument getDocumentByFileStorageId(Long fileStorageId) {
        Query query = getPersistenceManager().newNamedQuery(
                BrokerageApplicationDocument.class,
                "getDocumentByFileStorageId"
        );
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("fileStorageId", fileStorageId);

        return (BrokerageApplicationDocument) query.executeWithMap(params);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BrokerageApplicationDocument> getDocumentListByBrokerageApplication(Long brokerageApplicationId) {
        Collection<BrokerageApplicationDocument> result = new ArrayList<BrokerageApplicationDocument>();
        Query query = getPersistenceManager().newNamedQuery(
                BrokerageApplicationDocument.class,
                "getDocumentListByBrokerageApplication"
        );
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageApplicationId", brokerageApplicationId);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplicationDocument> brokerageApplicationDocuments =
                (Collection<BrokerageApplicationDocument>) query.executeWithMap(params);

        if (brokerageApplicationDocuments != null) {
            result = brokerageApplicationDocuments;
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BrokerageApplicationDocument> getDocumentListByBrokerageApplicationAndStates(
            Long brokerageApplicationId, List<BrokerageApplicationDocumentState> states) {

        Collection<BrokerageApplicationDocument> result = new ArrayList<BrokerageApplicationDocument>();
        Query query = getPersistenceManager().newNamedQuery(
                BrokerageApplicationDocument.class,
                "getDocumentListByBrokerageApplicationAndStates"
        );

        if (states == null || states.isEmpty()) {
            states = Arrays.asList(BrokerageApplicationDocumentState.values());
        }

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageApplicationId", brokerageApplicationId);
        params.put("states", states);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplicationDocument> brokerageApplicationDocuments =
                (Collection<BrokerageApplicationDocument>) query.executeWithMap(params);

        if (brokerageApplicationDocuments != null) {
            result = brokerageApplicationDocuments;
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<BrokerageApplicationDocument> getDocumentsToRemove(Date lowerLimitDate) {
        Collection<BrokerageApplicationDocument> result = new ArrayList<BrokerageApplicationDocument>();
        Query query = getPersistenceManager().newNamedQuery(
                BrokerageApplicationDocument.class,
                "getDocumentsToRemove"
        );

        if (lowerLimitDate == null) {
            return result;
        }

        List<BrokerageApplicationDocumentState> states = Collections.singletonList(BrokerageApplicationDocumentState.SAVED);
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("lowerLimitDate", lowerLimitDate);
        params.put("states", states);

        @SuppressWarnings("unchecked")
        Collection<BrokerageApplicationDocument> brokerageApplicationDocuments =
                (Collection<BrokerageApplicationDocument>) query.executeWithMap(params);

        if (brokerageApplicationDocuments != null) {
            result = brokerageApplicationDocuments;
        }

        return result;
    }
}
