package de.smava.webapp.applicant.brokerage.type;

/**
 * Brokerage application document state
 *
 * @author Adam Tomecki
 * @since 26.10.2017
 */
public enum BrokerageApplicationDocumentState {
    RECEIVED,
    SAVED,
    DELETED
}
