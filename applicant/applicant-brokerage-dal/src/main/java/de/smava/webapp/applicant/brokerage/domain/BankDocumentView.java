//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank document view)}
import de.smava.webapp.applicant.brokerage.domain.history.BankDocumentViewHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankDocumentViews'.
 *
 * 
 *
 * @author generator
 */
public class BankDocumentView extends BankDocumentViewHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(bank document view)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _extRefNumber;
        protected Long _customerNumber;
        protected Long _accountId;
        protected Long _brokerageBankId;
        protected String _bankName;
        protected Date _creationDate;
        protected Date _sendDate;
        protected Date _receiveDate;
        protected String _firstName;
        protected String _lastName;
        protected Long _brokerageAccountId;
        protected String _additionalData;
        
                            /**
     * Setter for the property 'ext ref number'.
     *
     * 
     *
     */
    public void setExtRefNumber(String extRefNumber) {
        if (!_extRefNumberIsSet) {
            _extRefNumberIsSet = true;
            _extRefNumberInitVal = getExtRefNumber();
        }
        registerChange("ext ref number", _extRefNumberInitVal, extRefNumber);
        _extRefNumber = extRefNumber;
    }
                        
    /**
     * Returns the property 'ext ref number'.
     *
     * 
     *
     */
    public String getExtRefNumber() {
        return _extRefNumber;
    }
                                            
    /**
     * Setter for the property 'customer number'.
     *
     * 
     *
     */
    public void setCustomerNumber(Long customerNumber) {
        _customerNumber = customerNumber;
    }
            
    /**
     * Returns the property 'customer number'.
     *
     * 
     *
     */
    public Long getCustomerNumber() {
        return _customerNumber;
    }
                                            
    /**
     * Setter for the property 'account id'.
     *
     * 
     *
     */
    public void setAccountId(Long accountId) {
        _accountId = accountId;
    }
            
    /**
     * Returns the property 'account id'.
     *
     * 
     *
     */
    public Long getAccountId() {
        return _accountId;
    }
                                            
    /**
     * Setter for the property 'brokerage bank id'.
     *
     * 
     *
     */
    public void setBrokerageBankId(Long brokerageBankId) {
        _brokerageBankId = brokerageBankId;
    }
            
    /**
     * Returns the property 'brokerage bank id'.
     *
     * 
     *
     */
    public Long getBrokerageBankId() {
        return _brokerageBankId;
    }
                                    /**
     * Setter for the property 'bank name'.
     *
     * 
     *
     */
    public void setBankName(String bankName) {
        if (!_bankNameIsSet) {
            _bankNameIsSet = true;
            _bankNameInitVal = getBankName();
        }
        registerChange("bank name", _bankNameInitVal, bankName);
        _bankName = bankName;
    }
                        
    /**
     * Returns the property 'bank name'.
     *
     * 
     *
     */
    public String getBankName() {
        return _bankName;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'send date'.
     *
     * 
     *
     */
    public void setSendDate(Date sendDate) {
        if (!_sendDateIsSet) {
            _sendDateIsSet = true;
            _sendDateInitVal = getSendDate();
        }
        registerChange("send date", _sendDateInitVal, sendDate);
        _sendDate = sendDate;
    }
                        
    /**
     * Returns the property 'send date'.
     *
     * 
     *
     */
    public Date getSendDate() {
        return _sendDate;
    }
                                    /**
     * Setter for the property 'receive date'.
     *
     * 
     *
     */
    public void setReceiveDate(Date receiveDate) {
        if (!_receiveDateIsSet) {
            _receiveDateIsSet = true;
            _receiveDateInitVal = getReceiveDate();
        }
        registerChange("receive date", _receiveDateInitVal, receiveDate);
        _receiveDate = receiveDate;
    }
                        
    /**
     * Returns the property 'receive date'.
     *
     * 
     *
     */
    public Date getReceiveDate() {
        return _receiveDate;
    }
                                    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("first name", _firstNameInitVal, firstName);
        _firstName = firstName;
    }
                        
    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    public String getFirstName() {
        return _firstName;
    }
                                    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("last name", _lastNameInitVal, lastName);
        _lastName = lastName;
    }
                        
    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    public String getLastName() {
        return _lastName;
    }
                                            
    /**
     * Setter for the property 'brokerage account id'.
     *
     * 
     *
     */
    public void setBrokerageAccountId(Long brokerageAccountId) {
        _brokerageAccountId = brokerageAccountId;
    }
            
    /**
     * Returns the property 'brokerage account id'.
     *
     * 
     *
     */
    public Long getBrokerageAccountId() {
        return _brokerageAccountId;
    }
                                    /**
     * Setter for the property 'additional data'.
     *
     * 
     *
     */
    public void setAdditionalData(String additionalData) {
        if (!_additionalDataIsSet) {
            _additionalDataIsSet = true;
            _additionalDataInitVal = getAdditionalData();
        }
        registerChange("additional data", _additionalDataInitVal, additionalData);
        _additionalData = additionalData;
    }
                        
    /**
     * Returns the property 'additional data'.
     *
     * 
     *
     */
    public String getAdditionalData() {
        return _additionalData;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BankDocumentView.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _extRefNumber=").append(_extRefNumber);
            builder.append("\n    _bankName=").append(_bankName);
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _additionalData=").append(_additionalData);
            builder.append("\n}");
        } else {
            builder.append(BankDocumentView.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BankDocumentView asBankDocumentView() {
        return this;
    }
}
