package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageApplicationCheckoutStatus;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationCheckoutStatuss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationCheckoutStatusHistory extends AbstractBrokerageApplicationCheckoutStatus {

    protected transient Date _updateTimestampInitVal;
    protected transient boolean _updateTimestampIsSet;



    /**
     * Returns the initial value of the property 'update timestamp'.
     */
    public Date updateTimestampInitVal() {
        Date result;
        if (_updateTimestampIsSet) {
            result = _updateTimestampInitVal;
        } else {
            result = getUpdateTimestamp();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'update timestamp'.
     */
    public boolean updateTimestampIsDirty() {
        return !valuesAreEqual(updateTimestampInitVal(), getUpdateTimestamp());
    }

    /**
     * Returns true if the setter method was called for the property 'update timestamp'.
     */
    public boolean updateTimestampIsSet() {
        return _updateTimestampIsSet;
    }

}
