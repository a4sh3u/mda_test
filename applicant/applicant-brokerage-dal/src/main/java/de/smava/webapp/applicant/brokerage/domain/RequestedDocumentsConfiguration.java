//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(requested documents configuration)}
import de.smava.webapp.applicant.brokerage.domain.history.RequestedDocumentsConfigurationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RequestedDocumentsConfigurations'.
 *
 * 
 *
 * @author generator
 */
public class RequestedDocumentsConfiguration extends RequestedDocumentsConfigurationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(requested documents configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected String _employmentType;
        protected String _applicationType;
        protected Integer _minVolume;
        protected Integer _maxVolume;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration _brokerageBankConfiguration;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'employment type'.
     *
     * 
     *
     */
    public void setEmploymentType(String employmentType) {
        if (!_employmentTypeIsSet) {
            _employmentTypeIsSet = true;
            _employmentTypeInitVal = getEmploymentType();
        }
        registerChange("employment type", _employmentTypeInitVal, employmentType);
        _employmentType = employmentType;
    }
                        
    /**
     * Returns the property 'employment type'.
     *
     * 
     *
     */
    public String getEmploymentType() {
        return _employmentType;
    }
                                    /**
     * Setter for the property 'application type'.
     *
     * 
     *
     */
    public void setApplicationType(String applicationType) {
        if (!_applicationTypeIsSet) {
            _applicationTypeIsSet = true;
            _applicationTypeInitVal = getApplicationType();
        }
        registerChange("application type", _applicationTypeInitVal, applicationType);
        _applicationType = applicationType;
    }
                        
    /**
     * Returns the property 'application type'.
     *
     * 
     *
     */
    public String getApplicationType() {
        return _applicationType;
    }
                                    /**
     * Setter for the property 'min volume'.
     *
     * 
     *
     */
    public void setMinVolume(Integer minVolume) {
        if (!_minVolumeIsSet) {
            _minVolumeIsSet = true;
            _minVolumeInitVal = getMinVolume();
        }
        registerChange("min volume", _minVolumeInitVal, minVolume);
        _minVolume = minVolume;
    }
                        
    /**
     * Returns the property 'min volume'.
     *
     * 
     *
     */
    public Integer getMinVolume() {
        return _minVolume;
    }
                                    /**
     * Setter for the property 'max Volume'.
     *
     * 
     *
     */
    public void setMaxVolume(Integer maxVolume) {
        if (!_maxVolumeIsSet) {
            _maxVolumeIsSet = true;
            _maxVolumeInitVal = getMaxVolume();
        }
        registerChange("max Volume", _maxVolumeInitVal, maxVolume);
        _maxVolume = maxVolume;
    }
                        
    /**
     * Returns the property 'max Volume'.
     *
     * 
     *
     */
    public Integer getMaxVolume() {
        return _maxVolume;
    }
                                            
    /**
     * Setter for the property 'brokerage bank configuration'.
     *
     * 
     *
     */
    public void setBrokerageBankConfiguration(de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration brokerageBankConfiguration) {
        _brokerageBankConfiguration = brokerageBankConfiguration;
    }
            
    /**
     * Returns the property 'brokerage bank configuration'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration getBrokerageBankConfiguration() {
        return _brokerageBankConfiguration;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageBankConfiguration instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBankConfiguration.getChangeSet().isEmpty()) {
             for (String element : _brokerageBankConfiguration.getChangeSet()) {
                 result.add("brokerage bank configuration : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(RequestedDocumentsConfiguration.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _employmentType=").append(_employmentType);
            builder.append("\n    _applicationType=").append(_applicationType);
            builder.append("\n}");
        } else {
            builder.append(RequestedDocumentsConfiguration.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public RequestedDocumentsConfiguration asRequestedDocumentsConfiguration() {
        return this;
    }
}
