package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDoc;
import de.smava.webapp.applicant.brokerage.domain.CustomerUploadedDoc;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'CustomerUploadedDocs'.
 *
 * @author generator
 */
public interface CustomerUploadedDocEntityInterface {

    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'deletion date'.
     *
     * 
     *
     */
    void setDeletionDate(Date deletionDate);

    /**
     * Returns the property 'deletion date'.
     *
     * 
     *
     */
    Date getDeletionDate();
    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    void setStatus(de.smava.webapp.applicant.brokerage.domain.DocumentStatus status);

    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.DocumentStatus getStatus();
    /**
     * Setter for the property 'external storage id'.
     *
     * 
     *
     */
    void setExternalStorageId(String externalStorageId);

    /**
     * Returns the property 'external storage id'.
     *
     * 
     *
     */
    String getExternalStorageId();
    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    void setCategory(de.smava.webapp.applicant.brokerage.domain.DocumentCategory category);

    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.DocumentCategory getCategory();
    /**
     * Setter for the property 'file name'.
     *
     * 
     *
     */
    void setFileName(String fileName);

    /**
     * Returns the property 'file name'.
     *
     * 
     *
     */
    String getFileName();
    /**
     * Setter for the property 'source'.
     *
     * 
     *
     */
    void setSource(de.smava.webapp.applicant.brokerage.domain.DocumentSource source);

    /**
     * Returns the property 'source'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.DocumentSource getSource();
    /**
     * Setter for the property 'advisor id'.
     *
     * 
     *
     */
    void setAdvisorId(Long advisorId);

    /**
     * Returns the property 'advisor id'.
     *
     * 
     *
     */
    Long getAdvisorId();
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    void setApplicant(de.smava.webapp.applicant.domain.Applicant applicant);

    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.Applicant getApplicant();
    /**
     * Setter for the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    void setBrokerageUploadedDocs(Set<BrokerageUploadedDoc> brokerageUploadedDocs);

    /**
     * Setter for the property 'bank document id'.
     *
     *
     *
     */
    void setBankDocumentId(String bankDocumentId);

    /**
     * Returns the property 'bank document id'.
     *
     *
     *
     */
    String getBankDocumentId();

    /**
     * Returns the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    Set<BrokerageUploadedDoc> getBrokerageUploadedDocs();
    /**
     * Helper method to get reference of this object as model type.
     */
    CustomerUploadedDoc asCustomerUploadedDoc();
}
