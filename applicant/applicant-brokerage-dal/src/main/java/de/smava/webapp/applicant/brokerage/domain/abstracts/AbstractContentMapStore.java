//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(content map store)}

import de.smava.webapp.applicant.brokerage.domain.ContentMapStore;
import de.smava.webapp.applicant.brokerage.domain.interfaces.ContentMapStoreEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ContentMapStores'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractContentMapStore
    extends BrokerageEntity    implements ContentMapStoreEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractContentMapStore.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof ContentMapStore)) {
            return;
        }
        
        this.setClazzName(((ContentMapStore) oldEntity).getClazzName());    
        this.setClazzId(((ContentMapStore) oldEntity).getClazzId());    
        this.setKey(((ContentMapStore) oldEntity).getKey());    
        this.setValue(((ContentMapStore) oldEntity).getValue());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof ContentMapStore)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getClazzName(), ((ContentMapStore) otherEntity).getClazzName());    
        equals = equals && valuesAreEqual(this.getClazzId(), ((ContentMapStore) otherEntity).getClazzId());    
        equals = equals && valuesAreEqual(this.getKey(), ((ContentMapStore) otherEntity).getKey());    
        equals = equals && valuesAreEqual(this.getValue(), ((ContentMapStore) otherEntity).getValue());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(content map store)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

