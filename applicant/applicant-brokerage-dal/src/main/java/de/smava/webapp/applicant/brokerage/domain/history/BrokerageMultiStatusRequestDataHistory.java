package de.smava.webapp.applicant.brokerage.domain.history;


import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageMultiStatusRequestData;

import java.util.Date;


/**
 * The domain object that has all history aggregation related fields for 'BrokerageMultiStatusRequestDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageMultiStatusRequestDataHistory extends AbstractBrokerageMultiStatusRequestData {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _requestInitVal;
    protected transient boolean _requestIsSet;
    protected transient String _responseInitVal;
    protected transient boolean _responseIsSet;
    protected transient String _httpStateInitVal;
    protected transient boolean _httpStateIsSet;


    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }

    /**
     * Returns the initial value of the property 'state'.
     */
    public de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState stateInitVal() {
        de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }

    /**
     * Returns the initial value of the property 'request'.
     */
    public String requestInitVal() {
        String result;
        if (_requestIsSet) {
            result = _requestInitVal;
        } else {
            result = getRequest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'request'.
     */
    public boolean requestIsDirty() {
        return !valuesAreEqual(requestInitVal(), getRequest());
    }

    /**
     * Returns true if the setter method was called for the property 'request'.
     */
    public boolean requestIsSet() {
        return _requestIsSet;
    }

    /**
     * Returns the initial value of the property 'response'.
     */
    public String responseInitVal() {
        String result;
        if (_responseIsSet) {
            result = _responseInitVal;
        } else {
            result = getResponse();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response'.
     */
    public boolean responseIsDirty() {
        return !valuesAreEqual(responseInitVal(), getResponse());
    }

    /**
     * Returns true if the setter method was called for the property 'response'.
     */
    public boolean responseIsSet() {
        return _responseIsSet;
    }

    /**
     * Returns the initial value of the property 'http_state'
     */
    public String httpStateInitVal() {
        String result;
        if (_httpStateIsSet) {
            result = _httpStateInitVal;
        } else {
            result = getHttpState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'http_state'.
     */
    public boolean httpStateIsDirty() {
        return !valuesAreEqual(httpStateInitVal(), getHttpState());
    }

    /**
     * Returns true if the setter ,ethod was called for the property 'http_state'.
     */
    public boolean httpStateIsSet() {
        return _httpStateIsSet;
    }

}
