package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractVehicleInformation;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'VehicleInformations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class VehicleInformationHistory extends AbstractVehicleInformation {

    protected transient String _modelInitVal;
    protected transient boolean _modelIsSet;
    protected transient String _brandInitVal;
    protected transient boolean _brandIsSet;
    protected transient Double _vehiclePriceInitVal;
    protected transient boolean _vehiclePriceIsSet;
    protected transient Double _downPaymentInitVal;
    protected transient boolean _downPaymentIsSet;
    protected transient Date _registrationYearInitVal;
    protected transient boolean _registrationYearIsSet;
    protected transient Integer _powerKwInitVal;
    protected transient boolean _powerKwIsSet;
    protected transient Integer _vehicleKmInitVal;
    protected transient boolean _vehicleKmIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.VehicleType _vehicleTypeInitVal;
    protected transient boolean _vehicleTypeIsSet;


	
    /**
     * Returns the initial value of the property 'model'.
     */
    public String modelInitVal() {
        String result;
        if (_modelIsSet) {
            result = _modelInitVal;
        } else {
            result = getModel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'model'.
     */
    public boolean modelIsDirty() {
        return !valuesAreEqual(modelInitVal(), getModel());
    }

    /**
     * Returns true if the setter method was called for the property 'model'.
     */
    public boolean modelIsSet() {
        return _modelIsSet;
    }
	
    /**
     * Returns the initial value of the property 'brand'.
     */
    public String brandInitVal() {
        String result;
        if (_brandIsSet) {
            result = _brandInitVal;
        } else {
            result = getBrand();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'brand'.
     */
    public boolean brandIsDirty() {
        return !valuesAreEqual(brandInitVal(), getBrand());
    }

    /**
     * Returns true if the setter method was called for the property 'brand'.
     */
    public boolean brandIsSet() {
        return _brandIsSet;
    }
	
    /**
     * Returns the initial value of the property 'vehicle price'.
     */
    public Double vehiclePriceInitVal() {
        Double result;
        if (_vehiclePriceIsSet) {
            result = _vehiclePriceInitVal;
        } else {
            result = getVehiclePrice();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vehicle price'.
     */
    public boolean vehiclePriceIsDirty() {
        return !valuesAreEqual(vehiclePriceInitVal(), getVehiclePrice());
    }

    /**
     * Returns true if the setter method was called for the property 'vehicle price'.
     */
    public boolean vehiclePriceIsSet() {
        return _vehiclePriceIsSet;
    }
	
    /**
     * Returns the initial value of the property 'down payment'.
     */
    public Double downPaymentInitVal() {
        Double result;
        if (_downPaymentIsSet) {
            result = _downPaymentInitVal;
        } else {
            result = getDownPayment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'down payment'.
     */
    public boolean downPaymentIsDirty() {
        return !valuesAreEqual(downPaymentInitVal(), getDownPayment());
    }

    /**
     * Returns true if the setter method was called for the property 'down payment'.
     */
    public boolean downPaymentIsSet() {
        return _downPaymentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'registration year'.
     */
    public Date registrationYearInitVal() {
        Date result;
        if (_registrationYearIsSet) {
            result = _registrationYearInitVal;
        } else {
            result = getRegistrationYear();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'registration year'.
     */
    public boolean registrationYearIsDirty() {
        return !valuesAreEqual(registrationYearInitVal(), getRegistrationYear());
    }

    /**
     * Returns true if the setter method was called for the property 'registration year'.
     */
    public boolean registrationYearIsSet() {
        return _registrationYearIsSet;
    }
	
    /**
     * Returns the initial value of the property 'power kw'.
     */
    public Integer powerKwInitVal() {
        Integer result;
        if (_powerKwIsSet) {
            result = _powerKwInitVal;
        } else {
            result = getPowerKw();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'power kw'.
     */
    public boolean powerKwIsDirty() {
        return !valuesAreEqual(powerKwInitVal(), getPowerKw());
    }

    /**
     * Returns true if the setter method was called for the property 'power kw'.
     */
    public boolean powerKwIsSet() {
        return _powerKwIsSet;
    }
	
    /**
     * Returns the initial value of the property 'vehicle km'.
     */
    public Integer vehicleKmInitVal() {
        Integer result;
        if (_vehicleKmIsSet) {
            result = _vehicleKmInitVal;
        } else {
            result = getVehicleKm();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vehicle km'.
     */
    public boolean vehicleKmIsDirty() {
        return !valuesAreEqual(vehicleKmInitVal(), getVehicleKm());
    }

    /**
     * Returns true if the setter method was called for the property 'vehicle km'.
     */
    public boolean vehicleKmIsSet() {
        return _vehicleKmIsSet;
    }
	
    /**
     * Returns the initial value of the property 'vehicle type'.
     */
    public de.smava.webapp.applicant.brokerage.type.VehicleType vehicleTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.VehicleType result;
        if (_vehicleTypeIsSet) {
            result = _vehicleTypeInitVal;
        } else {
            result = getVehicleType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'vehicle type'.
     */
    public boolean vehicleTypeIsDirty() {
        return !valuesAreEqual(vehicleTypeInitVal(), getVehicleType());
    }

    /**
     * Returns true if the setter method was called for the property 'vehicle type'.
     */
    public boolean vehicleTypeIsSet() {
        return _vehicleTypeIsSet;
    }

}
