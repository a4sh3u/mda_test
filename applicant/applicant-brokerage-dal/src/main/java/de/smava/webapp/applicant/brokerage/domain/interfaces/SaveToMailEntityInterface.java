package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.brokerage.domain.SaveToMail;

import java.util.Date;


/**
 * The domain object that represents 'SaveToMails'.
 *
 * @author generator
 */
public interface SaveToMailEntityInterface {

    /**
     * Setter for the property 'registration route'.
     *
     * 
     *
     */
    void setRegistrationRoute(String registrationRoute);

    /**
     * Returns the property 'registration route'.
     *
     * 
     *
     */
    String getRegistrationRoute();
    /**
     * Setter for the property 'current page'.
     *
     * 
     *
     */
    void setCurrentPage(String currentPage);

    /**
     * Returns the property 'current page'.
     *
     * 
     *
     */
    String getCurrentPage();
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    void setLoanApplication(LoanApplication loanApplication);

    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    LoanApplication getLoanApplication();
    /**
     * Setter for the property 'mail triggered'.
     *
     * 
     *
     */
    void setMailTriggered(Date mailTriggered);

    /**
     * Returns the property 'mail triggered'.
     *
     * 
     *
     */
    Date getMailTriggered();
    /**
     * Helper method to get reference of this object as model type.
     */
    SaveToMail asSaveToMail();
}
