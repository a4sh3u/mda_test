package de.smava.webapp.applicant.brokerage.type;

public enum EventQueueState {
	OPEN,
	SUCCESSFUL,
	STATE_FAILED,
	VOID,
	DELETED,
	DRAFT
}
