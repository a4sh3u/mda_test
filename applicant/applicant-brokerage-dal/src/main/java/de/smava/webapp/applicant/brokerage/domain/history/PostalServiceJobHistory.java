package de.smava.webapp.applicant.brokerage.domain.history;

import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractPostalServiceJob;

import java.util.Date;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public abstract class PostalServiceJobHistory extends AbstractPostalServiceJob {

    protected transient Date _jobStartDateInitVal;
    protected transient boolean _jobStartDateIsSet;
    protected transient Date _jobEndDateInitVal;
    protected transient boolean _jobEndDateIsSet;
    protected transient Date _periodFromInitVal;
    protected transient boolean _periodFromIsSet;
    protected transient Date _periodToInitVal;
    protected transient boolean _periodToIsSet;


    public Date jobStartDateInitVal() {
        Date result;
        if (_jobStartDateIsSet) {
            result = _jobStartDateInitVal;
        } else {
            result = getJobStartDate();
        }
        return result;
    }

    public boolean jobStartDateIsDirty() {
        return !valuesAreEqual(jobStartDateInitVal(), getJobStartDate());
    }

    public boolean jobStartDateIsSet() {
        return _jobStartDateIsSet;
    }

    public Date endDateInitVal() {
        Date result;
        if (_jobEndDateIsSet) {
            result = _jobEndDateInitVal;
        } else {
            result = getJobEndDate();
        }
        return result;
    }

    public boolean endDateIsDirty() {
        return !valuesAreEqual(endDateInitVal(), getJobEndDate());
    }

    public boolean endDateIsSet() {
        return _jobEndDateIsSet;
    }


    public Date periodFromInitVal() {
        Date result;
        if (_periodFromIsSet) {
            result = _periodFromInitVal;
        } else {
            result = getPeriodFrom();
        }
        return result;
    }

    public boolean periodFromIsDirty() {
        return !valuesAreEqual(periodFromInitVal(), getPeriodFrom());
    }

    public boolean periodFromIsSet() {
        return _periodFromIsSet;
    }

    public Date periodToInitVal() {
        Date result;
        if (_periodToIsSet) {
            result = _periodToInitVal;
        } else {
            result = getPeriodTo();
        }
        return result;
    }

    public boolean periodToIsDirty() {
        return !valuesAreEqual(periodToInitVal(), getPeriodTo());
    }

    public boolean periodToIsSet() {
        return _periodToIsSet;
    }

}
