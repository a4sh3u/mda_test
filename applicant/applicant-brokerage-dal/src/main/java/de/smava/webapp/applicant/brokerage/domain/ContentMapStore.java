//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(content map store)}
import de.smava.webapp.applicant.brokerage.domain.history.ContentMapStoreHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ContentMapStores'.
 *
 * 
 *
 * @author generator
 */
public class ContentMapStore extends ContentMapStoreHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(content map store)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _clazzName;
        protected Long _clazzId;
        protected String _key;
        protected String _value;
        
                            /**
     * Setter for the property 'clazz name'.
     *
     * 
     *
     */
    public void setClazzName(String clazzName) {
        if (!_clazzNameIsSet) {
            _clazzNameIsSet = true;
            _clazzNameInitVal = getClazzName();
        }
        registerChange("clazz name", _clazzNameInitVal, clazzName);
        _clazzName = clazzName;
    }
                        
    /**
     * Returns the property 'clazz name'.
     *
     * 
     *
     */
    public String getClazzName() {
        return _clazzName;
    }
                                            
    /**
     * Setter for the property 'clazz id'.
     *
     * 
     *
     */
    public void setClazzId(Long clazzId) {
        _clazzId = clazzId;
    }
            
    /**
     * Returns the property 'clazz id'.
     *
     * 
     *
     */
    public Long getClazzId() {
        return _clazzId;
    }
                                    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    public void setKey(String key) {
        if (!_keyIsSet) {
            _keyIsSet = true;
            _keyInitVal = getKey();
        }
        registerChange("key", _keyInitVal, key);
        _key = key;
    }
                        
    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    public String getKey() {
        return _key;
    }
                                    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    public String getValue() {
        return _value;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ContentMapStore.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _clazzName=").append(_clazzName);
            builder.append("\n    _key=").append(_key);
            builder.append("\n    _value=").append(_value);
            builder.append("\n}");
        } else {
            builder.append(ContentMapStore.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ContentMapStore asContentMapStore() {
        return this;
    }
}
