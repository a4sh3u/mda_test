package de.smava.webapp.applicant.brokerage.domain.abstracts;

import de.smava.webapp.applicant.brokerage.domain.interfaces.PostalServiceMasterApplicationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public abstract class AbstractPostalServiceMasterApplication
        extends BrokerageEntity implements PostalServiceMasterApplicationEntityInterface {


    protected static final Logger LOGGER = Logger.getLogger(AbstractPostalServiceMasterApplication.class);
}
