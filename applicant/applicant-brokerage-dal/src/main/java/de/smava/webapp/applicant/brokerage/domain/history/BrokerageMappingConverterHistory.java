package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageMappingConverter;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageMappingConverters'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageMappingConverterHistory extends AbstractBrokerageMappingConverter {

    protected transient String _valueInitVal;
    protected transient boolean _valueIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.ConverterType _typeInitVal;
    protected transient boolean _typeIsSet;


	
    /**
     * Returns the initial value of the property 'value'.
     */
    public String valueInitVal() {
        String result;
        if (_valueIsSet) {
            result = _valueInitVal;
        } else {
            result = getValue();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'value'.
     */
    public boolean valueIsDirty() {
        return !valuesAreEqual(valueInitVal(), getValue());
    }

    /**
     * Returns true if the setter method was called for the property 'value'.
     */
    public boolean valueIsSet() {
        return _valueIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.brokerage.type.ConverterType typeInitVal() {
        de.smava.webapp.applicant.brokerage.type.ConverterType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }

}
