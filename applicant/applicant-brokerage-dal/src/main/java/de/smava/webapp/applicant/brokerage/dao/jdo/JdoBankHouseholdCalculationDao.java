//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank household calculation)}
import de.smava.webapp.applicant.brokerage.dao.BankHouseholdCalculationDao;
import de.smava.webapp.applicant.brokerage.domain.BankHouseholdCalculation;
import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation;
import de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'BankHouseholdCalculations'.
 *
 * @author generator
 */
@Repository(value = "applicantBankHouseholdCalculationDao")
public class JdoBankHouseholdCalculationDao extends JdoBaseDao implements BankHouseholdCalculationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBankHouseholdCalculationDao.class);

    private static final String CLASS_NAME = "BankHouseholdCalculation";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(bank household calculation)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the bank household calculation identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public BankHouseholdCalculation load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        BankHouseholdCalculation result = getEntity(BankHouseholdCalculation.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public BankHouseholdCalculation getBankHouseholdCalculation(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	BankHouseholdCalculation entity = findUniqueEntity(BankHouseholdCalculation.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the bank household calculation.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(BankHouseholdCalculation bankHouseholdCalculation) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBankHouseholdCalculation: " + bankHouseholdCalculation);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(bank household calculation)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(bankHouseholdCalculation);
    }

    /**
     * @deprecated Use {@link #save(BankHouseholdCalculation) instead}
     */
    public Long saveBankHouseholdCalculation(BankHouseholdCalculation bankHouseholdCalculation) {
        return save(bankHouseholdCalculation);
    }

    /**
     * Deletes an bank household calculation, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bank household calculation
     */
    public void deleteBankHouseholdCalculation(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteBankHouseholdCalculation: " + id);
        }
        deleteEntity(BankHouseholdCalculation.class, id);
    }

    /**
     * Retrieves all 'BankHouseholdCalculation' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<BankHouseholdCalculation> getBankHouseholdCalculationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<BankHouseholdCalculation> result = getEntities(BankHouseholdCalculation.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'BankHouseholdCalculation' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<BankHouseholdCalculation> getBankHouseholdCalculationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<BankHouseholdCalculation> result = getEntities(BankHouseholdCalculation.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BankHouseholdCalculation' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<BankHouseholdCalculation> getBankHouseholdCalculationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<BankHouseholdCalculation> result = getEntities(BankHouseholdCalculation.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankHouseholdCalculation' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<BankHouseholdCalculation> getBankHouseholdCalculationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BankHouseholdCalculation> result = getEntities(BankHouseholdCalculation.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BankHouseholdCalculation' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankHouseholdCalculation> findBankHouseholdCalculationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'BankHouseholdCalculation' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<BankHouseholdCalculation> findBankHouseholdCalculationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'BankHouseholdCalculation' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankHouseholdCalculation> findBankHouseholdCalculationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'BankHouseholdCalculation' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankHouseholdCalculation> findBankHouseholdCalculationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankHouseholdCalculation' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankHouseholdCalculation> findBankHouseholdCalculationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'BankHouseholdCalculation' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<BankHouseholdCalculation> findBankHouseholdCalculationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'BankHouseholdCalculation' instances.
     */
    public long getBankHouseholdCalculationCount() {
        long result = getEntityCount(BankHouseholdCalculation.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BankHouseholdCalculation' instances which match the given whereClause.
     */
    public long getBankHouseholdCalculationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(BankHouseholdCalculation.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'BankHouseholdCalculation' instances which match the given whereClause together with params specified in object array.
     */
    public long getBankHouseholdCalculationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(BankHouseholdCalculation.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bank household calculation)}
    //
    @Override
    public Collection<BankHouseholdCalculation> getByBank(BrokerageBank bank) {
        // fill parameters into query
        Query query = getPersistenceManager().newNamedQuery(BankHouseholdCalculation.class, "findByBrokerageBank");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bank", bank);

        // execute query
        @SuppressWarnings("unchecked")
        Collection<BankHouseholdCalculation> queryResult = (Collection<BankHouseholdCalculation>) query.executeWithMap(params);
        return queryResult;
    }

    @Override
    public Collection<BankHouseholdCalculation> findByBrokerageBankAndType(BrokerageBank bank, HouseholdCalculationType householdCalculationType) {
        // fill parameters into query
        Query query = getPersistenceManager().newNamedQuery(BankHouseholdCalculation.class, "findByBrokerageBankAndType");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("bank", bank);
        params.put("type", householdCalculationType);

        // execute query
        @SuppressWarnings("unchecked")
        Collection<BankHouseholdCalculation> queryResult = (Collection<BankHouseholdCalculation>) query.executeWithMap(params);
        return queryResult;
    }

    @Override
    public BankHouseholdCalculation getByBankAndHhc(BrokerageBank bank, HouseholdCalculation hhc) {
        List<OqlTerm> terms = new ArrayList<OqlTerm>();
        terms.add(OqlTerm.newTerm().equals("_brokerageBank._id", bank.getId()));
        terms.add(OqlTerm.newTerm().equals("_householdCalculation._id", hhc.getId()));

        OqlTerm term = OqlTerm.newTerm().and(terms.toArray(new OqlTerm[terms.size()]));

        Collection<BankHouseholdCalculation> result = findEntities(BankHouseholdCalculation.class, term.toString());
        if (result!=null && result.size()>0){
            return result.iterator().next();
        }
        return null;
    }

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
