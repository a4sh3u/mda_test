package de.smava.webapp.applicant.brokerage.domain.interfaces;


import de.smava.webapp.applicant.brokerage.domain.*;
import de.smava.webapp.brokerage.domain.BucketType;

import java.util.Set;


/**
 * The domain object that represents 'BrokerageBanks'.
 *
 * @author generator
 */
public interface BrokerageBankEntityInterface {

    /**
     * Setter for the property 'marketing partner id'.
     *
     * 
     *
     */
    void setMarketingPartnerId(Long marketingPartnerId);

    /**
     * Returns the property 'marketing partner id'.
     *
     * 
     *
     */
    Long getMarketingPartnerId();
    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'valid'.
     *
     * 
     *
     */
    void setValid(boolean valid);

    /**
     * Returns the property 'valid'.
     *
     * 
     *
     */
    boolean getValid();
    /**
     * Setter for the property 'comparison only'.
     *
     * 
     *
     */
    void setComparisonOnly(boolean comparisonOnly);

    /**
     * Returns the property 'comparison only'.
     *
     * 
     *
     */
    boolean getComparisonOnly();
    /**
     * Setter for the property 'send in offset'.
     *
     * 
     *
     */
    void setSendInOffset(Integer sendInOffset);

    /**
     * Returns the property 'send in offset'.
     *
     * 
     *
     */
    Integer getSendInOffset();
    /**
     * Setter for the property 'filter'.
     *
     * 
     *
     */
    void setFilter(Set<BrokerageBankFilter> filter);

    /**
     * Returns the property 'filter'.
     *
     * 
     *
     */
    Set<BrokerageBankFilter> getFilter();
    /**
     * Setter for the property 'configuration'.
     *
     * 
     *
     */
    void setConfiguration(de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration configuration);

    /**
     * Returns the property 'configuration'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration getConfiguration();
    /**
     * Setter for the property 'mappings'.
     *
     * 
     *
     */
    void setMappings(Set<BrokerageMapping> mappings);

    /**
     * Returns the property 'mappings'.
     *
     * 
     *
     */
    Set<BrokerageMapping> getMappings();
    /**
     * Setter for the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    void setBrokerageUploadedDocs(Set<BrokerageUploadedDoc> brokerageUploadedDocs);

    /**
     * Returns the property 'brokerage uploaded docs'.
     *
     * 
     *
     */
    Set<BrokerageUploadedDoc> getBrokerageUploadedDocs();
    /**
     * Setter for the property 'brokerage bank details'.
     *
     * 
     *
     */
    void setBrokerageBankDetails(Set<BrokerageBankDetail> brokerageBankDetails);

    /**
     * Returns the property 'brokerage bank details'.
     *
     * 
     *
     */
    Set<BrokerageBankDetail> getBrokerageBankDetails();
    /**
     * Setter for the property 'bucket type'.
     *
     * 
     *
     */
    void setBucketType(BucketType bucketType);

    /**
     * Returns the property 'bucket type'.
     *
     * 
     *
     */
    BucketType getBucketType();
    
    /**
     * Setter for the property 'status update'.
     */
    void setStatusUpdate(Set<de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod> statusUpdate);

    /**
     * Returns the property 'status update '.
     */
    String getStatusUpdateRaw();

    /**
     * Returns the property 'status update '.
     */
    Set<de.smava.webapp.applicant.brokerage.domain.BrokerageBankStatusUpdateMethod> getStatusUpdate();

    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageBank asBrokerageBank();
}
