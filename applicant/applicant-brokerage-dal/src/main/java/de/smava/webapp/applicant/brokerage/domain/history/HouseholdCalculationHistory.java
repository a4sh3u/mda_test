package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractHouseholdCalculation;




/**
 * The domain object that has all history aggregation related fields for 'HouseholdCalculations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class HouseholdCalculationHistory extends AbstractHouseholdCalculation {

    protected transient de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient String _mappingExpressionInitVal;
    protected transient boolean _mappingExpressionIsSet;



    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType typeInitVal() {
        de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'name'.
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName nameInitVal() {
        de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'mapping expression'.
     */
    public String mappingExpressionInitVal() {
        String result;
        if (_mappingExpressionIsSet) {
            result = _mappingExpressionInitVal;
        } else {
            result = getMappingExpression();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'mapping expression'.
     */
    public boolean mappingExpressionIsDirty() {
        return !valuesAreEqual(mappingExpressionInitVal(), getMappingExpression());
    }

    /**
     * Returns true if the setter method was called for the property 'mapping expression'.
     */
    public boolean mappingExpressionIsSet() {
        return _mappingExpressionIsSet;
    }

}
