package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageMappingConverter;


/**
 * The domain object that represents 'BrokerageMappingConverters'.
 *
 * @author generator
 */
public interface BrokerageMappingConverterEntityInterface {

    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.brokerage.type.ConverterType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.ConverterType getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageMappingConverter asBrokerageMappingConverter();
}
