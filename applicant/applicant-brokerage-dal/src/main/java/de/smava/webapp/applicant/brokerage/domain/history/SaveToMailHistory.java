package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractSaveToMail;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'SaveToMails'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SaveToMailHistory extends AbstractSaveToMail {

    protected transient String _registrationRouteInitVal;
    protected transient boolean _registrationRouteIsSet;
    protected transient String _currentPageInitVal;
    protected transient boolean _currentPageIsSet;
    protected transient Date _mailTriggeredInitVal;
    protected transient boolean _mailTriggeredIsSet;


	
    /**
     * Returns the initial value of the property 'registration route'.
     */
    public String registrationRouteInitVal() {
        String result;
        if (_registrationRouteIsSet) {
            result = _registrationRouteInitVal;
        } else {
            result = getRegistrationRoute();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'registration route'.
     */
    public boolean registrationRouteIsDirty() {
        return !valuesAreEqual(registrationRouteInitVal(), getRegistrationRoute());
    }

    /**
     * Returns true if the setter method was called for the property 'registration route'.
     */
    public boolean registrationRouteIsSet() {
        return _registrationRouteIsSet;
    }
	
    /**
     * Returns the initial value of the property 'current page'.
     */
    public String currentPageInitVal() {
        String result;
        if (_currentPageIsSet) {
            result = _currentPageInitVal;
        } else {
            result = getCurrentPage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'current page'.
     */
    public boolean currentPageIsDirty() {
        return !valuesAreEqual(currentPageInitVal(), getCurrentPage());
    }

    /**
     * Returns true if the setter method was called for the property 'current page'.
     */
    public boolean currentPageIsSet() {
        return _currentPageIsSet;
    }
		
    /**
     * Returns the initial value of the property 'mail triggered'.
     */
    public Date mailTriggeredInitVal() {
        Date result;
        if (_mailTriggeredIsSet) {
            result = _mailTriggeredInitVal;
        } else {
            result = getMailTriggered();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'mail triggered'.
     */
    public boolean mailTriggeredIsDirty() {
        return !valuesAreEqual(mailTriggeredInitVal(), getMailTriggered());
    }

    /**
     * Returns true if the setter method was called for the property 'mail triggered'.
     */
    public boolean mailTriggeredIsSet() {
        return _mailTriggeredIsSet;
    }

}
