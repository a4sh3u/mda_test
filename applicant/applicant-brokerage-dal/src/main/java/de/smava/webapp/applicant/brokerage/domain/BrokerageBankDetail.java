//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank detail)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageBankDetailHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBankDetails'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBankDetail extends BrokerageBankDetailHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bank detail)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBank _brokerageBank;
        protected de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName _name;
        protected String _value;
        
                                    
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageBankDetailName getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    public void setValue(String value) {
        if (!_valueIsSet) {
            _valueIsSet = true;
            _valueInitVal = getValue();
        }
        registerChange("value", _valueInitVal, value);
        _value = value;
    }
                        
    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    public String getValue() {
        return _value;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerage bank : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBankDetail.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _value=").append(_value);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBankDetail.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBankDetail asBrokerageBankDetail() {
        return this;
    }
}
