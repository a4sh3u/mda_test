//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage uploaded doc)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageUploadedDocHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageUploadedDocs'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageUploadedDoc extends BrokerageUploadedDocHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage uploaded doc)}
    public void setReceived(){
        setStatus(BrokerageUploadedDocStatus.RECEIVED);
        setReceiveDate(new Date());
    }
    // !!!!!!!! End of insert code section !!!!!!!!

        protected CustomerUploadedDoc _document;
        protected BrokerageBank _brokerageBank;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus _status;
        protected Date _sendDate;
        protected Date _receiveDate;
        protected Date _deletionDate;
        protected Long _advisor;
        protected Long _concatenatedDocumentId;
        protected Boolean _approved;
        
                                    
    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    public void setDocument(CustomerUploadedDoc document) {
        _document = document;
    }
            
    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    public CustomerUploadedDoc getDocument() {
        return _document;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    public void setStatus(de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus status) {
        if (!_statusIsSet) {
            _statusIsSet = true;
            _statusInitVal = getStatus();
        }
        registerChange("status", _statusInitVal, status);
        _status = status;
    }
                        
    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus getStatus() {
        return _status;
    }
                                    /**
     * Setter for the property 'send date'.
     *
     * 
     *
     */
    public void setSendDate(Date sendDate) {
        if (!_sendDateIsSet) {
            _sendDateIsSet = true;
            _sendDateInitVal = getSendDate();
        }
        registerChange("send date", _sendDateInitVal, sendDate);
        _sendDate = sendDate;
    }
                        
    /**
     * Returns the property 'send date'.
     *
     * 
     *
     */
    public Date getSendDate() {
        return _sendDate;
    }
                                    /**
     * Setter for the property 'receive date'.
     *
     * 
     *
     */
    public void setReceiveDate(Date receiveDate) {
        if (!_receiveDateIsSet) {
            _receiveDateIsSet = true;
            _receiveDateInitVal = getReceiveDate();
        }
        registerChange("receive date", _receiveDateInitVal, receiveDate);
        _receiveDate = receiveDate;
    }
                        
    /**
     * Returns the property 'receive date'.
     *
     * 
     *
     */
    public Date getReceiveDate() {
        return _receiveDate;
    }
                                    /**
     * Setter for the property 'deletion date'.
     *
     * 
     *
     */
    public void setDeletionDate(Date deletionDate) {
        if (!_deletionDateIsSet) {
            _deletionDateIsSet = true;
            _deletionDateInitVal = getDeletionDate();
        }
        registerChange("deletion date", _deletionDateInitVal, deletionDate);
        _deletionDate = deletionDate;
    }
                        
    /**
     * Returns the property 'deletion date'.
     *
     * 
     *
     */
    public Date getDeletionDate() {
        return _deletionDate;
    }
                                            
    /**
     * Setter for the property 'advisor'.
     *
     * 
     *
     */
    public void setAdvisor(Long advisor) {
        _advisor = advisor;
    }
            
    /**
     * Returns the property 'advisor'.
     *
     * 
     *
     */
    public Long getAdvisor() {
        return _advisor;
    }
                                            
    /**
     * Setter for the property 'concatenated document id'.
     *
     * 
     *
     */
    public void setConcatenatedDocumentId(Long concatenatedDocumentId) {
        _concatenatedDocumentId = concatenatedDocumentId;
    }
            
    /**
     * Returns the property 'concatenated document id'.
     *
     * 
     *
     */
    public Long getConcatenatedDocumentId() {
        return _concatenatedDocumentId;
    }
                                    /**
     * Setter for the property 'approved'.
     *
     * 
     *
     */
    public void setApproved(Boolean approved) {
        if (!_approvedIsSet) {
            _approvedIsSet = true;
            _approvedInitVal = getApproved();
        }
        registerChange("approved", _approvedInitVal, approved);
        _approved = approved;
    }
                        
    /**
     * Returns the property 'approved'.
     *
     * 
     *
     */
    public Boolean getApproved() {
        return _approved;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_document instanceof de.smava.webapp.commons.domain.Entity && !_document.getChangeSet().isEmpty()) {
             for (String element : _document.getChangeSet()) {
                 result.add("document : " + element);
             }
         }

         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerage bank : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageUploadedDoc.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _status=").append(_status);
            builder.append("\n}");
        } else {
            builder.append(BrokerageUploadedDoc.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageUploadedDoc asBrokerageUploadedDoc() {
        return this;
    }
}
