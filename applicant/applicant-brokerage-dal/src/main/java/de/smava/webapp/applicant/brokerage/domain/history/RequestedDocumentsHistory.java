package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractRequestedDocuments;




/**
 * The domain object that has all history aggregation related fields for 'RequestedDocumentss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RequestedDocumentsHistory extends AbstractRequestedDocuments {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient Integer _numberOfDocumentsInitVal;
    protected transient boolean _numberOfDocumentsIsSet;
    protected transient boolean _requiredInitVal;
    protected transient boolean _requiredIsSet;
    protected transient de.smava.webapp.applicant.brokerage.domain.DocumentCategory _typeInitVal;
    protected transient boolean _typeIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of documents'.
     */
    public Integer numberOfDocumentsInitVal() {
        Integer result;
        if (_numberOfDocumentsIsSet) {
            result = _numberOfDocumentsInitVal;
        } else {
            result = getNumberOfDocuments();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of documents'.
     */
    public boolean numberOfDocumentsIsDirty() {
        return !valuesAreEqual(numberOfDocumentsInitVal(), getNumberOfDocuments());
    }

    /**
     * Returns true if the setter method was called for the property 'number of documents'.
     */
    public boolean numberOfDocumentsIsSet() {
        return _numberOfDocumentsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'required'.
     */
    public boolean requiredInitVal() {
        boolean result;
        if (_requiredIsSet) {
            result = _requiredInitVal;
        } else {
            result = getRequired();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'required'.
     */
    public boolean requiredIsDirty() {
        return !valuesAreEqual(requiredInitVal(), getRequired());
    }

    /**
     * Returns true if the setter method was called for the property 'required'.
     */
    public boolean requiredIsSet() {
        return _requiredIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.brokerage.domain.DocumentCategory typeInitVal() {
        de.smava.webapp.applicant.brokerage.domain.DocumentCategory result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
	
}
