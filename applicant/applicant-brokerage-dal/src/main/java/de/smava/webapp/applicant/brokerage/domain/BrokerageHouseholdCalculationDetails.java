//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage household calculation details)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageHouseholdCalculationDetailsHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageHouseholdCalculationDetailss'.
 *
 * Partial results for household calculation
 *
 * @author generator
 */
public class BrokerageHouseholdCalculationDetails extends BrokerageHouseholdCalculationDetailsHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage household calculation details)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected BrokerageHouseholdCalculation _brokerageHouseholdCalculation;
        protected de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType _type;
        protected de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName _name;
        protected Double _firstApplicantValue;
        protected Double _secondApplicantValue;
        
                                    
    /**
     * Setter for the property 'brokerage household calculation'.
     *
     * 
     *
     */
    public void setBrokerageHouseholdCalculation(BrokerageHouseholdCalculation brokerageHouseholdCalculation) {
        _brokerageHouseholdCalculation = brokerageHouseholdCalculation;
    }
            
    /**
     * Returns the property 'brokerage household calculation'.
     *
     * 
     *
     */
    public BrokerageHouseholdCalculation getBrokerageHouseholdCalculation() {
        return _brokerageHouseholdCalculation;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'first applicant value'.
     *
     * 
     *
     */
    public void setFirstApplicantValue(Double firstApplicantValue) {
        if (!_firstApplicantValueIsSet) {
            _firstApplicantValueIsSet = true;
            _firstApplicantValueInitVal = getFirstApplicantValue();
        }
        registerChange("first applicant value", _firstApplicantValueInitVal, firstApplicantValue);
        _firstApplicantValue = firstApplicantValue;
    }
                        
    /**
     * Returns the property 'first applicant value'.
     *
     * 
     *
     */
    public Double getFirstApplicantValue() {
        return _firstApplicantValue;
    }
                                    /**
     * Setter for the property 'second applicant value'.
     *
     * 
     *
     */
    public void setSecondApplicantValue(Double secondApplicantValue) {
        if (!_secondApplicantValueIsSet) {
            _secondApplicantValueIsSet = true;
            _secondApplicantValueInitVal = getSecondApplicantValue();
        }
        registerChange("second applicant value", _secondApplicantValueInitVal, secondApplicantValue);
        _secondApplicantValue = secondApplicantValue;
    }
                        
    /**
     * Returns the property 'second applicant value'.
     *
     * 
     *
     */
    public Double getSecondApplicantValue() {
        return _secondApplicantValue;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageHouseholdCalculation instanceof de.smava.webapp.commons.domain.Entity && !_brokerageHouseholdCalculation.getChangeSet().isEmpty()) {
             for (String element : _brokerageHouseholdCalculation.getChangeSet()) {
                 result.add("brokerage household calculation : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageHouseholdCalculationDetails.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(BrokerageHouseholdCalculationDetails.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageHouseholdCalculationDetails asBrokerageHouseholdCalculationDetails() {
        return this;
    }
}
