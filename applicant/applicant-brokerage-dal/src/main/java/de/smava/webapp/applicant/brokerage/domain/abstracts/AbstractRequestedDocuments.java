//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(requested documents)}

import de.smava.webapp.applicant.brokerage.domain.RequestedDocuments;
import de.smava.webapp.applicant.brokerage.domain.interfaces.RequestedDocumentsEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RequestedDocumentss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractRequestedDocuments
    extends BrokerageEntity    implements RequestedDocumentsEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRequestedDocuments.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof RequestedDocuments)) {
            return;
        }
        
        this.setName(((RequestedDocuments) oldEntity).getName());    
        this.setNumberOfDocuments(((RequestedDocuments) oldEntity).getNumberOfDocuments());    
        this.setRequired(((RequestedDocuments) oldEntity).getRequired());    
        this.setType(((RequestedDocuments) oldEntity).getType());    
        this.setDocumentConfiguration(((RequestedDocuments) oldEntity).getDocumentConfiguration());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof RequestedDocuments)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((RequestedDocuments) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getNumberOfDocuments(), ((RequestedDocuments) otherEntity).getNumberOfDocuments());    
        equals = equals && valuesAreEqual(this.getRequired(), ((RequestedDocuments) otherEntity).getRequired());    
        equals = equals && valuesAreEqual(this.getType(), ((RequestedDocuments) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getDocumentConfiguration(), ((RequestedDocuments) otherEntity).getDocumentConfiguration());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(requested documents)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

