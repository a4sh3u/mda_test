package de.smava.webapp.applicant.brokerage.domain.history;

import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageApplicationDocument;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentDispatchType;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentState;

/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationDocuments'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author Adam Tomecki
 * @since 26.10.2017
 */
public abstract class BrokerageApplicationDocumentHistory extends AbstractBrokerageApplicationDocument {

    protected transient boolean _stateIsSet;
    protected transient BrokerageApplicationDocumentState _stateInitVal;
    protected transient boolean _dispatchTypeIsSet;
    protected transient BrokerageApplicationDocumentDispatchType _dispatchTypeInitVal;

    /**
     * Returns the initial value of the property 'state'.
     */
    public BrokerageApplicationDocumentState stateInitVal() {
        BrokerageApplicationDocumentState result;

        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }

        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }

    /**
     * Returns the initial value of the property 'dispatch type'.
     */
    public BrokerageApplicationDocumentDispatchType dispatchTypeInitVal() {
        BrokerageApplicationDocumentDispatchType result;

        if (_dispatchTypeIsSet) {
            result = _dispatchTypeInitVal;
        } else {
            result = getDispatchType();
        }

        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'dispatch type'.
     */
    public boolean dispatchTypeIsDirty() {
        return !valuesAreEqual(dispatchTypeInitVal(), getDispatchType());
    }

    /**
     * Returns true if the setter method was called for the property 'dispatch type'.
     */
    public boolean dispatchTypeIsSet() {
        return _dispatchTypeIsSet;
    }
}
