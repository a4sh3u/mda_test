package de.smava.webapp.applicant.brokerage.type;

public enum Category {

    STANDARD,
    /**
     * Sonstiges
     */
    OTHER,

    /**
     * Auto / Motorrad
     */
    CAR,

    /**
     * Wohnen
     */
    HOME,

    /**
     * Modernisieung am eigenen Objekt
     */
    HOME_MODERNISATION,

    /**
     * An-Umbau am eigenen Objekt
     */
    HOME_RENOVATION,

    /**
     * Altersgerechter Umbau am eigenen Objekt
     */
    HOME_RENOVATION_AGE,

    /**
     * Energetische Maßnahme am eigenen Objekt
     */
    HOME_RENOVATION_ENERGY,

    /**
     * Anbau Garage/Carport am eigenen Objekt
     */
    HOME_ADDITION_GARAGE_CARPORT,

    /**
     * Umschuldung / Ablösung
     */
    CONSOLIDATION,

    /**
     * Gewerbe
     */
    TRADE,

    /**
     * "Targobank Sonderkontingent"
     */
    TARGOBANK_SPECIAL,

    /**
     * "Antiquitäten & Kunst"
     */    
    ANTIQUES,

    /**
     * "Aus- & Weiterbildung"
     */
    EDUCATION,

    /**
     * "Betriebsmittelinvestition / Investition"
     */
    INVESTMENTS,

    /**
     * "Familie & Erziehung"
     */
    FAMILY,

    /**
     * "Feierlichkeiten & besondere Anlässe"
     */
    CELEBRATION,

    /**
     * "Freie Verwendung"
     */
    FREE_USAGE,

    /**
     * "Gemeinnützige Projekte"
     */
    COMMUNITY_SERVICE,

    /**
     * "Gesundheit & Lifestyle"
     */
    HEALTH,

    /**
     * "Gewerblicher Kreditbedarf"
     */
    COMMERCIAL_CREDIT,

    /**
     * "Liquidität"
     */
    COMPENSATE_OVERDRAFT,

    /**
     * "Reisen / Urlaub"
     */
    TRAVEL,

    /**
     * "Sammeln & Seltenes"
     */
    FREE_USE,

    /**
     * "Unterhaltungselektronik & Technik"
     */
    PC_AUDIO_VIDEO,

    /**
     * "Sport & Freizeit"
     */
    MAINTENANCE,

    /**
     * "Tierwelt"
     */
    WILDLIFE,

    /**
     * "Ballonfinanzierung"
     */
    BALLOON_FINANCING,

    /**
     * "Rahmenkredit"
     */
    GLOBAL_CREDIT;

}
