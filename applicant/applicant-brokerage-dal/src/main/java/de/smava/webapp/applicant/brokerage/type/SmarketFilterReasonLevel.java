package de.smava.webapp.applicant.brokerage.type;


public enum SmarketFilterReasonLevel {

    BROKERAGE_APPLICATION,
    SECOND_APPLICANT
}
