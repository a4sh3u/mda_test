package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.BrokerageBankMaintenanceDao;
import de.smava.webapp.applicant.brokerage.domain.BrokerageBankMaintenance;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * @author Jakub Janus
 * @since 02.08.2017
 */
@Repository(value = "applicantBrokerageBankMaintenanceDao")
public class JdoBrokerageBankMaintenanceDao extends JdoBaseDao implements BrokerageBankMaintenanceDao {

    private static final Logger LOGGER = Logger.getLogger(JdoBrokerageBankMaintenanceDao.class);

    @Override
    public BrokerageBankMaintenance load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("BrokerageBankMaintenance load for id " + id);
        }

        BrokerageBankMaintenance result = getEntity(BrokerageBankMaintenance.class, id);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("BrokerageBankMaintenance load result " + result);
        }

        return result;
    }

    @Override
    public boolean exists(Long id) {
        boolean result = false;

        if (id != null) {
            BrokerageBankMaintenance entity = findUniqueEntity(BrokerageBankMaintenance.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }

        return result;
    }

    @Override
    public Long save(BrokerageBankMaintenance brokerageBankMaintenance) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveBrokerageBankMaintenance: " + brokerageBankMaintenance);
        }

        return saveEntity(brokerageBankMaintenance);
    }

    @Override
    public Collection<BrokerageBankMaintenance> getBrokerageBankMaintenanceList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBrokerageBankMaintenanceList start");
        }

        Collection<BrokerageBankMaintenance> result = getEntities(BrokerageBankMaintenance.class);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBrokerageBankMaintenanceList size is " + result.size());
        }

        return result;
    }
}
