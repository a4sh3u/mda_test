//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import de.smava.webapp.applicant.account.domain.Document;
import de.smava.webapp.applicant.account.domain.DocumentContainer;
import de.smava.webapp.applicant.brokerage.additionaldata.AdditionalData;
import de.smava.webapp.applicant.brokerage.additionaldata.AdditionalDataFactory;
import de.smava.webapp.applicant.brokerage.domain.AdditionalBrokerageData;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationRequestData;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageApplicationEntityInterface;
import de.smava.webapp.applicant.brokerage.exception.BrokerageAdditionalDataException;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.applicant.brokerage.type.RdiType;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.brokerage.util.MappingUtils;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.StringWriter;
import java.util.List;
import java.util.StringTokenizer;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplications'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 *
 *
 * @author generator
 */
public abstract class AbstractBrokerageApplication
    extends BrokerageEntity    implements BrokerageApplicationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageApplication.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageApplication)) {
            return;
        }

        this.setCreationDate(CurrentDate.getDate());
        this.setBrokerageBank(((BrokerageApplication) oldEntity).getBrokerageBank());
        this.setExtRefNumber(((BrokerageApplication) oldEntity).getExtRefNumber());
        this.setPrevExtRefNumber(((BrokerageApplication) oldEntity).getPrevExtRefNumber());
        this.setState(((BrokerageApplication) oldEntity).getState());
        this.setAccount(((BrokerageApplication) oldEntity).getAccount());
        this.setFirstApplicant(((BrokerageApplication) oldEntity).getFirstApplicant());
        this.setSecondApplicant(((BrokerageApplication) oldEntity).getSecondApplicant());
        this.setReferenceBankAccount(((BrokerageApplication) oldEntity).getReferenceBankAccount());
        this.setRequestedRdiType(((BrokerageApplication) oldEntity).getRequestedRdiType());
        this.setRequestedAmount(((BrokerageApplication) oldEntity).getRequestedAmount());
        this.setRequestedDuration(((BrokerageApplication) oldEntity).getRequestedDuration());
        this.setMonthlyRate(((BrokerageApplication) oldEntity).getMonthlyRate());
        this.setEffectiveInterest(((BrokerageApplication) oldEntity).getEffectiveInterest());
        this.setAmount(((BrokerageApplication) oldEntity).getAmount());
        this.setDuration(((BrokerageApplication) oldEntity).getDuration());
        this.setRdiType(((BrokerageApplication) oldEntity).getRdiType());
        this.setBrokerageApplicationFilterReason(((BrokerageApplication) oldEntity).getBrokerageApplicationFilterReason());
        this.setSecondApplicantFilterReason(((BrokerageApplication) oldEntity).getSecondApplicantFilterReason());
        this.setLoanApplication(((BrokerageApplication) oldEntity).getLoanApplication());
        this.setBrokerageApplicationRequestDatas(((BrokerageApplication) oldEntity).getBrokerageApplicationRequestDatas());
        this.setCategory(((BrokerageApplication) oldEntity).getCategory());
        this.setPapSaleTrackedDate(((BrokerageApplication) oldEntity).getPapSaleTrackedDate());
        this.setLastStateRequest(((BrokerageApplication) oldEntity).getLastStateRequest());
        this.setLastStateChange(((BrokerageApplication) oldEntity).getLastStateChange());
        this.setDocumentContainer(((BrokerageApplication) oldEntity).getDocumentContainer());
        this.setEmailCreated(((BrokerageApplication) oldEntity).getEmailCreated());
        this.setEmailType(((BrokerageApplication) oldEntity).getEmailType());
        this.setDocumentsRequested(((BrokerageApplication) oldEntity).getDocumentsRequested());
        this.setDocumentsSent(((BrokerageApplication) oldEntity).getDocumentsSent());
        this.setAdditionalData(((BrokerageApplication) oldEntity).getAdditionalData());
        this.setHouseholdCalculation(((BrokerageApplication) oldEntity).getHouseholdCalculation());
        this.setPayoutDate(((BrokerageApplication) oldEntity).getPayoutDate());
        this.setSecondApplicantAllowed(((BrokerageApplication) oldEntity).isSecondApplicantAllowed());
        this.setBrokerageFilterReasonMultiValues(((BrokerageApplication) oldEntity).getBrokerageFilterReasonMultiValues());
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageApplication)) {
            equals = false;
        }


        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((BrokerageApplication) otherEntity).getBrokerageBank());
        equals = equals && valuesAreEqual(this.getExtRefNumber(), ((BrokerageApplication) otherEntity).getExtRefNumber());
        equals = equals && valuesAreEqual(this.getPrevExtRefNumber(), ((BrokerageApplication) otherEntity).getPrevExtRefNumber());
        equals = equals && valuesAreEqual(this.getState(), ((BrokerageApplication) otherEntity).getState());
        equals = equals && valuesAreEqual(this.getAccount(), ((BrokerageApplication) otherEntity).getAccount());
        equals = equals && valuesAreEqual(this.getFirstApplicant(), ((BrokerageApplication) otherEntity).getFirstApplicant());
        equals = equals && valuesAreEqual(this.getSecondApplicant(), ((BrokerageApplication) otherEntity).getSecondApplicant());
        equals = equals && valuesAreEqual(this.getReferenceBankAccount(), ((BrokerageApplication) otherEntity).getReferenceBankAccount());
        equals = equals && valuesAreEqual(this.getRequestedRdiType(), ((BrokerageApplication) otherEntity).getRequestedRdiType());
        equals = equals && valuesAreEqual(this.getRequestedAmount(), ((BrokerageApplication) otherEntity).getRequestedAmount());
        equals = equals && valuesAreEqual(this.getRequestedDuration(), ((BrokerageApplication) otherEntity).getRequestedDuration());
        equals = equals && valuesAreEqual(this.getMonthlyRate(), ((BrokerageApplication) otherEntity).getMonthlyRate());
        equals = equals && valuesAreEqual(this.getEffectiveInterest(), ((BrokerageApplication) otherEntity).getEffectiveInterest());
        equals = equals && valuesAreEqual(this.getAmount(), ((BrokerageApplication) otherEntity).getAmount());
        equals = equals && valuesAreEqual(this.getDuration(), ((BrokerageApplication) otherEntity).getDuration());
        equals = equals && valuesAreEqual(this.getRdiType(), ((BrokerageApplication) otherEntity).getRdiType());
        equals = equals && valuesAreEqual(this.getBrokerageApplicationFilterReason(), ((BrokerageApplication) otherEntity).getBrokerageApplicationFilterReason());
        equals = equals && valuesAreEqual(this.getSecondApplicantFilterReason(), ((BrokerageApplication) otherEntity).getSecondApplicantFilterReason());
        equals = equals && valuesAreEqual(this.getLoanApplication(), ((BrokerageApplication) otherEntity).getLoanApplication());
        equals = equals && valuesAreEqual(this.getBrokerageApplicationRequestDatas(), ((BrokerageApplication) otherEntity).getBrokerageApplicationRequestDatas());
        equals = equals && valuesAreEqual(this.getCategory(), ((BrokerageApplication) otherEntity).getCategory());
        equals = equals && valuesAreEqual(this.getPapSaleTrackedDate(), ((BrokerageApplication) otherEntity).getPapSaleTrackedDate());
        equals = equals && valuesAreEqual(this.getLastStateRequest(), ((BrokerageApplication) otherEntity).getLastStateRequest());
        equals = equals && valuesAreEqual(this.getLastStateChange(), ((BrokerageApplication) otherEntity).getLastStateChange());
        equals = equals && valuesAreEqual(this.getDocumentContainer(), ((BrokerageApplication) otherEntity).getDocumentContainer());
        equals = equals && valuesAreEqual(this.getEmailCreated(), ((BrokerageApplication) otherEntity).getEmailCreated());
        equals = equals && valuesAreEqual(this.getEmailType(), ((BrokerageApplication) otherEntity).getEmailType());
        equals = equals && valuesAreEqual(this.getDocumentsRequested(), ((BrokerageApplication) otherEntity).getDocumentsRequested());
        equals = equals && valuesAreEqual(this.getDocumentsSent(), ((BrokerageApplication) otherEntity).getDocumentsSent());
        equals = equals && valuesAreEqual(this.getAdditionalData(), ((BrokerageApplication) otherEntity).getAdditionalData());
        equals = equals && valuesAreEqual(this.getHouseholdCalculation(), ((BrokerageApplication) otherEntity).getHouseholdCalculation());
        equals = equals && valuesAreEqual(this.getPayoutDate(), ((BrokerageApplication) otherEntity).getPayoutDate());
        equals = equals && valuesAreEqual(this.isSecondApplicantAllowed(), ((BrokerageApplication) otherEntity).isSecondApplicantAllowed());
        equals = equals && valuesAreEqual(this.getBrokerageFilterReasonMultiValues(), ((BrokerageApplication) otherEntity).getBrokerageFilterReasonMultiValues());

        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage application)}

    /**
     * Returns the latest BrokerageApplicationRequestData of the given request type.
     * @param type
     * @return
     */
    @JsonIgnore
    public BrokerageApplicationRequestData getLatestBard(BrokerageApplicationRequestType type) {

        BrokerageApplicationRequestData latestBard = null;

        for ( BrokerageApplicationRequestData bard : getBrokerageApplicationRequestDatas()) {
            if (type != null && !type.equals(bard.getType())) {
                continue;
            }

            if( latestBard == null ) {
                latestBard = bard;
                continue;
            }

            if ( bard.getCreationDate().after( latestBard.getCreationDate() ) ) {
                latestBard = bard;
            }
        }

        return latestBard;
    }

    /**
     * Returns the first BrokerageApplicationRequestData that had the given state
     * @param state
     * @return
     */
    @JsonIgnore
    public BrokerageApplicationRequestData getFirstBard(BrokerageState state) {

        BrokerageApplicationRequestData firstBard = null;

        for ( BrokerageApplicationRequestData bard : getBrokerageApplicationRequestDatas()) {
            if (state != null && !state.equals(bard.getState())) {
                continue;
            }

            if( firstBard == null || ( bard.getCreationDate().before( firstBard.getCreationDate() ) )) {
                firstBard = bard;
            }
        }

        return firstBard;
    }

    /**
     * Thats a shortcut for getFirstBard(BrokerageState.APPLICATION_APPLIED) required for evaluation
     * within the rule engine acceptance rule.
     * @return
     */
    @JsonIgnore
    public BrokerageApplicationRequestData getFirstBardApplied() {
        return getFirstBard(BrokerageState.APPLICATION_APPLIED);
    }

    /**
     * Returns the XML stream of the latest bard of type TYPE_APPLICATION_REQUEST
     * @return
     */
    @JsonIgnore
    public String getLatestXMLResponse() {
        BrokerageApplicationRequestData bard = this.getLatestBard(BrokerageApplicationRequestType.TYPE_APPLICATION_REQUEST);

        String xmlResponse = "No XML response found!";

        if ( bard != null && !StringUtils.isEmpty(bard.getResponse())) {
            xmlResponse = bard.getResponse();
        }

        return xmlResponse;
    };

    @JsonIgnore
    public int getRemainingDurationOfPayedoutCredit(){

        if ( getPayoutDate() != null && getDuration() != null){

            return getDuration() -  MappingUtils.getNumberOfMonthsOfPeriod(getPayoutDate(), CurrentDate.getDate());
        }
        return -1;
    }

    /* ########### begin of brokerage sending ################
     *
     * transient properties for mapping handling
     * use them with caution else where!!!
     * */
    private transient BrokerageApplicationRequestData _currentRequestData;

    public final BrokerageApplicationRequestData getCurrentRequestData() {
        return _currentRequestData;
    }

    public final void setCurrentRequestData(
            BrokerageApplicationRequestData currentRequestData) {
        _currentRequestData = currentRequestData;
        if ( currentRequestData != null) {
//			_brokerageApplicationRequestDatas.add(currentRequestData);
        }
    }

    @JsonIgnore
    public final boolean hasAttachment() {
        boolean hasAttachments = false;

        if (null != this.getDocumentContainer()
                && CollectionUtils.isNotEmpty(this.getDocumentContainer().getDocuments())
                && DocumentContainer.TYPE_REGISTRATION_COMPILATION.equals(this.getDocumentContainer().getType())) {

            for (Document doc : this.getDocumentContainer().getDocuments()) {
                if (CollectionUtils.isNotEmpty(doc.getAttachments())) {
                    hasAttachments = true;
                    break;
                }
            }
        }

        return hasAttachments;
    }

    /**
     * Setter for the property 'effective interest'.
     */
    public void setEffectiveInterest(Double effectiveInterest, boolean propagate) {
        setEffectiveInterest(effectiveInterest);
        if ( propagate && _currentRequestData != null){
            _currentRequestData.setEffectiveInterest(effectiveInterest);
        }
    }

    /**
     * Setter for the property 'amount'.
     */
    public void setAmount(Double amount, boolean propagate) {
        setAmount(amount);
        if ( propagate && _currentRequestData != null){
            _currentRequestData.setAmount(amount);
        }
    }

    /**
     * Setter for the property 'duration'.
     */
    public void setDuration(Integer duration, boolean propagate) {
        setDuration(duration);
        if ( propagate && _currentRequestData != null){
            _currentRequestData.setDuration(duration);
        }
    }

    /**
     * Setter for the property 'rdi type'.
     */
    public void setRdiType(RdiType rdiType, boolean propagate) {
        setRdiType(rdiType);
        if ( propagate && _currentRequestData != null){
            _currentRequestData.setRdiType(rdiType);
        }
    }


    /**
     * Setter for the property 'monthly rate'.
     */
    public void setMonthlyRate(Double monthlyRate, boolean propagate) {
        setMonthlyRate(monthlyRate);
        if ( propagate && _currentRequestData != null){
            _currentRequestData.setMonthlyRate(monthlyRate);
        }
    }

	/* ########### end of brokerage sending ################ */




    public String getTokenizedExtRefNumber() {
        if (StringUtils.isNotBlank(this.getExtRefNumber())) {
            StringTokenizer refNumberTokenizer = new StringTokenizer(this.getExtRefNumber(), ";");

            if (refNumberTokenizer.countTokens() <= 2) {
                return this.getExtRefNumber();
            } else {
                return refNumberTokenizer.nextToken() + ";" + refNumberTokenizer.nextToken();
            }
        }
        return "";
    }

    /**
     * @deprecated due to the change of Additional Data schema, use {@link #writeAdditionalData(AdditionalData)} instead.
     */
    @Deprecated
    public void writeAdditionalData(AdditionalBrokerageData value) {
        if (value == null){
            setAdditionalData(null);
        } else{
            JsonFactory fac = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            StringWriter out = new StringWriter();

            try {
                // Create pretty printer:
                JsonGenerator gen = fac.createJsonGenerator(out);
                gen.useDefaultPrettyPrinter();

                FilterProvider filters =
                        new SimpleFilterProvider().addFilter(value.determineFilterName(),
                                SimpleBeanPropertyFilter.serializeAllExcept());

                // Now write:
                mapper.writer(filters).writeValue(gen, value);

                setAdditionalData(mapper.writer(filters).writeValueAsString(value));
            } catch (Exception e) {
                LOGGER.info("problem formatting additional data: ", e);
            }
        }
    }

    /**
     * Writes to database additional data serialized from the proper object to JSON string
     *
     * @param additionalData {@link AdditionalData}
     */
    public void writeAdditionalData(AdditionalData additionalData) {
        if (additionalData != null) {
            if (getBrokerageBank().getName().equals(additionalData.retrieveBankName())) {

                setAdditionalData(additionalData.asJson());

            } else {
                LOGGER.error(
                        String.format(
                                "Problem writing additional data for ba (id = %s): object for %s expected, %s given",
                                getId(),
                                getBrokerageBank().getName(),
                                additionalData.retrieveBankName()
                        )
                );
            }
        }
    }

    /**
     * Implementation of the method is quite error prone und should not be used
     * anymore. Use {@link #readAdditionalData(Class)} instead.
     * @param className
     * @return
     */
    @Deprecated
    public AdditionalBrokerageData readAdditionalData( AdditionalBrokerageData className ) {
        return readAdditionalData(className.getClass());
    }

    /**
     * @deprecated due to the change of Additional Data schema, use {@link #writeAdditionalData(AdditionalData)} instead.
     */
    @Deprecated
    public <T extends AdditionalBrokerageData> T readAdditionalData( Class<T> clazz ) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            if (getAdditionalData() != null) {
                return mapper.readValue(getAdditionalData(), clazz);
            } else {
                return clazz.newInstance();
            }
        } catch (Exception e) {
            LOGGER.error("problem reading additional data ba(id="+getId()+"): ", e);
        }

        return null;
    }

    /**
     * Reads additional data from database and deserialize it to the proper object
     *
     * @return {@link AdditionalData}
     */
    public AdditionalData readAdditionalData() {
        ObjectMapper mapper = new ObjectMapper();

        try {
            AdditionalData additionalData = AdditionalDataFactory.create(getBrokerageBank().getName());

            if (additionalData != null && getAdditionalData() != null) {
                return mapper.readValue(getAdditionalData(), additionalData.getClass());
            } else {
                return additionalData;
            }
        } catch (Exception e) {
            LOGGER.error("Problem getting additional data for ba (id = " + getId() + "): ", e);
        }

        return null;
    }

    /**
     * WARNING: It is not possible to set additionalData manually!
     * Use {@link #writeAdditionalData(de.smava.webapp.applicant.brokerage.domain.AdditionalBrokerageData)} instead!
     * @param additionalData
     */
    @Override
    @Deprecated
    public void setAdditionalData(String additionalData) {
        LOGGER.error("Unallowed operation setAdditionalData(AdditionalBrokerageData): Use writeAdditionalData(AdditionalBrokerageData) instead! ",
                new BrokerageAdditionalDataException());
    }

    @JsonIgnore
    public BrokerageApplicationRequestData getLatestBrokerageApplicationRequestDataForType( BrokerageApplicationRequestType type){
        BrokerageApplicationRequestData ret = null;
        for ( BrokerageApplicationRequestData bard : getBrokerageApplicationRequestDatas()) {
            if ( type.equals(bard.getType())){
                if ( ret == null || ( ret.getCreationDate().before(bard.getCreationDate()))){
                    ret = bard;
                }
            }
        }
        return ret;
    }

    /**
     * Returns the latest bard for brokerage application
     *
     * @return the latest brokerage application request data {@link BrokerageApplicationRequestData}
     */
    @JsonIgnore
    public BrokerageApplicationRequestData getLatestBrokerageApplicationRequestData() {
        BrokerageApplicationRequestData ret = null;

        for (BrokerageApplicationRequestData bard : getBrokerageApplicationRequestDatas()) {
            if (ret == null || (ret.getCreationDate().before(bard.getCreationDate()))) {
                ret = bard;
            }
        }

        return ret;
    }

    /**
     * Returns the latest bard with the given type for brokerage application
     *
     * @return the latest brokerage application request data with the given type {@link BrokerageApplicationRequestData}
     */
    @JsonIgnore
    public BrokerageApplicationRequestData getLatestBrokerageApplicationRequestData(List<BrokerageApplicationRequestType> bardTypes) {
        BrokerageApplicationRequestData ret = null;

        for (BrokerageApplicationRequestData bard : getBrokerageApplicationRequestDatas()) {
            if ((ret == null || (ret.getCreationDate().before(bard.getCreationDate())))
                    && bardTypes.contains(bard.getType())) {

                ret = bard;
            }
        }

        return ret;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

