package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * Factory for AdditionalData
 *
 * @author Adam
 * @since 28.04.2016.
 */
public class AdditionalDataFactory {

    /**
     * Private constructor introduced to prevent the initialization of this class from outside
     */
    private AdditionalDataFactory() {

    }

    /**
     * Creates additional data instance for specified bank
     *
     * @param bankName bank for which additional data is created
     * @return new additional data object {@link AdditionalData}
     */
    public static AdditionalData create(final String bankName) {

        if (BrokerageBank.BANK_POSTBANK.equals(bankName)) {
            return new PostbankAdditionalData();
        }

        if (BrokerageBank.BANK_CREDITPLUS.equals(bankName)) {
            return new CreditplusAdditionalData();
        }

        if (BrokerageBank.BANK_PSD.equals(bankName)) {
            return new PsdAdditionalData();
        }

        if (BrokerageBank.BANK_SANTANDER_BESTCREDIT.equals(bankName)) {
            return new BestCreditAdditionalData();
        }

        if (BrokerageBank.BANK_DKB.equals(bankName)) {
            return new DkbAdditionalData();
        }

        if (BrokerageBank.BANK_DSL.equals(bankName)) {
            return new DslAdditionalData();
        }

        if (BrokerageBank.BANK_WUNSCHKREDIT.equals(bankName)) {
            return new WunschkreditAdditionalData();
        }

        if (BrokerageBank.BANK_KREDIT2GO.equals(bankName)) {
            return new Kredit2GoAdditionalData();
        }

        if (BrokerageBank.BANK_SWK.equals(bankName)) {
            return new SwkAdditionalData();
        }

        if (BrokerageBank.BANK_ONLINEKREDIT.equals(bankName)) {
            return new OnlinekreditAdditionalData();
        }

        if (BrokerageBank.BANK_CARCREDIT.equals(bankName)) {
            return new CarCreditAdditionalData();
        }

        if (BrokerageBank.BANK_SKG.equals(bankName)) {
            return new SkgAdditionalData();
        }

        if (BrokerageBank.BANK_ABK.equals(bankName)) {
            return new AbkAdditionalData();
        }

        if (BrokerageBank.BANK_POSTBANK_BUSINESS_CREDIT.equals(bankName)) {
            return new PostbankBusinessCreditAdditionalData();
        }

        if (BrokerageBank.BANK_KREDIT_PRIVAT.equals(bankName)) {
            return new KreditPrivatAdditionalData();
        }

        if (BrokerageBank.BANK_VONESSEN.equals(bankName)) {
            return new VonEssenAdditionalData();
        }

        if (BrokerageBank.BANK_TARGOBANK.equals(bankName)) {
            return new TargobankAdditionalData();
        }

        if (BrokerageBank.BANK_AUXMONEY.equals(bankName)) {
            return new AuxmoneyAdditionalData();
        }

        if (BrokerageBank.BANK_VONESSEN_SUBPRIME.equals(bankName)) {
            return new VonEssenSubprimeAdditionalData();
        }

        if (BrokerageBank.BANK_ADAC.equals(bankName)) {
            return new AdacAdditionalData();
        }

        if (BrokerageBank.BANK_OYAKANKER.equals(bankName)) {
            return new OyakAnkerAdditionalData();
        }

        if (BrokerageBank.BANK_BANKOFSCOTLAND.equals(bankName)) {
            return new BankOfScotlandAdditionalData();
        }

        if (BrokerageBank.BANK_KREDIT2DAY.equals(bankName)) {
            return new Kredit2DayAdditionalData();
        }

        if (BrokerageBank.BANK_INGDIBA.equals(bankName)) {
            return new IngDibaAdditionalData();
        }

        if (BrokerageBank.BANK_BARCLAYS.equals(bankName)) {
            return new BarclaysAdditionalData();
        }

        return null;
    }
}
