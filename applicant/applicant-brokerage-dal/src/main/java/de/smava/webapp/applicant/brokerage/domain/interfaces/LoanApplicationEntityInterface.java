package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.LoanApplication;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'LoanApplications'.
 *
 * @author generator
 */
public interface LoanApplicationEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(de.smava.webapp.applicant.brokerage.type.BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.BrokerageState getState();
    /**
     * Setter for the property 'account'.
     *
     * 
     *
     */
    void setAccount(de.smava.webapp.applicant.account.domain.Account account);

    /**
     * Returns the property 'account'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.account.domain.Account getAccount();
    /**
     * Setter for the property 'applicant relationship'.
     *
     * 
     *
     */
    void setApplicantRelationship(de.smava.webapp.applicant.domain.ApplicantsRelationship applicantRelationship);

    /**
     * Returns the property 'applicant relationship'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.ApplicantsRelationship getApplicantRelationship();
    /**
     * Setter for the property 'requested rdi type'.
     *
     * 
     *
     */
    void setRequestedRdiType(de.smava.webapp.applicant.brokerage.type.RdiType requestedRdiType);

    /**
     * Returns the property 'requested rdi type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.RdiType getRequestedRdiType();
    /**
     * Setter for the property 'requested amount'.
     *
     * 
     *
     */
    void setRequestedAmount(double requestedAmount);

    /**
     * Returns the property 'requested amount'.
     *
     * 
     *
     */
    double getRequestedAmount();
    /**
     * Setter for the property 'requested duration'.
     *
     * 
     *
     */
    void setRequestedDuration(int requestedDuration);

    /**
     * Returns the property 'requested duration'.
     *
     * 
     *
     */
    int getRequestedDuration();
    /**
     * Setter for the property 'shared loan'.
     *
     * 
     *
     */
    void setSharedLoan(boolean sharedLoan);

    /**
     * Returns the property 'shared loan'.
     *
     * 
     *
     */
    boolean getSharedLoan();
    /**
     * Setter for the property 'brokerage applications'.
     *
     * 
     *
     */
    void setBrokerageApplications(Set<BrokerageApplication> brokerageApplications);

    /**
     * Returns the property 'brokerage applications'.
     *
     * 
     *
     */
    Set<BrokerageApplication> getBrokerageApplications();
    /**
     * Setter for the property 'category'.
     *
     * 
     *
     */
    void setCategory(de.smava.webapp.applicant.brokerage.type.Category category);

    /**
     * Returns the property 'category'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.Category getCategory();
    /**
     * Setter for the property 'vehicle information'.
     *
     * 
     *
     */
    void setVehicleInformation(de.smava.webapp.applicant.brokerage.domain.VehicleInformation vehicleInformation);

    /**
     * Returns the property 'vehicle information'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.VehicleInformation getVehicleInformation();
    /**
     * Setter for the property 'initiator type'.
     *
     * 
     *
     */
    void setInitiatorType(de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType initiatorType);

    /**
     * Returns the property 'initiator type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType getInitiatorType();
    /**
     * Setter for the property 'initiator tool'.
     *
     * 
     *
     */
    void setInitiatorTool(String initiatorTool);

    /**
     * Returns the property 'initiator tool'.
     *
     * 
     *
     */
    String getInitiatorTool();
    /**
     * Setter for the property 'reached email timeout'.
     *
     * 
     *
     */
    void setReachedEmailTimeout(boolean reachedEmailTimeout);

    /**
     * Returns the property 'reached email timeout'.
     *
     * 
     *
     */
    boolean getReachedEmailTimeout();
    /**
     * Setter for the property 'visible'.
     *
     * 
     *
     */
    void setVisible(boolean visible);

    /**
     * Returns the property 'visible'.
     *
     * 
     *
     */
    boolean getVisible();
    /**
     * Helper method to get reference of this object as model type.
     */
    LoanApplication asLoanApplication();
}
