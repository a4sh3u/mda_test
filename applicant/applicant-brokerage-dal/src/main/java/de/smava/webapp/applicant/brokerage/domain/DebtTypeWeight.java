//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(debt type weight)}
import de.smava.webapp.applicant.brokerage.domain.history.DebtTypeWeightHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'DebtTypeWeights'.
 *
 * 
 *
 * @author generator
 */
public class DebtTypeWeight extends DebtTypeWeightHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(debt type weight)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.IndebtednessDebtType _debtType;
        protected Double _weight;
        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBank _brokerageBank;
        
                            /**
     * Setter for the property 'debt type'.
     *
     * 
     *
     */
    public void setDebtType(de.smava.webapp.applicant.type.IndebtednessDebtType debtType) {
        if (!_debtTypeIsSet) {
            _debtTypeIsSet = true;
            _debtTypeInitVal = getDebtType();
        }
        registerChange("debt type", _debtTypeInitVal, debtType);
        _debtType = debtType;
    }
                        
    /**
     * Returns the property 'debt type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.IndebtednessDebtType getDebtType() {
        return _debtType;
    }
                                    /**
     * Setter for the property 'weight'.
     *
     * 
     *
     */
    public void setWeight(Double weight) {
        if (!_weightIsSet) {
            _weightIsSet = true;
            _weightInitVal = getWeight();
        }
        registerChange("weight", _weightInitVal, weight);
        _weight = weight;
    }
                        
    /**
     * Returns the property 'weight'.
     *
     * 
     *
     */
    public Double getWeight() {
        return _weight;
    }
                                            
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerage bank : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DebtTypeWeight.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _debtType=").append(_debtType);
            builder.append("\n}");
        } else {
            builder.append(DebtTypeWeight.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public DebtTypeWeight asDebtTypeWeight() {
        return this;
    }
}
