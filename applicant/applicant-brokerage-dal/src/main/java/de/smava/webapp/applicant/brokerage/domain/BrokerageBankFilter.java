//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank filter)}
import de.smava.webapp.applicant.brokerage.domain.history.BrokerageBankFilterHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBankFilters'.
 *
 * 
 *
 * @author generator
 */
public class BrokerageBankFilter extends BrokerageBankFilterHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage bank filter)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _condition;
        protected de.smava.webapp.applicant.brokerage.type.FilterReasonCode _filterReason;
        
                            /**
     * Setter for the property 'condition'.
     *
     * 
     *
     */
    public void setCondition(String condition) {
        if (!_conditionIsSet) {
            _conditionIsSet = true;
            _conditionInitVal = getCondition();
        }
        registerChange("condition", _conditionInitVal, condition);
        _condition = condition;
    }
                        
    /**
     * Returns the property 'condition'.
     *
     * 
     *
     */
    public String getCondition() {
        return _condition;
    }
                                    /**
     * Setter for the property 'filter reason'.
     *
     * 
     *
     */
    public void setFilterReason(de.smava.webapp.applicant.brokerage.type.FilterReasonCode filterReason) {
        if (!_filterReasonIsSet) {
            _filterReasonIsSet = true;
            _filterReasonInitVal = getFilterReason();
        }
        registerChange("filter reason", _filterReasonInitVal, filterReason);
        _filterReason = filterReason;
    }
                        
    /**
     * Returns the property 'filter reason'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.FilterReasonCode getFilterReason() {
        return _filterReason;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageBankFilter.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _condition=").append(_condition);
            builder.append("\n    _filterReason=").append(_filterReason);
            builder.append("\n}");
        } else {
            builder.append(BrokerageBankFilter.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BrokerageBankFilter asBrokerageBankFilter() {
        return this;
    }
}
