//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(checkout flow page)}

import de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage;
import de.smava.webapp.applicant.brokerage.domain.interfaces.CheckoutFlowPageEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CheckoutFlowPages'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCheckoutFlowPage
    extends BrokerageEntity    implements CheckoutFlowPageEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCheckoutFlowPage.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof CheckoutFlowPage)) {
            return;
        }
        
        this.setName(((CheckoutFlowPage) oldEntity).getName());    
        this.setState(((CheckoutFlowPage) oldEntity).getState());    
        this.setPreviousPageId(((CheckoutFlowPage) oldEntity).getPreviousPageId());    
        this.setCheckoutFlow(((CheckoutFlowPage) oldEntity).getCheckoutFlow());    
            
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof CheckoutFlowPage)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((CheckoutFlowPage) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getState(), ((CheckoutFlowPage) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getPreviousPageId(), ((CheckoutFlowPage) otherEntity).getPreviousPageId());    
        equals = equals && valuesAreEqual(this.getCheckoutFlow(), ((CheckoutFlowPage) otherEntity).getCheckoutFlow());    
            
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(checkout flow page)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

