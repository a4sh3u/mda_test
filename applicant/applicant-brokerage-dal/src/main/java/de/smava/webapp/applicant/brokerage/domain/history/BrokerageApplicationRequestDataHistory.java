package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageApplicationRequestData;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageApplicationRequestDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageApplicationRequestDataHistory extends AbstractBrokerageApplicationRequestData {

    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _requestInitVal;
    protected transient boolean _requestIsSet;
    protected transient String _responseInitVal;
    protected transient boolean _responseIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.BrokerageState _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient Double _monthlyRateInitVal;
    protected transient boolean _monthlyRateIsSet;
    protected transient Double _effectiveInterestInitVal;
    protected transient boolean _effectiveInterestIsSet;
    protected transient Double _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient Integer _durationInitVal;
    protected transient boolean _durationIsSet;
    protected transient de.smava.webapp.applicant.brokerage.type.RdiType _rdiTypeInitVal;
    protected transient boolean _rdiTypeIsSet;
    protected transient Date _responseDateInitVal;
    protected transient boolean _responseDateIsSet;
    protected transient String _httpStateInitVal;
    protected transient boolean _httpStateIsSet;
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType typeInitVal() {
        de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
			
    /**
     * Returns the initial value of the property 'request'.
     */
    public String requestInitVal() {
        String result;
        if (_requestIsSet) {
            result = _requestInitVal;
        } else {
            result = getRequest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'request'.
     */
    public boolean requestIsDirty() {
        return !valuesAreEqual(requestInitVal(), getRequest());
    }

    /**
     * Returns true if the setter method was called for the property 'request'.
     */
    public boolean requestIsSet() {
        return _requestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'response'.
     */
    public String responseInitVal() {
        String result;
        if (_responseIsSet) {
            result = _responseInitVal;
        } else {
            result = getResponse();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response'.
     */
    public boolean responseIsDirty() {
        return !valuesAreEqual(responseInitVal(), getResponse());
    }

    /**
     * Returns true if the setter method was called for the property 'response'.
     */
    public boolean responseIsSet() {
        return _responseIsSet;
    }
	
    /**
     * Returns the initial value of the property 'state'.
     */
    public de.smava.webapp.applicant.brokerage.type.BrokerageState stateInitVal() {
        de.smava.webapp.applicant.brokerage.type.BrokerageState result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'monthly rate'.
     */
    public Double monthlyRateInitVal() {
        Double result;
        if (_monthlyRateIsSet) {
            result = _monthlyRateInitVal;
        } else {
            result = getMonthlyRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'monthly rate'.
     */
    public boolean monthlyRateIsDirty() {
        return !valuesAreEqual(monthlyRateInitVal(), getMonthlyRate());
    }

    /**
     * Returns true if the setter method was called for the property 'monthly rate'.
     */
    public boolean monthlyRateIsSet() {
        return _monthlyRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'effective interest'.
     */
    public Double effectiveInterestInitVal() {
        Double result;
        if (_effectiveInterestIsSet) {
            result = _effectiveInterestInitVal;
        } else {
            result = getEffectiveInterest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'effective interest'.
     */
    public boolean effectiveInterestIsDirty() {
        return !valuesAreEqual(effectiveInterestInitVal(), getEffectiveInterest());
    }

    /**
     * Returns true if the setter method was called for the property 'effective interest'.
     */
    public boolean effectiveInterestIsSet() {
        return _effectiveInterestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public Double amountInitVal() {
        Double result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'duration'.
     */
    public Integer durationInitVal() {
        Integer result;
        if (_durationIsSet) {
            result = _durationInitVal;
        } else {
            result = getDuration();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'duration'.
     */
    public boolean durationIsDirty() {
        return !valuesAreEqual(durationInitVal(), getDuration());
    }

    /**
     * Returns true if the setter method was called for the property 'duration'.
     */
    public boolean durationIsSet() {
        return _durationIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rdi type'.
     */
    public de.smava.webapp.applicant.brokerage.type.RdiType rdiTypeInitVal() {
        de.smava.webapp.applicant.brokerage.type.RdiType result;
        if (_rdiTypeIsSet) {
            result = _rdiTypeInitVal;
        } else {
            result = getRdiType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rdi type'.
     */
    public boolean rdiTypeIsDirty() {
        return !valuesAreEqual(rdiTypeInitVal(), getRdiType());
    }

    /**
     * Returns true if the setter method was called for the property 'rdi type'.
     */
    public boolean rdiTypeIsSet() {
        return _rdiTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'response date'.
     */
    public Date responseDateInitVal() {
        Date result;
        if (_responseDateIsSet) {
            result = _responseDateInitVal;
        } else {
            result = getResponseDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response date'.
     */
    public boolean responseDateIsDirty() {
        return !valuesAreEqual(responseDateInitVal(), getResponseDate());
    }

    /**
     * Returns true if the setter method was called for the property 'response date'.
     */
    public boolean responseDateIsSet() {
        return _responseDateIsSet;
    }

    /**
     * Returns the initial value of the property response date
     */
    public String httpStateInitVal() {
        String result;
        if (_httpStateIsSet) {
            result = _httpStateInitVal;
        } else {
            result = getHttpState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'http state'.
     */
    public boolean httpStateIsDirty() {
        return !valuesAreEqual(httpStateInitVal(), getHttpState());
    }

    /**
     * Returns true if the setter method was called for the property 'http state'.
     */
    public boolean httpStateIsSet() {
        return _httpStateIsSet;
    }
}
