package de.smava.webapp.applicant.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public class Reference {

    @JsonProperty
    private String id;

    @JsonProperty
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
