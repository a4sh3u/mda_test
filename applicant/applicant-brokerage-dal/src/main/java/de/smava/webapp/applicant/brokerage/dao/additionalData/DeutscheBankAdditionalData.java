package de.smava.webapp.applicant.brokerage.dao.additionalData;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonFilter(DeutscheBankAdditionalData.DEUTSCHE_BANK_FILTER)
public class DeutscheBankAdditionalData extends AbstractAdditionalData implements Serializable {

    private static final long serialVersionUID = 2778647487909847649L;

    public static final String DEUTSCHE_BANK_FILTER = "deutscheBankFilter";

    public static final String RETURN_CODE = "returnCode";
    public static final String TRANSACTION_ID = "transactionId";
    public static final String LOAN_OFFER_ID = "loanOfferId";
    public static final String UPSELLING_OFFER = "upsellingOffer";
    public static final String REQUEST_STATE_TEXT = "requestStateText";
    public static final String REASON_FOR_REPAIRS = "reasonForRepairs";
    public static final String REASON_FOR_REFUSALS = "reasonForRefusals";
    public static final String FMKV = "fmkv";
    public static final String AMOUNT_LAST_INSTALLMENT = "schlussrate";
    public static final String NUMBER_INSTALLMENTS = "anzahlRaten";
    public static final String INTEREST_RATE = "sollzins";
    public static final String AMOUNT_INTERESTS = "gesamtzinsen";
    public static final String TOTAL_LOAN_AMOUNT = "gesamtkreditbetrag";
    public static final String DATE_FIRST_INSTALLMENT = "ersteRateZum";
    public static final String ALTERNATIVE_REQUESTED_AMOUNT = "alternativeRequestedAmount";
    public static final String ALTERNATIVE_REQUESTED_DURATION = "alternativeRequestedDuration";
    public static final String MESSAGES = "messages";
    public static final String ORDER_REPRINT_DATE = "orderReprintDate";

    public static final String UPSELLING_OFFER_AMOUNT = "Kreditbetrag";
    public static final String UPSELLING_OFFER_DURATION = "Laufzeit";
    public static final String UPSELLING_OFFER_EFFECTIVE_INTEREST = "Eff. Zins";
    public static final String UPSELLING_OFFER_MONTHLY_RATE = "Rate";
    public static final String UPSELLING_OFFER_RDI_TYPE = "RKV";

    @JsonProperty(RETURN_CODE)
    protected Integer returnCode;

    @JsonProperty(TRANSACTION_ID)
    protected String transactionId;

    @JsonProperty(LOAN_OFFER_ID)
    protected String loanOfferId;

    @JsonProperty(UPSELLING_OFFER)
    protected Map<String, String> upsellingOffer;

    @JsonProperty(REQUEST_STATE_TEXT)
    protected String requestStateText;

    @JsonProperty(REASON_FOR_REPAIRS)
    protected List<String> reasonForRepairs;

    @JsonProperty(REASON_FOR_REFUSALS)
    protected List<String> reasonForRefusals;

    @JsonProperty(FMKV)
    protected BigDecimal fmkv;

    @JsonProperty(AMOUNT_LAST_INSTALLMENT)
    protected BigDecimal amountLastInstallment;

    @JsonProperty(NUMBER_INSTALLMENTS)
    protected Integer numberInstallments;

    @JsonProperty(INTEREST_RATE)
    protected BigDecimal interestRate;

    @JsonProperty(AMOUNT_INTERESTS)
    protected BigDecimal amountInterests;

    @JsonProperty(TOTAL_LOAN_AMOUNT)
    protected BigDecimal totalLoanAmount;

    @JsonProperty(DATE_FIRST_INSTALLMENT)
    protected Date dateFirstInstallment;

    @JsonProperty(ALTERNATIVE_REQUESTED_AMOUNT)
    protected Double alternativeRequestedAmount;

    @JsonProperty(ALTERNATIVE_REQUESTED_DURATION)
    protected Integer alternativeRequestedDuration;

    @JsonProperty(MESSAGES)
    protected String messages;

    @JsonProperty(ORDER_REPRINT_DATE)
    protected Date orderReprintDate;


    @Override
    public String determineFilterName() {
        return DEUTSCHE_BANK_FILTER;
    }


    public Integer getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getLoanOfferId() {
        return loanOfferId;
    }

    public void setLoanOfferId(String loanOfferId) {
        this.loanOfferId = loanOfferId;
    }

    public Map<String, String> getUpsellingOffer() {
        return upsellingOffer;
    }

    public void setUpsellingOffer(Map<String, String> upsellingOffer) {
        this.upsellingOffer = upsellingOffer;
    }

    public String getRequestStateText() {
        return requestStateText;
    }

    public void setRequestStateText(String requestStateText) {
        this.requestStateText = requestStateText;
    }

    public List<String> getReasonForRepairs() {
        return reasonForRepairs;
    }

    public void setReasonForRepairs(List<String> reasonForRepairs) {
        this.reasonForRepairs = reasonForRepairs;
    }

    public List<String> getReasonForRefusals() {
        return reasonForRefusals;
    }

    public void setReasonForRefusals(List<String> reasonForRefusals) {
        this.reasonForRefusals = reasonForRefusals;
    }

    public BigDecimal getFmkv() {
        return fmkv;
    }

    public void setFmkv(BigDecimal fmkv) {
        this.fmkv = fmkv;
    }

    public void setAmountLastInstallment(BigDecimal amountLastInstallment) {
        this.amountLastInstallment = amountLastInstallment;
    }

    public BigDecimal getAmountLastInstallment() {
        return amountLastInstallment;
    }

    public Integer getNumberInstallments() {
        return numberInstallments;
    }

    public void setNumberInstallments(Integer numberInstallments) {
        this.numberInstallments = numberInstallments;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setAmountInterests(BigDecimal amountInterests) {
        this.amountInterests = amountInterests;
    }

    public BigDecimal getAmountInterests() {
        return amountInterests;
    }

    public void setTotalLoanAmount(BigDecimal totalLoanAmount) {
        this.totalLoanAmount = totalLoanAmount;
    }

    public BigDecimal getTotalLoanAmount() {
        return totalLoanAmount;
    }

    public void setDateFirstInstallment(Date dateFirstInstallment) {
        this.dateFirstInstallment = dateFirstInstallment;
    }

    public Date getDateFirstInstallment() {
        return dateFirstInstallment;
    }

    public void setAlternativeRequestedAmount(Double alternativeRequestedAmount) {
        this.alternativeRequestedAmount = alternativeRequestedAmount;
    }

    public Double getAlternativeRequestedAmount() {
        return alternativeRequestedAmount;
    }

    public void setAlternativeRequestedDuration(Integer alternativeRequestedDuration) {
        this.alternativeRequestedDuration = alternativeRequestedDuration;
    }

    public Integer getAlternativeRequestedDuration() {
        return alternativeRequestedDuration;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getMessages() {
        return messages;
    }

    public void setOrderReprintDate(Date orderReprintDate) {
        this.orderReprintDate = orderReprintDate;
    }

    public Date getOrderReprintDate() {
        return orderReprintDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public HashMap<String, Object> readAdditionalData() {
        return readAdditionalData(DEUTSCHE_BANK_FILTER, RETURN_CODE, TRANSACTION_ID, UPSELLING_OFFER, REQUEST_STATE_TEXT, REASON_FOR_REPAIRS,
                REASON_FOR_REFUSALS, AMOUNT_LAST_INSTALLMENT, NUMBER_INSTALLMENTS, INTEREST_RATE, AMOUNT_INTERESTS,
                TOTAL_LOAN_AMOUNT, DATE_FIRST_INSTALLMENT, MESSAGES, ORDER_REPRINT_DATE);
    }
}
