package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationRequestData;
import de.smava.webapp.applicant.brokerage.domain.BrokerageMultiStatusRequestData;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageApplicationRequestDatas'.
 *
 * @author generator
 */
public interface BrokerageApplicationRequestDataEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.BrokerageApplicationRequestType getType();
    /**
     * Setter for the property 'brokerage application'.
     *
     * 
     *
     */
    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerage application'.
     *
     * 
     *
     */
    BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'brokerage multi status request data'.
     *
     * 
     *
     */
    void setBrokerageMultiStatusRequestData(BrokerageMultiStatusRequestData brokerageMultiStatusRequestData);

    /**
     * Returns the property 'brokerage multi status request data'.
     *
     * 
     *
     */
    BrokerageMultiStatusRequestData getBrokerageMultiStatusRequestData();
    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    void setRequest(String request);

    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    String getRequest();
    /**
     * Setter for the property 'response'.
     *
     * 
     *
     */
    void setResponse(String response);

    /**
     * Returns the property 'response'.
     *
     * 
     *
     */
    String getResponse();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(de.smava.webapp.applicant.brokerage.type.BrokerageState state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.BrokerageState getState();
    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    void setMonthlyRate(Double monthlyRate);

    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    Double getMonthlyRate();
    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    void setEffectiveInterest(Double effectiveInterest);

    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    Double getEffectiveInterest();
    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    void setAmount(Double amount);

    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    Double getAmount();
    /**
     * Setter for the property 'duration'.
     *
     * 
     *
     */
    void setDuration(Integer duration);

    /**
     * Returns the property 'duration'.
     *
     * 
     *
     */
    Integer getDuration();
    /**
     * Setter for the property 'rdi type'.
     *
     * 
     *
     */
    void setRdiType(de.smava.webapp.applicant.brokerage.type.RdiType rdiType);

    /**
     * Returns the property 'rdi type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.RdiType getRdiType();
    /**
     * Setter for the property 'response date'.
     *
     * 
     *
     */
    void setResponseDate(Date responseDate);

    /**
     * Returns the property 'response date'.
     *
     * 
     *
     */
    Date getResponseDate();

    /**
     * Setter for http state
     */
    void setHttpState(String httpState);

    /**
     * Getter for http state
     */
    String getHttpState();

    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationRequestData asBrokerageApplicationRequestData();
}
