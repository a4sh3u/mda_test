package de.smava.webapp.applicant.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @author asaadat
 * @since 02.08.2017.
 */
public class DocumentInformation {

    @JsonProperty
    private List<CustomerDocToBank> customerDocSentToBank;

    public List<CustomerDocToBank> getCustomerDocSentToBank() {
        if (this.customerDocSentToBank == null) {
            this.customerDocSentToBank = new ArrayList<CustomerDocToBank>();
        }

        return this.customerDocSentToBank;
    }

    public void setCustomerDocSentToBank(List<CustomerDocToBank> customerDocSentToBank) {
        this.customerDocSentToBank = customerDocSentToBank;
    }

}
