package de.smava.webapp.applicant.brokerage.domain.interfaces;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentDispatchType;
import de.smava.webapp.applicant.brokerage.type.BrokerageApplicationDocumentState;

import java.util.Date;

/**
 * The domain object that represents 'BrokerageApplicationDocuments'.
 *
 * @author Adam Tomecki
 * @since 26.10.2017
 */
public interface BrokerageApplicationDocumentEntityInterface {

    Date getCreationDate();

    void setCreationDate(Date creationDate);

    BrokerageApplication getBrokerageApplication();

    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    String getContentType();

    void setContentType(String contentType);

    BrokerageApplicationDocumentDispatchType getDispatchType();

    void setDispatchType(BrokerageApplicationDocumentDispatchType dispatchType);

    Long getFileStorageId();

    void setFileStorageId(Long fileStorageId);

    String getName();

    void setName(String name);

    String getNotes();

    void setNotes(String notes);

    BrokerageApplicationDocumentState getState();

    void setState(BrokerageApplicationDocumentState state);

    String getSubject();

    void setSubject(String subject);
}
