//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(household calculation)}
import de.smava.webapp.applicant.brokerage.domain.history.HouseholdCalculationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'HouseholdCalculations'.
 *
 * Household calculation based on applicant data
 *
 * @author generator
 */
public class HouseholdCalculation extends HouseholdCalculationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(household calculation)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType _type;
        protected de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName _name;
        protected String _mappingExpression;
        
                            /**
     * Setter for the property 'type'.
     *
     * Distinguishes the type of this household calculation item
     *
     */
    public void setType(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * Distinguishes the type of this household calculation item
     *
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationType getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.HouseholdCalculationName getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'mapping expression'.
     *
     * Mapping from applicant data in Spring expression language
     *
     */
    public void setMappingExpression(String mappingExpression) {
        if (!_mappingExpressionIsSet) {
            _mappingExpressionIsSet = true;
            _mappingExpressionInitVal = getMappingExpression();
        }
        registerChange("mapping expression", _mappingExpressionInitVal, mappingExpression);
        _mappingExpression = mappingExpression;
    }
                        
    /**
     * Returns the property 'mapping expression'.
     *
     * Mapping from applicant data in Spring expression language
     *
     */
    public String getMappingExpression() {
        return _mappingExpression;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(HouseholdCalculation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _name=").append(_name);
            builder.append("\n    _mappingExpression=").append(_mappingExpression);
            builder.append("\n}");
        } else {
            builder.append(HouseholdCalculation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public HouseholdCalculation asHouseholdCalculation() {
        return this;
    }
}
