package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDoc;
import de.smava.webapp.applicant.brokerage.domain.CustomerUploadedDoc;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageUploadedDocs'.
 *
 * @author generator
 */
public interface BrokerageUploadedDocEntityInterface {

    /**
     * Setter for the property 'document'.
     *
     * 
     *
     */
    void setDocument(CustomerUploadedDoc document);

    /**
     * Returns the property 'document'.
     *
     * 
     *
     */
    CustomerUploadedDoc getDocument();
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    void setBrokerageBank(BrokerageBank brokerageBank);

    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    BrokerageBank getBrokerageBank();
    /**
     * Setter for the property 'status'.
     *
     * 
     *
     */
    void setStatus(de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus status);

    /**
     * Returns the property 'status'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus getStatus();
    /**
     * Setter for the property 'send date'.
     *
     * 
     *
     */
    void setSendDate(Date sendDate);

    /**
     * Returns the property 'send date'.
     *
     * 
     *
     */
    Date getSendDate();
    /**
     * Setter for the property 'receive date'.
     *
     * 
     *
     */
    void setReceiveDate(Date receiveDate);

    /**
     * Returns the property 'receive date'.
     *
     * 
     *
     */
    Date getReceiveDate();
    /**
     * Setter for the property 'deletion date'.
     *
     * 
     *
     */
    void setDeletionDate(Date deletionDate);

    /**
     * Returns the property 'deletion date'.
     *
     * 
     *
     */
    Date getDeletionDate();
    /**
     * Setter for the property 'advisor'.
     *
     * 
     *
     */
    void setAdvisor(Long advisor);

    /**
     * Returns the property 'advisor'.
     *
     * 
     *
     */
    Long getAdvisor();
    /**
     * Setter for the property 'concatenated document id'.
     *
     * 
     *
     */
    void setConcatenatedDocumentId(Long concatenatedDocumentId);

    /**
     * Returns the property 'concatenated document id'.
     *
     * 
     *
     */
    Long getConcatenatedDocumentId();
    /**
     * Setter for the property 'approved'.
     *
     * 
     *
     */
    void setApproved(Boolean approved);

    /**
     * Returns the property 'approved'.
     *
     * 
     *
     */
    Boolean getApproved();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageUploadedDoc asBrokerageUploadedDoc();
}
