package de.smava.webapp.applicant.brokerage.domain.abstracts;

import de.smava.webapp.applicant.brokerage.domain.BrokerageFilterReasonMultiVal;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageFilterReasonMultiValInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;

/**
 * @author Adam Budzinski
 * @since 04.09.2017
 */
public abstract class AbstractBrokerageFilterReasonMultiVal extends BrokerageEntity implements BrokerageFilterReasonMultiValInterface {

    @Override
    public void copyFromOldEntity(Entity oldEntity) {
        if (!(oldEntity instanceof BrokerageFilterReasonMultiVal)) {
            return;
        }

        BrokerageFilterReasonMultiVal oldBrokerageFilterReasonMultiVal = (BrokerageFilterReasonMultiVal) oldEntity;

        setApplicantId(oldBrokerageFilterReasonMultiVal.getApplicantId());
        setBrokerageApplication(oldBrokerageFilterReasonMultiVal.getBrokerageApplication());
        setLoanApplication(oldBrokerageFilterReasonMultiVal.getLoanApplication());
        setFilterLevel(oldBrokerageFilterReasonMultiVal.getFilterLevel());
        setFilterReason(oldBrokerageFilterReasonMultiVal.getFilterReason());
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageFilterReasonMultiVal)) {
            equals = false;
        }

        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((BrokerageFilterReasonMultiVal) otherEntity).getBrokerageApplication());
        equals = equals && valuesAreEqual(this.getLoanApplication(), ((BrokerageFilterReasonMultiVal) otherEntity).getLoanApplication());
        equals = equals && valuesAreEqual(this.getApplicantId(), ((BrokerageFilterReasonMultiVal) otherEntity).getApplicantId());
        equals = equals && valuesAreEqual(this.getFilterLevel(), ((BrokerageFilterReasonMultiVal) otherEntity).getFilterLevel());
        equals = equals && valuesAreEqual(this.getFilterReason(), ((BrokerageFilterReasonMultiVal) otherEntity).getFilterReason());

        return equals;
    }
}
