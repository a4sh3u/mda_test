/**
 * 
 */
package de.smava.webapp.applicant.brokerage.type;

/**
 * @author bvoss
 *
 */
public enum MappingDirectionType {
	
	IN, OUT

}
