package de.smava.webapp.applicant.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public class Scoring {

    @JsonProperty
    private String color;

    @JsonProperty
    private Integer score;

    @JsonProperty
    private String text;

    @JsonProperty
    private String rapClass;

    @JsonProperty
    private Double freeIncome;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRapClass() {
        return rapClass;
    }

    public void setRapClass(String rapClass) {
        this.rapClass = rapClass;
    }

    public Double getFreeIncome() {
        return freeIncome;
    }

    public void setFreeIncome(Double freeIncome) {
        this.freeIncome = freeIncome;
    }
}
