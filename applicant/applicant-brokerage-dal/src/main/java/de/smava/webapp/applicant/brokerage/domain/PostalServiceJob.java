package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.history.PostalServiceJobHistory;

import java.util.Date;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public class PostalServiceJob extends PostalServiceJobHistory {
    protected Date _jobStartDate;
    protected Date _jobEndDate;
    protected Date _periodFrom;
    protected Date _periodTo;

    @Override
    public Date getJobStartDate() {
        return _jobStartDate;
    }

    @Override
    public void setJobStartDate(Date _jobStartDate) {
        this._jobStartDate = _jobStartDate;
    }

    @Override
    public Date getJobEndDate() {
        return _jobEndDate;
    }

    @Override
    public void setJobEndDate(Date _jobEndDate) {
        this._jobEndDate = _jobEndDate;
    }

    @Override
    public Date getPeriodFrom() {
        return _periodFrom;
    }

    @Override
    public void setPeriodFrom(Date _periodFrom) {
        this._periodFrom = _periodFrom;
    }

    @Override
    public Date getPeriodTo() {
        return _periodTo;
    }

    @Override
    public void setPeriodTo(Date _periodTo) {
        this._periodTo = _periodTo;
    }
}
