//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(requested documents configuration)}

import de.smava.webapp.applicant.brokerage.domain.RequestedDocumentsConfiguration;
import de.smava.webapp.applicant.brokerage.domain.interfaces.RequestedDocumentsConfigurationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'RequestedDocumentsConfigurations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractRequestedDocumentsConfiguration
    extends BrokerageEntity    implements RequestedDocumentsConfigurationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractRequestedDocumentsConfiguration.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof RequestedDocumentsConfiguration)) {
            return;
        }
        
        this.setName(((RequestedDocumentsConfiguration) oldEntity).getName());    
        this.setEmploymentType(((RequestedDocumentsConfiguration) oldEntity).getEmploymentType());    
        this.setApplicationType(((RequestedDocumentsConfiguration) oldEntity).getApplicationType());    
        this.setMinVolume(((RequestedDocumentsConfiguration) oldEntity).getMinVolume());    
        this.setMaxVolume(((RequestedDocumentsConfiguration) oldEntity).getMaxVolume());    
        this.setBrokerageBankConfiguration(((RequestedDocumentsConfiguration) oldEntity).getBrokerageBankConfiguration());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof RequestedDocumentsConfiguration)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((RequestedDocumentsConfiguration) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getEmploymentType(), ((RequestedDocumentsConfiguration) otherEntity).getEmploymentType());    
        equals = equals && valuesAreEqual(this.getApplicationType(), ((RequestedDocumentsConfiguration) otherEntity).getApplicationType());    
        equals = equals && valuesAreEqual(this.getMinVolume(), ((RequestedDocumentsConfiguration) otherEntity).getMinVolume());    
        equals = equals && valuesAreEqual(this.getMaxVolume(), ((RequestedDocumentsConfiguration) otherEntity).getMaxVolume());    
        equals = equals && valuesAreEqual(this.getBrokerageBankConfiguration(), ((RequestedDocumentsConfiguration) otherEntity).getBrokerageBankConfiguration());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(requested documents configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

