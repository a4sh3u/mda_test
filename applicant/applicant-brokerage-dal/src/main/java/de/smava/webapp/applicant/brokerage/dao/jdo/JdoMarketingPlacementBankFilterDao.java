//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao.jdo;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(marketing placement bank filter)}
import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.type.FilterType;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import de.smava.webapp.applicant.brokerage.dao.MarketingPlacementBankFilterDao;
import de.smava.webapp.applicant.brokerage.domain.MarketingPlacementBankFilter;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Repository;


import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.log4j.Logger;

// !!!!!!!! End of insert code section !!!!!!!!



/**
 * DAO implementation for the domain object 'MarketingPlacementBankFilters'.
 *
 * @author generator
 */
@Repository(value = "applicantMarketingPlacementBankFilterDao")
public class JdoMarketingPlacementBankFilterDao extends JdoBaseDao implements MarketingPlacementBankFilterDao {

    private static final Logger LOGGER = Logger.getLogger(JdoMarketingPlacementBankFilterDao.class);

    private static final String CLASS_NAME = "MarketingPlacementBankFilter";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customFields|bean(marketing placement bank filter)}
        //
        // insert custom fields here
        //
    // !!!!!!!! End of insert code section !!!!!!!!

    /**
     * Returns an attached copy of the marketing placement bank filter identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public MarketingPlacementBankFilter load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        MarketingPlacementBankFilter result = getEntity(MarketingPlacementBankFilter.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public MarketingPlacementBankFilter getMarketingPlacementBankFilter(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	MarketingPlacementBankFilter entity = findUniqueEntity(MarketingPlacementBankFilter.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the marketing placement bank filter.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(MarketingPlacementBankFilter marketingPlacementBankFilter) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveMarketingPlacementBankFilter: " + marketingPlacementBankFilter);
        }
        // !!!!!!!! You can insert code here: !!!!!!!!																														{|saveEntity|bean(marketing placement bank filter)}
        // !!!!!!!! End of insert code section !!!!!!!!
        return saveEntity(marketingPlacementBankFilter);
    }

    /**
     * @deprecated Use {@link #save(MarketingPlacementBankFilter) instead}
     */
    public Long saveMarketingPlacementBankFilter(MarketingPlacementBankFilter marketingPlacementBankFilter) {
        return save(marketingPlacementBankFilter);
    }

    /**
     * Deletes an marketing placement bank filter, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the marketing placement bank filter
     */
    public void deleteMarketingPlacementBankFilter(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteMarketingPlacementBankFilter: " + id);
        }
        deleteEntity(MarketingPlacementBankFilter.class, id);
    }

    /**
     * Retrieves all 'MarketingPlacementBankFilter' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<MarketingPlacementBankFilter> result = getEntities(MarketingPlacementBankFilter.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'MarketingPlacementBankFilter' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<MarketingPlacementBankFilter> result = getEntities(MarketingPlacementBankFilter.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPlacementBankFilter' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<MarketingPlacementBankFilter> result = getEntities(MarketingPlacementBankFilter.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPlacementBankFilter' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<MarketingPlacementBankFilter> getMarketingPlacementBankFilterList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<MarketingPlacementBankFilter> result = getEntities(MarketingPlacementBankFilter.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPlacementBankFilter' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacementBankFilter> findMarketingPlacementBankFilterList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'MarketingPlacementBankFilter' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<MarketingPlacementBankFilter> findMarketingPlacementBankFilterList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'MarketingPlacementBankFilter' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacementBankFilter> findMarketingPlacementBankFilterList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'MarketingPlacementBankFilter' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacementBankFilter> findMarketingPlacementBankFilterList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPlacementBankFilter' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacementBankFilter> findMarketingPlacementBankFilterList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'MarketingPlacementBankFilter' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<MarketingPlacementBankFilter> findMarketingPlacementBankFilterList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPlacementBankFilter' instances.
     */
    public long getMarketingPlacementBankFilterCount() {
        long result = getEntityCount(MarketingPlacementBankFilter.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPlacementBankFilter' instances which match the given whereClause.
     */
    public long getMarketingPlacementBankFilterCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(MarketingPlacementBankFilter.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'MarketingPlacementBankFilter' instances which match the given whereClause together with params specified in object array.
     */
    public long getMarketingPlacementBankFilterCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(MarketingPlacementBankFilter.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }


    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(marketing placement bank filter)}
    //
    // insert custom methods here
    @Override
    public boolean isFilteredExternalAffiliateId(String externalAffiliateId, BrokerageBank bb) {


        OqlTerm oqlTerm = OqlTerm.newTerm();
        oqlTerm.and(
                oqlTerm.equals("_externalAffiliateId", externalAffiliateId),
                oqlTerm.equals("_brokerageBank._id", bb.getId()),
                oqlTerm.equals("_filterType", FilterType.EXCLUDE.name()));



        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, oqlTerm.toString());

        return ( ! result.isEmpty());
    }

    @Override
    public Collection<MarketingPlacementBankFilter> findIncludeFilters(BrokerageBank bb) {
        OqlTerm oqlTerm = OqlTerm.newTerm();
        oqlTerm.and(
                oqlTerm.equals("_brokerageBank._id", bb.getId()),
                oqlTerm.equals("_filterType", FilterType.INCLUDE.name()));

        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, oqlTerm.toString());
        return result;
    }

    @Override
    public Collection<MarketingPlacementBankFilter> findIncludeFilters(String externalAffiliateId) throws UnsupportedEncodingException {
        OqlTerm oqlTerm = OqlTerm.newTerm();
        oqlTerm.and(
                oqlTerm.equals("_externalAffiliateId", new String (Base64.encodeBase64(externalAffiliateId.getBytes("utf-8")))),
                oqlTerm.equals("_filterType", FilterType.INCLUDE.name()));

        Collection<MarketingPlacementBankFilter> result = findEntities(MarketingPlacementBankFilter.class, oqlTerm.toString());
        return result;
    }
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
