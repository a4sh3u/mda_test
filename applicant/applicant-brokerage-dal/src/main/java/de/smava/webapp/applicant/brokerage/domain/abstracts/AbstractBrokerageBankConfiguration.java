//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage bank configuration)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageBankConfiguration;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageBankConfigurationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageBankConfigurations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageBankConfiguration
    extends BrokerageEntity    implements BrokerageBankConfigurationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageBankConfiguration.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageBankConfiguration)) {
            return;
        }
        
        this.setName(((BrokerageBankConfiguration) oldEntity).getName());    
        this.setDocumentUpload(((BrokerageBankConfiguration) oldEntity).getDocumentUpload());    
        this.setDocumentUploadViaSftp(((BrokerageBankConfiguration) oldEntity).getDocumentUploadViaSftp());    
        this.setDocumentUploadViaPost(((BrokerageBankConfiguration) oldEntity).getDocumentUploadViaPost());    
        this.setDocumentUploadViaBankApi(((BrokerageBankConfiguration) oldEntity).getDocumentUploadViaBankApi());    
        this.setVideoIdent(((BrokerageBankConfiguration) oldEntity).getVideoIdent());    
        this.setEsign(((BrokerageBankConfiguration) oldEntity).getEsign());    
        this.setBrokerageBank(((BrokerageBankConfiguration) oldEntity).getBrokerageBank());    
        this.setRequestedDocumentsConfigurations(((BrokerageBankConfiguration) oldEntity).getRequestedDocumentsConfigurations());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageBankConfiguration)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getName(), ((BrokerageBankConfiguration) otherEntity).getName());    
        equals = equals && valuesAreEqual(this.getDocumentUpload(), ((BrokerageBankConfiguration) otherEntity).getDocumentUpload());    
        equals = equals && valuesAreEqual(this.getDocumentUploadViaSftp(), ((BrokerageBankConfiguration) otherEntity).getDocumentUploadViaSftp());    
        equals = equals && valuesAreEqual(this.getDocumentUploadViaPost(), ((BrokerageBankConfiguration) otherEntity).getDocumentUploadViaPost());    
        equals = equals && valuesAreEqual(this.getDocumentUploadViaBankApi(), ((BrokerageBankConfiguration) otherEntity).getDocumentUploadViaBankApi());    
        equals = equals && valuesAreEqual(this.getVideoIdent(), ((BrokerageBankConfiguration) otherEntity).getVideoIdent());    
        equals = equals && valuesAreEqual(this.getEsign(), ((BrokerageBankConfiguration) otherEntity).getEsign());    
        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((BrokerageBankConfiguration) otherEntity).getBrokerageBank());    
        equals = equals && valuesAreEqual(this.getRequestedDocumentsConfigurations(), ((BrokerageBankConfiguration) otherEntity).getRequestedDocumentsConfigurations());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage bank configuration)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

