package de.smava.webapp.applicant.brokerage.domain;

import java.util.Arrays;
import java.util.List;

/**
 * Created by asaadat on 08.04.16.
 */
public enum BrokerageUploadedDocStatus {

    CREATED,
    SENT,
    RECEIVED,
    DELETED,
    OTHER;

    private static final List<BrokerageUploadedDocStatus> ACTIVE = Arrays.asList(new BrokerageUploadedDocStatus[]{CREATED, SENT, RECEIVED});

    public static List<BrokerageUploadedDocStatus> activeStatusList(){
        return ACTIVE;
    }


    public boolean isActive(){
        return activeStatusList().contains(this);
    }
    
    public static String convert(BrokerageUploadedDocStatus brokerageUploadedDocStatus) {
        switch (brokerageUploadedDocStatus) {
            case CREATED:
                return "Neu";
            case SENT:
                return "Gesendet";
            case RECEIVED:
                return "Entgegengenommen";
            case DELETED:
                return "Gelöscht";
            default:
                return "Other";

        }
    }

    public static BrokerageUploadedDocStatus convert(String brokerageUploadedDocStatusStr) {
        if (brokerageUploadedDocStatusStr.equalsIgnoreCase("Neu")) {
            return CREATED;
        } else if (brokerageUploadedDocStatusStr.equalsIgnoreCase("Gesendet")) {
            return SENT;
        } else if (brokerageUploadedDocStatusStr.equalsIgnoreCase("Entgegengenommen")) {
            return RECEIVED;
        } else if (brokerageUploadedDocStatusStr.equalsIgnoreCase("Abgelehnt")) {
            return DELETED;
        } else {
            return OTHER;
        }
    }
}
