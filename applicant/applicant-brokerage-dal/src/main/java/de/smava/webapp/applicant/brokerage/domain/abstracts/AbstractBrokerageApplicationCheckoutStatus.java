//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application checkout status)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationCheckoutStatus;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageApplicationCheckoutStatusEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationCheckoutStatuss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageApplicationCheckoutStatus
    extends BrokerageEntity    implements BrokerageApplicationCheckoutStatusEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageApplicationCheckoutStatus.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageApplicationCheckoutStatus)) {
            return;
        }
        
        this.setBrokerageApplicationId(((BrokerageApplicationCheckoutStatus) oldEntity).getBrokerageApplicationId());    
        this.setCurrentCheckoutFlow(((BrokerageApplicationCheckoutStatus) oldEntity).getCurrentCheckoutFlow());    
        this.setCurrentCheckoutFlowPage(((BrokerageApplicationCheckoutStatus) oldEntity).getCurrentCheckoutFlowPage());    
        this.setUpdateTimestamp(((BrokerageApplicationCheckoutStatus) oldEntity).getUpdateTimestamp());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageApplicationCheckoutStatus)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageApplicationId(), ((BrokerageApplicationCheckoutStatus) otherEntity).getBrokerageApplicationId());    
        equals = equals && valuesAreEqual(this.getCurrentCheckoutFlow(), ((BrokerageApplicationCheckoutStatus) otherEntity).getCurrentCheckoutFlow());    
        equals = equals && valuesAreEqual(this.getCurrentCheckoutFlowPage(), ((BrokerageApplicationCheckoutStatus) otherEntity).getCurrentCheckoutFlowPage());    
        equals = equals && valuesAreEqual(this.getUpdateTimestamp(), ((BrokerageApplicationCheckoutStatus) otherEntity).getUpdateTimestamp());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage application checkout status)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

