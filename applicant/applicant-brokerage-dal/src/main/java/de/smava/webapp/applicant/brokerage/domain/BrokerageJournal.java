package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageJournalEntityInterface;
import de.smava.webapp.applicant.brokerage.type.DirectionType;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.brokerage.domain.LoanApplication;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageJournals'.
 */
public class BrokerageJournal extends BrokerageEntity implements BrokerageJournalEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(BrokerageJournal.class);

    protected Date _creationDate;
    protected Date _sendUntilDate;
    protected de.smava.webapp.applicant.brokerage.type.BrokerageJournalType _type;
    protected String _text;
    protected Long _customerNumber;
    protected Advisor _advisor;
    protected LoanApplication _loanApplication;
    protected BrokerageApplication _brokerageApplication;
    protected Boolean _sendByPost;
    protected DirectionType _direction;
    protected String _subject;
    protected String _externalRefId;

    public void setCreationDate(Date creationDate) {
        _creationDate = creationDate;
    }

    public Date getCreationDate() {
        return _creationDate;
    }

    public void setSendUntilDate(Date sendUntilDate) {
        _sendUntilDate = sendUntilDate;
    }

    public Date getSendUntilDate() {
        return _sendUntilDate;
    }

    public void setType(de.smava.webapp.applicant.brokerage.type.BrokerageJournalType type) {
        _type = type;
    }

    public de.smava.webapp.applicant.brokerage.type.BrokerageJournalType getType() {
        return _type;
    }

    @Override
    public void setCustomerNumber(Long customerNumber) {
        this._customerNumber = customerNumber;
    }

    @Override
    public Long getCustomerNumber() {
        return this._customerNumber;
    }

    public void setAdvisor(Advisor advisor) {
        _advisor = advisor;
    }

    public Advisor getAdvisor() {
        return _advisor;
    }

    public void setLoanApplication(LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }

    public LoanApplication getLoanApplication() {
        return _loanApplication;
    }

    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }

    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }

    public void setSendByPost(Boolean sendByPost) {
        _sendByPost = sendByPost;
    }

    public Boolean getSendByPost() {
        return _sendByPost;
    }

    public void setText(String text) {
        _text = text;
    }

    public String getText() {
        return _text;
    }

    public void setDirection(DirectionType direction) {
        _direction = direction;
    }

    @Override
    public String getExternalRefId() {
        return _externalRefId;
    }

    @Override
    public void setExternalRefId(String externalRefId) {
        _externalRefId = externalRefId;
    }

    public DirectionType getDirection() {
        return _direction;
    }

    public void setSubject(String subject) {
        _subject = subject;
    }

    public String getSubject() {
        return _subject;
    }

    @Deprecated
    public java.util.Set<String> getFullChangeSet() {
        java.util.Set<String> result = new java.util.HashSet<String>();
        result.addAll(getChangeSet());

        if (_loanApplication instanceof de.smava.webapp.commons.domain.Entity && !_loanApplication.getChangeSet().isEmpty()) {
            for (String element : _loanApplication.getChangeSet()) {
                result.add("loan application : " + element);
            }
        }

        if (_brokerageApplication instanceof de.smava.webapp.commons.domain.Entity && !_brokerageApplication.getChangeSet().isEmpty()) {
            for (String element : _brokerageApplication.getChangeSet()) {
                result.add("brokerage application : " + element);
            }
        }

        return result;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageJournal.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(BrokerageJournal.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
     * Helper method to get reference of this object as model type.
     */
    public BrokerageJournal asBrokerageJournal() {
        return this;
    }


    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageJournal)) {
            return;
        }

        this.setCreationDate(CurrentDate.getDate());
        this.setSendUntilDate(((BrokerageJournal) oldEntity).getSendUntilDate());
        this.setType(((BrokerageJournal) oldEntity).getType());
        this.setText(((BrokerageJournal) oldEntity).getText());
        this.setCustomerNumber(((BrokerageJournal) oldEntity).getCustomerNumber());
        this.setAdvisor(((BrokerageJournal) oldEntity).getAdvisor());
        this.setLoanApplication(((BrokerageJournal) oldEntity).getLoanApplication());
        this.setBrokerageApplication(((BrokerageJournal) oldEntity).getBrokerageApplication());
        this.setSendByPost(((BrokerageJournal) oldEntity).getSendByPost());
        this.setExternalRefId(((BrokerageJournal) oldEntity).getExternalRefId());
        this.setDirection(((BrokerageJournal) oldEntity).getDirection());
        this.setSubject(((BrokerageJournal) oldEntity).getSubject());

    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {

        if (otherEntity == null) {
            return false;
        }

        if (!(otherEntity instanceof BrokerageJournal)) {
            return false;
        }

        boolean equals = true;

        equals = equals && valuesAreEqual(this.getSendUntilDate(), ((BrokerageJournal) otherEntity).getSendUntilDate());
        equals = equals && valuesAreEqual(this.getType(), ((BrokerageJournal) otherEntity).getType());
        equals = equals && valuesAreEqual(this.getText(), ((BrokerageJournal) otherEntity).getText());
        equals = equals && valuesAreEqual(this.getCustomerNumber(), ((BrokerageJournal) otherEntity).getCustomerNumber());
        equals = equals && valuesAreEqual(this.getAdvisor(), ((BrokerageJournal) otherEntity).getAdvisor());
        equals = equals && valuesAreEqual(this.getLoanApplication(), ((BrokerageJournal) otherEntity).getLoanApplication());
        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((BrokerageJournal) otherEntity).getBrokerageApplication());
        equals = equals && valuesAreEqual(this.getSendByPost(), ((BrokerageJournal) otherEntity).getSendByPost());
        equals = equals && valuesAreEqual(this.getExternalRefId(), ((BrokerageJournal) otherEntity).getExternalRefId());
        equals = equals && valuesAreEqual(this.getDirection(), ((BrokerageJournal) otherEntity).getDirection());
        equals = equals && valuesAreEqual(this.getSubject(), ((BrokerageJournal) otherEntity).getSubject());

        return equals;
    }
}
