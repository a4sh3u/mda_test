package de.smava.webapp.applicant.brokerage.dao;

import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.applicant.domain.ApplicantsRelationship;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * DAO interface for the domain object 'LoanApplications'.
 */
public interface LoanApplicationDao extends BrokerageSchemaDao<LoanApplication> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Retrieves all 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<LoanApplication> getLoanApplicationList();

    /**
     * Retrieves a page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<LoanApplication> getLoanApplicationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<LoanApplication> getLoanApplicationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<LoanApplication> getLoanApplicationList(Pageable pageable, Sortable sortable);

    LoanApplication findCurrentLoanApplicationForAccount(Long accountId);

    LoanApplication findCurrentLoanApplicationInitiatedByCustomerForAccount(Long accountId);

    LoanApplication findCurrentVisibleLoanApplicationForAccount(Long accountId);

    LoanApplication findCurrentLoanApplicationWithCarFinanceForAccount(Long accountId);

    LoanApplication findCurrentLoanApplication(ApplicantsRelationship applicantsRelationship);

    List<LoanApplication> getAllLoanApplicationsWithThirdPartyLoans(Long accountId);

    List<LoanApplication> getCurrentPostalServiceMasterApplications(Date startDate, Date endDate);

}
