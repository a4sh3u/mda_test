package de.smava.webapp.applicant.brokerage.type;

/**
 * Created by aherr on 02.03.15.
 */
public enum RdiType {

    NONE,
    DEATH,
    DEATH_INVALIDITY,
    DEATH_INVALIDITY_UNEMPLOYMENT;
}
