package de.smava.webapp.applicant.brokerage.additionaldata.property;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Adam
 * @since 25.04.2016.
 */
public class MissingDocuments {

    @JsonProperty
    @JsonFormat(locale = "de", shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "CET")
    private Date expirationDate;

    @JsonProperty
    private List<String> documents;

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public List<String> getDocuments() {
        if (this.documents == null) {
            this.documents = new ArrayList<String>();
        }

        return this.documents;
    }
}
