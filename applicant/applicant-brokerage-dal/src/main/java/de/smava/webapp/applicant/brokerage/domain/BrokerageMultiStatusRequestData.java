//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage multi status request data)}

import de.smava.webapp.applicant.brokerage.domain.history.BrokerageMultiStatusRequestDataHistory;

import java.util.Date;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageMultiStatusRequestDatas'.
 * <p>
 * Data related to multi-status request to bank
 *
 * @author generator
 */
public class BrokerageMultiStatusRequestData extends BrokerageMultiStatusRequestDataHistory {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(brokerage multi status request data)}
    // !!!!!!!! End of insert code section !!!!!!!!

    protected Date _creationDate;
    protected BrokerageBank _brokerageBank;
    protected de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState _state;
    protected String _request;
    protected String _response;
    protected String _httpState;

    /**
     * Setter for the property 'creation date'.
     */
    public void setCreationDate(Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }

    /**
     * Returns the property 'creation date'.
     */
    public Date getCreationDate() {
        return _creationDate;
    }

    /**
     * Setter for the property 'brokerage bank'.
     */
    public void setBrokerageBank(BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }

    /**
     * Returns the property 'brokerage bank'.
     */
    public BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }

    /**
     * Setter for the property 'state'.
     */
    public void setState(de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }

    /**
     * Returns the property 'state'.
     */
    public de.smava.webapp.applicant.brokerage.type.MultiStatusRequestState getState() {
        return _state;
    }

    /**
     * Setter for the property 'request'.
     */
    public void setRequest(String request) {
        if (!_requestIsSet) {
            _requestIsSet = true;
            _requestInitVal = getRequest();
        }
        registerChange("request", _requestInitVal, request);
        _request = request;
    }

    /**
     * Returns the property 'request'.
     */
    public String getRequest() {
        return _request;
    }

    /**
     * Setter for the property 'response'.
     */
    public void setResponse(String response) {
        if (!_responseIsSet) {
            _responseIsSet = true;
            _responseInitVal = getResponse();
        }
        registerChange("response", _responseInitVal, response);
        _response = response;
    }

    /**
     * Returns the property 'response'.
     */
    public String getResponse() {
        return _response;
    }

    /**
     * Setter for the property http state
     */
    public void setHttpState(String httpState) {
        if (!_httpStateIsSet) {
            _httpStateIsSet = true;
            _httpStateInitVal = getHttpState();
        }
        registerChange("http state", _httpStateInitVal, httpState);
        _httpState = httpState;
    }

    /**
     * Returns the property 'http_state'
     */
    public String getHttpState() {
        return _httpState;
    }

    /**
     * Get list of full changes in the object and sub-elements
     */
    public java.util.Set<String> getFullChangeSet() {
        java.util.Set<String> result = new java.util.HashSet<String>();
        result.addAll(getChangeSet());


        if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
            for (String element : _brokerageBank.getChangeSet()) {
                result.add("brokerage bank : " + element);
            }
        }

        return result;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BrokerageMultiStatusRequestData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _request=").append(_request);
            builder.append("\n    _response=").append(_response);
            builder.append("\n    _httpState=").append(_httpState);
            builder.append("\n}");
        } else {
            builder.append(BrokerageMultiStatusRequestData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
     * Helper method to get reference of this object as model type.
     */
    public BrokerageMultiStatusRequestData asBrokerageMultiStatusRequestData() {
        return this;
    }
}
