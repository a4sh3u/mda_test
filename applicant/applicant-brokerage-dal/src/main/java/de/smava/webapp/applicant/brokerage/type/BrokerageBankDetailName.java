package de.smava.webapp.applicant.brokerage.type;

/**
 * Created by asaadat on 17.06.16.
 */
public enum BrokerageBankDetailName {

    RECIPIENT_NAME("recipientName"),
    ADDRESS("address1"),
    ADDRESS2("address2"),
    POST_CODE("postalcode"),
    CITY("city"),
    LABEL("label");

    private final String value;

    BrokerageBankDetailName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BrokerageBankDetailName fromValue(String v) {
        for (BrokerageBankDetailName c : BrokerageBankDetailName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
