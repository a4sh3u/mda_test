package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationFilterReason;


/**
 * The domain object that represents 'BrokerageApplicationFilterReasons'.
 *
 * @author generator
 */
public interface BrokerageApplicationFilterReasonEntityInterface {

    /**
     * Setter for the property 'reason code'.
     *
     * 
     *
     */
    void setReasonCode(de.smava.webapp.applicant.brokerage.type.FilterReasonCode reasonCode);

    /**
     * Returns the property 'reason code'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.type.FilterReasonCode getReasonCode();
    /**
     * Setter for the property 'reason description'.
     *
     * 
     *
     */
    void setReasonDescription(String reasonDescription);

    /**
     * Returns the property 'reason description'.
     *
     * 
     *
     */
    String getReasonDescription();
    /**
     * Helper method to get reference of this object as model type.
     */
    BrokerageApplicationFilterReason asBrokerageApplicationFilterReason();
}
