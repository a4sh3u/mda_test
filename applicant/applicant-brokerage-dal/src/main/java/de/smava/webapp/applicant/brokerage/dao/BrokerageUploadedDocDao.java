//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage uploaded doc)}
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.transaction.Synchronization;

import de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDocStatus;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.brokerage.domain.BrokerageUploadedDoc;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageUploadedDocs'.
 *
 * @author generator
 */
public interface BrokerageUploadedDocDao extends BrokerageSchemaDao<BrokerageUploadedDoc> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage uploaded doc identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageUploadedDoc getBrokerageUploadedDoc(Long id);

    /**
     * Saves the brokerage uploaded doc.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageUploadedDoc(BrokerageUploadedDoc brokerageUploadedDoc);

    /**
     * Deletes an brokerage uploaded doc, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage uploaded doc
     */
    void deleteBrokerageUploadedDoc(Long id);

    /**
     * Retrieves all 'BrokerageUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageUploadedDoc> getBrokerageUploadedDocList();

    /**
     * Retrieves a page of 'BrokerageUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageUploadedDoc> getBrokerageUploadedDocList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageUploadedDoc' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageUploadedDoc> getBrokerageUploadedDocList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageUploadedDoc> getBrokerageUploadedDocList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageUploadedDoc' instances.
     */
    long getBrokerageUploadedDocCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage uploaded doc)}
    //
    // insert custom methods here
    //

    Collection<BrokerageUploadedDoc> fetchDocumentsByBrokerageBankAndStatusIn(Long brokerageBankId, List<BrokerageUploadedDocStatus> statusList);
    Collection<BrokerageUploadedDoc> fetchDocumentsByBrokerageBankAndAccountIdAndStatusIn(Long brokerageBankId, Long accountId, List<BrokerageUploadedDocStatus> statusList);

    // !!!!!!!! End of insert code section !!!!!!!!
}
