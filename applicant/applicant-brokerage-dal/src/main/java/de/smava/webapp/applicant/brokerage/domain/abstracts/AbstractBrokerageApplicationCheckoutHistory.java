package de.smava.webapp.applicant.brokerage.domain.abstracts;



import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationCheckoutHistory;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageApplicationCheckoutHistoryEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                    



/**
 * The domain object that represents 'BrokerageApplicationCheckoutHistorys'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageApplicationCheckoutHistory
    extends BrokerageEntity    implements BrokerageApplicationCheckoutHistoryEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageApplicationCheckoutHistory.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageApplicationCheckoutHistory)) {
            return;
        }
        
        this.setBrokerageApplicationId(((BrokerageApplicationCheckoutHistory) oldEntity).getBrokerageApplicationId());    
        this.setCheckoutFlowPage(((BrokerageApplicationCheckoutHistory) oldEntity).getCheckoutFlowPage());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setInitiator(((BrokerageApplicationCheckoutHistory) oldEntity).getInitiator());    
        this.setAdditionalData(((BrokerageApplicationCheckoutHistory) oldEntity).getAdditionalData());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageApplicationCheckoutHistory)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageApplicationId(), ((BrokerageApplicationCheckoutHistory) otherEntity).getBrokerageApplicationId());    
        equals = equals && valuesAreEqual(this.getCheckoutFlowPage(), ((BrokerageApplicationCheckoutHistory) otherEntity).getCheckoutFlowPage());    
            
        equals = equals && valuesAreEqual(this.getInitiator(), ((BrokerageApplicationCheckoutHistory) otherEntity).getInitiator());    
        equals = equals && valuesAreEqual(this.getAdditionalData(), ((BrokerageApplicationCheckoutHistory) otherEntity).getAdditionalData());    
        
        return equals;
    }



}

