package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBankHouseholdCalculation;




/**
 * The domain object that has all history aggregation related fields for 'BankHouseholdCalculations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BankHouseholdCalculationHistory extends AbstractBankHouseholdCalculation {

    protected transient String _bankExpressionInitVal;
    protected transient boolean _bankExpressionIsSet;


			
    /**
     * Returns the initial value of the property 'bank expression'.
     */
    public String bankExpressionInitVal() {
        String result;
        if (_bankExpressionIsSet) {
            result = _bankExpressionInitVal;
        } else {
            result = getBankExpression();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank expression'.
     */
    public boolean bankExpressionIsDirty() {
        return !valuesAreEqual(bankExpressionInitVal(), getBankExpression());
    }

    /**
     * Returns true if the setter method was called for the property 'bank expression'.
     */
    public boolean bankExpressionIsSet() {
        return _bankExpressionIsSet;
    }

}
