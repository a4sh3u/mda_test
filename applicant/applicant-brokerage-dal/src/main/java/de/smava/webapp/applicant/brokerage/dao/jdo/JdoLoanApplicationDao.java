package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.LoanApplicationDao;
import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.domain.ApplicantsRelationship;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.sql.Timestamp;
import java.util.*;

/**
 * DAO implementation for the domain object 'LoanApplications'.
 *
 * @author generator
 */
@Repository(value = "applicantLoanApplicationDao")
public class JdoLoanApplicationDao extends JdoBaseDao implements LoanApplicationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoLoanApplicationDao.class);

    private static final String CLASS_NAME = "LoanApplication";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    /**
     * Returns an attached copy of the loan application identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public LoanApplication load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        LoanApplication result = getEntity(LoanApplication.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        return exists(LoanApplication.class, id);
    }

    /*
     * Saves the loan application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persistence manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(LoanApplication loanApplication) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveLoanApplication: " + loanApplication);
        }
        return saveEntity(loanApplication);
    }

    /**
     * Retrieves all 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<LoanApplication> getLoanApplicationList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<LoanApplication> getLoanApplicationList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'LoanApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<LoanApplication> getLoanApplicationList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'LoanApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<LoanApplication> getLoanApplicationList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = getEntities(LoanApplication.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'LoanApplication' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'LoanApplication' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'LoanApplication' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'LoanApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'LoanApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'LoanApplication' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<LoanApplication> findLoanApplicationList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<LoanApplication> result = findEntities(LoanApplication.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'LoanApplication' instances which match the given whereClause.
     */
    public long getLoanApplicationCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(LoanApplication.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'LoanApplication' instances which match the given whereClause together with params specified in object array.
     */
    public long getLoanApplicationCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(LoanApplication.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    @Override
    public LoanApplication findCurrentLoanApplicationForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentLoanApplicationForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() == 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    @Override
    public LoanApplication findCurrentLoanApplicationInitiatedByCustomerForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentLoanApplicationInitiatedByCustomerForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() == 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    @Override
    public LoanApplication findCurrentVisibleLoanApplicationForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentVisibleLoanApplicationForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() >= 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    @Override
    public LoanApplication findCurrentLoanApplicationWithCarFinanceForAccount(Long accountId) {

        LoanApplication returnApp = null;

        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentLoanApplicationWithCarFinanceForAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);
        params.put(2, accountId);

        @SuppressWarnings("unchecked")
        Collection<LoanApplication> loanApps = (Collection<LoanApplication>) query.executeWithMap(params);

        if(loanApps.size() == 1) {
            returnApp = loanApps.iterator().next();
        }

        return returnApp;
    }

    @Override
    public LoanApplication findCurrentLoanApplication(ApplicantsRelationship applicantsRelationship) {
        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "findCurrentLoanApplicationForApplicantsRelationship");
        query.setUnique(true);

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("applicantsRelationshipId", applicantsRelationship.getId());

        return (LoanApplication) query.executeWithMap(parameters);
    }

    @Override
    public List<LoanApplication> getAllLoanApplicationsWithThirdPartyLoans(Long accountId) {
        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "getAllLoanApplicationsWithThirdPartyLoans");
        @SuppressWarnings("unchecked")
        List<LoanApplication> loanApps =
                (List<LoanApplication>) query.executeWithMap(Collections.<String, Object>singletonMap("loanApplicationId", accountId));
        return loanApps;
    }

    @Override
    public List<LoanApplication> getCurrentPostalServiceMasterApplications(Date startDate, Date endDate) {
        Query query = getPersistenceManager().newNamedQuery(LoanApplication.class, "getCurrentPostalServiceMasterApplications");

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, new Timestamp(startDate.getTime()));
        parameters.put(2, new Timestamp(endDate.getTime()));

        @SuppressWarnings("unchecked")
        List<LoanApplication> loanApps = (List<LoanApplication>) query.executeWithMap(parameters);

        return loanApps;
    }
}
