package de.smava.webapp.applicant.brokerage.domain.history;

import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractPostalServiceMasterApplication;

import java.util.Date;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public abstract class PostalServiceMasterApplicationHistory extends AbstractPostalServiceMasterApplication {
    protected transient Date _dateInitVal;
    protected transient boolean _dateIsSet;
    protected transient de.smava.webapp.applicant.brokerage.domain.PostalServiceJob _postalServiceJobInitVal;
    protected transient boolean _postalServiceJobIsSet;
    protected transient de.smava.webapp.applicant.brokerage.domain.LoanApplication _loanApplicationInitVal;
    protected transient boolean _loanApplicationIsSet;
    protected transient String _brokerageApplicationsInitVal;
    protected transient boolean _brokerageApplicationsIsSet;

    public Date dateInitVal() {
        Date result;
        if (_dateIsSet) {
            result = _dateInitVal;
        } else {
            result = getDate();
        }
        return result;
    }

    public boolean dateIsDirty() {
        return !valuesAreEqual(dateInitVal(), getDate());
    }

    public boolean dateIsSet() {
        return _dateIsSet;
    }

    public de.smava.webapp.applicant.brokerage.domain.PostalServiceJob postalServiceJobInitVal() {
        de.smava.webapp.applicant.brokerage.domain.PostalServiceJob result;
        if (_postalServiceJobIsSet) {
            result = _postalServiceJobInitVal;
        } else {
            result = getPostalServiceJob();
        }
        return result;
    }

    public boolean postalServiceJobIsDirty() {
        return !valuesAreEqual(postalServiceJobInitVal(), getPostalServiceJob());
    }

    public boolean postalServiceJobIsSet() {
        return _postalServiceJobIsSet;
    }

    public de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplicationInitVal() {
        de.smava.webapp.applicant.brokerage.domain.LoanApplication result;
        if (_loanApplicationIsSet) {
            result = _loanApplicationInitVal;
        } else {
            result = getLoanApplication();
        }
        return result;
    }

    public boolean loanApplicationIsDirty() {
        return !valuesAreEqual(loanApplicationInitVal(), getLoanApplication());
    }

    public boolean loanApplicationIsSet() {
        return _loanApplicationIsSet;
    }


    public String brokerageApplicationsInitVal() {
        String result;
        if (_brokerageApplicationsIsSet) {
            result = _brokerageApplicationsInitVal;
        } else {
            result = getBrokerageApplications();
        }
        return result;
    }

    public boolean brokerageApplicationsIsDirty() {
        return !valuesAreEqual(brokerageApplicationsInitVal(), getBrokerageApplications());
    }

    public boolean brokerageApplicationsIsSet() {
        return _brokerageApplicationsIsSet;
    }

}
