package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author Jakub
 * @since 06.06.2016.
 */
public class PsdAdditionalData extends AbstractAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_PSD;
    }
}
