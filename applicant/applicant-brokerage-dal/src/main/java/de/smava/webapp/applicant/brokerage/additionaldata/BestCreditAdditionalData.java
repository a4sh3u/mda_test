package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;

/**
 * @author KF
 * @since 10.08.2016.
 */
public class BestCreditAdditionalData extends AbstractAdditionalData {

    /**
     * {@link AdditionalData#retrieveBankName()}
     */
    @Override
    public String retrieveBankName() {
        return BrokerageBank.BANK_SANTANDER_BESTCREDIT;
    }
}
