//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank document view)}

import de.smava.webapp.applicant.brokerage.domain.BankDocumentView;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BankDocumentViews'.
 *
 * @author generator
 */
public interface BankDocumentViewDao extends BrokerageSchemaDao<BankDocumentView> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the bank document view identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BankDocumentView getBankDocumentView(Long id);

    /**
     * Saves the bank document view.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBankDocumentView(BankDocumentView bankDocumentView);

    /**
     * Deletes an bank document view, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the bank document view
     */
    void deleteBankDocumentView(Long id);

    /**
     * Retrieves all 'BankDocumentView' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BankDocumentView> getBankDocumentViewList();

    /**
     * Retrieves a page of 'BankDocumentView' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BankDocumentView> getBankDocumentViewList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BankDocumentView' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BankDocumentView> getBankDocumentViewList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BankDocumentView' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BankDocumentView> getBankDocumentViewList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BankDocumentView' instances.
     */
    long getBankDocumentViewCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(bank document view)}
    //
    // insert custom methods here

    Collection<BankDocumentView> fetchBankDocumentByBankName(String bankName);

//    Collection<BankDocumentView> findBankDocumentList(String bankName, String extRefNumber, Long customerNumber,
//                                                      String firstName, String lastName,
//                                                      Date offerDateFrom, Date offerDateTo,
//                                                      Integer offset, Integer limit, final String sortBy, final Boolean descending);

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
