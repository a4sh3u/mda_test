//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application request data)}

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationRequestData;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageApplicationRequestDataEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

                                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BrokerageApplicationRequestDatas'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBrokerageApplicationRequestData
    extends BrokerageEntity    implements BrokerageApplicationRequestDataEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBrokerageApplicationRequestData.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BrokerageApplicationRequestData)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setType(((BrokerageApplicationRequestData) oldEntity).getType());    
        this.setBrokerageApplication(((BrokerageApplicationRequestData) oldEntity).getBrokerageApplication());    
        this.setBrokerageMultiStatusRequestData(((BrokerageApplicationRequestData) oldEntity).getBrokerageMultiStatusRequestData());    
        this.setRequest(((BrokerageApplicationRequestData) oldEntity).getRequest());    
        this.setResponse(((BrokerageApplicationRequestData) oldEntity).getResponse());    
        this.setState(((BrokerageApplicationRequestData) oldEntity).getState());    
        this.setMonthlyRate(((BrokerageApplicationRequestData) oldEntity).getMonthlyRate());    
        this.setEffectiveInterest(((BrokerageApplicationRequestData) oldEntity).getEffectiveInterest());    
        this.setAmount(((BrokerageApplicationRequestData) oldEntity).getAmount());    
        this.setDuration(((BrokerageApplicationRequestData) oldEntity).getDuration());    
        this.setRdiType(((BrokerageApplicationRequestData) oldEntity).getRdiType());    
        this.setResponseDate(((BrokerageApplicationRequestData) oldEntity).getResponseDate());
        this.setHttpState(((BrokerageApplicationRequestData) oldEntity).getHttpState());
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageApplicationRequestData)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getType(), ((BrokerageApplicationRequestData) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((BrokerageApplicationRequestData) otherEntity).getBrokerageApplication());    
        equals = equals && valuesAreEqual(this.getBrokerageMultiStatusRequestData(), ((BrokerageApplicationRequestData) otherEntity).getBrokerageMultiStatusRequestData());    
        equals = equals && valuesAreEqual(this.getRequest(), ((BrokerageApplicationRequestData) otherEntity).getRequest());    
        equals = equals && valuesAreEqual(this.getResponse(), ((BrokerageApplicationRequestData) otherEntity).getResponse());    
        equals = equals && valuesAreEqual(this.getState(), ((BrokerageApplicationRequestData) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getMonthlyRate(), ((BrokerageApplicationRequestData) otherEntity).getMonthlyRate());    
        equals = equals && valuesAreEqual(this.getEffectiveInterest(), ((BrokerageApplicationRequestData) otherEntity).getEffectiveInterest());    
        equals = equals && valuesAreEqual(this.getAmount(), ((BrokerageApplicationRequestData) otherEntity).getAmount());    
        equals = equals && valuesAreEqual(this.getDuration(), ((BrokerageApplicationRequestData) otherEntity).getDuration());    
        equals = equals && valuesAreEqual(this.getRdiType(), ((BrokerageApplicationRequestData) otherEntity).getRdiType());    
        equals = equals && valuesAreEqual(this.getResponseDate(), ((BrokerageApplicationRequestData) otherEntity).getResponseDate());    
        equals = equals && valuesAreEqual(this.getHttpState(), ((BrokerageApplicationRequestData) otherEntity).getHttpState());

        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(brokerage application request data)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

