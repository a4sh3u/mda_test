package de.smava.webapp.applicant.brokerage.domain;

import de.smava.webapp.applicant.brokerage.domain.history.BrokerageFilterReasonMultiValHistory;
import de.smava.webapp.applicant.brokerage.type.SmarketFilterReasonLevel;

/**
 * @author Adam Budziński
 * @since 05.09.2017
 */
public class BrokerageFilterReasonMultiVal extends BrokerageFilterReasonMultiValHistory {

    private LoanApplication _loanApplication;
    private BrokerageApplication _brokerageApplication;
    private Long _applicantId;
    private SmarketFilterReasonLevel _filterLevel;
    private BrokerageApplicationFilterReason _filterReason;

    public void setLoanApplication(LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }

    public LoanApplication getLoanApplication() {
        return _loanApplication;
    }

    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }

    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }

    public void setApplicantId(Long applicantId) {

        if (!applicantIdIsSet()) {
            _applicantIdIsSet = true;
            _applicantIdInitVal = getApplicantId();
        }
        registerChange("applicantId", _applicantIdInitVal, applicantId);
        _applicantId = applicantId;
    }

    public Long getApplicantId() {
        return _applicantId;
    }

    public void setFilterLevel(SmarketFilterReasonLevel filterLevel) {

        if (!filterLevelIsSet()) {
            _filterLevelIsSet = true;
            _filterLevelInitVal = getFilterLevel();
        }
        registerChange("filterLevel", _applicantIdInitVal, filterLevel);
        _filterLevel = filterLevel;
    }

    public SmarketFilterReasonLevel getFilterLevel() {
        return _filterLevel;
    }

    public void setFilterReason(BrokerageApplicationFilterReason filterReason) {
        _filterReason = filterReason;
    }

    public BrokerageApplicationFilterReason getFilterReason() {
        return _filterReason;
    }
}