package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.AdvisorGroupAssignmentDao;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroup;
import de.smava.webapp.applicant.brokerage.domain.AdvisorGroupAssignment;
import de.smava.webapp.applicant.brokerage.dto.AdvisorGroupAssignmentExpanded;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'AdvisorGroupAssignments'.
 */
@Repository
public class JdoAdvisorGroupAssignmentDao extends JdoGenericDao<AdvisorGroupAssignment> implements AdvisorGroupAssignmentDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAdvisorGroupAssignmentDao.class);

    private static final String CLASS_NAME = "AdvisorGroupAssignment";
    private static final String STRING_GET = "get";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";

    /**
     * Returns an attached copy of the advisor group assignment identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public AdvisorGroupAssignment load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        AdvisorGroupAssignment result = getPersistenceManager().getObjectById(AdvisorGroupAssignment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
        return getPersistenceManager().getObjectById(AdvisorGroupAssignment.class, id) != null;
    }

    /**
     * Saves the advisor group assignment.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(AdvisorGroupAssignment advisorGroupAssignment) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAdvisorGroupAssignment: " + advisorGroupAssignment);
        }
        return saveEntity(advisorGroupAssignment);
    }

    /**
     * Deletes an advisor group assignment, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the advisor group assignment
     */
    public void deleteAdvisorGroupAssignment(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAdvisorGroupAssignment: " + id);
        }
        final PersistenceManager pm = getPersistenceManager();
        AdvisorGroupAssignment entity = pm.getObjectById(AdvisorGroupAssignment.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Deleting AdvisorGroupAssignment: " + entity);
        }

        pm.deletePersistent(entity);
        pm.evict(entity);
    }

    /**
     * Retrieves all 'AdvisorGroupAssignment' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<AdvisorGroupAssignment> getAdvisorGroupAssignmentList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<AdvisorGroupAssignment> result = (Collection<AdvisorGroupAssignment>) getPersistenceManager().newQuery(AdvisorGroupAssignment.class).execute();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<AdvisorGroupAssignmentExpanded> getActiveAdvisorGroupAssignments() {
        Query query = getPersistenceManager().newQuery("javax.jdo.query.SQL",
                "select ga.id as id, ga.advisor_id as \"advisorId\", " +
                    "ga.advisor_group_id as \"groupId\", g.name as name, g.condition as condition, " +
                    "g.priority as priority " +
                    "from brokerage.advisor_group_assignment ga " +
                    "join brokerage.advisor_group g on g.id = ga.advisor_group_id " +
                    "where ga.expiration_date is null;");
        query.setResultClass(AdvisorGroupAssignmentExpanded.class);
        return (Collection<AdvisorGroupAssignmentExpanded>) query.execute();
    }

    @Override
    public void expireAdvisorGroupAssignment(Long advisorId, Long advisorGroupId) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroupAssignment.class, "expireAdvisorGroupAssignment");

        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, advisorId);
        parameters.put(2, advisorGroupId);

        query.executeWithMap(parameters);
    }

    public Collection<AdvisorGroupAssignment> findByAdvisorAndGroup(Long advisorId, AdvisorGroup advisorGroup){
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroupAssignment.class, "findByAdvisorAndGroup");
        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, advisorGroup.getId());
        parameters.put(2, advisorId);


        Collection<AdvisorGroupAssignment> advisorGroupAssignments = (Collection<AdvisorGroupAssignment>) query.executeWithMap(parameters);
        if (advisorGroupAssignments == null) {
            return new ArrayList<AdvisorGroupAssignment>();
        }
        return advisorGroupAssignments;
    }
    public Collection<AdvisorGroupAssignment> findByGroup(Long advisorGroupId) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorGroupAssignment.class, "findByGroup");
        Map<Integer, Object> parameters = new HashMap<Integer, Object>();
        parameters.put(1, advisorGroupId);

        Collection<AdvisorGroupAssignment> advisorGroupAssignments = (Collection<AdvisorGroupAssignment>) query.executeWithMap(parameters);
        if (advisorGroupAssignments == null) {
            return new ArrayList<AdvisorGroupAssignment>();
        }
        return advisorGroupAssignments;
    }

}
