package de.smava.webapp.applicant.brokerage.domain.interfaces;

import java.util.Date;

/**
 * @author Wojciech Teprek
 * @since 15.01.2018
 */
public interface PostalServiceJobEntityInterface {

    void setJobStartDate(Date date);
    Date getJobStartDate();
    void setJobEndDate(Date date);
    Date getJobEndDate();
    void setPeriodFrom(Date date);
    Date getPeriodFrom();
    void setPeriodTo(Date date);
    Date getPeriodTo();
}
