package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.CheckoutFlow;
import de.smava.webapp.applicant.brokerage.domain.CheckoutFlowPage;

import java.util.Set;


/**
 * The domain object that represents 'CheckoutFlows'.
 *
 * @author generator
 */
public interface CheckoutFlowEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'first page id'.
     *
     * 
     *
     */
    void setFirstPageId(Long firstPageId);

    /**
     * Returns the property 'first page id'.
     *
     * 
     *
     */
    Long getFirstPageId();
    /**
     * Setter for the property 'checkout block'.
     *
     * 
     *
     */
    void setCheckoutBlock(de.smava.webapp.applicant.brokerage.domain.CheckoutBlock checkoutBlock);

    /**
     * Returns the property 'checkout block'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.brokerage.domain.CheckoutBlock getCheckoutBlock();
    /**
     * Setter for the property 'checkout flow pages'.
     *
     * 
     *
     */
    void setCheckoutFlowPages(Set<CheckoutFlowPage> checkoutFlowPages);

    /**
     * Returns the property 'checkout flow pages'.
     *
     * 
     *
     */
    Set<CheckoutFlowPage> getCheckoutFlowPages();
    /**
     * Helper method to get reference of this object as model type.
     */
    CheckoutFlow asCheckoutFlow();
}
