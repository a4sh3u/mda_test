//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank household calculation)}

import de.smava.webapp.applicant.brokerage.domain.BankHouseholdCalculation;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BankHouseholdCalculationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankHouseholdCalculations'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * Bank specific household calculation
 *
 * @author generator
 */
public abstract class AbstractBankHouseholdCalculation
    extends BrokerageEntity    implements BankHouseholdCalculationEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBankHouseholdCalculation.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BankHouseholdCalculation)) {
            return;
        }
        
        this.setBrokerageBank(((BankHouseholdCalculation) oldEntity).getBrokerageBank());    
        this.setHouseholdCalculation(((BankHouseholdCalculation) oldEntity).getHouseholdCalculation());    
        this.setBankExpression(((BankHouseholdCalculation) oldEntity).getBankExpression());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BankHouseholdCalculation)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getBrokerageBank(), ((BankHouseholdCalculation) otherEntity).getBrokerageBank());    
        equals = equals && valuesAreEqual(this.getHouseholdCalculation(), ((BankHouseholdCalculation) otherEntity).getHouseholdCalculation());    
        equals = equals && valuesAreEqual(this.getBankExpression(), ((BankHouseholdCalculation) otherEntity).getBankExpression());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(bank household calculation)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

