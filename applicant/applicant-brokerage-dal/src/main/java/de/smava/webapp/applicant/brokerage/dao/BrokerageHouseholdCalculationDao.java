//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage household calculation)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageHouseholdCalculations'.
 *
 * @author generator
 */
public interface BrokerageHouseholdCalculationDao extends BrokerageSchemaDao<BrokerageHouseholdCalculation> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage household calculation identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageHouseholdCalculation getBrokerageHouseholdCalculation(Long id);

    /**
     * Saves the brokerage household calculation.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageHouseholdCalculation(BrokerageHouseholdCalculation brokerageHouseholdCalculation);

    /**
     * Deletes an brokerage household calculation, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage household calculation
     */
    void deleteBrokerageHouseholdCalculation(Long id);

    /**
     * Retrieves all 'BrokerageHouseholdCalculation' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageHouseholdCalculation> getBrokerageHouseholdCalculationList();

    /**
     * Retrieves a page of 'BrokerageHouseholdCalculation' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageHouseholdCalculation> getBrokerageHouseholdCalculationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageHouseholdCalculation' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageHouseholdCalculation> getBrokerageHouseholdCalculationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageHouseholdCalculation' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageHouseholdCalculation> getBrokerageHouseholdCalculationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageHouseholdCalculation' instances.
     */
    long getBrokerageHouseholdCalculationCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage household calculation)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
