package de.smava.webapp.applicant.brokerage.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by asaadat on 22.03.16.
 */
public enum DocumentStatus {

    CREATED,
    ACCEPTED,
    REJECTED,
    DELETED,
    OUTDATED,
    OTHER;

    /**
     * allowed transition
     */

    private static List<DocumentStatus> FROM_CREATED = Arrays.asList(ACCEPTED, REJECTED, OUTDATED);
    private static List<DocumentStatus> FROM_ACCEPTED = Arrays.asList(REJECTED, OUTDATED);
    private static List<DocumentStatus> FROM_REJECTED = Arrays.asList(ACCEPTED, OUTDATED);
    private static List<DocumentStatus> FROM_OUTDATED = Arrays.asList(ACCEPTED, REJECTED);


    public static String convert(DocumentStatus documentStatus) {
        switch (documentStatus) {
            case CREATED:
                return "Neu";
            case ACCEPTED:
                return "OK";
            case REJECTED:
                return "Abgelehnt";
            case DELETED:
                return "Gelöscht";
            case OUTDATED:
                return "Alt";
            default:
                return "Other";

        }
    }

    public static DocumentStatus convert(String documentStatusStr) {
        if (documentStatusStr.equalsIgnoreCase("Neu")) {
            return CREATED;
        } else if (documentStatusStr.equalsIgnoreCase("OK")) {
            return ACCEPTED;
        } else if (documentStatusStr.equalsIgnoreCase("Abgelehnt")) {
            return REJECTED;
        } else if (documentStatusStr.equalsIgnoreCase("Gelöscht")) {
            return DELETED;
        } else if (documentStatusStr.equalsIgnoreCase("Alt")) {
            return OUTDATED;
        } else {
            return OTHER;
        }
    }


    public boolean isTransitionAllowed(DocumentStatus documentStatus) {
        if(this.equals(documentStatus)) return true;
        switch (this) {
            case CREATED:
                return FROM_CREATED.contains(documentStatus);
            case ACCEPTED:
                return FROM_ACCEPTED.contains(documentStatus);
            case REJECTED:
                return FROM_REJECTED.contains(documentStatus);
            case OUTDATED:
                return FROM_OUTDATED.contains(documentStatus);
            default:
                return false;

        }
    }


}
