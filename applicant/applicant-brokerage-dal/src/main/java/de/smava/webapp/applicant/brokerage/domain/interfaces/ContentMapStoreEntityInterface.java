package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.applicant.brokerage.domain.ContentMapStore;


/**
 * The domain object that represents 'ContentMapStores'.
 *
 * @author generator
 */
public interface ContentMapStoreEntityInterface {

    /**
     * Setter for the property 'clazz name'.
     *
     * 
     *
     */
    void setClazzName(String clazzName);

    /**
     * Returns the property 'clazz name'.
     *
     * 
     *
     */
    String getClazzName();
    /**
     * Setter for the property 'clazz id'.
     *
     * 
     *
     */
    void setClazzId(Long clazzId);

    /**
     * Returns the property 'clazz id'.
     *
     * 
     *
     */
    Long getClazzId();
    /**
     * Setter for the property 'key'.
     *
     * 
     *
     */
    void setKey(String key);

    /**
     * Returns the property 'key'.
     *
     * 
     *
     */
    String getKey();
    /**
     * Setter for the property 'value'.
     *
     * 
     *
     */
    void setValue(String value);

    /**
     * Returns the property 'value'.
     *
     * 
     *
     */
    String getValue();
    /**
     * Helper method to get reference of this object as model type.
     */
    ContentMapStore asContentMapStore();
}
