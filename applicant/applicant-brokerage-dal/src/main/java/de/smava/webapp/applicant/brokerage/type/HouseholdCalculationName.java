package de.smava.webapp.applicant.brokerage.type;

/**
 * @author pharbert
 * @since 21.03.16.
 */
public enum HouseholdCalculationName {
    // incomes
    EMPLOYED_MAIN_INCOME("employedMainIncome"),
    SELF_EMPLOYED_MAIN_INCOME("selfEmployedMainIncome"),
    EMPLOYED_ADDITIONAL_INCOME("employedAdditionalIncome"),
    SELF_EMPLOYED_ADDITIONAL_INCOME("selfEmployedAdditionalIncome"),
    APPRENTICE_INCOME("apprenticeIncome"),
    SHORT_TIME_WORK_INCOME("shortTimeWorkIncome"),
    MINI_JOB_INCOME("miniJobIncome"),
    MATERNITY_LEAVE_INCOME("maternityLeaveIncome"),
    PENSION("pension"),
    COMPANY_PENSION("companyPension"),
    PRIVATE_PENSION("privatePension"),
    WIDOW_PENSION("widowPension"),
    ORPHAN_PENSION("orphanPension"),
    HALF_ORPHAN_PENSION("halfOrphanPension"),
    DISABILITY_PENSION("disabilityPension"),
    DISABILITY_BENEFITS("disabilityBenefits"),
    JOB_DISABILITY_PENSION("jobDisabilityPension"),
    PUBLIC_INJURY_INSURANCE_PENSION("publicInjuryInsurancePension"),
    PRIVATE_INJURY_INSURANCE_PENSION("privateInjuryInsurancePension"),
    JOB_ACCIDENT_PENSION("jobAccidentPension"),
    INVALIDITY_PENSION("invalidityPension"),
    ROYALITIES("royalities"),
    COMMISSIONS("commissions"),
    COMMISSIONS_ADDITIONAL("commissionsAdditional"),
    BUSINESS_EXPENSES("businessExpenses"),
    OVERTIME("overtime"),
    SICK_PAY("sickPay"),
    ATTENDANCE_BENEFITS("attendanceBenefits"),
    BEING_BLIND_BENEFITS("beingBlindBenefits"),
    ACCOMMODATION_BENEFITS("accommodationBenefits"),
    PARENTAL_BENEFITS("parentalBenefits"),
    CHILD_BENEFITS("childBenefits"),
    CHILD_SUPPORT_INCOME("childSupportIncome"),
    SPOUSE_SUPPORT_INCOME("spouseSupportIncome"),
    NET_RENTAL_INCOME("netRentalIncome"),
    SPOUSE_PART_OF_RENT("spousePartOfRent"),
    PHOTOVOLTAICS_INCOME("photovoltaicsIncome"),
    INTEREST_INCOME("interestIncome"),
    CAPITAL_FORMING_BENEFITS("capitalFormingBenefits"),
    INSOLVENCY_MONEY("insolvencyMoney"),
    PROFESSIONAL_TRAINING_SUPPORT("professionalTrainingSupport"),
    SCHOLARSHIPS("scholarships"),
    ENTREPENEUR_LOAN("entrepeneurLoan"),
    UNEMPLOYMENT_BENEFITS("unemploymentBenefits"),
    SOCIAL_SECURITY_BENEFITS("socialSecurityBenefits"),

    // expenses
    BRUT_RENT_EXPENSE("brutRentExpense"),
    MANDATORY_PRIVATE_HEALTH_INSURANCE("mandatoryPrivateHealthInsurance"),
    VOLUNTARY_PRIVATE_HEALTH_INSURANCE("voluntaryPrivateHealthInsurance"),
    PRIVATE_NURSING_INSURANCE("privateNursingInsurance"),
    INSURANCE_EXPENSES("insuranceExpenses"),
    CHILD_SUPPORT_EXPENSES("childSupportExpenses"),
    SPOUSE_SUPPORT_EXPENSES("spouseSupportExpenses"),
    THIRD_PARTY_DEBT_LEASING("thirdPartyDebtLeasing"),
    THIRD_PARTY_DEBT_LIFE_INSURANCE("thirdPartyDebtLifeInsurance"),
    THIRD_PARTY_DEBT_BUILDING_SAVE_LOAN("thirdPartyDebtBuildingSaveLoan"),
    THIRD_PARTY_DEBT_INSTALMENT_LOAN("thirdPartyDebtInstalmentLoan"),
    THIRD_PARTY_DEBT_CONSUMER_CREDIT("thirdPartyDebtConsumerCredit"),
    THIRD_PARTY_DEBT_EMPLOYER_LOAN("thirdPartyDebtEmployerLoan"),
    THIRD_PARTY_DEBT_NULL_INTEREST_LOAN("thirdPartyDebtNullInterestLoan"),
    THIRD_PARTY_DEBT_BALLOON_PAYMENT_MORTGAGE("thirdPartyDebtBalloonPaymentMortgage"),
    THIRD_PARTY_DEBT_PUBLIC_OFFICIAL_LOAN("thirdPartyDebtPublicOfficialLoan"),
    THIRD_PARTY_DEBT_BUSINESS_LOAN("thirdPartyDebtBusinessLoan"),
    GLOBAL_CREDIT("globalCredit"),
    CREDITCARD_CREDIT("creditcardCredit"),
    OVERDRAFT_CREDIT("overdraftCredit"),
    THIRD_PARTY_DEBT_ESTATE_FINANCING_NOT_RENTED("thirdPartyDebtEstateFinancingNotRented"),
    THIRD_PARTY_DEBT_ESTATE_FINANCING_FULLY_RENTED("thirdPartyDebtEstateFinancingFullyRented"),
    THIRD_PARTY_DEBT_CAR_CREDIT("thirdPartyDebtCarCredit"),
    UTILITIES_AS_RENTER("utilitiesAsRenter"),
    UTILITIES_FOR_OWNED_ESTATE("utilitiesForOwnedEstate"),
    COST_OF_CARS("costOfCars"),
    COST_OF_MOTORBIKES("costOfMotorbikes"),
    LIVING_COSTS("livingCosts"),
    SEPARATED_MARITAL_STATE_COSTS("separatedMaritalStateCosts"),

    // calculations
    DISPOSABLE_INCOME("disposableIncome"),
    INDEBTEDNESS_RATE("indebtednessRate");

    private final String value;

    HouseholdCalculationName(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HouseholdCalculationName fromValue(String v) {
        for (HouseholdCalculationName c : HouseholdCalculationName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}