package de.smava.webapp.applicant.brokerage.domain.interfaces;



import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.applicant.brokerage.type.DirectionType;
import de.smava.webapp.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.BrokerageJournal;
import de.smava.webapp.brokerage.domain.LoanApplication;

import java.util.Date;


/**
 * The domain object that represents 'BrokerageJournals'.
 *
 * @author generator
 */
public interface BrokerageJournalEntityInterface {

    void setCreationDate(Date creationDate);

    Date getCreationDate();

    void setSendUntilDate(Date sendUntilDate);

    Date getSendUntilDate();

    void setType(de.smava.webapp.applicant.brokerage.type.BrokerageJournalType type);

    de.smava.webapp.applicant.brokerage.type.BrokerageJournalType getType();

    void setCustomerNumber(Long customerNumber);

    Long getCustomerNumber();

    void setAdvisor(Advisor advisor);

    Advisor getAdvisor();

    void setLoanApplication(LoanApplication loanApplication);

    LoanApplication getLoanApplication();

    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    BrokerageApplication getBrokerageApplication();

    void setSendByPost(Boolean sendByPost);

    Boolean getSendByPost();

    void setText(String text);

    String getText();

    void setSubject(String subject);

    String getSubject();

    DirectionType getDirection();

    void setDirection(DirectionType direction);

    String getExternalRefId();

    void setExternalRefId(String externalRefId);

    BrokerageJournal asBrokerageJournal();
}
