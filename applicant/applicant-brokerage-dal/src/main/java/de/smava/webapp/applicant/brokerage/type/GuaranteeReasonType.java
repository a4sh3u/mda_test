package de.smava.webapp.applicant.brokerage.type;

/**
 * Created by dkeller on 17.11.15.
 */
public enum GuaranteeReasonType {
    RE_RESULT,
    BANK
}
