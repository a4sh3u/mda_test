package de.smava.webapp.applicant.brokerage.domain.abstracts;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplicationDocument;
import de.smava.webapp.applicant.brokerage.domain.interfaces.BrokerageApplicationDocumentEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;

/**
 * The domain object that represents 'BrokerageApplicationDocuments'.
 * This class is actually intended to contain all custom logic of entities that
 * relies on existing entity fields
 *
 * @author Adam Tomecki
 * @since 26.10.2017
 */
public abstract class AbstractBrokerageApplicationDocument extends BrokerageEntity
        implements BrokerageApplicationDocumentEntityInterface {

    /**
     * {@inheritDoc}
     */
    @Override
    public void copyFromOldEntity(Entity oldEntity) {
        if (!(oldEntity instanceof BrokerageApplicationDocument)) {
            return;
        }

        BrokerageApplicationDocument oldBrokerageApplicationDocument = (BrokerageApplicationDocument) oldEntity;

        setCreationDate(oldBrokerageApplicationDocument.getCreationDate());
        setBrokerageApplication(oldBrokerageApplicationDocument.getBrokerageApplication());
        setContentType(oldBrokerageApplicationDocument.getContentType());
        setDispatchType(oldBrokerageApplicationDocument.getDispatchType());
        setFileStorageId(oldBrokerageApplicationDocument.getFileStorageId());
        setName(oldBrokerageApplicationDocument.getName());
        setNotes(oldBrokerageApplicationDocument.getNotes());
        setState(oldBrokerageApplicationDocument.getState());
        setSubject(oldBrokerageApplicationDocument.getSubject());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BrokerageApplicationDocument)) {
            equals = false;
        }

        equals = equals && valuesAreEqual(this.getCreationDate(), ((BrokerageApplicationDocument) otherEntity).getCreationDate());
        equals = equals && valuesAreEqual(this.getBrokerageApplication(), ((BrokerageApplicationDocument) otherEntity).getBrokerageApplication());
        equals = equals && valuesAreEqual(this.getContentType(), ((BrokerageApplicationDocument) otherEntity).getContentType());
        equals = equals && valuesAreEqual(this.getDispatchType(), ((BrokerageApplicationDocument) otherEntity).getDispatchType());
        equals = equals && valuesAreEqual(this.getFileStorageId(), ((BrokerageApplicationDocument) otherEntity).getFileStorageId());
        equals = equals && valuesAreEqual(this.getName(), ((BrokerageApplicationDocument) otherEntity).getName());
        equals = equals && valuesAreEqual(this.getNotes(), ((BrokerageApplicationDocument) otherEntity).getNotes());
        equals = equals && valuesAreEqual(this.getState(), ((BrokerageApplicationDocument) otherEntity).getState());
        equals = equals && valuesAreEqual(this.getSubject(), ((BrokerageApplicationDocument) otherEntity).getSubject());

        return equals;
    }
}
