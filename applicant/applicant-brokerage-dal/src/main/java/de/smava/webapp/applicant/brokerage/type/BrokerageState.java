package de.smava.webapp.applicant.brokerage.type;

import java.util.*;

import static de.smava.webapp.applicant.brokerage.type.BrokerageState.Label.*;

/**
 * @author bvoss
 *
 */
public enum BrokerageState {

	APPLICATION_PAYOUT ("APPLICATION_PAYOUT", PAYING_OUT),
	APPLICATION_ACCEPTED ("APPLICATION_ACCEPTED", WITH_OFFERS),
	APPLICATION_DOCUMENTS_RECEIVED ("APPLICATION_DOCUMENTS_RECEIVED", WITH_OFFERS),
	APPLICATION_DOCUMENTS_MISSING ("APPLICATION_DOCUMENTS_MISSING", WITH_OFFERS),
	APPLICATION_OBJECTED ("APPLICATION_OBJECTED", WITH_OFFERS),
	APPLICATION_APPLIED ("APPLICATION_APPLIED", WITH_OFFERS),
	APPLICATION_PENDING ("APPLICATION_PENDING", PENDING),
	APPLICATION_CANCELLED ("APPLICATION_CANCELLED", NO_OFFERS),
	APPLICATION_DENIED_OFFLINE ("APPLICATION_DENIED_OFFLINE", NO_OFFERS),
	APPLICATION_DENIED ("APPLICATION_DENIED", NO_OFFERS),
	APPLICATION_UNKNOWN ("APPLICATION_UNKNOWN", PENDING),
	APPLICATION_FAILED ("APPLICATION_FAILED", NO_OFFERS),
	APPLICATION_NOT_PROCESSED ("APPLICATION_NOT_PROCESSED", NO_OFFERS),
	APPLICATION_SENDING ("APPLICATION_SENDING", PENDING),
	APPLICATION_FILTERED ("APPLICATION_FILTERED", NO_OFFERS),
	APPLICATION_PRECHECK_GREEN ("APPLICATION_PRECHECK_GREEN", NO_OFFERS),
	APPLICATION_PRECHECK_YELLOW ("APPLICATION_PRECHECK_YELLOW", NO_OFFERS),
	APPLICATION_INCOMPLETE ("APPLICATION_INCOMPLETE", SHORTLEAD),
	APPLICATION_WAITING_FOR_SCORE ("APPLICATION_WAITING_FOR_SCORE", NO_OFFERS);

	public enum Label {
		PAYING_OUT,
		SHORTLEAD,
		NO_OFFERS,
		WITH_OFFERS,
		PENDING,
		UNKNOWN
	}

    private final String value;
	private final Label label;

	BrokerageState(String v, Label label) {
		value = v;
		this.label = label;
	}

    public String value() {
        return value;
    }

	public Label label() {
		return label;
	}

    public static BrokerageState fromValue(String v) {
        for (BrokerageState c : BrokerageState.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

	public static final Collection<BrokerageState> STATE_MESSAGE_REQUIRED_STATES =
			Collections.unmodifiableCollection(Arrays.asList(
					APPLICATION_PAYOUT,
					APPLICATION_ACCEPTED,
					APPLICATION_DOCUMENTS_RECEIVED,
					APPLICATION_DOCUMENTS_MISSING,
					APPLICATION_APPLIED,
					APPLICATION_UNKNOWN,
					APPLICATION_DENIED,
					APPLICATION_FAILED,
					APPLICATION_PENDING,
					APPLICATION_DENIED_OFFLINE,
					APPLICATION_CANCELLED,
					APPLICATION_OBJECTED));

	public static final Collection<BrokerageState> LIMITED_STATE_MESSAGE_REQUIRED_STATES =
			Collections.unmodifiableCollection(Arrays.asList(	APPLICATION_PAYOUT,
					APPLICATION_ACCEPTED,
					APPLICATION_DOCUMENTS_RECEIVED,
					APPLICATION_DOCUMENTS_MISSING,
					APPLICATION_APPLIED,
					APPLICATION_UNKNOWN,
					APPLICATION_FAILED,
					APPLICATION_PENDING,
					APPLICATION_CANCELLED,
					APPLICATION_OBJECTED));
	

	static List<BrokerageState> priorityList;
	static {
		priorityList = new ArrayList<BrokerageState>();
		priorityList.add(APPLICATION_PAYOUT);
		priorityList.add(APPLICATION_ACCEPTED);
		priorityList.add(APPLICATION_DOCUMENTS_RECEIVED);
		priorityList.add(APPLICATION_DOCUMENTS_MISSING);
		priorityList.add(APPLICATION_OBJECTED);
		priorityList.add(APPLICATION_APPLIED); 
		priorityList.add(APPLICATION_PENDING); 
		priorityList.add(APPLICATION_CANCELLED); 
		priorityList.add(APPLICATION_DENIED_OFFLINE);
		priorityList.add(APPLICATION_DENIED); 
		priorityList.add(APPLICATION_PRECHECK_GREEN);
		priorityList.add(APPLICATION_PRECHECK_YELLOW);
		priorityList.add(APPLICATION_UNKNOWN);
		priorityList.add(APPLICATION_FAILED); 
		priorityList.add(APPLICATION_NOT_PROCESSED);
		priorityList.add(APPLICATION_SENDING);
		priorityList.add(APPLICATION_WAITING_FOR_SCORE);
		priorityList.add(APPLICATION_FILTERED);
		
	}

    public static List<BrokerageState> getPriorityOrderedStates(){
		return priorityList;
	}
	
	public static Collection<BrokerageState> getSuccessfulStates() {
		Collection<BrokerageState> result = new ArrayList<BrokerageState>();
		result.add(APPLICATION_APPLIED);
		result.add(APPLICATION_ACCEPTED);

		return result;
	}

	public static Collection<BrokerageState> getMySmavaOfferStates() {
		Collection<BrokerageState> result = new ArrayList<BrokerageState>();
		result.add(APPLICATION_PAYOUT);
		result.add(APPLICATION_ACCEPTED);
		result.add(APPLICATION_DOCUMENTS_RECEIVED);
		result.add(APPLICATION_DOCUMENTS_MISSING);
		result.add(APPLICATION_OBJECTED);
		result.add(APPLICATION_APPLIED);
		result.add(APPLICATION_PENDING);
		result.add(APPLICATION_DENIED_OFFLINE);
		result.add(APPLICATION_UNKNOWN);
		result.add(APPLICATION_SENDING);

		return result;
	}
	
	public static Collection<BrokerageState> getCasiOfferStates() {
		Collection<BrokerageState> result = new ArrayList<BrokerageState>();
		result.add(APPLICATION_PAYOUT);
		result.add(APPLICATION_ACCEPTED);
		result.add(APPLICATION_DOCUMENTS_RECEIVED);
		result.add(APPLICATION_DOCUMENTS_MISSING);
		result.add(APPLICATION_OBJECTED);
		result.add(APPLICATION_APPLIED);
		result.add(APPLICATION_PENDING);
		result.add(APPLICATION_CANCELLED);
		result.add(APPLICATION_DENIED_OFFLINE);
		result.add(APPLICATION_UNKNOWN);
		result.add(APPLICATION_SENDING);
		result.add(APPLICATION_PRECHECK_GREEN);
		result.add(APPLICATION_PRECHECK_YELLOW);

		return result;
	}

	public static Collection<BrokerageState> getFailedStates() {
		Collection<BrokerageState> result = new ArrayList<BrokerageState>();
		result.add(APPLICATION_DENIED);
		result.add(APPLICATION_FAILED);
		result.add(APPLICATION_FILTERED);

		return result;
	}

	public static Collection<BrokerageState> getPrecheckStates() {
		Collection<BrokerageState> result = new ArrayList<BrokerageState>();
		result.add(APPLICATION_PRECHECK_GREEN);
		result.add(APPLICATION_PRECHECK_YELLOW);

		return result;
	}

	public static Collection<BrokerageState> getBankAcceptedStates() {
		return Arrays.asList(
				BrokerageState.APPLICATION_APPLIED,
				BrokerageState.APPLICATION_ACCEPTED,
				BrokerageState.APPLICATION_DOCUMENTS_RECEIVED,
				BrokerageState.APPLICATION_DOCUMENTS_MISSING,
				BrokerageState.APPLICATION_PENDING
		);
	}
}
