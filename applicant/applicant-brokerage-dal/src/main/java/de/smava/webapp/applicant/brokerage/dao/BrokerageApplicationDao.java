//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(brokerage application)}

import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.BrokerageBank;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;
import java.util.Date;
import java.util.List;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'BrokerageApplications'.
 *
 * @author generator
 */
public interface BrokerageApplicationDao extends BrokerageSchemaDao<BrokerageApplication> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the brokerage application identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    BrokerageApplication getBrokerageApplication(Long id);

    /**
     * Saves the brokerage application.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveBrokerageApplication(BrokerageApplication brokerageApplication);

    /**
     * Deletes an brokerage application, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the brokerage application
     */
    void deleteBrokerageApplication(Long id);

    /**
     * Retrieves all 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<BrokerageApplication> getBrokerageApplicationList();

    /**
     * Retrieves a page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'BrokerageApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<BrokerageApplication> getBrokerageApplicationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'BrokerageApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<BrokerageApplication> getBrokerageApplicationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'BrokerageApplication' instances.
     */
    long getBrokerageApplicationCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(brokerage application)}
    //
    // insert custom methods here

    Collection<BrokerageApplication> fetchBrokerageApplicationByAccountAndBankIn(Long accountId,
                                                                                 List<BrokerageBank> brokerageBanks);

    Collection<BrokerageApplication> fetchBrokerageApplications(List<Long> brokerageApplicationIds);

    Collection<BrokerageApplication> fetchBrokerageApplicationByAccountAndBankInAndStateInDescOrder(Long accountId,
                                                                                                    List<BrokerageBank> brokerageBanks,
                                                                                                    List<BrokerageState> brokerageStates);

    Collection<BrokerageApplication> fetchBrokerageApplicationBankIdAndByExtRef(Long bankId, String extRefNumber);

    Collection<BrokerageApplication> fetchBrokerageApplicationsWithFailedDocRequestByBanks(List<BrokerageBank> brokerageBanks, Date date);

    Collection<BrokerageApplication> findBrokerageApplicationList(Account account, BrokerageBank bank, Collection<BrokerageState> states, Date startSearch, Date endSearch);

    Collection<BrokerageApplication> findBrokerageApplicationList(BrokerageBank bank, Collection<BrokerageState> states, Date startSearch, Date endSearch);

    Collection<BrokerageApplication> findBrokerageApplicationList(Account account, BrokerageBank bank, Collection<BrokerageState> states);

    Collection<Long> getRecentlyPaidoutBrokerageApplications(int minElapsedDays, int maxElapsedDays);

    Collection<BrokerageApplication> findBrokerageApplicationList(Collection<BrokerageBank> brokerageBanks, Collection<BrokerageState> states, Date startSearch, Date endSearch);

    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
