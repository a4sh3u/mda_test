package de.smava.webapp.applicant.brokerage.dao;

import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.brokerage.domain.PostalServiceMasterApplication;
/**
 * DAO interface for the domain object 'PostalServiceMasterApplications'.
 *
 * @author generator
 */
public interface PostalServiceMasterApplicationDao extends BrokerageSchemaDao<PostalServiceMasterApplication> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the vehicle information identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    PostalServiceMasterApplication getPostalServiceMasterApplication(Long id);

    /**
     * Saves the vehicle information.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long savePostalServiceMasterApplication(PostalServiceMasterApplication vehicleInformation);

    /**
     * Deletes an vehicle information, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the vehicle information
     */
    void deletePostalServiceMasterApplication(Long id);

    /**
     * Retrieves all 'PostalServiceMasterApplication' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<PostalServiceMasterApplication> getPostalServiceMasterApplicationList();

    /**
     * Retrieves a page of 'PostalServiceMasterApplication' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<PostalServiceMasterApplication> getPostalServiceMasterApplicationList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'PostalServiceMasterApplication' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<PostalServiceMasterApplication> getPostalServiceMasterApplicationList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'PostalServiceMasterApplication' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<PostalServiceMasterApplication> getPostalServiceMasterApplicationList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'PostalServiceMasterApplication' instances.
     */
    long getPostalServiceMasterApplicationCount();

}
