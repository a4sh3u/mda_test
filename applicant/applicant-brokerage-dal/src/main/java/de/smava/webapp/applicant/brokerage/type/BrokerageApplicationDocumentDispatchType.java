package de.smava.webapp.applicant.brokerage.type;

/**
 * Brokerage application document dispatch type
 *
 * @author Adam Tomecki
 * @since 26.10.2017
 */
public enum BrokerageApplicationDocumentDispatchType {
    EMAIL,
    DOWNLOAD
}
