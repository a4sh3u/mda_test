package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBrokerageBankConfiguration;




/**
 * The domain object that has all history aggregation related fields for 'BrokerageBankConfigurations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BrokerageBankConfigurationHistory extends AbstractBrokerageBankConfiguration {

    protected transient String _nameInitVal;
    protected transient boolean _nameIsSet;
    protected transient boolean _documentUploadInitVal;
    protected transient boolean _documentUploadIsSet;
    protected transient boolean _documentUploadViaSftpInitVal;
    protected transient boolean _documentUploadViaSftpIsSet;
    protected transient boolean _documentUploadViaPostInitVal;
    protected transient boolean _documentUploadViaPostIsSet;
    protected transient boolean _documentUploadViaBankApiInitVal;
    protected transient boolean _documentUploadViaBankApiIsSet;
    protected transient boolean _videoIdentInitVal;
    protected transient boolean _videoIdentIsSet;
    protected transient boolean _esignInitVal;
    protected transient boolean _esignIsSet;


	
    /**
     * Returns the initial value of the property 'name'.
     */
    public String nameInitVal() {
        String result;
        if (_nameIsSet) {
            result = _nameInitVal;
        } else {
            result = getName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'name'.
     */
    public boolean nameIsDirty() {
        return !valuesAreEqual(nameInitVal(), getName());
    }

    /**
     * Returns true if the setter method was called for the property 'name'.
     */
    public boolean nameIsSet() {
        return _nameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'document upload'.
     */
    public boolean documentUploadInitVal() {
        boolean result;
        if (_documentUploadIsSet) {
            result = _documentUploadInitVal;
        } else {
            result = getDocumentUpload();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'document upload'.
     */
    public boolean documentUploadIsDirty() {
        return !valuesAreEqual(documentUploadInitVal(), getDocumentUpload());
    }

    /**
     * Returns true if the setter method was called for the property 'document upload'.
     */
    public boolean documentUploadIsSet() {
        return _documentUploadIsSet;
    }
	
    /**
     * Returns the initial value of the property 'document upload via sftp'.
     */
    public boolean documentUploadViaSftpInitVal() {
        boolean result;
        if (_documentUploadViaSftpIsSet) {
            result = _documentUploadViaSftpInitVal;
        } else {
            result = getDocumentUploadViaSftp();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'document upload via sftp'.
     */
    public boolean documentUploadViaSftpIsDirty() {
        return !valuesAreEqual(documentUploadViaSftpInitVal(), getDocumentUploadViaSftp());
    }

    /**
     * Returns true if the setter method was called for the property 'document upload via sftp'.
     */
    public boolean documentUploadViaSftpIsSet() {
        return _documentUploadViaSftpIsSet;
    }
	
    /**
     * Returns the initial value of the property 'document upload via post'.
     */
    public boolean documentUploadViaPostInitVal() {
        boolean result;
        if (_documentUploadViaPostIsSet) {
            result = _documentUploadViaPostInitVal;
        } else {
            result = getDocumentUploadViaPost();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'document upload via post'.
     */
    public boolean documentUploadViaPostIsDirty() {
        return !valuesAreEqual(documentUploadViaPostInitVal(), getDocumentUploadViaPost());
    }

    /**
     * Returns true if the setter method was called for the property 'document upload via post'.
     */
    public boolean documentUploadViaPostIsSet() {
        return _documentUploadViaPostIsSet;
    }
	
    /**
     * Returns the initial value of the property 'document upload via bank api'.
     */
    public boolean documentUploadViaBankApiInitVal() {
        boolean result;
        if (_documentUploadViaBankApiIsSet) {
            result = _documentUploadViaBankApiInitVal;
        } else {
            result = getDocumentUploadViaBankApi();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'document upload via bank api'.
     */
    public boolean documentUploadViaBankApiIsDirty() {
        return !valuesAreEqual(documentUploadViaBankApiInitVal(), getDocumentUploadViaBankApi());
    }

    /**
     * Returns true if the setter method was called for the property 'document upload via bank api'.
     */
    public boolean documentUploadViaBankApiIsSet() {
        return _documentUploadViaBankApiIsSet;
    }
	
    /**
     * Returns the initial value of the property 'video ident'.
     */
    public boolean videoIdentInitVal() {
        boolean result;
        if (_videoIdentIsSet) {
            result = _videoIdentInitVal;
        } else {
            result = getVideoIdent();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'video ident'.
     */
    public boolean videoIdentIsDirty() {
        return !valuesAreEqual(videoIdentInitVal(), getVideoIdent());
    }

    /**
     * Returns true if the setter method was called for the property 'video ident'.
     */
    public boolean videoIdentIsSet() {
        return _videoIdentIsSet;
    }

    /**
     * Returns the initial value of the property 'esign'.
     */
    public boolean esignInitVal() {
        boolean result;
        if (_esignIsSet) {
            result = _esignInitVal;
        } else {
            result = getEsign();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'esign'.
     */
    public boolean esignIsDirty() {
        return !valuesAreEqual(esignInitVal(), getEsign());
    }

    /**
     * Returns true if the setter method was called for the property 'esign'.
     */
    public boolean esignIsSet() {
        return _esignIsSet;
    }
		
}
