//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(save to mail)}
import de.smava.webapp.applicant.brokerage.domain.history.SaveToMailHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SaveToMails'.
 *
 * 
 *
 * @author generator
 */
public class SaveToMail extends SaveToMailHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(save to mail)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _registrationRoute;
        protected String _currentPage;
        protected LoanApplication _loanApplication;
        protected Date _mailTriggered;
        
                            /**
     * Setter for the property 'registration route'.
     *
     * 
     *
     */
    public void setRegistrationRoute(String registrationRoute) {
        if (!_registrationRouteIsSet) {
            _registrationRouteIsSet = true;
            _registrationRouteInitVal = getRegistrationRoute();
        }
        registerChange("registration route", _registrationRouteInitVal, registrationRoute);
        _registrationRoute = registrationRoute;
    }
                        
    /**
     * Returns the property 'registration route'.
     *
     * 
     *
     */
    public String getRegistrationRoute() {
        return _registrationRoute;
    }
                                    /**
     * Setter for the property 'current page'.
     *
     * 
     *
     */
    public void setCurrentPage(String currentPage) {
        if (!_currentPageIsSet) {
            _currentPageIsSet = true;
            _currentPageInitVal = getCurrentPage();
        }
        registerChange("current page", _currentPageInitVal, currentPage);
        _currentPage = currentPage;
    }
                        
    /**
     * Returns the property 'current page'.
     *
     * 
     *
     */
    public String getCurrentPage() {
        return _currentPage;
    }
                                            
    /**
     * Setter for the property 'loan application'.
     *
     * 
     *
     */
    public void setLoanApplication(LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loan application'.
     *
     * 
     *
     */
    public LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                    /**
     * Setter for the property 'mail triggered'.
     *
     * 
     *
     */
    public void setMailTriggered(Date mailTriggered) {
        if (!_mailTriggeredIsSet) {
            _mailTriggeredIsSet = true;
            _mailTriggeredInitVal = getMailTriggered();
        }
        registerChange("mail triggered", _mailTriggeredInitVal, mailTriggered);
        _mailTriggered = mailTriggered;
    }
                        
    /**
     * Returns the property 'mail triggered'.
     *
     * 
     *
     */
    public Date getMailTriggered() {
        return _mailTriggered;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_loanApplication instanceof de.smava.webapp.commons.domain.Entity && !_loanApplication.getChangeSet().isEmpty()) {
             for (String element : _loanApplication.getChangeSet()) {
                 result.add("loan application : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SaveToMail.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _registrationRoute=").append(_registrationRoute);
            builder.append("\n    _currentPage=").append(_currentPage);
            builder.append("\n}");
        } else {
            builder.append(SaveToMail.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SaveToMail asSaveToMail() {
        return this;
    }
}
