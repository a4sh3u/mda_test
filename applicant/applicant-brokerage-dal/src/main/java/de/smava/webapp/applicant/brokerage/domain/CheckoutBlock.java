//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(checkout block)}
import de.smava.webapp.applicant.brokerage.domain.history.CheckoutBlockHistory;

import java.util.Set;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CheckoutBlocks'.
 *
 * 
 *
 * @author generator
 */
public class CheckoutBlock extends CheckoutBlockHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(checkout block)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected Integer _sortOrder;
        protected Set<CheckoutFlow> _checkoutFlows;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                    /**
     * Setter for the property 'sort order'.
     *
     * 
     *
     */
    public void setSortOrder(Integer sortOrder) {
        if (!_sortOrderIsSet) {
            _sortOrderIsSet = true;
            _sortOrderInitVal = getSortOrder();
        }
        registerChange("sort order", _sortOrderInitVal, sortOrder);
        _sortOrder = sortOrder;
    }
                        
    /**
     * Returns the property 'sort order'.
     *
     * 
     *
     */
    public Integer getSortOrder() {
        return _sortOrder;
    }
                                            
    /**
     * Setter for the property 'checkout flows'.
     *
     * 
     *
     */
    public void setCheckoutFlows(Set<CheckoutFlow> checkoutFlows) {
        _checkoutFlows = checkoutFlows;
    }
            
    /**
     * Returns the property 'checkout flows'.
     *
     * 
     *
     */
    public Set<CheckoutFlow> getCheckoutFlows() {
        return _checkoutFlows;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CheckoutBlock.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(CheckoutBlock.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CheckoutBlock asCheckoutBlock() {
        return this;
    }
}
