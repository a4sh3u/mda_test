//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank household calculation)}
import de.smava.webapp.applicant.brokerage.domain.history.BankHouseholdCalculationHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankHouseholdCalculations'.
 *
 * Bank specific household calculation
 *
 * @author generator
 */
public class BankHouseholdCalculation extends BankHouseholdCalculationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(bank household calculation)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.brokerage.domain.BrokerageBank _brokerageBank;
        protected de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation _householdCalculation;
        protected String _bankExpression;
        
                                    
    /**
     * Setter for the property 'brokerage bank'.
     *
     * 
     *
     */
    public void setBrokerageBank(de.smava.webapp.applicant.brokerage.domain.BrokerageBank brokerageBank) {
        _brokerageBank = brokerageBank;
    }
            
    /**
     * Returns the property 'brokerage bank'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.BrokerageBank getBrokerageBank() {
        return _brokerageBank;
    }
                                            
    /**
     * Setter for the property 'household calculation'.
     *
     * 
     *
     */
    public void setHouseholdCalculation(de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation householdCalculation) {
        _householdCalculation = householdCalculation;
    }
            
    /**
     * Returns the property 'household calculation'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.HouseholdCalculation getHouseholdCalculation() {
        return _householdCalculation;
    }
                                    /**
     * Setter for the property 'bank expression'.
     *
     * Bank specific calculation of household data in Spring expression language
     *
     */
    public void setBankExpression(String bankExpression) {
        if (!_bankExpressionIsSet) {
            _bankExpressionIsSet = true;
            _bankExpressionInitVal = getBankExpression();
        }
        registerChange("bank expression", _bankExpressionInitVal, bankExpression);
        _bankExpression = bankExpression;
    }
                        
    /**
     * Returns the property 'bank expression'.
     *
     * Bank specific calculation of household data in Spring expression language
     *
     */
    public String getBankExpression() {
        return _bankExpression;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_brokerageBank instanceof de.smava.webapp.commons.domain.Entity && !_brokerageBank.getChangeSet().isEmpty()) {
             for (String element : _brokerageBank.getChangeSet()) {
                 result.add("brokerage bank : " + element);
             }
         }

         if (_householdCalculation instanceof de.smava.webapp.commons.domain.Entity && !_householdCalculation.getChangeSet().isEmpty()) {
             for (String element : _householdCalculation.getChangeSet()) {
                 result.add("household calculation : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BankHouseholdCalculation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _bankExpression=").append(_bankExpression);
            builder.append("\n}");
        } else {
            builder.append(BankHouseholdCalculation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BankHouseholdCalculation asBankHouseholdCalculation() {
        return this;
    }
}
