package de.smava.webapp.applicant.brokerage.dao.jdo;

import de.smava.webapp.applicant.brokerage.dao.CustomerUploadedDocDao;
import de.smava.webapp.applicant.brokerage.domain.*;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'CustomerUploadedDocs'.
 */
@Repository(value = "applicantCustomerUploadedDocDao")
public class JdoCustomerUploadedDocDao extends JdoBaseDao implements CustomerUploadedDocDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCustomerUploadedDocDao.class);

    private static final String CLASS_NAME = "CustomerUploadedDoc";
    private static final String STRING_GET = "get";
    private static final String STRING_FIND = "find";
    private static final String STRING_LIST_CLASS = "List()";
    private static final String STRING_COUNT_CLASS = "Count()";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";
    private static final String STRING_SIZE = "size=";
    private static final String STRING_COUNT = "count=";
    private static final String STRING_WHERE_CLAUSE = " whereClause=";
    private static final String STRING_PARAMS = " params=";
    private static final String STRING_PAGEABLE = " pageable=";
    private static final String STRING_SORTABLE = " sortable=";

    /**
     * Returns an attached copy of the customer uploaded doc identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public CustomerUploadedDoc load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        CustomerUploadedDoc result = getEntity(CustomerUploadedDoc.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public CustomerUploadedDoc getCustomerUploadedDoc(Long id) {
        return load(id);
    }

    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	CustomerUploadedDoc entity = findUniqueEntity(CustomerUploadedDoc.class, "_id == " + id);
	    	result = entity != null;

	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the customer uploaded doc.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(CustomerUploadedDoc customerUploadedDoc) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCustomerUploadedDoc: " + customerUploadedDoc);
        }
        return saveEntity(customerUploadedDoc);
    }

    /**
     * @deprecated Use {@link #save(CustomerUploadedDoc) instead}
     */
    public Long saveCustomerUploadedDoc(CustomerUploadedDoc customerUploadedDoc) {
        return save(customerUploadedDoc);
    }

    /**
     * Deletes an customer uploaded doc, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the customer uploaded doc
     */
    public void deleteCustomerUploadedDoc(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteCustomerUploadedDoc: " + id);
        }
        deleteEntity(CustomerUploadedDoc.class, id);
    }

    /**
     * Retrieves all 'CustomerUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     */
    public Collection<CustomerUploadedDoc> getCustomerUploadedDocList() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + "start");
        }
        Collection<CustomerUploadedDoc> result = getEntities(CustomerUploadedDoc.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a page of 'CustomerUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    public Collection<CustomerUploadedDoc> getCustomerUploadedDocList(Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable);
        }
        Collection<CustomerUploadedDoc> result = getEntities(CustomerUploadedDoc.class, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'CustomerUploadedDoc' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    public Collection<CustomerUploadedDoc> getCustomerUploadedDocList(Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_SORTABLE + sortable);
        }
        Collection<CustomerUploadedDoc> result = getEntities(CustomerUploadedDoc.class, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CustomerUploadedDoc' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    public Collection<CustomerUploadedDoc> getCustomerUploadedDocList(Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<CustomerUploadedDoc> result = getEntities(CustomerUploadedDoc.class, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'CustomerUploadedDoc' instance which match the given whereClause. The whereClause
     * must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CustomerUploadedDoc> findCustomerUploadedDocList(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        Collection<CustomerUploadedDoc> result = findEntities(CustomerUploadedDoc.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a collection of 'CustomerUploadedDoc' instance which match the given whereClause together with params specified in object array.
     * The whereClause must not contain the 'where' keyword. I.e.
     * The parameters must be referenced in the where clause
     * <pre>
     *  <code>whereClause = "value == 1000 && objectValue > objectParam PARAMETERS package.of.Object objectParam";</code>
     * </pre>
     */
    public Collection<CustomerUploadedDoc> findCustomerUploadedDocList(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        Collection<CustomerUploadedDoc> result = findEntities(CustomerUploadedDoc.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }



    /**
     * Retrieves a page of 'CustomerUploadedDoc' instance which match the given whereClause.
     * The page information is read from the supplied pageable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CustomerUploadedDoc> findCustomerUploadedDocList(String whereClause, Pageable pageable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable);
        }
        Collection<CustomerUploadedDoc> result = findEntities(CustomerUploadedDoc.class, whereClause, pageable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted collection of 'CustomerUploadedDoc' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CustomerUploadedDoc> findCustomerUploadedDocList(String whereClause, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_SORTABLE + sortable);
        }
        Collection<CustomerUploadedDoc> result = findEntities(CustomerUploadedDoc.class, whereClause, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CustomerUploadedDoc' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CustomerUploadedDoc> findCustomerUploadedDocList(String whereClause, Pageable pageable, Sortable sortable) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<CustomerUploadedDoc> result = findEntities(CustomerUploadedDoc.class, whereClause, pageable, sortable);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Retrieves a sorted page of 'CustomerUploadedDoc' instance which match the given whereClause.
     * The sorting information is read from the supplied sortable.
     * The whereClause must not contain the 'where' keyword.
     * The page information is read from the supplied pageable. I.e.
     * <pre>
     *  <code>whereClause = "value > 1000";</code>
     * </pre>
     */
    public Collection<CustomerUploadedDoc> findCustomerUploadedDocList(String whereClause, Pageable pageable, Sortable sortable, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PAGEABLE + pageable + STRING_SORTABLE + sortable);
        }
        Collection<CustomerUploadedDoc> result = findEntities(CustomerUploadedDoc.class, whereClause, pageable, sortable, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_FIND + CLASS_NAME + STRING_LIST_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_SIZE + result.size());
        }
        return result;
    }

    /**
     * Returns the number of 'CustomerUploadedDoc' instances.
     */
    public long getCustomerUploadedDocCount() {
        long result = getEntityCount(CustomerUploadedDoc.class);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'CustomerUploadedDoc' instances which match the given whereClause.
     */
    public long getCustomerUploadedDocCount(String whereClause) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause);
        }
        long result = getEntityCount(CustomerUploadedDoc.class, whereClause);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * Returns the number of 'CustomerUploadedDoc' instances which match the given whereClause together with params specified in object array.
     */
    public long getCustomerUploadedDocCount(String whereClause, Object ... params) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_START + STRING_WHERE_CLAUSE + whereClause + STRING_PARAMS + Arrays.toString(params));
        }
        long result = getEntityCount(CustomerUploadedDoc.class, whereClause, params);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_COUNT_CLASS + STRING_DELIMITER + STRING_RESULT + STRING_COUNT + result);
        }
        return result;
    }

    /**
     * fetch list of uploaded documents information which is belong to an specific account
     *
     * @param accountId account id
     * @return list of {@link CustomerUploadedDoc} which contains uploaded documents information
     */
    @Override
    public Collection<CustomerUploadedDoc> fetchDocumentsByAccount(Long accountId) {
        Query query = getPersistenceManager().newNamedQuery(CustomerUploadedDoc.class, "fetchDocumentsByAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, accountId);

        @SuppressWarnings("unchecked")
        Collection<CustomerUploadedDoc> customerUploadedDocs = (Collection<CustomerUploadedDoc>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    @Override
    public Collection<CustomerUploadedDoc> fetchDocumentsBySmavaId(Long smavaId) {
        Query query = getPersistenceManager().newNamedQuery(CustomerUploadedDoc.class, "fetchDocumentsBySmavaId");

        @SuppressWarnings("unchecked")
        Collection<CustomerUploadedDoc> customerUploadedDocs =
                (Collection<CustomerUploadedDoc>) query.executeWithMap(Collections.singletonMap(1, smavaId));

        return customerUploadedDocs;
    }

    @Override
    public Collection<CustomerUploadedDoc> fetchDocuments(List<Long> documentIds) {
        Query query = getPersistenceManager().newNamedQuery(CustomerUploadedDoc.class, "fetchDocuments");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("documentIds", documentIds);

        @SuppressWarnings("unchecked")
        Collection<CustomerUploadedDoc> customerUploadedDocs = (Collection<CustomerUploadedDoc>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    @Override
    public Collection<CustomerUploadedDoc> fetchDocuments(Long accountId, List<Long> documentIds) {
        Query query = getPersistenceManager().newNamedQuery(CustomerUploadedDoc.class, "fetchDocumentsByAccountId");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("accountId", accountId);
        params.put("documentIds", documentIds);

        @SuppressWarnings("unchecked")
        Collection<CustomerUploadedDoc> customerUploadedDocs = (Collection<CustomerUploadedDoc>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

    /**
     * fetch an specific uploaded document information which is belong to an specific account
     *
     * @param id document id
     * @param accountId account id
     * @return {@link CustomerUploadedDoc} which contains uploaded document information
     */
    @Override
    public CustomerUploadedDoc fetchDocumentByAccount(Long id, Long accountId) {
        CustomerUploadedDoc result = null;
        Query query = getPersistenceManager().newNamedQuery(CustomerUploadedDoc.class, "fetchDocumentByAccount");
        Map<Integer, Object> params = new HashMap<Integer, Object>();

        params.put(1, id);
        params.put(2, accountId);

        @SuppressWarnings("unchecked")
        Collection<CustomerUploadedDoc> customerUploadedDocs = (Collection<CustomerUploadedDoc>) query.executeWithMap(params);
        if (customerUploadedDocs != null && customerUploadedDocs.size() > 0) {
            result = customerUploadedDocs.iterator().next();
        }

        return  result;
    }

    private static final String MIN_DIRECT_PER_CONTRACT_SQL
            = "select * from brokerage.customer_uploaded_doc where account_id = :accountId and status in (:docStates)";

    @Override
    public Collection<CustomerUploadedDoc> fetchDocumentsByAccountAndStatusIn(Long accountId, List<DocumentStatus> status) {
        Query query = getPersistenceManager().newNamedQuery(CustomerUploadedDoc.class, "fetchDocumentsByAccountAndStatusIn");
        Map<String, Object> params = new HashMap<String, Object>();

        if (status == null || status.isEmpty()) {
            status = new ArrayList<DocumentStatus>();

            status.add(DocumentStatus.CREATED);
            status.add(DocumentStatus.ACCEPTED);
            status.add(DocumentStatus.REJECTED);
            status.add(DocumentStatus.DELETED);
            status.add(DocumentStatus.OUTDATED);
        }

        params.put("accountId", accountId);
        params.put("docStatus", status);

        @SuppressWarnings("unchecked")
        Collection<CustomerUploadedDoc> customerUploadedDocs = (Collection<CustomerUploadedDoc>) query.executeWithMap(params);

        return customerUploadedDocs;
    }

}
