//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.brokerage.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(vehicle information)}

import de.smava.webapp.applicant.brokerage.domain.history.VehicleInformationHistory;

import java.util.Date;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'VehicleInformations'.
 *
 * 
 *
 * @author generator
 */
public class VehicleInformation extends VehicleInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(vehicle information)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _model;
        protected String _brand;
        protected Double _vehiclePrice;
        protected Double _downPayment;
        protected Date _registrationYear;
        protected Integer _powerKw;
        protected Integer _vehicleKm;
        protected de.smava.webapp.applicant.brokerage.type.VehicleType _vehicleType;
        
                            /**
     * Setter for the property 'model'.
     *
     * 
     *
     */
    public void setModel(String model) {
        if (!_modelIsSet) {
            _modelIsSet = true;
            _modelInitVal = getModel();
        }
        registerChange("model", _modelInitVal, model);
        _model = model;
    }
                        
    /**
     * Returns the property 'model'.
     *
     * 
     *
     */
    public String getModel() {
        return _model;
    }
                                    /**
     * Setter for the property 'brand'.
     *
     * 
     *
     */
    public void setBrand(String brand) {
        if (!_brandIsSet) {
            _brandIsSet = true;
            _brandInitVal = getBrand();
        }
        registerChange("brand", _brandInitVal, brand);
        _brand = brand;
    }
                        
    /**
     * Returns the property 'brand'.
     *
     * 
     *
     */
    public String getBrand() {
        return _brand;
    }
                                    /**
     * Setter for the property 'vehicle price'.
     *
     * 
     *
     */
    public void setVehiclePrice(Double vehiclePrice) {
        if (!_vehiclePriceIsSet) {
            _vehiclePriceIsSet = true;
            _vehiclePriceInitVal = getVehiclePrice();
        }
        registerChange("vehicle price", _vehiclePriceInitVal, vehiclePrice);
        _vehiclePrice = vehiclePrice;
    }
                        
    /**
     * Returns the property 'vehicle price'.
     *
     * 
     *
     */
    public Double getVehiclePrice() {
        return _vehiclePrice;
    }
                                    /**
     * Setter for the property 'down payment'.
     *
     * 
     *
     */
    public void setDownPayment(Double downPayment) {
        if (!_downPaymentIsSet) {
            _downPaymentIsSet = true;
            _downPaymentInitVal = getDownPayment();
        }
        registerChange("down payment", _downPaymentInitVal, downPayment);
        _downPayment = downPayment;
    }
                        
    /**
     * Returns the property 'down payment'.
     *
     * 
     *
     */
    public Double getDownPayment() {
        return _downPayment;
    }
                                    /**
     * Setter for the property 'registration year'.
     *
     * 
     *
     */
    public void setRegistrationYear(Date registrationYear) {
        if (!_registrationYearIsSet) {
            _registrationYearIsSet = true;
            _registrationYearInitVal = getRegistrationYear();
        }
        registerChange("registration year", _registrationYearInitVal, registrationYear);
        _registrationYear = registrationYear;
    }
                        
    /**
     * Returns the property 'registration year'.
     *
     * 
     *
     */
    public Date getRegistrationYear() {
        return _registrationYear;
    }
                                    /**
     * Setter for the property 'power kw'.
     *
     * 
     *
     */
    public void setPowerKw(Integer powerKw) {
        if (!_powerKwIsSet) {
            _powerKwIsSet = true;
            _powerKwInitVal = getPowerKw();
        }
        registerChange("power kw", _powerKwInitVal, powerKw);
        _powerKw = powerKw;
    }
                        
    /**
     * Returns the property 'power kw'.
     *
     * 
     *
     */
    public Integer getPowerKw() {
        return _powerKw;
    }
                                    /**
     * Setter for the property 'vehicle km'.
     *
     * 
     *
     */
    public void setVehicleKm(Integer vehicleKm) {
        if (!_vehicleKmIsSet) {
            _vehicleKmIsSet = true;
            _vehicleKmInitVal = getVehicleKm();
        }
        registerChange("vehicle km", _vehicleKmInitVal, vehicleKm);
        _vehicleKm = vehicleKm;
    }
                        
    /**
     * Returns the property 'vehicle km'.
     *
     * 
     *
     */
    public Integer getVehicleKm() {
        return _vehicleKm;
    }
                                    /**
     * Setter for the property 'vehicle type'.
     *
     * 
     *
     */
    public void setVehicleType(de.smava.webapp.applicant.brokerage.type.VehicleType vehicleType) {
        if (!_vehicleTypeIsSet) {
            _vehicleTypeIsSet = true;
            _vehicleTypeInitVal = getVehicleType();
        }
        registerChange("vehicle type", _vehicleTypeInitVal, vehicleType);
        _vehicleType = vehicleType;
    }
                        
    /**
     * Returns the property 'vehicle type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.type.VehicleType getVehicleType() {
        return _vehicleType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(VehicleInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _model=").append(_model);
            builder.append("\n    _brand=").append(_brand);
            builder.append("\n    _vehicleType=").append(_vehicleType);
            builder.append("\n}");
        } else {
            builder.append(VehicleInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public VehicleInformation asVehicleInformation() {
        return this;
    }
}
