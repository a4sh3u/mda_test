package de.smava.webapp.applicant.brokerage.domain.history;



import de.smava.webapp.applicant.brokerage.domain.abstracts.AbstractBankDocumentView;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'BankDocumentViews'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BankDocumentViewHistory extends AbstractBankDocumentView {

    protected transient String _extRefNumberInitVal;
    protected transient boolean _extRefNumberIsSet;
    protected transient String _bankNameInitVal;
    protected transient boolean _bankNameIsSet;
    protected transient Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Date _sendDateInitVal;
    protected transient boolean _sendDateIsSet;
    protected transient Date _receiveDateInitVal;
    protected transient boolean _receiveDateIsSet;
    protected transient String _firstNameInitVal;
    protected transient boolean _firstNameIsSet;
    protected transient String _lastNameInitVal;
    protected transient boolean _lastNameIsSet;
    protected transient String _additionalDataInitVal;
    protected transient boolean _additionalDataIsSet;


	
    /**
     * Returns the initial value of the property 'ext ref number'.
     */
    public String extRefNumberInitVal() {
        String result;
        if (_extRefNumberIsSet) {
            result = _extRefNumberInitVal;
        } else {
            result = getExtRefNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'ext ref number'.
     */
    public boolean extRefNumberIsDirty() {
        return !valuesAreEqual(extRefNumberInitVal(), getExtRefNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'ext ref number'.
     */
    public boolean extRefNumberIsSet() {
        return _extRefNumberIsSet;
    }
				
    /**
     * Returns the initial value of the property 'bank name'.
     */
    public String bankNameInitVal() {
        String result;
        if (_bankNameIsSet) {
            result = _bankNameInitVal;
        } else {
            result = getBankName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank name'.
     */
    public boolean bankNameIsDirty() {
        return !valuesAreEqual(bankNameInitVal(), getBankName());
    }

    /**
     * Returns true if the setter method was called for the property 'bank name'.
     */
    public boolean bankNameIsSet() {
        return _bankNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public Date creationDateInitVal() {
        Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'send date'.
     */
    public Date sendDateInitVal() {
        Date result;
        if (_sendDateIsSet) {
            result = _sendDateInitVal;
        } else {
            result = getSendDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'send date'.
     */
    public boolean sendDateIsDirty() {
        return !valuesAreEqual(sendDateInitVal(), getSendDate());
    }

    /**
     * Returns true if the setter method was called for the property 'send date'.
     */
    public boolean sendDateIsSet() {
        return _sendDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'receive date'.
     */
    public Date receiveDateInitVal() {
        Date result;
        if (_receiveDateIsSet) {
            result = _receiveDateInitVal;
        } else {
            result = getReceiveDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'receive date'.
     */
    public boolean receiveDateIsDirty() {
        return !valuesAreEqual(receiveDateInitVal(), getReceiveDate());
    }

    /**
     * Returns true if the setter method was called for the property 'receive date'.
     */
    public boolean receiveDateIsSet() {
        return _receiveDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'first name'.
     */
    public String firstNameInitVal() {
        String result;
        if (_firstNameIsSet) {
            result = _firstNameInitVal;
        } else {
            result = getFirstName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first name'.
     */
    public boolean firstNameIsDirty() {
        return !valuesAreEqual(firstNameInitVal(), getFirstName());
    }

    /**
     * Returns true if the setter method was called for the property 'first name'.
     */
    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last name'.
     */
    public String lastNameInitVal() {
        String result;
        if (_lastNameIsSet) {
            result = _lastNameInitVal;
        } else {
            result = getLastName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last name'.
     */
    public boolean lastNameIsDirty() {
        return !valuesAreEqual(lastNameInitVal(), getLastName());
    }

    /**
     * Returns true if the setter method was called for the property 'last name'.
     */
    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }
		
    /**
     * Returns the initial value of the property 'additional data'.
     */
    public String additionalDataInitVal() {
        String result;
        if (_additionalDataIsSet) {
            result = _additionalDataInitVal;
        } else {
            result = getAdditionalData();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'additional data'.
     */
    public boolean additionalDataIsDirty() {
        return !valuesAreEqual(additionalDataInitVal(), getAdditionalData());
    }

    /**
     * Returns true if the setter method was called for the property 'additional data'.
     */
    public boolean additionalDataIsSet() {
        return _additionalDataIsSet;
    }

}
