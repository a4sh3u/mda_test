package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Adam
 * @since 28.04.2016.
 */
public class AdditionalDataFactoryTest {

    @Test
    public void testCreateWhenBankNameEmpty() throws Exception {
        // for
        String bankName = null;

        // when
        AdditionalData result = AdditionalDataFactory.create(bankName);

        // then
        assertThat("Result for empty bankName should be null", result, is(nullValue()));
    }

    @Test
    public void testCreateWhenBankNameUnknown() throws Exception {
        // for
        String bankName = "unknown";

        // when
        AdditionalData result = AdditionalDataFactory.create(bankName);

        // then
        assertThat("Result for unknown bankName should be null", result, is(nullValue()));
    }

    @Test
    public void testCreateWhenBankNameCorrect() throws Exception {
        List<String> banksWithAdditionalDataInNewSchema = Arrays.asList(
                BrokerageBank.BANK_POSTBANK,
                BrokerageBank.BANK_CREDITPLUS,
                BrokerageBank.BANK_PSD,
                BrokerageBank.BANK_SANTANDER_BESTCREDIT,
                BrokerageBank.BANK_DKB,
                BrokerageBank.BANK_DSL,
                BrokerageBank.BANK_WUNSCHKREDIT,
                BrokerageBank.BANK_KREDIT2GO,
                BrokerageBank.BANK_CARCREDIT,
                BrokerageBank.BANK_SKG,
                BrokerageBank.BANK_SWK,
                BrokerageBank.BANK_ONLINEKREDIT,
                BrokerageBank.BANK_ABK,
                BrokerageBank.BANK_POSTBANK_BUSINESS_CREDIT,
                BrokerageBank.BANK_KREDIT_PRIVAT,
                BrokerageBank.BANK_VONESSEN,
                BrokerageBank.BANK_TARGOBANK,
                BrokerageBank.BANK_AUXMONEY,
                BrokerageBank.BANK_VONESSEN_SUBPRIME,
                BrokerageBank.BANK_ADAC,
                BrokerageBank.BANK_OYAKANKER,
                BrokerageBank.BANK_BANKOFSCOTLAND,
                BrokerageBank.BANK_KREDIT2DAY,
                BrokerageBank.BANK_INGDIBA,
                BrokerageBank.BANK_BARCLAYS
        );

        // for
        for (String bankName : BrokerageBank.BANKS) {
            // when
            AdditionalData result = AdditionalDataFactory.create(bankName);

            // then
            if (banksWithAdditionalDataInNewSchema.contains(bankName)) {
                assertThat("Result for " + bankName + " should not be null", result, is(notNullValue()));
            } else {
                assertThat("Result for " + bankName + " should be null", result, is(nullValue()));
            }
        }
    }
}
