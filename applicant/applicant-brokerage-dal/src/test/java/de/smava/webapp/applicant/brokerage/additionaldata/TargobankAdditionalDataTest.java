package de.smava.webapp.applicant.brokerage.additionaldata;

import de.smava.webapp.brokerage.domain.BrokerageBank;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

/**
 * @author Adam Tomecki
 * @since 02.03.2017
 */
public class TargobankAdditionalDataTest {

    private TargobankAdditionalData targobankAdditionalData;

    @Before
    public void setUp() throws Exception {
        targobankAdditionalData = (TargobankAdditionalData) AdditionalDataFactory.create(BrokerageBank.BANK_TARGOBANK);
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#retrieveBankName()
     */
    @Test
    public void testRetrieveBankName() throws Exception {
        assertThat("Wrong bank name", targobankAdditionalData.retrieveBankName(), is("targobank"));
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#getOciMaxLoan()
     */
    @Test
    public void testGetOciMaxLoanWhenInfoEmpty() throws Exception {
        // for empty information
        // when
        String result = targobankAdditionalData.getOciMaxLoan();

        // then
        assertThat("Wrong max OCI loan value", result, is(nullValue()));
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#getOciMaxLoan()
     */
    @Test
    public void testGetOciMaxLoanWhenNull() throws Exception {
        // for
        targobankAdditionalData.getInformation().add("ociMaxLoan: ");

        // when
        String result = targobankAdditionalData.getOciMaxLoan();

        // then
        assertThat("Wrong max OCI loan value", result, is(nullValue()));
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#getOciMaxLoan()
     */
    @Test
    public void testGetOciMaxLoanWhenNotNull() throws Exception {
        // for
        targobankAdditionalData.getInformation().add("ociMaxLoan: 1234");

        // when
        String result = targobankAdditionalData.getOciMaxLoan();

        // then
        assertThat("Wrong max OCI loan value", result, is("1234"));
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#getOciStatus()
     */
    @Test
    public void testGetOciStatusWhenInfoEmpty() throws Exception {
        // for empty information
        // when
        String result = targobankAdditionalData.getOciStatus();

        // then
        assertThat("Wrong OCI status value", result, is(nullValue()));
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#getOciStatus()
     */
    @Test
    public void testGetOciStatusWhenNull() throws Exception {
        // for
        targobankAdditionalData.getInformation().add("ociStatus: ");

        // when
        String result = targobankAdditionalData.getOciStatus();

        // then
        assertThat("Wrong OCI status value", result, is(nullValue()));
    }

    /**
     * @throws Exception
     * @see TargobankAdditionalData#getOciStatus()
     */
    @Test
    public void testGetOciStatusWhenNotNull() throws Exception {
        // for
        targobankAdditionalData.getInformation().add("ociStatus: A");

        // when
        String result = targobankAdditionalData.getOciStatus();

        // then
        assertThat("Wrong OCI status value", result, is("A"));
    }
}