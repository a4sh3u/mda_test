package de.smava.webapp.applicant.brokerage.domain;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by aibrahim on 15.06.16.
 */
public class DocumentStatusTest {
    @Test
    public void testStatusCreated() throws Exception {
        assertTrue(DocumentStatus.CREATED.isTransitionAllowed(DocumentStatus.CREATED));
        assertTrue(DocumentStatus.CREATED.isTransitionAllowed(DocumentStatus.ACCEPTED));
        assertTrue(DocumentStatus.CREATED.isTransitionAllowed(DocumentStatus.REJECTED));
        assertTrue(DocumentStatus.CREATED.isTransitionAllowed(DocumentStatus.OUTDATED));
        assertFalse(DocumentStatus.CREATED.isTransitionAllowed(DocumentStatus.OTHER));
    }


    @Test
    public void testStatusAccepted() throws Exception {
        assertFalse(DocumentStatus.ACCEPTED.isTransitionAllowed(DocumentStatus.CREATED));
        assertTrue(DocumentStatus.ACCEPTED.isTransitionAllowed(DocumentStatus.ACCEPTED));
        assertTrue(DocumentStatus.ACCEPTED.isTransitionAllowed(DocumentStatus.REJECTED));
        assertTrue(DocumentStatus.ACCEPTED.isTransitionAllowed(DocumentStatus.OUTDATED));
        assertFalse(DocumentStatus.ACCEPTED.isTransitionAllowed(DocumentStatus.OTHER));
    }


    @Test
    public void testStatusRejected() throws Exception {
        assertFalse(DocumentStatus.REJECTED.isTransitionAllowed(DocumentStatus.CREATED));
        assertTrue(DocumentStatus.REJECTED.isTransitionAllowed(DocumentStatus.ACCEPTED));
        assertTrue(DocumentStatus.REJECTED.isTransitionAllowed(DocumentStatus.REJECTED));
        assertTrue(DocumentStatus.REJECTED.isTransitionAllowed(DocumentStatus.OUTDATED));
        assertFalse(DocumentStatus.REJECTED.isTransitionAllowed(DocumentStatus.OTHER));
    }



    @Test
    public void testStatusOutdated() throws Exception {
        assertFalse(DocumentStatus.OUTDATED.isTransitionAllowed(DocumentStatus.CREATED));
        assertTrue(DocumentStatus.OUTDATED.isTransitionAllowed(DocumentStatus.ACCEPTED));
        assertTrue(DocumentStatus.OUTDATED.isTransitionAllowed(DocumentStatus.REJECTED));
        assertTrue(DocumentStatus.OUTDATED.isTransitionAllowed(DocumentStatus.OUTDATED));
        assertFalse(DocumentStatus.OUTDATED.isTransitionAllowed(DocumentStatus.OTHER));
    }

}