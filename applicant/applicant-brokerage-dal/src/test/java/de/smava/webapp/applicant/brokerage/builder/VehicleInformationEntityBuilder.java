package de.smava.webapp.applicant.brokerage.builder;

import de.smava.webapp.applicant.brokerage.domain.VehicleInformation;
import de.smava.webapp.applicant.brokerage.type.VehicleType;
import de.smava.webapp.applicant.builder.EntityBuilder;

import java.util.Date;

/**
 * Created by Adam on 2015-07-31.
 */
public class VehicleInformationEntityBuilder extends EntityBuilder<VehicleInformation> {

    protected VehicleInformation createEntity() {
        return new VehicleInformation();
    }

    public static VehicleInformationEntityBuilder aBuilder() { return new VehicleInformationEntityBuilder(); }

    public VehicleInformationEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setBrand("Some Brand");
        entity.setDownPayment(1233.0);
        entity.setModel("Some Model");
        entity.setPowerKw(123);
        entity.setRegistrationYear(new Date());
        entity.setVehicleKm(123);
        entity.setVehiclePrice(100000.0);
        entity.setVehicleType(VehicleType.CAR);

        return this;
    }
}
