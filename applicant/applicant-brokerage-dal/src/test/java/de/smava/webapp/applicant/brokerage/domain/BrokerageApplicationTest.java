package de.smava.webapp.applicant.brokerage.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.smava.webapp.applicant.brokerage.additionaldata.AdditionalData;
import de.smava.webapp.applicant.brokerage.additionaldata.PostbankAdditionalData;
import de.smava.webapp.applicant.brokerage.additionaldata.property.MissingDocuments;
import de.smava.webapp.applicant.brokerage.builder.BrokerageAppEntityBuilder;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.junit.Test;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

/**
 * @author Adam
 * @since 28.04.2016.
 */
public class BrokerageApplicationTest {

    @Test
    public void testWriteAdditionalDataForCorrectBank() throws Exception {
        // for
        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setAdditionalData(null);
        brokerageApplication.setBrokerageBank(new BrokerageBank());
        brokerageApplication.getBrokerageBank().setName(de.smava.webapp.brokerage.domain.BrokerageBank.BANK_POSTBANK);

        // when
        brokerageApplication.writeAdditionalData(new PostbankAdditionalData());

        // then
        assertThat("Result for writing additional data object should not be null",
                brokerageApplication.getAdditionalData(), is(notNullValue()));
    }

    @Test
    public void testWriteAdditionalDataJsonFormat() throws Exception {
        // for
        String pattern = "[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9][0-9]Z";
        Pattern r = Pattern.compile(pattern);

        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setBrokerageBank(new BrokerageBank());
        brokerageApplication.getBrokerageBank().setName(de.smava.webapp.brokerage.domain.BrokerageBank.BANK_POSTBANK);

        PostbankAdditionalData postbankAddData = new PostbankAdditionalData();
        MissingDocuments missingDocuments = new MissingDocuments();
        Date dt = CurrentDate.getDate();
        missingDocuments.setExpirationDate(dt);
        postbankAddData.setMissingDocuments(missingDocuments);
        brokerageApplication.writeAdditionalData(postbankAddData);

        // when
        String addData = brokerageApplication.getAdditionalData();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(addData);
        String jsonDateString = root.get("missingDocuments").get("expirationDate").toString();
        Matcher m = r.matcher(jsonDateString);

        // then
        assertThat("Date should be in format: YYYY-MM-DDTHH:mm:ss.sssZ", m.find(), is(true));
    }

    @Test
    public void testWriteAdditionalDataForWrongBank() throws Exception {
        // for
        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setAdditionalData("some additional data for ABK");
        brokerageApplication.setBrokerageBank(new BrokerageBank());
        brokerageApplication.getBrokerageBank().setName(de.smava.webapp.brokerage.domain.BrokerageBank.BANK_ABK);

        // when
        brokerageApplication.writeAdditionalData(new PostbankAdditionalData());

        // then
        assertThat("Result for writing additional data from one bank to another should not be proceed",
                brokerageApplication.getAdditionalData(), is("some additional data for ABK"));
    }

    @Test
    public void testReadAdditionalDataWhenBrokerageBankEmpty() {
        // for
        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setBrokerageBank(null);

        // when
        AdditionalData result = brokerageApplication.readAdditionalData();

        // then
        assertThat("Result for empty brokerage bank should be null", result, is(nullValue()));
    }

    @Test
    public void testReadAdditionalDataWhenAdditionalDataEmpty() throws Exception {
        // for
        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setBrokerageBank(new BrokerageBank());
        brokerageApplication.getBrokerageBank().setName(de.smava.webapp.brokerage.domain.BrokerageBank.BANK_POSTBANK);
        brokerageApplication.setAdditionalData(null);

        // when
        AdditionalData result = brokerageApplication.readAdditionalData();

        // then
        assertThat("Result for reading additional data should not be null", result, is(notNullValue()));
    }

    @Test
    public void testReadAdditionalDataWhenAdditionalDataCorrect() throws Exception {
        // for
        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setBrokerageBank(new BrokerageBank());
        brokerageApplication.getBrokerageBank().setName(de.smava.webapp.brokerage.domain.BrokerageBank.BANK_POSTBANK);
        brokerageApplication.setAdditionalData("{\n" +
                "    \"references\": [{\n" +
                "        \"name\": \"reference_name\",\n" +
                "        \"id\": \"reference_id\"\n" +
                "    }],\n" +
                "    \"missingDocuments\": {\n" +
                "        \"expirationDate\": \"2012-04-23T18:25:43.511Z\",\n" +
                "        \"documents\": [\"Doc1\", \"Doc2\"]\n" +
                "    },\n" +
                "    \"messages\": [\"Message1\", \"Message2\"],\n" +
                "    \"information\": [\"Info1\", \"Info2\"],\n" +
                "    \"scoring\": {\n" +
                "        \"color\": \"blue\",\n" +
                "        \"score\": 1,\n" +
                "        \"text\": \"scoring text\",\n" +
                "        \"rapClass\": \"rap_class\",\n" +
                "        \"freeIncome\": 1\n" +
                "    },\n" +
                "    \"schufaInformation\": [{\n" +
                "        \"applicant\": 1,\n" +
                "        \"partialScore\": \"588\",\n" +
                "        \"riskCategory\": \"T\",\n" +
                "        \"schufaAttributes\": [{\n" +
                "            \"code\": \"schufa_attribute_code\",\n" +
                "            \"date\": null,\n" +
                "            \"currency\": \"schufa_attribute_currency\",\n" +
                "            \"amount\": 999,\n" +
                "            \"numberOfRates\": 48,\n" +
                "            \"typeOfRates\": \"\",\n" +
                "            \"text\": \"\",\n" +
                "            \"done\": false,\n" +
                "            \"doneDate\": null\n" +
                "        }]\n" +
                "    }],\n" +
                "    \"alternativeOffers\": [{\n" +
                "        \"loanAmount\": 10000,\n" +
                "        \"effInterestRate\": 5.05,\n" +
                "        \"duration\": 48,\n" +
                "        \"monthlyRate\": 20.00,\n" +
                "        \"lastRate\": 15.02,\n" +
                "        \"interestAmount\": 399.50,\n" +
                "        \"rdi\": \"DEATH\",\n" +
                "        \"rdiAmount\": 73.99,\n" +
                "        \"serviceFee\": 54.30,\n" +
                "        \"loanAmountTotal\": 10600.76\n" +
                "    }]\n" +
                "}");

        // when
        AdditionalData result = brokerageApplication.readAdditionalData();

        // then
        assertThat("Wrong class returned", result.getClass(), is(PostbankAdditionalData.class.getClass()));

        PostbankAdditionalData postbankAdditionalData = (PostbankAdditionalData) result;

        assertThat("Alternative offers should not be null", postbankAdditionalData.getAlternativeOffers(), is(notNullValue()));
        assertThat("Information should not be null", postbankAdditionalData.getInformation(), is(notNullValue()));
        assertThat("Messages should not be null", postbankAdditionalData.getMessages(), is(notNullValue()));
        assertThat("Missing documents should not be null", postbankAdditionalData.getMissingDocuments(), is(notNullValue()));
        assertThat("References should not be null", postbankAdditionalData.getReferences(), is(notNullValue()));
        assertThat("Schufa information should not be null", postbankAdditionalData.getSchufaInformation(), is(notNullValue()));
        assertThat("Scoring should not be null", postbankAdditionalData.getScoring(), is(notNullValue()));
    }

    @Test
    public void testReadAdditionalDataWhenAdditionalDataIncorrect() throws Exception {
        // for
        BrokerageApplication brokerageApplication = BrokerageAppEntityBuilder.aBuilder().withData().build();
        brokerageApplication.setBrokerageBank(new BrokerageBank());
        brokerageApplication.getBrokerageBank().setName(de.smava.webapp.brokerage.domain.BrokerageBank.BANK_POSTBANK);
        brokerageApplication.setAdditionalData("{\"unknownField\": 111}");

        // when
        AdditionalData result = brokerageApplication.readAdditionalData();

        // then
        assertThat("Result for incorrect additional data should be null", result, is(nullValue()));
    }
}
