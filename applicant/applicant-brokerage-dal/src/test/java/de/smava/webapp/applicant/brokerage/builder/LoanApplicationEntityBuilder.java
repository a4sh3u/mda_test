package de.smava.webapp.applicant.brokerage.builder;

import de.smava.webapp.applicant.account.builder.AccountEntityBuilder;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.brokerage.domain.LoanApplication;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.applicant.brokerage.type.Category;
import de.smava.webapp.applicant.brokerage.type.LoanApplicationInitiatorType;
import de.smava.webapp.applicant.brokerage.type.RdiType;
import de.smava.webapp.applicant.builder.ApplicantRelationshipEntityBuilder;
import de.smava.webapp.applicant.builder.EntityBuilder;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class LoanApplicationEntityBuilder extends EntityBuilder<LoanApplication> {

    protected LoanApplication createEntity() {
        return new LoanApplication();
    }

    public static LoanApplicationEntityBuilder aBuilder() {
        return new LoanApplicationEntityBuilder();
    }

    public LoanApplicationEntityBuilder withFullData() {
        entity.setCreationDate(getRandomDate());
        entity.setState(BrokerageState.APPLICATION_ACCEPTED);
        entity.setAccount(AccountEntityBuilder.aBuilder().withData().build());
        entity.setApplicantRelationship(ApplicantRelationshipEntityBuilder.aBuilder().withFullData().build());
        entity.setRequestedRdiType(RdiType.NONE);
        entity.setRequestedAmount(3000.00);
        entity.setRequestedDuration(80);
        entity.setSharedLoan(true);

        Set<BrokerageApplication> brokerage = new HashSet<BrokerageApplication>();
        brokerage.add(BrokerageAppEntityBuilder.aBuilder().withData().build());
        entity.setBrokerageApplications(brokerage);
        entity.setCategory(Category.STANDARD);
        entity.setVehicleInformation(VehicleInformationEntityBuilder.aBuilder().withData().build());
        entity.setInitiatorType(LoanApplicationInitiatorType.CUSTOMER);
        entity.setInitiatorTool("initiator toll");
        entity.setReachedEmailTimeout(false);
        entity.setVisible(true);

        return this;
    }

    public LoanApplicationEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setCategory(Category.CAR);
        entity.setState(BrokerageState.APPLICATION_APPLIED);

        return this;
    }

    public LoanApplicationEntityBuilder withApplicantRelationship() {
        entity.setApplicantRelationship(ApplicantRelationshipEntityBuilder.aBuilder().withApplicants().withMarried().build());
        return this;
    }

    public LoanApplicationEntityBuilder withVehicleInformation() {
        entity.setVehicleInformation(VehicleInformationEntityBuilder.aBuilder().withData().build());
        return this;
    }

    public LoanApplicationEntityBuilder withAccount() {
        entity.setAccount(AccountEntityBuilder.aBuilder().withData().build());
        return this;
    }
}
