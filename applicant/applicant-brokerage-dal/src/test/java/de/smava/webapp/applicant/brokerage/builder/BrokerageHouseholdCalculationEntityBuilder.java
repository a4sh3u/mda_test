package de.smava.webapp.applicant.brokerage.builder;

import de.smava.webapp.applicant.brokerage.domain.BrokerageHouseholdCalculation;
import de.smava.webapp.applicant.builder.EntityBuilder;
import de.smava.webapp.commons.currentdate.CurrentDate;

/**
 * @author Adam
 * @since 13.07.2016.
 */
public class BrokerageHouseholdCalculationEntityBuilder extends EntityBuilder<BrokerageHouseholdCalculation> {

    protected BrokerageHouseholdCalculation createEntity() {
        return new BrokerageHouseholdCalculation();
    }

    public static BrokerageHouseholdCalculationEntityBuilder aBuilder() { return new BrokerageHouseholdCalculationEntityBuilder(); }

    public BrokerageHouseholdCalculationEntityBuilder withData() {
        entity.setId(getNextId());
        entity.setBrokerageApplication(getNextId());
        entity.setCreationDate(CurrentDate.getDate());
        entity.setDisposableIncome(3000d);
        entity.setDisposableIncomeSecondApplicant(null);
        entity.setExpenses(1000d);
        entity.setExpensesSecondApplicant(null);
        entity.setIncome(4000d);
        entity.setIncomeSecondApplicant(null);
        entity.setIndebtedness(2000d);
        entity.setIndebtednessSecondApplicant(null);

        return this;
    }

    public BrokerageHouseholdCalculationEntityBuilder withSecondApplicant() {
        entity.setDisposableIncomeSecondApplicant(2500d);
        entity.setExpensesSecondApplicant(500d);
        entity.setIncomeSecondApplicant(3500d);
        entity.setIndebtednessSecondApplicant(1500d);

        return this;
    }
}
