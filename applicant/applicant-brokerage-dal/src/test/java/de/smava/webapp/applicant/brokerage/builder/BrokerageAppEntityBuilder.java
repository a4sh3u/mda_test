package de.smava.webapp.applicant.brokerage.builder;

import de.smava.webapp.applicant.account.builder.AccountEntityBuilder;
import de.smava.webapp.applicant.brokerage.domain.*;
import de.smava.webapp.applicant.brokerage.type.BrokerageState;
import de.smava.webapp.applicant.brokerage.type.Category;
import de.smava.webapp.applicant.brokerage.type.FilterReasonCode;
import de.smava.webapp.applicant.brokerage.type.RdiType;
import de.smava.webapp.applicant.builder.ApplicantEntityBuilder;
import de.smava.webapp.applicant.builder.BankAccountEntityBuilder;
import de.smava.webapp.applicant.builder.EntityBuilder;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class BrokerageAppEntityBuilder extends EntityBuilder<BrokerageApplication> {

    protected BrokerageApplication createEntity() {
        return new BrokerageApplication();
    }

    public static BrokerageAppEntityBuilder aBuilder() {
        return new BrokerageAppEntityBuilder();
    }

    public BrokerageAppEntityBuilder withRequestData() {
        BrokerageApplicationRequestData requestData = new BrokerageApplicationRequestData();
        this.entity.setCurrentRequestData(requestData);

        return this;
    }

    public BrokerageAppEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        BrokerageBank bank = new BrokerageBank();
        bank.setName("dkb Bank");
        entity.setRequestedAmount(123.45);
        entity.setRequestedDuration(11);
        entity.setRequestedRdiType(RdiType.DEATH_INVALIDITY_UNEMPLOYMENT);
        entity.setCategory(Category.CAR);

        return this;
    }

    public BrokerageAppEntityBuilder withFullData() {
        entity.setCreationDate(getRandomDate());
        BrokerageBank bank = new BrokerageBank();
        bank.setName("dkb Bank");
        entity.setBrokerageBank(bank);
        entity.setExtRefNumber("extRefNumber");
        entity.setPrevExtRefNumber("prewExtRefNymber");
        entity.setState(BrokerageState.APPLICATION_ACCEPTED);
        entity.setAccount(AccountEntityBuilder.aBuilder().withData().build());
        entity.setFirstApplicant(ApplicantEntityBuilder.aBuilder().withPersonalData().withPhone().withHomeAddress().withPreviousHomeAddress()
                        .withBankAccount().withNationalityInfo().withHousehold().withPreviousEmploymentAsEmployee().withMainIncomeAsEmployee()
                        .withThirdPartyLoans().withRandomEmail(20).build());
        entity.setSecondApplicant(ApplicantEntityBuilder.aBuilder().withPersonalData().withPhone().withHomeAddress().withPreviousHomeAddress()
                        .withBankAccount().withNationalityInfo().withHousehold().withPreviousEmploymentAsEmployee().withMainIncomeAsEmployee()
                        .withThirdPartyLoans().withRandomEmail(20).build());
        entity.setReferenceBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());
        entity.setRequestedRdiType(RdiType.DEATH_INVALIDITY_UNEMPLOYMENT);
        entity.setRequestedAmount(123.45);
        entity.setRequestedDuration(11);
        entity.setMonthlyRate(77.7);
        entity.setEffectiveInterest(55.0);
        entity.setAmount(5000.0);
        entity.setDuration(80);
        entity.setRdiType(RdiType.DEATH_INVALIDITY_UNEMPLOYMENT);
        BrokerageApplicationFilterReason reason = new BrokerageApplicationFilterReason();
        reason.setReasonCode(FilterReasonCode.AMOUNT_TOO_HIGH);
        reason.setReasonDescription("reason desctiprion");
        entity.setBrokerageApplicationFilterReason(reason);
        BrokerageApplicationFilterReason reason2 = new BrokerageApplicationFilterReason();
        reason.setReasonCode(FilterReasonCode.APPLICANT_TOO_OLD);
        reason.setReasonDescription("reason desctiprion to old");
        entity.setLoanApplication(LoanApplicationEntityBuilder.aBuilder().withFullData().build());
        entity.setCategory(Category.CAR);
        entity.setPapSaleTrackedDate(getRandomDate());
        entity.setLastStateRequest(getRandomDate());
        entity.setLastStateChange(getRandomDate());
        entity.setEmailCreated(getRandomDate());
        entity.setEmailType("email type");
        entity.setDocumentsRequested(getRandomDate());
        entity.setDocumentsSent(getRandomDate());
        entity.setAdditionalData("additionalData");
        entity.setHouseholdCalculation(BrokerageHouseholdCalculationEntityBuilder.aBuilder().withData().build());
        entity.setPayoutDate(getRandomDate());
        entity.setSecondApplicantAllowed(true);

        return this;
    }

    public BrokerageAppEntityBuilder withReferenceBankAccount() {
        entity.setReferenceBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public BrokerageAppEntityBuilder withFirstApplicant() {
        entity.setFirstApplicant(
                ApplicantEntityBuilder.aBuilder()
                        .withPersonalData()
                        .withPhone()
                        .withHomeAddress()
                        .withPreviousHomeAddress()
                        .withBankAccount()
                        .withNationalityInfo()
                        .withHousehold()
                        .withPreviousEmploymentAsEmployee()
                        .withMainIncomeAsEmployee()
                        .withThirdPartyLoans()
                        .withRandomEmail(20)
                        .build());

        return this;
    }

    public BrokerageAppEntityBuilder withSecondApplicant() {
        entity.setSecondApplicant(
                ApplicantEntityBuilder.aBuilder()
                        .withPersonalData()
                        .withPhone()
                        .withHomeAddress()
                        .withPreviousHomeAddress()
                        .withBankAccount()
                        .withNationalityInfo()
                        .withHousehold()
                        .withPreviousEmploymentAsEmployee()
                        .withMainIncomeAsEmployee()
                        .withThirdPartyLoans()
                        .withRandomEmail(20)
                        .build());

        return this;
    }

    public BrokerageAppEntityBuilder withLoanApplication() {
        entity.setLoanApplication(LoanApplicationEntityBuilder.aBuilder().withData().withApplicantRelationship().build());

        return this;
    }

    public BrokerageAppEntityBuilder withLoanApplicationFullData() {
        entity.setLoanApplication(LoanApplicationEntityBuilder.aBuilder().withFullData().build());

        return this;
    }

    public BrokerageAppEntityBuilder withAccount() {
        entity.setAccount(AccountEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public BrokerageAppEntityBuilder withFirstApplicantHavingRentExpenses() {
        entity.setFirstApplicant(
                ApplicantEntityBuilder.aBuilder()
                        .withPersonalData()
                        .withPhone()
                        .withHomeAddress()
                        .withBankAccount()
                        .withNationalityInfo()
                        .withHousehold()
                        .withPreviousEmploymentAsEmployee()
                        .withThirdPartyLoans()
                        .withMainIncomeAsEmployee()
                        .withRentExpenses()
                        .build());

        return this;
    }

    public BrokerageAppEntityBuilder withSecondApplicantHavingRentExpenses() {
        entity.setSecondApplicant(
                ApplicantEntityBuilder.aBuilder()
                        .withPersonalData()
                        .withPhone()
                        .withHomeAddress()
                        .withBankAccount()
                        .withNationalityInfo()
                        .withHousehold()
                        .withPreviousEmploymentAsEmployee()
                        .withThirdPartyLoans()
                        .withMainIncomeAsEmployee()
                        .withRentExpenses()
                        .build());

        return this;
    }

    public BrokerageAppEntityBuilder withElderlyFirstApplicant() {
        entity.setFirstApplicant(
                ApplicantEntityBuilder.aBuilder()
                        .withElderlyPersonalData()
                        .withPhone()
                        .withHomeAddress()
                        .withBankAccount()
                        .withNationalityInfo()
                        .withHousehold()
                        .withPreviousEmploymentAsEmployee()
                        .withThirdPartyLoans()
                        .withRentExpenses()
                        .build());

        return this;
    }

    public BrokerageAppEntityBuilder withHouseholdCalculation() {
        entity.setHouseholdCalculation(
                BrokerageHouseholdCalculationEntityBuilder.aBuilder()
                        .withData()
                        .withSecondApplicant()
                        .build()
        );

        return this;
    }
}
