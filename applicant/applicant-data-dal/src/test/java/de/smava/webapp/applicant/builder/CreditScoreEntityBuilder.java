package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.CreditScore;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class CreditScoreEntityBuilder extends EntityBuilder<CreditScore> {

    protected CreditScore createEntity() {
        return new CreditScore();
    }

    public static CreditScoreEntityBuilder aBuilder() {
        return new CreditScoreEntityBuilder();
    }

    public CreditScoreEntityBuilder withData() {
        entity.setId(getNextId());
        entity.setRating("Some rating");
        entity.setScore(1234l);
        entity.setChangeDate(getRandomDate());
        entity.setType("sample type");

        return this;
    }
}
