package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Estate;
import de.smava.webapp.applicant.domain.Expense;
import de.smava.webapp.applicant.domain.OtherHousingCost;
import de.smava.webapp.applicant.type.EstateRentType;
import de.smava.webapp.applicant.type.ExpenseType;
import de.smava.webapp.applicant.type.OtherHousingCostType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Sebastian Hanulok
 * @since 2015-07-15.
 */
public class ExpensesEntityBuilder extends EntityBuilder<Set<Expense>> {

    protected Set<Expense> createEntity() {
        return new HashSet<Expense>();
    }

    public static ExpensesEntityBuilder aBuilder() {
        return new ExpensesEntityBuilder();
    }

    public ExpensesEntityBuilder withRent() {
        entity.add(ExpenseEntityBuilder.aBuilder().withRent().build());

        return this;
    }

    public ExpensesEntityBuilder withOtherHousingCosts() {
        entity.add(ExpenseEntityBuilder.aBuilder().withOtherHousingCosts().build());

        return this;
    }

    public ExpensesEntityBuilder withEstate() {
        entity.add(ExpenseEntityBuilder.aBuilder().withEstate().build());

        return this;
    }

    public ExpensesEntityBuilder withExpense(ExpenseType expenseType) {
        entity.add(ExpenseEntityBuilder.aBuilder().withExpense(expenseType).build());

        return this;
    }

    public ExpensesEntityBuilder withAllDefinedExpenses() {
        entity.addAll(prepareExpenseTestingDataMap().values());

        return this;
    }

    public static Map<String, Expense> prepareExpenseTestingDataMap() {
        int counter = 2;
        double amount = fib(counter);
        Map<String, Expense> expenseMap = new HashMap<String, Expense>();

        for (ExpenseType expenseType : ExpenseType.values()) {

            if (ExpenseType.HOUSING_COST.equals(expenseType)) {

                for (OtherHousingCostType otherHousingCostType : OtherHousingCostType.values()) {
                    OtherHousingCost expense = (OtherHousingCost) ExpenseEntityBuilder.aBuilder().withOtherHousingCosts().build();
                    expense.setOtherHousingCostType(otherHousingCostType);
                    expense.setMonthlyAmount(amount);
                    expenseMap.put(expenseType.name().concat(otherHousingCostType.name()), expense);

                    counter++;
                    amount = fib(counter);
                }

                for (EstateRentType estateRentType : EstateRentType.values()) {
                    Estate estateExpense = (Estate) ExpenseEntityBuilder.aBuilder().withEstate().build();
                    estateExpense.setPartiallyRented(estateRentType);
                    estateExpense.setMonthlyAmount(amount);
                    expenseMap.put(expenseType.name().concat(estateRentType.name()), estateExpense);

                    counter++;
                    amount = fib(counter);
                }

            } else {
                Expense expense = ExpenseEntityBuilder.aBuilder().withExpense(expenseType).build();
                expense.setMonthlyAmount(amount);
                expenseMap.put(expenseType.name(), expense);

                counter++;
                amount = fib(counter);
            }
        }

        return expenseMap;
    }

    private static double fib(int n) {
        if (n > 2) {
            return fib(n - 1) + fib(n - 2);
        }

        return 1;
    }
}
