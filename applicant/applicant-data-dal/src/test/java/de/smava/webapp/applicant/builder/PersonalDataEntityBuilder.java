package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.PersonalData;
import de.smava.webapp.applicant.type.Gender;
import de.smava.webapp.applicant.type.MaritalState;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class PersonalDataEntityBuilder extends EntityBuilder<PersonalData> {

    protected PersonalData createEntity() {
        return new PersonalData();
    }

    public static PersonalDataEntityBuilder aBuilder() {
        return new PersonalDataEntityBuilder();
    }

    public PersonalDataEntityBuilder withData() {
        entity.setId(getNextId());
        entity.setFirstName("First Name");
        entity.setLastName("Last Name");
        entity.setBirthName("Birth Name");
        entity.setGender(Gender.MALE);
        entity.setBirthDate(getRandomDate());
        entity.setMaritalState(MaritalState.MARRIED);

        return this;
    }

    public PersonalDataEntityBuilder withRandomData(Integer length) {

        entity.setId(getNextId());
        entity.setFirstName(generateRandomAlphabetic(length));
        entity.setLastName(generateRandomAlphabetic(length));
        entity.setBirthName(generateRandomAlphabetic(length));
        entity.setGender(GenderEntityBuilder.aBuilder().withRandom().build());
        entity.setBirthDate(getRandomDate());
        entity.setMaritalState(MaritalStateEntityBuilder.aBuilder().withRandom().build());

        return this;
    }

    public PersonalDataEntityBuilder withElderlyPersonalData() {
        entity.setFirstName("Darth");
        entity.setLastName("Vader");
        entity.setId(getNextId());
        entity.setGender(Gender.MALE);
        entity.setBirthDate(getDate("1959-09-10 12:23:34"));
        entity.setMaritalState(MaritalState.MARRIED);

        return this;
    }
}
