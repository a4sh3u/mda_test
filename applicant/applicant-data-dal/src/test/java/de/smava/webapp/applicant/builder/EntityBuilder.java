package de.smava.webapp.applicant.builder;


import org.apache.commons.lang.RandomStringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sebastian Hanulok on 2015-07-16.
 */
public abstract class EntityBuilder<T> {
    private static Long idValue = 1L;
    protected T entity;

    protected Long getNextId() {
        return idValue++;
    }

    protected abstract T createEntity();

    protected EntityBuilder() {
        this.entity = createEntity();
    }

    protected void setEntity(T value) {
        this.entity = value;
    }

    public T build() {
        return entity;
    }

    public static Date getDate(String value) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        try {
            return dateFormatter.parse(value);
        } catch (ParseException e) {
            throw new RuntimeException("Error during parsing. Expected pattern is: yyyy-MM-dd hh:mm:ss for value: " + value, e);
        }
    }

    protected Date getRandomDate() {
        return getDate("2014-12-12 01:02:03");
    }

    protected String generateRandomAlphabetic(int length) {
       return RandomStringUtils.randomAlphabetic(length);
    }

    protected String generateRandomNumeric(int length) {
       return RandomStringUtils.randomNumeric(length);
    }

    protected String generateRandomAlphanumeric(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

}
