package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.ThirdPartyLoan;
import de.smava.webapp.applicant.type.ThirdPartyLoanSource;
import de.smava.webapp.applicant.type.ThirdPartyLoanType;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class ThirdPartyLoansEntityBuilder extends EntityBuilder<Set<ThirdPartyLoan>> {

    protected Set<ThirdPartyLoan> createEntity() {
        return new HashSet<ThirdPartyLoan>();
    }

    public static ThirdPartyLoansEntityBuilder aBuilder() {
        return new ThirdPartyLoansEntityBuilder();
    }

    public ThirdPartyLoansEntityBuilder withData() {
        return withData(true, ThirdPartyLoanType.OTHER);
    }

    public ThirdPartyLoansEntityBuilder withData(Boolean consolidated) {
        return withData(consolidated, ThirdPartyLoanType.OTHER);
    }

    public ThirdPartyLoansEntityBuilder withData(Boolean consolidated, ThirdPartyLoanType loanType) {
        ThirdPartyLoan ThirdPartyLoan = ThirdPartyLoanEntityBuilder.aBuilder().withData(consolidated, loanType).build();

        entity.add(ThirdPartyLoan);

        return this;
    }

    public ThirdPartyLoansEntityBuilder withAllSourceTypes() {
        entity.add(ThirdPartyLoanEntityBuilder.aBuilder().withData(ThirdPartyLoanSource.ADVISOR).build());
        entity.add(ThirdPartyLoanEntityBuilder.aBuilder().withData(ThirdPartyLoanSource.CUSTOMER).build());
        entity.add(ThirdPartyLoanEntityBuilder.aBuilder().withData(ThirdPartyLoanSource.DELETED).build());
        entity.add(ThirdPartyLoanEntityBuilder.aBuilder().withData(ThirdPartyLoanSource.MERGED).build());
        entity.add(ThirdPartyLoanEntityBuilder.aBuilder().withData(ThirdPartyLoanSource.SCHUFA).build());

        return this;
    }

    public ThirdPartyLoansEntityBuilder withAllTypes() {
        ThirdPartyLoan elem;
        List<Boolean> consolidation = Arrays.asList(true, false);
        Double drawingLimit = 1000d;
        Double monthlyRate = 100d;

        for(ThirdPartyLoanType type: ThirdPartyLoanType.values()) {
            for (Boolean item : consolidation) {
                elem = ThirdPartyLoanEntityBuilder.aBuilder().withData(item, type).build();
                elem.setDrawingLimit(drawingLimit);
                elem.setMonthlyRate(monthlyRate);
                entity.add(elem);

                drawingLimit++;
                monthlyRate++;
            }
        }

        return this;
    }
}
