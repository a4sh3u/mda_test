package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Phone;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class PhonesEntityBuilder extends EntityBuilder<Phone> {

    protected Phone createEntity() {
        return new Phone();
    }

    public static PhonesEntityBuilder aBuilder() {
        return new PhonesEntityBuilder();
    }

    public PhonesEntityBuilder withLandLine() {
        entity = PhoneEntityBuilder.aBuilder().withLandLine().build();

        return this;
    }

    public PhonesEntityBuilder withMobile() {
        entity = PhoneEntityBuilder.aBuilder().withMobile().build();

        return this;
    }
}
