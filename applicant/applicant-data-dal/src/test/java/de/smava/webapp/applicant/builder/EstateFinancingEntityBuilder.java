package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.EstateFinancing;

import java.util.Random;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class EstateFinancingEntityBuilder extends EntityBuilder<EstateFinancing> {

    protected EstateFinancing createEntity() {
        return new EstateFinancing();
    }

    public static EstateFinancingEntityBuilder aBuilder() {
        return new EstateFinancingEntityBuilder();
    }

    public EstateFinancing build() {
        return entity;
    }

    public EstateFinancingEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setMonthlyFinancingAmount(new Random().nextDouble());

        return this;
    }
}
