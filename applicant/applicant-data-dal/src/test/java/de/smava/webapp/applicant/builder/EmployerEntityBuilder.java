package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Employer;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class EmployerEntityBuilder extends EntityBuilder<Employer> {

    protected Employer createEntity() { return new Employer();
    }

    public static EmployerEntityBuilder aBuilder() {
        return new EmployerEntityBuilder();
    }

    public EmployerEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setName("Employer name");
        entity.setPhone(PhoneEntityBuilder.aBuilder().withMobile().build());
        entity.setAddress(AddressEntityBuilder.aBuilder().withData().build());

        return this;
    }
}
