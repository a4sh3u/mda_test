package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Income;
import de.smava.webapp.applicant.type.*;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Sebastian Hanulok
 * @since 2015-07-15.
 */
public class AdditionalIncomesEntityBuilder extends EntityBuilder<Set<Income>> {

    protected Set<Income> createEntity() {
        return new HashSet<Income>();
    }

    public static AdditionalIncomesEntityBuilder aBuilder() {
        return new AdditionalIncomesEntityBuilder();
    }

    public AdditionalIncomesEntityBuilder withRetiree() {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withRetiree()
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withRetiree(IncomeType incomeType, EmploymentType employmentType, RetireeType retireeType, PensionType pensionType) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withRetiree(
                        incomeType,
                        employmentType,
                        retireeType,
                        pensionType
                )
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withCeo() {
        Income additionalIncome = IncomeEntityBuilder.aBuilder().withCeo().build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withCeo(IncomeType incomeType, SalariedStaffType salariedStaffType) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withCeo(
                        incomeType,
                        salariedStaffType)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withEmployee() {
        Income additionalIncome = IncomeEntityBuilder.aBuilder().withEmployee().build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, EmployeeType employeeType) {
        return withEmployee(incomeType, employmentType, salariedStaffType, employeeType, null);
    }

    public AdditionalIncomesEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, EmployeeType employeeType, Double monthlyNetIncome) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withEmployee(
                        incomeType,
                        employmentType,
                        salariedStaffType,
                        employeeType,
                        monthlyNetIncome)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withSoldier() {
        Income additionalIncome = IncomeEntityBuilder.aBuilder().withSoldier().build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withSoldier(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, SoldierType soldierType) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withSoldier(
                        incomeType,
                        employmentType,
                        salariedStaffType,
                        soldierType)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withSpouseAlimony() {
        return withSpouseAlimony(350d);
    }

    public AdditionalIncomesEntityBuilder withSpouseAlimony(Double monthlyNetIncome) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withSpouseAlimony(monthlyNetIncome)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withRentalIncome() {
        return withRentalIncome(400d);
    }

    public AdditionalIncomesEntityBuilder withRentalIncome(Double monthlyNetIncome) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withRentalIncome(monthlyNetIncome)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withChildBenefit() {
        return withChildBenefit(400d);
    }

    public AdditionalIncomesEntityBuilder withChildBenefit(Double monthlyNetIncome) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withChildBenefit(monthlyNetIncome)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withPensionSupplement() {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withPensionSupplement()
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withChildAlimony() {
        return withChildAlimony(400d);
    }

    public AdditionalIncomesEntityBuilder withChildAlimony(Double monthlyNetIncome) {
        Income additionalIncome = IncomeEntityBuilder.aBuilder()
                .withChildAlimony(monthlyNetIncome)
                .build();

        entity.add(additionalIncome);

        return this;
    }

    public AdditionalIncomesEntityBuilder withAllIncomeTypes() {
        Set<Income> incomes = new HashSet<Income>();
        int counter = 2;

        for (IncomeType incomeType : IncomeType.values()) {
            incomes.add(IncomeEntityBuilder.aBuilder().withIncomeType(incomeType, fib(counter)).build());
            counter++;
        }

        entity.addAll(incomes);

        return this;
    }

    public AdditionalIncomesEntityBuilder withAllSalariedStaffTypes() {
        Set<Income> incomes = new HashSet<Income>();
        int counter = 2;

        for (SalariedStaffType salariedStaffType : SalariedStaffType.values()) {
            incomes.add(
                    IncomeEntityBuilder.aBuilder().withEmployee(
                            IncomeType.EMPLOYMENT,
                            EmploymentType.SALARIED_STAFF,
                            salariedStaffType,
                            EmployeeType.EMPLOYEE,
                            fib(counter)
                    ).build());
            counter++;
        }

        entity.addAll(incomes);

        return this;
    }

    public AdditionalIncomesEntityBuilder withAllPensionTypes() {
        Set<Income> incomes = new HashSet<Income>();
        int counter = 2;

        for (PensionType pensionType : PensionType.values()) {
            incomes.add(
                    IncomeEntityBuilder.aBuilder().withPensionSupplement(
                            pensionType,
                            IncomeType.PENSION_SUPPLEMENT,
                            fib(counter)
                    ).build());
            counter++;
        }

        entity.addAll(incomes);

        return this;
    }

    private double fib(int n) {
        if (n > 2) {
            return fib(n - 1) + fib(n - 2);
        }

        return 1;
    }
}
