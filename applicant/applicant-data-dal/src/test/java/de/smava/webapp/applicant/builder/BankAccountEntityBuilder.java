package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.BankAccount;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class BankAccountEntityBuilder extends EntityBuilder<BankAccount> {

    protected BankAccount createEntity() {
        return new BankAccount();
    }

    public static BankAccountEntityBuilder aBuilder() {
        return new BankAccountEntityBuilder();
    }

    public BankAccountEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setBankName("Bank name");
        entity.setIban("DE89 3704 0044 0532 0130 00");
        entity.setBankCode("1234");
        entity.setAccountNumber("1234 56789");
        entity.setBic("Bic");

        return this;
    }

    public BankAccountEntityBuilder withRandomData(int length) {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setBankName("Bank name" + generateRandomAlphabetic(length));
        entity.setIban("Iban number" + generateRandomAlphanumeric(length));
        entity.setBankCode("12345678"+ generateRandomAlphanumeric(length));
        entity.setAccountNumber("12345 67890" + generateRandomAlphanumeric(length));
        entity.setBic("Bic" + generateRandomAlphabetic(length));

        return this;
    }
}
