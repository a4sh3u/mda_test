package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.ApplicantsRelationship;
import de.smava.webapp.applicant.type.ApplicantsRelationshipType;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class ApplicantRelationshipEntityBuilder extends EntityBuilder<ApplicantsRelationship> {

    protected ApplicantsRelationship createEntity() {
        return new ApplicantsRelationship();
    }

    public static ApplicantRelationshipEntityBuilder aBuilder() {
        return new ApplicantRelationshipEntityBuilder();
    }

    public ApplicantRelationshipEntityBuilder withFullData() {
        entity.setApplicant1(ApplicantEntityBuilder.aBuilder().withFullData().build());
        entity.setApplicant2(ApplicantEntityBuilder.aBuilder().withFullData().build());
        entity.setRelationshipType(ApplicantsRelationshipType.MARRIED);
        entity.setSharedHousehold(true);

        return this;
    }

    public ApplicantRelationshipEntityBuilder withApplicants() {
        entity.setApplicant1(ApplicantEntityBuilder.aBuilder()
                .withData()
                .withMainIncomeAsEmployee()
                .withPersonalData()
                .withNationalityInfo()
                .build());

        entity.setApplicant2(ApplicantEntityBuilder.aBuilder()
                .withData()
                .withMainIncomeAsEmployee()
                .withPersonalData()
                .withNationalityInfo()
                .build());

        return this;
    }

    public ApplicantRelationshipEntityBuilder withMarried() {
        entity.setId(getNextId());
        entity.setRelationshipType(ApplicantsRelationshipType.MARRIED);
        entity.setSharedHousehold(true);

        return this;
    }
}
