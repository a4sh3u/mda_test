package de.smava.webapp.applicant.domain.abstracts;

import de.smava.webapp.applicant.builder.ApplicantEntityBuilder;
import de.smava.webapp.applicant.builder.EmployerEntityBuilder;
import de.smava.webapp.applicant.builder.EmploymentEntityBuilder;
import de.smava.webapp.applicant.builder.IncomeEntityBuilder;
import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.type.*;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.hamcrest.MatcherAssert;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by PAWEL on 2016-02-25.
 */
public class AbstractApplicantMainIncomeTest {

    @Test
    public void testRetrieveEmployer() throws Exception {
        //for Employee
        Applicant applicant = ApplicantEntityBuilder.aBuilder().withMainIncomeAsEmployee().build();
        Employer employer = ((Employee) applicant.getMainIncome()).getEmployer();

        //when
        Employer employerRetieved = applicant.retrieveEmployer();

        //then
        assertThat("Employee", employer, is(employerRetieved));


        //for retiree
        applicant = ApplicantEntityBuilder.aBuilder().build();
        Retiree retiree = (Retiree) IncomeEntityBuilder.aBuilder().withRetiree().build();
        Employer rentInsurance = EmployerEntityBuilder.aBuilder().withData().build();
        retiree.setRentInsurance(rentInsurance);
        applicant.setMainIncome(retiree);

        //when
        employerRetieved = applicant.retrieveEmployer();

        //then
        assertThat("Retiree", rentInsurance, is(employerRetieved));


        // for empty Employer
        retiree.setRentInsurance(null);
        applicant.setMainIncome(retiree);

        //when
        employerRetieved = applicant.retrieveEmployer();

        //then
        assertThat("Retiree with empty employer", employerRetieved, is(nullValue()));


        // for empty
        applicant = new Applicant();

        //when
        employerRetieved = applicant.retrieveEmployer();

        //then
        assertThat("empty", employerRetieved, is(nullValue()));
    }

    @Test
    public void testRetrieveIsSalariedStaff() throws Exception {
        //for correct basic
        Applicant applicant = ApplicantEntityBuilder.aBuilder().withMainIncomeAsEmployee().build();

        //when
        boolean isSalariedStaff = applicant.retrieveIsSalariedStaff();

        //then
        assertThat("correct basic", isSalariedStaff, is(true));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        isSalariedStaff = applicant.retrieveIsSalariedStaff();

        //then
        assertThat("empty", isSalariedStaff, is(false));


        //for correct Soldier
        applicant = ApplicantEntityBuilder.aBuilder().build();
        Income soldier = IncomeEntityBuilder.aBuilder().withSoldier().build();
        applicant.setMainIncome(soldier);

        //when
        isSalariedStaff = applicant.retrieveIsSalariedStaff();

        //then
        assertThat("correct Soldier", isSalariedStaff, is(true));


        //for incorrect SelfEmployed
        applicant = ApplicantEntityBuilder.aBuilder().build();
        Income selfEmployed = IncomeEntityBuilder.aBuilder().withSelfEmployed().build();
        applicant.setMainIncome(selfEmployed);

        //when
        isSalariedStaff = applicant.retrieveIsSalariedStaff();

        //then
        assertThat("incorrect SelfEmployed", isSalariedStaff, is(false));


        //for correct Official
        applicant = ApplicantEntityBuilder.aBuilder().build();
        Income official = IncomeEntityBuilder.aBuilder().withOfficial().build();
        applicant.setMainIncome(official);

        //when
        isSalariedStaff = applicant.retrieveIsSalariedStaff();

        //then
        assertThat("correct Official", isSalariedStaff, is(true));


        //for incorrect but set
        applicant = ApplicantEntityBuilder.aBuilder().build();
        Income retiree = IncomeEntityBuilder.aBuilder().withRetiree().build();
        applicant.setMainIncome(retiree);

        //when
        isSalariedStaff = applicant.retrieveIsSalariedStaff();

        //then
        assertThat("incorrect Retiree", isSalariedStaff, is(false));
    }

    @Test
    public void testRetrieveIsSalariedStaffWithIncomeParameter() throws Exception {
        //for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        SalariedStaff salariedStaff = (SalariedStaff) IncomeEntityBuilder.aBuilder().withEmployee().build();

        //when
        boolean isSalariedStaff = applicant.retrieveIsSalariedStaff(salariedStaff);

        //then
        assertThat(isSalariedStaff, is(true));

        //for
        Income retiree = IncomeEntityBuilder.aBuilder().withRetiree().build();

        //when
        isSalariedStaff = applicant.retrieveIsSalariedStaff(retiree);

        //then
        assertThat(isSalariedStaff, is(false));
    }

    @Test
    public void testRetrieveSalariedStaff() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Employment result = applicant.retrieveSalariedStaff();

        //then
        assertThat("empty", result, is(nullValue()));


        // for correct
        SalariedStaff salariedStaff = (SalariedStaff) IncomeEntityBuilder.aBuilder().withEmployee().build();
        applicant.setMainIncome(salariedStaff);

        //when
        result = applicant.retrieveSalariedStaff();

        //then
        assertThat("correct", (SalariedStaff) result, is(salariedStaff));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withRetiree().build());

        //when
        result = applicant.retrieveSalariedStaff();

        //then
        assertThat("incorrect", result, is(nullValue()));
    }


    @Test
    public void testRetrieveIsEmployee() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().withMainIncomeAsEmployee().build();

        //when
        boolean isEmployee = applicant.retrieveIsEmployee();

        //then
        assertThat("correct", isEmployee, is(true));


        //for incorrect
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withRetiree().build());

        //when
        isEmployee = applicant.retrieveIsEmployee();

        //then
        assertThat("incorrect", isEmployee, is(false));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        isEmployee = applicant.retrieveIsEmployee();

        //then
        assertThat("empty", isEmployee, is(false));
    }

    @Test
    public void testRetrieveIsEmployeeWithIncomeParameter() throws Exception {
        //for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().withMainIncomeAsEmployee().build();
        Income income = IncomeEntityBuilder.aBuilder().withRetiree().build();

        //when
        boolean isEmployee = applicant.retrieveIsEmployee(income);

        //then
        assertThat(isEmployee, is(false));

        //for
        income = IncomeEntityBuilder.aBuilder().withEmployee().build();

        //when
        isEmployee = applicant.retrieveIsEmployee(income);

        //then
        assertThat(isEmployee, is(true));
    }

    @Test
    public void testRetrieveIsSelfEmployed() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income selfEmployed = IncomeEntityBuilder.aBuilder().withSelfEmployed().build();
        applicant.setMainIncome(selfEmployed);

        //when
        boolean isSelfEmployed = applicant.retrieveIsSelfEmployed();

        //then
        assertThat("correct", isSelfEmployed, is(true));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withCeo().build());

        //when
        isSelfEmployed = applicant.retrieveIsSelfEmployed();

        //then
        assertThat("in-correct", isSelfEmployed, is(false));


        //for empty
        applicant = new Applicant();

        //when
        isSelfEmployed = applicant.retrieveIsSelfEmployed();

        //then
        assertThat("empty", isSelfEmployed, is(false));
    }

    @Test
    public void testRetrieveSelfEmployed() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        SelfEmployed selfEmployedRetrieved = applicant.retrieveSelfEmployed();

        //then
        assertThat("empty", selfEmployedRetrieved, is(nullValue()));


        // for correct
        Income selfEmployed = IncomeEntityBuilder.aBuilder().withSelfEmployed().build();
        applicant.setMainIncome(selfEmployed);

        //when
        selfEmployedRetrieved = applicant.retrieveSelfEmployed();

        //then
        assertThat("correct", selfEmployedRetrieved, is(selfEmployed));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withRetiree().build());

        //when
        selfEmployedRetrieved = applicant.retrieveSelfEmployed();

        //then
        assertThat("in-correct", selfEmployedRetrieved, is(nullValue()));
    }

    @Test
    public void testRetrieveIsSoldier() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income soldier = IncomeEntityBuilder.aBuilder().withSoldier().build();
        applicant.setMainIncome(soldier);

        //when
        boolean isSoldier = applicant.retrieveIsSoldier();

        //then
        assertThat("correct", isSoldier, is(true));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withRetiree().build());

        //when
        isSoldier = applicant.retrieveIsSoldier();

        //then
        assertThat("in-correct", isSoldier, is(false));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        isSoldier = applicant.retrieveIsSoldier();

        //then
        assertThat("empty", isSoldier, is(false));
    }


    @Test
    public void testRetrieveIsSoldierWithIncomeParameter() throws Exception {
        //for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income soldier = IncomeEntityBuilder.aBuilder().withSoldier().build();

        //when
        boolean isSoldier = applicant.retrieveIsSoldier(soldier);

        //then
        assertThat(isSoldier, is(true));

        //for
        Income selfEmployed = IncomeEntityBuilder.aBuilder().withSelfEmployed().build();

        //when
        isSoldier = applicant.retrieveIsSoldier(selfEmployed);

        //then
        assertThat(isSoldier, is(false));
    }

    @Test
    public void testRetrieveSoldier() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Soldier soldierRetrieved = applicant.retrieveSoldier();

        //then
        assertThat("empty", soldierRetrieved, is(nullValue()));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withRetiree().build());
        //when
        soldierRetrieved = applicant.retrieveSoldier();

        //then
        assertThat("in-correct", soldierRetrieved, is(nullValue()));


        //for correct
        Soldier soldier = (Soldier) IncomeEntityBuilder.aBuilder().withSoldier().build();
        soldier.setSoldierType(SoldierType.PROF_SOLDIER);
        applicant.setMainIncome(soldier);

        //when
        soldierRetrieved = applicant.retrieveSoldier();

        //then
        assertThat("correct", soldierRetrieved, is(soldier));
    }

    @Test
    public void testRetrieveEmployee() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Employment result = applicant.retrieveEmployee();

        //then
        assertThat("empty", result, is(nullValue()));


        // for correct
        Employee employee = (Employee) EmploymentEntityBuilder.aBuilder().withEmployee().build();
        applicant.setMainIncome(employee);

        //when
        result = applicant.retrieveEmployee();

        //then
        assertThat("correct", (Employee) result, is(employee));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withRetiree().build());

        //when
        result = applicant.retrieveEmployee();

        //then
        assertThat("in-correct", result, is(nullValue()));
    }

    @Test
    public void testRetrieveIsOfficial() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income official = IncomeEntityBuilder.aBuilder().withOfficial().build();
        applicant.setMainIncome(official);

        //when
        boolean isOfficial = applicant.retrieveIsOfficial();

        //then
        assertThat("correct", isOfficial, is(true));


        // for incorrect
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withFreelancer().build());

        //when
        isOfficial = applicant.retrieveIsOfficial();

        //then
        assertThat("in-correct", isOfficial, is(false));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        isOfficial = applicant.retrieveIsOfficial();

        //then
        assertThat("empty", isOfficial, is(false));
    }

    @Test
    public void testRetrieveIsOfficialWithIncomeParameter() throws Exception {
        //for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income official = IncomeEntityBuilder.aBuilder().withOfficial().build();

        //when
        boolean isOfficial = applicant.retrieveIsOfficial(official);

        //then
        assertThat(isOfficial, is(true));

        //for
        Income income = IncomeEntityBuilder.aBuilder().withEmployee().build();

        //when
        isOfficial = applicant.retrieveIsOfficial(income);

        //then
        assertThat(isOfficial, is(false));
    }

    @Test
    public void testRetrieveOfficial() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income official = IncomeEntityBuilder.aBuilder().withOfficial().build();
        applicant.setMainIncome(official);

        //when
        Employment result = applicant.retrieveOfficial();

        //then
        assertThat("correct", (Official) result, is(official));


        // for incorrect
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withFreelancer().build());

        //when
        result = applicant.retrieveOfficial();

        //then
        assertThat("in-correct", result, is(nullValue()));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        result = applicant.retrieveOfficial();

        //then
        assertThat("empty", result, is(nullValue()));
    }

    @Test
    public void testRetrieveIsRetiree() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income retiree = IncomeEntityBuilder.aBuilder().withRetiree().build();
        applicant.setMainIncome(retiree);

        //when
        boolean isRetiree = applicant.retrieveIsRetiree();

        //then
        assertThat("correct", isRetiree, is(true));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withCeo().build());

        //when
        isRetiree = applicant.retrieveIsRetiree();

        //then
        assertThat("in-correct", isRetiree, is(false));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        isRetiree = applicant.retrieveIsRetiree();

        //then
        assertThat("empty", isRetiree, is(false));
    }

    @Test
    public void testRetrieveRetiree() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Employment result = applicant.retrieveRetiree();

        //then
        assertThat("empty", result, is(nullValue()));


        // for correct
        Income retiree = IncomeEntityBuilder.aBuilder().withRetiree().build();
        applicant.setMainIncome(retiree);

        //when
        result = applicant.retrieveRetiree();

        //then
        MatcherAssert.assertThat(result, is(retiree));


        // for in-correct
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withCeo().build());

        //when
        result = applicant.retrieveRetiree();

        //then
        assertThat(result, is(nullValue()));
    }

    @Test
    public void testRetrieveIsRetireeWithIncomeParameter() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income retiree = IncomeEntityBuilder.aBuilder().withRetiree().build();

        //when
        boolean isRetiree = applicant.retrieveIsRetiree(retiree);

        //then
        assertThat("correct", isRetiree, is(true));


        // for in-correct
        //when
        isRetiree = applicant.retrieveIsRetiree(IncomeEntityBuilder.aBuilder().withCeo().build());

        //then
        assertThat("in-correct", isRetiree, is(false));


        //for empty
        //when
        isRetiree = applicant.retrieveIsRetiree(null);

        //then
        assertThat("empty", isRetiree, is(false));
    }

    @Test
    public void testRetrieveIsPensioner() throws Exception {
        //for correct
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Retiree pensioner = (Retiree) IncomeEntityBuilder.aBuilder().withRetiree().build();
        pensioner.setRetireeType(RetireeType.PENSIONER);
        applicant.setMainIncome(pensioner);

        //when
        boolean isPensioner = applicant.retrieveIsPensioner();

        //then
        assertThat("correct", isPensioner, is(true));


        // for in-correct
        pensioner.setRetireeType(RetireeType.RETIREE);
        applicant.setMainIncome(pensioner);

        //when
        isPensioner = applicant.retrieveIsPensioner();

        //then
        assertThat("in-correct", isPensioner, is(false));


        //for empty
        applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        isPensioner = applicant.retrieveIsPensioner();

        //then
        assertThat("empty", isPensioner, is(false));
    }

    @Test
    public void testRetrieveIsFreelancer() throws Exception {

        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Boolean result = applicant.retrieveIsFreelancer();

        //then
        assertThat("empty", result, is(false));

        // for correct
        Employment freelancer = (Freelancer) IncomeEntityBuilder.aBuilder().withFreelancer().build();
        applicant.setMainIncome(freelancer);

        //when
        result = applicant.retrieveIsFreelancer();

        //then
        assertThat("correct", result, is(true));


        // for correct
        Employment employment = (Employment) IncomeEntityBuilder.aBuilder().withRetiree().build();
        applicant.setMainIncome(employment);

        //when
        result = applicant.retrieveIsFreelancer();

        //then
        assertThat("in-correct", result, is(false));

    }

    @Test
    public void testRetrieveIsFreelancerWithIncomeParameter() throws Exception {

        //for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Freelancer freelancer = (Freelancer) IncomeEntityBuilder.aBuilder().withFreelancer().build();

        //when
        boolean isFreelancer = applicant.retrieveIsFreelancer(freelancer);

        //then
        assertThat(isFreelancer, is(true));

        //for
        SalariedStaff mainIncome = (SalariedStaff) IncomeEntityBuilder.aBuilder().withEmployee().build();

        //when
        isFreelancer = applicant.retrieveIsFreelancer(mainIncome);

        //then
        assertThat(isFreelancer, is(false));
    }

    @Test
    public void testRetrieveFreelancer() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Employment result = applicant.retrieveFreelancer();

        //then
        assertThat("empty", result, is(nullValue()));

        // for correct
        Employment freelancer = (Freelancer) IncomeEntityBuilder.aBuilder().withFreelancer().build();
        applicant.setMainIncome(freelancer);

        //when
        result = applicant.retrieveFreelancer();

        //then
        assertThat("correct", result, is(freelancer));


        // for correct
        Employment employment = (Employment) IncomeEntityBuilder.aBuilder().withRetiree().build();
        applicant.setMainIncome(employment);

        //when
        result = applicant.retrieveFreelancer();

        //then
        assertThat("in-correct", result, is(nullValue()));
    }

    @Test
    public void testRetrieveIsEmployment() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        boolean isEmployment = applicant.retrieveIsEmployment();

        //then
        assertThat("empty", isEmployment, is(false));


        // for correct
        Income employment = EmploymentEntityBuilder.aBuilder().withEmployee().build();
        applicant.setMainIncome(employment);

        //when
        isEmployment = applicant.retrieveIsEmployment();

        //then
        assertThat("correct", isEmployment, is(true));


        //for incorrect
        employment = IncomeEntityBuilder.aBuilder().withRentalIncome().build();
        applicant.setMainIncome(employment);

        //when
        isEmployment = applicant.retrieveIsEmployment();

        //then
        assertThat("incorrect", isEmployment, is(false));
    }

    @Test
    public void testRetrieveIsEmploymentWithIncomeParameter() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        boolean isEmployment = applicant.retrieveIsEmployment(null);

        //then
        assertThat("empty", isEmployment, is(false));


        // for correct
        Income employment = EmploymentEntityBuilder.aBuilder().withEmployee().build();
        applicant.setMainIncome(employment);

        //when
        isEmployment = applicant.retrieveIsEmployment(employment);

        //then
        assertThat("correct", isEmployment, is(true));


        //for incorrect
        employment = IncomeEntityBuilder.aBuilder().withRentalIncome().build();
        applicant.setMainIncome(employment);

        //when
        isEmployment = applicant.retrieveIsEmployment(employment);

        //then
        assertThat("incorrect", isEmployment, is(false));
    }

    @Test
    public void testRetrieveEmployment() throws Exception {
        //for empty
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Employment employment = EmploymentEntityBuilder.aBuilder().withEmployee().build();

        //when
        Employment employmentRetrieved = applicant.retrieveEmployment();

        //then
        assertThat("empty", employmentRetrieved, is(nullValue()));


        // for correct
        applicant.setMainIncome(employment);

        //when
        employmentRetrieved = applicant.retrieveEmployment();

        //then
        assertThat("correct", employmentRetrieved, is(employment));
    }
}