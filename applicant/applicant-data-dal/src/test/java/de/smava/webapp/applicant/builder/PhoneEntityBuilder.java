package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Phone;
import de.smava.webapp.applicant.type.PhoneType;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class PhoneEntityBuilder extends EntityBuilder<Phone> {

    protected Phone createEntity() {
        return new Phone();
    }

    public static PhoneEntityBuilder aBuilder() {
        return new PhoneEntityBuilder();
    }

    public PhoneEntityBuilder withLandLine() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setCode("321");
        entity.setNumber("789 456 123");
        entity.setPhoneType(PhoneType.LANDLINE);

        return this;
    }
    public PhoneEntityBuilder withMobile() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setCode("123");
        entity.setNumber("123 456 789");
        entity.setPhoneType(PhoneType.MOBILE);

        return this;
    }

    public PhoneEntityBuilder withRandomLandLine() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setCode(generateRandomNumeric(3));
        entity.setNumber(generateRandomNumeric(3) + " " + generateRandomNumeric(3) + " " + generateRandomNumeric(3));
        entity.setPhoneType(PhoneType.LANDLINE);

        return this;
    }

    public PhoneEntityBuilder withRandomMobile() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setCode(generateRandomNumeric(3));
        entity.setNumber(generateRandomNumeric(3) + " " + generateRandomNumeric(3) + " " + generateRandomNumeric(3));
        entity.setPhoneType(PhoneType.MOBILE);

        return this;
    }

    public PhoneEntityBuilder withRandomNumber(int length, PhoneType type) {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCreationDate(getRandomDate());
        entity.setCode(generateRandomNumeric(3));
        entity.setNumber(generateRandomNumeric(length));
        entity.setPhoneType(type);

        return this;
    }
}
