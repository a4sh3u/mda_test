package de.smava.webapp.applicant.domain.abstracts;


import de.smava.webapp.applicant.domain.Applicant;
import de.smava.webapp.applicant.domain.Estate;
import de.smava.webapp.applicant.domain.Expense;
import de.smava.webapp.applicant.domain.Investment;
import de.smava.webapp.applicant.builder.ExpenseEntityBuilder;
import de.smava.webapp.applicant.type.EstateRentType;
import de.smava.webapp.applicant.type.ExpenseType;
import de.smava.webapp.applicant.builder.ExpensesEntityBuilder;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by PAWEL on 2016-02-25.
 */
public class AbstractApplicantExpenseTest {

    @Test
    public void testRetrievePrivateLeasings() throws Exception {
        // for
        Map<String, Expense> expenseMap = ExpensesEntityBuilder.prepareExpenseTestingDataMap();
        Set<Expense> expenses = new HashSet<Expense>();
        Expense expenseLeasing = expenseMap.get(ExpenseType.PRIVATE_LEASING.name());
        expenses.addAll(expenseMap.values());

        Applicant applicant = new Applicant();
        applicant.setExpenses(expenses);

        // when
        List<Expense> leasingExpenses = applicant.retrievePrivateLeasings();

        // then
        assertThat(leasingExpenses, is(notNullValue()));
        assertThat(leasingExpenses.size(), is(1));
        assertThat(leasingExpenses.contains(expenseLeasing), is(true));
    }

    @Test
    public void testRetrieveBusinessLeasings() throws Exception {
        // for
        Map<String, Expense> expenseMap = ExpensesEntityBuilder.prepareExpenseTestingDataMap();
        Set<Expense> expenses = new HashSet<Expense>();
        Expense expenseBusinessLeasing = expenseMap.get(ExpenseType.BUSINESS_LEASING.name());

        expenses.addAll(expenseMap.values());
        Applicant applicant = new Applicant();
        applicant.setExpenses(expenses);

        // when
        List<Expense> leasingExpenses = applicant.retrieveBusinessLeasings();

        // then
        assertThat(leasingExpenses, is(notNullValue()));
        assertThat(leasingExpenses.size(), is(1));
        assertThat(leasingExpenses.contains(expenseBusinessLeasing), is(true));
    }

    @Test
    public void testRetrieveIsPrivatelyHealthInsured() throws Exception {
        // for empty
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build();
        Applicant applicant = new Applicant();

        // when
        boolean isHealthInsured = applicant.retrieveIsPrivatelyHealthInsured();

        // then
        assertThat("is empty", isHealthInsured, is(false));


        // for set correctly
        applicant.setExpenses(expenses);

        // when
        isHealthInsured = applicant.retrieveIsPrivatelyHealthInsured();

        // then
        assertThat("is correct", isHealthInsured, is(true));
    }

    @Test
    public void testRetrieveExpensesByType() throws Exception {
        // for empty
        Applicant applicant = new Applicant();

        // when
        List<Expense> result = applicant.retrieveExpenses(ExpenseType.CHILD_ALIMONY);

        // then
        assertThat("Empty", result, is(notNullValue()));
        assertThat("Empty", result.size(), is(0));


        // for existing
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build();
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(ExpenseType.CHILD_ALIMONY);

        // then
        assertThat("Exists 1", result, is(notNullValue()));
        assertThat("Exists 1", result.size(), is(1));


        // for duplicated
        Expense expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(ExpenseType.INVESTMENT);

        // then
        assertThat("Exists 1", result, is(notNullValue()));
        assertThat("Exists 1", result.size(), is(1));

        // for not existing
        expenses.clear();
        expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(ExpenseType.CHILD_ALIMONY);

        // then
        assertThat("Not Exists", result, is(notNullValue()));
        assertThat("Not Exists", result.size(), is(0));
    }

    @Test
    public void testRetrieveExpensesListByTypeEmptyExpenses() throws Exception {
        // for empty
        Applicant applicant = new Applicant();

        // when
        List<Expense> result = applicant.retrieveExpenses(Arrays.asList(ExpenseType.CHILD_ALIMONY));

        // then
        assertThat("Table should not be empty", result, is(notNullValue()));
    }

    @Test
    public void testRetrieveExpensesListByTypeExistingExpense() throws Exception {
        // for existing
        Applicant applicant = new Applicant();
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build();
        applicant.setExpenses(expenses);

        // when
        List<Expense> result = applicant.retrieveExpenses(Arrays.asList(ExpenseType.CHILD_ALIMONY));

        // then
        assertThat("Table should not be empty", result, is(notNullValue()));
        assertThat("Should find only one", result.size(), is(1));
        assertThat("Should find CHILD_ALIMONY", result.get(0).getExpenseType(), is(ExpenseType.CHILD_ALIMONY));

    }

    @Test
    public void testRetrieveExpensesListByTypeManyExpenses() throws Exception {
        // for duplicated
        Applicant applicant = new Applicant();
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build();
        Expense expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        List<Expense> result = applicant.retrieveExpenses(Arrays.asList(ExpenseType.INVESTMENT));

        // then
        assertThat("Table should not be empty", result, is(notNullValue()));
        assertThat("Should find only one", result.size(), is(1));
        assertThat("Should find INVESTMENT", result.get(0).getExpenseType(), is(ExpenseType.INVESTMENT));
    }

    @Test
    public void testRetrieveExpensesListByTypeCollectManyExpenses() throws Exception {
        // for duplicated
        Applicant applicant = new Applicant();
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build();
        Expense expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        List<Expense> result = applicant.retrieveExpenses(Arrays.asList(ExpenseType.INVESTMENT, ExpenseType.CHILD_ALIMONY));

        // then
        assertThat("Table should not be empty", result, is(notNullValue()));
        assertThat("Should find only one", result.size(), is(2));
    }

    @Test
    public void testRetrieveExpensesListByTypeMissingExpenses() throws Exception {
        // for not existing
        Applicant applicant = new Applicant();
        Set<Expense> expenses = new HashSet<Expense>();
        Expense expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        List<Expense> result = applicant.retrieveExpenses(Arrays.asList(ExpenseType.CHILD_ALIMONY));

        // then
        assertThat("Table should not be empty", result, is(notNullValue()));
        assertThat("Did not expect to find expense", result.size(), is(0));
    }

    @Test
    public void testRetrieveExpensesByClass() throws Exception {
        // for empty
        Applicant applicant = new Applicant();

        // when
        List<Expense> result = applicant.retrieveExpenses(Investment.class);

        // then
        assertThat("Empty", result, is(notNullValue()));
        assertThat("Empty", result.size(), is(0));


        // for existing
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withEstate().build();
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(Estate.class);

        // then
        assertThat("Exists 1", result, is(notNullValue()));
        assertThat("Exists 1", result.size(), is(1));


        // for duplicated
        Expense expense = ExpenseEntityBuilder.aBuilder().withEstate().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(Estate.class);

        // then
        assertThat("Exists 2", result, is(notNullValue()));
        assertThat("Exists 2", result.size(), is(2));

        // for not existing
        expenses.clear();
        expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(Estate.class);

        // then
        assertThat("Not Exists", result, is(notNullValue()));
        assertThat("Not Exists", result.size(), is(0));
    }

    @Test
    public void testRetrieveExpensesByEstateRentType() throws Exception {
        // for empty
        Applicant applicant = new Applicant();

        // when
        List<Expense> result = applicant.retrieveExpenses(EstateRentType.FULLY_RENTED);

        // then
        assertThat("Empty", result, is(notNullValue()));
        assertThat("Empty", result.size(), is(0));


        // for existing
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withEstate().build();
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(EstateRentType.PARTIALLY_RENTED);

        // then
        assertThat("Exists 1", result, is(notNullValue()));
        assertThat("Exists 1", result.size(), is(1));


        // for duplicated
        Expense expense = ExpenseEntityBuilder.aBuilder().withEstate().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(EstateRentType.PARTIALLY_RENTED);

        // then
        assertThat("Exists 2", result, is(notNullValue()));
        assertThat("Exists 2", result.size(), is(2));

        // for not existing
        expenses.clear();
        expense = ExpenseEntityBuilder.aBuilder().build();
        expenses.add(expense);
        applicant.setExpenses(expenses);

        // when
        result = applicant.retrieveExpenses(EstateRentType.FULLY_RENTED);

        // then
        assertThat("Not Exists", result, is(notNullValue()));
        assertThat("Not Exists", result.size(), is(0));
    }

    @Test
    public void testRetrieveIsLivingOwnEstate() throws Exception {
        // for empty
        Applicant applicant = new Applicant();

        // when
        boolean isLivingInOwnEstate = applicant.retrieveIsLivingInOwnEstate();

        // then
        assertThat("empty", isLivingInOwnEstate, is(false));


        // for correct
        Set<Expense> expenses = ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build();
        applicant.setExpenses(expenses);

        // when
        isLivingInOwnEstate = applicant.retrieveIsLivingInOwnEstate();

        // then
        assertThat("correct", isLivingInOwnEstate, is(true));
    }

}