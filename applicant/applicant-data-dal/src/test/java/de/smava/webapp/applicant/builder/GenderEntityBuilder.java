package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.type.*;

import java.util.Random;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class GenderEntityBuilder extends EntityBuilder<Gender> {

    protected Gender createEntity() {
        return Gender.FEMALE;
    }

    public static GenderEntityBuilder aBuilder() {
        return new GenderEntityBuilder();
    }

    public GenderEntityBuilder withRandom() {
        int pick = new Random().nextInt(Gender.values().length);
        Gender answer = Gender.values()[pick];

        this.setEntity(answer);

        return this;
    }
}
