package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Employee;
import de.smava.webapp.applicant.type.EmployeeType;
import de.smava.webapp.applicant.type.EmploymentType;
import de.smava.webapp.applicant.type.SalariedStaffType;

/**
 * Created by Sebastian Hanulok on 2015-07-16.
 */
public class EmployeeEntityBuilder extends EntityBuilder<Employee> {

    protected Employee createEntity() {
        return new Employee();
    }

    public static EmployeeEntityBuilder aBuilder() {
        return new EmployeeEntityBuilder();
    }

    public EmployeeEntityBuilder withEmployee() {
        entity.setId(getNextId());
        entity.setEmployeeType(EmployeeType.EMPLOYEE);
        entity.setEmploymentType(EmploymentType.SALARIED_STAFF);
        entity.setEmployedSince(EntityBuilder.getDate("2013-10-09 12:34:56"));
        entity.setEmployer(EmployerEntityBuilder.aBuilder().withData().build());
        entity.setTemporarilyEmployed(true);
        entity.setEndOfTemporaryEmployment(EntityBuilder.getDate("2013-10-10 11:24:56"));
        entity.setStaffType(SalariedStaffType.APPRENTICE);

        return this;
    }

    public EmployeeEntityBuilder withEmployeeSelfEmployed() {
        entity.setId(getNextId());
        entity.setEmployeeType(EmployeeType.OTHER);
        entity.setEmploymentType(EmploymentType.SELF_EMPLOYED);
        entity.setEmployedSince(EntityBuilder.getDate("2013-10-09 12:34:56"));
        entity.setEmployer(EmployerEntityBuilder.aBuilder().withData().build());
        entity.setTemporarilyEmployed(true);
        entity.setEndOfTemporaryEmployment(EntityBuilder.getDate("2013-10-10 11:24:56"));
        entity.setStaffType(SalariedStaffType.OTHER);

        return this;
    }

    public EmployeeEntityBuilder withEmployeeCeo() {

        entity.setId(getNextId());
        entity.setEmployeeType(EmployeeType.CEO);
        entity.setEmploymentType(EmploymentType.SALARIED_STAFF);
        entity.setEmployedSince(EntityBuilder.getDate("2013-10-09 12:34:56"));
        entity.setEmployer(EmployerEntityBuilder.aBuilder().withData().build());
        entity.setTemporarilyEmployed(true);
        entity.setEndOfTemporaryEmployment(EntityBuilder.getDate("2013-10-10 11:24:56"));
        entity.setStaffType(SalariedStaffType.FULL_TIME);

        return this;
    }
}
