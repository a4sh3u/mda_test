package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Address;
import de.smava.webapp.applicant.type.Country;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class AddressEntityBuilder extends EntityBuilder<Address> {

    protected Address createEntity() {
        return new Address();
    }

    public static AddressEntityBuilder aBuilder() {
        return new AddressEntityBuilder();
    }

    public AddressEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCountry(Country.AF);
        entity.setZipCode("12345");
        entity.setCity("City");
        entity.setStreet("Street Name");
        entity.setHouseNumber("1234");
        entity.setCreationDate(EntityBuilder.getDate("2014-02-23 02:23:12"));

        return this;
    }

    public AddressEntityBuilder withRandomData(int length) {
        entity.setId(Long.valueOf(getNextId()));
        entity.setCountry(CountryEntityBuilder.aBuilder().withRandom().build());
        entity.setCreationDate(EntityBuilder.getDate("2014-02-23 02:23:12"));
        if (length > 0) {
            entity.setStreet("Street of " + generateRandomAlphabetic(length));
            entity.setHouseNumber(generateRandomNumeric(length));
            entity.setZipCode(generateRandomNumeric(length));
            entity.setCity("City of " + generateRandomAlphabetic(length));
        } else {
            entity.setStreet("Street of " + generateRandomAlphabetic(20));
            entity.setHouseNumber(generateRandomNumeric(3));
            entity.setZipCode(generateRandomNumeric(5));
            entity.setCity("City of " + generateRandomAlphabetic(20));
        }

        return this;
    }
}
