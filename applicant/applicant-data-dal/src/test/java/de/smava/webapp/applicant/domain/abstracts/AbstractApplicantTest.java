package de.smava.webapp.applicant.domain.abstracts;

import de.smava.webapp.applicant.builder.*;
import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.type.EstateRentType;
import de.smava.webapp.applicant.type.IncomeType;
import de.smava.webapp.applicant.type.OtherHousingCostType;
import de.smava.webapp.applicant.type.ThirdPartyLoanSource;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

/**
 * Created by PAWEL on 2016-02-10.
 */
public class AbstractApplicantTest {

    @Test
    public void testCopyFromOldEntity() {

        Applicant applicant = new Applicant();

        // for incorrect param
        Address address = new Address();
        applicant.setCreationDate(null);

        // when
        applicant.copyFromOldEntity(address);

        // than
        assertThat("Incorrect param", applicant.getCreationDate(), is(nullValue()));


        // for
        Applicant oldApplicant = generateApplicant();

        // when
        applicant.copyFromOldEntity(oldApplicant);

        // than
        assertThat("NationalityInfo",applicant.getNationalityInfo(), is(oldApplicant.getNationalityInfo()));
        assertThat("HomeAddress",applicant.getHomeAddress(), is(oldApplicant.getHomeAddress()));
        assertThat("PreviousHomeAddress",applicant.getPreviousHomeAddress(), is(oldApplicant.getPreviousHomeAddress()));
        assertThat("PreviousEmployment",applicant.getPreviousEmployment(), is(oldApplicant.getPreviousEmployment()));
        assertThat("Household",applicant.getHousehold(), is(oldApplicant.getHousehold()));
        assertThat("MainIncome",applicant.getMainIncome(), is(oldApplicant.getMainIncome()));
        assertThat("PersonalData",applicant.getPersonalData(), is(oldApplicant.getPersonalData()));
        assertThat("Email",applicant.getEmail(), is(oldApplicant.getEmail()));
        assertThat("CreditScore", applicant.getCreditScores(), is(oldApplicant.getCreditScores()));
        assertThat("BankAccount",applicant.getBankAccount(), is(oldApplicant.getBankAccount()));
    }

    @Test
    public void testFunctionallyEquals() {
        Applicant oldApplicant = generateApplicant();
        Applicant applicant = new Applicant();

        // with empty param
        // when
        Boolean result = applicant.functionallyEquals(null);

        // then
        assertThat("empty param", result, is(false));


        // with incorrect parameter
        Address address = new Address();
        applicant.setCreationDate(null);

        // when
        result = applicant.functionallyEquals(address);

        // then
        assertThat("incorrect param", result, is(false));


        // with
        applicant.copyFromOldEntity(oldApplicant);

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("are the same", result, is(true));


        // with NationalityInfo incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setNationalityInfo(NationalityInfoEntityBuilder.aBuilder().withRandomData(22).build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other NationalityInfo", result, is(false));


        // with HomeAddress incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setHomeAddress(LivingAddressEntityBuilder.aBuilder().withData().build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other HomeAddress", result, is(false));


        // with PreviousHomeAddress incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setPreviousHomeAddress(AddressEntityBuilder.aBuilder().withRandomData(23).build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other PreviousHomeAddress", result, is(false));


        // with PreviousEmployment incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setPreviousEmployment(EmploymentEntityBuilder.aBuilder().withEmployee().build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other PreviousEmployment", result, is(false));


        // with Household incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setHousehold(HouseholdEntityBuilder.aBuilder().withData().build());
        applicant.getHousehold().setNumberOfChildren(31);

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other Household", result, is(false));


        // with MainIncome incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setMainIncome(IncomeEntityBuilder.aBuilder().withCapitalIncome().build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other MainIncome", result, is(false));


        // with PersonalData incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setPersonalData(PersonalDataEntityBuilder.aBuilder().withData().build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other PersonalData", result, is(false));


        // with Email incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setEmail("xxx@yyy.pl");

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other Email", result, is(false));


        // with CreditScore incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setCreditScores(CreditScoresEntityBuilder.aBuilder().withData().build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other CreditScore", result, is(false));


        // with BankAccount incorrect
        applicant.copyFromOldEntity(oldApplicant);
        applicant.setBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());

        // when
        result = applicant.functionallyEquals(oldApplicant);

        // then
        assertThat("other BankAccount", result, is(false));

    }

    private Applicant generateApplicant() {
        Applicant applicant = ApplicantEntityBuilder.aBuilder()
                .withData()
                .withNationalityInfo()
                .withHousehold()
                .withMainIncomeAsEmployee()
                .withBankAccount()
                .withEmail()
                .withPersonalData()
                .withCreditScoreData()
                .withHomeAddress()
                .withPreviousHomeAddress()
                .withPreviousEmploymentAsEmployee()
                .build();

        return applicant;
    }

    @Test
    public void testRetrieveMainAdditionalIncome() throws Exception {
        // for empty MainIncome and empty AdditionalIncome
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        Boolean main = applicant.retrieveIsMainIncome(IncomeType.CHILD_ALIMONY);
        List<Income> additional = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("Main/Additional income empty - main", main, is(false));
        assertThat("Main/Additional income empty - additional", additional, is(notNullValue()));
        assertThat("Main/Additional income empty - additional", additional.size(), is(0));


        //for correct MainIncome only
        applicant.setMainIncome(getIncomeChildAlimony(0d));

        //when
        main = applicant.retrieveIsMainIncome(IncomeType.CHILD_ALIMONY);
        additional = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("correct MainIncome - main", main, is(true));
        assertThat("correct MainIncome - additional", additional, is(notNullValue()));
        assertThat("correct MainIncome - additional", additional.size(), is(0));


        //for incorrect MainIncome only
        applicant.setMainIncome(getIncomeOther(10d));

        //when
        main = applicant.retrieveIsMainIncome(IncomeType.CHILD_ALIMONY);
        additional = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("incorrect MainIncome - mian", main, is(false));
        assertThat("incorrect MainIncome - additional", additional, is(notNullValue()));
        assertThat("incorrect MainIncome - additional", additional.size(), is(0));


        //for incorrect MainIncome and correct AdditionalIncome
        Income firstAdditionalIncome = getIncomeChildAlimony(20d);
        Income secondAdditionalIncome = getIncomeOther(30d);
        Income thirdAdditionalIncome = getIncomeChildAlimony(40d);
        Set<Income> additionalIncomes = new HashSet<Income>();
        additionalIncomes.addAll(Arrays.asList(
                firstAdditionalIncome,
                secondAdditionalIncome,
                thirdAdditionalIncome
        ));
        applicant.setAdditionalIncomes(additionalIncomes);

        //when
        main = applicant.retrieveIsMainIncome(IncomeType.CHILD_ALIMONY);
        additional = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("correct AdditionalIncome - main", main, is(false));
        assertThat("correct AdditionalIncome - additional", additional, is(notNullValue()));
        assertThat("correct AdditionalIncome - additional", additional.size(), is(2));


        //for correct MainIncome and correct AdditionalIncome
        applicant.setMainIncome(getIncomeChildAlimony(30d));

        //when
        main = applicant.retrieveIsMainIncome(IncomeType.CHILD_ALIMONY);
        additional = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("correct Main and Additional Incomes - main", main, is(true));
        assertThat("correct Main and Additional Incomes - additional", additional, is(notNullValue()));
        assertThat("correct Main and Additional Incomes - additional", additional.size(), is(2));
    }

    @Test
    public void testRetrieveMainAdditionalIncomeListForSingle() throws Exception {
        // for empty MainIncome and empty AdditionalIncome
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //when
        List<Income> additional = applicant.retrieveAdditionalIncome(Arrays.asList(IncomeType.CHILD_ALIMONY));

        //then
        assertThat("Main/Additional income empty - additional", additional, is(notNullValue()));
        assertThat("Main/Additional income empty - additional", additional.size(), is(0));
    }

    @Test
    public void testRetrieveMainAdditionalIncomeListForMany() throws Exception {

        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();

        //for incorrect MainIncome and correct AdditionalIncome
        Income firstAdditionalIncome = getIncomeChildAlimony(20d);
        Income secondAdditionalIncome = getIncomeOther(30d);
        Income thirdAdditionalIncome = getIncomeChildAlimony(40d);
        Set<Income> additionalIncomes = new HashSet<Income>();
        additionalIncomes.addAll(Arrays.asList(
                firstAdditionalIncome,
                secondAdditionalIncome,
                thirdAdditionalIncome
        ));
        applicant.setAdditionalIncomes(additionalIncomes);

        //when
        List<Income> additional = applicant.retrieveAdditionalIncome(Arrays.asList(IncomeType.CHILD_ALIMONY));

        //then
        assertThat("correct AdditionalIncome - additional", additional, is(notNullValue()));
        assertThat("correct AdditionalIncome - additional", additional.size(), is(2));


        //when
        additional = applicant.retrieveAdditionalIncome(Arrays.asList(IncomeType.CHILD_ALIMONY, IncomeType.RENTAL_INCOME));

        //then
        assertThat("correct AdditionalIncome - additional", additional, is(notNullValue()));
        assertThat("correct AdditionalIncome - additional", additional.size(), is(3));
    }

    @Test
    public void testRetrieveAdditionalIncome() throws Exception {
        // for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income firstAdditionalIncome = getIncomeChildAlimony(20d);
        Income secondAdditionalIncome = getIncomeOther(30d);
        Income soldier = IncomeEntityBuilder.aBuilder().withSoldier().build();
        Income employee = IncomeEntityBuilder.aBuilder().withEmployee().build();

        Set<Income> additionalIncomes = new HashSet<Income>();
        additionalIncomes.addAll(Arrays.asList(
                firstAdditionalIncome,
                secondAdditionalIncome,
                soldier,
                employee
        ));
        applicant.setAdditionalIncomes(additionalIncomes);

        //when
        List<Income> incomesByType = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("correct Additional Incomes found - 1 for CHILD_ALIMONY", incomesByType.size(), is(1));
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByType.get(0), is(firstAdditionalIncome));

        //for
        Income fourthAdditionalIncome = getIncomeChildAlimony(40d);
        Income fifthAdditionalIncome = getIncomeOther(30d);
        additionalIncomes.addAll(Arrays.asList(
                fourthAdditionalIncome,
                fifthAdditionalIncome
        ));

        applicant.setAdditionalIncomes(additionalIncomes);

        //when
        incomesByType = applicant.retrieveAdditionalIncome(IncomeType.CHILD_ALIMONY);

        //then
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByType.size(), is(2));
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByType.contains(firstAdditionalIncome), is(true));
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByType.contains(fourthAdditionalIncome), is(true));
    }

    @Test
    public void testRetrieveAdditionalIncomeByInstance() throws Exception {
        // for
        Applicant applicant = ApplicantEntityBuilder.aBuilder().build();
        Income firstAdditionalIncome = getIncomeChildAlimony(20d);
        Income secondAdditionalIncome = getIncomeOther(30d);
        Income thirdAdditionalIncome = getIncomeChildAlimony(40d);
        Income soldier = IncomeEntityBuilder.aBuilder().withSoldier().build();
        Income employee = IncomeEntityBuilder.aBuilder().withEmployee().build();

        Set<Income> additionalIncomes = new HashSet<Income>();
        additionalIncomes.addAll(Arrays.asList(
                firstAdditionalIncome,
                secondAdditionalIncome,
                thirdAdditionalIncome,
                soldier,
                employee
        ));
        applicant.setAdditionalIncomes(additionalIncomes);

        //when
        List<Income> incomesByClass = applicant.retrieveAdditionalIncome(Soldier.class);

        //then
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByClass.size(), is(1));
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByClass.get(0), is(soldier));

        //for
        Income soldier2 = IncomeEntityBuilder.aBuilder().withSoldier().build();
        Income employee2 = IncomeEntityBuilder.aBuilder().withEmployee().build();
        additionalIncomes.addAll(Arrays.asList(
                soldier2,
                employee2
        ));

        applicant.setAdditionalIncomes(additionalIncomes);

        //when
        incomesByClass = applicant.retrieveAdditionalIncome(Soldier.class);

        //then
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByClass.size(), is(2));
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByClass.contains(soldier), is(true));
        assertThat("correct Additional Incomes found - 1 for Soldier", incomesByClass.contains(soldier2), is(true));
    }

    @Test
    public void testRetrieveIsExpenseMatchingPartiallyRentedEstateRentType() {
        // for empty
        Applicant applicant = new Applicant();

        // when
        boolean result = applicant.retrieveIsExpenseMatchingPartiallyRentedEstateRentType(new ArrayList<EstateRentType>());

        // than
        assertThat("empty", result, is(false));


        // for empty filter
        applicant.setExpenses(ExpensesEntityBuilder.aBuilder().withEstate().build());

        // when
        result = applicant.retrieveIsExpenseMatchingPartiallyRentedEstateRentType(new ArrayList<EstateRentType>());

        // than
        assertThat("empty filter", result, is(false));


        // for correct data
        List<EstateRentType> filter = new ArrayList<EstateRentType>();
        filter.add(EstateRentType.PARTIALLY_RENTED);

        // when
        result = applicant.retrieveIsExpenseMatchingPartiallyRentedEstateRentType(filter);

        // than
        assertThat("correct data", result, is(true));


        // for incorrect filter
        filter.clear();
        filter.add(EstateRentType.FULLY_RENTED);

        // when
        result = applicant.retrieveIsExpenseMatchingPartiallyRentedEstateRentType(filter);

        // than
        assertThat("incorrect filter", result, is(false));
    }

    @Test
    public void testRetrieveIsExpenseMatchingOtherHousingCostType() {
        // for empty
        Applicant applicant = new Applicant();

        // when
        boolean result = applicant.retrieveIsExpenseMatchingOtherHousingCostType(new ArrayList<OtherHousingCostType>());

        // than
        assertThat("empty", result, is(false));


        // for empty filter
        applicant.setExpenses(ExpensesEntityBuilder.aBuilder().withOtherHousingCosts().build());

        // when
        result = applicant.retrieveIsExpenseMatchingOtherHousingCostType(new ArrayList<OtherHousingCostType>());

        // than
        assertThat("empty filter", result, is(false));


        // for correct data
        List<OtherHousingCostType> filter = new ArrayList<OtherHousingCostType>();
        filter.add(OtherHousingCostType.RENT_CONTRIBUTION);

        // when
        result = applicant.retrieveIsExpenseMatchingOtherHousingCostType(filter);

        // than
        assertThat("correct data", result, is(true));


        // for incorrect filter
        filter.clear();
        filter.add(OtherHousingCostType.MILITARY);

        // when
        result = applicant.retrieveIsExpenseMatchingOtherHousingCostType(filter);

        // than
        assertThat("incorrect filter", result, is(false));
    }

    @Test
    public void testRetrieveThirdPartyLoans() {
        // for
        Applicant applicant = new Applicant();
        applicant.setThirdPartyLoans(ThirdPartyLoansEntityBuilder.aBuilder().withAllSourceTypes().build());

        // when
        Set<ThirdPartyLoan> result1 = applicant.retrieveThirdPartyLoans(null);

        // than
        assertThat(result1.size(), is(5));

        // when
        Set<ThirdPartyLoan> result2 = applicant.retrieveThirdPartyLoans(Collections.<ThirdPartyLoanSource>emptyList());

        // than
        assertThat(result2.size(), is(0));

        // when
        Set<ThirdPartyLoan> result3 = applicant.retrieveThirdPartyLoans(Arrays.asList(ThirdPartyLoanSource.ADVISOR, ThirdPartyLoanSource.CUSTOMER));

        // than
        assertThat(result3.size(), is(2));

        // when
        Set<ThirdPartyLoan> result4 = applicant.retrieveThirdPartyLoans(Arrays.asList(ThirdPartyLoanSource.DELETED, ThirdPartyLoanSource.SCHUFA, ThirdPartyLoanSource.MERGED));

        // than
        assertThat(result4.size(), is(3));
    }

    private Income getIncomeChildBenefit(Double amount) {
        return IncomeEntityBuilder.aBuilder().withChildBenefit(amount).build();
    }

    private Income getIncomeChildAlimony(Double amount) {
        return IncomeEntityBuilder.aBuilder().withChildAlimony(amount).build();
    }

    private Income getIncomeSpouseAlimony(Double amount) {
        return IncomeEntityBuilder.aBuilder().withSpouseAlimony(amount).build();
    }

    private Income getIncomeOther(Double amount) {
        return IncomeEntityBuilder.aBuilder().withRentalIncome(amount).build();
    }

}