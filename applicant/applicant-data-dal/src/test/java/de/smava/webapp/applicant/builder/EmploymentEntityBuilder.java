package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Employment;

/**
 * Created by Sebastian Hanulok on 2015-07-16.
 */
public class EmploymentEntityBuilder extends EntityBuilder<Employment> {

    protected Employment createEntity() {
        return new Employment();
    }

    public static EmploymentEntityBuilder aBuilder() {
        return new EmploymentEntityBuilder();
    }

    public EmploymentEntityBuilder withEmployee() {
        entity = (Employment)IncomeEntityBuilder.aBuilder().withEmployee().build();

        return this;
    }

    public EmploymentEntityBuilder withRetiree() {
        entity = (Employment)IncomeEntityBuilder.aBuilder().withRetiree().build();

        return this;
    }

    public EmploymentEntityBuilder withOfficial() {
        entity = (Employment)IncomeEntityBuilder.aBuilder().withOfficial().build();

        return this;
    }

    public EmploymentEntityBuilder withSoldier() {
        entity = (Employment)IncomeEntityBuilder.aBuilder().withSoldier().build();

        return this;
    }

}
