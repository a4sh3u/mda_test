package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.CreditScore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class CreditScoresEntityBuilder extends EntityBuilder<List<CreditScore>> {

    protected List<CreditScore> createEntity() {
        return new ArrayList<CreditScore>();
    }

    public static CreditScoresEntityBuilder aBuilder() {
        return new CreditScoresEntityBuilder();
    }

    public CreditScoresEntityBuilder withData() {

        entity.add(CreditScoreEntityBuilder.aBuilder().withData().build());

        return this;
    }
}
