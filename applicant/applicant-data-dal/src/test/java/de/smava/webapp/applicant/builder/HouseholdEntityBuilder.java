package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.Household;
import de.smava.webapp.applicant.type.DebtCreditCardsType;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class HouseholdEntityBuilder extends EntityBuilder<Household> {

    protected Household createEntity() {
        return new Household();
    }

    public static HouseholdEntityBuilder aBuilder() {
        return new HouseholdEntityBuilder();
    }

    public HouseholdEntityBuilder withData() {
        entity.setId(getNextId());
        entity.setNumberOfChildren(2);
        entity.setNumberOfPersonsInHousehold(3);
        entity.setNumberOfCars(1);
        entity.setNumberOfMotorbikes(5);

        return this;
    }

    public HouseholdEntityBuilder withCreditCard() {
        entity.setDeptCreditCards(DebtCreditCardsType.CREDIT_CARD_ONLY);

        return this;
    }
}
