package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.type.*;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author Sebastian Hanulok
 * @since 2015-07-15.
 */
public class IncomeEntityBuilder extends EntityBuilder<Income> {

    private static final Logger LOGGER = Logger.getLogger(IncomeEntityBuilder.class);

    protected Income createEntity() {
        return new Income();
    }

    public static IncomeEntityBuilder aBuilder() {
        return new IncomeEntityBuilder();
    }

    public IncomeEntityBuilder withRetiree() {
        return withRetiree(
                IncomeType.RENTAL_INCOME,
                EmploymentType.RETIREE,
                RetireeType.RETIREE,
                PensionType.COMPANY_PENSION);
    }

    public IncomeEntityBuilder withRetiree(IncomeType incomeType, EmploymentType employmentType, RetireeType retireeType, PensionType pensionType) {
        Retiree retiree = new Retiree();

        retiree.setRetirementStart(getRandomDate());
        retiree.setRetireeType(retireeType);
        retiree.setPensionType(pensionType);
        retiree.setEmploymentType(employmentType);
        retiree.setRetirementStart(getDate("2012-06-01 01:00:00"));

        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = dateFormatter.parse("2012-06-01 01:00:00");
            retiree.setRetirementStart(date);
        } catch (Exception e) {
        }

        entity = setIncomeData(retiree, incomeType, null);

        return this;
    }

    public IncomeEntityBuilder withPensionSupplement() {
        return withPensionSupplement(
                PensionType.SUPPLEMENTARY_PENSION,
                IncomeType.PENSION_SUPPLEMENT,
                null);
    }

    public IncomeEntityBuilder withPensionSupplement(PensionType pensionType, IncomeType incomeType) {
        return withPensionSupplement(
                pensionType,
                incomeType,
                null);
    }

    public IncomeEntityBuilder withPensionSupplement(PensionType pensionType, IncomeType incomeType, Double monthlyNetIncome) {
        PensionSupplement pensionSupplement = new PensionSupplement();

        pensionSupplement.setPensionSupplementType(pensionType);

        entity = setIncomeData(pensionSupplement, incomeType, monthlyNetIncome);

        return this;
    }

    public IncomeEntityBuilder withCeo() {
        return withCeo(IncomeType.EMPLOYMENT, SalariedStaffType.FULL_TIME);
    }

    public IncomeEntityBuilder withCeo(IncomeType incomeType, SalariedStaffType salariedStaffType) {
        return withEmployee(
                incomeType,
                EmploymentType.SALARIED_STAFF,
                salariedStaffType,
                EmployeeType.CEO,
                false);
    }

    public IncomeEntityBuilder withOfficial() {
        return withOfficial(
                IncomeType.EMPLOYMENT,
                EmploymentType.SALARIED_STAFF,
                SalariedStaffType.MINOR_EMPLOYMENT,
                OfficialServiceGradeType.PRIVATE);
    }

    public IncomeEntityBuilder withOfficial(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, OfficialServiceGradeType officialServiceGradeType) {
        Official official = new Official();

        setIncomeData(official, incomeType, null);
        official.setEmploymentType(employmentType);
        setSalariedStaff(official, salariedStaffType);
        official.setOfficialServiceGradeType(officialServiceGradeType);

        entity = official;

        return this;
    }

    public IncomeEntityBuilder withEmployee() {
        return withEmployee(
                IncomeType.EMPLOYMENT,
                EmploymentType.SALARIED_STAFF,
                SalariedStaffType.FULL_TIME,
                EmployeeType.EMPLOYEE,
                null,
                false);
    }

    public IncomeEntityBuilder withEmployee(boolean isTempEmployed) {
        return withEmployee(
                IncomeType.EMPLOYMENT,
                EmploymentType.SALARIED_STAFF,
                SalariedStaffType.FULL_TIME,
                EmployeeType.EMPLOYEE,
                null,
                isTempEmployed);
    }

    public IncomeEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, EmployeeType employeeType) {
        return withEmployee(
                incomeType,
                employmentType,
                salariedStaffType,
                employeeType,
                null,
                false);
    }


    public IncomeEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, EmployeeType employeeType, boolean isTempEmployed) {
        return withEmployee(
                incomeType,
                employmentType,
                salariedStaffType,
                employeeType,
                null,
                false);
    }


    public IncomeEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, EmployeeType employeeType, Double monthlyNetIncome) {
        return withEmployee(
                incomeType,
                employmentType,
                salariedStaffType,
                employeeType,
                monthlyNetIncome,
                false);
    }

    public IncomeEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, EmployeeType employeeType, Double monthlyNetIncome, boolean isTempEmployed) {
        Employee employee = new Employee();

        setIncomeData(employee, incomeType, null);
        employee.setEmploymentType(employmentType);
        setSalariedStaff(employee, salariedStaffType);
        employee.setEmployeeType(employeeType);
        employee.setTemporarilyEmployed(isTempEmployed);

        if (isTempEmployed) {
            employee.setEndOfTemporaryEmployment(new Date());
        }

        if (monthlyNetIncome != null) {
            employee.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = employee;

        return this;
    }

    public IncomeEntityBuilder withEmployee(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, IndustrySectorType industrySectorType, EmployeeType employeeType) {
        Employee employee = new Employee();

        setIncomeData(employee, incomeType, null);
        employee.setEmploymentType(employmentType);
        setSalariedStaff(employee, salariedStaffType);
        employee.setIndustrySector(industrySectorType);
        employee.setEmployeeType(employeeType);
        employee.setTemporarilyEmployed(false);

        entity = employee;

        return this;
    }

    public IncomeEntityBuilder withSoldier() {
        return withSoldier(
                IncomeType.EMPLOYMENT,
                EmploymentType.SALARIED_STAFF,
                SalariedStaffType.FULL_TIME,
                SoldierType.MILITARY_SERVICE);
    }

    public IncomeEntityBuilder withOtherEmployment() {
        return withEmployee(
                IncomeType.OTHER,
                EmploymentType.SALARIED_STAFF,
                SalariedStaffType.OTHER,
                EmployeeType.OTHER,
                false);
    }

    public IncomeEntityBuilder withSoldier(IncomeType incomeType, EmploymentType employmentType, SalariedStaffType salariedStaffType, SoldierType soldierType) {
        Soldier soldier = new Soldier();

        setIncomeData(soldier, incomeType, null);
        soldier.setEmploymentType(employmentType);
        setSalariedStaff(soldier, salariedStaffType);
        soldier.setSoldierType(soldierType);

        entity = soldier;

        return this;
    }

    public IncomeEntityBuilder withEmployment() {
        return withEmployment(IncomeType.EMPLOYMENT, EmploymentType.SELF_EMPLOYED);
    }

    public IncomeEntityBuilder withEmployment(IncomeType incomeType, EmploymentType employmentType) {
        Employment employment = new Employment();

        setIncomeData(employment, incomeType, null);
        employment.setEmploymentType(employmentType);

        entity = employment;

        return this;
    }

    public IncomeEntityBuilder withSelfEmployed() {
        return withSelfEmployed(IncomeType.EMPLOYMENT, IndustrySectorType.CAR_INDUSTRY);
    }

    public IncomeEntityBuilder withSelfEmployed(IncomeType incomeType, IndustrySectorType industrySectorType) {
        return withSelfEmployed(incomeType, industrySectorType, EmploymentType.SELF_EMPLOYED);
    }

    public IncomeEntityBuilder withSelfEmployed(IncomeType incomeType, IndustrySectorType industrySectorType, EmploymentType employedType) {
        SelfEmployed selfEmployed = new SelfEmployed();

        setIncomeData(selfEmployed, incomeType, null);
        selfEmployed.setEmploymentType(employedType);
        selfEmployed.setIndustrySector(industrySectorType);

        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = dateFormatter.parse("2014-02-01 01:00:00");
            selfEmployed.setStartOfBusiness(date);
        } catch (Exception e) {
        }

        entity = selfEmployed;

        return this;
    }

    public IncomeEntityBuilder withSpouseAllimony() {
        return withSpouseAllimony(null);
    }

    public IncomeEntityBuilder withSpouseAllimony(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.SPOUSE_ALIMONY, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withChildAlimony() {
        return withChildAlimony(500d);
    }

    public IncomeEntityBuilder withChildAlimony(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.CHILD_ALIMONY, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withFreelancer() {
        return withFreelancer(IncomeType.OTHER, EmploymentType.SELF_EMPLOYED);
    }

    public IncomeEntityBuilder withFreelancer(IncomeType incomeType, EmploymentType employmentType) {
        Freelancer freelancer = new Freelancer();
        freelancer.setEmploymentType(employmentType);
        freelancer.setIncomeType(incomeType);
        freelancer.setOccupationType(FreelancerOccupationType.COMPUTER_SCIENTIST);
        entity = freelancer;
        return this;
    }

    public IncomeEntityBuilder withUneployed() {
        return withEmployment(null, EmploymentType.UNEMPLOYED);
    }

    public IncomeEntityBuilder withHomekeeper() {
        return withEmployment(IncomeType.CHILD_BENEFIT_GRANT, EmploymentType.HOMEKEEPER);
    }

    public IncomeEntityBuilder withStudent() {
        return withEmployment(IncomeType.EDUCATION_SUPPORT, EmploymentType.STUDENT);
    }

    public IncomeEntityBuilder withScholar() {
        return withEmployment(IncomeType.CHILD_ALIMONY, EmploymentType.SCHOLAR);
    }

    public IncomeEntityBuilder withSpouseAlimony() {
        return withSpouseAlimony(null);
    }

    public IncomeEntityBuilder withSpouseAlimony(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.SPOUSE_ALIMONY, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withProvisionIncome() {
        return withProvisionIncome(null);
    }

    public IncomeEntityBuilder withProvisionIncome(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.PROVISION_INCOME, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withProvisionAuxiliary() {
        return withProvisionAuxiliary(null);
    }

    public IncomeEntityBuilder withProvisionAuxiliary(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.PROVISION_AUXILIARY, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withProfitShare() {
        return withProfitShare(null);
    }

    public IncomeEntityBuilder withProfitShare(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.PROFIT_SHARE, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withCapitalIncome() {
        return withCapitalIncome(null);
    }

    public IncomeEntityBuilder withCapitalIncome(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.CAPITAL_INCOME, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withRentalIncome() {
        return withRentalIncome(null);
    }

    public IncomeEntityBuilder withRentalIncome(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.RENTAL_INCOME, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withChildBenefit() {
        return withChildBenefit(500d);
    }

    public IncomeEntityBuilder withChildBenefit(Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, IncomeType.CHILD_BENEFIT, null);

        if (monthlyNetIncome != null) {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withIncomeType(IncomeType incomeType) {
        Income income = new Income();

        setIncomeData(income, incomeType, null);

        entity = income;

        return this;
    }

    public IncomeEntityBuilder withIncomeType(IncomeType incomeType, Double monthlyNetIncome) {
        Income income = new Income();

        setIncomeData(income, incomeType, null);
        income.setMonthlyNetIncome(monthlyNetIncome);

        entity = income;

        return this;
    }

    protected Income setIncomeData(Income income, IncomeType incomeType, Double monthlyNetIncome) {
        income.setId(getNextId());
        income.setIncomeType(incomeType);
        income.setDescription("Description for Income");
        income.setCreationDate(getDate("2001-09-08 23:11:00"));
        income.setExpirationDate(getDate("2011-09-08 23:11:00"));

        if (monthlyNetIncome == null) {
            income.setMonthlyNetIncome(new Random().nextDouble());
        } else {
            income.setMonthlyNetIncome(monthlyNetIncome);
        }

        return income;
    }

    protected SalariedStaff setSalariedStaff(SalariedStaff salariedStaff, SalariedStaffType salariedStaffType) {
        salariedStaff.setStaffType(salariedStaffType);
        salariedStaff.setEmployedSince(getRandomDate());
        salariedStaff.setEmployer(EmployerEntityBuilder.aBuilder().withData().build());
        salariedStaff.setIndustrySector(IndustrySectorType.BUILDING_INDUSTRY);
        salariedStaff.setStaffType(salariedStaffType);
        salariedStaff.setProbationPeriod(false);
        salariedStaff.setEndOfProbationPeriod(getRandomDate());

        return salariedStaff;
    }
}
