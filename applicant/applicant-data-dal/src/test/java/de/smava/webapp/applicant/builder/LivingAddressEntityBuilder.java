package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.LivingAddress;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class LivingAddressEntityBuilder extends EntityBuilder<LivingAddress> {

    protected LivingAddress createEntity() {
        return new LivingAddress();
    }

    public static LivingAddressEntityBuilder aBuilder() {
        return new LivingAddressEntityBuilder();
    }

    public LivingAddressEntityBuilder withData() {
        entity.setId(Long.valueOf(getNextId()));
        entity.setMoveInDate(getRandomDate());
        entity.setAddress(AddressEntityBuilder.aBuilder().withData().build());

        return this;
    }
}
