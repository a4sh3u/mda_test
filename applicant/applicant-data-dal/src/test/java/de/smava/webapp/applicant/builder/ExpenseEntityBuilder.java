package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.type.EstateRentType;
import de.smava.webapp.applicant.type.EstateType;
import de.smava.webapp.applicant.type.ExpenseType;
import de.smava.webapp.applicant.type.OtherHousingCostType;

import java.util.Random;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class ExpenseEntityBuilder extends EntityBuilder<Expense> {

    protected Expense createEntity() {
        return new Expense();
    }

    public static ExpenseEntityBuilder aBuilder() {
        return new ExpenseEntityBuilder();
    }

    public ExpenseEntityBuilder withRent() {
        Rent rent = new Rent();

        setExpenseData(rent, ExpenseType.HOUSING_COST);

        entity = rent;

        return this;
    }

    public ExpenseEntityBuilder withOtherHousingCosts() {
        OtherHousingCost otherHousingCost = new OtherHousingCost();

        setExpenseData(otherHousingCost, ExpenseType.HOUSING_COST);
        otherHousingCost.setOtherHousingCostType(OtherHousingCostType.RENT_CONTRIBUTION);

        entity = otherHousingCost;

        return this;
    }

    public ExpenseEntityBuilder withEstate() {
        Estate estate = new Estate();

        setExpenseData(estate, ExpenseType.HOUSING_COST);
        estate.setSquaremetersUsed(10d);
        estate.setPartiallyRented(EstateRentType.PARTIALLY_RENTED);
        estate.setEstateType(EstateType.APARTMENT);
        estate.setEstateFinancing(EstateFinancingEntityBuilder.aBuilder().withData().build());

        entity = estate;

        return this;
    }


    public ExpenseEntityBuilder withEstateFulllyRented() {
        Estate estate = new Estate();

        setExpenseData(estate, ExpenseType.HOUSING_COST);
        estate.setSquaremetersUsed(10d);
        estate.setPartiallyRented(EstateRentType.FULLY_RENTED);
        estate.setEstateType(EstateType.APARTMENT);
        estate.setEstateFinancing(EstateFinancingEntityBuilder.aBuilder().withData().build());

        entity = estate;

        return this;
    }


    public ExpenseEntityBuilder withEstateNotRented() {
        Estate estate = new Estate();

        setExpenseData(estate, ExpenseType.HOUSING_COST);
        estate.setSquaremetersUsed(15d);
        estate.setPartiallyRented(EstateRentType.NOT_RENTED);
        estate.setEstateType(EstateType.APARTMENT);
        estate.setEstateFinancing(EstateFinancingEntityBuilder.aBuilder().withData().build());

        entity = estate;

        return this;
    }

    public ExpenseEntityBuilder withExpense(ExpenseType expenseType) {
        Expense expense = new Expense();

        setExpenseData(expense, expenseType);

        entity = expense;

        return this;
    }

    protected void setExpenseData(Expense expense, ExpenseType expenseType) {
        expense.setId(getNextId());
        expense.setDescription("Description");
        expense.setCreationDate(getDate("2011-07-23 06:23:00"));
        expense.setExpirationDate(getDate("2015-07-23 06:23:00"));
        expense.setMonthlyAmount(new Random().nextDouble());
        expense.setExpenseType(expenseType);
    }
}
