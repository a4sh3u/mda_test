package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.type.EmploymentType;
import de.smava.webapp.commons.currentdate.CurrentDate;

import java.util.*;

/**
 * Created by Sebastian Hanulok on 2015-07-16.
 */
public class ApplicantEntityBuilder extends EntityBuilder<Applicant> {

    protected Applicant createEntity() {
        return new Applicant();
    }

    public static ApplicantEntityBuilder aBuilder() {
        return new ApplicantEntityBuilder().withEmptyExpense();
    }

    public ApplicantEntityBuilder withData() {
        entity.setCreationDate(CurrentDate.getDate());

        return this;
    }

    public ApplicantEntityBuilder withFullData() {
        entity.setCreationDate(getRandomDate());
        entity.setNationalityInfo(NationalityInfoEntityBuilder.aBuilder().withFullData().build());
        entity.setHomeAddress(LivingAddressEntityBuilder.aBuilder().withData().build());
        entity.setPreviousHomeAddress(null);

        Employment employment = new Employment();
        employment.setEmploymentType(EmploymentType.SALARIED_STAFF);
        entity.setPreviousEmployment(employment);
        entity.setHousehold(HouseholdEntityBuilder.aBuilder().withData().withCreditCard().build());
        entity.setMainIncome(IncomeEntityBuilder.aBuilder().withEmployee().build());

        Set<Income> additionalIncomes = new HashSet<Income>();
        entity.setAdditionalIncomes(additionalIncomes);
        entity.setExpenses(ExpensesEntityBuilder.aBuilder().build());

        Set<Phone> phones = new HashSet<Phone>();
        phones.add(PhonesEntityBuilder.aBuilder().withMobile().build());
        entity.setPhones(phones);
        entity.setPersonalData(PersonalDataEntityBuilder.aBuilder().withData().build());
        entity.setEmail("email@email");
        entity.setCreditScores(CreditScoresEntityBuilder.aBuilder().withData().build());
        entity.setThirdPartyLoans(ThirdPartyLoansEntityBuilder.aBuilder().withData().build());
        entity.setExternalThirdPartyLoans(null);
        entity.setCustomerThirdPartyLoans(null);
        entity.setBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withCreditScoreData() {
        List<CreditScore> creditScores = new ArrayList<CreditScore>();

        creditScores.add(CreditScoreEntityBuilder.aBuilder().withData().build());

        entity.setCreditScores(creditScores);

        return this;
    }

    public ApplicantEntityBuilder withPersonalData() {
        entity.setPersonalData(PersonalDataEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withNationalityInfo() {
        entity.setNationalityInfo(NationalityInfoEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withRandomPersonalData(int length) {
        entity.setPersonalData(PersonalDataEntityBuilder.aBuilder().withRandomData(length).build());

        return this;
    }

    public ApplicantEntityBuilder withRandomNationalityInfo(int length) {
        entity.setNationalityInfo(NationalityInfoEntityBuilder.aBuilder().withRandomData(length).build());

        return this;
    }

    public ApplicantEntityBuilder withHousehold() {
        entity.setHousehold(HouseholdEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withPhone() {
        Set<Phone> phones = new HashSet<Phone>();
        phones.add(PhoneEntityBuilder.aBuilder().withLandLine().build());

        entity.setPhones(phones);

        return this;
    }

    public ApplicantEntityBuilder withHomeAddress() {
        entity.setHomeAddress(LivingAddressEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withEmail() {
        entity.setEmail("test@test.test");

        return this;
    }

    public ApplicantEntityBuilder withRandomEmail(int length) {
        entity.setEmail(
                generateRandomAlphanumeric(length)
                        + "@"
                        + generateRandomAlphanumeric(10).toLowerCase()
                        + '.'
                        + generateRandomAlphanumeric(3).toLowerCase()
                        + '.'
                        + CountryEntityBuilder.aBuilder().withRandom().build().toString().toLowerCase()
        );

        return this;
    }

    public ApplicantEntityBuilder withPreviousHomeAddress() {
        entity.setPreviousHomeAddress(AddressEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withBankAccount() {
        entity.setBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());

        return this;
    }

    public ApplicantEntityBuilder withPreviousEmploymentAsEmployee() {
        entity.setPreviousEmployment(EmploymentEntityBuilder.aBuilder().withEmployee().build());

        return this;
    }

    public ApplicantEntityBuilder withThirdPartyLoans() {
        entity.setThirdPartyLoans(ThirdPartyLoansEntityBuilder.aBuilder().withData(true).withData(false).build());

        return this;
    }

    public ApplicantEntityBuilder withAllThirdPartyLoans() {
        entity.setThirdPartyLoans(ThirdPartyLoansEntityBuilder.aBuilder().withAllTypes().build());

        return this;
    }

    public ApplicantEntityBuilder withEmptyExpense() {
        entity.setExpenses(ExpensesEntityBuilder.aBuilder().build());

        return this;
    }

    public ApplicantEntityBuilder withMainIncomeAsEmployee() {
        entity.setMainIncome(IncomeEntityBuilder.aBuilder().withEmployee().build());

        return this;
    }

    public ApplicantEntityBuilder withElderlyPersonalData() {
        entity.setPersonalData(PersonalDataEntityBuilder.aBuilder().withElderlyPersonalData().build());

        return this;
    }

    public ApplicantEntityBuilder withRentExpenses() {
        entity.setExpenses(ExpensesEntityBuilder.aBuilder().withRent().build());

        return this;
    }

    public ApplicantEntityBuilder withEstateExpenses() {
        entity.setExpenses(ExpensesEntityBuilder.aBuilder().withEstate().build());

        return this;
    }

    public ApplicantEntityBuilder withAllAdditionalIncomes() {
        entity.setAdditionalIncomes(AdditionalIncomesEntityBuilder.aBuilder().withAllIncomeTypes().build());

        return this;
    }

    public ApplicantEntityBuilder withAdditionalIncomePensionSupplement() {
        entity.setAdditionalIncomes(AdditionalIncomesEntityBuilder.aBuilder().withPensionSupplement().build());

        return this;
    }

    public ApplicantEntityBuilder withAllExpenses() {
        entity.setExpenses(ExpensesEntityBuilder.aBuilder().withAllDefinedExpenses().build());

        return this;
    }
}
