package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.type.Country;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class CountryEntityBuilder extends EntityBuilder<Country> {

    protected Country createEntity() {
        return  Country.DE;
    }

    public static CountryEntityBuilder aBuilder() {
        return new CountryEntityBuilder();
    }

    public CountryEntityBuilder withRandom() {
        int pick = new Random().nextInt(Country.values().length);
        Country answer = Country.values()[pick];

        this.setEntity(answer);

        if (Arrays.asList(Country.default_DE, Country.OTHER, Country.STATELESS).contains(answer)) {
            withRandom();
        }

        return this;
    }
}
