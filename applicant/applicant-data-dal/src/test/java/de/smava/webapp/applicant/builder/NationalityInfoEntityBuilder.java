package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.NationalityInfo;
import de.smava.webapp.applicant.type.Country;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class NationalityInfoEntityBuilder extends EntityBuilder<NationalityInfo> {

    protected NationalityInfo createEntity() {
        return new NationalityInfo();
    }

    public static NationalityInfoEntityBuilder aBuilder() {
        return new NationalityInfoEntityBuilder();
    }

    public NationalityInfoEntityBuilder withData() {

        entity.setId(getNextId());
        entity.setCountryOfBirth(Country.DE);
        entity.setNationality(Country.DE);
        entity.setPlaceOfBirth("Who knows");

        return this;
    }

    public NationalityInfoEntityBuilder withFullData() {
        entity.setNationality(Country.DE);
        entity.setPlaceOfBirth("place of birth");
        entity.setCountryOfBirth(Country.DE);
        entity.setWorkPermitUnlimited(true);
        //    entity.setEndOfWorkPermit(new Date(4102394400));
        entity.setResidencePermitUnlimited(true);
        //    entity.setEndOfResidencePermit(new Date(4102394400));

        return this;
    }

    public  NationalityInfoEntityBuilder withRandomData(Integer length) {
        entity.setId(getNextId());
        entity.setCountryOfBirth(CountryEntityBuilder.aBuilder().withRandom().build());
        entity.setNationality(CountryEntityBuilder.aBuilder().withRandom().build());
        entity.setPlaceOfBirth(generateRandomAlphabetic(length));

        return this;
    }
}
