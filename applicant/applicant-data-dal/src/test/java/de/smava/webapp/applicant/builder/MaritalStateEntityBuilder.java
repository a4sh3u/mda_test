package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.type.MaritalState;

import java.util.Random;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class MaritalStateEntityBuilder extends EntityBuilder<MaritalState> {

    protected MaritalState createEntity() {
        return MaritalState.MARRIED;
    }

    public static MaritalStateEntityBuilder aBuilder() {
        return new MaritalStateEntityBuilder();
    }

    public MaritalStateEntityBuilder withRandom() {
        int pick = new Random().nextInt(MaritalState.values().length);
        MaritalState answer = MaritalState.values()[pick];

        setEntity(answer);

        return this;
    }
}
