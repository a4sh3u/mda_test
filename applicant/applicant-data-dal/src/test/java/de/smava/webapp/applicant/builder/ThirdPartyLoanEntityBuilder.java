package de.smava.webapp.applicant.builder;

import de.smava.webapp.applicant.domain.ThirdPartyLoan;
import de.smava.webapp.applicant.type.ThirdPartyLoanSource;
import de.smava.webapp.applicant.type.ThirdPartyLoanType;
import de.smava.webapp.commons.currentdate.CurrentDate;
import org.joda.time.DateTime;

import java.util.Random;

/**
 * Created by Sebastian Hanulok on 2015-07-15.
 */
public class ThirdPartyLoanEntityBuilder extends EntityBuilder<ThirdPartyLoan> {

    protected ThirdPartyLoan createEntity() {
        return new ThirdPartyLoan();
    }

    public static ThirdPartyLoanEntityBuilder aBuilder() {
        return new ThirdPartyLoanEntityBuilder();
    }

    public ThirdPartyLoanEntityBuilder withData() {
        return withData(true, ThirdPartyLoanType.OTHER, ThirdPartyLoanSource.CUSTOMER);
    }

    public ThirdPartyLoanEntityBuilder withData(ThirdPartyLoanSource thirdPartyLoanSource) {
        return withData(true, ThirdPartyLoanType.OTHER, thirdPartyLoanSource);
    }

    public ThirdPartyLoanEntityBuilder withData(Boolean consolidated) {
        return withData(consolidated, ThirdPartyLoanType.OTHER, ThirdPartyLoanSource.CUSTOMER);
    }

    public ThirdPartyLoanEntityBuilder withData(Boolean consolidated, ThirdPartyLoanType loanType) {
        return withData(consolidated, loanType, ThirdPartyLoanSource.CUSTOMER);
    }

    public ThirdPartyLoanEntityBuilder withData(Boolean consolidated, ThirdPartyLoanType loanType, ThirdPartyLoanSource thirdPartyLoanSource) {
        entity.setId(getNextId());
        entity.setConsolidationWish(consolidated);
        entity.setRedemptionSum(2000d);
        entity.setMonthlyRate(200d);
        entity.setBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());
        DateTime now = new DateTime(CurrentDate.getDate());
        entity.setLoanStartDate(now.toDate());
        entity.setLoanEndDate(now.plusYears(1).toDate());
        entity.setOriginalAmount(2000d);
        entity.setLoanType(loanType);
        entity.setLoanSource(thirdPartyLoanSource);

        return this;
    }

    public ThirdPartyLoanEntityBuilder withTypeAndRandomData(Boolean consolidated, ThirdPartyLoanType type) {
        entity.setId(getNextId());
        entity.setConsolidationWish(consolidated);
        entity.setRedemptionSum(new Random().nextDouble());
        entity.setMonthlyRate(new Random().nextDouble());
        entity.setBankAccount(BankAccountEntityBuilder.aBuilder().withData().build());
        DateTime now = new DateTime(CurrentDate.getDate());
        entity.setLoanStartDate(now.toDate());
        entity.setLoanEndDate(now.plusYears(1).toDate());
        entity.setOriginalAmount(2000.0);
        entity.setLoanType(type);
        entity.setLoanSource(ThirdPartyLoanSource.CUSTOMER);

        return this;
    }
}
