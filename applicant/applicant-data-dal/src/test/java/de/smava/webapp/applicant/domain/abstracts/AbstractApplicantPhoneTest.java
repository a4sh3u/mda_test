package de.smava.webapp.applicant.domain.abstracts;

import de.smava.webapp.applicant.domain.Applicant;
import de.smava.webapp.applicant.domain.Phone;
import de.smava.webapp.applicant.builder.PhonesEntityBuilder;
import de.smava.webapp.applicant.type.*;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by PAWEL on 2016-02-10.
 */
public class AbstractApplicantPhoneTest {

    @Test
    public void testRetrievePhoneByPhoneType() throws Exception {
        Phone mobile = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone landline = PhonesEntityBuilder.aBuilder().withLandLine().build();
        Phone landline2 = PhonesEntityBuilder.aBuilder().withLandLine().build();
        landline2.setCreationDate(DateTime.now().toDate());

        // for empty
        Applicant applicant = new Applicant();
        Set<Phone> phones = new HashSet<Phone>();

        // when
        Phone phone = applicant.retrievePhone(PhoneType.MOBILE);

        // then
        assertThat("For empty", phone, is(nullValue()));


        // for
        phones.addAll(Arrays.asList(
                landline,
                mobile
        ));

        applicant.setPhones(phones);

        // when
        phone = applicant.retrievePhone(PhoneType.MOBILE);

        // then
        assertThat("For Mobile", phone, is(mobile));
        assertThat("For Mobile", phone.getPhoneType(), is(PhoneType.MOBILE));


        // when
        phone = applicant.retrievePhone(PhoneType.LANDLINE);

        // then
        assertThat("For Landline", phone, is(landline));
        assertThat("For Landline", phone.getPhoneType(), is(PhoneType.LANDLINE));


        // when
        phone = applicant.retrievePhone(PhoneType.WORK);

        // then
        assertThat("For Work", phone, is(nullValue()));


        // for multi phones
        phones.add(landline2);
        landline.setCreationDate(DateTime.now().minusDays(1).toDate());

        // when
        phone = applicant.retrievePhone(PhoneType.LANDLINE);

        // then
        assertThat("For Work", phone, is(landline2));
    }

    @Test
    public void testRetrieveLandlinePhone() throws Exception {
        // for
        Phone mobile = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone landline = PhonesEntityBuilder.aBuilder().withLandLine().build();
        Applicant applicant = new Applicant();
        Set<Phone> phones = new HashSet<Phone>();

        applicant.setPhones(phones);

        // when
        Phone phone = applicant.retrieveLandlinePhone();

        // then
        assertThat("Empty list", phone, is(nullValue()));


        // for
        phones.addAll(Arrays.asList(
                mobile
        ));

        applicant.setPhones(phones);

        // when
        phone = applicant.retrieveLandlinePhone();

        // then
        assertThat("Skipp phone", phone, is(nullValue()));


        // for
        phones.addAll(Arrays.asList(
                mobile,
                landline
        ));

        applicant.setPhones(phones);

        // when
        phone = applicant.retrieveLandlinePhone();

        // then
        assertThat("Correctly selected phone", phone, is(landline));
        assertThat("Correctly selected phone", phone.getPhoneType(), is(landline.getPhoneType()));
    }

    @Test
    public void testRetrieveLandlinePhones() throws Exception {

        Phone mobileFirst = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone mobileSecond = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone landlineFirst = PhonesEntityBuilder.aBuilder().withLandLine().build();
        Phone landlineSecond = PhonesEntityBuilder.aBuilder().withLandLine().build();

        // for empty
        Applicant applicant = new Applicant();
        Set<Phone> phones = new HashSet<Phone>();

        // when
        Set<Phone> landlinePhones = applicant.retrieveLandlinePhones();

        // then
        assertThat("Empty list", landlinePhones, is(notNullValue()));


        // for
        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveLandlinePhones();

        // then
        assertThat("Empty list", landlinePhones, is(notNullValue()));
        assertThat("Empty list", landlinePhones.size(), is(0));


        // for
        phones.addAll(Arrays.asList(
                mobileFirst, mobileSecond
        ));

        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveLandlinePhones();

        // then
        assertThat("Correctly skipped", landlinePhones, is(notNullValue()));
        assertThat("Correctly skipped", landlinePhones.size(), is(0));


        // for
        phones.addAll(Arrays.asList(
                mobileFirst, mobileSecond,
                landlineFirst, landlineSecond
        ));

        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveLandlinePhones();

        // then
        assertThat("Correctly selected", landlinePhones, is(notNullValue()));
        assertThat("Correctly selected", landlinePhones.size(), is(2));
        assertThat("Correctly selected", landlinePhones.contains(landlineFirst), is(true));
        assertThat("Correctly selected", landlinePhones.contains(landlineSecond), is(true));
    }

    @Test
    public void testRetrieveMobilePhone() throws Exception {
        // for
        Phone mobile = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone landline = PhonesEntityBuilder.aBuilder().withLandLine().build();
        Applicant applicant = new Applicant();
        Set<Phone> phones = new HashSet<Phone>();

        applicant.setPhones(phones);

        // when
        Phone phone = applicant.retrieveMobilePhone();

        // then
        assertThat("Empty list", phone, is(nullValue()));


        // for
        phones.addAll(Arrays.asList(
                landline
        ));

        applicant.setPhones(phones);

        // when
        phone = applicant.retrieveMobilePhone();

        // then
        assertThat("Skipped phone", phone, is(nullValue()));


        // for
        phones.addAll(Arrays.asList(
                mobile,
                landline
        ));

        applicant.setPhones(phones);

        // when
        phone = applicant.retrieveMobilePhone();

        // then
        assertThat("Correctly selected", phone, is(mobile));
        assertThat("Correctly selected", phone.getPhoneType(), is(mobile.getPhoneType()));
    }

    @Test
    public void testRetrieveMobilePhones() throws Exception {

        Phone mobileFirst = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone mobileSecond = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone landlineFirst = PhonesEntityBuilder.aBuilder().withLandLine().build();
        Phone landlineSecond = PhonesEntityBuilder.aBuilder().withLandLine().build();

        // for empty
        Applicant applicant = new Applicant();
        Set<Phone> phones = new HashSet<Phone>();

        // when
        Set<Phone> landlinePhones = applicant.retrieveMobilePhones();

        // then
        assertThat("Empty list", landlinePhones, is(notNullValue()));


        // for
        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveMobilePhones();

        // then
        assertThat("Empty list", landlinePhones, is(notNullValue()));
        assertThat("Empty list", landlinePhones.size(), is(0));


        // for
        phones.addAll(Arrays.asList(
                landlineFirst, landlineSecond
        ));

        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveMobilePhones();

        // then
        assertThat("Skipp phones", landlinePhones, is(notNullValue()));
        assertThat("Skipp phones", landlinePhones.size(), is(0));


        // for
        phones.addAll(Arrays.asList(
                mobileFirst, mobileSecond,
                landlineFirst, landlineSecond
        ));

        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveMobilePhones();

        // then
        assertThat("Correctly selected phones", landlinePhones, is(notNullValue()));
        assertThat("Correctly selected phones", landlinePhones.size(), is(2));
        assertThat("Correctly selected phones", landlinePhones.contains(mobileFirst), is(true));
        assertThat("Correctly selected phones", landlinePhones.contains(mobileSecond), is(true));
    }

    @Test
    public void testRetrieveWorkPhone() throws Exception {
        // for
        Phone mobileFirst = PhonesEntityBuilder.aBuilder().withMobile().build();
        mobileFirst.setCreationDate(new Date());
        Phone mobileSecond = PhonesEntityBuilder.aBuilder().withMobile().build();
        mobileSecond.setCreationDate(new Date());
        Phone landlineFirst = PhonesEntityBuilder.aBuilder().withLandLine().build();
        landlineFirst.setCreationDate(new Date());
        Phone landlineSecond = PhonesEntityBuilder.aBuilder().withLandLine().build();
        landlineSecond.setCode("678");
        mobileSecond.setPhoneType(PhoneType.WORK);
        landlineSecond.setPhoneType(PhoneType.WORK);

        Applicant applicant = new Applicant();
        Comparator<Phone> comparator = new Comparator<Phone>() {
            @Override
            public int compare(Phone o1, Phone o2) {
                return o1.getCreationDate().compareTo(o2.getCreationDate());
            }
        };

        Set<Phone> phones = new TreeSet<Phone>(comparator);

        applicant.setPhones(phones);

        // when
        Phone workPhone = applicant.retrieveWorkPhone();

        // then
        assertThat("Empty phone", workPhone, is(nullValue()));


        // for
        phones.addAll(Arrays.asList(
                mobileFirst,
                landlineFirst, landlineSecond
        ));

        applicant.setPhones(phones);

        // when
        workPhone = applicant.retrieveWorkPhone();

        // then
        assertThat("expected landline", workPhone, is(notNullValue()));
        assertThat("expected landline", workPhone, is(landlineSecond));
    }

    @Test
    public void testRetrieveWorkPhones() throws Exception {
        // for
        Phone mobileFirst = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone mobileSecond = PhonesEntityBuilder.aBuilder().withMobile().build();
        Phone landlineFirst = PhonesEntityBuilder.aBuilder().withLandLine().build();
        Phone landlineSecond = PhonesEntityBuilder.aBuilder().withLandLine().build();
        mobileSecond.setPhoneType(PhoneType.WORK);
        landlineSecond.setPhoneType(PhoneType.WORK);

        // for empty
        Applicant applicant = new Applicant();
        Set<Phone> phones = new HashSet<Phone>();

        // when
        Set<Phone> landlinePhones = applicant.retrieveWorkPhones();

        // then
        assertThat("Empty list", landlinePhones, is(notNullValue()));


        // for
        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveWorkPhones();

        // then
        assertThat("Empty list", landlinePhones, is(notNullValue()));
        assertThat("Empty list", landlinePhones.size(), is(0));


        // for
        phones.addAll(Arrays.asList(
                mobileFirst, landlineFirst
        ));

        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveWorkPhones();

        // then
        assertThat("Skipped phones", landlinePhones, is(notNullValue()));
        assertThat("Correctly selected", landlinePhones.size(), is(0));


        // for
        phones.addAll(Arrays.asList(
                mobileFirst, mobileSecond,
                landlineFirst, landlineSecond
        ));

        applicant.setPhones(phones);

        // when
        landlinePhones = applicant.retrieveWorkPhones();

        // then
        assertThat("Correctly selected", landlinePhones, is(notNullValue()));
        assertThat("Correctly selected", landlinePhones.size(), is(2));
        assertThat("Correctly selected", landlinePhones.contains(mobileSecond), is(true));
        assertThat("Correctly selected", landlinePhones.contains(landlineSecond), is(true));
    }

}