//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(expense)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.Expense;
import de.smava.webapp.applicant.domain.interfaces.ExpenseEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Expenses'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractExpense
    extends BrokerageEntity implements ExpenseEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractExpense.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Expense)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
            
        this.setExpenseType(((Expense) oldEntity).getExpenseType());    
        this.setDescription(((Expense) oldEntity).getDescription());    
        this.setMonthlyAmount(((Expense) oldEntity).getMonthlyAmount());    
        this.setExpirationDate(((Expense) oldEntity).getExpirationDate());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Expense)) {
            equals = false;
        }
        
            
            
        equals = equals && valuesAreEqual(this.getExpenseType(), ((Expense) otherEntity).getExpenseType());    
        equals = equals && valuesAreEqual(this.getDescription(), ((Expense) otherEntity).getDescription());    
        equals = equals && valuesAreEqual(this.getMonthlyAmount(), ((Expense) otherEntity).getMonthlyAmount());    
        equals = equals && valuesAreEqual(this.getExpirationDate(), ((Expense) otherEntity).getExpirationDate());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(expense)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

