package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.PersonalData;

import java.util.Date;


/**
 * The domain object that represents 'PersonalDatas'.
 *
 * @author generator
 */
public interface PersonalDataEntityInterface {

    /**
     * Setter for the property 'gender'.
     *
     * 
     *
     */
    void setGender(de.smava.webapp.applicant.type.Gender gender);

    /**
     * Returns the property 'gender'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.Gender getGender();
    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    void setFirstName(String firstName);

    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    String getFirstName();
    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    void setLastName(String lastName);

    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    String getLastName();
    /**
     * Setter for the property 'birth name'.
     *
     * 
     *
     */
    void setBirthName(String birthName);

    /**
     * Returns the property 'birth name'.
     *
     * 
     *
     */
    String getBirthName();
    /**
     * Setter for the property 'birth date'.
     *
     * 
     *
     */
    void setBirthDate(Date birthDate);

    /**
     * Returns the property 'birth date'.
     *
     * 
     *
     */
    Date getBirthDate();
    /**
     * Setter for the property 'marital state'.
     *
     * 
     *
     */
    void setMaritalState(de.smava.webapp.applicant.type.MaritalState maritalState);

    /**
     * Returns the property 'marital state'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.MaritalState getMaritalState();
    /**
     * Helper method to get reference of this object as model type.
     */
    PersonalData asPersonalData();
}
