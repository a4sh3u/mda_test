package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractRent;




/**
 * The domain object that has all history aggregation related fields for 'Rents'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RentHistory extends AbstractRent {




}
