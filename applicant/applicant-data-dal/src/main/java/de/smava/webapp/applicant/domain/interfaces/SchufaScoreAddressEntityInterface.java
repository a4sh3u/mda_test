package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Address;
import de.smava.webapp.applicant.domain.SchufaScoreAddress;
import de.smava.webapp.applicant.domain.SchufaScoreRequestData;


/**
 * The domain object that represents 'SchufaScoreAddresss'.
 *
 * @author generator
 */
public interface SchufaScoreAddressEntityInterface {

    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    Address getAddress();
    /**
     * Setter for the property 'request data'.
     *
     * 
     *
     */
    void setRequestData(SchufaScoreRequestData requestData);

    /**
     * Returns the property 'request data'.
     *
     * 
     *
     */
    SchufaScoreRequestData getRequestData();
    /**
     * Helper method to get reference of this object as model type.
     */
    SchufaScoreAddress asSchufaScoreAddress();
}
