//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(estate)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.EstateEntityInterface;
import de.smava.webapp.applicant.domain.interfaces.HousingCostEntityInterface;import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Estates'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * The person is the owner of his home. Either its a house or a freehold flat.
 *
 * @author generator
 */
public abstract class AbstractEstate
    extends HousingCost    implements EstateEntityInterface    ,HousingCostEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractEstate.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Estate)) {
            return;
        }
        
        this.setPartiallyRented(((Estate) oldEntity).getPartiallyRented());    
        this.setSquaremetersUsed(((Estate) oldEntity).getSquaremetersUsed());    
        this.setEstateType(((Estate) oldEntity).getEstateType());    
        this.setEstateFinancing(((Estate) oldEntity).getEstateFinancing());    
        super.copyFromOldEntity(oldEntity);
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Estate)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getPartiallyRented(), ((Estate) otherEntity).getPartiallyRented());    
        equals = equals && valuesAreEqual(this.getSquaremetersUsed(), ((Estate) otherEntity).getSquaremetersUsed());    
        equals = equals && valuesAreEqual(this.getEstateType(), ((Estate) otherEntity).getEstateType());    
        equals = equals && valuesAreEqual(this.getEstateFinancing(), ((Estate) otherEntity).getEstateFinancing());    
        equals = equals && super.functionallyEquals(otherEntity);
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(estate)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

