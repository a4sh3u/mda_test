package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Applicant;
import de.smava.webapp.applicant.domain.CreditScore;
import de.smava.webapp.applicant.domain.SchufaScore;

import java.util.Date;


/**
 * The domain object that represents 'CreditScores'.
 *
 * @author generator
 */
public interface CreditScoreEntityInterface {

    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    void setApplicant(Applicant applicant);

    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    Applicant getApplicant();
    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    void setScore(Long score);

    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    Long getScore();
    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    String getRating();
    /**
     * Setter for the property 'schufa score'.
     *
     * 
     *
     */
    void setSchufaScore(SchufaScore schufaScore);

    /**
     * Returns the property 'schufa score'.
     *
     * 
     *
     */
    SchufaScore getSchufaScore();
    /**
     * Setter for the property 'change date'.
     *
     * 
     *
     */
    void setChangeDate(Date changeDate);

    /**
     * Returns the property 'change date'.
     *
     * 
     *
     */
    Date getChangeDate();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Helper method to get reference of this object as model type.
     */
    CreditScore asCreditScore();
}
