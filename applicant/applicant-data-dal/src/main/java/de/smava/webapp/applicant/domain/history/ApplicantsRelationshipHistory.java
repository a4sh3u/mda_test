package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractApplicantsRelationship;




/**
 * The domain object that has all history aggregation related fields for 'ApplicantsRelationships'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ApplicantsRelationshipHistory extends AbstractApplicantsRelationship {

    protected transient de.smava.webapp.applicant.type.ApplicantsRelationshipType _relationshipTypeInitVal;
    protected transient boolean _relationshipTypeIsSet;
    protected transient Boolean _sharedHouseholdInitVal;
    protected transient boolean _sharedHouseholdIsSet;


			
    /**
     * Returns the initial value of the property 'relationshipType'.
     */
    public de.smava.webapp.applicant.type.ApplicantsRelationshipType relationshipTypeInitVal() {
        de.smava.webapp.applicant.type.ApplicantsRelationshipType result;
        if (_relationshipTypeIsSet) {
            result = _relationshipTypeInitVal;
        } else {
            result = getRelationshipType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'relationshipType'.
     */
    public boolean relationshipTypeIsDirty() {
        return !valuesAreEqual(relationshipTypeInitVal(), getRelationshipType());
    }

    /**
     * Returns true if the setter method was called for the property 'relationshipType'.
     */
    public boolean relationshipTypeIsSet() {
        return _relationshipTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'shared household'.
     */
    public Boolean sharedHouseholdInitVal() {
        Boolean result;
        if (_sharedHouseholdIsSet) {
            result = _sharedHouseholdInitVal;
        } else {
            result = getSharedHousehold();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'shared household'.
     */
    public boolean sharedHouseholdIsDirty() {
        return !valuesAreEqual(sharedHouseholdInitVal(), getSharedHousehold());
    }

    /**
     * Returns true if the setter method was called for the property 'shared household'.
     */
    public boolean sharedHouseholdIsSet() {
        return _sharedHouseholdIsSet;
    }

}
