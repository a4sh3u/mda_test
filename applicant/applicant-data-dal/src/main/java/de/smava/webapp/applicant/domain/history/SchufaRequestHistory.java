package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSchufaRequest;





/**
 * The domain object that has all history aggregation related fields for 'SchufaRequests'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SchufaRequestHistory extends AbstractSchufaRequest {

    protected transient String _requestXmlInitVal;
    protected transient boolean _requestXmlIsSet;
    protected transient String _responseXmlInitVal;
    protected transient boolean _responseXmlIsSet;
    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _stateInitVal;
    protected transient boolean _stateIsSet;
    protected transient String _typeInitVal;
    protected transient boolean _typeIsSet;
    protected transient String _accountNumberInitVal;
    protected transient boolean _accountNumberIsSet;
    protected transient String _schufaPersonKeyInitVal;
    protected transient boolean _schufaPersonKeyIsSet;


	
    /**
     * Returns the initial value of the property 'request xml'.
     */
    public String requestXmlInitVal() {
        String result;
        if (_requestXmlIsSet) {
            result = _requestXmlInitVal;
        } else {
            result = getRequestXml();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'request xml'.
     */
    public boolean requestXmlIsDirty() {
        return !valuesAreEqual(requestXmlInitVal(), getRequestXml());
    }

    /**
     * Returns true if the setter method was called for the property 'request xml'.
     */
    public boolean requestXmlIsSet() {
        return _requestXmlIsSet;
    }
	
    /**
     * Returns the initial value of the property 'response xml'.
     */
    public String responseXmlInitVal() {
        String result;
        if (_responseXmlIsSet) {
            result = _responseXmlInitVal;
        } else {
            result = getResponseXml();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'response xml'.
     */
    public boolean responseXmlIsDirty() {
        return !valuesAreEqual(responseXmlInitVal(), getResponseXml());
    }

    /**
     * Returns true if the setter method was called for the property 'response xml'.
     */
    public boolean responseXmlIsSet() {
        return _responseXmlIsSet;
    }
	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'state'.
     */
    public String stateInitVal() {
        String result;
        if (_stateIsSet) {
            result = _stateInitVal;
        } else {
            result = getState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'state'.
     */
    public boolean stateIsDirty() {
        return !valuesAreEqual(stateInitVal(), getState());
    }

    /**
     * Returns true if the setter method was called for the property 'state'.
     */
    public boolean stateIsSet() {
        return _stateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'type'.
     */
    public String typeInitVal() {
        String result;
        if (_typeIsSet) {
            result = _typeInitVal;
        } else {
            result = getType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'type'.
     */
    public boolean typeIsDirty() {
        return !valuesAreEqual(typeInitVal(), getType());
    }

    /**
     * Returns true if the setter method was called for the property 'type'.
     */
    public boolean typeIsSet() {
        return _typeIsSet;
    }
		
    /**
     * Returns the initial value of the property 'account number'.
     */
    public String accountNumberInitVal() {
        String result;
        if (_accountNumberIsSet) {
            result = _accountNumberInitVal;
        } else {
            result = getAccountNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'account number'.
     */
    public boolean accountNumberIsDirty() {
        return !valuesAreEqual(accountNumberInitVal(), getAccountNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'account number'.
     */
    public boolean accountNumberIsSet() {
        return _accountNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'schufa person key'.
     */
    public String schufaPersonKeyInitVal() {
        String result;
        if (_schufaPersonKeyIsSet) {
            result = _schufaPersonKeyInitVal;
        } else {
            result = getSchufaPersonKey();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa person key'.
     */
    public boolean schufaPersonKeyIsDirty() {
        return !valuesAreEqual(schufaPersonKeyInitVal(), getSchufaPersonKey());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa person key'.
     */
    public boolean schufaPersonKeyIsSet() {
        return _schufaPersonKeyIsSet;
    }
	
}
