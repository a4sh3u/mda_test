//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(estate)}
import de.smava.webapp.applicant.domain.history.EstateHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Estates'.
 *
 * The person is the owner of his home. Either its a house or a freehold flat.
 *
 * @author generator
 */
public class Estate extends EstateHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(estate)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.EstateRentType _partiallyRented;
        protected Double _squaremetersUsed;
        protected de.smava.webapp.applicant.type.EstateType _estateType;
        protected EstateFinancing _estateFinancing;
        
                            /**
     * Setter for the property 'partially rented'.
     *
     * Partially rented means an estate where rooms or flats are rented to third persons for money!
     *
     */
    public void setPartiallyRented(de.smava.webapp.applicant.type.EstateRentType partiallyRented) {
        if (!_partiallyRentedIsSet) {
            _partiallyRentedIsSet = true;
            _partiallyRentedInitVal = getPartiallyRented();
        }
        registerChange("partially rented", _partiallyRentedInitVal, partiallyRented);
        _partiallyRented = partiallyRented;
    }
                        
    /**
     * Returns the property 'partially rented'.
     *
     * Partially rented means an estate where rooms or flats are rented to third persons for money!
     *
     */
    public de.smava.webapp.applicant.type.EstateRentType getPartiallyRented() {
        return _partiallyRented;
    }
                                    /**
     * Setter for the property 'squaremeters used'.
     *
     * 
     *
     */
    public void setSquaremetersUsed(Double squaremetersUsed) {
        if (!_squaremetersUsedIsSet) {
            _squaremetersUsedIsSet = true;
            _squaremetersUsedInitVal = getSquaremetersUsed();
        }
        registerChange("squaremeters used", _squaremetersUsedInitVal, squaremetersUsed);
        _squaremetersUsed = squaremetersUsed;
    }
                        
    /**
     * Returns the property 'squaremeters used'.
     *
     * 
     *
     */
    public Double getSquaremetersUsed() {
        return _squaremetersUsed;
    }
                                    /**
     * Setter for the property 'estate type'.
     *
     * 
     *
     */
    public void setEstateType(de.smava.webapp.applicant.type.EstateType estateType) {
        if (!_estateTypeIsSet) {
            _estateTypeIsSet = true;
            _estateTypeInitVal = getEstateType();
        }
        registerChange("estate type", _estateTypeInitVal, estateType);
        _estateType = estateType;
    }
                        
    /**
     * Returns the property 'estate type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.EstateType getEstateType() {
        return _estateType;
    }
                                            
    /**
     * Setter for the property 'estate financing'.
     *
     * 
     *
     */
    public void setEstateFinancing(EstateFinancing estateFinancing) {
        _estateFinancing = estateFinancing;
    }
            
    /**
     * Returns the property 'estate financing'.
     *
     * 
     *
     */
    public EstateFinancing getEstateFinancing() {
        return _estateFinancing;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_estateFinancing instanceof de.smava.webapp.commons.domain.Entity && !_estateFinancing.getChangeSet().isEmpty()) {
             for (String element : _estateFinancing.getChangeSet()) {
                 result.add("estate financing : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Estate.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _partiallyRented=").append(_partiallyRented);
            builder.append("\n    _estateType=").append(_estateType);
            builder.append("\n}");
        } else {
            builder.append(Estate.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Estate asEstate() {
        return this;
    }
}
