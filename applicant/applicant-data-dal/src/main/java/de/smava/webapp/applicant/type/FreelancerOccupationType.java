package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 02.03.15.
 */
public enum FreelancerOccupationType {

    LAWYER,
    PHARMACIST,
    ARCHITECT,
    MD,
    TRANSLATER,
    IT_CONSULTANT,
    PHOTOGRAPHER,
    MIDWIFE,
    HEALER,
    ENGINEER,
    JOURNALIST,
    PHYSICAL_THERAPIST,
    NOTARY,
    PSYCHIATRIST,
    EXPERT,
    STRUCTURAL_ENGINEER,
    TAX_ADVISOR,
    VETERINARY,
    AUDITOR,
    DENTIST,
    OTHER,
    GERIATRIC_NURSE,
    MOBILE_NURSE,
    NURSE,
    GRAPHIC_DESIGNER,
    SPEECH_THERAPIST,
    CLASSIC_MUSICIAN,
    PHYSIOTHERAPIST,

    ERGOTHERAPIST, //Ergotherapeut
    MASSAGE_THERAPIST, //Heilmasseur
    DIETICIAN, //Diätassistent
    NUTRITIONIST, //Ernährungsberater
    DENTAL_TECHNICAL, //Zahntechniker
    MEDICAL_TECHNICAL_ASSISTENT, //Medizinisch-technischer Assistent
    EXECUTIVE_CONSULTANT, //Unternehmensberater
    LIQUIDATOR, //Insolvenzverwalter
    DATA_PROTECTION_OFFICER, //Datenschutzbeauftragter
    RECRUITMENT_CONSULTANT, //Personalberater
    URBAN_PLANNER, //Stadtplaner
    COMPUTER_SCIENTIST, //Informatiker
    GEOGRAPHER, //Geograf
    DRAFTSMAN, //Konstrukteur
    GRAPHIC_DESIGNER2, //Grafikerdesigner/in
    INTERIOR_DECORATOR, //Raumausstatter
    ADORNER, //Dekorateur
    HISTORIAN, //Historiker
    BROADCASTER, //Rundfunksprecher
    OPERA_SINGER, //Opernsänger
    MORTICIAN; //Bestatter
}
