package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.PensionSupplement;


/**
 * The domain object that represents 'PensionSupplements'.
 *
 * @author generator
 */
public interface PensionSupplementEntityInterface {

    /**
     * Setter for the property 'pension supplement type'.
     *
     * 
     *
     */
    void setPensionSupplementType(de.smava.webapp.applicant.type.PensionType pensionSupplementType);

    /**
     * Returns the property 'pension supplement type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.PensionType getPensionSupplementType();
    /**
     * Helper method to get reference of this object as model type.
     */
    PensionSupplement asPensionSupplement();
}
