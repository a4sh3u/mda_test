package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 15.06.15.
 */
public enum SoldierType {

    /**
     * Zeitsoldat
     */
    REGULAR,

    /**
     * Berufssoldat
     */
    PROF_SOLDIER,

    /**
     * Wehrdienstleistender
     */
    MILITARY_SERVICE

}
