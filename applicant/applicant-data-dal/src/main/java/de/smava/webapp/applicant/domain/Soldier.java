//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(soldier)}
import de.smava.webapp.applicant.domain.history.SoldierHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Soldiers'.
 *
 * 
 *
 * @author generator
 */
public class Soldier extends SoldierHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(soldier)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.SoldierType _soldierType;
        
                            /**
     * Setter for the property 'soldier type'.
     *
     * 
     *
     */
    public void setSoldierType(de.smava.webapp.applicant.type.SoldierType soldierType) {
        if (!_soldierTypeIsSet) {
            _soldierTypeIsSet = true;
            _soldierTypeInitVal = getSoldierType();
        }
        registerChange("soldier type", _soldierTypeInitVal, soldierType);
        _soldierType = soldierType;
    }
                        
    /**
     * Returns the property 'soldier type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.SoldierType getSoldierType() {
        return _soldierType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Soldier.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _soldierType=").append(_soldierType);
            builder.append("\n}");
        } else {
            builder.append(Soldier.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Soldier asSoldier() {
        return this;
    }
}
