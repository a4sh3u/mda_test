package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Estate;
import de.smava.webapp.applicant.domain.EstateFinancing;


/**
 * The domain object that represents 'Estates'.
 *
 * @author generator
 */
public interface EstateEntityInterface {

    /**
     * Setter for the property 'partially rented'.
     *
     * Partially rented means an estate where rooms or flats are rented to third persons for money!
     *
     */
    void setPartiallyRented(de.smava.webapp.applicant.type.EstateRentType partiallyRented);

    /**
     * Returns the property 'partially rented'.
     *
     * Partially rented means an estate where rooms or flats are rented to third persons for money!
     *
     */
    de.smava.webapp.applicant.type.EstateRentType getPartiallyRented();
    /**
     * Setter for the property 'squaremeters used'.
     *
     * 
     *
     */
    void setSquaremetersUsed(Double squaremetersUsed);

    /**
     * Returns the property 'squaremeters used'.
     *
     * 
     *
     */
    Double getSquaremetersUsed();
    /**
     * Setter for the property 'estate type'.
     *
     * 
     *
     */
    void setEstateType(de.smava.webapp.applicant.type.EstateType estateType);

    /**
     * Returns the property 'estate type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.EstateType getEstateType();
    /**
     * Setter for the property 'estate financing'.
     *
     * 
     *
     */
    void setEstateFinancing(EstateFinancing estateFinancing);

    /**
     * Returns the property 'estate financing'.
     *
     * 
     *
     */
    EstateFinancing getEstateFinancing();
    /**
     * Helper method to get reference of this object as model type.
     */
    Estate asEstate();
}
