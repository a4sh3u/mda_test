package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSelfEmployedIncomeExpense;




/**
 * The domain object that has all history aggregation related fields for 'SelfEmployedIncomeExpenses'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SelfEmployedIncomeExpenseHistory extends AbstractSelfEmployedIncomeExpense {

    protected transient int _yearInitVal;
    protected transient boolean _yearIsSet;
    protected transient Double _annualGrossTurnoverInitVal;
    protected transient boolean _annualGrossTurnoverIsSet;
    protected transient Double _annualExpensesInitVal;
    protected transient boolean _annualExpensesIsSet;


	
    /**
     * Returns the initial value of the property 'year'.
     */
    public int yearInitVal() {
        int result;
        if (_yearIsSet) {
            result = _yearInitVal;
        } else {
            result = getYear();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'year'.
     */
    public boolean yearIsDirty() {
        return !valuesAreEqual(yearInitVal(), getYear());
    }

    /**
     * Returns true if the setter method was called for the property 'year'.
     */
    public boolean yearIsSet() {
        return _yearIsSet;
    }
	
    /**
     * Returns the initial value of the property 'annual gross turnover'.
     */
    public Double annualGrossTurnoverInitVal() {
        Double result;
        if (_annualGrossTurnoverIsSet) {
            result = _annualGrossTurnoverInitVal;
        } else {
            result = getAnnualGrossTurnover();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'annual gross turnover'.
     */
    public boolean annualGrossTurnoverIsDirty() {
        return !valuesAreEqual(annualGrossTurnoverInitVal(), getAnnualGrossTurnover());
    }

    /**
     * Returns true if the setter method was called for the property 'annual gross turnover'.
     */
    public boolean annualGrossTurnoverIsSet() {
        return _annualGrossTurnoverIsSet;
    }
	
    /**
     * Returns the initial value of the property 'annual expenses'.
     */
    public Double annualExpensesInitVal() {
        Double result;
        if (_annualExpensesIsSet) {
            result = _annualExpensesInitVal;
        } else {
            result = getAnnualExpenses();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'annual expenses'.
     */
    public boolean annualExpensesIsDirty() {
        return !valuesAreEqual(annualExpensesInitVal(), getAnnualExpenses());
    }

    /**
     * Returns true if the setter method was called for the property 'annual expenses'.
     */
    public boolean annualExpensesIsSet() {
        return _annualExpensesIsSet;
    }
	
}
