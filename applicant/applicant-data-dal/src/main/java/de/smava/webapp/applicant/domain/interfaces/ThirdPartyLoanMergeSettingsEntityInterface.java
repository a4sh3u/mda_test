package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.ThirdPartyLoanMergeSettings;


/**
 * The domain object that represents 'ThirdPartyLoanMergeSettingss'.
 *
 * @author generator
 */
public interface ThirdPartyLoanMergeSettingsEntityInterface {

    /**
     * Setter for the property 'schufa token code'.
     *
     * 
     *
     */
    void setSchufaTokenCode(String schufaTokenCode);

    /**
     * Returns the property 'schufa token code'.
     *
     * 
     *
     */
    String getSchufaTokenCode();
    /**
     * Setter for the property 'maximum merge level'.
     *
     * 
     *
     */
    void setMaximumMergeLevel(Integer maximumMergeLevel);

    /**
     * Returns the property 'maximum merge level'.
     *
     * 
     *
     */
    Integer getMaximumMergeLevel();
    /**
     * Helper method to get reference of this object as model type.
     */
    ThirdPartyLoanMergeSettings asThirdPartyLoanMergeSettings();
}
