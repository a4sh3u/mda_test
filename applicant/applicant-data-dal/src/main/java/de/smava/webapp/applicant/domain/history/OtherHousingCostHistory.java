package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractOtherHousingCost;




/**
 * The domain object that has all history aggregation related fields for 'OtherHousingCosts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class OtherHousingCostHistory extends AbstractOtherHousingCost {

    protected transient de.smava.webapp.applicant.type.OtherHousingCostType _otherHousingCostTypeInitVal;
    protected transient boolean _otherHousingCostTypeIsSet;


	
    /**
     * Returns the initial value of the property 'other housing cost type'.
     */
    public de.smava.webapp.applicant.type.OtherHousingCostType otherHousingCostTypeInitVal() {
        de.smava.webapp.applicant.type.OtherHousingCostType result;
        if (_otherHousingCostTypeIsSet) {
            result = _otherHousingCostTypeInitVal;
        } else {
            result = getOtherHousingCostType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'other housing cost type'.
     */
    public boolean otherHousingCostTypeIsDirty() {
        return !valuesAreEqual(otherHousingCostTypeInitVal(), getOtherHousingCostType());
    }

    /**
     * Returns true if the setter method was called for the property 'other housing cost type'.
     */
    public boolean otherHousingCostTypeIsSet() {
        return _otherHousingCostTypeIsSet;
    }

}
