//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(credit score)}
import de.smava.webapp.applicant.domain.history.CreditScoreHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CreditScores'.
 *
 * 
 *
 * @author generator
 */
public class CreditScore extends CreditScoreHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(credit score)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Applicant _applicant;
        protected Long _score;
        protected String _rating;
        protected SchufaScore _schufaScore;
        protected java.util.Date _changeDate;
        protected String _type;
        
                                    
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    public void setApplicant(Applicant applicant) {
        _applicant = applicant;
    }
            
    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    public Applicant getApplicant() {
        return _applicant;
    }
                                            
    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    public void setScore(Long score) {
        _score = score;
    }
            
    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    public Long getScore() {
        return _score;
    }
                                    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    public String getRating() {
        return _rating;
    }
                                            
    /**
     * Setter for the property 'schufa score'.
     *
     * 
     *
     */
    public void setSchufaScore(SchufaScore schufaScore) {
        _schufaScore = schufaScore;
    }
            
    /**
     * Returns the property 'schufa score'.
     *
     * 
     *
     */
    public SchufaScore getSchufaScore() {
        return _schufaScore;
    }
                                    /**
     * Setter for the property 'change date'.
     *
     * 
     *
     */
    public void setChangeDate(java.util.Date changeDate) {
        if (!_changeDateIsSet) {
            _changeDateIsSet = true;
            _changeDateInitVal = getChangeDate();
        }
        registerChange("change date", _changeDateInitVal, changeDate);
        _changeDate = changeDate;
    }
                        
    /**
     * Returns the property 'change date'.
     *
     * 
     *
     */
    public java.util.Date getChangeDate() {
        return _changeDate;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_applicant instanceof de.smava.webapp.commons.domain.Entity && !_applicant.getChangeSet().isEmpty()) {
             for (String element : _applicant.getChangeSet()) {
                 result.add("applicant : " + element);
             }
         }

         if (_schufaScore instanceof de.smava.webapp.commons.domain.Entity && !_schufaScore.getChangeSet().isEmpty()) {
             for (String element : _schufaScore.getChangeSet()) {
                 result.add("schufa score : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(CreditScore.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _changeDate=").append(_changeDate);
            builder.append("\n    _type=").append(_type);
            builder.append("\n}");
        } else {
            builder.append(CreditScore.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public CreditScore asCreditScore() {
        return this;
    }
}
