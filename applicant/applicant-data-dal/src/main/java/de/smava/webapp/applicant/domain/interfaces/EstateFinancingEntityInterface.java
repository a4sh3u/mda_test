package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.EstateFinancing;
import de.smava.webapp.applicant.domain.Investment;

import java.util.Set;


/**
 * The domain object that represents 'EstateFinancings'.
 *
 * @author generator
 */
public interface EstateFinancingEntityInterface {

    /**
     * Setter for the property 'monthly financing amount'.
     *
     * 
     *
     */
    void setMonthlyFinancingAmount(Double monthlyFinancingAmount);

    /**
     * Returns the property 'monthly financing amount'.
     *
     * 
     *
     */
    Double getMonthlyFinancingAmount();
    /**
     * Setter for the property 'repayment replacements'.
     *
     * 
     *
     */
    void setRepaymentReplacements(Set<Investment> repaymentReplacements);

    /**
     * Returns the property 'repayment replacements'.
     *
     * 
     *
     */
    Set<Investment> getRepaymentReplacements();
    /**
     * Helper method to get reference of this object as model type.
     */
    EstateFinancing asEstateFinancing();
}
