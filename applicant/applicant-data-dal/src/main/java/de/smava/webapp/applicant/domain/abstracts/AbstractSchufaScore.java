//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.SchufaScoreEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaScores'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSchufaScore
    extends BrokerageEntity    implements SchufaScoreEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaScore.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SchufaScore)) {
            return;
        }
        
        this.setApplicant(((SchufaScore) oldEntity).getApplicant());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setScore(((SchufaScore) oldEntity).getScore());    
        this.setRating(((SchufaScore) oldEntity).getRating());    
        this.setType(((SchufaScore) oldEntity).getType());    
        this.setRemark(((SchufaScore) oldEntity).getRemark());    
        this.setState(((SchufaScore) oldEntity).getState());    
        this.setRequestData(((SchufaScore) oldEntity).getRequestData());    
        this.setRequest(((SchufaScore) oldEntity).getRequest());    
        this.setVersion(((SchufaScore) oldEntity).getVersion());    
        this.setTokens(((SchufaScore) oldEntity).getTokens());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SchufaScore)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getApplicant(), ((SchufaScore) otherEntity).getApplicant());    
            
        equals = equals && valuesAreEqual(this.getScore(), ((SchufaScore) otherEntity).getScore());    
        equals = equals && valuesAreEqual(this.getRating(), ((SchufaScore) otherEntity).getRating());    
        equals = equals && valuesAreEqual(this.getType(), ((SchufaScore) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getRemark(), ((SchufaScore) otherEntity).getRemark());    
        equals = equals && valuesAreEqual(this.getState(), ((SchufaScore) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getRequestData(), ((SchufaScore) otherEntity).getRequestData());    
        equals = equals && valuesAreEqual(this.getRequest(), ((SchufaScore) otherEntity).getRequest());    
        equals = equals && valuesAreEqual(this.getVersion(), ((SchufaScore) otherEntity).getVersion());    
        equals = equals && valuesAreEqual(this.getTokens(), ((SchufaScore) otherEntity).getTokens());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(schufa score)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

