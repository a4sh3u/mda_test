package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 24.02.15.
 */
public enum DebtCreditCardsType {

    NO_DEBIT_OR_CREDIT_CARD, DEBIT_CARD_ONLY, CREDIT_CARD_ONLY, DEBIT_AND_CREDIT_CARD;

}
