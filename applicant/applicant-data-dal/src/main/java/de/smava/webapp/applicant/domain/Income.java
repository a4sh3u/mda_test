//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(income)}
import de.smava.webapp.applicant.domain.history.IncomeHistory;
import de.smava.webapp.applicant.type.IncomeType;

import java.util.Map;
import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Incomes'.
 *
 * 
 *
 * @author generator
 */
public class Income extends IncomeHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(income)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected de.smava.webapp.applicant.type.IncomeType _incomeType;
        protected String _description;
        protected Double _monthlyNetIncome;
        protected java.util.Date _expirationDate;
        protected Boolean _constantOverThreeMonths;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'income type '.
     *
     * 
     *
     */
    public void setIncomeType(de.smava.webapp.applicant.type.IncomeType incomeType) {
        if (!_incomeTypeIsSet) {
            _incomeTypeIsSet = true;
            _incomeTypeInitVal = getIncomeType();
        }
        registerChange("income type ", _incomeTypeInitVal, incomeType);
        _incomeType = incomeType;
    }
                        
    /**
     * Returns the property 'income type '.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.IncomeType getIncomeType() {
        return _incomeType;
    }
                                    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'monthly net income'.
     *
     * 
     *
     */
    public void setMonthlyNetIncome(Double monthlyNetIncome) {
        if (!_monthlyNetIncomeIsSet) {
            _monthlyNetIncomeIsSet = true;
            _monthlyNetIncomeInitVal = getMonthlyNetIncome();
        }
        registerChange("monthly net income", _monthlyNetIncomeInitVal, monthlyNetIncome);
        _monthlyNetIncome = monthlyNetIncome;
    }
                        
    /**
     * Returns the property 'monthly net income'.
     *
     * 
     *
     */
    public Double getMonthlyNetIncome() {
        return _monthlyNetIncome;
    }
                                    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    public void setExpirationDate(java.util.Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    public java.util.Date getExpirationDate() {
        return _expirationDate;
    }
                                    /**
     * Setter for the property 'constant over three months'.
     *
     * 
     *
     */
    public void setConstantOverThreeMonths(Boolean constantOverThreeMonths) {
        if (!_constantOverThreeMonthsIsSet) {
            _constantOverThreeMonthsIsSet = true;
            _constantOverThreeMonthsInitVal = getConstantOverThreeMonths();
        }
        registerChange("constant over three months", _constantOverThreeMonthsInitVal, constantOverThreeMonths);
        _constantOverThreeMonths = constantOverThreeMonths;
    }
                        
    /**
     * Returns the property 'constant over three months'.
     *
     * 
     *
     */
    public Boolean getConstantOverThreeMonths() {
        return _constantOverThreeMonths;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Income.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _incomeType=").append(_incomeType);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _expirationDate=").append(_expirationDate);
            builder.append("\n}");
        } else {
            builder.append(Income.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Income asIncome() {
        return this;
    }
}
