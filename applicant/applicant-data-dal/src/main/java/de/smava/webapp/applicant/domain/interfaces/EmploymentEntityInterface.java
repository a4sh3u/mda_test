package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Employment;
import de.smava.webapp.applicant.type.EmploymentType;


/**
 * The domain object that represents 'Employments'.
 *
 * @author generator
 */
public interface EmploymentEntityInterface {

    /**
     * Setter for the property 'employment type'.
     *
     * 
     *
     */
    void setEmploymentType(EmploymentType employmentType);

    /**
     * Returns the property 'employment type'.
     *
     * 
     *
     */
    EmploymentType getEmploymentType();
    /**
     * Helper method to get reference of this object as model type.
     */
    Employment asEmployment();
}
