package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSchufaScoreRequestData;
import de.smava.webapp.applicant.type.Gender;

import java.util.Date;




/**
 * The domain object that has all history aggregation related fields for 'SchufaScoreRequestDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SchufaScoreRequestDataHistory extends AbstractSchufaScoreRequestData {

    protected transient String _firstNameInitVal;
    protected transient boolean _firstNameIsSet;
    protected transient String _lastNameInitVal;
    protected transient boolean _lastNameIsSet;
    protected transient Gender _genderInitVal;
    protected transient boolean _genderIsSet;
    protected transient java.util.Date _birthDateInitVal;
    protected transient boolean _birthDateIsSet;


	
    /**
     * Returns the initial value of the property 'first name'.
     */
    public String firstNameInitVal() {
        String result;
        if (_firstNameIsSet) {
            result = _firstNameInitVal;
        } else {
            result = getFirstName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first name'.
     */
    public boolean firstNameIsDirty() {
        return !valuesAreEqual(firstNameInitVal(), getFirstName());
    }

    /**
     * Returns true if the setter method was called for the property 'first name'.
     */
    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last name'.
     */
    public String lastNameInitVal() {
        String result;
        if (_lastNameIsSet) {
            result = _lastNameInitVal;
        } else {
            result = getLastName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last name'.
     */
    public boolean lastNameIsDirty() {
        return !valuesAreEqual(lastNameInitVal(), getLastName());
    }

    /**
     * Returns true if the setter method was called for the property 'last name'.
     */
    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'gender'.
     */
    public Gender genderInitVal() {
        Gender result;
        if (_genderIsSet) {
            result = _genderInitVal;
        } else {
            result = getGender();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'gender'.
     */
    public boolean genderIsDirty() {
        return !valuesAreEqual(genderInitVal(), getGender());
    }

    /**
     * Returns true if the setter method was called for the property 'gender'.
     */
    public boolean genderIsSet() {
        return _genderIsSet;
    }
	
    /**
     * Returns the initial value of the property 'birth date'.
     */
    public java.util.Date birthDateInitVal() {
        java.util.Date result;
        if (_birthDateIsSet) {
            result = _birthDateInitVal;
        } else {
            result = getBirthDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birth date'.
     */
    public boolean birthDateIsDirty() {
        return !valuesAreEqual(birthDateInitVal(), getBirthDate());
    }

    /**
     * Returns true if the setter method was called for the property 'birth date'.
     */
    public boolean birthDateIsSet() {
        return _birthDateIsSet;
    }
	
}
