package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Phone;


/**
 * The domain object that represents 'Phones'.
 *
 * @author generator
 */
public interface PhoneEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(java.util.Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    java.util.Date getCreationDate();
    /**
     * Setter for the property 'phone type'.
     *
     * 
     *
     */
    void setPhoneType(de.smava.webapp.applicant.type.PhoneType phoneType);

    /**
     * Returns the property 'phone type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.PhoneType getPhoneType();
    /**
     * Setter for the property 'code'.
     *
     * 
     *
     */
    void setCode(String code);

    /**
     * Returns the property 'code'.
     *
     * 
     *
     */
    String getCode();
    /**
     * Setter for the property 'number'.
     *
     * 
     *
     */
    void setNumber(String number);

    /**
     * Returns the property 'number'.
     *
     * 
     *
     */
    String getNumber();
    /**
     * Helper method to get reference of this object as model type.
     */
    Phone asPhone();
}
