package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Official;


/**
 * The domain object that represents 'Officials'.
 *
 * @author generator
 */
public interface OfficialEntityInterface {

    /**
     * Setter for the property 'official service grade type'.
     *
     * 
     *
     */
    void setOfficialServiceGradeType(de.smava.webapp.applicant.type.OfficialServiceGradeType officialServiceGradeType);

    /**
     * Returns the property 'official service grade type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.OfficialServiceGradeType getOfficialServiceGradeType();
    /**
     * Helper method to get reference of this object as model type.
     */
    Official asOfficial();
}
