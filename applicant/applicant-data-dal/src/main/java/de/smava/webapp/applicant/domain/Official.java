//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(official)}
import de.smava.webapp.applicant.domain.history.OfficialHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Officials'.
 *
 * 
 *
 * @author generator
 */
public class Official extends OfficialHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(official)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.OfficialServiceGradeType _officialServiceGradeType;
        
                            /**
     * Setter for the property 'official service grade type'.
     *
     * 
     *
     */
    public void setOfficialServiceGradeType(de.smava.webapp.applicant.type.OfficialServiceGradeType officialServiceGradeType) {
        if (!_officialServiceGradeTypeIsSet) {
            _officialServiceGradeTypeIsSet = true;
            _officialServiceGradeTypeInitVal = getOfficialServiceGradeType();
        }
        registerChange("official service grade type", _officialServiceGradeTypeInitVal, officialServiceGradeType);
        _officialServiceGradeType = officialServiceGradeType;
    }
                        
    /**
     * Returns the property 'official service grade type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.OfficialServiceGradeType getOfficialServiceGradeType() {
        return _officialServiceGradeType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Official.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _officialServiceGradeType=").append(_officialServiceGradeType);
            builder.append("\n}");
        } else {
            builder.append(Official.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Official asOfficial() {
        return this;
    }
}
