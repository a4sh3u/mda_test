package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Employer;
import de.smava.webapp.applicant.domain.Retiree;

import java.util.Date;


/**
 * The domain object that represents 'Retirees'.
 *
 * @author generator
 */
public interface RetireeEntityInterface {

    /**
     * Setter for the property 'rent insurance'.
     *
     * 
     *
     */
    void setRentInsurance(Employer rentInsurance);

    /**
     * Returns the property 'rent insurance'.
     *
     * 
     *
     */
    Employer getRentInsurance();
    /**
     * Setter for the property 'retirement start'.
     *
     * 
     *
     */
    void setRetirementStart(Date retirementStart);

    /**
     * Returns the property 'retirement start'.
     *
     * 
     *
     */
    Date getRetirementStart();
    /**
     * Setter for the property 'retiree type'.
     *
     * 
     *
     */
    void setRetireeType(de.smava.webapp.applicant.type.RetireeType retireeType);

    /**
     * Returns the property 'retiree type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.RetireeType getRetireeType();
    /**
     * Setter for the property 'pension type'.
     *
     * 
     *
     */
    void setPensionType(de.smava.webapp.applicant.type.PensionType pensionType);

    /**
     * Returns the property 'pension type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.PensionType getPensionType();
    /**
     * Helper method to get reference of this object as model type.
     */
    Retiree asRetiree();
}
