package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 15.06.15.
 */
public enum EstateType {

    /**
     * Kein Immobilienbesitz
     */
    NONE,

    /**
     * Eigentumswohnung
     */
    APARTMENT,

    /**
     * Einfamilienhaus
     */
    SINGLE_FAMILY,

    /**
     * Mehrfamilienhaus
     */
    MULTI_FAMILY,

    /**
     * Gewerbefläche
     */
    COMMERCIAL
}
