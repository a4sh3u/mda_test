//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(applicant)}
import de.smava.webapp.applicant.domain.history.ApplicantHistory;

import java.util.*;

import java.util.Set;import java.util.Set;import java.util.Set;import java.util.Set;import de.smava.webapp.applicant.domain.BankAccount;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Applicants'.
 *
 * 
 *
 * @author generator
 */
public class Applicant extends ApplicantHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(applicant)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected NationalityInfo _nationalityInfo;
        protected LivingAddress _homeAddress;
        protected Address _previousHomeAddress;
        protected Employment _previousEmployment;
        protected Household _household;
        protected Income _mainIncome;
        protected Set<Income> _additionalIncomes;
        protected Set<Expense> _expenses;
        protected Set<Phone> _phones;
        protected PersonalData _personalData;
        protected String _email;
        protected List<CreditScore> _creditScores;
        protected Set<ThirdPartyLoan> _thirdPartyLoans;
        protected Set<ExternalThirdPartyLoan> _externalThirdPartyLoans;
        protected Set<CustomerThirdPartyLoan> _customerThirdPartyLoans;
        protected BankAccount _bankAccount;
        protected String _personId;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                            
    /**
     * Setter for the property 'nationality info'.
     *
     * 
     *
     */
    public void setNationalityInfo(NationalityInfo nationalityInfo) {
        _nationalityInfo = nationalityInfo;
    }
            
    /**
     * Returns the property 'nationality info'.
     *
     * 
     *
     */
    public NationalityInfo getNationalityInfo() {
        return _nationalityInfo;
    }
                                            
    /**
     * Setter for the property 'home address'.
     *
     * 
     *
     */
    public void setHomeAddress(LivingAddress homeAddress) {
        _homeAddress = homeAddress;
    }
            
    /**
     * Returns the property 'home address'.
     *
     * 
     *
     */
    public LivingAddress getHomeAddress() {
        return _homeAddress;
    }
                                            
    /**
     * Setter for the property 'previous home address'.
     *
     * 
     *
     */
    public void setPreviousHomeAddress(Address previousHomeAddress) {
        _previousHomeAddress = previousHomeAddress;
    }
            
    /**
     * Returns the property 'previous home address'.
     *
     * 
     *
     */
    public Address getPreviousHomeAddress() {
        return _previousHomeAddress;
    }
                                            
    /**
     * Setter for the property 'previous employment'.
     *
     * 
     *
     */
    public void setPreviousEmployment(Employment previousEmployment) {
        _previousEmployment = previousEmployment;
    }
            
    /**
     * Returns the property 'previous employment'.
     *
     * 
     *
     */
    public Employment getPreviousEmployment() {
        return _previousEmployment;
    }
                                            
    /**
     * Setter for the property 'household'.
     *
     * 
     *
     */
    public void setHousehold(Household household) {
        _household = household;
    }
            
    /**
     * Returns the property 'household'.
     *
     * 
     *
     */
    public Household getHousehold() {
        return _household;
    }
                                            
    /**
     * Setter for the property 'main income'.
     *
     * 
     *
     */
    public void setMainIncome(Income mainIncome) {
        _mainIncome = mainIncome;
    }
            
    /**
     * Returns the property 'main income'.
     *
     * 
     *
     */
    public Income getMainIncome() {
        return _mainIncome;
    }
                                            
    /**
     * Setter for the property 'additional incomes'.
     *
     * 
     *
     */
    public void setAdditionalIncomes(Set<Income> additionalIncomes) {
        _additionalIncomes = additionalIncomes;
    }
            
    /**
     * Returns the property 'additional incomes'.
     *
     * 
     *
     */
    public Set<Income> getAdditionalIncomes() {
        return _additionalIncomes;
    }
                                            
    /**
     * Setter for the property 'expenses'.
     *
     * 
     *
     */
    public void setExpenses(Set<Expense> expenses) {
        _expenses = expenses;
    }
            
    /**
     * Returns the property 'expenses'.
     *
     * 
     *
     */
    public Set<Expense> getExpenses() {
        return _expenses;
    }
                                            
    /**
     * Setter for the property 'phones'.
     *
     * 
     *
     */
    public void setPhones(Set<Phone> phones) {
        _phones = phones;
    }
            
    /**
     * Returns the property 'phones'.
     *
     * 
     *
     */
    public Set<Phone> getPhones() {
        return _phones;
    }
                                            
    /**
     * Setter for the property 'personal data'.
     *
     * 
     *
     */
    public void setPersonalData(PersonalData personalData) {
        _personalData = personalData;
    }
            
    /**
     * Returns the property 'personal data'.
     *
     * 
     *
     */
    public PersonalData getPersonalData() {
        return _personalData;
    }
                                    /**
     * Setter for the property 'email'.
     *
     * Email of the applicant. For the main borrower this is equal to the account's email. Note that it cannot be unique since a borrower can be main borrower and second borrower (with 2 records in that case).
     *
     */
    public void setEmail(String email) {
        if (!_emailIsSet) {
            _emailIsSet = true;
            _emailInitVal = getEmail();
        }
        registerChange("email", _emailInitVal, email);
        _email = email;
    }
                        
    /**
     * Returns the property 'email'.
     *
     * Email of the applicant. For the main borrower this is equal to the account's email. Note that it cannot be unique since a borrower can be main borrower and second borrower (with 2 records in that case).
     *
     */
    public String getEmail() {
        return _email;
    }
                                            
    /**
     * Setter for the property 'credit scores'.
     *
     * 
     *
     */
    public void setCreditScores(List<CreditScore> creditScores) {
        _creditScores = creditScores;
    }
            
    /**
     * Returns the property 'credit scores'.
     *
     * 
     *
     */
    public List<CreditScore> getCreditScores() {
        return _creditScores;
    }
                                            
    /**
     * Setter for the property 'third party loans'.
     *
     * 
     *
     */
    public void setThirdPartyLoans(Set<ThirdPartyLoan> thirdPartyLoans) {
        _thirdPartyLoans = thirdPartyLoans;
    }
            
    /**
     * Returns the property 'third party loans'.
     *
     * 
     *
     */
    public Set<ThirdPartyLoan> getThirdPartyLoans() {
        return _thirdPartyLoans;
    }
                                            
    /**
     * Setter for the property 'external third party loans'.
     *
     * 
     *
     */
    public void setExternalThirdPartyLoans(Set<ExternalThirdPartyLoan> externalThirdPartyLoans) {
        _externalThirdPartyLoans = externalThirdPartyLoans;
    }
            
    /**
     * Returns the property 'external third party loans'.
     *
     * 
     *
     */
    public Set<ExternalThirdPartyLoan> getExternalThirdPartyLoans() {
        return _externalThirdPartyLoans;
    }
                                            
    /**
     * Setter for the property 'customer third party loans'.
     *
     * 
     *
     */
    public void setCustomerThirdPartyLoans(Set<CustomerThirdPartyLoan> customerThirdPartyLoans) {
        _customerThirdPartyLoans = customerThirdPartyLoans;
    }
            
    /**
     * Returns the property 'customer third party loans'.
     *
     * 
     *
     */
    public Set<CustomerThirdPartyLoan> getCustomerThirdPartyLoans() {
        return _customerThirdPartyLoans;
    }
                                            
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    public void setBankAccount(BankAccount bankAccount) {
        _bankAccount = bankAccount;
    }
            
    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    public BankAccount getBankAccount() {
        return _bankAccount;
    }

    public String getPersonId() {
        return _personId;
    }

    public void setPersonId(String personId) {
        if (!_personIdIsSet) {
            _personIdIsSet = true;
            _personIdInitVal = getPersonId();
        }
        registerChange("personId", _personIdInitVal, personId);
        _personId = personId;
    }

    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_nationalityInfo instanceof de.smava.webapp.commons.domain.Entity && !_nationalityInfo.getChangeSet().isEmpty()) {
             for (String element : _nationalityInfo.getChangeSet()) {
                 result.add("nationality info : " + element);
             }
         }

         if (_homeAddress instanceof de.smava.webapp.commons.domain.Entity && !_homeAddress.getChangeSet().isEmpty()) {
             for (String element : _homeAddress.getChangeSet()) {
                 result.add("home address : " + element);
             }
         }

         if (_previousHomeAddress instanceof de.smava.webapp.commons.domain.Entity && !_previousHomeAddress.getChangeSet().isEmpty()) {
             for (String element : _previousHomeAddress.getChangeSet()) {
                 result.add("previous home address : " + element);
             }
         }

         if (_previousEmployment instanceof de.smava.webapp.commons.domain.Entity && !_previousEmployment.getChangeSet().isEmpty()) {
             for (String element : _previousEmployment.getChangeSet()) {
                 result.add("previous employment : " + element);
             }
         }

         if (_household instanceof de.smava.webapp.commons.domain.Entity && !_household.getChangeSet().isEmpty()) {
             for (String element : _household.getChangeSet()) {
                 result.add("household : " + element);
             }
         }

         if (_mainIncome instanceof de.smava.webapp.commons.domain.Entity && !_mainIncome.getChangeSet().isEmpty()) {
             for (String element : _mainIncome.getChangeSet()) {
                 result.add("main income : " + element);
             }
         }

         if (_personalData instanceof de.smava.webapp.commons.domain.Entity && !_personalData.getChangeSet().isEmpty()) {
             for (String element : _personalData.getChangeSet()) {
                 result.add("personal data : " + element);
             }
         }

         if (_bankAccount instanceof de.smava.webapp.commons.domain.Entity && !_bankAccount.getChangeSet().isEmpty()) {
             for (String element : _bankAccount.getChangeSet()) {
                 result.add("bank account : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Applicant.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _email=").append(_email);
            builder.append("\n    _personId=").append(_personId);
            builder.append("\n}");
        } else {
            builder.append(Applicant.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Applicant asApplicant() {
        return this;
    }
}
