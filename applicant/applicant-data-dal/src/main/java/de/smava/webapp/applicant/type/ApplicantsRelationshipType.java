package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 02.03.15.
 */
public enum ApplicantsRelationshipType {

    MARRIED,
    LIFE_PARTNER,
    SINGLE,
    OTHER;
}
