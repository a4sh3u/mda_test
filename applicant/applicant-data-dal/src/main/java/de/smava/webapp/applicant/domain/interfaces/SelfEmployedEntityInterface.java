package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.SelfEmployed;
import de.smava.webapp.applicant.domain.SelfEmployedIncomeExpense;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'SelfEmployeds'.
 *
 * @author generator
 */
public interface SelfEmployedEntityInterface {

    /**
     * Setter for the property 'trade taxes required'.
     *
     * 
     *
     */
    void setTradeTaxesRequired(Boolean tradeTaxesRequired);

    /**
     * Returns the property 'trade taxes required'.
     *
     * 
     *
     */
    Boolean getTradeTaxesRequired();
    /**
     * Setter for the property 'church taxes required'.
     *
     * 
     *
     */
    void setChurchTaxesRequired(Boolean churchTaxesRequired);

    /**
     * Returns the property 'church taxes required'.
     *
     * 
     *
     */
    Boolean getChurchTaxesRequired();
    /**
     * Setter for the property 'start of business'.
     *
     * 
     *
     */
    void setStartOfBusiness(Date startOfBusiness);

    /**
     * Returns the property 'start of business'.
     *
     * 
     *
     */
    Date getStartOfBusiness();
    /**
     * Setter for the property 'yearly calculations'.
     *
     * 
     *
     */
    void setYearlyCalculations(Set<SelfEmployedIncomeExpense> yearlyCalculations);

    /**
     * Returns the property 'yearly calculations'.
     *
     * 
     *
     */
    Set<SelfEmployedIncomeExpense> getYearlyCalculations();
    /**
     * Setter for the property 'industry sector'.
     *
     * 
     *
     */
    void setIndustrySector(de.smava.webapp.applicant.type.IndustrySectorType industrySector);

    /**
     * Returns the property 'industry sector'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.IndustrySectorType getIndustrySector();
    /**
     * Setter for the property 'self employed type'.
     *
     * 
     *
     */
    void setSelfEmployedType(de.smava.webapp.applicant.type.SelfEmployedType selfEmployedType);

    /**
     * Returns the property 'self employed type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.SelfEmployedType getSelfEmployedType();
    /**
     * Helper method to get reference of this object as model type.
     */
    SelfEmployed asSelfEmployed();
}
