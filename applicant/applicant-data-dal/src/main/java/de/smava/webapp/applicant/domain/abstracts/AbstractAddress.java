//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(address)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.AddressEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Addresss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractAddress
    extends BrokerageEntity    implements AddressEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractAddress.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Address)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setStreet(((Address) oldEntity).getStreet());    
        this.setHouseNumber(((Address) oldEntity).getHouseNumber());    
        this.setZipCode(((Address) oldEntity).getZipCode());    
        this.setCity(((Address) oldEntity).getCity());    
        this.setCountry(((Address) oldEntity).getCountry());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Address)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getStreet(), ((Address) otherEntity).getStreet());    
        equals = equals && valuesAreEqual(this.getHouseNumber(), ((Address) otherEntity).getHouseNumber());    
        equals = equals && valuesAreEqual(this.getZipCode(), ((Address) otherEntity).getZipCode());    
        equals = equals && valuesAreEqual(this.getCity(), ((Address) otherEntity).getCity());    
        equals = equals && valuesAreEqual(this.getCountry(), ((Address) otherEntity).getCountry());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(address)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

