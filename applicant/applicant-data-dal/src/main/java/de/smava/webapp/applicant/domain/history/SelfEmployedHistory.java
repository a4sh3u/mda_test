package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSelfEmployed;



/**
 * The domain object that has all history aggregation related fields for 'SelfEmployeds'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SelfEmployedHistory extends AbstractSelfEmployed {

    protected transient Boolean _tradeTaxesRequiredInitVal;
    protected transient boolean _tradeTaxesRequiredIsSet;
    protected transient Boolean _churchTaxesRequiredInitVal;
    protected transient boolean _churchTaxesRequiredIsSet;
    protected transient java.util.Date _startOfBusinessInitVal;
    protected transient boolean _startOfBusinessIsSet;
    protected transient de.smava.webapp.applicant.type.IndustrySectorType _industrySectorInitVal;
    protected transient boolean _industrySectorIsSet;
    protected transient de.smava.webapp.applicant.type.SelfEmployedType _selfEmployedTypeInitVal;
    protected transient boolean _selfEmployedTypeIsSet;


	
    /**
     * Returns the initial value of the property 'trade taxes required'.
     */
    public Boolean tradeTaxesRequiredInitVal() {
        Boolean result;
        if (_tradeTaxesRequiredIsSet) {
            result = _tradeTaxesRequiredInitVal;
        } else {
            result = getTradeTaxesRequired();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'trade taxes required'.
     */
    public boolean tradeTaxesRequiredIsDirty() {
        return !valuesAreEqual(tradeTaxesRequiredInitVal(), getTradeTaxesRequired());
    }

    /**
     * Returns true if the setter method was called for the property 'trade taxes required'.
     */
    public boolean tradeTaxesRequiredIsSet() {
        return _tradeTaxesRequiredIsSet;
    }
	
    /**
     * Returns the initial value of the property 'church taxes required'.
     */
    public Boolean churchTaxesRequiredInitVal() {
        Boolean result;
        if (_churchTaxesRequiredIsSet) {
            result = _churchTaxesRequiredInitVal;
        } else {
            result = getChurchTaxesRequired();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'church taxes required'.
     */
    public boolean churchTaxesRequiredIsDirty() {
        return !valuesAreEqual(churchTaxesRequiredInitVal(), getChurchTaxesRequired());
    }

    /**
     * Returns true if the setter method was called for the property 'church taxes required'.
     */
    public boolean churchTaxesRequiredIsSet() {
        return _churchTaxesRequiredIsSet;
    }
	
    /**
     * Returns the initial value of the property 'start of business'.
     */
    public java.util.Date startOfBusinessInitVal() {
        java.util.Date result;
        if (_startOfBusinessIsSet) {
            result = _startOfBusinessInitVal;
        } else {
            result = getStartOfBusiness();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'start of business'.
     */
    public boolean startOfBusinessIsDirty() {
        return !valuesAreEqual(startOfBusinessInitVal(), getStartOfBusiness());
    }

    /**
     * Returns true if the setter method was called for the property 'start of business'.
     */
    public boolean startOfBusinessIsSet() {
        return _startOfBusinessIsSet;
    }
		
    /**
     * Returns the initial value of the property 'industry sector'.
     */
    public de.smava.webapp.applicant.type.IndustrySectorType industrySectorInitVal() {
        de.smava.webapp.applicant.type.IndustrySectorType result;
        if (_industrySectorIsSet) {
            result = _industrySectorInitVal;
        } else {
            result = getIndustrySector();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'industry sector'.
     */
    public boolean industrySectorIsDirty() {
        return !valuesAreEqual(industrySectorInitVal(), getIndustrySector());
    }

    /**
     * Returns true if the setter method was called for the property 'industry sector'.
     */
    public boolean industrySectorIsSet() {
        return _industrySectorIsSet;
    }
	
    /**
     * Returns the initial value of the property 'self employed type'.
     */
    public de.smava.webapp.applicant.type.SelfEmployedType selfEmployedTypeInitVal() {
        de.smava.webapp.applicant.type.SelfEmployedType result;
        if (_selfEmployedTypeIsSet) {
            result = _selfEmployedTypeInitVal;
        } else {
            result = getSelfEmployedType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'self employed type'.
     */
    public boolean selfEmployedTypeIsDirty() {
        return !valuesAreEqual(selfEmployedTypeInitVal(), getSelfEmployedType());
    }

    /**
     * Returns true if the setter method was called for the property 'self employed type'.
     */
    public boolean selfEmployedTypeIsSet() {
        return _selfEmployedTypeIsSet;
    }

}
