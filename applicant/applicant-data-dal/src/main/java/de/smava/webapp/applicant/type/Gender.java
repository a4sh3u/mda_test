package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 24.02.15.
 */
public enum Gender {
    MALE,
    FEMALE,
    UNKOWN;

    public static String getTitle(Gender gender) {
        switch (gender) {
            case MALE:
                return "Herr";
            case FEMALE:
                return "Frau";
            default:
                return "unknown";
        }
    }


    public String getTitle() {
        return getTitle(this);
    }
}
