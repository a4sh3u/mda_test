//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(address)}
import de.smava.webapp.applicant.domain.history.AddressHistory;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Addresss'.
 *
 * 
 *
 * @author generator
 */
public class Address extends AddressHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(address)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected String _street;
        protected String _houseNumber;
        protected String _zipCode;
        protected String _city;
        protected de.smava.webapp.applicant.type.Country _country;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'street'.
     *
     * 
     *
     */
    public void setStreet(String street) {
        if (!_streetIsSet) {
            _streetIsSet = true;
            _streetInitVal = getStreet();
        }
        registerChange("street", _streetInitVal, street);
        _street = street;
    }
                        
    /**
     * Returns the property 'street'.
     *
     * 
     *
     */
    public String getStreet() {
        return _street;
    }
                                    /**
     * Setter for the property 'house number'.
     *
     * 
     *
     */
    public void setHouseNumber(String houseNumber) {
        if (!_houseNumberIsSet) {
            _houseNumberIsSet = true;
            _houseNumberInitVal = getHouseNumber();
        }
        registerChange("house number", _houseNumberInitVal, houseNumber);
        _houseNumber = houseNumber;
    }
                        
    /**
     * Returns the property 'house number'.
     *
     * 
     *
     */
    public String getHouseNumber() {
        return _houseNumber;
    }
                                    /**
     * Setter for the property 'zip code'.
     *
     * 
     *
     */
    public void setZipCode(String zipCode) {
        if (!_zipCodeIsSet) {
            _zipCodeIsSet = true;
            _zipCodeInitVal = getZipCode();
        }
        registerChange("zip code", _zipCodeInitVal, zipCode);
        _zipCode = zipCode;
    }
                        
    /**
     * Returns the property 'zip code'.
     *
     * 
     *
     */
    public String getZipCode() {
        return _zipCode;
    }
                                    /**
     * Setter for the property 'city'.
     *
     * 
     *
     */
    public void setCity(String city) {
        if (!_cityIsSet) {
            _cityIsSet = true;
            _cityInitVal = getCity();
        }
        registerChange("city", _cityInitVal, city);
        _city = city;
    }
                        
    /**
     * Returns the property 'city'.
     *
     * 
     *
     */
    public String getCity() {
        return _city;
    }
                                    /**
     * Setter for the property 'country'.
     *
     * 
     *
     */
    public void setCountry(de.smava.webapp.applicant.type.Country country) {
        if (!_countryIsSet) {
            _countryIsSet = true;
            _countryInitVal = getCountry();
        }
        registerChange("country", _countryInitVal, country);
        _country = country;
    }
                        
    /**
     * Returns the property 'country'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.Country getCountry() {
        return _country;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Address.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _street=").append(_street);
            builder.append("\n    _houseNumber=").append(_houseNumber);
            builder.append("\n    _zipCode=").append(_zipCode);
            builder.append("\n    _city=").append(_city);
            builder.append("\n    _country=").append(_country);
            builder.append("\n}");
        } else {
            builder.append(Address.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Address asAddress() {
        return this;
    }
}
