package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 11.04.15.
 */
public enum ThirdPartyLoanType {

    /**
     * Ratenkredit
     */
    INSTALLMENT_CREDIT,

    /**
     * Konsumentenkredit
     */
    CONSUMER_CREDIT,

    /**
     * Rahmenkredit
     */
    GLOBAL_CREDIT,

    /**
     * Abrufkredit
     */
    CALL_CREDIT,

    /**
     * Dispositionskredit
     */
    OVERDRAFT_CREDIT,

    /**
     * Kreditkartenkredit
     */
    CREDITCARD_CREDIT,

    /**
     * Autokredit
     */
    CAR_CREDIT,

    /**
     * Arbeitgeberdarlehen
     */
    EMPLOYERS_LOAN,

    /**
     * Beamtendarlehen
     */
    OFFICIAL_LOAN,

    /**
     * Geschäftskredit
     */
    BUSINESS_LOAN,

    /**
     * 0% Finanzierung
     */
    ZERO_FINANCING,

    /**
     * Ballonfinanzierung
     */
    BALLOON_FINANCING,

    /**
     * Rest
     */
    OTHER
}
