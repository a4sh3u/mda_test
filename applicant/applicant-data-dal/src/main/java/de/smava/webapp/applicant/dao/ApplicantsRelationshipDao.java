package de.smava.webapp.applicant.dao;

import de.smava.webapp.applicant.domain.Applicant;
import de.smava.webapp.applicant.domain.ApplicantsRelationship;
import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import javax.transaction.Synchronization;
import java.util.Collection;

/**
 * DAO interface for the domain object 'ApplicantsRelationships'.
 */
public interface ApplicantsRelationshipDao extends BrokerageSchemaDao<ApplicantsRelationship> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Retrieves all 'ApplicantsRelationship' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<ApplicantsRelationship> getApplicantsRelationshipList();

    /**
     * Retrieves a page of 'ApplicantsRelationship' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<ApplicantsRelationship> getApplicantsRelationshipList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'ApplicantsRelationship' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<ApplicantsRelationship> getApplicantsRelationshipList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'ApplicantsRelationship' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<ApplicantsRelationship> getApplicantsRelationshipList(Pageable pageable, Sortable sortable);

    /**
     * Returns the number of 'ApplicantsRelationship' instances.
     */
    long getApplicantsRelationshipCount();

    /**
     * Return latest ApplicantsRelationship for accountId.
     *
     * @param accountId - applicant.Account.id
     * @return
     */
    ApplicantsRelationship findLatestApplicantsRelationship(long accountId);

    ApplicantsRelationship findByLoanApplicationId(long loanApplicationId);

    Collection<ApplicantsRelationship> findApplicantRelationshipsByAccount(final long accountId);

    /**
     * Return previous ApplicantsRelationship based on Applicant object.
     * It simply looks fol latest ApplicantsRelationship belongs to account where
     * applicant1 != applicant (which was send as attribute).
     *
     * @param applicant
     * @return
     */
    ApplicantsRelationship findPreviousApplicantsRelationshipByFirstApplicant(Applicant applicant);

}
