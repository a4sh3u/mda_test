//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(investment)}
import de.smava.webapp.applicant.domain.history.InvestmentHistory;

import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Investments'.
 *
 * investments are expenses that are payed every month.
 *
 * @author generator
 */
public class Investment extends InvestmentHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(investment)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.InvestmentType _investmentType;
        protected EstateFinancing _estateFinancing;
        
                            /**
     * Setter for the property 'investment type'.
     *
     * 
     *
     */
    public void setInvestmentType(de.smava.webapp.applicant.type.InvestmentType investmentType) {
        if (!_investmentTypeIsSet) {
            _investmentTypeIsSet = true;
            _investmentTypeInitVal = getInvestmentType();
        }
        registerChange("investment type", _investmentTypeInitVal, investmentType);
        _investmentType = investmentType;
    }
                        
    /**
     * Returns the property 'investment type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.InvestmentType getInvestmentType() {
        return _investmentType;
    }
                                            
    /**
     * Setter for the property 'estate financing'.
     *
     * 
     *
     */
    public void setEstateFinancing(EstateFinancing estateFinancing) {
        _estateFinancing = estateFinancing;
    }
            
    /**
     * Returns the property 'estate financing'.
     *
     * 
     *
     */
    public EstateFinancing getEstateFinancing() {
        return _estateFinancing;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_estateFinancing instanceof de.smava.webapp.commons.domain.Entity && !_estateFinancing.getChangeSet().isEmpty()) {
             for (String element : _estateFinancing.getChangeSet()) {
                 result.add("estate financing : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Investment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _investmentType=").append(_investmentType);
            builder.append("\n}");
        } else {
            builder.append(Investment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Investment asInvestment() {
        return this;
    }
}
