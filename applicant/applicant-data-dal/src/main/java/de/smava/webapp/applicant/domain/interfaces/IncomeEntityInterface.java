package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Income;


/**
 * The domain object that represents 'Incomes'.
 *
 * @author generator
 */
public interface IncomeEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(java.util.Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    java.util.Date getCreationDate();
    /**
     * Setter for the property 'income type '.
     *
     * 
     *
     */
    void setIncomeType(de.smava.webapp.applicant.type.IncomeType incomeType);

    /**
     * Returns the property 'income type '.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.IncomeType getIncomeType();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'monthly net income'.
     *
     * 
     *
     */
    void setMonthlyNetIncome(Double monthlyNetIncome);

    /**
     * Returns the property 'monthly net income'.
     *
     * 
     *
     */
    Double getMonthlyNetIncome();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(java.util.Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    java.util.Date getExpirationDate();
    /**
     * Setter for the property 'constant over three months'.
     *
     * 
     *
     */
    void setConstantOverThreeMonths(Boolean constantOverThreeMonths);

    /**
     * Returns the property 'constant over three months'.
     *
     * 
     *
     */
    Boolean getConstantOverThreeMonths();
    /**
     * Helper method to get reference of this object as model type.
     */
    Income asIncome();
}
