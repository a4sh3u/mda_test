package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.EstateFinancing;
import de.smava.webapp.applicant.domain.Investment;


/**
 * The domain object that represents 'Investments'.
 *
 * @author generator
 */
public interface InvestmentEntityInterface {

    /**
     * Setter for the property 'investment type'.
     *
     * 
     *
     */
    void setInvestmentType(de.smava.webapp.applicant.type.InvestmentType investmentType);

    /**
     * Returns the property 'investment type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.InvestmentType getInvestmentType();
    /**
     * Setter for the property 'estate financing'.
     *
     * 
     *
     */
    void setEstateFinancing(EstateFinancing estateFinancing);

    /**
     * Returns the property 'estate financing'.
     *
     * 
     *
     */
    EstateFinancing getEstateFinancing();
    /**
     * Helper method to get reference of this object as model type.
     */
    Investment asInvestment();
}
