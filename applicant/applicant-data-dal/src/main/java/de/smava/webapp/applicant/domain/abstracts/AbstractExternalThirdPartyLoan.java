//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(external third party loan)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.ExternalThirdPartyLoan;
import de.smava.webapp.applicant.domain.interfaces.ExternalThirdPartyLoanEntityInterface;
import de.smava.webapp.applicant.type.ThirdPartyLoanSource;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Months;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ExternalThirdPartyLoans'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractExternalThirdPartyLoan
    extends BrokerageEntity    implements ExternalThirdPartyLoanEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractExternalThirdPartyLoan.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof ExternalThirdPartyLoan)) {
            return;
        }
        
        this.setOriginalAmount(((ExternalThirdPartyLoan) oldEntity).getOriginalAmount());    
        this.setMonthlyRate(((ExternalThirdPartyLoan) oldEntity).getMonthlyRate());    
        this.setLoanStartDate(((ExternalThirdPartyLoan) oldEntity).getLoanStartDate());    
        this.setLoanEndDate(((ExternalThirdPartyLoan) oldEntity).getLoanEndDate());    
        this.setLoanId(((ExternalThirdPartyLoan) oldEntity).getLoanId());    
        this.setEffectiveInterest(((ExternalThirdPartyLoan) oldEntity).getEffectiveInterest());    
        this.setLoanType(((ExternalThirdPartyLoan) oldEntity).getLoanType());    
        this.setRedemptionSum(((ExternalThirdPartyLoan) oldEntity).getRedemptionSum());    
        this.setBankAccount(((ExternalThirdPartyLoan) oldEntity).getBankAccount());    
        this.setDrawingLimit(((ExternalThirdPartyLoan) oldEntity).getDrawingLimit());    
        this.setSameCreditBankAccount(((ExternalThirdPartyLoan) oldEntity).getSameCreditBankAccount());    
        this.setSchufaTokenCode(((ExternalThirdPartyLoan) oldEntity).getSchufaTokenCode());    
        this.setLoanSource(((ExternalThirdPartyLoan) oldEntity).getLoanSource());    
        
    }



    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof ExternalThirdPartyLoan)) {
            equals = false;
        }

        equals = equals && valuesAreEqual(this.getOriginalAmount(), ((ExternalThirdPartyLoan) otherEntity).getOriginalAmount());
        equals = equals && valuesAreEqual(this.getMonthlyRate(), ((ExternalThirdPartyLoan) otherEntity).getMonthlyRate());
        equals = equals && valuesAreEqual(this.getLoanStartDate(), ((ExternalThirdPartyLoan) otherEntity).getLoanStartDate());
        equals = equals && valuesAreEqual(this.getLoanEndDate(), ((ExternalThirdPartyLoan) otherEntity).getLoanEndDate());
        equals = equals && valuesAreEqual(this.getLoanId(), ((ExternalThirdPartyLoan) otherEntity).getLoanId());
        equals = equals && valuesAreEqual(this.getEffectiveInterest(), ((ExternalThirdPartyLoan) otherEntity).getEffectiveInterest());
        equals = equals && valuesAreEqual(this.getLoanType(), ((ExternalThirdPartyLoan) otherEntity).getLoanType());
        equals = equals && valuesAreEqual(this.getRedemptionSum(), ((ExternalThirdPartyLoan) otherEntity).getRedemptionSum());
        equals = equals && valuesAreEqual(this.getBankAccount(), ((ExternalThirdPartyLoan) otherEntity).getBankAccount());
        equals = equals && valuesAreEqual(this.getDrawingLimit(), ((ExternalThirdPartyLoan) otherEntity).getDrawingLimit());
        equals = equals && valuesAreEqual(this.getSameCreditBankAccount(), ((ExternalThirdPartyLoan) otherEntity).getSameCreditBankAccount());
        equals = equals && valuesAreEqual(this.getSchufaTokenCode(), ((ExternalThirdPartyLoan) otherEntity).getSchufaTokenCode());
        equals = equals && valuesAreEqual(this.getLoanSource(), ((ExternalThirdPartyLoan) otherEntity).getLoanSource());

        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(external third party loan)}


    public Integer getMonthsRemaining() {
        Integer monthsRemaining = null;

        if (this.getLoanEndDate() != null) {
            monthsRemaining = Months.monthsBetween(new DateTime(CurrentDate.getDate()), new DateTime(this.getLoanEndDate())).getMonths();
        }

        return monthsRemaining;
    }

    public Double getLoanAmount() {
        Double originalAmount = this.getOriginalAmount();
        if ((originalAmount != null) && (Math.abs(originalAmount) >= 0.01)) {
            return originalAmount;
        } else {
            return this.getDrawingLimit();
        }
    }

    public boolean isRepresentingSameShufaToken(AbstractExternalThirdPartyLoan other) {
        boolean equals = true;

        if (other == null) {
            equals = false;
        }

        equals = equals && ThirdPartyLoanSource.SCHUFA.equals(this.getLoanSource());
        equals = equals && valuesAreEqual(this.getSchufaTokenCode(), other.getSchufaTokenCode());

        equals = equals && valuesAreEqual(this.getOriginalAmount(), other.getOriginalAmount());
        equals = equals && valuesAreEqual(this.getMonthlyRate(), other.getMonthlyRate());
        equals = equals && valuesAreEqual(this.getLoanStartDate(), other.getLoanStartDate());
        equals = equals && valuesAreEqual(this.getLoanEndDate(), other.getLoanEndDate());
        equals = equals && valuesAreEqual(this.getDrawingLimit(), other.getDrawingLimit());

        return equals;
    }
    // !!!!!!!! End of insert code section !!!!!!!!
}

