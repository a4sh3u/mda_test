//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(customer third party loan)}

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.CustomerThirdPartyLoan;
import de.smava.webapp.applicant.domain.interfaces.CustomerThirdPartyLoanEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Months;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'CustomerThirdPartyLoans'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractCustomerThirdPartyLoan
    extends BrokerageEntity    implements CustomerThirdPartyLoanEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCustomerThirdPartyLoan.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof CustomerThirdPartyLoan)) {
            return;
        }
        
        this.setOriginalAmount(((CustomerThirdPartyLoan) oldEntity).getOriginalAmount());    
        this.setMonthlyRate(((CustomerThirdPartyLoan) oldEntity).getMonthlyRate());    
        this.setLoanStartDate(((CustomerThirdPartyLoan) oldEntity).getLoanStartDate());    
        this.setLoanEndDate(((CustomerThirdPartyLoan) oldEntity).getLoanEndDate());    
        this.setLoanId(((CustomerThirdPartyLoan) oldEntity).getLoanId());    
        this.setEffectiveInterest(((CustomerThirdPartyLoan) oldEntity).getEffectiveInterest());    
        this.setConsolidationWish(((CustomerThirdPartyLoan) oldEntity).getConsolidationWish());    
        this.setLoanType(((CustomerThirdPartyLoan) oldEntity).getLoanType());    
        this.setRedemptionSum(((CustomerThirdPartyLoan) oldEntity).getRedemptionSum());    
        this.setBankAccount(((CustomerThirdPartyLoan) oldEntity).getBankAccount());    
        this.setDrawingLimit(((CustomerThirdPartyLoan) oldEntity).getDrawingLimit());    
        this.setSameCreditBankAccount(((CustomerThirdPartyLoan) oldEntity).getSameCreditBankAccount());    
        this.setLoanSource(((CustomerThirdPartyLoan) oldEntity).getLoanSource());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof CustomerThirdPartyLoan)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getOriginalAmount(), ((CustomerThirdPartyLoan) otherEntity).getOriginalAmount());    
        equals = equals && valuesAreEqual(this.getMonthlyRate(), ((CustomerThirdPartyLoan) otherEntity).getMonthlyRate());    
        equals = equals && valuesAreEqual(this.getLoanStartDate(), ((CustomerThirdPartyLoan) otherEntity).getLoanStartDate());    
        equals = equals && valuesAreEqual(this.getLoanEndDate(), ((CustomerThirdPartyLoan) otherEntity).getLoanEndDate());    
        equals = equals && valuesAreEqual(this.getLoanId(), ((CustomerThirdPartyLoan) otherEntity).getLoanId());    
        equals = equals && valuesAreEqual(this.getEffectiveInterest(), ((CustomerThirdPartyLoan) otherEntity).getEffectiveInterest());    
        equals = equals && valuesAreEqual(this.getConsolidationWish(), ((CustomerThirdPartyLoan) otherEntity).getConsolidationWish());    
        equals = equals && valuesAreEqual(this.getLoanType(), ((CustomerThirdPartyLoan) otherEntity).getLoanType());    
        equals = equals && valuesAreEqual(this.getRedemptionSum(), ((CustomerThirdPartyLoan) otherEntity).getRedemptionSum());    
        equals = equals && valuesAreEqual(this.getBankAccount(), ((CustomerThirdPartyLoan) otherEntity).getBankAccount());    
        equals = equals && valuesAreEqual(this.getDrawingLimit(), ((CustomerThirdPartyLoan) otherEntity).getDrawingLimit());    
        equals = equals && valuesAreEqual(this.getSameCreditBankAccount(), ((CustomerThirdPartyLoan) otherEntity).getSameCreditBankAccount());    
        equals = equals && valuesAreEqual(this.getLoanSource(), ((CustomerThirdPartyLoan) otherEntity).getLoanSource());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														+{|custom implementations|bean(customer third party loan)}


    private static final double TWELVE_MONTHS = 1200;

    public Double getRemainingDebt() {

        if (this.getMonthsRemaining() != null && this.getMonthlyRate() != null) {
            double adjustedInterest = (getEffectiveInterest() == null) || (getEffectiveInterest() == 0) ? .1 : getEffectiveInterest();
            double monthlyInterest = adjustedInterest / TWELVE_MONTHS;

            double remainingDebt = getMonthlyRate() / (monthlyInterest * Math.pow(1 + monthlyInterest, getMonthsRemaining()) / (Math.pow(1 + monthlyInterest, getMonthsRemaining()) - 1));
            if (this.getRedemptionSum() != null) {
                remainingDebt += this.getRedemptionSum();
            }
            return remainingDebt;

        }

        return null;
    }


    public Integer getMonthsRemaining() {
        Integer monthsRemaining = null;

        if (this.getLoanEndDate() != null) {
            monthsRemaining = Months.monthsBetween(new DateTime(CurrentDate.getDate()), new DateTime(this.getLoanEndDate())).getMonths();
        }

        return monthsRemaining;
    }

    public Double getLoanAmount() {
        Double originalAmount = this.getOriginalAmount();
        if ((originalAmount != null) && (Math.abs(originalAmount) >= 0.01)) {
            return originalAmount;
        } else {
            return this.getDrawingLimit();
        }
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

