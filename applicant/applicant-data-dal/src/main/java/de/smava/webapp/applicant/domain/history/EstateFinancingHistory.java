package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractEstateFinancing;




/**
 * The domain object that has all history aggregation related fields for 'EstateFinancings'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EstateFinancingHistory extends AbstractEstateFinancing {

    protected transient Double _monthlyFinancingAmountInitVal;
    protected transient boolean _monthlyFinancingAmountIsSet;


	
    /**
     * Returns the initial value of the property 'monthly financing amount'.
     */
    public Double monthlyFinancingAmountInitVal() {
        Double result;
        if (_monthlyFinancingAmountIsSet) {
            result = _monthlyFinancingAmountInitVal;
        } else {
            result = getMonthlyFinancingAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'monthly financing amount'.
     */
    public boolean monthlyFinancingAmountIsDirty() {
        return !valuesAreEqual(monthlyFinancingAmountInitVal(), getMonthlyFinancingAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'monthly financing amount'.
     */
    public boolean monthlyFinancingAmountIsSet() {
        return _monthlyFinancingAmountIsSet;
    }
	
}
