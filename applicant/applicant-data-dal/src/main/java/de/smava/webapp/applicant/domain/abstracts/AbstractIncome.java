//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(income)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.Income;
import de.smava.webapp.applicant.domain.interfaces.IncomeEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Incomes'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractIncome
    extends BrokerageEntity implements IncomeEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractIncome.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Income)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setIncomeType(((Income) oldEntity).getIncomeType());    
        this.setDescription(((Income) oldEntity).getDescription());    
        this.setMonthlyNetIncome(((Income) oldEntity).getMonthlyNetIncome());    
            
        this.setExpirationDate(((Income) oldEntity).getExpirationDate());    
        this.setConstantOverThreeMonths(((Income) oldEntity).getConstantOverThreeMonths());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Income)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getIncomeType(), ((Income) otherEntity).getIncomeType());    
        equals = equals && valuesAreEqual(this.getDescription(), ((Income) otherEntity).getDescription());    
        equals = equals && valuesAreEqual(this.getMonthlyNetIncome(), ((Income) otherEntity).getMonthlyNetIncome());    
            
        equals = equals && valuesAreEqual(this.getExpirationDate(), ((Income) otherEntity).getExpirationDate());    
        equals = equals && valuesAreEqual(this.getConstantOverThreeMonths(), ((Income) otherEntity).getConstantOverThreeMonths());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(income)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

