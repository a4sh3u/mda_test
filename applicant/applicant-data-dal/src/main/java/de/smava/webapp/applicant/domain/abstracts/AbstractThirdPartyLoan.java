//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(third party loan)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.ThirdPartyLoan;
import de.smava.webapp.applicant.domain.interfaces.ThirdPartyLoanEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Months;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ThirdPartyLoans'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractThirdPartyLoan
    extends BrokerageEntity    implements ThirdPartyLoanEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractThirdPartyLoan.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof ThirdPartyLoan)) {
            return;
        }
        
        this.setOriginalAmount(((ThirdPartyLoan) oldEntity).getOriginalAmount());    
        this.setMonthlyRate(((ThirdPartyLoan) oldEntity).getMonthlyRate());    
        this.setLoanStartDate(((ThirdPartyLoan) oldEntity).getLoanStartDate());    
        this.setLoanEndDate(((ThirdPartyLoan) oldEntity).getLoanEndDate());    
        this.setLoanId(((ThirdPartyLoan) oldEntity).getLoanId());    
        this.setEffectiveInterest(((ThirdPartyLoan) oldEntity).getEffectiveInterest());    
        this.setConsolidationWish(((ThirdPartyLoan) oldEntity).getConsolidationWish());    
        this.setLoanType(((ThirdPartyLoan) oldEntity).getLoanType());    
        this.setRedemptionSum(((ThirdPartyLoan) oldEntity).getRedemptionSum());    
        this.setBankAccount(((ThirdPartyLoan) oldEntity).getBankAccount());    
        this.setDrawingLimit(((ThirdPartyLoan) oldEntity).getDrawingLimit());    
        this.setSameCreditBankAccount(((ThirdPartyLoan) oldEntity).getSameCreditBankAccount());    
        this.setLoanSource(((ThirdPartyLoan) oldEntity).getLoanSource());    
        this.setCustomerThirdPartyLoan(((ThirdPartyLoan) oldEntity).getCustomerThirdPartyLoan());    
        this.setExternalThirdPartyLoan(((ThirdPartyLoan) oldEntity).getExternalThirdPartyLoan());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof ThirdPartyLoan)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getOriginalAmount(), ((ThirdPartyLoan) otherEntity).getOriginalAmount());    
        equals = equals && valuesAreEqual(this.getMonthlyRate(), ((ThirdPartyLoan) otherEntity).getMonthlyRate());    
        equals = equals && valuesAreEqual(this.getLoanStartDate(), ((ThirdPartyLoan) otherEntity).getLoanStartDate());    
        equals = equals && valuesAreEqual(this.getLoanEndDate(), ((ThirdPartyLoan) otherEntity).getLoanEndDate());    
        equals = equals && valuesAreEqual(this.getLoanId(), ((ThirdPartyLoan) otherEntity).getLoanId());    
        equals = equals && valuesAreEqual(this.getEffectiveInterest(), ((ThirdPartyLoan) otherEntity).getEffectiveInterest());    
        equals = equals && valuesAreEqual(this.getConsolidationWish(), ((ThirdPartyLoan) otherEntity).getConsolidationWish());    
        equals = equals && valuesAreEqual(this.getLoanType(), ((ThirdPartyLoan) otherEntity).getLoanType());    
        equals = equals && valuesAreEqual(this.getRedemptionSum(), ((ThirdPartyLoan) otherEntity).getRedemptionSum());    
        equals = equals && valuesAreEqual(this.getBankAccount(), ((ThirdPartyLoan) otherEntity).getBankAccount());    
        equals = equals && valuesAreEqual(this.getDrawingLimit(), ((ThirdPartyLoan) otherEntity).getDrawingLimit());    
        equals = equals && valuesAreEqual(this.getSameCreditBankAccount(), ((ThirdPartyLoan) otherEntity).getSameCreditBankAccount());    
        equals = equals && valuesAreEqual(this.getLoanSource(), ((ThirdPartyLoan) otherEntity).getLoanSource());    
        equals = equals && valuesAreEqual(this.getCustomerThirdPartyLoan(), ((ThirdPartyLoan) otherEntity).getCustomerThirdPartyLoan());    
        equals = equals && valuesAreEqual(this.getExternalThirdPartyLoan(), ((ThirdPartyLoan) otherEntity).getExternalThirdPartyLoan());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(third party loan)}


    private static final double TWELVE_MONTHS = 1200;

    public Double getRemainingDebt() {

        if (this.getMonthsRemaining() != null && this.getMonthlyRate() != null) {
            double adjustedInterest = (getEffectiveInterest() == null) || (getEffectiveInterest() == 0) ? 8.0 : getEffectiveInterest();
            double monthlyInterest = adjustedInterest / TWELVE_MONTHS;

            double remainingDebt = getMonthlyRate() / (monthlyInterest * Math.pow(1 + monthlyInterest, getMonthsRemaining()) / (Math.pow(1 + monthlyInterest, getMonthsRemaining()) - 1));
            if (this.getRedemptionSum() != null) {
                remainingDebt += this.getRedemptionSum();
            }

            if ( getOriginalAmount() != null) {
                return Math.min(remainingDebt, getOriginalAmount());
            } else {
                return remainingDebt;
            }

        }

        return null;
    }


    public Integer getMonthsRemaining() {
        Integer monthsRemaining = null;

        if (this.getLoanEndDate() != null) {
            monthsRemaining = Months.monthsBetween(new DateTime(CurrentDate.getDate()), new DateTime(this.getLoanEndDate())).getMonths();
        }

        return monthsRemaining;
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

