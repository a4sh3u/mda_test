package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractExpense;




/**
 * The domain object that has all history aggregation related fields for 'Expenses'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ExpenseHistory extends AbstractExpense {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient de.smava.webapp.applicant.type.ExpenseType _expenseTypeInitVal;
    protected transient boolean _expenseTypeIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient Double _monthlyAmountInitVal;
    protected transient boolean _monthlyAmountIsSet;
    protected transient java.util.Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expense type'.
     */
    public de.smava.webapp.applicant.type.ExpenseType expenseTypeInitVal() {
        de.smava.webapp.applicant.type.ExpenseType result;
        if (_expenseTypeIsSet) {
            result = _expenseTypeInitVal;
        } else {
            result = getExpenseType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expense type'.
     */
    public boolean expenseTypeIsDirty() {
        return !valuesAreEqual(expenseTypeInitVal(), getExpenseType());
    }

    /**
     * Returns true if the setter method was called for the property 'expense type'.
     */
    public boolean expenseTypeIsSet() {
        return _expenseTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'monthly amount'.
     */
    public Double monthlyAmountInitVal() {
        Double result;
        if (_monthlyAmountIsSet) {
            result = _monthlyAmountInitVal;
        } else {
            result = getMonthlyAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'monthly amount'.
     */
    public boolean monthlyAmountIsDirty() {
        return !valuesAreEqual(monthlyAmountInitVal(), getMonthlyAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'monthly amount'.
     */
    public boolean monthlyAmountIsSet() {
        return _monthlyAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public java.util.Date expirationDateInitVal() {
        java.util.Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }

}
