//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(self employed income expense)}
import de.smava.webapp.applicant.domain.history.SelfEmployedIncomeExpenseHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SelfEmployedIncomeExpenses'.
 *
 * 
 *
 * @author generator
 */
public class SelfEmployedIncomeExpense extends SelfEmployedIncomeExpenseHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(self employed income expense)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected int _year;
        protected Double _annualGrossTurnover;
        protected Double _annualExpenses;
        protected SelfEmployed _selfEmployed;
        
                            /**
     * Setter for the property 'year'.
     *
     * 
     *
     */
    public void setYear(int year) {
        if (!_yearIsSet) {
            _yearIsSet = true;
            _yearInitVal = getYear();
        }
        registerChange("year", _yearInitVal, year);
        _year = year;
    }
                        
    /**
     * Returns the property 'year'.
     *
     * 
     *
     */
    public int getYear() {
        return _year;
    }
                                    /**
     * Setter for the property 'annual gross turnover'.
     *
     * 
     *
     */
    public void setAnnualGrossTurnover(Double annualGrossTurnover) {
        if (!_annualGrossTurnoverIsSet) {
            _annualGrossTurnoverIsSet = true;
            _annualGrossTurnoverInitVal = getAnnualGrossTurnover();
        }
        registerChange("annual gross turnover", _annualGrossTurnoverInitVal, annualGrossTurnover);
        _annualGrossTurnover = annualGrossTurnover;
    }
                        
    /**
     * Returns the property 'annual gross turnover'.
     *
     * 
     *
     */
    public Double getAnnualGrossTurnover() {
        return _annualGrossTurnover;
    }
                                    /**
     * Setter for the property 'annual expenses'.
     *
     * 
     *
     */
    public void setAnnualExpenses(Double annualExpenses) {
        if (!_annualExpensesIsSet) {
            _annualExpensesIsSet = true;
            _annualExpensesInitVal = getAnnualExpenses();
        }
        registerChange("annual expenses", _annualExpensesInitVal, annualExpenses);
        _annualExpenses = annualExpenses;
    }
                        
    /**
     * Returns the property 'annual expenses'.
     *
     * 
     *
     */
    public Double getAnnualExpenses() {
        return _annualExpenses;
    }
                                            
    /**
     * Setter for the property 'self employed'.
     *
     * 
     *
     */
    public void setSelfEmployed(SelfEmployed selfEmployed) {
        _selfEmployed = selfEmployed;
    }
            
    /**
     * Returns the property 'self employed'.
     *
     * 
     *
     */
    public SelfEmployed getSelfEmployed() {
        return _selfEmployed;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_selfEmployed instanceof de.smava.webapp.commons.domain.Entity && !_selfEmployed.getChangeSet().isEmpty()) {
             for (String element : _selfEmployed.getChangeSet()) {
                 result.add("self employed : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SelfEmployedIncomeExpense.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _year=").append(_year);
            builder.append("\n}");
        } else {
            builder.append(SelfEmployedIncomeExpense.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SelfEmployedIncomeExpense asSelfEmployedIncomeExpense() {
        return this;
    }
}
