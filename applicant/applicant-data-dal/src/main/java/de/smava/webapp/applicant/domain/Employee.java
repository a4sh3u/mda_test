//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(employee)}
import de.smava.webapp.applicant.domain.history.EmployeeHistory;
import de.smava.webapp.applicant.type.EmployeeType;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Employees'.
 *
 * 
 *
 * @author generator
 */
public class Employee extends EmployeeHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(employee)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.EmployeeType _employeeType;
        
                            /**
     * Setter for the property 'employee type'.
     *
     * 
     *
     */
    public void setEmployeeType(de.smava.webapp.applicant.type.EmployeeType employeeType) {
        if (!_employeeTypeIsSet) {
            _employeeTypeIsSet = true;
            _employeeTypeInitVal = getEmployeeType();
        }
        registerChange("employee type", _employeeTypeInitVal, employeeType);
        _employeeType = employeeType;
    }
                        
    /**
     * Returns the property 'employee type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.EmployeeType getEmployeeType() {
        return _employeeType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Employee.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _employeeType=").append(_employeeType);
            builder.append("\n}");
        } else {
            builder.append(Employee.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Employee asEmployee() {
        return this;
    }
}
