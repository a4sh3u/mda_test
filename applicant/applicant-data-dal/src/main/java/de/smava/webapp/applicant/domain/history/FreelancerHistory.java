package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractFreelancer;




/**
 * The domain object that has all history aggregation related fields for 'Freelancers'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class FreelancerHistory extends AbstractFreelancer {

    protected transient de.smava.webapp.applicant.type.FreelancerOccupationType _occupationTypeInitVal;
    protected transient boolean _occupationTypeIsSet;


	
    /**
     * Returns the initial value of the property 'occupationType'.
     */
    public de.smava.webapp.applicant.type.FreelancerOccupationType occupationTypeInitVal() {
        de.smava.webapp.applicant.type.FreelancerOccupationType result;
        if (_occupationTypeIsSet) {
            result = _occupationTypeInitVal;
        } else {
            result = getOccupationType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'occupationType'.
     */
    public boolean occupationTypeIsDirty() {
        return !valuesAreEqual(occupationTypeInitVal(), getOccupationType());
    }

    /**
     * Returns true if the setter method was called for the property 'occupationType'.
     */
    public boolean occupationTypeIsSet() {
        return _occupationTypeIsSet;
    }

}
