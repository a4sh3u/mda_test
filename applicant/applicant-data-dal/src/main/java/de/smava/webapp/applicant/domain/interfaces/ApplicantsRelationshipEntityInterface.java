package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Applicant;
import de.smava.webapp.applicant.domain.ApplicantsRelationship;


/**
 * The domain object that represents 'ApplicantsRelationships'.
 *
 * @author generator
 */
public interface ApplicantsRelationshipEntityInterface {

    /**
     * Setter for the property 'applicant1'.
     *
     * 
     *
     */
    void setApplicant1(Applicant applicant1);

    /**
     * Returns the property 'applicant1'.
     *
     * 
     *
     */
    Applicant getApplicant1();
    /**
     * Setter for the property 'applicant2'.
     *
     * 
     *
     */
    void setApplicant2(Applicant applicant2);

    /**
     * Returns the property 'applicant2'.
     *
     * 
     *
     */
    Applicant getApplicant2();
    /**
     * Setter for the property 'relationshipType'.
     *
     * 
     *
     */
    void setRelationshipType(de.smava.webapp.applicant.type.ApplicantsRelationshipType relationshipType);

    /**
     * Returns the property 'relationshipType'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.ApplicantsRelationshipType getRelationshipType();
    /**
     * Setter for the property 'shared household'.
     *
     * 
     *
     */
    void setSharedHousehold(Boolean sharedHousehold);

    /**
     * Returns the property 'shared household'.
     *
     * 
     *
     */
    Boolean getSharedHousehold();
    /**
     * Helper method to get reference of this object as model type.
     */
    ApplicantsRelationship asApplicantsRelationship();
}
