//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(phone)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.Phone;
import de.smava.webapp.applicant.domain.interfaces.PhoneEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                    
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Phones'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractPhone
    extends BrokerageEntity implements PhoneEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractPhone.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Phone)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setPhoneType(((Phone) oldEntity).getPhoneType());    
        this.setCode(((Phone) oldEntity).getCode());    
        this.setNumber(((Phone) oldEntity).getNumber());    
            
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Phone)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getPhoneType(), ((Phone) otherEntity).getPhoneType());    
        equals = equals && valuesAreEqual(this.getCode(), ((Phone) otherEntity).getCode());    
        equals = equals && valuesAreEqual(this.getNumber(), ((Phone) otherEntity).getNumber());    
            
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(phone)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

