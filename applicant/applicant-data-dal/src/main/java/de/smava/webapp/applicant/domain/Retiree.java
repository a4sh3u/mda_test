//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(retiree)}
import de.smava.webapp.applicant.domain.history.RetireeHistory;
import de.smava.webapp.applicant.type.RetireeType;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Retirees'.
 *
 * 
 *
 * @author generator
 */
public class Retiree extends RetireeHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(retiree)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Employer _rentInsurance;
        protected java.util.Date _retirementStart;
        protected de.smava.webapp.applicant.type.RetireeType _retireeType;
        protected de.smava.webapp.applicant.type.PensionType _pensionType;
        
                                    
    /**
     * Setter for the property 'rent insurance'.
     *
     * 
     *
     */
    public void setRentInsurance(Employer rentInsurance) {
        _rentInsurance = rentInsurance;
    }
            
    /**
     * Returns the property 'rent insurance'.
     *
     * 
     *
     */
    public Employer getRentInsurance() {
        return _rentInsurance;
    }
                                    /**
     * Setter for the property 'retirement start'.
     *
     * 
     *
     */
    public void setRetirementStart(java.util.Date retirementStart) {
        if (!_retirementStartIsSet) {
            _retirementStartIsSet = true;
            _retirementStartInitVal = getRetirementStart();
        }
        registerChange("retirement start", _retirementStartInitVal, retirementStart);
        _retirementStart = retirementStart;
    }
                        
    /**
     * Returns the property 'retirement start'.
     *
     * 
     *
     */
    public java.util.Date getRetirementStart() {
        return _retirementStart;
    }
                                    /**
     * Setter for the property 'retiree type'.
     *
     * 
     *
     */
    public void setRetireeType(de.smava.webapp.applicant.type.RetireeType retireeType) {
        if (!_retireeTypeIsSet) {
            _retireeTypeIsSet = true;
            _retireeTypeInitVal = getRetireeType();
        }
        registerChange("retiree type", _retireeTypeInitVal, retireeType);
        _retireeType = retireeType;
    }
                        
    /**
     * Returns the property 'retiree type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.RetireeType getRetireeType() {
        return _retireeType;
    }
                                    /**
     * Setter for the property 'pension type'.
     *
     * 
     *
     */
    public void setPensionType(de.smava.webapp.applicant.type.PensionType pensionType) {
        if (!_pensionTypeIsSet) {
            _pensionTypeIsSet = true;
            _pensionTypeInitVal = getPensionType();
        }
        registerChange("pension type", _pensionTypeInitVal, pensionType);
        _pensionType = pensionType;
    }
                        
    /**
     * Returns the property 'pension type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.PensionType getPensionType() {
        return _pensionType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_rentInsurance instanceof de.smava.webapp.commons.domain.Entity && !_rentInsurance.getChangeSet().isEmpty()) {
             for (String element : _rentInsurance.getChangeSet()) {
                 result.add("rent insurance : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Retiree.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _retirementStart=").append(_retirementStart);
            builder.append("\n    _retireeType=").append(_retireeType);
            builder.append("\n    _pensionType=").append(_pensionType);
            builder.append("\n}");
        } else {
            builder.append(Retiree.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Retiree asRetiree() {
        return this;
    }
}
