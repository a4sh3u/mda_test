package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 15.06.15.
 */
public enum InvestmentType {
    /**
     * Bauspardarlehen
     */
    BUILDING_SOCIETY_LOAN,

    /**
     * Private Altersvorsorge
     */
    PRIVATE_PENSION_PLAN,

    /**
     * Kapital Lebensversicherung
     */
    LIFE_CAPITAL_INSURANCE
}
