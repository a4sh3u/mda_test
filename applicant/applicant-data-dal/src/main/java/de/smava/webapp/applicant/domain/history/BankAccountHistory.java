package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractBankAccount;





/**
 * The domain object that has all history aggregation related fields for 'BankAccounts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class BankAccountHistory extends AbstractBankAccount {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _bankNameInitVal;
    protected transient boolean _bankNameIsSet;
    protected transient String _ibanInitVal;
    protected transient boolean _ibanIsSet;
    protected transient String _bicInitVal;
    protected transient boolean _bicIsSet;
    protected transient String _accountNumberInitVal;
    protected transient boolean _accountNumberIsSet;
    protected transient String _bankCodeInitVal;
    protected transient boolean _bankCodeIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank name'.
     */
    public String bankNameInitVal() {
        String result;
        if (_bankNameIsSet) {
            result = _bankNameInitVal;
        } else {
            result = getBankName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank name'.
     */
    public boolean bankNameIsDirty() {
        return !valuesAreEqual(bankNameInitVal(), getBankName());
    }

    /**
     * Returns true if the setter method was called for the property 'bank name'.
     */
    public boolean bankNameIsSet() {
        return _bankNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'iban'.
     */
    public String ibanInitVal() {
        String result;
        if (_ibanIsSet) {
            result = _ibanInitVal;
        } else {
            result = getIban();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'iban'.
     */
    public boolean ibanIsDirty() {
        return !valuesAreEqual(ibanInitVal(), getIban());
    }

    /**
     * Returns true if the setter method was called for the property 'iban'.
     */
    public boolean ibanIsSet() {
        return _ibanIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bic'.
     */
    public String bicInitVal() {
        String result;
        if (_bicIsSet) {
            result = _bicInitVal;
        } else {
            result = getBic();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bic'.
     */
    public boolean bicIsDirty() {
        return !valuesAreEqual(bicInitVal(), getBic());
    }

    /**
     * Returns true if the setter method was called for the property 'bic'.
     */
    public boolean bicIsSet() {
        return _bicIsSet;
    }
	
    /**
     * Returns the initial value of the property 'account number'.
     */
    public String accountNumberInitVal() {
        String result;
        if (_accountNumberIsSet) {
            result = _accountNumberInitVal;
        } else {
            result = getAccountNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'account number'.
     */
    public boolean accountNumberIsDirty() {
        return !valuesAreEqual(accountNumberInitVal(), getAccountNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'account number'.
     */
    public boolean accountNumberIsSet() {
        return _accountNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'bank code'.
     */
    public String bankCodeInitVal() {
        String result;
        if (_bankCodeIsSet) {
            result = _bankCodeInitVal;
        } else {
            result = getBankCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'bank code'.
     */
    public boolean bankCodeIsDirty() {
        return !valuesAreEqual(bankCodeInitVal(), getBankCode());
    }

    /**
     * Returns true if the setter method was called for the property 'bank code'.
     */
    public boolean bankCodeIsSet() {
        return _bankCodeIsSet;
    }

}
