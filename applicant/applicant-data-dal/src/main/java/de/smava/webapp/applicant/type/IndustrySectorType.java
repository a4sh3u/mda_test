package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 24.02.15.
 */
public enum IndustrySectorType {

    AGRICULTURE_FORESTRY_FISHERY(4860L),
    MINING(4861L),
    POWER_WATER_WASTE_MANAGEMENT(4862L),
    CHEMICAL_INDUSTRY(4863L),
    MECHANICAL_INDUSTRY(4864L),
    FOOD_INDUSTRY(4865L),
    BUILDING_INDUSTRY(4866L),
    CAR_INDUSTRY(4867L),
    CAR_SERVICES(4868L),
    WHOLESALE_RETAIL(4869L),
    TRANSPORT_TOURISM(4870L),
    MAIL_TELECOMMUNICATION(4871L),
    BANK_INSURANCE(4872L),
    EDUCATION(4873L),
    CRAFTS_AND_TRADES(4874L),
    HEALTH_CARE(4875L),
    INFORMATION_TECHNOLOGY(4876L),
    MEDIA(4877L),
    LEGAL_TAX_CONSULTANCY(4878L),
    REAL_ESTATE_ARCHITECTURE(4879L),
    CIVIL_SERVICE(4880L),
    OTHER_SERVICE(4881L),
    OTHER_INDUSTRIES(4882L),
    OTHER(4883L);

    private final Long databaseId;

    private IndustrySectorType(Long id) {
        this.databaseId = id;
    }

    public Long getDatabaseId() {
        return databaseId;
    }

    public static IndustrySectorType fromDatabaseId(Long databaseId) {
        for (IndustrySectorType type : IndustrySectorType.values()) {
            if (type.getDatabaseId().equals(databaseId)) {
                return type;
            }
        }
        return null;
    }
}
