package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractRetiree;




/**
 * The domain object that has all history aggregation related fields for 'Retirees'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class RetireeHistory extends AbstractRetiree {

    protected transient java.util.Date _retirementStartInitVal;
    protected transient boolean _retirementStartIsSet;
    protected transient de.smava.webapp.applicant.type.RetireeType _retireeTypeInitVal;
    protected transient boolean _retireeTypeIsSet;
    protected transient de.smava.webapp.applicant.type.PensionType _pensionTypeInitVal;
    protected transient boolean _pensionTypeIsSet;


		
    /**
     * Returns the initial value of the property 'retirement start'.
     */
    public java.util.Date retirementStartInitVal() {
        java.util.Date result;
        if (_retirementStartIsSet) {
            result = _retirementStartInitVal;
        } else {
            result = getRetirementStart();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'retirement start'.
     */
    public boolean retirementStartIsDirty() {
        return !valuesAreEqual(retirementStartInitVal(), getRetirementStart());
    }

    /**
     * Returns true if the setter method was called for the property 'retirement start'.
     */
    public boolean retirementStartIsSet() {
        return _retirementStartIsSet;
    }
	
    /**
     * Returns the initial value of the property 'retiree type'.
     */
    public de.smava.webapp.applicant.type.RetireeType retireeTypeInitVal() {
        de.smava.webapp.applicant.type.RetireeType result;
        if (_retireeTypeIsSet) {
            result = _retireeTypeInitVal;
        } else {
            result = getRetireeType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'retiree type'.
     */
    public boolean retireeTypeIsDirty() {
        return !valuesAreEqual(retireeTypeInitVal(), getRetireeType());
    }

    /**
     * Returns true if the setter method was called for the property 'retiree type'.
     */
    public boolean retireeTypeIsSet() {
        return _retireeTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'pension type'.
     */
    public de.smava.webapp.applicant.type.PensionType pensionTypeInitVal() {
        de.smava.webapp.applicant.type.PensionType result;
        if (_pensionTypeIsSet) {
            result = _pensionTypeInitVal;
        } else {
            result = getPensionType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'pension type'.
     */
    public boolean pensionTypeIsDirty() {
        return !valuesAreEqual(pensionTypeInitVal(), getPensionType());
    }

    /**
     * Returns true if the setter method was called for the property 'pension type'.
     */
    public boolean pensionTypeIsSet() {
        return _pensionTypeIsSet;
    }

}
