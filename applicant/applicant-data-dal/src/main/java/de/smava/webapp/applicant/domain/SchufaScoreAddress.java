//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score address)}
import de.smava.webapp.applicant.domain.history.SchufaScoreAddressHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaScoreAddresss'.
 *
 * 
 *
 * @author generator
 */
public class SchufaScoreAddress extends SchufaScoreAddressHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(schufa score address)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Address _address;
        protected SchufaScoreRequestData _requestData;
        
                                    
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    public void setAddress(Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    public Address getAddress() {
        return _address;
    }
                                            
    /**
     * Setter for the property 'request data'.
     *
     * 
     *
     */
    public void setRequestData(SchufaScoreRequestData requestData) {
        _requestData = requestData;
    }
            
    /**
     * Returns the property 'request data'.
     *
     * 
     *
     */
    public SchufaScoreRequestData getRequestData() {
        return _requestData;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_address instanceof de.smava.webapp.commons.domain.Entity && !_address.getChangeSet().isEmpty()) {
             for (String element : _address.getChangeSet()) {
                 result.add("address : " + element);
             }
         }

         if (_requestData instanceof de.smava.webapp.commons.domain.Entity && !_requestData.getChangeSet().isEmpty()) {
             for (String element : _requestData.getChangeSet()) {
                 result.add("request data : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaScoreAddress.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(SchufaScoreAddress.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SchufaScoreAddress asSchufaScoreAddress() {
        return this;
    }
}
