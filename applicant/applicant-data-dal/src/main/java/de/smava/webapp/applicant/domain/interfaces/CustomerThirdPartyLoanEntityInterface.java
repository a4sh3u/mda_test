package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.BankAccount;
import de.smava.webapp.applicant.domain.CustomerThirdPartyLoan;

import java.util.Date;


/**
 * The domain object that represents 'CustomerThirdPartyLoans'.
 *
 * @author generator
 */
public interface CustomerThirdPartyLoanEntityInterface {

    /**
     * Setter for the property 'original amount'.
     *
     * 
     *
     */
    void setOriginalAmount(Double originalAmount);

    /**
     * Returns the property 'original amount'.
     *
     * 
     *
     */
    Double getOriginalAmount();
    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    void setMonthlyRate(Double monthlyRate);

    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    Double getMonthlyRate();
    /**
     * Setter for the property 'loan start date'.
     *
     * 
     *
     */
    void setLoanStartDate(Date loanStartDate);

    /**
     * Returns the property 'loan start date'.
     *
     * 
     *
     */
    Date getLoanStartDate();
    /**
     * Setter for the property 'loan end date'.
     *
     * 
     *
     */
    void setLoanEndDate(Date loanEndDate);

    /**
     * Returns the property 'loan end date'.
     *
     * 
     *
     */
    Date getLoanEndDate();
    /**
     * Setter for the property 'loan id'.
     *
     * Here is the foreign key to LoanApplication. We cannot refer to the type directly, because we would run in cyclic dependencies between the dal projects.
     *
     */
    void setLoanId(Long loanId);

    /**
     * Returns the property 'loan id'.
     *
     * Here is the foreign key to LoanApplication. We cannot refer to the type directly, because we would run in cyclic dependencies between the dal projects.
     *
     */
    Long getLoanId();
    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    void setEffectiveInterest(Double effectiveInterest);

    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    Double getEffectiveInterest();
    /**
     * Setter for the property 'consolidation wish'.
     *
     * 
     *
     */
    void setConsolidationWish(boolean consolidationWish);

    /**
     * Returns the property 'consolidation wish'.
     *
     * 
     *
     */
    boolean getConsolidationWish();
    /**
     * Setter for the property 'loan type'.
     *
     * 
     *
     */
    void setLoanType(de.smava.webapp.applicant.type.ThirdPartyLoanType loanType);

    /**
     * Returns the property 'loan type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.ThirdPartyLoanType getLoanType();
    /**
     * Setter for the property 'redemption sum'.
     *
     * 
     *
     */
    void setRedemptionSum(Double redemptionSum);

    /**
     * Returns the property 'redemption sum'.
     *
     * 
     *
     */
    Double getRedemptionSum();
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    void setBankAccount(BankAccount bankAccount);

    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    BankAccount getBankAccount();
    /**
     * Setter for the property 'drawing limit'.
     *
     * 
     *
     */
    void setDrawingLimit(Double drawingLimit);

    /**
     * Returns the property 'drawing limit'.
     *
     * 
     *
     */
    Double getDrawingLimit();
    /**
     * Setter for the property 'same credit bank account'.
     *
     * 
     *
     */
    void setSameCreditBankAccount(Boolean sameCreditBankAccount);

    /**
     * Returns the property 'same credit bank account'.
     *
     * 
     *
     */
    Boolean getSameCreditBankAccount();
    /**
     * Setter for the property 'loan source'.
     *
     * 
     *
     */
    void setLoanSource(de.smava.webapp.applicant.type.ThirdPartyLoanSource loanSource);

    /**
     * Returns the property 'loan source'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.ThirdPartyLoanSource getLoanSource();
    /**
     * Helper method to get reference of this object as model type.
     */
    CustomerThirdPartyLoan asCustomerThirdPartyLoan();
}
