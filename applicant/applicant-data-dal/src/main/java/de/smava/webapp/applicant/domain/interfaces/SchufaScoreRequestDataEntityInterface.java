package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.SchufaScoreAddress;
import de.smava.webapp.applicant.domain.SchufaScoreRequestData;
import de.smava.webapp.applicant.type.Gender;

import java.util.Date;
import java.util.List;


/**
 * The domain object that represents 'SchufaScoreRequestDatas'.
 *
 * @author generator
 */
public interface SchufaScoreRequestDataEntityInterface {

    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    void setFirstName(String firstName);

    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    String getFirstName();
    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    void setLastName(String lastName);

    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    String getLastName();
    /**
     * Setter for the property 'gender'.
     *
     * 
     *
     */
    void setGender(Gender gender);

    /**
     * Returns the property 'gender'.
     *
     * 
     *
     */
    Gender getGender();
    /**
     * Setter for the property 'birth date'.
     *
     * 
     *
     */
    void setBirthDate(Date birthDate);

    /**
     * Returns the property 'birth date'.
     *
     * 
     *
     */
    Date getBirthDate();
    /**
     * Setter for the property 'addresses'.
     *
     * 
     *
     */
    void setAddresses(List<SchufaScoreAddress> addresses);

    /**
     * Returns the property 'addresses'.
     *
     * 
     *
     */
    List<SchufaScoreAddress> getAddresses();
    /**
     * Helper method to get reference of this object as model type.
     */
    SchufaScoreRequestData asSchufaScoreRequestData();
}
