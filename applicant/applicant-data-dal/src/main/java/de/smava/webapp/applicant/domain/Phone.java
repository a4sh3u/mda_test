//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(phone)}
import de.smava.webapp.applicant.domain.history.PhoneHistory;
import de.smava.webapp.applicant.type.PhoneType;

import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Phones'.
 *
 * 
 *
 * @author generator
 */
public class Phone extends PhoneHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(phone)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected de.smava.webapp.applicant.type.PhoneType _phoneType;
        protected String _code;
        protected String _number;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'phone type'.
     *
     * 
     *
     */
    public void setPhoneType(de.smava.webapp.applicant.type.PhoneType phoneType) {
        if (!_phoneTypeIsSet) {
            _phoneTypeIsSet = true;
            _phoneTypeInitVal = getPhoneType();
        }
        registerChange("phone type", _phoneTypeInitVal, phoneType);
        _phoneType = phoneType;
    }
                        
    /**
     * Returns the property 'phone type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.PhoneType getPhoneType() {
        return _phoneType;
    }
                                    /**
     * Setter for the property 'code'.
     *
     * 
     *
     */
    public void setCode(String code) {
        if (!_codeIsSet) {
            _codeIsSet = true;
            _codeInitVal = getCode();
        }
        registerChange("code", _codeInitVal, code);
        _code = code;
    }
                        
    /**
     * Returns the property 'code'.
     *
     * 
     *
     */
    public String getCode() {
        return _code;
    }
                                    /**
     * Setter for the property 'number'.
     *
     * 
     *
     */
    public void setNumber(String number) {
        if (!_numberIsSet) {
            _numberIsSet = true;
            _numberInitVal = getNumber();
        }
        registerChange("number", _numberInitVal, number);
        _number = number;
    }
                        
    /**
     * Returns the property 'number'.
     *
     * 
     *
     */
    public String getNumber() {
        return _number;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Phone.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _phoneType=").append(_phoneType);
            builder.append("\n    _code=").append(_code);
            builder.append("\n    _number=").append(_number);
            builder.append("\n}");
        } else {
            builder.append(Phone.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Phone asPhone() {
        return this;
    }
}
