package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractNationalityInfo;



/**
 * The domain object that has all history aggregation related fields for 'NationalityInfos'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class NationalityInfoHistory extends AbstractNationalityInfo {

    protected transient de.smava.webapp.applicant.type.Country _nationalityInitVal;
    protected transient boolean _nationalityIsSet;
    protected transient String _placeOfBirthInitVal;
    protected transient boolean _placeOfBirthIsSet;
    protected transient de.smava.webapp.applicant.type.Country _countryOfBirthInitVal;
    protected transient boolean _countryOfBirthIsSet;
    protected transient Boolean _workPermitUnlimitedInitVal;
    protected transient boolean _workPermitUnlimitedIsSet;
    protected transient java.util.Date _endOfWorkPermitInitVal;
    protected transient boolean _endOfWorkPermitIsSet;
    protected transient Boolean _residencePermitUnlimitedInitVal;
    protected transient boolean _residencePermitUnlimitedIsSet;
    protected transient java.util.Date _endOfResidencePermitInitVal;
    protected transient boolean _endOfResidencePermitIsSet;


	
    /**
     * Returns the initial value of the property 'nationality'.
     */
    public de.smava.webapp.applicant.type.Country nationalityInitVal() {
        de.smava.webapp.applicant.type.Country result;
        if (_nationalityIsSet) {
            result = _nationalityInitVal;
        } else {
            result = getNationality();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'nationality'.
     */
    public boolean nationalityIsDirty() {
        return !valuesAreEqual(nationalityInitVal(), getNationality());
    }

    /**
     * Returns true if the setter method was called for the property 'nationality'.
     */
    public boolean nationalityIsSet() {
        return _nationalityIsSet;
    }
	
    /**
     * Returns the initial value of the property 'place of birth'.
     */
    public String placeOfBirthInitVal() {
        String result;
        if (_placeOfBirthIsSet) {
            result = _placeOfBirthInitVal;
        } else {
            result = getPlaceOfBirth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'place of birth'.
     */
    public boolean placeOfBirthIsDirty() {
        return !valuesAreEqual(placeOfBirthInitVal(), getPlaceOfBirth());
    }

    /**
     * Returns true if the setter method was called for the property 'place of birth'.
     */
    public boolean placeOfBirthIsSet() {
        return _placeOfBirthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'country of birth'.
     */
    public de.smava.webapp.applicant.type.Country countryOfBirthInitVal() {
        de.smava.webapp.applicant.type.Country result;
        if (_countryOfBirthIsSet) {
            result = _countryOfBirthInitVal;
        } else {
            result = getCountryOfBirth();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'country of birth'.
     */
    public boolean countryOfBirthIsDirty() {
        return !valuesAreEqual(countryOfBirthInitVal(), getCountryOfBirth());
    }

    /**
     * Returns true if the setter method was called for the property 'country of birth'.
     */
    public boolean countryOfBirthIsSet() {
        return _countryOfBirthIsSet;
    }
	
    /**
     * Returns the initial value of the property 'work permit unlimited'.
     */
    public Boolean workPermitUnlimitedInitVal() {
        Boolean result;
        if (_workPermitUnlimitedIsSet) {
            result = _workPermitUnlimitedInitVal;
        } else {
            result = getWorkPermitUnlimited();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'work permit unlimited'.
     */
    public boolean workPermitUnlimitedIsDirty() {
        return !valuesAreEqual(workPermitUnlimitedInitVal(), getWorkPermitUnlimited());
    }

    /**
     * Returns true if the setter method was called for the property 'work permit unlimited'.
     */
    public boolean workPermitUnlimitedIsSet() {
        return _workPermitUnlimitedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end of work permit'.
     */
    public java.util.Date endOfWorkPermitInitVal() {
        java.util.Date result;
        if (_endOfWorkPermitIsSet) {
            result = _endOfWorkPermitInitVal;
        } else {
            result = getEndOfWorkPermit();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end of work permit'.
     */
    public boolean endOfWorkPermitIsDirty() {
        return !valuesAreEqual(endOfWorkPermitInitVal(), getEndOfWorkPermit());
    }

    /**
     * Returns true if the setter method was called for the property 'end of work permit'.
     */
    public boolean endOfWorkPermitIsSet() {
        return _endOfWorkPermitIsSet;
    }
	
    /**
     * Returns the initial value of the property 'residence permit unlimited'.
     */
    public Boolean residencePermitUnlimitedInitVal() {
        Boolean result;
        if (_residencePermitUnlimitedIsSet) {
            result = _residencePermitUnlimitedInitVal;
        } else {
            result = getResidencePermitUnlimited();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'residence permit unlimited'.
     */
    public boolean residencePermitUnlimitedIsDirty() {
        return !valuesAreEqual(residencePermitUnlimitedInitVal(), getResidencePermitUnlimited());
    }

    /**
     * Returns true if the setter method was called for the property 'residence permit unlimited'.
     */
    public boolean residencePermitUnlimitedIsSet() {
        return _residencePermitUnlimitedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end of residence permit'.
     */
    public java.util.Date endOfResidencePermitInitVal() {
        java.util.Date result;
        if (_endOfResidencePermitIsSet) {
            result = _endOfResidencePermitInitVal;
        } else {
            result = getEndOfResidencePermit();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end of residence permit'.
     */
    public boolean endOfResidencePermitIsDirty() {
        return !valuesAreEqual(endOfResidencePermitInitVal(), getEndOfResidencePermit());
    }

    /**
     * Returns true if the setter method was called for the property 'end of residence permit'.
     */
    public boolean endOfResidencePermitIsSet() {
        return _endOfResidencePermitIsSet;
    }

}
