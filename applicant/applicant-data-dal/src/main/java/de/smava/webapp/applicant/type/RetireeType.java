package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 02.03.15.
 */
public enum RetireeType {

    /**
     * Rentner
     */
    RETIREE,

    /**
     * Pensionär
     */
    PENSIONER;
}
