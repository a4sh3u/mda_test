//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(living address)}
import de.smava.webapp.applicant.domain.history.LivingAddressHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LivingAddresss'.
 *
 * Extends an address by applying additional information like the time of moving in.
 *
 * @author generator
 */
public class LivingAddress extends LivingAddressHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(living address)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _moveInDate;
        protected Address _address;
        
                            /**
     * Setter for the property 'move in date'.
     *
     * 
     *
     */
    public void setMoveInDate(java.util.Date moveInDate) {
        if (!_moveInDateIsSet) {
            _moveInDateIsSet = true;
            _moveInDateInitVal = getMoveInDate();
        }
        registerChange("move in date", _moveInDateInitVal, moveInDate);
        _moveInDate = moveInDate;
    }
                        
    /**
     * Returns the property 'move in date'.
     *
     * 
     *
     */
    public java.util.Date getMoveInDate() {
        return _moveInDate;
    }
                                            
    /**
     * Setter for the property 'address'.
     *
     * Extended address
     *
     */
    public void setAddress(Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     *
     * Extended address
     *
     */
    public Address getAddress() {
        return _address;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_address instanceof de.smava.webapp.commons.domain.Entity && !_address.getChangeSet().isEmpty()) {
             for (String element : _address.getChangeSet()) {
                 result.add("address : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(LivingAddress.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _moveInDate=").append(_moveInDate);
            builder.append("\n}");
        } else {
            builder.append(LivingAddress.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public LivingAddress asLivingAddress() {
        return this;
    }
}
