package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Employee;


/**
 * The domain object that represents 'Employees'.
 *
 * @author generator
 */
public interface EmployeeEntityInterface {

    /**
     * Setter for the property 'employee type'.
     *
     * 
     *
     */
    void setEmployeeType(de.smava.webapp.applicant.type.EmployeeType employeeType);

    /**
     * Returns the property 'employee type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.EmployeeType getEmployeeType();
    /**
     * Helper method to get reference of this object as model type.
     */
    Employee asEmployee();
}
