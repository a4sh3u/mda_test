//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(nationality info)}
import de.smava.webapp.applicant.domain.history.NationalityInfoHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'NationalityInfos'.
 *
 * 
 *
 * @author generator
 */
public class NationalityInfo extends NationalityInfoHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(nationality info)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.Country _nationality;
        protected String _placeOfBirth;
        protected de.smava.webapp.applicant.type.Country _countryOfBirth;
        protected Boolean _workPermitUnlimited;
        protected java.util.Date _endOfWorkPermit;
        protected Boolean _residencePermitUnlimited;
        protected java.util.Date _endOfResidencePermit;
        
                            /**
     * Setter for the property 'nationality'.
     *
     * 
     *
     */
    public void setNationality(de.smava.webapp.applicant.type.Country nationality) {
        if (!_nationalityIsSet) {
            _nationalityIsSet = true;
            _nationalityInitVal = getNationality();
        }
        registerChange("nationality", _nationalityInitVal, nationality);
        _nationality = nationality;
    }
                        
    /**
     * Returns the property 'nationality'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.Country getNationality() {
        return _nationality;
    }
                                    /**
     * Setter for the property 'place of birth'.
     *
     * 
     *
     */
    public void setPlaceOfBirth(String placeOfBirth) {
        if (!_placeOfBirthIsSet) {
            _placeOfBirthIsSet = true;
            _placeOfBirthInitVal = getPlaceOfBirth();
        }
        registerChange("place of birth", _placeOfBirthInitVal, placeOfBirth);
        _placeOfBirth = placeOfBirth;
    }
                        
    /**
     * Returns the property 'place of birth'.
     *
     * 
     *
     */
    public String getPlaceOfBirth() {
        return _placeOfBirth;
    }
                                    /**
     * Setter for the property 'country of birth'.
     *
     * 
     *
     */
    public void setCountryOfBirth(de.smava.webapp.applicant.type.Country countryOfBirth) {
        if (!_countryOfBirthIsSet) {
            _countryOfBirthIsSet = true;
            _countryOfBirthInitVal = getCountryOfBirth();
        }
        registerChange("country of birth", _countryOfBirthInitVal, countryOfBirth);
        _countryOfBirth = countryOfBirth;
    }
                        
    /**
     * Returns the property 'country of birth'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.Country getCountryOfBirth() {
        return _countryOfBirth;
    }
                                    /**
     * Setter for the property 'work permit unlimited'.
     *
     * 
     *
     */
    public void setWorkPermitUnlimited(Boolean workPermitUnlimited) {
        if (!_workPermitUnlimitedIsSet) {
            _workPermitUnlimitedIsSet = true;
            _workPermitUnlimitedInitVal = getWorkPermitUnlimited();
        }
        registerChange("work permit unlimited", _workPermitUnlimitedInitVal, workPermitUnlimited);
        _workPermitUnlimited = workPermitUnlimited;
    }
                        
    /**
     * Returns the property 'work permit unlimited'.
     *
     * 
     *
     */
    public Boolean getWorkPermitUnlimited() {
        return _workPermitUnlimited;
    }
                                    /**
     * Setter for the property 'end of work permit'.
     *
     * 
     *
     */
    public void setEndOfWorkPermit(java.util.Date endOfWorkPermit) {
        if (!_endOfWorkPermitIsSet) {
            _endOfWorkPermitIsSet = true;
            _endOfWorkPermitInitVal = getEndOfWorkPermit();
        }
        registerChange("end of work permit", _endOfWorkPermitInitVal, endOfWorkPermit);
        _endOfWorkPermit = endOfWorkPermit;
    }
                        
    /**
     * Returns the property 'end of work permit'.
     *
     * 
     *
     */
    public java.util.Date getEndOfWorkPermit() {
        return _endOfWorkPermit;
    }
                                    /**
     * Setter for the property 'residence permit unlimited'.
     *
     * 
     *
     */
    public void setResidencePermitUnlimited(Boolean residencePermitUnlimited) {
        if (!_residencePermitUnlimitedIsSet) {
            _residencePermitUnlimitedIsSet = true;
            _residencePermitUnlimitedInitVal = getResidencePermitUnlimited();
        }
        registerChange("residence permit unlimited", _residencePermitUnlimitedInitVal, residencePermitUnlimited);
        _residencePermitUnlimited = residencePermitUnlimited;
    }
                        
    /**
     * Returns the property 'residence permit unlimited'.
     *
     * 
     *
     */
    public Boolean getResidencePermitUnlimited() {
        return _residencePermitUnlimited;
    }
                                    /**
     * Setter for the property 'end of residence permit'.
     *
     * 
     *
     */
    public void setEndOfResidencePermit(java.util.Date endOfResidencePermit) {
        if (!_endOfResidencePermitIsSet) {
            _endOfResidencePermitIsSet = true;
            _endOfResidencePermitInitVal = getEndOfResidencePermit();
        }
        registerChange("end of residence permit", _endOfResidencePermitInitVal, endOfResidencePermit);
        _endOfResidencePermit = endOfResidencePermit;
    }
                        
    /**
     * Returns the property 'end of residence permit'.
     *
     * 
     *
     */
    public java.util.Date getEndOfResidencePermit() {
        return _endOfResidencePermit;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(NationalityInfo.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _nationality=").append(_nationality);
            builder.append("\n    _placeOfBirth=").append(_placeOfBirth);
            builder.append("\n    _countryOfBirth=").append(_countryOfBirth);
            builder.append("\n    _endOfWorkPermit=").append(_endOfWorkPermit);
            builder.append("\n    _endOfResidencePermit=").append(_endOfResidencePermit);
            builder.append("\n}");
        } else {
            builder.append(NationalityInfo.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public NationalityInfo asNationalityInfo() {
        return this;
    }
}
