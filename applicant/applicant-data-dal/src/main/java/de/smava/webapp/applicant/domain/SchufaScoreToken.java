//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score token)}
import de.smava.webapp.applicant.domain.history.SchufaScoreTokenHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaScoreTokens'.
 *
 * 
 *
 * @author generator
 */
public class SchufaScoreToken extends SchufaScoreTokenHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(schufa score token)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected SchufaScore _score;
        protected String _description;
        protected String _tokenCode;
        protected String _currency;
        protected String _amount;
        protected String _date;
        protected String _rateType;
        protected String _rateCount;
        protected String _accountNumber;
        protected String _tokenText;
        protected String _textField;
        protected String _customTokenCode;
        protected double _calculatedRate;
        protected String _comment;
        protected boolean _finished;
        protected boolean _business;
        protected Double _customRate;
        protected boolean _isSmavaToken;
        protected boolean _refinance;
        
                                    
    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    public void setScore(SchufaScore score) {
        _score = score;
    }
            
    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    public SchufaScore getScore() {
        return _score;
    }
                                    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'token code'.
     *
     * 
     *
     */
    public void setTokenCode(String tokenCode) {
        if (!_tokenCodeIsSet) {
            _tokenCodeIsSet = true;
            _tokenCodeInitVal = getTokenCode();
        }
        registerChange("token code", _tokenCodeInitVal, tokenCode);
        _tokenCode = tokenCode;
    }
                        
    /**
     * Returns the property 'token code'.
     *
     * 
     *
     */
    public String getTokenCode() {
        return _tokenCode;
    }
                                    /**
     * Setter for the property 'currency'.
     *
     * 
     *
     */
    public void setCurrency(String currency) {
        if (!_currencyIsSet) {
            _currencyIsSet = true;
            _currencyInitVal = getCurrency();
        }
        registerChange("currency", _currencyInitVal, currency);
        _currency = currency;
    }
                        
    /**
     * Returns the property 'currency'.
     *
     * 
     *
     */
    public String getCurrency() {
        return _currency;
    }
                                    /**
     * Setter for the property 'amount'.
     *
     * 
     *
     */
    public void setAmount(String amount) {
        if (!_amountIsSet) {
            _amountIsSet = true;
            _amountInitVal = getAmount();
        }
        registerChange("amount", _amountInitVal, amount);
        _amount = amount;
    }
                        
    /**
     * Returns the property 'amount'.
     *
     * 
     *
     */
    public String getAmount() {
        return _amount;
    }
                                    /**
     * Setter for the property 'date'.
     *
     * 
     *
     */
    public void setDate(String date) {
        if (!_dateIsSet) {
            _dateIsSet = true;
            _dateInitVal = getDate();
        }
        registerChange("date", _dateInitVal, date);
        _date = date;
    }
                        
    /**
     * Returns the property 'date'.
     *
     * 
     *
     */
    public String getDate() {
        return _date;
    }
                                    /**
     * Setter for the property 'rate type'.
     *
     * 
     *
     */
    public void setRateType(String rateType) {
        if (!_rateTypeIsSet) {
            _rateTypeIsSet = true;
            _rateTypeInitVal = getRateType();
        }
        registerChange("rate type", _rateTypeInitVal, rateType);
        _rateType = rateType;
    }
                        
    /**
     * Returns the property 'rate type'.
     *
     * 
     *
     */
    public String getRateType() {
        return _rateType;
    }
                                    /**
     * Setter for the property 'rate count'.
     *
     * 
     *
     */
    public void setRateCount(String rateCount) {
        if (!_rateCountIsSet) {
            _rateCountIsSet = true;
            _rateCountInitVal = getRateCount();
        }
        registerChange("rate count", _rateCountInitVal, rateCount);
        _rateCount = rateCount;
    }
                        
    /**
     * Returns the property 'rate count'.
     *
     * 
     *
     */
    public String getRateCount() {
        return _rateCount;
    }
                                    /**
     * Setter for the property 'account number'.
     *
     * 
     *
     */
    public void setAccountNumber(String accountNumber) {
        if (!_accountNumberIsSet) {
            _accountNumberIsSet = true;
            _accountNumberInitVal = getAccountNumber();
        }
        registerChange("account number", _accountNumberInitVal, accountNumber);
        _accountNumber = accountNumber;
    }
                        
    /**
     * Returns the property 'account number'.
     *
     * 
     *
     */
    public String getAccountNumber() {
        return _accountNumber;
    }
                                    /**
     * Setter for the property 'token text'.
     *
     * 
     *
     */
    public void setTokenText(String tokenText) {
        if (!_tokenTextIsSet) {
            _tokenTextIsSet = true;
            _tokenTextInitVal = getTokenText();
        }
        registerChange("token text", _tokenTextInitVal, tokenText);
        _tokenText = tokenText;
    }
                        
    /**
     * Returns the property 'token text'.
     *
     * 
     *
     */
    public String getTokenText() {
        return _tokenText;
    }
                                    /**
     * Setter for the property 'text field'.
     *
     * 
     *
     */
    public void setTextField(String textField) {
        if (!_textFieldIsSet) {
            _textFieldIsSet = true;
            _textFieldInitVal = getTextField();
        }
        registerChange("text field", _textFieldInitVal, textField);
        _textField = textField;
    }
                        
    /**
     * Returns the property 'text field'.
     *
     * 
     *
     */
    public String getTextField() {
        return _textField;
    }
                                    /**
     * Setter for the property 'custom token code'.
     *
     * 
     *
     */
    public void setCustomTokenCode(String customTokenCode) {
        if (!_customTokenCodeIsSet) {
            _customTokenCodeIsSet = true;
            _customTokenCodeInitVal = getCustomTokenCode();
        }
        registerChange("custom token code", _customTokenCodeInitVal, customTokenCode);
        _customTokenCode = customTokenCode;
    }
                        
    /**
     * Returns the property 'custom token code'.
     *
     * 
     *
     */
    public String getCustomTokenCode() {
        return _customTokenCode;
    }
                                    /**
     * Setter for the property 'calculated rate'.
     *
     * 
     *
     */
    public void setCalculatedRate(double calculatedRate) {
        if (!_calculatedRateIsSet) {
            _calculatedRateIsSet = true;
            _calculatedRateInitVal = getCalculatedRate();
        }
        registerChange("calculated rate", _calculatedRateInitVal, calculatedRate);
        _calculatedRate = calculatedRate;
    }
                        
    /**
     * Returns the property 'calculated rate'.
     *
     * 
     *
     */
    public double getCalculatedRate() {
        return _calculatedRate;
    }
                                    /**
     * Setter for the property 'comment'.
     *
     * 
     *
     */
    public void setComment(String comment) {
        if (!_commentIsSet) {
            _commentIsSet = true;
            _commentInitVal = getComment();
        }
        registerChange("comment", _commentInitVal, comment);
        _comment = comment;
    }
                        
    /**
     * Returns the property 'comment'.
     *
     * 
     *
     */
    public String getComment() {
        return _comment;
    }
                                    /**
     * Setter for the property 'finished'.
     *
     * 
     *
     */
    public void setFinished(boolean finished) {
        if (!_finishedIsSet) {
            _finishedIsSet = true;
            _finishedInitVal = getFinished();
        }
        registerChange("finished", _finishedInitVal, finished);
        _finished = finished;
    }
                        
    /**
     * Returns the property 'finished'.
     *
     * 
     *
     */
    public boolean getFinished() {
        return _finished;
    }
                                    /**
     * Setter for the property 'business'.
     *
     * 
     *
     */
    public void setBusiness(boolean business) {
        if (!_businessIsSet) {
            _businessIsSet = true;
            _businessInitVal = getBusiness();
        }
        registerChange("business", _businessInitVal, business);
        _business = business;
    }
                        
    /**
     * Returns the property 'business'.
     *
     * 
     *
     */
    public boolean getBusiness() {
        return _business;
    }
                                    /**
     * Setter for the property 'custom rate'.
     *
     * 
     *
     */
    public void setCustomRate(Double customRate) {
        if (!_customRateIsSet) {
            _customRateIsSet = true;
            _customRateInitVal = getCustomRate();
        }
        registerChange("custom rate", _customRateInitVal, customRate);
        _customRate = customRate;
    }
                        
    /**
     * Returns the property 'custom rate'.
     *
     * 
     *
     */
    public Double getCustomRate() {
        return _customRate;
    }
                                    /**
     * Setter for the property 'is smava token'.
     *
     * 
     *
     */
    public void setIsSmavaToken(boolean isSmavaToken) {
        if (!_isSmavaTokenIsSet) {
            _isSmavaTokenIsSet = true;
            _isSmavaTokenInitVal = getIsSmavaToken();
        }
        registerChange("is smava token", _isSmavaTokenInitVal, isSmavaToken);
        _isSmavaToken = isSmavaToken;
    }
                        
    /**
     * Returns the property 'is smava token'.
     *
     * 
     *
     */
    public boolean getIsSmavaToken() {
        return _isSmavaToken;
    }
                                    /**
     * Setter for the property 'refinance'.
     *
     * 
     *
     */
    public void setRefinance(boolean refinance) {
        if (!_refinanceIsSet) {
            _refinanceIsSet = true;
            _refinanceInitVal = getRefinance();
        }
        registerChange("refinance", _refinanceInitVal, refinance);
        _refinance = refinance;
    }
                        
    /**
     * Returns the property 'refinance'.
     *
     * 
     *
     */
    public boolean getRefinance() {
        return _refinance;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_score instanceof de.smava.webapp.commons.domain.Entity && !_score.getChangeSet().isEmpty()) {
             for (String element : _score.getChangeSet()) {
                 result.add("score : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaScoreToken.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _tokenCode=").append(_tokenCode);
            builder.append("\n    _currency=").append(_currency);
            builder.append("\n    _amount=").append(_amount);
            builder.append("\n    _date=").append(_date);
            builder.append("\n    _rateType=").append(_rateType);
            builder.append("\n    _rateCount=").append(_rateCount);
            builder.append("\n    _accountNumber=").append(_accountNumber);
            builder.append("\n    _tokenText=").append(_tokenText);
            builder.append("\n    _textField=").append(_textField);
            builder.append("\n    _customTokenCode=").append(_customTokenCode);
            builder.append("\n    _calculatedRate=").append(_calculatedRate);
            builder.append("\n    _comment=").append(_comment);
            builder.append("\n    _finished=").append(_finished);
            builder.append("\n    _business=").append(_business);
            builder.append("\n    _isSmavaToken=").append(_isSmavaToken);
            builder.append("\n    _refinance=").append(_refinance);
            builder.append("\n}");
        } else {
            builder.append(SchufaScoreToken.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SchufaScoreToken asSchufaScoreToken() {
        return this;
    }
}
