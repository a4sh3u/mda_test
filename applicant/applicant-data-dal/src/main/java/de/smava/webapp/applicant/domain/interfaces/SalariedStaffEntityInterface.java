package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Employer;
import de.smava.webapp.applicant.domain.SalariedStaff;

import java.util.Date;


/**
 * The domain object that represents 'SalariedStaffs'.
 *
 * @author generator
 */
public interface SalariedStaffEntityInterface {

    /**
     * Setter for the property 'employed since'.
     *
     * 
     *
     */
    void setEmployedSince(Date employedSince);

    /**
     * Returns the property 'employed since'.
     *
     * 
     *
     */
    Date getEmployedSince();
    /**
     * Setter for the property 'industry sector'.
     *
     * 
     *
     */
    void setIndustrySector(de.smava.webapp.applicant.type.IndustrySectorType industrySector);

    /**
     * Returns the property 'industry sector'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.IndustrySectorType getIndustrySector();
    /**
     * Setter for the property 'employer'.
     *
     * 
     *
     */
    void setEmployer(Employer employer);

    /**
     * Returns the property 'employer'.
     *
     * 
     *
     */
    Employer getEmployer();
    /**
     * Setter for the property 'staff type'.
     *
     * 
     *
     */
    void setStaffType(de.smava.webapp.applicant.type.SalariedStaffType staffType);

    /**
     * Returns the property 'staff type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.SalariedStaffType getStaffType();
    /**
     * Setter for the property 'probation period'.
     *
     * 
     *
     */
    void setProbationPeriod(Boolean probationPeriod);

    /**
     * Returns the property 'probation period'.
     *
     * 
     *
     */
    Boolean getProbationPeriod();
    /**
     * Setter for the property 'end of probation period'.
     *
     * 
     *
     */
    void setEndOfProbationPeriod(Date endOfProbationPeriod);

    /**
     * Returns the property 'end of probation period'.
     *
     * 
     *
     */
    Date getEndOfProbationPeriod();
    /**
     * Setter for the property 'temporarily employed'.
     *
     * 
     *
     */
    void setTemporarilyEmployed(Boolean temporarilyEmployed);

    /**
     * Returns the property 'temporarily employed'.
     *
     * 
     *
     */
    Boolean getTemporarilyEmployed();
    /**
     * Setter for the property 'end of temporary employment'.
     *
     * 
     *
     */
    void setEndOfTemporaryEmployment(Date endOfTemporaryEmployment);

    /**
     * Returns the property 'end of temporary employment'.
     *
     * 
     *
     */
    Date getEndOfTemporaryEmployment();
    /**
     * Helper method to get reference of this object as model type.
     */
    SalariedStaff asSalariedStaff();
}
