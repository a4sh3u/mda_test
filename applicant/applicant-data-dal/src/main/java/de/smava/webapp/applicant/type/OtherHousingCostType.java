package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 02.03.15.
 */
public enum OtherHousingCostType {
    PARENTS,

    /**
     * Mietbeteiligung
     */
    RENT_CONTRIBUTION,
    RENT_FREE,
    MILITARY,
    OTHER;
}
