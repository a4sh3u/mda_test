//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(retiree)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.domain.Retiree;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Retirees'.
 *
 * @author generator
 */
public interface RetireeDao extends BrokerageSchemaDao<Retiree> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the retiree identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Retiree getRetiree(Long id);

    /**
     * Saves the retiree.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveRetiree(Retiree retiree);

    /**
     * Deletes an retiree, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the retiree
     */
    void deleteRetiree(Long id);

    /**
     * Retrieves all 'Retiree' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Retiree> getRetireeList();

    /**
     * Retrieves a page of 'Retiree' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Retiree> getRetireeList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Retiree' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Retiree> getRetireeList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Retiree' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Retiree> getRetireeList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'Retiree' instances.
     */
    long getRetireeCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(retiree)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
