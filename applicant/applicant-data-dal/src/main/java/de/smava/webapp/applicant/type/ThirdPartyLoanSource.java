package de.smava.webapp.applicant.type;

/**
 * Created by lulitzky on 26.04.16.
 */
public enum ThirdPartyLoanSource {
        CUSTOMER,

        SCHUFA,

        ADVISOR,

        MERGED,

        DELETED;
}
