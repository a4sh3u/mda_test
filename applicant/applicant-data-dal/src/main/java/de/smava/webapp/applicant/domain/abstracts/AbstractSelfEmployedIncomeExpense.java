//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(self employed income expense)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.SelfEmployedIncomeExpense;
import de.smava.webapp.applicant.domain.interfaces.SelfEmployedIncomeExpenseEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SelfEmployedIncomeExpenses'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSelfEmployedIncomeExpense
    extends BrokerageEntity    implements SelfEmployedIncomeExpenseEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSelfEmployedIncomeExpense.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SelfEmployedIncomeExpense)) {
            return;
        }
        
        this.setYear(((SelfEmployedIncomeExpense) oldEntity).getYear());    
        this.setAnnualGrossTurnover(((SelfEmployedIncomeExpense) oldEntity).getAnnualGrossTurnover());    
        this.setAnnualExpenses(((SelfEmployedIncomeExpense) oldEntity).getAnnualExpenses());    
            
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SelfEmployedIncomeExpense)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getYear(), ((SelfEmployedIncomeExpense) otherEntity).getYear());    
        equals = equals && valuesAreEqual(this.getAnnualGrossTurnover(), ((SelfEmployedIncomeExpense) otherEntity).getAnnualGrossTurnover());    
        equals = equals && valuesAreEqual(this.getAnnualExpenses(), ((SelfEmployedIncomeExpense) otherEntity).getAnnualExpenses());    
            
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(self employed income expense)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

