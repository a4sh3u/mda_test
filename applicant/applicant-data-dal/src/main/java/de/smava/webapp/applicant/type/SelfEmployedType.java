package de.smava.webapp.applicant.type;

/**
 * @author pobrebski
 * @since 2016-12-07
 */
public enum SelfEmployedType {

    FREELANCER,
    TRADESMAN,
    MERCHANT,
    NONINCORPERATED,
    ENTERPRISE,
    OTHER;

    private SelfEmployedType() {
    }
}
