package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSalariedStaff;



/**
 * The domain object that has all history aggregation related fields for 'SalariedStaffs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SalariedStaffHistory extends AbstractSalariedStaff {

    protected transient java.util.Date _employedSinceInitVal;
    protected transient boolean _employedSinceIsSet;
    protected transient de.smava.webapp.applicant.type.IndustrySectorType _industrySectorInitVal;
    protected transient boolean _industrySectorIsSet;
    protected transient de.smava.webapp.applicant.type.SalariedStaffType _staffTypeInitVal;
    protected transient boolean _staffTypeIsSet;
    protected transient Boolean _probationPeriodInitVal;
    protected transient boolean _probationPeriodIsSet;
    protected transient java.util.Date _endOfProbationPeriodInitVal;
    protected transient boolean _endOfProbationPeriodIsSet;
    protected transient Boolean _temporarilyEmployedInitVal;
    protected transient boolean _temporarilyEmployedIsSet;
    protected transient java.util.Date _endOfTemporaryEmploymentInitVal;
    protected transient boolean _endOfTemporaryEmploymentIsSet;


	
    /**
     * Returns the initial value of the property 'employed since'.
     */
    public java.util.Date employedSinceInitVal() {
        java.util.Date result;
        if (_employedSinceIsSet) {
            result = _employedSinceInitVal;
        } else {
            result = getEmployedSince();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employed since'.
     */
    public boolean employedSinceIsDirty() {
        return !valuesAreEqual(employedSinceInitVal(), getEmployedSince());
    }

    /**
     * Returns true if the setter method was called for the property 'employed since'.
     */
    public boolean employedSinceIsSet() {
        return _employedSinceIsSet;
    }
	
    /**
     * Returns the initial value of the property 'industry sector'.
     */
    public de.smava.webapp.applicant.type.IndustrySectorType industrySectorInitVal() {
        de.smava.webapp.applicant.type.IndustrySectorType result;
        if (_industrySectorIsSet) {
            result = _industrySectorInitVal;
        } else {
            result = getIndustrySector();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'industry sector'.
     */
    public boolean industrySectorIsDirty() {
        return !valuesAreEqual(industrySectorInitVal(), getIndustrySector());
    }

    /**
     * Returns true if the setter method was called for the property 'industry sector'.
     */
    public boolean industrySectorIsSet() {
        return _industrySectorIsSet;
    }
		
    /**
     * Returns the initial value of the property 'staff type'.
     */
    public de.smava.webapp.applicant.type.SalariedStaffType staffTypeInitVal() {
        de.smava.webapp.applicant.type.SalariedStaffType result;
        if (_staffTypeIsSet) {
            result = _staffTypeInitVal;
        } else {
            result = getStaffType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'staff type'.
     */
    public boolean staffTypeIsDirty() {
        return !valuesAreEqual(staffTypeInitVal(), getStaffType());
    }

    /**
     * Returns true if the setter method was called for the property 'staff type'.
     */
    public boolean staffTypeIsSet() {
        return _staffTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'probation period'.
     */
    public Boolean probationPeriodInitVal() {
        Boolean result;
        if (_probationPeriodIsSet) {
            result = _probationPeriodInitVal;
        } else {
            result = getProbationPeriod();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'probation period'.
     */
    public boolean probationPeriodIsDirty() {
        return !valuesAreEqual(probationPeriodInitVal(), getProbationPeriod());
    }

    /**
     * Returns true if the setter method was called for the property 'probation period'.
     */
    public boolean probationPeriodIsSet() {
        return _probationPeriodIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end of probation period'.
     */
    public java.util.Date endOfProbationPeriodInitVal() {
        java.util.Date result;
        if (_endOfProbationPeriodIsSet) {
            result = _endOfProbationPeriodInitVal;
        } else {
            result = getEndOfProbationPeriod();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end of probation period'.
     */
    public boolean endOfProbationPeriodIsDirty() {
        return !valuesAreEqual(endOfProbationPeriodInitVal(), getEndOfProbationPeriod());
    }

    /**
     * Returns true if the setter method was called for the property 'end of probation period'.
     */
    public boolean endOfProbationPeriodIsSet() {
        return _endOfProbationPeriodIsSet;
    }
	
    /**
     * Returns the initial value of the property 'temporarily employed'.
     */
    public Boolean temporarilyEmployedInitVal() {
        Boolean result;
        if (_temporarilyEmployedIsSet) {
            result = _temporarilyEmployedInitVal;
        } else {
            result = getTemporarilyEmployed();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'temporarily employed'.
     */
    public boolean temporarilyEmployedIsDirty() {
        return !valuesAreEqual(temporarilyEmployedInitVal(), getTemporarilyEmployed());
    }

    /**
     * Returns true if the setter method was called for the property 'temporarily employed'.
     */
    public boolean temporarilyEmployedIsSet() {
        return _temporarilyEmployedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'end of temporary employment'.
     */
    public java.util.Date endOfTemporaryEmploymentInitVal() {
        java.util.Date result;
        if (_endOfTemporaryEmploymentIsSet) {
            result = _endOfTemporaryEmploymentInitVal;
        } else {
            result = getEndOfTemporaryEmployment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'end of temporary employment'.
     */
    public boolean endOfTemporaryEmploymentIsDirty() {
        return !valuesAreEqual(endOfTemporaryEmploymentInitVal(), getEndOfTemporaryEmployment());
    }

    /**
     * Returns true if the setter method was called for the property 'end of temporary employment'.
     */
    public boolean endOfTemporaryEmploymentIsSet() {
        return _endOfTemporaryEmploymentIsSet;
    }

}
