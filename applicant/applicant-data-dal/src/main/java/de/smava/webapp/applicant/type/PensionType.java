package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 22.05.15.
 */
public enum PensionType {

    /**
     * Altersrente Gesetzlich
     */
    PUBLIC_PENSION,

    /**
     *  Private Altersvorsorge
     */
    PRIVATE_PENSION,

    /**
     * Betriebsrente
     */
    COMPANY_PENSION,

    /**
     * Zusatzrente
     */
    SUPPLEMENTARY_PENSION,

    /**
     * Witwenrente
     */
    WIDOW_PENSION,

    /**
     * Waisenrente
     */
    ORPHAN_PENSION,

    /**
     * Halbwaisenrente
     */
    HALF_ORPHAN_PENSION,

    /**
     * Erwerbsunfähigkeitsrente
     */
    DISABILITY_PENSION,

    /**
     * Erwerbsminderungsrente
     */
    DISABILITY_BENEFIT,

    /**
     * Berufsunfähigkeitsrente
     */
    JOB_DISABILITY_PENSION,

    /**
     * Berufsunfallrente
     */
    JOB_ACCIDENT_PENSION,

    /**
     * Behindertenrente
     */
    INVALIDITY_PENSION,

    /**
     * Verletztenrente aus der gesetz. Unfallversicherung
     */
    PUBLIC_INJURY_INSURANCE_PENSION,

    /**
     * Verletztenrente aus der öffent. Unfallversicherung
     */
    PRIVATE_INJURY_INSURANCE_PENSION,

    OTHER;
}
