package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.*;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'SchufaScores'.
 *
 * @author generator
 */
public interface SchufaScoreEntityInterface {

    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    void setApplicant(Applicant applicant);

    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    Applicant getApplicant();
    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    void setScore(int score);

    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    int getScore();
    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    void setRating(String rating);

    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    String getRating();
    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    void setType(String type);

    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    String getType();
    /**
     * Setter for the property 'remark'.
     *
     * 
     *
     */
    void setRemark(String remark);

    /**
     * Returns the property 'remark'.
     *
     * 
     *
     */
    String getRemark();
    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    void setState(String state);

    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    String getState();
    /**
     * Setter for the property 'request data'.
     *
     * 
     *
     */
    void setRequestData(SchufaScoreRequestData requestData);

    /**
     * Returns the property 'request data'.
     *
     * 
     *
     */
    SchufaScoreRequestData getRequestData();
    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    void setRequest(SchufaRequest request);

    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    SchufaRequest getRequest();
    /**
     * Setter for the property 'version'.
     *
     * 
     *
     */
    void setVersion(String version);

    /**
     * Returns the property 'version'.
     *
     * 
     *
     */
    String getVersion();
    /**
     * Setter for the property 'tokens'.
     *
     * 
     *
     */
    void setTokens(Set<SchufaScoreToken> tokens);

    /**
     * Returns the property 'tokens'.
     *
     * 
     *
     */
    Set<SchufaScoreToken> getTokens();
    /**
     * Helper method to get reference of this object as model type.
     */
    SchufaScore asSchufaScore();
}
