//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(employment)}

import de.smava.webapp.applicant.domain.history.EmploymentHistory;
import de.smava.webapp.applicant.type.EmploymentType;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Employments'.
 *
 * 
 *
 * @author generator
 */
public class Employment extends EmploymentHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(employment)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected EmploymentType _employmentType;
        
                            /**
     * Setter for the property 'employment type'.
     *
     * 
     *
     */
    public void setEmploymentType(EmploymentType employmentType) {
        if (!_employmentTypeIsSet) {
            _employmentTypeIsSet = true;
            _employmentTypeInitVal = getEmploymentType();
        }
        registerChange("employment type", _employmentTypeInitVal, employmentType);
        _employmentType = employmentType;
    }
                        
    /**
     * Returns the property 'employment type'.
     *
     * 
     *
     */
    public EmploymentType getEmploymentType() {
        return _employmentType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Employment.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _employmentType=").append(_employmentType);
            builder.append("\n}");
        } else {
            builder.append(Employment.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Employment asEmployment() {
        return this;
    }
}
