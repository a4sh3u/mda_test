//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(household)}
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.applicant.domain.Household;
import de.smava.webapp.applicant.domain.interfaces.HouseholdEntityInterface;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Households'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * Houshold situation for describing the living situation more in detail.
 *
 * @author generator
 */
public abstract class AbstractHousehold
    extends BrokerageEntity implements HouseholdEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractHousehold.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Household)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setNumberOfChildren(((Household) oldEntity).getNumberOfChildren());    
        this.setNumberOfPersonsInHousehold(((Household) oldEntity).getNumberOfPersonsInHousehold());    
        this.setNumberOfCars(((Household) oldEntity).getNumberOfCars());    
        this.setNumberOfMotorbikes(((Household) oldEntity).getNumberOfMotorbikes());    
        this.setDeptCreditCards(((Household) oldEntity).getDeptCreditCards());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Household)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getNumberOfChildren(), ((Household) otherEntity).getNumberOfChildren());    
        equals = equals && valuesAreEqual(this.getNumberOfPersonsInHousehold(), ((Household) otherEntity).getNumberOfPersonsInHousehold());    
        equals = equals && valuesAreEqual(this.getNumberOfCars(), ((Household) otherEntity).getNumberOfCars());    
        equals = equals && valuesAreEqual(this.getNumberOfMotorbikes(), ((Household) otherEntity).getNumberOfMotorbikes());    
        equals = equals && valuesAreEqual(this.getDeptCreditCards(), ((Household) otherEntity).getDeptCreditCards());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(household)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

