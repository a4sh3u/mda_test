package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 28.05.15.
 */
public enum SalariedStaffType {

    /**
     * Vollzeit (default)
     */
    FULL_TIME,

    /**
     * Teilzeit
     */
    PART_TIME,

    /**
     * Nebenjob
     */
    SIDE_JOB,

    /**
     * Geringfügige Beschäftigung
     */
    MINOR_EMPLOYMENT,

    /**
     * Mini Job
     */
    MINI_JOB,

    /**
     * Mutterschutz/Erziehungsurlaub
     */
    MATERNITY_PROTECTION,

    /**
     * Kurzarbeit
     */
    SHORT_TIME,

    /**
     * Zeitarbeit
     */
    INTERIM,

    /**
     * Saisonarbeit
     */
    SEASONAL,

    /**
     * Azubi
     */
    APPRENTICE,

    /**
     * Sonstige
     */
    OTHER
}
