package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Expense;


/**
 * The domain object that represents 'Expenses'.
 *
 * @author generator
 */
public interface ExpenseEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(java.util.Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    java.util.Date getCreationDate();
    /**
     * Setter for the property 'expense type'.
     *
     * 
     *
     */
    void setExpenseType(de.smava.webapp.applicant.type.ExpenseType expenseType);

    /**
     * Returns the property 'expense type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.ExpenseType getExpenseType();
    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    void setDescription(String description);

    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    String getDescription();
    /**
     * Setter for the property 'monthly amount'.
     *
     * 
     *
     */
    void setMonthlyAmount(Double monthlyAmount);

    /**
     * Returns the property 'monthly amount'.
     *
     * 
     *
     */
    Double getMonthlyAmount();
    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    void setExpirationDate(java.util.Date expirationDate);

    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    java.util.Date getExpirationDate();
    /**
     * Helper method to get reference of this object as model type.
     */
    Expense asExpense();
}
