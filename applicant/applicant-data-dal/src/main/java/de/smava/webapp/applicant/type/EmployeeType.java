package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 25.02.15.
 */
public enum EmployeeType {

    /**
     * Angestellter
     */
    EMPLOYEE,

    /**
     * Arbeiter
     */
    LABOURER,

    /**
     * CEO
     */
    CEO,

    /**
     * Sonstige
     */
    OTHER,

    /**
     * CC-3558 / CC-3556: Extend EmployeeType bei "EXECUTIVE_EMPLOYEE"
     * Geschafstfuhrer / leitender Angestellter
     */
    EXECUTIVE_EMPLOYEE;
}
