//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa request)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.SchufaRequestEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaRequests'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSchufaRequest
    extends BrokerageEntity    implements SchufaRequestEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaRequest.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SchufaRequest)) {
            return;
        }
        
        this.setRequestXml(((SchufaRequest) oldEntity).getRequestXml());    
        this.setResponseXml(((SchufaRequest) oldEntity).getResponseXml());    
        this.setCreationDate(CurrentDate.getDate());    
        this.setData(((SchufaRequest) oldEntity).getData());    
        this.setState(((SchufaRequest) oldEntity).getState());    
        this.setType(((SchufaRequest) oldEntity).getType());    
        this.setApplicant(((SchufaRequest) oldEntity).getApplicant());    
        this.setAccountNumber(((SchufaRequest) oldEntity).getAccountNumber());    
        this.setSchufaPersonKey(((SchufaRequest) oldEntity).getSchufaPersonKey());    
        this.setPersonId(((SchufaRequest) oldEntity).getPersonId());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SchufaRequest)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getRequestXml(), ((SchufaRequest) otherEntity).getRequestXml());    
        equals = equals && valuesAreEqual(this.getResponseXml(), ((SchufaRequest) otherEntity).getResponseXml());    
            
        equals = equals && valuesAreEqual(this.getData(), ((SchufaRequest) otherEntity).getData());    
        equals = equals && valuesAreEqual(this.getState(), ((SchufaRequest) otherEntity).getState());    
        equals = equals && valuesAreEqual(this.getType(), ((SchufaRequest) otherEntity).getType());    
        equals = equals && valuesAreEqual(this.getApplicant(), ((SchufaRequest) otherEntity).getApplicant());    
        equals = equals && valuesAreEqual(this.getAccountNumber(), ((SchufaRequest) otherEntity).getAccountNumber());    
        equals = equals && valuesAreEqual(this.getSchufaPersonKey(), ((SchufaRequest) otherEntity).getSchufaPersonKey());    
        equals = equals && valuesAreEqual(this.getPersonId(), ((SchufaRequest) otherEntity).getPersonId());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(schufa request)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

