package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractInvestment;




/**
 * The domain object that has all history aggregation related fields for 'Investments'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class InvestmentHistory extends AbstractInvestment {

    protected transient de.smava.webapp.applicant.type.InvestmentType _investmentTypeInitVal;
    protected transient boolean _investmentTypeIsSet;


	
    /**
     * Returns the initial value of the property 'investment type'.
     */
    public de.smava.webapp.applicant.type.InvestmentType investmentTypeInitVal() {
        de.smava.webapp.applicant.type.InvestmentType result;
        if (_investmentTypeIsSet) {
            result = _investmentTypeInitVal;
        } else {
            result = getInvestmentType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'investment type'.
     */
    public boolean investmentTypeIsDirty() {
        return !valuesAreEqual(investmentTypeInitVal(), getInvestmentType());
    }

    /**
     * Returns true if the setter method was called for the property 'investment type'.
     */
    public boolean investmentTypeIsSet() {
        return _investmentTypeIsSet;
    }
	
}
