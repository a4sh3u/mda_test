//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(applicant)}

import de.smava.webapp.applicant.domain.*;
import de.smava.webapp.applicant.domain.interfaces.ApplicantEntityInterface;
import de.smava.webapp.applicant.type.*;
import de.smava.webapp.commons.currentdate.CurrentDate;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.*;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Applicants'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * @author generator
 */
public abstract class AbstractApplicant
        extends BrokerageEntity implements ApplicantEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(AbstractApplicant.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof Applicant)) {
            return;
        }

        this.setCreationDate(CurrentDate.getDate());
        this.setNationalityInfo(((Applicant) oldEntity).getNationalityInfo());
        this.setHomeAddress(((Applicant) oldEntity).getHomeAddress());
        this.setPreviousHomeAddress(((Applicant) oldEntity).getPreviousHomeAddress());
        this.setPreviousEmployment(((Applicant) oldEntity).getPreviousEmployment());
        this.setHousehold(((Applicant) oldEntity).getHousehold());
        this.setMainIncome(((Applicant) oldEntity).getMainIncome());


        this.setPersonalData(((Applicant) oldEntity).getPersonalData());
        this.setEmail(((Applicant) oldEntity).getEmail());
        this.setCreditScores(((Applicant) oldEntity).getCreditScores());

        this.setBankAccount(((Applicant) oldEntity).getBankAccount());

    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof Applicant)) {
            equals = false;
        }

        equals = equals && valuesAreEqual(this.getNationalityInfo(), ((Applicant) otherEntity).getNationalityInfo());
        equals = equals && valuesAreEqual(this.getHomeAddress(), ((Applicant) otherEntity).getHomeAddress());
        equals = equals && valuesAreEqual(this.getPreviousHomeAddress(), ((Applicant) otherEntity).getPreviousHomeAddress());
        equals = equals && valuesAreEqual(this.getPreviousEmployment(), ((Applicant) otherEntity).getPreviousEmployment());
        equals = equals && valuesAreEqual(this.getHousehold(), ((Applicant) otherEntity).getHousehold());
        equals = equals && valuesAreEqual(this.getMainIncome(), ((Applicant) otherEntity).getMainIncome());


        equals = equals && valuesAreEqual(this.getPersonalData(), ((Applicant) otherEntity).getPersonalData());
        equals = equals && valuesAreEqual(this.getEmail(), ((Applicant) otherEntity).getEmail());
        equals = equals && valuesAreEqual(this.getCreditScores(), ((Applicant) otherEntity).getCreditScores());

        equals = equals && valuesAreEqual(this.getBankAccount(), ((Applicant) otherEntity).getBankAccount());

        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(applicant)}

    /**
     * Returns a specific phone type for an applicant
     *
     * @param type the phone type
     * @return A @see Phone
     * @deprecated use {@link #retrievePhone(PhoneType type)} instead.
     */
    @Deprecated
    public Phone getPhoneForType(PhoneType type) {
        Phone result = null;

        for (Phone phone : getPhones()) {
            if (type.equals(phone.getPhoneType())
                    && (result == null || result.getCreationDate().before(phone.getCreationDate()))) {
                result = phone;
            }
        }

        return result;
    }

    /**
     * Returns a specific phone type for an applicant with latest CreationDate
     *
     * @param phoneType the phone type
     * @return A @see Phone
     */
    public Phone retrievePhone(PhoneType phoneType) {
        Phone result = null;

        if(getPhones() == null) {
            return result;
        }

        for (Phone phone : getPhones()) {
            if (phoneType.equals(phone.getPhoneType())
                    && (result == null || result.getCreationDate().before(phone.getCreationDate()))) {
                result = phone;
            }
        }

        return result;
    }

    /**
     * Returns a set of all Landline phones for an applicant
     * After all it is a Set
     *
     * @return A @see java.util.Set
     * @deprecated use {@link #retrieveLandlinePhones()} instead.
     */
    @Deprecated
    public Set<Phone> getLandlinePhones() {
        Set<Phone> phones = getPhones();
        Set<Phone> landlinePhones = new HashSet<Phone>();

        if (phones != null) {
            for (Phone phone : phones) {
                if (PhoneType.LANDLINE.equals(phone.getPhoneType())) {
                    landlinePhones.add(phone);
                }
            }
        }
        return landlinePhones;
    }

    /**
     * Returns first landline phones as applicant
     * After all it is a Set
     *
     * @return A @see Phone
     */
    public Phone retrieveLandlinePhone() {
        Set<Phone> phones = retrieveLandlinePhones();
        if (!phones.isEmpty()) {
            return phones.iterator().next();
        }

        return null;
    }

    /**
     * Returns a set of applicant landline phones
     * After all it is a Set
     *
     * @return A @see java.util.Set
     */
    public Set<Phone> retrieveLandlinePhones() {
        Set<Phone> phones = getPhones();
        Set<Phone> landlinePhones = new HashSet<Phone>();

        if (phones != null) {
            for (Phone phone : phones) {
                if (PhoneType.LANDLINE.equals(phone.getPhoneType())) {
                    landlinePhones.add(phone);
                }
            }
        }
        return landlinePhones;
    }

    /**
     * Returns a set of all mobile phones for an applicant
     * After all it is a Set
     *
     * @return A @see java.util.Set
     * @deprecated use {@link #retrieveMobilePhones()} instead.
     */
    @Deprecated
    public Set<Phone> getMobilePhones() {
        Set<Phone> phones = getPhones();
        Set<Phone> landlinePhones = new HashSet<Phone>();
        if (phones != null) {
            for (Phone phone : phones) {
                if (PhoneType.MOBILE.equals(phone.getPhoneType())) {
                    landlinePhones.add(phone);
                }
            }
        }
        return landlinePhones;
    }

    /**
     * Returns first mobile phones for an applicant
     * After all it is a Set
     *
     * @return A @see Phone
     */
    public Phone retrieveMobilePhone() {
        Set<Phone> phones = retrieveMobilePhones();
        if (!phones.isEmpty()) {
            return phones.iterator().next();
        }

        return null;
    }

    /**
     * Returns a set of all mobile phones for an applicant
     * After all it is a Set
     *
     * @return A @see java.util.Set
     */
    public Set<Phone> retrieveMobilePhones() {
        Set<Phone> phones = getPhones();
        Set<Phone> landlinePhones = new HashSet<Phone>();

        if (phones != null) {
            for (Phone phone : phones) {
                if (PhoneType.MOBILE.equals(phone.getPhoneType())) {
                    landlinePhones.add(phone);
                }
            }
        }

        return landlinePhones;
    }

    /**
     * Returns a set of all work phones for an applicant
     * After all it is a Set
     *
     * @return A @see java.util.Set
     * @deprecated use {@link #retrieveWorkPhones()} instead.
     */
    @Deprecated
    public Set<Phone> getWorkPhones() {
        Set<Phone> phones = getPhones();
        Set<Phone> workPhones = new HashSet<Phone>();
        if (phones != null) {
            for (Phone phone : phones) {
                if (PhoneType.WORK.equals(phone.getPhoneType())) {
                    workPhones.add(phone);
                }
            }
        }
        return workPhones;
    }

    /**
     * Return applicant work phones anapplicant
     * After all it is a Set
     *
     * @return A @see Phone
     */
    public Phone retrieveWorkPhone() {
        Set<Phone> phones = retrieveWorkPhones();
        if (!phones.isEmpty()) {
            return phones.iterator().next();
        }

        return null;
    }

    /**
     * Return a set of all applicant work phones
     * After all it is a Set
     *
     * @return A @see java.util.Set
     */
    public Set<Phone> retrieveWorkPhones() {
        Set<Phone> phones = getPhones();
        Set<Phone> workPhones = new HashSet<Phone>();

        if (phones != null) {
            for (Phone phone : phones) {
                if (PhoneType.WORK.equals(phone.getPhoneType())) {
                    workPhones.add(phone);
                }
            }
        }
        return workPhones;
    }

    /**
     * Checks whether an applicant is privately health insured
     * or Kassenpatient
     *
     * @return A @see java.lang.boolean
     * @deprecated This method is scheduled to be removed soon and use {@link #retrieveIsPrivatelyHealthInsured()} instead.
     */
    @Deprecated
    public boolean isPrivatelyHealthInsured() {
        Set<Expense> expenses = getExpenses();
        for (Expense expense : expenses) {
            if (ExpenseType.PRIVATE_HEALTH_INSURANCE.equals(expense.getExpenseType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether an applicant is privately health insured or Kassenpatient
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsPrivatelyHealthInsured() {
        return !retrieveExpenses(ExpenseType.PRIVATE_HEALTH_INSURANCE).isEmpty();
    }

    /**
     * Checks whether an applicant is living in his own estate
     *
     * @return A @see java.lang.boolean
     * @deprecated This method is scheduled to be removed soon and use {@link #retrieveIsLivingInOwnEstate()} instead.
     */
    @Deprecated
    public boolean isLivingInOwnEstate() {
        Set<Expense> expenses = getExpenses();
        for (Expense expense : expenses) {
            if (ExpenseType.HOUSING_COST.equals(expense.getExpenseType())
                    && expense instanceof Estate
                    && EstateRentType.NOT_RENTED.equals(((Estate) expense).getPartiallyRented())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks whether an applicant is living in his own estate
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsLivingInOwnEstate() {
        boolean isLivingInOwnEstate = false;

        List<Expense> ownedEstates = retrieveExpenses(Estate.class);
        List<Expense> housingCostExpenses = retrieveExpenses(ExpenseType.HOUSING_COST);

        for (Expense estateExpense : ownedEstates) {
            Estate estate = (Estate) estateExpense;
            if (!housingCostExpenses.isEmpty()
                    && housingCostExpenses.contains(estateExpense)
                    && EstateRentType.NOT_RENTED.equals(estate.getPartiallyRented())) {

                isLivingInOwnEstate = true;
            }
        }

        return isLivingInOwnEstate;
    }


    /**
     * @return true if the applicant owns Estate Property
     */
    public boolean isOwnsProperty(){
        return ! retrieveExpenses(Estate.class).isEmpty();
    }


    /**
     * Returns the private leasings for an applicant
     *
     * @return A @see java.util.List
     * @deprecated This method is scheduled to be removed soon and use {@link #retrievePrivateLeasings()} instead.
     */
    @Deprecated
    public List<Expense> getPrivateLeasings() {
        List<Expense> privateLeasings = new ArrayList<Expense>();
        Set<Expense> expenses = getExpenses();

        for (Expense expense : expenses) {
            if (ExpenseType.PRIVATE_LEASING.equals(expense.getExpenseType())) {
                privateLeasings.add(expense);
            }
        }
        return privateLeasings;
    }

    /**
     * Returns the private leasings for an applicant
     *
     * @return A @see java.util.List
     */
    public List<Expense> retrievePrivateLeasings() {
        return retrieveExpenses(ExpenseType.PRIVATE_LEASING);
    }

    /**
     * Returns the business leasing's for an applicant
     *
     * @return A @see java.util.List
     * @deprecated This method is scheduled to be removed soon and use {@link #retrieveBusinessLeasings()} instead.
     */
    @Deprecated
    public List<Expense> getBusinessLeasings() {
        List<Expense> businessLeasings = new ArrayList<Expense>();
        Set<Expense> expenses = getExpenses();

        for (Expense expense : expenses) {
            if (ExpenseType.BUSINESS_LEASING.equals(expense.getExpenseType())) {
                businessLeasings.add(expense);
            }
        }
        return businessLeasings;
    }

    /**
     * Returns the business leasing's for an applicant
     *
     * @return A @see java.util.List
     */
    public List<Expense> retrieveBusinessLeasings() {
        return retrieveExpenses(ExpenseType.BUSINESS_LEASING);
    }

    /**
     * Returns the investments for an applicant
     *
     * @return A @see java.util.List
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public List<Investment> getInvestments() {
        List<Investment> investments = new ArrayList<Investment>();
        Set<Expense> expenses = getExpenses();
        for (Expense expense : expenses) {
            if (ExpenseType.INVESTMENT.equals(expense.getExpenseType()) ||
                    expense instanceof Investment) {
                if (expense instanceof Investment) {
                    investments.add(((Investment) expense));
                }

            }
        }
        return investments;
    }

    /**
     * Returns the monthlyamount of not rented
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalEstateMonthlyAmountOfNotRented() instead.
     */
    @Deprecated
    public Double getEstateMonthlyAmountOfNotRented() {
        Set<Expense> expenses = getExpenses();
        for (Expense expense : expenses) {
            if (expense instanceof Estate &&
                    EstateRentType.NOT_RENTED.equals(((Estate) expense).getPartiallyRented())) {
                return ((Estate) expense).getMonthlyAmount();
            }
        }
        return null;
    }

    /**
     * Returns the monthly financing amount of fully rented
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalEstateMonthlyFinancingAmountOfFullyRented() instead.
     */
    @Deprecated
    public Double getEstateMonthlyFinancingAmountOfFullyRented() {
        Set<Expense> expenses = getExpenses();
        for (Expense expense : expenses) {
            if (expense instanceof Estate &&
                    (EstateRentType.FULLY_RENTED.equals(((Estate) expense).getPartiallyRented()) ||
                            EstateRentType.PARTIALLY_RENTED.equals(((Estate) expense).getPartiallyRented())) &&
                    ((Estate) expense).getEstateFinancing() != null) {
                return ((Estate) expense).getEstateFinancing().getMonthlyFinancingAmount();
            }
        }
        return null;
    }

    /**
     * Returns the monthly financing amount of not rented
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalEstateMonthlyFinancingAmountOfNotRented() instead.
     */
    @Deprecated
    public Double getEstateMonthlyFinancingAmountOfNotRented() {
        Set<Expense> expenses = getExpenses();
        for (Expense expense : expenses) {
            if (expense instanceof Estate &&
                    (EstateRentType.NOT_RENTED.equals(((Estate) expense).getPartiallyRented()) ||
                            EstateRentType.PARTIALLY_RENTED.equals(((Estate) expense).getPartiallyRented())) &&
                    ((Estate) expense).getEstateFinancing() != null) {
                return ((Estate) expense).getEstateFinancing().getMonthlyFinancingAmount();
            }
        }
        return null;
    }

    /**
     * Returns the employer of an applicant
     *
     * @return A @see Employer
     * @deprecated use {@link #retrieveEmployer()} instead.
     */
    @Deprecated
    public Employer getEmployer() {
        if (getMainIncome() instanceof SalariedStaff) {
            SalariedStaff salariedStaff = (SalariedStaff) getMainIncome();
            return salariedStaff.getEmployer();
        }
        return null;
    }

    /**
     * Returns the employer of an applicant
     *
     * @return A @see Employer
     */
    public Employer retrieveEmployer() {
        Employer employer = null;

        if (retrieveIsSalariedStaff()) {
            SalariedStaff salariedStaff = retrieveSalariedStaff();
            employer = salariedStaff.getEmployer();
        }

        if (retrieveIsRetiree()) {
            Retiree retiree = retrieveRetiree();
            employer = retiree.getRentInsurance();
        }

        return employer;
    }

    /**
     * Checks whether an Main Income is salaried or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsSalariedStaff() {
        return retrieveIsSalariedStaff(getMainIncome());
    }

    /**
     * Checks whether an any Income is salaried or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsSalariedStaff(Income income) {
        if (income instanceof SalariedStaff) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome SalariedStaff of an applicant
     *
     * @return A @see SalariedStaff
     */
    public SalariedStaff retrieveSalariedStaff() {
        if (retrieveIsSalariedStaff()) {
            return (SalariedStaff) getMainIncome();
        }
        return null;
    }

    /**
     * Returns the SalariedStaffType of an applicant
     *
     * @return A @see SalariedStaffType
     * @deprecated This method is scheduled to be removed soon and retrieveIsSalariedStaff as alternative method has been specified.
     */
    @Deprecated
    public SalariedStaffType getSalariedStaffType() {
        if (retrieveIsSalariedStaff()) {
            return retrieveSalariedStaff().getStaffType();
        }
        return null;
    }

    /**
     * Returns the SalariedStaffType of an applicant's additional income
     *
     * @return A @see SalariedStaffType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public SalariedStaffType getSalariedStaffTypeForAdditionalEmployment(Income aditionalIncome) {
        if (aditionalIncome instanceof SalariedStaff) {
            SalariedStaff salariedStaff = (SalariedStaff) aditionalIncome;
            return salariedStaff.getStaffType();
        }
        return null;
    }

    /**
     * Checks whether an Main Income is an employee or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsEmployee() {
        return retrieveIsEmployee(getMainIncome());
    }

    /**
     * Checks whether an any Income is an employee or not
     *
     * @param income income to check
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsEmployee(Income income) {
        if (income instanceof Employee) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome Employee of an applicant
     *
     * @return A @see Employee
     */
    public Employee retrieveEmployee() {
        if (retrieveIsEmployee()) {
            return (Employee) getMainIncome();
        }
        return null;
    }

    /**
     * Returns the employee type of an applicant
     *
     * @return A @see EmploymentType
     * @deprecated This method is scheduled to be removed soon and no new method is defined
     */
    @Deprecated
    public EmployeeType getEmployeeType() {
        if (getMainIncome() instanceof Employee) {
            Employee employee = (Employee) getMainIncome();
            return employee.getEmployeeType();
        }
        return null;
    }

    /**
     * Checks whether an Main Income is self-employed or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsSelfEmployed() {
        return retrieveIsSelfEmployed(getMainIncome());
    }

    /**
     * Checks whether any Income is for self-employed or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsSelfEmployed(Income income) {
        if (income instanceof SelfEmployed) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome SelfEmployed of an applicant
     *
     * @return A @see SelfEmployed
     */
    public SelfEmployed retrieveSelfEmployed() {
        if (retrieveIsSelfEmployed()) {
            return (SelfEmployed) getMainIncome();
        }
        return null;
    }

    /**
     * Checks whether an Main Income is freelancer or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsFreelancer() {
        return retrieveIsFreelancer(getMainIncome());
    }

    /**
     * Checks whether any Income is for freelancer or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsFreelancer(Income income) {
        if (income instanceof Freelancer) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome Freelancer of an applicant
     *
     * @return A @see Freelancer
     */
    public Freelancer retrieveFreelancer() {
        if (retrieveIsFreelancer()) {
            return (Freelancer) getMainIncome();
        }
        return null;
    }

    /**
     * Checks whether an Main Income is a soldier or (better)not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsSoldier() {
        return retrieveIsSoldier(getMainIncome());
    }

    /**
     * Checks whether an Any Income is a soldier or (better)not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsSoldier(Income income) {
        if (income instanceof Soldier) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome Soldier of an applicant
     *
     * @return A @see Soldier
     */
    public Soldier retrieveSoldier() {
        if (retrieveIsSoldier()) {
            return (Soldier) getMainIncome();
        }
        return null;
    }

    /**
     * Returns the soldier type
     *
     * @return A @see SoldierType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public SoldierType getSoldierType() {
        Soldier soldier = (Soldier) getMainIncome();
        return soldier.getSoldierType();
    }

    /**
     * Checks whether an Main Income is an official or (better)not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsOfficial() {
        return retrieveIsOfficial(getMainIncome());
    }

    /**
     * Checks whether an Any Income is an official or (better)not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsOfficial(Income income) {
        if (income instanceof Official) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome Official of an applicant
     *
     * @return A @see Official
     */
    public Official retrieveOfficial() {
        if (retrieveIsOfficial()) {
            return (Official) getMainIncome();
        }
        return null;
    }

    /**
     * Checks whether an Main Income is an retiree or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsRetiree() {
        return retrieveIsRetiree(getMainIncome());
    }

    /**
     * Checks whether an Any Income is an retiree or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsRetiree(Income income) {
        if (income instanceof Retiree) {
            return true;
        }
        return false;
    }

    /**
     * Returns the MainIncome Retiree of an applicant
     *
     * @return A @see Retiree
     */
    public Retiree retrieveRetiree() {
        if (retrieveIsRetiree()) {
            return (Retiree) getMainIncome();
        }
        return null;
    }

    /**
     * Checks whether an applicant is a pensioner or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsPensioner() {
        if (retrieveIsRetiree()) {
            Retiree retiree = retrieveRetiree();

            if (RetireeType.PENSIONER.equals(retiree.getRetireeType())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the pension type
     *
     * @return A @see PensionType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public PensionType getPensionType() {
        if (getMainIncome() instanceof Retiree) {
            Retiree retiree = (Retiree) getMainIncome();
            if (retiree.getPensionType() != null) {
                return retiree.getPensionType();
            }
        }
        return null;
    }

    /**
     * Returns the retiree type
     *
     * @return A @see RetireeType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public RetireeType getRetireeType() {
        if (getMainIncome() instanceof Retiree) {
            Retiree retiree = (Retiree) getMainIncome();
            if (retiree.getRetireeType() != null) {
                return retiree.getRetireeType();
            }
        }
        return null;
    }

    /**
     * Checks whether an applicant is in probation period
     *
     * @return A @see java.lang.boolean
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public boolean isInProbationPeriod() {
        if (getMainIncome() instanceof Employee) {
            Employee employee = (Employee) getMainIncome();
            if (employee.getProbationPeriod() != null) {
                return employee.getProbationPeriod();
            }
        }
        return false;
    }

    /**
     * Returns the date of the end of the probation period
     *
     * @return A @see java.util.date
     * @deprecated This method is scheduled to be removed soon, use Helper:getEndOfProbationPeriod() instead.
     */
    @Deprecated
    public Date getEndOfProbationPeriod() {
        if (getMainIncome() instanceof SalariedStaff) {
            SalariedStaff salariedStaff = (SalariedStaff) getMainIncome();
            return salariedStaff.getEndOfProbationPeriod();
        }
        return null;
    }

    /**
     * Returns the date of the start of the occupation
     *
     * @return A @see java.util.date
     * @deprecated This method is scheduled to be removed soon and use Helper:getEmployedSince() instead
     */
    @Deprecated
    public Date getStartOfOccupation() {
        if (getMainIncome() instanceof SalariedStaff) {
            SalariedStaff salariedStaff = (SalariedStaff) getMainIncome();
            return salariedStaff.getEmployedSince();
        }
        if (getMainIncome() instanceof SelfEmployed) {
            SelfEmployed selfEmployed = (SelfEmployed) getMainIncome();
            return selfEmployed.getStartOfBusiness();
        }
        return null;
    }

    /**
     * Checks whether an applicant is required to pay trade taxes
     *
     * @return A @see java.lang.boolean
     * @deprecated This method is scheduled to be removed soon and use Helper:getTradeTaxesRequired instead
     */
    @Deprecated
    public boolean getTradeTaxesRequired() {
        if (getMainIncome() instanceof SelfEmployed) {
            SelfEmployed selfEmployed = (SelfEmployed) getMainIncome();
            return selfEmployed.getTradeTaxesRequired();
        }
        return false;
    }

    /**
     * Checks whether an applicant is fanatic enough to pay church taxes
     *
     * @return A @see java.lang.boolean
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public boolean getChurchTaxesRequired() {
        if (getMainIncome() instanceof SelfEmployed) {
            SelfEmployed selfEmployed = (SelfEmployed) getMainIncome();
            return selfEmployed.getChurchTaxesRequired();
        }
        return false;
    }

    /**
     * Checks whether an applicant is temporarily employed
     *
     * @return A @see java.lang.boolean
     * @deprecated use Helper:getIsTemporarilyEmployed() instead.
     */
    @Deprecated
    public boolean isTemporarilyEmployed() {
        if (getMainIncome() instanceof Employee) {
            Employee employee = (Employee) getMainIncome();
            return employee.getTemporarilyEmployed();
        }
        return false;
    }

    /**
     * Returns the date of the end of a temporary employment
     *
     * @return A @see java.util.date
     * @deprecated This method is scheduled to be removed soon and use Helper:getEndOfTemporaryEmployment() instead.
     */
    @Deprecated
    public Date getEndOfTemporaryEmployment() {
        if (retrieveIsEmployee()) {
            Employee employee = retrieveEmployee();
            return employee.getEndOfTemporaryEmployment();
        }
        return null;
    }

    /**
     * Checks whether an Main Income is an employment or not
     *
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsEmployment() {
        return retrieveIsEmployment(getMainIncome());
    }

    /**
     * Checks whether an any Income is an employment or not
     *
     * @param income income to check
     * @return A @see java.lang.boolean
     */
    public boolean retrieveIsEmployment(Income income) {
        if (income instanceof Employment) {
            return true;
        }
        return false;
    }

    /**
     * Returns the employment of an applicant
     *
     * @return A @see Employment
     */
    public Employment retrieveEmployment() {
        if (retrieveIsEmployment()) {
            return (Employment) getMainIncome();
        }
        return null;
    }

    /**
     * Returns the FreelancerOccupationType of an applicant
     *
     * @return A @see FreelancerOccupationType
     * @deprecated This method is scheduled to be removed soon and now new function defined
     */
    @Deprecated
    public FreelancerOccupationType getFreelancerOccupationType() {
        if (getMainIncome() instanceof Freelancer) {
            Freelancer freelancer = (Freelancer) getMainIncome();
            return freelancer.getOccupationType();
        }
        return null;
    }

    /**
     * Returns the OfficialServiceGradeType of an applicant
     *
     * @return A @see OfficialServiceGradeType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public OfficialServiceGradeType getOfficialServiceGradeType() {
        if (getMainIncome() instanceof Official) {
            Official official = (Official) getMainIncome();
            return official.getOfficialServiceGradeType();
        }
        return null;
    }

    /**
     * Returns the self employed expenses
     *
     * @return A @see java.util.Set
     * @deprecated This method is scheduled to be removed soon and and no other method defined
     */
    @Deprecated
    public Set<SelfEmployedIncomeExpense> getSelfEmployedIncomeExpenses() {
        if (getMainIncome() instanceof SelfEmployed) {
            SelfEmployed selfemployed = (SelfEmployed) getMainIncome();
            return selfemployed.getYearlyCalculations();
        }
        return null;
    }

    /**
     * Returns the fully rented estate type
     *
     * @return A @see EstateType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public EstateType getFullyRentedEstateType() {

        assert (getExpenses() != null);
        for (Expense expense : getExpenses()) {
            if (expense instanceof Estate && EstateRentType.FULLY_RENTED.equals(((Estate) expense).getPartiallyRented())) {
                return ((Estate) expense).getEstateType();
            }
        }
        return null;
    }

    /**
     * Returns the fully rented square meters used
     *
     * @return A @see java.lang.Double
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public Double getFullyRentedSquaremetersUsed() {

        assert (getExpenses() != null);
        for (Expense expense : getExpenses()) {
            if (expense instanceof Estate &&
                    (EstateRentType.FULLY_RENTED.equals(((Estate) expense).getPartiallyRented()))) {
                return ((Estate) expense).getSquaremetersUsed();
            }
        }
        return null;
    }

    /**
     * Returns the not rented estate type
     *
     * @return A @see EstateType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public EstateType getNotRentedEstateType() {

        assert (getExpenses() != null);
        for (Expense expense : getExpenses()) {
            if (expense instanceof Estate &&
                    (EstateRentType.NOT_RENTED.equals(((Estate) expense).getPartiallyRented()))) {
                return ((Estate) expense).getEstateType();
            }
        }
        return null;
    }

    /**
     * Returns the not rented square meters used
     *
     * @return A @see java.lang.Double
     * @deprecated This method is scheduled to be removed soon use Helper:getNotRentedSquaremetersUsed() instead.
     */
    @Deprecated
    public Double getNotRentedSquaremetersUsed() {

        assert (getExpenses() != null);
        for (Expense expense : getExpenses()) {
            if (expense instanceof Estate &&
                    (EstateRentType.NOT_RENTED.equals(((Estate) expense).getPartiallyRented()))) {
                return ((Estate) expense).getSquaremetersUsed();
            }
        }
        return null;
    }

    /**
     * Returns the OtherHousingCostType
     *
     * @return A @see OtherHousingCostType
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public OtherHousingCostType getHousingCostType() {

        assert (getExpenses() != null);
        for (Expense expense : getExpenses()) {
            if (expense instanceof OtherHousingCost) {
                return ((OtherHousingCost) expense).getOtherHousingCostType();
            }
        }
        return null;
    }

    /**
     * Returns the IndustrySectorType
     *
     * @return A @see IndustrySectorType
     * @deprecated This method is scheduled to be removed soon use Helper:getIndustrySectorType() instead.
     */
    @Deprecated
    public IndustrySectorType getIndustrySectorType() {

        if (getMainIncome() instanceof SalariedStaff) {
            SalariedStaff salariedStaff = (SalariedStaff) getMainIncome();
            return salariedStaff.getIndustrySector();
        }
        if (getMainIncome() instanceof SelfEmployed) {
            SelfEmployed selfEmployed = (SelfEmployed) getMainIncome();
            return selfEmployed.getIndustrySector();
        }
        return null;
    }

    /**
     * Returns the IndustrySectorType
     *
     * @return A @see IndustrySectorType
     */
    public IndustrySectorType retrieveIndustrySectorType() {
        if (retrieveIsSalariedStaff()) {
            SalariedStaff salariedStaff = retrieveSalariedStaff();
            return salariedStaff.getIndustrySector();
        }
        if (retrieveIsSelfEmployed()) {
            SelfEmployed selfEmployed = retrieveSelfEmployed();
            return selfEmployed.getIndustrySector();
        }
        return null;
    }    

    /**
     * Returns the ReceivingChildAlimony
     *
     * @return A @see java.lang.Double
     * @deprecated This method is scheduled to be removed soon use helper:getTotalReceivingChildAlimony() instead.
     */
    @Deprecated
    public Double getReceivingChildAlimony() {
        if (IncomeType.CHILD_ALIMONY.equals(getMainIncome().getIncomeType())) {
            return getMainIncome().getMonthlyNetIncome();
        }
        Set<Income> aditionalIncomes = getAdditionalIncomes();
        if (aditionalIncomes != null) {
            for (Income aditionalIncome : aditionalIncomes) {
                if (IncomeType.CHILD_ALIMONY.equals(aditionalIncome.getIncomeType())) {
                    return aditionalIncome.getMonthlyNetIncome();
                }
            }
        }
        return null;
    }

    /**
     * Returns the receiving ChildBenefit
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalReceivingChildBenefit() instead.
     */
    @Deprecated
    public Double getReceivingChildBenefit() {
        if (IncomeType.CHILD_BENEFIT.equals(getMainIncome().getIncomeType())) {
            return getMainIncome().getMonthlyNetIncome();
        }
        Set<Income> incomes = getAdditionalIncomes();
        if (incomes != null) {
            for (Income income : incomes) {
                if (IncomeType.CHILD_BENEFIT.equals(income.getIncomeType())) {
                    return income.getMonthlyNetIncome();
                }
            }
        }
        return null;
    }

    /**
     * Returns the paying ChildAlimony
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalPayingChildAlimony() instead.
     */
    @Deprecated
    public Double getPayingChildAlimony() {
        for (Expense expense : getExpenses()) {
            if (ExpenseType.CHILD_ALIMONY.equals(expense.getExpenseType())) {
                return expense.getMonthlyAmount();
            }
        }
        return null;
    }

    /**
     * Returns the paying SpouseAlimony
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalPayingSpouseAlimony() instead.
     */
    @Deprecated
    public Double getPayingSpouseAlimony() {
        for (Expense expense : getExpenses()) {
            if (ExpenseType.SPOUSE_ALIMONY.equals(expense.getExpenseType())) {
                return expense.getMonthlyAmount();
            }
        }
        return null;
    }

    /**
     * Returns the total rent income
     *
     * @return A @see java.lang.Double
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public Double getTotalRentalIncome() {
        Double totalRentalIncome = 0D;
        if (IncomeType.RENTAL_INCOME.equals(getMainIncome().getIncomeType())
                && null != getMainIncome().getMonthlyNetIncome()) {
            totalRentalIncome += getMainIncome().getMonthlyNetIncome();
        }
        Set<Income> aditionalIncomes = getAdditionalIncomes();
        if (aditionalIncomes != null) {
            for (Income aditionalIncome : aditionalIncomes) {
                if (IncomeType.RENTAL_INCOME.equals(aditionalIncome.getIncomeType())
                        && null != aditionalIncome.getMonthlyNetIncome()) {
                    totalRentalIncome += aditionalIncome.getMonthlyNetIncome();
                }
            }
        }
        return totalRentalIncome;
    }

    /**
     * Returns the ReceivingSpouseAlimony
     *
     * @return A @see java.lang.Double
     * @deprecated This method is scheduled to be removed soon use Helper:getTotalReceivingSpouseAlimony() instead.
     */
    @Deprecated
    public Double getReceivingSpouseAlimony() {
        Double totalSpouseAlimony = 0D;

        if (IncomeType.SPOUSE_ALIMONY.equals(getMainIncome().getIncomeType()) &&
                null != getMainIncome().getMonthlyNetIncome()) {
            totalSpouseAlimony += getMainIncome().getMonthlyNetIncome();
        }

        if (getAdditionalIncomes() != null) {
            for (Income aditionalIncome : getAdditionalIncomes()) {
                if (IncomeType.SPOUSE_ALIMONY.equals(aditionalIncome.getIncomeType())
                        && null != aditionalIncome.getMonthlyNetIncome()) {
                    totalSpouseAlimony += aditionalIncome.getMonthlyNetIncome();
                }
            }
        }
        return totalSpouseAlimony;
    }

    /**
     * Returns the OtherPensionIncomes
     *
     * @return A @see java.lang.Double
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public Double getOtherPensionIncomes() {
        Double totalPensionIncome = 0D;

        if (IncomeType.PENSION_SUPPLEMENT.equals(getMainIncome().getIncomeType())
                && null != getMainIncome().getMonthlyNetIncome()) {
            totalPensionIncome += getMainIncome().getMonthlyNetIncome();
        }

        if (getAdditionalIncomes() != null) {
            for (Income aditionalIncome : getAdditionalIncomes()) {
                if (IncomeType.PENSION_SUPPLEMENT.equals(aditionalIncome.getIncomeType())
                        && null != aditionalIncome.getMonthlyNetIncome()) {
                    totalPensionIncome += aditionalIncome.getMonthlyNetIncome();
                }

            }
        }
        return totalPensionIncome;
    }

    /**
     * Returns the expenses for private health insurance
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalExpensesPrivateHealthInsurance() instead.
     */
    @Deprecated
    public Double getExpensesPrivateHealthInsurance() {
        for (Expense expense : getExpenses()) {
            if (ExpenseType.PRIVATE_HEALTH_INSURANCE.equals(expense.getExpenseType())) {
                return expense.getMonthlyAmount();
            }
        }
        return null;
    }

    /**
     * Returns the monthly rent amount for an applicant
     *
     * @return A @see java.lang.Double
     * @deprecated use Helper:getTotalMonthlyRentAmount() instead.
     */
    @Deprecated
    public Double getMonthlyRentAmount() {
        Double totalRent = 0D;

        for (Expense expense : getExpenses()) {
            if (ExpenseType.HOUSING_COST.equals(expense.getExpenseType()) &&
                    !(expense instanceof Estate)
                    && null != expense.getMonthlyAmount()) {
                totalRent += expense.getMonthlyAmount();
            }
        }
        return totalRent;
    }

    /**
     * Checks whether an applicant has thrid party loans
     *
     * @return A @see java.lang.boolean
     * @deprecated This method is scheduled to be removed soon and no alternative method has been specified.
     */
    @Deprecated
    public boolean checkForSharedLoans() {
        Set<ThirdPartyLoan> thirdPartyLoans_FirstApplicant = getThirdPartyLoans();

        if (thirdPartyLoans_FirstApplicant != null && thirdPartyLoans_FirstApplicant.size() > 0) {
            for (ThirdPartyLoan thirdPartyLoan : thirdPartyLoans_FirstApplicant) {
                if (thirdPartyLoan != null &&
                        thirdPartyLoan.getSameCreditBankAccount() != null &&
                        thirdPartyLoan.getSameCreditBankAccount()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get list of Expenses that matches specified ExpenseTypes as parameter.
     * 0 in case expense is not found.
     *
     * @param ExpenseTypeList containing list of reqested type
     * @return expensesWithRequestedType list of Expense with specified type
     */
    public List<Expense> retrieveExpenses(List<ExpenseType> ExpenseTypeList) {
        List<Expense> expensesWithRequestedType = new ArrayList<Expense>();

        if (getExpenses() != null) {
            for (Expense expense : getExpenses()) {
                if (expense.getExpenseType() != null && ExpenseTypeList.contains(expense.getExpenseType())) {
                    expensesWithRequestedType.add(expense);
                }
            }
        }
        return expensesWithRequestedType;
    }

     /**
     * Get list of Expenses that matches specified ExpenseType as parameter.
     * 0 in case expense is not found.
     *
     * @param type containing reqested type
     * @return expensesWithRequestedType list of Expense with specified type
     */
    public List<Expense> retrieveExpenses(ExpenseType type) {
        List<Expense> expensesWithRequestedType = new ArrayList<Expense>();

        if (getExpenses() != null) {
            for (Expense expense : getExpenses()) {
                if (expense.getExpenseType() != null && type.equals(expense.getExpenseType())) {
                    expensesWithRequestedType.add(expense);
                }
            }
        }
        return expensesWithRequestedType;
    }

    /**
     * @param myInstance containing reqested Class
     * @return expensesWithRequestedType list of Expense with specified class
     */
    public List<Expense> retrieveExpenses(Class myInstance) {
        List<Expense> expensesWithRequestedInstance = new ArrayList<Expense>();

        if (getExpenses() != null) {
            for (Expense expense : getExpenses()) {
                if (myInstance.isInstance(expense)) {
                    expensesWithRequestedInstance.add(expense);
                }
            }
        }
        return expensesWithRequestedInstance;
    }

    /**
     * @param type containing reqested type
     * @return expensesWithRequestedType list of Expense with specified type
     */
    public List<Expense> retrieveExpenses(EstateRentType type) {
        List<Expense> expensesWithRequestedType = new ArrayList<Expense>();

        if (getExpenses() != null) {
            for (Expense expense : getExpenses()) {
                if (expense instanceof Estate && type.equals(((Estate) expense).getPartiallyRented())) {
                    expensesWithRequestedType.add(expense);
                }
            }
        }
        return expensesWithRequestedType;
    }

    /**
     * Returns true if Main incomes if matching incomeType
     *
     * @param incomeType limit output value to one type only
     * @return A boolean
     */
    public boolean retrieveIsMainIncome(IncomeType incomeType) {
        if (getMainIncome() != null &&
                incomeType.equals(getMainIncome().getIncomeType())) {

            return true;
        }

        return false;
    }

    /**
     * Returns List of Additional Incomes that matching specified incomeType
     *
     * @param incomeTypeList limit output value to selected types
     * @return A @see @see java.util.List
     */
    public List<Income> retrieveAdditionalIncome(List<IncomeType> incomeTypeList) {
        List<Income> incomes = new ArrayList<Income>();

        if (getAdditionalIncomes() != null) {
            for (Income income : getAdditionalIncomes()) {
                if (incomeTypeList.contains(income.getIncomeType())) {
                    incomes.add(income);
                }
            }
        }
        return incomes;
    }

     /**
     * Returns List of Additional Incomes that matching specified incomeType
     *
     * @param incomeType limit output value to one type only
     * @return A @see @see java.util.List
     */
    public List<Income> retrieveAdditionalIncome(IncomeType incomeType) {
        List<Income> incomes = new ArrayList<Income>();

        if (getAdditionalIncomes() != null) {
            for (Income income : getAdditionalIncomes()) {
                if (incomeType.equals(income.getIncomeType())) {
                    incomes.add(income);
                }
            }
        }
        return incomes;
    }

    /**
     * @param myInstance containing requested Class
     * @return incomesWithRequestedInstance list of Income with specified class
     */
    public List<Income> retrieveAdditionalIncome(Class myInstance) {
        List<Income> incomesWithRequestedInstance = new ArrayList<Income>();

        if (getAdditionalIncomes() != null) {
            for (Income income : getAdditionalIncomes()) {
                if (myInstance.isInstance(income)) {
                    incomesWithRequestedInstance.add(income);
                }
            }
        }
        return incomesWithRequestedInstance;
    }

    /**
     * @param estateRentTypes     containing reqested types set
     * @return boolean            whether estateRentType was included in expense
     */
    public boolean retrieveIsExpenseMatchingPartiallyRentedEstateRentType(List<EstateRentType> estateRentTypes) {
        Estate estate;

        if(getExpenses() == null) {
            return false;
        }

        for (Expense expense : getExpenses()) {
            if (expense instanceof Estate) {
                estate = (Estate) expense;
                if (estateRentTypes.contains(estate.getPartiallyRented())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param otherHousingCostTypes containing reqested types set
     * @return boolean              whether otherHousingCostType was included in expense
     */
    public boolean retrieveIsExpenseMatchingOtherHousingCostType(List<OtherHousingCostType> otherHousingCostTypes) {
        OtherHousingCost otherHousingCost;

        if(getExpenses() == null) {
            return false;
        }

        for (Expense expense : getExpenses()) {
            if (expense instanceof OtherHousingCost) {
                otherHousingCost = (OtherHousingCost) expense;
                if (otherHousingCostTypes.contains(otherHousingCost.getOtherHousingCostType())) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Collect thirdPartyLoans by loan source. If source is null (not empty but null), then return all thirdPartyLoans
     * @param thirdPartyLoanSources containing loan sources set
     * @return Set<ThirdPartyLoan> collected thirdPartyLoan by loan source
     */
    public Set<ThirdPartyLoan> retrieveThirdPartyLoans(List<ThirdPartyLoanSource> thirdPartyLoanSources) {
        Set<ThirdPartyLoan> thirdPartyLoans = new HashSet<ThirdPartyLoan>();

        if (thirdPartyLoanSources == null) {
            return getThirdPartyLoans();
        }

        if (getThirdPartyLoans() != null) {
            for (ThirdPartyLoan thirdPartyLoan : getThirdPartyLoans()) {
                if (thirdPartyLoanSources.contains(thirdPartyLoan.getLoanSource())) {
                    thirdPartyLoans.add(thirdPartyLoan);
                }
            }
        }

        return thirdPartyLoans;
    }


    // !!!!!!!! End of insert code section !!!!!!!!
}

