//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(nationality info)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.domain.NationalityInfo;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'NationalityInfos'.
 *
 * @author generator
 */
public interface NationalityInfoDao extends BrokerageSchemaDao<NationalityInfo> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the nationality info identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    NationalityInfo getNationalityInfo(Long id);

    /**
     * Saves the nationality info.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveNationalityInfo(NationalityInfo nationalityInfo);

    /**
     * Deletes an nationality info, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the nationality info
     */
    void deleteNationalityInfo(Long id);

    /**
     * Retrieves all 'NationalityInfo' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<NationalityInfo> getNationalityInfoList();

    /**
     * Retrieves a page of 'NationalityInfo' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<NationalityInfo> getNationalityInfoList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'NationalityInfo' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<NationalityInfo> getNationalityInfoList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'NationalityInfo' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<NationalityInfo> getNationalityInfoList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'NationalityInfo' instances.
     */
    long getNationalityInfoCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(nationality info)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
