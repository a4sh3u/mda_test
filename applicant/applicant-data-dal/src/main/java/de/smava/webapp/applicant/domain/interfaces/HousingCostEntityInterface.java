package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.HousingCost;


/**
 * The domain object that represents 'HousingCosts'.
 *
 * @author generator
 */
public interface HousingCostEntityInterface {

    /**
     * Helper method to get reference of this object as model type.
     */
    HousingCost asHousingCost();
}
