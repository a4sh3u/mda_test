package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Ceo;


/**
 * The domain object that represents 'Ceos'.
 *
 * @author generator
 */
public interface CeoEntityInterface {

    /**
     * Helper method to get reference of this object as model type.
     */
    Ceo asCeo();
}
