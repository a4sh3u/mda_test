//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(applicants relationship)}
import de.smava.webapp.applicant.domain.history.ApplicantsRelationshipHistory;
import de.smava.webapp.applicant.type.ApplicantsRelationshipType;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ApplicantsRelationships'.
 *
 * 
 *
 * @author generator
 */
public class ApplicantsRelationship extends ApplicantsRelationshipHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(applicants relationship)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Applicant _applicant1;
        protected Applicant _applicant2;
        protected de.smava.webapp.applicant.type.ApplicantsRelationshipType _relationshipType;
        protected Boolean _sharedHousehold;
        
                                    
    /**
     * Setter for the property 'applicant1'.
     *
     * 
     *
     */
    public void setApplicant1(Applicant applicant1) {
        _applicant1 = applicant1;
    }
            
    /**
     * Returns the property 'applicant1'.
     *
     * 
     *
     */
    public Applicant getApplicant1() {
        return _applicant1;
    }
                                            
    /**
     * Setter for the property 'applicant2'.
     *
     * 
     *
     */
    public void setApplicant2(Applicant applicant2) {
        _applicant2 = applicant2;
    }
            
    /**
     * Returns the property 'applicant2'.
     *
     * 
     *
     */
    public Applicant getApplicant2() {
        return _applicant2;
    }
                                    /**
     * Setter for the property 'relationshipType'.
     *
     * 
     *
     */
    public void setRelationshipType(de.smava.webapp.applicant.type.ApplicantsRelationshipType relationshipType) {
        if (!_relationshipTypeIsSet) {
            _relationshipTypeIsSet = true;
            _relationshipTypeInitVal = getRelationshipType();
        }
        registerChange("relationshipType", _relationshipTypeInitVal, relationshipType);
        _relationshipType = relationshipType;
    }
                        
    /**
     * Returns the property 'relationshipType'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.ApplicantsRelationshipType getRelationshipType() {
        return _relationshipType;
    }
                                    /**
     * Setter for the property 'shared household'.
     *
     * 
     *
     */
    public void setSharedHousehold(Boolean sharedHousehold) {
        if (!_sharedHouseholdIsSet) {
            _sharedHouseholdIsSet = true;
            _sharedHouseholdInitVal = getSharedHousehold();
        }
        registerChange("shared household", _sharedHouseholdInitVal, sharedHousehold);
        _sharedHousehold = sharedHousehold;
    }
                        
    /**
     * Returns the property 'shared household'.
     *
     * 
     *
     */
    public Boolean getSharedHousehold() {
        return _sharedHousehold;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_applicant1 instanceof de.smava.webapp.commons.domain.Entity && !_applicant1.getChangeSet().isEmpty()) {
             for (String element : _applicant1.getChangeSet()) {
                 result.add("applicant1 : " + element);
             }
         }

         if (_applicant2 instanceof de.smava.webapp.commons.domain.Entity && !_applicant2.getChangeSet().isEmpty()) {
             for (String element : _applicant2.getChangeSet()) {
                 result.add("applicant2 : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ApplicantsRelationship.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _relationshipType=").append(_relationshipType);
            builder.append("\n}");
        } else {
            builder.append(ApplicantsRelationship.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ApplicantsRelationship asApplicantsRelationship() {
        return this;
    }
}
