//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(freelancer)}
import de.smava.webapp.applicant.domain.history.FreelancerHistory;
import de.smava.webapp.applicant.type.FreelancerOccupationType;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Freelancers'.
 *
 * 
 *
 * @author generator
 */
public class Freelancer extends FreelancerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(freelancer)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.FreelancerOccupationType _occupationType;
        
                            /**
     * Setter for the property 'occupationType'.
     *
     * 
     *
     */
    public void setOccupationType(de.smava.webapp.applicant.type.FreelancerOccupationType occupationType) {
        if (!_occupationTypeIsSet) {
            _occupationTypeIsSet = true;
            _occupationTypeInitVal = getOccupationType();
        }
        registerChange("occupationType", _occupationTypeInitVal, occupationType);
        _occupationType = occupationType;
    }
                        
    /**
     * Returns the property 'occupationType'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.FreelancerOccupationType getOccupationType() {
        return _occupationType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Freelancer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _occupationType=").append(_occupationType);
            builder.append("\n}");
        } else {
            builder.append(Freelancer.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Freelancer asFreelancer() {
        return this;
    }
}
