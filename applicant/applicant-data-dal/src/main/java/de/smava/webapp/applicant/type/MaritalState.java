package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 24.02.15.
 */
public enum MaritalState {

    MARRIED,
    WIDOWED,
    UNMARRIED,
    DIVORCED,
    COHABITATION,
    SEPARATED;

}
