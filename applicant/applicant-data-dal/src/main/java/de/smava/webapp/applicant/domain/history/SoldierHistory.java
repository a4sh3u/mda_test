package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSoldier;




/**
 * The domain object that has all history aggregation related fields for 'Soldiers'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SoldierHistory extends AbstractSoldier {

    protected transient de.smava.webapp.applicant.type.SoldierType _soldierTypeInitVal;
    protected transient boolean _soldierTypeIsSet;


	
    /**
     * Returns the initial value of the property 'soldier type'.
     */
    public de.smava.webapp.applicant.type.SoldierType soldierTypeInitVal() {
        de.smava.webapp.applicant.type.SoldierType result;
        if (_soldierTypeIsSet) {
            result = _soldierTypeInitVal;
        } else {
            result = getSoldierType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'soldier type'.
     */
    public boolean soldierTypeIsDirty() {
        return !valuesAreEqual(soldierTypeInitVal(), getSoldierType());
    }

    /**
     * Returns true if the setter method was called for the property 'soldier type'.
     */
    public boolean soldierTypeIsSet() {
        return _soldierTypeIsSet;
    }

}
