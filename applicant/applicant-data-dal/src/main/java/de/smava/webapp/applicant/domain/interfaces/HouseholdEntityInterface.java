package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Household;
import de.smava.webapp.applicant.type.DebtCreditCardsType;


/**
 * The domain object that represents 'Households'.
 *
 * @author generator
 */
public interface HouseholdEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(java.util.Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    java.util.Date getCreationDate();
    /**
     * Setter for the property 'number of children'.
     *
     * 
     *
     */
    void setNumberOfChildren(Integer numberOfChildren);

    /**
     * Returns the property 'number of children'.
     *
     * 
     *
     */
    Integer getNumberOfChildren();
    /**
     * Setter for the property 'number of persons in household'.
     *
     * 
     *
     */
    void setNumberOfPersonsInHousehold(Integer numberOfPersonsInHousehold);

    /**
     * Returns the property 'number of persons in household'.
     *
     * 
     *
     */
    Integer getNumberOfPersonsInHousehold();
    /**
     * Setter for the property 'number of cars'.
     *
     * 
     *
     */
    void setNumberOfCars(Integer numberOfCars);

    /**
     * Returns the property 'number of cars'.
     *
     * 
     *
     */
    Integer getNumberOfCars();
    /**
     * Setter for the property 'number of motorbikes'.
     *
     * 
     *
     */
    void setNumberOfMotorbikes(Integer numberOfMotorbikes);

    /**
     * Returns the property 'number of motorbikes'.
     *
     * 
     *
     */
    Integer getNumberOfMotorbikes();
    /**
     * Setter for the property 'dept credit cards'.
     *
     * 
     *
     */
    void setDeptCreditCards(DebtCreditCardsType deptCreditCards);

    /**
     * Returns the property 'dept credit cards'.
     *
     * 
     *
     */
    DebtCreditCardsType getDeptCreditCards();
    /**
     * Helper method to get reference of this object as model type.
     */
    Household asHousehold();
}
