package de.smava.webapp.applicant.domain.history;


import de.smava.webapp.applicant.domain.abstracts.AbstractHousehold;
import de.smava.webapp.applicant.type.DebtCreditCardsType;



/**
 * The domain object that has all history aggregation related fields for 'Households'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class HouseholdHistory extends AbstractHousehold {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient Integer _numberOfChildrenInitVal;
    protected transient boolean _numberOfChildrenIsSet;
    protected transient Integer _numberOfPersonsInHouseholdInitVal;
    protected transient boolean _numberOfPersonsInHouseholdIsSet;
    protected transient Integer _numberOfCarsInitVal;
    protected transient boolean _numberOfCarsIsSet;
    protected transient Integer _numberOfMotorbikesInitVal;
    protected transient boolean _numberOfMotorbikesIsSet;
    protected transient DebtCreditCardsType _deptCreditCardsInitVal;
    protected transient boolean _deptCreditCardsIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of children'.
     */
    public Integer numberOfChildrenInitVal() {
        Integer result;
        if (_numberOfChildrenIsSet) {
            result = _numberOfChildrenInitVal;
        } else {
            result = getNumberOfChildren();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of children'.
     */
    public boolean numberOfChildrenIsDirty() {
        return !valuesAreEqual(numberOfChildrenInitVal(), getNumberOfChildren());
    }

    /**
     * Returns true if the setter method was called for the property 'number of children'.
     */
    public boolean numberOfChildrenIsSet() {
        return _numberOfChildrenIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of persons in household'.
     */
    public Integer numberOfPersonsInHouseholdInitVal() {
        Integer result;
        if (_numberOfPersonsInHouseholdIsSet) {
            result = _numberOfPersonsInHouseholdInitVal;
        } else {
            result = getNumberOfPersonsInHousehold();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of persons in household'.
     */
    public boolean numberOfPersonsInHouseholdIsDirty() {
        return !valuesAreEqual(numberOfPersonsInHouseholdInitVal(), getNumberOfPersonsInHousehold());
    }

    /**
     * Returns true if the setter method was called for the property 'number of persons in household'.
     */
    public boolean numberOfPersonsInHouseholdIsSet() {
        return _numberOfPersonsInHouseholdIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of cars'.
     */
    public Integer numberOfCarsInitVal() {
        Integer result;
        if (_numberOfCarsIsSet) {
            result = _numberOfCarsInitVal;
        } else {
            result = getNumberOfCars();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of cars'.
     */
    public boolean numberOfCarsIsDirty() {
        return !valuesAreEqual(numberOfCarsInitVal(), getNumberOfCars());
    }

    /**
     * Returns true if the setter method was called for the property 'number of cars'.
     */
    public boolean numberOfCarsIsSet() {
        return _numberOfCarsIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number of motorbikes'.
     */
    public Integer numberOfMotorbikesInitVal() {
        Integer result;
        if (_numberOfMotorbikesIsSet) {
            result = _numberOfMotorbikesInitVal;
        } else {
            result = getNumberOfMotorbikes();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number of motorbikes'.
     */
    public boolean numberOfMotorbikesIsDirty() {
        return !valuesAreEqual(numberOfMotorbikesInitVal(), getNumberOfMotorbikes());
    }

    /**
     * Returns true if the setter method was called for the property 'number of motorbikes'.
     */
    public boolean numberOfMotorbikesIsSet() {
        return _numberOfMotorbikesIsSet;
    }
	
    /**
     * Returns the initial value of the property 'dept credit cards'.
     */
    public DebtCreditCardsType deptCreditCardsInitVal() {
        DebtCreditCardsType result;
        if (_deptCreditCardsIsSet) {
            result = _deptCreditCardsInitVal;
        } else {
            result = getDeptCreditCards();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'dept credit cards'.
     */
    public boolean deptCreditCardsIsDirty() {
        return !valuesAreEqual(deptCreditCardsInitVal(), getDeptCreditCards());
    }

    /**
     * Returns true if the setter method was called for the property 'dept credit cards'.
     */
    public boolean deptCreditCardsIsSet() {
        return _deptCreditCardsIsSet;
    }

}
