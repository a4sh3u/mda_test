//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score token)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.SchufaScoreTokenEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                                                                            
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaScoreTokens'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSchufaScoreToken
    extends BrokerageEntity    implements SchufaScoreTokenEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractSchufaScoreToken.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SchufaScoreToken)) {
            return;
        }
        
        this.setScore(((SchufaScoreToken) oldEntity).getScore());    
        this.setDescription(((SchufaScoreToken) oldEntity).getDescription());    
        this.setTokenCode(((SchufaScoreToken) oldEntity).getTokenCode());    
        this.setCurrency(((SchufaScoreToken) oldEntity).getCurrency());    
        this.setAmount(((SchufaScoreToken) oldEntity).getAmount());    
        this.setDate(((SchufaScoreToken) oldEntity).getDate());    
        this.setRateType(((SchufaScoreToken) oldEntity).getRateType());    
        this.setRateCount(((SchufaScoreToken) oldEntity).getRateCount());    
        this.setAccountNumber(((SchufaScoreToken) oldEntity).getAccountNumber());    
        this.setTokenText(((SchufaScoreToken) oldEntity).getTokenText());    
        this.setTextField(((SchufaScoreToken) oldEntity).getTextField());    
        this.setCustomTokenCode(((SchufaScoreToken) oldEntity).getCustomTokenCode());    
        this.setCalculatedRate(((SchufaScoreToken) oldEntity).getCalculatedRate());    
        this.setComment(((SchufaScoreToken) oldEntity).getComment());    
        this.setFinished(((SchufaScoreToken) oldEntity).getFinished());    
        this.setBusiness(((SchufaScoreToken) oldEntity).getBusiness());    
        this.setCustomRate(((SchufaScoreToken) oldEntity).getCustomRate());    
        this.setIsSmavaToken(((SchufaScoreToken) oldEntity).getIsSmavaToken());    
        this.setRefinance(((SchufaScoreToken) oldEntity).getRefinance());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SchufaScoreToken)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getScore(), ((SchufaScoreToken) otherEntity).getScore());    
        equals = equals && valuesAreEqual(this.getDescription(), ((SchufaScoreToken) otherEntity).getDescription());    
        equals = equals && valuesAreEqual(this.getTokenCode(), ((SchufaScoreToken) otherEntity).getTokenCode());    
        equals = equals && valuesAreEqual(this.getCurrency(), ((SchufaScoreToken) otherEntity).getCurrency());    
        equals = equals && valuesAreEqual(this.getAmount(), ((SchufaScoreToken) otherEntity).getAmount());    
        equals = equals && valuesAreEqual(this.getDate(), ((SchufaScoreToken) otherEntity).getDate());    
        equals = equals && valuesAreEqual(this.getRateType(), ((SchufaScoreToken) otherEntity).getRateType());    
        equals = equals && valuesAreEqual(this.getRateCount(), ((SchufaScoreToken) otherEntity).getRateCount());    
        equals = equals && valuesAreEqual(this.getAccountNumber(), ((SchufaScoreToken) otherEntity).getAccountNumber());    
        equals = equals && valuesAreEqual(this.getTokenText(), ((SchufaScoreToken) otherEntity).getTokenText());    
        equals = equals && valuesAreEqual(this.getTextField(), ((SchufaScoreToken) otherEntity).getTextField());    
        equals = equals && valuesAreEqual(this.getCustomTokenCode(), ((SchufaScoreToken) otherEntity).getCustomTokenCode());    
        equals = equals && valuesAreEqual(this.getCalculatedRate(), ((SchufaScoreToken) otherEntity).getCalculatedRate());    
        equals = equals && valuesAreEqual(this.getComment(), ((SchufaScoreToken) otherEntity).getComment());    
        equals = equals && valuesAreEqual(this.getFinished(), ((SchufaScoreToken) otherEntity).getFinished());    
        equals = equals && valuesAreEqual(this.getBusiness(), ((SchufaScoreToken) otherEntity).getBusiness());    
        equals = equals && valuesAreEqual(this.getCustomRate(), ((SchufaScoreToken) otherEntity).getCustomRate());    
        equals = equals && valuesAreEqual(this.getIsSmavaToken(), ((SchufaScoreToken) otherEntity).getIsSmavaToken());    
        equals = equals && valuesAreEqual(this.getRefinance(), ((SchufaScoreToken) otherEntity).getRefinance());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(schufa score token)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

