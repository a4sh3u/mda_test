package de.smava.webapp.applicant.type;

/**
 * Created by aherr on 09.06.15.
 */
public enum EstateRentType {

    /**
     * Eigennutzung
     */
    NOT_RENTED,

    /**
     * Eigennutzung und Vermietung
     */
    PARTIALLY_RENTED,

    /**
     * Ausschließliche Vermietung
     */
    FULLY_RENTED
}
