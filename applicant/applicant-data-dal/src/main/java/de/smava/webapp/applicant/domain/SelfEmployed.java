//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(self employed)}
import de.smava.webapp.applicant.domain.history.SelfEmployedHistory;
import de.smava.webapp.applicant.type.IndustrySectorType;
import de.smava.webapp.applicant.type.SelfEmployedType;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SelfEmployeds'.
 *
 * 
 *
 * @author generator
 */
public class SelfEmployed extends SelfEmployedHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(self employed)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Boolean _tradeTaxesRequired;
        protected Boolean _churchTaxesRequired;
        protected java.util.Date _startOfBusiness;
        protected Set<SelfEmployedIncomeExpense> _yearlyCalculations;
        protected de.smava.webapp.applicant.type.IndustrySectorType _industrySector;
        protected de.smava.webapp.applicant.type.SelfEmployedType _selfEmployedType;
        
                            /**
     * Setter for the property 'trade taxes required'.
     *
     * 
     *
     */
    public void setTradeTaxesRequired(Boolean tradeTaxesRequired) {
        if (!_tradeTaxesRequiredIsSet) {
            _tradeTaxesRequiredIsSet = true;
            _tradeTaxesRequiredInitVal = getTradeTaxesRequired();
        }
        registerChange("trade taxes required", _tradeTaxesRequiredInitVal, tradeTaxesRequired);
        _tradeTaxesRequired = tradeTaxesRequired;
    }
                        
    /**
     * Returns the property 'trade taxes required'.
     *
     * 
     *
     */
    public Boolean getTradeTaxesRequired() {
        return _tradeTaxesRequired;
    }
                                    /**
     * Setter for the property 'church taxes required'.
     *
     * 
     *
     */
    public void setChurchTaxesRequired(Boolean churchTaxesRequired) {
        if (!_churchTaxesRequiredIsSet) {
            _churchTaxesRequiredIsSet = true;
            _churchTaxesRequiredInitVal = getChurchTaxesRequired();
        }
        registerChange("church taxes required", _churchTaxesRequiredInitVal, churchTaxesRequired);
        _churchTaxesRequired = churchTaxesRequired;
    }
                        
    /**
     * Returns the property 'church taxes required'.
     *
     * 
     *
     */
    public Boolean getChurchTaxesRequired() {
        return _churchTaxesRequired;
    }
                                    /**
     * Setter for the property 'start of business'.
     *
     * 
     *
     */
    public void setStartOfBusiness(java.util.Date startOfBusiness) {
        if (!_startOfBusinessIsSet) {
            _startOfBusinessIsSet = true;
            _startOfBusinessInitVal = getStartOfBusiness();
        }
        registerChange("start of business", _startOfBusinessInitVal, startOfBusiness);
        _startOfBusiness = startOfBusiness;
    }
                        
    /**
     * Returns the property 'start of business'.
     *
     * 
     *
     */
    public java.util.Date getStartOfBusiness() {
        return _startOfBusiness;
    }
                                            
    /**
     * Setter for the property 'yearly calculations'.
     *
     * 
     *
     */
    public void setYearlyCalculations(Set<SelfEmployedIncomeExpense> yearlyCalculations) {
        _yearlyCalculations = yearlyCalculations;
    }
            
    /**
     * Returns the property 'yearly calculations'.
     *
     * 
     *
     */
    public Set<SelfEmployedIncomeExpense> getYearlyCalculations() {
        return _yearlyCalculations;
    }
                                    /**
     * Setter for the property 'industry sector'.
     *
     * 
     *
     */
    public void setIndustrySector(de.smava.webapp.applicant.type.IndustrySectorType industrySector) {
        if (!_industrySectorIsSet) {
            _industrySectorIsSet = true;
            _industrySectorInitVal = getIndustrySector();
        }
        registerChange("industry sector", _industrySectorInitVal, industrySector);
        _industrySector = industrySector;
    }
                        
    /**
     * Returns the property 'industry sector'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.IndustrySectorType getIndustrySector() {
        return _industrySector;
    }
                                    /**
     * Setter for the property 'self employed type'.
     *
     * 
     *
     */
    public void setSelfEmployedType(de.smava.webapp.applicant.type.SelfEmployedType selfEmployedType) {
        if (!_selfEmployedTypeIsSet) {
            _selfEmployedTypeIsSet = true;
            _selfEmployedTypeInitVal = getSelfEmployedType();
        }
        registerChange("self employed type", _selfEmployedTypeInitVal, selfEmployedType);
        _selfEmployedType = selfEmployedType;
    }
                        
    /**
     * Returns the property 'self employed type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.SelfEmployedType getSelfEmployedType() {
        return _selfEmployedType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SelfEmployed.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _startOfBusiness=").append(_startOfBusiness);
            builder.append("\n    _industrySector=").append(_industrySector);
            builder.append("\n    _selfEmployedType=").append(_selfEmployedType);
            builder.append("\n}");
        } else {
            builder.append(SelfEmployed.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SelfEmployed asSelfEmployed() {
        return this;
    }
}
