//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(expense)}
import de.smava.webapp.applicant.domain.history.ExpenseHistory;
import de.smava.webapp.applicant.type.ExpenseType;

import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Expenses'.
 *
 * 
 *
 * @author generator
 */
public class Expense extends ExpenseHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(expense)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected de.smava.webapp.applicant.type.ExpenseType _expenseType;
        protected String _description;
        protected Double _monthlyAmount;
        protected java.util.Date _expirationDate;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'expense type'.
     *
     * 
     *
     */
    public void setExpenseType(de.smava.webapp.applicant.type.ExpenseType expenseType) {
        if (!_expenseTypeIsSet) {
            _expenseTypeIsSet = true;
            _expenseTypeInitVal = getExpenseType();
        }
        registerChange("expense type", _expenseTypeInitVal, expenseType);
        _expenseType = expenseType;
    }
                        
    /**
     * Returns the property 'expense type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.ExpenseType getExpenseType() {
        return _expenseType;
    }
                                    /**
     * Setter for the property 'description'.
     *
     * 
     *
     */
    public void setDescription(String description) {
        if (!_descriptionIsSet) {
            _descriptionIsSet = true;
            _descriptionInitVal = getDescription();
        }
        registerChange("description", _descriptionInitVal, description);
        _description = description;
    }
                        
    /**
     * Returns the property 'description'.
     *
     * 
     *
     */
    public String getDescription() {
        return _description;
    }
                                    /**
     * Setter for the property 'monthly amount'.
     *
     * 
     *
     */
    public void setMonthlyAmount(Double monthlyAmount) {
        if (!_monthlyAmountIsSet) {
            _monthlyAmountIsSet = true;
            _monthlyAmountInitVal = getMonthlyAmount();
        }
        registerChange("monthly amount", _monthlyAmountInitVal, monthlyAmount);
        _monthlyAmount = monthlyAmount;
    }
                        
    /**
     * Returns the property 'monthly amount'.
     *
     * 
     *
     */
    public Double getMonthlyAmount() {
        return _monthlyAmount;
    }
                                    /**
     * Setter for the property 'expiration date'.
     *
     * 
     *
     */
    public void setExpirationDate(java.util.Date expirationDate) {
        if (!_expirationDateIsSet) {
            _expirationDateIsSet = true;
            _expirationDateInitVal = getExpirationDate();
        }
        registerChange("expiration date", _expirationDateInitVal, expirationDate);
        _expirationDate = expirationDate;
    }
                        
    /**
     * Returns the property 'expiration date'.
     *
     * 
     *
     */
    public java.util.Date getExpirationDate() {
        return _expirationDate;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Expense.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _expenseType=").append(_expenseType);
            builder.append("\n    _description=").append(_description);
            builder.append("\n    _expirationDate=").append(_expirationDate);
            builder.append("\n}");
        } else {
            builder.append(Expense.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Expense asExpense() {
        return this;
    }
}
