package de.smava.webapp.applicant.type;

/**
 * <p>
 * This is a separator defining the type of employment.
 * </p>
 * <p>
 * From technical point of view the employment type is required
 * to find out what tables needs to be loaded to provide the data
 * required for a certain kind of employment.
 * </p>
 * Created by aherr on 02.03.15.
 */
public enum EmploymentType {

    SALARIED_STAFF,
    SELF_EMPLOYED,
    RETIREE,

    /**
     * Arbeitslos
     */
    UNEMPLOYED,

    /**
     * Schüler
     */
    STUDENT,

    /**
     * Student
     */
    SCHOLAR,

    /**
     * Hausmann, -frau
     */
    HOMEKEEPER;
}
