package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.OtherHousingCost;


/**
 * The domain object that represents 'OtherHousingCosts'.
 *
 * @author generator
 */
public interface OtherHousingCostEntityInterface {

    /**
     * Setter for the property 'other housing cost type'.
     *
     * Type of this housing situation. See enum for details.
     *
     */
    void setOtherHousingCostType(de.smava.webapp.applicant.type.OtherHousingCostType otherHousingCostType);

    /**
     * Returns the property 'other housing cost type'.
     *
     * Type of this housing situation. See enum for details.
     *
     */
    de.smava.webapp.applicant.type.OtherHousingCostType getOtherHousingCostType();
    /**
     * Helper method to get reference of this object as model type.
     */
    OtherHousingCost asOtherHousingCost();
}
