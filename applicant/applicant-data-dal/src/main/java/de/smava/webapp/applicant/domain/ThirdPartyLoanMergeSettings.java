//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(third party loan merge settings)}
import de.smava.webapp.applicant.domain.history.ThirdPartyLoanMergeSettingsHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ThirdPartyLoanMergeSettingss'.
 *
 * 
 *
 * @author generator
 */
public class ThirdPartyLoanMergeSettings extends ThirdPartyLoanMergeSettingsHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(third party loan merge settings)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _schufaTokenCode;
        protected Integer _maximumMergeLevel;
        
                            /**
     * Setter for the property 'schufa token code'.
     *
     * 
     *
     */
    public void setSchufaTokenCode(String schufaTokenCode) {
        if (!_schufaTokenCodeIsSet) {
            _schufaTokenCodeIsSet = true;
            _schufaTokenCodeInitVal = getSchufaTokenCode();
        }
        registerChange("schufa token code", _schufaTokenCodeInitVal, schufaTokenCode);
        _schufaTokenCode = schufaTokenCode;
    }
                        
    /**
     * Returns the property 'schufa token code'.
     *
     * 
     *
     */
    public String getSchufaTokenCode() {
        return _schufaTokenCode;
    }
                                    /**
     * Setter for the property 'maximum merge level'.
     *
     * 
     *
     */
    public void setMaximumMergeLevel(Integer maximumMergeLevel) {
        if (!_maximumMergeLevelIsSet) {
            _maximumMergeLevelIsSet = true;
            _maximumMergeLevelInitVal = getMaximumMergeLevel();
        }
        registerChange("maximum merge level", _maximumMergeLevelInitVal, maximumMergeLevel);
        _maximumMergeLevel = maximumMergeLevel;
    }
                        
    /**
     * Returns the property 'maximum merge level'.
     *
     * 
     *
     */
    public Integer getMaximumMergeLevel() {
        return _maximumMergeLevel;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ThirdPartyLoanMergeSettings.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _schufaTokenCode=").append(_schufaTokenCode);
            builder.append("\n}");
        } else {
            builder.append(ThirdPartyLoanMergeSettings.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ThirdPartyLoanMergeSettings asThirdPartyLoanMergeSettings() {
        return this;
    }
}
