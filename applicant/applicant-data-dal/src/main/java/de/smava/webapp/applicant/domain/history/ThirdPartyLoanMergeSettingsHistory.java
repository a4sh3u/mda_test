package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractThirdPartyLoanMergeSettings;




/**
 * The domain object that has all history aggregation related fields for 'ThirdPartyLoanMergeSettingss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ThirdPartyLoanMergeSettingsHistory extends AbstractThirdPartyLoanMergeSettings {

    protected transient String _schufaTokenCodeInitVal;
    protected transient boolean _schufaTokenCodeIsSet;
    protected transient Integer _maximumMergeLevelInitVal;
    protected transient boolean _maximumMergeLevelIsSet;


	
    /**
     * Returns the initial value of the property 'schufa token code'.
     */
    public String schufaTokenCodeInitVal() {
        String result;
        if (_schufaTokenCodeIsSet) {
            result = _schufaTokenCodeInitVal;
        } else {
            result = getSchufaTokenCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'schufa token code'.
     */
    public boolean schufaTokenCodeIsDirty() {
        return !valuesAreEqual(schufaTokenCodeInitVal(), getSchufaTokenCode());
    }

    /**
     * Returns true if the setter method was called for the property 'schufa token code'.
     */
    public boolean schufaTokenCodeIsSet() {
        return _schufaTokenCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'maximum merge level'.
     */
    public Integer maximumMergeLevelInitVal() {
        Integer result;
        if (_maximumMergeLevelIsSet) {
            result = _maximumMergeLevelInitVal;
        } else {
            result = getMaximumMergeLevel();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'maximum merge level'.
     */
    public boolean maximumMergeLevelIsDirty() {
        return !valuesAreEqual(maximumMergeLevelInitVal(), getMaximumMergeLevel());
    }

    /**
     * Returns true if the setter method was called for the property 'maximum merge level'.
     */
    public boolean maximumMergeLevelIsSet() {
        return _maximumMergeLevelIsSet;
    }

}
