//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(household)}
import de.smava.webapp.applicant.domain.history.HouseholdHistory;
import de.smava.webapp.applicant.type.DebtCreditCardsType;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Households'.
 *
 * Houshold situation for describing the living situation more in detail.
 *
 * @author generator
 */
public class Household extends HouseholdHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(household)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected Integer _numberOfChildren;
        protected Integer _numberOfPersonsInHousehold;
        protected Integer _numberOfCars;
        protected Integer _numberOfMotorbikes;
        protected DebtCreditCardsType _deptCreditCards;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'number of children'.
     *
     * 
     *
     */
    public void setNumberOfChildren(Integer numberOfChildren) {
        if (!_numberOfChildrenIsSet) {
            _numberOfChildrenIsSet = true;
            _numberOfChildrenInitVal = getNumberOfChildren();
        }
        registerChange("number of children", _numberOfChildrenInitVal, numberOfChildren);
        _numberOfChildren = numberOfChildren;
    }
                        
    /**
     * Returns the property 'number of children'.
     *
     * 
     *
     */
    public Integer getNumberOfChildren() {
        return _numberOfChildren;
    }
                                    /**
     * Setter for the property 'number of persons in household'.
     *
     * 
     *
     */
    public void setNumberOfPersonsInHousehold(Integer numberOfPersonsInHousehold) {
        if (!_numberOfPersonsInHouseholdIsSet) {
            _numberOfPersonsInHouseholdIsSet = true;
            _numberOfPersonsInHouseholdInitVal = getNumberOfPersonsInHousehold();
        }
        registerChange("number of persons in household", _numberOfPersonsInHouseholdInitVal, numberOfPersonsInHousehold);
        _numberOfPersonsInHousehold = numberOfPersonsInHousehold;
    }
                        
    /**
     * Returns the property 'number of persons in household'.
     *
     * 
     *
     */
    public Integer getNumberOfPersonsInHousehold() {
        return _numberOfPersonsInHousehold;
    }
                                    /**
     * Setter for the property 'number of cars'.
     *
     * 
     *
     */
    public void setNumberOfCars(Integer numberOfCars) {
        if (!_numberOfCarsIsSet) {
            _numberOfCarsIsSet = true;
            _numberOfCarsInitVal = getNumberOfCars();
        }
        registerChange("number of cars", _numberOfCarsInitVal, numberOfCars);
        _numberOfCars = numberOfCars;
    }
                        
    /**
     * Returns the property 'number of cars'.
     *
     * 
     *
     */
    public Integer getNumberOfCars() {
        return _numberOfCars;
    }
                                    /**
     * Setter for the property 'number of motorbikes'.
     *
     * 
     *
     */
    public void setNumberOfMotorbikes(Integer numberOfMotorbikes) {
        if (!_numberOfMotorbikesIsSet) {
            _numberOfMotorbikesIsSet = true;
            _numberOfMotorbikesInitVal = getNumberOfMotorbikes();
        }
        registerChange("number of motorbikes", _numberOfMotorbikesInitVal, numberOfMotorbikes);
        _numberOfMotorbikes = numberOfMotorbikes;
    }
                        
    /**
     * Returns the property 'number of motorbikes'.
     *
     * 
     *
     */
    public Integer getNumberOfMotorbikes() {
        return _numberOfMotorbikes;
    }
                                    /**
     * Setter for the property 'dept credit cards'.
     *
     * 
     *
     */
    public void setDeptCreditCards(DebtCreditCardsType deptCreditCards) {
        if (!_deptCreditCardsIsSet) {
            _deptCreditCardsIsSet = true;
            _deptCreditCardsInitVal = getDeptCreditCards();
        }
        registerChange("dept credit cards", _deptCreditCardsInitVal, deptCreditCards);
        _deptCreditCards = deptCreditCards;
    }
                        
    /**
     * Returns the property 'dept credit cards'.
     *
     * 
     *
     */
    public DebtCreditCardsType getDeptCreditCards() {
        return _deptCreditCards;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Household.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _deptCreditCards=").append(_deptCreditCards);
            builder.append("\n}");
        } else {
            builder.append(Household.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Household asHousehold() {
        return this;
    }
}
