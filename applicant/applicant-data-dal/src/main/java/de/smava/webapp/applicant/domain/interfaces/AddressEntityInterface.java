package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Address;


/**
 * The domain object that represents 'Addresss'.
 *
 * @author generator
 */
public interface AddressEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(java.util.Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    java.util.Date getCreationDate();
    /**
     * Setter for the property 'street'.
     *
     * 
     *
     */
    void setStreet(String street);

    /**
     * Returns the property 'street'.
     *
     * 
     *
     */
    String getStreet();
    /**
     * Setter for the property 'house number'.
     *
     * 
     *
     */
    void setHouseNumber(String houseNumber);

    /**
     * Returns the property 'house number'.
     *
     * 
     *
     */
    String getHouseNumber();
    /**
     * Setter for the property 'zip code'.
     *
     * 
     *
     */
    void setZipCode(String zipCode);

    /**
     * Returns the property 'zip code'.
     *
     * 
     *
     */
    String getZipCode();
    /**
     * Setter for the property 'city'.
     *
     * 
     *
     */
    void setCity(String city);

    /**
     * Returns the property 'city'.
     *
     * 
     *
     */
    String getCity();
    /**
     * Setter for the property 'country'.
     *
     * 
     *
     */
    void setCountry(de.smava.webapp.applicant.type.Country country);

    /**
     * Returns the property 'country'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.Country getCountry();
    /**
     * Helper method to get reference of this object as model type.
     */
    Address asAddress();
}
