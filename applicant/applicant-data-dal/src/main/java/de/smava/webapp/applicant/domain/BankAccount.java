//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account)}
import de.smava.webapp.applicant.domain.history.BankAccountHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankAccounts'.
 *
 * 
 *
 * @author generator
 */
public class BankAccount extends BankAccountHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(bank account)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _creationDate;
        protected String _bankName;
        protected String _iban;
        protected String _bic;
        protected String _accountNumber;
        protected String _bankCode;
        
                            /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'bank name'.
     *
     * 
     *
     */
    public void setBankName(String bankName) {
        if (!_bankNameIsSet) {
            _bankNameIsSet = true;
            _bankNameInitVal = getBankName();
        }
        registerChange("bank name", _bankNameInitVal, bankName);
        _bankName = bankName;
    }
                        
    /**
     * Returns the property 'bank name'.
     *
     * 
     *
     */
    public String getBankName() {
        return _bankName;
    }
                                    /**
     * Setter for the property 'iban'.
     *
     * 
     *
     */
    public void setIban(String iban) {
        if (!_ibanIsSet) {
            _ibanIsSet = true;
            _ibanInitVal = getIban();
        }
        registerChange("iban", _ibanInitVal, iban);
        _iban = iban;
    }
                        
    /**
     * Returns the property 'iban'.
     *
     * 
     *
     */
    public String getIban() {
        return _iban;
    }
                                    /**
     * Setter for the property 'bic'.
     *
     * 
     *
     */
    public void setBic(String bic) {
        if (!_bicIsSet) {
            _bicIsSet = true;
            _bicInitVal = getBic();
        }
        registerChange("bic", _bicInitVal, bic);
        _bic = bic;
    }
                        
    /**
     * Returns the property 'bic'.
     *
     * 
     *
     */
    public String getBic() {
        return _bic;
    }
                                    /**
     * Setter for the property 'account number'.
     *
     * 
     *
     */
    public void setAccountNumber(String accountNumber) {
        if (!_accountNumberIsSet) {
            _accountNumberIsSet = true;
            _accountNumberInitVal = getAccountNumber();
        }
        registerChange("account number", _accountNumberInitVal, accountNumber);
        _accountNumber = accountNumber;
    }
                        
    /**
     * Returns the property 'account number'.
     *
     * 
     *
     */
    public String getAccountNumber() {
        return _accountNumber;
    }
                                    /**
     * Setter for the property 'bank code'.
     *
     * 
     *
     */
    public void setBankCode(String bankCode) {
        if (!_bankCodeIsSet) {
            _bankCodeIsSet = true;
            _bankCodeInitVal = getBankCode();
        }
        registerChange("bank code", _bankCodeInitVal, bankCode);
        _bankCode = bankCode;
    }
                        
    /**
     * Returns the property 'bank code'.
     *
     * 
     *
     */
    public String getBankCode() {
        return _bankCode;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(BankAccount.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _bankName=").append(_bankName);
            builder.append("\n    _iban=").append(_iban);
            builder.append("\n    _bic=").append(_bic);
            builder.append("\n    _accountNumber=").append(_accountNumber);
            builder.append("\n    _bankCode=").append(_bankCode);
            builder.append("\n}");
        } else {
            builder.append(BankAccount.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public BankAccount asBankAccount() {
        return this;
    }
}
