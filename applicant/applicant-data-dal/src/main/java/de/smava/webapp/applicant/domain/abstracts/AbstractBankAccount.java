//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(bank account)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.BankAccountEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'BankAccounts'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractBankAccount
    extends BrokerageEntity    implements BankAccountEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractBankAccount.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof BankAccount)) {
            return;
        }
        
        this.setCreationDate(CurrentDate.getDate());    
        this.setBankName(((BankAccount) oldEntity).getBankName());    
        this.setIban(((BankAccount) oldEntity).getIban());    
        this.setBic(((BankAccount) oldEntity).getBic());    
        this.setAccountNumber(((BankAccount) oldEntity).getAccountNumber());    
        this.setBankCode(((BankAccount) oldEntity).getBankCode());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof BankAccount)) {
            equals = false;
        }
        
            
        equals = equals && valuesAreEqual(this.getBankName(), ((BankAccount) otherEntity).getBankName());    
        equals = equals && valuesAreEqual(this.getIban(), ((BankAccount) otherEntity).getIban());    
        equals = equals && valuesAreEqual(this.getBic(), ((BankAccount) otherEntity).getBic());    
        equals = equals && valuesAreEqual(this.getAccountNumber(), ((BankAccount) otherEntity).getAccountNumber());    
        equals = equals && valuesAreEqual(this.getBankCode(), ((BankAccount) otherEntity).getBankCode());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(bank account)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

