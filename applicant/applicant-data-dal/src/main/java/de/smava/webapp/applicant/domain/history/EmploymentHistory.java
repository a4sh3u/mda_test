package de.smava.webapp.applicant.domain.history;


import de.smava.webapp.applicant.domain.abstracts.AbstractEmployment;
import de.smava.webapp.applicant.type.EmploymentType;



/**
 * The domain object that has all history aggregation related fields for 'Employments'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EmploymentHistory extends AbstractEmployment {

    protected transient EmploymentType _employmentTypeInitVal;
    protected transient boolean _employmentTypeIsSet;


	
    /**
     * Returns the initial value of the property 'employment type'.
     */
    public EmploymentType employmentTypeInitVal() {
        EmploymentType result;
        if (_employmentTypeIsSet) {
            result = _employmentTypeInitVal;
        } else {
            result = getEmploymentType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employment type'.
     */
    public boolean employmentTypeIsDirty() {
        return !valuesAreEqual(employmentTypeInitVal(), getEmploymentType());
    }

    /**
     * Returns true if the setter method was called for the property 'employment type'.
     */
    public boolean employmentTypeIsSet() {
        return _employmentTypeIsSet;
    }

}
