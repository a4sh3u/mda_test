//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(living address)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.LivingAddressEntityInterface;
import org.apache.log4j.Logger;

        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'LivingAddresss'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * Extends an address by applying additional information like the time of moving in.
 *
 * @author generator
 */
public abstract class AbstractLivingAddress
    extends BrokerageEntity    implements LivingAddressEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractLivingAddress.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof LivingAddress)) {
            return;
        }
        
        this.setMoveInDate(((LivingAddress) oldEntity).getMoveInDate());    
        this.setAddress(((LivingAddress) oldEntity).getAddress());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof LivingAddress)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getMoveInDate(), ((LivingAddress) otherEntity).getMoveInDate());    
        equals = equals && valuesAreEqual(this.getAddress(), ((LivingAddress) otherEntity).getAddress());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(living address)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

