package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractSchufaScoreToken;




/**
 * The domain object that has all history aggregation related fields for 'SchufaScoreTokens'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class SchufaScoreTokenHistory extends AbstractSchufaScoreToken {

    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient String _tokenCodeInitVal;
    protected transient boolean _tokenCodeIsSet;
    protected transient String _currencyInitVal;
    protected transient boolean _currencyIsSet;
    protected transient String _amountInitVal;
    protected transient boolean _amountIsSet;
    protected transient String _dateInitVal;
    protected transient boolean _dateIsSet;
    protected transient String _rateTypeInitVal;
    protected transient boolean _rateTypeIsSet;
    protected transient String _rateCountInitVal;
    protected transient boolean _rateCountIsSet;
    protected transient String _accountNumberInitVal;
    protected transient boolean _accountNumberIsSet;
    protected transient String _tokenTextInitVal;
    protected transient boolean _tokenTextIsSet;
    protected transient String _textFieldInitVal;
    protected transient boolean _textFieldIsSet;
    protected transient String _customTokenCodeInitVal;
    protected transient boolean _customTokenCodeIsSet;
    protected transient double _calculatedRateInitVal;
    protected transient boolean _calculatedRateIsSet;
    protected transient String _commentInitVal;
    protected transient boolean _commentIsSet;
    protected transient boolean _finishedInitVal;
    protected transient boolean _finishedIsSet;
    protected transient boolean _businessInitVal;
    protected transient boolean _businessIsSet;
    protected transient Double _customRateInitVal;
    protected transient boolean _customRateIsSet;
    protected transient boolean _isSmavaTokenInitVal;
    protected transient boolean _isSmavaTokenIsSet;
    protected transient boolean _refinanceInitVal;
    protected transient boolean _refinanceIsSet;


		
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'token code'.
     */
    public String tokenCodeInitVal() {
        String result;
        if (_tokenCodeIsSet) {
            result = _tokenCodeInitVal;
        } else {
            result = getTokenCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'token code'.
     */
    public boolean tokenCodeIsDirty() {
        return !valuesAreEqual(tokenCodeInitVal(), getTokenCode());
    }

    /**
     * Returns true if the setter method was called for the property 'token code'.
     */
    public boolean tokenCodeIsSet() {
        return _tokenCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'currency'.
     */
    public String currencyInitVal() {
        String result;
        if (_currencyIsSet) {
            result = _currencyInitVal;
        } else {
            result = getCurrency();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'currency'.
     */
    public boolean currencyIsDirty() {
        return !valuesAreEqual(currencyInitVal(), getCurrency());
    }

    /**
     * Returns true if the setter method was called for the property 'currency'.
     */
    public boolean currencyIsSet() {
        return _currencyIsSet;
    }
	
    /**
     * Returns the initial value of the property 'amount'.
     */
    public String amountInitVal() {
        String result;
        if (_amountIsSet) {
            result = _amountInitVal;
        } else {
            result = getAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'amount'.
     */
    public boolean amountIsDirty() {
        return !valuesAreEqual(amountInitVal(), getAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'amount'.
     */
    public boolean amountIsSet() {
        return _amountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'date'.
     */
    public String dateInitVal() {
        String result;
        if (_dateIsSet) {
            result = _dateInitVal;
        } else {
            result = getDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'date'.
     */
    public boolean dateIsDirty() {
        return !valuesAreEqual(dateInitVal(), getDate());
    }

    /**
     * Returns true if the setter method was called for the property 'date'.
     */
    public boolean dateIsSet() {
        return _dateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rate type'.
     */
    public String rateTypeInitVal() {
        String result;
        if (_rateTypeIsSet) {
            result = _rateTypeInitVal;
        } else {
            result = getRateType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rate type'.
     */
    public boolean rateTypeIsDirty() {
        return !valuesAreEqual(rateTypeInitVal(), getRateType());
    }

    /**
     * Returns true if the setter method was called for the property 'rate type'.
     */
    public boolean rateTypeIsSet() {
        return _rateTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'rate count'.
     */
    public String rateCountInitVal() {
        String result;
        if (_rateCountIsSet) {
            result = _rateCountInitVal;
        } else {
            result = getRateCount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'rate count'.
     */
    public boolean rateCountIsDirty() {
        return !valuesAreEqual(rateCountInitVal(), getRateCount());
    }

    /**
     * Returns true if the setter method was called for the property 'rate count'.
     */
    public boolean rateCountIsSet() {
        return _rateCountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'account number'.
     */
    public String accountNumberInitVal() {
        String result;
        if (_accountNumberIsSet) {
            result = _accountNumberInitVal;
        } else {
            result = getAccountNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'account number'.
     */
    public boolean accountNumberIsDirty() {
        return !valuesAreEqual(accountNumberInitVal(), getAccountNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'account number'.
     */
    public boolean accountNumberIsSet() {
        return _accountNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'token text'.
     */
    public String tokenTextInitVal() {
        String result;
        if (_tokenTextIsSet) {
            result = _tokenTextInitVal;
        } else {
            result = getTokenText();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'token text'.
     */
    public boolean tokenTextIsDirty() {
        return !valuesAreEqual(tokenTextInitVal(), getTokenText());
    }

    /**
     * Returns true if the setter method was called for the property 'token text'.
     */
    public boolean tokenTextIsSet() {
        return _tokenTextIsSet;
    }
	
    /**
     * Returns the initial value of the property 'text field'.
     */
    public String textFieldInitVal() {
        String result;
        if (_textFieldIsSet) {
            result = _textFieldInitVal;
        } else {
            result = getTextField();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'text field'.
     */
    public boolean textFieldIsDirty() {
        return !valuesAreEqual(textFieldInitVal(), getTextField());
    }

    /**
     * Returns true if the setter method was called for the property 'text field'.
     */
    public boolean textFieldIsSet() {
        return _textFieldIsSet;
    }
	
    /**
     * Returns the initial value of the property 'custom token code'.
     */
    public String customTokenCodeInitVal() {
        String result;
        if (_customTokenCodeIsSet) {
            result = _customTokenCodeInitVal;
        } else {
            result = getCustomTokenCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'custom token code'.
     */
    public boolean customTokenCodeIsDirty() {
        return !valuesAreEqual(customTokenCodeInitVal(), getCustomTokenCode());
    }

    /**
     * Returns true if the setter method was called for the property 'custom token code'.
     */
    public boolean customTokenCodeIsSet() {
        return _customTokenCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'calculated rate'.
     */
    public double calculatedRateInitVal() {
        double result;
        if (_calculatedRateIsSet) {
            result = _calculatedRateInitVal;
        } else {
            result = getCalculatedRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'calculated rate'.
     */
    public boolean calculatedRateIsDirty() {
        return !valuesAreEqual(calculatedRateInitVal(), getCalculatedRate());
    }

    /**
     * Returns true if the setter method was called for the property 'calculated rate'.
     */
    public boolean calculatedRateIsSet() {
        return _calculatedRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'comment'.
     */
    public String commentInitVal() {
        String result;
        if (_commentIsSet) {
            result = _commentInitVal;
        } else {
            result = getComment();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'comment'.
     */
    public boolean commentIsDirty() {
        return !valuesAreEqual(commentInitVal(), getComment());
    }

    /**
     * Returns true if the setter method was called for the property 'comment'.
     */
    public boolean commentIsSet() {
        return _commentIsSet;
    }
	
    /**
     * Returns the initial value of the property 'finished'.
     */
    public boolean finishedInitVal() {
        boolean result;
        if (_finishedIsSet) {
            result = _finishedInitVal;
        } else {
            result = getFinished();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'finished'.
     */
    public boolean finishedIsDirty() {
        return !valuesAreEqual(finishedInitVal(), getFinished());
    }

    /**
     * Returns true if the setter method was called for the property 'finished'.
     */
    public boolean finishedIsSet() {
        return _finishedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'business'.
     */
    public boolean businessInitVal() {
        boolean result;
        if (_businessIsSet) {
            result = _businessInitVal;
        } else {
            result = getBusiness();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'business'.
     */
    public boolean businessIsDirty() {
        return !valuesAreEqual(businessInitVal(), getBusiness());
    }

    /**
     * Returns true if the setter method was called for the property 'business'.
     */
    public boolean businessIsSet() {
        return _businessIsSet;
    }
	
    /**
     * Returns the initial value of the property 'custom rate'.
     */
    public Double customRateInitVal() {
        Double result;
        if (_customRateIsSet) {
            result = _customRateInitVal;
        } else {
            result = getCustomRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'custom rate'.
     */
    public boolean customRateIsDirty() {
        return !valuesAreEqual(customRateInitVal(), getCustomRate());
    }

    /**
     * Returns true if the setter method was called for the property 'custom rate'.
     */
    public boolean customRateIsSet() {
        return _customRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'is smava token'.
     */
    public boolean isSmavaTokenInitVal() {
        boolean result;
        if (_isSmavaTokenIsSet) {
            result = _isSmavaTokenInitVal;
        } else {
            result = getIsSmavaToken();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'is smava token'.
     */
    public boolean isSmavaTokenIsDirty() {
        return !valuesAreEqual(isSmavaTokenInitVal(), getIsSmavaToken());
    }

    /**
     * Returns true if the setter method was called for the property 'is smava token'.
     */
    public boolean isSmavaTokenIsSet() {
        return _isSmavaTokenIsSet;
    }
	
    /**
     * Returns the initial value of the property 'refinance'.
     */
    public boolean refinanceInitVal() {
        boolean result;
        if (_refinanceIsSet) {
            result = _refinanceInitVal;
        } else {
            result = getRefinance();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'refinance'.
     */
    public boolean refinanceIsDirty() {
        return !valuesAreEqual(refinanceInitVal(), getRefinance());
    }

    /**
     * Returns true if the setter method was called for the property 'refinance'.
     */
    public boolean refinanceIsSet() {
        return _refinanceIsSet;
    }

}
