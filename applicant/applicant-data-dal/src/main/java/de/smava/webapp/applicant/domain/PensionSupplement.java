//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(pension supplement)}
import de.smava.webapp.applicant.domain.history.PensionSupplementHistory;
import de.smava.webapp.applicant.type.PensionType;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PensionSupplements'.
 *
 * 
 *
 * @author generator
 */
public class PensionSupplement extends PensionSupplementHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(pension supplement)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.PensionType _pensionSupplementType;
        
                            /**
     * Setter for the property 'pension supplement type'.
     *
     * 
     *
     */
    public void setPensionSupplementType(de.smava.webapp.applicant.type.PensionType pensionSupplementType) {
        if (!_pensionSupplementTypeIsSet) {
            _pensionSupplementTypeIsSet = true;
            _pensionSupplementTypeInitVal = getPensionSupplementType();
        }
        registerChange("pension supplement type", _pensionSupplementTypeInitVal, pensionSupplementType);
        _pensionSupplementType = pensionSupplementType;
    }
                        
    /**
     * Returns the property 'pension supplement type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.PensionType getPensionSupplementType() {
        return _pensionSupplementType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(PensionSupplement.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _pensionSupplementType=").append(_pensionSupplementType);
            builder.append("\n}");
        } else {
            builder.append(PensionSupplement.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public PensionSupplement asPensionSupplement() {
        return this;
    }
}
