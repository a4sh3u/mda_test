//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score request data)}

import de.smava.webapp.applicant.domain.history.SchufaScoreRequestDataHistory;
import de.smava.webapp.applicant.type.Gender;

import java.util.Date;
import java.util.List;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaScoreRequestDatas'.
 *
 * 
 *
 * @author generator
 */
public class SchufaScoreRequestData extends SchufaScoreRequestDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(schufa score request data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _firstName;
        protected String _lastName;
        protected Gender _gender;
        protected java.util.Date _birthDate;
        protected List<SchufaScoreAddress> _addresses;
        
                            /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("first name", _firstNameInitVal, firstName);
        _firstName = firstName;
    }
                        
    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    public String getFirstName() {
        return _firstName;
    }
                                    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("last name", _lastNameInitVal, lastName);
        _lastName = lastName;
    }
                        
    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    public String getLastName() {
        return _lastName;
    }
                                    /**
     * Setter for the property 'gender'.
     *
     * 
     *
     */
    public void setGender(Gender gender) {
        if (!_genderIsSet) {
            _genderIsSet = true;
            _genderInitVal = getGender();
        }
        registerChange("gender", _genderInitVal, gender);
        _gender = gender;
    }
                        
    /**
     * Returns the property 'gender'.
     *
     * 
     *
     */
    public Gender getGender() {
        return _gender;
    }
                                    /**
     * Setter for the property 'birth date'.
     *
     * 
     *
     */
    public void setBirthDate(java.util.Date birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;
            _birthDateInitVal = getBirthDate();
        }
        registerChange("birth date", _birthDateInitVal, birthDate);
        _birthDate = birthDate;
    }
                        
    /**
     * Returns the property 'birth date'.
     *
     * 
     *
     */
    public java.util.Date getBirthDate() {
        return _birthDate;
    }
                                            
    /**
     * Setter for the property 'addresses'.
     *
     * 
     *
     */
    public void setAddresses(List<SchufaScoreAddress> addresses) {
        _addresses = addresses;
    }
            
    /**
     * Returns the property 'addresses'.
     *
     * 
     *
     */
    public List<SchufaScoreAddress> getAddresses() {
        return _addresses;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaScoreRequestData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _gender=").append(_gender);
            builder.append("\n    _birthDate=").append(_birthDate);
            builder.append("\n}");
        } else {
            builder.append(SchufaScoreRequestData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SchufaScoreRequestData asSchufaScoreRequestData() {
        return this;
    }
}
