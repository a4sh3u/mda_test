package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.NationalityInfo;

import java.util.Date;


/**
 * The domain object that represents 'NationalityInfos'.
 *
 * @author generator
 */
public interface NationalityInfoEntityInterface {

    /**
     * Setter for the property 'nationality'.
     *
     * 
     *
     */
    void setNationality(de.smava.webapp.applicant.type.Country nationality);

    /**
     * Returns the property 'nationality'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.Country getNationality();
    /**
     * Setter for the property 'place of birth'.
     *
     * 
     *
     */
    void setPlaceOfBirth(String placeOfBirth);

    /**
     * Returns the property 'place of birth'.
     *
     * 
     *
     */
    String getPlaceOfBirth();
    /**
     * Setter for the property 'country of birth'.
     *
     * 
     *
     */
    void setCountryOfBirth(de.smava.webapp.applicant.type.Country countryOfBirth);

    /**
     * Returns the property 'country of birth'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.Country getCountryOfBirth();
    /**
     * Setter for the property 'work permit unlimited'.
     *
     * 
     *
     */
    void setWorkPermitUnlimited(Boolean workPermitUnlimited);

    /**
     * Returns the property 'work permit unlimited'.
     *
     * 
     *
     */
    Boolean getWorkPermitUnlimited();
    /**
     * Setter for the property 'end of work permit'.
     *
     * 
     *
     */
    void setEndOfWorkPermit(Date endOfWorkPermit);

    /**
     * Returns the property 'end of work permit'.
     *
     * 
     *
     */
    Date getEndOfWorkPermit();
    /**
     * Setter for the property 'residence permit unlimited'.
     *
     * 
     *
     */
    void setResidencePermitUnlimited(Boolean residencePermitUnlimited);

    /**
     * Returns the property 'residence permit unlimited'.
     *
     * 
     *
     */
    Boolean getResidencePermitUnlimited();
    /**
     * Setter for the property 'end of residence permit'.
     *
     * 
     *
     */
    void setEndOfResidencePermit(Date endOfResidencePermit);

    /**
     * Returns the property 'end of residence permit'.
     *
     * 
     *
     */
    Date getEndOfResidencePermit();
    /**
     * Helper method to get reference of this object as model type.
     */
    NationalityInfo asNationalityInfo();
}
