package de.smava.webapp.applicant.type;

/**
 * Created by mpetrick on 26.02.16.
 */
public enum IndebtednessDebtType {

    /**
     * Ratenkredit
     */
    INSTALLMENT_CREDIT,

    /**
     * Konsumentenkredit
     */
    CONSUMER_CREDIT,

    /**
     * Rahmenkredit
     */
    GLOBAL_CREDIT,

    /**
     * Abrufkredit
     */
    CALL_CREDIT,

    /**
     * Dispositionskredit
     */
    OVERDRAFT_CREDIT,

    /**
     * Kreditkartenkredit
     */
    CREDITCARD_CREDIT,

    /**
     * Autokredit
     */
    CAR_CREDIT,

    /**
     * Arbeitgeberdarlehen
     */
    EMPLOYERS_LOAN,

    /**
     * Beamtendarlehen
     */
    OFFICIAL_LOAN,

    /**
     * Geschäftskredit
     */
    BUSINESS_LOAN,

    /**
     * Immobiliendarlehen
     */
    ESTATE_CREDIT,

    /**
     * 0% Finanzierung
     */
    ZERO_FINANCING,

    /**
     * Ballonfinanzierung
     */
    BALLOON_FINANCING,

    /**
     * Privatleasing
     */
    PRIVATE_LEASING,

    /**
     * Businessleasing
     */
    BUSINESS_LEASING,

    /**
     * Rest
     */
    OTHER
}
