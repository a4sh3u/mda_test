package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractApplicant;




/**
 * The domain object that has all history aggregation related fields for 'Applicants'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ApplicantHistory extends AbstractApplicant {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _emailInitVal;
    protected transient boolean _emailIsSet;
    protected transient String _personIdInitVal;
    protected transient boolean _personIdIsSet;

	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
											
    /**
     * Returns the initial value of the property 'email'.
     */
    public String emailInitVal() {
        String result;
        if (_emailIsSet) {
            result = _emailInitVal;
        } else {
            result = getEmail();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'email'.
     */
    public boolean emailIsDirty() {
        return !valuesAreEqual(emailInitVal(), getEmail());
    }

    /**
     * Returns true if the setter method was called for the property 'email'.
     */
    public boolean emailIsSet() {
        return _emailIsSet;
    }


    /**
     * Returns the initial value of the property 'personId'.
     */
    public String personIdInitVal() {
        String result;
        if (_personIdIsSet) {
            result = _personIdInitVal;
        } else {
            result = getPersonId();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'personId'.
     */
    public boolean personIdDirty() {
        return !valuesAreEqual(personIdInitVal(), getPersonId());
    }

    /**
     * Returns true if the setter method was called for the property 'personId'.
     */
    public boolean personIdIsSet() {
        return _personIdIsSet;
    }
					
}
