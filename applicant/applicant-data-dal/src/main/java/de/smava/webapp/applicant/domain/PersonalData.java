//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(personal data)}
import de.smava.webapp.applicant.domain.history.PersonalDataHistory;
import de.smava.webapp.applicant.type.Gender;
import de.smava.webapp.applicant.type.MaritalState;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PersonalDatas'.
 *
 * 
 *
 * @author generator
 */
public class PersonalData extends PersonalDataHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(personal data)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.Gender _gender;
        protected String _firstName;
        protected String _lastName;
        protected String _birthName;
        protected java.util.Date _birthDate;
        protected de.smava.webapp.applicant.type.MaritalState _maritalState;
        
                            /**
     * Setter for the property 'gender'.
     *
     * 
     *
     */
    public void setGender(de.smava.webapp.applicant.type.Gender gender) {
        if (!_genderIsSet) {
            _genderIsSet = true;
            _genderInitVal = getGender();
        }
        registerChange("gender", _genderInitVal, gender);
        _gender = gender;
    }
                        
    /**
     * Returns the property 'gender'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.Gender getGender() {
        return _gender;
    }
                                    /**
     * Setter for the property 'first name'.
     *
     * 
     *
     */
    public void setFirstName(String firstName) {
        if (!_firstNameIsSet) {
            _firstNameIsSet = true;
            _firstNameInitVal = getFirstName();
        }
        registerChange("first name", _firstNameInitVal, firstName);
        _firstName = firstName;
    }
                        
    /**
     * Returns the property 'first name'.
     *
     * 
     *
     */
    public String getFirstName() {
        return _firstName;
    }
                                    /**
     * Setter for the property 'last name'.
     *
     * 
     *
     */
    public void setLastName(String lastName) {
        if (!_lastNameIsSet) {
            _lastNameIsSet = true;
            _lastNameInitVal = getLastName();
        }
        registerChange("last name", _lastNameInitVal, lastName);
        _lastName = lastName;
    }
                        
    /**
     * Returns the property 'last name'.
     *
     * 
     *
     */
    public String getLastName() {
        return _lastName;
    }
                                    /**
     * Setter for the property 'birth name'.
     *
     * 
     *
     */
    public void setBirthName(String birthName) {
        if (!_birthNameIsSet) {
            _birthNameIsSet = true;
            _birthNameInitVal = getBirthName();
        }
        registerChange("birth name", _birthNameInitVal, birthName);
        _birthName = birthName;
    }
                        
    /**
     * Returns the property 'birth name'.
     *
     * 
     *
     */
    public String getBirthName() {
        return _birthName;
    }
                                    /**
     * Setter for the property 'birth date'.
     *
     * 
     *
     */
    public void setBirthDate(java.util.Date birthDate) {
        if (!_birthDateIsSet) {
            _birthDateIsSet = true;
            _birthDateInitVal = getBirthDate();
        }
        registerChange("birth date", _birthDateInitVal, birthDate);
        _birthDate = birthDate;
    }
                        
    /**
     * Returns the property 'birth date'.
     *
     * 
     *
     */
    public java.util.Date getBirthDate() {
        return _birthDate;
    }
                                    /**
     * Setter for the property 'marital state'.
     *
     * 
     *
     */
    public void setMaritalState(de.smava.webapp.applicant.type.MaritalState maritalState) {
        if (!_maritalStateIsSet) {
            _maritalStateIsSet = true;
            _maritalStateInitVal = getMaritalState();
        }
        registerChange("marital state", _maritalStateInitVal, maritalState);
        _maritalState = maritalState;
    }
                        
    /**
     * Returns the property 'marital state'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.MaritalState getMaritalState() {
        return _maritalState;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(PersonalData.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _gender=").append(_gender);
            builder.append("\n    _firstName=").append(_firstName);
            builder.append("\n    _lastName=").append(_lastName);
            builder.append("\n    _birthName=").append(_birthName);
            builder.append("\n    _birthDate=").append(_birthDate);
            builder.append("\n    _maritalState=").append(_maritalState);
            builder.append("\n}");
        } else {
            builder.append(PersonalData.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public PersonalData asPersonalData() {
        return this;
    }
}
