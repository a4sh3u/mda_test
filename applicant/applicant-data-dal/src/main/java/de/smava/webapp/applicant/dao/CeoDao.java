//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    dao.tpl
//
//
//
//
package de.smava.webapp.applicant.dao;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(ceo)}
import java.io.Serializable;
import java.util.Collection;

import javax.transaction.Synchronization;

import de.smava.webapp.commons.dao.BaseDao;
import de.smava.webapp.commons.pagination.Pageable;
import de.smava.webapp.commons.pagination.Sortable;

import de.smava.webapp.applicant.domain.Ceo;
// !!!!!!!! End of insert code section !!!!!!!!

/**
 * DAO interface for the domain object 'Ceos'.
 *
 * @author generator
 */
public interface CeoDao extends BrokerageSchemaDao<Ceo> {

    /**
     * Set a synchronization for the current transaction to receive events for 'beforeCompletion' and 'afterCompletion' of transaction.
     */
    void setSynchronization(Synchronization synchronization);

    /**
     * Returns an attached copy of the ceo identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    Ceo getCeo(Long id);

    /**
     * Saves the ceo.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveCeo(Ceo ceo);

    /**
     * Deletes an ceo, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the ceo
     */
    void deleteCeo(Long id);

    /**
     * Retrieves all 'Ceo' instances and returns them attached to
     * the current persistence manager.
     */
    Collection<Ceo> getCeoList();

    /**
     * Retrieves a page of 'Ceo' instances and returns them attached to
     * the current persistence manager.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     */
    Collection<Ceo> getCeoList(Pageable pageable);

    /**
     * Retrieves a sorted collection of 'Ceo' instanes and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     *
     * @see Sortable
     */
    Collection<Ceo> getCeoList(Sortable sortable);

    /**
     * Retrieves a sorted page of 'Ceo' instances and returns them attached to
     * the current persistence manager.
     * The sorting information is read from the supplied sortable.
     * The page information is read from the supplied pageable.
     *
     * @see Pageable
     * @see Sortable
     */
    Collection<Ceo> getCeoList(Pageable pageable, Sortable sortable);



    /**
     * Returns the number of 'Ceo' instances.
     */
    long getCeoCount();





    // !!!!!!!! You can insert code here: !!!!!!!!																														{|customMethods|bean(ceo)}
    //
    // insert custom methods here
    //
    // !!!!!!!! End of insert code section !!!!!!!!
}
