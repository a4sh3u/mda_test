package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractEmployee;




/**
 * The domain object that has all history aggregation related fields for 'Employees'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EmployeeHistory extends AbstractEmployee {

    protected transient de.smava.webapp.applicant.type.EmployeeType _employeeTypeInitVal;
    protected transient boolean _employeeTypeIsSet;


	
    /**
     * Returns the initial value of the property 'employee type'.
     */
    public de.smava.webapp.applicant.type.EmployeeType employeeTypeInitVal() {
        de.smava.webapp.applicant.type.EmployeeType result;
        if (_employeeTypeIsSet) {
            result = _employeeTypeInitVal;
        } else {
            result = getEmployeeType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'employee type'.
     */
    public boolean employeeTypeIsDirty() {
        return !valuesAreEqual(employeeTypeInitVal(), getEmployeeType());
    }

    /**
     * Returns true if the setter method was called for the property 'employee type'.
     */
    public boolean employeeTypeIsSet() {
        return _employeeTypeIsSet;
    }

}
