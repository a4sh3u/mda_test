package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.*;

import java.util.List;
import java.util.Set;


/**
 * The domain object that represents 'Applicants'.
 *
 * @author generator
 */
public interface ApplicantEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(java.util.Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    java.util.Date getCreationDate();
    /**
     * Setter for the property 'nationality info'.
     *
     * 
     *
     */
    void setNationalityInfo(NationalityInfo nationalityInfo);

    /**
     * Returns the property 'nationality info'.
     *
     * 
     *
     */
    NationalityInfo getNationalityInfo();
    /**
     * Setter for the property 'home address'.
     *
     * 
     *
     */
    void setHomeAddress(LivingAddress homeAddress);

    /**
     * Returns the property 'home address'.
     *
     * 
     *
     */
    LivingAddress getHomeAddress();
    /**
     * Setter for the property 'previous home address'.
     *
     * 
     *
     */
    void setPreviousHomeAddress(Address previousHomeAddress);

    /**
     * Returns the property 'previous home address'.
     *
     * 
     *
     */
    Address getPreviousHomeAddress();
    /**
     * Setter for the property 'previous employment'.
     *
     * 
     *
     */
    void setPreviousEmployment(Employment previousEmployment);

    /**
     * Returns the property 'previous employment'.
     *
     * 
     *
     */
    Employment getPreviousEmployment();
    /**
     * Setter for the property 'household'.
     *
     * 
     *
     */
    void setHousehold(Household household);

    /**
     * Returns the property 'household'.
     *
     * 
     *
     */
    Household getHousehold();
    /**
     * Setter for the property 'main income'.
     *
     * 
     *
     */
    void setMainIncome(Income mainIncome);

    /**
     * Returns the property 'main income'.
     *
     * 
     *
     */
    Income getMainIncome();
    /**
     * Setter for the property 'additional incomes'.
     *
     * 
     *
     */
    void setAdditionalIncomes(Set<Income> additionalIncomes);

    /**
     * Returns the property 'additional incomes'.
     *
     * 
     *
     */
    Set<Income> getAdditionalIncomes();
    /**
     * Setter for the property 'expenses'.
     *
     * 
     *
     */
    void setExpenses(Set<Expense> expenses);

    /**
     * Returns the property 'expenses'.
     *
     * 
     *
     */
    Set<Expense> getExpenses();
    /**
     * Setter for the property 'phones'.
     *
     * 
     *
     */
    void setPhones(Set<Phone> phones);

    /**
     * Returns the property 'phones'.
     *
     * 
     *
     */
    Set<Phone> getPhones();
    /**
     * Setter for the property 'personal data'.
     *
     * 
     *
     */
    void setPersonalData(PersonalData personalData);

    /**
     * Returns the property 'personal data'.
     *
     * 
     *
     */
    PersonalData getPersonalData();
    /**
     * Setter for the property 'email'.
     *
     * Email of the applicant. For the main borrower this is equal to the account's email. Note that it cannot be unique since a borrower can be main borrower and second borrower (with 2 records in that case).
     *
     */
    void setEmail(String email);

    /**
     * Returns the property 'email'.
     *
     * Email of the applicant. For the main borrower this is equal to the account's email. Note that it cannot be unique since a borrower can be main borrower and second borrower (with 2 records in that case).
     *
     */
    String getEmail();
    /**
     * Setter for the property 'credit scores'.
     *
     * 
     *
     */
    void setCreditScores(List<CreditScore> creditScores);

    /**
     * Returns the property 'credit scores'.
     *
     * 
     *
     */
    List<CreditScore> getCreditScores();
    /**
     * Setter for the property 'third party loans'.
     *
     * 
     *
     */
    void setThirdPartyLoans(Set<ThirdPartyLoan> thirdPartyLoans);

    /**
     * Returns the property 'third party loans'.
     *
     * 
     *
     */
    Set<ThirdPartyLoan> getThirdPartyLoans();
    /**
     * Setter for the property 'external third party loans'.
     *
     * 
     *
     */
    void setExternalThirdPartyLoans(Set<ExternalThirdPartyLoan> externalThirdPartyLoans);

    /**
     * Returns the property 'external third party loans'.
     *
     * 
     *
     */
    Set<ExternalThirdPartyLoan> getExternalThirdPartyLoans();
    /**
     * Setter for the property 'customer third party loans'.
     *
     * 
     *
     */
    void setCustomerThirdPartyLoans(Set<CustomerThirdPartyLoan> customerThirdPartyLoans);

    /**
     * Returns the property 'customer third party loans'.
     *
     * 
     *
     */
    Set<CustomerThirdPartyLoan> getCustomerThirdPartyLoans();
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    void setBankAccount(BankAccount bankAccount);

    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    BankAccount getBankAccount();


    /**
     * retuns the person Id
     * @return he person Id
     */
    String getPersonId();


    /**
     * setter for personId
     * @param personId
     */
    void setPersonId( String personId);

    /**
     * Helper method to get reference of this object as model type.
     */
    Applicant asApplicant();
}
