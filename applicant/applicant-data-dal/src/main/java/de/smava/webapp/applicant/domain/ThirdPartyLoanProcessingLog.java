//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(third party loan processing log)}
import de.smava.webapp.applicant.domain.history.ThirdPartyLoanProcessingLogHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ThirdPartyLoanProcessingLogs'.
 *
 * 
 *
 * @author generator
 */
public class ThirdPartyLoanProcessingLog extends ThirdPartyLoanProcessingLogHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(third party loan processing log)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _processingResult;
        protected de.smava.webapp.applicant.domain.ExternalThirdPartyLoan _externalThirdPartyLoan;
        protected de.smava.webapp.applicant.domain.CustomerThirdPartyLoan _customerThirdPartyLoan;
        protected de.smava.webapp.applicant.domain.ThirdPartyLoan _thirdPartyLoan;
        
                            /**
     * Setter for the property 'processing result'.
     *
     * 
     *
     */
    public void setProcessingResult(String processingResult) {
        if (!_processingResultIsSet) {
            _processingResultIsSet = true;
            _processingResultInitVal = getProcessingResult();
        }
        registerChange("processing result", _processingResultInitVal, processingResult);
        _processingResult = processingResult;
    }
                        
    /**
     * Returns the property 'processing result'.
     *
     * 
     *
     */
    public String getProcessingResult() {
        return _processingResult;
    }
                                            
    /**
     * Setter for the property 'external third party loan'.
     *
     * 
     *
     */
    public void setExternalThirdPartyLoan(de.smava.webapp.applicant.domain.ExternalThirdPartyLoan externalThirdPartyLoan) {
        _externalThirdPartyLoan = externalThirdPartyLoan;
    }
            
    /**
     * Returns the property 'external third party loan'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.ExternalThirdPartyLoan getExternalThirdPartyLoan() {
        return _externalThirdPartyLoan;
    }
                                            
    /**
     * Setter for the property 'customer third party loan'.
     *
     * 
     *
     */
    public void setCustomerThirdPartyLoan(de.smava.webapp.applicant.domain.CustomerThirdPartyLoan customerThirdPartyLoan) {
        _customerThirdPartyLoan = customerThirdPartyLoan;
    }
            
    /**
     * Returns the property 'customer third party loan'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.CustomerThirdPartyLoan getCustomerThirdPartyLoan() {
        return _customerThirdPartyLoan;
    }
                                            
    /**
     * Setter for the property 'third party loan'.
     *
     * 
     *
     */
    public void setThirdPartyLoan(de.smava.webapp.applicant.domain.ThirdPartyLoan thirdPartyLoan) {
        _thirdPartyLoan = thirdPartyLoan;
    }
            
    /**
     * Returns the property 'third party loan'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.domain.ThirdPartyLoan getThirdPartyLoan() {
        return _thirdPartyLoan;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_externalThirdPartyLoan instanceof de.smava.webapp.commons.domain.Entity && !_externalThirdPartyLoan.getChangeSet().isEmpty()) {
             for (String element : _externalThirdPartyLoan.getChangeSet()) {
                 result.add("external third party loan : " + element);
             }
         }

         if (_customerThirdPartyLoan instanceof de.smava.webapp.commons.domain.Entity && !_customerThirdPartyLoan.getChangeSet().isEmpty()) {
             for (String element : _customerThirdPartyLoan.getChangeSet()) {
                 result.add("customer third party loan : " + element);
             }
         }

         if (_thirdPartyLoan instanceof de.smava.webapp.commons.domain.Entity && !_thirdPartyLoan.getChangeSet().isEmpty()) {
             for (String element : _thirdPartyLoan.getChangeSet()) {
                 result.add("third party loan : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ThirdPartyLoanProcessingLog.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _processingResult=").append(_processingResult);
            builder.append("\n}");
        } else {
            builder.append(ThirdPartyLoanProcessingLog.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ThirdPartyLoanProcessingLog asThirdPartyLoanProcessingLog() {
        return this;
    }
}
