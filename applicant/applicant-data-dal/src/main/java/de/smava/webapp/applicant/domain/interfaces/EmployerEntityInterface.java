package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Address;
import de.smava.webapp.applicant.domain.Employer;
import de.smava.webapp.applicant.domain.Phone;


/**
 * The domain object that represents 'Employers'.
 *
 * @author generator
 */
public interface EmployerEntityInterface {

    /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    void setName(String name);

    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    String getName();
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    void setAddress(Address address);

    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    Address getAddress();
    /**
     * Setter for the property 'phone'.
     *
     * 
     *
     */
    void setPhone(Phone phone);

    /**
     * Returns the property 'phone'.
     *
     * 
     *
     */
    Phone getPhone();
    /**
     * Helper method to get reference of this object as model type.
     */
    Employer asEmployer();
}
