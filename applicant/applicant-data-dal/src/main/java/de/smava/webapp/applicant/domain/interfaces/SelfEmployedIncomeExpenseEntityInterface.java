package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.SelfEmployed;
import de.smava.webapp.applicant.domain.SelfEmployedIncomeExpense;


/**
 * The domain object that represents 'SelfEmployedIncomeExpenses'.
 *
 * @author generator
 */
public interface SelfEmployedIncomeExpenseEntityInterface {

    /**
     * Setter for the property 'year'.
     *
     * 
     *
     */
    void setYear(int year);

    /**
     * Returns the property 'year'.
     *
     * 
     *
     */
    int getYear();
    /**
     * Setter for the property 'annual gross turnover'.
     *
     * 
     *
     */
    void setAnnualGrossTurnover(Double annualGrossTurnover);

    /**
     * Returns the property 'annual gross turnover'.
     *
     * 
     *
     */
    Double getAnnualGrossTurnover();
    /**
     * Setter for the property 'annual expenses'.
     *
     * 
     *
     */
    void setAnnualExpenses(Double annualExpenses);

    /**
     * Returns the property 'annual expenses'.
     *
     * 
     *
     */
    Double getAnnualExpenses();
    /**
     * Setter for the property 'self employed'.
     *
     * 
     *
     */
    void setSelfEmployed(SelfEmployed selfEmployed);

    /**
     * Returns the property 'self employed'.
     *
     * 
     *
     */
    SelfEmployed getSelfEmployed();
    /**
     * Helper method to get reference of this object as model type.
     */
    SelfEmployedIncomeExpense asSelfEmployedIncomeExpense();
}
