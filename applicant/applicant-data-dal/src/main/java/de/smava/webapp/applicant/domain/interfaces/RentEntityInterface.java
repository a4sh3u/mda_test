package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Rent;


/**
 * The domain object that represents 'Rents'.
 *
 * @author generator
 */
public interface RentEntityInterface {

    /**
     * Helper method to get reference of this object as model type.
     */
    Rent asRent();
}
