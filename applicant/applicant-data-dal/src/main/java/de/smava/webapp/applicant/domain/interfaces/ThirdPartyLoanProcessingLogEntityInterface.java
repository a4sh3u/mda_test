package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.ThirdPartyLoanProcessingLog;


/**
 * The domain object that represents 'ThirdPartyLoanProcessingLogs'.
 *
 * @author generator
 */
public interface ThirdPartyLoanProcessingLogEntityInterface {

    /**
     * Setter for the property 'processing result'.
     *
     * 
     *
     */
    void setProcessingResult(String processingResult);

    /**
     * Returns the property 'processing result'.
     *
     * 
     *
     */
    String getProcessingResult();
    /**
     * Setter for the property 'external third party loan'.
     *
     * 
     *
     */
    void setExternalThirdPartyLoan(de.smava.webapp.applicant.domain.ExternalThirdPartyLoan externalThirdPartyLoan);

    /**
     * Returns the property 'external third party loan'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.ExternalThirdPartyLoan getExternalThirdPartyLoan();
    /**
     * Setter for the property 'customer third party loan'.
     *
     * 
     *
     */
    void setCustomerThirdPartyLoan(de.smava.webapp.applicant.domain.CustomerThirdPartyLoan customerThirdPartyLoan);

    /**
     * Returns the property 'customer third party loan'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.CustomerThirdPartyLoan getCustomerThirdPartyLoan();
    /**
     * Setter for the property 'third party loan'.
     *
     * 
     *
     */
    void setThirdPartyLoan(de.smava.webapp.applicant.domain.ThirdPartyLoan thirdPartyLoan);

    /**
     * Returns the property 'third party loan'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.domain.ThirdPartyLoan getThirdPartyLoan();
    /**
     * Helper method to get reference of this object as model type.
     */
    ThirdPartyLoanProcessingLog asThirdPartyLoanProcessingLog();
}
