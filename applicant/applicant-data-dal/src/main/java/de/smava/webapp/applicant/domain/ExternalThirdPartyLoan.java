//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(external third party loan)}
import de.smava.webapp.applicant.domain.history.ExternalThirdPartyLoanHistory;

import java.util.*;

import de.smava.webapp.applicant.domain.BankAccount;
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ExternalThirdPartyLoans'.
 *
 * 
 *
 * @author generator
 */
public class ExternalThirdPartyLoan extends ExternalThirdPartyLoanHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(external third party loan)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Double _originalAmount;
        protected Double _monthlyRate;
        protected java.util.Date _loanStartDate;
        protected java.util.Date _loanEndDate;
        protected Long _loanId;
        protected Double _effectiveInterest;
        protected de.smava.webapp.applicant.type.ThirdPartyLoanType _loanType;
        protected Double _redemptionSum;
        protected BankAccount _bankAccount;
        protected Double _drawingLimit;
        protected Boolean _sameCreditBankAccount;
        protected String _schufaTokenCode;
        protected de.smava.webapp.applicant.type.ThirdPartyLoanSource _loanSource;
        
                            /**
     * Setter for the property 'original amount'.
     *
     * 
     *
     */
    public void setOriginalAmount(Double originalAmount) {
        if (!_originalAmountIsSet) {
            _originalAmountIsSet = true;
            _originalAmountInitVal = getOriginalAmount();
        }
        registerChange("original amount", _originalAmountInitVal, originalAmount);
        _originalAmount = originalAmount;
    }
                        
    /**
     * Returns the property 'original amount'.
     *
     * 
     *
     */
    public Double getOriginalAmount() {
        return _originalAmount;
    }
                                    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    public void setMonthlyRate(Double monthlyRate) {
        if (!_monthlyRateIsSet) {
            _monthlyRateIsSet = true;
            _monthlyRateInitVal = getMonthlyRate();
        }
        registerChange("monthly rate", _monthlyRateInitVal, monthlyRate);
        _monthlyRate = monthlyRate;
    }
                        
    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    public Double getMonthlyRate() {
        return _monthlyRate;
    }
                                    /**
     * Setter for the property 'loan start date'.
     *
     * 
     *
     */
    public void setLoanStartDate(java.util.Date loanStartDate) {
        if (!_loanStartDateIsSet) {
            _loanStartDateIsSet = true;
            _loanStartDateInitVal = getLoanStartDate();
        }
        registerChange("loan start date", _loanStartDateInitVal, loanStartDate);
        _loanStartDate = loanStartDate;
    }
                        
    /**
     * Returns the property 'loan start date'.
     *
     * 
     *
     */
    public java.util.Date getLoanStartDate() {
        return _loanStartDate;
    }
                                    /**
     * Setter for the property 'loan end date'.
     *
     * 
     *
     */
    public void setLoanEndDate(java.util.Date loanEndDate) {
        if (!_loanEndDateIsSet) {
            _loanEndDateIsSet = true;
            _loanEndDateInitVal = getLoanEndDate();
        }
        registerChange("loan end date", _loanEndDateInitVal, loanEndDate);
        _loanEndDate = loanEndDate;
    }
                        
    /**
     * Returns the property 'loan end date'.
     *
     * 
     *
     */
    public java.util.Date getLoanEndDate() {
        return _loanEndDate;
    }
                                            
    /**
     * Setter for the property 'loan id'.
     *
     * Here is the foreign key to LoanApplication. We cannot refer to the type directly, because we would run in cyclic dependencies between the dal projects.
     *
     */
    public void setLoanId(Long loanId) {
        _loanId = loanId;
    }
            
    /**
     * Returns the property 'loan id'.
     *
     * Here is the foreign key to LoanApplication. We cannot refer to the type directly, because we would run in cyclic dependencies between the dal projects.
     *
     */
    public Long getLoanId() {
        return _loanId;
    }
                                    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    public void setEffectiveInterest(Double effectiveInterest) {
        if (!_effectiveInterestIsSet) {
            _effectiveInterestIsSet = true;
            _effectiveInterestInitVal = getEffectiveInterest();
        }
        registerChange("effective interest", _effectiveInterestInitVal, effectiveInterest);
        _effectiveInterest = effectiveInterest;
    }
                        
    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    public Double getEffectiveInterest() {
        return _effectiveInterest;
    }
                                    /**
     * Setter for the property 'loan type'.
     *
     * 
     *
     */
    public void setLoanType(de.smava.webapp.applicant.type.ThirdPartyLoanType loanType) {
        if (!_loanTypeIsSet) {
            _loanTypeIsSet = true;
            _loanTypeInitVal = getLoanType();
        }
        registerChange("loan type", _loanTypeInitVal, loanType);
        _loanType = loanType;
    }
                        
    /**
     * Returns the property 'loan type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.ThirdPartyLoanType getLoanType() {
        return _loanType;
    }
                                    /**
     * Setter for the property 'redemption sum'.
     *
     * 
     *
     */
    public void setRedemptionSum(Double redemptionSum) {
        if (!_redemptionSumIsSet) {
            _redemptionSumIsSet = true;
            _redemptionSumInitVal = getRedemptionSum();
        }
        registerChange("redemption sum", _redemptionSumInitVal, redemptionSum);
        _redemptionSum = redemptionSum;
    }
                        
    /**
     * Returns the property 'redemption sum'.
     *
     * 
     *
     */
    public Double getRedemptionSum() {
        return _redemptionSum;
    }
                                            
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    public void setBankAccount(BankAccount bankAccount) {
        _bankAccount = bankAccount;
    }
            
    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    public BankAccount getBankAccount() {
        return _bankAccount;
    }
                                    /**
     * Setter for the property 'drawing limit'.
     *
     * 
     *
     */
    public void setDrawingLimit(Double drawingLimit) {
        if (!_drawingLimitIsSet) {
            _drawingLimitIsSet = true;
            _drawingLimitInitVal = getDrawingLimit();
        }
        registerChange("drawing limit", _drawingLimitInitVal, drawingLimit);
        _drawingLimit = drawingLimit;
    }
                        
    /**
     * Returns the property 'drawing limit'.
     *
     * 
     *
     */
    public Double getDrawingLimit() {
        return _drawingLimit;
    }
                                    /**
     * Setter for the property 'same credit bank account'.
     *
     * 
     *
     */
    public void setSameCreditBankAccount(Boolean sameCreditBankAccount) {
        if (!_sameCreditBankAccountIsSet) {
            _sameCreditBankAccountIsSet = true;
            _sameCreditBankAccountInitVal = getSameCreditBankAccount();
        }
        registerChange("same credit bank account", _sameCreditBankAccountInitVal, sameCreditBankAccount);
        _sameCreditBankAccount = sameCreditBankAccount;
    }
                        
    /**
     * Returns the property 'same credit bank account'.
     *
     * 
     *
     */
    public Boolean getSameCreditBankAccount() {
        return _sameCreditBankAccount;
    }
                                    /**
     * Setter for the property 'schufa token code'.
     *
     * 
     *
     */
    public void setSchufaTokenCode(String schufaTokenCode) {
        if (!_schufaTokenCodeIsSet) {
            _schufaTokenCodeIsSet = true;
            _schufaTokenCodeInitVal = getSchufaTokenCode();
        }
        registerChange("schufa token code", _schufaTokenCodeInitVal, schufaTokenCode);
        _schufaTokenCode = schufaTokenCode;
    }
                        
    /**
     * Returns the property 'schufa token code'.
     *
     * 
     *
     */
    public String getSchufaTokenCode() {
        return _schufaTokenCode;
    }
                                    /**
     * Setter for the property 'loan source'.
     *
     * 
     *
     */
    public void setLoanSource(de.smava.webapp.applicant.type.ThirdPartyLoanSource loanSource) {
        if (!_loanSourceIsSet) {
            _loanSourceIsSet = true;
            _loanSourceInitVal = getLoanSource();
        }
        registerChange("loan source", _loanSourceInitVal, loanSource);
        _loanSource = loanSource;
    }
                        
    /**
     * Returns the property 'loan source'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.ThirdPartyLoanSource getLoanSource() {
        return _loanSource;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_bankAccount instanceof de.smava.webapp.commons.domain.Entity && !_bankAccount.getChangeSet().isEmpty()) {
             for (String element : _bankAccount.getChangeSet()) {
                 result.add("bank account : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ExternalThirdPartyLoan.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _loanStartDate=").append(_loanStartDate);
            builder.append("\n    _loanEndDate=").append(_loanEndDate);
            builder.append("\n    _loanType=").append(_loanType);
            builder.append("\n    _schufaTokenCode=").append(_schufaTokenCode);
            builder.append("\n    _loanSource=").append(_loanSource);
            builder.append("\n}");
        } else {
            builder.append(ExternalThirdPartyLoan.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ExternalThirdPartyLoan asExternalThirdPartyLoan() {
        return this;
    }
}
