package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Freelancer;


/**
 * The domain object that represents 'Freelancers'.
 *
 * @author generator
 */
public interface FreelancerEntityInterface {

    /**
     * Setter for the property 'occupationType'.
     *
     * 
     *
     */
    void setOccupationType(de.smava.webapp.applicant.type.FreelancerOccupationType occupationType);

    /**
     * Returns the property 'occupationType'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.FreelancerOccupationType getOccupationType();
    /**
     * Helper method to get reference of this object as model type.
     */
    Freelancer asFreelancer();
}
