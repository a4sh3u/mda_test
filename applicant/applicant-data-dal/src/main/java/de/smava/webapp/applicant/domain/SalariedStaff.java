//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(salaried staff)}
import de.smava.webapp.applicant.domain.history.SalariedStaffHistory;

import java.util.Date;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SalariedStaffs'.
 *
 * 
 *
 * @author generator
 */
public abstract class SalariedStaff extends SalariedStaffHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(salaried staff)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected java.util.Date _employedSince;
        protected de.smava.webapp.applicant.type.IndustrySectorType _industrySector;
        protected Employer _employer;
        protected de.smava.webapp.applicant.type.SalariedStaffType _staffType;
        protected Boolean _probationPeriod;
        protected java.util.Date _endOfProbationPeriod;
        protected Boolean _temporarilyEmployed;
        protected java.util.Date _endOfTemporaryEmployment;
        
                            /**
     * Setter for the property 'employed since'.
     *
     * 
     *
     */
    public void setEmployedSince(java.util.Date employedSince) {
        if (!_employedSinceIsSet) {
            _employedSinceIsSet = true;
            _employedSinceInitVal = getEmployedSince();
        }
        registerChange("employed since", _employedSinceInitVal, employedSince);
        _employedSince = employedSince;
    }
                        
    /**
     * Returns the property 'employed since'.
     *
     * 
     *
     */
    public java.util.Date getEmployedSince() {
        return _employedSince;
    }
                                    /**
     * Setter for the property 'industry sector'.
     *
     * 
     *
     */
    public void setIndustrySector(de.smava.webapp.applicant.type.IndustrySectorType industrySector) {
        if (!_industrySectorIsSet) {
            _industrySectorIsSet = true;
            _industrySectorInitVal = getIndustrySector();
        }
        registerChange("industry sector", _industrySectorInitVal, industrySector);
        _industrySector = industrySector;
    }
                        
    /**
     * Returns the property 'industry sector'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.IndustrySectorType getIndustrySector() {
        return _industrySector;
    }
                                            
    /**
     * Setter for the property 'employer'.
     *
     * 
     *
     */
    public void setEmployer(Employer employer) {
        _employer = employer;
    }
            
    /**
     * Returns the property 'employer'.
     *
     * 
     *
     */
    public Employer getEmployer() {
        return _employer;
    }
                                    /**
     * Setter for the property 'staff type'.
     *
     * 
     *
     */
    public void setStaffType(de.smava.webapp.applicant.type.SalariedStaffType staffType) {
        if (!_staffTypeIsSet) {
            _staffTypeIsSet = true;
            _staffTypeInitVal = getStaffType();
        }
        registerChange("staff type", _staffTypeInitVal, staffType);
        _staffType = staffType;
    }
                        
    /**
     * Returns the property 'staff type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.SalariedStaffType getStaffType() {
        return _staffType;
    }
                                    /**
     * Setter for the property 'probation period'.
     *
     * 
     *
     */
    public void setProbationPeriod(Boolean probationPeriod) {
        if (!_probationPeriodIsSet) {
            _probationPeriodIsSet = true;
            _probationPeriodInitVal = getProbationPeriod();
        }
        registerChange("probation period", _probationPeriodInitVal, probationPeriod);
        _probationPeriod = probationPeriod;
    }
                        
    /**
     * Returns the property 'probation period'.
     *
     * 
     *
     */
    public Boolean getProbationPeriod() {
        return _probationPeriod;
    }
                                    /**
     * Setter for the property 'end of probation period'.
     *
     * 
     *
     */
    public void setEndOfProbationPeriod(java.util.Date endOfProbationPeriod) {
        if (!_endOfProbationPeriodIsSet) {
            _endOfProbationPeriodIsSet = true;
            _endOfProbationPeriodInitVal = getEndOfProbationPeriod();
        }
        registerChange("end of probation period", _endOfProbationPeriodInitVal, endOfProbationPeriod);
        _endOfProbationPeriod = endOfProbationPeriod;
    }
                        
    /**
     * Returns the property 'end of probation period'.
     *
     * 
     *
     */
    public java.util.Date getEndOfProbationPeriod() {
        return _endOfProbationPeriod;
    }
                                    /**
     * Setter for the property 'temporarily employed'.
     *
     * 
     *
     */
    public void setTemporarilyEmployed(Boolean temporarilyEmployed) {
        if (!_temporarilyEmployedIsSet) {
            _temporarilyEmployedIsSet = true;
            _temporarilyEmployedInitVal = getTemporarilyEmployed();
        }
        registerChange("temporarily employed", _temporarilyEmployedInitVal, temporarilyEmployed);
        _temporarilyEmployed = temporarilyEmployed;
    }
                        
    /**
     * Returns the property 'temporarily employed'.
     *
     * 
     *
     */
    public Boolean getTemporarilyEmployed() {
        return _temporarilyEmployed;
    }
                                    /**
     * Setter for the property 'end of temporary employment'.
     *
     * 
     *
     */
    public void setEndOfTemporaryEmployment(java.util.Date endOfTemporaryEmployment) {
        if (!_endOfTemporaryEmploymentIsSet) {
            _endOfTemporaryEmploymentIsSet = true;
            _endOfTemporaryEmploymentInitVal = getEndOfTemporaryEmployment();
        }
        registerChange("end of temporary employment", _endOfTemporaryEmploymentInitVal, endOfTemporaryEmployment);
        _endOfTemporaryEmployment = endOfTemporaryEmployment;
    }
                        
    /**
     * Returns the property 'end of temporary employment'.
     *
     * 
     *
     */
    public java.util.Date getEndOfTemporaryEmployment() {
        return _endOfTemporaryEmployment;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_employer instanceof de.smava.webapp.commons.domain.Entity && !_employer.getChangeSet().isEmpty()) {
             for (String element : _employer.getChangeSet()) {
                 result.add("employer : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SalariedStaff.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _employedSince=").append(_employedSince);
            builder.append("\n    _industrySector=").append(_industrySector);
            builder.append("\n    _staffType=").append(_staffType);
            builder.append("\n    _endOfProbationPeriod=").append(_endOfProbationPeriod);
            builder.append("\n    _endOfTemporaryEmployment=").append(_endOfTemporaryEmployment);
            builder.append("\n}");
        } else {
            builder.append(SalariedStaff.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SalariedStaff asSalariedStaff() {
        return this;
    }
}
