package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractAddress;




/**
 * The domain object that has all history aggregation related fields for 'Addresss'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AddressHistory extends AbstractAddress {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient String _streetInitVal;
    protected transient boolean _streetIsSet;
    protected transient String _houseNumberInitVal;
    protected transient boolean _houseNumberIsSet;
    protected transient String _zipCodeInitVal;
    protected transient boolean _zipCodeIsSet;
    protected transient String _cityInitVal;
    protected transient boolean _cityIsSet;
    protected transient de.smava.webapp.applicant.type.Country _countryInitVal;
    protected transient boolean _countryIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'street'.
     */
    public String streetInitVal() {
        String result;
        if (_streetIsSet) {
            result = _streetInitVal;
        } else {
            result = getStreet();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'street'.
     */
    public boolean streetIsDirty() {
        return !valuesAreEqual(streetInitVal(), getStreet());
    }

    /**
     * Returns true if the setter method was called for the property 'street'.
     */
    public boolean streetIsSet() {
        return _streetIsSet;
    }
	
    /**
     * Returns the initial value of the property 'house number'.
     */
    public String houseNumberInitVal() {
        String result;
        if (_houseNumberIsSet) {
            result = _houseNumberInitVal;
        } else {
            result = getHouseNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'house number'.
     */
    public boolean houseNumberIsDirty() {
        return !valuesAreEqual(houseNumberInitVal(), getHouseNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'house number'.
     */
    public boolean houseNumberIsSet() {
        return _houseNumberIsSet;
    }
	
    /**
     * Returns the initial value of the property 'zip code'.
     */
    public String zipCodeInitVal() {
        String result;
        if (_zipCodeIsSet) {
            result = _zipCodeInitVal;
        } else {
            result = getZipCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'zip code'.
     */
    public boolean zipCodeIsDirty() {
        return !valuesAreEqual(zipCodeInitVal(), getZipCode());
    }

    /**
     * Returns true if the setter method was called for the property 'zip code'.
     */
    public boolean zipCodeIsSet() {
        return _zipCodeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'city'.
     */
    public String cityInitVal() {
        String result;
        if (_cityIsSet) {
            result = _cityInitVal;
        } else {
            result = getCity();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'city'.
     */
    public boolean cityIsDirty() {
        return !valuesAreEqual(cityInitVal(), getCity());
    }

    /**
     * Returns true if the setter method was called for the property 'city'.
     */
    public boolean cityIsSet() {
        return _cityIsSet;
    }
	
    /**
     * Returns the initial value of the property 'country'.
     */
    public de.smava.webapp.applicant.type.Country countryInitVal() {
        de.smava.webapp.applicant.type.Country result;
        if (_countryIsSet) {
            result = _countryInitVal;
        } else {
            result = getCountry();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'country'.
     */
    public boolean countryIsDirty() {
        return !valuesAreEqual(countryInitVal(), getCountry());
    }

    /**
     * Returns true if the setter method was called for the property 'country'.
     */
    public boolean countryIsSet() {
        return _countryIsSet;
    }

}
