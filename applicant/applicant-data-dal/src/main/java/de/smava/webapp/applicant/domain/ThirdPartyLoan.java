//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(third party loan)}

import de.smava.webapp.applicant.domain.history.ThirdPartyLoanHistory;

import java.util.Date;
import java.util.Set;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ThirdPartyLoans'.
 *
 * 
 *
 * @author generator
 */
public class ThirdPartyLoan extends ThirdPartyLoanHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(third party loan)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Double _originalAmount;
        protected Double _monthlyRate;
        protected java.util.Date _loanStartDate;
        protected java.util.Date _loanEndDate;
        protected Long _loanId;
        protected Double _effectiveInterest;
        protected boolean _consolidationWish;
        protected de.smava.webapp.applicant.type.ThirdPartyLoanType _loanType;
        protected Double _redemptionSum;
        protected BankAccount _bankAccount;
        protected Double _drawingLimit;
        protected Boolean _sameCreditBankAccount;
        protected de.smava.webapp.applicant.type.ThirdPartyLoanSource _loanSource;
        protected CustomerThirdPartyLoan _customerThirdPartyLoan;
        protected ExternalThirdPartyLoan _externalThirdPartyLoan;
        protected Set<ThirdPartyLoanProcessingLog> _processingLogs;
        
                            /**
     * Setter for the property 'original amount'.
     *
     * 
     *
     */
    public void setOriginalAmount(Double originalAmount) {
        if (!_originalAmountIsSet) {
            _originalAmountIsSet = true;
            _originalAmountInitVal = getOriginalAmount();
        }
        registerChange("original amount", _originalAmountInitVal, originalAmount);
        _originalAmount = originalAmount;
    }
                        
    /**
     * Returns the property 'original amount'.
     *
     * 
     *
     */
    public Double getOriginalAmount() {
        return _originalAmount;
    }
                                    /**
     * Setter for the property 'monthly rate'.
     *
     * 
     *
     */
    public void setMonthlyRate(Double monthlyRate) {
        if (!_monthlyRateIsSet) {
            _monthlyRateIsSet = true;
            _monthlyRateInitVal = getMonthlyRate();
        }
        registerChange("monthly rate", _monthlyRateInitVal, monthlyRate);
        _monthlyRate = monthlyRate;
    }
                        
    /**
     * Returns the property 'monthly rate'.
     *
     * 
     *
     */
    public Double getMonthlyRate() {
        return _monthlyRate;
    }
                                    /**
     * Setter for the property 'loan start date'.
     *
     * 
     *
     */
    public void setLoanStartDate(java.util.Date loanStartDate) {
        if (!_loanStartDateIsSet) {
            _loanStartDateIsSet = true;
            _loanStartDateInitVal = getLoanStartDate();
        }
        registerChange("loan start date", _loanStartDateInitVal, loanStartDate);
        _loanStartDate = loanStartDate;
    }
                        
    /**
     * Returns the property 'loan start date'.
     *
     * 
     *
     */
    public java.util.Date getLoanStartDate() {
        return _loanStartDate;
    }
                                    /**
     * Setter for the property 'loan end date'.
     *
     * 
     *
     */
    public void setLoanEndDate(java.util.Date loanEndDate) {
        if (!_loanEndDateIsSet) {
            _loanEndDateIsSet = true;
            _loanEndDateInitVal = getLoanEndDate();
        }
        registerChange("loan end date", _loanEndDateInitVal, loanEndDate);
        _loanEndDate = loanEndDate;
    }
                        
    /**
     * Returns the property 'loan end date'.
     *
     * 
     *
     */
    public java.util.Date getLoanEndDate() {
        return _loanEndDate;
    }
                                            
    /**
     * Setter for the property 'loan id'.
     *
     * Here is the foreign key to LoanApplication. We cannot refer to the type directly, because we would run in cyclic dependencies between the dal projects.
     *
     */
    public void setLoanId(Long loanId) {
        _loanId = loanId;
    }
            
    /**
     * Returns the property 'loan id'.
     *
     * Here is the foreign key to LoanApplication. We cannot refer to the type directly, because we would run in cyclic dependencies between the dal projects.
     *
     */
    public Long getLoanId() {
        return _loanId;
    }
                                    /**
     * Setter for the property 'effective interest'.
     *
     * 
     *
     */
    public void setEffectiveInterest(Double effectiveInterest) {
        if (!_effectiveInterestIsSet) {
            _effectiveInterestIsSet = true;
            _effectiveInterestInitVal = getEffectiveInterest();
        }
        registerChange("effective interest", _effectiveInterestInitVal, effectiveInterest);
        _effectiveInterest = effectiveInterest;
    }
                        
    /**
     * Returns the property 'effective interest'.
     *
     * 
     *
     */
    public Double getEffectiveInterest() {
        return _effectiveInterest;
    }
                                    /**
     * Setter for the property 'consolidation wish'.
     *
     * 
     *
     */
    public void setConsolidationWish(boolean consolidationWish) {
        if (!_consolidationWishIsSet) {
            _consolidationWishIsSet = true;
            _consolidationWishInitVal = getConsolidationWish();
        }
        registerChange("consolidation wish", _consolidationWishInitVal, consolidationWish);
        _consolidationWish = consolidationWish;
    }
                        
    /**
     * Returns the property 'consolidation wish'.
     *
     * 
     *
     */
    public boolean getConsolidationWish() {
        return _consolidationWish;
    }
                                    /**
     * Setter for the property 'loan type'.
     *
     * 
     *
     */
    public void setLoanType(de.smava.webapp.applicant.type.ThirdPartyLoanType loanType) {
        if (!_loanTypeIsSet) {
            _loanTypeIsSet = true;
            _loanTypeInitVal = getLoanType();
        }
        registerChange("loan type", _loanTypeInitVal, loanType);
        _loanType = loanType;
    }
                        
    /**
     * Returns the property 'loan type'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.ThirdPartyLoanType getLoanType() {
        return _loanType;
    }
                                    /**
     * Setter for the property 'redemption sum'.
     *
     * 
     *
     */
    public void setRedemptionSum(Double redemptionSum) {
        if (!_redemptionSumIsSet) {
            _redemptionSumIsSet = true;
            _redemptionSumInitVal = getRedemptionSum();
        }
        registerChange("redemption sum", _redemptionSumInitVal, redemptionSum);
        _redemptionSum = redemptionSum;
    }
                        
    /**
     * Returns the property 'redemption sum'.
     *
     * 
     *
     */
    public Double getRedemptionSum() {
        return _redemptionSum;
    }
                                            
    /**
     * Setter for the property 'bank account'.
     *
     * 
     *
     */
    public void setBankAccount(BankAccount bankAccount) {
        _bankAccount = bankAccount;
    }
            
    /**
     * Returns the property 'bank account'.
     *
     * 
     *
     */
    public BankAccount getBankAccount() {
        return _bankAccount;
    }
                                    /**
     * Setter for the property 'drawing limit'.
     *
     * 
     *
     */
    public void setDrawingLimit(Double drawingLimit) {
        if (!_drawingLimitIsSet) {
            _drawingLimitIsSet = true;
            _drawingLimitInitVal = getDrawingLimit();
        }
        registerChange("drawing limit", _drawingLimitInitVal, drawingLimit);
        _drawingLimit = drawingLimit;
    }
                        
    /**
     * Returns the property 'drawing limit'.
     *
     * 
     *
     */
    public Double getDrawingLimit() {
        return _drawingLimit;
    }
                                    /**
     * Setter for the property 'same credit bank account'.
     *
     * 
     *
     */
    public void setSameCreditBankAccount(Boolean sameCreditBankAccount) {
        if (!_sameCreditBankAccountIsSet) {
            _sameCreditBankAccountIsSet = true;
            _sameCreditBankAccountInitVal = getSameCreditBankAccount();
        }
        registerChange("same credit bank account", _sameCreditBankAccountInitVal, sameCreditBankAccount);
        _sameCreditBankAccount = sameCreditBankAccount;
    }
                        
    /**
     * Returns the property 'same credit bank account'.
     *
     * 
     *
     */
    public Boolean getSameCreditBankAccount() {
        return _sameCreditBankAccount;
    }
                                    /**
     * Setter for the property 'loan source'.
     *
     * 
     *
     */
    public void setLoanSource(de.smava.webapp.applicant.type.ThirdPartyLoanSource loanSource) {
        if (!_loanSourceIsSet) {
            _loanSourceIsSet = true;
            _loanSourceInitVal = getLoanSource();
        }
        registerChange("loan source", _loanSourceInitVal, loanSource);
        _loanSource = loanSource;
    }
                        
    /**
     * Returns the property 'loan source'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.type.ThirdPartyLoanSource getLoanSource() {
        return _loanSource;
    }
                                            
    /**
     * Setter for the property 'customer third party loan'.
     *
     * 
     *
     */
    public void setCustomerThirdPartyLoan(CustomerThirdPartyLoan customerThirdPartyLoan) {
        _customerThirdPartyLoan = customerThirdPartyLoan;
    }
            
    /**
     * Returns the property 'customer third party loan'.
     *
     * 
     *
     */
    public CustomerThirdPartyLoan getCustomerThirdPartyLoan() {
        return _customerThirdPartyLoan;
    }
                                            
    /**
     * Setter for the property 'external third party loan'.
     *
     * 
     *
     */
    public void setExternalThirdPartyLoan(ExternalThirdPartyLoan externalThirdPartyLoan) {
        _externalThirdPartyLoan = externalThirdPartyLoan;
    }
            
    /**
     * Returns the property 'external third party loan'.
     *
     * 
     *
     */
    public ExternalThirdPartyLoan getExternalThirdPartyLoan() {
        return _externalThirdPartyLoan;
    }
                                            
    /**
     * Setter for the property 'processing logs'.
     *
     * 
     *
     */
    public void setProcessingLogs(Set<ThirdPartyLoanProcessingLog> processingLogs) {
        _processingLogs = processingLogs;
    }
            
    /**
     * Returns the property 'processing logs'.
     *
     * 
     *
     */
    public Set<ThirdPartyLoanProcessingLog> getProcessingLogs() {
        return _processingLogs;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_bankAccount instanceof de.smava.webapp.commons.domain.Entity && !_bankAccount.getChangeSet().isEmpty()) {
             for (String element : _bankAccount.getChangeSet()) {
                 result.add("bank account : " + element);
             }
         }

         if (_customerThirdPartyLoan instanceof de.smava.webapp.commons.domain.Entity && !_customerThirdPartyLoan.getChangeSet().isEmpty()) {
             for (String element : _customerThirdPartyLoan.getChangeSet()) {
                 result.add("customer third party loan : " + element);
             }
         }

         if (_externalThirdPartyLoan instanceof de.smava.webapp.commons.domain.Entity && !_externalThirdPartyLoan.getChangeSet().isEmpty()) {
             for (String element : _externalThirdPartyLoan.getChangeSet()) {
                 result.add("external third party loan : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(ThirdPartyLoan.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _loanStartDate=").append(_loanStartDate);
            builder.append("\n    _loanEndDate=").append(_loanEndDate);
            builder.append("\n    _consolidationWish=").append(_consolidationWish);
            builder.append("\n    _loanType=").append(_loanType);
            builder.append("\n    _loanSource=").append(_loanSource);
            builder.append("\n}");
        } else {
            builder.append(ThirdPartyLoan.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public ThirdPartyLoan asThirdPartyLoan() {
        return this;
    }
}
