//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(personal data)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.PersonalDataEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'PersonalDatas'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractPersonalData
    extends BrokerageEntity    implements PersonalDataEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractPersonalData.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof PersonalData)) {
            return;
        }
        
        this.setGender(((PersonalData) oldEntity).getGender());    
        this.setFirstName(((PersonalData) oldEntity).getFirstName());    
        this.setLastName(((PersonalData) oldEntity).getLastName());    
        this.setBirthName(((PersonalData) oldEntity).getBirthName());    
        this.setBirthDate(((PersonalData) oldEntity).getBirthDate());    
        this.setMaritalState(((PersonalData) oldEntity).getMaritalState());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof PersonalData)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getGender(), ((PersonalData) otherEntity).getGender());    
        equals = equals && valuesAreEqual(this.getFirstName(), ((PersonalData) otherEntity).getFirstName());    
        equals = equals && valuesAreEqual(this.getLastName(), ((PersonalData) otherEntity).getLastName());    
        equals = equals && valuesAreEqual(this.getBirthName(), ((PersonalData) otherEntity).getBirthName());    
        equals = equals && valuesAreEqual(this.getBirthDate(), ((PersonalData) otherEntity).getBirthDate());    
        equals = equals && valuesAreEqual(this.getMaritalState(), ((PersonalData) otherEntity).getMaritalState());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(personal data)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

