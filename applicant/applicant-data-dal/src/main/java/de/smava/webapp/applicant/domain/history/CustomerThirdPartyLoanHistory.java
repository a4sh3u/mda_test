package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractCustomerThirdPartyLoan;





/**
 * The domain object that has all history aggregation related fields for 'CustomerThirdPartyLoans'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class CustomerThirdPartyLoanHistory extends AbstractCustomerThirdPartyLoan {

    protected transient Double _originalAmountInitVal;
    protected transient boolean _originalAmountIsSet;
    protected transient Double _monthlyRateInitVal;
    protected transient boolean _monthlyRateIsSet;
    protected transient java.util.Date _loanStartDateInitVal;
    protected transient boolean _loanStartDateIsSet;
    protected transient java.util.Date _loanEndDateInitVal;
    protected transient boolean _loanEndDateIsSet;
    protected transient Double _effectiveInterestInitVal;
    protected transient boolean _effectiveInterestIsSet;
    protected transient boolean _consolidationWishInitVal;
    protected transient boolean _consolidationWishIsSet;
    protected transient de.smava.webapp.applicant.type.ThirdPartyLoanType _loanTypeInitVal;
    protected transient boolean _loanTypeIsSet;
    protected transient Double _redemptionSumInitVal;
    protected transient boolean _redemptionSumIsSet;
    protected transient Double _drawingLimitInitVal;
    protected transient boolean _drawingLimitIsSet;
    protected transient Boolean _sameCreditBankAccountInitVal;
    protected transient boolean _sameCreditBankAccountIsSet;
    protected transient de.smava.webapp.applicant.type.ThirdPartyLoanSource _loanSourceInitVal;
    protected transient boolean _loanSourceIsSet;


	
    /**
     * Returns the initial value of the property 'original amount'.
     */
    public Double originalAmountInitVal() {
        Double result;
        if (_originalAmountIsSet) {
            result = _originalAmountInitVal;
        } else {
            result = getOriginalAmount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'original amount'.
     */
    public boolean originalAmountIsDirty() {
        return !valuesAreEqual(originalAmountInitVal(), getOriginalAmount());
    }

    /**
     * Returns true if the setter method was called for the property 'original amount'.
     */
    public boolean originalAmountIsSet() {
        return _originalAmountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'monthly rate'.
     */
    public Double monthlyRateInitVal() {
        Double result;
        if (_monthlyRateIsSet) {
            result = _monthlyRateInitVal;
        } else {
            result = getMonthlyRate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'monthly rate'.
     */
    public boolean monthlyRateIsDirty() {
        return !valuesAreEqual(monthlyRateInitVal(), getMonthlyRate());
    }

    /**
     * Returns true if the setter method was called for the property 'monthly rate'.
     */
    public boolean monthlyRateIsSet() {
        return _monthlyRateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'loan start date'.
     */
    public java.util.Date loanStartDateInitVal() {
        java.util.Date result;
        if (_loanStartDateIsSet) {
            result = _loanStartDateInitVal;
        } else {
            result = getLoanStartDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'loan start date'.
     */
    public boolean loanStartDateIsDirty() {
        return !valuesAreEqual(loanStartDateInitVal(), getLoanStartDate());
    }

    /**
     * Returns true if the setter method was called for the property 'loan start date'.
     */
    public boolean loanStartDateIsSet() {
        return _loanStartDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'loan end date'.
     */
    public java.util.Date loanEndDateInitVal() {
        java.util.Date result;
        if (_loanEndDateIsSet) {
            result = _loanEndDateInitVal;
        } else {
            result = getLoanEndDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'loan end date'.
     */
    public boolean loanEndDateIsDirty() {
        return !valuesAreEqual(loanEndDateInitVal(), getLoanEndDate());
    }

    /**
     * Returns true if the setter method was called for the property 'loan end date'.
     */
    public boolean loanEndDateIsSet() {
        return _loanEndDateIsSet;
    }
		
    /**
     * Returns the initial value of the property 'effective interest'.
     */
    public Double effectiveInterestInitVal() {
        Double result;
        if (_effectiveInterestIsSet) {
            result = _effectiveInterestInitVal;
        } else {
            result = getEffectiveInterest();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'effective interest'.
     */
    public boolean effectiveInterestIsDirty() {
        return !valuesAreEqual(effectiveInterestInitVal(), getEffectiveInterest());
    }

    /**
     * Returns true if the setter method was called for the property 'effective interest'.
     */
    public boolean effectiveInterestIsSet() {
        return _effectiveInterestIsSet;
    }
	
    /**
     * Returns the initial value of the property 'consolidation wish'.
     */
    public boolean consolidationWishInitVal() {
        boolean result;
        if (_consolidationWishIsSet) {
            result = _consolidationWishInitVal;
        } else {
            result = getConsolidationWish();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'consolidation wish'.
     */
    public boolean consolidationWishIsDirty() {
        return !valuesAreEqual(consolidationWishInitVal(), getConsolidationWish());
    }

    /**
     * Returns true if the setter method was called for the property 'consolidation wish'.
     */
    public boolean consolidationWishIsSet() {
        return _consolidationWishIsSet;
    }
	
    /**
     * Returns the initial value of the property 'loan type'.
     */
    public de.smava.webapp.applicant.type.ThirdPartyLoanType loanTypeInitVal() {
        de.smava.webapp.applicant.type.ThirdPartyLoanType result;
        if (_loanTypeIsSet) {
            result = _loanTypeInitVal;
        } else {
            result = getLoanType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'loan type'.
     */
    public boolean loanTypeIsDirty() {
        return !valuesAreEqual(loanTypeInitVal(), getLoanType());
    }

    /**
     * Returns true if the setter method was called for the property 'loan type'.
     */
    public boolean loanTypeIsSet() {
        return _loanTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'redemption sum'.
     */
    public Double redemptionSumInitVal() {
        Double result;
        if (_redemptionSumIsSet) {
            result = _redemptionSumInitVal;
        } else {
            result = getRedemptionSum();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'redemption sum'.
     */
    public boolean redemptionSumIsDirty() {
        return !valuesAreEqual(redemptionSumInitVal(), getRedemptionSum());
    }

    /**
     * Returns true if the setter method was called for the property 'redemption sum'.
     */
    public boolean redemptionSumIsSet() {
        return _redemptionSumIsSet;
    }
		
    /**
     * Returns the initial value of the property 'drawing limit'.
     */
    public Double drawingLimitInitVal() {
        Double result;
        if (_drawingLimitIsSet) {
            result = _drawingLimitInitVal;
        } else {
            result = getDrawingLimit();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'drawing limit'.
     */
    public boolean drawingLimitIsDirty() {
        return !valuesAreEqual(drawingLimitInitVal(), getDrawingLimit());
    }

    /**
     * Returns true if the setter method was called for the property 'drawing limit'.
     */
    public boolean drawingLimitIsSet() {
        return _drawingLimitIsSet;
    }
	
    /**
     * Returns the initial value of the property 'same credit bank account'.
     */
    public Boolean sameCreditBankAccountInitVal() {
        Boolean result;
        if (_sameCreditBankAccountIsSet) {
            result = _sameCreditBankAccountInitVal;
        } else {
            result = getSameCreditBankAccount();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'same credit bank account'.
     */
    public boolean sameCreditBankAccountIsDirty() {
        return !valuesAreEqual(sameCreditBankAccountInitVal(), getSameCreditBankAccount());
    }

    /**
     * Returns true if the setter method was called for the property 'same credit bank account'.
     */
    public boolean sameCreditBankAccountIsSet() {
        return _sameCreditBankAccountIsSet;
    }
	
    /**
     * Returns the initial value of the property 'loan source'.
     */
    public de.smava.webapp.applicant.type.ThirdPartyLoanSource loanSourceInitVal() {
        de.smava.webapp.applicant.type.ThirdPartyLoanSource result;
        if (_loanSourceIsSet) {
            result = _loanSourceInitVal;
        } else {
            result = getLoanSource();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'loan source'.
     */
    public boolean loanSourceIsDirty() {
        return !valuesAreEqual(loanSourceInitVal(), getLoanSource());
    }

    /**
     * Returns true if the setter method was called for the property 'loan source'.
     */
    public boolean loanSourceIsSet() {
        return _loanSourceIsSet;
    }

}
