package de.smava.webapp.applicant.dao;

import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.dao.BaseDao;

/**
 * Created by dkeller on 08.09.15.
 */
public interface BrokerageSchemaDao<T extends BrokerageEntity> extends BaseDao<T>{

}
