package de.smava.webapp.applicant.type;

/**
 * Taken from CMS Aufzaehlungen
 * Created by dkeller on 24.02.15.
 */
public enum Country {

    default_DE, //Deutschland
    AF, //Afghanistan
    EG, //Ägypten
    AL, //Albanien
    DZ, //Algerien
    VI, //Amerikanischen Jungferninseln
    UM, //Amerikanisch-Ozeanien
    AS, //Amerikanisch-Samoa
    AD, //Andorra
    AO, //Angola
    AI, //Anguilla
    AG, //Antigua und Barbuda
    GQ, //Äquatorialguinea
    AR, //Argentinien
    AM, //Armenien
    AW, //Aruba
    AZ, //Aserbaidschan
    ET, //Äthiopien
    AU, //Australien
    BS, //Bahamas
    BH, //Bahrain
    BD, //Bangladesch
    BB, //Barbados
    BY, //Belarus / Weißrußland
    BE, //Belgien, Königreich
    BZ, //Belize
    BJ, //Benin
    BM, //Bermuda
    BT, //Bhutan
    BO, //Bolivien
    BA, //Bosnien-Herzegowina
    BW, //Botsuana
    BR, //Brasilien
    VG, //Britischen Jungferninseln
    BN, //Brunei / Darussalam
    BG, //Bulgarien
    BF, //Burkina Faso
    BI, //Burundi
    CL, //Chile
    CN, //China
    CK, //Cookinseln
    CR, //Costa Rica
    DK, //Dänemark
    DE, //Deutschland
    BV, //die Bouvetinsel
    DM, //Dominica
    DO, //Dominikanische Republik
    DJ, //Dschibuti
    EC, //Ecuador
    SV, //El Salvador
    CI, //Elfenbeinküste
    ER, //Eritrea
    EE, //Estland
    FK, //Falklandinseln
    FO, //Färöer (zu Dänemark)
    FJ, //Fidschi
    FI, //Finnland
    FR, //Frankreich, Französische Republik
    TF, //Französische Süd- und Antarktisgebiete
    PF, //Französisch-Polynesien
    GA, //Gabun, Gabunische Republik
    GM, //Gambia
    GE, //Georgien
    GH, //Ghana
    GI, //Gibraltar
    GD, //Grenada / Granada
    GR, //Griechenland
    GL, //Grönland
    GB, //Großbritannien
    GP, //Guadeloupe
    GU, //Guam, das Territorium
    GT, //Guatemala, Republik
    GF, //Guayana / (Französisch-)Guayana
    GN, //Guinea
    GW, //Guinea-Bissau, Republik
    GY, //Guyana, Kooperative Republik
    HT, //Haiti
    HM, //Heard und McDonaldinseln
    HN, //Honduras
    HK, //Hongkong
    IN, //Indien
    ID, //Indonesien
    IQ, //Irak
    IR, //Iran
    IE, //Irland
    IS, //Island
    IL, //Israel
    IT, //Italien
    JM, //Jamaika
    JP, //Japan
    YE, //Jemen
    JO, //Jordanien / Haschemitisches Königreich
    KY, //Kaimaninseln
    KH, //Kambodscha
    CM, //Kamerun
    CA, //Kanada
    CV, //Kap Verde
    KZ, //Kasachstan, Republik
    QA, //Katar
    KE, //Kenia, Republik
    KG, //Kirgisistan
    KI, //Kiribati
    CC, //Kokosinseln
    CO, //Kolumbien
    KM, //Komoren
    CG, //Kongo
    CD, //Kongo / Kinshasa
    KV, //Kosovo
    HR, //Kroatien
    CU, //Kuba
    KW, //Kuwait
    LA, //Laos, Demokratische Volksrepublik
    LS, //Lesotho
    LV, //Lettland
    LB, //Libanon, Libanesische Republik
    LR, //Liberia
    LY, //Libyen
    LI, //Liechtenstein
    LT, //Litauen
    LU, //Luxemburg
    MO, //Macau
    MG, //Madagaskar
    MW, //Malawi
    MY, //Malaysia
    MV, //Malediven
    ML, //Mali
    MT, //Malta
    MP, //Marianen
    MA, //Marokko
    MH, //Marshallinseln
    MQ, //Martinique
    MR, //Mauretanien
    MU, //Mauritius
    YT, //Mayotte
    MK, //Mazedonien
    MX, //Mexiko
    FM, //Mikronesien
    MD, //Moldau
    MC, //Monaco
    MN, //Mongolei
    ME, //Montenegro
    MS, //Montserrat
    MZ, //Mosambik
    MM, //Myanmar (Burma)
    NA, //Namibia
    NR, //Nauru
    NP, //Nepal
    NC, //Neukaledonien
    NZ, //Neuseeland
    NI, //Nicaragua
    NL, //Niederlande
    AN, //Niederländische Antillen
    NE, //Niger
    NG, //Nigeria
    NU, //Niue
    KP, //Nordkorea
    NF, //Norfolkinsel das Territorium
    NO, //Norwegen
    OM, //Oman
    AT, //Österreich
    PK, //Pakistan
    PS, //Palästina / Gaza-Streifen / Westjordanland
    PW, //Palau
    PA, //Panama
    PG, //Papua-Neuguinea
    PY, //Paraguay
    PE, //Peru
    PH, //Philippinen
    PN, //Pitcairninseln
    PL, //Polen
    PT, //Portugal
    PR, //Puerto Rico
    RE, //Réunion
    RW, //Ruanda
    RO, //Rumänien
    RU, //Russland / Russische Föderation
    SB, //Salomonen
    ZM, //Sambia
    WS, //Samoa / Westsamoa
    SM, //San Marino / Saint Marin
    ST, //Sao Tomé und Principe
    SA, //Saudi-Arabien
    SE, //Schweden
    CH, //Schweiz
    SN, //Senegal
    RS, //Serbien
    YU, //Serbien und Montenegro
    SC, //Seychellen
    SL, //Sierra Leone
    ZW, //Simbabwe
    SG, //Singapur
    SK, //Slowakei
    SI, //Slowenien
    SO, //Somalia
    ES, //Spanien
    LK, //Sri Lanka
    SH, //St. Helena
    KN, //St. Kitts und Nevis
    LC, //St. Lucia / Santa Lucia
    PM, //St. Pierre und Miquelon
    VC, //St. Vincent und die Grenadinen
    ZA, //Südafrika
    SD, //Sudan
    GS, //Südgeorgien / Südliche Sandwichinseln
    KR, //Südkorea
    SR, //Suriname
    SJ, //Svalbard und Jan Mayen
    SZ, //Swasiland
    SY, //Syrien / Siria
    TJ, //Tadschikistan
    TW, //Taiwan / Formosa / China
    TZ, //Tansania
    TH, //Thailand
    TL, //Timor-Leste
    TG, //Togo
    TK, //Tokelau
    TO, //Tonga
    TT, //Trinidad und Tobago
    TD, //Tschad / Tchad
    IO, //Tschagosinseln / Britisches Territorium
    CZ, //Tschechische Republik / Tschechien
    TN, //Tunesien
    TR, //Türkei
    TM, //Turkmenistan
    TC, //Turks- und Caicosinseln
    TV, //Tuvalu
    UG, //Uganda
    UA, //Ukraine
    HU, //Ungarn
    UY, //Uruguay
    US, //USA / Vereinigte Staaten von Amerika
    UZ, //Usbekistan
    VU, //Vanuatu
    VA, //Vatikanstadt / Heiliger Stuhl
    VE, //Venezuela
    AE, //Vereinigte Arabische Emirate
    VN, //Vietnam
    WF, //Wallis und Futuna
    CX, //Weihnachtsinsel
    EH, //Westsahara
    CF, //Zentralafrikanische Republik
    CY, //Zypern
    STATELESS, //Staatenlos
    OTHER; //Sonstige

    public boolean isGermany() {
        return this == default_DE || this == DE;
    }
}
