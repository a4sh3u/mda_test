//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(self employed)}

import de.smava.webapp.applicant.domain.Employment;
import de.smava.webapp.applicant.domain.SelfEmployed;
import de.smava.webapp.applicant.domain.interfaces.EmploymentEntityInterface;
import de.smava.webapp.applicant.domain.interfaces.SelfEmployedEntityInterface;
import de.smava.webapp.applicant.type.IndustrySectorType;
import de.smava.webapp.applicant.type.SelfEmployedType;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SelfEmployeds'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSelfEmployed
    extends Employment    implements SelfEmployedEntityInterface    ,EmploymentEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractSelfEmployed.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SelfEmployed)) {
            return;
        }
        
        this.setTradeTaxesRequired(((SelfEmployed) oldEntity).getTradeTaxesRequired());    
        this.setChurchTaxesRequired(((SelfEmployed) oldEntity).getChurchTaxesRequired());    
        this.setStartOfBusiness(((SelfEmployed) oldEntity).getStartOfBusiness());    
        this.setYearlyCalculations(((SelfEmployed) oldEntity).getYearlyCalculations());    
        this.setIndustrySector(((SelfEmployed) oldEntity).getIndustrySector());    
        this.setSelfEmployedType(((SelfEmployed) oldEntity).getSelfEmployedType());    
        super.copyFromOldEntity(oldEntity);
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SelfEmployed)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getTradeTaxesRequired(), ((SelfEmployed) otherEntity).getTradeTaxesRequired());    
        equals = equals && valuesAreEqual(this.getChurchTaxesRequired(), ((SelfEmployed) otherEntity).getChurchTaxesRequired());    
        equals = equals && valuesAreEqual(this.getStartOfBusiness(), ((SelfEmployed) otherEntity).getStartOfBusiness());    
        equals = equals && valuesAreEqual(this.getYearlyCalculations(), ((SelfEmployed) otherEntity).getYearlyCalculations());    
        equals = equals && valuesAreEqual(this.getIndustrySector(), ((SelfEmployed) otherEntity).getIndustrySector());    
        equals = equals && valuesAreEqual(this.getSelfEmployedType(), ((SelfEmployed) otherEntity).getSelfEmployedType());    
        equals = equals && super.functionallyEquals(otherEntity);
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(self employed)}

    private static final List<SelfEmployedType> SELF_EMPLOYED_TYPES_WITH_NO_TRADE_TAX_REQUIRED = Arrays.asList(
            SelfEmployedType.FREELANCER,
            SelfEmployedType.NONINCORPERATED,
            SelfEmployedType.ENTERPRISE,
            SelfEmployedType.OTHER
    );


    /**
     * Implements logic for trade taxes described by ticket CC-3557
     *
     * @param selfEmployedType   {@see SelfEmployedType}
     * @param industrySectorType {@see IndustrySectorType}
     * @return boolean value for trade taxes true if required
     */
    public boolean retrieveIsTradeTaxRequired() {

        if (SelfEmployedType.TRADESMAN.equals(this.getSelfEmployedType())) {
            return !IndustrySectorType.AGRICULTURE_FORESTRY_FISHERY.equals(this.getIndustrySector());
        }

        return !SELF_EMPLOYED_TYPES_WITH_NO_TRADE_TAX_REQUIRED.contains(this.getSelfEmployedType());
    }

    // !!!!!!!! End of insert code section !!!!!!!!
}

