package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractEstate;




/**
 * The domain object that has all history aggregation related fields for 'Estates'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class EstateHistory extends AbstractEstate {

    protected transient de.smava.webapp.applicant.type.EstateRentType _partiallyRentedInitVal;
    protected transient boolean _partiallyRentedIsSet;
    protected transient Double _squaremetersUsedInitVal;
    protected transient boolean _squaremetersUsedIsSet;
    protected transient de.smava.webapp.applicant.type.EstateType _estateTypeInitVal;
    protected transient boolean _estateTypeIsSet;


	
    /**
     * Returns the initial value of the property 'partially rented'.
     */
    public de.smava.webapp.applicant.type.EstateRentType partiallyRentedInitVal() {
        de.smava.webapp.applicant.type.EstateRentType result;
        if (_partiallyRentedIsSet) {
            result = _partiallyRentedInitVal;
        } else {
            result = getPartiallyRented();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'partially rented'.
     */
    public boolean partiallyRentedIsDirty() {
        return !valuesAreEqual(partiallyRentedInitVal(), getPartiallyRented());
    }

    /**
     * Returns true if the setter method was called for the property 'partially rented'.
     */
    public boolean partiallyRentedIsSet() {
        return _partiallyRentedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'squaremeters used'.
     */
    public Double squaremetersUsedInitVal() {
        Double result;
        if (_squaremetersUsedIsSet) {
            result = _squaremetersUsedInitVal;
        } else {
            result = getSquaremetersUsed();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'squaremeters used'.
     */
    public boolean squaremetersUsedIsDirty() {
        return !valuesAreEqual(squaremetersUsedInitVal(), getSquaremetersUsed());
    }

    /**
     * Returns true if the setter method was called for the property 'squaremeters used'.
     */
    public boolean squaremetersUsedIsSet() {
        return _squaremetersUsedIsSet;
    }
	
    /**
     * Returns the initial value of the property 'estate type'.
     */
    public de.smava.webapp.applicant.type.EstateType estateTypeInitVal() {
        de.smava.webapp.applicant.type.EstateType result;
        if (_estateTypeIsSet) {
            result = _estateTypeInitVal;
        } else {
            result = getEstateType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'estate type'.
     */
    public boolean estateTypeIsDirty() {
        return !valuesAreEqual(estateTypeInitVal(), getEstateType());
    }

    /**
     * Returns true if the setter method was called for the property 'estate type'.
     */
    public boolean estateTypeIsSet() {
        return _estateTypeIsSet;
    }
	
}
