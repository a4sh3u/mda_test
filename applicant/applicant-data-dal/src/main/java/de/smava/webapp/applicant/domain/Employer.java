//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(employer)}
import de.smava.webapp.applicant.domain.history.EmployerHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'Employers'.
 *
 * 
 *
 * @author generator
 */
public class Employer extends EmployerHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(employer)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected String _name;
        protected Address _address;
        protected Phone _phone;
        
                            /**
     * Setter for the property 'name'.
     *
     * 
     *
     */
    public void setName(String name) {
        if (!_nameIsSet) {
            _nameIsSet = true;
            _nameInitVal = getName();
        }
        registerChange("name", _nameInitVal, name);
        _name = name;
    }
                        
    /**
     * Returns the property 'name'.
     *
     * 
     *
     */
    public String getName() {
        return _name;
    }
                                            
    /**
     * Setter for the property 'address'.
     *
     * 
     *
     */
    public void setAddress(Address address) {
        _address = address;
    }
            
    /**
     * Returns the property 'address'.
     *
     * 
     *
     */
    public Address getAddress() {
        return _address;
    }
                                            
    /**
     * Setter for the property 'phone'.
     *
     * 
     *
     */
    public void setPhone(Phone phone) {
        _phone = phone;
    }
            
    /**
     * Returns the property 'phone'.
     *
     * 
     *
     */
    public Phone getPhone() {
        return _phone;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_address instanceof de.smava.webapp.commons.domain.Entity && !_address.getChangeSet().isEmpty()) {
             for (String element : _address.getChangeSet()) {
                 result.add("address : " + element);
             }
         }

         if (_phone instanceof de.smava.webapp.commons.domain.Entity && !_phone.getChangeSet().isEmpty()) {
             for (String element : _phone.getChangeSet()) {
                 result.add("phone : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(Employer.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _name=").append(_name);
            builder.append("\n}");
        } else {
            builder.append(Employer.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public Employer asEmployer() {
        return this;
    }
}
