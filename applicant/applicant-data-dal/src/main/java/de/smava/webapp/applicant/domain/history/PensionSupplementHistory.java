package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractPensionSupplement;




/**
 * The domain object that has all history aggregation related fields for 'PensionSupplements'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class PensionSupplementHistory extends AbstractPensionSupplement {

    protected transient de.smava.webapp.applicant.type.PensionType _pensionSupplementTypeInitVal;
    protected transient boolean _pensionSupplementTypeIsSet;


	
    /**
     * Returns the initial value of the property 'pension supplement type'.
     */
    public de.smava.webapp.applicant.type.PensionType pensionSupplementTypeInitVal() {
        de.smava.webapp.applicant.type.PensionType result;
        if (_pensionSupplementTypeIsSet) {
            result = _pensionSupplementTypeInitVal;
        } else {
            result = getPensionSupplementType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'pension supplement type'.
     */
    public boolean pensionSupplementTypeIsDirty() {
        return !valuesAreEqual(pensionSupplementTypeInitVal(), getPensionSupplementType());
    }

    /**
     * Returns true if the setter method was called for the property 'pension supplement type'.
     */
    public boolean pensionSupplementTypeIsSet() {
        return _pensionSupplementTypeIsSet;
    }

}
