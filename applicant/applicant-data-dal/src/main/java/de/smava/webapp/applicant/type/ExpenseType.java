package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 24.02.15.
 */
public enum ExpenseType {

    /**
     * Haus
     */
    HOUSING_COST,

    /**
     * Investment
     */
    INVESTMENT,

    /**
     * Private Krankenversicherung (verpflichtend)
     */
    PRIVATE_HEALTH_INSURANCE,

    /**
     * Krankenzusatzversicherung Privat/Gesetzlich (freiwillig)
     */
    HEALTH_INSURANCE_ADD,

    /**
     * Private Pflegeversicherung
     */
    NURSING_INSURANCE,

    /**
     * Risiko Lebensversicherung
     */
    LIFE_RISK_INSURANCE,

    /**
     * Versicherungsbeiträge
     */
    INSURANCE_OTHER,

    MORTGAGE,

    /**
     * Kindesunterhalt
     */
    CHILD_ALIMONY,

    /**
     * Ehegattenunterhalt
     */
    SPOUSE_ALIMONY,

    PRIVATE_LEASING,
    BUSINESS_LEASING,

    /**
     * sonstige
     */
    OTHER
}
