//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(salaried staff)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;

import de.smava.webapp.applicant.domain.interfaces.SalariedStaffEntityInterface;
import de.smava.webapp.applicant.domain.interfaces.EmploymentEntityInterface;import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                        
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SalariedStaffs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractSalariedStaff
    extends Employment    implements SalariedStaffEntityInterface    ,EmploymentEntityInterface{

    protected static final Logger LOGGER = Logger.getLogger(AbstractSalariedStaff.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof SalariedStaff)) {
            return;
        }
        
        this.setEmployedSince(((SalariedStaff) oldEntity).getEmployedSince());    
        this.setIndustrySector(((SalariedStaff) oldEntity).getIndustrySector());    
        this.setEmployer(((SalariedStaff) oldEntity).getEmployer());    
        this.setStaffType(((SalariedStaff) oldEntity).getStaffType());    
        this.setProbationPeriod(((SalariedStaff) oldEntity).getProbationPeriod());    
        this.setEndOfProbationPeriod(((SalariedStaff) oldEntity).getEndOfProbationPeriod());    
        this.setTemporarilyEmployed(((SalariedStaff) oldEntity).getTemporarilyEmployed());    
        this.setEndOfTemporaryEmployment(((SalariedStaff) oldEntity).getEndOfTemporaryEmployment());    
        super.copyFromOldEntity(oldEntity);
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof SalariedStaff)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getEmployedSince(), ((SalariedStaff) otherEntity).getEmployedSince());    
        equals = equals && valuesAreEqual(this.getIndustrySector(), ((SalariedStaff) otherEntity).getIndustrySector());    
        equals = equals && valuesAreEqual(this.getEmployer(), ((SalariedStaff) otherEntity).getEmployer());    
        equals = equals && valuesAreEqual(this.getStaffType(), ((SalariedStaff) otherEntity).getStaffType());    
        equals = equals && valuesAreEqual(this.getProbationPeriod(), ((SalariedStaff) otherEntity).getProbationPeriod());    
        equals = equals && valuesAreEqual(this.getEndOfProbationPeriod(), ((SalariedStaff) otherEntity).getEndOfProbationPeriod());    
        equals = equals && valuesAreEqual(this.getTemporarilyEmployed(), ((SalariedStaff) otherEntity).getTemporarilyEmployed());    
        equals = equals && valuesAreEqual(this.getEndOfTemporaryEmployment(), ((SalariedStaff) otherEntity).getEndOfTemporaryEmployment());    
        equals = equals && super.functionallyEquals(otherEntity);
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(salaried staff)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

