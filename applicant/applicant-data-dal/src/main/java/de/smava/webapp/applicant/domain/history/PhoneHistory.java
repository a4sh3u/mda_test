package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractPhone;




/**
 * The domain object that has all history aggregation related fields for 'Phones'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class PhoneHistory extends AbstractPhone {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient de.smava.webapp.applicant.type.PhoneType _phoneTypeInitVal;
    protected transient boolean _phoneTypeIsSet;
    protected transient String _codeInitVal;
    protected transient boolean _codeIsSet;
    protected transient String _numberInitVal;
    protected transient boolean _numberIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'phone type'.
     */
    public de.smava.webapp.applicant.type.PhoneType phoneTypeInitVal() {
        de.smava.webapp.applicant.type.PhoneType result;
        if (_phoneTypeIsSet) {
            result = _phoneTypeInitVal;
        } else {
            result = getPhoneType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'phone type'.
     */
    public boolean phoneTypeIsDirty() {
        return !valuesAreEqual(phoneTypeInitVal(), getPhoneType());
    }

    /**
     * Returns true if the setter method was called for the property 'phone type'.
     */
    public boolean phoneTypeIsSet() {
        return _phoneTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'code'.
     */
    public String codeInitVal() {
        String result;
        if (_codeIsSet) {
            result = _codeInitVal;
        } else {
            result = getCode();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'code'.
     */
    public boolean codeIsDirty() {
        return !valuesAreEqual(codeInitVal(), getCode());
    }

    /**
     * Returns true if the setter method was called for the property 'code'.
     */
    public boolean codeIsSet() {
        return _codeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'number'.
     */
    public String numberInitVal() {
        String result;
        if (_numberIsSet) {
            result = _numberInitVal;
        } else {
            result = getNumber();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'number'.
     */
    public boolean numberIsDirty() {
        return !valuesAreEqual(numberInitVal(), getNumber());
    }

    /**
     * Returns true if the setter method was called for the property 'number'.
     */
    public boolean numberIsSet() {
        return _numberIsSet;
    }

}
