package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractIncome;




/**
 * The domain object that has all history aggregation related fields for 'Incomes'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class IncomeHistory extends AbstractIncome {

    protected transient java.util.Date _creationDateInitVal;
    protected transient boolean _creationDateIsSet;
    protected transient de.smava.webapp.applicant.type.IncomeType _incomeTypeInitVal;
    protected transient boolean _incomeTypeIsSet;
    protected transient String _descriptionInitVal;
    protected transient boolean _descriptionIsSet;
    protected transient Double _monthlyNetIncomeInitVal;
    protected transient boolean _monthlyNetIncomeIsSet;
    protected transient java.util.Date _expirationDateInitVal;
    protected transient boolean _expirationDateIsSet;
    protected transient Boolean _constantOverThreeMonthsInitVal;
    protected transient boolean _constantOverThreeMonthsIsSet;


	
    /**
     * Returns the initial value of the property 'creation date'.
     */
    public java.util.Date creationDateInitVal() {
        java.util.Date result;
        if (_creationDateIsSet) {
            result = _creationDateInitVal;
        } else {
            result = getCreationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'creation date'.
     */
    public boolean creationDateIsDirty() {
        return !valuesAreEqual(creationDateInitVal(), getCreationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'creation date'.
     */
    public boolean creationDateIsSet() {
        return _creationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'income type '.
     */
    public de.smava.webapp.applicant.type.IncomeType incomeTypeInitVal() {
        de.smava.webapp.applicant.type.IncomeType result;
        if (_incomeTypeIsSet) {
            result = _incomeTypeInitVal;
        } else {
            result = getIncomeType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'income type '.
     */
    public boolean incomeTypeIsDirty() {
        return !valuesAreEqual(incomeTypeInitVal(), getIncomeType());
    }

    /**
     * Returns true if the setter method was called for the property 'income type '.
     */
    public boolean incomeTypeIsSet() {
        return _incomeTypeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'description'.
     */
    public String descriptionInitVal() {
        String result;
        if (_descriptionIsSet) {
            result = _descriptionInitVal;
        } else {
            result = getDescription();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'description'.
     */
    public boolean descriptionIsDirty() {
        return !valuesAreEqual(descriptionInitVal(), getDescription());
    }

    /**
     * Returns true if the setter method was called for the property 'description'.
     */
    public boolean descriptionIsSet() {
        return _descriptionIsSet;
    }
	
    /**
     * Returns the initial value of the property 'monthly net income'.
     */
    public Double monthlyNetIncomeInitVal() {
        Double result;
        if (_monthlyNetIncomeIsSet) {
            result = _monthlyNetIncomeInitVal;
        } else {
            result = getMonthlyNetIncome();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'monthly net income'.
     */
    public boolean monthlyNetIncomeIsDirty() {
        return !valuesAreEqual(monthlyNetIncomeInitVal(), getMonthlyNetIncome());
    }

    /**
     * Returns true if the setter method was called for the property 'monthly net income'.
     */
    public boolean monthlyNetIncomeIsSet() {
        return _monthlyNetIncomeIsSet;
    }
	
    /**
     * Returns the initial value of the property 'expiration date'.
     */
    public java.util.Date expirationDateInitVal() {
        java.util.Date result;
        if (_expirationDateIsSet) {
            result = _expirationDateInitVal;
        } else {
            result = getExpirationDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'expiration date'.
     */
    public boolean expirationDateIsDirty() {
        return !valuesAreEqual(expirationDateInitVal(), getExpirationDate());
    }

    /**
     * Returns true if the setter method was called for the property 'expiration date'.
     */
    public boolean expirationDateIsSet() {
        return _expirationDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'constant over three months'.
     */
    public Boolean constantOverThreeMonthsInitVal() {
        Boolean result;
        if (_constantOverThreeMonthsIsSet) {
            result = _constantOverThreeMonthsInitVal;
        } else {
            result = getConstantOverThreeMonths();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'constant over three months'.
     */
    public boolean constantOverThreeMonthsIsDirty() {
        return !valuesAreEqual(constantOverThreeMonthsInitVal(), getConstantOverThreeMonths());
    }

    /**
     * Returns true if the setter method was called for the property 'constant over three months'.
     */
    public boolean constantOverThreeMonthsIsSet() {
        return _constantOverThreeMonthsIsSet;
    }

}
