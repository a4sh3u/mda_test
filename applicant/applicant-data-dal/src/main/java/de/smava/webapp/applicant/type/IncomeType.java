package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 24.02.15.
 */
public enum IncomeType {

    /**
     * any kind of work
     */
    EMPLOYMENT,

    PENSION_SUPPLEMENT,

    /**
     * Wohngeld
     */
    ACCOMODATION_ALLOWANCE,


    /**
     * Kindergeld
     */
    CHILD_BENEFIT,

    /**
     * Kindergeldzuschuss
     */
    CHILD_BENEFIT_GRANT,

    /**
     * Elterngeld
     */
    PARENTAL_ALLOWANCE,

    /**
     * Kindesunterhalt
     */
    CHILD_ALIMONY,

    /**
     * Ehegattenunterhalt
     */
    SPOUSE_ALIMONY,

    /**
     * Provisionszahlungen
     */
    PROVISION_INCOME,

    /**
     * Provisions-Nebeneinkunft
     */
    PROVISION_AUXILIARY,

    /**
     * Überstunden+Schichtzulagen
     */
    OVERTIME_SHIFT_BONUS,

    /**
     * Tantieme
     */
    PROFIT_SHARE,

    /**
     * Spesen
     */
    ALLOWANCE,

    /**
     * Kapitalerträge/Zinserträge
     */
    CAPITAL_INCOME,

    /**
     * vermögenswirksame Leistungen
     */
    CAPITAL_FORMING,

    /**
     * Existenzgründerdarlehen
     */
    STARTUP_LOAN,

    /**
     * Konkursausfallgeld
     */
    INSOLVENCY_MONEY,

    /**
     * Mieteinnahmen
     */
    RENTAL_INCOME,

    /**
     * Mietbeteiligung Lebenspartner
     */
    RENTAL_CONTRIBUTION,

    /**
     * Photovoltaik-Einnahmen
     */
    PHOTOVOLTAIC_INCOME,

    /**
     * Krankengeld
     */
    SICK_BENEFIT,

    /**
     * Pflegegeld
     */
    ATTENDANCE_ALLOWANCE,

    /**
     * Blindengeld
     */
    BLIND_BENEFIT,

    /**
     * Student mit Bafög
     */
    EDUCATION_SUPPORT,

    /**
     * Stipendium
     */
    SCHOLARSHIP,

    /**
     * Aus- und Weiterbildung Zuschuss
     */
    TRAINING_GRANT,

    /**
     * Monatliche Einnahmen aus ALG II (Aufstocker)
     */
    LOW_WAGE_GRANT,

    /**
     * Sonstiges
     */
    OTHER;

}
