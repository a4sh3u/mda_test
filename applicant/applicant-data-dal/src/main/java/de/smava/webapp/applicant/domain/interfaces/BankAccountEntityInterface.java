package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.BankAccount;

import java.util.Date;


/**
 * The domain object that represents 'BankAccounts'.
 *
 * @author generator
 */
public interface BankAccountEntityInterface {

    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    void setCreationDate(Date creationDate);

    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    Date getCreationDate();
    /**
     * Setter for the property 'bank name'.
     *
     * 
     *
     */
    void setBankName(String bankName);

    /**
     * Returns the property 'bank name'.
     *
     * 
     *
     */
    String getBankName();
    /**
     * Setter for the property 'iban'.
     *
     * 
     *
     */
    void setIban(String iban);

    /**
     * Returns the property 'iban'.
     *
     * 
     *
     */
    String getIban();
    /**
     * Setter for the property 'bic'.
     *
     * 
     *
     */
    void setBic(String bic);

    /**
     * Returns the property 'bic'.
     *
     * 
     *
     */
    String getBic();
    /**
     * Setter for the property 'account number'.
     *
     * 
     *
     */
    void setAccountNumber(String accountNumber);

    /**
     * Returns the property 'account number'.
     *
     * 
     *
     */
    String getAccountNumber();
    /**
     * Setter for the property 'bank code'.
     *
     * 
     *
     */
    void setBankCode(String bankCode);

    /**
     * Returns the property 'bank code'.
     *
     * 
     *
     */
    String getBankCode();
    /**
     * Helper method to get reference of this object as model type.
     */
    BankAccount asBankAccount();
}
