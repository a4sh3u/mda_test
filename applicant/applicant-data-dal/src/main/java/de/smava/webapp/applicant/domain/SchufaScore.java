//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(schufa score)}
import de.smava.webapp.applicant.domain.history.SchufaScoreHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'SchufaScores'.
 *
 * 
 *
 * @author generator
 */
public class SchufaScore extends SchufaScoreHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(schufa score)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Applicant _applicant;
        protected java.util.Date _creationDate;
        protected int _score;
        protected String _rating;
        protected String _type;
        protected String _remark;
        protected String _state;
        protected SchufaScoreRequestData _requestData;
        protected SchufaRequest _request;
        protected String _version;
        protected Set<SchufaScoreToken> _tokens;
        
                                    
    /**
     * Setter for the property 'applicant'.
     *
     * 
     *
     */
    public void setApplicant(Applicant applicant) {
        _applicant = applicant;
    }
            
    /**
     * Returns the property 'applicant'.
     *
     * 
     *
     */
    public Applicant getApplicant() {
        return _applicant;
    }
                                    /**
     * Setter for the property 'creation date'.
     *
     * 
     *
     */
    public void setCreationDate(java.util.Date creationDate) {
        if (!_creationDateIsSet) {
            _creationDateIsSet = true;
            _creationDateInitVal = getCreationDate();
        }
        registerChange("creation date", _creationDateInitVal, creationDate);
        _creationDate = creationDate;
    }
                        
    /**
     * Returns the property 'creation date'.
     *
     * 
     *
     */
    public java.util.Date getCreationDate() {
        return _creationDate;
    }
                                    /**
     * Setter for the property 'score'.
     *
     * 
     *
     */
    public void setScore(int score) {
        if (!_scoreIsSet) {
            _scoreIsSet = true;
            _scoreInitVal = getScore();
        }
        registerChange("score", _scoreInitVal, score);
        _score = score;
    }
                        
    /**
     * Returns the property 'score'.
     *
     * 
     *
     */
    public int getScore() {
        return _score;
    }
                                    /**
     * Setter for the property 'rating'.
     *
     * 
     *
     */
    public void setRating(String rating) {
        if (!_ratingIsSet) {
            _ratingIsSet = true;
            _ratingInitVal = getRating();
        }
        registerChange("rating", _ratingInitVal, rating);
        _rating = rating;
    }
                        
    /**
     * Returns the property 'rating'.
     *
     * 
     *
     */
    public String getRating() {
        return _rating;
    }
                                    /**
     * Setter for the property 'type'.
     *
     * 
     *
     */
    public void setType(String type) {
        if (!_typeIsSet) {
            _typeIsSet = true;
            _typeInitVal = getType();
        }
        registerChange("type", _typeInitVal, type);
        _type = type;
    }
                        
    /**
     * Returns the property 'type'.
     *
     * 
     *
     */
    public String getType() {
        return _type;
    }
                                    /**
     * Setter for the property 'remark'.
     *
     * 
     *
     */
    public void setRemark(String remark) {
        if (!_remarkIsSet) {
            _remarkIsSet = true;
            _remarkInitVal = getRemark();
        }
        registerChange("remark", _remarkInitVal, remark);
        _remark = remark;
    }
                        
    /**
     * Returns the property 'remark'.
     *
     * 
     *
     */
    public String getRemark() {
        return _remark;
    }
                                    /**
     * Setter for the property 'state'.
     *
     * 
     *
     */
    public void setState(String state) {
        if (!_stateIsSet) {
            _stateIsSet = true;
            _stateInitVal = getState();
        }
        registerChange("state", _stateInitVal, state);
        _state = state;
    }
                        
    /**
     * Returns the property 'state'.
     *
     * 
     *
     */
    public String getState() {
        return _state;
    }
                                            
    /**
     * Setter for the property 'request data'.
     *
     * 
     *
     */
    public void setRequestData(SchufaScoreRequestData requestData) {
        _requestData = requestData;
    }
            
    /**
     * Returns the property 'request data'.
     *
     * 
     *
     */
    public SchufaScoreRequestData getRequestData() {
        return _requestData;
    }
                                            
    /**
     * Setter for the property 'request'.
     *
     * 
     *
     */
    public void setRequest(SchufaRequest request) {
        _request = request;
    }
            
    /**
     * Returns the property 'request'.
     *
     * 
     *
     */
    public SchufaRequest getRequest() {
        return _request;
    }
                                    /**
     * Setter for the property 'version'.
     *
     * 
     *
     */
    public void setVersion(String version) {
        if (!_versionIsSet) {
            _versionIsSet = true;
            _versionInitVal = getVersion();
        }
        registerChange("version", _versionInitVal, version);
        _version = version;
    }
                        
    /**
     * Returns the property 'version'.
     *
     * 
     *
     */
    public String getVersion() {
        return _version;
    }
                                            
    /**
     * Setter for the property 'tokens'.
     *
     * 
     *
     */
    public void setTokens(Set<SchufaScoreToken> tokens) {
        _tokens = tokens;
    }
            
    /**
     * Returns the property 'tokens'.
     *
     * 
     *
     */
    public Set<SchufaScoreToken> getTokens() {
        return _tokens;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_applicant instanceof de.smava.webapp.commons.domain.Entity && !_applicant.getChangeSet().isEmpty()) {
             for (String element : _applicant.getChangeSet()) {
                 result.add("applicant : " + element);
             }
         }

         if (_requestData instanceof de.smava.webapp.commons.domain.Entity && !_requestData.getChangeSet().isEmpty()) {
             for (String element : _requestData.getChangeSet()) {
                 result.add("request data : " + element);
             }
         }

         if (_request instanceof de.smava.webapp.commons.domain.Entity && !_request.getChangeSet().isEmpty()) {
             for (String element : _request.getChangeSet()) {
                 result.add("request : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(SchufaScore.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _creationDate=").append(_creationDate);
            builder.append("\n    _score=").append(_score);
            builder.append("\n    _rating=").append(_rating);
            builder.append("\n    _type=").append(_type);
            builder.append("\n    _remark=").append(_remark);
            builder.append("\n    _state=").append(_state);
            builder.append("\n    _version=").append(_version);
            builder.append("\n}");
        } else {
            builder.append(SchufaScore.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public SchufaScore asSchufaScore() {
        return this;
    }
}
