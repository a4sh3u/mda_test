package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Address;
import de.smava.webapp.applicant.domain.LivingAddress;

import java.util.Date;


/**
 * The domain object that represents 'LivingAddresss'.
 *
 * @author generator
 */
public interface LivingAddressEntityInterface {

    /**
     * Setter for the property 'move in date'.
     *
     * 
     *
     */
    void setMoveInDate(Date moveInDate);

    /**
     * Returns the property 'move in date'.
     *
     * 
     *
     */
    Date getMoveInDate();
    /**
     * Setter for the property 'address'.
     *
     * Extended address
     *
     */
    void setAddress(Address address);

    /**
     * Returns the property 'address'.
     *
     * Extended address
     *
     */
    Address getAddress();
    /**
     * Helper method to get reference of this object as model type.
     */
    LivingAddress asLivingAddress();
}
