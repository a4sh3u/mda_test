package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractThirdPartyLoanProcessingLog;




/**
 * The domain object that has all history aggregation related fields for 'ThirdPartyLoanProcessingLogs'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class ThirdPartyLoanProcessingLogHistory extends AbstractThirdPartyLoanProcessingLog {

    protected transient String _processingResultInitVal;
    protected transient boolean _processingResultIsSet;


	
    /**
     * Returns the initial value of the property 'processing result'.
     */
    public String processingResultInitVal() {
        String result;
        if (_processingResultIsSet) {
            result = _processingResultInitVal;
        } else {
            result = getProcessingResult();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'processing result'.
     */
    public boolean processingResultIsDirty() {
        return !valuesAreEqual(processingResultInitVal(), getProcessingResult());
    }

    /**
     * Returns true if the setter method was called for the property 'processing result'.
     */
    public boolean processingResultIsSet() {
        return _processingResultIsSet;
    }
			
}
