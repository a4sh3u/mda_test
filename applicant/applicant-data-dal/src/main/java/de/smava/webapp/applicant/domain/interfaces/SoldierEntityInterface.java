package de.smava.webapp.applicant.domain.interfaces;



import de.smava.webapp.applicant.domain.Soldier;


/**
 * The domain object that represents 'Soldiers'.
 *
 * @author generator
 */
public interface SoldierEntityInterface {

    /**
     * Setter for the property 'soldier type'.
     *
     * 
     *
     */
    void setSoldierType(de.smava.webapp.applicant.type.SoldierType soldierType);

    /**
     * Returns the property 'soldier type'.
     *
     * 
     *
     */
    de.smava.webapp.applicant.type.SoldierType getSoldierType();
    /**
     * Helper method to get reference of this object as model type.
     */
    Soldier asSoldier();
}
