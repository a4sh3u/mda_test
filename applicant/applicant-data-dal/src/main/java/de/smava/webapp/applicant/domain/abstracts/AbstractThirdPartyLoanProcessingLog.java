//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainAbstractEntity.tpl
//
//
//
//
package de.smava.webapp.applicant.domain.abstracts;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(third party loan processing log)}
import de.smava.webapp.applicant.domain.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import de.smava.webapp.commons.domain.Entity;
import de.smava.webapp.commons.currentdate.CurrentDate;


import de.smava.webapp.applicant.domain.interfaces.ThirdPartyLoanProcessingLogEntityInterface;
import de.smava.webapp.commons.domain.Entity;
import org.apache.log4j.Logger;

                
// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'ThirdPartyLoanProcessingLogs'.
 * This class is actually intended to contain all "custom logic of entities that
 * relies on existing entity fields
 *
 * 
 *
 * @author generator
 */
public abstract class AbstractThirdPartyLoanProcessingLog
    extends BrokerageEntity    implements ThirdPartyLoanProcessingLogEntityInterface    {

    protected static final Logger LOGGER = Logger.getLogger(AbstractThirdPartyLoanProcessingLog.class);

    @Override
    public void copyFromOldEntity(Entity oldEntity) {

        if (!(oldEntity instanceof ThirdPartyLoanProcessingLog)) {
            return;
        }
        
        this.setProcessingResult(((ThirdPartyLoanProcessingLog) oldEntity).getProcessingResult());    
        this.setExternalThirdPartyLoan(((ThirdPartyLoanProcessingLog) oldEntity).getExternalThirdPartyLoan());    
        this.setCustomerThirdPartyLoan(((ThirdPartyLoanProcessingLog) oldEntity).getCustomerThirdPartyLoan());    
        this.setThirdPartyLoan(((ThirdPartyLoanProcessingLog) oldEntity).getThirdPartyLoan());    
        
    }

    @Override
    public boolean functionallyEquals(Entity otherEntity) {
        boolean equals = true;

        if (otherEntity == null) {
            equals = false;
        }

        if (!(otherEntity instanceof ThirdPartyLoanProcessingLog)) {
            equals = false;
        }
        
        equals = equals && valuesAreEqual(this.getProcessingResult(), ((ThirdPartyLoanProcessingLog) otherEntity).getProcessingResult());    
        equals = equals && valuesAreEqual(this.getExternalThirdPartyLoan(), ((ThirdPartyLoanProcessingLog) otherEntity).getExternalThirdPartyLoan());    
        equals = equals && valuesAreEqual(this.getCustomerThirdPartyLoan(), ((ThirdPartyLoanProcessingLog) otherEntity).getCustomerThirdPartyLoan());    
        equals = equals && valuesAreEqual(this.getThirdPartyLoan(), ((ThirdPartyLoanProcessingLog) otherEntity).getThirdPartyLoan());    
        
        return equals;
    }

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|custom implementations|bean(third party loan processing log)}
    // !!!!!!!! End of insert code section !!!!!!!!
}

