//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(other housing cost)}
import de.smava.webapp.applicant.domain.history.OtherHousingCostHistory;
import de.smava.webapp.applicant.type.OtherHousingCostType;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'OtherHousingCosts'.
 *
 * Anything else not matching Estate or Rent housing type.
 *
 * @author generator
 */
public class OtherHousingCost extends OtherHousingCostHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(other housing cost)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.type.OtherHousingCostType _otherHousingCostType;
        
                            /**
     * Setter for the property 'other housing cost type'.
     *
     * Type of this housing situation. See enum for details.
     *
     */
    public void setOtherHousingCostType(de.smava.webapp.applicant.type.OtherHousingCostType otherHousingCostType) {
        if (!_otherHousingCostTypeIsSet) {
            _otherHousingCostTypeIsSet = true;
            _otherHousingCostTypeInitVal = getOtherHousingCostType();
        }
        registerChange("other housing cost type", _otherHousingCostTypeInitVal, otherHousingCostType);
        _otherHousingCostType = otherHousingCostType;
    }
                        
    /**
     * Returns the property 'other housing cost type'.
     *
     * Type of this housing situation. See enum for details.
     *
     */
    public de.smava.webapp.applicant.type.OtherHousingCostType getOtherHousingCostType() {
        return _otherHousingCostType;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(OtherHousingCost.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _otherHousingCostType=").append(_otherHousingCostType);
            builder.append("\n}");
        } else {
            builder.append(OtherHousingCost.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public OtherHousingCost asOtherHousingCost() {
        return this;
    }
}
