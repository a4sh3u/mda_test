//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(estate financing)}
import de.smava.webapp.applicant.domain.history.EstateFinancingHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'EstateFinancings'.
 *
 * TODO
 *
 * @author generator
 */
public class EstateFinancing extends EstateFinancingHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(estate financing)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected Double _monthlyFinancingAmount;
        protected Set<Investment> _repaymentReplacements;
        
                            /**
     * Setter for the property 'monthly financing amount'.
     *
     * 
     *
     */
    public void setMonthlyFinancingAmount(Double monthlyFinancingAmount) {
        if (!_monthlyFinancingAmountIsSet) {
            _monthlyFinancingAmountIsSet = true;
            _monthlyFinancingAmountInitVal = getMonthlyFinancingAmount();
        }
        registerChange("monthly financing amount", _monthlyFinancingAmountInitVal, monthlyFinancingAmount);
        _monthlyFinancingAmount = monthlyFinancingAmount;
    }
                        
    /**
     * Returns the property 'monthly financing amount'.
     *
     * 
     *
     */
    public Double getMonthlyFinancingAmount() {
        return _monthlyFinancingAmount;
    }
                                            
    /**
     * Setter for the property 'repayment replacements'.
     *
     * 
     *
     */
    public void setRepaymentReplacements(Set<Investment> repaymentReplacements) {
        _repaymentReplacements = repaymentReplacements;
    }
            
    /**
     * Returns the property 'repayment replacements'.
     *
     * 
     *
     */
    public Set<Investment> getRepaymentReplacements() {
        return _repaymentReplacements;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(EstateFinancing.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(EstateFinancing.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public EstateFinancing asEstateFinancing() {
        return this;
    }
}
