package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractPersonalData;



/**
 * The domain object that has all history aggregation related fields for 'PersonalDatas'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class PersonalDataHistory extends AbstractPersonalData {

    protected transient de.smava.webapp.applicant.type.Gender _genderInitVal;
    protected transient boolean _genderIsSet;
    protected transient String _firstNameInitVal;
    protected transient boolean _firstNameIsSet;
    protected transient String _lastNameInitVal;
    protected transient boolean _lastNameIsSet;
    protected transient String _birthNameInitVal;
    protected transient boolean _birthNameIsSet;
    protected transient java.util.Date _birthDateInitVal;
    protected transient boolean _birthDateIsSet;
    protected transient de.smava.webapp.applicant.type.MaritalState _maritalStateInitVal;
    protected transient boolean _maritalStateIsSet;


	
    /**
     * Returns the initial value of the property 'gender'.
     */
    public de.smava.webapp.applicant.type.Gender genderInitVal() {
        de.smava.webapp.applicant.type.Gender result;
        if (_genderIsSet) {
            result = _genderInitVal;
        } else {
            result = getGender();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'gender'.
     */
    public boolean genderIsDirty() {
        return !valuesAreEqual(genderInitVal(), getGender());
    }

    /**
     * Returns true if the setter method was called for the property 'gender'.
     */
    public boolean genderIsSet() {
        return _genderIsSet;
    }
	
    /**
     * Returns the initial value of the property 'first name'.
     */
    public String firstNameInitVal() {
        String result;
        if (_firstNameIsSet) {
            result = _firstNameInitVal;
        } else {
            result = getFirstName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'first name'.
     */
    public boolean firstNameIsDirty() {
        return !valuesAreEqual(firstNameInitVal(), getFirstName());
    }

    /**
     * Returns true if the setter method was called for the property 'first name'.
     */
    public boolean firstNameIsSet() {
        return _firstNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'last name'.
     */
    public String lastNameInitVal() {
        String result;
        if (_lastNameIsSet) {
            result = _lastNameInitVal;
        } else {
            result = getLastName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'last name'.
     */
    public boolean lastNameIsDirty() {
        return !valuesAreEqual(lastNameInitVal(), getLastName());
    }

    /**
     * Returns true if the setter method was called for the property 'last name'.
     */
    public boolean lastNameIsSet() {
        return _lastNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'birth name'.
     */
    public String birthNameInitVal() {
        String result;
        if (_birthNameIsSet) {
            result = _birthNameInitVal;
        } else {
            result = getBirthName();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birth name'.
     */
    public boolean birthNameIsDirty() {
        return !valuesAreEqual(birthNameInitVal(), getBirthName());
    }

    /**
     * Returns true if the setter method was called for the property 'birth name'.
     */
    public boolean birthNameIsSet() {
        return _birthNameIsSet;
    }
	
    /**
     * Returns the initial value of the property 'birth date'.
     */
    public java.util.Date birthDateInitVal() {
        java.util.Date result;
        if (_birthDateIsSet) {
            result = _birthDateInitVal;
        } else {
            result = getBirthDate();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'birth date'.
     */
    public boolean birthDateIsDirty() {
        return !valuesAreEqual(birthDateInitVal(), getBirthDate());
    }

    /**
     * Returns true if the setter method was called for the property 'birth date'.
     */
    public boolean birthDateIsSet() {
        return _birthDateIsSet;
    }
	
    /**
     * Returns the initial value of the property 'marital state'.
     */
    public de.smava.webapp.applicant.type.MaritalState maritalStateInitVal() {
        de.smava.webapp.applicant.type.MaritalState result;
        if (_maritalStateIsSet) {
            result = _maritalStateInitVal;
        } else {
            result = getMaritalState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'marital state'.
     */
    public boolean maritalStateIsDirty() {
        return !valuesAreEqual(maritalStateInitVal(), getMaritalState());
    }

    /**
     * Returns true if the setter method was called for the property 'marital state'.
     */
    public boolean maritalStateIsSet() {
        return _maritalStateIsSet;
    }

}
