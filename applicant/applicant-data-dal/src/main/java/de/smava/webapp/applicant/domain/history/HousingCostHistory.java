package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractHousingCost;




/**
 * The domain object that has all history aggregation related fields for 'HousingCosts'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class HousingCostHistory extends AbstractHousingCost {




}
