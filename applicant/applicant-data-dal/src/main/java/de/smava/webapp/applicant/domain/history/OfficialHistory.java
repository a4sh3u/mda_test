package de.smava.webapp.applicant.domain.history;



import de.smava.webapp.applicant.domain.abstracts.AbstractOfficial;




/**
 * The domain object that has all history aggregation related fields for 'Officials'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class OfficialHistory extends AbstractOfficial {

    protected transient de.smava.webapp.applicant.type.OfficialServiceGradeType _officialServiceGradeTypeInitVal;
    protected transient boolean _officialServiceGradeTypeIsSet;


	
    /**
     * Returns the initial value of the property 'official service grade type'.
     */
    public de.smava.webapp.applicant.type.OfficialServiceGradeType officialServiceGradeTypeInitVal() {
        de.smava.webapp.applicant.type.OfficialServiceGradeType result;
        if (_officialServiceGradeTypeIsSet) {
            result = _officialServiceGradeTypeInitVal;
        } else {
            result = getOfficialServiceGradeType();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'official service grade type'.
     */
    public boolean officialServiceGradeTypeIsDirty() {
        return !valuesAreEqual(officialServiceGradeTypeInitVal(), getOfficialServiceGradeType());
    }

    /**
     * Returns true if the setter method was called for the property 'official service grade type'.
     */
    public boolean officialServiceGradeTypeIsSet() {
        return _officialServiceGradeTypeIsSet;
    }

}
