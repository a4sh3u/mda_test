package de.smava.webapp.applicant.type;

/**
 * Created by dkeller on 15.06.15.
 */
public enum OfficialServiceGradeType {

    /**
     * Einfacher Dienst
     */
    PRIVATE,

    /**
     * Mittlerer Dienst
     */
    MIDDLE,

    /**
     * Gehobener Dienst
     */
    UPPER_MIDDLE,

    /**
     * Höherer Dienst
     */
    UPPER
}
