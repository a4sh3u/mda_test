package de.smava.webapp.applicant.casi.domain;

import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.applicant.casi.domain.interfaces.AdvisorStatisticsInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;

import java.util.Date;

public class AdvisorStatistics extends BrokerageEntity implements AdvisorStatisticsInterface {

    private Advisor advisor;

    private Date creationDate;

    private Integer advisedLeadCount;

    private Date lastUpdate;

    public AdvisorStatistics() {}

    public Advisor getAdvisor() {
        return advisor;
    }

    public void setAdvisor(Advisor advisorId) {
        this.advisor = advisorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getAdvisedLeadCount() {
        return advisedLeadCount;
    }

    public void setAdvisedLeadCount(Integer advisedLeadCount) {
        this.advisedLeadCount = advisedLeadCount;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
