package de.smava.webapp.applicant.casi.domain.interfaces;

import de.smava.webapp.account.domain.Advisor;

import java.util.Date;

public interface AdvisorStatisticsInterface {

    Advisor getAdvisor();

    void setAdvisor(Advisor advisor);

    Date getCreationDate();

    void setCreationDate(Date creationDate);

    Integer getAdvisedLeadCount();

    void setAdvisedLeadCount(Integer advisedLeadCount);

    Date getLastUpdate();

    void setLastUpdate(Date lastUpdate);
}
