package de.smava.webapp.applicant.casi.dao;


import de.smava.webapp.applicant.casi.domain.AdvisorStatistics;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import org.joda.time.LocalDate;

import java.util.Date;

public interface AdvisorStatisticsDao extends BrokerageSchemaDao<AdvisorStatistics> {

    AdvisorStatistics loadStatisticBy(Long advisorId, Date date);

    Integer calculateMonthlyPickedLeads(Long advisorId, Date date);

    boolean isSwitchedFromOpenStateAfterDate(Long accountSmavaId, LocalDate afterDate, Long exceptCurrentStatusId);
}
