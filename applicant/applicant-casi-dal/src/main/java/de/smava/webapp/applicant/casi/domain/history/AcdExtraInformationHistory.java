package de.smava.webapp.applicant.casi.domain.history;

import de.smava.webapp.applicant.casi.domain.abstracts.AbstractAcdExtraInformation;

/**
 * The domain object that has all history aggregation related fields for 'AcdExtraInformations'.
 * please also see AOP aspect that is actually utilizing this entities.
 *
 * @author generator
 */
public abstract class AcdExtraInformationHistory extends AbstractAcdExtraInformation {

    protected transient Double _ingDibaHhcInitVal;
    protected transient boolean _ingDibaHhcIsSet;
    protected transient Integer _pageInitVal;
    protected transient boolean _pageIsSet;
    protected transient Double _smavaSchufaScoreInitVal;
    protected transient boolean _smavaSchufaScoreIsSet;
    protected transient Boolean _smavaSchufaStateInitVal;
    protected transient boolean _smavaSchufaStateIsSet;


    /**
     * Returns the initial value of the property 'ingDibaHhc'.
     */
    public Double ingDibaHhcInitVal() {
        Double result;
        if (_ingDibaHhcIsSet) {
            result = _ingDibaHhcInitVal;
        } else {
            result = getIngDibaHhc();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'ingDibaHhc'.
     */
    public boolean ingDibaHhcIsDirty() {
        return !valuesAreEqual(ingDibaHhcInitVal(), getIngDibaHhc());
    }

    /**
     * Returns true if the setter method was called for the property 'ingDibaHhc'.
     */
    public boolean ingDibaHhcIsSet() {
        return _ingDibaHhcIsSet;
    }
	
    /**
     * Returns the initial value of the property 'page'.
     */
    public Integer pageInitVal() {
        Integer result;
        if (_pageIsSet) {
            result = _pageInitVal;
        } else {
            result = getPage();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'page'.
     */
    public boolean pageIsDirty() {
        return !valuesAreEqual(pageInitVal(), getPage());
    }

    /**
     * Returns true if the setter method was called for the property 'page'.
     */
    public boolean pageIsSet() {
        return _pageIsSet;
    }
	
    /**
     * Returns the initial value of the property 'smavaSchufaScore'.
     */
    public Double smavaSchufaScoreInitVal() {
        Double result;
        if (_smavaSchufaScoreIsSet) {
            result = _smavaSchufaScoreInitVal;
        } else {
            result = getSmavaSchufaScore();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'smavaSchufaScore'.
     */
    public boolean smavaSchufaScoreIsDirty() {
        return !valuesAreEqual(smavaSchufaScoreInitVal(), getSmavaSchufaScore());
    }

    /**
     * Returns true if the setter method was called for the property 'smavaSchufaScore'.
     */
    public boolean smavaSchufaScoreIsSet() {
        return _smavaSchufaScoreIsSet;
    }
	
    /**
     * Returns the initial value of the property 'smavaSchufaState'.
     */
    public Boolean smavaSchufaStateInitVal() {
        Boolean result;
        if (_smavaSchufaStateIsSet) {
            result = _smavaSchufaStateInitVal;
        } else {
            result = getSmavaSchufaState();
        }
        return result;
    }

    /**
     * Compares the initial value with the current value of the property 'smavaSchufaState'.
     */
    public boolean smavaSchufaStateIsDirty() {
        return !valuesAreEqual(smavaSchufaStateInitVal(), getSmavaSchufaState());
    }

    /**
     * Returns true if the setter method was called for the property 'smavaSchufaState'.
     */
    public boolean smavaSchufaStateIsSet() {
        return _smavaSchufaStateIsSet;
    }

}
