package de.smava.webapp.applicant.casi.dao;

import de.smava.webapp.applicant.casi.domain.SmsTemplate;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;

import java.util.Collection;

public interface SmsTemplateDao extends BrokerageSchemaDao<SmsTemplate> {

    Collection<SmsTemplate> getAllEditableSmsTemplates();

    Collection<SmsTemplate> getAllSmsTemplates();

    SmsTemplate getSmsTemplateByKey(String key);
}
