package de.smava.webapp.applicant.casi.dao.jdo;

import de.smava.webapp.account.dao.AdvisorDao;
import de.smava.webapp.account.domain.Advisor;
import de.smava.webapp.applicant.casi.dao.AdvisorStatisticsDao;
import de.smava.webapp.applicant.casi.domain.AdvisorStatistics;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Repository(value = "advisorStatisticsDao")
public class JdoAdvisorStatisticsDao extends JdoBaseDao implements AdvisorStatisticsDao {
    private static final Logger log = Logger.getLogger(JdoAdvisorStatisticsDao.class);

    @Autowired
    private AdvisorDao advisorDao;

    @Override
    public AdvisorStatistics load(Long id) {
        return getEntity(AdvisorStatistics.class, id);
    }

    @Override
    public Long save(AdvisorStatistics entity) {
        return saveEntity(entity);
    }

    @Override
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            AdvisorStatistics entity = findUniqueEntity(AdvisorStatistics.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }

    @Override
    public AdvisorStatistics loadStatisticBy(Long advisorId, Date date) {
        Advisor advisor = advisorDao.load(advisorId);
        if (advisor == null) {
            log.error("Cant find advisor :"+advisorId);
            throw new IllegalArgumentException("Cant find advisor "+advisorId);
        }
        Query select = getPersistenceManager().newNamedQuery(AdvisorStatistics.class, "findAdvisorStatistics");
        select.setUnique(true);
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("advisorId", advisorId);
        parameters.put("creationDate", new Timestamp(date.getTime()));

        AdvisorStatistics uniqueEntity = (AdvisorStatistics) select.executeWithMap(parameters);
        if (uniqueEntity != null) {
            return uniqueEntity;
        } else {
            log.info("Create new statistic for advisor :"+advisorId);
            return create(advisor, date);
        }
    }

    @Override
    public Integer calculateMonthlyPickedLeads(Long advisorId, Date date) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("advisorId", advisorId);
        LocalDate currentDate = new LocalDate(date);
        LocalDate startOfMonth = currentDate.minusDays(currentDate.getDayOfMonth() - 1);
        params.put("startOfMonth", new Timestamp(startOfMonth.toDateTimeAtStartOfDay().getMillis()));
        params.put("today", new Timestamp(currentDate.toDateTimeAtStartOfDay().getMillis()));
        String select = "SELECT sum(statistic.advised_lead_count) FROM brokerage.advisor_statistics statistic" +
                        " WHERE statistic.advisor_id = :advisorId" +
                        "   AND statistic.creation_date >= :startOfMonth " +
                        "   AND statistic.creation_date <= :today";
        Query query = getPersistenceManager().newQuery(Query.SQL, select);
        query.setResultClass(Integer.class);
        query.setUnique(true);
        return (Integer) query.executeWithMap(params);
    }

    public boolean isSwitchedFromOpenStateAfterDate(Long accountSmavaId, LocalDate afterDate, Long exceptCurrentStatusId) {
        if (accountSmavaId == null || afterDate == null || exceptCurrentStatusId == null) {
             throw new IllegalArgumentException("Parameter is null");
        }
        Timestamp creationDate = new Timestamp(afterDate.toDateTimeAtStartOfDay().getMillis());
        String select =
                "SELECT count(stateHistory.*)" +
                "  FROM (SELECT state as previousState," +
                "               creation_date as previousCreationDate, "+
                "               LEAD(state) OVER(ORDER BY creation_date) as nextState," +
                "               LEAD(creation_date) OVER(ORDER BY creation_date) as nextStateCreationTime" +
                "          FROM public.customer_process_status cps" +
                "         WHERE cps.account_smava_id = "+ accountSmavaId +
                "           AND cps.id != "+exceptCurrentStatusId +
                "       ) stateHistory" +
                " WHERE stateHistory.nextState NOTNULL" +
                "   AND stateHistory.previousState = 'OFFEN'" +
                "   AND stateHistory.nextState != stateHistory.previousState" +
                "   AND stateHistory.previousCreationDate >= '"+creationDate+"'"+
                "   AND stateHistory.nextStateCreationTime >= '"+creationDate+"'";
        Query query = getPersistenceManager().newQuery(Query.SQL, select);
        query.setResultClass(Long.class);
        query.setUnique(true);
        Long count = (Long)query.execute();
        return count != null && count > 0;
    }



    private AdvisorStatistics create(Advisor advisor, Date creationDate) {
        AdvisorStatistics newStatistic = new AdvisorStatistics();
        newStatistic.setAdvisor(advisor);
        newStatistic.setCreationDate(creationDate);
        newStatistic.setAdvisedLeadCount(0);
        return newStatistic;
    }
}
