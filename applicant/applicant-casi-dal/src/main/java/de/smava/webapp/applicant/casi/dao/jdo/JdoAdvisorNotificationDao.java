package de.smava.webapp.applicant.casi.dao.jdo;

import de.smava.webapp.applicant.casi.dao.AdvisorNotificationDao;
import de.smava.webapp.applicant.casi.domain.AdvisorNotification;
import de.smava.webapp.applicant.casi.domain.DeniedOfflineReason;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import java.util.*;

/**
 * DAO implementation for the domain object 'AdvisorNotifications'.
 *
 * @author generator
 */
@Repository(value = "applicantAdvisorNotificationDao")
public class JdoAdvisorNotificationDao extends JdoBaseDao implements AdvisorNotificationDao {

    private static final Logger LOGGER = Logger.getLogger(JdoAdvisorNotificationDao.class);

    private static final String CLASS_NAME = "AdvisorNotification";
    private static final String STRING_GET = "get";
    private static final String STRING_DELIMITER = " - ";
    private static final String STRING_START = "start:";
    private static final String STRING_RESULT = "result: ";
    private static final String STRING_ID = " id=";

    /**
     * Returns an attached copy of the advisor notification identified by the given id.
     *
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    public AdvisorNotification load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_START + STRING_ID + id);
        }
        AdvisorNotification result = getEntity(AdvisorNotification.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(STRING_GET + CLASS_NAME + STRING_DELIMITER + STRING_RESULT + result);
        }
        return result;
    }

    /**
     * @deprecated Use {@link #load(Long) instead}
     */
    public AdvisorNotification getAdvisorNotification(Long id) {
        return load(id);
    }
    
    /**
     * Checks if unique entity exists in database.
     */
    public boolean exists(Long id) {
    	boolean result = false;
    	if (id != null) {
	    	AdvisorNotification entity = findUniqueEntity(AdvisorNotification.class, "_id == " + id);
	    	result = entity != null;
	    	
	    	if (result) {
	    		result = entity.getId() == id.longValue();
	    	}
    	}
    	return result;
    }

    /**
     * Saves the advisor notification.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     */
    public Long save(AdvisorNotification advisorNotification) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveAdvisorNotification: " + advisorNotification);
        }
        return saveEntity(advisorNotification);
    }

    /**
     * @deprecated Use {@link #save(AdvisorNotification) instead}
     */
    public Long saveAdvisorNotification(AdvisorNotification advisorNotification) {
        return save(advisorNotification);
    }

    /**
     * Deletes an advisor notification, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the advisor notification
     */
    public void deleteAdvisorNotification(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAdvisorNotification: " + id);
        }
        deleteEntity(AdvisorNotification.class, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void deleteAllAdvisorNotifications(List<Long> borrowerSmavaIds, boolean onlyRead) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("deleteAllAdvisorNotifications by borrowerSmavaIds: " + borrowerSmavaIds);
        }
        if(borrowerSmavaIds.isEmpty()){
            return;
        }
        final PersistenceManager pm = getPersistenceManager();
        String deleteQuery = buildDeleteQuery(borrowerSmavaIds, onlyRead);
        Query q = pm.newQuery(Query.SQL, deleteQuery);

        q.execute();

        pm.evictAll(false, DeniedOfflineReason.class);
        pm.evictAll(false, AdvisorNotification.class);
    }

    /**
     * @see AdvisorNotificationDao#findByBrokerageApplicationId(Long)
     */
    @Override
    public Collection<AdvisorNotification> findByBrokerageApplicationId(Long brokerageApplicationId) {
        Query query = getPersistenceManager().newNamedQuery(AdvisorNotification.class, "findByBrokerageApplicationId");
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("brokerageApplicationId", brokerageApplicationId);

        @SuppressWarnings("unchecked")
        Collection<AdvisorNotification> advisorNotifications = (Collection<AdvisorNotification>) query.executeWithMap(params);

        return advisorNotifications;

    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<AdvisorNotification> getByAdvisor(List<Long> borrowerSmavaIds, final Integer offset, final Integer limit, final String sortBy, final Boolean descending) {
        if (borrowerSmavaIds.isEmpty()) {
            return Collections.emptyList();
        } else {
            String selectNotificationsQuery = buildSelectQuery(borrowerSmavaIds, offset, limit, sortBy, descending);
            Query q = getPersistenceManager().newQuery(Query.SQL, selectNotificationsQuery);
            q.setClass(AdvisorNotification.class);

            return (Collection<AdvisorNotification>) q.execute();
        }
    }

    private String buildSelectQuery(List<Long> borrowerSmavaIds, Integer offset, Integer limit, String sortBy, Boolean descending) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT an.id as id,");
        queryBuilder.append(" an.brokerage_application_id as brokerage_application_id,");
        queryBuilder.append(" an.event_type as event_type,");
        queryBuilder.append(" an.timestamp as timestamp,");
        queryBuilder.append(" an.read as read,");
        queryBuilder.append(" an.borrower_smava_id as borrower_smava_id");
        queryBuilder.append(" FROM brokerage.advisor_notification an");
        queryBuilder.append(" INNER JOIN brokerage.account borrower ON an.borrower_smava_id = borrower.customer_number");
        queryBuilder.append(" WHERE");
        appendCustomerNumbersInQuery(queryBuilder, borrowerSmavaIds);

        if(sortBy != null){
            appendOrderQuery(queryBuilder, sortBy, descending);
        }

        if(limit != null){
            appendOffsetQuery(queryBuilder, offset, limit);
        }

        return queryBuilder.toString();
    }

    private void appendCustomerNumbersInQuery(StringBuilder queryBuilder, List<Long> borrowerSmavaIds) {
        queryBuilder.append(" borrower.customer_number IN (");
        for (int i = 0; i < borrowerSmavaIds.size(); i++) {
            queryBuilder.append(borrowerSmavaIds.get(i));
            if (i != borrowerSmavaIds.size() - 1) {
                queryBuilder.append(", ");
            }
        }
        queryBuilder.append(")");
    }

    private void appendOrderQuery(StringBuilder queryBuilder, String sortBy, Boolean descending) {
        queryBuilder.append(" ORDER BY ").append(sortBy);
        queryBuilder.append(descending ? " DESC" : " ASC");
    }

    private void appendOffsetQuery(StringBuilder queryBuilder, Integer offset, Integer limit) {
        queryBuilder.append(" OFFSET ").append(offset);
        queryBuilder.append(" LIMIT ").append(limit);
    }

    @Override
    public long getCountByAdvisor(List<Long> borrowerSmavaIds, boolean notRead) {
        if (borrowerSmavaIds.isEmpty()) {
            return 0;
        } else {
            String countQuery = buildCountQuery(borrowerSmavaIds, notRead);
            Query q = getPersistenceManager().newQuery(Query.SQL, countQuery);
            q.setResultClass(Long.class);
            q.setUnique(true);

            return (Long) q.execute();
        }
    }

    private String buildCountQuery(List<Long> borrowerSmavaIds, boolean notRead){
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT count(an.id) FROM brokerage.advisor_notification an");
        queryBuilder.append(" INNER JOIN brokerage.account borrower ON an.borrower_smava_id = borrower.customer_number");
        queryBuilder.append(" WHERE");
        appendCustomerNumbersInQuery(queryBuilder, borrowerSmavaIds);
        if (notRead){
            appendOnlyReadQuery(queryBuilder, false);
        }
        return queryBuilder.toString();
    }

    private String buildDeleteQuery(List<Long> borrowerSmavaIds, boolean onlyRead){
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("DELETE");
        queryBuilder.append(" FROM brokerage.advisor_notification");
        queryBuilder.append(" WHERE id IN");
        queryBuilder.append(" (SELECT an.id as id FROM brokerage.advisor_notification an");
        queryBuilder.append(" INNER JOIN brokerage.account borrower ON an.borrower_id = borrower.id");
        queryBuilder.append(" WHERE");

        appendCustomerNumbersInQuery(queryBuilder, borrowerSmavaIds);
        if (onlyRead){
            appendOnlyReadQuery(queryBuilder, true);
        }
        queryBuilder.append(")");
        return queryBuilder.toString();
    }

    private void appendOnlyReadQuery(StringBuilder queryBuilder, boolean isRead) {
        queryBuilder.append(" AND an.read = ").append(isRead);
    }
}