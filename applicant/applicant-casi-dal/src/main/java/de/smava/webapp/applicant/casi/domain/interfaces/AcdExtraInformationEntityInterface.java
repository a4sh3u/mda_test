package de.smava.webapp.applicant.casi.domain.interfaces;

import de.smava.webapp.applicant.casi.domain.*;

/**
 * The domain object that represents 'AcdExtraInformations'.
 *
 * @author generator
 */
public interface AcdExtraInformationEntityInterface {

    /**
     * Setter for the property 'loanApplication'.
     *
     * 
     *
     */
    public void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication);

    /**
     * Returns the property 'loanApplication'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication();
    /**
     * Setter for the property 'ingDibaHhc'.
     *
     * 
     *
     */
    public void setIngDibaHhc(Double ingDibaHhc);

    /**
     * Returns the property 'ingDibaHhc'.
     *
     * 
     *
     */
    public Double getIngDibaHhc();
    /**
     * Setter for the property 'page'.
     *
     * 
     *
     */
    public void setPage(Integer page);

    /**
     * Returns the property 'page'.
     *
     * 
     *
     */
    public Integer getPage();
    /**
     * Setter for the property 'smavaSchufaScore'.
     *
     * 
     *
     */
    public void setSmavaSchufaScore(Double smavaSchufaScore);

    /**
     * Returns the property 'smavaSchufaScore'.
     *
     * 
     *
     */
    public Double getSmavaSchufaScore();
    /**
     * Setter for the property 'smavaSchufaState'.
     *
     * 
     *
     */
    public void setSmavaSchufaState(Boolean smavaSchufaState);

    /**
     * Returns the property 'smavaSchufaState'.
     *
     * 
     *
     */
    public Boolean getSmavaSchufaState();
    /**
     * Helper method to get reference of this object as model type.
     */
    public AcdExtraInformation asAcdExtraInformation();
}
