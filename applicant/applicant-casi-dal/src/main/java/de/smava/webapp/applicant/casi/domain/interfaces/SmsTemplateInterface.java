package de.smava.webapp.applicant.casi.domain.interfaces;

import de.smava.webapp.commons.domain.Entity;

import java.io.Serializable;

public interface SmsTemplateInterface extends Serializable, Entity {

    String getKey();

    void setKey(String key);

    String getDisplayName();

    void setDisplayName(String displayName);

    String getContent();

    void setContent(String content);

    Boolean getEditable();

    void setEditable(Boolean editable);

}
