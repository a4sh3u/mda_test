package de.smava.webapp.applicant.casi.dao.jdo;

import de.smava.webapp.applicant.casi.dao.CallJournalDao;
import de.smava.webapp.applicant.casi.domain.CallJournal;
import de.smava.webapp.commons.dao.jdo.JdoGenericDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class JdoCallJournalDao extends JdoGenericDao<CallJournal> implements CallJournalDao {

    private static final Logger LOGGER = Logger.getLogger(JdoCallJournalDao.class);

    @Override
    public CallJournal load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Load CallJournal for ID: " + id);
        }
        CallJournal result = getEntity(CallJournal.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Result of load CallJournal" + result);
        }
        return result;
    }

    @Override
    public Long save(CallJournal callJournal) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("save CallJournal: " + callJournal);
        }
        return saveEntity(callJournal);
    }

    @Override
    public boolean exists(Long id) {
        boolean result = false;
        if (id != null) {
            CallJournal entity = findUniqueEntity(CallJournal.class, "_id == " + id);
            result = entity != null;

            if (result) {
                result = entity.getId() == id.longValue();
            }
        }
        return result;
    }
}
