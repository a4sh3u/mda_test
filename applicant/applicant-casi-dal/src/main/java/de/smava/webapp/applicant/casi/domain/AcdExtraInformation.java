//
//
//
//
//    T H I S   F I L E   W A S   A U T O M A T I C A L L Y   G E N E R A T E D.
//
//
//
//
//    You should only edit code inside the sections marked with
//    "You can insert code here:", which will not get lost,
//    when the file is generated anew.
//
//    If you need to change something outside those sections,
//    you can either change the model, this file is based on:
//
//    model.xml
//
//    Or you can change the template:
//
//    domainModelClass.tpl
//
//
//
//
package de.smava.webapp.applicant.casi.domain;

// !!!!!!!! You can insert code here: !!!!!!!!																														{|imports|bean(acd extra information)}
import de.smava.webapp.applicant.casi.domain.history.AcdExtraInformationHistory;

import java.util.*;


// !!!!!!!! End of insert code section !!!!!!!!


/**
 * The domain object that represents 'AcdExtraInformations'.
 *
 * 
 *
 * @author generator
 */
public class AcdExtraInformation extends AcdExtraInformationHistory  {

    // !!!!!!!! You can insert code here: !!!!!!!!																														{|additonal static members|bean(acd extra information)}
    // !!!!!!!! End of insert code section !!!!!!!!

        protected de.smava.webapp.applicant.brokerage.domain.LoanApplication _loanApplication;
        protected Double _ingDibaHhc;
        protected Integer _page;
        protected Double _smavaSchufaScore;
        protected Boolean _smavaSchufaState;
        
                                    
    /**
     * Setter for the property 'loanApplication'.
     *
     * 
     *
     */
    public void setLoanApplication(de.smava.webapp.applicant.brokerage.domain.LoanApplication loanApplication) {
        _loanApplication = loanApplication;
    }
            
    /**
     * Returns the property 'loanApplication'.
     *
     * 
     *
     */
    public de.smava.webapp.applicant.brokerage.domain.LoanApplication getLoanApplication() {
        return _loanApplication;
    }
                                    /**
     * Setter for the property 'ingDibaHhc'.
     *
     * 
     *
     */
    public void setIngDibaHhc(Double ingDibaHhc) {
        if (!_ingDibaHhcIsSet) {
            _ingDibaHhcIsSet = true;
            _ingDibaHhcInitVal = getIngDibaHhc();
        }
        registerChange("ingDibaHhc", _ingDibaHhcInitVal, ingDibaHhc);
        _ingDibaHhc = ingDibaHhc;
    }
                        
    /**
     * Returns the property 'ingDibaHhc'.
     *
     * 
     *
     */
    public Double getIngDibaHhc() {
        return _ingDibaHhc;
    }
                                    /**
     * Setter for the property 'page'.
     *
     * 
     *
     */
    public void setPage(Integer page) {
        if (!_pageIsSet) {
            _pageIsSet = true;
            _pageInitVal = getPage();
        }
        registerChange("page", _pageInitVal, page);
        _page = page;
    }
                        
    /**
     * Returns the property 'page'.
     *
     * 
     *
     */
    public Integer getPage() {
        return _page;
    }
                                    /**
     * Setter for the property 'smavaSchufaScore'.
     *
     * 
     *
     */
    public void setSmavaSchufaScore(Double smavaSchufaScore) {
        if (!_smavaSchufaScoreIsSet) {
            _smavaSchufaScoreIsSet = true;
            _smavaSchufaScoreInitVal = getSmavaSchufaScore();
        }
        registerChange("smavaSchufaScore", _smavaSchufaScoreInitVal, smavaSchufaScore);
        _smavaSchufaScore = smavaSchufaScore;
    }
                        
    /**
     * Returns the property 'smavaSchufaScore'.
     *
     * 
     *
     */
    public Double getSmavaSchufaScore() {
        return _smavaSchufaScore;
    }
                                    /**
     * Setter for the property 'smavaSchufaState'.
     *
     * 
     *
     */
    public void setSmavaSchufaState(Boolean smavaSchufaState) {
        if (!_smavaSchufaStateIsSet) {
            _smavaSchufaStateIsSet = true;
            _smavaSchufaStateInitVal = getSmavaSchufaState();
        }
        registerChange("smavaSchufaState", _smavaSchufaStateInitVal, smavaSchufaState);
        _smavaSchufaState = smavaSchufaState;
    }
                        
    /**
     * Returns the property 'smavaSchufaState'.
     *
     * 
     *
     */
    public Boolean getSmavaSchufaState() {
        return _smavaSchufaState;
    }
            
    /**
     * Get list of full changes in the object and sub-elements
     */
     public java.util.Set<String> getFullChangeSet() {
         java.util.Set<String> result = new java.util.HashSet<String>();
         result.addAll(getChangeSet());


         if (_loanApplication instanceof de.smava.webapp.commons.domain.Entity && !_loanApplication.getChangeSet().isEmpty()) {
             for (String element : _loanApplication.getChangeSet()) {
                 result.add("loanApplication : " + element);
             }
         }

         return result;
     }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AcdExtraInformation.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n}");
        } else {
            builder.append(AcdExtraInformation.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

    /**
    * Helper method to get reference of this object as model type.
    */
    public AcdExtraInformation asAcdExtraInformation() {
        return this;
    }
}
