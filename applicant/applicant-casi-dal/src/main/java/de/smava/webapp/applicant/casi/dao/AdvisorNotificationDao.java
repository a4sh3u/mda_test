package de.smava.webapp.applicant.casi.dao;

import de.smava.webapp.applicant.casi.domain.AdvisorNotification;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;
import de.smava.webapp.commons.dao.BaseDao;

import java.util.Collection;
import java.util.List;

/**
 * DAO interface for the domain object 'AdvisorNotifications'.
 *
 * @author generator
 */
public interface AdvisorNotificationDao extends BrokerageSchemaDao<AdvisorNotification> {

    /**
     * Returns an attached copy of the advisor notification identified by the given id.
     *
     *
     * @deprecated Use {@link BaseDao#load(Long)} instead.
     * @throws org.springframework.orm.ObjectRetrievalFailureException if the user could not be found
     */
    AdvisorNotification getAdvisorNotification(Long id);

    /**
     * Saves the advisor notification.
     * The invoker is responsible that the passed in object is either detached or
     * still attached to the persisten manager that is bound in the transactional context of this call.
     * <b>Note:</b> The passed in object will be in 'hollow' state after returning!
     *
     * @deprecated Use {@link BaseDao<>#save(T)} instead.
     */
    Long saveAdvisorNotification(AdvisorNotification advisorNotification);

    /**
     * Deletes an advisor notification, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param id the id of the advisor notification
     */
    void deleteAdvisorNotification(Long id);

    /**
     * Deletes all advisor notifications, does not cascade. The invoker is responsible for keeping the
     * referential integrity intact.
     *
     * @param borrowerSmavaIds the list with borrowers ids
     * @param onlyRead delete only already read advisor notifications
     */
    void deleteAllAdvisorNotifications(List<Long> borrowerSmavaIds, boolean onlyRead);

    /**
     * find all AdvisorNotification for BrokerageApplication
     *
     * @param brokerageApplicationId {@link Long} BrokerageApplication ID
     * @return Collection
     */
    Collection<AdvisorNotification> findByBrokerageApplicationId(Long brokerageApplicationId);


    Collection<AdvisorNotification> getByAdvisor(List<Long> borrowerSmavaIds, Integer offset,
                                                 Integer limit, String sortBy, Boolean descending);

    long getCountByAdvisor(List<Long> borrowerSmavaIds, boolean onlyRead);
}
