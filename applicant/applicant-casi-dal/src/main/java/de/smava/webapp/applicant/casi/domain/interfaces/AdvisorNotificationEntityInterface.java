package de.smava.webapp.applicant.casi.domain.interfaces;


import de.smava.webapp.applicant.account.domain.Account;
import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.casi.domain.DeniedOfflineReason;

import java.util.Date;
import java.util.Set;


/**
 * The domain object that represents 'AdvisorNotifications'.
 *
 * @author generator
 */
public interface AdvisorNotificationEntityInterface {

    /**
     * Setter for the property 'brokerageApplication'.
     *
     * 
     *
     */
    void setBrokerageApplication(BrokerageApplication brokerageApplication);

    /**
     * Returns the property 'brokerageApplication'.
     *
     * 
     *
     */
    BrokerageApplication getBrokerageApplication();
    /**
     * Setter for the property 'event type'.
     *
     * 
     *
     */
    void setEventType(String eventType);

    /**
     * Returns the property 'event type'.
     *
     * 
     *
     */
    String getEventType();

    /**
     * Setter for the property 'borrower'.
     *
     *
     *
     */
    void setBorrower(Account borrower);

    /**
     * Returns the property 'borrower'.
     *
     *
     *
     */
    Account getBorrower();

    /**
     * Returns the property 'borrowerSmavaId'.
     *
     *
     *
     */
    Long getBorrowerSmavaId();

    /**
     * Setter for the property 'borrowerSmavaId'.
     *
     *
     *
     */
    void setBorrowerSmavaId(Long smavaId);

    /**
     * Setter for the property 'timestamp'.
     *
     * 
     *
     */
    void setTimestamp(Date timestamp);

    /**
     * Returns the property 'timestamp'.
     *
     * 
     *
     */
    Date getTimestamp();
    /**
     * Setter for the property 'read'.
     *
     * 
     *
     */
    void setRead(Boolean read);

    /**
     * Returns the property 'read'.
     *
     * 
     *
     */
    Boolean getRead();

    void setDeniedReasons(Set<DeniedOfflineReason> deniedOfflineReasons);

    Set<DeniedOfflineReason> getDeniedReasons();
}
