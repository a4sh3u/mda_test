package de.smava.webapp.applicant.casi.domain;

import de.smava.webapp.applicant.casi.domain.interfaces.CallJournalInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;

import java.util.Date;

public class CallJournal extends BrokerageEntity implements CallJournalInterface {

    private Long advisorSmavaRefId;

    /**
     * Here placed account ID from public schema
     */
    private long borrowerId;

    private String destinationNumber;

    private Long loanApplicationId;

    private Date callDateTime;

    private boolean callSuccessful;

    private String failureReason;

    public CallJournal() {
    }

    public CallJournal(long advisorSmavaRefId, long borrowerId, String destinationNumber, Long loanApplicationId,
                       Date callDateTime, boolean callSuccessful, String failureReason) {
        this.advisorSmavaRefId = advisorSmavaRefId;
        this.borrowerId = borrowerId;
        this.destinationNumber = destinationNumber;
        this.loanApplicationId = loanApplicationId;
        this.callDateTime = callDateTime;
        this.callSuccessful = callSuccessful;
        this.failureReason = failureReason;
    }

    public Long getAdvisorSmavaRefId() {
        return advisorSmavaRefId;
    }

    public void setAdvisorSmavaRefId(Long advisorSmavaReId) {
        this.advisorSmavaRefId = advisorSmavaReId;
    }

    @Override
    public long getBorrowerId() {
        return borrowerId;
    }

    public void setBorrowerId(long borrowerId) {
        this.borrowerId = borrowerId;
    }

    @Override
    public String getDestinationNumber() {
        return destinationNumber;
    }

    @Override
    public void setDestinationNumber(String destinationNumber) {
        this.destinationNumber = destinationNumber;
    }

    @Override
    public Long getLoanApplicationId() {
        return loanApplicationId;
    }

    @Override
    public void setLoanApplicationId(Long loanApplicationId) {
        this.loanApplicationId = loanApplicationId;
    }

    @Override
    public Date getCallDateTime() {
        return callDateTime;
    }

    @Override
    public void setCallDateTime(Date callDateTime) {
        this.callDateTime = callDateTime;
    }

    @Override
    public boolean isCallSuccessful() {
        return callSuccessful;
    }

    @Override
    public void setCallSuccessful(boolean callSuccessful) {
        this.callSuccessful = callSuccessful;
    }

    @Override
    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }
}
