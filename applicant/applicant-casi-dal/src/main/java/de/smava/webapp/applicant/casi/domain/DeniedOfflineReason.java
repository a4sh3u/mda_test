package de.smava.webapp.applicant.casi.domain;

import de.smava.webapp.applicant.casi.domain.interfaces.DeniedOfflineReasonEntityInterface;
import de.smava.webapp.applicant.casi.domain.type.DeniedReason;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;


public class DeniedOfflineReason extends BrokerageEntity implements DeniedOfflineReasonEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(DeniedOfflineReason.class);

    protected AdvisorNotification advisorNotification;
    protected DeniedReason reasonCode;
    protected String reasonDescription;
        
     /**
     * Setter for the property 'reason code'.
     */
    @Override
    public void setReasonCode(DeniedReason reasonCode) {
        this.reasonCode = reasonCode;
    }
                        
    /**
     * Returns the property 'reason code'.
     */
    @Override
    public DeniedReason getReasonCode() {
        return reasonCode;
    }

     /**
     * Setter for the property 'reason description'.
     */
    @Override
    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }
                        
    /**
     * Returns the property 'reason description'.
     */
    @Override
    public String getReasonDescription() {
        return reasonDescription;
    }

    /**
     * Returns the property 'reason advisorNotification'.
     */
    @Override
    public AdvisorNotification getAdvisorNotification() {
        return advisorNotification;
    }

    /**
     * Setter for the property 'advisor notification'.
     */
    @Override
    public void setAdvisorNotification(AdvisorNotification advisorNotification) {
        this.advisorNotification = advisorNotification;
    }

    /**
     * A string representation of this object. Mainly for debugging.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(DeniedOfflineReason.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    reasonCode=").append(reasonCode);
            builder.append("\n    reasonDescription=").append(reasonDescription);
            builder.append("\n}");
        } else {
            builder.append(DeniedOfflineReason.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }
}
