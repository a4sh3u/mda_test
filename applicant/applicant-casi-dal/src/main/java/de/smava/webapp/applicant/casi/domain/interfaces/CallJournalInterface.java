package de.smava.webapp.applicant.casi.domain.interfaces;

import de.smava.webapp.commons.domain.Entity;

import java.io.Serializable;
import java.util.Date;

public interface CallJournalInterface extends Serializable, Entity {

    Long getAdvisorSmavaRefId();

    void setAdvisorSmavaRefId(Long advisorSmavaRefId);

    long getBorrowerId();

    void setBorrowerId(long borrowerId);

    String getDestinationNumber();

    void setDestinationNumber(String destinationNumber);

    Long getLoanApplicationId();

    void setLoanApplicationId(Long loanApplicationId);

    Date getCallDateTime();

    void setCallDateTime(Date CallDateTime);

    boolean isCallSuccessful();

    void setCallSuccessful(boolean callSuccessful);

    String getFailureReason();

    void setFailureReason(String failureReason);

}
