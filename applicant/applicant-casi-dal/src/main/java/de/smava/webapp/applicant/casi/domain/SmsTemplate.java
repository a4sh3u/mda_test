package de.smava.webapp.applicant.casi.domain;

import de.smava.webapp.applicant.casi.domain.interfaces.SmsTemplateInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;

public class SmsTemplate extends BrokerageEntity implements SmsTemplateInterface {

    private String key;

    private String displayName;

    private String content;

    private Boolean editable;

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void setKey(String key) {
        this.key = key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

}
