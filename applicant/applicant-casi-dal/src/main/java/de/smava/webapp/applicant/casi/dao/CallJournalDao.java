package de.smava.webapp.applicant.casi.dao;

import de.smava.webapp.applicant.casi.domain.CallJournal;
import de.smava.webapp.applicant.dao.BrokerageSchemaDao;

public interface CallJournalDao extends BrokerageSchemaDao<CallJournal> {

}
