package de.smava.webapp.applicant.casi.dao.jdo;

import de.smava.webapp.applicant.casi.dao.SmsTemplateDao;
import de.smava.webapp.applicant.casi.domain.SmsTemplate;
import de.smava.webapp.commons.dao.jdo.JdoBaseDao;
import de.smava.webapp.commons.domainutil.jdo.OqlTerm;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.jdo.Query;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Repository
public class JdoSmsTemplateDao extends JdoBaseDao implements SmsTemplateDao {

    private static final Logger LOGGER = Logger.getLogger(JdoSmsTemplateDao.class);

    @Override
    public Collection<SmsTemplate> getAllEditableSmsTemplates() {
        Query query = getPersistenceManager().newQuery(SmsTemplate.class);
        query.setFilter("this.editable == true");
        List<SmsTemplate> smsTemplates = (List<SmsTemplate>) query.execute();
        return smsTemplates != null ? smsTemplates : Collections.<SmsTemplate>emptyList();
    }

    @Override
    public Collection<SmsTemplate> getAllSmsTemplates() {
        Query query = getPersistenceManager().newQuery(SmsTemplate.class);
        List<SmsTemplate> smsTemplates = (List<SmsTemplate>) query.execute();
        return smsTemplates != null ? smsTemplates : Collections.<SmsTemplate>emptyList();
    }

    @Override
    public SmsTemplate getSmsTemplateByKey(String key) {
        String where = OqlTerm.newTerm().equals("key", key).toString();
        return findUniqueEntity(SmsTemplate.class, where);
    }

    @Override
    public SmsTemplate load(Long id) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Load SmsTemplate for ID: " + id);
        }
        SmsTemplate result = getEntity(SmsTemplate.class, id);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Result of load SmsTemplate " + result);
        }
        return result;
    }

    @Override
    public Long save(SmsTemplate smsTemplate) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("save SmsTemplate: " + smsTemplate);
        }
        return saveEntity(smsTemplate);
    }

    @Override
    public boolean exists(Long id) {
        return exists(SmsTemplate.class, id);
    }
}
