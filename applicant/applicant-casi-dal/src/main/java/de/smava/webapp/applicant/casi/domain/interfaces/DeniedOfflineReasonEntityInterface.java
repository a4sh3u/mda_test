package de.smava.webapp.applicant.casi.domain.interfaces;

import de.smava.webapp.applicant.casi.domain.AdvisorNotification;
import de.smava.webapp.applicant.casi.domain.type.DeniedReason;

public interface DeniedOfflineReasonEntityInterface {

    void setAdvisorNotification(AdvisorNotification advisorNotification);

    AdvisorNotification getAdvisorNotification();

    void setReasonCode(DeniedReason reasonCode);

    DeniedReason getReasonCode();

    void setReasonDescription(String reasonDescription);

    String getReasonDescription();
}
