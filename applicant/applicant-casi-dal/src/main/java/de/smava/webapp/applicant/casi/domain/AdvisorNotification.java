package de.smava.webapp.applicant.casi.domain;

import de.smava.webapp.applicant.brokerage.domain.BrokerageApplication;
import de.smava.webapp.applicant.casi.domain.interfaces.AdvisorNotificationEntityInterface;
import de.smava.webapp.applicant.domain.BrokerageEntity;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The domain object that represents 'AdvisorNotifications'.
 *
 * 
 *
 * @author generator
 */
public class AdvisorNotification extends BrokerageEntity implements AdvisorNotificationEntityInterface {

    protected static final Logger LOGGER = Logger.getLogger(AdvisorNotification.class);

    protected BrokerageApplication _brokerageApplication;
    protected String _eventType;
    protected de.smava.webapp.applicant.account.domain.Account _borrower;
    protected Long _borrowerSmavaId;
    protected Date _timestamp;
    protected Boolean _read;
    protected Set<DeniedOfflineReason> _deniedOfflineReasons;
                                    
    public void setBrokerageApplication(BrokerageApplication brokerageApplication) {
        _brokerageApplication = brokerageApplication;
    }
            
    public BrokerageApplication getBrokerageApplication() {
        return _brokerageApplication;
    }

    public void setEventType(String eventType) {
        _eventType = eventType;
    }
                        
    public String getEventType() {
        return _eventType;
    }

    public void setBorrower(de.smava.webapp.applicant.account.domain.Account borrower) {
        _borrower = borrower;
    }

    public de.smava.webapp.applicant.account.domain.Account getBorrower() {
        return _borrower;
    }

    public Long getBorrowerSmavaId() {
        return _borrowerSmavaId;
    }

    public void setBorrowerSmavaId(Long smavaId) {
        this._borrowerSmavaId = smavaId;
    }

    public void setTimestamp(Date timestamp) {
        _timestamp = timestamp;
    }
                        
    public Date getTimestamp() {
        return _timestamp;
    }

    public void setRead(Boolean read) {
        _read = read;
    }
                        
    public Boolean getRead() {
        return _read;
    }

    public Set<DeniedOfflineReason> getDeniedReasons() {
        return _deniedOfflineReasons;
    }

    public void setDeniedReasons(Set<DeniedOfflineReason> _deniedOfflineReasons) {
        this._deniedOfflineReasons = _deniedOfflineReasons;
    }

    public java.util.Set<String> getFullChangeSet() {
        Set<String> result = new HashSet<String>(getChangeSet());

         if (_brokerageApplication != null && !_brokerageApplication.getChangeMap().keySet().isEmpty()) {
             for (String element : _brokerageApplication.getChangeMap().keySet() ) {
                 result.add("brokerageApplication : " + element);
             }
         }

         if (_borrower != null && !_borrower.getChangeMap().keySet().isEmpty()) {
             for (String element : _borrower.getChangeMap().keySet()) {
                 result.add("borrower : " + element);
             }
         }

         return result;
     }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (LOGGER.isDebugEnabled()) {
            builder.append(AdvisorNotification.class.getName()).append("(id=)").append(getId()).append(") {");
            builder.append("\n    _eventType=").append(_eventType);
            builder.append("\n}");
        } else {
            builder.append(AdvisorNotification.class.getName()).append("(id=)").append(getId()).append(")");
        }
        return builder.toString();
    }

}
