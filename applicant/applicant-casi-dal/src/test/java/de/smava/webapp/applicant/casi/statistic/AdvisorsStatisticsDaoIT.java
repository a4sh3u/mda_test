package de.smava.webapp.applicant.casi.statistic;

import de.smava.webapp.applicant.casi.dao.AdvisorStatisticsDao;
import de.smava.webapp.brokerage.domain.type.CustomerProcessReason;
import de.smava.webapp.brokerage.domain.type.CustomerProcessState;
import de.smava.webapp.casi.dao.CustomerProcessStatusDao;
import de.smava.webapp.casi.domain.CustomerProcessStatus;
import org.joda.time.LocalDate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static de.smava.webapp.brokerage.domain.type.CustomerProcessReason.*;
import static de.smava.webapp.brokerage.domain.type.CustomerProcessState.*;

@ContextConfiguration("classpath*:dao-context.xml")
@Transactional
@TransactionConfiguration(transactionManager = "jdoTransactionManager")
@RunWith(SpringJUnit4ClassRunner.class)
public class AdvisorsStatisticsDaoIT {
    @Autowired
    private CustomerProcessStatusDao customerProcessStatusDao;
    @Autowired
    private AdvisorStatisticsDao advisorStatisticsDao;
    private final long SMAVA_ID = 1;

    /**
     * story AF-1071 testCase T4506
     */
    @Test
    public void testSwitchingFrom_NEUKUNDE_to_BESTANDSKUNDE_to_newStatus() {
        createStatus(OFFEN, NEUKUNDE, LocalDate.now().minusDays(2).toDate());
        createStatus(OFFEN, BESTANDSKUNDE, LocalDate.now().minusDays(1).toDate());
        createStatus(GENEHMIGT, LAUT_BANK, LocalDate.now().minusDays(1).toDate());
        CustomerProcessStatus exceptCurrentStatus = createStatus(ABGESCHLOSSEN, AUSGEZAHLT, LocalDate.now().minusDays(1).toDate());

        boolean switchedFromOpenState = advisorStatisticsDao.isSwitchedFromOpenStateAfterDate(SMAVA_ID, LocalDate.now().minusDays(28), exceptCurrentStatus.getId());
        Assert.assertEquals(true, switchedFromOpenState);
    }

    /**
     * story AF-1071 testCases T4235 T4231 T4233
     */
    @Test
    public void testSwitchingWithMoreThan28Days() {
        createStatus(OFFEN, NEUKUNDE, LocalDate.now().minusDays(30).toDate());
        createStatus(BEI_BANK, LAUT_BANK, LocalDate.now().minusDays(29).toDate());

        CustomerProcessStatus exceptCurrentStatus = createStatus(GENEHMIGT, AUSGEZAHLT, LocalDate.now().toDate());
        boolean switchedFromOpenState = advisorStatisticsDao.isSwitchedFromOpenStateAfterDate(SMAVA_ID, LocalDate.now().minusDays(28), exceptCurrentStatus.getId());
        Assert.assertEquals(false, switchedFromOpenState);
    }

    /**
     * story AF-1071 testCase T4232
     */
    @Test
    public void testSwitchingWithLessThan28Days() {
        createStatus(OFFEN, NEUKUNDE, LocalDate.now().minusDays(5).toDate());
        createStatus(BEI_BANK, LAUT_BANK, LocalDate.now().minusDays(3).toDate());

        CustomerProcessStatus exceptCurrentStatus = createStatus(GENEHMIGT, AUSGEZAHLT, LocalDate.now().toDate());
        boolean switchedFromOpenState = advisorStatisticsDao.isSwitchedFromOpenStateAfterDate(SMAVA_ID, LocalDate.now().minusDays(28), exceptCurrentStatus.getId());
        Assert.assertEquals(true, switchedFromOpenState);
    }

    /**
     * story AF-1071 testCase T4232
     */
    @Test
    public void testWithPreviousLongHistory() {
        createStatus(OFFEN, NEUKUNDE, LocalDate.now().minusDays(30).toDate());
        createStatus(BEI_BANK, LAUT_BANK, LocalDate.now().minusDays(29).toDate());

        createStatus(OFFEN, BESTANDSKUNDE, LocalDate.now().minusDays(10).toDate());
        createStatus(BEI_BANK, LAUT_BANK, LocalDate.now().minusDays(8).toDate());

        CustomerProcessStatus exceptCurrentStatus = createStatus(GENEHMIGT, AUSGEZAHLT, LocalDate.now().toDate());
        boolean switchedFromOpenState = advisorStatisticsDao.isSwitchedFromOpenStateAfterDate(SMAVA_ID, LocalDate.now().minusDays(28), exceptCurrentStatus.getId());
        Assert.assertEquals(true, switchedFromOpenState);
    }

    @Test
    public void testAtTheLimitOfTime() {
        createStatus(OFFEN, NEUKUNDE, LocalDate.now().minusDays(29).toDate());
        createStatus(BEI_BANK, LAUT_BANK, LocalDate.now().minusDays(28).toDate());

        CustomerProcessStatus exceptCurrentStatus = createStatus(GENEHMIGT, AUSGEZAHLT, LocalDate.now().minusDays(28).toDate());
        boolean switchedFromOpenState = advisorStatisticsDao.isSwitchedFromOpenStateAfterDate(SMAVA_ID, LocalDate.now().minusDays(28), exceptCurrentStatus.getId());
        Assert.assertEquals(false, switchedFromOpenState);
    }

    private CustomerProcessStatus createStatus(CustomerProcessState state, CustomerProcessReason reason, Date creationDate) {
        CustomerProcessStatus status = new CustomerProcessStatus();
        status.setCreatedBy("k");
        status.setCreationDate(creationDate);
        status.setAccountId(1L);
        status.setAccountSmavaId(SMAVA_ID);
        status.setState(state);
        status.setReason(reason);
        customerProcessStatusDao.save(status);
        return status;
    }
}
